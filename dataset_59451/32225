{{featured article}}
{{Infobox coin
| Country             = United States
| Denomination        = Maine Centennial half dollar
| Value               = 50 cents (0.50
| Unit                = [[United States dollar|US dollars]])
| Mass                = 12.5
| Diameter            = 30.61
| Diameter_inh       = 1.20
| Thickness           = 2.15
| Thickness_inch      = 0.08
| Edge                = [[Reeding|Reeded]]
| Silver_troy_oz      = 0.36169
| Composition =
  {{plainlist |
* 90.0% silver
* 10.0% copper
 }}
| Years of Minting    = 1920
| Mintage =50,028 including 28 pieces for the [[United States Assay Commission|Assay Commission]] 
| Mint marks            = None, all pieces struck at the [[Philadelphia Mint]] without mint mark
| Obverse             = Maine centennial half dollar commemorative obverse.jpg
| Obverse Design      = Arms of Maine
| Obverse Designer    = [[Anthony de Francisci]], based on sketches by an unknown artist
| Obverse Design Date   = 1920
| Reverse               = Maine centennial half dollar commemorative reverse.jpg
| Reverse Design        = Pine wreath
| Reverse Designer    = [[Anthony de Francisci]], based on sketches by an unknown artist
| Reverse Design Date   = 1920
}}
The '''Maine Centennial half dollar''' is a [[Early United States commemorative coins|commemorative coin]] struck in 1920 by the [[United States Mint|United States Bureau of the Mint]].  It was sculpted by [[Anthony de Francisci]], following sketches by an unknown artist from the [[U.S. state]] of [[Maine]].

Officials in Maine wanted a commemorative [[half dollar (United States coin)|half dollar]] to circulate as an advertisement for the centennial of the state's admission to the Union, and of the planned celebrations. A bill to allow such a coin passed Congress without opposition, but then the state's centennial commission decided to sell the coins at $1, double the face value.  The [[Commission of Fine Arts]] disliked the proposed design, and urged changes, but Maine officials insisted, and de Francisci converted the sketches to plaster models, from which [[Glossary of numismatics#D|coinage dies]] could be made. 

Fifty thousand pieces, half the authorized mintage, were struck for release to the public. They were issued too late to be sold at the centennial celebrations in [[Portland, Maine|Portland]], but eventually the coins were all sold, though relatively few went to coin collectors.  They list for hundreds to thousands of dollars, depending on condition.
== Inception and legislation ==
Governor [[Carl Milliken]] and the [[Executive Council of Maine|council of Maine]] wanted a [[half dollar (United States coin)|half dollar]] issued to commemorate the centennial of the state's 1820 admission to the Union.  Initially, the idea was to have a circulating commemorative that could advertise the centennial celebrations in Maine. Later, after federal authorizing legislation for the coin was approved by Congress, the centennial commission decided to sell the coins for $1 each, rather than letting them pass from hand to hand in circulation.{{sfn|Bowers|pp=135–36}}

That legislation for a Maine Centennial half dollar had been introduced in the House of Representatives by the state's [[John A. Peters (1864–1953)|John A. Peters]] on February 11, 1920, with the bill designated as H.R. 12460.<ref name ="profile">{{cite web|url=http://li.proquest.com/legislativeinsight/LegHistMain.jsp?searchtype=DOCPAGE&parentAccNo=PL66-199&docAccNo=PL66-199&docType=LEG_HIST&resultsClick=true&id=1469797716831|title=Maine Statehood 100th Anniversary 50-Cent Piece |publisher=ProQuest Congressional|accessdate=July 30, 2016|subscription=yes}}</ref> It was referred to the [[List of defunct United States congressional committees|Committee on Coinage, Weights and Measures]], of which Indiana Congressman [[Albert Vestal]] was the chairman. When the committee met, on February 23, 1920, Congressman Peters told members of the history of the state and citizens' desire to celebrate the centennial, including with a commemorative coin. He stated that he had spoken with the [[Director of the Mint]], [[Raymond T. Baker]], who had told Peters that he and  [[United States Secretary of the Treasury|Treasury Secretary]] [[David F. Houston]] planned to endorse the bill, the text of which had been borrowed from the bill authorizing the 1918 [[Illinois Centennial half dollar]]. Ohio's [[William A. Ashbrook]] recalled that he had been a member of the committee that had approved the Illinois bill; he had favored it and now favored the Maine bill. Minnesota's [[Oscar E. Keller]] asked Peters to confirm there would be no expense to the federal government, which Peters did.{{sfn|House hearings|pp=3–5}} [[Clay Stone Briggs]] of Texas wanted to know if the Maine bill's provisions were identical to those of the Illinois act, and Peters confirmed it.{{sfn|House hearings|pp=2–4, 8–9}} On March 20, Vestal filed a report on behalf of his committee, recommending that the House pass the bill, and reproducing a letter from Houston stating that the Treasury had no objection.<ref>{{cite web|author=House Committee on Coinage, Weights and Measures|url=https://openlibrary.org/books/OL18296332M/|subscription=yes|title=Coinage of 50-Cent Pieces in Commemoration of the Admission of the State of Maine into the Union|date=March 20, 1920}}</ref>

Three coinage bills—Maine Centennial, Alabama Centennial, and Pilgrim Tercentenary—were considered in that order by the House of Representatives on April 21, 1920. After Peters addressed the House in favor of the Maine bill, Connecticut's [[John Q. Tilson]] inquired if the proposed coin would replace the existing design (the [[Walking Liberty half dollar]]) for the rest of the year; Peters explained that it would not, and that only 100,000 coins would bear the commemorative design.<ref>{{USCongRec|1920|5947|date=April 21, 1920}} {{subscription}}</ref>  [[John Franklin Miller (representative)|John Franklin Miller]] of Washington state asked who would bear the expenses of the [[Glossary of numismatics#D|coinage dies]], and Peters responded that the state of Maine would. Virginia's [[Andrew Jackson Montague]] asked if the Treasury Department had endorsed the bill, and Peters informed him that both Houston and Baker had. Vestal asked that the bill be passed, but Ohio's [[Warren Gard]] had questions about what would happen to the coins once they entered circulation; Peters stated that they would, once issued, be treated as ordinary half dollars. In response to questions by Gard, Peters explained that although Maine would pay for the dies, they would become federal government property. Peters added that though there would be no statewide celebration in Maine for the centennial, there would be local observances. Gard had no further questions about the Maine bill (he would also quiz the sponsors of the Alabama and Pilgrim bills), and on Vestal's motion it passed without recorded dissent.<ref name = "cr1">{{USCongRec|1920|5947–5950|date=April 21, 1920}} {{subscription}}</ref>

The following day, April 22, 1920, the House reported its passage of the Maine bill to the Senate.<ref>{{USCongRec|1920|5966|date=April 22, 1920}} {{subscription}}</ref> The bill was referred to the [[Senate Committee on Banking and Currency]]; on April 28, Connecticut's [[George P. McLean]] reported it back with a recommendation that it pass.<ref>{{USCongRec|1920|6202|date=April 28, 1920}} {{subscription}}</ref> On May 3, McLean asked that the three coin bills (Maine, Alabama and Pilgrim) be considered by the Senate immediately, fearing that though they were on the Senate's agenda for that day, they might not be reached, and believing urgent action was required. Utah Senator [[Reed Smoot]] objected: Smoot's attempt to bring up an anti-dumping trade bill out of turn had just been objected to by [[Charles S. Thomas]] of Colorado. Smoot, however, stated if the coin bills had not been reached by about 2:00 pm, there would probably not be any objection.<ref>{{USCongRec|1920|6443|date=May 3, 1920}} {{subscription}}</ref> When McLean tried again to advance the coin bills, Kansas' [[Charles Curtis]] asked if there was any urgency. McLean replied that as the three coin bills were to mark ongoing anniversaries, there was a need to have them authorized and get the production process started. All three bills passed the Senate without opposition<ref>{{USCongRec|1920|6454|date=May 3, 1920}} {{subscription}}</ref> and the Maine bill was enacted with the signature of President [[Woodrow Wilson]] on May 10, 1920.<ref name = "profile" />

== Preparation ==
[[File:Anthony de Francisci in his studio.jpg|thumb|left|[[Anthony de Francisci]]]]
On May 14, 1920, four days after Wilson signed the bill, Director of the Mint Baker sent sketches of the proposed design to the chairman of the [[Commission of Fine Arts]], [[Charles Moore (Commission of Fine Arts)|Charles Moore]], for an opinion as to its merits. The design had been prepared by the officials in charge of the centennial commemoration, and had been given to Baker by Peters. Moore forwarded the sketches to the sculptor-member of the Commission, [[James Earle Fraser (sculptor)|James Earle Fraser]].  Having received no reply, Moore on May 26 sent a telegram to Fraser telling him that the Maine authorities wanted the coins by June 28.  Fraser immediately replied by telegram, that he disliked the design as it was "ordinary", and that it was an error to approve sketches; a plaster model should be made by a sculptor. Moore expanded on this in a letter to Houston the following day, "our new silver coinage has reached a high degree of perfection because it was designed by competent men. We should not return to the low standards which have formerly prevailed."{{sfn|Taxay|pp=39–40}}

Moore in his letter urged a change of design, stating that the sketch, if translated into a coin, "would bring humiliation to the people of Maine".{{sfn|Bowers|pp=135–36}} However, Maine officials refused and insisted on the submitted sketches.{{sfn|Bowers|p=136}} After discussions among Peters, Moore, and various officials, an agreement was reached whereby the sketches would be converted into plaster models, and Fraser engaged his onetime student, [[Anthony de Francisci]], to do the work. The younger sculptor completed the work by early July, and the models were approved by the Commission on July 9.{{sfn|Taxay|pp=40–42}} The Engraving Department at the [[Philadelphia Mint]] created the coin dies utilizing de Francisci's models.{{sfn|Bowers|p=136}} Either Chief Engraver [[George T. Morgan]], or his assistant, future chief engraver [[John R. Sinnock]], changed the moose and pine tree on the coin from being in relief (as in de Francisci's models), to be sunken into the coin. This was probably in an attempt to improve the striking quality of the coins, and if so, had limited success, as the full detail would not appear on many coins.{{sfn|Swiatek & Breen|pp=147, 150}}

== Design ==
[[File:Seal of Maine.svg|thumb|right|The [[Seal of Maine]]]]

The [[obverse and reverse|obverse]] of the Maine Centennial half dollar depicts the arms of Maine, based on [[Seal of Maine|the state's seal]]. At its center is a shield with a pine tree, sunken [[incuse]] into the coin, and below the tree a moose, lying down. The shield is flanked by two male figures, one bearing a scythe and representing Agriculture; the other, supporting an anchor, represents Commerce. Above the shield is the legend ''Dirigo'', Latin for "I direct", and above that a five-pointed star. Below the shield is a scroll with the state's name. Near the rim are <small>UNITED STATES OF AMERICA</small> and <small>HALF DOLLAR</small>. The reverse contains a wreath of pine needles and cones (Maine is known as the Pine Tree State) around <small>MAINE CENTENNIAL 1820–1920</small> as well as the various mottoes required by law to be present on the coinage.{{sfn|Swiatek|pp=110–11}}{{sfn|Swiatek & Breen|p=147}}

[[Numismatics|Numismatist]] [[Don Taxay]], in his history of commemorative coins, speculated that "De {{sic}} Francisci did not altogether favor them".{{sfn|Taxay|p=42}} According to Taxay, the two human figures on the obverse "were too small to retain their beauty after reduction [from the plaster models to coin size] and seem trivial. The reverse, with its wreath of pine cones, is eminently uninspired."{{sfn|Taxay|p=42}} Arlie Slabaugh, in his volume on commemoratives, noted that the half dollar "does not resemble the work by the same sculptor for the [[Peace dollar]] the following year [1921]."{{sfn|Slabaugh|p=41}}

Art historian [[Cornelius Vermeule]] deprecated the Maine half dollar, but did not blame de Francisci, as the piece "was modeled by the sculptor according to required specifications and is therefore not considered typical of his art, or indeed of any art."{{sfn|Vermeule|p=159}} Vermeule stated, "it looks just like a prize medal for a county fair or school athletic day."{{sfn|Vermeule|p=159}} Nonetheless, feeling that de Francisci could have insisted on a more artistic design, Vermeule found "the Maine Centennial was not his shining moment".{{sfn|Vermeule|p=160}}

== Production, distribution, and collecting ==
[[File:NNC-US-1921-1$-Peace dollar.jpg|thumb|left|De Francisci designed the [[Peace dollar]] in 1921.]]

Celebrations for the state's centennial were held in Maine's largest city, [[Portland, Maine|Portland]], on July 4, 1920. Peters had hoped to have the half dollars available for distribution then, but because of the design controversy, they were not. He wrote to Assistant Director of the Mint [[Mary M. O'Reilly]] on July 14, expressing his frustration at the delay and stating that though the Portland festivities had passed, the state could still get some benefit from the coins if they received them during 1920. Otherwise, "we might as well wait for the next Centennial [in 2020] which I judge would be more convenient and in accordance with the speed at which we are going".{{sfn|Flynn|pp=296–297}} He concluded by asking that the Mint let him know of the next obstacle ahead of time. Governor Milliken also wrote, on July 20, reminding Mint officials that the coin was authorized by a special act of Congress, and asking when the first consignment would be ready.{{sfn|Flynn|pp=296–297}}

In the late summer of 1920, a total of 50,028 Maine Centennial half dollars were produced at the Philadelphia Mint, including 28 pieces reserved for inspection and testing at the 1921 meeting of the annual [[Assay Commission]].{{sfn|Swiatek|p=111}} No special care was taken in the minting; they were ejected into bins and many display [[Glossary of numismatics#B|bag marks]].{{sfn|Flynn|p=120}} They were sent to Maine and placed on sale through the Office of the State Treasurer at a price of $1. Thirty thousand sold immediately and they remained on sale through the treasurer's office until all fifty thousand were vended,{{sfn|Swiatek|p=111}} though this did not happen until at least 1929.{{sfn|Bowers|p=137}} Bowers speculated that had the full 100,000 authorized coins been struck, most of the additional quantity would have been returned to the Mint and melted for lack of buyers.{{sfn|Bowers|p=138}} Many pieces were spent in the years after 1920 and entered circulation.{{sfn|Flynn|p=120}}

Relatively few were sold to the coin collecting community, and the majority of surviving specimens display the effects of careless handling. The 2015 deluxe edition of [[Richard S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'' lists the coin at $140 to $685, depending on condition—an exceptional piece sold for $7,050 in 2014.{{sfn|Yeoman|p=1125}}

== References ==
{{Reflist|20em}}

== Sources ==
* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 1992
  | title = Commemorative Coins of the United States: A Complete Encyclopedia
  | publisher = Bowers and Merena Galleries, Inc.
  | location = Wolfeboro, NH
  | isbn = 978-0-943161-35-8
  | ref = {{sfnRef|Bowers}}
  }}
* {{cite book
  | last = Flynn
  | first = Kevin
  | year = 2008
  | title = The Authoritative Reference on Commemorative Coins 1892–1954
  | publisher = Kyle Vick
  | location = Roswell, GA
  | oclc = 711779330
  | ref = {{sfnRef|Flynn}}
  }}
* {{cite book
  | last = Slabaugh
  | first = Arlie R.
  | edition = second
  | year = 1975
  | title = United States Commemorative Coinage
  | publisher = Whitman Publishing
  | location = Racine, WI
  | isbn = 978-0-307-09377-6
  | ref = {{sfnRef|Slabaugh}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | year = 2012
  | title = Encyclopedia of the Commemorative Coins of the United States
  | publisher = KWS Publishers
  | location = Chicago
  | isbn = 978-0-9817736-7-4
  | ref = {{sfnRef|Swiatek}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | last2 = Breen
  | first2 = Walter
  | authorlink2 = Walter Breen
  | year = 1981
  | title = The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954
  | publisher = Arco Publishing
  | location = New York
  | lastauthoramp=y
  | isbn = 978-0-668-04765-4
  | ref = {{sfnRef|Swiatek & Breen}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1967
  | title = An Illustrated History of U.S. Commemorative Coinage
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-01536-3
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
|author=United States House of Representatives Committee on Coinage, Weights and Measures
|title=Authorizing Coinage of Memorial 50-Cent Piece for the State of Alabama
|date=March 26, 1920
|url=http://congressional.proquest.com/congressional/result/pqpresultpage.gispdfhitspanel.pdflink/$2fapp-bin$2fgis-hearing$2ff$2f3$2fb$2f2$2fhrg-1920-cwe-0003_from_1_to_10.pdf+/13227,+66,+H.R.$40$2fapp-gis$2fhearing$2fhrg-1920-cwe-0003$40Hearing$40Hearings+Published$40March+26,+1920$40?pgId=d3a77df2-fdfe-4a18-bd1f-d33d359f7041&rsId=154395B5B2D
|publisher=[[United States Government Printing Office]]
|ref={{sfnRef|House hearings}}
|subscription=yes
}}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, MA
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2015
  | title = [[A Guide Book of United States Coins]]
  | edition = 1st Deluxe
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-4307-6
  | ref = {{sfnRef|Yeoman}}
  }}

==External links==
* {{Commons category-inline|Maine Centennial half dollar}}

{{Coinage (United States)}}
{{Portal bar|Arts|Business and economics|Maine|Numismatics|United States|Visual arts}}

[[Category:1920 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:Fifty-cent coins]]
[[Category:United States silver coins]]