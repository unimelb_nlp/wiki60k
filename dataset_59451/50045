{{Infobox television episode
 |image_size = 280px
 |image = Game of Thrones-S03-E05 Kissed by Fire.jpg
 |caption = In The Hound's trial by combat with Beric Dondarrion, his fear of fire is shown.
 |title = Kissed by Fire
 |series = [[Game of Thrones]]
 |season = 3
 |episode = 5
 |director = [[Alex Graves]]
 |writer = [[Bryan Cogman]]
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = Anette Haellmigk
 |editor = Katie Weiland
 |production = 
 |airdate = {{Start date|2013|4|28}}
 |length = 58 minutes
 |guests = *[[Diana Rigg]] as Olenna Tyrell
* [[Gwendoline Christie]] as Brienne of Tarth
* [[Ian McElhinney]] as Barristan Selmy
* [[Michael McElhatton]] as Roose Bolton
* [[Paul Kaye]] as Thoros of Myr
* [[Richard Dormer]] as Beric Dondarrion
* [[Mackenzie Crook]] as Orell
* [[Anton Lesser]] as Qyburn
* [[Clive Russell]] as Brynden Tully
* [[Tobias Menzies]] as Edmure Tully
* [[Kristofer Hivju]] as Tormund Giantsbane
* [[Noah Taylor]] as Locke
* [[Finn Jones]] as Loras Tyrell
* John Stahl as Rickard Karstark
* [[Tara Fitzgerald]] as Selyse Baratheon
* [[Kerry Ingram]] as Shireen Baratheon
* [[Nathalie Emmanuel]] as Missandei
* [[Jacob Anderson]] as Grey Worm
* [[Philip McGinley]] as Anguy
* [[Daniel Portman]] as Podrick Payne
* [[Will Tudor]] as Olyver
* [[Dean-Charles Chapman]] as Martyn Lannister
* Timothy Gibbons as Willem Lannister
* Shaun Blaney as a Young Captive
 |season_list =
 |prev = [[And Now His Watch Is Ended]]
 |next = [[The Climb (Game of Thrones)|The Climb]]
 |episode_list = [[Game of Thrones (season 3)|''Game of Thrones'' (season 3)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}

"'''Kissed by Fire'''" is the fifth episode of the [[Game of Thrones (season 3)|third season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 25th episode of the series. Directed by [[Alex Graves]] and written by [[Bryan Cogman]], it aired on April 28, 2013.

The episode won the Primetime Emmy Award for Outstanding Make-up for a Single-Camera Series (Non-Prosthetic) at the [[65th Primetime Creative Arts Emmy Awards]].

==Plot==

===At Dragonstone===
Queen Selyse Baratheon ([[Tara Fitzgerald]]) is visited by her husband, King Stannis ([[Stephen Dillane]]), who admits his infidelity to her. He is surprised when she tells him that Melisandre has told her everything, and that she not only has no problem with it, but encourages it as service to the Lord of Light. Stannis then visits his daughter, Princess Shireen Baratheon ([[Kerry Ingram]]). When she asks about the battle and Ser Davos Seaworth ([[Liam Cunningham]]), Stannis tells her that Davos has been imprisoned for treason. Later, Shireen sneaks down to the dungeons to visit Davos and bring him a book, but Davos admits to her that he is illiterate. She begins teaching him to read, using a book on Aegon I's conquest of Westeros.

===In the Riverlands===
Thoros of Myr ([[Paul Kaye]]) leads the Brotherhood in prayer before Lord Beric Dondarrion ([[Richard Dormer]]) and "the Hound" Sandor Clegane ([[Rory McCann]]) begin their trial by combat. Dondarrion lights his sword on fire, frightening the Hound, due to his [[pyrophobia]]. Nonetheless, the Hound soon overpowers Dondarrion and kills him. When a furious Arya Stark ([[Maisie Williams]]) moves to kill the Hound, she is stopped by Gendry ([[Joe Dempsie]]). The three are then astounded to find that Dondarrion has been resurrected by Thoros, who frees the Hound, accepting that the judgment by battle has proved that the Lord of Light has more for him to do. Later, Arya finds Gendry repairing Dondarrion's armor, and he tells her that he intends to stay with the Brotherhood and work for them as a smith. After leaving Gendry, Arya talks with Thoros about taking her to Riverrun, before Dondarrion joins them. Thoros tells Arya that the Lord of Light has resurrected Dondarrion six times.

===At Riverrun===
The captives Martyn and Willem Lannister are slain by Lord Rickard Karstark (John Stahl) and his men. King Robb Stark ([[Richard Madden]]) confronts Lord Karstark and orders he be locked in the dungeon, and his men who assisted him be hanged. When he orders Karstark be killed for his treason, Queen Talisa Stark ([[Oona Chaplin]]), Lady Catelyn Stark ([[Michelle Fairley]]), and Lord Edmure Tully ([[Tobias Menzies]]) entreat him to hold Karstark as a prisoner, to keep the Karstark men loyal to their cause. However, Robb denies them, and personally executes Karstark.

Outraged with the death of their leader, the Karstark forces abandon the Northern army. Robb tries to plan a strategy for continuing the war against the Lannisters. He then tells Talisa that his new plan is to attack Casterly Rock, the home of the Lannisters. To replace the lost Karstark forces, he intends to forge an alliance with Lord Walder Frey; the man who controls the Twins, and whose daughter he was to marry in exchange for [[Baelor#At the Twins|letting his army cross]] on their way to rescue Robb's Father, Eddard.

===At Harrenhal===
Locke ([[Noah Taylor]]) delivers Jaime Lannister ([[Nikolaj Coster-Waldau]]) and Brienne of Tarth ([[Gwendoline Christie]]) to Lord Roose Bolton ([[Michael McElhatton]]). Bolton, furious that his men have maimed a valuable hostage, frees Brienne and orders his men to take Jaime to see Qyburn ([[Anton Lesser]]), a maester who was stripped of his chain. Qyburn treats Jaime's right forearm; Jaime requests that he do so without analgesics. Later, Jaime is taken to the baths, where Brienne is already bathing. Jaime then tells her of Robert's Rebellion, the "Mad King" Aerys Targaryen, and the Mad King's plot to burn all of King's Landing using stores of wildfire hidden by a pyromancer through the city during the Sack of King's Landing. Jaime reveals that he slew the Mad King to save the city, its people and his own father's life.

===Beyond the Wall===
Orell pries information about the Wall patrols from Jon ([[Kit Harington]]). Jon lies that there are a thousand men left guarding the Wall; Tormund ([[Kristofer Hivju]]) accepts his claim, but threatens to kill him if he lies. Shortly afterward, Ygritte ([[Rose Leslie]]) steals Jon's sword Longclaw and has him chase her into a cave, where she disrobes and convinces him to break his Night's Watch vows and make love with her.

===In Slaver's Bay===
While on the march, Ser Jorah Mormont ([[Iain Glen]]) and Ser Barristan Selmy ([[Ian McElhinney]]) discuss the siege of Pyke during Balon Greyjoy's first rebellion against the throne. Jorah pries into Barristan's motives for joining Daenerys Targaryen's cause, and tries to ascertain if Barristan is aware that Jorah was working as a spy for Varys, under King Robert Baratheon and his Small Council, when he first joined Daenerys. Barristan replies that he did not serve on Robert's Small Council, and appears to be unaware of Jorah's secret.

Daenerys ([[Emilia Clarke]]) assembles the officers for her Unsullied army. She tells them to select their own leader from among them, and they select a man called Grey Worm ([[Jacob Anderson]]). Upon learning that their names were given to them as slaves to remind them of their standing, she tells them that they are free to choose their own names. Grey Worm tells her that he will keep his current name, as it is the one he had when she liberated the Unsullied and thus he considers it lucky.

===In King's Landing===
Queen Cersei Lannister ([[Lena Headey]]) asks Lord Petyr Baelish ([[Aidan Gillen]]) for assistance in ridding King's Landing of the Tyrells, whom she claims do not hold the Crown's best interests at heart. Later, Lady Sansa Stark ([[Sophie Turner (actress)|Sophie Turner]]) and Lady Margaery Tyrell ([[Natalie Dormer]]) watch Ser Loras Tyrell ([[Finn Jones]]) practicing his swordplay. After the practice, Loras and his squire, Olyvar (Will Tudor), have sex. Unbeknownst to Loras, Olyvar is a spy for Lord Baelish and reports to him the Tyrells' plan to marry off Sansa. Baelish then meets with Sansa to discuss their journey to the Vale, but she tells him that she wants to stay in King's Landing.

Tyrion Lannister ([[Peter Dinklage]]) has a meeting with Lady Olenna ([[Diana Rigg]]) regarding the rising cost of the upcoming royal wedding. She agrees to pay for half of the wedding, the news of which Tyrion takes to his father, Lord Tywin ([[Charles Dance]]). Tywin tells Tyrion that he has learned of the Tyrells' plot to marry Sansa to Loras, and that he intends to act first by having Tyrion wed Sansa. Tyrion objects, but to no avail. Cersei is pleased by her brother's discomfort at the notion, until Tywin tells her that she will soon be wed to Ser Loras. Having spent years unhappily married in an arranged marriage she tries to convince him to spare her, but he refuses.

==Production==

===Writing===
[[File:Bryan Cogman Fan Photograph (cropped).jpg|right|thumb|upright|Series veteran [[Bryan Cogman]] wrote the episode, his third episode of the series.]]
"Kissed by Fire" is the third episode in the series written by the co-producer and executive story editor [[Bryan Cogman]], after the first season's "[[Cripples, Bastards, and Broken Things]]" and the second's "[[What Is Dead May Never Die]]". Cogman is the member of the writing team entrusted with keeping the show's [[Bible (writing)|bible]] and mapping the story arcs with those of the original books for each season.

The sections of [[George R. R. Martin]]'s novel ''[[A Storm of Swords]]'' adapted in the episode include parts from chapters 20, 21, 27, 32, 35, 38 and 40 (Tyrion III, Catelyn III, Jon III, Jaime IV, Arya VI, Jaime V, and Arya VII).<ref name="westeros">{{cite web|url=http://www.westeros.org/GoT/Episodes/Entry/Kissed_by_Fire/Book_Spoilers/|title=EP305:Kissed by Fire|work=Westeros.org|first=Elio|last=Garcia|accessdate=29 April 2013}}</ref>

The scenes with Stannis' wife and daughter were written to present the characters, whose introduction had been delayed in the show since the beginning of season 2. The idea of Queen Selyse conserving the fetuses of her stillborn sons in glass, absent in the original novels, was a notion that Cogman came up with while writing the episode.<ref>{{cite web|url=http://www.hbo.com/game-of-thrones#/game-of-thrones/episodes/3/25-kissed-by-fire/interview/bryan-cogman.html|title=Interview With Bryan Cogman|work=HBO|accessdate=29 April 2013}}</ref>

Cogman enjoyed that the episode he was assigned to write included several fan-favorite scenes, and involved a lot of material with the child actors: "The kids are always my favorite characters to write... Maybe it’s because I’m so fond of the actors who play them, and I’ve watched them grow up for the past four years."<ref>{{cite web|url=http://geek-news.mtv.com/2012/10/02/interview-bryan-cogman-inside-hbos-game-of-thrones/|title=Interview: Bryan Cogman On 'Inside HBO's Game of Thrones,' And Season Three|last=Zalben|first=Alex|publisher=[[MTV Geek]]|date=2 October 2012|accessdate=26 April 2013}}</ref> He wrote all the Arya scenes before starting with the other storylines.

Initially the episode did not include any scene with Daenerys, but early in pre-production some scenes originally written by [[David Benioff]] and [[D. B. Weiss]] for [[The Climb (Game of Thrones)|the next episode]] were more moved into the script. The confrontation between Jon Snow and Orell was written and included by Benioff and Weiss later during production.<ref>{{cite web|url=http://winteriscoming.net/2013/04/ask-a-got-writer-bryan-cogman-on-the-writing-process-robb-and-talisa-and-renlys-peach/|title=Ask a GoT Writer: Bryan Cogman on the writing process, Robb and Talisa, and Renly’s peach|work=WinterIsComing.net|date=24 April 2012|accessdate=26 April 2013}}</ref>

===Casting===
The episode introduces Stannis' family with actresses [[Tara Fitzgerald]] and [[Kerry Ingram]] as queen Selyse Baratheon and princess Shireen Baratheon, respectively. Selyse had briefly appeared in [[The North Remembers|the first season 2 episode]] during the burning of the gods at the Dragonstone beach, played by an uncredited extra. [[Jacob Anderson]] also debuts playing Grey Worm, the commander of the Unsullied.

===Filming locations===
{{Double image|right|Myvatn_Iceland_01.jpg|170|Grjotagja_cave_Iceland_1.JPG|150|The scenes beyond the Wall were filmed at the shores of [[Mývatn|lake Mývatn]], and at the nearby cave of [[Grjótagjá]].}}
Most of the episode was shot in the sets built in The Paint Hall studios in Belfast. Also in Northern Ireland, the [[Pollnagollum|Pollnagollum cave]] in Belmore Forest was used to film parts of the hideout of the Brotherhood, and the gardens of [[Gosford Castle]] served as the Riverrun exteriors where Lord Karstark was beheaded.<ref>{{cite web|url=http://winteriscoming.net/2012/08/day-26-gosford-castle-as-riverrun/|title=Day 26: Gosford Castle as Riverrun|work=WinterIsComing.net|date=3 August 2012|accessdate=29 April 2013}}</ref>

The scenes with Daenerys were filmed in Morocco, and the ones with Jon in Iceland. The Wildlings camp was built by the shores of [[Mývatn|lake Mývatn]], with its distinctive vertical lava formations clearly seen. The nearby grotto where Jon and Ygritte have sex is [[Grjótagjá|cave Grjótagjá]]; however, the cave was used mainly for establishing shot of Jon Snow and Ygrite in the cave, and most of this scene was filmed in the studio.<ref>{{cite news |url=http://www.telegraph.co.uk/travel/destinations/europe/iceland/articles/game-of-thrones-iceland-tour-filming-locations/ |title=Iceland's most spectacular Game of Thrones filming locations |work=The Daily Telegraph |first= Oliver|last= Smith |date=June 7, 2016 }}</ref> The thermal water pool of the cave is actually used for bathing and is a popular tourist attraction.<ref>{{cite web|url=http://reykjavik.travel/index.php/item/167#.UX5mWkpYB6M|title=More on Game of Thrones filming in Iceland|work=Eeykjavik Travel|date=16 November 2012 |accessdate=29 April 2013}}</ref>

Finally, two Croatian exteriors appear in the episode: the conversation between Cersei and Littlefinger takes place at the inner terrace of [[Lovrijenac|Fort Lovrijenac]], and Littlefinger's later visit to Sansa was filmed at the [[Trsteno Arboretum]].

==Reception==

===Ratings===
"Kissed by Fire" set a new ratings record for the series, with 5.35 million viewers for its first airing and a 2.8 share of adults aged 18 to 49.<ref>{{cite web|url=http://tvbythenumbers.zap2it.com/2013/04/30/sunday-cable-ratings-game-of-thrones-wins-night-nba-playoffs-vikings-the-client-list-mad-men-veep-more/180165/|title=Sunday Cable Ratings: 'Game of Thrones' Wins Night, NBA Playoffs, 'Vikings', 'The Client List', 'Mad Men', 'Veep' & More|last=Bibel|first=Sara|publisher=TV by the Numbers|date=April 30, 2013|accessdate=April 30, 2013}}</ref> In the United Kingdom, the episode was seen by 0.959 million viewers on [[Sky Atlantic]], being the channel's second highest-rated broadcast that week.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (29 April - 5 May 2013)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref>

===Critical reception===
"Kissed by Fire" received positive critical reviews after airing, with particular praise going to [[Nikolaj Coster-Waldau]] for his performance. [[Review aggregator]] [[Rotten Tomatoes]] surveyed 21 reviews of the episode and judged 100% of them to be positive. The website's critical consensus reads, "Despite lacking the big action reveals of the previous episode, 'Kissed by Fire' is anchored by a devastatingly intimate scene between Brienne and Jamie, and plenty of Lannister intrigue."<ref>{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s03/e05/|title=Kissed by Fire|work=[[Rotten Tomatoes]]|accessdate=May 3, 2016}}</ref> [[IGN]]'s Matt Fowler gave "Kissed by Fire" a 9.5/10, his highest rating of the season, writing "No dragons this week, but Game of Thrones still gave us some of its best material ever."<ref name="ign">{{cite web|url=http://ca.ign.com/articles/2013/04/29/game-of-thrones-kissed-by-fire-review|title=Game of Thrones: "Kissed by Fire" Review|last=Fowler|first=Matt|publisher=IGN|date=28 April 2013|accessdate=29 April 2013}}</ref> Reviewing for [[The A.V. Club]], David Sims gave the episode an "A-", commenting on how despite the lack of shocking moments like those of the last episode, the show delivers quality in its slower, dialogue-driven scenes.<ref name="av1">{{cite web|url=http://www.avclub.com/articles/kissed-by-fire-for-newbies,96589/|title="Kissed by Fire" (for newbies)|last=Sims|first=David|publisher=[[The A.V. Club]]|date=April 28, 2013|accessdate=April 29, 2013}}</ref> Todd VanDerWerff of The A.V. Club gave the episode a "B+".<ref name="av2">{{cite web|url=http://www.avclub.com/articles/kissed-by-fire-for-experts,96587/|title="Kissed by Fire" (for experts)|last=VanDerWerff|first=Todd|publisher=[[The A.V. Club]]|date=April 28, 2013|accessdate=April 29, 2013}}</ref> Sean T. Collins of the ''[[Rolling Stone]]'' magazine also gave an overwhelmingly positive review, calling it a "nearly flawless" episode, praising especially [[Maisie Williams]]' acting in the scenes with Arya and the Brotherhood.<ref>{{cite web|first=Sean T. |last=Collins |url=http://www.rollingstone.com/movies/news/game-of-thrones-recap-perfect-game-20130429 |title='Game of Thrones' Delivers a Near-Flawless Episode &#124; Movies News |work=[[Rolling Stone]] |date=2013-04-29 |accessdate=2013-05-11}}</ref>

===Accolades===
The episode won the Primetime Emmy Award for Outstanding Make-up for a Single-Camera Series (Non-Prosthetic) at the [[65th Primetime Creative Arts Emmy Awards]].

{| class="wikitable sortable plainrowheaders"
|-
! Year
! Award
! Category
! Nominee(s)
! Result
|-
| 2013
|scope="row"| [[65th Primetime Creative Arts Emmy Awards|Primetime Creative Arts Emmy Awards]]
|scope="row"| Outstanding Makeup for a Single-Camera Series (Non-Prosthetic)
|scope="row"| Paul Engelen and Melissa Lackersteen
| {{won}}
|-
| rowspan=2| 2014
|scope="row"| [[Hollywood Post Alliance|Hollywood Post Alliance Awards]]
|scope="row"| Outstanding Color Grading – Television
|scope="row"| Joe Finley
| {{nom}}
|-
|scope="row"| [[American Society of Cinematographers]]
|scope="row"| One-Hour Episodic Television Series
|scope="row"| Anette Haellmigk
| {{nom}}
|}

== References ==
{{Reflist|2}}

== External links ==
{{wikiquotepar|Game_of_Thrones_(TV_series)#Kissed_by_Fire_.5B3.05.5D|Kissed by Fire}}
* [http://www.hbo.com/game-of-thrones/episodes/3/25-kissed-by-fire/index.html "Kissed by Fire"] at [[HBO.com]]
* {{IMDb episode|2178788}}
* {{tv.com episode|game-of-thrones/kissed-by-fire-2676031}}

{{Game of Thrones Episodes}}

[[Category:2013 American television episodes]]
[[Category:Game of Thrones episodes]]