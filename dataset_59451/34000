{{Infobox single
| Name           = Touch Me I'm Sick
| Cover          = Touch Me I'm Sick.jpg
| Artist         = [[Mudhoney]]
| B-side         = "Sweet Young Thing Ain't Sweet No More"
| Released       = {{Start date|1988|8|1}}
| Format         = [[7" vinyl]]
| Recorded       = March 1988 at [[Reciprocal Recording]] in Seattle
| Genre          = [[Grunge]]
| Length         = {{Duration|m=2|s=23}}
| Writer         = *[[Mark Arm]], [[Steve Turner (guitarist)|Steve Turner]], [[Dan Peters]], [[Matt Lukin]]
| Label          = [[Sub Pop]]
| Producer       = [[Jack Endino]], Mudhoney
| This single    = "'''Touch Me I'm Sick'''"<br>(1988)
| Next single    = "[[Touch Me I'm Sick/Halloween|Touch Me I'm Sick"/"Halloween]]"<br>(1988)
}}
"'''Touch Me I'm Sick'''" is a song by the American [[alternative rock]] band [[Mudhoney]]. It was recorded in March 1988 at Seattle's [[Reciprocal Recording]] studio with producer [[Jack Endino]]. "Touch Me I'm Sick" was released as Mudhoney's debut single by [[independent record label]] [[Sub Pop]] on August 1, 1988. The song's lyrics, which feature dark humor, are a sarcastic take on issues such as disease and violent sex.

When it was first released, "Touch Me I'm Sick" was a hit on the [[independent music|indie]] circuit; it remains the band's most recognizable song. The heavily [[distortion (music)|distorted]] and fuzzy guitars, snarling vocals, blunt bass line and energetic drumming contributed to a dirty sound that influenced many local musicians, and helped develop the nascent Seattle [[grunge]] scene. According to [[Allmusic]], "the song's raw, primal energy made it an instant anthem which still stands as one of [grunge's] all-time classics".<ref name="amgsong">Huey, Steve. [{{Allmusic|class=song|id=touch-me-im-sick-t3650221|pure_url=yes}} "Touch Me I'm Sick" song review]. [[Allmusic]]. Retrieved on November 27, 2010.</ref>

==Origins and recording==
According to Mudhoney vocalist [[Mark Arm]], "Touch Me I'm Sick" originated from a discussion with [[Sub Pop]] owner [[Bruce Pavitt]], who "said: 'Hey, you sing about dogs. You sing about being sick. You got a shtick, it'll take you to the top.' And he basically gave us five chords, but he said don't use more than three within one song."<ref>Maslin, Janet. [http://query.nytimes.com/gst/fullpage.html?res=9401E4DC1F38F93BA35752C1A960958260 " Successful in Seattle: Turning Grunge to Gold"]. ''[[The New York Times]]''. November 8, 2006. Retrieved on July 1, 2007.</ref> Arm also states that "Touch Me I'm Sick" was a catchphrase around which the band built a song.<ref>Azerrad, p. 426</ref>

Mudhoney recorded the song at Seattle's [[Reciprocal Recording]] studio in March 1988, three months after the band's formation.<ref name="flipside">Cantu, Bob. [http://www.operationphoenixrecords.com/flipsidemudhoney.html "Flipside Interviews Mudhoney"]. ''[[Flipside (fanzine)|Flipside]]''. February 1998. Retrieved on July 1, 2007.</ref> Producer [[Jack Endino]] was surprised by how noisy the sessions were and how dirty the band wanted the guitars to sound; "for the most part, I just sort of stood back and let them go at it".<ref>"Left of the Dial". Episode 6, ''[[Seven Ages of Rock]]''. [[BBC Worldwide]] & [[VH1]] Classic. 2007.</ref> Guitarist [[Steve Turner (guitarist)|Steve Turner]] said that the band selected two of their "grungiest" songs for the single.<ref>Higgins, JR. "Mudhoney: No Nonsense Seattle Supergrunge". ''Backlash''. December 1988.</ref> "Sweet Young Thing Ain't Sweet No More" was to be the [[A-side and B-side|A-side]] of the single and "Touch Me I'm Sick" the B-side before, in drummer [[Dan Peters]]'s words, "that all got flipped around".<ref name="rocket">Ehrbar, Joe. "In Fuzz We Trust: Mudhoney". ''[[The Rocket (newspaper)|The Rocket]]''. January 2000.</ref><ref>[http://ogami.subpop.com/bands/mudhoney/march_to_fuzz/standard/14.html ''March to Fuzz'' liner notes]. "Sweet Young Thing Ain't Sweet No More" recording information. [[Sub Pop]]. Retrieved on July 1, 2007.</ref>

==Music and lyrics==
{{Listen
|filename=Mudhoney - Touch Me I'm Sick.ogg
|title="Touch Me I'm Sick"
|description=Sample of "Touch Me I'm Sick", illustrating the song's high tempo, main guitar riff, heavy use of distortion and frenetic drumming.}}

"Touch Me I'm Sick" has a straightforward [[garage punk]] structure with a simple repeating [[power chord]] [[riff]] played at a high [[tempo]].<ref name="amgsong"/> This is accompanied by a blunt bass line and frenetic drumming.<ref name="rocket"/> The song's dirty sound was produced using an Electro-Harmonix [[Big Muff]] [[Effects pedal|distortion pedal]], which is augmented by a second guitar providing more [[Distortion (guitar)|distortion]].<ref name="amgsong"/> Music writer Brian J. Barr referred to this noisy sound as "the sonic equivalent of an amplified comb scraping against paper".<ref name="barr">Barr, Brian J. Mudhoney promotional biography for ''[[Under a Billion Suns]]''. Sub Pop. March 7, 2006.</ref>

Critics have noted a [[The Stooges|Stooges]] influence in "Touch Me I'm Sick", typical of Mudhoney's early material.<ref name="flipside"/><ref name="amgalbum">Huey, Steve. [{{Allmusic|class=album|id=r13598|pure_url=yes}} "Review: ''Superfuzz Bigmuff Plus Early Singles''"]. Allmusic. Retrieved on June 30, 2007.</ref> Turner said: "In retrospect, it's [[The Yardbirds]]' 'Happenings Ten Years Time Ago' by way of The Stooges' 'Sick of You'. At the time I was trying for the stuttering [[R&B]] guitar of The Nights and Days."<ref name="linernotes">[http://ogami.subpop.com/bands/mudhoney/march_to_fuzz/standard/14.html ''March to Fuzz'' liner notes]. "Touch Me I'm Sick" recording information. Sub Pop. Retrieved on July 1, 2007.</ref> The song is also reminiscent of the [[hardcore punk]] of [[Black Flag (band)|Black Flag]].<ref name="BBC">[http://www.bbc.co.uk/music/sevenages/events/alternative-rock/sub-pops-first-single-mudhoneys-touch-me-im-sick/ "Sub Pop's first single: Mudhoney's 'Touch Me I'm Sick'"]. [[BBC]]'s ''Seven Ages of Rock''. Retrieved on July 2, 2007.</ref> In his book ''Loser: The Real Seattle Music Story'', Clark Humphrey accuses the song of being a copy of "The Witch" by [[The Sonics]]. The band have dismissed this claim, and questioned the writer's knowledge of music.<ref name="flipside"/>

Arm's lyrics, according to critic Steve Huey, are a rant about "disease, self-loathing, angst, and dirty sex".<ref name="amgsong"/> In an essay called "'Touch Me I'm Sick': Contagion as Critique in Punk and Performance Art", Catherine J. Creswell suggests that some of the lyrics refer to AIDS. According to Creswell, "In declaring 'Well, I'm diseased and I don't mind' and changing the final refrain to 'Fuck Me, I'm Sick!' the speaker declares himself to be the viral, 'AIDS-bearing,' 'polluting' person of contemporary fantasy".<ref name="genxegesis">Ulrich, John M. ;Harris, Andrea L. ''Genxegesis: Essays on Alternative Youth (sub)culture''. [[University of Wisconsin Press|Popular Press]]. 2003. pp. 86–87.</ref> Creswell, who also believes the song parodies the theme of seduction in contemporary rock music, points to lyrics that refer to impotence ("If you don't come, if you don't come, if you don't come, you'll die alone!") and violent possession or forcing ("I'll make you love me till the day you die!").<ref name="genxegesis"/> However, Arm says that he had not put much thought into the lyrics; while performing the song in concerts, he sometimes changes them to amuse himself.<ref name="rocket"/>

Another feature of "Touch Me I'm Sick" that has been commented upon is Arm's vocals. Huey refers to them as a "hysterical screech", and "snarling, demonic howls".<ref name="amgsong"/><ref name="amgalbum"/> Journalist Joe Ehrbar says that Arm begins the song with a "burp", before singing with a "nasally howl".<ref name="rocket"/> Creswell considers Arm's "overboard" vocals to mock a variety of rock stereotypes: the punk snarl, the "woozy slur" of [[hard rock]], [[garage rock]] "yea-ahs", R&B-style wails and a "[[Jerry Lee Lewis]] shudder".<ref name="genxegesis"/>

==Release and reception==
[[File:Mark Arm.jpg|thumb|upright|left|Vocalist [[Mark Arm]] in 2007. Arm had initially dismissed the song as a "B-side toss off".<ref name="linernotes"/>]]
"Touch Me I'm Sick" was released on August 1, 1988, as a [[7" vinyl]]. It was Mudhoney's debut release. Initially, [[Sub Pop]] released 800&nbsp;clear coffee-brown vinyl copies, 200&nbsp;black vinyl copies and a few assorted vinyl color copies of the single.<ref name="azzerad426">Azerrad, pp. 426–27</ref> The limited release numbers were inspired by another [[independent music|indie]] label, [[Amphetamine Reptile]]. Sub Pop owners [[Bruce Pavitt]] and [[Jonathan Poneman]] reasoned the limited supply would increase demand, and utilized different colors of vinyl in order to rationalize further limited pressings and to increase the single's allure as a collectible item.<ref name="azzerad426"/> The record, which came in a white paper bag without a picture sleeve, had an inscription on the A-side: "What does the word 'crack' mean to you?". The B-side sticker featured the toilet picture that later became the cover art of the sleeved second edition of the single.<ref>[http://www.subpop.com/releases/mudhoney/singles/touch_me_im_sick_sweet_young_thing "Touch Me Im Sick"/"Sweet Young Thing"]. Sub Pop. Retrieved on July 1, 2007.</ref>

According to Pavitt, "It was just a limited edition, maybe 800 pieces, but people all over America started raving about it. People that we really respected."<ref>Rose, Cynthia. "Sub Pop: See Label For Details&nbsp;— An Interview with Bruce Pavitt". ''[[Dazed & Confused (magazine)|Dazed & Confused]]''. 1994.</ref>
The single was an indie hit in Seattle, and "Touch Me I'm Sick" became Mudhoney's most recognizable song.<ref name="amgsong"/> When asked in an interview about the sales figures of the single, Turner replied, "The first [pressing sold] 1,000, then 3,000 of the reissue, then it was out of print for a while; then they made 2,000 more and those are probably gone."<ref>LaVella, Mike. [http://www.operationphoenixrecords.com/mudhoneyinterview.html "''Maximum Rocknroll'' Interviews Mudhoney"]. ''[[Maximum Rocknroll]]''. August 1990. Retrieved on July 1, 2007.</ref> The single's success caught the band by surprise; Arm had initially dismissed the song as a "B-side toss off".<ref name="linernotes"/> "Touch Me I'm Sick" and B-side "Sweet Young Thing Ain't Sweet No More" were later included on the Mudhoney compilation albums ''[[Superfuzz Bigmuff Plus Early Singles]]'' (1990) and ''[[March to Fuzz]]'' (2000).

===Sonic Youth cover===
Prior to the release of the "Touch Me I'm Sick" single, Pavitt sent a five-song Mudhoney tape to New York [[alternative rock]] band [[Sonic Youth]] for the members' opinions. Sonic Youth immediately proposed a [[split album|split single]] where each band [[cover version|covered]] the other.<ref>Azerrad, p. 428</ref> Sonic Youth covered "Touch Me I'm Sick" while Mudhoney covered Sonic Youth's "Halloween". "[[Touch Me I'm Sick/Halloween]]" was released as a limited edition 7"&nbsp;vinyl by Sub Pop in December 1988. The cover was included in the deluxe edition of ''[[Daydream Nation]]'' (2007), and offers a female perspective of the song with bassist [[Kim Gordon]] handling the vocals.

==Legacy==
{{quote box|width=23em|"I got really depressed ... I was just going to be a stripper for the rest of my life and never have a band again. But I heard Mudhoney's 'Touch Me I'm Sick' one night, and I was saved. I knew that I could scream on cue like that."|— [[Courtney Love]] on her decision to give up stripping and pursue a career in music<ref>Wise, Nick. ''Kurt and Courtney: Talking''. Omnibus press. p. 16. 2004. ISBN 978-1-84449-098-1</ref>}}

Following the success of the "Touch Me I'm Sick" single in the Seattle area, Sub Pop positioned Mudhoney as the flagship band of their roster and undertook heavy promotion for the group.<ref>Olsen, Matt. [http://www.webcitation.org/5Xaak2i7I "The Story of Sub Pop (or How I Lost the Weight and How I Plan to Keep It Off)"]. Sub Pop. Retrieved on April 28, 2008.</ref> The band's early material received airplay on [[college radio]] and influenced many local musicians, including [[Kurt Cobain]] of [[Nirvana (band)|Nirvana]].<ref name="BBC"/> In a few years, many Seattle grunge bands signed to [[Record label#Major labels|major labels]] and broke into the mainstream, achieving mass popularity. Although Mudhoney never attained this level of mainstream acceptance, according to Allmusic's Mark Deming, the band's "indie-scene success laid the groundwork for the movement that would (briefly) make Seattle, WA, the new capital of the rock & roll universe".<ref>Deming, Mark. [{{Allmusic|class=artist|id=p4977|pure_url=yes}} Mudhoney biography]. Allmusic. Retrieved on April 28, 2008.</ref>

Since its release, "Touch Me I'm Sick" has been accorded classic status within the grunge genre.<ref name="barr"/> Writing for Allmusic, Steve Huey described the song as "the ultimate grunge anthem" and "a crucial and vastly influential touchstone in the evolution of the grunge movement, virtually defining the term".<ref name="amgsong"/><ref name="amgalbum"/> For its northwestern rock exhibit, the [[Rock and Roll Hall of Fame]] requested the song's original lyrics sheet. Since it did not exist—Arm briefly considered making a fake one by writing down the lyrics, crumpling the sheet, and then burning the edges—the band instead donated Turner's old [[Big Muff]] pedals.<ref>"Generation Spokesmodel". ''B.B Gun'' #4. 1997.</ref>

"Touch Me I'm Sick" remains Mudhoney's most popular song. Joe Ehrbar called it "the song most of us would come to know [the band] by".<ref name="rocket"/> Arm considers the track to be Mudhoney's highwater mark,
<blockquote>There's something special about that first single, we were never quite able to recapture that sound. I don't know if it was the guitars or the recording. It was just a really gnarly, gnarly guitar sound. We've gotten some since, but they've been a different kind. I think it had more to do with the actual electromagnetic chemistry of what was going through our amps that day. It was just a cool, fried-out sound.<ref>La Briola, John. "Here's Mud in Your Ear". ''[[Westword]]''. August 2001.</ref></blockquote>

The song was referenced in the 1992 film ''[[Singles (1992 film)|Singles]]'', which is set against the backdrop of the Seattle grunge scene. The fictional band in the film, Citizen Dick, perform a song called "Touch Me I'm Dick"—a wordplay on Mudhoney's song.<ref name="flipside"/> In 2003, [[Charles Peterson (photographer)|Charles Peterson]] published a book of photography titled ''Touch Me I'm Sick''. It features black-and-white photographs of bands (including Mudhoney) and concerts, and focuses on the alternative music scene of the 1980s and 1990s.<ref>[http://www.powells.com/cgi-bin/biblio?show=Hardcover:New:1576871916:40.00 "Touch Me I'm Sick by Charles Peterson"]. [[Powell's Books]]. Retrieved on May 4, 2008.</ref>

===Accolades===
{|class="wikitable"
|-
! Year
! Publication/<br />author
! Country
! Accolade
! Rank
|-
| 2002
| ''[[NME]]''
| United Kingdom
| 100 Greatest Singles Of All Time<ref>"100 Greatest Singles Of All Time". ''[[NME]]''. November 2002.</ref>
|align="center"| 99
|-
| 2004
| ''[[Kerrang!]]''
| United Kingdom
| 666 Songs You Must Own (Grunge)<ref>"666 Songs You Must Own". ''[[Kerrang!]]''. November 2004.</ref>
|align="center"| 5
|-
| 2005
| ''[[Blender magazine|Blender]]''
| United States
| The 500 Greatest Songs Since You Were Born (1981–2005)<ref>[https://web.archive.org/web/20090410102336/http://www.blender.com/lists/68125/500-greatest-songs-since-you-were-born-451-500.html?p=3 "The 500 Greatest Songs Since You Were Born: 351-400"]. ''[[Blender magazine|Blender]]''. October 2005. Retrieved on August 24, 2009.</ref>
|align="center"| 396
|-
| 2006
| [[Toby Creswell]]
| Australia
| ''1001 Songs: The Great Songs of All Time''<ref>[[Toby Creswell|Creswell, Toby]]. ''1001 Songs: The Great Songs of All Time and the Artists, Stories and Secrets Behind Them''. Thunder's Mouth Press. 2006. pp. 708–09. ISBN 1-56025-915-9</ref>
|align="center"| *
|-
|}
<small><nowiki>*</nowiki> denotes an unordered list</small>

==Track listing==
;7" single (SP18)
Both songs credited to [[Mark Arm]], [[Steve Turner (guitarist)|Steve Turner]], [[Dan Peters]] and [[Matt Lukin]].
#"Touch Me I'm Sick"&nbsp;– 2:23
#"Sweet Young Thing Ain't Sweet No More"&nbsp;– 3:35

==Notes==
{{Reflist|30em}}

==References==
*Azerrad, Michael. ''[[Our Band Could Be Your Life|Our Band Could Be Your Life: Scenes from the American Indie Underground, 1981–1991]]''. Little Brown and Company, 2001. ISBN 0-316-78753-1

{{Mudhoney}}
{{featured article}}

[[Category:1988 singles]]
[[Category:Debut singles]]
[[Category:Mudhoney songs]]
[[Category:Sub Pop singles]]
[[Category:Songs about sexuality]]
[[Category:1988 songs]]