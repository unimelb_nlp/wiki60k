{{Infobox airport
| name         = Reid–Hillview Airport of Santa Clara County
| nativename   = 
| nativename-a = 
| nativename-r = 
| image        = KRHV 31Approach.jpg
| caption      = View of the airport from the south
| image2       = KRHV airport diagram.png
| caption2     = Reid–Hillview Airport diagram
| FAA          = RHV
| LID          = 
| IATA         = RHV
| ICAO         = KRHV
| type         = Public
| owner-oper   = 
| owner        = [[Santa Clara County, California]]
| operator     = 
| city-served  = 
| location     = [[San Jose, California]]
| built        = <!-- for military airports -->
| used         = <!-- for military airports -->
| metric-elev  = 
| elevation-m  = 40.5
| elevation-f  = 133
| coordinates  = {{coord|37|19|58|N|121|49|11|W|type:airport_region:US-CA|display=inline,title}}
| website      = 
| r1-number    = 13L/31R
| r1-length-m  = 945
| r1-length-f  = 3100
| r1-surface   = [[Asphalt]]
| r2-number    = 13R/31L
| r2-length-m  = 945
| r2-length-f  = 3099
| r2-surface   = Asphalt
}}
'''Reid–Hillview Airport of Santa Clara County''' {{Airport codes|RHV|KRHV|RHV}} is in the eastern part of [[San Jose, California|San Jose]],<ref>{{cite gnis|id=1653953|name=Reid–Hillview Airport of Santa Clara County|accessdate=2009-05-04}}</ref> in [[Santa Clara County, California|Santa Clara County]], [[California]].  The airport is owned by Santa Clara County and is near the Evergreen district of San Jose where aviation pioneer [[John J. Montgomery]] experimented with gliders in 1911. Reid–Hillview Airport is also the official general aviation airport for the 2015 Super Bowl in Levis Stadium (nearby Santa Clara, CA).

Reid–Hillview is a [[general aviation]] airport; there is no scheduled airline service. As with most general aviation airports [[air charter]] operations are available. The airport has a [[control tower]] that operates 7 a.m. to 10 p.m. local time. The [[Federal Aviation Administration|FAA]] classifies Reid–Hillview as a [[reliever airport]] for [[San Jose International Airport]].<ref>{{cite web
  | title = National Plan of Integrated Airport Systems (NPIAS) Reports
  | url = http://www.faa.gov/airports/planning_capacity/npias/reports/
  | publisher = [[Federal Aviation Administration]]
  | accessdate = 2008-02-22 }}
</ref>

== History ==
Groundbreaking for Reid–Hillview airport came in 1937. Bob and Cecil Reid built the Garden City Airport in 1935, which was quickly closed to make room for [[U.S. Route 101]]. Their second site was northwest of the Hillview golf course, hence the name. Until 1946 the single runway was unpaved.

Reid–Hillview was a single runway airport until 1965, when a second runway was added. The control tower opened in October 1967.<ref>{{cite web
  | title = A Brief History of Reid–Hillview Airport
  | url = http://www.niceairaviation.com/about_reidhillview_airport.htm
  | publisher = Nice Air Aviation
  | accessdate = 2008-02-22 }}</ref>

The airport was the origin for an emergency supply airlift to the [[Watsonville Municipal Airport]] following the [[1989 Loma Prieta earthquake]], after mountain and coastal roads were blocked, cutting off [[Santa Cruz, California|Santa Cruz]] and [[Watsonville, California|Watsonville]] from relief efforts by ground.  The Watsonville Airport estimates that it received 100 tons of supplies via the airlift during the week following the quake.<ref>{{cite web
  | title = Watsonville's Airports: Past – Present – Future
  | publisher = Watsonville Airport
  | url = http://www.watsonvilleairport.com/history.html
  | accessdate = 2008-02-29 }}
</ref> John McAvoy and Bill Dunn of the Reid–Hillview Airport Association received the 1990 Grand Award from the [[Metropolitan Transportation Commission (San Francisco Bay Area)|Bay Area's Metropolitan Transportation Commission]] for organizing the airlift.<ref>{{cite web
  | title = MTC Awards: Grand Award
  | publisher = [[Metropolitan Transportation Commission (San Francisco Bay Area)|Metropolitan Transportation Commission]]
  | date = 2007-03-22
  | url = http://www.mtc.ca.gov/about_mtc/awards/winners/grandaward.htm
  | accessdate = 2008-02-29 }}
</ref>

In 2010 [[San Jose State University]]'s Aviation program relocated to Reid–Hillview.  The University operates out of the Swift Building, where it holds classes during the academic year.

== Fixed-base operators (FBOs) ==

[[File:KRHV.jpg|thumb|right|220px|Overlooking transient parking and the control tower.]]
At Reid–Hillview, [[Fixed-base operator|fixed-base operators (FBOs)]] compete for fuel sales, aircraft rentals and/or flight training.
*[http://www.flying20.com/ The Flying 20's]
*[http://www.air-accord.com/ Air Accord]
*[http://www.aerodynamicaviation.com/ AeroDynamic Aviation], formerly [[Amelia Reid]] Aviation
*[http://www.niceairaviation.com/ Nice Air Aviation]
*[http://www.squadron2.com/ Squadron 2 Flying Club]
*[http://tradewindsaviation.com/ Trade Winds Aviation]
*[http://www.victoryhangar.com/ Victory Aero Maintenance]
*[http://aerialavionics.com Aerial Avionics]
*[http://sjfuel.weebly.com San Jose Fuel Company]

== Future airport plans ==

The Santa Clara County wrote up a [http://www.countyairports.org/docs/MasterPlan/RHV_Masterplan-complete.pdf 120+ page document] containing information for future plans and expansions for Reid–Hillview. Around the mid-2000s the County announced they would demo the second story of the Airport Terminal to make room for new offices for the Airport Administration. They failed to follow up on this plan and the second story has been sitting idle ever since.

Included in the document, the County also planned on adding small expansions to the runway to allow turboprops and small business jets to land. Although turboprops and small business jets still land in the current runway, this minor expansion will allow a larger flow of these type of aircraft to land.

== Ground transportation ==
[[County Route G21 (California)|Capitol Expressway (County Route G21)]] is at the entrance to the airport at Cunningham Ave.

=== Rental cars/taxi ===
Enterprise car rental is available on the field next to the Airport Shoppe.  Nice Air also has rental vehicles.

=== Bus ===
The [[Santa Clara Valley Transportation Authority]] (VTA) has its [[Eastridge Transit Center]] across Tully Road at [[Eastridge Center|Eastridge Mall]], about 1/2 mile walk southbound along Capitol Expressway.

VTA has plans for a [[bus rapid transit]] line from the [[Diridon Station|Diridon train station]] in downtown San Jose to the [[Eastridge Transit Center]]. Further plans with less certain funding include a light rail extension from the [[Alum Rock (VTA)|Alum Rock station]] to Eastridge. In both cases, the segment along Capitol Expressway would serve Reid–Hillview Airport.

=== Other airport amenities ===
As of a few years ago,{{when|date=April 2016}} Reid–Hillview Airport had vending machines, a pilot's lounge and break room. [[Santa Clara County, California|Santa Clara County]] passed a new law stating the terminal would no longer allow unhealthy food or drinks to be sold inside. This means coffee and all the vending machines that used to be there would no longer exist in the terminal. Now, all that remains is a water drinking fountain and bathrooms. However, there is still a seating area inside the terminal.

== See also ==
{{commons category}}
*[[Alum Rock Airport]], historical airport nearby in Alum Rock from 1919 to approximately 1936
*[[Amelia Reid]]
*[[List of attractions in Silicon Valley]]
*[[Palo Alto Airport]]
*[[San Jose International Airport]]
*[[San Martin Airport]]

== References ==
{{reflist}}

==External links==
*[http://www.countyairports.org/rhv.html Reid–Hillview Airport] (official site)
*[http://www.rhvaa.org/ Reid–Hillview Airport Association]
*[http://www.eaa62.org/ Experimental Aircraft Association (EAA) Chapter 62] – Local chapter of the [[Experimental Aircraft Association]]
*[http://www.santaclaravalley99s.org/ Santa Clara Valley Chapter of Ninety-Nines] – Local chapter of the [[Ninety-Nines]] International organization for women pilots
*[[Civil Air Patrol]] Squadrons based at RHV: search & rescue, aerospace education
**[http://sq80.cawgcap.org/ Squadron 80] – Senior squadron, adult volunteers
**[http://sq36.cawgcap.org/ Squadron 36] – Cadet squadron, youth volunteers
*[http://www.aerodynamicaviation.com/webcam/ live webcam at AeroDynamic Aviation]
* Photos and video
**[https://www.flickr.com/search/?q=reid-hillview+airport search for "reid-hillview airport" at Flickr]
**[http://www.webshots.com/search?query=reid-hillview+airport search for "reid-hillview airport" at WebShots]
**[https://www.youtube.com/results?search_query=reid-hillview+airport search for "reid-hillview airport" at YouTube]
*[http://www.epa.gov/fedrgstr/EPA-IMPACT/2004/December/Day-21/i27823.htm  Approval Of Noise Compatibility Program for Reid–Hillview Airport, San Jose, CA] at [[United States Environmental Protection Agency|Environmental Protection Agency]]
*{{FAA-airport|ID=RHV|use=PU|own=PU|site=02203.*A}}
*{{FAA-diagram|05591}}
{{US-airport-ga|RHV}}

{{Airports in the San Francisco Bay Area}}

{{DEFAULTSORT:Reid-Hillview Airport}}
[[Category:Buildings and structures in San Jose, California]]
[[Category:Transportation in San Jose, California]]
[[Category:Airports in Santa Clara County, California]]
[[Category:Airports established in 1937]]