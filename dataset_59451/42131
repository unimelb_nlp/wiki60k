{{Infobox company
 | name   = Aeromarine Plane and Motor Company
 | logo   = 
 | slogan         = 
 | type   = 
 | fate           = 
 | Predecessor    = 
 | successor      = 
 | foundation     = 1914
 | defunct        = 1930
 | location       = [[Keyport, New Jersey]], [[United States]]
 | industry       = [[Aerospace]]
 | products       = Aircraft and aircraft engines
 | key_people     = [[Inglis M. Upperçu]], founder <br> [[Harry Bruno]]
 | num_employees  = 
 | parent         = 
 | subsid         = 
}}
The '''Aeromarine Plane and Motor Company''' was an early American aircraft manufacturer founded by [[Inglis M. Upperçu]] which operated from 1914 to 1930. From 1928 to 1930 it was known as the '''Aeromarine-Klemm Corporation'''.

==History==
The beginnings of the company dated to 1908, when Uppercu began to finance aeronautical experiments by a small firm at [[Keyport, New Jersey|Keyport]], [[New Jersey]]. In 1914, Aeromarine itself was founded at Keyport with Uppercu as president.<ref>Angelucci, p. 35.</ref> Aeromarine built mostly military [[seaplane]]s and [[flying boat]]s, the most significant of which were the models [[Aeromarine 39|39]] and [[Aeromarine 40|40]].
The company broke new ground in aviation by offering some of the first regularly scheduled flights. Aviation promoter [[Harry Bruno]] worked with Aeromarine to commercialize the transportation potential of airflight.

In [[1928 in aviation|1928]], the firm renamed itself Aeromarine-Klemm Corporation and began producing mostly [[Klemm]] aircraft designs, until the Great Depression forced its closure in [[1930 in aviation|1930]].<ref>Angelucci, p. 35.</ref>

The firm also built aero engines. After Aeromarine itself went out of business, the production of Aeromarine engines was continued by the [[Uppercu-Burnelli Corporation]].<ref>Angelucci, p. 35.</ref>

*Factory Site {{coord|40.443097|N|74.189394|W|}}

[[File:Seaplane in flight.jpg|thumb|right|[[Aeromarine 75]]]]

A subsidiary "Aeromarine Sightseeing and Navigation Company" merged with Florida West Indies Airways, Inc to form the [[Aeromarine West Indies Airways]], later renamed to "Aeromarine Airways". it operated the Aeromarine 75 and Aeromarine 85 aircraft.

== Aircraft ==

{| class="wikitable" 
|-
|+ Summary of aircraft built by Aeromarine<ref>{{cite journal|title=none|magazine=Skyways|date=April 2001}}</ref>
|- 
! Model name
! First flight
! Number built
! Type

|-
|align=left| [[Aeromarine Model B]]
|align=center| {{avyear|1910}}
|align=center| 
|align=left| Canard
|-
|align=left| [[Aeromarine xx]]
|align=center| {{avyear|1914}}
|align=center| 
|align=left| Canard
|-
|align=left| [[Aeromarine 39]]
|align=center| {{avyear|1917}}
|align=center| 50
|align=left| two-seat land-or-water based trainer
|-
|align=left| [[Aeromarine M-1]]
|align=center| {{avyear|1917}}
|align=center| 6
|align=left| advanced trainer
|-
|align=left| [[Aeromarine 700]]
|align=center| {{avyear|1917}}
|align=center| 2
|align=left| experimental torpedo bomber, powered by Aeromarine engine
|-
|align=left| [[Aeromarine DH-4B]]
|align=center| {{avyear|1917}}
|align=center| 125
|align=left| conversion of [[Airco DH.4]] for de Havilland
|-
|align=left| [[Aeromarine 40]]
|align=center| {{avyear|1918}}
|align=center| 50
|align=left| two-seat flying boat trainer
|-
|align=left| [[Aeromarine 50]]
|align=center| {{avyear|1919}}
|align=center| 
|align=left| Limousine Flying Boat
|-
|align=left| [[Aeromarine ML]]
|align=center| {{avyear|1920}}
|align=center| 
|align=left| Experimental
|-
|align=left| [[Aeromarine A.S.]]
|align=center| {{avyear|1920}}
|align=center| 3
|align=left| Seaplane fighter - Ship's Scout
|-
|align=left| [[Aeromarine S.S.]]
|align=center| {{avyear|1920}}
|align=center| 3
|align=left| Seaplane fighter - Sea Scout
|-
|align=left| Aeromarine NBS-1
|align=center| {{avyear|1920}}
|align=center| 25
|align=left| production of [[Martin NBS-1]] for US Army
|-
|align=left| [[Aeromarine Model 60|Aeromarine 60]]
|align=center| {{avyear|1920}}
|align=center| 
|align=left| Flying Boat
|-
|align=left| [[Aeromarine 80]]
|align=center| {{avyear|1920}}
|align=center| 1
|align=left| Conversion of [[Curtiss HS-2L]]
|-
|align=left| [[Aeromarine 85]]
|align=center| {{avyear|1920}}
|align=center| 1
|align=left| Conversion of [[Curtiss HS-2L]]
|-
|align=left| [[Aeromarine WM]]
|align=center| {{avyear|1922}}
|align=center| 
|align=left| Mailplane
|-
|align=left| [[Aeromarine Sportsman]]
|align=center| {{avyear|1922}}
|align=center| 
|align=left| Mailplane, version of Aeromarine 39-B
|-
|align=left| [[Aeromarine PG-1]]
|align=center| {{avyear|1922}}
|align=center| 3
|align=left| Ground attack design by US Army [[Engineering Division]]
|-
|align=left| [[Aeromarine 52]]
|align=center| {{avyear|1922}}
|align=center| 
|align=left| Civil Transport
|-
|align=left| [[Aeromarine 55]]
|align=center| {{avyear|1922}}
|align=center| 
|align=left| Civil Transport
|-
|align=left| [[Aeromarine L.D.B XII]]
|align=center| {{avyear|1923}}
|align=center| not built
|align=left| Night bomber
|-
|align=left| [[Aeromarine L.D.B XIII]]
|align=center| {{avyear|1923}}
|align=center| not built
|align=left| Night bomber
|-
|align=left| Aeromarine 75
|align=center| {{avyear|1920}}
|align=center| 6-8
|align=left| Conversion of [[Felixstowe F5L]] for civilian use<ref>[http://airandspace.si.edu/collections/artifact.cfm?object=nasm_A19240007000  "Felixstowe (NAF) F-5-L (hull only) - Long Description "] {{webarchive |url=https://web.archive.org/web/20140714182830/http://airandspace.si.edu/collections/artifact.cfm?object=nasm_A19240007000 |date=July 14, 2014 }} Smithsonian National Air and Space Museum</ref>
|-
|align=left| [[Aeromarine AM-1]]
|align=center| {{avyear|1923}}
|align=center| 1
|align=left| Mailplane
|-
|align=left| [[Aeromarine AM-3]]
|align=center| {{avyear|1923}}
|align=center| 1
|align=left| Mailplane, modification of AM-1 design
|-
|align=left| [[Aeromarine AMC]]
|align=center| {{avyear|1924}}
|align=center| 1
|align=left| Passenger seaplane with aluminium hull
|-
|align=left| [[Aeromarine AM-2]]
|align=center| {{avyear|1924}}
|align=center| 1
|align=left| Mailplane, slight modification of AM-1 design
|-
|align=left| [[Aeromarine EO]]
|align=center| {{avyear|1924}}
|align=center| 1
|align=left| Sportplane
|-
|align=left| [[Aeromarine AT]]
|align=center| {{avyear|1924}}
|align=center| 0
|align=left| Proposed army transport
|-
|align=left| [[Aeromarine ASM]]
|align=center| {{avyear|1924}}
|align=center| 
|align=left| Sport
|-
|align=left| [[Aeromarine CO-L]]
|align=center| {{avyear|1924}}
|align=center| 
|align=left| Observation aircraft
|-
|align=left| [[Aeromarine ADA]]
|align=center| {{avyear|1924}}
|align=center| 
|align=left| Agricultural aircraft
|-
|align=left| [[Aeromarine Messenger]]
|align=center| {{avyear|1924}}
|align=center| 1
|align=left| Experimental
|-
|align=left| [[Aeromarine BM-1]]
|align=center| {{avyear|1920s}}
|align=center| Not built
|align=left| Proposed mailplane
|-
|}

==Engines==

* [[Aeromarine L-6|L-6]],
* [[Aeromarine AL]]
* [[Aeromarine NAL]]
* [[Aeromarine S]]
* [[Aeromarine S-12]]
* [[Aeromarine AR-3]], radial 3, 4.125x4.00=160.37 (2.63L)  40-55&nbsp;hp@2050-2400rpm (later re-issued as Lenape Papoose)
* [[Aeromarine AR-3-40]]
* [[Aeromarine AR-5]]
* [[Aeromarine AR-7]]
* [[Aeromarine AL-24]]
* [[Aeromarine B-9]]
* [[Aeromarine B-45]]
* [[Aeromarine B-90]]
* [[Aeromarine D-12]]
* [[Aeromarine K-6]]
* [[Aeromarine L-6]] inline 6, 4.25 x 6.50 = 553.27cu in (9.07l)  130-145&nbsp;hp @ 1700rpm
* [[Aeromarine L-6-D]] (direct drive)
* [[Aeromarine L-6-G]] (geared)
* [[Aeromarine L-8]]
* [[Aeromarine RAD]]
* [[Aeromarine T-6]]
* [[Aeromarine U-6]]
* [[Aeromarine U-6-D]]
* [[Aeromarine U-8]]
* [[Aeromarine U-8-873]]
* [[Aeromarine U-8D]]
* [[Aeromarine 85hp]]
* [[Aeromarine 100hp]]

==Notes==
{{reflist}}

==References==
* Angelucci, Enzo. ''The American Fighter: The Definitive Guide to American Fighter Aircraft From 1917 to the Present''. New York: Orion Books, 1987. ISBN 0-517-56588-9.
* Gunston, Bill. (1993). ''World Encyclopaedia of Aircraft Manufacturers''. Naval Institute Press: Annapolis, Maryland. p.&nbsp;13
*{{citation |url=http://www.timetableimages.com/ttimages/aeromfl.htm |work=The Aeromarine Website |title=Fleet list of Aeromarine aircraft |last=Daniel Kusrow  |date=2012 |publisher=Daniel Kusrow & Bjorn Larsson }}

==External links==
* [http://www.timetableimages.com/ttimages/aerom.htm The Aeromarine Website]
 
{{Aeromarine}}

[[Category:Companies based in Monmouth County, New Jersey]]
[[Category:Defunct companies based in New Jersey]]
[[Category:Defunct aircraft engine manufacturers of the United States]]
[[Category:Defunct aircraft manufacturers of the United States]]


{{aero-corp-stub}}