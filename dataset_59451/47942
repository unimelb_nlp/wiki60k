{{Infobox website
| name               = Foodie.fm
| logo               = 
| logo_size          = <!-- default 250px -->
| logo_alt           = 
| logo_caption       = <!-- or: | logocaption =  -->
| screenshot         = 
| collapsible        = <!-- set as "on", "y", etc, otherwise omit/leave blank -->
| collapsetext       = <!-- collapsible area's heading (default "Screenshot"); omit/leave blank if collapsible not set -->
| background         = <!-- for collapsetext heading; default grey (gainsboro) -->
| screenshot_size    = <!-- default 300px -->
| screenshot_alt     = 
| caption            = 
| url                =  {{URL|www.Foodie.fm}} 
| slogan             = 
| commercial         = Yes
| type               = Online shopping
| registration       = Yes
| language           = English
| num_users          = 
| content_license    = <!-- or: | content_licence = -->
| programming_language = 
| owner              = <!-- or: | owners = -->
| author             = <!-- or: creator / authors / creators -->
| editor             = <!-- or: | editors = -->
| launch_date        = {{start date and age|2009|df=yes/no}} 
| revenue            = 
| alexa              = <!-- {{increase}} {{steady}} {{decrease}} [http://www.alexa.com/siteinfo/example.com ##] (US/Global MM/YYYY) -->
| ip                 = 
| current_status     = 
| footnotes          = 
}}

{{Orphan|date=March 2014}}

'''Foodie.fm is''' an [[online shopping]] service that incorporates grocery shopping and recipe discovery into a social context. The service is available on the web and for a mobile device and can be downloaded for [[IPhone]], [[Nokia]], [[Android (operating system)]] or as a [[Facebook]] application. Foodie.fm has been referred to as the Facebook for groceries, and described as enabling smart grocery shopping since users personalize their information.<ref>[http://www.thegrocer.co.uk/companies/supermarkets/tesco/tesco-signs-up-for-facebook-for-groceries/224847.article Tesco signs up for 'Facebook for groceries'<!-- Bot generated title -->]</ref> to receive product and recipe recommendations. Referred to as 'social eatworking' and forecasted as a trend for 2012,<ref>[http://www.thisislondon.co.uk/lifestyle/article-24022807-looking-ahead-trends-2012.do Looking ahead: trends 2012 - Life & Style - London Evening Standard<!-- Bot generated title -->]</ref> users post and share shopping ideas, recipes and recommendations with other users on the service.

== Content ==
The application functions by recommending recipes and suggesting groceries for the user to buy based on specified preferences. Users can also connect to other users and family members to plan cooking and grocery shopping together by sharing shopping lists. The service is connected to all of the stores in a given grocery chain and connects users to their local store and product inventory. In the [[United Kingdom]], the Foodie.fm product assortment is linked to [[Tesco]] through public Tesco [[API]] where users can check-out with Tesco.com. In [[Finland]], the company has a cooperation with [[S Group]] which is one of the largest retail chains of the [[Nordic countries]] with reportedly 44% market share in Finland.<ref>[http://raspwire.com/foodie-fm-launches-personalized-social-shopping-platform-for-groceries/do Ontdek de WinstMethode | Eenvoudig online geld verdienen<!-- Bot generated title -->]{{dead link|date=March 2014}}</ref>

== History ==
Founded by Kalle Koutajoki, Samuli Mattila, Lauri Arte and Arto Lukkarila in 2009, the service is available in [[Finland]] and the [[United Kingdom]] as Beta. The company is based in [[Finland]] but formally launched internationally at Le Web conference in December 2011.<ref>Wauters, Robin (December 5, 2011). [http://eu.techcrunch.com/2011/12/05/eat-smarter-foodie-fm-debuts-personalized-grocery-shopping-platform/ "Eat smarter: Foodie.fm debuts personalized grocery shopping platform]. [[TechCrunch]].</ref> As a business model, the company provides a [[Personalized marketing|one-to-one]] channel (personalized marketing) between the retailer and the consumer.

In an interview with CEO Kalle Koutajoki, he emphasizes that Foodie.fm has been created with internationalization in mind, and that the UK service will have more features such as [[crowdsourcing]] and revised commenting to make the service even more personal.<ref>[http://www.arcticstartup.com/2011/10/06/interview-with-kalle-koutajoki-of-foodie-on-developing-for-symbian Interview With Kalle Koutajoki Of Foodie On Developing For Symbian<!-- Bot generated title -->]</ref>

== Technology ==
Foodie.fm’s recommendation system relies on a patent-pending technology which learns from a user’s eating and buying habits and suggests groceries and recipes accordingly.{{citation needed|date=March 2014}} The system also takes into account user profile settings which specifies food allergies, personal budget and dietary restrictions.{{citation needed|date=March 2014}}

== Notes ==
{{reflist}}

== References ==
* [http://philadelphia.grubstreet.com/2012/05/manage_food_allergies_online_w.html Manage Food Allergies Online with Foodie.fm - Grub Street Philadelphia<!-- Bot generated title -->]
* [http://www.thisislondon.co.uk/lifestyle/article-24022807-looking-ahead-trends-2012.do "Looking ahead: trends 2012"] ''London Evening Standard''. December 19, 2011

== External links ==
* Khan, Sardar Mohkim (April 28, 2011). [http://www.arcticstartup.com/2011/04/28/foodie-fm-becomes-more-social "Foodie.fm Becomes More Social"]
* Bessonova, Anna (May 25, 2011). [http://www.arcticstartup.com/2011/05/25/foodie-fm-gets-updated-eyes-uk-for-expansion "Foodie.fm Gets Updated, Eyes UK For Expansion"]
* Vesterinen, Ville (February 5, 2010). [http://www.arcticstartup.com/2010/02/05/foodie-fm-helps-you-grocery-shop-smarter/ "Foodie.fm Helps You Grocery Shop Smarter"] 
* Vilpponen, Antti (October 6, 2011). [http://www.arcticstartup.com/2011/10/06/interview-with-kalle-koutajoki-of-foodie-on-developing-for-symbian?utm_source=dlvr.it&utm_medium=twitter "Interview With Kalle Koutajoki of Foodie On Developing For Symbian"] 
* Ashton, Kris (December 11, 2011). [http://www.dailydealmedia.com/642foodie-fm-makes-for-smarter-grocery-shopping-and-healthier-eating/ "Foodie.fm Makes for Smarter Grocery Shopping and Healthier Eating"]
* Hughes, Ian (December 22, 2011). [http://www.pocket-lint.com/news/43550/share-food-tips-and-recipes "Website of the day: Foodie.Fm"] 
* [http://www.goodnewsfinland.com/archive/news/international-launch-of-foodie-fm/ "International launch of Foodie.fm] ''Good News! From Finland''. December 9, 2011.

[[Category:E-commerce]]
[[Category:Social networking services]]
[[Category:Social networking websites]]