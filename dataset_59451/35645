{{DISPLAYTITLE:Ability (''Fringe'')}}
{{Infobox television episode 
| title = Ability
| series = [[Fringe (TV series)|Fringe]]
| image =
| caption = 
| season = 1
| episode = 14
| airdate = February 10, 2009
| production = 3T7663
| story = [[Glen Whitman]]<br/>[[Robert Chiappetta]]
| teleplay = [[David H. Goodman]]
| director = [[Norberto Barba]]
| guests = 
* [[Jared Harris]] as [[List of Fringe characters#David Robert Jones|David Robert Jones]]
* [[Michael Gaston]] as [[List of Fringe characters#Sanford Harris|Sanford Harris]]
* [[Chance Kelly]] as [[List of Fringe characters#Mitchell Loeb|Mitchell Loeb]]
* [[Kenneth Tigar]] as [[List of Fringe characters#Johan Lennox|Johan Lennox]]
* [[Clark Middleton]] as [[List of Fringe characters#Edward Markham|Edward Markham]]
* [[Noah Bean]] as Agent Kemp
* Philip LeStrange as Thomas Avery
* [[Michael Cerveris]] as the [[List of Fringe characters#September (The Observer)|Observer]]
| episode_list = [[Fringe (season 1)|''Fringe'' (season 1)]]<br>[[List of Fringe episodes|List of ''Fringe'' episodes]]
| prev = [[The Transformation]]
| next = [[Inner Child (Fringe)|Inner Child]]
}}

"'''Ability'''" is the fourteenth [[List of Fringe episodes|episode]] of the [[Fringe (season 1)|first season]] of the American [[science fiction]] [[drama]] [[television program|television series]] ''[[Fringe (TV series)|Fringe]]''. The plot follows the Fringe team's investigation into [[Mythology of Fringe#ZFT|ZFT]] and [[David Robert Jones (Fringe)|David Robert Jones]], who claims that [[Olivia Dunham|Olivia]] is a soldier equipped with abilities to fight in an upcoming war between two [[parallel universe (fiction)|parallel universes]]. A skeptical Olivia must discover a way to avoid unleashing an attack that causes fatal accelerated cellular growth in its victims.

The episode's teleplay was written by co-executive producer [[David H. Goodman]] from a story by executive story editors [[Glen Whitman]] and [[Robert Chiappetta]]. It was directed by [[Norberto Barba]], his only contribution to the series. [[Jared Harris]] guest starred as Jones, alongside actors [[Clark Middleton]], [[Michael Gaston]], [[Noah Bean]], and [[Chance Kelly]].

It first aired in the United States on February 10, 2009 on the [[Fox Broadcasting Company|Fox network]] to an estimated 9.83 million viewers, placing second in its timeslot. The episode earned a 4.1/10 ratings share among adults aged 18 to 49, meaning that it was seen by 4.1 percent of all 18- to 49-year-olds, and 10 percent of all 18- to 49-year-olds watching television at the time of broadcast. "Ability" received mixed reviews from television critics.

==Plot==
A newspaper vendor dies by suffocation after receiving a [[United States two-dollar bill|two-dollar bill]] coated in a chemical substance that causes all his orifices to close up. Walter analyzes the chemical agent as a catalyst that speeds up the protein production in scar tissue and accelerating cell growth.  Meanwhile, Olivia postulates that the initials "ZFT" may not be of a terrorist organization but of a book, and discovers the German name, "Zerstörung durch Fortschritte der Technologie", roughly "Destruction by Advancement of Technology". Peter takes Olivia to a rare book storekeeper, Edward Markam ([[Clark Middleton]]) who gets a copy of the ZFT from another collector. Peter returns with it to Walter's lab, learning that it is a typewritten manifesto preparing "soldiers" for an upcoming war between [[multiverse|two universes]].

Concurrently, David Robert Jones ([[Jared Harris]]) turns himself in at the FBI headquarters. Suffering from effects of being teleported out of his German prison cell by Walter's technology, he insists on only speaking to Olivia, warning that she is the only one that can stop a bomb from going off in 36 hours. Sanford Harris ([[Michael Gaston]]) instead orders Olivia to join other agents in raiding a warehouse which they believe Jones and his men used. Evidence confirms Jones had been there, but an agent dies from suffocation after finding another two-dollar bill.

Olivia convinces Harris to let her see Jones alone, attributing the agent's death to his misfire. In the interrogation room, Olivia discovers that Jones believes she is one of the soldiers in his war, having been a test subject on a Massive Dynamic [[nootropic]] drug known as "Cortexiphan" when she was a child. Jones claims that she is special, and instructs her to a remote site with a key in his possession to retrieve a package. Olivia follows his instructions, finding a package full of strange puzzles. The first puzzle is a light box containing a number of lights which Jones' instructions require her to disable with her thoughts only. Olivia, having learned from Nina Sharp that the only Cortexiphan trials were done in Ohio, far from her childhood Jacksonville, Florida home, is confident Jones is mistaken.

As Jones' condition worsens and he is brought to Walter's lab, Peter rigs the lightbox to make the lights go off automatically. Olivia performs the test in front of Jones, and he supplies her the address of the bomb.  When Olivia and the FBI arrive, they find that the bomb is set to release the deadly agent across the city but can only be defused if Olivia turns off a similar array of lights as were on the puzzle. Despite faking the earlier test, Olivia is successful at disabling the lights and the bomb with her thoughts. 

In the episode's epilogue, Jones, having been transferred to secured hospital, is rescued by his men, leaving a message on the wall telling Olivia she passed. Meanwhile, Walter, who has also started reading the ZFT, recognizes a unique offset letter, and finds that his own lab typewriter produces the same offset. Olivia receives a call from Nina who had further looked into the Cortexiphan trials and discovered a smaller case study that occurred at Jacksonville.

==Production==
[[File:Noah Bean (as Bo Decker) in the Huntington Theatre Company production of William Ingeís BUS STOP. Directed by Nicholas Martin.jpg|right|180px|thumb|The episode featured a brief appearance by actor [[Noah Bean]], who plays an FBI agent killed in a fringe event.]]
The episode's teleplay was written by co-executive producer [[David H. Goodman]] based on a story by executive story editors and scientists [[Glen Whitman]] and [[Robert Chiappetta]].<ref name=episode>{{cite episode |title=Ability |episodelink= |series=Fringe |serieslink=Fringe (TV series) |credits=[[Norberto Barba]] (director), [[David H. Goodman]] (writer), [[Glen Whitman]] (writer), [[Robert Chiappetta]] (writer) |network=[[Fox Broadcasting Company|Fox]] |airdate=2009-02-10 |season=1 |number=14}}</ref> It was Goodman's fifth contribution to the series and Whitman and Chiappetta's first.{{sfn|Stuart|2011|pp=201–04}} The episode was directed by [[Norberto Barba]], his only directional credit with the series.<ref name=episode/>

[[Noah Bean]] noted that his character Officer Kemp "meets pretty gruesome, unbelievable ends," as Kemp's face becomes covered with skin and scar tissue.<ref name=decipher/> To create the effect of Kemp's orifices closing, the crew used a combination of make-up and computer graphic effects. They created casts of Bean, including the front of his face, ears, back, and torso. They applied make-up over parts of his face at different stages as the scene demanded. For Olivia's unsuccessful emergency [[tracheotomy]], the actress cut into a fake neck that Bean wore.<ref name=decipher>{{cite video |people=Steve Kelley, Colin Maclellan, Kirk Acevedo, Anna Torv, Noah Bean |date=2009 |title=Fringe: Deciphering the Scene: "Ability" | medium=DVD |publisher=[[Warner Bros.]] Television |location=''Fringe: The Complete First Season'' Disc 5 }}</ref>

Guest actor Jared Harris made his third appearance of the season in "Ability". At the time, the actor had not heard whether his character would be returning,<ref>{{cite web |url=http://blastr.com/2009/02/in-which-we-meet-fringes-mr-jones-who-sees-his-future.php |title=In which we meet Fringe's Mr. Jones, who sees his future|first=Fred|last=Topel |publisher=[[Blastr]] |date=2009-02-06 |accessdate= 2011-04-02}}</ref> though he later appeared in the [[There's More Than One of Everything|first season finale]]<ref>{{cite web |url=http://blastr.com/2009/05/how-fringes-season-finale.php |title=How Fringe's season finale will change everything up |first=Ian|last=Spelling|publisher=[[Blastr]] |date=2009-05-12 |accessdate=2011-04-02}}</ref> and five [[Fringe (season 4)|fourth season]] episodes.<ref>{{Cite web|url=http://www.tvguide.com/celebrities/jared-harris/credits/155920 |title=Jared Harris: Credits |work=[[TV Guide]] |accessdate=2013-06-13}}</ref> Harris described his character as an "anti-hero who is fighting for the just cause"<ref>{{Cite web|url=http://xfinity.comcast.net/blogs/tv/2009/05/12/interview-jared-harris-on-fringe-mad-men-and-his-legendary-father/ |title=Interview: Jared Harris on ‘Fringe,’ ‘Mad Men’ and His Legendary Father |publisher=Comcast.net |first=Tara |last=Bennet |date=2009-05-12 |accessdate=2013-05-10}}</ref> and the episode as "a page-turner... When I read it, I said, 'Ah, this is a fantastic one. I can't wait to see it.' It's directed with a lot of energy, a lot of tension. It's a good one, a really good one".<ref>{{cite web |url=http://blastr.com/2009/02/jared-harris-says-fringe-will-reveal-the-mystery-of-mr-jones.php |title=Jared Harris will reveal the mystery behind Fringe's Mr. Jones |first=Ian|last=Spelling |publisher=[[Blastr]] |date=2009-02-10 |accessdate= 2011-04-02}}</ref>

The episode featured the first of many guest appearances by actor Clark Middleton as the rare bookseller [[List of Fringe characters#Edward Markham|Edward Markham]].<ref>{{Cite web|url=http://www.tvguide.com/celebrities/clark-middleton/credits/228317 |title=Clark Middleton: Credits |work=[[TV Guide]] |accessdate=2013-05-10}}</ref> In addition to Bean and Harris, the episode's guest stars included Michael Gaston as [[List of Fringe characters#Sanford Harris|Sanford Harris]], [[Chance Kelly]] as [[List of Fringe characters#Mitchell Loeb|Mitchell Loeb]], [[Kenneth Tigar]] as [[List of Fringe characters#Johan Lennox|Johan Lennox]], Philip LeStrange as Thomas Avery, and [[Michael Cerveris]] as the [[List of Fringe characters#September (The Observer)|Observer]].<ref name=episode/>

==Reception==
===Ratings===
"Ability" was watched by an estimated 9.83 million viewers on its initial broadcast in the United States, placing in second in its timeslot behind [[CBS]]' ''[[The Mentalist]]''. ''Fringe'' was the [[Fox Broadcasting Company|Fox network]]'s fourth most watched show for the week, and received a 5.9/9 rating share among all American households. Among adults aged 18 to 49, the episode finished in fourteenth place for the week by earning a 4.1/10 ratings share, meaning that it was seen by 4.1 percent of all 18- to 49-year-olds, and 10 percent of all 18- to 49-year-olds watching television at the time of broadcast.<ref>{{cite web |url=http://tvbythenumbers.com/2009/02/18/top-fox-primetime-shows-february-9-15-2009/13039 |title=Top Fox Primetime Shows, February 9–15, 2009 |publisher=TV by the Numbers |last=Seidman |first=Robert |date=2009-02-18 |accessdate=2013-05-10}}</ref><ref>{{cite web |url=http://www.highbeam.com/doc/1G1-195322755.html |title=Nielsen primetime ratings report |work=[[Daily Variety]] |date=2009-02-19 |accessdate=2013-05-10}} {{Subscription needed}}</ref>

===Reviews===
{{Quote box
 | quote  = "After a couple of less than stellar stories ''Fringe'' actually delivered some real surprises and came out from under its warm blanket of confusion, thankfully."
 | source = – Den of Geek<ref name=denofgeekreview/>
 | width  = 25em
 |bgcolor=#ADD8E6
 | align  =left
}}

Den of Geek viewed the episode positively, observing that in contrast to the [[The Transformation|previous installment]], "Ability" "progresses the main plot arc in a substantial way... I’m now really interested to see what happens next, whereas last week I wasn’t that bothered."<ref name=denofgeekreview>{{cite web |url=http://www.denofgeek.us/tv/fringe/9205/jj-abrams-fringe-episode-14-review |title=JJ Abrams' Fringe episode 14 review |publisher=[[Den of Geek]] |first=|last=|date=2009-02-17 |accessdate=2013-05-10}}</ref> [[IGN]]'s Ramsey Isler was a little more critical and rated the episode 7.8/10. He noted that though the ending with Walter "was as good as you could ask for" and enjoyed David Robert Jones as a villain, the episode had a "number of rough patches," as the middle was "paced slowly" and lacking in suspense, Peter's "handiness was again reduced to ...'that guy with connections'", and the audience was "once again subjected to the torture that is Sanford Harris".<ref name=ignreview/> After describing the Olivia-Peter scene near the end as "tense and cleverly written," Isler concluded his review by noting references to ''[[The X-Files]]'' and hoping Jones would appear again soon.<ref name=ignreview>{{cite web |url=http://tv.ign.com/articles/953/953320p1.html |title=Fringe: "Ability" Review |publisher=[[IGN]] |first=Ramsey |last=Isler |date=2009-02-11 |accessdate=2011-03-18}}</ref> 

Noel Murray from ''[[The A.V. Club]]'' graded the episode an ''A-'', explaining he thought it must have been "satisfying" for those viewers tired of the "monster-of-the-week" storylines. Murray also enjoyed two twists: that Olivia had to shut off the lights for real, and that Walter discovers he wrote the ZFT manual at the end.<ref>{{cite web |url=http://www.avclub.com/articles/ability,23674/ |title=Ability |work=[[The A.V. Club]] |first=Noel|last=Murray |date=2009-02-10 |accessdate=2011-03-18}}</ref> Jane Boursaw of [[AOL TV]] (previously ''[[TV Squad]]'') opined that it was a "really great episode that's probably worth another viewing to pick up more clues,"<ref>{{cite web |url=http://www.aoltv.com/2009/02/11/fringe-ability/ |title=Fringe: Ability |publisher=[[AOL TV]] |first=Jane |last=Boursaw |date=2009-02-11 |accessdate=2013-05-10}}</ref> while the ''[[Los Angeles Times]]''{{'}} Andrew Hanson thought it "managed to have a couple good twists to it."<ref>{{cite web |url=http://latimesblogs.latimes.com/showtracker/2009/02/fringe-quite-an.html |title='Fringe': Quite an 'Ability' |work=[[Los Angeles Times]] |first=Andrew |last=Hanson |date=2009-02-11 |accessdate=2013-05-10}}</ref> [[Annalee Newitz]] of [[io9]] praised "Ability", remarking that "overall, this was a terrific episode, and a great way to go into a show hiatus. We got a lot of payoff when we discovered why Olivia was being stalked by the ZFT weirdos, and we learned more about the Pattern than we had in a really long time."<ref>{{cite web |url=http://io9.com/5151177/its-back-to-the-pattern-on-fringe |title=It's Back to the Pattern on Fringe |publisher=[[io9]] |first=Annalee |last=Newitz |date=2009-02-11 |accessdate=2013-05-10}}</ref>

==References==
{{reflist|30em}}

;Works cited
* {{Cite book|url=https://books.google.com/books?id=KDj4aMpiT8YC& |title=Into the Looking Glass: Exploring the Worlds of Fringe |last=Stuart |first=Sarah Clarke |publisher=ECW Press |year=2011 |isbn=1-77041-051-1 |ref=harv}}

==External links==
{{Wikiquote|Fringe#Ability_.5B1.13.5D|Ability}}
* [https://web.archive.org/web/20110301081948/http://www.fox.com:80/fringe/recaps/season-1/episode-14 "Ability"] at [[Fox Broadcasting Company|Fox]]
* {{IMDb episode|1352058|Ability}}
* {{tv.com episode|fringe/ability-1250636|Ability}}

{{Fringe show}}

{{good article}}
{{DEFAULTSORT:Ability}}
[[Category:2009 television episodes]]
[[Category:Fringe (season 1) episodes]]