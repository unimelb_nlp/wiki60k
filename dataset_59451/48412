{{Multiple issues|
{{BLP sources|date=November 2015}}
{{BLP primary sources|date=November 2015}}
{{lead too short|date=August 2016}}
}}
{{Use British English|date=August 2011}}
{{Use dmy dates|date=March 2012}}
{{Infobox person
| name = Sam Hazeldine
| image =
| birth_name = Samuel Hazeldine
| birth_date = {{Birth date and age|1972|3|29|df=y}}
| birth_place = [[Hammersmith]], [[London]], [[England]]
| death_date =
| death_place =
| othername =
| occupation = Actor
| years active = 2003–present
| spouse = 
| homepage =
}}
'''Samuel Hazeldine''' (born 29 March 1972) is an English actor who played the role of Ivan in the [[feature film]] ''[[The Raven (2012 film)|The Raven]]''.<ref>[http://www.theravenmovie.com/ Ivan], ''[[The Raven (2012 film)|The Raven]]''</ref>

==Early life and family==
Hazeldine was born in [[Hammersmith]], [[London]], the son of Rebecca Moore and actor [[James Hazeldine]].<ref>{{cite news| url= http://www.bbc.co.uk/pressoffice/pressreleases/stories/2005/04_april/08/new_tricks_armstrong.shtmland |title= Sam is the son of James Hazeldine| website=[[BBC]].co.uk| location= London| date= 8 April 2005}}</ref> He attended the [[Royal Academy of Dramatic Art]] (RADA). 

==Career==
Hazeldine left the Royal Academy of Dramatic Art in 1993 to pursue a career in music.<ref>{{cite web | url= http://www.thestage.co.uk/features/showpeople/feature.php/2571/sam-hazeldine |title= Sam Hazeldine| work= [[The Stage]]| location= London| date= 10 June 2004}}</ref>

He returned to acting in 2003, making his professional debut as D.C. David Butcher in ''[[Prime Suspect|Prime Suspect 6]]''.

==Works==
===Theatre roles===
* George in [[Talawa Theatre Company]]'s production of ''[[Blues for Mister Charlie]]'' (2004)
* Horatio in ''[[Hamlet]]'', [[English Touring Theatre]] (2006)
* Cassio in ''[[Othello]]'', [[Salisbury Playhouse]]  (2007)
* Tom Connor in ''[[Snowbound (play)|Snowbound]]'', [[Trafalgar Studios]] (2008)<ref>{{cite web| last= Tripney| first= Natasha |url= http://www.thestage.co.uk/reviews/review.php/20190/snowbound |title= ''Snowbound''| work= The Stage| date= 19 March 2008}}</ref>
* Orsino in "[[Twelfth Night]]", [[York Theatre Royal]] (2009)
* Lenny in ''[[The Homecoming]]'', York Theatre Royal (2009)<ref>{{cite web| url= http://www.thestage.co.uk/reviews/review.php/24606/the-homecoming |title= ''The Homecoming''| work= The Stage| date= 4 June 2009}}</ref>
* The Soldier in the [[Royal Shakespeare Company]]'s production of ''[[Dennis Kelly|The Gods Weep]]'' (2010)
* Turner in ''[[Ditch]]'', [[HighTide]]/[[Old Vic|Old Vic Tunnels]] (2010)<ref>{{cite news| url=  http://www.bbc.co.uk/news/10238610 |title= Theatre goes underground| website= BBC.co.uk| date= 7 June 2010}}</ref>
* Borgheim in ''[[Little Eyolf]]'', [[Almeida Theatre]] (2015)

===Television===
* ''Prime Suspect'' (2003) as D.C. David Butcher
*''Shameless'' (2004) as Quentin (episode 1.3)
*''Passer By'' (2004) as Young Man
*''The Bill'' (2004) as David Latham (episode "Love, Lies and Limos")
* ''Doctors'' (2005) as Rob Tanner (episode: "Dream Lover")
* ''Dalziel and Pascoe'' as Sean Doherty (episode: "Dust Thou Art")
* ''New Tricks'' as Mark Lane (episode 2.3)
* ''[[Foyle's War]]'' (2005) as Will Grayson (episode: " Invasion")
* ''Holby City'' (2005) as Philip Jennssen (episode: "Snake in the Grass")
*''Robin Hood'' (2005) as Owen (episode "Who Shot the Sheriff?")
*''[[Life on Mars (UK TV series)|Life on Mars]]'' (2006) as Collin Raimes (episode 1.1)
* ''The Extraordinary Equiano'' (2007) as Captain Pasqual
* ''[[Persuasion (2007 film)|Persuasion]]'' (2007) as Charles Musgrove<ref>{{cite web| url=  |title= ''Persuasion''| work=[[The Hollywood Reporter]]| date= March 29, 2007 |first= Ray |last= Bennett | agency= Associated Press| access-date= July 28, 2016}}</ref>
*''[[Midsomer Murders]]'' (2007) as Simon Dixon (4 episodes)
*''Emmerdale'' (2008) as Dr Roger Elliot (episode: "Puff of Smoke")
* ''Heartbeat'' (2008) as Frank Kelly (episode: "Living Off the Land")
* ''[[Waterloo Road (TV series)|Waterloo Road]]'' (2009) as Captain Andy Rigby (episode 4.15)
* ''Paradox'' (2009) as Matt Hughes (episode 1.5)
*''The Little House'' (2010) as David<ref>{{cite news| last= Plunkett| first= John | url= https://www.theguardian.com/media/2010/nov/02/little-house-tv-ratings |title= ''The Little House'' |work= [[The Guardian]]| date= 1 November 2010}}</ref>  (2 episodes)
* ''[[Lewis (TV series)|Lewis]]'' (2011) as Dane Wise (episode: "The Mind Has Mountains")
* ''Eternal Law'' (2012)<ref>[http://www.radiotimes.com/episode/pf5mq/eternal-law--series-1---episode-6 ''Eternal Law''], ''[[Radio Times]]'', 5 March 2011.</ref> as Bruno Hale (episode 1.6)
* ''Accused'' (2012) as Ray Dakin (episode 2.4)
* ''[[Ripper Street]]'' (2013) as Doggett (episode 1.5)
* ''[[Lightfields]]'' (2013) as Albert Felwood (5 episodes)
* ''[[Resurrection (U.S. TV series)|Resurrection]]'' (2014) as Caleb Richards (8 episodes)
* ''[[The Dovekeepers]]'' (2015) as [[Lucius Flavius Silva]] (2 episodes)
*''[[Peaky Blinders (TV series)|Peaky Blinders]]'' (2015) as Georgie Sewell (5 episodes)

===Film===
{| class="wikitable"  
|-
! Year
! Film
! Role
! Notes
|-
| 2004
| ''[[Bridget Jones: The Edge of Reason (film)|Bridget Jones: The Edge of Reason]]''
| Journalist
|
|-
| 2005
| ''[[Chromophobia (film)|Chromophobia]]''
| Muso Assistant
|
|-
| 2008
| ''Trip''<ref>[http://www.bbc.co.uk/filmnetwork/films/p0060pp9 ''Trip''], ''[[BBC]]''</ref>
| Dad
| Short film
|-
| 2009
| ''Good as Gone''<ref>[http://www.bbc.co.uk/filmnetwork/films/p006xwlm ''Good as Gone''], ''[[BBC]]''</ref>
| Red
| Short Film
|-
| rowspan="2"|2010
| ''[[The Wolfman (2010 film)|The Wolfman]]''
| Horatio
|
|-
| ''Just Before Dawn''
| Lewis
| Short film
|-
| rowspan="4"|2011
| ''[[Weekender (film)|Weekender]]''
| Maurice
|
|-
| ''Don't Let Him In''
| Shaun
|
|-
| ''Ouroboros''
| Damien
| Short film
|-
| ''Lullaby''
| Gary
| Short film
|-
| rowspan="3"|2012
| ''[[The Raven]]''
| Ivan
|
|-
| ''Dead Mine''
| Stanley 
|
|-
| ''[[Riot on Redchurch Street]]''
| Ray Mahoney
|
|-
|2013
|[[The Machine (2013 film)|''The Machine'']]
| James
|-
| rowspan="4"|2014
|''[[Still (film)|Still]]''
| Josh
|
|-
| ''[['71 (film)|'71]]''
| C.O.
|
|-
| ''[[The Monuments Men]]''
| Colonel Langton
|
|-
| ''[[Between Places]]''
| Roger
| Short Film
|-
| rowspan="2"|2015
|''[[Cherry Tree (2015 film)|Cherry Tree]]''
|  Sean<ref>{{cite web| url= http://bloody-disgusting.com/videos/3355884/cherry-tree-trailer-infested-scares/ | title= ‘Cherry Tree’ Trailer Infested With Scares| date= August 4, 2015 |first= Brad |last= Miska| website= bloody-disgusting.com| access-date= July 28, 2016}}</ref> 
|
|-
| ''[[Learning to Breathe (film)|Learning to Breathe]]''
| Noah
|
|-
| rowspan="4"|2016
| ''[[Grimsby (film)|Grimsby]]''
| Chilcott
|
|-
| ''[[The Huntsman: Winter's War]]''
| Liefr
|
|-
| ''[[Mechanic: Resurrection]]''
| Riah Crain
|
|-
| ''[[The Journey Is The Destination (film)|The Journey Is The Destination]]''
| O'Reilly
|
|-
| rowspan=''5''|2017
| ''[[The Hitman's Bodyguard]]''
| Garrett
| Filming
|-
|}

==References==
{{Reflist|30em}}

==External links==
* {{IMDb name|1486831}}

{{DEFAULTSORT:Hazeldine, Sam}}
[[Category:1972 births]]
[[Category:Alumni of the Royal Academy of Dramatic Art]]
[[Category:English male film actors]]
[[Category:English male television actors]]
[[Category:English male voice actors]]
[[Category:Living people]]
[[Category:People educated at Denstone College]]
[[Category:Male actors from London]]
[[Category:21st-century English male actors]]
[[Category:People from Tottenham]]
[[Category:People from Hammersmith and Fulham (London borough)]]
[[Category:English male stage actors]]
[[Category:English male Shakespearean actors]]