{{about|the novel}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = Sense and Sensibility
| image          = SenseAndSensibilityTitlePage.jpg
| caption        = Title page from the original 1811 edition
| title_orig     =
| translator     =
| author         = [[Jane Austen]]
| cover_artist   =
| country        = United Kingdom
| language       = English
| series         =
| genre          = [[Romance novel]]
| publisher      = [[Thomas Egerton (publisher)|Thomas Egerton]], Military Library (Whitehall, London)
| release_date   = 1811
| media_type     =
| pages          =
| isbn           =
| oclc           = 44961362
| preceded_by    =
| followed_by    = [[Pride and Prejudice]]
}}
'''''Sense and Sensibility''''' is a novel by [[Jane Austen]], published in 1811. It was published anonymously; ''By A Lady'' appears on the cover page where the author's name might have been. It tells the story of the Dashwood sisters, Elinor and Marianne, both of age to marry.

The novel follows the young women to their new home with their widowed mother, a meagre cottage on the property of a distant relative, where they experience love, romance and heartbreak. The novel is set in [[southwest England]], [[London]] and [[Sussex]] between 1792 and 1797.<ref>{{cite book |first=Deirdre |last=Le Faye |year=2002 |title=Jane Austen: The World of Her Novels |location=London |publisher=[[Frances Lincoln Publishers]] |page=155 |isbn=0-7112-1677-0}}</ref>

The novel sold out its first print run of 750 copies in the middle of 1813, marking a success for its author, who then had a second print run later that year. The novel continued in publication throughout the 19th, 20th and 21st centuries.

== Plot summary ==
{{plot|date=August 2016}}
Because of [[primogeniture]], when Mr Henry Dashwood dies, his house, Norland Park, passes directly to his son John, the child of his first wife. His second wife, Mrs Dashwood, and their daughters, [[Elinor Dashwood|Elinor]], [[Marianne Dashwood|Marianne]] and Margaret, inherit only a small income. On his deathbed, Mr Dashwood extracts a promise from his son, to take care of his half-sisters. John's greedy wife, Fanny, soon persuades him to renege on the promise. John and Fanny immediately move in as the new owners of Norland, while the Dashwood women are treated as unwelcome guests. Mrs Dashwood seeks somewhere else to live. In the meantime, Fanny's brother, [[Edward Ferrars]] visits Norland and soon forms an attachment with Elinor. Fanny disapproves of the match and offends Mrs Dashwood with the implication that Elinor is motivated by money.

Mrs Dashwood moves her family to Barton Cottage in [[Devon]]shire, near the home of her cousin, Sir John Middleton. Their new home is modest but they are warmly received by Sir John and welcomed into local society—meeting his wife, Lady Middleton, his mother-in-law, Mrs Jennings and his friend, Colonel Brandon. Colonel Brandon is attracted to Marianne, and Mrs Jennings teases them about it. Marianne is not pleased as she considers the thirty-five-year-old Colonel Brandon an old bachelor, incapable of falling in love or inspiring love in anyone else.

[[File:Sense and Sensibility Illustration Chap 12.jpg|right|thumb|220px|A 19th-century illustration by Hugh Thomson showing [[John Willoughby|Willoughby]] cutting a lock of [[Marianne Dashwood|Marianne]]'s hair]]
Marianne, out for a walk, gets caught in the rain, slips and sprains her ankle. The dashing [[John Willoughby]] sees the accident and assists her. Marianne quickly comes to admire his good looks and outspoken views on poetry, music, art and love. His attentions lead Elinor and Mrs Dashwood to suspect that the couple are secretly engaged. Elinor cautions Marianne against her unguarded conduct, but Marianne refuses to check her emotions. Abruptly, Mr Willoughby informs the Dashwoods that his aunt, upon whom he is financially dependent, is sending him to London on business, indefinitely. Marianne is distraught and abandons herself to her sorrow.

Edward Ferrars pays a short visit to Barton Cottage but seems unhappy. Elinor fears that he no longer has feelings for her, but will not show her heartache. After Edward departs, Anne and Lucy Steele, the vulgar cousins of Lady Middleton, come to stay at Barton Park. Lucy informs Elinor in confidence of her secret four-year engagement to Edward Ferrars that started when he was studying with her uncle, and she displays proof. Elinor realises that Lucy's visit and revelations are the result of Lucy's jealousy and cunning calculation, and understands Edward's recent behavior towards her. She acquits Edward of blame and pities him for being held to a loveless engagement by his sense of honour.

Elinor and Marianne accompany Mrs Jennings to London. On arriving, Marianne rashly writes several personal letters to Willoughby, which go unanswered. When they meet at a dance, Mr Willoughby greets Marianne reluctantly and coldly, to her extreme distress. Soon Marianne receives a curt letter enclosing their former correspondence and love tokens, including a [[Lock of hair|lock of her hair]] and informing her of his engagement to a young lady with a large fortune. Marianne is devastated. After Elinor has read the letter, Marianne tells her that she and Willoughby were never engaged, but she loved him and thought that he loved her. 

Colonel Brandon visits the sisters and reveals to Elinor that Willoughby's aunt disinherited him after she learned that he had seduced Brandon's fifteen-year-old ward, Miss Williams, then abandoned her when she became pregnant. This is why he chose to marry for money rather than love. Brandon was in love with Miss Williams' mother as a young man, when she was his father's ward, but she was forced into an unhappy marriage to Brandon's brother that ended in scandal and divorce; Marianne strongly reminds him of her.

The Steele sisters come to London as guests of Mrs Jennings and after a brief acquaintance they are asked to stay at John and Fanny Dashwood's London house. Lucy sees the invitation as a personal compliment, rather than what it is, a slight to Elinor and Marianne who should have received such invitation first. Too talkative, Anne Steele betrays Lucy's secret. As a result, the Misses Steele are turned out of the house, and Edward is ordered to break off the engagement on pain of disinheritance. Edward refuses to comply and is immediately disinherited in favour of his brother, gaining respect for his conduct, and sympathy from Elinor and Marianne. Colonel Brandon shows his admiration by offering Edward the living of Delaford parsonage.

Mrs Jennings takes Elinor and Marianne to the country to visit her second daughter. In her misery over Willoughby's marriage, Marianne becomes dangerously ill. Willoughby arrives to repent and reveals to Elinor that his love for Marianne was genuine. He elicits Elinor's pity because his choice has made him unhappy, but she is disgusted by the callous way in which he talks of Miss Williams and of his own wife. He also reveals that his aunt forgave him after his marriage, meaning that if he had married Marianne he would have had both money and love.

When Marianne recovers, Elinor tells her of Willoughby's visit. Marianne realises that she could never have been happy with Willoughby's immoral and expansive nature. She values Elinor's conduct in her similar situation and resolves to model herself after Elinor's courage and good sense. Edward arrives and reveals that, after his disinheritance, Lucy jilted him in favour of his now wealthy brother, Robert. Edward and Elinor soon marry, and later Marianne marries Colonel Brandon, having gradually come to love him.

==Characters==

===Main characters===
*'''[[Elinor Dashwood]]''' – the sensible and reserved eldest daughter of Mr and Mrs Henry Dashwood. She is 19 years old at the beginning of the book. She becomes attached to Edward Ferrars, the brother-in-law of her elder half-brother, John. Always feeling a keen sense of responsibility to her family and friends, she places their welfare and interests above her own, and suppresses her own strong emotions in a way that leads others to think she is indifferent or cold-hearted.
*'''[[Marianne Dashwood]]''' – the romantically inclined and eagerly expressive second daughter of Mr and Mrs Henry Dashwood. She is 16 years old at the beginning of the book. She is the object of the attentions of Colonel Brandon and Mr Willoughby. She is attracted to young, handsome, romantically spirited Willoughby and does not think much of the older, more reserved Colonel Brandon. Marianne undergoes the most development within the book, learning her sensibilities have been selfish. She decides her conduct should be more like that of her elder sister, Elinor.
*'''[[Edward Ferrars]]''' – the elder of Fanny Dashwood's two brothers. He forms an attachment to Elinor Dashwood. Years before meeting the Dashwoods, Ferrars proposed to Lucy Steele, the niece of his tutor. The engagement has been kept secret owing to the expectation that Ferrars' family would object to his marrying Miss Steele. He is disowned by his mother on discovery of the engagement after refusing to give it up.
*'''[[John Willoughby]]''' – a philandering nephew of a neighbour of the Middletons, a dashing figure who charms Marianne and shares her artistic and cultural sensibilities. It is generally presumed by many of their mutual acquaintances that he is engaged to marry Marianne (partly due to her own overly familiar actions, i.e., addressing personal letters directly to him), however he is already engaged to the wealthy Sophia Grey. He is also contrasted by Austen as being "... a man resembling "the hero of a favourite story"".<ref>{{Cite book|title=Searching for Jane Austen|last=Auerbach|first=Emily|publisher=The University of Wisconsin Press|year=2004|isbn=0-299-20180-5|location=London, England|pages=112|quote="...a man resembling "the hero of a favourite story"".|via=Google, Google Books}}</ref> 
*'''Colonel Brandon''' – a close friend of Sir John Middleton. He is 35 years old at the beginning of the book. He falls in love with Marianne at first sight, as she reminds him of his father's ward whom he had fallen in love with when he was young. He is prevented from marrying the ward because his father was determined she marry his older brother. He was sent into the military abroad to be away from her, and while gone, the girl suffered numerous misfortunes—partly as a consequence of her unhappy marriage. She finally dies penniless and disgraced, and with a natural (i.e., illegitimate) daughter, who becomes the ward of the Colonel. He is a very honourable friend to the Dashwoods, particularly Elinor, and offers Edward Ferrars a [[Parson's freehold|living]] after Edward is disowned by his mother.

===Minor characters===
* '''Henry Dashwood''' &ndash; a wealthy gentleman who dies at the beginning of the story. The terms of his estate &mdash; entailment to a male heir &mdash; prevent him from leaving anything to his second wife and their children. He asks John, his son by his first wife, to look after (meaning ensure the financial security of) his second wife and their three daughters.
* '''Mrs Dashwood''' &ndash; the second wife of Henry Dashwood, who is left in difficult financial straits by the death of her husband. She is 40 years old at the beginning of the book. Much like her daughter Marianne, she is very emotive and often makes poor decisions based on emotion rather than reason.
* '''Margaret Dashwood''' &ndash; the youngest daughter of Mr and Mrs Henry Dashwood. She is thirteen at the beginning of the book. She is also romantic and good-tempered but not expected to be as clever as her sisters when she grows older.
* '''John Dashwood''' &ndash; the son of Henry Dashwood by his first wife. He intends to do well by his half-sisters, but he has a keen sense of [[avarice]], and is easily swayed by his wife.
* '''Fanny Dashwood''' &ndash; the wife of John Dashwood, and sister to Edward and Robert Ferrars. She is vain, selfish, and snobbish. She spoils her son Harry. She is very harsh to her husband's half-sisters and stepmother, especially since she fears her brother Edward is attached to Elinor.
* '''Sir John Middleton''' &ndash; a distant relative of Mrs Dashwood who, after the death of Henry Dashwood, invites her and her three daughters to live in a cottage on his property. Described as a wealthy, sporting man who served in the army with Colonel Brandon, he is very affable and keen to throw frequent parties, picnics, and other social gatherings to bring together the young people of their village. He and his mother-in-law, Mrs Jennings, make a jolly, teasing, and gossipy pair.
* '''Lady Middleton''' &ndash; the genteel, but reserved wife of Sir John Middleton, she is quieter than her husband, and is primarily concerned with mothering her four spoiled children.
* '''Mrs Jennings''' &ndash; mother to Lady Middleton and Charlotte Palmer. A widow who has married off all her children, she spends most of her time visiting her daughters and their families, especially the Middletons. She and her son-in-law, Sir John Middleton, take an active interest in the romantic affairs of the young people around them and seek to encourage suitable matches, often to the particular chagrin of Elinor and Marianne.
* '''Robert Ferrars''' &ndash; the younger brother of Edward Ferrars and Fanny Dashwood, he is most concerned about status, fashion, and his new [[barouche]]. He subsequently marries Miss Lucy Steele after Edward is disowned.
* '''Mrs Ferrars''' &ndash; Fanny Dashwood and Edward and Robert Ferrars' mother. A bad-tempered, unsympathetic woman who embodies all the foibles demonstrated in Fanny and Robert's characteristics. She is determined that her sons should marry well. After having disowned her eldest son for his engagement to Lucy Steele, she probably also later disinherited her younger son for his marriage to the self-same girl.  
* '''Charlotte Palmer''' &ndash; the daughter of Mrs Jennings and the younger sister of Lady Middleton, Mrs Palmer is jolly, but empty-headed, and laughs at inappropriate things, such as her husband's continual rudeness to her and to others.
* '''Thomas Palmer''' &ndash; the husband of Charlotte Palmer who is running for a seat in Parliament, but is idle and often rude. He is considerate toward the Dashwood sisters.
* '''Lucy Steele''' &ndash; a young, distant relation of Mrs Jennings, who has for some time been secretly engaged to Edward Ferrars. She assiduously cultivates the friendship with Elinor Dashwood and Mrs John Dashwood. Limited in formal education and financial means, she is nonetheless attractive, clever, manipulative, cunning and scheming.
* '''Anne/Nancy Steele''' &ndash; Lucy Steele's elder, socially-inept, and less clever sister.
* '''Miss Sophia Grey''' &ndash; a wealthy and malicious heiress whom Mr Willoughby marries to retain his comfortable lifestyle after he is disinherited by his aunt.
* '''Lord Morton''' &ndash; the father of Miss Morton.
* '''Miss Morton''' &ndash; a wealthy woman whom Mrs Ferrars wants her eldest son, Edward, and later Robert, to marry.
* '''Mr Pratt''' &ndash; an uncle of Lucy Steele and Edward's tutor.
* '''Eliza Williams (Jr.)''' (daughter) &ndash; the ward of Col. Brandon, she is about 15 years old and bore an [[illegitimate]] child to John Willoughby. She has the same name as her mother.
* '''Eliza Williams (Sr.)''' (mother) &ndash; the former love interest of Colonel Brandon. Williams was Brandon's father's ward, and was forced by him to marry Brandon's older brother. The marriage was an unhappy one, and it is revealed that her daughter was left as Colonel Brandon's ward when he found his lost love dying in a poorhouse.
* '''Mrs Smith''' &ndash; the wealthy aunt of Mr Willoughby who disowns him for seducing and abandoning the young Eliza Williams, Col. Brandon's ward.

==Development of the novel==
Jane Austen wrote the first draft of the novel in the form of a novel-in-letters ([[Epistolary novel|epistolary form]]) sometime around 1795 when she was about 19 years old, and gave it the title ''Elinor and Marianne''. She later changed the form to a narrative and the title to ''Sense and Sensibility''.<ref>{{cite book |last=Le Faye |first=Deirdre |title=Jane Austen: The World of Her Novels |location=London |publisher=Frances Lincoln Publishers |year=2002 |isbn=0-7112-1677-0 |page=154}}</ref>  The title of the book, and that of her next published novel, ''[[Pride and Prejudice]]'' (1813), may be suggestive of political conflicts of the 1790s, as well as the movement from the Neoclassic to the Romantic Era (Elinor representing the Neoclassic, and Marianne representing the Romantic). <ref>{{cite book |first=Christopher John |last=Murray |year=2004 |title=Encyclopedia of the Romantic Era: A-K |publisher=Taylor and Francis Books |volume=1 |page=41 |isbn=1-57958-361-X}}</ref>{{Clarify|reason= in what way suggestive of?|date=July 2016}} 

Austen drew inspiration for ''Sense and Sensibility'' from other novels of the 1790s that treated similar themes, including Adam Stevenson's "Life and Love" (1785) which he had written about himself and a relationship that was not meant to be. Jane West's ''A Gossip's Story'' (1796), which features one sister full of rational sense and another sister of romantic, emotive sensibility, is considered to have been an inspiration as well. West’s romantic sister-heroine also shares her first name, Marianne, with Austen’s. There are further textual similarities, described in a modern edition of West's novel.<ref>{{cite book |contributor-last=Looser |contributor-first=Devoney |contribution=Introduction |first=Jane |last=West |title=A Gossip's Story, |editor1=Looser|editor1-first= Devoney |editor2=O'Connor |editor2-first=Melinda |editor3=Kelly|editor3-first=Caitlin |location=Richmond, Virginia |publisher=Valancourt Books |year=2015 |isbn=978-1943910151}}</ref>

==Title==
"Sense" means good judgment or prudence, and "sensibility" means sensitivity or emotionality. "Sense" is identified with the character of Elinor, while "sensibility" is identified with the character of Marianne. By changing the title, Austen added "philosophical depth" to what began as a sketch of two characters.<ref>{{cite book |first=Harold |last=Bloom |year=2009 |title=Bloom's Modern Critical Reviews: Jane Austen |location=New York |publisher=Infobase Publishing |page=252 |isbn=978-1-60413-397-4}}</ref>

== Critical views ==
Austen biographer [[Claire Tomalin]] argues that ''Sense and Sensibility'' has a "wobble in its approach", which developed because Austen, in the course of writing the novel, gradually became less certain about whether sense or sensibility should triumph.<ref>{{cite book |first=Claire |last=Tomalin |year=1997 |title=Jane Austen: A Life |location=New York |publisher=Random House |page=155 |isbn=0-679-44628-1}}</ref> Austen characterises Marianne as a sweet lady with attractive qualities: intelligence, musical talent, frankness, and the capacity to love deeply. She also acknowledges that Willoughby, with all his faults, continues to love and, in some measure, appreciate Marianne. For these reasons, some readers find Marianne's ultimate marriage to Colonel Brandon an unsatisfactory ending.<ref>{{cite book |first=Claire |last=Tomalin |year=1997 |title=Jane Austen: A Life |location=New York |publisher=Random House |pages=156–157 |isbn=0-679-44628-1}}</ref>

As quoted by the writers at Create Space "Other interpretations, however, have argued that Austen's intention was not to debate the superior value of either sense or sensibility in good judgment, but rather to demonstrate that both qualities are equally important, but must be in balance."<ref>{{Cite web|url=https://www.createspace.com/4674973|title=Sense and Sensibility|date=February 2014|website=Create Space}}</ref>  The novel is an early example of the category romance novel.<ref>{{cite book |last=Regis |first=Pamela |title=A Natural History of the Romance Novel |publisher= University of Pennsylvania Press |isbn=978-0-8122-3303-2 |date=2007}}</ref>

==Publication history==
[[File:Houghton EC8 Au747 811s (B) - Sense and Sensibility spines.jpg|thumb|right|The three volumes of the first edition of ''Sense and Sensibility'', 1811]]
In 1811, Thomas Egerton of the Military Library publishing house in London accepted the manuscript for publication in three volumes. Austen paid to have the book published and paid the publisher a commission on sales. The cost of publication was more than a third of Austen's annual household income of £460 (about £15,000 in 2008 currency).<ref name=Sanborn>{{cite web |work=Jane Austen's World |title=Pride and Prejudice Economics: Or Why a Single Man with a Fortune of £4,000 Per Year is a Desirable Husband |date=10 February 2008 |url=http://janeaustensworld.wordpress.com/2008/02/10/the-economics-of-pride-and-prejudice-or-why-a-single-man-with-a-fortune-of-4000-per-year-is-a-desirable-husband/ |last=Sanborn |first=Vic |accessdate=27 August 2016}}</ref> She made a profit of £140 (almost £5,000 in 2008 currency)<ref name=Sanborn /> on the first edition, which sold all 750 printed copies by July 1813. A second edition was advertised in October 1813.

The novel has been in continuous publication through to the 21st century as popular and critical appreciation of all the novels by Jane Austen slowly grew.

==Adaptations==
The book has been adapted for film and television a number of times, including a [[Sense and Sensibility (1981 TV serial)|1981 serial for TV]] directed by Rodney Bennett; a [[Sense and Sensibility (film)|1995 film]] adapted by [[Emma Thompson]] and directed by [[Ang Lee]]; a version in [[Tamil language|Tamil]] called ''[[Kandukondain Kandukondain]]'', released in 2000, starring Ajith Kumar (Edward Ferrars), Tabu (Elinor), [[Aishwarya Rai]];<ref>{{cite book | url=https://books.google.co.in/books?id=xgIA0yjV8uUC&pg=PA76&lpg=PA76&dq=kandukondain+kandukondain+sense+and+sensibility&source=bl&ots=nQ0rHnctKO&sig=DwMHxgKerJOAcdrg9ROtKPi7p4E&hl=en&sa=X&ved=0ahUKEwjmn6bQsvjPAhWKHZQKHdDsDGQ4ChDoAQgwMAQ#v=onepage&q=kandukondain%20kandukondain%20sense%20and%20sensibility&f=false | title=Literary Intermediality: The Transit of Literature Through the Media Circuit | publisher=Peter Lang | year=2007 | pages=76}}</ref> and [[Sense and Sensibility (2008 miniseries)|a 2008 TV series]] on [[BBC]] adapted by [[Andrew Davies (writer)|Andrew Davies]] and directed by John Alexander.

''Sense & Sensibility, the Musical'' (book and lyrics by Jeffrey Haddow and music by Neal Hampton) received its world premiere by the Denver Center Theatre Company in April 2013 staged by Tony-nominated director Marcia Milgrom Dodge. In 2014, the Utah Shakespeare Festival presented Joseph Hanreddy and J.R. Sullivan's adaptation of the novel. In 2016, the [http://bedlam.org/ Bedlam theatrical troupe] mounted a well-received [[minimalist]] production, adapted by Kate Hamill and directed by Eric Tucker, from a [[repertory]] run in 2014.<ref>{{Cite news |url=https://www.nytimes.com/2016/02/05/theater/review-a-whirlwind-of-delicious-gossip-in-sense-sensibility.html?nytmobile=0|title=Review: A Whirlwind of Delicious Gossip in ‘Sense & Sensibility’ |newspaper=New York Times |last=Brantley|first=Ben|access-date=14 April 2016}}</ref>

In 2013, author [[Joanna Trollope]] published ''Sense & Sensibility: A Novel''<ref>{{cite book |title=Sense & Sensibility: A Novel |last=Trollope |first=Joanna |author-link=Joanna Trollope |year=2013 |publisher=HarperCollins |isbn=978-0007461769}}</ref> as a part of series called The Austen Project by the publisher, bringing the characters into the present day and providing modern satire.<ref>{{cite news |url=http://www.independent.co.uk/arts-entertainment/books/reviews/book-review-sense-sensibility-by-joanna-trollope-8887105.html |title=Book review: Sense & Sensibility, By Joanna Trollope |last=Craig |first=Amanda |newspaper=The Independent |date=18 October 2013 |accessdate=15 September 2016}}</ref>

== See also ==
{{Portal|Jane Austen|Literature|Novels}}
*[[Sense and Sensibility (1981 TV serial)|''Sense and Sensibility'' (1981 TV serial)]]
*[[Sense and Sensibility (film)|''Sense and Sensibility'' (1995 film)]] starring [[Emma Thompson]] and [[Kate Winslet]]
*''[[Kandukondain Kandukondain]]'' (2000 Tamil film) starring [[Tabu (actress)|Tabu]] and [[Aishwarya Rai]]
*''[[Material Girls]]'' (2006 film), starring [[Hilary Duff|Hilary]] and [[Haylie Duff]]
*[[Sense and Sensibility (2008 TV serial)|''Sense and Sensibility'' (2008 TV serial)]], BBC serial starring [[Hattie Morahan]] and [[Charity Wakefield]]
*''[[Sense and Sensibility and Sea Monsters]]'' (2009 parody novel)
*''[[From Prada to Nada]]'' (2011 film), starring [[Camilla Belle]] and [[Alexa Vega]]
*''[[Scents and Sensibility]]'' (2011 film) starring [[Ashley Williams (actress)|Ashley Williams]] and [[Marla Sokoloff]]


==References==
{{reflist}}

==External links==
{{Wikisource-multi|Sense and Sensibility|below=[http://wsexport.wmflabs.org/tool/book.php?lang=en&format=epub&page=Sense_and_Sensibility Download as ePub]}}
{{wikiquote}}
* [http://epublib.info/sense-and-sensibility-by-jane-austin Sense and Sensibility] ePub, Mobi, PDF versions
{{Gutenberg|no=161|name=Sense and Sensibility}}
*{{OL work}}
* {{librivox book | title=Sense and Sensibility | author=Jane Austen}}
*[http://www.jasna.org/info/images/map-ss-1200.jpg Map] of locations in ''Sense and Sensibility''

{{Jane Austen}}
{{Sense and Sensibility}}

{{DEFAULTSORT:Sense And Sensibility}}
[[Category:Debut novels]]
[[Category:1811 novels]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]
[[Category:Novels by Jane Austen]]
[[Category:Novels set in England]]
[[Category:Works published under a pseudonym]]
[[Category:British novels adapted into plays]]
[[Category:Novels about nobility]]
[[Category:Novels adapted into television programs]]