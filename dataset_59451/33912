{{Infobox grape variety
| name            = Thomcord
| image           = Thomcord grape - USDA photo 01.jpg
| alt             = Several cluster of blue-black grapes hang from the vine
| caption         = Thomcord grapes
| species         = ''[[Vitis vinifera]]'' x ''[[Vitis labrusca]]''
| also_called     = 
| color           = Noir
| color_alt       = Blue-black
| origin          = [[California]]
| pedigree1       = [[Sultana (grape)|Sultanina]] (Thompson Seedless)
| pedigree2       = [[Concord (grape)|Concord]]
| hazards         = [[Uncinula necator|powdery mildew]] (tolerant)
| breeder         = Ramming, David W.<br>Tarailo, Ronald L.
| institute       = [[Agricultural Research Service]] (ARS), USDA
| crossing_year   = 1983
| regions         = [[San Joaquin Valley]], [[California]]
| seeds_formation = 
| flowers_sex     = 
}}

'''Thomcord''' is a [[Seedless fruit|seedless]] [[table grape]] variety and a [[Hybrid (biology)|hybrid]] of the popular Thompson Seedless or [[Sultana (grape)|Sultanina]] grape (a ''[[Vitis vinifera]]'' variety) and [[Concord (grape)|Concord grape]] (a ''[[Vitis labrusca]]'' variety).  Thomcord was developed in 1983 by Californian grape breeders working for the [[Agricultural Research Service]] (ARS), an agency of the [[United States Department of Agriculture]] (USDA), as part of a test to better understand a new seedless grape breeding procedure.

Its aromatic, "[[Vitis labrusca|labrusca]]" flavor is similar to that of Concord, but mellowed by the mild, sweet taste from Thompson Seedless.  Thomcord grows well in hot, dry climates, ripens between late July and mid-August, and tolerates [[Uncinula necator|powdery mildew]].  It is a productive variety, yielding an average of {{convert|15.1|kg|lb|abbr=on}} of grapes per vine, but has produced as much as {{convert|30|to|32|kg|lb|abbr=on}} per vine in grower trials.  The berries weigh between {{convert|2.72|and|3.38|g|oz|abbr=on}} and have a medium-thick, blue-black skin that adheres to the fruit, unlike Concord, which has a thick skin that can slip off the pulp easily.  The [[Seedless fruit|aborted seeds]] in the fruit body are relatively small, but larger than those in Thompson Seedless.

The plant is not [[Plant quarantine|restricted]] for propagation and distribution.  Virus-free propagation material is available from the Foundation Plant Services (FPS) at the [[University of California, Davis]], and its genetic material is archived at the [[National Plant Germplasm System]].  After 17&nbsp;years of testing, it was declared ready for use in 2003.  It is currently available in supermarkets.{{r|TJ}}

==Description==
Thomcord grape is a hybrid of Thompson Seedless grape (''[[Vitis vinifera]]'', or Sultanina), which is popular in supermarkets during the summer, and seeded Concord grape (''[[Vitis labrusca]]''), commonly used to make grape juice and jelly.{{r|2008Ramming|2006_USDA}} It is a plump, juicy, seedless [[table grape]] and is slightly firmer than Concord. Thomcord has a blue-black skin with medium thickness and a whitish bloom.{{r|2008Ramming|2006_USDA|2004FPS}} Unlike Concord, whose tough skin separates easily from the fruit, Thomcord has a more edible skin that clings to the flesh, much like Thompson Seedless.{{r|2008Ramming|2006_Sacramento_Bee}} It has an aromatic flavor, similar to the Concord in taste ("[[Vitis labrusca|labrusca]]"), though lighter due to the sweet, mild taste from Thompson Seedless.{{r|2008Ramming|2006_USDA|2006_Sacramento_Bee}}

Thomcord is suitable for hot, dry growing conditions, more so than Concord and other Concord seedless types.  Its adaptability to hot dry climates was derived from Thompson Seedless.  It grows well in [[Wine Country (California)|California's vineyards]], particularly the [[San Joaquin Valley]],{{r|2008Ramming}} just like Thompson Seedless.{{r|2006_USDA}}  The plant is tolerant of (but not resistant to) [[Uncinula necator|powdery mildew]],{{r|2004FPS|2006_Sacramento_Bee}} and is less susceptible to the [[fungus]] than [[Ruby Seedless (grape)|Ruby Seedless]], but more susceptible than [[Mars (grape)|Mars]], [[Venus (grape)|Venus]], [[Niabell (grape)|Niabell]], and [[Cayuga White]] varieties.{{r|2008Ramming}}  The fungus can affect its [[Leaf|leaves]], [[Plant stem|stems]], [[rachis]] (stem of the grape cluster), and [[Berry (botany)|berries]].  The grape ripens in the summer (mid-season), between late July and mid-August.{{r|2006_USDA|NVRC}}

{| class="wikitable" style="margin: 1em auto 1em auto; width: 700pt;"
|+ Comparison between the Thomcord and its pedigree parents{{r|2008Ramming|2006_Sacramento_Bee}}
|-
! 
! Color
! Skin type
! Seed type
! Aborted seed size
! Growing conditions
! Flavor profile
|-
! Thompson Seedless
| White
| Adhering, thin
| Aborted
| Very small
| Hot, dry climate
| Mild & sweet
|-
! Concord
| Blue
| Separable, thick & tough
| Viable
| {{n/a}}
| High humidity
| "Foxy"
|- style="background:#ffdead"
! style="background:#ffdead" | Thomcord
| Blue-black
| Adhering, medium thickness
| Aborted
| Small
| Hot, dry climate
| "labrusca", but mild & sweet
|}

===Production details===
Thomcord is a productive variety, with a yield comparable to Thompson Seedless.  When two [[Canopy (grape)#Cordon|cordons]] (arms) of the vines are [[Vine training|trained]] horizontally on wires ("[[Vine training#Components of a grapevine|bilateral-trained]]") and are pruned to remove most of the previous year's growth ("[[Vine training#Components of a grapevine|spur-pruned]]") during the winter, it can produce up to {{convert|13|-|16|kg|lb|abbr=on}} per vine, or an average of {{convert|15.1|kg|lb|abbr=on}}.{{r|2008Ramming|2004FPS}}  In 2002, cane-pruned vines of Thomcord were significantly more productive than [[Coronation (grape)|Sovereign Coronation]] and were comparable to the Venus variety, averaging {{convert|21.3|kg|lb|abbr=on}} per vine.  Unlike Thompson Seedless, which has its cluster size thinned as a normal production practice, Thomcord's is not thinned because of its smaller cluster size.  The grape clusters range in weight between {{convert|259|and|534|g|lb|abbr=on}}{{r|2008Ramming}} and average {{convert|340|g|lb|abbr=on}},{{r|2004FPS}} have [[Grape cluster description|medium to slightly loose tightness]] (or are "well-filled", meaning the individual [[Pedicel (botany)|pedicel]]s are not easily visible), and have a conical shape with a small wing.{{r|2008Ramming|2004FPS}}

Compared with Thompson Seedless, the berry weight and diameter of Thomcord are larger, but cluster tightness is similar.  The berry length ranged between {{convert|18.2|and|18.3|mm|in|abbr=on}} and the diameter ranged from {{convert|16.7|to|17.2|mm|in|abbr=on}} in tests between 2001 and 2002.  The berries weigh between {{convert|2.72|and|3.38|g|oz|abbr=on}}, averaging {{convert|2.85|g|oz|abbr=on}} in 2002, which is on par with Venus, but heavier than Sovereign Coronation, and even more so than Thompson Seedless.  The fruit's size has not been shown to increase appreciably by [[girdling]] the vines or by applying [[gibberellic acid]] when the berries set.{{r|2008Ramming}}

The aborted seeds of Thomcord are small, but in some years they can become sclerified (a thickening and [[Lignin|lignification]] of the walls of plant cells and the subsequent dying off of the [[protoplast]]s), making them more noticeable inside the medium-soft flesh.  There are usually two aborted seeds per berry, which averaged between 14 and 22.3&nbsp;mg in 2001 and 2002.  This varied in comparison to Venus depending on the year and location, was comparable to the Sovereign Coronation, and was significantly smaller than the [[Sovereign Rose (grape)|Sovereign Rose]] and [[Saturn (grape)|Saturn]] varieties.  However, as with the other [[cultivar]]s, it was consistently larger than Thompson Seedless, which had the smallest aborted seeds.{{r|2008Ramming}}

===Vegetative description===

The mature leaves on the vine have three lobes with open upper lateral [[Sinus (botany)|sinuses]] (spaces between the lobes) of medium depth.  The main vein is slightly longer than the [[Petiole (botany)|petiole]] (stalk attaching the leaf blade to the stem), and the petiole sinus opens widely.  Between the veins on the underside of both the mature and young leaf there are dense hairs that lie flat against the surface.  The teeth on the edge of the leaf blade are convex on both sides, medium in size, and short relative to their width.  Young leaf blades are dark copper red on the upper surface.{{r|2008Ramming}}

The [[shoot]]s have at least three consecutive [[tendril]]s. Young shoots are fully open and have very dense hairs of medium [[anthocyanin]] coloration that lie flat against the tip.  The [[Plant stem|internode]] of the young shoot is green with red stripes on the front (dorsal) side and solid green on the back (ventral) side.{{r|2008Ramming}}

==History==

{{multiple image
| footer    = The Thomcord grape is a hybrid of the Thompson Seedless grape (left) and the Concord grape (right).
| image1    = Thompson seedless grapes.JPG
| width1    = 235
| alt1      = On a white plate rests a cluster of golden, green-colored Thompson Seedless grapes.
| image2    = ConcordGrapes.jpg
| width2    = 145
| alt2      = Several cluster of purple-colored Concord grapes mixed among the vine's leaves
}}
In 1983,{{r|2004FPS}} research horticulturist David W. Ramming and technician Ronald L. Tarailo—Californian grape breeders working for the ARS, the chief scientific research agency of the USDA—crossed Thompson Seedless and Concord in order to answer a technical question about a newly developed procedure for breeding novel, superior seedless grapes.{{r|2006_USDA}}  The researchers wanted to demonstrate that plants created from embryo culture were derived from fertilized eggs (zygotic) instead of the maternal tissue (somatic).{{r|2008Ramming}}  From 1231&nbsp;[[emasculation]]s (removal of male flower parts to control pollination) of Thompson Seedless, the researchers produced 130&nbsp;[[ovule]]s using [[embryo rescue procedure]]s.{{r|2008Ramming|2004FPS}}  From these, 40&nbsp;[[embryo]]s developed and three [[seedling]]s were planted.  The original seedling of Thomcord was planted in 1984 in plots in cooperation with [[California State University, Fresno]].{{r|2008Ramming}}  It was later selected in 1986 by Ramming and Tarailo and tested in the San Joaquin Valley under the name A29-67, and was introduced as "Thomcord."{{r|2008Ramming|2006_USDA}}

The new hybrid was tested and scrutinized for 17&nbsp;years before it was declared ready for growers and gardeners and was released on {{date|11 September 2003}}.{{r|2008Ramming|2006_USDA}}  Around 2008, trials outside of California were just beginning.{{r|2008Ramming}}  Thomcord quickly became a hit at [[farmers' market]]s while it was being tested, and it has appeared in the fresh-fruit section at supermarkets.{{r|TJ}} This continued the long-standing success of the ARS' grape-breeding research in California, which has developed some of the most popular seedless grapes on the market as well as red, white, and black grapes varieties for hobbyists and professional growers since 1923.{{r|2006_USDA}}

Although it has been called a "sentimental favorite" at farmers' markets, it is not expected to become a major commercial variety because its flavor is not as neutral as more popular grapes, such as Thompson Seedless, [[Crimson Seedless]], or [[Flame Seedless]].  However, Ramming predicted that it would become a specialty item, much like the [[Muscat (grape and wine)|Muscat varieties]], due to its distinctive, Concord-like flavor.{{r|2006_Sacramento_Bee}}  Because of its strong reception at farmers' markets, it could compete with Concord and Niabell varieties in eastern markets, according to Ramming.{{r|2008Ramming}}

==Availability==

The Foundation Plant Services (FPS) at the [[University of California, Davis]] indexed Thomcord and found it to be free of known viruses.  The FPS offers certified virus-free propagation material.  The FPS also deposited genetic material in the [[National Plant Germplasm System]], which offers material for research, including development and commercialization of new cultivars.  The ARS does not offer Thomcord plants for distribution.{{r|2008Ramming}}

Thomcord is a public variety and is not [[Plant quarantine|restricted]] in its propagation and distribution.{{r|2004FPS}}

==See also==
* [[List of grape varieties]]

==References==
{{Reflist|refs=

<ref name="NVRC">{{cite web | title = The USDA/ARS grape breeding program at Parlier, CA | last = Ramming | first = D.W | publisher = National Viticulture Research Conference | url = http://groups.ucanr.org/nvrc/files/40749.pdf | format = PDF | accessdate = 2 January 2011 | archiveurl = http://www.webcitation.org/5vRZcNITP | archivedate = 2 January 2011}}</ref>

<ref name="2008Ramming">{{cite journal | last = Ramming | first = D.W. | title = ‘Thomcord’ Grape | journal = HortScience | publisher = The American Society for Horticultural Science | year = 2008 | volume = 43 | issue = 3 | pages = 945–946 | url = http://etmd.nal.usda.gov/bitstream/10113/18005/1/IND44086015.pdf | format = PDF | accessdate = 2 January 2011 | archiveurl = http://www.webcitation.org/5vRaTpCRn | archivedate = 2 January 2011}}</ref>

<ref name="2006_USDA">{{cite web | title = Thomcord Grape: Flavorful, Attractive—and Seedless! | last = Wood | first = M. | url = http://www.ars.usda.gov/is/pr/2006/060616.htm | publisher = United States Department of Agriculture | date = 16 June 2006 | accessdate = 2 January 2011 | archiveurl = http://www.webcitation.org/5vQrqhg1k | archivedate = 2 January 2011}}</ref>

<ref name="2006_Sacramento_Bee">{{cite web | title = THOMCORD: New Grape Variety Draws Ready Fans | last = Pollock | first = D. | url = http://www.hintonnews.net/national/060731-shns-grape.html | publisher = Sacramento Bee | date = 31 July 2006 | accessdate = 2 January 2011 | archiveurl = http://www.webcitation.org/5vQyGABW2 | archivedate = 2 January 2011}}</ref>

<ref name="TJ">{{cite web | title = Thomcord Seedless Grapes | url = http://www.traderjoes.com/digin/post/thomcord-seedless-grapes | date = 14 August 2014 | accessdate = 27 September 2015 | archiveurl = http://www.webcitation.org/6brsmYlJj | archivedate = 27 September 2015}}</ref>

<ref name="2004FPS">{{cite journal | title = Sweet Scarlet and Thomcord — Two New Table Grape Varieties Released from ARS | journal = FPS Grape Program Newsletter | publisher = Foundation Plant Services |date=October 2004 | page = 4 | url = http://ucce.ucdavis.edu/files/filelibrary/5760/27513.pdf | format = PDF | accessdate = 2 January 2011 | archiveurl = http://www.webcitation.org/5vR1F4mKM | archivedate = 2 January 2011}}</ref>
}}
{{Hybrid grape varieties}}
{{Subject bar | portal1 = Food | portal2 = Plants}}
{{featured article}}

[[Category:Table grape varieties]]
[[Category:Hybrid grape varieties]]