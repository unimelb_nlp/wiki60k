{{Use dmy dates|date=July 2016}}
{{Use Australian English|date=July 2016}}
{{Orphan|date=April 2015}}

'''Sidney Robert Freshwater''', OAM, (1919- ) was a senior Australian [[cycling]] administrator who played a prominent role in the establishment of the professional cycling league in [[Australia]]. He was invited by the [[Union Cycliste Internationale]] to become a commissaire and was the first Australian to hold an A Grade UCI professional cycling licence.

Freshwater was awarded an [http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=886211&search_type=quick&showInd=true Medal of the Order of Australia] in 1982 for his services to the sport of cycling. Referees for his award included renowned Australian cyclist and politician [[Hubert Opperman|Sir Hubert Opperman]] and VN (Norm) Gailey MBE, President of the Australian Amateur Cycling Federation.

At the age of 16, he won the St George (Sydney, Australia) Junior Championship and was the 100 miles record holder "around the block" for more than a decade. He was also a member of the winning team in 5 NSW Road Teams Premierships.

He held an extensive list of senior organising and control positions as a sports administrator, including:
* League of NSW Wheelmen: Councillor (1943-1987): NSW handicapper (1947-1953) Honorary Secretary (1950-1955) President (1955-1969) Patron Councillor (1970-1980) Awarded life membership
* Tour of the West: Organiser and director of first annual multiple stage cycling tour in Australia (1949-1955)
* Australian Cycling Council: NSW delegate (1952,1953) National Secretary (1953-1983)
* For Good of Cycling Committee: NSW Pedals Club organiser (1955,1956) NSW & Australian Schoolboy Cycling Championship originator and organiser (1957-1961)
* Commonwealth Jubilee Cycling Council (1951)
* Australian Cycling Federation: Secretary (1983-1985) with the AACA President Norm Gailey MBA organised and established the original Australian Cycling Federation as an umbrella body to oversee the national amateur and professional bodies
* Union Cycliste Internationale: first exam 1978 in New Zealand and acted as a reserve commissaire at the 1977 World Championships in Holland. Full commissaire World Championships Leicester, UK (1982) and Goodwood (1982); Commissaire Control Panel World Junior Road and Track Championships (1982)
* Chief Commissaire Australian Professional Road Championships: Launceston (1978) Newcastle (1979) Track Championships Brisbane (1983) Lavington Carnival (1983) and Commonwealth Games Brisbane: member of Commissaire Control Panel (1982)

== References ==

{{reflist}}
* The Australian Roll of Honour 1975-96: First edition; published 1997 by Alistair Taylor, Roll of Honour Publications Pty Ltd ISBN 0 908578 57 1
* Cycling United: Freshwater returns - ACTN, February 1993 pp5–6
* Sid Freshwater by John Drummond - National Cycling, August/September 1983 pp84–86
* Life story no. 6: Sid Freshwater - The Australian Cyclist, June 1955 p18
* NSW Unity Dinner: Amateurs, independents, professionals with same ideals - The Australian Cyclist, August 1948 p33
* [http://trove.nla.gov.au/ndp/del/article/23316210?searchTerm=sid+freshwater&searchLimits=l-australian=yThe%20Argus%20Melbourne The Argus Melbourne], 15 September 1953 'Plan to extend tour'
* [http://trove.nla.gov.au/ndp/del/article/103995967?searchTerm=sid+freshwater&searchLimits=l-australian=y Western Herald] 3 July 1959 'Cycling circuit approved'
* [http://trove.nla.gov.au/ndp/del/article/49631124?searchTerm=sid+freshwater&searchLimits=l-australian=y The West Australian] 27 April 1954 'Wheelmen plan Sydney to Melbourne race'
* [http://trove.nla.gov.au/ndp/del/article/100168104?searchTerm=sid+freshwater&searchLimits=l-australian=y Goulburn Evening Post] 29 August 1951 'Goulburn-Sydney race plans'
* [http://trove.nla.gov.au/ndp/del/article/104018210?searchTerm=sid+freshwater&searchLimits=l-australian=y Western Herald] 7 June 1957 'Schoolboy race extended'


{{DEFAULTSORT:Freshwater, Sidney Robert}}
[[Category:Living people]]
[[Category:Australian male cyclists]]
[[Category:1919 births]]