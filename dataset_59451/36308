{{Use dmy dates|date=January 2013}}
{{good article}}
{{Infobox single <!-- See Wikipedia:WikiProject Songs -->
 |Name           = El Amor
 |Cover          = Arjona-elamor2011.jpg
 |Artist         = [[Ricardo Arjona]]
 |Album          = [[Independiente (Ricardo Arjona album)|Independiente]]
 |Released       = {{Start date|2011|08|23|df=y}}
 |Format         = [[Music download|Digital download]]
 |Recorded       = 2011
 |Genre          = [[Latin pop]]
 |Length         = {{Duration|m=4|s=49}}
 |Label          = Metamorfosis
 |Writer         = {{Flatlist|
*Ricardo Arjona
*[[Tommy Torres]]}}
 |Producer       = {{Flatlist|
*Ricardo Arjona
*Dan Warner 
*Lee Levin
*Tommy Torres}}
 |Last single    = "[[Marta (Ricardo Arjona song)|Marta]]"<br/>(2011)
 |This single    = "'''El Amor'''"<br/>(2011)
 |Next single    = "[[Fuiste Tú]]"<br/>(2012)
 |Misc           =
}}

"'''El Amor'''" is a [[latin pop]] song by [[Guatemala]]n recording artist [[Ricardo Arjona]], released on 23 August 2011 as the [[lead single]] from his thirteenth [[studio album]], ''[[Independiente (Ricardo Arjona album)|Independiente]]'' (2011). The song was written and produced by Arjona along with longtime collaborators Dan Warner and Lee Levin under their stage name Los Gringos, with additional production work from [[Puerto Rico|Puerto Rican]] [[singer-songwriter]] [[Tommy Torres]]. "El Amor" is the first single Arjona releases under his new record label, Metamorfosis.

Lyrically, "El Amor" was written by Arjona in an attempt to show all sides of love, explaining that "So many good things about love has been shown, that somebody had to turn it around and tell the bad ones". The song became Arjona's fourth single to reach number one on the US ''[[Billboard (magazine)|Billboard]]'' [[Top Latin Songs]], and the seventh to do so on the [[Latin Pop Songs]] chart. It also became his first song ever to reach number one on the [[Tropical Songs]] chart, improving his previous peak of number two almost twelve years prior. "El Amor" also topped several national charts from Latin America.

An accompanying [[music video]] for "El Amor" was released in September 2011. It was directed by Ricardo Calderón and filmed in [[black and white]]. The clip shows Arjona singing while a wedding is being celebrated inside a chapel. "El Amor" was performed by Arjona as part of a televised show broadcast by [[Televisa]], which included guest appearances by [[Gaby Moreno]], Ricky Muñoz from Mexican band [[Intocable]] and [[Paquita la del Barrio]]. It was also included on the set list for his ongoing tour, the [[Metamorfosis World Tour]].

==Background==
[[File:Arjona.jpg|thumb|200px|left|In 2011, Arjona founded his own record label, [[Metamorfosis (label)|Metamorfosis]]. "El Amor" is the first single released under the label.]]
''Independiente'' is the first album Arjona released as an independent artist, and through his own record label, [[Metamorfosis (label)|Metamorfosis]], a company he created to refocus his career.<ref>(12 October 2011). [http://musica.univision.com/noticias/article/2011-10-12/ricardo-arjona-billboard-independiente-disco-oro "Ricardo Arjona, de nuevo en la cima con 'Independiente'"] (in Spanish). ''Univisión Musica''. Retrieved 15 October 2011.</ref> Presided by Arjona and some friends, ''Metamorfosis'' is based in Miami and Mexico City,<ref name=indep34>(7 July 2011). [http://www.sandiegored.com/noticias/14853/Ricardo-Arjona-sacara-un-nuevo-disco-bajo-su-propio-sello-independiente/ "Ricardo Arjona sacará un nuevo disco bajo su propio sello independiente"] (in Spanish). ''San Diego Red''. Retrieved 30 October 2011.</ref> and also includes the [[photographer]] and [[Director (film)|director]] Ricardo Calderón, [[Universal Music Group|Universal Music México]]'s executive Humberto Calderon and [[BMG Music|BMG]]'s Miriam Sommerz.<ref name=indep34/><ref>[http://www.metamorfosis.be/somos somos < metamorfosis.be] (in Spanish). Metamorfosis. Retrieved 30 October 2011. {{webarchive |url=https://web.archive.org/web/20120105020247/http://www.metamorfosis.be/somos |date=5 January 2012 }}</ref> Although the album is marketed with the new label, distribution was handled by [[Warner Music]]. Arjona commented many times, that he considered the way he decided to go independent raised more compromise than freedom, saying that "Inside the word 'Independent', even when it sounds like extreme freedom, there's a big amount of compromise and the responsibility of being able to administrate, in the best way possible, such independence."<ref>(4 October 2011). [http://www.el-nacional.com/noticia/3648/21/Ricardo-Arjona:-Estoy-empezando-de-nuevo-y-me-gusta.html "Ricardo Arjona: Estoy empezando de nuevo y me gusta"] (in Spanish). ''El Nacional'' (Venezuela). Retrieved 15 October 2011.</ref> 

''Independiente'' was composed and written within one year,<ref>(3 October 2011). [https://www.google.com/hostednews/epa/article/ALeqM5h7VYAUB5BW4JQuJJXg6tx5pA-4bA?docId=1621707 "Arjona goza de "libertad extrema" consciente de sus riesgos"] (in Spanish). EFE Agency. Retrieved 15 October 2011.</ref> and marks the fourth time Arjona had collaborated with [[Tommy Torres]], who had helped writing, composing, producing and providing backing vocals. The other three albums in which the two artists had worked together are ''[[Quién Dijo Ayer]]'', in which Torres helped producing the singles "[[Quién (Ricardo Arjona song)|Quién]]" and "[[Quiero (Ricardo Arjona song)|Quiero]]", and offering additional work on the new versions of Arjona's hits; ''[[5to Piso]]'', and ''[[Adentro]]'', respectively. Also, in the album, Arjona returned to his classic and trademark sound, which Torres has helped crafting it since six years now, after the drastic change he made in ''[[Poquita Ropa]]''. On that album, the artist made use of the fewest instruments possible, simplifying his sound, and introducing what he called a "stripped-down version of his music".<ref>(26 August 2010). [http://www.lapatria.com/story/arjona-presenta-canciones-con-poquita-ropa "Arjona presenta canciones con Poquita Ropa"]{{dead link|date=June 2012}} (in Spanish). ''La Patria''. Retrieved 15 October 2011.</ref>

Weeks before the release of ''Independiente'', Arjona issued a letter in which he talked about his past relations with recording companies. In the letter, he revealed that he entered in his first record label as an exchange, commenting that "a producer, friend of mine, told them [the record label] that if they don't sign me they won't sign two artists he had [at that time]", and that he received the "minimum royalty percentage" out from his most successful albums.<ref>(26 September 2011). [http://www.emol.com/noticias/magazine/2011/09/26/505071/las-razones-de-la-independencia-de-arjona.html "Arjona desclasifica los episodios menos glamorosos de su paso por las discográficas"] (in Spanish). ''Emol.com''. Retrieved 15 October 2011.</ref> ''[[Billboard (magazine)|Billboard]]'' notes that although other groups have decided to launch independently their works after having a contract with major record labels, Arjona is by far the most important artist in the Latin pop to follow this trend.<ref name=indep34/>

==Composition==
{{Listen
 |filename     = El_Amor_Ricardo_Arjona.ogg
 |title        = "El Amor"
 |description  = A 24-second sample of "El Amor", displaying the opening verse as sung by Arjona. He considers the song to be his "most tawdry" single to date and to contain "those big dark events within love that nobody talk about".
 |pos          = right
}}
"El Amor" is a piano and guitar mid-tempo [[Latin pop]] ballad written by Arjona and produced by himself alongside longtime collaborators Dan Warner, Lee Levin and [[Tommy Torres]].<ref name=liner/> Recording work was held by Torres, alongside Carlos "Junior" Cabral, Isaias García, Jerald Romero and Dan Rudin. The song is the first lead single to be produced by Arjona along with Torres since ''5to Piso''{{'s}} "[[Como Duele (Ricardo Arjona song)|Como Duele]]" back in 2008.<ref>''5to Piso'' (Digital booklet). [[Ricardo Arjona]]. Warner Music Mexico: a division of the [[Warner Music Group]]. 2008.</ref> ''[[Poquita Ropa]]''{{'s}} lead single, "[[Puente (song)|Puente]]" (2010), was produced by Arjona and Dan Warner.<ref>''Poquita Ropa'' (Digital booklet). [[Ricardo Arjona]]. Warner Music Mexico: a division of the [[Warner Music Group]]. 2010. 825646802180.</ref> Torres also provided background vocals on the song.<ref name=liner/> 

"El Amor" was written by Arjona in an attempt to show all sides of love. As said by Arjona himself, "So many good things about love has been shown, that somebody had to turn it around and tell the bad ones".<ref>[http://www.informador.com.mx/entretenimiento/2011/328959/6/ricardo-arjona-obtiene-numero-uno-con-su-primer-sencillo-el-amor.htm "Ricardo Arjona obtiene número uno con su primer sencillo 'El amor'"] (in Spanish). El Informador. Retrieved 15 October 2011.</ref> The development of "El Amor" was motivated by Arjona's idea of showing "those big dark events within love that nobody talk about", saying that "love's dark sides are really fundamental to understand its great value".<ref>(19 September 2011). [http://rumberos.net/rumberos/index.php?option=com_content&view=article&id=17507:ricardo-arjona-muestra-los-distintos-angulos-de-el-amor-&catid=3:newsflash&Itemid=3 "Ricardo Arjona muestra los distintos ángulos de ''El Amor''"] (in Spanish). Rumberos.net. Retrieved 15 October 2011.</ref> 

In an interview in February 2012, the singer stated that "El Amor" was the "most tawdry" song he has released throughout his career. He further added that the fact that they chose the song was a "contradiction" because it was not "the song which could better represent the entire album." He also added that the song was "very strong" and "a bit dark".<ref name=PL34>{{cite web|title=Ricardo Arjona piensa que "El amor" es cursi|url=http://www.prensalibre.com/espectaculos/Ricardo-Arjona-piensa-amor-cursi_0_641336087.html|publisher=Prensa Libre|accessdate=12 July 2012|language=Spanish}}</ref>  The song marks the return of the signature and more mainstream sound of Arjona on a music basis, after the multi-genre and politically charged ''[[Poquita Ropa]]'''s lead single, "[[Puente (song)|Puente]]", which failed to make impact on the United States<ref name=Latin/> and only managed to reach #36 on the Latin Pop Songs chart.<ref name=Lpop/>

==Chart performance==
In the United States, "El Amor" became Arjona's first top ten single in the ''Billboard'' [[Top Latin Songs]] since "[[Sin Ti... Sin Mi]]" in 2008 and the first to chart there since "[[Tocando Fondo]]" in 2009.<ref name=Latin/> The song also restored Arjona's chart success, after the ''Poquita Ropa'' era, on which none of the three singles released off the album managed to attain chart success, and eventually only one appeared on any chart.<ref name=Latin/> The song finally reached number one position on the list, becoming his fourth chart-topper, and the first to do so since "[[El Problema]]", ten years prior.<ref name=Latin/><ref>{{cite news|url=http://www.billboard.com/charts/2011-10-22/latin-songs|title=Top Latin Songs|date=22 October 2011|work=Billboard| location=United States | publisher=Nielsen Business Media|accessdate=22 February 2012}}</ref> On the [[Latin Pop Songs]] chart, "El Amor" became Arjona's seventh song to reach the top spot of the chart, and the first to do so since "[[Como Duele (Ricardo Arjona song)|Como Duele]]" in 2008.<ref name=Lpop/><ref>{{cite news|url=http://www.billboard.com/charts/2011-10-29/latin-pop-songs|title=Latin Pop Songs|date=29 October 2011|work=Billboard|accessdate=22 October 2011| publisher=Nielsen Business Media|location=United States}}</ref> Also, "El Amor" became his first song ever to reach number one on the [[Tropical Songs]] chart, breaking his previous record held by "Cuando", which reached number two twelve years prior, in 2000.<ref name=TRO-EA>{{cite web|title=Tropical Songs|url=http://www.billboard.com/charts/2011-10-22/tropical-songs|work=Billboard|publisher=Nielsen Business Media|accessdate=12 July 2012}}</ref><ref name="RAW">{{cite news|title=Ricardo Arjona: Awards|url=http://www.allmusic.com/artist/ricardo-arjona-mn0000889126/awards|work=Allmusic|location=United States|publisher=Rovi Corporation|accessdate=6 June 2012}}</ref> In Venezuela, "El Amor" reached a peak of number five.<ref name=RecordReport/> It also reached number six on Mexico.<ref name=MonitorLatino/> The song also became a hit in the rest Latin America, reaching number one in Argentina, Colombia, Chile, Costa Rica, Panama and Guatemala.<ref>{{cite web|title='Independiente' de Ricardo Arjona, primer lugar en iTunes en Estados Unidos y México |url=http://www.cmi.com.co/?n=71295 |publisher=CM& |accessdate=14 July 2012 |deadurl=yes |archiveurl=http://www.webcitation.org/67Uxs1LlY?url=http://www.cmi.com.co/?n=71295 |archivedate=8 May 2012 |df=dmy }}</ref><ref name=latincharts1>(30 September 2011). [http://www.larazon.com.ar/show/Arjona-independiente_0_282000146.html "Un Arjona "independiente""] (in Spanish). ''La Razón''. Retrieved 15 October 2011.</ref> On the ''Billboard'' year-end charts for 2011, "El Amor" finished at number 73 on the Top Latin Songs chart.<ref name=LSY/> The song also finished within the top fifty songs on the Latin Pop Songs and Tropical Songs charts, appearing at number 41 and number 44, respectively.<ref name=LPY/><ref name=TRY/>

==Release and promotion==
"El Amor" was released as the first single from ''Independiente'' on 23 August 2011 in Canada, United States and Mexico as a [[Music download|digital download]] through the [[iTunes Store]].<ref name=REL/> The single was later made available on the rest of Latin America and some European countries such as the United Kingdom, Spain, Germany and France on 6 September 2011.<ref name=REL/> "El Amor" is the first single Arjona released under his recently founded record label, [[Metamorfosis (label)|Metamorfosis]].<ref name=indep34/>

===Music video===
[[File:Arjona Santiago Chile 2012.jpg|thumb|250px|right|Arjona performed "El Amor" alongside new songs and past hits on his [[Metamorfosis World Tour]], as well as on a program broadcast by [[Televisa]] in 2011.]]
Ricardo Arjona released on 8 September 2011 the [[music video]] for "El Amor".<ref name=MV>{{cite web|title=Ricardo Arjona estrena a nivel mundial su nuevo video clip ´El amor´|language=Spanish|url=http://www.rpp.com.pe/2011-09-08-ricardo-arjona-estrena-a-nivel-mundial-su-nuevo-video-clip-el-amor-noticia_402136.html|work=RPP Noticias|publisher=Grupo RPP|accessdate=10 July 2012|location=Perú|date=8 September 2011}}</ref> It debuted on Arjona's official website, and then was sent to music channels around United States and Latin America. The clip was shot in Mexico City and was filmed entirely in [[black-and-white]].<ref name=MV/><ref>{{cite web|date=8 September 2011|last=Torres|first=Emmy|title=Ricardo Arjona estrena el video clip "El Amor"|url=http://www.entornointeligente.com/articulo/1168792/Ricardo-Arjona-estrena-el-video-clip-El-Amor|publisher=Entorno Inteligente|accessdate=10 July 2012|language=Spanish}}</ref> The music video was directed by Ricardo Calderón, who has also directed the music video for Arjona's 2008 single "Como Duele".<ref name=MV/> Calderón also produced the video, as well as developing the story and design.<ref name=MV/> Before releasing the video, the singer launched a contest on his website that consisted on uploading to his official [[YouTube]] account videos related to the song, and the winner received a trip to Argentina to meet him.<ref>{{cite web|title=Cantante guatemalteco Ricardo Arjona estrena video de su sencillo titulado El amor|language=Spanish|url=http://www.prensalibre.com/espectaculos/Cantante-guatemalteco-Ricardo-Arjona-titulado_0_550745054.html|publisher=Prensa Libre|accessdate=10 July 2012}}</ref> 

The clip starts with a bride reaching, on a limousine, the chapel on which she's about to get married. As she is shown walking inside the chapel's doors, Arjona starts to sing while sit on the chairs in the empty chapel. Then, the bride is shown again alongside the groom in the altar, ready to make the vows, while the chapel is filled with the invitees. Then, scenes of Arjona playing the piano and singing, and scenes of the ceremony are interpolated, showing the behaviour of the invitees. Later, the invitees are shown involved in discussions and fights while Arjona keeps singing the chorus and verses of the song. As the fights between the invitees increase, the groom rejects the bride and she, stunned, escapes from the chapel. Then, it is shown that all was an illusion from the bride, and they are shown exiting the chapel, married.<ref name=YT>{{cite web|title=Ricardo Arjona – El Amor (Video Oficial)|url=https://www.youtube.com/watch?v=vWtJJRTqVSI|work=[[YouTube]]|publisher=Google, Inc.|accessdate=10 July 2012}}</ref> As of 11 July 2012, the video has reached 14 million views on YouTube.<ref name=YT/>

===Live performances===
"El Amor" was in the set list for a televised program in 2011. The special included guest singers such as [[Gaby Moreno]], Ricky Muñoz (from Mexican band [[Intocable]]) and [[Paquita la del Barrio]].<ref>{{cite news|last=Morán|first=Barbara|title=Ricardo Arjona dará un especial de TV sobre su nuevo disco Independiente|language=Spanish|url=http://entretenimiento.starmedia.com/musica/ricardo-arjona-dara-especial-tv-sobre-su-nuevo-disco-independiente.html|work=Starmedia|publisher=Orange Corporation|location=Venezuela|date=4 October 2011|accessdate=6 July 2012}}</ref> Broadcast by [[Televisa]], the program was made to showcast the new fourteen songs included on ''Independiente''. Ricky Muñoz commented that he was "happy to do things for Ricardo [Arjona]" and elaborated that the met each other "some time ago" and that it was "a very special situation."<ref>{{cite web|title=Arjona llega a la TV con un programa especial|url=http://conexiontotal.mx/2011/10/03/arjona-llega-a-la-tv-con-un-programa-especial/|publisher=Conexión Total|accessdate=10 July 2012|language=Spanish}}</ref> The show was later bordcasted on 5 November 2011 by [[Canal de las Estrellas]].<ref>{{cite web|title=Ricardo Arjona realiza inedito dueto junto a Paquita la del Barrio|url=http://www.lanacion.cl/ricardo-arjona-realiza-inedito-duo-junto-a-paquita-la-del-barrio/noticias/2011-11-03/164013.html|language=Spanish|work=La Nación|publisher=Empresa Periodistica La Nación S.A.|accessdate=10 July 2012|location=Chile|date=3 November 2011}}</ref> The song is also present on his ongoing [[Metamorfosis World Tour]]. It is performed while on the first of the four ambiences on the concert, alongside "Lo Que Está Bien Está Mal", "Animal Nocturno", "Hay Amores" and "Desnuda".<ref name="show5">Trapé, Oscar. [http://www.diariouno.com.ar/espectaculos/Ricardo-Arjona-convoco-a-mas-de-25.000-personas-en-el-Malvinas-Argentinas-20120426-0003.html "Ricardo Arjona convocó a más de 25.000 personas en el Malvinas Argentinas"] (in Spanish). Diario Uno. Retrieved on 26 April 2012.</ref>
{{clear}}

==Credits and personnel==
Credits are taken from ''Independiente'' [[liner notes]].<ref name=liner>''Independiente'' (booklet). [[Ricardo Arjona]]. [[Mexico City]]: ''Metamorfosis'', a division of [[Warner Music Latina|Warner Music Mexico S.A. de C.V]]. 2011. 825646649396.</ref>
{{div col|3}}
*Ricardo Arjona – chorus
*Dan Warner – bass, guitar, recording engineering
*Lee Levin – drums, percussion, recording engineering
*Matt Rollings – piano, Hammond B3, recording engineering
*Tommy Torres – chorus, recording engineering
*Chris MacDonald – arrangement, directing
*Pamela Sixfin – violin
*David Angell – violin, viola
*Connie Ellisor – violin
*Mary Katherine VanOsdale – violin
*Karen Winkelmann – violin
*Carolyn Bailey – violin
*Erin Hall – violin
*Zeneba Bowers – violin
*Cornelia Heard – violin
*James Grosjean – viola
*Elizabeth Lamb – viola
*Anthony LaMarchina – cello
*Julia Tanner – cello
*Craig Nelson – bass
*Carlos "Junior" Cabral – recording engineering
*Isaias García – recording engineering
*Jerald Romero – recording engineering
*Dan Rudin – recording engineering
*David Thoener – mixing
{{div col end}}

==Trackslisting==
*'''Digital Download'''
#"El Amor" – 4:47

*'''Remix featuring O'Neill'''
#"El Amor" {{small|(featuring O'Neill)}} – 4:02

==Charts==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable sortable" border="1"
|-
! Chart (2011)
! Peak<br />position
|-
|Mexico ([[Monitor Latino]])<ref name=MonitorLatino>{{cite web|url=http://www.monitorlatino.com/ |title=Monitor Latino |archiveurl=http://www.webcitation.org/62oDcFYG0?url=http://www.monitorlatino.com/ |archivedate=30 October 2011 |publisher=[[Monitor Latino]] |deadurl=yes |accessdate=13 June 2014 |df=dmy }}</ref>
| style="text-align:center;"|6
|-
|Mexico ([[Billboard (magazine)|''Billboard'' International]])<ref>{{cite news|title=Mexican Airplay|url=http://www.billboard.biz/bbbiz/charts/chart-search-results/albums/12007957|date=29 October 2011|work=Billboard|location=United States|publisher=Nielsen Business Media|accessdate=6 September 2012}}</ref>
| style="text-align:center;"|7
|-
{{singlechart|Billboardlatinsongs|1|refname=Latin|artist=Ricardo Arjona|artistid={{BBID|Ricardo Arjona}}|accessdate=6 July 2012}}
|-
{{singlechart|Billboardlatinpopsongs|1|refname=Lpop|artist=Ricardo Arjona|artistid={{BBID|Ricardo Arjona}}|accessdate=6 July 2012}}
|-
{{singlechart|Billboardtropicalsongs|1|refname=trop|artist=Ricardo Arjona|artistid={{BBID|Ricardo Arjona}}|accessdate=6 July 2012}}
|-
| Venezuela Top Latino ([[Record Report]])<ref name=RecordReport>{{cite web|title=Top Latino|archiveurl=https://web.archive.org/web/20111209132315/http://www.recordreport.com.ve/publico/?i=latino|url=http://www.recordreport.com.ve/publico/?i=latino|archivedate=9 December 2011 |publisher=[[Record Report]]|accessdate=6 July 2012}}</ref>
|align="center"|4
|-
|}
{{col-2}}

===Yearly charts===
{| class="wikitable sortable plainrowheaders"
|-
!align="center"|Chart (2011)
!align="center"|Peak<br />position
|-
|align="left"|[[US]] [[Latin Songs]] (''[[Billboard (magazine)|Billboard]]'')<ref name=LSY>[http://www.billboard.com/charts/year-end/2011/hot-latin-songs?begin=71&order=position "Top Latin Songs of 2011"] ''Billboard''. Nielsen Business Media. Retrieved on 20 April 2012.</ref>
| style="text-align:center;"|73
|-
|align="left"|US [[Latin Pop Songs]] (''[[Billboard (magazine)|Billboard]]'')<ref name=LPY>[http://www.billboard.com/charts/year-end/2011/hot-latin-pop-songs?begin=41&order=position "Latin Pop Songs of 2011"]. ''Billboard''. Nielsen Business Media. Retrieved on 20 April 2012.</ref>
| style="text-align:center;"|41
|-
|align="left"|US [[Tropical Songs]] (''[[Billboard (magazine)|Billboard]]'')<ref name=TRY>[http://www.billboard.com/charts/year-end/2011/hot-tropical-songs?begin=41&order=position "Tropical Songs of 2011"]. ''Billboard''. Nielsen Business Media. Retrieved on 20 April 2012.</ref>
| style="text-align:center;"|44
|-
|}
{{col-end}}

==Release history==
{| class="wikitable plainrowheaders"
|+ Digital releases<ref name=REL>Digital vendors information:
*Canada: [http://itunes.apple.com/ca/album/el-amor-single/id457758007 "El Amor – Single"]. [[iTunes Store]] (Canada). Apple, Inc. Retrieved on 19 April 2012.
*United States: [http://itunes.apple.com/mx/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (United States). Apple, Inc. Retrieved on 19 April 2012.
*Mexico: [http://itunes.apple.com/mx/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Mexico). Apple, Inc. Retrieved on 19 April 2012.
*Spain: [http://itunes.apple.com/es/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Spain). Apple, Inc. Retrieved on 19 April 2012.
*United Kingdom: [http://itunes.apple.com/gb/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (UK). Apple, Inc. Retrieved on 19 April 2012.
*France: [http://itunes.apple.com/fr/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (France). Apple, Inc. Retrieved on 19 April 2012.
*Argentina: [http://itunes.apple.com/ar/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Argentina). Apple, Inc. Retrieved on 19 April 2012.
*Germany: [http://itunes.apple.com/de/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Germany). Apple, Inc. Retrieved on 19 April 2012.
*Venezuela: [http://itunes.apple.com/ve/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Venezuela). Apple, Inc. Retrieved on 19 April 2012.
*Chile: [http://itunes.apple.com/cl/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Chile). Apple, Inc. Retrieved on 19 April 2012.
*Portugal: [http://itunes.apple.com/pt/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Portugal). Apple, Inc. Retrieved on 19 April 2012.
*Brazil: [http://itunes.apple.com/br/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Brazil). Apple, Inc. Retrieved on 19 April 2012.
*Italy: [http://itunes.apple.com/it/album/el-amor-single/id457758007 "El Amor – Single"]. iTunes Store (Italy). Apple, Inc. Retrieved on 19 April 2012.
</ref>
! scope="col"| Country   
! scope="col"| Date   
! scope="col"| Format
! scope="col"| Label
|-
! scope="row"| [[Canada]]
| rowspan="3"| 23 August 2011
| rowspan="13"| [[Music download|Digital download]]
| rowspan="13"| Metamorfosis / [[Warner Music]]
|-
! scope="row"| [[United States]]
|-
! scope="row"| [[Mexico]]
|-
! scope="row"| [[Spain]]
| rowspan="10"| 6 September 2011
|-
! scope="row"| [[United Kingdom]]
|-
! scope="row"| [[France]]
|-
! scope="row"| [[Argentina]]
|-
! scope="row"| [[Germany]]
|-
! scope="row"| [[Venezuela]]
|-
! scope="row"| [[Chile]]
|-
! scope="row"| [[Portugal]]
|-
! scope="row"| [[Brazil]]
|-
! scope="row"| [[Italy]]
|-
|}

==References==
{{Reflist|3}}

{{Ricardo Arjona}}
{{Ricardo Arjona singles}}

{{DEFAULTSORT:Amor, El}}
[[Category:2011 singles]]
[[Category:Ricardo Arjona songs]]
[[Category:Billboard Hot Latin Songs number-one singles]]
[[Category:Songs written by Ricardo Arjona]]
[[Category:2011 songs]]
[[Category:Spanish-language songs]]