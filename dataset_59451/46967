The '''Charter of Liberties and Privileges''' was an act passed by the [[Province of New York|New York]] colonial assembly during its first session in 1683 that laid out the political organization of the colony, set up the procedures for election to the assembly, created 12 counties, and guaranteed certain individual rights for the colonists.  The colony operated under the Charter until May 1686 when [[Thomas Dongan]], the governor of New York, received instructions from [[King James II]] that New York would be assimilated into the [[Dominion of New England]].   After the [[Glorious Revolution]] [[William and Mary]] appointed a new governor, who convened the colonial assembly on April 5, 1691.

== Background ==

James, the Duke of York and the colonial proprietor of New York, was in exile in Brussels and Edinburgh from 1679-1681 during the Exclusion Crisis. Upon his return to England he appointed Dongan to succeed Edmund Andros as governor of New York. Dongan’s instructions, which were sealed by James on January 27, 1683, specifically directed Dongan to hold elections for a colonial assembly. Several factors contributed to James decision to extend representative government to his colony.[1]

First, James was operating from a position of political weakness in England. He had been forced into exile by fears surrounding his succession and, although his brother Charles II had largely assuaged the fears of the exclusionists, there was still general popular uneasiness with his Catholicism.

Second, the early 1680s were a time when legal opinions were generally in favor of the independence of local assemblies. In 1677 the Lords of Trade had tried to restrict the Jamaica assembly by writing Poynings' Law into the governor’s commission. This would essentially reduce the Jamaica assembly – as Poynings' Law had reduced the Irish Parliament – to merely approving laws initiated in the English Parliament with no original powers of its own. In response, Jones – the attorney general of England – issued an opinion holding that the people of Jamaica could only be governed by laws made there under the King’s authority.

Third, New York was suffering from poor economic and political conditions in the early 1680s. As a result of these problems, Dongan was presented with a petition for representative government by the people of East Hampton upon his arrival in the summer of 1683. Although this petition could not have directly influenced James decision to allow for an assembly (since he had issued the instructions more than 6 months earlier), it is indicative of the general feeling of unrest in the colony at the time.

These factors, combined with the fact that every other colony had a local assembly, convinced James that the calling of an assembly was the only way to insure stability and prosperity in the New York. In addition, recent struggles in Pennsylvania and New Jersey in which the colonial assemblies in those colonies asserted their right to representative government and to certain individual liberties informed the subsequent actions of the New York Assembly. Viewed in this light, the passing of the Charter is another instance of colonists attempting to carve out more space for themselves in their relationship with their proprietor.[2]

== Approval and Revocation ==

The Charter was approved by Dongan and his council, and on October 31, 1683, it was published by voice at City Hall.  A year later, James had signed the Charter in England, but the death of [[Charles II of England|Charles II]] and [[James II of England|James’s]] ascension to the throne caused the Charter never to be delivered to New York.

Instead, James was convinced – from his new perspective as sovereign – that the Charter gave colonists in New York rights and privileges that were too broad.  The investment of legislative authority in the governor, council, and “people” in the general assembly, for instance, was broader than in other colonies.  Further, James felt that the Charter made the governor too dependent on his council and the insistence on triennial legislatures put a greater obligation on that government than on any other colonial one.

As a result, James did not confirm the Charter.  Instead he assimilated New York into the newly formed [[Dominion of New England]] under Governor [[Edmund Andros]].  However, it was not until May 1686 that Dongan received a new set of instructions declaring that the Charter be disallowed.  In the interim, the colony operated as if the Charter were in place, with the assembly meeting for a total of three times.  Upon receipt of his new instructions, Dongan read them to his council but not to the assembly, which was not called again.<ref name=Kammen>{{cite book|last=Kammen|first=Michael|title=Colonial New York: A History|year=1975|publisher=Charles Scribner's Sons|location=New York|isbn=0-684-14325-9|pages=426}}</ref>

== Effects of the Glorious Revolution ==

In 1689, after word that James had been overthrown reached the colonies, Bostonians rose up and overthrew Andros and New Yorkers – led eventually by [[Jacob Leisler]] – rose up and took control of the colony from Lieutenant Governor Nichols.  By 1691 William and Mary had appointed [[Henry Sloughter]] as the new governor, and he convened a new assembly which enacted “An act for declaring what are the rights and privileges of their Magesties’ subjects inhabiting within the province of New York."<ref name=Chester>{{cite book|last=Chester|first=Alden|title=Legal and Judicial History of New York|year=1911|publisher=National Americana Society|location=New York|isbn=0-89941-297-1}}</ref>

== References ==
{{Reflist}}

== External links ==
* http://www.montauk.com/history/seeds/charter.htm

<!--- Categories --->





[[Category:Articles created via the Article Wizard]]
[[Category:Pre-statehood history of New York]]
[[Category:Thirteen Colonies documents]]