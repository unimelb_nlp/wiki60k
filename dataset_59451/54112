{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox MP
| honorific-prefix = [[The Honourable]]
| name = Mark Butler
| honorific-suffix = [[Member of Parliament|MP]]
| image = Mark Butler 2016.jpg
|office            = [[Minister for Climate Change (Australia)|Minister for Climate Change]]
|term_start         = 1 July 2013
|term_end        = 18 September 2013
|primeminister      = [[Kevin Rudd]]
|predecessor        = [[Greg Combet]]
| successor         = ''Office abolished''
|office1             = [[Minister for Sustainability, Environment, Water, Population and Communities (Australia)|Minister for the Environment and Water]]
|term_start1         = 1 July 2013
|term_end1           = 18 September 2013
|primeminister1      = [[Kevin Rudd]]
|predecessor1        = [[Tony Burke]]
| successor1         = [[Greg Hunt]]
|office2 = [[Minister for Social Inclusion (Australia)|Minister for Social Inclusion]]
|primeminister2 = [[Julia Gillard]]<br>[[Kevin Rudd]]
|term_start2 = 14 December 2011
|successor2= ''[[Minister for Social Inclusion (Australia)|Office Abolished]]''
|predecessor2 = [[Tanya Plibersek]]
|term_end2 = 1 July 2013
|office3 = [[Minister for Mental Health and Ageing (Australia)|Minister for Mental Health and Ageing]]
|primeminister3 = [[Julia Gillard]]<br>[[Kevin Rudd]]
|term_start3 = 12 September 2010
|predecessor3 = [[Nicola Roxon]] 
|term_end3 = 1 July 2013
|successor3 = [[Jacinta Collins]]
| constituency_MP4 = [[Division of Port Adelaide|Port Adelaide]]
| parliament4 = Australian
| majority4 = 
| predecessor4 = [[Rod Sawford]]
| successor4 = 
| term_start4 = 24 November 2007
| term_end4 = 
| office5 = National President of the<br>[[Australian Labor Party]]
| term_start5 = 17 June 2015
| term_end5 =
| predecessor5 = [[Jenny McAllister]]
| successor5 =
| birth_date = {{birth date and age|df=yes|1970|7|8}}
| birth_place = 
| death_date = 
| death_place = 
| nationality = Australian
| spouse = Suzanne Critchley
| party = [[Australian Labor Party]]
| relations = 
| children = 2
| residence =
| alma_mater =[[University of Adelaide]]<br/>[[Deakin University]] 
| occupation = 
| profession =[[Trade unionist]]<br/>[[Politician]]
| religion =
| signature =
| website = 
| footnotes =
}}
'''Mark Christopher Butler''' (born 8 July 1970) is an [[Politics of Australia|Australian politician]], representing the electoral division of [[Division of Port Adelaide|Port Adelaide]] in the [[Parliament of Australia|Commonwealth Parliament]] since 2007. He is a member of the [[Australian Labor Party|Australian Labor Part]]<nowiki/>y.

Before entering parliament, Butler was the South Australian secretary of the [[United Voice|Liquour, Hospitality and Miscellaneous Workers Union (LHMU)]].

Butler served as [[Minister for Climate Change (Australia)|Minister for Climate Change]] and [[Minister for Sustainability, Environment, Water, Population and Communities (Australia)|Minister for the Environment, Heritage and Water]] in the [[Second Rudd Ministry]]. Since the [[Australian federal election, 2013|2013 Federal Election]] he has been the [[Shadow Cabinet|Shadow Minister]] for Environment, Climate Change and Water.

== Early life ==
Butler was born in [[Canberra]] on 8 July 1970. He is the great-great-grandson and great-grandson respectively of conservative [[Premier of South Australia|Premiers]] of South Australia, [[Richard Butler (Australian politician)|Sir Richard B. Butler]] and [[Richard Layton Butler|Sir Richard L. Butler]].<ref name=":0">{{Cite web|url = http://www.hawkerbritton.com/images/data/Mark%20Butler%20(October%2013).pdf|title = Hawker Britton profile: Hon Mark Butler MP|date = October 2013|access-date = 20 January 2016|website = Hawker Britton|publisher = |last = |first = }}</ref>

Butler was educated at the [[University of Adelaide]], where he graduated with a [[Bachelor of Arts]] in [[Jurisprudence]] and a [[Bachelor of Laws]] with [[First Class Honours]]. He later completed a [[Master of International Affairs|Master of International Relations]] degree at [[Deakin University]].<ref name=":0" />

Butler was active in [[Student activism|student politics]] while at university and became friends with future South Australian Labor Party leaders including [[Penny Wong]] and [[Jay Weatherill]].<ref>{{Cite web|title = Freakish powers of a formidable operator|url = http://www.smh.com.au/news/national/freakish-powers-of-a-formidable-operator/2007/12/07/1196813021299.html?page=fullpage|website = Sydney Morning Herald|date = 8 December 2007}}</ref>

== Trade union career ==
In 1996 Butler became the Secretary of the South Australian branch of the [[United Voice|Liquour, Hospitality and Miscellaneous Workers Union (LHMU),]] and in 1997 he was elected the youngest-ever President of the Labor Party in South Australia.<ref name=":1">{{Cite web|title = Political fixers - Mark Butler|url = http://www.thepowerindex.com.au/political-fixers/mark-butler|website = The Power Index|access-date = 2016-01-20|date = 26 July 2011|publisher = |last = Barry|first = Paul}}</ref>

Butler was a key figure in the [[National Left (Australia)|Labor Left]] faction. He developed a close working relationship with his NSW counterpart [[Anthony Albanese]] and represented the faction on the ALP National Executive since 2000.<ref name=":1" /> He would later serve as Albanese's campaign manager in the [[Australian Labor Party leadership spill, October 2013|October 2013 election]] for the Federal ALP Leadership.<ref>{{Cite web|title = Labor leader pair make their pitch to party faithful|url = http://www.smh.com.au/federal-politics/political-news/labor-leader-pair-make-their-pitch-to-party-faithful-20130924-2uciw.html|website = The Sydney Morning Herald|access-date = 2016-01-20|date = 25 September 2013|last = Kenny|first = Mark}}</ref>

Butler was also noted for his constructive relationship with the [[Labor Right]] faction in South Australia, particularly then-Secretary of the [[Shop, Distributive and Allied Employees Association]], [[Don Farrell]].<ref name=":1" />

==Political career==
Butler was elected as the Labor member for the electoral division of [[Division of Port Adelaide|Port Adelaide]] at the [[Australian federal election, 2007|2007 Federal Election]].

In a 2009 [[First Rudd Ministry]] reshuffle, Butler was appointed Parliamentary Secretary for Health. On 14 September 2010, he was sworn in as [[Minister for Mental Health and Ageing (Australia)|Minister for Mental Health and Ageing]] in the [[Second Gillard Ministry]]. On 12 September 2011 he was given the additional responsibility of Minister Assisting the Prime Minister on Mental Health Reform. On 14 December 2011, Butler's ministry was renamed Mental Health and Aged Care, and he became a member of Cabinet.<ref>{{cite news |url=http://www.abc.net.au/news/2011-12-12/gillard-announces-cabinet-reshuffle/3726500 |author=Thompson, Jenny |title=Gillard unveils expanded Cabinet |date=12 December 2001 |work=[[ABC News (Australia)|ABC News]] |location=Australia |accessdate=11 July 2013 }}</ref>

After the 2013 election, [[Bill Shorten]] named Butler as the Shadow Minister for the Environment.

On 17 June 2015, Butler was elected National President of the Australian Labor Party.<ref>http://www.abc.net.au/news/2015-06-17/mark-butler-named-new-national-president-of-labor-party/6553702</ref>

==Personal life==
Butler lives in [[Woodville Park, South Australia|Woodville Park]] with his wife and two children. He supports the [[Port Adelaide Football Club]].<ref>[http://www.alp.org.au/mark_butler Mark Butler profile: ALP]</ref>

==See also==
* [[First Rudd ministry]]
* [[First Gillard ministry]]
* [[Second Gillard ministry]]
* [[Second Rudd ministry]]

==References==
{{reflist}}

==External links==
*[http://markbutler.alp.org.au/ Official website]
*[http://www.aph.gov.au/Senators_and_Members/Parliamentarian?MPID=HWK Parliamentary Profile: Australian Parliament website]
*[http://www.alp.org.au/mark_butler Parliamentary Profile: Labor website]{{OpenAustralia}}
*[https://theyvoteforyou.org.au/people/representatives/port_adelaide/mark_butler Summary of parliamentary voting for Mark Butler MP on TheyVoteForYou.org.au]

{{s-start}}
{{s-par|au}}
{{s-bef|before=[[Rod Sawford]]}}
{{s-ttl|title=[[Member of Parliament|Member]] for [[Division of Port Adelaide|Port Adelaide]]|years=2007–present}}
{{s-inc}}
{{s-off}}
{{s-bef|before=[[Justine Elliot]]}}
{{s-ttl|title=[[Minister for Mental Health and Ageing (Australia)|Minister for Mental Health and Ageing]]|years=2010–2013}}
{{s-aft| after=[[Jacinta Collins]]}}
{{s-bef|before=[[Tanya Plibersek]]}}
{{s-ttl|title=[[Minister for Social Inclusion (Australia)|Minister for Social Inclusion]]|years=2011–2013}}
{{s-aft| after=[[Julie Collins]]}}
{{S-bef| before=[[Tony Burke]] }}
{{s-ttl | title=[[Minister for Sustainability, Environment, Water, Population and Communities (Australia)|Minister for Environment and Water]] | years=2013}}
{{s-aft|after=[[Greg Hunt]] <br>{{small|as Minister for Environment}}}}
{{S-bef| before=[[Greg Combet]] }}
{{s-ttl | title=[[Minister for Climate Change (Australia)|Minister for Climate Change]] | years=2013}}
{{s-aft|after=''Office abolished''}}
{{s-end}}

{{Second Rudd Cabinet}}
{{Gillard Ministry}}
{{Current South Australia Representatives}}

{{Authority control}}

{{DEFAULTSORT:Butler, Mark}}
[[Category:Australian people of English descent]]
[[Category:Australian Labor Party members of the Parliament of Australia]]
[[Category:Members of the Australian House of Representatives]]
[[Category:Members of the Australian House of Representatives for Port Adelaide]]
[[Category:People from Adelaide]]
[[Category:1970 births]]
[[Category:Living people]]
[[Category:Government ministers of Australia]]
[[Category:Members of the Cabinet of Australia]]
[[Category:Labor Left politicians]]
[[Category:21st-century Australian politicians]]