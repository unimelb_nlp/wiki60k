{{Infobox musical artist
|name = Sid Griffin
|background = solo_singer
|birth_date = {{birth date and age|1955|09|18}}
|birth_place = [[Louisville, Kentucky|Louisville]], [[Kentucky]], United states
|instrument   = [[Singing|Vocals]], [[guitar]], [[mandolin]]
|genre   = [[Americana (music)|Americana]], [[Bluegrass music|alternative bluegrass]]
|occupation = Singer, songwriter, instrumentalist, bandleader, author
|label    = Prima Records, [[Island Records|Island]]
| associated_acts     = [[The Long Ryders]], [[The Coal Porters]]
|years_active   = 1980s-present
|website   = {{URL|http://www.sidgriffin.com}}
|}}
'''Albert Sidney "Sid" Griffin'''  (born September 18, 1955) is an American singer, songwriter, guitarist-mandolinist, bandleader, and author who lives in [[London, England]]. He led the [[Long Ryders]] band in the 1980s, founded the [[Coal Porters]] group in the 1990s, has recorded several solo albums and is the author of volumes on [[Bob Dylan]], [[Gram Parsons]] and [[bluegrass music]].

==Early life==
Griffin was born in [[Louisville, Kentucky]]. He is an eighth-generation Kentuckian.<ref name="rocksbackpages">{{cite web|url=http://www.rocksbackpages.com/article.html?ArticleID=21166 |title=What a Long, Strange Ryde(r) It's Been: Sid Griffin |author=[[Bill Wasserzieher]] |work=Articles, reviews and interviews from Rock's Backpages |publisher=Rocksbackpages.com |date= |accessdate=2012-12-01}}</ref> After graduating from [[Ballard High School (Louisville, Kentucky)|Ballard High School]] in eastern Louisville and playing in a band called The Frosties,<ref>Doggett, Peter. Record Collector, April 1997.</ref> Griffin attended the [[University of South Carolina]], receiving a bachelor's degree in journalism in 1977. He then moved to [[Los Angeles, California]], to launch a career as a musician.<ref>Brooks, Farleigh. "Beautiful dreamer". Leoweekly.com. Retrieved 16 January 2011.</ref>

==Career==
He briefly played in the [[punk band]] Death Wish before joining Shelley Ganz to form the Unclaimed in 1979.<ref name="allmusic">{{cite web|last=Deming |first=Mark |url=http://www.allmusic.com/artist/sid-griffin-mn0000755037 |title=Sid Griffin - Music Biography, Credits and Discography |publisher=AllMusic |date=2005-05-26 |accessdate=2012-12-01}}</ref> Steeped in the [[garage band]] ethos of the 1960s, the Unclaimed released a self-titled four-track EP on the Moxie label in September 1980, which included the early Griffin compositions "Time to Time" and "Deposition Central."

Griffin left the Unclaimed in November 1981, along with band's bassist Barry Shank, to form the nascent Long Ryders, adding Greg Sowders on drums and, after a period of searching, guitarist Stephen McCarthy. [[Steve Wynn (songwriter)|Steve Wynn]] of [[Dream Syndicate]] was briefly a member also.<ref name="allmusic" /> Shank resigned from the band to pursue a doctoral degree after a year, and Australian Des Brewer took over on bass in time for the Long Ryders' debut EP, ''10-5-60'' (1983). Tom Stevens then replaced Brewer and joined Griffin, Sowders and McCarthy for the Long Ryders' ''Native Sons'' (1984), the band's first full-length album on Frontier Records, and two subsequent major label releases, ''State of Our Union'' (1985) and ''Two Fisted Tales'' (1987), on [[Island Records]]. The Long Ryders broke up in December 1987.<ref>{{cite web|last=Huey |first=Steve |url=http://www.allmusic.com/artist/the-long-ryders-mn0000082091 |title=The Long Ryders - Music Biography, Credits and Discography |publisher=AllMusic |date=1987-12-15 |accessdate=2012-12-01}}</ref> For a lengthy oral history interview with its former members, visit the Perfect Sound Forever website.<ref>{{cite web|url=http://www.furious.com/perfect/longryders.html |title=Perfect Sound Forever: An Oral History of the Long Ryders and Paisley L.A. (Part 1) |publisher=Furious.com |date= |accessdate=2012-12-01}}</ref> In January 2016, [[Cherry Red Records]] released a four-CD box set, ''Final Wild Songs'', comprising the band's three full-length albums, their one EP, various rarities and demos, and a 15-song live Benelux radio performance.

Following the demise of the Long Ryders, Griffin put together the earliest version of the Coal Porters in Los Angeles. Like his previous band, the Coal Porters were initially rooted in the music of the [[Byrds]], Gram Parsons, and the [[Buffalo Springfield]], playing country and [[country rock]] songs written by Griffin with occasional collaborators. Before the band's first solid lineup fell into place, many of Griffin's musician friends made appearances at early gigs and performed on initial recordings, including drummer Greg Sowders from the Long Ryders and Billy Bremner of [[Rockpile]]. After relocating to London along with Coal Porters' bassist Ian Thompson (later a member of Lindisfarne), Griffin recruited new players that have included Kevin Morris ([[Dr. Feelgood (band)|Dr. Feelgood]]), Ian Gibbons [[The Kinks]], and John Bennett (High Llamas), with additional support from such [[Billy Bragg]] associates Dave Woodhead, Wiggy, and Grant Showbiz.<ref name="rocksbackpages" /> The present Coal Porters' lineup consists of Griffin on vocals and mandolin, Neil Robert Herd guitar and vocals, Kerenza Peacock ([[Adele]], [[Seasick Steve]], [[Paul Weller]]) fiddle and vocals, Paul Fitzgerald banjo and vocals, and Andrew Stafford bass.

In addition to the Long Ryders' catalog and eight full-length albums and two EPs credited to the Coal Porters, Griffin has recorded four solo discs: ''Little Victories'' (1997), the performance collection ''Sid Griffin: Worldwide Live, 1997-2002'' (2002), ''As Certain as Sunrise'' (2005) and ''The Trick Is to Breathe'' (2014). ''Little Victories'' includes "The Rate of Exchange," which Griffin wrote with Steve Wynn, an unusual cover of the [[Thelonious Monk]] composition "Monk's Mood," a version of [[Phil Ochs]]' "Sailors and Soldiers," and Griffin's own "Distant Trains" and "Jimmy Reed,"  ''As Certain as Sunrise'' features "The Last Kentucky Waltz," "Where Bluebirds Fly," and the lullaby "Written Upon the Birth of My Daughter." ''The Trick Is to Breathe'', released in September 2014, was recorded in [[Nashville]] with producer [[Thomm Jutz]] and features such supporting musicians as [[Rounder Records]] artist [[Sierra Hull]] on mandolin. [[Justin Moses]] banjo and fiddle, and [[Grammy]]-winner [[Mark Fain]] ([[Ricky Skaggs]]) on bass. The album offers 10 new compositions as well as a fresh version of "Everywhere," a song written by Griffin and [[Greg Trooper]] in the 1990s which appeared on the [[Billy Bragg]] album ''Don't Try This at Home'', and an acoustic version of the [[Dino Valenti]] composition and [[The Youngbloods]] hit "Get Together."   Griffin has also worked as a producer, helming the Lindisfarne album ''Here Comes the Neighbourhood''.<ref name="allmusic" />

Griffin published his first book, ''Gram Parsons – A Musical Biography'' (Sierra Books), in 1985. The volume includes interviews with [[Emmylou Harris]], [[Chris Hillman]], [[Peter Fonda]] and other artists who associated with Parsons during his days with the [[International Submarine Band]], the Byrds, the [[Flying Burrito Brothers]] and as a solo performer.

Griffin's second book, ''Bluegrass Guitar: Know the Players, Play the Music'', appeared in 2005 (Backbeat Books) with Eric Thompson as co-author. This volume includes profiles on Mother [[Maybelle Carter]], [[Doc Watson]], [[Clarence White]], [[Norman Blake (American musician)|Norman Blake]], [[Tony Rice]] and [[Bryan Sutton]], as well as chapters on equipment, techniques and an instructional CD.

Griffin then published two volumes focusing on Bob Dylan. ''Million Dollar Bash: Bob Dylan, [[The Band]], and [[The Basement Tapes]]'' appeared in 2007 (Jawbone Press), followed by ''Shelter from the Storm'' in 2010 (Jawbone Press). The garage rock journal ''[[Ugly Things]]'', in a review of ''Million Dollar Bash'', called Griffin "an Aesop-meets-Rabelais" storyteller.<ref>Wasserzieher, Bill. Ugly Things, Winter-Spring 2008.</ref> A revised and expanded second edition of ''Million Dollar Bash'' appeared in November 2014 to coincide with Sony's release of [[The Basement Tapes]] in an unabridged, six-disc format, for which Griffin provided the introductory essay. In addition, Griffin has written booklet essays for numerous albums and contributed to such publications as ''[[Mojo (magazine)|Mojo]]'', ''[[Q (magazine)|Q]]'', ''[[NME]]'', ''Rock 'n' Reel'', and the ''[[Manchester Guardian]]''. He is also the co-author of the [[BBC]] television documentary ''Gram Parsons: Fallen Angel''.<ref>Benz, Matthew. No Depression, January–February 1997</ref>

The [[Americana Music Association]] and the Museum of Country Music in [[Nashville, Tennessee]], honored Griffin's more than 30 years in music with a career retrospective on Sept. 10, 2010, with [[David Fricke]] of ''[[Rolling Stone]]'' as moderator. Excerpts from program may be viewed on YouTube.<ref>{{cite web|author= |url=https://www.youtube.com/watch?v=vVmigo7OT-E |title=Sid Griffin in conversation with David Fricke - Part 1 |publisher=YouTube |date= |accessdate=2012-12-01}}</ref> The complete program is available also at the Sid Griffin website.<ref>{{cite web|url=http://www.sidgriffin.com/2010/10/22/david-fricke-with-special-guest-sid-griffin |title=David Fricke with special guest |publisher=Sid Griffin |date=2010-10-22 |accessdate=2012-12-01}}</ref> Griffin also served as the keynote speaker at the Americana Music Association UK Conference in London on Feb. 1, 2017. <ref>theamauk.org/conference-awards-2016 sid griffin</ref>

==Discography==

===The Unclaimed===
* ''the Unclaimed'' EP (1980)<ref>[http://www.discogs.com/release/4227301 Discogs - ''the Unclaimed'' - 7"-vinyl-EP 1983, Moxie Records (M 1036) US]</ref><ref>[http://web.tiscalinet.it/wrongway/shelley/discography.htm Shelley Ganz - ''the Unclaimed'' (discography)]</ref>

===Long Ryders===
* ''10-5-60'' (1983)
* ''Native Sons'' (1984)
* ''State of Our Union'' (1985)
* ''Two-Fisted Tales'' (1987)
* ''Metallic B.O.'' (1989)
* ''BBC Radio One Live in Concert'' (1994)
* ''Anthology'' (1998)
* ''Three Minute Warnings: the Long Ryders Live in New York City'' (2003)
* ''The Best of the Long Ryders'' (2004)
* ''State of Our Reunion'' (2004)
* ''Final Wild Songs'' (2016)

===Coal Porters===
* ''Rebels Without Applause'' (1991)
* ''Land of Hope and Crosby'' (1994)
* ''Los London'' (1995)
* ''EP Roulette'' (1998)
* ''Gram Parsons Tribute Concert'' (1999)
* ''Western Electric'' (1999)
* ''Chris Hillman Tribute Concert'' (2001)
* ''How Dark This Earth Will Shine'' (2004)
* ''Turn the Water On, Boy'' (2008)
* ''Durango'' (2010)
* ''Find the One'' (2012)
* ''No. 6'' (2016)

===Solo===
* ''Little Victories'' (1997)
* ''Sid Griffin: Worldwide Live, 1997-2002'' (2002)
* ''As Certain as Sunrise'' (2005)
* ''The Trick Is to Breathe'' (2014)

== Videos ==
*[https://www.youtube.com/watch?v=N6_LVM1KhTc Ode To Bobbie Gentry] - Sid Griffin  (2014)
*[https://www.youtube.com/watch?v=RntR3jEpIDw Looking For Lewis and Clark] - The Long Ryders
*[https://www.youtube.com/watch?v=Mc2pNhAT4AE The Day The Last Ramone Died] - The Coal Porters (Official Video) (2016)

==References==
{{Reflist}}

==Bibliography==
* ''Gram Parsons: A Music Biography''. Sierra Books, 1985. ISBN 0-916003-01-9.
* ''Bluegrass Guitar: Know the Players, Play the Music''. Backbeat Books, 2005. ISBN 0-87930-870-2.
* ''Million Dollar Bash: Bob Dylan, The Band, and the Basement Tapes''. Jawbone Books, 2007. ISBN 978-1-906002-05-3.
* ''Shelter from the Storm: Bob Dylan's Rolling Thunder Years''. Jawbone Books, 2010. ISBN 978-1-906002-27-5.

==External links==
*{{Official website|http://www.sidgriffin.com/}}

{{Authority control}}

{{DEFAULTSORT:Griffin, Sid}}
[[Category:1955 births]]
[[Category:Living people]]
[[Category:Musicians from Louisville, Kentucky]]
[[Category:Ballard High School (Louisville, Kentucky) alumni]]
[[Category:University of South Carolina alumni]]
[[Category:Songwriters from Kentucky]]
[[Category:American alternative country musicians]]
[[Category:Bluegrass musicians from Kentucky]]
[[Category:American biographers]]
[[Category:Male biographers]]
[[Category:American expatriates in the United Kingdom]]
[[Category:Singers from Kentucky]]