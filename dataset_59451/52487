{{Infobox journal
| title = Oxford German Studies
| cover = Oxford German Studies.jpg
| editor = [[Terence James Reed]], [[Nigel F Palmer|Nigel F. Palmer]]
| discipline = [[German studies]]
| language = English, German
| former_names =
| abbreviation = Oxf. Ger. Stud.
| publisher = [[Routledge]]
| country =
| frequency = Quarterly
| history = 1965-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=yogs20
| link1 = http://www.tandfonline.com/toc/yogs20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/yogs20
| link2-name = Online archive
| JSTOR =
| OCLC = 1640444
| LCCN = 82641755
| CODEN =
| ISSN = 0078-7191
| eISSN = 1745-9214
}}
'''''Oxford German Studies''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering [[German studies]]. It was established in 1965 was published by [[Maney Publishing]] (formerly the [[Modern Humanities Research Association]]), until its takeover by [[Taylor & Francis]], where it is now published under their [[Imprint (publishing)|imprint]] [[Routledge]]. It was established, among others, by [[Peter Ganz]] ([[St Edmund Hall, Oxford]]).<ref>{{cite news |last1=Palmer |first1=Nigel |url=http://www.independent.co.uk/news/obituaries/professor-peter-ganz-416037.html |title=Professor Peter Ganz |date=2006-09-15 |periodical=[[The Independent]] |accessdate=2016-11-14}}</ref> The [[editors-in-chief]] are [[Terence James Reed]] and [[Nigel F Palmer|Nigel F. Palmer]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Arts and Humanities Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-11-14}}</ref>
*[[British Humanities Index]]
*[[Current Contents]]/Arts & Humanities<ref name=ISI/>
*[[International Bibliography of Periodical Literature]]
*[[Linguistics and Language Behaviour Abstracts]]
*[[MLA Bibliography]]
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-11-14}}</ref>
}}

==References==
{{Reflist}}

==External links==
*{{Official website|1=http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=yogs20}}

[[Category:Publications associated with the University of Oxford]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1965]]
[[Category:Multilingual journals]]
[[Category:Area studies journals]]
[[Category:Routledge academic journals]]