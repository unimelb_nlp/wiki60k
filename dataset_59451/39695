{{EngvarB|date=October 2013}}
{{Use dmy dates|date=October 2013}}
{{Infobox Christian leader
| type = bishop
| honorific-prefix = [[The Right Reverend]]
| name = Laurence Bonaventure Sheil, OFM
| honorific-suffix =
| title = Bishop of Adelaide
| image = Lawrence Sheil c1872.jpg
| imagesize =
| alt =
| caption =
| church =
| archdiocese =
| province =
| metropolis =
| diocese = [[Roman Catholic Archdiocese of Adelaide|Adelaide]]
| see =
| enthroned = 16 September 1866
| ended = 1 March 1872
| predecessor = Bishop [[Patrick Geoghegan (bishop)|Patrick Geoghegan]] OFM
| opposed =
| successor = Archbishop [[Christopher Reynolds (bishop)|Christopher Reynolds]]
| ordination = 1839
| consecration = 15 August 1866
| other_post =

<!---------- Personal details ---------->
| birth_name =
| birth_date = {{Birth date|1815|12|24|df=yes}}
| birth_place = [[Wexford]], Ireland, [[United Kingdom of Great Britain and Ireland]]
| death_date = {{Death date and age|1872|03|01|1815|12|24|df=yes}}
| death_place = [[Willunga, South Australia]], [[British Empire]]
| nationality =
| signature =
}}

'''Laurence Bonaventure Sheil [[Franciscans|OFM]]''' (24 December 1815 – 1 March 1872) was an Irish [[Franciscans|Franciscan friar]], who served as the third [[Roman Catholic Church|Roman Catholic]] [[Roman Catholic Archdiocese of Adelaide|Bishop of Adelaide]].  Born in Ireland, he was educated at [[St Peter's College, Wexford]], and at the Franciscan College of St Isidore, Rome, Sheil was sent to the British Colony of [[New South Wales]] in Australia after being [[Holy Orders|ordained]] a priest.  There, he served as an educator and administrator, before poor health saw him move to [[Ballarat]] as [[archdeacon]].

In 1866, Sheil became the third Bishop of Adelaide.  His reign was characterised by poor administration, with his extensive absence from the diocese contributing to severe factionalism within the clergy.  Sheil's mismanagement culminated in his [[excommunication]] of [[Mary MacKillop]],<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" /> who later became Australia's first saint.<ref name="Nun becomes first Australian saint">{{cite news|title=Nun becomes first Australian saint|url=http://english.aljazeera.net/news/asia-pacific/2010/10/20101017144255773848.html|accessdate=1 February 2011|newspaper=Al Jazeera|date=17 October 2010}}</ref> He died in March 1872, rescinding his excommunication of MacKillop on his deathbed.

== Early life ==
Sheil was born on 24 December 1815 in [[Wexford]], Ireland.  From 1832, he attended the [[Franciscan]] College of St Isidore in Rome, where he taught after he was ordained in 1839.  After serving as guardian of the convents of St Francis at [[Cork (city)|Cork]] and [[Carrickbeg]], Sheil travelled to [[Melbourne, Australia]], arriving on 12 February 1853.  There, he served as the secretary and manager of the Victorian Catholic education board, and taught at a Melbourne seminary.  Sheil's failing health saw him moved to [[Ballarat]] in 1859, where he became [[archdeacon]].  In 1866 he was chosen to replace [[Patrick Geoghegan]] as Bishop of [[Roman Catholic Archdiocese of Adelaide|Adelaide]].<ref name="Sheil, Laurence Bonaventure (1815 - 1872)">{{cite web|last=Bickerton|first=Ian J|title=Sheil, Laurence Bonaventure|url=http://adbonline.anu.edu.au/biogs/A060129b.htm|work=Australian Dictionary of Biography|accessdate=31 January 2011}}</ref>

== Episcopacy ==
After being [[consecration (Catholic Church)|consecrated]] on 15 August 1866 by Bishop of [[Roman Catholic Archdiocese of Melbourne|Melbourne]], [[James Alipius Goold|James Goold]], Sheil was installed as Bishop of Adelaide on 16 September, that year.<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" />

The diocese of Australia expanded significantly during Sheil's term as bishop.  The number of priests rose from 17 to 30, and a number of new parishes were founded.  The founding of the [[Sisters of St Joseph of the Sacred Heart|Sisters of St. Joseph]] in 1866 by [[Mary Mackillop]] and [[Julian Tenison Woods]] contributed to a large improvement in Catholic education within the diocese.  Sheil appointed Woods as the director general of Catholic education in the diocese, and by 1871, there were 71 Catholic schools in the diocese, more than half run by the Josephites.<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" />

Although Sheil had been an effective educator in Victoria, his episcopacy was characterised by weak leadership and poor administration.<ref name="The Catholic Priesthood of South Australia, 1844-1915">{{cite journal|last=Schumann|first=Ruth|title=The Catholic Priesthood of South Australia, 1844–1915|journal=Journal of Religious History|year=1990|volume=16|issue=1|pages=51–73|doi=10.1111/j.1467-9809.1990.tb00649.x}}</ref> His travels to Europe to recruit priests and attend the [[First Vatican Council]] meant that he spent less than two years of his six-year reign in the diocese.<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" /> In the absence of effective leadership, clergy disunity became rife,<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" /> with a factional grouping forming around the Franciscan priest Charles Horan.<ref name="From Our Broken Toil">{{cite book|last=Press|first=Margaret M.|title=From Our Broken Toil – South Australian Catholics 1836 – 1906|year=1986|publisher=The Catholic Archdiocese of Adelaide|isbn=0-949807-35-4|pages= 181–193}}</ref> In 1871, when Sheil returned from a visit to Europe, Horan's faction alleged that the Josephites were incompetent and ignorant, petitioning him to take direct control of the order.<ref name="From Our Broken Toil" /> Sheil, who was increasingly acting under Horan's influence, demoted Tenison Woods from his administrative position within the diocese, disbanded the Josephite [[novitiate]] and brought the Sisters of St. Joseph directly under his control. In doing so, he removed Tenison Woods and MacKillop from their positions of influence within the order. MacKillop strongly objected, and in response, Sheil [[excommunicated]] her for disobedience.<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" /><ref name="From Our Broken Toil" /> The excommunication prompted further disunity amongst South Australian Catholics, with letters criticising the Bishop in local papers.  Prompted by these events, a group of Catholic [[laity|laymen]] wrote to Cardinal [[Alessandro Barnabò]], Prefect of the [[Congregation Propaganda Fide|Congregation of the Propagation of the Faith]] in Rome.  The letter was strongly critical of Sheil's excommunication of MacKillop, the management of diocesan finances and impropriety within the clergy.<ref name="From Our Broken Toil" />

Throughout early 1872, Sheil's became steadily more ill.  On his deathbead in [[Willunga]], he rescinded his excommunication of MacKillop, claiming he had been betrayed by his advisors.<ref name="From Our Broken Toil" /> He died of a [[carbuncle]] on 1 March 1872.<ref name="Sheil, Laurence Bonaventure (1815 - 1872)" />

== Legacy ==

After Sheil's death, two reports into his episcopacy and the state of the Diocese of Adelaide were conducted.  The first, written by a Jesuit priest in the diocese to his [[Father-General of the Jesuits|Father-General]] in Rome, was fiercely critical of Sheil's recruitment of priests from Ireland.  Of the 21 priests he had brought to the diocese, the report stated that one had died, one had such poor health as to be useless, five had been dismissed for impropriety, five had insufficient knowledge to be effective as priests and six had become involved in diocesan factionalism.  Not only, the report alleged, had only two of the twenty-one recruited priests been useful, but many of them had known deficiencies before Sheil recruited them.<ref name="From Our Broken Toil" />

The second report, commissioned by the Congregation of the Propagation of the Faith, was conducted by Bishop of [[Roman Catholic Archdiocese of Hobart|Hobart]] Daniel Murphy and Bishop of [[Roman Catholic Diocese of Bathurst in Australia|Bathurst]] Matthew Quinn.  Murphy and Quinn travelled around the diocese, inspecting parishes and collecting evidence.  Having concluded their investigations, they expelled Charles Horan and other factional leaders from the diocese, and recommended [[Christopher Reynolds (bishop)|Christopher Reynolds]] (who had been acting as administrator of the Diocese since Sheil's death) be appointed as the next Bishop of Adelaide.<ref name="From Our Broken Toil" />

== References ==
{{reflist}}

== External links ==
{{commons category|Laurence Bonaventure Sheil (bishop)}}
*[http://adbonline.anu.edu.au/biogs/A060129b.htm ''Sheil, Laurence Bonaventure''] at the Australian Dictionary of Biography, Online Edition
*[http://www.catholic-hierarchy.org/bishop/bshiel.html ''Bishop Laurence Bonaventure Sheil, O.F.M.''] at catholic-hierarchy.org

{{Bishops and Archbishops of the Roman Catholic Archdiocese of Adelaide}}

{{DEFAULTSORT:Sheil, Laurence Bonaventure}}
[[Category:1814 births]]
[[Category:1872 deaths]]
[[Category:People from County Wexford]]
[[Category:Irish Friars Minor]]
[[Category:Franciscan missionaries]]
[[Category:Irish Roman Catholic missionaries]]
[[Category:Franciscan bishops]]
[[Category:Irish Roman Catholic bishops]]
[[Category:19th-century Roman Catholic bishops]]
[[Category:Roman Catholic Bishops of Adelaide]]
[[Category:Infectious disease deaths in South Australia]]
[[Category:Burials in South Australia]]
[[Category:Roman Catholic missionaries in Australia]]