{{lead missing|date=January 2015}}
{{overly detailed|date=January 2015}}
{{Orphan|date=January 2015}}

== History ==
[[File:Żarki - kościół p.w. śś Szymona i judy Tadeusza.jpg|thumbnail|right|Żarki Catholic church]]

At the time of [[World War I]], [[Żarki]] belonged to the district of [[Dąbrowa Górnicza|Dąbrowa]].
On August 27, 1918, the Żarki city council requested permission from the officer in charge of Civil Affairs<ref name=barefoot>{{cite book|title=Poland 1918 Locals|date=1999|publisher=J. Barefoot Ltd|location=England|isbn=0-906845-55-6|page=42|url=http://www.jbarefoot.co.uk}}</ref> to create a local post.<ref name=utrechts>{{cite news|title=Utrechts Nieuwsblad|accessdate=22 December 2014|work=Over Postzegels|date=13 March 1959|url=http://www.hetutrechtsarchief.nl/collectie/kranten/un/1959/0313|location=Utrecht|page=17|language=Dutch|format=Newspaper}}</ref>
On September 18, 1918, the city council requested permission to collect delivery payments and issue stamps.<ref name=kronenberg>{{cite book|last1=Kronenberg|first1=Dr. Stanley|title=Kronenberg Collection: 1918-1919 Local Issues|publisher=Polonus Philatelic Society|location=New Jersey|url=http://www.polonus.org/home.php?author=1&topic=43&page=exhibits_det|accessdate=22 December 2014}}</ref>
The district military command issued permit number ''13. 138. V. A.'' on September 30, 1918 and requested 5 stamps of each denomination as samples.<ref name=kronenberg /> 
The post began and ended operation shortly after in October, 1918, only having issued one stamp design featuring the Żarki Catholic church.<ref name=barefoot />

[[Philately|Philatelic]] historians disagree over who is responsible for organizing the effort to start the Żarki local post. Some, including noted Polish stamp expert Stefan Petriuk,<ref name=petriuk>{{cite web|title=Biografia: PETRIUK Stefan|url=http://www.zgpzf.pl/biography.php?id=424|website=http://www.zgpzf.pl|publisher=Polski Związek Filatelistów Zarząd Główny|accessdate=11 December 2014}}</ref> believe that the effort was started by the local postmaster Peter Franczak after seeing the efforts in neighboring [[Sosnowiec]] and [[Zawiercie]].<ref>{{cite web|last1=Ginsburg|first1=Sam|title=Introduction to Przedbórz Philatelic History|url=http://www.prz.ginsburgs.org/histgeo/phist/mainframe.html|accessdate=13 December 2014}}</ref><ref name=petriuk_book>{{cite book|last1=Petriuk|first1=Stefan|title=Stadtpostämter im besetzten Polen 1915 – 1918|date=1985|location=Germany}}</ref> Others believe that well-known stamp dealer Szlojme Abramsohn (Abramson) persuaded Franczak and authorities to begin the post. Subsequent events (see [[{{Talk other|Draft:Postal_history_of_%C5%BBarki}}#Forgeries|Forgeries]]) have shown Abramsohn might have been motivated to do this for his own financial gain.<ref name=barefoot /><ref name=petriuk_book /><ref name=psb>{{cite web|title=ABRAMSOHN (Abramson) Szlojme|url=http://psbprzedborz.pl/wordpress3/2009/09/abramsohn-abramson-szlojme/|website=http://psbprzedborz.pl|publisher=Przedborski Słownik Biograficzny|accessdate=11 December 2014}}</ref>

Delivery service was established with rates denominated in [[Heller (money)|heller]] (abbreviated as ''h''), the currency of the [[Austro-Hungarian Empire]] that occupied the region at the time. Delivery rates were 3h for newspapers and postcards, 5h for letters, and 12h for registered and express mail. Service was also extended to neighboring Cholon, [[Niegowa]], and [[Włodowice, Silesian Voivodeship|Włodowice]] where rates were 5h for newspapers and postcards and 12h for all other mail.<ref name=barefoot /><ref name=kronenberg />

== Translation ==
[[File:Żarki Stamp Inscription.jpg|thumb|right|Detail of the text inscription on Żarki local stamp issues.]]

The name of the local post as printed on stamps was ''Poczta miejska w Żarkach'', which translates to English as ''City post in Żarki''.<ref name=stamp_encyclopaedia>{{cite web|title=Stamp Encyclopaedia Poland|url=http://www.stampspoland.nl/series/town/zarki.html|website=http://www.stampspoland.nl/|publisher=Ben Nieborg|accessdate=11 December 2014}}</ref> In the [[Polish language]], ''Żarkach'' is the locative [[declension]] of the town name ''Żarki''.

== Printing Process ==
All issued stamps were printed as sheets of fifty stamps organized in five rows of ten stamps<ref name=fischer /> using a [[Lithography|lithographic]] process and feature 11½ [[Perforation gauge|perforation]].<ref name=barefoot /><ref name=sw>{{cite book|last1=Bungerz|first1=Alexander|title="SW" welt-katalog|date=1921|publisher=Sammler-Woche|location=München|page=810}}</ref><ref name=linn>{{cite book|last1=Tyler|first1=Varro E.|title=Focus on Forgeries: A Guide to Forgeries of Common Stamps|date=2000|publisher=Linn's Stamp News|location=Sidney, Ohio|isbn=0-940403-87-0|page=217|edition=2nd}}</ref>

=== Plate Differences ===
In the original stamp issue, all printings of the 3h denomination are identical; odd and even plate positions only differ by slightly different vertical centering.
The higher 5h and 12h denominations of the first issue and all denominations of the third issue have slight differences around the numeral areas depending on odd or even plate position.
Based upon this, it is concluded that the printing process for the 3h issue involved transferring the stamp 50 times to form an entire plate and that the numeral areas in the higher denominations were redesigned 4 times for each future denomination to produce pairs that were then transferred 25 times each to form entire plates.<ref name=kronenberg />

== Issues ==
Three issues of stamps, all within the month of October, 1918, are the only stamps to ever be issued by the Żarki local post.  All stamps were denominated in [[Heller (money)|''heller'']] (abbreviated as ''h''), the currency of the [[Austro-Hungarian Empire]] that occupied the region at the time.

=== October 10, 1918 ===
10,000 each of denominations 3h (blue), 5h (red), and 12h (yellow-olive) were printed, but only 5,000 of each were issued. The remainder were held and used for surcharging.<ref name=barefoot /><ref name=fischer /><ref name=sw />

Two subtle variations exist of only the 5h and 12h values of this issue depending on odd or even plate position. All 3h values are identical.<ref name=kronenberg />

==== Type Ⅰ ====
Stamps of this type are odd numbered positions in the plate of 50 stamps, counting from left to right, top to bottom.<ref name=kronenberg />

Identifying features visible on the 5h stamp:<ref name=kronenberg />
* Left-hand numeral ''5'' has a thorn at the 1 o'clock position of its curved part
* Left-hand numeral shield inner circle has a spot at the 11 o'clock position
* Right-hand numeral ''5'' has an interruption at its 1 o'clock position of its curved part
* Right-hand numeral ''5'' is positioned higher within its numeral shield

Identifying features visible on the 12h stamp:<ref name=kronenberg />
* Left-hand and right-hand numeral ''2'' are identical

==== Type Ⅱ ====
Stamps of this type are even numbered positions in the plate of 50 stamps, counting from left-to-right, top-to-bottom.<ref name=kronenberg />

Identifying features visible on the 5h stamp:<ref name=kronenberg />
* Right-hand numeral ''5'' is positioned lower within its numeral shield

Identifying features visible on the 12h stamp:<ref name=kronenberg />
* Right-hand numeral ''2'' is flattened on the left-hand side

=== October 18, 1918 ===
Only a week later, rates were doubled and 5,000 each of newly issued stamps were [[Handstamp|hand-stamped]] in pairs with the new values of 6h (blue), 10h (red), and 24h (green).<ref name=fischer /><ref name=sw /> The 6h and 24h values used a red hand-stamp, while the 10h value used a violet hand-stamp. Hand-stamp values using incorrect colors have been identified as [[Forgery|forgeries]].<ref name=barefoot /> The hand-stamps read 6, 10, or 24 ''halerzy'', which is the genitive declension of the Polish word ''halerz''<ref>{{cite web|author1=Wiktionary contributors|title=halerz|url=http://en.wiktionary.org/w/index.php?title=halerz&oldid=21593034|publisher=Wiktionary, The Free Dictionary|accessdate=12 December 2014}}</ref> that translates to [[Heller (money)|''heller'']] in English.

Two subtle variations exist of this issue depending on odd or even plate position.<ref name=kronenberg />

==== Type Ⅰ ====
Stamps of this type are odd numbered positions in the plate of 50 stamps, counting from left to right, top to bottom.<ref name=kronenberg />

Identifying features:<ref name=kronenberg />
* Crescent formed by the shadow of the circular church window is smooth and circular
* Rectangular window to the right of the circular window has an interrupted white vertical line on the left-hand side

==== Type Ⅱ ====
Stamps of this type are even numbered positions in the plate of 50 stamps, counting from left to right, top to bottom.<ref name=kronenberg />

Identifying features:<ref name=kronenberg />
* Crescent formed by the shadow of the circular church window is not smooth and has a visible corner
* Rectangular window to the right of the circular window has an uninterrupted white vertical line on the left-hand side

{{multiple image
| align = center
| direction = horizontal
| width = 200
<!-- 6h/3h -->
| image1 = Poczta Miejska w Żarkach 6h 3h.jpg
| caption1 = 6h red hand-stamp on 3h stamp
<!-- 10h/5h -->
| image2 = Poczta Miejska w Żarkach 10h 5h.jpg
| caption2 = 10h violet hand-stamp on 5h stamp
<!-- 24h/12h -->
| image3 = Poczta Miejska w Żarkach 24h 12h.jpg
| caption3 = 24h red hand-stamp on 12h stamp
<!-- Extra -->
| header = Genuine Cancelled Stamps
| header_align = center
}}

=== October 24, 1918 ===
Yet another week later, stamps were issued using the new rates of 6h (violet), 10h (green), and 24h (orange) without hand-stamps.<ref name=barefoot /><ref name=fischer /><ref name=sw />

Two subtle variations exist of this issue depending on odd or even plate position.<ref name=kronenberg />

==== Type Ⅰ ====
Stamps of this type are odd numbered positions in the plate of 50 stamps, counting from left to right, top to bottom.<ref name=kronenberg />

Identifying features visible on the 6h stamp:<ref name=kronenberg />
* Three shading lines are visible between the top of the numeral shield and the inside frame line on both sides.

Identifying features visible on the 10h stamp:<ref name=kronenberg />
* Right-hand numeral ''1'' has a sharp top
* Three shading lines are visible between the top of the right-hand numeral shield and the inner frame line

Identifying features visible on the 24h stamp:<ref name=kronenberg />
* Right-hand numeral ''2'' has an indentation in the center of the inside of its bow
* First and second shading lines above the right-hand numeral shield converge above the center of the shield so that two lines are visible over its left part and three over its right part

==== Type Ⅱ ====
Stamps of this type are even numbered positions in the plate of 50 stamps, counting from left-to-right, top-to-bottom.<ref name=kronenberg />

Identifying features visible on the 6h stamp:<ref name=kronenberg />
* Right-hand numeral ''6'' has an indentation on the bottom
* Two shading lines are visible above each numeral shield
* Top frame line of the left-hand numeral shield is thick

Identifying features visible on the 10h stamp:<ref name=kronenberg />
* Left-hand and right-hand numerals ''1'' have flat tops
* Two shading lines are visible above the left part of the right-hand numeral
* Three shading lines are visible above the right part of the right-hand numeral

Identifying features visible on the 24h stamp:<ref name=kronenberg />
* Two shading lines are visible above both the left-hand and right-hand numeral shields

== Errors ==

The Fischer catalog<ref name=fischer /> states that genuine issues exist of the 6h and 24h values with violet hand-stamps.  Barefoot contradicts this, stating that while older literature refers to hand-prints in other colors, all known instances are forgeries.<ref name=barefoot />

Barefoot states that a variety of the 6h stamp exists where part of the right-hand roof of the church is omitted, leaving a white void. This is referred to as ''snow on the roof''.<ref name=barefoot />

Polish philatelic expert Dr. Stanley Kronenberg identified at least one genuine 3h stamp that contained two hand-stamps due to human error.  The town clerk used the correct color stamp pad, but mistakenly used the 10h hand-stamp instead of the intended 6h hand-stamp.  The clerk decided to correct the error by over-stamping the correct 6h value in black ink to make it more visible.<ref name=kronenberg />

== Forgeries==
After the closure of the local post on October 28, 1918 and all genuine stock was exhausted, Abramsohn sought to fulfill philatelic demand for the original stamps by printing forgeries at the Adolph Panski lithography printing works in Piotrków (now [[Piotrków Trybunalski]]) in 1928.<ref name=barefoot /> The forgeries spread so much through the Polish and German<ref>{{cite book|last1=Roberts|first1=Kenneth Lewis|title=Europe's Morning After|date=1921|publisher=Harper & Brothers|location=New York|pages=388–389}}</ref> stamp trade that it is estimated now that most stamps in general collections will be found to be forgeries.<ref name=barefoot />

{{multiple image
| align = center
| direction = horizontal
| width = 400
<!-- 6h/3h -->
| image1 = Poczta Miejska w Żarkach 6h 3h Pair Perf Cancel Forgery.jpg
| caption1 = Perforated, cancelled 6h hand-stamp on pair of 3h stamps.
<!-- 10h/5h -->
| image2 = Poczta Miejska w Żarkach 6h 3h Pair Imperf Forgery.jpg
| caption2 = Imperforated 6h hand-stamp on pair of 3h stamps.
<!-- Extra -->
| header = Forged October 18, 1918 Issues
| header_align = center
}}

=== Identification ===

There exists a number of known ways to identify forgeries.  All forgeries that Abramsohn printed used the same design, so forgeries of all nine original issued values share the following properties:<ref name=barefoot />

==== Perforation ====
Genuine stamps were only issued with 11½ perforation.<ref name=barefoot /><ref name=fischer /><ref name=linn />  Forged stamps may have this or other perforation and also appear imperforate.<ref name=barefoot /><ref name=linn />

==== Paper ====
Genuine stamps were printed on paper that [[Fluorescence|fluoresces]] under a [[Blacklight lamp|blacklight]]. Generally, forgeries do not fluoresce.<ref name=fischer>{{cite book|title=Katalog Polskich Znakow Pocztowych Tom II|date=2010|publisher=Fischer|location=Poland|isbn=978-83-62457-08-3|page=52|edition=2010|url=http://www.fischer.pl}}</ref>

==== Clouds ====
Genuine stamps have a very light or no border around the top-center clouds.  Forged stamps have a hard dark edge visible around the clouds.<ref name=barefoot />
{{multiple image
| align = none | direction = horizontal | width = 100
| image1 = Żarki Genuine Clouds.jpg | caption1 = Genuine
| image2 = Żarki Forgery Clouds.jpg | caption2 = Forgery
| header = Cloud Detail | header_align = center
}}

==== Group of Three ====
On genuine stamps, the group of three people standing near the center of the stamp are all equal height and clearly visible.  On forged stamps, the left-most person is shorter than the other two and is possibly touching the person to the right.<ref name=barefoot /><ref name=stamp_encyclopaedia /><ref name=fischer /><ref name=linn />
{{multiple image
| align = none | direction = horizontal | width = 100
| image1 = Żarki Genuine Group of Three.jpg | caption1 = Genuine
| image2 = Żarki Forged Group of Three.jpg | caption2 = Forgery
| header = Group of Three Detail | header_align = center
}}

==== Rows of White Bricks ====
On genuine stamps, the church has three rows of white bricks clearly visible in the center of the stamp.  On forged stamps, only two white rows are visible.<ref name=barefoot /><ref name=stamp_encyclopaedia /><ref name=linn />
{{multiple image
| align = none | direction = horizontal | width = 100
| image1 = Żarki Genuine Bricks.jpg | caption1 = Genuine
| image2 = Żarki Forged Bricks.jpg | caption2 = Forgery
| header = Rows of White Bricks Detail | header_align = center
}}

==== Crescent ====
On genuine stamps, the crescent formed by the shadow in the circle on the center of the church is aligned to about 8 o'clock.  On forged stamps, the crescent is found directly left closer to 9 o'clock.<ref name=barefoot /><ref name=linn />
{{multiple image
| align = none | direction = horizontal | width = 100
| image1 = Żarki Genuine Crescent.jpg | caption1 = Genuine
| image2 = Żarki Forged Crescent.jpg | caption2 = Forgery
| header = Crescent Detail | header_align = center
}}

==== Trees ====
On genuine stamps, the tree tops fade into the sky.  On forged stamps, the trees have a bold outline with solid leaves.<ref name=barefoot />
{{multiple image
| align = none | direction = horizontal | width = 100
| image1 = Żarki Genuine Trees.jpg | caption1 = Genuine
| image2 = Żarki Forged Trees.jpg | caption2 = Forgery
| header = Trees Detail | header_align = center
}}

==== Wagon ====
On genuine stamps, the horse-drawn wagon on the left of the stamp has a white roof that is shaded on the bottom by 10 short fine lines.  On forgeries, the wagon is shaded by only 8 heavy lines that extend nearly to the top.<ref name=barefoot />
{{multiple image
| align = none | direction = horizontal | width = 100
| image1 = Żarki Genuine Wagon.jpg | caption1 = Genuine
| image2 = Żarki Forged Wagon.jpg | caption2 = Forgery
| header = Wagon Detail | header_align = center
}}

==== Cancellation ====
Genuine Żarki cancellations are 36mm in diameter, while forged cancellations are 34mm in diameter.<ref name=kronenberg />
Additionally, the spacing between the inner and outer circle is 5.2mm in diameter in the genuine cancellation and only 4.5mm in forged cancellations.<ref name=kronenberg />

==== Alignment Marks ====
Forged sheets contain alignment marks that are not present on genuine sheets.
The alignment marks are visible before the 21st position and after the 30th position as well as above the 6th position and under the 46th position.<ref name=kronenberg />

==== 3h Variations ====
Forged 3h (blue) values are known two contain two variations depending on plate position.
Odd numbered positions in the forged sheets contain a small blot northwest of the letter ''P'' and a dot below ''rk''.<ref name=kronenberg />
Even numbered positions contain a dot on the left side of the center of the right numeral ''3'', dots below ''e'', and often above ''s''.<ref name=kronenberg />

== Closure ==
The local delivery service ceased operation on October 28, 1918.<ref name=barefoot /> It is estimated by noted Polish philatelic expert Stefan Petriuk<ref name=petriuk /> that only about 300 genuine stamps survive, with only a handful appearing on postal matter.<ref name=barefoot />

== See also ==
* [[Postage stamps and postal history of Poland]]
* [[Żarki]]

== References ==
{{Reflist}}

== External links ==
* [http://www.stampspoland.nl/series/town/zarki.html Stamp Encyclopaedia Poland]

{{DEFAULTSORT:Postal history of Zarki}}
[[Category:Forgeries]]
[[Category:Philately of Poland]]
[[Category:1918 in Poland]]