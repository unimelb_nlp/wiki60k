{{good article}}

{{Use dmy dates|date=December 2013}}
{|{{Infobox ship begin |display title=STV ''Astrid'' |infobox caption=STV ''Astrid''}} <!-- age of sail -->
{{Infobox ship image
|Ship image=[[File:Brigg Astrid 2008.JPG|300px]]
|Ship caption=''Astrid'' in 2008
}}
{{Infobox ship career
|Hide header=
|Ship country=
|Ship flag=
|Ship name=*''W.U.T.A.'' (1924–37)
*''Astrid'' (since 1937)
|Ship owner=*N. Müller (1930–37)
*J. Jeppson (1937– )
*Astrid Trust (1989–96)
*Ineke and Pieter de Kam (2006–13)
|Ship ordered=
|Ship builder=G van Leeuwen, Scheveningen
|Ship original cost=
|Ship laid down=
|Ship launched=1918<ref name="AstridHistory" />
|Ship acquired=
|Ship commissioned=
|Ship decommissioned=
|Ship in service=1924<ref name="MarineTraffic" />
|Ship out of service=2013
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship honors=
|Ship captured=
|Ship fate=
|Ship status=Scrapped<ref name="WestCorkTimes18Sep" /><ref name="scrap" />
|Ship notes=
|Ship registry=*{{flagicon|Netherlands}} Dordrecht, Netherlands (1924–37)
*{{flagicon|Sweden}} Skillinge, Sweden (1937–75)
*{{flagicon|Lebanon}} Lebanon (1975–84)
*{{flagicon|United Kingdom|civil}} Weymouth, United Kingdom (1984–97)
*{{flagicon|Netherlands}} [[Vlissingen]], Netherlands (1997–2013)
|Ship identification =*[[Code Letters]] QOSF (1924–34)
*{{ICS|Quebec}}{{ICS|Oscar}}{{ICS|Sierra}}{{ICS|Foxtrot}}
*Code Letters PIRV (1934–37)
*{{ICS|Papa}}{{ICS|India}}{{ICS|Romeo}}{{ICS|Victor}}
*Code Letters SLEK (1937–75)
*{{ICS|Sierra}}{{ICS|Lima}}{{ICS|Echo}}{{ICS|Kilo}}
*Code Letters PCDS (? - 2013)*{{ICS|Papa}}{{ICS|Charlie}}{{ICS|Delta}}{{ICS|Sierra}}
*{{IMO Number|5027792}}<ref name="MarineTraffic" />
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=
|Ship tonnage=*{{GRT|182}}, {{NRT|143}} (as built)
*{{GT|140}};<ref name="Int" /> {{DWT|215}}<ref name="MarineTraffic" />
|Ship length={{convert|41.90|m|ftin|abbr=on}}<ref name="Int" />
|Ship beam={{convert|6.48|m|ftin}}<ref name="Int" />
|Ship draught={{convert|2.65|m|ftin}}<ref name="Int" />
|Ship depth={{convert|2.87|m|ftin}}<ref name="Int" />
|Ship hold depth=
|Ship propulsion=Sail and motor
|Ship sail plan=*[[Lugger]] (as built)
*[[Schooner]] (c.1930–37)
*[[Brig]] (1989–2013)
|Ship complement=
|Ship armament=
|Ship notes=
}}
|}'''STV ''Astrid''''' was a  {{convert|41.90|m|ftin|adj=on}} long [[tall ship]] that was built in 1918 in the [[Netherlands]] as a [[lugger]] and originally named ''W.U.T.A.'', short for ''Wacht Uw Tijd Af'' meaning "Bide Your Time". She was later transferred to Swedish ownership, renamed ''Astrid'' and sailed on the [[Baltic Sea]] until 1975. She then sailed under a Lebanese flag and was allegedly used for [[drug smuggling]]. After being found burnt out on the coast of England in the early 1980s, she was overhauled and used as a sailing training vessel. She was based in [[Weymouth, Dorset]], United Kingdom and was informally known as "Weymouth's vessel".

''Astrid'' ran aground off the coast of Ireland on 24 July 2013, and subsequently sank, with all on board rescued. She was salvaged on 9 September 2013, but as the cost of restoring her was too high she was scrapped and broken up by April 2014.

== Structure ==
''Astrid'' was a dual-masted, square-rigged, iron/steel-hulled [[tall ship]].<ref name="IrishTimes26Jul13" /><ref name="Int" /> She was {{convert|42|m|ft|adj=on}} in length, with a {{convert|7|m|ft|adj=on}} [[Beam (nautical)|beam]], a {{convert|2.7|m|ft|adj=on}} [[Draft (hull)|draught]] and a mast height of {{convert|25|m|ft}}.<ref name="MarineTraffic" /><ref name="AtSail" /> In addition to her sails, she also had a small {{convert|253|kW|hp|adj=on}} [[Scania AB|Scania]] Ds 1402 four-stroke diesel motor for propulsion.<ref name="SkyNews25Jul13" /><ref name="AtSail" /><ref name="Int" /> ''Astrid'' was the smallest tall ship in the Dutch fleet.<ref name="AtSeaTraining" />

As built, ''Astrid'' had a [[gross register tonnage]] of 182; she was 143 [[net register tonnage]] with a capacity of 123 tons under her deck. Her dimensions were {{convert|100.9|ft|m|disp=flip}} length, {{convert|21.3|ft|m|disp=flip}} beam and {{convert|9.5|ft|m|disp=flip}} depth.<ref name="Wuta30" /> Her [[four-stroke engine|four-stroke]] single cycle, single action 2-cylinder auxiliary [[diesel engine]] was built by [[Deutz AG|Gasmotorenfabrikant Deutz]] [[Aktiengesellschaft|A.G.]], [[Köln]], [[Weimar Republic|Germany]]. The cylinders were {{convert|11|x|17.75|in|mm|lk=on|disp=flip}} bore by stroke.<ref name="Wuta32" />

''Astrid'' had two [[deckhouse]]s: one at the [[stern]] with navigational equipment and maps, and another forward containing a bar. The lower deck had twelve 2-person cabins (of which three could be used as 3-person cabins) as well as showers, toilets and a [[Galley (kitchen)|galley]].<ref name="AtSail" />

== Career ==
''Astrid'' was built in 1918 in [[Scheveningen]], [[Netherlands]] by G van Leeuwen as ''W.U.T.A.'', short for ''Wacht Uw Tijd Af'' meaning "Bide Your Time". She was originally rigged as a [[lugger]].<ref name="AstridHistory" /><ref name="Telegraaf" /><ref name="Wuta30" /> By 1930, she had been re-rigged as an [[schooner|auxiliary schooner]]. Then owned by N. Müller, her port of registry was [[Dordrecht]], [[South Holland]] and her [[Code Letters]] were QOSF.<ref name="Wuta30" /> In 1934, her Code Letters were changed to PIRV.<ref name="Wuta34" />

In 1937, ''Astrid'' (then known as ''W.U.T.A.'') was sold to Swede J. Jeppson and renamed ''Astrid''. Her port of registry was changed to [[Skillinge]] and the Code Letters SLEK were allocated. By this time, she had been derigged and was operating on her engine alone.<ref name="AstridHistory" /><ref name="Wuta37" /><ref name="Astrid37" /> ''Astrid'' was used on trade routes in the [[North Sea]] and [[Baltic Sea]] until 1975.<ref name="SkyNews25Jul13" /><ref name="Times300690" /> ''Astrid'' then sailed under a Lebanese flag.<ref name="Astrid" /> She was allegedly used for [[drug smuggling]], and was being shadowed in the [[English Channel]] by [[HM Customs and Excise]] when she mysteriously caught fire.<ref name="SkyNews25Jul13" /><ref name="Astrid" /><ref name="Times300690" /> ''Astrid'' was found abandoned and burnt to a shell off the coast of England in the early 1980s by Graham Neilson.<ref name="AstridHistory" /><ref name="SkyNews25Jul13" /><ref name="Times300690" />

''Astrid'' was transferred to British ownership in 1984 when she was overhauled for the Astrid Trust,<ref name="Astrid" /><ref name="Neilson" /><ref name="Times040989" /> a [[private company limited by guarantee]].<ref name="AstridTrust" /> The restoration was financially supported by Sir [[Jack Hayward]], and also involved Rear Admiral [[Charles Williams (Royal Navy)|Charles Williams]].<ref name="times_2July15" /> She was subsequently dedicated on 17 May 1989 by [[Anne, Princess Royal|Princess Anne]],<ref name="Times190589" /> following which she took part in the 800th anniversary celebrations of the [[Lord Mayor of London]].<ref name="Times030789" /> ''Astrid'' then competed in the 1989 [[Cutty Sark Tall Ships' Race]].<ref name="Times030789" /><ref name="Times100589" /> She also competed in the 1990 and 1991 Cutty Sark Tall Ships' Races.<ref name="Times300690" /><ref name="Times130791" /> ''Astrid'' made more than 16 crossings of the Atlantic Ocean as a [[training ship]].<ref name="Astrid" /><ref name="Neilson" />

''Astrid'' was captained by Paul Compton until 1999.<ref name="DorsetEcho30Aug" /> In December 1996, the Astrid Trust was [[wound up]], and ''Astrid'', then lying at [[Barbados]], was put up for sale with an asking price of [[Pound Sterling|£]]750,000.<ref name="Times311296" /> Following her sale in 1997,<ref name="Neilson" /> ''Astrid'' was converted into a luxury sailing vessel in 1999–2000, after which she could carry 45 passengers, or have 24 guest crew members.<ref name="Astrid" /> Since circa 2006, she had been owned by Ineke and Pieter de Kam.<ref name="SkyNews25Jul13" /> ''Astrid'' was a regular participant, and winner of several prizes, in Tall Ship Races and Regattas.<ref name="AtSeaTraining" /> ''Astrid'' was based in [[Weymouth Harbour, Dorset|Weymouth Harbour]], and was informally known as "Weymouth's vessel".<ref name="DorsetEcho30Aug" />

== Sinking and salvage operation ==
[[File:Astrid Brig sinking off cork coast. corkcoast-com.JPG|thumb|right|''Astrid'' sinking on 24 July 2013]]
[[File:Astrid Wrack Kinsale.JPG|thumb|The wreck of ''Astrid'' after she had been raised.]]
''Astrid'' departed from [[Southampton]], UK on 14 July 2013 and was due to arrive in [[Cherbourg]], Seine-Maritime, France on 28 July 2013 as part of a [[European Union]] International Exchange program.<ref name="AtSeaTraining" />

As of 14:00 on 23 July 2013, the ship was anchored in [[Oysterhaven Anchorage]] in County Cork. The 30 crew members of the ship consisted of the [[Master (naval)|master]]; three permanent crew members; a cook on a temporary contract; a mentor; and 24 trainees aged from 15-24 (eight from [[Ireland]], four from the [[Netherlands]], three from the [[United Kingdom]], six from [[France]], two from [[Belgium]] and one from [[Spain]]).<ref name="Int" /> This meant that there were 23 teenagers and 7 adults on board,<ref name="SkyNews25Jul13" /> consisting of 24 trainees and 6 permanent crew.<ref name="interimreport" /> Pieter de Kam was captaining the ship.<ref name="Report" /><ref name="CampusIE2015" />

The ''Astrid'' raised its anchor at around 11:00 on 24 July 2013, and left Oysterhaven on motor power.<ref name="Int" /> She was one of 50 vessels participating in Ireland's 2013 Gathering Cruise between [[Oysterhaven]] and Kinsale.<ref name="SkyNews25Jul13" /> At around 11:35 her sails were being raised while the engine was still in use, and the ship was sailing towards the south-west at around 3 knots. At around 11:40, the ship's engine failed.<ref name="Int" />

Around midday on 24 July 2013, ''Astrid'' ran aground on Quay Rock at [[Ballymacus Point]], near the [[Sovereign Islands (Ireland)|Sovereign Islands]] in southern Ireland, while attempting to enter the harbour near [[Kinsale]], [[County Cork]].<ref name="BBCNews24Jul13" /><ref name="IrishTimes19Sep" /> The ship's engine failure prevented her from pulling herself off the rocks.<ref name="IrishExaminer24Jul13" /> The thirty crew members were rescued from the tall ship, with 18 being rescued by a [[Royal National Lifeboat Institution|RNLI]] lifeboat and the other 12 by another tall ship. Four RNLI lifeboats and two [[Irish Coast Guard]] helicopters were involved in the rescue.<ref name="BBCNews24Jul13" /> The incident was filmed by the Irish Coast Guard.<ref name="BBCNews25Jul13" /> None of the crew suffered any injuries.<ref name="Int" />

The entire hull of the vessel was covered by water, with a {{convert|200|m|adj=on}} exclusion zone being enforced by the patrol ship {{ship|LÉ|Róisín|P51|6}}.<ref name="IrishTimes26Jul13" /> On 26 July, divers and a surveyor started assessing whether salvage and repair of the ship would be possible.<ref name="IrishTimes26Jul13" /> Initial reports indicated that Astrid had been looted overnight from 26–27 July, with the ship's wheel and heavy brass compass and bell stolen from the ship. However, it soon emerged that some of items were missing from the ship within hours of the incident; subsequent news reports stated that the wheel was likely ripped off the ship by the sea within 48 hours of the accident, and the Irish Coastguard and Naval Service issued a warning to stay away from the wreck.<ref name="Herald1Aug13" /> Salvage divers recovered all three missing items from the ship on 9 August, and they were handed over to the ship's owner.<ref name="TheJournal_story" /> A video showing their recovery, and the damage to the ship, was later released.<ref name="video" />

Extensive damage was found by the diver's preliminary examination of the ship, including tearing and inch-sized gaps in ''Astrid''{{'}}s hull, as well as popped [[rivet]]s and spread plates.<ref name="IrishExaminer30Jul13" /> The salvage company ''Blue Ocean'', of [[Castletownbere]], were appointed to recover the ship.<ref name="EveningEcho" /> The plans for the salvage operation consisted of removing around 3.5 tonnes of [[diesel fuel]] from the ship's fuel tanks, pumping water out of the ship and cutting away equipment including the rigging and masts, before a [[crane vessel|floating crane]] lifted the vessel from the rocks.<ref name="IrishExaminer30Jul13" /> The ship would then be taken to a nearby port to be handed over to the insurers.<ref name="IrishExaminer31Jul13" /> As of the end of July 2013 it was thought that the ship will never sail again, and that she would be written off as a [[Marine_insurance#Actual_total_loss_and_constructive_total_loss|total constructive loss]].<ref name="IrishExaminer30Jul13" /> A month after sinking, ''Astrid'' was still under water.<ref name="Afloat25Aug" />

Plans for the salvage operation were approved by the Irish Coast Guard,<ref name="IrishTimes31Aug" /> and the salvage operation began on 1 September, with work to remove loose ropes and secure the fuel containers;<ref name="Journal3Sep" /> the salvage operation was expected to take up to 3 weeks.<ref name="Afloat4Sep" /><ref name="WestCorkTimes6Sep" /> ''Astrid'' was recovered by the [[sheerleg]] ''[[GPS Atlas]]'' on 9 September 2013 by Atlantic Towage and Marine, and was transported on a barge to Kinsale for assessment and an investigation of what caused the accident.<ref name="RTE9Sep" /><ref name="Independent10Sep" /><ref name="Afloat10Sep" /> The insurers of the ship deemed the cost of restoring ''Astrid'' to be too high due to the damage caused while she was partially submerged (an economic [[write-off]]); as such she was scrapped and taken to [[Cork Harbour]] to be broken up.<ref name="WestCorkTimes18Sep" /><ref name="AstridEnd" /><ref name="Int" /> The scrapping process was completed by April 2014.<ref name="scrap" /><ref name="scrap2" />

== Investigation ==
On 23 July 2014, a day before the anniversary of the ''Astrid'''s sinking, an interim announcement from the [[Marine Casualty Investigation Board]] (MCIB) said that the loss was due to engine failure, and that a full report would be posted once standard procedure to ensure "[[natural justice]]" had been followed.<ref name="interimreport" /> They rated the type of incident as a "Very Serious Marine Casualty".<ref name="Int" /> On the same day, it was reported that some of the canvas sails from the Astrid, as well as some of the timber from the lifeboats, had been turned into [[designer]] [[handbag]]s.<ref name="handbags" /><ref name="handbags2" /><ref name="handbags3" />

The MCIB released their full report on 11 February 2015. The report found that the main cause of the incident was that the ship had not been operated safely in compliance with international conventions, and that the direct cause of the ship's grounding was due to engine failure as a result of fresh water contamination of the engine's fuel, which occurred by human error when the water was taken on board in Brighton on 12 July 2013. It found a "catalogue of failures and breaches of international regulations", including unsafe route planning that was influenced by photo opportunities rather than following the safest route, and it recommended that the master of the ship should always have authority to override courses during promotional activities to ensure the safety of the ship and its crew and passengers. It found that the [[SOLAS Convention]]s had been breached, and that the ship had not been certified as a passenger ship for either EU or international voyages, that the crew were not appropriately certified, and that the ship should not have been at sea. The [[liferafts]] were three months overdue of inspection, the [[Certificate of Seaworthiness]] was invalid, and the master's [[Certificate of Competency]] had expired a month before the accident. It also noted that mistakes were made with the mayday alert, causing a 10-minute delay in deploying the RNLI and Coast Guard, which could have had a significant impact if the conditions of the incident had been worse. The ship's owners were financially ruined by the incident.<ref name="Report" /><ref name="CampusIE2015" />

== See also ==
{{commons category|Astrid (ship, 1924)}}
* [[List of shipwrecks in 2013]]
* [[List of tall ships]]

== References ==
{{reflist|2|refs=<ref name="BBCNews24Jul13">{{cite news |url=http://www.bbc.co.uk/news/world-europe-23430217 |title=Tall ship Astrid crew rescued off Cork |work=[[BBC News]] |date=24 July 2013}}</ref>
<ref name="BBCNews25Jul13">{{cite news |url=http://www.bbc.co.uk/news/world-europe-23449861 |title=County Cork tall ship rescue 'a miracle' |work=[[BBC News]] |date=25 July 2013}}</ref>
<ref name="IrishExaminer24Jul13">{{cite news |url=http://www.irishexaminer.com/breakingnews/ireland/major-rescue-operation-underway-as-tall-ship-sinks-off-cork-coast-601548.html |title=30 rescued from distressed tall ship in major operation off Cork coast |newspaper=[[Irish Examiner]] |date=24 July 2013}}</ref>
<ref name="SkyNews25Jul13">{{cite news |url=http://news.sky.com/story/1120099/tall-ship-astrid-sinks-after-hitting-rocks |title=Tall Ship Astrid Sinks After Hitting Rocks |work=[[Sky News]] |date=25 July 2013}}</ref>
<ref name="MarineTraffic">{{cite web |url=http://www.marinetraffic.com/ais/shipdetails.aspx?imo=5027792 |title=ASTRID – Vessel's Details and Current Position – 5027792 – 244361000 |website=Marine Traffic |accessdate=26 July 2013}}</ref>
<ref name="Astrid">{{cite web |url=http://www.tallshipastrid.nl/?page_id=4&lang=en |title=The ship |website=Tallship Astrid |publisher=Rederij Horizon Sailing |accessdate=26 July 2013}}</ref>
<ref name="AstridHistory">{{cite web |url=http://www.tallshipastrid.nl/?page_id=50&lang=en |title=History |website=Tallship Astrid |publisher=Rederij Horizon Sailing |accessdate=26 July 2013}}</ref>
<ref name="IrishTimes26Jul13">{{cite news |url=http://www.irishtimes.com/news/ireland/irish-news/divers-and-surveyor-to-consider-possible-salvage-of-astrid-1.1475208 |title=Divers and surveyor to consider possible salvage of 'Astrid' |last=Siggins |first=Lorna |newspaper=[[The Irish Times]] |date=26 July 2013}}</ref>
<ref name="Neilson">{{cite web |url=http://www.adventureundersail.com/graham_neilsons_vision.html |title=Greyham Neilson's Vision |website=Adventure Under Sail Ltd. |accessdate=26 July 2013 | archiveurl=https://web.archive.org/web/20130820123746/http://www.adventureundersail.com/graham_neilsons_vision.html | archivedate=20 August 2013}}</ref>
<ref name="AtSail">{{cite web |url=http://atseasailtraining.com/177/ships/4/astrid.html |title=Astrid |website=At Sea Sail Training |accessdate=28 July 2013}}</ref>
<ref name="Telegraaf">{{cite news |url=http://www.telegraaf.nl/vaarkrant/21758406/__Nederlands_zeilschip_breekt_voor_Ierse_kust__.html |title= Nederlands zeilschip breekt voor Ierse kust |work=[[De Telegraaf]] |date=24 July 2013 |language=Dutch}}</ref>
<ref name="EveningEcho">{{cite news |url=http://www.eveningecho.ie/2013/07/30/astrid-plundered/ |title=Astrid Plundered |newspaper=[[Evening Echo]] |date=30 July 2013}}{{dead link |date=August 2013}}</ref>
<ref name="IrishExaminer30Jul13">{{cite news |url=http://www.irishexaminer.com/ireland/coast-guard-presses-astrids-insurers-for-prompt-removal-of-shipwreck-238294.html |title=Coast Guard presses Astrid's insurers for prompt removal of shipwreck |last=English |first=Eoin |newspaper=[[Irish Examiner]] |date=30 July 2013}}</ref>
<ref name="IrishExaminer31Jul13">{{cite news |url=http://www.irishexaminer.com/ireland/wreck-removal-plan-for-astrid-in-final-stages-238397.html |title=Wreck removal plan for Astrid in final stages  |last=English |first=Eoin |newspaper=[[Irish Examiner]] | date=31 July 2013}}</ref>
<ref name="Herald1Aug13">{{cite news |url=http://www.herald.ie/news/warning-to-avoid-stricken-astrid-29465423.html |title=Warning to avoid stricken 'Astrid' |first=Ralph |last=Riegel |newspaper=[[Evening Herald]] |date=1 August 2013}}</ref>
<ref name="TheJournal_story">{{cite news |url=http://www.thejournal.ie/astrid-theft-bell-1030687-Aug2013 |title=So, it turns out the Astrid bell and wheel weren't stolen… |newspaper=[[TheJournal.ie]] |date=10 August 2013}}</ref>
<ref name="AtSeaTraining">{{cite web |url=http://www.atseasailtraining.com/userfiles/file/02-05-2013%20EU%20Exchange%20Astrid%20and%20Antwerp%20Flyer%20Gathering%20at%20Sea.pdf |format=pdf |title=A European Union International Exchange 2013 |website=At Sea Sail Training |accessdate=14 August 2013}}</ref>
<ref name="video">{{cite news |url=http://www.independent.ie/irish-news/video-salvage-divers-find-deck-items-feared-looted-from-shipwreck-29503926.html |title=Video: Salvage divers find deck items feared looted from shipwreck |first=Ralph |last=Riegel |first2=Barry |last2=Duggan |newspaper=[[Irish Independent]] |date=16 August 2013}}</ref>
<ref name="DorsetEcho30Aug">{{cite news | url=http://www.dorsetecho.co.uk/news/10644428.Sunken_Tall_Ship_Astrid_could_be_saved_if_Irish_Coast_Guard_approve_plans/ | title=Sunken Tall Ship Astrid could be saved if Irish Coast Guard approve plans | first=Laura | last=Kitching | newspaper=[[Dorset Echo]] | date=30 August 2013}}</ref>
<ref name="Afloat25Aug">{{cite magazine |title = Tall Ship Astrid, A Sorry Sight for Irish Waters|date = 25 August 2013|url = http://afloat.ie/sail/tall-ships/item/23035-tall-ship-astrid-a-sorry-sight-for-irish-waters |magazine=Afloat}}</ref>
<ref name="IrishTimes31Aug">{{cite news | url=http://www.irishtimes.com/news/ireland/irish-news/coast-guard-approves-plans-to-salvage-tall-ship-1.1511060 | title= Coast Guard approves plans to salvage tall ship | newspaper=The Irish Times | date=31 August 2013}}</ref>
<ref name="Journal3Sep">{{cite news | url=http://www.thejournal.ie/tall-ship-astrid-salvage-1065216-Sep2013/ | title=Operation to lift the Astrid from the sea should be completed by next week  | newspaper=TheJournal | date=3 September 2013}}</ref>
<ref name="Afloat4Sep">{{cite magazine | url=http://afloat.ie/sail/tall-ships/item/23172-tall-ship-astrid-salvage-begins-off-kinsale | title= Salvage of Tall Ship Astrid Begins off Kinsale  |magazine=Afloat | date=4 September 2013}}</ref>
<ref name="WestCorkTimes6Sep">{{cite news | url=http://westcorktimes.com/home/?p=20668 | title=Astrid wreck to be lifted within weeks | newspaper=West Cork Times | date=6 September 2013}}</ref>
<ref name="RTE9Sep">{{cite news | url=http://www.rte.ie/news/2013/0909/473223-astrid/ | title=Astrid salvaged off Cork coast |work=Raidió Teilifís Éireann | date=9 September 2013}}</ref>
<ref name="Independent10Sep">{{cite news | url=http://www.independent.ie/irish-news/astrid-lifted-in-major-salvage-operation-29565746.html | title=Astrid lifted in major salvage operation | newspaper=The Irish Independent | date=10 September 2013}}</ref>
<ref name="Afloat10Sep">{{cite magazine | url=http://afloat.ie/sail/tall-ships/item/23241-tall-ship-astrid-back-from-the-deep-dutch-vessel-successfully-raised-in-kinsale | title= Tall Ship Astrid Back From the Deep, Dutch Vessel Successfully Raised in Kinsale |magazine=Afloat | date=10 September 2013}}</ref>
<ref name="WestCorkTimes18Sep">{{cite news | url=http://westcorktimes.com/home/?p=20923 | title= Astrid wreck set for the scrap heap | work=[[West Cork Times]] | date=18 September 2013}}</ref>
<ref name="Wuta30">{{cite web |url=http://www.plimsollshipdata.org/pdffile.php?name=30a0200.pdf |format=pdf |title=Lloyd's Register, Sailing Vessels |year=1930 |website=Plimsoll Ship Data |accessdate=19 September 2013}}</ref>
<ref name="Wuta32">{{cite web |url=http://www.plimsollshipdata.org/pdffile.php?name=32a0590.pdf |format=pdf |title=Lloyd's Register, Steamers & Motorships under 300 Tons, Trawlers, &c. |year=1932 |website=Plimsoll Ship Data |accessdate=19 September 2013}}</ref>
<ref name="Wuta34">{{cite web |url=http://www.plimsollshipdata.org/pdffile.php?name=34a0595.pdf |format=pdf |title=Lloyd's Register, Navires á Vapeur de Moins de 300 tx, Chalutiers, &c. |year=1934 |website=Plimsoll Ship Data |accessdate=19 September 2013}}</ref>
<ref name="Wuta37">{{cite web |url=http://www.plimsollshipdata.org/pdffile.php?name=37a0647.pdf |format=pdf |title=Lloyd's Register, Navires á Vapeur de Moins de 300 tx, Chalutiers, &c. |year=1937 |website=Plimsoll Ship Data |accessdate=19 September 2013}}</ref>
<ref name="Astrid37">{{cite web |url=http://www.plimsollshipdata.org/pdffile.php?name=37a0660.pdf |format=pdf |title=Lloyd's Register, Steamers & Motorships under 300 Tons, Trawlers, &c. |year=1937 |website=Plimsoll Ship Data |accessdate=19 September 2013}}</ref>
<ref name="Times040989">{{Cite newspaper The Times |articlename=An advanced scholar ship |day_of_week=Monday |date=4 September 1989 |page_number=31 |issue=63490 |column=A-F }}</ref>
<ref name="Times030789">{{cite newspaper The Times |articlename=Tall ship joins the Lord Mayor's celebration |author=Mark Pepper |day_of_week=Monday |date=3 July 1989 |page_number=3 |issue=63436 |column=D-H }}</ref>
<ref name="Times190589">{{cite newspaper The Times |articlename=Tall ship sets sail for youth |author=Mark Pepper |day_of_week=Friday |date=19 May 1989 |page_number=6 |issue=63398 |column=E-G }}</ref>
<ref name="Times300690">{{cite newspaper The Times |articlename=Deep sea challenge for Karen |author=Malcolm McKeag |day_of_week=Saturday |date=30 June 1990 |page_number38= |issue=63746 |column=H }}</ref>
<ref name="Times130791">{{cite newspaper The Times |articlename=Tides turning against the tall ships |author=George Hill |day_of_week=Saturday |date=13 July 1991 |page_number=16 |issue=64071 |column=D-H }}</ref>
<ref name="Times100589">{{cite newspaper The Times |articlename=Record fleet will sail from London |author=Barry Pickthall ||day_of_week=Wednesday |date=10 May 1989 |page_number=47 |issue=63390 |column=E-F }}</ref>
<ref name="Times311296">{{cite newspaper The Times |articlename=Uncertain fate of sail-training ship |author=Nick Messenger |day_of_week=Tuesday |date=31 December 1996 |page_number=15 |issue=65774 |column=F-G }}</ref>
<ref name="IrishTimes19Sep">{{cite news | url=http://www.irishtimes.com/news/ireland/irish-news/astrid-owner-speaks-of-his-sadness-at-decision-to-scrap-the-95-year-old-historic-brig-1.1532289 | title='Astrid' owner speaks of his sadness at decision to scrap the 95-year-old historic brig | newspaper=The Irish Times | date=19 September 2013}}</ref>
<ref name="AstridEnd">{{cite web | url=http://www.tallshipastrid.nl/?p=1016&lang=en | title=The end of a rich history | website=Rederij Horizon Sailing | date=30 December 2013 | accessdate=15 January 2014}}</ref>
<ref name="AstridTrust">{{cite web | url=http://www.cdrex.com/the-astrid-trust-7050965.html | title=The Astrid Trust | accessdate=25 February 2014}}</ref>
<ref name="scrap">{{cite web | url=http://afloat.ie/sail/tall-ships/item/25015-as-astrid-is-boxed-trainees-return-to-the-ocean-wave-on-an-irish-vessel | title= Tall Ship Astrid is Boxed As Trainees Return to the Ocean Wave on an Irish Vessel | publisher=Afloat.ie | date=23 April 2014 | accessdate=25 April 2014}}</ref>
<ref name="scrap2">{{cite web | url=http://www.corkcoast.com/astrid/2013.12.08%20Final%20Breakup/index.html | title= Astrid Tallship: end of dismantling Kinsale Co Cork Ireland | accessdate=25 April 2014}}</ref>
<ref name="interimreport">{{cite news | url=http://www.irishtimes.com/news/ireland/irish-news/astrid-sail-training-ship-loss-due-to-engine-failure-says-interim-investigation-1.1875046 | title=‘Astrid’ sail training ship loss due to engine failure says interim investigation | newspaper=The Irish Times | date=23 July 2014 | accessdate=28 July 2014}}</ref>
<ref name="handbags">{{cite news | url=http://www.independent.ie/irish-news/news/wrecks-to-riches-as-brothers-have-astrid-all-sewn-up-30452310.html | title=Wrecks to riches as brothers have Astrid all sewn up | newspaper=The Irish Independent | date=23 July 2014 | accessdate=28 July 2014}}</ref>
<ref name="handbags2">{{cite news | url=http://westcorktimes.com/home/archives/27036 | title=Designer upcyclers bring new life to Astrid life rafts | newspaper=West Cork Times | date=23 July 2014 | accessdate=28 July 2014}}</ref>
<ref name="handbags3">{{cite news | url=http://www.southernstar.ie/News/Upcycling-brothers-are-rigged-up-to-bag-new-life-for-Astrids-sails-24072014.htm | title=Upcycling brothers are rigged up to bag new life for Astrid's sails | newspaper=[[The Southern Star (County Cork)|The Southern Star]] | date=23 July 2014 | accessdate=28 July 2014}}</ref>
<ref name="Int">{{cite web|url=http://mcib.ie/reports/?thisid=2082 |title=Astrid Interim Report |publisher=Marine Casualty Investigation Board |date=22 July 2014 |accessdate=31 July 2014}}</ref>
<ref name="CampusIE2015">{{cite web | url=http://campus.ie/surviving-college/damning-review-tall-ship-sinking-finds-catalogue-failures-and-breaches | title=Damning review of tall ship sinking finds 'catalogue of failures and breaches of international regulations' | publisher=Campus.ie | accessdate=12 February 2015}}</ref>
<ref name="Report">{{cite report | title=Investigation into the loss of the Sail Training Passenger Vessel STV Astrid on 24th July 2013 | url=http://mcib.ie/reports/ | publisher=[[Marine Casualty Investigation Board]] | date=11 February 2015}}</ref>
<ref name="times_2July15">{{cite news | url=http://www.thetimes.co.uk/tto/opinion/obituaries/article4483313.ece | title=Rear Admiral Charles Williams. | newspaper=[[The Times]]'' | date=30 June 2015 | page=52 | accessdate=2 July 2015}}</ref>

}}

{{2013 shipwrecks}}

{{coord missing|Ireland}}

{{DEFAULTSORT:Astrid, STV}}
[[Category:Ships built in the Netherlands]]
[[Category:Merchant ships of the Netherlands]]
[[Category:Merchant ships of Sweden]]
[[Category:Merchant ships of Lebanon]]
[[Category:Training ships of the United Kingdom]]
[[Category:Sail training ships]]
[[Category:Merchant ships of the United Kingdom]]
[[Category:Tall ships]]
[[Category:Maritime incidents in Ireland]]
[[Category:Maritime incidents in 2013]]
[[Category:World War II merchant ships of Sweden]]
[[Category:Lost sailing vessels]]