{{italic title}}
[[File:Archiv für slavische Philologie, volume I, 1876.jpg|thumb|''Archiv für slavische Philologie'', volume I, 1876.]]
'''''Archiv für slavische Philologie''''' is the oldest [[Slavic studies|Slavic]] philological journal, generally considered as the best in the field at the time it was published. It was founded in 1875 by [[Vatroslav Jagić]] and published by [[Weidmannsche Buchhandlung]] in Berlin, and thanks to the historian [[Theodor Mommsen]] the journal received financial support from the [[Prussian Ministry of Education]]. Jagić edited the journal of since 1876 intermittently until 1920, when the 37th volume was published. After Jagić's death in 1923 it was issued irregularly by [[Erich Berneker]], but after 42 volumes it was finally shut down in 1929.<ref name="HE">{{citation |contribution-url=http://enciklopedija.hr/Natuknica.aspx?ID=3605 |contribution=Archiv für slavische Philologie |title=[[Croatian Encyclopedia]] |year=1999–2009 |accessdate=May 20, 2014 |publisher=[[Leksikografski zavod Miroslav Krleža]] |language=Croatian}}</ref>

Jagić published papers about phonological, grammatical and syntactical structure of all [[Slavic languages]], descriptions of ancient Slavic literary monuments, oral literary tradition, culture, [[Slavic mythology|mythology]], bibliographies and other texts. He decided on [[German language|German]] as the language of the journal which sparked a backlash in Slavic countries, but it was due to this decision that the journal became a mediator between the Slavic and Western European science and Slavic studies reached the level of [[Germanic studies|Germanic]] and [[Romance studies]].<ref name="HE"/>

All of the prominent Slavic and many non-Slavic philologists contributed to the journal. Jagić himself was a prolific contributor, writing extensive studies and contributions in sections ''Anzeigen'' and ''Kleine Mitteilungen''.<ref name="HE"/>

{{Wikisourcelang|de|Archiv für slavische Philologie}}

==References==
{{reflist}}

{{DEFAULTSORT:Archiv fur slavische Philologie}}
[[Category:Philology journals]]
[[Category:German-language journals]]