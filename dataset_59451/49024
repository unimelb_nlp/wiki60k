{{Infobox person
| name              = Robert Kerr
| birth_place       = [[Glasgow, Scotland]]
| birth_date              = Year unknown
| death_date        = 1918
| spouse = Miss Jeffrey
| occupation        = {{unbulleted list| Physician, Missionary }}
}}
'''Robert Kerr''', M.D., (?-1918) was a British physician, missionary, judge, and author. Sent to [[Morocco]] by the Jewish Committee of the English Presbyterian Church to work as both a medical and religious missionary, Kerr provided various types of medical care to both [[Judaism|Jewish]] and [[Muslim]] people. He served in Morocco for 30 years and provided aid to a diverse group of patients. The bulk of his missionary work is recorded in his novels ''Pioneering in Morocco: A Record of Seven Years’ Medical Mission Work in the Palace and the Hut'' and ''Morocco After Twenty-Five Years''.

==Early Life and Call to Missionary Work==

Robert Kerr was born in [[Glasgow, Scotland]] (date unknown), into a [[Presbyterianism|Presbyterian]] family. Originally practicing medicine in [[Scotland]] and [[England]], Kerr was called upon by the [[Presbyterian Church of England]] to serve as both a medical and religious missionary to the people of [[Morocco]]. Sharing Kerr’s passion for religious mission work and care for the underprivileged, his wife Miss Jeffrey joined him in Morocco along with his children.<ref name= "PioneeringMorocco">Kerr, Robert. ''Pioneering in Morocco: A Record of Seven Years' Medical Mission Work in the Palace and the Hut''. Hazell, Watson, & Viney, 2007.</ref>

==Missionary Work==

Robert Kerr pursued his medical and religious missionary work through the Jewish Committee of the English Presbyterian Church. His missionary work began with his dispatch to [[Rabat, Morocco]] on February 20, 1886.<ref name= "PioneeringMorocco" /> Over the course of 30 years, Kerr would focus his work on the communities of Rabat and [[Salé]]. His wife Miss Jeffrey would accompany him on his missionary trips and work alongside Kerr, providing care especially to women and children.<ref name="TwentyFiveYears">Kerr, Robert. ''Morocco After Twenty-Five Years''. Murray and Evenden, Ltd, 1912.</ref> Kerr eventually worked as an independent missionary in 1894, resigning his ties with the Presbyterian Church of England due to a lack of funding from the group.<ref name="PioneeringMorocco" /> Both of Kerr's novels focus on his work in Rabat and Salé.<ref name="PioneeringMorocco" /><ref name="TwentyFiveYears" />

===Missionary Work in Rabat===
Kerr was the first Christian missionary to serve in [[Rabat]], and he cared for over 2,000 people in the city during his career. His missionary work extended to both Jews and Muslims, and he also provided aid to the tribesmen of Beni Hassan, Zenior, Ziarr, and El Arab who traveled to the capital.<ref name="Volume42">''The Missionary Review of the World''. 42nd ed. New York: Missionary Review Publishing Company, Inc., 1919.</ref> In Rabat, Kerr served at the Medical Mission Housf.<ref name="PioneeringMorocco" /> Kerr's talent for medicine in Rabat was noted by the [[Moorish]] leadership, and Kerr was often called upon to treat inmates of the [[Sultan]] and members of the Sultan's family. This was considered an honor, as Kerr was a [[Christian]] and normally would not have had intimate access to members of Muslim and Moorish leadership.<ref name="PioneeringMorocco" /> Kerr's services were also invoked in matters of perilous cases of childbirth; however, his Christian religion often presented a barrier between him and the Muslims. Many traditional Muslims refused his services, but his help was received more readily by the Jews. Kerr was one of the first missionaries to treat both Muslims and Jews equally and not exclusively focus on one race in Rabat. While his medical services were eventually accepted universally in Rabat, his efforts of religious conversion were met with resistance. One young Jewish man once communicated to Kerr that his "parents would disown [him]" if he "confessed Christ."<ref name="PioneeringMorocco" /> Although he failed to convert a significant number of the population of Rabat to Christianity, specifically [[Presbyterianism]], Kerr did succeed in creating better relations between the Muslim, Jewish, and Christian faiths. In one case, Kerr was summoned to treat a boy who had been run over by a European's carriage. Tensions were building as a rumor had spread that the Christian had deliberately injured the boy. However, Kerr was able to verify that the boy was not substantially injured and that the boy had rather been told to run in front of the carriage as part of a scheme to portray the Christians and Europeans in a poor light. Kerr provided for the medical needs of the people of Rabat and also improved relations between different religions and ethnic groups in the city.<ref name="Volume28">''The Missionary Review of the World''. 28th ed. New York: Funk & Wagnalls Company, 1915.</ref>

===Missionary Work in Salé===

Kerr's work in [[Salé]] also centered on medical care and religious conversion. His work in Salé was carried out synchronously with his missionary service in Rabat, and he made his first trip to Salé in March 1886.<ref name="PioneeringMorocco" /> In Salé, Kerr mainly treated cases of [[malaria]], [[smallpox]], and [[Tuberculosis|consumption]]. His preventative efforts focused on vaccination and hygiene education.<ref name="TwentyFiveYears" /> In Salé, Kerr encountered stronger resistance to medical treatment as he was a Christian. Met with jeers and curses upon entering the city, Kerr found it difficult to establish trust among the Muslims and Jewish residents. However, following a successful smallpox vaccination campaign in the midst of an epidemic, the people came to accept his aid. Kerr became the first Christian missionary to establish permanent residency within the city gates. Additionally, during a rebellion against Europeans within Salé, Kerr became the only Christian European allowed to enter the city. Although his efforts of religious conversion were again met with resistance, religion eventually posed no barrier in regards to medical treatment.<ref name="PioneeringMorocco" /><ref name="TwentyFiveYears" /> Kerr also assumed status as a judge within Salé. He settled arguments regarding interfamily feuds, small wars, and theft.<ref name="PioneeringMorocco" /><ref name="Volume42" /> Kerr came to be accepted by both the Muslims and Jews of the city, and his patients praised his medical work.<ref name="TwentyFiveYears" />

==Legacy==

Kerr's 30 years of service impacted a diverse group of people, and he provided aid to thousands over the course of his missionary work in [[Morocco]].<ref name="Volume42" /> Kerr fulfilled a variety of positions of authority, and he compiled his experiences into two separate novels.<ref name="PioneeringMorocco" /><ref name="TwentyFiveYears" /> Kerr also frequently authored columns for newspapers regarding his views on the French occupation of Morocco, and he was featured as a writer for multiple publications.<ref name="Volume36">''The Missionary Review of the World''. 36th ed. New York: Funk & Wagnalls Company, 1913.</ref> Upon Kerr's passing, the Khalifa of Rabat expressed the community's deep sense of loss of "[their] precious doctor."<ref name="Volume42" /> Although Kerr intended to place the proceeds of his novel ''Morocco After Twenty-Five Years'' towards building a hospital in Rabat, he passed before he could achieve his goal.<ref name="TwentyFiveYears" /> Kerr's work in both [[Rabat]] and [[Salé]] is considered significant due to his nondiscrimination on the basis of ethnicity or religion in regards to medical treatment, as he served both Muslims and Jews. Kerr also earned honorary positions of authority within the Muslim and Jewish communities in Rabat and Salé, and he was eventually treated similarly to a native.<ref name="PioneeringMorocco" /><ref name="TwentyFiveYears" /> His advice for travelers within his novels has even been incorporated into travel guides today.<ref name="TravelGuide">Bidwell, Margaret, and Robin Bidwell. ''Morocco: The Traveller’s Companion''. New York: Tauris Parke Paperbacks, 2005.</ref> Kerr's ability to break down religious barriers and gain acceptance into different cultures has solidified him as a notable medical missionary.

==Writings==
* ''Pioneering in Morocco: A Record of Seven Years' Medical Mission Work in the Palace and the Hut'' (2007)
* ''Morocco After Twenty-Five Years'' (1912)

==References==
{{reflist}}

== Robert Kerr ==

{{DEFAULTSORT:Kerr, Robert}}
[[Category:Christian medical missionaries]]
[[Category:Year of birth missing]]
[[Category:1918 deaths]]
[[Category:Scottish Presbyterian missionaries]]
[[Category:Presbyterian missionaries in Morocco]]
[[Category:British expatriates in Morocco]]
[[Category:19th-century Scottish medical doctors]]