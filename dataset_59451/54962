{{refimprove|date=May 2009}}
{{Infobox district
<!-- See Template:Infobox district for additional fields and descriptions -->
| name                    = Gulripshi District
| native_name             = {{lang|ka|გულრიფშის რაიონი}}<br/>{{lang|ab|Гәылрыҧшь араион}}
| type                    = [[Districts of Abkhazia|District]]
| image_skyline           = Dranda.jpg
| image_alt               = 
| image_caption           = [[Dranda Cathedral]] is one of the oldest in Abkhazia
| image_map               = Abkhazia_Gulripsh_map.svg
| map_alt                 = 
| map_caption             = Location of Gulripshi district in Abkhazia
| latd  =  |latm  =  |lats  =  |latNS  = 
| longd =  |longm =  |longs =  |longEW = 
| coordinates_type        = region:GE-AB_type:adm1st
| coordinates_display     = inline,title
| coordinates_footnotes   = 
| subdivision_type        = [[List of sovereign states|Country]]
| subdivision_type1       = [[List of sovereign states#Other states|''De Facto'' state]]
| subdivision_name        = [[Georgia (country)|Georgia]]
| subdivision_name1       = [[Abkhazia]]<ref>{{Abkhazia-note}}</ref>
| established_title       = 
| established_date        = 
| seat_type               = [[Capital (political)|Capital]]
| seat                    = [[Gulripshi]]
| government_footnotes    = 
| leader_party            = 
| leader_title            = Governor
| leader_name             = [[Aslan Baratelia]]
| unit_pref               = Metric<!-- or US or UK -->
| area_footnotes          = 
| area_total_km2          = 1835
| area_note               = 
| elevation_footnotes     = 
| elevation_m             = 
| population_footnotes    = 
| population_total        = 18032
| population_as_of        = 2011
| population_density_km2  = auto
| population_demonym      = 
| population_note         = 
| timezone1               = [[Moscow Time|MSK]]
| utc_offset1             = +3
| postal_code_type        = 
| postal_code             = 
| area_code_type          = 
| area_code               = 
| website                 = <!-- [http://www.example.com example.com] -->
| footnotes               = 
}}
'''Gulripshi District''' ({{lang-ka|გულრიფშის რაიონი}}, {{lang-ab|Гәылрыҧшь араион}}) is a district of [[Abkhazia]], one of [[Georgia (country)|Georgia]]’s breakaway republics. It corresponds to the eponymous [[Administrative divisions of Georgia (country)|Georgian district]]. Its capital is [[Gulripshi]], the town by the same name. Until the August 2008 [[Battle of the Kodori Valley]], the north-eastern part of Gulripshi district was part of [[Upper Abkhazia]], the corner of Abkhazia controlled by Georgia until the [[Battle of the Kodori Valley]] during the [[2008 South Ossetia War|August 2008 South Ossetia War]]. Upper Abkhazia was home to 1,956 of the district's 19,918 inhabitants, most of whom were ethnic [[Svan people|Svans]] (a subgroub of the [[Georgian people]]).<ref name=census>[http://www.ethno-kavkaz.narod.ru/rnabkhazia.html 2003 (2002) Census results]</ref> Most of these fled before the battle and have not yet returned.

Of note is the [[Dranda Cathedral]] sitting over a shrine built by Justinian in 551. The medieval principality of [[Dal-Tsabal]] was centered in the district. Abkhazia's main airport, [[Sukhumi Dranda Airport]], is also located in Gulripshi district.

==Administration==

[[Adgur Kharazia]] was reappointed as Administration Head on 10 May 2001 following the March 2001 local elections.<ref name="press2001-92">{{cite news|title=Выпуск № 92|url=http://abkhazia.narod.ru/gb/1579|accessdate=24 April 2016|agency=[[Apsnypress]]|date=10 May 2001}}</ref>

On 18 December 2002, President Ardzinba released Kharazia as Administration Head and appointed him as Minister for Agriculture and Food.<ref name=uzel32238>{{cite news|script-title=ru:Новые назначения в правительстве Абхазии|url=http://www.kavkaz-uzel.ru/articles/32238/|accessdate=18 April 2011|newspaper=[[Caucasian Knot]]|language=Russian|date=18 December 2002}}</ref>

On 16 June 2003, President Ardzinba appointed [[Tamaz Gogia]] as Administration Head.<ref name=apress030616>{{cite news|title=Выпуск № 121|url=http://apsnypress.narod.ru/2003/06_jun-03/AP06-16-03.htm|accessdate=20 January 2012|newspaper=[[Apsnypress]]|date=16 June 2003}}</ref> In the beginning of 2004, district officials stayed away from work in protest of what they perceived as rudeness from Gogia. In response, Gogia applied for resignation which President Ardzinba granted on 9 February, appointing First Deputy Head [[Aslan Baratelia]] as acting Head.<ref name=uzel50583>{{cite news|last=Kuchuberia|first=Anzhela|title=Президент Абхазии освободил Тамаза Гогия от обязанностей главы администрации Гулрипшского района|url=http://www.kavkaz-uzel.ru/articles/50583/|accessdate=20 January 2012|newspaper=[[Caucasian Knot]]|date=10 February 2004}}</ref><ref name="press2004-028">{{cite news|title=Выпуск №28|url=http://www.abkhaziya.org/news_detail.html?nid=2012|accessdate=27 April 2016|agency=[[Apsnypress]]|date=9 February 2004}}</ref>

On 24 March 2005, newly elected President [[Sergei Bagapsh]] replaced Administration Head [[Aslan Baratelia]] with [[Mikhail Logua]].<ref name=agov1768>{{cite news|last=Bagapsh|first=Sergei|authorlink=Sergei Bagapsh|title=О главе Администрации Гулрыпшского района|url=http://www.abkhaziagov.org/ru/president/activities/decree/detail.php?ID=1768|accessdate=20 January 2012|newspaper=Administration of the President of the Republic of Abkhazia|date=24 March 2005}}</ref> In the [[Abkhazian presidential election, 2011|2011 Presidential election]], Logua successfully ran for Vice President alongside [[Alexander Ankvab]]. He was succeeded on 14 December by [[Timur Eshba]], who had previously been Deputy Head.<ref name=apress4957>{{cite news|title=Тимур Эшба назначен главой администрации Гульрипшского района|url=http://apsnypress.info/news/4957.html|accessdate=30 December 2011|newspaper=[[Apsnypress]]|date=15 December 2011}}</ref>

Following the [[Abkhazian Revolution|May 2014 Revolution]] and the election of [[Raul Khajimba]] as President, on 23 September 2014 he replaced Eshba with [[Aslan Baratelia]].<ref name=apress13342>{{cite news|title=У К А З О главе администрации Гулрыпшского района
|url=http://apsnypress.info/news/13342.html|accessdate=26 October 2014|agency=[[Apsnypress]]|date=23 October 2014}}</ref>

===List of Administration Heads===

{| class="wikitable" width=75% cellpadding="2"
|-style="background-color:#E9E9E9; font-weight:bold" align=left
| #
| width=240|Name
| width=200|From
|
| width=200|Until
|
| width=160|President
| width=200|Comments
|-
|rowspan=2|
|rowspan=2|[[Adgur Kharazia]]
|1993
|<ref name=pra128>{{cite web|title=Харазия Адгур Рафетович|url=http://www.parlamentra.org/officials/?ELEMENT_ID=128|publisher=People's Assembly - Parliament of the Republic of Abkhazia|accessdate=11 December 2012}}</ref>
|26 November 1994
|
|
|
|-
|26 November 1994
|
|18 December 2002
|<ref name=uzel32238/>
|rowspan=3|[[Vladislav Ardzinba]] 
|
|-
| 
|[[Tamaz Gogia]]
|16 June 2003
|<ref name=apress030616/>
|9 February 2004
|<ref name="press2004-028"/>
|
|-
|
|[[Aslan Baratelia]]
|9 February 2004
|<ref name="press2004-028"/>
|24 March 2005
|<ref name=agov1768/>
|First time
|-
| rowspan=2|
| rowspan=2|[[Mikhail Logua]]
|24 March 2005
|<ref name=agov1768/>
|29 May 2011
| 
| [[Sergei Bagapsh]]
|
|-
|29 May 2011
|
|26 September 2011
|
| rowspan=2|[[Alexander Ankvab]]
|
|-
|rowspan=2|
|rowspan=2|[[Timur Eshba]]
|14 December 2011
| <ref name=apress4957/>
|1 June 2014
|
|
|-
|1 June 2014
|

|23 October 2014
|<ref name=apress13342/>
|''[[Valeri Bganba]]''
|
|-
|
|[[Aslan Baratelia]]
|23 October 2014
|<ref name=apress13342/>
|''Present''
|
|[[Raul Khajimba]]
|Second time
|}

==Demographics==
At the time of the 2011 census, the population of the district was 18 032 people, consisting of:<ref name=census/>
* [[Armenians]] (46.8%)
* [[Abkhazians]] (33.6%)
* [[Russians]] (11.4%)
* [[Georgians]] (4.6%)
* [[Ukrainians]] (0.9%)
* [[Greeks]] (0.7%)

==Settlements==
The district's main settlements are:
*[[Gulripshi]]
*[[Tsebelda (village)|Tsebelda]]
*[[Dranda]]
*[[Babushera]]
*[[Chkhalta]] (the main village of what used to be [[Upper Abkhazia]])

==See also==
*[[Kodori Valley]]

==References==
{{Reflist}}

{{Districts of Georgia}}
{{Administrative divisions of Abkhazia}}

{{coord|43.1000|N|41.4170|E|source:wikidata|display=title}}

[[Category:Districts of Abkhazia]]
[[Category:Districts of Georgia (country)]]
[[Category:Gulripshi District| ]]