{{Infobox person
| name                    = Jay Barbree
| image                   = Barbree.jpg
| caption                 = Barbree as Master of Ceremonies at the 50th anniversary of rocket launches from Cape Canaveral.
| birth_name               = 
| birth_date               = {{birth date and age|1933|11|26}}<ref name="book2" />
| birth_place              = Georgia
| death_date               =
| death_place              =
| restingplace            =
| restingplacecoordinates =
| othername               =
| occupation              = News broadcaster, <br>reporter, author
| yearsactive             = 50
| spouse                  = Jo Barbree
| partner                 =
| children                =
| parents                 =
| influences              =
| influenced              =
| website                 =
| academyawards           =
| afiawards               =
| arielaward              =
| emmyawards              = 1970: NBC News Space Unit - Emmy award for coverage of the Apollo moon landing<ref name="eagle" />
| grammyawards            =
| iftaawards              =
| laurenceolivierawards   =
| naacpimageawards        =
| sagawards               =
| awards                  = 1995: NASA Award - for being the only journalist to have covered all 100 manned spaceflights.
}}

'''Jay Barbree''' (born November 26, 1933) is a correspondent for [[NBC News]], focusing on [[Human spaceflight|space travel]].  Barbree is the only [[journalist]] to have covered every manned [[Spaceflight|space mission]] in the United States, beginning with the first American in space, [[Alan Shepard]] aboard ''[[Freedom 7]]'' in 1961, continuing through to the last mission of the Space Shuttle, ''[[Space Shuttle Atlantis|Atlantis's]]'' [[STS-135]] mission in July 2011.<ref name="stpet">{{Cite web|url=http://www.sptimes.com/2006/09/04/State/His_place_with_space.shtml|title=A journalist who’s covered every manned NASA launch will be on the job Wednesday when ''Atlantis'' takes off.|accessdate=September 11, 2007|publisher=St. Petersburg Times|year=2006|author=Curtis Krueger}}</ref><ref name="dunn1">{{Cite web|url=http://www.abcnews.go.com/Entertainment/wireStory?id=3547687 |title=Memoir Details Reporting on Spaceflight |accessdate=September 11, 2007 |publisher=ABC News / [[Associated Press]] |year=2007 |author=Marcia Dunn |deadurl=yes |archiveurl=https://web.archive.org/web/20110622085850/http://abcnews.go.com/Entertainment/wireStory?id=3547687 |archivedate=June 22, 2011 }}</ref><ref name="disc">{{Cite web|url=http://dsc.discovery.com/news/2007/02/09/astronautculture_spa.html?category=space&guid=20070209121530|title=Astronaut Culture Stresses Achievement|accessdate=September 16, 2007|publisher=The Discovery Channel - Associated Press|year=2007|author=Seth Borenstein}}</ref> Barbree has been present for all 135 [[Space Shuttle|space shuttle]] launches, and every manned launch for the [[Project Mercury|Mercury]], [[Project Gemini|Gemini]], and [[Apollo program|Apollo]] eras. In total, Barbree has been witness to 166 [[Human spaceflight|manned]] [[space launch]]es.

==Early life==
Barbree grew up on his family's [[farm]] in [[Early County]], [[Georgia (U.S. state)|Georgia]], and entered the [[United States Air Force]] in 1950, when he was only 16 years of age.<ref name="stpet" /> Following the Air Force, Barbree began his broadcast [[journalism]] [[career]] at [[WALB]] in [[Albany, Georgia]] where, in 1957, he saw [[Sputnik|Sputnik's]] spent [[Launch vehicle|booster rocket]] orbiting in the sky and then wrote radio and TV reports about the [[Soviet Union]]'s launch of the first [[artificial satellite]].<ref name="ftoday">{{Cite web|url=http://www.floridatoday.com/apps/pbcs.dll/article?AID=/20070902/NEWS02/709020311/1007|title=The sage of space: Jay Barbree marks 50 years of space program reporting with book|accessdate= September 16, 2007 |publisher=Florida Today|year=2007|author=Todd Halvorson}}</ref><ref name="excerpt">{{Cite web|url=http://harpercanada.com/global_scripts/product_catalog/book_xml.asp?isbn=0061233927&tc=cx|title=Chapter One: Sputnik|accessdate=September 11, 2007|publisher=Harper Collins|year=2007|author=Jay Barbree}}</ref><ref name="harper">{{Cite web|url=http://harpercollins.ca/global_scripts/product_catalog/book_xml.asp?isbn=0061233927|title=Life from Cape Canaveral - Jay Barbree at Harper Collins|accessdate=September 11, 2007|publisher=Harper Collins|year=2007|author=Harper Collins}}</ref><ref name="book1" />

==Career==

===Reporting career===
[[File:Vanguard rocket explodes.jpg|thumb|200px|Vanguard exploded within seconds of launch on December 6, 1957.]]
Barbree was so interested in the [[space program]], that he paid for his own ticket to get to [[Cape Canaveral]], [[Florida]] in 1957, and watched the attempted [[Project Vanguard|Vanguard]] launch. The failed launch was one Barbree would never forget: ''"There's ignition. We can see the flames,"'' Barbree reported. ''"Vanguard's engine is lit and it's burning. But wait... wait a moment, there's... there's no liftoff! It appears to be crumbling in its own fire... It's burning on the pad... Vanguard has crumbled into flames. It failed ladies and gentlemen, Vanguard has failed."''<ref name="ftoday" />

Early the next year, he returned and witnessed the successful launch of ''[[Explorer 1]]'' on January 31, 1958, all the while calling in his reports to WALB. Eventually, Barbree was hired by radio station WEZY in [[Cocoa Beach]], and worked as a traffic reporter, covering the space program as well.<ref name="stpet" />

Six months later, Barbree joined [[NBC]] as a [[part-time]] [[space program]] [[reporter]], eventually moving up to [[full-time]].<ref name="dunn1" /> Over the years, Barbree had been offered the opportunity to move to [[Washington, D.C.]], or [[New York City]], but he turned down every offer, preferring to stay and report on what had quickly become his passion, [[spaceflight]].<ref name="stpet" />

{{quote|"This is a job where … you have to be, whether you like it or not, a certain member of the space family."<ref name="stpet" />}}

In 1958, while in a restroom, Barbree overheard a general and a [[NASA]] official, talking about an upcoming launch called "[[Project SCORE]]", one of the earliest American satellites.<ref name="ftoday" /> This would become one of Barbree's many "scoops", when after a bit of digging, he found that President [[Dwight D. Eisenhower]] would use the satellite to broadcast a pre-recorded Christmas message from outer space.<ref name="stpet" /><ref name="ftoday" /> When SCORE launched in 1958, Barbree broadcast the story, knowing the military would not deny it once the satellite was in space.<ref name="stpet" />

In the early days of the space program, astronauts and reporters would often socialize together in Cocoa Beach, and had a very different relationship than they do today.<ref name="ftoday" /> Barbree described his relationship with the astronauts as a friend and confidant, often going out to dinner with them, or socializing together when the astronauts were in town. In his book, Barbree writes that in 1961 Alan Shepard told him an "off the record" fact: He was going to be the first American astronaut in space. Barbree noted that if he were to report this, it would not only jeopardize the friendships, but possibly his career as well, so he said nothing. Barbree also recounts a conversation with Gus Grissom about the astronaut’s concerns regarding Apollo not long before the fatal [[Apollo 1]] fire.<ref name="book1" /><ref name="fire">{{Cite web|url=http://www.msnbc.msn.com/id/16797157/|title=Apollo 1’s tale retold: ‘Fire in the cockpit!’ - 40 years after tragedy, seasoned space writer re-creates the scene|accessdate=September 11, 2007|publisher=MSNBC|year=2004|author=Jay Barbree}}</ref><ref name="soar">{{Cite web| url=http://www.msnbc.msn.com/id/20641051/|title=Soar into a behind-the-scenes look of space| accessdate=September 11, 2007| publisher=The Today Show - MSNBC|year=2007|author=MSNBC}}</ref> Barbree's association with the astronauts had some unexpected bonuses as well, [[Neil Armstrong]] carried a gold coin to the moon on [[Apollo 11]] for Barbree, and [[Pete Conrad]] flew several flags and patches on [[Apollo 12]], which Barbree later handed out to friends.<ref name="stpet" />

In the early 1980s, when NASA developed the [[Teacher in Space]] program, a similar program was developed, the Journalist in Space program.<ref name="time">{{cite news|url=http://www.time.com/time/magazine/article/0,9171,139766,00.html?iid=chix-sphere|title=A Realm Where Age Doesn't Count|accessdate=September 12, 2007|publisher=Time Magazine / CNN|author=Roger Rosenblatt | date=June 24, 2001}}</ref><ref name="inspace">{{Cite web|url=http://www.spacetoday.org/Astronauts/BarbaraMorganTeacherAstronaut.html|title=May fly sometime:|accessdate=September 12, 2007|publisher=Space Today Online|year=2005|author=Space Today Online}}</ref> Barbree was one of forty finalists to be selected as a Journalist in Space.<ref name="sf">{{Cite web|url=http://www.spacefacts.de/bios/candidates/english/barbaree_jay.htm|title=Jay Barbree Biography|accessdate=September 12, 2007|publisher=Spacefacts|year=2005|author=Spacefacts}}</ref><ref name="sf2">{{Cite web|url=http://www.spacefacts.de/english/e_journalist.htm|title=Candidates for the "Journalist in Space Program"|accessdate=September 12, 2007|publisher=Spacefacts|year=2006|author=Spacefacts}}</ref><ref name="wsf">{{Cite web|url=http://www.worldspaceflight.com/bios/journalist.htm|title=Journalist-in-Space|accessdate=September 12, 2007|publisher=worldspaceflight.com|author=worldspaceflight.com}}</ref> Both the teacher and journalist programs were discontinued after the [[Challenger accident|Space Shuttle ''Challenger'' disaster]].

In 1986, following the [[Space Shuttle Challenger|Space Shuttle ''Challenger'']] accident, Barbree placed a telephone call to a friend and retired employee of NASA, who—as a favor to Barbree—went to [[Kennedy Space Center]], looked over the accident information and analysis being done, and later reported the early findings to Barbree.<ref name="stpet" /> Consequently, Barbree was the first journalist to report on the source of the destruction of the [[Space Shuttle Challenger disaster|Space Shuttle ''Challenger'']]: Faulty O-rings.<ref name="ftoday" /><ref name="ap2">{{Cite web|url=http://www.msnbc.msn.com/id/20496533/|title=Jay Barbree chronicles launches and lore in ‘Live From Cape Canaveral’|accessdate=September 11, 2007|publisher=MSNBC|year=2007|author=Marcia Dunn}}</ref><ref name="msnbc1">{{Cite web|url=http://www.msnbc.msn.com/id/3077897/|title=NBC’s Cape Canaveral correspondent retraces the ''Challenger'' tragedy|accessdate=September 11, 2007|publisher=NBC - MSNBC|year=1997|author=Jay Barbree}}</ref>  He was also part of the NBC News Space Unit that won an [[Emmy]] award for NBC's coverage of the ''[[Apollo 11]]'' moon landing.<ref name="eagle">{{Cite web|url=http://www.msnbc.msn.com/id/5462500/|title=The moments before the Eagle landed|accessdate=September 11, 2007|publisher=MSNBC|year=2004|author=Jay Barbree}}</ref><ref name="itwire">{{Cite web|url=http://www.itwire.com/content/view/14388/1102/|title=Jay Barbree’s new book: "Live From Cape Canaveral"|accessdate=September 16, 2007|publisher=iTWire|year=2007|author=William Atkins}}</ref> Following the Space Shuttle ''Columbia'' accident, Barbree was the first reporter to break the news of an internal NASA memo expressing concerns about foam striking the orbiter's left wing during ascent.<ref name="columb">{{Cite web|url=http://www.jsonline.com/story/index.aspx?id=115573|title=After ritual, reporters get back to digging|accessdate=September 11, 2007|publisher=Milwaukee Journal Sentinel|year=2003|author=Milwaukee Journal Sentinel}} {{Dead link|date=September 2010|bot=H3llBot}}</ref>

In 1995, NASA awarded him with recognition for being the "only journalist known to have covered all 100 flights". Among those present for the ceremony were several NASA officials, Alan Shepard, and shuttle commander [[Robert L. Gibson]].<ref name="stpet" />

In 2011, Barbree was honored by the [[Space Foundation]] as a recipient of the Douglas S. Morrow Public Outreach Award in recognition of the role he played in shaping the way the nation views and understands space.<ref>http://www.spacefoundation.org/news/story.php?id=1080</ref>

At age seventy-seven, Barbree is one of the longest-serving [[broadcast network|network]] correspondents to work continuously on a single subject. He started working for NBC on July 21, 1958 covering the space program, and continues in that capacity to the present. He has never missed a mission launch, despite suffering a [[heart attack]] while jogging along Cocoa Beach in 1987, and being declared clinically dead for several minutes.<ref name="book2">{{cite book | last = Barbree | first = Jay | title = The Day I Died | publisher = New Horizon Pr | location = City | year = 1990 | isbn = 0-88282-061-3 |url=http://books.google.ca/books?id=RY2ArbM692MC&q=jay+barbree+1933&dq=jay+barbree+1933&source=bl&ots=LZJYAvzkdb&sig=A6MlQ4ieJqsIYs0jEMaB1tNyuCk&hl=en&sa=X&ei=eUY5UMnyBtDoiwLzvIGICQ&ved=0CDYQ6AEwAQ}}</ref> Following his heart attack, he had bypass surgery, and still did not miss any launches.<ref name="book2" />

Barbree covers the shuttle program and space missions for the cable network [[MSNBC]], and NBC's news reports covering missions. His contract with NBC runs through 2010, allowing him to complete the coverage of the entire space shuttle program, and he has expressed hope that he will be there for the return to the moon missions.<ref name="stpet" />

===Career as author===
Barbree is the author or coauthor of seven books, including two [[memoir]]s.<ref name="dunn1" /><ref name="book1">{{cite book | last = Barbree | first = Jay | title = Live from Cape Canaveral: Covering the Space Race, from Sputnik to Today | publisher = Collins/Smithsonian Books | location = London |pages=336pp | year = 2007 | isbn = 978-0-06-123392-0 }}</ref> In 1993, Shepard, fellow Mercury astronaut [[Deke Slayton]], journalist Howard Benedict, and Barbree collaborated to write the book ''[[Moon Shot]]''.<ref name="stpet" /> Slayton was a participant in name only and died before the book was completed.

Barbree's most recent book, ''"Live from Cape Canaveral: Covering the Space Race, from Sputnik to Today"'', was released on August 28, 2007, to coincide with the 50th anniversary of spaceflight, that began with the [[Sputnik 1]] launch on October 4, 1957.<ref name="early">{{Cite web|url=http://www.earlycountynews.com/news/2007/0905/Front_Page/004.html|title=Jay Barbree recounts 50 years at the Cape |accessdate=September 11, 2007|publisher=Early County News|year=2007|author=Early County News}}</ref> The foreword is written by [[Tom Brokaw]]. Barbree's book attempts to illustrate how the media has changed in their coverage of the space programs, from the early enthusiasm to the relative disinterest in the program today.<ref name="foust">{{Cite web|url=http://www.thespacereview.com/article/944/1|title=Review: "Live from Cape Canaveral"|accessdate=September 11, 2007|publisher=The Space Review|year=2007|author=Jeff Foust}}</ref> Barbree says he wrote the book because as he looked back over his career, when recalling all the people he'd worked with, very few were left. Barbree claims that he told himself, ''"You're the only one who has covered the whole program going all the way back to the beginning, and if you don't tell the story, who is going to do it?"''<ref name="ftoday" />{{clarify|reason=sounds like WP:SPAM or self-promotion|date=August 2013}} It received generally poor reviews from space historians. {{citation needed|date=April 2013}}

Barbree also collaborated with [[Martin Caidin]] on a number of non-fiction works, such as ''Destination Mars: In Art, Myth and Science'' (Penguin, 1997, ISBN 978-0-670-86020-3) and ''A Journey Through Time: Exploring the Universe with the Hubble Space Telescope'' (Penguin, 1995, ISBN 0-670-86018-2). Barbree also wrote the [[novelization]] of "Pilot Error", an episode of ''[[The Six Million Dollar Man]]'', a television series based upon Caidin's novel ''[[Cyborg (novel)|Cyborg]]'' (Warner, 1975, ISBN 0-446-76835-9).

Over the years, Barbree has stated that he has stayed away from sensationalizing the space program, or those associated with it, and commented that he would not put some items that could be considered harmful into his newest book, stating "The whole idea of the book is not to hurt somebody."<ref name="ap2" /> Barbree attempts to illustrate this in his memoir by telling of a private investigator who approached him with an audio tape which allegedly contained proof of an extramarital affair involving an astronaut. Barbree told the investigator he would speak to his superiors, but then proceeded to erase the tape.<ref name="foust" />

==Personal life==
Barbree has been married since 1960 to the former Jo Reisinger, whom he met while covering her participation in [[Florida]] [[beauty pageant]]s. They live in [[Merritt Island]]. They have three  [[children]], Steve, Alicia, and Karla. Their son Scott died in infancy following a [[premature birth]].<ref name="ftoday" /><ref name="book1" /><ref name="bio1">{{Cite web|url=http://harpercollins.com/authors/31936/Jay_Barbree/index.aspx|title=Jay Barbree|accessdate=September 11, 2007|publisher=Harper Collins Publishers|year=2007|author=Harper Collins Publishers}}</ref>

==Bibliography==
{{refbegin}}
*{{cite book | last = Barbree | first = Jay | title = Neil Armstrong | publisher = St. Martin's Press | location = New York | year = 2014 | isbn = 1-250-04071-X }}
*{{cite book | last = Barbree | first = Jay | title = Live from Cape Canaveral: Covering the Space Race, from Sputnik to Today  | publisher = Collins/Smithsonian Books | location = London | year = 2007 | pages=336pp |isbn = 978-0-06-123392-0}}
*{{cite book | last = Barbree | first = Jay | title = Destination Mars | publisher = Penguin Studio | location = New York | year = 1997 | isbn = 0-670-86020-4 }}
*{{cite book | last = Barbree | first = Jay |author2=Caidin, Martin | title = A Journey through Time: Exploring the Universe with the Hubble Space Telescope | publisher = Penguin Studio | location = New York | year = 1995 | isbn = 0-670-86018-2 }}
*{{cite book | last = Shepard | first = Alan | authorlink = |author2=Slayton, Deke |author3=Barbree, Jay | title = Moon Shot: The Inside Story of America's Race to the Moon | publisher = Turner Pub | location = Atlanta | year = 1994 | isbn = 1-878685-54-6}}
*{{cite book | last = Barbree | first = Jay | title = The Day I Died | publisher = New Horizon Pr | location = City | year = 1990 | isbn = 0-88282-061-3}}
*{{cite book | last =Barbree | first =Jay | authorlink = | title =The Hydra Pit | publisher =Ashley Books | location = | pages =316pp | url = |year= 1977 | isbn =0-87949-084-5 }}
*{{cite book | last = Barbree | first = Jay | title = Six Million Dollar Man, No. 4 : Pilot Error | publisher = Warner Books, Incorporated | location = City | year = 1975 | isbn = 0-446-76835-9}}
{{refend}}

==References==
{{reflist|2}}

==External links==
*[http://www.nasa.gov/mission_pages/shuttle/main/index.html NASA's Space Shuttle Site]
*[http://www.msnbc.msn.com/id/3033063/ MSNBC Space News]
*{{IMDb name | id=0053866| name=Jay Barbree}}
*{{C-SPAN|Jay Barbree}}
{{Good article}}

{{Authority control}}

{{Use American English|date=January 2014}}

{{DEFAULTSORT:Barbree, Jay}}
[[Category:American television reporters and correspondents]]
[[Category:American broadcast news analysts]]
[[Category:1934 births]]
[[Category:Living people]]
[[Category:NBC News]]
[[Category:People from Merritt Island, Florida]]