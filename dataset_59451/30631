{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:USS Dakotan World War I.jpg|300px|SS ''Dakotan'' prior to [[World War I]]]]
|Ship caption=SS ''Dakotan'' prior to [[World War I]]
}}
{{Infobox ship career
|Ship name=SS ''Dakotan''
|Ship owner=[[American-Hawaiian Steamship Company]]
|Ship registry=New York<ref name=NYT-freight_steamer />
|Ship route=
|Ship ordered=September 1911<ref name=Cochran-358>Cochran and Ginger, p. 358.</ref>
|Ship awarded=
|Ship builder=*[[Maryland Steel]]
*[[Sparrows Point, Maryland]]
|Ship original cost=$672,000<ref name=Cochran-365>Cochran and Ginger, p. 365.</ref>
|Ship yard number=125<ref name=Colton>{{cite web | last = Colton | first = Tim | url = http://www.shipbuildinghistory.com/history/shipyards/2large/inactive/bethsparrowspoint.htm | title = Bethlehem Steel Company, Sparrows Point MD | work = Shipbuildinghistory.com | publisher = The Colton Company | accessdate = 12 August 2008 }}</ref>
|Ship laid down=
|Ship launched=10 August 1912
|Ship christened=
|Ship completed=November 1912<ref name="Colton" />
|Ship maiden voyage=
|Ship identification=U.S. official number: 210753
|Ship fate=expropriated by U.S. Army, 29 May 1917
|Ship notes=
}}
{{Infobox ship career
|Ship country=United States 
|Ship flag={{USN flag|1918}}
|Ship name=USAT ''Dakotan''
|Ship acquired=29 May 1917<ref name=CW-315 />
|Ship fate=transferred to U.S. Navy, 29 January 1919
}}
{{Infobox ship career
|Ship country=United States 
|Ship flag={{USN flag|1920}}
|Ship name=USS ''Dakotan'' (ID-3882)
|Ship acquired=29 January 1919
|Ship commissioned=29 January 1919
|Ship decommissioned=31 July 1919
|Ship struck=
|Ship fate=returned to owners, 31 July 1919<ref name=DANFS />
}}
{{Infobox ship career
|Ship name=SS ''Dakotan''
|Ship owner=[[American-Hawaiian Steamship Company]]
|Ship route=
|Ship acquired=31 July 1919
|Ship fate=requisitioned by [[War Shipping Administration]]; transferred to [[Soviet Union]] under [[Lend-Lease]]
}}
{{Infobox ship career
|Ship country=Soviet Union
|Ship flag=[[Image:Flag of the Soviet Union 1955.svg|100x35px|Civil Ensign of the Soviet Union]]
|Ship name=SS ''Zyrianin'' (''Зырянин'' in [[Cyrillic script|Cyrillic]])<ref name=FESCO />
|Ship namesake=[[Komi peoples]]
|Ship identification={{IMO Number|5399664}}<ref name=Miramar />
|Ship operator=
* 1943–1957: [[Far East Shipping Company]]<ref name=FESCO />
* 1957–1969: Black Sea Shipping Company<ref name=FESCO />
|Ship acquired=December 1942
|Ship fate=scrapped 1969
}}
{{Infobox ship characteristics
|Ship type=[[cargo ship]]
|Ship tonnage={{GRT|6,537}}<ref name="Cochran-365" />
{{DWT|10,175|long}}<ref name="Cochran-365" />
|Ship length=*{{convert|407|ft|10|in|m|abbr=on}} ([[length between perpendiculars|LPP]])<ref name=Miramar />
*{{convert|428|ft|9|in|m|abbr=on}} ([[length overall|overall]])<ref name=DANFS />
|Ship beam={{convert|53|ft|6|in|m|abbr=on}}<ref name=DANFS />
|Ship draft={{convert|23|ft|m|abbr=on}}<ref name=DANFS />
|Ship depth=
|Ship hold depth={{convert|29|ft|6|in|m|abbr=on}}<ref name=WP-steamer_dakotan />
|Ship propulsion=*oil-fired boilers<ref name=Cochran-357>Cochran and Ginger, p. 357.</ref>
*1 × [[quadruple expansion steam engine|quadruple-expansion]] [[steam engine]]<ref name=Miramar />
*1 × [[screw propeller]]<ref name="Cochran-357" />
|Ship speed={{convert|15|knots|km/h}}<ref name=DANFS />
|Ship capacity=*Cargo: {{convert|492549|cuft}}<ref name="Cochran-365" />
*Passengers: 16<ref name=WP-steamer_dakotan />
|Ship crew=18 officers, 40 crewmen
|Ship notes=Sister ships: {{SS|Minnesotan||2}}, {{SS|Montanan||2}}, {{SS|Pennsylvanian||2}}, {{SS|Panaman||2}}, {{SS|Washingtonian||2}}, {{SS|Iowan||2}}, {{SS|Ohioan|1914|2}}<ref name="Colton" />
}}
{{Infobox ship characteristics
|Header caption=(as USS ''Dakotan'')
|Ship displacement=14,375 t<ref name=DANFS />
|Ship troops=1,685<ref>Crowell and Wilson, p. 568.</ref>
|Ship capacity=
|Ship complement=88<ref name=DANFS />
|Ship armament=2 × {{convert|5|in|mm|adj=on}} guns (World War I)<ref name=DANFS />
|Ship notes=
}}
|}
'''SS ''Dakotan''''' was a [[cargo ship]] built in 1912 for the [[American-Hawaiian Steamship Company]] that served as a [[transport ship]] in the United States [[Army Transport Service]] in [[World War I]], and then was transferred to the [[Soviet Union]] under [[Lend-Lease]] in [[World War II]] before being finally scrapped in 1969. During World War I, she was taken over by the United States Army as '''USAT ''Dakotan'''''. Near the end of that war she was transferred to the [[United States Navy]] and [[ship commissioning|commissioned]] as '''USS ''Dakotan'' (ID-3882)'''. During World War II, the ship was transferred to the Soviet Union and renamed '''SS ''Zyrianin''''' (or '''''Зырянин''''' in [[Cyrillic script|Cyrillic]]).

''Dakotan'' was built by the [[Maryland Steel Company]] as one of eight sister ships for the American-Hawaiian Steamship Company, and was employed in inter-coastal service via the [[Isthmus of Tehuantepec]] and the [[Panama Canal]] after it opened. During World War I, as USAT ''Dakotan'', the ship carried cargo and animals to France. ''Dakotan'' was in the first American convoy to sail to France after the United States entered the war in April 1917. In Navy service, USS ''Dakotan'' carried cargo to France and returned over 8,800 American troops after the [[Armistice with Germany|Armistice]].

After her Navy service ended in 1919, she was returned to her original owners and resumed relatively uneventful cargo service over the next twenty years. ''Dakotan'' ran aground off the coast of Mexico in 1923 but was freed and towed to port for repairs. Early in World War II, the ship was requisitioned by the [[War Shipping Administration]] and transferred to the Soviet Union under the terms of [[Lend-Lease]] in December 1942. Sailing as SS ''Zyrianin'', the ship remained a part of the Soviet [[merchant fleet]] into the late 1960s.

==Design and construction==
In September 1911, the [[American-Hawaiian Steamship Company]] placed an order with the [[Maryland Steel Company]] of [[Sparrows Point, Maryland]], for four new [[cargo ships]]—{{SS|Minnesotan||2}}, ''Dakotan'', {{SS|Pennsylvanian||2}}, and {{SS|Montanan||2}}.<ref group=Note>Maryland Steel had built three ships—{{SS|Kentuckian||2}}, ''Georgian'', and ''Honolulan''—for American-Hawaiian in 1909 in what proved to be a satisfactory arrangement for both companies. See: Cochran and Ginger, p. 358.</ref> The contract cost of the ships was set at the construction cost plus an 8% profit for Maryland Steel, but with a maximum cost of $640,000 per ship. The construction was financed by Maryland Steel with a credit plan that called for a 5% down payment in cash with nine monthly installments for the balance. The deal had provisions that allowed some of the nine installments to be converted into longer-term notes or mortgages. The final cost of ''Dakotan'', including financing costs, was $66.00 per [[deadweight tonnage|deadweight ton]], which totaled just under $672,000.<ref name="Cochran-358" />

''Dakotan'' (Maryland Steel yard no. 125)<ref name="Colton" /> was the second ship built under the original contract.<ref group=Note>Further contracts on similar terms were signed in November 1911 and May 1912 to build four additional ships: {{SS|Panaman||2}}, {{SS|Washingtonian||2}}, {{SS|Iowan||2}}, {{SS|Ohioan|1914|2}}. See: Cochran and Ginger, p. 358, and Colton.</ref> She was [[ship naming and launching|launched]] on 10 August 1912,<ref name=WP-steamer_dakotan>{{cite news | title = Steamer Dakotan afloat | work = The Washington Post | date = 11 August 1912 | page = 3 }}</ref> and delivered to American-Hawaiian in November.<ref name="Colton" /> ''Dakotan'' was {{GRT|6,537|disp=long}},<ref name="Cochran-365" /> and was {{convert|428|ft|9|in|m}} in length and {{convert|53|ft|6|in|m}} [[beam (nautical)|abeam]].<ref name=DANFS>{{cite DANFS | author = [[Naval Historical Center]] | title = Dakotan | url = http://www.history.navy.mil/research/histories/ship-histories/danfs/d/dakotan.html | short = first }}</ref> She had a [[deadweight tonnage]] of {{DWT|10,175|long}} and a storage capacity of {{convert|492519|cuft}}.<ref name="Cochran-365" /> A single [[steam engine]] with oil-fired [[boiler]]s driving a single [[screw propeller]] provided her power;<ref name="Cochran-357" /> her speed was {{convert|15|knots|km/h}}.<!-- speed --><ref name=DANFS /> The steamer had accommodations for 18 officers, 40 crewmen, and could carry up to 16 passengers.<ref name=WP-steamer_dakotan />

==Early career==
When ''Dakotan'' began sailing for American-Hawaiian, the company shipped cargo from [[East Coast of the United States|East Coast]] ports via the Tehuantepec Route to [[West Coast of the United States|West Coast]] ports and [[Hawaii]], and vice versa. Shipments on the Tehuantepec Route arrived at Mexican ports—[[Salina Cruz|Salina Cruz, Oaxaca]], for eastbound cargo, and [[Coatzacoalcos]] for westbound cargo—and traversed the [[Isthmus of Tehuantepec]] on the [[Tehuantepec National Railway]].<ref>Hovey, p. 78.</ref> Eastbound shipments were primarily sugar and pineapple from Hawaii, while westbound cargoes were general in nature.<ref name=Cochran-355-56>Cochran and Ginger, pp. 355–56.</ref> ''Dakotan'' sailed in this service on the east side of North America.<ref>{{cite news | title = American-Hawaiian Steamship Co. | type = display ad | work = [[Los Angeles Times]] | date = 13 April 1914 | page = I-4 }}</ref><ref name=NYT-for_early>{{cite news | url = https://query.nytimes.com/mem/archive-free/pdf?res=9B05E2DD173AE633A25755C0A9639C946596D6CF | format = pdf | title = For early canal cargo | work = The New York Times | date = 6 May 1914 | accessdate = 14 August 2008 | page = 7 }}</ref>

At the time of the [[United States occupation of Veracruz]] on 21 April 1914, ''Dakotan'' was in port at Coatzacoalcos.<ref>{{cite news | url = https://query.nytimes.com/mem/archive-free/pdf?res=980CE5D7163AE633A25756C2A9629C946596D6CF | format = pdf | title = Funston off for Vera Cruz, General Wood to follow | work = The New York Times | date = 25 April 1914 | accessdate =2008-08-14 | page = 4 }}</ref> There she loaded 127 American refugees from sugar plantations in the area and steamed to [[Veracruz, Veracruz|Veracruz]].<ref>{{cite news | title = Mexicans tearing up railway outside Vera Cruz and burning bridges | work = The Washington Post | date = 27 April 1914 | page = 5 }}</ref> As a consequence of the American action, the [[Victoriano Huerta|Huerta]]-led Mexican government closed the Tehuantepec National Railway to American shipping.<ref name=Cochran-360>Cochran and Ginger, p. 360.</ref>

In early May, ''[[The New York Times]]'' reported that ''Dakotan'' had sailed to [[Cristóbal, Colón|Cristóbal]] to pick up a cargo of sugar that had been originally slated for transport via Tehuantepec. According to the article, the sugar was to be carried on barges through the still-unopened [[Panama Canal]], then loaded onto ''Dakotan''.<ref>{{cite news | url = https://query.nytimes.com/mem/archive-free/pdf?res=9B05E2DD173AE633A25755C0A9639C946596D6CF | format = pdf | title = For early canal cargo | work = The New York Times | date = 6 May 1914 | accessdate = 13 August 2008 | page = 7 }}</ref> There was no indication in the newspaper whether this mission was completed or not, but it is known that American-Hawaii returned to its historic route of sailing cargo around South America via the [[Straits of Magellan]] after Tehuantepec was closed but before the canal opened.<ref name=Cochran-360 />

With the opening of the Panama Canal on 15 August, American-Hawaiian ships switched to using the canal.<ref name=Cochran-360 /> In early September, American-Hawaiian announced that ''Dakotan'' would sail on a route from New York via the canal to [[San Francisco, California|San Francisco]] and on to either [[Seattle, Washington|Seattle]] or [[Tacoma, Washington|Tacoma]].<ref>{{cite news | title = Trans Atlantic ship news | work = The Wall Street Journal | date = 12 September 1914 | page = 6 }}</ref> When landslides closed the canal in October 1915, all American-Hawaiian ships, including ''Dakotan'', returned to the Straits of Magellan route.<ref name=Cochran-361>Cochran and Ginger, p. 361.</ref>

In 1916, ''Dakotan'' was one of several American-Hawaiian cargo ships [[ship chartering|chartered]] by the [[DuPont|DuPont Nitrate Company]] to carry [[sodium nitrate]] from [[Chile]] to the United States.<ref name=CSM-ship_brings>{{cite news | title = Ship brings cargo of soda nitrate | work = The Christian Science Monitor | date = 1 May 1916 | page = 11 }}</ref> ''Dakotan'' and the other cargo ships in this South American service would typically deliver loads of coal, gasoline, or steel in exchange for the sodium nitrate.<ref name=Cochran-362>Cochran and Ginger, p. 362.</ref> In May, ''[[The Christian Science Monitor]]'' reported on what may have been a typical delivery for ''Dakotan''. The ship had left [[Tocopilla]] with 91,872 bags—about {{convert|9000|LT|t}}—of sodium nitrate for use in making explosives, and, after transiting the newly reopened Panama Canal, arrived in [[Philadelphia, Pennsylvania|Philadelphia]].<ref name=CSM-ship_brings /><ref group=Note>''Dakotan'' was the first steamer to arrive in Philadelphia via the Panama Canal after its reopening.</ref>

==World War I==
After the United States declared war on Germany in April 1917, the United States Army, needing transports to move its men and [[materiel]] to France, convened a select committee of shipping executives who pored over registries of American shipping to evaluate transport capabilities. The committee selected ''Dakotan'', her sister ship ''Montanan'', and twelve other American-flagged ships that were sufficiently fast, could carry enough fuel in their [[:wikt:bunker|bunkers]] for [[transatlantic crossing]]s, and, most importantly, were in port or not far at sea.<ref>Sharpe, p. 359.</ref><ref>Crowell and Wilson, pp. 313–14.</ref> After ''Dakotan'' discharged her last load of cargo, she was officially handed over to the Army on 29 May.<ref name=CW-315>Crowell and Wilson, p. 315.</ref>

Before troop transportation began, all of the ships were hastily refitted. Of the fourteen ships, four, including ''Dakotan'' and ''Montanan'', were designated to carry animals and cargo; the other ten were designated to carry human passengers. Ramps and stalls were built on the four ships chosen to carry animals. Gun platforms were installed on each ship before it docked at the [[Brooklyn Navy Yard]], where the guns were put in place.<ref>Crowell and Wilson, p. 316.</ref><ref group=Note>The only exception was for {{SS|Finland}}, an [[American Line]] steamer in transatlantic service to [[Liverpool]]. ''Finland'' had already been outfitted for guns in early 1917.</ref> All the ships were manned by merchant officers and crews but carried military personnel: two U.S. Navy officers, Navy gun crews, [[quartermaster]]s, [[signalman (rank)|signalmen]], and [[radio|wireless]] operators. The senior Navy officer on board would take control if a ship came under attack.<ref>Gleaves, p. 102.</ref>

The American convoy carrying the first units of the [[American Expeditionary Force]] was separated into four groups;<ref group=Note>The individual groups of the first convoy were typically counted as separate convoys in post-war sources. See, for example, Crowell and Wilson, Appendix G, p. 603.</ref> ''Dakotan'' was in the fourth group with her sister ship ''Montanan'', Army transports {{USAT|El Occidente||2}} and {{USAT|Edward Luckenbach||2}}, and accompanied by the group's escorts: [[cruiser]] {{USS|St. Louis|C-20|2}}, U.S. Navy [[transport]] {{USS|Hancock|AP-3|2}}, and [[destroyer]]s {{USS|Shaw|DD-68|2}}, {{USS|Ammen|DD-35|2}}, and {{USS|Flusser|DD-20|2}}.<ref>Gleaves, p. 38.</ref> ''Dakotan'' departed with her group on the morning of 17 June for [[Brest, France|Brest]], France, steaming at an {{convert|11|knots|km/h|adj=on}} pace.<ref>Gleaves, p. 42.</ref> A thwarted submarine attack on the first convoy group,<ref>Gleaves, pp. 42–43.</ref> and reports of heavy submarine activity off of Brest resulted in a change in the convoy's destination to [[Saint-Nazaire]].<ref>Gleaves, p. 45.</ref>

''Dakotan'' departed Saint-Nazaire on 14 July in the company of her convoy mates ''El Occidente'', ''Montanan'', and ''Edward Luckenbach''. Joining the return trip were Army transport {{USAT|Momus||2}}, Navy armed [[collier (ship)|collier]] {{USS|Cyclops|AC-4|2}}, Navy [[oiler (ship)|oiler]] {{USS|Kanawha|AO-1|2}}, and [[cruiser]] {{USS|Seattle|ACR-11|2}}, the flagship of [[Rear admiral (United States)|Rear Admiral]] [[Albert Gleaves]], the head of the Navy's [[Cruiser and Transport Force]].<ref>Gleaves, p. 54.</ref>

[[Image:Bridge of USS Dakotan.jpg|thumb|left|The bridge and foredeck of USS ''Dakotan'', c. 1919]]
Sources do not reveal ''Dakotan''{{'}}s movements over the next months, but on 6 September 1917, the [[Naval Armed Guard]]smen aboard ''Dakotan'' shelled a German submarine after its [[periscope]] had been sighted.<ref>Bureau of Ordnance, pp. 51–52.</ref> On 29 January 1919, ''Dakotan'' was transferred to the Navy and [[ship commissioning|commissioned]] the same day, with [[Lieutenant Commander (United States)|Lieutenant Commander]] J. Simmons, {{USNRF|first=short}}, in command.<ref name=DANFS /> Outfitted for service as a [[troop transport]] to return American servicemen from Europe, ''Dakotan'' made five transatlantic roundtrips to France as part of the Navy's [[Cruiser and Transport Force]] between 15 February and 20 July. Eastbound journeys delivered cargo to Saint-Nazaire and [[Bordeaux]] for the [[US Army|Army of Occupation]]; westbound trips returned soldiers to the United States. ''Dakotan'' carried a total of 8,812 troops on her five westbound voyages.<ref name=Gleaves-254>Gleaves, pp. 254–55.</ref> ''Dakotan'' returned from her final voyage on 20 July,<ref name=Gleaves-254 /> was decommissioned at New York on 31 July, and returned to American-Hawaiian the same day.<ref name=DANFS />

==Interwar years==
''Dakotan'' resumed cargo service with American-Hawaiian after her return from World War I service. Although the company had abandoned its original Hawaiian sugar routes,<ref name=Cochran-363>Cochran and Ginger, p. 363.</ref> ''Dakotan'' continued inter-coastal service through the Panama Canal in a relatively uneventful manner over the next twenty years. One incident of note occurred on 20 August 1923 when ''Dakotan'' issued [[distress call]]s after she ran aground at [[Cabo San Lazaro|Cabo San Lázaro]] on the Pacific coast of Mexico. The Navy transport ship {{USS|Henderson|AP-1|2}} and the [[Standard Oil]] [[tank ship|tanker]] ''Charles Pratt'' responded to ''Dakotan''{{'}}s calls.<ref name=NYT-freight_steamer>{{cite news | title = Freight steamer ashore | work = The New York Times | date = 21 August 1923 | page = 3 }}</ref> ''Charles Pratt'' successfully freed ''Dakotan'', which had suffered damage to her [[Rudder#Boat rudders details|rudder post]] in the accident. The American-Hawaiian ship ''Nevadan'' arrived and towed ''Dakotan'' to [[Los Angeles, California|Los Angeles]] for repairs.<ref>{{cite news | title = Dakotan, pulled off reef, being towed to port | work = The Los Angeles Times | date = 22 August 1923 | page = II-3 }}</ref>

[[Image:SS Zyrianin.jpg|thumb|right|SS ''Zyrianin'' in port at [[San Francisco, California|San Francisco]], c. 1943]]
In 1933, two members of ''Dakotan''{{'}}s crew had medical emergencies that received news coverage. The first, in February, involved a seaman with an abdominal disorder. He was transferred from the eastbound ''Dakotan'' to the [[Dollar Line]] ocean liner {{SS|President Hayes||2}} which carried him to Los Angeles to receive medical attention.<ref>{{cite news | last = Cave | first = Wayne B. | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times | date = 13 February 1933 | page = 11 }}</ref> The second occurred in July when ''Dakotan''{{'}}s [[quartermaster (naval)|quartermaster]] came down with [[appendicitis]] near [[Balboa, Panama|Balboa]]. Radio calls for assistance brought the U.S. Navy's Destroyer Division 7 to ''Dakotan''{{'}}s aid.<ref group=Note>Destroyer Division 7 consisted of {{USS|Childs|DD-241|2}}, {{USS|Barry|DD-248|2}}, and {{USS|Williamson|DD-244|2}}</ref> The destroyer unit's medical officer boarded ''Dakotan'' and performed an [[Appendicectomy|appendectomy]] on the man, who was too ill to be moved off the ship.<ref>{{cite news | title = Destroyer doctor saves freighter officer at sea | work = The New York Times | date = 13 July 1933 | page = 39 }} The article does not state on which ship the doctor was stationed.</ref>

==World War II and later career==
After the United States entered [[World War II]], in 1941 - though most of Europe had been involved since summer 1939 - ''Dakotan'' was requisitioned by the [[War Shipping Administration]] (WSA), but continued to be operated by American-Hawaiian.<ref name=CSM-US_awards>{{cite news | last = Stone | first = Leon | title = U.S. awards $7,247,637 to Hawaiian ship firm | work = The Christian Science Monitor | date = 31 March 1945 | page = 4 }}</ref> In December 1942, ''Dakotan'' was transferred to the [[Soviet Union]] under [[Lend-Lease]], and renamed ''Zyrianin'' (''Зырянин'' {{IPA-ru|zɨˈrʲanʲɪn}}).<!-- Cyrillic spelling --><ref name=FESCO>{{cite web | title = Реестр флота ДВМП: Зырянин (Dakotan) | url = http://www.fesco.ru/fleetr/second/f260.html | language = Russian | publisher = FESCO Transport Group | accessdate = 24 August 2008 }} [https://translate.google.com/translate?u=http%3A%2F%2Fntic.msun.ru%2Fntic%2Fexhibition%2Ffesco%2Fsecond%2Ff260.html&sl=ru&tl=en&hl=en&ie=UTF-8 Google translation into English].</ref><ref name=NHC_photo>{{cite web | author = Naval Historical Center | title = Picture Data: Photo #NH 91246 | url = http://www.history.navy.mil/our-collections/photography/numerical-list-of-images/nhhc-series/nh-series/NH-91000/NH-91246.html | work = Online Library of Selected Images | publisher = Navy Department, Naval Historical Center | date = 17 April 2005 | accessdate = 14 August 2008 }}</ref><!--English spelling, Lend-Lease, other details--> Throughout the rest of the war, ''Dakotan'' made at least one trip to the United States, being photographed in port at San Francisco in August 1943.<ref name=NHC_photo /> Near the end of World War II, the WSA offered a payment of $670,210 to American-Hawaiian for the former ''Dakotan'' as part of a $7.2 million settlement for eleven American-Hawaiian ships that had been requisitioned by the WSA.<ref name=CSM-US_awards /> ''Zyrianin'' remained a part of the Soviet merchant fleet through the 1960s, and was listed in ''[[Lloyd's Register]]'' until the 1970–71 edition.<ref name=NHC_photo />

''Zyrianin'' was operated by the [[Far East Shipping Company]] (<small>FESCO</small>) from 1943 to 1957. From 1957, she was operated by the [[Black Sea Shipping Company]]. The ship was written off and scrapped at [[Split, Croatia|Split]], [[Socialist Federal Republic of Yugoslavia|Yugoslavia]] in 1969.<ref name=FESCO /><ref name=Miramar>{{cite web | url =http://www.miramarshipindex.org.nz/ship/list?search_op=OR&IDNo=5399664| title =''Dakotan''| work = Miramar Ship Index | publisher = R.B.Haworth | accessdate = 12 August 2008 }}</ref>

==Notes==
{{Reflist|group=Note}}

==References==
{{Reflist|30em}}

==Bibliography==
{{Refbegin}}
* {{cite book | author = [[Bureau of Ordnance]] | title = Navy Ordnance Activities: World War, 1917–1918 | location = [[Washington, D.C.]] | publisher = [[United States Government Printing Office]] | year = 1920 | oclc = 686627 }}
* {{cite journal | last = Cochran | first = Thomas C. | authorlink = Thomas C. Cochran (historian) |author2=[[Ray Ginger]]  | title = The American-Hawaiian Steamship Company, 1899–1919 | journal = The Business History Review | volume = 28 | issue = 4 |date=December 1954 | pages = 343–365 | location = [[Boston, Massachusetts|Boston]] | publisher = The President and Fellows of Harvard College | oclc = 216113867 | doi = 10.2307/3111801 | jstor = 3111801 }}
* {{cite book | last = Crowell | first = Benedict | authorlink = Benedict Crowell |author2=Robert Forrest Wilson | title = The Road to France: The Transportation of Troops and Military Supplies, 1917–1918 | series = How America Went to War: An Account From Official Sources of the Nation's War Activities, 1917–1920 | location = [[New Haven, Connecticut|New Haven]] | publisher = [[Yale University Press]] | year = 1921 | oclc = 18696066 }}
* {{Gleaves}}
* {{cite journal | last = Hovey | first = Edmund Otis | title = The Isthmus of Tehuantepec and the Tehuantepec National Railway | journal = Bulletin of the American Geographical Society | volume = 39 | issue = 2 | year = 1907 | pages = 78–91 | location = New York | publisher = [[American Geographical Society]] | oclc = 2097765 | doi = 10.2307/198380 | jstor = 198380 }}
* {{cite DANFS | author = [[Naval Historical Center]] | title = Dakotan | url = http://www.history.navy.mil/danfs/d1/dakotan.htm | accessdate = 13 August 2008 }}
* {{cite book | last = Sharpe | first = Henry Granville | authorlink = Henry Granville Sharpe | title = The Quartermaster Corps in the Year 1917 in the World War | location = New York | publisher = [[The Century Company|Century Co.]] | year = 1921 | oclc = 7980339 }}
{{Refend}}

==External links==
* {{navsource|12/173882|Dakotan}}
*[https://archive.org/stream/pacificmarinerev3437paci#page/n226/mode/1up Engine/engine room photos]

{{Use dmy dates|date=January 2012}}

{{featured article}}

{{DEFAULTSORT:Dakotan}}
[[Category:Cargo ships]]
[[Category:Ships built in Sparrows Point, Maryland]]
[[Category:1912 ships]]
[[Category:World War I merchant ships of the United States]]
[[Category:World War I auxiliary ships of the United States]]
[[Category:Cargo ships of the United States Navy]]
[[Category:Unique transports of the United States Navy]]
[[Category:United States Navy North Dakota-related ships]]
[[Category:United States Navy South Dakota-related ships]]
[[Category:World War II merchant ships of the United States]]
[[Category:World War II auxiliary ships of the United States]]
[[Category:World War II merchant ships of the Soviet Union]]
[[Category:Merchant ships of the Soviet Union]]
[[Category:Soviet Union–United States relations]]