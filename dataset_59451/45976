{{Infobox musical artist<!--See Wikipedia:WikiProject Musicians -->
| name = A-Lee
| image_size =
| background = solo_singer
| caption =
| image = A-Lee 2013.jpg
| birth_name =  Ali Pirzad-Amoli
| birth_date  = {{Birth date and age|1988|05|17|df=y}}
| birth_place = [[Gjøvik]], [[Norway]]
| origin = [[Oslo]], Norway
| instrument  = Vocals
| genre = [[Pop music|Pop]], [[Hip hop music|hip hop]], [[electropop]], [[dance-pop]]
| occupation = [[Music artist (occupation)|Recording artist]], [[Rapping|rapper]], [[Singing|singer]], [[songwriter]], [[Record producer|executive producer]], [[Chief executive officer|CEO]]
| years_active = 2005–present
| label = [[Tee Productions]] <small>(2007-2008)</small><br>EE Records <small>(2009-Present)</small><br>[[Cosmos Music Group|Cosmos Music]] <small>(2009-2012)</small><br>[[Columbia Records|Columbia]]/[[Sony Music Entertainment|Sony Music]] <small>(2012-2015)</small><br>[[BMG Rights Management|BMG Chrysalis (publishing)]]<br><small>(2012-Present)</small>
| associated_acts =
| website = {{url|aleemusic.com}}
}}

'''Ali Pirzad-Amoli''' ({{lang-fa|علی پیرزاد آملی}}; born May 17, 1988),<ref name="A-Lee's article in Norwegian newspaper Dagbladet">{{cite news
| title = A-Lee i lykkesjokk over å være radio-vinner
| url = http://www.dagbladet.no/2012/09/01/kultur/a-lee/ali_pirzad-amoli/gramo/radiospilling/23198580/
| publisher = Dagbladet (Norwegian newspaper)
| date = 1 September 2012
| accessdate = 13 November 2012}}</ref> better known by his [[stage name]] '''A-Lee''' (also sometimes stylized as '''A-LEE''') is a [[Norway|Norwegian]] [[Pop music|pop]] [[Music artist (occupation)|recording artist]], [[Rapping|rapper]], [[Singing|singer]], [[songwriter]] and [[Record producer|executive producer]].<ref name="A-Lee Official Website Biography">{{cite news
| title = A-Lee Biography
| url = http://aleemusic.com/biography_1/
| publisher = A-Lee Official Website
| date = 13 November 2012
| accessdate = 13 November 2012}}</ref> A-Lee is the co-founder and [[Chief executive officer|CEO]] of his own record label EE Records <ref name="A-Lee Official Website Biography"/> and with [[BMG Rights Management|BMG Chrysalis]] as a songwriter.<ref name="A-Lee Signs To BMG">{{cite news
| title = BMG Official Website
| url = http://www.bmg.com/category/news/?n=scan-a-lee-signs-to-bmg
| publisher = BMG Rights Management/BMG Chrysalis
| date = 13 November 2012
| accessdate = 13 November 2012}}</ref><ref name="A-Lee Artist Page To BMG">{{cite news
| title = A-Lee artist page on BMG Official Website
| url = http://www.bmg.com/category/music/?artist=a-lee
| publisher = BMG Rights Management/BMG Chrysalis
| date = 29 December 2012
| accessdate = 29 December 2012}}</ref>

A-Lee is a [[Music recording sales certification|multi-gold and platinum]] selling artist<ref name="A-Lee Official Website Biography"/><ref name="A-Lee Signs To BMG"/> who released his debut album ''Missing'' in 2010<ref name="Cosmos Music Group artists list">{{cite news
| title = Cosmos Music Group artists list
| url = http://www.cosmosmusicgroup.com/artists.php
| publisher = Cosmos Music Group
| date = September 2010
| accessdate = 13 November 2012}}</ref><ref name="A-Lee Missing album - 730.no article">{{cite news
| title = Norges nye rapstjerne
| url = http://www.730.no/musicitem.aspx?newsId=6243
| publisher = 730.no
| date = 12 October 2010
| accessdate = 13 November 2012}}</ref> and his second album ''[[Forever Lost (A-Lee album)|Forever Lost]]'' in 2012<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Official Website Biography"/><ref name="A-Lee Signs To BMG"/> which contains numerous chart topping songs. His singles [[The One (A-Lee song)|"The One"]] and [[World So Cold (A-Lee song)|"World So Cold"]] reached respectively ''#7'' and ''#12'' at [[VG-lista|Norwegian Official Charts VG-Lista]].<ref name="A-Lee Charts VG-Lista Official Website">{{cite news
| title = A-Lee on VG-Lista Norwegian Official Charts
| url = http://lista.vg.no/artist/a-lee/5527
| publisher = VG-Lista (Norwegian newspaper)
| date = 13 November 2012
| accessdate = 13 November 2012}}</ref><ref name="A-Lee NorwegianCharts.com Website">{{cite news
| title = A-LEE IN NORWEGIAN CHARTS
| url = http://norwegiancharts.com/showinterpret.asp?interpret=A-Lee
| publisher = NorwegianCharts.com
| date = 15 November 2012
| accessdate = 15 November 2012}}</ref><br>A-Lee was also certified the most-played Norwegian [[Music artist (occupation)|artist]] on radio in Norway in 2011 and his single [[The One (A-Lee song)|"The One"]] was the most-played song on all [[List of Norwegian-language radio stations|Norwegian radio]] in 2011.<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Official Website Biography"/><ref name="A-Lee GRAMO Statistics for 2011 PDF file">{{cite news
| title = GRAMO Statistics for 2011
| url = http://gramo.no/files/docs/stat2011.pdf
| publisher = GRAMO
| date = 3 September 2012
| accessdate = 13 November 2012}}</ref> In 2012, A-Lee performed his summer smash hit "Feelgood" at huge annual show [[VG-Lista]] [[Rådhusplassen]] in [[Oslo]]<ref name="A-Lee VG-Lista Tour 2012">{{cite news
| title = VG-Lista Tour 2012 artists
| url = http://no.vglista.no/artister/oslo
| publisher = VG-Lista
| date = 29 June 2012
| accessdate = 13 November 2012}}</ref><ref name="A-Lee VG-Lista 2012 Oslo YouTube">{{cite news
| title = A-LEE&nbsp;— FEELGOOD&nbsp;— VG-LISTA RÅDHUSPLASSEN 2012
| url = https://www.youtube.com/watch?v=rh4C77xN4WU
| publisher = YouTube
| date = 2 July 2012
| accessdate = 13 November 2012}}</ref> where he attended for the third consecutive year after 2010 and 2011.<ref name="A-Lee VG-Lista 2010 Oslo YouTube">{{cite news
| title = A-LEE&nbsp;— WORLD SO COLD LIVE&nbsp;— VG-LISTA 2010
| url = https://www.youtube.com/watch?v=kKxHB64clh0
| publisher = YouTube
| date = 6 July 2010
| accessdate = 13 November 2012}}</ref><ref name="A-Lee VG-Lista 2011 Oslo YouTube">{{cite news
| title = A-LEE&nbsp;— THE ONE (LIVE) VG-LISTA 2011
| url = https://www.youtube.com/watch?v=g695aITz3_A
| publisher = YouTube
| date = 18 June 2011
| accessdate = 13 November 2012}}</ref>

During his career, A-Lee made many appearances on Norwegian TV ([[MTV Norway]], national channels [[Norwegian Broadcasting Corporation|NRK]]<ref name="A-Lee VG-Lista 2012 Oslo YouTube"/><ref name="A-Lee VG-Lista 2010 Oslo YouTube"/><ref name="A-Lee VG-Lista 2011 Oslo YouTube"/> & [[TV 2 (Norway)|TV2]],<ref name="A-Lee TV2 God Morgen Norge">{{cite news
| title = A-LEE&nbsp;— OVER YOU (LIVE) GOD MORGEN NORGE
| url = https://www.youtube.com/watch?v=l4Ujw9xLCPQ
| publisher = YouTube
| date = 30 October 2011
| accessdate = 13 November 2012}}</ref> [[The Voice TV Norway|The Voice TV]]), extensive Norwegian radio ([[NRJ Radio]], [[The Voice (radio station)|The Voice]], [[NRK P3]], NRK mP3, [[P4 Radio Hele Norge|P4]], P5, JRG Group). A-Lee performed more than 300 concerts, including as support for [[Tinie Tempah]], [[50 Cent]], [[Snoop Dogg]], [[Nas]], [[Busta Rhymes]], [[Game (rapper)|The Game]] and more...<ref name="A-Lee Official Website Biography"/>

==Early life==
A-Lee was born in [[Gjøvik]], Norway, to [[Iranian people|Iranian]]-born parents.<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/> When he was three years old, his parents divorced. At age of ten, he and his mother moved to [[Oslo]], Norway then when he was thirteen years old, they moved to [[Los Angeles]], [[California]] where A-Lee lived for almost 1 year. He then decided to move back to Oslo to live with his father and grew up in the borough [[Årvoll]], [[Bjerke]].

A-Lee grew up listening to [[Michael Jackson]], [[Eminem]], [[Jay-Z]], [[Nas]] and [[Kanye West]] and citing them as his biggest inspirations.

==Music career==

===2005-2008: Early career===
A-Lee started in the hip-hop/rap duo Cam'N A-Lee with rapper Cam'leon. In 2005, Cam'N A-Lee won the [[by:Larm]]'s ''Audience Award''<ref name="A-Lee by:larm audience award">{{cite news
| title = Lumsk vant Bylarmstipendet&nbsp;— Christer Falck årets by:Larmer
| url = http://www.ballade.no/nmi.nsf/doc/art2005021414115963796749
| publisher = Ballade.no
| date = 14 February 2005
| accessdate = 13 November 2012}}</ref><ref name="A-Lee by:larm audience award 2">{{cite news
| title = 16 år og Norges hip-håp
| url = http://www.side2.no/musikk/article344666.ece
| publisher = Side2.no
| date = 11 February 2005
| accessdate = 13 November 2012}}</ref> and were nominated for ''Årets Urørt'' by [[NRK P3]] with the song "What Do You Mean" featuring Chicosepoy, a member of the Norwegian [[Hip hop music|rap group]] [[Karpe Diem]].<ref name="A-Lee Årets Urørt">{{cite news
| title = Hør på Oslos urørte artister!
| url = http://www.aftenposten.no/kultur/musikk/article964822.ece
| publisher = Aftenposten.no
| date = 8 February 2005
| accessdate = 13 November 2012}}</ref> The duo Cam'N A-Lee disbanded in end 2006 after the release of their [[Extended play|EP]] ''Schmokin' Skills''.<ref name="Cam'N A-lee&nbsp;— Schmokin' Skills">{{cite news
| title = Cam'N A-lee&nbsp;— Schmokin' Skills
| url = http://www.roughrhythm.com/Webshop/Skiver/?module=Webshop;action=Product.publicOpen;id=3
| publisher = roughrhythm.com
| year = 2006
| accessdate = 13 November 2012}}</ref>

In January 2007, A-Lee was signed by Norwegian [[Hip hop music|hip hop]] [[Record producer|producer]] [[Tommy Tee]] on his [[Record label|label]] [[Tee Productions]]. A-Lee appeared on few [[Tommy Tee]]'s releases but during 2008, A-Lee and Tommy Tee decided to go separate ways on good terms.

===2008-2009: From demo to EE Records===
In Summer 2008, after recording a first [[Demo (music)|demo]], A-Lee went to [[New York City|New York]] to meet people from [[music industry]] working in labels such as [[Jay-Z]]'s [[Roc Nation]], [[Eminem]]'s [[Shady Records]] and [[Ryan Leslie]]'s NextSelection Lifestyle Group. From this experience, A-Lee received positive feedbacks which motivated him to continue on his way. Back from [[New York City|New York]], A-Lee started working on new songs which he recorded between Norway and [[Switzerland]].

In fall 2009, A-Lee founded his own [[record label]] EE Records with his [[Talent manager|manager]] Shahrouz Ghafourian<ref name="A-Lee Official Website Biography"/> and released his first official single "Bump Off".<ref name="A-Lee Bump Off Side2">{{cite news
| title = Sex, drap og Frøken Norge-hjelp
| url = http://www.side2.no/musikk/article2734989.ece
| publisher = Side2.no
| date = 19 October 2009
| accessdate = 13 November 2012}}</ref> He also collaborates with famous [[Music recording sales certification|multi-platinum]] duo [[Madcon]] on the single "Keep Talking".<ref name="A-Lee Keep Talking 730.no">{{cite news
| title = Toppløs Tshawe, grønn Mira Craig
| url = http://www.730.no/musicitem.aspx?newsId=2612
| publisher = 730.no
| date = 8 September 2009
| accessdate = 13 November 2012}}</ref>
In December 2009, A-Lee signed a distribution deal with [[Scandinavia]]n biggest [[Independent record label|independent label]] [[Cosmos Music Group|Bonnier Amigo Music Group]] (now known as [[Cosmos Music Group]]) to release his debut album ''Missing''.<ref name="Cosmos Music Group artists list"/><ref name="A-Lee Missing album - 730.no article"/>

===2010-2011: Success and ''The One''===
In May 2010, A-Lee released his breakthrough single [[World So Cold (A-Lee song)|"World So Cold"]] which sold ''[[Music recording sales certification|Platinum]]'' in Norway<ref name="A-Lee Signs To BMG"/><ref name="A-Lee IFPI NOR">{{cite news
| title = IFPI Norway Certification
| url = http://www.ifpi.no/sok/index_trofe.htm
| publisher = IFPI.no
| date = 13 November 2012
| accessdate = 13 November 2012}}</ref> and reached ''#12'' at [[VG-lista|Norwegian Official Charts VG-Lista]].<ref name="A-Lee Charts VG-Lista Official Website"/><ref name="A-Lee NorwegianCharts.com Website"/> The single was the most played song on radio ''NRK mP3'' in 2010<ref name="A-Lee NRK mP3 2010">{{cite news
| title = mP3s Topp 100 fra 2010
| url = http://www.nrk.no/mp3/2011/01/mest-spilt-i-2010/
| publisher = nrk.no
| date = 1 January 2011
| accessdate = 13 November 2012}}</ref><ref name="A-Lee NRK mP3 2011">{{cite news
| title = A-Lee på Topp!
| url = http://www.nrk.no/mp3/2011/12/a-lee-pa-topp-2/
| publisher = nrk.no
| date = 16 December 2011
| accessdate = 13 November 2012}}</ref> and was also the 6th most played song on all [[List of Norwegian-language radio stations|Norwegian radio]] in 2010.<ref name="A-Lee GRAMO Statistics for 2010 PDF file">{{cite news
| title = GRAMO Statistics for 2010
| url = http://gramo.no/files/docs/stat2010.pdf
| publisher = GRAMO
| date = 4 November 2012
| accessdate = 13 November 2012}}</ref>

In January 2011 A-Lee released the single [[The One (A-Lee song)|"The One"]] which sold ''[[Music recording sales certification|2x Platinum]]'' in Norway<ref name="A-Lee Official Website Biography"/><ref name="A-Lee Signs To BMG"/><ref name="A-Lee IFPI NOR"/> and reached ''#7'' at [[VG-lista|Norwegian Official Charts VG-Lista]].<ref name="A-Lee Charts VG-Lista Official Website"/><ref name="A-Lee NorwegianCharts.com Website"/> A-Lee was also certified the most-played Norwegian [[Music artist (occupation)|artist]] on radio in Norway in 2011<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Official Website Biography"/><ref name="A-Lee GRAMO Statistics for 2011 PDF file"/> and his single [[The One (A-Lee song)|"The One"]] was the most-played song on all [[List of Norwegian-language radio stations|Norwegian radio]] in 2011<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Official Website Biography"/><ref name="A-Lee GRAMO Statistics for 2011 PDF file"/> and ranked ''#2'' at the ''[[NRJ Radio|NRJ radio Norway best song of 2011]]''.<ref name="A-Lee NRJ best song of 2011">{{cite news
 |title=Best of: NRJs topp 8 klokken 8 
 |url=http://www.nrj.no/artikkel/best-of-nrjs-topp-8-klokken-8-2/ 
 |publisher=NRJ.no 
 |date=30 December 2011 
 |accessdate=13 November 2012 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120122030752/http://www.nrj.no:80/artikkel/best-of-nrjs-topp-8-klokken-8-2/ 
 |archivedate=22 January 2012 
 |df= 
}}</ref> The song [[The One (A-Lee song)|"The One"]] was also nominated for ''Best Norwegian Hit'' at [[NRJ Music Award|NRJ Music Awards 2012]].<ref name="A-Lee NRJ Awards 2012"/><br>
Again in 2011, A-Lee released the singles [[Hear the Crowd|"Hear The Crowd"]] (''[[Music recording sales certification|Gold]]'' in Norway,<ref name="A-Lee Official Website Biography"/><ref name="A-Lee Signs To BMG"/><ref name="A-Lee IFPI NOR"/> 7th most played song on all [[List of Norwegian-language radio stations|Norwegian radio]] in 2011)<ref name="A-Lee GRAMO Statistics for 2011 PDF file"/> and "[[Before My Eyes]]" (''Gold'' in Norway). Both singles received great radio attention in Norway.

From all this success, [[United Kingdom|British]] [[Talent manager|music manager]] and former [[Chief executive officer|CEO]] of [[RCA Music Group|RCA Label Group (UK)]] [[Craig Logan]] became interested by A-Lee and brought his music to [[RCA Records|RCA]]/[[Sony Music Entertainment|Sony Music UK]] which proposed A-Lee a worldwide [[record contract]] in August 2011. Unfortunately, due to the restructuring of [[Sony Music Entertainment|Sony Music]] in September and October 2011,<ref name="RCA restructuring">{{cite news
| title = RCA's Peter Edge, Tom Corson on the Shuttering of Jive, J and Arista
| url = http://www.billboard.biz/bbbiz/industry/record-labels/rca-s-peter-edge-tom-corson-on-the-shuttering-1005394732.story
| publisher = Billboard.no
| date = 7 October 2011
| accessdate = 13 November 2012}}</ref> the contract could not be finalized.

===2012: album ''Forever Lost''===
In January 2012, A-Lee and his [[record label|label]] EE Records signed a [[Music licensing|licensing deal]] with [[Sony Music Entertainment|Sony Music]]<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Official Website Biography"/> and started working on his second album ''[[Forever Lost (A-Lee album)|Forever Lost]]''. A-Lee also had two nominations at the [[NRJ Music Award|NRJ Music Awards 2012]]: ''Best Norwegian Act'' and ''Best Norwegian Hit''.<ref name="A-Lee NRJ Awards 2012">{{cite news
 |title=NRJ Music Awards 2012&nbsp;– stem på dine favoritter! 
 |url=http://www.nrj.no/artikkel/nrj-music-awards-2012-stem-pa-dine-favoritter/ 
 |publisher=NRJ.no 
 |date=25 December 2011 
 |accessdate=13 November 2012 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120905035723/http://www.nrj.no:80/artikkel/nrj-music-awards-2012-stem-pa-dine-favoritter/ 
 |archivedate=5 September 2012 
 |df= 
}}</ref>

In May 2012, A-Lee released his summer smash hit "Feelgood" which went ''[[Music recording sales certification|4x Platinum]]'' in Norway.<ref name="A-Lee Official Website Biography"/><ref name="A-Lee Signs To BMG"/><ref name="A-Lee IFPI NOR"/> In June 2012, he was part of the ''VG-Lista Tour 2012''<ref name="A-Lee VG-Lista Tour 2012"/> where he performed his summer smash hit "Feelgood" in particular at the huge annual show [[VG-Lista]] [[Rådhusplassen]] in Oslo<ref name="A-Lee VG-Lista 2012 Oslo YouTube"/> where he attended for the third consecutive year after 2010 and 2011.<ref name="A-Lee VG-Lista 2010 Oslo YouTube"/><ref name="A-Lee VG-Lista 2011 Oslo YouTube"/> "Feelgood" reached also ''#1'' for most played [[music video]] of [[The Voice TV Norway]] in 2012 and ''#3'' for most played song of [[The Voice (radio station)|The Voice radio Norway]] in 2012.

In September 2012, A-Lee released his single "Over You" (''[[Music recording sales certification|Platinum]]'' in Norway) produced by Martin K (signed on [[StarRoc]], the label owned by [[Jay-Z]] and [[Stargate (production team)|Stargate]]).<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Over You 730.no">{{cite news
| title = Sommerflørten er over
| url = http://www.730.no/musicitem.aspx?newsId=11529
| publisher = 730.no
| date = 31 August 2012
| accessdate = 13 November 2012}}</ref> Famous [[Netherlands|Dutch]] [[Model (profession)|model]] Sylvia Geersen who was the [[runner-up]] of [[Netherlands|Dutch]] reality television show [[Holland's Next Top Model]]<ref name="A-Lee Over You TV2 article">{{cite news
| title = Leker seg med toppmodell i ny musikkvideo
| url = http://www.tv2.no/underholdning/coverme/leker-seg-med-toppmodell-i-ny-musikkvideo-3893760.html
| publisher = TV2.no
| date = 8 October 2012
| accessdate = 13 November 2012}}</ref><ref name="A-Lee Over You 730.no2">{{cite news
| title = A-Lee i syden med Top Model-jente
| url = http://www.730.no/musicitem.aspx?newsId=11827
| publisher = 730.no
| date = 8 October 2012
| accessdate = 13 November 2012}}</ref> is playing the main role in the "Over You" [[music video]].<ref name="A-Lee Over You Music Video YouTube">{{cite news
| title = A-LEE&nbsp;— OVER YOU (OFFICIAL MUSIC VIDEO)
| url = https://www.youtube.com/watch?v=U7F86g1E4V0
| publisher = YouTube
| date = 8 October 2012
| accessdate = 13 November 2012}}</ref> "Over You" reached also ''#6'' for most played [[music video]] of [[The Voice TV Norway]] in 2012.

In October 2012, A-Lee released his new album ''[[Forever Lost (A-Lee album)|Forever Lost]]'',<ref name="A-Lee's article in Norwegian newspaper Dagbladet"/><ref name="A-Lee Signs To BMG"/> which is full of [[Hit single|hits]] produced by Martin K (''[[Jessie J]]'', ''[[Lionel Richie]]'', ''[[Sugababes]]''), Bernt Rune Stray (''[[Rihanna]]'', ''[[Ne-Yo]]'', ''[[Whitney Houston]]''), BPM (''[[Sarah Skaalum Jørgensen|Sarah from X-Factor Denmark]]''), Thomas Eriksen (''[[Aaliyah]]'', ''[[Franz Ferdinand (band)|Franz Ferdinand]]'', ''[[The Saturdays]]''), Slipmats (''[[Jennifer Hudson]]'') and Ground Rules.<ref name="A-Lee Official Website Biography"/>

Following the ''[[Forever Lost (A-Lee album)|Forever Lost]]'' release, A-Lee signed a worldwide [[Publishing contract|publishing deal]] with the successful company [[BMG Rights Management|BMG Chrysalis]].<ref name="A-Lee Signs To BMG"/><ref name="A-Lee Artist Page To BMG"/> A-Lee was also part of the TV show ''Cover Me''<ref name="A-Lee Cover Me TV2 announcement">{{cite news
| title = Disse artistene er med i "Cover me"
| url = http://www.tv2.no/underholdning/coverme/disse-artistene-er-med-i-cover-me-3852855.html
| publisher = TV2.no
| date = 19 August 2012
| accessdate = 13 November 2012}}</ref> broadcast in October 2012 on Norwegian television channel [[TV 2 (Norway)|TV2]]. He performed "People Get Moving" a song he especially wrote for the TV show.<ref name="A-Lee People Get Moving Cover Me">{{cite news
| title = A-LEE&nbsp;— PEOPLE GET MOVING (COVER ME TV2)
| url = https://www.youtube.com/watch?v=fUveLwIGC_E
| publisher = YouTube
| date = 14 October 2012
| accessdate = 13 November 2012}}</ref>

===2013-present: ''Flashy'', ''New Day'' & ''Rainbow''===
In January 2013, A-Lee is, for the second consecutive year, nominated 2 times for the [[NRJ Music Award|NRJ Music Awards 2013]] in the categories ''Best Norwegian Act'' and ''Best Norwegian Hit'' for his successful smash single "Feelgood".<ref name="A-Lee Signs To BMG"/><ref name="A-Lee NRJ Awards 2013">{{cite news
| title = NRJ Music Awards 2013&nbsp;– stem på dine favoritter!
| url = http://www.nrj.no/artikkel/nrj-music-awards-2013-stem-pa-dine-favoritter/
| publisher = NRJ.no
| date = 29 December 2012
| accessdate = 29 December 2012}}</ref> A-Lee attended the edition 2013 of [[Midem|MIDEM]] which is held been annually in and around the [[Palais des Festivals et des Congrès]] in [[Cannes]], [[France]].

In August 2013, A-Lee released his new single "Flashy" featured by Swedish pop artist [[Eric Saade]] who also released the song on his album ''[[Forgive Me (Eric Saade album)|Forgive Me]]'' which reached ''#1'' at Swedish Official Charts [[Sverigetopplistan]].

In May 2014, A-Lee released the single "New Day" and its music video.<ref name="A-Lee New Day Music Video YouTube">{{cite news
| title = A-Lee&nbsp;- New Day
| url = https://www.youtube.com/watch?v=Vlq8deeUj4A
| publisher = YouTube
| date = 21 May 2014
| accessdate = 21 November 2014}}</ref>

In May 2015, A-Lee released his new single "Rainbow" and its music video.<ref name="A-Lee Rainbow Music Video YouTube">{{cite news
| title = A-Lee&nbsp;- Rainbow
| url = https://www.youtube.com/watch?v=fC31UsO-od0
| publisher = YouTube
| date = 29 June 2015
| accessdate = 29 June 2015}}</ref>

==Discography==

===Studio albums===
{| class="wikitable" style="text-align:center;"
|-
! scope="col" rowspan="2" style="width:11em;" | Title
! scope="col" rowspan="2" style="width:25em;" | Album details
! scope="col" colspan="1" style="width:11em;" | Peak chart positions
! scope="col" rowspan="2" style="width:11em;" | [[List of music recording certifications|Certifications]]
|-
! scope="col" style="width:3em;font-size:90%;"|[[VG-lista|NOR]]<ref name="A-Lee Charts VG-Lista Official Website"/><ref name="A-Lee NorwegianCharts.com Website"/>
|-
! scope="row" | '''''Missing'''''
|
* Released: 11 October 2010
* Label: EE Records
* Formats: [[Compact Disc|CD]], [[Music download|digital download]]
|&nbsp;—
|
*&nbsp;—
|-
! scope="row" | [[Forever Lost (A-Lee album)|'''''Forever Lost''''']]
|
* Released: 5 October 2012
* Label: EE Records, [[Columbia Records|Columbia]]/[[Sony Music Entertainment|Sony Music]]
* Formats: [[Compact Disc|CD]], [[Music download|digital download]], streaming
|&nbsp;—
|
*&nbsp;—
|-
|}

===Singles===
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
! scope="col" rowspan="2" style="width:16em;" | Title
! scope="col" rowspan="2" style="width:3em;" | Year
! scope="col" colspan="1" | Peak chart positions
! scope="col" rowspan="2" style="width:15em;" | [[List of music recording certifications|Certifications]]
! scope="col" rowspan="2" | Album
|-
! scope="col" style="width:3em;font-size:90%;"|[[VG-lista|NOR]]<ref name="A-Lee Charts VG-Lista Official Website"/><ref name="A-Lee NorwegianCharts.com Website"/>
|-
! scope="row" | [[World So Cold (A-Lee song)|"World So Cold"]]<br><span style="font-size:85%;">(featuring Marcus Only)</span>
|rowspan="1"|2010
| 12
|
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> Platinum
|rowspan="6"| ''[[Forever Lost (A-Lee album)|Forever Lost]]''
|-
! scope="row" | [[The One (A-Lee song)|"The One"]]
|rowspan="3"|2011
| 7
|
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> 2x Platinum
|-
! scope="row" | [[Hear the Crowd|"Hear The Crowd"]]
|&nbsp;—
|
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> Gold
|-
! scope="row" | "[[Before My Eyes]]"
|&nbsp;—
| 
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> Gold
|-
! scope="row" | "Feelgood"<br><span style="font-size:85%;">(featuring [[Elisabeth Carew]])</span>
|rowspan="3"|2012
|&nbsp;—
| 
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> 4x Platinum
|-
! scope="row" | "Over You"
|&nbsp;—
|
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> Platinum
|-
! scope="row" | "People Get Moving"
|&nbsp;—
| 
*&nbsp;—
|rowspan="1"| Non-album single
|-
! scope="row" | "Like You"
|rowspan="2"|2013
|&nbsp;—
| 
*&nbsp;—
|rowspan="1"| ''[[Forever Lost (A-Lee album)|Forever Lost]]''
|-
! scope="row" | "Flashy"<br><span style="font-size:85%;">(featuring [[Eric Saade]])</span>
|&nbsp;—
| 
* [[International Federation of the Phonographic Industry|IFPI]] Norway:<ref name="A-Lee IFPI NOR"/> Gold
|rowspan="1"| ''[[Forgive Me (Eric Saade album)|Forgive Me]]''<span style="font-size:85%;"> (album of Eric Saade)</span>
|-
! scope="row" | "New Day"
|rowspan="1"|2014
|&nbsp;—
| 
*&nbsp;—
|rowspan="1"| Non-album single
|-
! scope="row" | "Rainbow"
|rowspan="1"|2015
|&nbsp;—
| 
*&nbsp;—
|rowspan="1"| Non-album single
|-

|}

==Music videos==
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
!scope="col"|Title
!scope="col"|Year
!scope="col"|Director(s)
|-
! scope="row"| "[[Before My Eyes]]"
| 2012
| rowspan="1"|—<ref name="A-Lee Before My Eyes Music Video YouTube">{{cite news
| title = A-LEE&nbsp;— BEFORE MY EYES (OFFICIAL MUSIC VIDEO)
| url = https://www.youtube.com/watch?v=aXkS2Zys1zI
| publisher = YouTube
| date = 14 January 2012
| accessdate = 13 November 2012}}</ref>
|-
! scope="row"| "Feelgood" <small>(featuring Elisabeth Carew)</small>
| 2012
| rowspan="1"|Pål Erik Helgerud<ref name="A-Lee Feelgood Music Video YouTube">{{cite news
| title = A-LEE&nbsp;— FEELGOOD (FEAT ELISABETH CAREW) OFFICIAL MUSIC VIDEO
| url = https://www.youtube.com/watch?v=DgAsRUqHA1g
| publisher = YouTube
| date = 14 June 2012
| accessdate = 13 November 2012}}</ref>
|-
! scope="row"| "Over You"
| 2012
| rowspan="1"|Joon Brandt<ref name="A-Lee Over You Music Video YouTube"/>
|-
! scope="row"| "New Day"
| 2014
| rowspan="1"|Frederic Esnault<ref name="A-Lee New Day Music Video YouTube"/>
|-
! scope="row"| "Rainbow"
| 2015
| rowspan="1"|—<ref name="A-Lee Rainbow Music Video YouTube"/>
|}

==Awards and nominations==
{| class="wikitable"
|-
!Year
!Event
!Prize
!Nominated work
!Result
!Ref
|-
|rowspan="1"|2005
||[[by:Larm]]
|Audience Award
|
|{{won}}
|<ref name="A-Lee by:larm audience award"/><ref name="A-Lee by:larm audience award 2"/>
|-
|rowspan="1"|2005
||[[NRK P3]] Urørt
|Årets Urørt
|"What Do You Mean" <span style="font-size:85%;">(featuring Chicosepoy)</span>
|{{nom}}
|<ref name="A-Lee Årets Urørt"/>
|-
|rowspan="1"|2012
||[[NRJ Music Award|NRJ Music Awards 2012]]
|Best Norwegian Act
|
|{{nom}}
|<ref name="A-Lee NRJ Awards 2012"/>
|-
|rowspan="1"|2012
||[[NRJ Music Award|NRJ Music Awards 2012]]
|Best Norwegian Hit
|"The One"
|{{nom}}
|<ref name="A-Lee NRJ Awards 2012"/>
|-
|rowspan="1"|2013
||[[NRJ Music Award|NRJ Music Awards 2013]]
|Best Norwegian Act
|
|{{nom}}
|<ref name="A-Lee NRJ Awards 2013"/>
|-
|rowspan="1"|2013
||[[NRJ Music Award|NRJ Music Awards 2013]]
|Best Norwegian Hit
|"Feelgood" <span style="font-size:85%;">(featuring Elisabeth Carew)</span>
|{{nom}}
|<ref name="A-Lee NRJ Awards 2013"/>
|-
|}

==External links==
* [http://www.aleemusic.com A-Lee Official Site]
* [https://www.facebook.com/aleemusicofficial A-Lee Official Facebook]
* [https://www.twitter.com/aleemusic A-Lee Official Twitter]
* [http://www.instagram.com/aleeofficial A-Lee Official Instagram]
* [https://www.youtube.com/aleemusicdottcom A-Lee YouTube Channel]
* [http://www.itunes.com/a-lee A-Lee iTunes Page]

==References==
{{reflist}}

[[Category:Musicians from Gjøvik]]
[[Category:Norwegian male singers]]
[[Category:Norwegian songwriters]]
[[Category:English-language singers of Norway]]
[[Category:Living people]]
[[Category:1988 births]]
[[Category:Norwegian expatriates in the United States]]
[[Category:Norwegian people of Iranian descent]]