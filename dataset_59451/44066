{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
{{Infobox airline
| airline          = Bristow Helicopters Limited
| logo             = Bristow helicopters signage.svg
| logo_size        = 200px
| fleet_size       = 490
| destinations     = 
| IATA             = UH
| ICAO             = BHL
| callsign         = BRISTOW
| parent           = The Bristow Group
| founded          = 1955
| headquarters     = [[Aberdeen]], [[Scotland]], [[United Kingdom]]
| operations base  = [[Aberdeen Airport]]
| hubs             = 
| focus_cities     = 
| key_people       = [[Jonathan E. Baliff]] {{small|(CEO)}}
| revenue          = {{profit}}[[United States Dollar|US$]]1.67B {{small|(''FY 2014'')}}<ref name = 2014IncomeStatmenent>[[wikinvest:stock/BRISTOW GROUP (BRS)/Data/Income Statement|BRISTOW GROUP (BRS) annual SEC income statement filing via Wikinvest.]]</ref>
| operating_income = {{profit}}[[United States Dollar|US$]]177M {{small|(''FY 2014'')}}<ref name = 2014IncomeStatmenent/>
| net_income       = {{profit}}[[United States Dollar|US$]]187M {{small|(''FY 2014'')}}<ref name = 2014IncomeStatmenent/>
| assets           = {{increase}}[[United States Dollar|US$]]3.54{{small|(''FY 2014'')}}<ref name = 2014BalanceSheet>[[wikinvest:stock/BRISTOW GROUP (BRS)/Data/Balance Sheet|BRISTOW GROUP (BRS) annual SEC balance sheet filing via Wikinvest.]]</ref>
| equity           = {{increase}}[[United States Dollar|US$]]1.77B {{small|(''FY 2014'')}}<ref name = 2014BalanceSheet/>
| website          = {{URL|bristowgroup.com}}
| traded_as        = {{NYSE|BRS}}<br>[[S&P 600|S&P 600 component]]
}}

'''Bristow Helicopters Limited''' is a [[United Kingdom|British]] civil helicopter operator originally based at [[Aberdeen Airport]], [[Scotland]] which is now part of the U.S. based '''Bristow Group''' ({{NYSE|BRS}}, [[S&P 600|S&P 600 component]]) which in turn has its corporate headquarters in [[Houston, Texas]].

Bristow Helicopters Limited holds a [[Civil Aviation Authority (United Kingdom)|United Kingdom Civil Aviation Authority]] Type A Operating Licence, it is permitted to carry passengers, cargo and mail on aircraft with  20 seats or more.<ref>[http://www.caa.co.uk/default.aspx?catid=183&pagetype=90&pageid=340 UK CAA - Operating Licence Holders]</ref>  The U.S. division of Bristow is a [[Federal Aviation Administration]] ([[FAA]]) approved Part 135 air carrier.

==History==
Bristow Helicopters Limited was established in June 1955 by [[Alan Bristow]].

[[File:Westland Wessex 60 G-ATBZ Bristow N.Denes 07.06.70 edited-2.jpg|thumb|right|[[Westland Wessex|Westland Wessex 60]] helicopter of Bristows in 1970]] 
[[File:Hiller UH-12C G-ASOE Bristow MW 14.06.68 edited-2.jpg|thumb|right|[[Hiller UH-12]]C used by Bristow to train Army Air Corps pilots.]]
From 17 February 1965 and onwards, it operated the [[Westland Wessex 60]] ten-seat helicopter in support of [[North Sea Oil]] industry off-shore installations.<ref name="vert2015-04">{{cite news|first=Kenneth I. |last=Swartz |url=http://www.verticalmag.com/news/article/31306 |title=Setting the Standard |work=Vertical Magazine |date=16 April 2015 |accessdate=18 April 2015 |archive-url=https://web.archive.org/web/20150418013604/http://www.verticalmag.com/news/article/31306 |archive-date=2015-04-18 |deadurl=yes |df= }}</ref>

During the late 1960s, Bristow operated a fleet of [[Hiller UH-12]] training helicopters based at [[AAC Middle Wallop]] which were used to train flight crews for the UK [[Army Air Corps (United Kingdom)|Army Air Corps]].

In 1985, it was acquired by [[British and Commonwealth Holdings]] plc.<ref>[http://www.alacrastore.com/storecontent/Thomson_M&A/British_Commonwealth_Hldgs_acquires_remaining_interest_in_Bristow_Helicopter_Group_from_British_Commonwealth_Hldgs-237505040 British & Commonwealth acquires remaining interest in Bristow Helicopter Group]</ref>

In 1996, Bristow Helicopters was purchased by '''Offshore Logistics''', an [[United States|American]] offshore helicopter operator which operated as '''Air Logistics''' in the U.S. [[Gulf of Mexico]] and [[Alaska]], and was structured as a [[reverse takeover]]. The group now operates and maintains a global fleet of over 400 aircraft.  In 2006 Offshore Logistics re-branded itself as 'The [[Bristow Group]]'.

The Bristow Group expanded their portfolio in April 2007 with the purchase of Helicopter Adventures, a Florida-based flight school, Helicopter Adventures was subsequently renamed '''Bristow Academy'''.<ref>http://www.bristowgroup.com/pdf/Bristow_Corporate_Newsletter_May_2007.pdf</ref><ref>Smith, Dale. "[http://bristowgroup.com/_assets/filer/2011/12/21/bristow-academy-featured-in-rotor-wing-magazine-july-2011.pdf Training Profile: Bristow Academy]" page 27-31. ''Rotor&Wing'', July 2011.</ref> The deal also provided the Bristow Group with the world's largest civilian fleet of Schweizer aircraft.<ref>http://www.heli.com/helicopter-sales/</ref>

===Bristow (formerly Air Logistics)===

In January 2010, Bristow announced the retirement of the Air Logistics name and [[Gulf of Mexico]] operations would operate under the name Bristow. Bristow provides helicopter services, maintenance and other support services to the oil and gas industry. It operates more than 170 single and twin-turbine helicopters in the United States. These receive support, materials and operational assistance from its regional headquarters and primary maintenance facility located at the [[Acadiana Regional Airport]] in [[New Iberia, Louisiana]].

===[[Eastern Airways]] and [[Airnorth]]===

Fixed Wing
Bristow has controlling interests in [[Eastern Airways]] which is a regional airline based in the U.K. operating fixed wing regional jet and turboprop aircraft and [[Airnorth]], also a regional airline, based in Australia operating fixed wing regional jet and turboprop aircraft. Both Eastern Airways and Airnorth operate scheduled passenger services, shuttle flights for oil and gas industry personnel, and charter services.<ref name="bristowgroup.com">http://www.bristowgroup.com</ref>

==Joint ventures==
In addition to its wholly owned international operations, Bristow Group maintains service agreements and equity interests in helicopter operators in Brazil, Canada, Colombia, Egypt, Kazakhstan, Turkmenistan, Mexico, Norway and Russia (Sakhalin) and the United Kingdom. This allows Bristow to extend its range of services into new and developing oil and gas markets and helps provide a lower cost structure in some operating areas. Partners include:
[[File:Norsk Helikopter.png|thumb|Norsk Helikopter-scheme]]

* [[Cougar Helicopters]]<ref>http://www.cougar.ca, 9/4/2012 press release:  Bristow to acquire assets and minority equity interest in Cougar Helicopters</ref>
*[[Helicopteros Nacionales de Colombia]], Helicol S.A., Colombia
*[[Petroleum Air Services]], Egypt
*[[Atyrau Bristow Airways Services]] (ABAS), [[Kazakhstan]]
*[[Turkmenistan Helicopters Limited]], [[Turkmenistan]]
*Heliservicio Campeche, [[Mexico]]
*[[Norsk Helikopter]], [[Norway]] - now Bristow Norway
*[[Sakhalin Bristow Air Services]] AKA [http://www.aviashelf.com/eng/about/ Aviashelf], [[Sakhalin]], [[Russia]]
*[[FBH Limited]], [[UK]]

Although not a joint venture, in 2015 Bristow and [[AgustaWestland]] agreed to develop offshore and search and rescue capabilities for the [[AgustaWestland AW609|AW609]] [[tiltrotor]].<ref>[http://www.verticalmag.com/news/article/AgustaWestlandandBristowSignExclusivePlatformDevelopmentAgre AgustaWestland and Bristow Sign Exclusive Platform Development Agreement for the AW609 Tiltrotor Program] ''AgustaWestland'' PR, 3 March 2015.</ref> This could simplify a typical trip from [[Clapham Common]] to an oil rig by using just one aircraft.<ref name=vmag2015>Johnson, Oliver. "[http://www.verticalmag.com/news/article/ChartingBristowsCourse Charting Bristow's Course]" ''Vertical'', August 2015. [https://web.archive.org/web/20150826205022/http://www.verticalmag.com/news/article/ChartingBristowsCourse Archive]</ref> Bristow intends to order more than 10 tiltrotors.<ref name="ain2015-03-03b">{{cite news |first=Mark |last=Huber |url=http://www.ainonline.com/aviation-news/business-aviation/2015-03-03/bristow-commits-being-partner-and-customer-aw609-civil-tiltrotor |title=Bristow Commits To Being Partner and Customer for AW609 Civil Tiltrotor  |work=Aviation International News |date=3 March 2015 |accessdate=5 March 2015 |deadurl=no}}</ref>

==Military==
The [[Search and Rescue Training Unit]] at [[RAF Valley]] is a detachment of the [[Defence Helicopter Flying School]] at [[RAF Shawbury]], from which its aircraft are distinguished by their flotation bags, rescue winches and cable cutters above the cockpit roof. The aircraft are maintained to EASA standards but are military registered allowing them to operate outside civilian flight restrictions.

All the Defence Helicopter Flying School Helicopters and Synthetic Training Equipment are owned by [http://www.fbheliservices.com/ FB Heliservices], a consortium of Bristow Helicopters and FR Aviation, who provide 40% of the instructional staff, all the ground school and simulator staff, carry out all maintenance and provide support services.

==Search and Rescue==
[[File:HM Coast Guard G-BDIJ.png|thumb|Sikorsky S-61N operating for [[HM Coastguard]]]]
[[File:Vliegveld De Kooy-Helikopters.jpg|thumb| Bristow Helicopter (G-JSAR) at [[De Kooy Airfield]]. G-JSAR was a Search and Rescue helicopter.]]
Bristow helicopters operated Sikorsky S-61N helicopters on behalf of [[Her Majesty's Coastguard]], the [[United Kingdom]]'s [[Coast Guard]], until July 2007 after which there was a 12-month transitional period whilst [[CHC Helicopter]] took over the contract replacing the S-61N with new helicopters.

Bristow operated four dedicated Search and Rescue (SAR) sites in the UK, on behalf of the Coast Guard Service. The units were located at [[Isle of Portland|Portland]] (EGDP) and [[Lee-on-Solent]] (EGHF) on the south coast of England, at [[Stornoway]] (EGPO) in the [[Outer Hebrides]], and at [[Sumburgh Airport|Sumburgh]] (EGPB) in the [[Shetland Isles]].

Northern [[North Sea]] services operated from [[Aberdeen]] (EGPD), [[Scatsta]] (EGPM) and [[Stavanger]] (ENZV).

Southern North Sea services operate from Norwich (EGSH), Humberside (EGNJ) and [[Den Helder]] (EHKD) with its support organisation based at [[Redhill Aerodrome|Redhill]] (EGKR).

Bristow S61N's were responsible to carry out SAR tasks, operating from [[Den Helder Airport]] on behalf of the [[oil and gas industry]].

On 26 March 2013 Bristow was awarded a 10-year contract to operate the search and rescue operations in the United Kingdom, at the time being provided by CHC Helicopter (on behalf of Her Majesty's Coastguard), the Royal Air Force and the Royal Navy.<ref>{{cite news |title=Bristow Group to take over UK search and rescue from RAF |url=http://www.bbc.co.uk/news/uk-21934077 |newspaper=BBC News |date=26 March 2013 |accessdate=30 March 2013}}</ref> Bristow is currently operating [[AgustaWestland AW139]] and [[Sikorsky S-92]] helicopters in support of this contract. They were originally planning on using AW189 helicopters but the procurement programme has been delayed, resulting in AAR AIrlift Group claiming the first civilian registered SAR AW189, based in the Falkland Islands, in support of the UK Government <ref name="bristowgroup.com"/><ref>http://www.aarcorp.com/aar-commences-search-and-rescue-operations-for-uk-ministry-of-defence-in-the-falkland-islands/</ref><ref>https://www.flightglobal.com/news/articles/bristow-still-waiting-on-aw189-sar-introduction-416808/</ref>

==Fleet==
{{main article|Bristow Helicopters Fleet}}

Bristow operates a large fleet of over 450 helicopters and aircraft, which includes unconsolidated affiliates and joint venture partners.<ref>http://www.bristowgroup.com/about-bristow/helicopter-fleet/</ref> Bristow intends to reduce fleet variety from 24 helicopter types to six.<ref name=vmag2015/> For subsidiaries Eastern Airways and Airnorth fixed wing jet and turboprop aircraft, see [[Eastern Airways]] and [[Airnorth]].

==Incidents==
*5N-ABQ, a Scottish Aviation Twin Pioneer Srs1, crashed on 4 April 1967 in Nigeria during a single engine approach.
*[[G-ASWI North Sea ditching]] - On 13 August 1981 a Westland Wessex 60 helicopter lost power to the main rotor gearbox, going out of control during the ensuing [[Autorotation (helicopter)|autorotation]]. The flight was carrying 11 gas workers from the Leman gas field to [[Bacton, Norfolk]]. All people on board were lost.<ref>http://www.aaib.gov.uk/cms_resources/4-1983%20G-ASWI.pdf Report No: 4/1983. Report on the accident to Westland Wessex 60, G-ASWI, 12 miles ENE of Bacton, Norfolk on 13 August 1981</ref><ref>
http://www.aaib.gov.uk/cms_resources/4%2D1983%20G%2DASWI%20Append%2Epdf 4/1983 Westland Wessex 60, G-ASWI Appendices</ref>
*On 4 July 1983, Bristow Helicopters AS332L Super Puma (G-TIGD) crashed on landing at Aberdeen. During the approach to Aberdeen from the North Hutton platform, a loud bang was heard, followed by severe vibration. A PAN call was made to ATC by the crew. Shortly before landing control was lost and the helicopter struck the runway heavily on its side. 10 of 16 passengers received serious injuries. A tail boom panel had become detached in flight and damaged all five tail rotor blades. The resulting imbalance to the tail rotor assembly led to the separation of this unit and subsequent loss of control.
*G-BJJR a Bell 212 that crashed with the loss of two crew on approach to the Cecil Provine in 1984.
*VR-BIG Aerospatiale SA-330J Puma 5 December 1991 in [[Mermaid Sound]], [[Dampier, Western Australia]], after a pick-up from departing [[LNG]] tanker in night VFR conditions, entered vortex ring state and ditched. Stayed afloat for over 2 hrs.
*G-TIGH on 14 March 1992 at 1950 a Bristow's Tiger (Super Puma) ferrying passengers from the Cormorant Alpha to the flotel Safe Supporter, lost altitude and crashed. Of the two crew and 14 passengers on board, one crew member and ten passengers were lost.
*G-TIGK on [[Bristow Flight 56C]] between Aberdeen and oil rigs in the North Sea. On 19 January 1995 the AS 332L Super Puma helicopter was struck by lightning. The flight was carrying 16 oil workers from Aberdeen to an oil platform at the Brae oilfield. All people on board survived.
*[[2002 Bristow Helicopters Sikorsky S-76A crash|G-BJVX North Sea Crash]] - G-BJVX, a commercial Sikorsky S-76A helicopter operated by Norwich-based Bristow Helicopters, crashed in the evening of 16 July 2002 in the southern North Sea while it was making a ten-minute flight between the gas production platform Clipper and the drilling rig Global Santa Fe Monarch, after which it was to return to Norwich Airport. The 22-year-old helicopter was flying at an altitude of about {{convert|320|ft|m}} when workers on the Global Santa Fe Monarch heard "a loud bang". No witnesses were actually watching the aircraft at the time, but some saw it dive steeply into the sea. A witness also reported seeing the helicopter's rotor head with rotor blades attached falling into the sea after the body of the helicopter had impacted. The accident caused the death of all those on board (two crew members and nine Shell workers as passengers). The body of the eleventh man has never been recovered.
*G-JSAR Eurocopter Super Puma [[Search and rescue|SAR]] - ditched in the North Sea on 22 November 2006. G-JSAR was operated from [[Den Helder Airport]] in the Netherlands on behalf of oil companies. All on board survived uninjured <ref>http://www.onderzoeksraad.nl/docs/rapporten/2006060e_2006137_G-JSAR_preliminary_report.pdf{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
*On 12 August 2015 a Sikorsky S76C+ with registration number 5N-BGD returning to Lagos from an offshore rig with ten passengers, all oil workers, crashed into the Lagos lagoon close to the {{convert|11.8|km|mi}} long third mainland bridge, the longest of three bridges connecting the Lagos mainland to the Island. The helicopter was five minutes away from landing at the Muritala Muhammed airport in Lagos. Four oil workers and the two crew died while six others were rescued alive.

==See also==
*[[Whirlwind (novel)]] - A novel by James Clavell, first published in 1986, which was closely inspired by the true struggle of Bristow Helicopters to escape the revolutionary forces and get their employees and equipment out of the unstable, deteriorating situation in Iran. Much of the story mirrors these and other contemporary events.
*[[Bristow Norway]] - (formerly Norsk Helikopter) is a Norwegian helicopter company that transports crew to oil installations in the North Sea.
*[[Petroleum Helicopters International]]

==References==
{{Reflist|30em}}

==Further reading==
*{{cite book|author1=Bristow, A. |author2=Malone, P.  |lastauthoramp=yes | title=Alan Bristow Helicopter Pioneer: The Autobiography | publisher=Pen & Sword Books | place=Barnsley, UK | year=2009 | isbn=978-1-84884-208-3}}

==External links==
{{commons category|Bristow Helicopters}}
Bristow sites:
*[http://www.bristowgroup.com/ Bristow Group]
*[http://www.bristowsar.com/ Bristow Search and Rescue]
*[http://www.helis.com/database/go/uk_bristow_helicopters_part_of_offshore_logisitcs_group.php Bristow page] at Helicopter History site
*[http://www.lidertaxiaereo.com.br/ lider Aviação]
Other sites:
*[http://www.telegraph.co.uk/news/obituaries/finance-obituaries/5245637/Alan-Bristow.html Alan Bristow]-Daily Telegraph obituaries
*[http://www.airliners.net/search/photo.search?search_field=datedesc&q=Bristow+Helicopters&sort_order=photo_id+desc&page=1&page_limit=15&sid=5d1ee600a9ccf48147c7fca2810e0e5a Lotsa pics at Airliners.net]

{{Airlines of the United Kingdom}}
{{helicopter airlines}}

[[Category:Bristow Helicopters| ]]
[[Category:1953 establishments in Scotland]]
[[Category:Airlines established in 1953]]
[[Category:Airlines of the United Kingdom]]
[[Category:Companies listed on the New York Stock Exchange]]
[[Category:Helicopter airlines]]
[[Category:British companies established in 1953]]