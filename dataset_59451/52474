{{Infobox journal
| cover =
| discipline   = [[Economics]], [[Political science|Political Science]], [[Law]]
| language     = English, German
| abbreviation = ORDO
| publisher    = DE GRUYTER
| country      = Germany
| history      = 1948–present
| website      = http://www.ordo-journal.com/en
| ISSN         = 0048-2129
}}
'''''ORDO — Jahrbuch für die Ordnung von Wirtschaft und Gesellschaft''''' (English: ''The Ordo Yearbook of Economic and Social Order'', most commonly referred to as ''Ordo Yearbook'', or simply as ''ORDO'') is a [[peer review|peer-reviewed]] [[academic journal]] established in 1948 by the German economists [[Walter Eucken]] and [[Franz Böhm]]. The journal focuses on the economic and political institutions governing modern society.

== History ==
The term [[ordoliberalism]] was coined echoing the journal's title.<ref>Hero Moeller (1950): "Liberalismus." in: ''Jahrbücher für Nationalökonomie und Statistik'', Vol. 162, pp. 214-238.</ref> Furthermore, the concept of [[social market economy]], being the main [[Economic system|economic model]] used in [[Western Europe|Western]] and [[Northern Europe]] during and after the [[Cold War]] era, has been developed nearly exclusively within ''ORDO''.<ref>{{cite journal|author=Carl J. Friedrich |year=1955|title=The Political Thought of Neo-Liberalism|journal=American Political Science Review |volume=49 |issue=2 |pages=509–525 |doi=10.2307/1951819 |publisher=American Political Science Association |jstor=1951819}}</ref><ref>Wolfgang Streeck and Kozo Yamamura (2005): ''The Origins of Nonliberal Capitalism: Germany and Japan in Comparison''. Cornell University Press</ref><ref>Knut Borchardt (1991): ''Perspectives on Modern German Economic History and Policy''. Cambridge University Press</ref>

Today, the journal's mission is to provide a forum of debate for scholars of [[interdisciplinarity|diverse disciplines]] such as [[economics]], [[law]], [[political science]], [[sociology]], and [[philosophy]].<ref>Frank Boenker, Agnès Labrousse, and Jean-Daniel Weisz (2000): "[https://books.google.com/books?id=E365H_XHyjsC&printsec=frontcover&hl=de#PPP1,M1 The Evolution of Ordoliberalism in the Light of the Ordo Yearbook. A Bibliometric Analysis]." in: A. Labrousse and J. D. Weisz (Eds.), ''Institutional Economics in France and Germany. German Ordoliberalism versus the French Regulation School'', Berlin: Springer, pp. 159-182.</ref> ''ORDO'' is published annually. Articles are published either in German or in English. ''ORDO'' also contains [[book review]]s.

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.ordo-journal.com/en}}

{{DEFAULTSORT:Ordo (Journal)}}

[[Category:Economics journals]]
[[Category:Political science journals]]
[[Category:Publications established in 1948]]
[[Category:Multilingual journals]]
[[Category:Annual journals]]