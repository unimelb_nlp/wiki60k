{{good article}}

{|{{Infobox ship begin |infobox caption=}} <!-- warship classes --><!-- caption: yes, nodab, or <caption text> -->
{{Infobox ship image
|Ship image=SMS Bussard Daressalam 1907-14.jpg
|Ship image size=300px
|Ship caption=SMS ''Bussard'' in [[Dar es Salaam]]
}}
{{Infobox ship class overview
|Builders=
|Operators=
|Class before={{sclass-|Schwalbe|cruiser|4}}
|Class after={{SMS|Gefion}}
|Subclasses=
|Built range=1888&ndash;1895
|In commission range=
|Total ships building=
|Total ships planned=
|Total ships completed=6
|Total ships cancelled=
|Total ships active=
|Total ships laid up=
|Total ships lost=3
|Total ships retired=
|Total ships scrapped=3
|Total ships preserved=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship type=[[Unprotected cruiser]]
|Ship displacement={{convert|1868|t|LT|abbr=on}}
|Ship length={{convert|82.60|m|ft|0|abbr=on}}
|Ship beam={{convert|12.50|m|ft|0|abbr=on}}
|Ship draft={{convert|4.45|m|ftin|abbr=on}}
|Ship propulsion= 3-cylinder triple expansion engines, 2 screws
|Ship speed={{convert|15.5|kn}}
|Ship range={{convert|2990|nmi|km|-1|abbr=on}} at {{convert|9|kn}}
|Ship complement=*9 officers
*152 enlisted men
|Ship armament=*8 × {{convert|10.5|cm|in|abbr=on}} guns
*5 × revolver cannon
*2 × {{convert|35|cm|in|abbr=on}} [[torpedo tube]]s
|Ship notes=
}}
|}

The '''''Bussard'' class''' of [[unprotected cruiser]]s were built for the German ''[[Kaiserliche Marine]]'' (Imperial Navy) in the late 1880s and early 1890s. The class comprised six ships: {{SMS|Bussard||2}}, the [[lead ship]], {{SMS|Falke||2}}, {{SMS|Seeadler||2}}, {{SMS|Cormoran|1892|2}}, {{SMS|Condor||2}}, and {{SMS|Geier||2}}. Designed for service in Germany's colonial empire, the class emphasized a long-range cruising radius and relatively heavy armament; they were also the last cruisers in the ''Kaiserliche Marine'' to be equipped with an auxiliary sailing rig. The ships were equipped with eight {{convert|10.5|cm|sp=us|adj=on}} guns.

All six ships served abroad for the majority of their careers, primarily in Africa and the south Pacific, where they assissted in the suppression of uprisings such as the [[Boxer Rebellion]] in China and the [[Sokehs Rebellion]] in the [[Caroline Islands]]. ''Cormoran'' participated in the seizure of the [[Kiautschou Bay concession]] in China in 1897, and ''Falke'' was involved in the [[Venezuela Crisis of 1902–03]]. ''Bussard'' and ''Falke'' were broken up for scrap in 1912, but the remaining four ships were still in service following the outbreak of [[World War I]] in August 1914.

''Cormoran'' was based in [[Tsingtao]] with unusable engines; she was scuttled in the harbor since she was no longer operational. ''Geier'' briefly operated against British shipping in the Pacific before having to put into [[Hawaii]] for internment by the then-neutral United States. After the United States entered the war in April 1917, she was seized and commissioned into the [[US Navy]] as USS ''Schurz''; she served as an escort until she was accidentally sunk following a collision with a freighter in June 1918. ''Seeadler'' and ''Condor'', meanwhile, had been converted into mine storage [[hulk (ship)|hulks]] after the start of the war. ''Seeadler'' was destroyed by an accidental explosion in 1917. ''Condor'' was the only member of the class to survive the war, and she was scrapped in 1921.

==Design==
The ''Bussard'' class was designed for service abroad in [[German colonial empire|Germany's colonial possessions]], and were an improvement over the preceding {{sclass-|Schwalbe|cruiser|4}} of [[unprotected cruiser]]s. The design for the ''Bussard'' class was prepared in 1888. The ships were significantly larger and faster than the ''Schwalbe'' class, but mounted the same battery of guns&mdash;though only ''Bussard'' carried the same type of guns&mdash;the rest carried a newer, [[quick-firing gun|quick-firing]] model.<ref>Gröner, pp. 93&ndash;97</ref><ref name=G253/> They were also the last cruiser class in the German ''[[Kaiserliche Marine]]'' (Imperial Navy) to be equipped with a sailing rig; the subsequent unprotected cruiser {{SMS|Gefion||2}} was entirely steam-powered.<ref>Gröner, pp. 97&ndash;99</ref>

===General characteristics===
The ships of the ''Bussard'' class all differed slightly in their characteristics. The first two ships, {{SMS|Bussard||2}} and {{SMS|Falke||2}}, were {{convert|79.62|m|ftin|sp=us}} [[length at the waterline|long at the waterline]] and {{convert|82.60|m|ft|0|abbr=on}} [[length overall|long overall]]. They had a [[beam (nautical)|beam]] of {{convert|12.50|m|ft|0|abbr=on}} and a [[draft (hull)|draft]] of {{convert|4.45|m|ftin|abbr=on}} forward and {{convert|5.63|m|ftin|abbr=on}} aft. They displaced {{convert|1559|t|LT|sp=us}} as designed and up to {{convert|1868|t|LT|abbr=on}} at full combat load. The next three ships, {{SMS|Seeadler||2}}, {{SMS|Condor||2}}, and {{SMS|Cormoran|1892|2}}, were {{convert|79.62|m|ftin|abbr=on}} long at the waterline and had the same overall length as their earlier [[sister ship]]s. They had a beam of {{convert|12.70|m|ftin|abbr=on}} and a draft of {{convert|4.42|m|ftin|abbr=on}} forward and {{convert|5.35|m|ftin|abbr=on}} aft. They displaced {{convert|1612|t|LT|abbr=on}} as designed and {{convert|1864|t|LT|abbr=on}} at full load. The last ship, ''Geier'', was 79.62&nbsp;m long at the waterline and {{convert|83.90|m|ftin|abbr=on}} long overall. She had a beam of {{convert|10.60|m|ftin|abbr=on}} and a draft of {{convert|4.74|m|ftin|abbr=on}} forward and {{convert|5.22|m|ftin|abbr=on}} aft.<ref name=G97>Gröner, p. 97</ref>

The cruisers' hulls were constructed with transverse steel frames with yellow pine planking up to the upper deck. A layer of [[Muntz metal]] sheathing covered the hull to protect the wood from [[shipworm]]. The stem and sternposts were constructed with steel and timber. A bronze [[naval ram]] was fitted at the bow. The hull was divided into ten [[watertight compartment]]s, and a [[double bottom]] was installed below the boiler rooms. The ships were good sea boats, but they rolled badly and the [[sponson]]s for the main guns caused severe vibration. Since ''Geier'' was laid down after the other five ships entered service, she was redesigned slightly to discard the sponsons, and so she did not suffer from bad vibration. They were very maneuverable, except for turns into the wind when steaming at low speed. The ships had a crew of 9&nbsp;officers and 152&nbsp;enlisted men. They carried a number of smaller boats, including one picket boat, one [[Cutter (boat)|cutter]], two [[yawl]]s, and two [[dinghy|dinghies]].<ref name=G97/>

===Propulsion===
[[File:StateLibQld 1 142447 Cormoran (ship).jpg|thumb|upright||''Cormoran'' in drydock in Sydney showing the arrangement of the screws and rudder]]
Their propulsion system consisted of two horizontal 3-cylinder triple-expansion [[steam engine]]s powered by four coal-fired cylindrical boilers; the engines were rated at {{convert|2800|ihp|lk=in}} and were placed in their own [[engine room]]s. The engines drove a pair of 3-bladed screws that were {{convert|3|m|ftin|abbr=on}} wide in diameter. The boilers were divided into two [[Fire room|boiler rooms]] and were trunked into a single funnel. The ships were fitted with an auxiliary [[Barquentine|schooner barque]] rig with a total surface area of {{convert|856|to|877|m2|abbr=on}}. Steering was controlled by a single [[rudder]]. Each ship was equipped with a pair of electricity generators with a combined output of {{convert|24|kW}} at 67&nbsp;[[volt]]s.<ref name=G97/>

The propulsion system provided a top speed of {{convert|15.5|kn|abbr=on}}, though all six ships exceeded their design speeds while on [[sea trials]], reaching between {{convert|15.7|to|16.9|kn}}. The ships carried between {{convert|170|to|205|MT|abbr=on}} of coal as designed, and they could accommodate up to {{convert|305|to|320|MT|abbr=on}} of coal using additional storage spaces. This provided a range of between {{convert|2990|to|3610|nmi|lk=in}} at {{convert|9|kn}}.<ref name=G97/>

===Armament===
The first ship was armed with eight [[10.5 cm K L/35|10.5&nbsp;cm K L/35]] guns in single pedestal mounts, supplied with 800&nbsp;rounds of ammunition in total. They had a range of {{convert|8200|m|ftin|abbr=on}}. The five subsequent ships were equipped with newer quick-firing SK L/35 versions of the 10.5&nbsp;cm guns. These newer guns also had a longer range, of {{convert|10800|m|ftin|abbr=on}}.<ref name=G97/> Two guns were placed side by side on the [[forecastle]], two on each [[broadside]]&mdash;one in a sponson and the other in a [[gun port]]&mdash;and two side by side on the [[quarterdeck]]. ''Geier'' did not use sponsons for the second pair of guns, instead simply mounting them on the upper deck. The gun armament was rounded out by five revolver cannon.<ref name=G253>Gardiner, p. 253</ref> The first five ships were also equipped with two {{convert|35|cm|abbr=on}} [[torpedo tube]]s, both of which were mounted on the deck. ''Geier'' instead had larger {{convert|45|cm|abbr=on}} torpedo tubes. Each ship carried five [[torpedo]]es.<ref name=G97/>

==Ships==
[[File:Bundesarchiv Bild 146-2008-0171, Kleiner Kreuzer "SMS Falke".jpg|thumb|''Falke'' in 1892]]
{| class="wikitable plainrowheaders"
! scope="col" | Name
! scope="col" | Builder<ref name=G97/>
! scope="col" | Laid down<ref name=G978/>
! scope="col" | Launched<ref name=G978/>
! scope="col" | Commissioned<ref name=G978/>
|-
! scope="row" | {{SMS|Bussard||2}}
| [[Kaiserliche Werft Danzig|''Kaiserliche Werft'']], [[Danzig]]
| 1888
| 23 January 1890
| 7 October 1890
|-
! scope="row" | {{SMS|Falke||2}}
| [[Kaiserliche Werft Kiel|''Kaiserliche Werft'']], [[Kiel]]
| 1890
| 4 April 1891
| 14 September 1891
|-
! scope="row" | {{SMS|Seeadler||2}}
| ''Kaiserliche Werft'', Danzig
| 1890
| 2 February 1892
| 17 August 1892
|-
! scope="row" | {{SMS|Condor||2}}
| [[Blohm & Voss]], [[Hamburg]]
| 1891
| 23 February 1892
| 9 December 1892
|-
! scope="row" | {{SMS|Cormoran|1892|2}}
| ''Kaiserliche Werft'', Danzig
| 1890
| 17 May 1892
| 25 July 1893
|-
! scope="row" | {{SMS|Geier||2}}
| [[Kaiserliche Werft Wilhelmshaven|''Kaiserliche Werft'']], [[Wilhelmshaven]]
| 1893
| 18 October 1894
| 24 October 1895
|}

{{clear}}

==Service history==

All six ships of the class spent the majority of their careers abroad, primarily in Germany's colonial possessions in Africa and the Pacific. ''Seeadler'' visited the United States in March 1893, along with the [[protected cruiser]] {{SMS|Kaiserin Augusta||2}}, for the belated celebrations for the 400th anniversary of [[Christopher Columbus|Columbus]]'s crossing of the Atlantic.<ref>Sondhaus, p. 206</ref> In July of that year, while assigned to the [[German East Asia Squadron|East Asia Division]], ''Bussard'' and ''Falke'' assisted in the suppression of [[Mata'afa Iosefo]]'s revolt in [[Samoa]], along with a British [[corvette]].<ref>Clowes et al., p. 414</ref> Throughout the 1890s, ''Seeadler'' was assigned to Germany's colonies in [[German East Africa|East Africa]] and [[German Southwest Africa|Southwest Africa]], where she suppressed local uprisings.<ref>Hildebrand, Röhr, & Steinmetz Vol. 7, pp. 152&ndash;153</ref>

[[File:Bundesarchiv Bild 134-C0105, SMS "Geier", Kleiner Kreuzer.jpg|thumb|''Geier'' in 1894|left]]
In November 1897, ''Cormoran'' took part in the seizure of the [[Kiautschou Bay concession]] in the [[Shandong Peninsula]] in [[Qing China]].<ref>Hildebrand, Röhr, & Steinmetz Vol. 2, p. 193</ref> ''Geier'' was present in the Caribbean during the [[Spanish–American War]] in 1898, though she took no active role in the conflict.<ref>Nunez, p. 76</ref> Between 1898 and 1900, ''Bussard'' and ''Seeadler'' were modernized in Germany.<ref name=G97/> In 1900, ''Seeadler'' participated in the suppression of the [[Boxer Rebellion]] in China, including a blockade of the Chinese coast.<ref>Hildebrand, Röhr, & Steinmetz Vol. 7, p. 154</ref> In December 1902, ''Falke'' and the [[protected cruiser]] {{SMS|Vineta|1897|2}} joined British forces in the [[Venezuela Crisis of 1902–03]] after Venezuelan forces seized a British merchant ship. The two cruisers helped British warships bombard Venezuelan coastal fortifications and blockade the coast.<ref>Marley, pp. 924–925</ref>

''Cormoran'' and ''Geier'' were modernized between 1907 and 1909; only ''Falke'' and ''Condor'' never returned for major dockyard work.<ref name=G97/> ''Condor'' and ''Cormoran'' suppressed the [[Sokehs Rebellion]] in the [[Caroline Islands]] in January 1911, along with the [[light cruiser]] {{SMS|Leipzig||2}}.<ref>Hildebrand, Röhr, & Steinmetz Vol. 2, p. 191</ref> In 1912, when the [[Second Balkan War]] broke out, ''Geier'' was stationed in the eastern [[Mediterranean Sea]] to observe the hostilities.<ref>Vego, p. 124</ref> Both ''Bussard'' and ''Falke'' were stricken from the naval register on 25 October 1912 and broken up the following year, at Hamburg and the ''Kaiserliche Werft'' in Danzig, respectively.<ref name=G978>Gröner, pp. 97&ndash;98</ref>

After the outbreak of [[World War I]] in August 1914, ''Seeadler'' was converted into a mine storage [[hulk (ship)|hulk]] in Wilhelmshaven. An accidental explosion in April 1917 destroyed the ship, and her wreck was never raised. ''Condor'' was used for the same purpose in Kiel; she survived the war and was broken up for scrap in 1921 in Hamburg. ''Cormoran'', still stationed in Tsingtao, was scuttled in the harbor because her engines were in poor condition.<ref name=G98>Gröner, p. 98</ref> ''Geier'' meanwhile operated against British merchant shipping in the Pacific following the onset of hostilities. By October, she was running low on coal and had been isolated from any sources of support; she therefore steamed to [[Hawaii]], where she was interned by the [[US Navy]]. After the United States declared war on Germany on 6 April 1917, she was seized and commissioned as USS ''Schurz'' for use as an escort vessel. She was sunk after colliding with a merchant ship in June 1918.<ref>{{cite web|title=Schurz|url=http://www.history.navy.mil/danfs/s7/schurz.htm|publisher=[[Naval History & Heritage Command]]|accessdate=5 May 2012}}</ref>

==Notes==
{{Reflist|colwidth=20em}}

==References==
*{{cite book|last1=Clowes|first1=William Laird|author1-link=William Laird Clowes|last2=Markham|first2=Clements|author2-link=Clements Markham|last3=Mahan|first3=Alfred Thayer|author-link3=Alfred Thayer Mahan|last4=Wilson|first4=Herbert Wrigley|author-link4=Herbert Wrigley Wilson|last5=Roosevelt|first5=Theodore|author-link5=Theodore Roosevelt|title=The Royal Navy: A History From the Earliest Times to the Death of Queen Victoria|volume=VII|publisher=Sampson Low, Marston and Company|location=London, UK|year=1903|oclc=1296915}}
*{{cite book|editor-last=Gardiner|editor-first=Robert|title=Conway's All the World's Fighting Ships 1860–1905|publisher=Conway Maritime Press|location=Greenwich|year=1979|isbn=0-8317-0302-4}}
*{{cite book|last=Gröner|first=Erich|title=German Warships 1815–1945|year=1990|publisher=Naval Institute Press|location=Annapolis, MD|isbn=0-87021-790-9}}
* {{cite book| last1 = Hildebrand| first1 = Hans H.| last2 = Röhr| first2 = Albert| last3 = Steinmetz| first3 = Hans-Otto| year = 1993| title = Die Deutschen Kriegsschiffe| volume = 2|location = [[Ratingen]]|publisher = Mundus Verlag|isbn = 978-3-8364-9743-5}}
* {{cite book| last1 = Hildebrand| first1 = Hans H.| last2 = Röhr| first2 = Albert| last3 = Steinmetz| first3 = Hans-Otto| year = 1993| title = Die Deutschen Kriegsschiffe|volume=7| publisher = Mundus Verlag| location = Ratingen, DE|asin=B003VHSRKE |asin-tld=de}}
*{{Cite book |last=Marley|first=David|title=Wars of the Americas: A Chronology of Armed Conflict in the Western Hemisphere, 1492 to the Present|year=2008|location=Santa Barbara, CA|publisher=ABC-CLIO|isbn=978-1-59884-100-8}}
*{{cite book|last=Nunez|first=Severo Gómez|title=The Spanish-American War: Blockades and Coast Defense|year=1899|publisher=Washington, Govt. Print. Off|location=Washington, DC}}
* {{cite book |last=Sondhaus|first=Lawrence|title=Preparing for Weltpolitik: German Sea Power Before the Tirpitz Era|year=1997|publisher=Naval Institute Press|location=Annapolis, MD|isbn=1-55750-745-7}}
*{{Cite book |last=Vego|first=Milan N.|title=Austro-Hungarian Naval Policy, 1904–14|year=1996|location=London|publisher=Frank Cass Publishers|isbn=978-0-7146-4209-3}}

{{Bussard class cruisers}}
{{German unprotected cruisers}}

[[Category:Bussard-class cruisers| ]]