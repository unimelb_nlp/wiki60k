{{Use dmy dates|date=February 2012}}
'''Tony Humphreys''' is an Irish educator and the author of eleven self-help books translated into 15 languages and available in 28 countries. They notably include ''A Different Kind of Teacher, Self-esteem: the Key to your Child's Future'' (1995), ''The Power of "Negative" Thinking'' (1996) and ''Children Feeling Good'' (1998).

Dr Humphreys is a parent-mentor and motivational speaker. He regularly writes about psychology and illness in the [[Irish Times]] and in the [[Irish Examiner]], where he has contributed a psychology column for over ten years. He believes strongly in the impact of unconditional love, or its absence, on physical health. He is the Director of a number of courses in Parent Mentoring, Interpersonal Communication and Relationship Studies at [[University College Cork]] and at [[All Hallows College]], Drumcondra, Dublin.

==Early Life and Education==
Upbringing left a significant psychological imprint on Tony Humphreys and on his philosophy. He was "constantly and unfavourably compared with his twin brother" and sought approval by assuming the role of carer for his invalided mother.<ref>
{{cite news
|author=Fergal Keane
|title=It's good for the soul to feel bad about yourself
|work=Sunday Independent
|date=2 April 2000
|url=http://www.independent.ie/unsorted/features/its-good-for-the-soul-to-feel-bad-about-yourself-513739.html}}</ref> He left school at fifteen<ref>
{{cite news
|work=Irish Times
|date=9 June 2004
|url=http://www.skoool.ie/skoool/examcentre_sc.asp?id=2932}}</ref> and joined a monastery at eighteen for a period of 7 years. He left the monastery a month before taking his vows "having lost all belief in Catholicism" immediately left his devoutly religious family, who had rejected him.<ref>
{{cite news
|last=Humphreys |first=Tony
|title=Heaven within
|work=Irish Examiner
|date=22 July 2011
|page=7}}</ref> He sought alternative spirituality and put himself through night school to obtain a degree and higher diploma in Physical Education, followed by an MA in 1977<ref>
{{citation
|author=Anthony Humphreys
|title=Cognitive, non-cognitive, biographic and demographic correlates of students' performance in first university examinations. Dissertation Thesis (M.A.)
|location=Cork
|publisher=Department of Applied Psychology, University College Cork
|year=1977}}</ref> and PhD in 1983<ref>
{{citation
|author=Anthony Humphreys
|title=An experimental investigation of hypnosis as an adjunct to the behavioural treatment of phobias. Thesis (Ph.D.)
|location=Birmingham
|publisher=Department of Clinical Psychology, University of Birmingham
|year=1983}}</ref> in Psychology.
 	
==Refrigerator mother theory==
	 
Tony Humphreys promotes the [[Refrigerator mother theory]] of the aetiology of autistic behaviours<ref>
{{cite news
|last=Humphreys |first=Tony
|title=Core connection: A diagnosis of Asperger's syndrome does little to help a child troubled by unhappy relationships
|work=Irish Examiner
|date=3 February 2012
|page=7}}</ref><ref>
{{cite news
|author=Brenda Power
|title=Peers may take issue with Dr Tony Humphreys’s claim to be "Ireland’s most influential psychologist"
|work=Sunday Times
|date=12 February 2012
|page=13}}</ref> first popularised by [[Bruno Bettelheim]], and does not believe in the existence of the clinical manifestations known as [[autism]] or [[autistic spectrum disorder]] (ASD). His published opinions have drawn vociferous complaints from the Minister for Health and Children, Dr [[James Reilly (Irish politician)|James Reilly]],<ref>
{{cite news
|author=Ronan McGreevy
|title=Reilly calls autism claims outrageous
|work=Irish Times
|date=14 February 2012
|url=http://www.irishtimes.com/newspaper/ireland/2012/0214/1224311746420.html}}</ref> from groups representing families affected by autism<ref>
{{cite news
|author=Kevin Whelan
|title=We don't need to defrost an assumption over 70 years old
|date=7 February 2012
|url=http://www.examiner.ie/ireland/we-dont-need-to-defrost-an-assumption-over-70-years-old-182912.html}}</ref> and a censure calling for retraction from the [[Psychological Society of Ireland]]<ref>
{{cite news
|author=Stephen Rogers
|title=Comments 'likely to cause upset'
|work=Irish Examiner
|date=10 February 2012
|url=http://www.irishexaminer.com/ireland/comments-likely-to-cause-upset-183321.html}}</ref> and his retirement by members of ICAAN (The Irish Council for Aspies and Autistic Networking).<ref>
{{cite web
|title=ICAAN calls for Dr Tony Humphreys' resignation
|url=http://www.facebook.com/photo.php?fbid=360506423967026&set=a.192864317397905.46791.175322202485450&type=1}}</ref> He continues to maintain that frigid parenting&nbsp;– conscious, subconscious or unconscious&nbsp;– is the root cause of 'autistic' behavioural issues in the family.<ref>
{{cite news
|title=Psychologist Humphreys shocked by Reaction to Autism Comments
|work=Irish Examiner
|date=11 February 2012
|url=http://www.examiner.ie/breakingnews/ireland/psychologist-humphreys-shocked-by-reaction-to-autism-comments-539463.html
|archiveurl=http://www.webcitation.org/65XsOMQoA
|archivedate=18 February 2012
|deadurl=no}}</ref> The Press Ombudsman of Ireland adjudicated that "the offence was not only widespread but grave, could have been interpreted as gratuitously provocative, and might have been avoided or at least minimized if the topic had been presented in a different manner".<ref>
{{cite news
|url=http://www.presscouncil.ie/decided-by-press-ombudsman/mr-mark-connolly-and-others-and-the-irish-examiner.2281.html
|author=Press Council of Ireland
|title=Mr Mark Connolly and Others and the Irish Examiner
|work=Decisions of the Press Ombudsman
|date=17 May 2012
}}</ref>

Dr Humphreys has taken the Refrigerator Mother theory further than others, with claims that abuse and emotional neglect are the cause of schizophrenia<ref>
{{cite news
|author=Kathryn Holmquist
|title=The power of negative thinking
|work=Irish Times
|date=21 May 1996
|page=11}}</ref> and a range of other childhood behaviours that are 'labelled' as medical disorders, including oppositional-defiant disorder (ODD), attention-deficit disorder (ADD), attention-deficit hyperactivity disorder (ADHD), dyspraxia and dyslexia<ref>
{{cite news
|last=Humphreys |first=Tony
|title=Our rigid labels: Neglect does not irretrievably 'hard-wire' infants&nbsp;– Damage can be undone with unconditional love
|date=28 October 2011
|page=5}}</ref> in addition to ASDs. He believes that these 'labelled disorders' are avoidant behavioural adjustments that enable a threatened child "to survive in a painful world of conditionality" and are curable through "unconditionally valuing and caring" relationships.<ref>
{{cite news
|last=Humphreys |first=Tony
|title=Learning about your 'worth'
|work=Irish Times
|date=7 February 1995
|page=5
}}</ref> He has made similar claims for emotionally challenging environments causing the onset of asthma.<ref>
{{cite news
|last=Humphreys |first=Tony
|title=An inner longing: The absence of unconditional love in the home is a factor in the onset of asthmatic attacks
|work=Irish Examiner
|date=21 May 2010
|page=5}}</ref> Both professional and advocate groups have decried his theory of the aetiology of schizophrenia.<ref>
{{cite news
|author=Ciara Doyle (Communications Assistant, Schizophrenia Ireland)
|title=Schizophrenia
|work=Irish Times
|date=6 June 1996
|page=15}}</ref>

==Books==
<!-- G&M books also published by Newleaf, details from WorldCat -->
{{refbegin}}
* {{cite book |last=Humphreys |first=Tony |year=1995 |title=A Different Kind of Teacher, Self-esteem the Key to your Child's Future |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-3790-9<!-- for newer editions -->}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=1996 |title=The Power of "Negative" Thinking |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-2443-5}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=1998a |title=A Different Kind of Discipline |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-2807-5}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=1998b |title=Children Feeling Good |location=Dublin |publisher=Veritas
|isbn=978-1-85390-374-8}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=2000 |title=Work and Worth: Take Back Your Life |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-3122-8}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=2002 |title=Self-Esteem, The Key to Your Child's Future |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-2990-4}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=2003 |title=Whose Life are you Living? |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-3662-9}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=2004a |title=All About Children, Questions Parents Ask |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-3740-4}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=2004b |title=Leaving The Nest |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-3734-3}}<!--
-->
* {{cite book |last=Humphreys |first=Tony |year=2005 |title=Myself, My Partner |location=Dublin |publisher=Gill&nbsp;&amp; Macmillan |isbn=978-0-7171-3914-9}}<!--
-->
{{refend}}

==References==
{{reflist}}

==See also==
* [[Autism]]
* [[Autistic spectrum disorder]]
* [[Asperger's syndrome]]
* [[Bruno Bettelheim]]
* [[Refrigerator mother theory]]

==External links==
* [http://www.tonyhumphreys.ie Tony Humphreys' home page]

{{Authority control}}

{{DEFAULTSORT:Humphreys, Tony}}

[[Category:Autism activists]]
[[Category:Autism]]
[[Category:Former Roman Catholics]]
[[Category:Irish educators]]
[[Category:Irish psychologists]]
[[Category:Living people]]
[[Category:Motherhood]]
[[Category:Obsolete medical theories]]
[[Category:Self-help writers]]
[[Category:Year of birth missing (living people)]]