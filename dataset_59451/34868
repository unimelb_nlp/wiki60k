{{Infobox hurricane season
| Basin=Atl
| Year=1914
| Track=1914 Atlantic hurricane season summary map.png
| First storm formed=September 15, 1914
| Last storm dissipated=October 26, 1914
| Strongest storm name=One (by default)
| Strongest storm pressure=995
| Strongest storm winds=60
| Total depressions=2 (record low)
| Total storms=1 (record low)
| Damages=
| Fatalities=None
| five seasons=[[1912 Atlantic hurricane season|1912]], [[1913 Atlantic hurricane season|1913]], '''1914''', [[1915 Atlantic hurricane season|1915]], [[1916 Atlantic hurricane season|1916]]
}}
The '''1914 Atlantic hurricane season''' was the least active Atlantic hurricane season on record, with only one known [[tropical cyclone|tropical storm]]. Although hurricane season typically encompasses a much larger time-span, actual activity was confined to the middle of September. The only tropical cyclone of the year developed in the region of [[The Bahamas]] on September 15 and drifted northwestward, moving inland over [[Florida]] and [[Georgia (U.S. state)|Georgia]]. Thorough warnings before the storm prevented any major damage. The 1914 season is one of only two that did not produce any hurricanes (the other being the [[1907 Atlantic hurricane season|1907 season]]). Due to the lack of modern technology, including satellite imagery, information is often sparse, and an additional tropical depression may have existed in late October.

==Season summary==
With only one official tropical cyclone, the 1914 season was the least active Atlantic hurricane season on record. It is one of only two Atlantic seasons without a storm of hurricane intensity (winds of {{convert|75|mph|km/h|abbr=on}} or stronger),<ref name="HURDAT2">{{cite web|author=Landsea, Chris|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT - 2005 Changes/Additions for 1911 to 1914|year=2005|publisher=National Oceanic and Atmospheric Administration Hurricane Research Division|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_1911-14.html|accessdate=March 15, 2011|display-authors=etal}}</ref> the other being the [[1907 Atlantic hurricane season|1907 season]].<ref name="HURDAT"/> The sole tropical storm's formation on September 14 represents the latest start to a hurricane season since officials records began in 1851.<ref>{{cite web|author=Bob Fogarty|title=2009 Hurricane Season - Is it Over Before it Began?|date=Fall 2009|publisher=National Weather Service|accessdate=March 19, 2011|url=http://www.srh.noaa.gov/images/ewx/Fall_09.pdf}}</ref>

Information on the 1914 season is chiefly based on data from the [[HURDAT|Atlantic hurricane database]] (HURDAT), which undertook a thorough [[Atlantic hurricane reanalysis|reanalysis of hurricanes]] from 1911 through 1914 in 2005. Several changes, mostly of a minor nature, were made to the September tropical storm. Additionally, two other systems during the year were formally considered for inclusion into the hurricane database; one of them was deemed a potential tropical depression, but considered too weak to be classified a tropical storm. The other was assessed as a non-tropical system. The 2005 HURDAT reanalysis relied largely on historical weather maps and ship reports in place of modern technology, including satellite imagery.<ref name="HURDAT2"/> 
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/09/1914 till:01/11/1914
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/09/1914

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_≤39_mph_(0–62_km/h)_(TD)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)_(TS)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)_(C1)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)_(C2)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)_(C3)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)_(C4)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_≥=156_mph_(≥250_km/h)_(C5)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:15/09/1914 till:19/09/1914 color:TS text:"Tropical Storm #1 (TS)"
  from:25/10/1914 till:26/10/1914 color:TD text:"TD #2 (TD)"

  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/09/1914 till:01/10/1914 text:September
  from:01/10/1914 till:01/11/1914 text:October
TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson hurricane wind scale]])"

</timeline>
</center>

==Systems==

===Tropical Storm One===
{{Infobox Hurricane Small
| Basin=Atl
| Image=One_1914-09-16_weather_map.jpg
|Track=1914 Atlantic tropical storm track.png
| Formed=September 15
| Dissipated=September 19
| 1-min winds=60
| Pressure=<995
}}
The first and only tropical storm of the season originated in a westward-moving [[tropical wave]] denoted on weather maps from September 13. Decreases in [[atmospheric pressure|air pressure]] occurred throughout the Bahamas, providing "strong indications of a disturbance".<ref name="MWR">{{cite journal|volume=42|issue=9|author=Bowie, Edward H|work=[[Monthly Weather Review]]|publisher=[[American Meteorological Society]]|pages=540|date=September 1914|accessdate=March 15, 2011|title=Storms And Warnings For September|url=http://docs.lib.noaa.gov/rescue/mwr/042/mwr-042-09-0540.pdf|format=PDF|bibcode = 1914MWRv...42..540B |doi = 10.1175/1520-0493(1914)42<540:SAWFS>2.0.CO;2 }}</ref> The system became a tropical depression at 00:00 [[UTC]] on September 15, approximately {{convert|200|mi|km|abbr=on}} east of [[Miami, Florida]]. It strengthened into a tropical storm about 12 hours later,<ref name="HURDAT">{{cite web|author=Hurricane Specialists Unit|publisher=National Hurricane Center|year=2010|accessdate=March 15, 2011|title=Easy to Read HURDAT 1851–2009|url=http://www.aoml.noaa.gov/hrd/hurdat/easyread-2009.html}}</ref> leading to the issuance of storm warnings from the east coast of Florida to as far north as [[Hatteras, North Carolina]].<ref name="MWR"/>

The system drifted northwest while gradually intensifying, and was situated south of the Georgia coast late on September 16. While most tropical systems in the vicinity tend to continue northward along the [[Eastern Seaboard]], the cyclone curved westward and moved ashore near the Florida–Georgia state border after achieving a peak intensity with [[maximum sustained winds]] of {{convert|70|mph|km/h|abbr=on}}.<ref name="HURDAT"/><ref name="MWR"/> It progressed inland over southern Georgia as it quickly weakened, but its intensity leveled off after around 18:00 UTC on September 17. The storm skirted the northern [[Gulf of Mexico]] as it swerved slightly south of due west, weakening to a tropical depression over southeastern [[Louisiana]].<ref name="HURDAT"/> By early September 19, the depression had further deteriorated into an open [[trough (meteorology)|trough]]&mdash;a poorly defined, elongated area of low pressure.<ref name="HURDAT2"/>

The storm produced widespread rainfall in the [[Southeastern United States]], accompanied by gale-force winds along the coast, and ships reported severe conditions at sea. High tides occurred around [[St. Augustine, Florida]], washing over the South Street Causeway. Winds from the storm dispersed large amounts of dead grass from marshes in the area. No significant damage was reported due to thorough warnings before the cyclone. A 2005 reanalysis of the storm made some minor changes to its listing in the [[HURDAT|official hurricane database]], setting back the time of formation and raising the peak intensity.<ref name="HURDAT2"/>

===Tropical depression===
In addition to the September tropical storm, a possible depression that remained below tropical storm intensity developed in late October. On October 24, a broad area of low pressure was present over the western Gulf of Mexico and [[Caribbean Sea]]. A possible center of low pressure, attached to a [[cold front]] extending southward, had formed within the larger system and moved toward the east. Another center of low pressure formed in the northwestern Caribbean on October 25 and is considered a tropical depression in contemporary research. The depression had weak winds due to the light pressure gradient in the region and, at its peak, it had a minimum central pressure of {{convert|1004|mbar|inHg|abbr=on}}. On October 26, the cold front associated with the [[extratropical cyclone]] to the north absorbed the tropical system. The next day, the extratropical system deteriorated into an open trough. Although the tropical low was reviewed for inclusion into the hurricane database as a tropical storm in 2005, it was deemed too weak.<ref name="HURDAT2"/>

==See also==
{{Portal|Tropical cyclones}}
*[[List of Florida hurricanes]]
*[[List of Atlantic hurricane seasons]]

==References==
{{reflist}}

==External links==
{{Commons category|1914 Atlantic hurricane season}}
*[http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1914.pdf Monthly Weather Review]

{{TC Decades|Year=1910|basin=Atlantic|type=hurricane}}
{{Good article}}

{{DEFAULTSORT:1914 Atlantic Hurricane Season}}
[[Category:1910–1919 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]