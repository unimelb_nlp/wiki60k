{{Use dmy dates|date=June 2015}}
{{Use South African English|date=June 2015}}
{{infobox military award
| name             = Meritorious Service Medal (South Africa)
| image            = [[File:Meritorious Service Medal (South Africa) George V.jpg|300px]]
| caption          = 
| awarded_by       = [[George V|the Monarch of the United Kingdom and the British Dominions, and Emperor of India]]
| country          = [[File:Red Ensign of South Africa 1912-1928.svg|25px]] [[File:Flag of South Africa 1928-1994.svg|25px]] [[Union of South Africa]]
| type             = Military long and meritorious service medal
| eligibility      = Warrant officers and senior non-commissioned officers
| for              = 21 years meritorious service
| campaign         = 
| status           = Discontinued in 1940
| description      = 
| motto            = 
| clasps           = 
| post-nominals    = 
| established      = 1914
| first_award      = 
| last_award       = 
| total            = 46
| posthumous       = 
| recipients       = 
| precedence_label = Order of wear
| individual       = 
| higher           = [[File:Flag of the United Kingdom.svg|25px]] Queen Elizabeth II's Long and Faithful Service Medal
| same             = [[File:Flag of the United Kingdom.svg|25px]] [[Meritorious Service Medal (United Kingdom)]]<br>[[File:Cape Colony flag.png|25px]] [[Meritorious Service Medal (Cape of Good Hope)]]<br>[[File:BlueEnsignNatal.png|25px]] [[Meritorious Service Medal (Natal)]]<br>[[File:Flag of New Zealand.svg|25px]] [[Meritorious Service Medal (New Zealand)]]
| lower            = [[File:Flag of the United Kingdom.svg|25px]] [[Accumulated Campaign Service Medal]]
| related          = 
| image2           = [[File:Ribbon - Meritorious Service Medal (Union).png|x29px]]
| caption2         = Ribbon bar
}}

In May 1895, Queen Victoria authorised Colonial governments to adopt various British military medals and to award them to their local permanent military forces. The [[Cape Colony|Cape of Good Hope]] and [[Colony of Natal]]  instituted their own territorial versions of the Meritorious Service Medal in terms of this authority. These two medals remained in use in the respective territories until after the establishment of the [[Union of South Africa]] in 1910.<ref name="Colonial">[http://www.geocities.ws/militaf/mil94.htm South African Medal Website – Colonial Military Forces] (Accessed 6 May 2015)</ref>

In 1914, the '''Meritorious Service Medal (South Africa)''' was instituted for the Union of South Africa, for award to selected senior non-commissioned officers of the Permanent Force of the newly established [[Union Defence Force (South Africa)|Union Defence Forces]] who had completed twenty-one years of meritorious service.<ref name="Union1913">[http://www.geocities.ws/militaf/mil13.htm South African Medal Website – Union Defence Forces (1913–1939)] (Accessed 9 May 2015)</ref>

==Origin==
The United Kingdom's [[Meritorious Service Medal (United Kingdom)|Meritorious Service Medal]] was instituted by [[Queen Victoria]] on 19 December 1845 to recognise meritorious service by [[non-commissioned officers]] of the British Army. Recipients were also granted an annuity, the amount of which was based on rank.<ref name="Gazette33700">{{londongazette|issue=33700|startpage=1893|date=20 March 1931}}</ref>

After Queen Victoria authorised Dominion and Colonial governments on 31 May 1895 to adopt the Meritorious Service Medal, as well as the [[Distinguished Conduct Medal]] and the [[Army Long Service and Good Conduct Medal]], and to award them to their local military forces, the [[Meritorious Service Medal (Cape of Good Hope)|Cape of Good Hope]] and [[Meritorious Service Medal (Natal)|Natal]] instituted their own versions of the Meritorious Service Medal.<ref name="Colonial"/><ref name="StratfordMSM">[http://www.stephen-stratford.co.uk/msm.htm Stephen Stratford Medals site - British Military & Criminal History - 1900 to 1999 - Army MSM] (Accessed 20 June 2015)</ref>

Other territories which took advantage of the authorisation include Canada, India, New South Wales, [[Meritorious Service Medal (New Zealand)|New Zealand]], Queensland, South Australia, Tasmania, Victoria and, from 1901, the [[Meritorious Service Medal (Australia)|Commonwealth of Australia]].<ref name="New ZealandMSM">[http://medals.nzdf.mil.nz/category/e/e1.html New Zealand Defence Force – New Zealand Long Service and Good Conduct Medals - The New Zealand Meritorious Service Medal] (Access date 21 June 2015)</ref><ref name="Australia">[http://www.itsanhonour.gov.au/honours/awards/imperial.cfm#msm_0275 Australian Government - It's an Honour - Imperial Awards - Meritorious Service Medal (1902-1975)] (Access date 21 June 2015)</ref>

==Union Defence Forces==
The [[Union of South Africa]] was established on 31 May 1910 in terms of the [[South Africa Act 1909|South Africa Act, 1909]], enacted by the Parliament of the [[United Kingdom]]. The Union Defence Forces were established in 1912 in terms of the Union Defence Act, no. 13 of 1912, enacted by the Parliament of the Union of South Africa.<ref name="South Africa Act">[[s:South Africa Act, 1909|South Africa Act, 1909, enacted by the Parliament of the United Kingdom, 20 September 1909]]</ref><ref name="SAR History">''The South African Railways - Historical Survey''. Editor George Hart, Publisher Bill Hart, Sponsored by Dorbyl Ltd., Published c. 1978, p. 25.</ref>

==Adoption==
The Meritorious Service Medal was adopted by the Union as the Meritorious Service Medal (South Africa) in 1914, but unlike the earlier [[Meritorious Service Medal (Cape of Good Hope)]] and [[Meritorious Service Medal (Natal)]], the name of the country was not inscribed on this medal's reverse. Instead, the South African medal is identical to the United Kingdom's Meritorious Service Medal, but with a ribbon exclusive to South Africa.<ref name="Colonial"/><ref name="Union1913"/>

==Award criteria==
Recipients of the Meritorious Service Medal (South Africa) were usually already holders of a long service and good conduct medal such as the [[Army Long Service and Good Conduct Medal (Cape of Good Hope)]], [[Army Long Service and Good Conduct Medal (Natal)]] or [[Permanent Forces of the Empire Beyond the Seas Medal]]. Until 1920, the award of the medal was coupled to a Meritorious Service Annuity. It could be awarded to selected warrant officers and senior non-commissioned officers of the Permanent Force who had completed twenty-one years of meritorious service.<ref name="Union1913"/><ref name="Gazette33700"/>

The medal and annuity were awarded sparingly and only to selected candidates, usually upon retirement as a reward for long and valuable service, upon recommendation by their commanding officers and selected from a list by the Commander-in-Chief of the Union Defence Forces, the [[Governor-General of the Union of South Africa]].<ref name="Union1913"/><ref name="Gazette33700"/>

==Gallantry==
[[File:Meritorious Service Medal (United Kingdom) George V v1.jpg|thumb|[[Meritorious Service Medal (United Kingdom)]], first King George V version]]
During the [[First World War]], as approved by Royal Warrant on 4 October 1916, non-commissioned officers below the rank of Sergeant and men also became eligible for the award of the Meritorious Service Medal, without the annuity, for acts of gallantry in the performance of military duty, not necessarily on active service, or in saving or attempting to save the life of an officer or soldier. For acts of gallantry, however, only the Meritorious Service Medal (United Kingdom) was awarded, irrespective of the recipient's nationality, and recipients were entitled to use the post-nominal letters "MSM". A Bar to the medal was instituted by Royal Warrant on 23 November 1916, which could be awarded to holders of the Meritorious Service Medal for subsequent acts of gallantry.<ref name="Gazette33700"/><ref name="Gallantry">[http://www.itsanhonour.gov.au/honours/awards/imperial.cfm#ms16 Australian Government - It's an Honour - Imperial Awards - Meritorious Service Medal (1916-1928) (for non-operational bravery or meritorious service directly connected with the war effort)] (Access date 25 June 2015)</ref>

==Order of wear==
In the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], the Meritorious Service Medal (South Africa) ranks on par with the Meritorious Service Medal (United Kingdom). It takes precedence after Queen Elizabeth II's Long and Faithful Service Medal and before the [[Accumulated Campaign Service Medal]].<ref name="London Gazette"/>

===South Africa===
{{main|South African military decorations order of wear#Order of wear}}
With effect from 6 April 1952, when a new South African set of decorations and medals was instituted to replace the British awards used to date, the older British decorations and medals applicable to South Africa continued to be worn in the same order of precedence but, with the exception of the [[Victoria Cross]], took precedence after all South African orders, decorations and medals awarded to South Africans on or after that date. Of the official British medals which were applicable to South Africans, the Meritorious Service Medal (South Africa) takes precedence as shown.<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3352|date=17 March 2003}}</ref><ref name="Notice1982">Government Notice no. 1982 of 1 October 1954 - ''Order of Precedence of Orders, Decorations and Medals'', published in the Government Gazette of 1 October 1954.</ref><ref name="Gazette 27376">Republic of South Africa Government Gazette Vol. 477, no. 27376, Pretoria, 11 March 2005, {{OCLC|72827981}}</ref>

[[File:Ribbon - Medal for Long Service and Good Conduct (Natal).png|x37px|Meritorious Service Medal (Natal)]] [[File:Ribbon - Meritorious Service Medal (Union).png|x37px|Meritorious Service Medal (South Africa)]] [[File:Ribbon - Army Long Service and Good Conduct Medal (Cape).png|x37px|Army Long Service and Good Conduct Medal (Cape of Good Hope)]] 
* Preceded by the [[Meritorious Service Medal (Natal)]].
* Succeeded by the [[Army Long Service and Good Conduct Medal (Cape of Good Hope)]].

==Description==
The medal was struck in silver and is a disk, 36 millimetres in diameter and with a raised rim on both sides. The suspender is an ornamented scroll pattern swiveling type, affixed to the medal by means of a single-toe claw and a pin through the upper edge of the medal.<ref name="Union1913"/><ref name="StratfordMSM"/>

;Obverse
The obverse has the effigy of [[George V|King George V]], bareheaded and in Field Marshal's uniform, and is inscribed "GEORGIVS V BRITT: OMN: REX ET IND: IMP:" around the perimeter.<ref name="Union1913"/><ref name="StratfordMSM"/><ref name="Canada">[http://www.veterans.gc.ca/eng/remembrance/medals-decorations/service-medals/msm Veterans Affairs Canada - British Meritorious Service Medal (MSM)] (Access date 25 June 2015)</ref>

;Reverse
The reverse has the words "FOR MERITORIOUS SERVICE" in three lines, encircled by a laurel wreath and surmounted by the Imperial Crown.<ref name="Union1913"/><ref name="Gazette33700"/><ref name="StratfordMSM"/>

;Ribbon
The ribbon is 32 millimetres wide, with a 5 millimetres wide dark blue band, a 6½ millimetres wide crimson band and a 2½ millimetres wide white band, repeated in reverse order and separated by a 4 millimetres wide dark blue band.<ref name="Union1913"/>

==Discontinuation==
The Meritorious Service Medal (South Africa) was awarded to only 46 individuals. Award of the medal was discontinued in 1940.<ref name="Union1913"/>

==References==
{{Reflist|30em}}
{{South African military decorations and medals}}
{{Efficiency and long service decorations and medals}}

[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|403.3]]
[[Category:Military decorations and medals of South Africa pre-1952]]
[[Category:Long and Meritorious Service Medals of Britain and the Commonwealth]]