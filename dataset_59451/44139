{{other uses}}
{{Infobox airport
| nativename   = 
| nativename-a = 
| FAA          = BET
| location     = <!--if different than above-->
| hub          = 
Passenger
*[[Grant Aviation]]
*[[Ravn Connect]]
*[[Flight Alaska|Yute Air]]
| coordinates  = {{coord|60|46|43|N|161|50|14|W|region:US-AK|display=inline,title}}
| pushpin_label          = '''BET'''
| pushpin_label_position = bottom
| r1-length-f  = 6,400
| r1-surface   = Asphalt
| name         = Bethel Airport
| image        = BET-c.jpg
| image-width  = 250
| IATA         = BET
| ICAO         = PABE
| type         = Public
| owner        = [[State of Alaska DOT&PF]] - Central Region
| operator     = 
| city-served  = [[Bethel, Alaska]]
| elevation-f  = 129
| website      = 
| pushpin_map            = USA Alaska
| pushpin_mapsize        = 250
| pushpin_map_caption    = Location of airport in Alaska
| r1-number    = 1L/19R
| r2-number    = 1R/19L
| r2-length-f  = 4,000
| r2-surface   = Asphalt
| r3-number    = 12/30
| r3-length-f  = 1,858
| r3-surface   = Asphalt/Gravel
| stat-year    = 2015
| stat1-header = Aircraft Operations
| stat1-data   = 122,000 (2014)
| stat2-header = Based Aircraft
| stat2-data   = 112
| stat3-header = Passengers
| stat3-data   = 311,270
| stat4-header = Freight
| stat4-data   = 62,000,000 lbs
| footnotes    = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=BET|use=PU|own=PU|site=50061.1*A}}. Federal Aviation Administration. Effective January 5, 2017.</ref>
}}

'''Bethel Airport''' {{Airport codes|BET|PABE|BET}} is a state owned, public use [[airport]] located three&nbsp;[[nautical mile]]s (6&nbsp;[[kilometre|km]]) southwest of the [[central business district]] of [[Bethel, Alaska|Bethel]], a city in the [[Bethel Census Area, Alaska|Bethel Census Area]] of the [[U.S. state]] of [[Alaska]].<ref name="FAA" />

As per [[Federal Aviation Administration]] records, the airport had 140,291 passenger boardings (enplanements) in [[calendar year]] 2008,<ref>
 {{cite web
 | url = http://www.faa.gov/airports/planning_capacity/passenger_allcargo_stats/passenger/media/cy08_all_enplanements.pdf
 | title = Enplanements for CY 2008 | format = PDF, 1.0 MB
 | work = CY 2008 Passenger Boarding and All-Cargo Data
 | publisher = Federal Aviation Administration | date = December 18, 2009
 }}
</ref> 134,848 enplanements in 2009, and 144,353 in 2010.<ref>
 {{cite web
 | url = http://www.faa.gov/airports/planning_capacity/passenger_allcargo_stats/passenger/media/cy10_all_enplanements.pdf
 | title = Enplanements for CY 2010 | format = PDF, 189 KB
 | work = CY 2010 Passenger Boarding and All-Cargo Data
 | publisher = Federal Aviation Administration | date = October 4, 2011
 }}
</ref> It is included in the [[National Plan of Integrated Airport Systems]] for 2011–2015, which [[FAA airport categories|categorized]] it as a ''primary commercial service'' airport (more than 10,000 enplanements per year).<ref>
 {{cite web
 |url=http://www.faa.gov/airports/planning_capacity/npias/reports/media/2011/npias_2011_appA.pdf 
 |title=2011–2015 NPIAS Report, Appendix A 
 |format=[[PDF]], 2.03 MB 
 |work=National Plan of Integrated Airport Systems 
 |publisher=Federal Aviation Administration 
 |date=October 4, 2010 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120927084535/http://www.faa.gov/airports/planning_capacity/npias/reports/media/2011/npias_2011_appA.pdf 
 |archivedate=2012-09-27 
 |df= 
}}
</ref>

==History==
Construction began September 21, 1941, and the airfield was activated July 4, 1942; it was known as [[Bethel Air Base]]. It was used by [[Air Transport Command (United States Air Force)|Air Transport Command]] as auxiliary airfield for [[Lend-Lease]] aircraft being flown to [[Siberia]]. The facility was transferred to Eleventh Air Force, then to Alaskan Air Command in 1945; it became the joint-use Bethel Airport. It was used for construction of AC&W [[Bethel Air Force Station]] in the mid-1950s. Full jurisdiction was turned over to Alaska Government in 1958.<ref>{{Air Force Historical Research Agency}}</ref>

== Facilities and aircraft ==
Bethel Airport covers an area of 1,056 [[acre]]s (427 [[hectare|ha]]) at an [[elevation]] of 129 feet (39 m) above [[mean sea level]]. It has three [[runway]]s: 1L/19R is 6,400 by 150 feet (1,951 x 46 m) with an [[asphalt]] surface; 1R/19L is 4,000 by 75 feet (1,219 x 23 m) with an asphalt surface; 12/30 is 1,858 by 75 feet (566 x 23 m) with an asphalt/[[gravel]] surface.<ref name="FAA" />

For the 12-month period ending January 1, 2011, the airport had 122,000 aircraft operations, an average of 334 per day: 54% [[air taxi]], 41% [[general aviation]], 4% [[airline|scheduled commercial]], and 1% [[military aviation|military]]. At that time there were 232 aircraft based at this airport: 90% single-[[aircraft engine|engine]], 7% multi-engine, 2% [[helicopter]], and 1% [[military aircraft|military]].<ref name="FAA" />

== Airlines and destinations ==

The following [[airline]]s offer scheduled passenger service:
{{Airport destination list
<!-- 1 -->
| {{nowrap|[[Alaska Airlines]]}} |[[Ted Stevens Anchorage International Airport|Anchorage]]
<!-- 2 -->
| [[Grant Aviation]] | [[Chefornak Airport|Chefornak]], [[Chevak Airport|Chevak]], [[Emmonak Airport|Emmonak]], [[Kipnuk Airport|Kipnuk]], [[Kotlik Airport|Kotlik]], [[Kwigillingok Airport|Kwigillingok]], [[Newtok Airport|Newtok]], [[Scammon Bay Airport|Scammon Bay]], [[Toksook Bay Airport|Toksook Bay]], [[Tununak Airport|Tununak]] <ref>{{cite web|url=https://apps6.tflite.com/TakeFlitePublicGRT/Pgschedule.aspx?detailed=y&group=y&showpilot=n&showgate=n |title=Schedule}} (retrieved January 9, 2017)</ref>
<!-- 3 --> 
| [[Ravn Alaska]]<br> {{nowrap|operated by [[Corvus Airlines]]}} |[[Ted Stevens Anchorage International Airport|Anchorage]] <ref>{{cite web|url=https://www.flyravn.com/flying-with-ravn/route-map/|title=Destinations}} (retrieved January 9, 2017)</ref>
<!-- 4 -->
| [[Ravn Connect]]<br> {{nowrap|operated by [[Hageland Aviation]]}} |[[Akiachak Airport|Akiak]], [[Akiak Airport|Akiak]], [[Aniak Airport|Aniak]], [[Atmautluak Airport|Atmautluak]], [[Chefornak Airport|Chefornak]], [[Chevak Airport|Chevak]], [[Eek Airport|Eek]], [[Goodnews Airport|Goodnews Bay]], [[Hooper Bay Airport|Hooper Bay]], [[Kalskag Airport|Kalskag]], [[Kasigluk Airport|Kasigluk]], [[Kipnuk Airport|Kipnuk]], [[Kongiganak Airport|Kongiganak]], [[Kwethluk Airport|Kwethluk]], [[Kwigillingok Airport|Kwigillingok]], [[Marshall Don Hunter Sr. Airport|Marshall]], [[Mekoryuk Airport|Mekoryuk]], [[Mountain Village Airport|Mountain Village]], [[Newtok Airport|Newtok]], [[Nightmute Airport|Nightmute]], [[Nunapitchuk Airport|Nunapitchuk]], [[Pilot Station Airport|Pilot Station]], [[Platinum Airport|Platinum]], [[Quinhagak Airport|Quinhagak]], [[Russian Mission Airport|Russian Mission]], [[St. Mary's Airport (Alaska)|St. Mary's]], [[Scammon Bay Airport|Scammon Bay]], [[Toksook Bay Airport|Toksook Bay]], [[Tuluksak Airport|Tuluksak]], [[Tuntutuliak Airport|Tuntutuliak]] [[Tununak Airport|Tununak]]<ref>{{cite web|url=https://www.flyravn.com/flying-with-ravn/route-map/|title=Destinations}} (retrieved January 9, 2017)</ref>
<!-- 5 -->
| [[Flight Alaska|Yute Air]] | [[Akiachak Airport|Akiachak]], [[Akiak Airport|Akiak]], [[Atmautluak Airport|Atmautluak]], [[Chefornak Airport|Chefornak]], [[Eek Airport|Eek]], [[Goodnews Airport|Goodnews Bay]], [[Kasigluk Airport|Kasigluk]], [[Kipnuk Airport|Kipnuk]], [[Kongiganak Airport|Kongiganak]], [[Kwigillingok Airport|Kwigillingok]], [[Kwethluk Airport|Kwethluk]], [[Marshall Don Hunter Sr. Airport|Marshall]], [[Napakiak Airport|Napakiak]], [[Napaskiak Airport|Napaskiak]], [[Newtok Airport|Newtok]], [[Nightmute Airport|Nightmute]], [[Nunapitchuk Airport|Nunapitchuk]], [[Pilot Station Airport|Pilot Station]], [[Platinum Airport|Platinum]], [[Quinhagak Airport|Quinhagak]], [[Toksook Bay Airport|Toksook Bay]], [[Tuluksak Airport|Tuluksak]], [[Tuntutuliak Airport|Tuntutuliak]], [[Tununak Airport|Tununak]]<ref>{{cite web|url=http://www.flyyuteair.com/flight-information.html|title=Flight Information}} (retrieved January 9, 2017)</ref>
}}

==Statistics==
{{Bar graph
| title = Carrier shares: (Dec 2015 - Nov 2016)<ref name ="transtats.bts.gov"></ref>

| bar_width   = 20
| width_units = em
| label_type  = Carrier&nbsp;&nbsp;
| data_type   = Passengers (arriving and departing)
| data_max    = 110,000
| label1   = [[Hageland Aviation|Hageland]]
| data1    = 110,000
| comment1 = 36.30%
| label2   = [[Alaska Airlines|Alaska]]
| data2    = 104,000
| comment2 = 34.29%
| label3   = [[Aero Flight|Yute Air]]
| data3    = 37,830
| comment3 = 12.49%
| label5   = [[Grant Aviation|Grant]]
| data5    = 22,430
| comment5 = 7.41%
| label4   = [[Era Aviation|Era]]
| data4    = 28,700
| comment4 = 9.48%
}}

{| class="wikitable"
|+ Top domestic destinations: (Dec 2015 - Nov 2016)<ref name="transtats.bts.gov">
  {{cite web
  | url = http://www.transtats.bts.gov/airports.asp?pn=1&Airport=BET&Airport_Name=Bethel,%20AK:%20Bethel%20Airport&carrier=FACTS
  | title = Bethel, AK: Bethel (BET)
  | publisher = [[Bureau of Transportation Statistics]] (BTS), [[Research and Innovative Technology Administration]] (RITA), [[U.S. Department of Transportation]]
  | date = December 2015 | accessdate = March 1, 2017
 }}
</ref>

===Top destinations===
! Rank
! City
! Passengers
! Carriers
|-
| 1
| [[Ted Stevens Anchorage International Airport|Anchorage, AK]]
| align=right | 66,470
| Alaska, Corvus
|-
|  2
| [[Quinhagak Airport|Quinhagak, AK]]
| align=right | 6,620
| Hageland, Yute Air
|-
|  3
| [[Kipnuk Airport|Kipnuk, AK]]
| align=right | 4,600
| Grant, Hageland, Yute Air
|-
|  4
| [[Chevak Airport|Chevak, AK]]
| align=right | 4,550
| Grant, Hageland
|-
|  5
| [[Hooper Bay Airport|Hooper Bay, AK]]
| align=right | 4,080
| Hageland
|-
|  6
| [[Toksook Bay Airport|Toksook Bay, AK]]
| align=right | 3,450
| Grant, Hageland, Yute Air
|-
|  7
| [[Kasigluk Airport|Kasigluk, AK]]
| align=right | 3,410
| Hageland, Yute Air
|-
|  8
| [[Emmonak Airport|Emmonak, AK]]
| align=right | 3,340
| Grant
|-
|  9
| [[Chefornak Airport|Chefornak, AK]]
| align=right | 3,270
| Grant, Hageland, Yute Air
|-
|  10
| [[Kongiganak Airport|Kongiganak, AK]]
| align=right | 3,130
| Hageland, Yute Air
|-
|}

== Cargo airlines ==
{| class="wikitable"
!Airline
!Destination
|-
|[[Alaska Central Express]]
|[[Ted Stevens Anchorage International Airport|Anchorage]]
|-
|[[Everts Air Cargo]]
|[[Ted Stevens Anchorage International Airport|Anchorage]]
|-
|[[Lynden Air Cargo]]
|[[Ted Stevens Anchorage International Airport|Anchorage]]
|-
|[[Northern Air Cargo]]
|[[Ted Stevens Anchorage International Airport|Anchorage]]
|}
* [[Alaska Central Express]]
* [[Arctic Transportation Services]]
* [[Everts Air Cargo]]
* [[Lynden Air Cargo]]
* [[Northern Air Cargo]]
* [[Yute Air]]

== References ==
{{Reflist}}

== External links ==
* [http://msrmaps.com/map.aspx?t=2&s=13&lat=60.7786&lon=-161.8372&w=600&h=600&lp=---+None+--- Topographic map] from [[USGS]] ''[[The National Map]]''
* {{FAA-diagram|05001}}
* {{FAA-procedures|BET}}
* {{US-airport2|BET|PABE}}
** {{FAA-delay|BET}}

[[Category:Airports in the Bethel Census Area, Alaska]]
[[Category:WAAS reference stations]]