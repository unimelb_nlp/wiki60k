{{Use dmy dates|date=April 2012}}
{{Infobox UN resolution
|number = 971
|organ = SC
|date = 12 January
|year = 1995
|meeting = 3,488
|code = S/RES/971
|document = http://undocs.org/S/RES/971(1995)
|for = 15
|abstention = 0
|against = 0
|subject = Abkhazia, Georgia
|result = Adopted 
|image = This apartment block was filled with refugees from Abkhazia. I believe it was demolished a couple of months ago..jpg
|caption = This apartment block accommodated refugees from Abkhazia
}}

'''United Nations Security Council resolution 971''', adopted unanimously on 12 January 1995, after reaffirming resolutions [[United Nations Security Council Resolution 849|849]] (1993), [[United Nations Security Council Resolution 854|854]] (1993), [[United Nations Security Council Resolution 858|858]] (1993), [[United Nations Security Council Resolution 876|876]] (1993), [[United Nations Security Council Resolution 881|881]] (1993), [[United Nations Security Council Resolution 892|892]] (1993), [[United Nations Security Council Resolution 896|896]] (1994), [[United Nations Security Council Resolution 901|901]] (1994), [[United Nations Security Council Resolution 906|906]] (1994), [[United Nations Security Council Resolution 934|934]] (1994) and [[United Nations Security Council Resolution 937|937]] (1994), the Council extended the [[United Nations Observer Mission in Georgia]] (UNOMIG) until 15 May 1995.<ref>{{cite book|last=United Nations, Dept. of Public Information|title=The United Nations and the situation in Georgia|publisher=United Nations, Dept. of Public Information|date=1995|page=12}}</ref>

The Security Council reaffirmed the [[sovereignty]] and [[territorial integrity]] of [[Georgia (country)|Georgia]] and the right to return of all [[refugee]]s, on which an agreement was signed. The parties were urged not to undertake any unilateral actions that would impede the political process. There was a lack of progress on a comprehensive peace settlement and a slow return of refugees. In this regard the parties were called upon to work towards a settlement, particularly on the issue of the political status of [[Abkhazia]]. The Council expressed satisfaction at the co-operation between UNOMIG and the [[Commonwealth of Independent States]] (CIS) [[peacekeeping]] force.

After extending the mandate of UNOMIG until 15 May 1995, the Secretary-General [[Boutros Boutros-Ghali]] was requested to report on the situation in Abkhazia and Georgia within two months from the adoption of the current resolution. Both parties, particularly the Abkhaz side, were urged to comply with their commitments concerning refugees and displaced persons. The Secretary-General was asked to co-operate with the CIS force in taking additional steps to ensure the return of refugees and displaced persons, while Member States were called upon to contribute to the voluntary fund established by the [[Agreement on a Cease-fire and Separation of Forces]].<ref>{{cite book|last=Bothe|first=Michael|author2=Kondoch, Boris |title=International Peacekeeping: The Yearbook of International Peace Operations|publisher=Martinus Nijhoff Publishers|date=2002|page=222|isbn=978-90-411-1920-9}}</ref>

==See also==
* [[Georgian–Abkhazian conflict]]
* [[List of United Nations Security Council Resolutions 901 to 1000]] (1994–1995)
* [[United Nations resolutions on Abkhazia]]
* [[War in Abkhazia (1992–1993)]]

==References==
{{reflist|colwidth=30em}}

==External links==
*[http://undocs.org/S/RES/971(1995) Text of the Resolution at undocs.org]
{{wikisource}}

{{UNSCR 1995}}

[[Category:1995 United Nations Security Council resolutions]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:1995 in Georgia (country)]]
[[Category:1995 in Abkhazia]]
[[Category:United Nations Security Council resolutions concerning Georgia (country)]]
[[Category:United Nations Security Council resolutions concerning Abkhazia]]
[[Category:January 1995 events]]