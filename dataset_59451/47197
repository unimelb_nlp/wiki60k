'''Conradson Carbon Residue''', commonly known as "Concarbon" or "CCR" is a laboratory test used to provide an indication of the [[petroleum coke|coke]]-forming tendencies of an oil. Quantitatively, the test measures the amount of carbonaceous residue remaining after the oil's [[evaporation]] and [[pyrolysis]].<ref>{{cite web|last1=Humboldt Testing Equipment|title=Conradson Carbon Residue Apparatus|url=http://www.humboldtmfg.com/conradson_carbon_residue_apparatus.html|accessdate=27 November 2015}}</ref><ref>{{cite web|last1=Merriam-Webster|title=Conradson Carbon Test.|url=http://www.merriam-webster.com/dictionary/conradson%20carbon%20test|website=Merriam-Webster.com|accessdate=27 November 2015}}</ref><ref>{{cite web|last1=International Standards Organization|title=Petroleum products -- Determination of carbon residue -- Conradson method|url=http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=20865|accessdate=27 November 2015}}</ref> In general, the test is applicable to petroleum products which are relatively non-volatile, and which decompose on distillation at atmospheric pressure.<ref name=ASTM>{{cite web|last1=ASTM International|title=Standard Test Method for Conradson Carbon Residue of Petroleum Products|url=http://www.pentasflora.com/wp-content/uploads/2014/02/D189-Carbon-Residue.pdf}}</ref> The phrase "Conradson Carbon Residue" and its common names can refer to either the test or the numerical value obtained from it.

==Test method==
A quantity of sample is weighed, placed in a crucible, and subjected to destructive distillation. During a fixed period of severe heating, the residue undergoes [[cracking (chemistry)|cracking]] and coking reactions . At the termination of the heating period, the crucible containing the carbonaceous residue is cooled in a desiccator and weighed. The residue remaining is calculated as a percentage of the original sample, and reported as Conradson carbon residue.<ref name=ASTM />

==Applications==
* For burner fuel, Concarbon provides an approximation of the tendency of the fuel to form deposits in vaporizing pot-type and sleeve-type burners.<ref name="ASTM" />
* For [[diesel fuel]], Concarbon correlates approximately with combustion chamber deposits, provided that alkyl nitrates are absent, or if present, that the test is performed on the base fuel without additive.<ref name=ASTM />
*For [[motor oil]], Concarbon was once regarded as indicative of the amount of carbonaceous deposits the oil would form in the combustion chamber of an engine. This is now considered to be of doubtful significance due to the presence of additives in many oils.<ref name="ASTM" />
* For [[gas oil]], Concarbon  provides a useful correlation in the manufacture of gas there from.<ref name="ASTM" />
* For [[delayed coker]]s, the Concarbon of the feed correlates positively to the amount of coke that will be produced.<ref name=Coking>{{cite web|last1=Colorado School of Mines|title=Delayed Coking|url=http://processengr.com/pdf_documents/co_school_mines_coker_chp5.pdf|accessdate=22 November 2015}}</ref><ref>{{cite web|author1=Shabron, John  |author2=Speight, James G.|title=Correlation between Carbon Residue and Molecular Weight|url=https://web.anl.gov/PCS/acsfuel/preprint%20archive/Files/42_2_SAN%20FRANCISCO_04-97_0386.pdf|publisher=Western Research Institute|accessdate=27 November 2015}}</ref>
* For [[fluid catalytic cracking]] units, the Concarbon of the feed can be used to estimate the feed's coke-forming tendency.<ref name=FCC>{{cite book|last1=Sadeghbeigi|first1=Reza|title=Fluid Catalytic Cracking Handbook: Design, Operation, and Troubleshooting of FCC Facilities|date=2000|publisher=Gulf Professional Publishing|isbn=0884152898|pages=52|url=https://books.google.com/books?id=z0qNlKizu5sC&pg=PA52&lpg=PA52&dq=concarbon+catalytic+cracking&source=bl&ots=8QCp3wVdVl&sig=Bd0faa7f7Lelas5DXaIioaXcJbU&hl=en&sa=X&ved=0ahUKEwi0nKnN8KLJAhXF4iYKHSQdD1oQ6AEIHTAA#v=onepage&q=concarbon%20catalytic%20cracking&f=false}}</ref>

==See also==
*[[Ramsbottom Carbon Residue]]

==References==
{{reflist}}

*
*
*
*

[[Category:Petroleum technology]]
[[Category:Geochemical processes]]
[[Category:Petroleum industry]]