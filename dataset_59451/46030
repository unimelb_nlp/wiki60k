{{Infobox writer <!-- For more information see [[:Template:Infobox Writer/doc]]. -->
|name = Adam Grant <!-- Deleting this line will use the article title as the page name. -->
|image = Adam Grant.jpg
|image_size = 
|alt = 
|caption = 
|pseudonym =
|birth_name =
|birth_date = {{Birth date and age|1981|8|13}}
|birth_place =
|death_date = <!-- {{Death date and age|Year|Month|Day|Year|Month|Day}} -->
|death_place =
|resting_place =
|occupation = Author, professor
|language = English
|nationality = American
|ethnicity = 
|citizenship =
|education = 
|alma_mater        = {{plainlist}}
* [[Harvard College]]
* [[University of Michigan]]
{{endplainlist}}
|period =
|genre = Motivation, Success, Business, Education, Management, Psychology
|subject =
|movement =
|notableworks = 
|spouse =
|partner =
|children =
|relatives =
|influences =
|influenced =
|awards =
|signature =
|signature_alt =
|website = {{URL|http://www.adamgrant.net/}}
|portaldisp =
}}

{{Like resume|date=April 2017}}

'''Adam M. Grant''' (born August 13, 1981) is an American author and a professor at the [[Wharton School of the University of Pennsylvania]]. He grew up in the suburbs of [[Detroit, Michigan]]. Grant has been recognized as both the youngest tenured and highest rated professor at the Wharton School.<ref>{{cite web|last=Dominus|first=Susan|title=Is Giving the Secret to Getting Ahead?|url=https://www.nytimes.com/2013/03/31/magazine/is-giving-the-secret-to-getting-ahead.html?pagewanted=7&%2359;smid=fb-share&amp&_r=0|publisher=The New York Times|accessdate=April 2, 2013}}</ref>

== Academic career ==
Grant has been Wharton's top-rated professor for five straight years.<ref>{{cite web|title=Wharton Page|url=https://mgmt.wharton.upenn.edu/profile/1323/}}</ref> He has been recognized as one of the world's 25 most influential management thinkers<ref>{{cite web|title=Thinkers 50|url=http://thinkers50.com/t50-ranking/2015-2/}}</ref> and the world's 40 best business professors under 40.<ref>{{cite web|title=Best Professors|url=http://poetsandquants.com/2011/02/08/best-prof-adam-grant/}}</ref> He earned his Ph.D. in [[organizational psychology]] from the University of Michigan, completing it in less than three years, and his B.A. from [[Harvard University]], [[magna cum laude]] and [[Phi Beta Kappa]].<ref>{{cite web|title=Adam Grant Website|url=http://www.adamgrant.net}}</ref>

Grant is the author of two [[New York Times bestselling books]] translated into 35 languages. '''Originals''' explores how individuals champion new ideas and leaders fight groupthink; it is a #1 national bestseller and one of Amazon's best books of February 2016. '''Give and Take''' examines why helping others drives our success, and was named one of the best books of 2013 by Amazon, Apple, ''[[the Financial Times]]'', and ''[[The Wall Street Journal]]''—as well as one of Oprah's riveting reads and ''[[Harvard Business Review]]''’s ideas that shaped management.

Grant delivered a 2016 [[TED talk]]<ref>{{cite web|title=TED Talk|url=http://www.ted.com/talks/adam_grant_the_surprising_habits_of_original_thinkers}}</ref> on the surprising habits of original thinkers and was voted the audience's favorite speaker at [[The Nantucket Project]] on the success of givers and takers.<ref>{{cite web|title=Nantucket Project Talk |url=https://www.youtube.com/watch?v=3NYG9r_9wT4}}</ref> His New York Times articles on Raising a moral child and How to raise a creative child have each been shared over 300,000 times on social media.

He has earned awards for distinguished scholarly achievement from the [[Academy of Management]], the [[American Psychological Association]], and the [[National Science Foundation]]. He has more than 60 publications in leading management and psychology journals, and his pioneering studies have increased performance and reduced burnout among engineers and sales professionals, enhanced call center productivity, and motivated safety behaviors among doctors, nurses and lifeguards.

==Personal life==
Grant is married to wife Allison, whom he met during graduate school in Michigan. The couple has two daughters.<ref>{{cite web|title=Award Citation|url=https://faculty.wharton.upenn.edu/wp-content/uploads/2013/04/AdamGrant-AmericanPsychologist.pdf|publisher=American Psychologist|accessdate=23 March 2017}}</ref>
Grant was an All-American [[springboard]] [[diving|diver]].<ref>Grant was rated [http://www.usadiver.com/NISCA_99_list.htm 47th in the US] among public school male students in 1999. [http://www.usadiver.com/NISCA_99_list.htm]</ref> During his college time and afterwards, he used to work as a professional [[magic (illusion)|magic]]ian.<ref>{{YouTube|id=1baNQmnRCVw|title=Adam Grant presenting his new book "Give and Take", while showing great story-telling as well as magician skills}}</ref>

==Awards==
* Thinkers50 Most Influential Global Management Thinkers (2015)
* World Economic Forum Young Global Leader (2015–present)
* New York Times Contributing Opinion Writer (2015–present)
* Class of 1984 Teaching Award, highest-rated Wharton MBA professor (2012, 2013, 2014, 2015, 16)
* HR's Most Influential International Thinkers (2014, 2015)
* The [[American Psychological Association]] (APA) Distinguished Scientific Award for Early Career Contribution to Applied Psychology (2011)
* Cummings Scholarly Achievement Award, [[Academy of Management]] OB Division (2011)
* The [[Society for Industrial and Organizational Psychology]] Distinguished Early Career Contributions Award – Science (2011)
* Owens Scholarly Achievement Award, Best Publication in I/O Psychology, SIOP (2010)
* [[National Science Foundation]] Graduate Research Fellowship (2004–2006)
* APA Early Research Award, Applied Science (2005)
* World's 40 Best Business School Professors Under 40, Poets and Quants / [[Fortune Magazine|Fortune]] (2011)

== Published works ==

=== Books ===
* 2016: ''Originals: How Non-Conformists Move the World,'' ISBN 978-0-525-42956-2
* 2013: ''Give and Take: Why Helping Others Drives Our Success,'' ISBN 978-0-670-02655-5

===Papers===
Grant's publications in leading management and psychology journals include:
* Grant, 2013: ''Rocking the boat but keeping it steady: The role of emotion regulation in employee voice'', '''Academy of Management Journal'''
* Grant, 2013: ''Rethinking the extraverted sales ideal: The ambivert advantage'', '''Psychological Science''' 24: 1024–1030.
* Grant, 2012: ''Leading with meaning: Beneficiary contact, prosocial impact, and the performance effects of transformational leadership'', '''Academy of Management Journal''', 55: 458–476.
* Grant & Schwartz, 2011: ''Too much of a good thing: The challenge and opportunity of the inverted-U'', '''Perspectives on Psychological Science''', 6: 61-76.
* Grant, Gino & Hofmann, 2011: ''Reversing the extraverted leadership advantage: The role of employee proactivity'', '''Academy of Management Journal''', 54: 528-550.
* Grant & Hofmann, 2011: ''It’s not all about me: Motivating hospital hand hygiene by focusing on patients'', '''Psychological Science''', 22: 1494-1499.
* Grant, & Berry, 2011: ''The necessity of others is the mother of invention: Intrinsic and prosocial motivations, perspective-taking, and creativity'', '''Academy of Management Journal''', 54: 73-96.
* Grant & Gino, 2010: ''A little thanks goes a long way: Explaining why gratitude expressions motivate prosocial behavior'', '''Journal of Personality and Social Psychology''', 98: 946-955.
* Grant & Wade-Benzoni, 2009: ''The hot and cool of death awareness at work: Mortality cues, aging, and self-protective and prosocial motivations'', '''Academy of Management Review''', 34: 600-622.
* Grant, 2008: ''The significance of task significance: Job performance effects, relational mechanisms, and boundary conditions'', '''Journal of Applied Psychology''', 93: 108-124.
* Grant, Dutton & Rosso, 2008: ''Giving commitment: Employee support programs and the prosocial sensemaking process'', '''Academy of Management Journal''', 51: 898-918.
* Grant, Campbell, Chen, Cottone, Lapedis & Lee, 2007: ''Impact and the art of motivation maintenance: The effects of contact with beneficiaries on persistence behavior'', '''Organizational Behavior and Human Decision Processes''', 103: 53-67.
* [https://mgmt.wharton.upenn.edu/profile/1323/research List of Publications]

==References==
{{reflist|2}}

== External links ==
{{external media
|audio1= [http://www.onbeing.org/program/adam-grant-successful-givers-toxic-takers-and-the-life-we-spend-at-work/8058 Successful Givers, Toxic Takers, and the Life We Spend at Work], ''On Being,'' October 22, 2015
}}
* [http://www.adamgrant.net/ Official webpage]

{{DEFAULTSORT:Grant, Adam}}
[[Category:Articles created via the Article Wizard]]
[[Category:Wharton School of the University of Pennsylvania faculty]]
[[Category:American male writers]]
[[Category:American non-fiction writers]]
[[Category:Living people]]
[[Category:University of Michigan alumni]]
[[Category:1981 births]]
[[Category:Harvard University alumni]]