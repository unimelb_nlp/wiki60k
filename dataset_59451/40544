{{Advert|date=April 2017}} 

The '''Institute For Digital Archaeology''' (IDA) is a private organization which has collaborated with academics and projects at [[Harvard University]], the [[University of Oxford]] (UK), and Dubai's Museum of the Future. It was founded in 2012 by its current executive director, Roger Michel. The IDA seeks to redefine the field of [[archaeology]] by fusing traditional methods with advanced [[digital image]] capture, [[digital image processing|processing]] and printing technologies.  The organization promotes this new discipline of Digital Archaeology through conferences, publications, interactive internet resources and an extensive range of workshops and certificate programs offered in collaboration with its educational partners.  In all of its work, the IDA encourages the crowd-sourcing of research through the use of open-access databases and other forms of information sharing.

In 2015, the IDA achieved wide international acclaim, including a cover story in the November 11, 2015 issue of ''Newsweek'', for its Million Image Database program aimed at preserving cultural heritage objects and architecture in the conflict zones of the Middle East.

==IDA aims==
In collaboration with its global partners, the IDA promotes cultural heritage preservation and a wider understanding and appreciation of the ancient world. In his address at the World Government Summit in Dubai on 8 February 2016, Michel described the IDA's goals:
:"It is our intention to provide an optimistic and constructive response to destructive threats to history and heritage.  We hope to highlight the potential for the triumph of human ingenuity over violence by offering innovative options for the stewardship of objects and architecture from our shared past.  We also hope to provoke thoughtful dialogue about the significance of heritage material. In particular, we hope that visitors to our photographic archives and reconstruction sites will consider the role of physical objects in defining their history and weigh carefully the question of where precisely history and heritage reside.  Finally, we hope to raise awareness of the human cost of lost heritage and enlist a new generation of scholars, students and volunteers to assist in efforts aimed at historical preservation and study."

== Current projects ==

=== The Triumphal Arch of Palmyra Reconstruction ===
In April 2016, the Institute or Digital Archaeology unveiled a 1/3 scale reproduction of the Triumphal Arch of Palmyra in Trafalgar Square, London. The original arch was destroyed by ISIS in August 2015. Mayor Boris Johnson unveiled the arch at 1:00pm on April 19, 2016. Dr. Mamoun Abdulkarim from DGAM in Syria showed his support for the project and also spoke to organizers and the media about the current state of antiquities in Palmyra, which was liberated several weeks before the unveiling occurred. The event was also attended by His Highness the Sheikh of Dubai, Mohammed bin Rashid Al Maktoum, several ambassadors to the UK and various MPs. The arch was on display in Trafalgar Square for three days, and was fully accessible to the public. Accompanying the exhibition were a series of educational programs including theatre workshops for London primary school children, the Storytiles project, which connected UK schoolchildren to children in Damascus, and demonstrations of 3D modeling from Vertex Modeling.<ref>{{Cite news|url=https://www.nytimes.com/2016/04/20/arts/international/replica-of-palmyra-arch-is-unveiled-in-trafalgar-square.html|title=Palmyra Arch Replica Is Unveiled in Trafalgar Square in London|last=Shea|first=Christopher D.|date=2016-04-19|newspaper=The New York Times|issn=0362-4331|access-date=2016-05-18}}</ref><ref>{{Cite web|url=http://www.cnn.com/2016/04/19/architecture/palmyra-triumphal-arch-replica-london/index.html|title=Palmyra's ancient Triumphal Arch resurrected|last=CNN|first=Sophie Eastaugh, for|website=CNN|access-date=2016-05-18}}</ref>

The unveiling in Trafalgar Square marked the beginning of a series of unveilings for the reconstructed arch. Subsequent displays will take place at the Venice Biennale and in New York City.

===The Million Image Database Program===
After a number of important ancient structures, including the 2000-year-old temple of [[Baalshamin|Baal Shamin]] in [[Palmyra]], [[Syria]], were destroyed in 2015 by [[Islamic State of Iraq and the Levant|Islamic State]] (IS) militants,<ref name="Times">{{cite news|last1=Coghlan|first1=Tom|last2=Moody|first2=Oliver|title=High-tech plan to save ancient sites from Isis|url=http://www.thetimes.co.uk/tto/news/world/middleeast/article4540560.ece|accessdate=24 September 2015|work=The Times|date=28 August 2015}}</ref> the IDA announced its plans to establish a digital record of historical sites and artifacts.<ref>{{cite web|url=http://www.sparpointgroup.com/news/vol13no35-oxford-3d|title=Oxford Deploying 5,000 Modified 3D Cameras to Fight ISIS|author=Sean Higgins|work=sparpointgroup.com}}</ref><ref>{{cite web|url=http://sputniknews.com/middleeast/20150902/1026494356/Middle-East-ruins-digital-preservation.html#ixzz3kgt8e2xo|title=Protecting History: The Digital Project Preserving Artifacts From ISIL Ruin|author=Sputnik|date=2 September 2015|work=sputniknews.com}}</ref><ref name="BBCToday">{{cite news|title=The digital race against IS|url=http://www.bbc.co.uk/programmes/p030ys68|accessdate=24 September 2015|work=BBC Radio 4 "Today" programme|agency=BBC|date=28 August 2015}}</ref><ref name="ArchDaily">{{cite news|last1=Rosenfield|first1=Karissa|title=Harvard and Oxford Take On ISIS with Digital Preservation Campaign|url=http://www.archdaily.com/772902/harvard-and-oxford-take-on-isis-with-digital-preservation-campaign|accessdate=24 September 2015|work=Arch Daily|date=1 September 2015}}</ref>

To accomplish this goal, the IDA, in consultation with [[UNESCO World Heritage Site|UNESCO]] and other partners, has deployed 5,000 3D cameras to partners in the Middle East.<ref name="CNN">{{cite news|last1=Mackay|first1=Mairi|title=Indiana Jones with a 3-D camera? Hi-tech fight to save antiquities from ISIS|url=http://edition.cnn.com/2015/08/28/middleeast/3d-mapping-ancient-monuments/|accessdate=24 September 2015|work=CNN|date=31 August 2015}}</ref> The cameras will be used to capture 3D scans of local ruins and relics.<ref>{{cite web|url=http://observer.com/2015/09/can-3-d-imaging-save-ancient-art-from-isis/|title=Can 3-D Imaging Save Ancient Art from ISIS?|author=Alanna Martinez|work=Observer}}</ref><ref>{{cite web|url=http://on.rt.com/6q3c|title=Scientists to flood Middle East with 1,000s of 3D cameras to ‘save’ ancient sites from ISIS|work=RT English}}</ref><ref name="Forbes">{{cite news|last1=Martin|first1=Guy|title=How England's Institute Of Digital Archeology Will Preserve The Art Isis Wants to Destroy|url=http://www.forbes.com/sites/guymartin/2015/08/31/how-englands-institute-of-digital-archeology-will-preserve-the-art-isis-wants-to-destroy/|accessdate=24 September 2015|work=Forbes|date=31 August 2015}}</ref>  The IDA has established a goal of capturing one million images by the end of 2016.<ref>{{cite web|url=http://www.latimes.com/world/middleeast/la-fg-antiquities-3d-preservation-20150901-htmlstory.html|title=As Islamic State destroys antiquities, a rush to get 3-D images of what's left|author=Los Angeles Times|date=2 September 2015|work=latimes.com}}</ref>  

As part of its Million Image Database project, the IDA has also begun a series of local reconstructions of defaced or destroyed structures using large scale 3D printing and machining technology to commence in 2016.<ref>{{cite web|url=http://3dprint.com/92473/million-image-database-project/|title=The Million Image Database Project: A ‘Flood’ of 3D Cameras to Turn the Tide Against ISIS Vandals|work=3DPrint.com}}</ref> To date, reconstruction projects have occurred in London and been announced for Venice, Oxford, New York City and Dubai.

===Marsoulas Caves with NYU and The University of  Toulouse===
In 2015, in collaboration with Carole Fritz and Giles Tosello of The [[University of Toulouse]], Ben Altshuler of the IDA made the first-ever high resolution scans of paleolithic paintings and inscriptions in the [[Marsoulas]] Cave in the south of France.  Using RTI, the IDA was able to reveal significant, previously unknown features of this important 13,000-year-old rock art site.<ref>{{cite web|url=http://www.britishmuseum.org/whats_on/events_calendar/event_detail.aspx?eventId=110&title=Marsoulas|title=British Museum - Events - Film - Marsoulas, the forgotten cave|work=British Museum}}</ref> The results will be published in the Spring, 2016 edition of the Journal of Digital Archaeology.

===Corpus of Ptolemaic Inscriptions===
Starting in 2013, the IDA began updating Oxford's Corpus of [[Ptolemaic Kingdom|Ptolemaic]] Inscriptions.  Many of the inscriptions, numbering 450 items and dating from 323 – 30 BCE, have eroded over time. It will be the first “full picture of the Greek epigraphy of the Ptolemaic period”.<ref name="digitalarchaeology.org.uk"/><ref>{{cite web|url=http://www.csad.ox.ac.uk/CPI/|title=Centre for the Study of Ancient Documents: Corpus of Ptolemaic Inscriptions|work=ox.ac.uk}}</ref>

== Educational Programs ==

=== Workshops ===
IDA workshops are run in conjunction with the IDA's reconstruction project, accompanying the display of the Triumphal Arch of Palmyra.

==== Gateways ====
With its partners, the IDA is committed to promoting the study of classics, ancient history, and ancient languages in schools. In collaboration with Classics for All, Magdalen College, Oxford, Snap Theatre, and colleagues in the Middle East, the Gateways program targets the cultural education of primary and secondary school children in the UK and the Middle East. Gateways connects children though shared activities exploring themes in culture, language, and ancient history. Projects in the Gateways program include the Storytiles project, a collaborative art project that connects students in the United Kingdom to students in Damascus through the creation of a large mosaic and the Trading Places Poetry and Picture project, which exchanges drawings and poetry between schools in the UK and Syria.<ref>{{Cite web|url=http://digitalarchaeology.org.uk/gateways/|title=Workshops|website=The Institute for Digital Archaeology|access-date=2016-05-18}}</ref>

==== Snap Theatre ====
Snap Theatre ran a taster workshops from series to be rolled out in schools over the next 12 months in the UK and Syria itself. Created by teacher,theatre director and writer Andy Graham who has worked extensively in Rwanda, Romania and Bangladesh, the interactive workshops allowed children explore the stories of how the arc came to be, the significance of Palmyra and even do some creative building of their own using some very special creative techniques.Professional actors and storytellers will inspire the children to want to read and research, to communicate their stories of hidden treasures far and wide.<ref>{{Cite web|url=http://www.snaptheatre.com/news/article/15|title=Summer 2016 / News / Snap Theatre|website=www.snaptheatre.com|access-date=2016-05-18}}</ref>

=== Digital Archaeology Certificate Degree Program ===
The  Institute for Digital Archaeology, in partnership with the University Of Massachusetts and Dubai’s Museum of the Future, will host a certificate degree program in the emerging field of Digital Archaeology. With faculty from Harvard University and the University of Oxford, the program will offer courses in high-technology imaging (including MSI, RTI and 3D), archaeology, museum conservation and object handling, epigraphy, history and ancient languages. As the official course description states:  “A technological revolution is afoot in the field of archaeology. New imaging techniques are allowing experts to document sketch lines under layers of paint, transcribe badly eroded inscriptions, and recover the faintest palimpsests. These powerful new tools are literally shedding new light on ancient objects, leading to stunning breakthroughs in the fields of epigraphy, archaeology, and papyrology.”<ref>{{Cite web|url=http://digitalarchaeology.org.uk/new-page-3/|title=Digital Archaeology Certificate Degree Program|website=The Institute for Digital Archaeology|access-date=2016-05-18}}</ref>

=== Oxford/Sicily Workshop ===
The IDA works with Harvard University Department of the Classics to send students from the Classical Civilization module to Sicily for a week each year. While in Sicily, the students conduct RTI imaging on sites at Motya, Siracusa and Morgantina. Originally begun in 2013 for graduate students, the course is now in its third year and is aimed at undergraduates.<ref>{{Cite web|url=http://isites.harvard.edu/course/colgsas-68089|title=Course Website Locator: 68089|website=isites.harvard.edu|access-date=2016-05-18}}</ref>

Beginning in 2016, the program will expand to Oxford. In the Oxford/Sicily workshops students will explore the archaeology of Sicily between the Bronze Age and the Norman conquest using state-of-the art digital imaging technology. Participants travel to Oxford where they will reside at Trinity College, one of Oxford's most beautiful constituent colleges. There they will work with Oxford University faculty and IDA staff developing the technical skills, language proficiency and historical perspectives necessary to make the most of the field work in Sicily that will occur during the final week of the program. The fieldwork will focus on the multi-cultural history of Sicily as a meeting place of Eastern and Western influences and as a center for trade in material goods and ideas.  While in Sicily, students will participate in workshops in digital imaging technology, including especially RTI, and will have the chance to develop their own image portfolios at both the Aidone Museum and in the Catania Amphitheater.  All imaging equipment will be supplied by the IDA. Students do not need to bring any special materials.  In Sicily, the daily workshops will be supplemented by lectures covering the main historical and archaeological features of the various sites visited.[http://digitalarchaeology.org.uk/new-page-2/]

== Past projects ==

===The Philae obelisk at Kingston Lacy===
In October and November 2014, the IDA, in association with [[Alan Bowman]], Ben Altshuler, and Charles Crowther of Oxford's Centre for the Study of Ancient Documents (CSAD), made [[Polynomial texture mapping|reflectance transformation imaging]] (RTI) scans of the [[Philae obelisk]] at [[Kingston Lacy]]. The team recovered significant, previously illegible Greek and Egyptian inscriptions.<ref name="digitalarchaeology.org.uk">http://digitalarchaeology.org.uk/projects</ref><ref>http://www.space.com/27835-philae-obelisk-and-lander-revealing-secrets-with-digital-tech.html)</ref>

===RTI Imaging of The Parian Marble===
In 2013, the IDA completed the RTI imaging of the [[Parian Chronicle|Parian Marble]] at the [[Ashmolean Museum]]. Hellenistic in origin, the Parian Marble is the earliest Greek chronological table detailing events of historical importance, specifically from 1582 BC to 299 BC. The scans revealed significant, previously illegible text.<ref name="digitalarchaeology.org.uk" />

=== The Vatican Library ===
The Institute for Digital Archaeology headed a project to transcribe a recently discovered, [[palimpsest]]ed text of [[Menander]]'s lost comedy ''The Wet-Nurse'' in the [[Vatican Library]]. Ben Altshuler assisted a team that included Nigel Wilson of Oxford University and Michael Phelps of EMEL by providing technical and epigraphic support in the transcription process. The project yielded a forthcoming edition of the poem to be published by the [[Vatican Publishing House]] in 2016.

===APA Oral History Project===
In 2013-2014, the IDA, in collaboration with [[Ward W. Briggs]] (until 2011 ''Carolina Distinguished Professor of Classics'' and ''Louise Fry Scudder Professor of Humanities'' at the [[University of South Carolina]]), recorded dozens of hour-long oral histories featuring leaders in the field of classics over the past 50 years. These were premiered at the 2014 [[American Philological Association]]. The IDA is now committed to adding further oral histories at the rate of approximately ten per year.<ref name="digitalarchaeology.org.uk"/>

===Database of Classical Scholars with University of South Carolina===
In partnership with Ward Briggs, the IDA has also created an online database of biobibliographies of noted deceased classical scholars from the eighteenth-century to the present.<ref name="digitalarchaeology.org.uk"/>

== Partners ==
*University of Massachusetts at Boston
*University of Oxford (UK)
*Museum of the Future, Dubai, UAE
*Institute for the Study of the Ancient World (ISAW)<ref>{{cite web|url=http://www.isaw.nyu.edu|title=Institute for the Study of the Ancient World|work=Institute for the Study of the Ancient World}}</ref>
*Classics for All
*UNESCO World Heritage<ref>{{cite web|url=http://en.unesco.org|title=UNESCO|work=UNESCO}}</ref>
*The Friends of Herculaneum Society<ref>{{cite web|url=http://www.herculaneum.ox.ac.uk|title=The Friends of Herculaneum Society|work=ox.ac.uk}}</ref>
*Harvard University Dept. of Classics<ref>{{cite web|url=http://www.classics.fas.harvard.edu|title=Department of the Classics|work=harvard.edu}}</ref>
*The Classics Conclave 
*Centre for the Study of Ancient Documents (CSAD)<ref>{{cite web|url=http://www.csad.ox.ac.uk|title=Centre for the Study of Ancient Documents|work=ox.ac.uk}}</ref>
*The Iris Project<ref>{{cite web|url=http://irisproject.org.uk/|title=The Iris Project|work=irisproject.org.uk}}</ref><ref>{{cite web|url=http://www.eoccc.org.uk/|title=East Oxford Community Classics Centre|work=eoccc.org.uk}}</ref>
*Kallos Gallery<ref>{{cite web|url=http://www.kallosgallery.com|title=Kallos Gallery|work=Kallos Gallery}}</ref>

==Publications==
*{{cite journal|last1=Altshuler|first1=Ben F S|last2=Mannack|first2=Thomas|title=Shedding New Light on Ancient Objects|journal=Arion: A Journal of Humanities and the Classics|date=2014|volume=22|issue=1|pages=53–74|doi=10.2307/arion.22.1.0053|jstor=arion.22.1.0053}}
*"Presentation on Digital Imaging in Archaeology", APA Conference 2015 
*''The Future of Digital Archaeology'', World Government Summit 2016

==External links==
*[http://www.digitalarchaeology.org.uk Official website]
*[http://www.millionimage.org.uk/ Million Image Database]
*[http://www.prospectmagazine.co.uk/world/islamic-state-how-to-save-syrias-antiquities Prospect Magazine: Islamic State: How to save Syria's Antiquities]
*[https://www.nytimes.com/aponline/2015/09/23/world/middleeast/ap-ml-syria-saving-heritage.html?ref=world&_r=0 The New York Times: Racing Against Militant Threat to Document Syria's Heritage]

==References==
{{reflist}}

[[Category:Archaeological organisations]]
[[Category:2012 establishments in the United Kingdom]]