'''Library publishing''', also known as '''campus-based publishing''',<ref>[http://www.sparc.arl.org/initiatives/campus-based-publishing]</ref> is the practice of an academic library providing [[academic publishing|publishing]] services.

==Concept==
A library publishing service usually publishes [[academic journals]] and often provides a broader range of publishing services as well.<ref>{{cite web|title=Library publishing services: strategies for success: final research report|url=https://docs.lib.purdue.edu/purduepress_ebooks/24/|publisher=SPARC|accessdate=24 September 2013|author=Mullin, J.L.|author2=Murray-Rust, C. |author3=Ogburn, J.L. |author4=Crow, R. |author5=Ivins, O. |author6=Mower, A. |author7=Nesdill, D. |author8=Newton, M.P. |author9=Speer, J. |author10=Watkinson, C. |page=6|year=2012}}</ref> This can include publishing other formats such as scholarly [[monographs]] and [[conference proceedings]].<ref>{{cite web|last=Hahn|first=Karla L.|title=Research library publishing services: new options for university publishing|page=5|url=http://www.arl.org/storage/documents/publications/research-library-publishing-services-mar08.pdf|publisher=Association of Research Libraries|accessdate=24 September 2013|year=2008}}</ref> It generally has a preference for [[open access]] publishing.<ref name=LPC>{{cite web|title=About us|url=http://www.librarypublishing.org/about-us|work=Library Publishing Coalition|accessdate=24 September 2013}}</ref>

Library publishing often focuses on electronic publishing rather than print, thus complemeting the role of traditional academic presses.<ref>{{cite journal|last=Harboe-Ree|first=Cathrine|title=Just advanced librarianship: the role of academic libraries as publishers|journal=Australian Academic & Research Libraries|year=2007|volume=38|issue=1|page=21|doi=10.1080/00048623.2007.10721264| url=http://www.tandfonline.com/doi/abs/10.1080/00048623.2007.10721264|accessdate=24 September 2013}}</ref>  Sometimes a library and a [[university press]] based at the same institution will form a partnership, with each focusing on their own area of expertise.<ref name=Hahn>{{cite web|last=Hahn|first=Karla L.|title=Research library publishing services: new options for university publishing|url=http://www.arl.org/storage/documents/publications/research-library-publishing-services-mar08.pdf|publisher=Association of Research Libraries|accessdate=24 September 2013|year=2008}}</ref><ref>{{cite journal|last=Wittenberg|title=Librarians as publishers: a new role in scholarly communication.|journal=Searcher|year=2004|volume=12|issue=10}}</ref> For example, the [[University of Pittsburgh]] library publishing service publishes peer-reviewed journals and also collaborates with the university press to publish [[open access monographs]].<ref>{{cite web|title=The university library system, University of Pittsburgh: how & why we publish.|url=http://opensuny.org/omp/index.php/IDSProject/catalog/book/25|work=Library publishing toolkit|publisher=IDS Project Press|accessdate=24 September 2013|author=Deliyannides, T.S.|author2=Gabler, V.E. |page=82|year=2013}}</ref>

Software is available to manage the journal publication process. The open source [[Open Journal Systems]] by the [[Public Knowledge Project]], and [[Digital Commons]]' [[bepress]], are both widely used by library publishing services.<ref>{{cite web|last=Hahn|first=Karla L.|title=Research library publishing services: new options for university publishing|page=14|url=http://www.arl.org/storage/documents/publications/research-library-publishing-services-mar08.pdf|publisher=Association of Research Libraries|accessdate=24 September 2013|year=2008}}</ref> Some libraries use Open Journal Systems to create [[overlay journal]]s which present scholarly content that is held in an [[institutional repository]].<ref>{{cite web|last=Brown|first=Josh|title=An introduction to overlay journals|url=http://discovery.ucl.ac.uk/19081/1/19081.pdf|publisher=UCL|accessdate=24 September 2013|year=2009}}</ref>

==History==
Library publishing has a long history and has been around since before the Internet.<ref>Maxim, G.E. 1965. A history of library publishing, 1600 to the present day. Thesis approved for Fellowship of the Library Association.</ref>

The Synergies project (2007-2011) was a collaboration between different Canadian universities to create infrastructure to support institutional publishing activities.<ref>{{cite journal|last=Devakos|first=R.|author2=Turko, K. |title=Synergies: building national infrastructure for Canadian scholarly publishing|journal=ARL Bi-Monthly|year=2007|volume=252/253|page=16|url=http://www.arl.org/storage/documents/publications/arl-br-252-253.pdf}}</ref> A survey conducted by Hahn in 2008 found that at that time 65% of research libraries in North America either had a library publishing service or were considering creating one.<ref name=Hahn />

In 2011 in the UK, [[Jisc]] funded three library publishing projects: Huddersfield Open Access Publishing (HOAP) at the [[University of Huddersfield]], SAS Open Journals at the [[University of London]], and EPICURE at [[University College London|UCL]].<ref>{{cite web|title=Scholarly communications|url=http://www.jisc.ac.uk/whatwedo/programmes/inf11/inf11scholcomm.aspx|work=Jisc|accessdate=24 September 2013}}</ref>

The Library Publishing Coalition was launched in 2013 to provide a hub for library publishing activities.<ref name=LPC /> In October 2013, during [[Open Access Week]], they launched a Library Publishing Directory<ref>{{cite web|title=Library Publishing Directory|url=http://www.librarypublishing.org/sites/librarypublishing.org/files/documents/LPC_LPDirectory2014.pdf|work=Library Publishing Coalition|accessdate=23 October 2013|author=Lippincott, Sarah K. (ed.)|year=2013}}</ref> which contains information about library publishing activities at 115 academic and research libraries.<ref>{{cite web|title=Library Publishing Directory: Announcing the 1st edition of the Library Publishing Directory|url=http://www.librarypublishing.org/resources/directory-library-publishing-services|work=Library Publishing Coalition|accessdate=23 October 2013|year=2013}}</ref>

==See also==
{{main cat}}
{{category see also|Academic journals published by university libraries}}
* [[University press]]

==References==
{{reflist|2}}

==Further reading==
* {{cite web |title=What’s Going on in the Library? Part 1: Librarian Publishers May Be More Important Than You Think |author=Phil Jones |publisher=[[The Scholarly Kitchen]] |date=Dec 1, 2014 |url=http://scholarlykitchen.sspnet.org/2014/12/01/whats-going-on-in-the-library-part-1-librarian-publishers-may-be-more-important-than-you-think/}}
* {{cite web |title=What’s Going on in the Library? Part 2: The Convergence of Data Repositories and Library Publishers |author=Phil Jones |publisher=[[The Scholarly Kitchen]] |date=Dec 9, 2014 |url=http://scholarlykitchen.sspnet.org/2014/12/01/whats-going-on-in-the-library-part-2-librarian-publishers-may-be-more-important-than-you-think/}}

==External links==
{{Library resources box}}
*[http://www.librarypublishing.org Library Publishing Coalition]
*[http://www.sparc.arl.org/resources/publishing Campus-based Publishing Resource Center]

[[Category:Academic publishing]]
[[Category:Open access (publishing)]]
[[Category:Library publishing| ]]
[[Category:Electronic publishing]]