'''Project Euclid''' is a collaborative partnership between [[Cornell University Library]] and [[Duke University Press]] which seeks to advance scholarly communication in [[Theoretical mathematics|theoretical]] and [[applied mathematics]] and [[statistics]] through partnerships with independent and society publishers. It was created to provide a platform for small publishers of [[scholarly journal]]s to move from print to electronic in a cost-effective way.<ref name=Crow>{{cite book |last=Crow|first=Raym |title=Campus-Based Publishing Partnerships: A Guide to Critical Issues |url=http://www.arl.org/sparc/partnering/guide/ |year=2009 |location=Washington, DC |publisher=[[Scholarly Publishing and Academic Resources Coalition|SPARC]] |accessdate=17 February 2012}}</ref>

Through a combination of support by subscribing libraries and participating publishers, Project Euclid has made 70% of its journal articles available as [[open access]]. As of 2010, Project Euclid provided access to over one million pages of open-access content.<ref>{{cite web |title=Project Euclid Reaches 1 Million Pages |url=http://dukeupress.typepad.com/dukeupresslog/2010/06/project-euclid-reaches-1-million-pages.html |accessdate=17 February 2012}}</ref>

==Mission and goals==
Project Euclid's stated mission is to advance scholarly communication in the field of theoretical and applied mathematics and statistics.<ref name=Euclid/> Through a "mixture of open access, subscription, and hosted subscription content it provides a way for small publishers (especially societies) to host their math or statistics content".<ref name=PAM>{{cite web |title=2011 PAM Division Award |url=http://pam.sla.org/2011/06/2011-pam-division-award/ |publisher=SLA |accessdate=17 February 2012}}</ref>

==History==
In 1999, Cornell University Library received a grant from the [[Andrew W. Mellon Foundation]] for the development of an online publishing service designed to support the transition for small, non-commercial [[mathematics journal]]s from paper to digital distribution.<ref>{{cite journal |last=Ehling |first=Terry |author2=Erich Staib  |title=The Coefficient Partnership: Project Euclid, Cornell University Library and Duke University Press |journal=Against the Grain |date=December 2008 – January 2009 |volume=20 |issue=6}}</ref> [[Duke University Press]], which had experience in putting its own math journals online and a similar interest in assisting non-commercial math journals, worked as Cornell's partner in developing the grant application and then in developing Project Euclid's publishing model.

Cornell launched Project Euclid in May 2003 with nineteen journals. In July 2008, Cornell University Library and Duke University Press established a joint venture and began co-managing Project Euclid. Duke assumed responsibility for "marketing, financial, and order fulfillment workflows" while Cornell continued to provide and support Project Euclid's IT infrastructure.<ref name=Crow/>

Currently, Project Euclid hosts both [[open access journals]] and [[monograph]]s, as well as its Prime collection of [[Peer review|peer-reviewed]] titles. Currently, there are over 60 journal titles from the United States, Japan, Europe, Brazil, and Iran. Euclid’s holdings as of February 2012 include: 110,400 journal articles from 64 titles, 162 monographs, and 23 [[conference proceedings]] volumes.<ref name=Euclid>{{cite web |title=Project Euclid |url=http://projecteuclid.org |accessdate=17 February 2012}}</ref>

In 2011, Project Euclid received the 2011 Division Award from the Physics-Astronomy-Mathematics Division of the [[Special Libraries Association]]. Given annually, this award recognizes significant contributions to the literature of physics, mathematics, or astronomy, and honors work that demonstrably improves the exchange of information within these three disciplines. The award also takes into consideration projects that benefit libraries.<ref name=PAM/>

==See also==
*[[DPubS]]
*[[D-Scribe Digital Publishing]]

==References==
{{reflist}}

==External links==
{{Official website|http://projecteuclid.org/}}

[[Category:Academic journal online publishing platforms]]
[[Category:Open access projects]]