{{Infobox journal
| title = Academy of Management Journal
| formernames = Journal of the Academy of Management 
| cover = [[File:AMJ cover 2012.jpg|200px]]
| discipline = [[Management]]
| abbreviation = Acad. Manag. J.
| editor = Gerard George
| publisher = [[Academy of Management]]
| country = United States
| frequency = Bimonthly
| history = 1958–present
| impact = 5.910
| impact-year = 2012
| openaccess =
| website = http://aom.org/amj/
| link1 = http://amj.aom.org/content/current
| link1-name = Online access
| link2 = http://amj.aom.org/content/by/year
| link2-name = Online archive
| ISSN = 0001-4273
| eISSN = 1948-0989
| OCLC = 803928135
| LCCN = 58003817
}}
The '''''Academy of Management Journal''''' a is [[peer-reviewed]] [[academic journal]] covering all aspects of [[management]]. It is published by the [[Academy of Management]] and was established in 1958 as the ''Journal of the Academy of Management'', obtaining its current name in 1963.<ref name=LC>{{cite web |url=http://lccn.loc.gov/58003817 |title=Academy of Management Journal |work=Library of Congress Catalog |publisher=[[Library of Congress]] |format= |accessdate=2014-05-01}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 5.919, ranking it third out of 172 journals in the category "Management"<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Management |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref> and third out of 116 journals in the category "Business".<ref name=WoS1>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Business |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref> In 2012 the journal was listed as one of the top 10 offenders in a practice called "[[coercive citation]]", wherein publishers manipulate their [[impact factor]]s to artificially boost their academic reputation.<ref>{{cite journal |doi=10.1126/science.1212540 |title=Coercive Citation in Academic Publishing |year=2012 |last1=Wilhite |first1=A. W. |last2=Fong |first2=E. A. |journal=Science |volume=335 |issue=6068 |pages=542–543 |pmid=22301307}}</ref> It is also on the ''[[Financial Times]]'' list of 45 journals used to rank business schools<ref>{{cite web |url=http://www.ft.com/cms/s/2/3405a512-5cbb-11e1-8f1f-00144feabdc0.html |title=45 Journals used in FT Research Rank |publisher=[[Financial Times]] |date=2012-02-22 |accessdate=2013-03-10}}</ref> and is one of the four general management journals that the [[University of Texas Dallas]] uses to rank the research productivity of universities.<ref>{{cite web|url=http://jindal.utdallas.edu/the-utd-top-100-business-school-research-rankings/rankings-by-journal/ |title=Rankings by Journal - Naveen Jindal School of Management - The University of Texas at Dallas |publisher=Jindal.utdallas.edu |date= |accessdate=2013-03-10}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://aom.org/amj/}}

{{DEFAULTSORT:Academy Of Management Journal}}
[[Category:Business and management journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1958]]
[[Category:English-language journals]]


{{management-journal-stub}}