{{Infobox journal
| title = Genetica
| cover =
| discipline = [[Genetics]], [[evolutionary biology]]
| abbreviation = Genetica
| editor = Pierre Capy, Ronny C. Woodruff
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Monthly
| history = 1919-present
| impact = 2.148
| impact-year = 2011
| openaccess =  [[Hybrid open access journal|Hybrid]]
| website = http://www.springer.com/life+sciences/journal/10709
| link1 = http://link.springer.com/journal/volumesAndIssues/10709
| link1-name = Online access
| ISSN = 0016-6707
| eISSN = 1573-6857
| OCLC = 704024115
| LCCN = 23013908
| CODEN = GENEA3
}}
'''''Genetica''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering research in [[genetics]] and [[evolutionary biology]]. It was established in January 1919 by [[Kluwer Academic]] (which later merged into Springer) and originally published articles in English, Dutch, French, and German. Publication was suspended from 1944 to 1946.<ref>{{cite web |title=Genetica |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/?term=0016-6707 |publisher=[[National Library of Medicine]] |work=NLM Catalog |accessdate=2013-04-02}}</ref> The journal allows [[self-archiving]] and [[Author-pays model|authors can pay]] extra for [[Open access (publishing)|open access]]. The [[editors-in-chief]] are Pierre Capy ([[French National Centre for Scientific Research]], [[Gif-sur-Yvette]]) and Ronny C. Woodruff ([[Bowling Green State University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[PubMed]]/[[MEDLINE]]
* [[Scopus]]
* [[EMBASE]]
* [[Chemical Abstracts Service]]
* [[InfoTrac|Academic OneFile]]
* [[AGRICOLA]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Biotechnology Citation Index]]
* [[CAB Abstracts]]
* [[Current Contents]]/Life Sciences
* [[Elsevier BIOBASE]]
* [[EMBiology]]
* [[INIS Atomindex]]
* [[The Zoological Record]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 2.148.<ref name=WoS>{{cite book |year=2011 |chapter=Genetica |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.springer.com/life+sciences/journal/10709}}

[[Category:Genetics journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1919]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]