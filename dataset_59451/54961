{{Infobox settlement
| name                = Gulripshi
| native_name         = {{lang|ka|გულრიფში}}, {{lang|ab|Гәылрыҧшь}}
| native_name_lang = <!-- Dealt with through language templates-->
| settlement_type     = Urban Type Settlement
| pushpin_map         = Gulripshi#Abkhazia#Georgia
| coordinates         = {{coord|42.931418|N|41.106128|E|region:Abkhazia|display=inline,title}}
| subdivision_type    = Country
| subdivision_name    = [[Abkhazia]] (''[[de facto]]'') <br> [[Georgia (country)|Georgia]] (''[[de jure]]'')
| subdivision_type1   = District
| subdivision_name1   = [[Gulripshi District]]
| subdivision_type2   = 
| subdivision_name2   = 
| population_total    = 3910<ref>http://www.ethno-kavkaz.narod.ru/gulripsh11.html {{ru icon}}</ref> 
| population_as_of    = 2011
}}
'''Gulripshi''' ({{lang-ka|გულრიფში}}, {{IPA-ka|gulripʰʃi||Gulripshi.ogg}}; {{lang-ab|Гәылрыҧшь}}, ''Gwylryphsh''; {{lang-ru|Гульрыпш}}, ''Gulrypsh'') is an [[urban settlement]] in [[Abkhazia]].<ref group="Note">{{Abkhazia-note}}</ref> It is located 12&nbsp;km from [[Sukhumi]], and is the capital of [[Gulripshi district]].

Nikolay Smetskoy built three sanatoria in Gulripsh between 1902 and 1913 for patients with pulmonary diseases and founded several parks with subtropical plants.<ref>"Наше Наследие" № 63-64 2002. [http://www.nasledie-rus.ru/podshivka/6421.php Творец зеленой Третьяковки] {{ru icon}}</ref> After the [[Russian Revolution (1917)|Russian Revolution]] the sanatoria were nationalised.

==Climate==
Gulripshi has a [[humid subtropical climate]] ([[Köppen climate classification|Köppen]]: ''Cfa'').

{{Weather box
|location = Gulripshi
|metric first = yes
|single line = yes
|Jan mean C = 5.7
|Feb mean C = 6.3
|Mar mean C = 9.3
|Apr mean C = 12.6
|May mean C = 17.1
|Jun mean C = 20.7
|Jul mean C = 23.4
|Aug mean C = 23.9
|Sep mean C = 20.6
|Oct mean C = 16.7
|Nov mean C = 11.9
|Dec mean C = 8.2
|year mean C = 
|Jan precipitation mm = 126
|Feb precipitation mm = 107
|Mar precipitation mm = 118
|Apr precipitation mm = 124
|May precipitation mm = 97
|Jun precipitation mm = 122
|Jul precipitation mm = 117
|Aug precipitation mm = 132
|Sep precipitation mm = 136
|Oct precipitation mm = 137
|Nov precipitation mm = 140
|Dec precipitation mm = 151
|year precipitation mm = 
|source 1 = Climate-Data.org<ref name = "Climate Data">{{cite web|url=http://en.climate-data.org/location/59346/|title=Climate: Gulripshi|accessdate = 7 April 2014
|publisher = Climate-Data.org}}</ref>
|date = April 2014
}}

[[File:Smetskoy sanatorium.jpg|thumb|right|Smetskoy's sanatorium (postcard)]]

== References ==
{{Reflist|group="Note"}}
{{Reflist}}
{{Cities and towns in Georgia (country)}}
{{Administrative divisions of Abkhazia}}

[[Category:Populated places in Gulripshi District]]


{{abkhazia-geo-stub}}
{{georgia-geo-stub}}