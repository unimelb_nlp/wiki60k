<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Arab
 |image=SunbeamArab.JPG          
 |caption=Preserved Sunbeam Arab
}}{{Infobox Aircraft Engine
|type =V-8, 90 degree, water-cooled, piston engine
<!-- The type of engine, eg: Air-cooled rotary, High-bypass turbofan -->
|national origin =[[United Kingdom|Britain]]
<!-- The country where the engine was first manufactured. Use the main nation (ie. UK), not constituent country (England); don't use "EU". List collaborative programs of only 2 or 3 nations; for more than 3, use "Multi-national -->
|manufacturer =[[Sunbeam Motor Car Company|Sunbeam]]
<!-- The firm which manufactured the engine. -->
|designer =[[Louis Coatalen]]<ref name="Brew">{{cite book|last=Brew|first=Alec|title=Sunbeam Aero-engines|publisher=Airlife Publishing|location=Shrewsbury|date=1998|isbn=1-84037-023-8}}</ref>
<!-- The person or persons who designed the engine. Only appropriate for single designers, not project leaders -->
|first run =1916<ref name="Brew"/>
<!-- The date that the engine was first run. If this hasn't happened, skip this field - don't speculate. -->
|major applications =[[Bristol Scout F]]<ref name="Brew"/>
<!-- aircraft (or other vehicles) that were powered by this engine -->
|produced =1917–1918<ref name="Brew"/>
<!-- Years in production (eg. 1970-1999) if still in active use but no longer built -->
|number built =6,110 ordered 1,311 built<ref name="Brew"/>
|program cost =
<!-- Total program cost -->
|unit cost =
<!-- Incremental or flyaway cost for military or retail price for commercial aircraft -->
|developed from =
<!-- The engine which formed the basis for this engine -->
|variants with their own articles =[[Sunbeam Dyak]] <br/> [[Sunbeam Pathan]]
<!-- Variants OF this engine -->
}}
|}

The '''Sunbeam Arab''' was a British First World War era [[aircraft engine|aero engine]].<ref name="Brew"/>

==Design and development==
By 1916 the demand for aero-engines was placing huge demands on  manufacturing. To help ease the pressure the [[War Office]] standardised on engines of about {{convert|200|hp|kW|0|abbr=on}}; one of these was a V-8 water-cooled engine from Sunbeam known as the Arab. Using cast aluminium alloy cylinder blocks and heads with die-cast aluminium alloy pistons, the Arab had a bore of {{convert|120|mm|in|2|abbr=on|disp=flip}} and stroke of {{convert|130|mm|in|2|abbr=on|disp=flip}} for a capacity of {{convert|11.762|l|cuin|2|abbr=on|disp=flip}}, developing {{convert|208|-|212|hp|kW|0|abbr=on}} at 2,000 rpm.<ref name="Brew"/>

First bench-run in 1916, the Arab was obviously inspired by the Hispano-Suiza V-8 engines but with very little in common when examined in detail. After submission to the Internal Combustion Engine Committee of the [[National Advisory Committee]] Sunbeam received an order for 1,000 in March 1917, increased to 2,000 in June 1917 as well as another 2,160 to be built by [[Austin Motors]] (1,000), [[Lanchester Motor Company]] (300), [[Napier & Son]] (300), and [[Willys Overland]] (560) in the [[United States of America]]. Bench testing revealed defects which required rectification, delaying completion of production drawings. Despite the delays one of the first flight-ready Arabs flew in a [[Martinsyde F.2]] two-seat fighter/reconnaissance aircraft in mid 1917.<ref name="Brew"/>

Service use of the Arab was limited because of poor reliability and persistent vibration problems, causing some 2,350 orders to be cancelled and remaining orders 'settled', compensating manufacturers for costs incurred.<ref name="Brew"/>

Developed from the Arab were the inverted V-8 '''Sunbeam Bedouin''', straight six '''Sunbeam Dyak''', W-12 '''Sunbeam Kaffir''', and 20 cylinder radial '''Sunbeam Malay'''.<ref name="Brew"/>

==Variants==
;Arab
:The production engine loosely based on the [[Hispano-Suiza 8]] V-8 engines.<ref name="Brew"/>
;Bedouin
:In common with many other contemporary engine manufacturers the Arab was re-designed to run inverted and given the name '''Sunbeam Bedouin'''. Intended to provide better forward visibility for single-engined aircraft there is no evidence that the Bedouoin was fitted to an aircraft or flew.<ref name="Brew"/>
;Kaffir
:A W-12 broad arrow engine using blocks, heads and valve-gear from the Arab, giving {{convert|300|hp|kW|0|abbr=on}}. Bore remained the same at {{convert|120|mm|in|2|abbr=on}}, but with a stroke of {{convert|135|mm|in|2|abbr=on}}.<ref name="Brew"/>
;[[Sunbeam Dyak|Dyak]]
:A straight six extrapolation of the Arab retaining the {{convert|120|mm|in|2|abbr=on}} stroke and {{convert|130|mm|in|2|abbr=on}} bore of the Arab, but with only two valves per cylinder as opposed to the three valves on the Arab.<ref name="Brew"/>
;[[Sunbeam Pathan|Pathan]]
:Coatalen expressed his interest in diesel engines by designing a diesel derivative of the Dyak with the same attributes, developing {{convert|100|hp|kW|0|abbr=on}} at 1,500&nbsp;rpm. Only prototypes of the Pathan were built.
;Malay
:The '''Sunbeam Malay''' was a 20-cylinder radial aircraft engine of {{convert|29.4|l|cuin|0|abbr=on}} capacity manufactured by [[Sunbeam Motor Car Company|Sunbeam]] using five four-cylinder blocks from the Arab arranged around a central crankshaft. The Malay retained the {{convert|120|mm|in|2|abbr=on}} × {{convert|130|mm|in|2|abbr=on}} bore and stroke of the Arab, as well as the three valves per cylinder and overhead cam shafts. Nominally rated at {{convert|500|hp|kW|0|abbr=on}}, the Malay was not put into production.<ref name="Brew"/>

==Applications==
''Data from Brew''.<ref name="Brew"/>
{{colbegin||22em}}
* [[Armstrong-Whitworth F.K.10]]
* [[Avro 530]]
* [[Bristol F.2b Fighter]]
* [[Bristol Scout F]]
* [[Fairey F.2a]]
* [[Fairey N.2a]]
* [[Grain Griffin]]
* [[Martinsyde F.2]]
* [[Norman Thompson N.2c]]
* [[Norman Thompson NT.2b]]
* [[Royal Aircraft Factory AE.3 Ram]]
* [[Royal Aircraft Factory SE.5a]]
* [[Sage 4B Seaplane Trainer]]
* [[Short Improved Navyplane]]
* [[Sopwith Cuckoo]]
* [[Sunbeam 1917 Bomber]]
* [[Supermarine Baby]]
{{colend}}

==Specifications (Arab I)==
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=''Sunbeam Aero-Engines.''<ref name="Brew"/>
|type=8-cylinder, upright, 90 degree Vee engine
|bore={{convert|4.72|in|mm|abbr=on}}
|stroke={{convert|5.12|in|mm|abbr=on}}
|displacement={{convert|717.77|cuin|l|2|abbr=on}}
|length={{convert|43.5|in|mm|abbr=on}}
|diameter=
|width={{convert|31.9|in|mm|abbr=on}}
|height={{convert|35.5|in|mm|abbr=on}}
|weight={{convert|530|lb|kg|0|abbr=on}}
|valvetrain=single overhead camshaft, three poppet valves/cylinder (one inlet, two exhaust)
|supercharger=
|turbocharger=
|fuelsystem=Single [[Claudel-Hobson]] carburettor
|fueltype=
|oilsystem=
|coolingsystem=Liquid-cooled
|power={{convert|208|hp|kW|0|abbr=on}} at 2,000&nbsp;rpm (takeoff power)
|specpower=
|compression=5.3:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=

|designer=
|reduction_gear=0.6:1, Left-hand tractor/Right Hand pusher

|general_other=
|components_other=
|performance_other=
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Hispano-Suiza 8]]
*[[Wolseley Viper]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{reflist}}

===Bibliography===
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* Brew,  Alec. ''Sunbeam Aero-Engines''. Airlife Publishing. Shrewsbury. ISBN 1-84037-023-8

==External links==
* {{Cite web
  |title=Sunbeam Overhead Cam Engines: The Arab
  |url=http://www.localhistory.scit.wlv.ac.uk/Museum/Transport/planes/SunbeamEngines7.htm
  |website=Wolverhampton Museum of Industry
  |archiveurl=https://web.archive.org/web/20080601032610/http://www.localhistory.scit.wlv.ac.uk/Museum/Transport/planes/SunbeamEngines7.htm
  |archivedate=2008
}}
* {{Cite web
  |title=Sunbeam Overhead Cam Engines
  |url=http://www.localhistory.scit.wlv.ac.uk/Museum/Transport/planes/SunbeamEngines10.htm
  |website=Wolverhampton Museum of Industry
  |archiveurl=https://web.archive.org/web/20080601085446/http://www.localhistory.scit.wlv.ac.uk/Museum/Transport/planes/SunbeamEngines10.htm
  |archivedate=2008
}}

{{Sunbeam aeroengines}}

[[Category:Sunbeam aircraft engines|Arab]]
[[Category:Aircraft piston engines 1910–1919]]