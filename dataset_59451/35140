{{Use British English|date=May 2014}}
{{good article}}
{{Infobox Horse race
|pagename=1993 Grand National
|horse race=[[Grand National]]
|location=[[Aintree Racecourse|Aintree]]
|date=3 April 1993
|winning horse= None
|winning jockey= None
|winning trainer = None
|winning owner = None
|conditions= Good to firm<ref name=results>[http://www.racingpost.com/horses/result_home.sd?race_id=51751&r_date=1993-04-03  3:50 Aintree Result], ''Racing Post'', 3 April 1993</ref>
|previous        = [[1992 Grand National|1992]]
|next            = [[1994 Grand National|1994]]
}}
{{ external media
| align  = right
| width  = 275px
| video1 = [https://www.youtube.com/watch?v=0t9a8YtgdGM All the 1990s Grand Nationals in full] Racing UK, BBC Sport, YouTube
}}
The '''1993 Grand National''' (officially the [[Martell (cognac)|Martell]] Grand National Chase Handicap Grade 3) was scheduled on 3 April 1993 to be the 147th running of the [[Grand National]] [[Horse racing|horse race]], held annually at [[Aintree Racecourse]] near [[Liverpool]], England.<ref name=results/>

It was the first and so far only time that the [[steeplechase]] was declared void, after 30 of the 39 runners began and carried on racing despite there having been a [[false start]]. Seven of the field even went on to complete the course, with Esha Ness crossing the finishing post first, in what would have been the second-fastest time ever.<ref name=BBC>On this day: 3 April 1993 -[http://news.bbc.co.uk/onthisday/hi/dates/stories/april/3/newsid_4216000/4216143.stm Grand National ends in 'shambles'], ''bbc.co.uk''</ref>

The [[Jockey Club]] decided not to re-run the race, and as a result it has often been called ''the race that never was''.<ref name=independent>Paul Hayward, [http://www.independent.co.uk/sport/racing-grand-national-1993-day-of-disaster-for-national-history-made-as-worlds-greatest-steeplechase-is-declared-void-1453141.html "Racing: Day of disaster for National"], ''[[The Independent]]'', 4 April 1993</ref><ref name=guardian>John White, "[https://www.theguardian.com/sport/2010/apr/03/esha-ness-1993-grand-national 3 April 1993: Esha Ness 'wins' the Grand National that never was]", ''The Guardian'', 3 April 2010</ref> Bookmakers were forced to refund an estimated £75 million in bets staked.<ref name=BBC/><ref name=guardian/> The Jockey Club launched an inquiry which led to a number of changes in the starting and recall procedures in future races.<ref name=independentinquiry/>

==Circumstances==
===False start===
{{Quote box
 |quote  = And they're away — oh, and once again the tape has snagged, and it's a recall... It was caught round Richard Dunwoody's neck, the tape. And they've been recalled — but the majority don't realise that it is a recall! They're going down to jump the first, they're going to!
 |source = The [[BBC]]'s lead commentator [[Peter O'Sullevan]] describes the second false start.<ref name=footage/>
 |width  = 35%
 |quoted = 1
 |align  = right
}}
The meeting at Aintree had been beset by problems before the race. Fifteen [[animal rights]] protesters invaded the course near the first fence (as had also happened at the [[1991 Grand National]]) resulting in a delayed start.<ref name=independent/> A first [[false start]] was caused by several riders becoming tangled in the starting tape. Starter Keith Brown, who was officiating his last National before retirement, waved his red recall flag and a second official, Ken Evans, who was situated 100 yards further down the track, in turn signalled to the leading runners to turn around. At the second attempt, the tape became tangled again — around the neck of jockey [[Richard Dunwoody]] — and Brown called another false start. However, this time his recall flag did not unfurl as he waved it.<ref name=footage>[https://www.youtube.com/watch?v=b2jHwZ1JerY BBC Live Coverage, 3 April 1993]</ref> As a result, 30 of the 39 riders set off around the track, oblivious to the recall.<ref name=BBC/><ref name=anorak>[http://grandnationalanorak.webs.com/theclassof93.htm Class of 93], ''Grand National Anorak''</ref>

===First circuit===
[[File:Aintreenationalcropped.jpg|thumb|250px|right|A map of the National Course at Aintree.]]
Officials, trainers and the crowd tried desperately to halt the race, but the majority of the field continued racing. By the [[Becher's Brook]] (the sixth fence) only one of the 30 still competing had fallen: outsider Farm Week at the fourth, who hampered David's Duky in the process.<ref name=anorak/>

[[Royal Athlete]] had gained popularity with the public after finishing third in the [[Cheltenham Gold Cup]] and was sent to post at 17/2, providing Ben de Haan, the [[1983 Grand National|1983]] winning jockey, with his 11th and final ride in the race. He fell at Valentine's (the ninth).

One fence later, outsider Senator Snugfit fell. The [[BBC]]'s commentary team, consisting of [[Peter O'Sullevan]], John Hanmer and [[Jim McGrath (Australian commentator)|Jim McGrath]] continued to describe proceedings, periodically reminding viewers that "it's got to be a void race".<ref name=footage/><ref name=anorak/>

It was not until the water jump — the final fence of the first circuit — that many riders became aware of the situation and pulled up, including champion jockey [[Peter Scudamore]] on Captain Dibble, [[Garrison Savannah (horse)|Garrison Savannah]], and Zeta's Lad, who was widely considered by tipsters as the form horse in the field,<ref>"Zeta's Lad to vindicate Upson's instinct: A chaser with a perfect record this season is selected by Richard Edmondson to maintain the sequence" - Richard Edmondson, [http://www.independent.co.uk/sport/racing-147th-grand-national-zetas-lad-to-vindicate-upsons-instinct-a-chaser-with-a-perfect-record-this-season-is-selected-by-richard-edmondson-to-maintain-the-sequence-1452955.html Racing: 147th Grand National], ''The Independent'', 3 April 1993</ref> having raced unbeaten in his five starts that season, including beating Romany King in the ''Racing Post'' Chase at [[Kempton Park Racecourse|Kempton]] two months prior.<ref>[http://www.racingpost.com/horses/result_home.sd?race_id=50664&r_date=1993-02-27&popup=yes 1993 Racing Post Chase result], ''Racing Post''</ref> Most of the horses at the rear were pulled up too, including Stay On Tracks, David's Duky, Direct, Mister Ed and the tailed-off Quirinus.<ref name=results/>

Captain Dibble was the [[Scottish Grand National]] winner in 1992 and vied for favouritism for the Grand National until a few days before the race. Scudamore had turned down a host of competitors to take the ride in his 13th National, and the pair were sent off at 9/1. Scudamore saw trainer [[Martin Pipe]] waving at him near the water jump to stop. The jockey had never won the National in his previous 12 attempts, and retired from racing a short time later.

Party Politics, who won the [[1992 Grand National]] with partner Carl Llewellyn, was also pulled up after the water jump when in a good position. Since his Aintree victory the previous April he had run unimpressively in two chases without Llewellyn in the saddle before they were reunited to win the Greenhalls Gold Cup at [[Haydock Park Racecourse|Haydock]] in February. As defending champion, and with his jockey taking his fourth ride in a Grand National, the horse was popular with the public who had backed him down to 7/1 favourite at the start.<ref>[http://www.racingpost.com/horses/horse_home.sd?horse_id=47364 Party Politics' career record], ''Racing Post''</ref>

===Second circuit===
Fourteen horses continued racing onto the second circuit, led by Sure Metal and Howe Street who between them held a decent lead until they both fell at the 20th fence.<ref name=footage/><ref name=anorak/>

This put Romany King into the lead, which he held on to until being passed at the final fence and finishing third. The horse had been narrowly beaten by Party Politics in the previous year's National but had won just one of his six races since, a moderate event at Exeter in November. He shared pre-race favouritism with Party Politics until shortly before the start when he drifted to 15/2 joint-second favourite. His Irish jockey, [[Adrian Maguire]], was one of nine riders making their debut in the race.<ref name=independent/>

One fence later, at the 21st, Joyful Noise refused, Paco's Boy fell, as did the tailed-off The Gooser. Interim Lib unseated his rider at the [[Canal Turn]] and a tailed-off Bonanza Boy refused at the same fence. Seven runners remained and went on to complete the course: Romany King, The Committee, Esha Ness, Cahervillahow, Givus A Buck, On The Other Hand and a distant Laura's Beau. As they crossed the Melling Road before approaching the penultimate fence, commentator Peter O'Sullevan declared the unfolding events "the greatest disaster in the history of the Grand National."<ref name=footage/><ref name=anorak/>

{{Quote box
 |quote  = So as they race up to the line, in the National that surely isn't, Esha Ness is the winner, second is Cahervillahow, third is Romany King, four The Committee, five is Givus A Buck. Then comes On The Other Hand and Laura's Beau and they are the only ones to have completed in the race that surely never was.
 |source = Peter O'Sullevan describes the climax of the 'race'.<ref name=footage/>
 |width  = 35%
 |quoted = 1
 |align  = right
}}

As they came to the elbow, on the 494-yard run-in to home, Cahervillahow, Romany King, The Committee and Esha Ness remained tight and vying for position. But it was 50/1 shot Esha Ness, ridden by John White and trained by [[Jenny Pitman]], who crossed the line first, in the second-fastest time in Grand National history.<ref name=guardian/><ref>John White, "[https://www.theguardian.com/sport/2010/apr/03/esha-ness-1993-grand-national 3 April 1993: Esha Ness 'wins' the Grand National that never was]", ''The Guardian'', Saturday 3 April 2010</ref> Cahervillahow came home second despite trailing in fourth at the elbow, Romany King was third and The Committee fourth. Givus A Buck completed in fifth, with On The Other Hand and Laura's Beau completing the seven finishers of the National that never was.<ref name=footage/><ref name=anorak/>

===Aftermath===
Initially there was confusion as to what would happen next. Keith Brown, the race starter, was interviewed briefly by the BBC and alluded to the possibility that the nine jockeys who noticed and obeyed his recall could be eligible to take part in a re-run. Several jockeys said that they thought the officials attempting to stop them were actually protestors.<ref name=independent/> Esha Ness's jockey John White said of the latter stages of the race: "I could see there were only a few horses around, but I thought the others had fallen or something."<ref name=BBC/>

Romany King's jockey [[Adrian Maguire]] said: "Going to [[The Chair (Aintree Racecourse)|The Chair]], I wondered what the hell was going on because I saw a fellow wandering nonchalantly across the fence. There were two cones in front of it, but the horses still in the race all kept going."<ref>[http://www.independent.co.uk/sport/racing-grand-national-i-would-have-been-sick-if-i-had-won-national-protagonists-have-their-say-on-the-falsestart-fiasco-1453142.html Racing: Grand National], ''The Independent'', 4 April 1993</ref>

The Jockey Club later declared the race void, ruled out any re-running of it, and launched an inquiry. Bookmakers were forced to refund an estimated £75 million in bets staked.<ref name=BBC/><ref name=guardian/>

Zeta's Lad trainer John Upson was among those trainers who were particularly angry, feeling, correctly as it transpired, that this was his only chance to have a leading contender in a National. Before the race Upson had said: "I'm not someone who always thinks their horse is going to win, but this year I just have a feeling." The horse was in mid-division taking the water jump but was instantly pulled up as jockey Robbie Supple, riding in his third National, realised the race was not on. Upson later revealed: "The reason I really blew my top was, that once the initial fiasco had happened, there was the starter standing there saying, 'Right, I'm disqualifying everything, apart from the nine that didn't go. I'll start the race again with nine runners.' At that stage I just thought the world had gone completely mad. The adrenaline was going and I was jumping up and down."<ref>[http://www.independent.co.uk/sport/general/racing-ten-years-after-and-the-farce-runs-on-592838.html "Racing: Ten years after and the farce runs on"], ''The Independent'', 30 March 2003</ref>

==Investigation==
An inquiry was chaired by [[High Court of Justice|High Court]] judge Sir Michael Connell, the deputy senior steward of the Jockey Club since 1988.<ref>[http://www.thejockeyclub.co.uk/documents/JC08.pdf Inquiry: 1993 Grand National], Jockey Club</ref> His report apportioned some blame to Keith Brown for allowing the horses to get too close to the tape, but most blame to Ken Evans, the official further down the track, for failing to notice the second false start.<ref>David Lister, "[http://www.independent.co.uk/opinion/officers-gentlemen-and-a-grand-national-flag-chap-1491789.html Officers, gentlemen and a Grand National flag chap]", ''The Independent'', 15 June 1993</ref> Brown retired later that year and said: "It was very sad for all concerned. Whatever could go wrong that day did."

A working group produced a 34-page report with recommendations following the official inquiry, which was approved by the Jockey Club. Considerable public discussion had arisen over the possibility of introducing electronic devices, such as horns or flashing lights, to provide a fail-safe starting and recall system. The use of modern technology however was dismissed on the basis of a lack of total success overseas, and being open to sabotage and technical failure.<ref name=independentinquiry>Richard Edmundson, "[http://www.independent.co.uk/sport/racing-national-inquiry-keeps-faith-in-flagwaving-in-the-aftermath-of-aintree-new-technology-is-shunned-but-traditional-methods-expanded-and-improved-1463542.html Racing: National inquiry keeps faith in flag-waving]", ''The Independent'', 26 August 1993</ref>

The tape at the start line was made more sturdy, consisting of three strands instead of one, and in a more distinctive pattern; the width of the start was also reduced. If a false start is called, two officials (in contact with the starter by radio) will wave fluorescent yellow flags at jockeys. Further up the course, a third official will be positioned so as to arrest those who fail to notice the two initial flags. If necessary, the third official will follow the field in a car to stop them.<ref name=independentinquiry/>

[[Andrew Parker Bowles]], who chaired the working group which produced the recommendations, said he was confident that the exceptional circumstances of the 1993 National would never be repeated: "You start 7,000 races a year with flagmen and it went wrong just three times last year, but one of them was the Grand National. It won't happen again."<ref name=independentinquiry/>

==Race card==
{| class="wikitable sortable"
|-
|'''Name'''
|'''[[Jockey]]'''
|'''Age'''
|'''Handicap (st-lb)'''
|'''[[Starting price|SP]]'''
|'''Fate'''
|-
|Quirinus <small>([[Czech Republic|CZE]])</small>
|J Brecka
|11
|11-10
|300/1
|Fence 17, pulled up
|-
|[[Garrison Savannah (horse)|Garrison Savannah]]
|M Pitman
|10
|11-08
|10/1
|Fence 16, pulled up
|-
|Chatam <small>([[United States|USA]])</small>
|J Lower
|9
|11-07
|28/1
|Did not start
|-
|Party Politics
|C Llewellyn
|9
|11-02
|7/1 F
|Fence 17, pulled up
|-
|Cahervillahow
|[[Charlie Swan (horse racing)|C Swan]]
|9
|10-11
|25/1
|Completed, 2nd
|-
|Captain Dibble
|[[Peter Scudamore|P Scudamore]]
|8
|10-08
|9/1
|Fence 17, pulled up
|-
|Romany King
|[[Adrian Maguire|A Maguire]]
|9
|10-07
|15/2
|Completed, 3rd
|-
|Roc de Prince <small>([[France|FRA]])</small>
|G McCourt
|10
|10-06
|66/1
|Did not start
|-
|Royle Speedmaster
|J P Durkan
|9
|10-05
|200/1
|Did not start
|-
|Zeta's Lad
|R Supple
|10
|10-04
|15/2
|Fence 17, pulled up
|-
|[[Royal Athlete]]
|B De Haan
|11
|10-04
|17/2
|Fence 10, fell
|-
|Interim Lib
|Mr. J Bradburne
|10
|10-04
|200/1
|Fence 24, unseated rider
|-
|On The Other Hand
|N Doughty
|10
|10-03
|20/1
|Completed, 6th
|-
|Direct
|P Niven
|10
|10-03
|100/1
|Fence 16, pulled up
|-
|Latent Talent
|[[Jamie Osborne|J Osborne]]
|9
|10-02
|28/1
|Did not start
|-
|Nos Na Gaoithe
|R Garritty
|10
|10-02
|66/1
|Did not start
|-
|Travel Over
|[[Marcus Armytage|Mr. M Armytage]]
|12
|10-02
|100/1
|Fence 2, pulled up
|-
|Wont Be Gone Long
|[[Richard Dunwoody|R Dunwoody]]
|11
|10-01
|16/1
|Did not start
|-
|Joyful Noise
|T Jarvis
|10
|10-01
|150/1
|Fence 20, refused
|-
|Farm Week
|S Hodgson
|11
|10-01
|200/1
|Fence 4, fell
|-
|Givus A Buck
|P Holley
|10
|10-00
|16/1
|Completed, 5th
|-
|Laura's Beau
|C O'Dwyer
|9
|10-00
|20/1
|Completed, 7th
|-
|The Committee
|[[Norman Williamson|N Williamson]]
|10
|10-00
|25/1
|Completed, 4th
|-
|Mister Ed
|D Morris
|10
|10-00
|25/1
|Fence 17, pulled up
|-
|Riverside Boy
|M Perrett
|10
|10-00
|28/1
|Fence 17, pulled up
|-
|Kildimo
|L Wyer
|13
|10-00
|40/1
|Did not start
|-
|Esha Ness
|J White
|10
|10-00
|50/1
|Completed, 1st
|-
|Stay on Tracks
|K Johnson
|11
|10-00
|50/1
|Fence 17, pulled up
|-
|Rowlandsons Jewels
|D Gallagher
|12
|10-00
|50/1
|Fence 17, pulled up
|-
|Sure Metal
|S J O'Neill
|10
|10-00
|50/1
|Fence 20, fell
|-
|Howe Street
|A Orkney
|10
|10-00
|66/1
|Fence 20, fell
|-
|The Gooser
|K O'Brien
|10
|10-00
|50/1
|Fence 21, fell
|-
|New Mill House
|T Horgan
|10
|10-00
|66/1
|Fence 18, pulled up
|-
|Bonanza Boy
|S McNeill
|12
|10-00
|100/1
|Fence 24, refused
|-
|David's Duky
|M Brennan
|11
|10-00
|100/1
|Fence 17, pulled up
|-
|Paco's Boy
|M Foster
|8
|10-00
|100/1
|Fence 20, fell
|-
|Formula One
|Mrs. J Davies
|11
|10-00
|200/1
|Did not start
|-
|Senator Snugfit <small>([[United States|USA]])</small>
|P Hobbs
|8
|10-00
|200/1
|Fence 11, fell
|-
|Tarqogan's Best
|B Clifford
|13
|10-00
|500/1
|Did not start
|}
<ref name=results/>

==References==
{{Reflist|2}}

==External links==
*[http://www.grand-national-world.co.uk/gnw/the_race/tales/1993a.html 1993 race] at ''Grand National World''.

{{Grand National}}
{{Use dmy dates|date=March 2012}}

[[Category:Grand National| 1993]]
[[Category:1993 in horse racing|Grand National]]
[[Category:1993 in English sport|Grand National]]
[[Category:20th century in Merseyside]]