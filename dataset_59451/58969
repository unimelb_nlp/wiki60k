{{for|the branch of biology|Molecular ecology}}
{{Infobox journal
| title = Molecular Ecology
| cover = [[File:Molecular Ecology.jpg]]
| abbreviation = Mol. Ecol. 
| discipline = [[Ecology]]
| editor = Loren Rieseberg
| publisher = Wiley-Blackwell
| history = 1992–present
| frequency = Twice monthly
| impact = 5.947
| impact-year = 2015
| ISSN = 0962-1083
| eISSN = 1365-294X
| CODEN = MOECEO
| OCLC = 39265322
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-294X
}}
'''''Molecular Ecology''''' is a twice monthly [[scientific]] journal covering investigations that use molecular genetic techniques to address questions in [[ecology]], [[evolution]], [[behavior]], and [[conservation biology|conservation]]. ''Molecular Ecology'' is published by [[Wiley-Blackwell]].

[[Harry Smith (botanist)|Harry Smith]] was the founding [[editor in chief]], while Loren Rieseberg is the current one. Its 2015 [[impact factor]] is 5.947.

==See also==
* ''[[Molecular Ecology Resources]]''

[[Category:Ecology journals]]
[[Category:English-language journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Biweekly journals]]
[[Category:Publications established in 1992]]


{{biology-journal-stub}}