{{Insolvency}}

'''Provisional liquidation''' is a process which exists as part of the [[insolvency|corporate insolvency]] laws of a number of [[common law jurisdictions]] whereby after the lodging of a petition for the [[winding-up]] of a [[company]] by the court, but before the court hears and determines the petition, the court may appoint a liquidator on a "provisional" basis.<ref name=Practical>{{cite web |url=http://uk.practicallaw.com/3-384-0074?service=finance |title=Provisional liquidation: a quick guide |publisher=Practical Law |author= |date= |accessdate=30 July 2015 }}</ref>  The provisional liquidator is appointed to safeguard the assets of the company and maintain the ''[[status quo]]'' pending the hearing of the petition.  Unlike a conventional [[liquidator (law)|liquidator]], a provisional liquidator does not assess claims against the company or try to distribute the company's assets to [[creditor]]s.<ref name=Worrells>{{cite web |url=http://www.worrells.net.au/InsolvencyResources/FactsheetArticle.aspx?ArticleId=14 |title=Provisional Liquidation |publisher=Worrells |author= |date=25 September 2013 |accessdate=30 July 2015 }}</ref>

In practice most instances of applications for a provisional liquidator involve some type of allegation of [[fraud]] or other misconduct relating to the company.<ref name=Fraud>{{cite web|url=http://www.lexology.com/library/detail.aspx?g=159e63e1-69ec-43c9-af12-e9b516fc0f01|title=Provisional liquidation and fraud |publisher=Lexology|author=Steven Fennell and Darren Bradshaw|date=19 June 2013|accessdate=30 July 2015}}</ref>

==Application==

Typically, an application for the appointment of a provisional liquidator is made by either:<ref name=Worrells />
# a creditor, where they are concerned that the assets of the company are at risk or might be dissipated or put beyond the reach of creditors during the period before the winding up application is heard;
# the shareholders, where they are concerned that the [[board of directors|directors of the company]] are acting improperly; or
# the company itself.  This may arise due to a dispute between directors and other officers, or because the company is insolvent and the directors do not want to risk insolvent trading claims in the period before an official liquidator is appointed.  In some jurisdictions (but not all) the company may apply to put itself into provisional liquidation to shield itself from creditor claims whilst it tries to implement a [[restructuring]].

The remedy is an exceptional one, and most instances of applications for a provisional liquidator are made because of concerns about some type of material impropriety.<ref name=Fraud />  In exceptional cases it is also possible for public authorities to apply for the appointment of a provisional liquidator to protect the public interest from fraud or other similar conduct, although this is much less common.<ref>See, for example, {{cite bailii|litigants=Re Treasure Traders Corporation Ltd|year=2005|court=EWHC|division=Ch|num=2774}}, in which the company was operating an unlawful lottery.</ref>

The court invariably has a discretion whether to appoint a provisional liquidator.  A court will not normally approve the application unless it is satisfied that there is a strong liklihood that a liquidator will be appointed on the substantive application.  But even if the company is likely to go into liquidation, provisional liquidation is still an exceptional interim or "emergency remedy".<ref name=Kill >{{cite web |url=http://www.3harecourt.com/assets/asset-store/file/articles/Lewis-Yousuf.pdf |title=Provisional liquidations - kill or cure? |publisher=Business Rescue |author=Daniel Lewis and Nyla Yousuf |date= |accessdate=30 July 2015 }}</ref>  There needs to be special reasons for the appointment of a provisional liquidator in the interim period.  Normally this is because the assets must face a high risk of dissipation or there must be some other urgent reason why a liquidator is required for the interim period.<ref name=Clayton >{{cite web |url=http://www.claytonutz.com/area_of_law/restructuring_and_insolvency/docs/ProvisionalLiquid.pdf |title=Restructuring & Insolvency - Provisional liquidation |publisher=[[Clayton Utz]] |author= |date= |accessdate=30 July 2015 }}</ref>

Applications are most likely to be granted in situations where:<ref name=Clayton />
* it is necessary to have an independent person investigate the company and its affairs;
* there was an urgent need to act to preserve assets of the company;
* there was a material [[conflict of interest]] between a director and the company; or
* there had been impropriety in the conduct of company’s affairs.

Conversely, because there needs to be some urgency or risk of dissipation of assets, applications are likely to be refused if:<ref name=Clayton />
* the only reason relied on by a creditor was that the company was insolvent (without more);
* an [[administrative receiver]] or [[administrator (law)|administrator]] had already been appointed, negating the risk of dissipation of assets;
* the application was made for an improper purpose, such as to maximise the applicant’s own position;
* the application was made simply to try and permit the company to continue to trade where the company is insolvency and clearly should be wound up; or
* there was a genuine dispute as to the creditor’s debt.

Given their nature, applications for provisional liquidation are often made urgently and without giving notice to the company or its directors.  Where the application is made without notice:<ref name=Practical />
# the applicant is under a duty to give "full and frank disclosure" to the court of all material facts (including any facts which are adverse to the applicant); and
# the applicant will normally have to give an undertaking in damages to compensate the company for any loss caused if it subsequently transpires after a full hearing that the order should not have been.<ref>''Re Highfield Commodities Ltd'' [1984] BCLC 623.</ref>

==Effect==

If the court makes the order then a provisional liquidator is appointed over the company, and the control of all assets of the company, and the conduct of any business and other affairs of the company are transferred to the provisional liquidator.  The directors cease to have any authority.<ref name=Worrells />  In almost all jurisdictions, the provisional liquidator will normally have to be a licensed insovency practitioner.   

The provisional liquidator will generally only have the powers and functions conferred upon him or her by the order of the court.  It is not normally part of the role of a provisional liquidator to collect and sell the assets of the company, or otherwise take steps in relation to the actual winding up the company.  The principal reason for the appointment is the preservation of the company's property.<ref>{{cite bailii|litigants=Ashborder BV v Green Gas Power Ltd|year=2005|court=EWHC|division=Ch|num=1031}}</ref>

One of the common functions of a provisional liquidator is to investigate whether the company's property has been misappropriated or its business has been wrongfully conducted.  But generally speaking a provisional liquidator will not have power to bring proceedings on behalf of the company for [[wrongful trading]], or to challenge transactions as [[undervalue transaction]]s or [[unfair preference]]s.<ref>{{Cite bailii|litigants=In the matter of Overnight Limited|year=2009|court=EWHC|division=Ch|num=601}}</ref>

==Termination==

Provisional liquidation will normally come to an end in one of three ways:<ref name=Practical />
# a full winding up order is made;
# an order of the court is made discharging the provisional liquidation (normally at an ''inter partes'' hearing to challenge the original appointment); or
# the winding up petition itself is dismissed.

==Specific jurisdictions==

The effect of provisional liquidation varies from jurisdiction to the jurisdiction.

===Australia===

{{Main|Australian insolvency law}}

In Australia provisional liquidation is regulated by section 472(2) of [[Corporations Act 2001]].<ref>{{cite web|url=http://www.austlii.edu.au/au/legis/cth/consol_act/ca2001172/s472.html|title=Corporations Act 2001, section 472|accessdate=30 July 2015}}.</ref>  In Australia provisional liquidators must be licensed insolvency practitioners.<ref name=Worrells />

===British Virgin Islands===

{{Main|British Virgin Islands bankruptcy law}}

Under British Virgin Islands law provisional liquidation is regulated by section 170(4) of the Insolvency Act 2003.<ref name=BVI>{{cite book|title=British Virgin Islands Commercial Law|author=[[Harney Westwood & Riegels]]|publisher=[[Sweet & Maxwell]]|edition=3rd|year=2014|isbn=9789626615294|at=7.073}}</ref>  In ''Akai Holdings Limited v Brinlow Investments Limited''<ref>(BVIHCV 2006/0134) at [44] onwards.</ref> it was held that an applicant would need to show four things:
# a petition before the court for the appointment of a liquidator;
# a good arguable case that a ground for the appointment of a liquidator exists;
# a good arguable case that the applicant has the necessary standing; and
# that the court should exercise its discretion to maintain the ''status quo'' in relation to the company’s assets.

===Canada===

In Canada provision liquidation is regulated under section 28 of the Winding-up and Restructuring Act (R.S.C., 1985, c. W-11).<ref>{{cite web |url=http://laws-lois.justice.gc.ca/eng/acts/w-11/page-8.html |title=Winding-up and Restructuring Act (R.S.C., 1985, c. W-11) |publisher=Justice Laws Website |author= |date= |accessdate=30 July 2015 }}</ref>

===Cayman Islands===

{{Main|Cayman Islands bankruptcy law}}

In the Cayman Islands provisional liquidation is principally regulated by section 104(3) of the Companies Law (2013) Revision.  One unusual feature of provisional liquidation in Cayman is that it is possible to appoint a provisional liquidator and still allow the directors to retain their powers of management.<ref>Companies Winding Up Rules (2008 Revision), Order 4, rule 7(3).</ref>  This in part facilitates a slightly unusual use of provisional liquidation in the Cayman Islands as part of a [[debtor in possession]] corporate rehabilitation prcoess.  The company itself will apply for an order for provisional liquidation, and provisional liquidators are appointed which effectively creates a stay on the claims of [[unsecured creditor]]s.  The  board of directors can then use that "breathing space" to try and implement a restructuring of the company's debts pursuant to a [[scheme of arrangement]], or pursuant to an international restructuring process in the courts of another jurisdictions.<ref name=Maples >{{cite web |url=http://www.maplesandcalder.com/news/article/cayman-islands-provisional-liquidations-in-support-of-chapter-11-proceedings-433/ |title=Cayman Islands Provisional Liquidations in Support of Chapter 11 Proceedings |publisher=[[Maples and Calder]] |date=17 December 2012 |accessdate=30 July 2015 }}</ref>  This approach has not been rejected in the courts of other jurisictions (such as Hong Kong),<ref name=Legend >{{cite web |url=http://www.internationallawoffice.com/newsletters/detail.aspx?g=f806367c-2c17-db11-8a10-00065bfd3168 |title=Corporate Rescue: The Need for Legislation |publisher=International Law Office |author=Andrew Kinnison and Jacky Yeung |date=4 August 2006 |accessdate=30 July 2015 }}</ref> but accepted in others.<ref>{{cite book |url=https://books.google.com.jm/books?id=f_yWaV_-_l4C&pg=PT136&lpg=PT136&dq=new+zealand+law+provisional+liquidation&source=bl&ots=D-jp6iPhZb&sig=mwLDD1xUQfxnM6jdHn3ywmCg6Lg&hl=en&sa=X&redir_esc=y#v=onepage&q=new%20zealand%20law%20provisional%20liquidation&f=false |title=Principles of International Insolvency |publisher=[[Sweet & Maxwell]] |author=Philip R. Wood |isbn=9781847032102 |year=2007 |accessdate=30 July 2015 }}</ref>

===Hong Kong===

In Hong Kong there are three types of provisional liquidators.<ref name=BWF>{{cite web |url=http://www.briscoewongferrier.com/web2/wp-content/uploads/2013/08/ProvisionalLiquidators1.pdf |title=Provisional Liquidators |publisher=Briscoe Wong Ferrier |author= |date= |accessdate=30 July 2015 }}</ref>  There are "traditional" provisional liquidators, appointed under section 193 of the Hong Kong Companies (Winding Up and Miscellaneous Provisions) Ordinance (Cap 32); there are also provisional liquidators appointed pursuant to a members' voluntary liquidation under section 228A of the Ordinance, and there are "Panel T" appointments under section 194(1A) of the Ordinance whereby the Official Receiver is appointed as provisional liquidator.  

Since the decision in the ''Legend'' case in 2006<ref>''Re Legend International Resorts Ltd'' [2006] 2 HKLRD 192</ref> provisional liquidation may not be used as a means of shielding the company from creditor's claims to facilitate a restructuring in Hong Kong,<ref name=Legend /> although prior to that date the practice was relatively common.<ref name=BWF />

===South Africa===

In South Africa "provisional liquidation" has a very different meaning.  When a creditor or other person applies to the court for the liquidation of a business, then the order is first made a provisional basis, and then subsequent confirmed (or not) at a full hearing, much like a ''decree nisi'' and a ''decree absolut'' in other jurisdictions.<ref>{{cite web|url=https://www.findanattorney.co.za/content_business-liquidation|title=Liquidating a Business in South Africa|publisher=FindanAttorney|author=Nanika Prinsloo|accessdate=30 July 2015}}</ref>

===United Kingdom===
{{Main|United Kingdom insolvency law}}
In the United Kingdom the power to appoint a provisional liquidator is found in section 135(1), of the [[Insolvency Act 1986]], and it is regarded as an "emergency procedure".<Ref name=Practical />  The categories of persons who may apply for the appointment are set out at rule 4.25(1) of the Insolvency Rules 1986.<ref>SI 1925 of 1986.</ref>  Under English law all provisional liquidators are required to be licensed insolvency practitioners.<ref>Insolvency Act 1986, section 388(1)(a).</ref> 
If, upon the application for the appointment of a liquidator, there are concerns about potential dissipation of assets, or misconduct on the part of the directors, then the court may order the appointment of a [[provisional liquidator]].<ref name=PL>{{cite web |url=http://uk.practicallaw.com/3-384-0074?service=finance |title=Provisional liquidation: a quick guide |publisher=Practical Law |author= |date= |accessdate=30 July 2015 }}</ref>  Provisional liquidation is essentially an 
"emergency procedure".  A provisional liquidator may only be appointed by the court only after a winding-up petition has been presented<ref>Insolvency Act 1986 s 135(1).</ref>  The main reason for appointing a provisional liquidator is normally to preserve the company's assets.  In practice most instances of applications for a provisional liquidator involve some kind of [[fraud]] or other misconduct.<ref>{{cite web|url=http://www.lexology.com/library/detail.aspx?g=159e63e1-69ec-43c9-af12-e9b516fc0f01|title=Provisional liquidation and fraud |publisher=Lexology|author=Steven Fennell and Darren Bradshaw|date=19 June 2013|accessdate=30 July 2015}}</ref>  The applicant will normally need to show that (a) it is likely that a winding up order will be made at the hearing of the petition; and (b) the company's assets are at risk prior to the hearing of the petition (which includes either dissipation of the company's assets, or the potential loss or destruction of the company's books and records.<ref>{{cite bailii|litigants=Revenue & Customs v Rochdale Drinks Distributors Ltd|year=2011|court=EWCA|division=Civ|num=1116}}</ref>  Accordingly, it will normally be necessary to establish either (or both) that: (a) the company is clearly insolvent, and it is likely that a winding up order will be made at the hearing of the petition; and/or (b) there has been the type of misconduct that would justify a just and equitable winding-up.  An appointment of a provisional liquidator may also be made where it is in the public interest.<ref>{{Cite bailii|litigants=Re Treasure Traders Corporation Ltd|year=2005|court=EWHC|division=Ch|num=2774}}</ref>

Because of the emergency nature of the remedy applications for provisional liquidation are very often made urgently and without giving notice to the company.  In such cases the applicant is under a legal obligation to give full and frank disclosure to the court of all material facts, including facts which are adverse to the applicant's own case.  The applicant will also normally be required to give an undertaking in damages in the event the court subsequently determines that the order should not have been made to compensate the company for any damage caused to it by the appointment.<ref>''Re Highfield Commodities Ltd'' [1984] BCLC 623.</ref>

If a provisional liquidator is appointed then the powers of the company's directors are effectively terminated, and the board will retain only a residual power to apply to dismiss or resist the winding-up petition.<ref>{{Cite bailii|litigants=Ashborder BV v Green Gas Power Ltd|year=2005|court=EWHC|division=Ch|num=1031}}</ref>  The appointment of a provisional liquidator comes to an end when:<ref name=PL />
# a liquidator is appointed at a full hearing;
# the provisional liquidator is discharged by order of the court; or
# the underlying winding up petition is dismissed.

==References==

{{Reflist}}

[[Category:Insolvency]]
[[Category:Fraud]]