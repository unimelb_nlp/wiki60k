{{for|the American playwright|Joseph A. Walker (playwright)}}
{{Use American English|date=November 2014}}
{{more footnotes|date=August 2011}}
{{Infobox astronaut
|name        = Joseph A. Walker
|image       = Joseph Albert Walker.jpg
|image_size  = 200
|caption     = Walker in 1961
|type        = [[USAF]] / [[NASA]] [[Astronaut]]
|nationality = American
|birth_name  = Joseph Albert Walker
|birth_date  = {{Birth date|1921|2|20}}
|birth_place = [[Washington, Pennsylvania]], U.S.
|death_date  = {{Death date and age|1966|06|08|1921|02|20|mf=yes}}  
|death_place = near [[Barstow, California]], U.S.
|occupation  = [[Test pilot]],<br>[[experimental physicist]]
|alma_mater  = [[Washington and Jefferson College]], B.A. 1942
|rank        = [[Captain (United States)|Captain]], [[U.S. Army Air Forces|USAF]]
|selection   = [[List of astronauts by year of selection#1958|1958 USAF Man In Space Soonest]]
|time        = 22 minutes
|mission     = [[X-15#Highest flights|X-15 Flight 90]], [[X-15#Highest flights|X-15 Flight 91]]
|insignia    = <!-- The X-15 logo is ''not'' a mission insignia -->
|Date of ret = August 22, 1963
|awards      = [[File:Dfc-usa.jpg|20px|link=Distinguished Flying Cross (United States)]]
|}}
'''Joseph Albert "Joe" Walker''' (February 20, 1921 – June 8, 1966) flew the world's first two [[spaceplane]] flights in 1963, thereby becoming the United States' seventh [[astronaut]]. Walker was a [[Captain (United States)|Captain]] in the [[United States Air Force]], an American [[World War II]] pilot, an [[experimental physicist]], a [[NASA]] [[test pilot]], and a member of the U.S. Air Force [[Man In Space Soonest]] spaceflight program. His two [[X-15]] [[Rocket plane|experimental rocket aircraft]] flights in 1963 that exceeded the [[Kármán line]]&nbsp;– the altitude of {{convert|100|km|abbr=off}}, generally considered to mark the threshold of [[outer space]]&nbsp;– qualified him as an astronaut under the rules of the U.S. Air Force and the [[Fédération Aéronautique Internationale]] (FAI).

==Biography==

===Early years and education===
Born in [[Washington, Pennsylvania]], Walker graduated from [[Trinity High School (Washington, Pennsylvania)|Trinity High School]] in 1938. He earned his [[Bachelor of Arts]] degree in [[physics]] from [[Washington and Jefferson College]] in 1942, before entering the United States Army Air Forces. He was married and had four children.

===Military service===
During World War II, Walker flew the [[Lockheed P-38 Lightning]] fighter and [[Lockheed P-38 Lightning|F-5A Lightning]] photo aircraft (a modified P-38) on weather [[reconnaissance]] flights. Walker earned the [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] once, awarded by [[Nathan Farragut Twining|General Nathan Twining]] in July 1944, and the [[Air Medal]] with seven [[oak leaf cluster]]s.

===Test pilot career===
[[File:Test pilots 1952 - Walker, Butchart, and Jones DVIDS738099.jpg|thumb|right|Walker with his fellow test pilots Butchart (center) and Jones, 1952]]
[[File:Pilot Joe Walker and the X-1A - GPN-2000-000095.jpg|thumb|left|Joe Walker and the X-1]]

After World War II, Walker separated from the Army Air Force and joined the [[National Advisory Committee for Aeronautics]] (NACA) [[Aircraft Engine Research Laboratory]] in [[Cleveland, Ohio]], as an experimental physicist. While in Cleveland, Walker became a test pilot, and he conducted icing research in flight, as well as in the NACA icing wind tunnel.  He transferred to the High-Speed Flight Research Station in [[Edwards, California]], in 1951.

Walker served for 15 years at the Edwards Flight Research Facility - now called the [[Neil A. Armstrong Flight Research Center]]. By the mid-1950s, he was a Chief Research Pilot. Walker worked on several pioneering research projects. He flew in three versions of the [[Bell X-1]]: the X-1#2 (two flights, first on 27 August 1951), X-1A (one flight), X-1E (21 flights). When Walker attempted a second flight in the X-1A on 8 August 1955, the rocket aircraft was damaged in an explosion just before being launched from the [[Boeing B-29 Superfortress|JTB-29A mothership]]. Walker was unhurt, though, and he climbed back into the mothership with the X-1A subsequently jettisoned.

Other research aircraft that he flew were the [[Douglas Skystreak|Douglas D-558-I Skystreak]] #3 (14 flights), [[Douglas Skyrocket|Douglas D-558-II Skyrocket]] #2 (three flights), D-558-II #3  (two flights), [[Douglas X-3 Stiletto]] (20 flights), [[Northrop X-4 Bantam]] (two flights), and [[Bell X-5]] (78 flights).

Walker was the chief project pilot for the X-3 program. Walker reportedly considered the X-3 to be the worst airplane that he ever flew. In addition to research aircraft, Walker flew many [[chase plane]]s during test flights of other aircraft, and he also flew in programs that involved the [[North American F-100 Super Sabre]], [[McDonnell F-101 Voodoo]], [[Convair F-102 Delta Dagger]], [[Lockheed F-104 Starfighter]] and [[Boeing B-47 Stratojet]].

====X-15 program====
[[File:Joe Walker beside an X-15 Airplane - GPN-2002-000012.jpg|thumb|right|Joe Walker with the X-15]]

{{Main|North American X-15}}
In 1958, Walker was one of the pilots selected for the U.S. Air Force's [[Man In Space Soonest]] (MISS) project, but that project never came to fruition. That same year, NACA became the National Aeronautics and Space Administration (NASA), and in 1960, Walker became the first NASA pilot to fly the X-15, and the second X-15 pilot, following [[Scott Crossfield]], the manufacturer's test pilot. On his first X-15 flight, Walker did not realize how much power its rocket engines had, and he was crushed backward into the pilot's seat, screaming, "Oh, my God!". Then, a flight controller jokingly replied "Yes? You called?" Walker would go on to fly the X-15 24 times, including the only two flights that exceeded {{convert|100|km|abbr=off}} in altitude, [[X-15 Flight 90|Flight 90]] (on 19 July 1963: {{convert|106|km|abbr=on}}) and [[X-15 Flight 91|Flight 91]] (on 22 August 1963: {{convert|108|km|abbr=on}}).

Walker was the first American civilian to make any spaceflight,<ref name="Space.com Joseph A Walker">[http://www.space.com/adastra/adastra_joewalker_061127.html " Joseph A Walker."] ''Space.com.'' Retrieved: 8 September 2010.</ref> and the second civilian overall, preceded only by the [[Soviet Union]]'s [[cosmonaut]], [[Valentina Tereshkova]]<ref name="Valentina Vladimirovna Tereshkova">[http://www.adm.yar.ru/english/section.aspx?section_id=74 "Valentina Vladimirovna Tereshkova."] ''adm.yar.ru.'' Retrieved: 8 September 2010.</ref> one month earlier. Flights 90 and 91 made Walker the first human to make multiple spaceflights.

Walker flew at the fastest speed in the X-15A-1: {{convert|4,104|mph|abbr=on}} ([[Mach number|Mach]] 5.92) during a flight on 27 June 1962 (the fastest flight in any of the three X-15s was about {{convert|4,520|mph|0|abbr=on}} (Mach 6.7) flown by [[William J. Knight]] in 1967).

====LLRV program====
{{Main|Lunar Landing Research Vehicle}}
Walker also became the first test pilot of the Bell [[Lunar Landing Research Vehicle]] (LLRV), which was used to develop piloting and operational techniques for lunar landings. On 30 October 1964, Walker took the LLRV on its maiden flight, reaching an altitude of about 10&nbsp;ft and a total flight time of just under one minute. He piloted 35 LLRV flights in total. [[Neil Armstrong]] later flew this craft many times in preparation for the [[spaceflight]] of [[Apollo 11]] - the first manned landing on the Moon - including crashing it once and barely escaping from it with his ejection seat.

===Death===
[[File:North American XB-70A Valkyrie just after collision 061122-F-1234P-037.jpg|thumb|Walker's F-104 tumbles in flames following the midair collision with the XB-70 ''62-0207'' on 8 June 1966]]

Walker was killed on 8 June 1966, when his [[F-104 Starfighter]] chase aircraft collided with a [[North American XB-70 Valkyrie]].<ref name=xb70aincoll>[https://news.google.com/newspapers?id=XHZQAAAAIBAJ&sjid=DhEEAAAAIBAJ&pg=5942%2C1339734 "XB-70A in collision, 2 die."] ''Milwaukee Sentinel'', 9 June 196, p. 1-part 1.</ref> At an altitude of about {{convert|25000|ft|km|abbr=on}}<ref name=ibixb>[https://news.google.com/newspapers?id=zEQaAAAAIBAJ&sjid=zScEAAAAIBAJ&pg=7224%2C5027111 "Inquiry begins into XB-70A collision."] ''Milwaukee Journal'', 9 June 1966, p. 12-part 1.</ref> Walker's Starfighter was one of five aircraft in a tight group formation for a [[General Electric]] publicity photo when his F-104 drifted into contact with the XB-70's right wingtip. The F-104 flipped over, and rolling inverted, passed over the top of the XB-70, struck both vertical stabilizers and the left wing and exploded, killing Walker.  {{#tag:ref|The famous test pilot [[Chuck Yeager]] expressed his personal opinion that Walker's inexperience at formation flying was to blame, although no specific cause was ever determined in the subsequent accident investigation.<ref name="yeagerbio">Yeager and Janos 1986, p. 226.</ref>|group=N}}  The Valkyrie entered an uncontrollable spin and crashed into the ground north of Barstow, California, killing co-pilot Carl Cross. Its pilot, [[Alvin S. White|Alvin White]], one of Walker's colleagues from the Man In Space Soonest program, ejected and was a sole survivor.

The USAF summary report of the accident investigation stated that, given the position of the F-104 relative to the XB-70, the F-104 pilot would not have been able to see the XB-70's wing, except by uncomfortably looking back over his left shoulder. The report stated that Walker, piloting the F-104, likely maintained his position by looking at the fuselage of the XB-70, forward of his position.

The F-104 was estimated to be {{convert|70|ft|-1|abbr=on}} to the side of, and {{convert|10|ft|0|abbr=on}} below, the fuselage of the XB-70. The report concluded that from that position, without appropriate sight cues, Walker was unable to properly perceive his motion relative to the Valkyrie, leading to his aircraft drifting into contact with the XB-70's wing.<ref name=Jenk_Land_p60>Jenkins and Landis 2002, p. 60.</ref><ref name=accident_report>''Summary Report: XB-70 Accident Investigation''. USAF,  1966.</ref>

The accident investigation also pointed to the wake vortex off the XB-70's right wingtip as the reason for the F-104's sudden roll over and into the bomber.<ref name=accident_report/> A sixth plane in the incident was a civilian [[Learjet 23]] that held the photographer. Because the formation flight and photo were unauthorized, the careers of several Air Force colonels ended as a result of this aviation accident.<ref name=cffstr>[https://news.google.com/newspapers?id=RhdZAAAAIBAJ&sjid=TOEDAAAAIBAJ&pg=6826%2C3497703 "Colonel fired for stunt role."] ''Eugene Register-Guard'', 6 August 1966. p. 4A.</ref><ref name=clpoxb >[https://news.google.com/newspapers?id=G24dAAAAIBAJ&sjid=G5sEAAAAIBAJ&pg=4172%2C2392386 "Colonel loses post over XB-70 crash."] ''Tuscaloosa News'', 16 August 1966, p. 1.</ref><ref>[http://www.check-six.com/Crash_Sites/XB-70_crash_site.htm "The Crash of the XB-70."] ''Check-Six.com.'' Retrieved: 8 September 2010.</ref>

==Awards and honors==
[[File:Joe Walker X-1E.jpg|thumb|Walker in his [[pressure suit]]<br>with the [[Bell X-1#X-1E|X-1E]]{{#tag:ref|Walker's X-1E was decorated with [[nose art]] of two dice and the name "Little Joe" (Little Joe being a slang term in the game of craps). Similar artwork reading "Little Joe the II" was applied to his X-15 for record-setting Flight 91. These were two rare cases of research aircraft carrying nose art.|group=N}}]]
[[File:Joe Walker Elementary.JPG|thumb|325 px|Joe Walker Elementary School]]

Walker was a charter member and one of the first Fellows of the [[Society of Experimental Test Pilots]]. He received the [[Collier Trophy|Robert J. Collier Trophy]], the [[Harmon Trophy|Harmon International Trophy for Aviators]], the [[Iven C. Kincheloe Award]], the [[John J. Montgomery Award]], and the [[Octave Chanute Award]]. His alma mater awarded him an [[Honorary Doctor]] of Aeronautical Sciences degree in 1961. The National Pilots Association named him the Pilot of the Year in 1963.

Walker was inducted into the [[Aerospace Walk of Honor]] in 1991, and the [[International Space Hall of Fame]] in 1995. Joe Walker Middle School in [[Quartz Hill, California]], is named in his honor as well as the [[Joe Walker Elementary]] School in Lagonda, Pennsylvania.

On 23 August 2005, NASA officially conferred on Walker his [[Astronaut Badge#Civilian badges|Astronaut Wings]], posthumously.<ref>Johnsen, Frederick A. [http://www.nasa.gov/missions/research/X-15_wings.html "X-15 Wings."] ''NASA'', 23 August 2005. Retrieved: 8 September 2010.</ref>

==See also==
{{Portal|Biography|World War II|United States Air Force|Spaceflight}}
* [[List of spaceflight records]]

==References==

===Notes===
{{Reflist}}

===Citations===
{{Reflist|group=N}}

===Bibliography===
{{Refbegin}}
* Coppinger, Rob. "Three new NASA astronauts, 40 years late". ''Flight International,'' 30 June 2005.
* [http://www.dfrc.nasa.gov/Gallery/Photo/Pilots/HTML/E-3361.html "Joe Walker in pressure suit with X-1E."] ''Dryden Flight Research Center Photo Archive.'' Retrieved: 8 September 2010.
* [http://www.dfrc.nasa.gov/Gallery/Photo/Pilots/HTML/E-2668.html "Joseph (Joe) A. Walker."]  ''Dryden Flight Research Center Photo Archive.'' Retrieved: 8 September 2010.
* Lefer, David. "Higher, faster, greater: X-15 test pilot who held record for altitude, speed is honored." ''Pittsburgh Post-Gazette'', 2 November 1995, p.&nbsp;C1.
* [[Milton O. Thompson|Thompson, Milton O.]] ''At The Edge Of Space: The X-15 Flight Program'', Washington, DC: Smithsonian Institution Press, 1992. ISBN 1-56098-1075.
* Winter, Frank H. and F. Robert van der Linden. "Out of the Past." ''Aerospace America,'' June 1991, p.&nbsp;5.
* [http://www.dfrc.nasa.gov/Gallery/Photo/X-1A/HTML/E-1758.html "X-1A with pilot Joe Walker."] ''Dryden Flight Research Center Photo Archive''. Retrieved: 8 September 2010.
* Yeager, Chuck and Leo Janos. ''Yeager: An Autobiography.'' New York: Bantam, 1986. ISBN 978-0-553-25674-1.
{{Refend}}

==External links==
{{commons category|Joseph Albert Walker}}
* [http://www.nasa.gov/centers/armstrong/news/Biographies/Pilots/bd-dfrc-p019.html#.VK12UHtRKBQ Walker's official NASA biography]
* [http://www.check-six.com/Crash_Sites/F-104N_crash_site.htm The Crash of Walker's F-104N]
* [http://www.astronautix.com/astros/waloseph.htm Astronautix biography of Joseph A. Walker]
* [http://www.spacefacts.de/bios/astronauts/english/walker_joseph.htm Spacefacts biography of Joseph A. Walker]
* [http://www.setp.org/winners/iven-c-kincheloe-recipients.html Iven C. Kincheloe Awards]
* {{Find a Grave|10922729|Joseph A. Walker}}

{{Use dmy dates|date=October 2010}}

{{DEFAULTSORT:Walker, Joseph A.}}
[[Category:1921 births]]
[[Category:1966 deaths]]
[[Category:1963 in spaceflight]]
[[Category:American astronauts]]
[[Category:American aviators]]
[[Category:Aviators from Pennsylvania]]
[[Category:NASA civilian astronauts]]
[[Category:American physicists]]
[[Category:People from Washington, Pennsylvania]]
[[Category:Washington & Jefferson College alumni]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:American test pilots]]
[[Category:Aviators killed in aviation accidents or incidents in the United States]]
[[Category:American military personnel of World War II]]
[[Category:Accidental deaths in California]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Air Medal]]
[[Category:Collier Trophy recipients]]
[[Category:Harmon Trophy winners]]
[[Category:X-15 program]]
[[Category:Flight altitude record holders]]