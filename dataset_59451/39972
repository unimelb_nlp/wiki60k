{{Infobox military person
|name= James Elms Swett
|birth_date         = {{Birth date|1920|6|15}}
|death_date         = {{death date and age|2009|1|18|1920|6|15}}
|birth_place        = [[Seattle, Washington]]
|death_place        = [[Redding, California]]
|placeofburial      =
|placeofburial_label= Place of burial
|image              = [[File:Swett JE.jpg|150px]]&nbsp;&nbsp;[[File:Moh right.gif|90px]]
|caption            = Swett in March 1949
|allegiance         = {{flag|United States of America|1912}}
|branch             ={{flag|United States Navy}}<br />{{flag|United States Marine Corps}}
|serviceyears       = 1941–1942 (USN)<br/>1942–1970 (USMC)
|rank               = [[Colonel (United States)|Colonel]]
|unit               = [[VMF-221]]
|battles            = [[World War II]]
*[[Battle of Guadalcanal]]
*[[Battle of Iwo Jima]]
*[[Battle of Okinawa]]
|awards             = [[Medal of Honor]]<br/>[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] (8)<br/>[[Purple Heart]] (2)
}}
[[File:JamesSwett2.jpg|200px|right|thumb|1st Lt James E. Swett and other members of VMF-221]]
'''James Elms Swett''' (June 15, 1920 – January 18, 2009) was a [[United States Marine Corps]] fighter pilot and [[flying ace|ace]]<ref name=AcePilots>[http://www.acepilots.com/usmc_aces.html "James E. Swett"] in ''Marine Corps Aces of World War Two''.</ref> during [[World War II]]. He was awarded the [[United States]]' highest military decoration, the [[Medal of Honor]], for actions while a division flight leader in [[VMF-221]] over [[Guadalcanal]] on April 7, 1943.

Subsequently he downed a total of 15.5 enemy aircraft during the war, earning eight [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]es and four [[Air Medals]].

==Biography==
Born on June 15, 1920 in [[Seattle, Washington]], James E. Swett graduated from [[San Mateo High School]], [[San Mateo, California]], and enrolled at the [[College of San Mateo]] in 1939. He earned a private pilot’s license, which amounted to 450 more hours of flying than he received during his Navy flight training. He enlisted in the [[United States Navy|U.S. Naval Reserve]] as a seaman second class on August 26, 1941, and started flight training in September.<ref name=GatheringEagles>"James E. Swett", ''Gathering of Eagles''</ref>

===Service in World War II===
Swett completed flight training in early 1942, placing in the top ten percent of his class. He was given the option to choose between a commission in the Marine Corps or the Navy, and he chose the Marine Corps. He was commissioned as a [[second lieutenant#United States|second lieutenant]] at [[NAS Corpus Christi]], [[Texas]], on April 1, 1942. He continued his advanced flight training, first at [[Quantico, Virginia]], then at Lake Michigan, became [[Modern United States Navy carrier air operations#Carrier qualifications|carrier qualified]] aboard the {{USS|Wolverine|IX-64|6}}, and finally received his wings at [[San Diego, California]]. In December 1942, he shipped out to the [[Southwest Pacific]], and when he arrived at [[Guadalcanal]] he was assigned to VMF-221, which was part of Marine Air Group 12.

===Medal of Honor action===
On April 7, 1943, on his first combat mission, Swett both became an ace and acted with such "conspicuous gallantry and intrepidity at the risk of his life above and beyond the call of duty" that he would be awarded the Medal of Honor.<ref name=GatheringEagles/><ref name=USMC_MOH>"1stLt James E. Swett", ''Marines Awarded the Medal of Honor''</ref><ref>Swett's flight on that memorable day is documented in [[Edward H. Sims]]' book ''Greatest Fighter Missions'', as the fourth chapter of that compilation.</ref>

His first mission was as a division leader on a [[combat air patrol]] over the [[Russell Islands]] early on the morning of April 7 in expectation of a large Japanese air attack. Landing to refuel, the four-plane division of [[Grumman F4F Wildcat]]s he was leading was scrambled after other aircraft reported 150 planes approaching [[Ironbottom Sound]], and intercepted a large formation of [[Japan]]ese [[Aichi D3A]] [[dive bomber]]s (Allied code name: "Val") attacking [[Tulagi]] harbor.<ref name=GatheringEagles/>

When the fight became a general melee, Swett pursued three Aichi D3A Vals diving on the harbor. After downing two, and while evading fire from the rear gunner of the third, the left wing of his F4F Wildcat was holed by U.S. antiaircraft fire. Despite this, he downed the third Val and turned toward a second formation of six Vals leaving the area.<ref name=GatheringEagles/>

Swett repeatedly attacked the line of dive bombers, downing each in turn with short bursts. He brought down four and was attacking a fifth when his ammunition was depleted and he had his cockpit shot up by return fire. Wounded, he decided to ditch his damaged fighter off the coast of [[Florida Island]], after it became clear that his oil cooler had been hit and he would not make it back to base. After a few seconds his engine seized, and despite initially being trapped in his cockpit underwater, Swett extricated himself and was rescued in Tulagi harbor after ditching his plane. This feat made the 22-year-old Marine aviator an [[Ace in a day|ace on his first combat mission]].<ref name=GatheringEagles/>

===Further combat service===
Swett returned to Guadalcanal after a short stay in a Naval hospital, and learned that Admiral [[Marc Mitscher]] had nominated him for the Medal of Honor. After a short rest in Australia, Swett checked out in the [[Vought F4U Corsair]] to which VMF-221 was converting and moved to a new base in the Russells. Promoted to [[Captain (United States)|captain]], Swett covered the Rendova landings on June 30, 1943, adding two [[Mitsubishi G4M]] "Betty" medium bombers to his score and sharing the downing of a [[Mitsubishi A6M Zero]].

Eleven days later, near the island of [[New Georgia]], Swett downed two more Bettys. Seeing his wingman's Corsair under attack, he also shot down a Zero. However, he failed to see a second Zero and was himself shot down. He was rescued by indigenous tribal members in a canoe and traveled by ten-man canoe for several hours to an Australian [[Coastwatchers|coast watcher's]] location. A [[PBY Catalina|PBY]] flying boat returned Swett to the Russells. In October 1943, over the major Japanese airbase at Kahili, [[Bougainville Island|Bougainville]], Swett added one confirmed Zero and one probable, but lost his wingman. In November, he added to his list of kills 2 more Vals and a possible [[Kawasaki Ki-61]] Tony, a new Japanese fighter.

On December 11, Swett returned to the United States on a Dutch motor ship, arriving in [[San Francisco]] on New Year's Eve. After less than 24 hours, he shipped out to [[San Diego]], where he was granted 30-days leave and married Lois Anderson, his longtime sweetheart. Swett was then transferred to [[NAS Santa Barbara]], [[California]], where he worked up a newly manned VMF 221 in the Corsair.

Now carrier-qualified and assigned to the {{USS|Bunker Hill|CV-17|6}}, Swett flew two strikes over [[Japan]] and then supported the landings at [[Iwo Jima]] and the operations on [[Okinawa]]. On May 11, 1945, he shot down a [[Yokosuka D4Y]] Judy [[kamikaze]], which he described as a "sitting duck". Swett watched from the air as the ''Bunker Hill'' was struck by two kamikazes, causing such damage that he was forced to land on another carrier.

Swett later returned to the States and was assigned to [[MCAS El Toro]], California, where he began to train for [[Operation Olympic]], the invasion of Japan. At war's end, VMF 221 was second in aerial victories among Marine Corps squadrons with 185 enemy planes downed. Swett's combat record includes 103 combat missions, 15.5 confirmed victories and four probables. He earned two [[Purple Heart]]s, eight [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]es, and the Medal of Honor.

===Post-war service===
Swett commanded VMF-141 flying Corsairs at [[NAS Alameda]], California, following the end of World War II. After the onset of the [[Korean War]] his squadron was deployed to Korea, but he was left behind because the Navy thought putting a Medal of Honor recipient in combat was too risky. Swett left active duty and continued service in the Marine Corps Reserve, retiring in 1970 in the rank of [[Colonel (United States)|colonel]].<ref name=WW_Swett>"Colonel James Elms Swett", ''Who's Who in Marine Corps History''</ref>

He worked in his father's company in San Francisco, making marine pumps and turbines. In 1960, after his father's death, Swett took over the company and ran it for 23 years, before passing it on to his son. In retirement, he became a frequent speaker at schools, where he shared his strong feelings about the values of respect and responsibility. In 2006, Swett's Medal of Honor action was recreated using computer graphics for [[History (U.S. TV channel)|The History Channel]] series ''Dogfights'' and Swett himself provided commentary. The episode first aired on November 24, 2006.<ref name=DogfightIMDB>{{Cite web|accessdate=2007-10-24|url=http://boards.imdb.com/title/tt0908310/|title="Dogfights" — Guadalcanal (2006)|publisher=[[Internet Movie Database]]}}</ref>

Swett moved to [[Redding, California]] in 2007 where he died on January 18, 2009,<ref name=CMOHS_bio>{{Cite web|accessdate=28 March 2009|url=http://www.cmohs.org/recipient-detail/3013/swett-james-elms.php|title=First Lieutenant Swett, James Elms|publisher=Congressional Medal of Honor Society}}</ref> in a Redding hospital from heart failure after a lengthy illness.<ref name=Redding>{{Cite news|accessdate=28 March 2009|url=http://www.redding.com/news/2009/jan/21/medal-honor-recipient-james-swett-redding-dies-88/|title=Medal of Honor recipient James Swett of Redding dies at 88|first=Jim|last=Schultz|work=Record Searchlight|location=Redding, CA|date=21 January 2009}}</ref><ref name=NYTobit>{{Cite news|accessdate=28 March 2009|url=https://www.nytimes.com/2009/01/26/washington/26swett.html?n=Top/Reference/Times%20Topics/Subjects/M/Military%20Personnel|title=James Swett, Who Downed 7 Planes in Attack, Dies at 88|first=Richard|last=Goldstein|date=25 January 2009|work=New York Times}}</ref>

The airport in Trinity Center, California was named in his honor.

==Medal of Honor citation==
The President of the United States takes pleasure in presenting the MEDAL OF HONOR to
<center>
'''FIRST LIEUTENANT JAMES E. SWETT'''<br/>
UNITED STATES MARINE CORPS RESERVE
</center>
for service as set forth in the following CITATION:

:For conspicuous gallantry and intrepidity at the risk of his life above and beyond the call of duty, as a division leader in Marine Fighting Squadron TWO TWENTY-ONE in action against enemy Japanese aerial forces in the [[Solomon Islands]] Area, April 7, 1943. In a daring flight to intercept a wave of 150 Japanese planes, [[First Lieutenant#United States|First Lieutenant]] Swett unhesitatingly hurled his four-plane division into action against a formation of fifteen enemy bombers and during his dive personally exploded three hostile planes in mid-air with accurate and deadly fire. Although separated from his division while clearing the heavy concentration of anti-aircraft fire, he boldly attacked six enemy bombers, engaged the first four in turn, and unaided, shot them down in flames. Exhausting his ammunition as he closed the fifth Japanese bomber, he relentlessly drove his attack against terrific opposition which partially disabled his engine, shattered the windscreen and slashed his face. In spite of this, he brought his battered plane down with skillful precision in the water off [[Tulagi]] without further injury. The superb airmanship and tenacious fighting spirit which enabled First Lieutenant Swett to destroy eight enemy bombers in a single flight were in keeping with the highest traditions of the [[Department of the Navy|United States Naval Service]].<ref name=USMC_MOH/>

/S/ [[Franklin D. Roosevelt|FRANKLIN D. ROOSEVELT]]

==See also==
*[[List of Medal of Honor recipients]]
*[[Jefferson J. DeBlanc]]

==References==
;Inline
{{Reflist}}

;General
:{{Marine Corps}}
*{{Cite web|accessdate=2007-10-21|url=http://www.tecom.usmc.mil/HD/Whos_Who/Swett_JE.htm|title=Colonel James Elms Swett, USMC (Retired)|work=Who's Who in Marine Corps History|publisher=History Division, United States Marine Corps}}
*{{Cite web|accessdate=|url=http://www.usmc.mil/moh.nsf/000003c919889c0385255f980058f5b6/0000033ba9f47a7385255fa600514787?OpenDocument|title=1stLt James E. Swett, Medal of Honor, 1943, Marine Fighting Squadron 221, Solomon Islands (Medal of Honor citation)|work=Marines Awarded the Medal of Honor|publisher=History Division, United States Marine Corps|archivedate=2007-03-05|archiveurl=https://web.archive.org/web/20070305080023/http://www.usmc.mil/moh.nsf/000003c919889c0385255f980058f5b6/0000033ba9f47a7385255fa600514787?OpenDocument}}
*{{Cite web|accessdate=2006-06-03|url=http://www.au.af.mil/au/goe/eaglebios/96bios/swett96.htm|title=James E. Swett (Bio)|work=Gathering of Eagles|publisher=USAF Air University|archiveurl=https://web.archive.org/web/20060423013135/http://www.au.af.mil/au/goe/eaglebios/96bios/swett96.htm|archivedate=2006-04-23}}
*{{Cite web|accessdate=|url=http://www.nps.gov/wapa/indepth/extContent/usmc/pcn-190-003122-00/sec7.htm|title=Post-Guadalcanal Operations|work=Time of the Aces: Marine Pilots in the Solomons}}
*{{Cite web|accessdate=|url=https://www.youtube.com/watch?v=A-C5xEorpRE|title=Guadalcanal|work=History Channel Dogfights: Guadalcanal, his personal interview}}
*{{Cite web|accessdate=|url=https://www.youtube.com/watch?v=bN-Hw0NfP80&feature=related|title=Guadalcanal|work=History Channel Dogfights: Guadalcanal, his personal interview}}
*{{Cite web|accessdate=2006-06-03|url=http://www.acepilots.com/usmc_aces.html#Swett|title=James E. Swett|work=Marine Corps Aces of World War Two — Wildcat and Corsair pilots at Guadalcanal and the Solomons|publisher=AcePilots.com}}

==Further reading==
*Mersky, Commander Peter B. [http://www.nps.gov/wapa/indepth/extContent/usmc/pcn-190-003122-00/index.htm ''Time of the Aces: Marine Pilots in the Solomons, 1942-1944''], Marines in World War II Commemorative Series, History and Museums Division, United States Marine Corps, 1993

==External links==
*{{Cite news|accessdate=September 29, 2010|url=http://www.washingtonpost.com/wp-dyn/content/article/2009/01/22/AR2009012203755.html|title=Washington Post obituary|work=The Washington Post|first=Adam|last=Bernstein|date=January 23, 2009}}

{{Top US World War II Aces}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=United States Marine Corps
| portal4=United States Navy
| portal6=World War II
}}

{{DEFAULTSORT:Swett, James E.}}
[[Category:1920 births]]
[[Category:2009 deaths]]
[[Category:American military personnel of World War II]]
[[Category:American World War II flying aces]]
[[Category:Aviators from Washington (state)]]
[[Category:Battle of Iwo Jima]]
[[Category:United States Marine Corps Medal of Honor recipients]]
[[Category:People from Seattle]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:United States Marine Corps officers]]
[[Category:United States Naval Aviators]]
[[Category:World War II recipients of the Medal of Honor]]