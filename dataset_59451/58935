{{Infobox journal
| title = Miscellanea Malacologica
| cover = [[File:Cover misc malacol.jpg]]
| editor = M. J. Faber, R. G. Moolenbeek, A. N. Van Der Bijl
| discipline = [[Malacology]]
| abbreviation = Misc. Malac.
| publisher = Marien Faber
| country = Netherlands
| frequency =
| history = 2004–present
| openaccess =
| license = 
| impact = 
| impact-year = 
| website = http://www.mollus.nl/page12.php
| link1 = http://www.mollus.nl/page3.php
| link1-name = List of volumes, issues, and papers
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 86123684
| LCCN = 
| CODEN = 
| ISSN = 1573-9953
| eISSN = 
}}

'''''Miscellanea Malacologica''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering [[malacology]], specifically  papers on the [[taxonomy (biology)|taxonomy]], [[nomenclature]], and [[zoogeography]] of [[mollusk]]s. The journal is published by Marien Faber ([[Duivendrecht]], the Netherlands) and was established in 2004. 

The name of the journal is [[Latin language|Latin]] for "malacological [[miscellany]]". The journal is a large format publication with color illustrations. It is published on an irregular basis: from 2004 to 2012 it had from two to five issues per year.<ref>{{cite web |url=http://www.amimalakos.com/pubblicazioni/misc-mal.htm |title=Miscellanea Malacologica |publisher=Associazione Malacologica Internazionale |issn=2037-9005 |work=Malacological News |accessdate=2013-04-30}}</ref> The journal is abstracted and indexed in [[The Zoological Record]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-04-30}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official|http://www.mollus.nl}}
* [http://www.mollus.nl/page10.php List] of [[Type (biology)|type specimens]] of [[Taxon|taxa]] described in the journal

[[Category:Malacology journals]]
[[Category:Publications established in 2004]]
[[Category:English-language journals]]