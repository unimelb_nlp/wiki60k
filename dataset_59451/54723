:''For the [[pipevine]], see ''[[Aristolochia longa]]''.
{{Infobox book
| name          = Sarrasine
| title_orig    =
| translator    =
| image         = BalzacSarrasine01.jpg
| caption = Image from ''Sarrasine''.
| author        = [[Honoré de Balzac]]
| illustrator   = Alcide Théophile Robaudi
| cover_artist  =
| country       = [[France]]
| language      = [[French language|French]]
| series        = [[La Comédie humaine]]
| subject       =
| genre         =
| publisher     = Charles Gosselin
| pub_date      = [[1831 in literature|1831]]
| english_pub_date =
| media_type    =
| pages         =
| isbn          =
| oclc          =
| preceded_by   = [[Facino Cane (novel)|Facino Cane]]
| followed_by   = [[Pierre Grassou]]
}}
'''''Sarrasine''''' is a novella written by [[Honoré de Balzac]]. It was published in 1830, and is part of his ''[[La Comédie Humaine|Comédie Humaine]]''.

==Introduction==

[[Balzac]], who began writing in 1819 while living alone in the rue Lesdiguières, undertook the composition of ''Sarrasine'' in 1830. Although he had steadily produced work for over a decade (without commercial success), ''Sarrasine'' was among his earliest publications to appear without a pseudonym.

During the period in which the novella was written, Balzac was involved in many [[Salon (gathering)|salons]], including that of [[Madame Recamier]]. Around the time in which ''Sarrasine'' was published, Balzac experienced great success with another work, ''[[La Peau de Chagrin]]'' (1831). As his career began to take off and his publications began to accumulate, Balzac developed increasingly lavish living habits and frequently made impulsive purchases (such as new furniture for his apartment and a hooded white cashmere gown designed to be worn by a monk, which he wore at night while writing), likely to distance himself from his family’s prior debt, which had resulted from his business as an editor and printer’s liquidation.

In 1841, an ill Balzac reached an agreement with Furne & Co., Dubochet, Hetzel and Paulin to publish ''[[La Comédie humaine]]''. In the 10 years that elapsed,{{clarify|date=October 2014}} Balzac had developed a political career, becoming heavily involved in [[High society (group)|high society]], which influenced much of his writing. However, he continued to have financial difficulties despite his success, such as with ''La Cronique de Paris'', a magazine he founded and abandoned, though he characteristically hid his worry in order to maintain appearances.

The first volume of ''[[La Comedie Humaine]]'' went on sale in July 1842. ''Sarrasine'' is part of its "Scenes de la vie parisienne".<ref>{{cite book|last=Bertault|first=Philippe|title=Balzac and The Human Comedy|year=1963|publisher=New York University Press|location=New York City|pages=vii-xvi}}</ref>

==Commentary==

[[Balzac]]'s ''Sarrasine'' was not paid much attention to prior to [[Roland Barthes]]' blow-by-blow [[structuralism|structuralist]]/[[post-structuralist]] analysis of the text in his book ''[[S/Z]]''(1970). Barthes dissects the text in accordance with five "codes" ([[hermeneutic]], semic, symbolic, proairetic, and cultural).

==Plot summary==

Around midnight during a ball the narrator is sitting at a window, out of sight, admiring the garden. He overhears the conversations of passers-by regarding the origins of the wealth of the mansion's owner, Monsieur de Lanty. There is also the presence of an unknown old man around the house, whom the family was oddly devoted to, and who frightened and intrigued the partygoers. When the man sits next to the narrator’s guest, Beatrix Rochefide, she touches him, and the narrator rushes her out of the room. The narrator says he knows who the man is and says he will tell her his story the next evening.

The next evening, the narrator tells Mme de Rochefide about Ernest-Jean Sarrasine, a passionate, artistic boy, who after having trouble in school became a prodigy of the sculptor [[Bouchardon]]. Sarrasine becomes a talented young man and, after one of his sculptures wins a competition, he heads to Rome where he sees a theatre performance featuring Zambinella. He falls in love with her, going to all of her performances and creating a clay mold of her. After spending time with her at a party, Sarrasine attempts to seduce Zambinella.  She is reticent, suggesting some hidden secret or danger to their alliance. Sarrasine becomes increasingly convinced that Zambinella is the ideal woman.  Sarrasine develops a plan to abduct her from a party at the French embassy. When Sarrasine arrives, Zambinella is dressed as a man. Sarrasine speaks to a cardinal, who is Zambinella’s patron, and is told that Zambinella is a [[castrato]]. Sarrasine refuses to believe the cardinal and leaves the party, seizing Zambinella. Once they are at his studio, Zambinella confirms that she is a castrato. Sarrasine is about to kill Zambinella as a group of the cardinal’s men barge in and stab Sarrasine. The narrator then reveals that the old man around the household is Zambinella, Marianina's maternal great uncle. The story ends with Mme de Rochefide's expressing her distress about the story she has just been told.

== Characters ==
'''The Narrator''' - The narrator tells the story of Sarrasine to Madame Rochefide, as a way to seduce her. He is a member of Paris' upper class and regularly frequents its grand balls.

'''Madame Rochefide''' - A delicate woman of great beauty whom the narrator invited to Monsieur de Lanty's ball.

'''Marianina''' - The de Lanty’s sixteen-year-old daughter who is strikingly beautiful, educated, and witty. Also described as sweet and modest, she could bring the same level purity of sound, sensibility, rightness of movement and pitch, soul and science, correctness, and feeling as the sultan’s daughter in the Magic Lamp could.

'''Filippo''' - Filippo is Marianina’s brother and Count de Lanty's son. He is handsome with skin of olive complexion, defined eyebrows, and fire of velvet eyes, and is often considered an ideal partner to many girls and mothers finding husbands for their daughters. He is also described as a walking image of [[Antinous]].

'''Monsieur de Lanty''' - The wealthy owner of the mansion hosting the ball. He is small, ugly, and pock-marked, a complete contrast to his wife and children. He is dark skinned like a Spaniard, dull as a banker, and compared to a politician because he is cold and reserved.

'''Madame de Lanty''' - County de Lanty’s beautiful wife and mother of Marianina and Filippo.

'''Ernest-Jean Sarrasine''' - The only son of a rich lawyer who, rather than following in his father's path as the family wants, becomes an artist, eventually having his talent as a sculptor recognized by Bouchardon.  He is generally more interested in art than in women, but on a trip to Italy falls in love with the opera star, La Zambinella, who serves as the model for his most perfect statue.  When he learns that Zambinella is a castrato, he tries to kill Zambinella and is himself killed instead.

'''Zambinella''' - A star of the Roman opera and the object of Sarrasine’s affection. Sarrasine is convinced that La Zambinella is the ideal woman.  La Zambinella is in fact a castrato.

'''Bouchardon''' - Sculptor who taught Sarrasine as a student and took him in as his own pupil.

==Themes==

===Opposites===
''Sarrasine'' is marked by oppositions.  The story opens with a description of the extremes of inside and out, day and night, beauty and ugliness, age and youth, male and female that prevail in French high society and at the de Lanty's ball.  Whereas the ball is young and full of life, the mysterious old man who enters it stands out as the mark of opposition. “If I look at him again, I shall believe that death itself has come looking for me,” says one beautiful young woman. 
The most significant opposite in the entire novella is man versus woman or male versus female. The story contemplates what it means to be a man and what it means to be a woman, and the degree to which those stand in opposition. The story also touches on oppositions between the generations, as Sarrasine himself is opposite to his father, on oppositions between the art world and the political world, on oppositions between France and Italy, and on oppositions between the ideal and the real.{{citation needed|date=October 2014}}

===Castration===
Roland Barthes identifies castration as one of the novella's main concerns. Zambinella is a castrato.  Because women were not allowed on the Roman stage, castrati regularly played the parts of women.  The tradition of the castrati ended in France before it did in Italy, and when Sarrasine arrives in Italy and meets Zambinella, he does not know about it.  Because Zambinella has the voice of a woman Sarrasine assumes La Zambinella is a woman. La Zambinella suggests that her womanhood might be in question, but Sarrasine is too enthralled with La Zambinella as the perfect woman to pay any attention. When Sarrasine finally learns Zambinella is a castrato, he first denies the possibility, then tries to kill La Zambinella, upon which he is himself killed.  Critics point out that Sarrasine may fear a kind of contagion of castration, or may feel that manhood in general or the division between men and women is threatened by possibility of castration.{{citation needed|date=October 2014}}  The novella ends with Mme de Rochefide and the narrator's condemning the castrato tradition as barbarian.

===Homosexuality===
Homosexuality is a common theme found in many of Honoré de Balzac's works, for example ''[[Illusions perdues]]'' (1837–43). In ''Sarrasine'', we meet Zambinella, a seemingly beautiful woman whom Sarrasine admires, but who turns out to be castrato. Sarrasine, who took Zambinella to be his ideal woman, is deeply distressed when he learns this and tries to kill Zambinella.  One possible explanation for Sarrasine's extreme reaction is that he fears that his love of La Zambinella is a mark of homosexuality.  Sarrasine's reaction, then, can be seen as an attempt to protect his heterosexuality. Zambinella does, in fact, have a partner: the cardinal. In ''Sarrasine'', the cardinal is Zambinella's "protector", which means that Zambinella would be the complementary role of "''mignon''". Barthes refers to Zambinella as "''mignon''" as it is used in French court society, where it means the homosexual lover, or "pet", of a man in power, in this case the cardinal, the "protector".<ref>{{cite book|title=The Tenor of "Sarrasine"}}</ref>

==Narrative strategies==
[[Honoré de Balzac|Balzac]]’s use of a [[frame story]] is the most significant narrative strategy in ''Sarrasine''.{{citation needed|date=October 2014}} In the frame story, an unnamed, male tells the story in the [[First-person narrative|first person]] to Mme de Rochefide, his guest at a ball. They come into close contact with a mysterious old man and see a beautiful painting.  The narrator promises to tell Mme de Rochefide the story of the painting and the old man.  The body of the novella and the framed story that the narrator relates to Mme de Rochefide are about Ernest Jean Sarrasine and Sarrasine's unusual relationship with Zambinella. [[Honoré de Balzac|Balzac]] also employs [[Nonlinear narrative|nonlinear narration]] in ''Sarrasine'': the framed story takes place many years earlier than it is related, and a few times the narrator jumps to the present and then returns to telling the framed story.{{citation needed|date=September 2014}}

==Historical context==

===Castrati in opera===
Sarrasine gives us a closer look at the role of [[Castrato|castrati]] in both common opera and in religious tradition. Catholicism in Italy dictated that there could be no female singers, and the high voice parts were usually played by either prepubescent boys or castrati. In order to become a [[castrato]], a boy had to give up his "manhood", i.e., have his testes removed at a very early age. Because of the popularity of Italian opera throughout 18th-century Europe (except France), castrati such as [[Farinelli]], Ferri, Pacchierotti, and [[Senesino]] became the first operatic superstars, earning enormous fees and hysterical public adulation.  However, many did not survive the surgery, or did not last very long after it. Castrati developed many health problems, as testosterone is needed for healthy growth in boys, and without the glands that supply the majority of testosterone during a critical period of development, the body does not grow correctly. Besides the only wanted side effect (the lack of lengthening the [[vocal chords]]), a castrato's arms and legs were often disproportionally long, they did not have much muscle mass, and other problems, such as [[osteoporosis]] and [[erectile dysfunction]] were common later in life. The story of Sarrasine is made much more believable by the fact that, due to their severe hormonal imbalance, castrati often developed real breast tissue, a condition called [[gynecomastia]].<ref>{{cite web|url=http://www.mayoclinic.com/health/male-hypogonadism/DS00300/DSECTION=symptoms |website=Mayo Clinic|title=Male Hypogonadism}}</ref>

==Literary context==

===Other 19th century French literature===
French literature flourished in the nineteenth century. Among the most famous authors from this time period is [[Victor Hugo]], who was known for works such as ''[[Les Misérables]]'' (1862), ''[[The Hunchback of Notre-Dame]]'' (1831), and ''[[The Toilers of the Sea]]''. Hugo was also known for influencing [[Romanticism]], a movement that spread to France in the 1820s and emphasized a sense of individuality and emotion. In his novel ''Les Misérables'', Hugo represents Romanticism and individuality with the character of Marius Pontmercy, who by attempting to court the character Cosette by sending her letters that reveal his love for her, adheres to Romantic movement ideas and beliefs.

Equally important is [[Gustave Flaubert]], known for his use of [[realism (arts)|realism]], which was influenced by [[Honoré de Balzac]]. Realism appears perhaps most famously in Flaubert’s first (and perhaps most famous{{citation needed|date=October 2014}}) novel ''[[Madame Bovary]]'' (1856). This novel represents the "real" experiences and feelings of a French woman who is obsessed with, and eventually dispossessed of, her romantic ideal of love.{{citation needed|date=October 2014}} In ''[[Salammbô]]'' (1862), Flaubert abandons realism for historical fiction with an account of the Mercenary Revolt in [[Tunisia]] during the 3rd century BCE.{{citation needed|date=October 2014}} Flaubert then returns to realism in ''L’Éducation Sentimentale'' (''[[Sentimental Education]]'', 1869) with a detailed account of lives during the revolution of 1848 in France. Other influential authors from this period, include Marie-Henri Beyle ( better known by the pseudonym [[Stendhal]]) and [[Charles Baudelaire]].{{citation needed|date=October 2014}}

===Realism===
[[Realism (arts)|Realism]] is an artistic movement originated in France in the 19th century by people who rejected both [[Idealism]] and [[Romanticism]]. The use of Romanticism in literature began to rise dramatically in the [[18th century in literature|18th century]] and was the predominant artistic movement in France until Realism. Realism was widely appreciated by people who opposed the inflated ideas of passion and drama that mark Romanticism. Those in the Realist movement wanted instead to portray the truth in every situation, avoiding exaggerating a scenario to emphasize only its good or bad qualities.  Realism also strove to represent life as it was experienced in its more mundane details by imperfect men and women rather than idealized characters in idealized situations.  Realism tends to describe middle or lower class milieux in order to paint a picture of the regular life of a majority of the population at the time the literature was written. From the people to the places, Realism strove to present everything in an undramatic and "true" manner.{{citation needed|date=September 2014}}

In ''Sarrasine'', Realism is reflected in the ways that every situation is described in its positive and negative aspects. For example, as a member of the [[castrato]] Zambinella can be praised, adored, or treated as if he doesn’t belong. The novella doesn’t romanticize the relationship between Zambinella and Sarrasine, either. The author depicts real and imperfect emotions between the two characters, from love to vengeance. Though Realism in literature was usually used for portraying the activities of middle and lower-class people, it was sometimes used in situations like this, and indeed often focused on characters and situations that might otherwise be socially marginalized.{{citation needed|date=September 2014}}

===Allusions and intertexts===
''Sarrasine'' makes many references and allusions to other sources, often to literature ([[Lord Byron]], [[Ann Radcliffe]], [[Jean-Jacques Rousseau]]), music ([[Gioacchino Rossini]]), religion, and the arts ([[Girodet]]'s ''[[Endymion (mythology)|Endymion]]'', [[Michelangelo]]). The most important allusions are to the figures of beauty in Greek culture: [[Adonis]], [[Endymion (mythology)|Endymion]], and [[Pygmalion (mythology)|Pygmalion]]. The intertexuality between ''Sarrasine'' and the myth of Pygmalion is a vital one, as it establishes the tragedy of misconception: Sarrasine creates a statue of the "female" La Zambinella, only to discover later that his subject wasn't a real "woman" as Sarrasine—that "a real woman is born from the statue".<ref>Barthes, Roland. ''S/Z/'', p. 208.</ref> Furthermore, the replication of the statue into marble, and into two separate portraits (Adonis, and Girodet's ''Endymion'') only perpetuates the symbolic notion that Sarrasine is always influenced by an intrinsic gender ambiguity.{{citation needed|date=October 2014}}

==Text==
*{{cite book|publisher=Project Gutenberg |edition=English translation |title=Sarrasine |url=http://onlinebooks.library.upenn.edu/webbin/gutbook/lookup?num=1826}}
*[[:s:fr:Sarrasine|Sarrasine]] {{fr icon}}

==Renditions in other media==
The composer Richard Beaudoin is writing an opera based on ''Sarrasine.''{{Citation needed|date=October 2013}}<ref>{{cite web|website=RichardBeaudoin.com|url=http://richardbeaudoin.com/|title=Home page}}</ref>

==Notes==
{{Reflist}}

==Sources==
* [[Roland Barthes|Barthes, Roland]]. ''[[S/Z: An Essay|S/Z]]''. Hill and Wang: New York, 1974.
* [[Jacques Derrida|Derrida, Jacques]]. ''Dissemination''. The University of Chicago Press: Chicago, 1981.
* [[Michel Foucault|Foucault, Michel.]] ''[[The History of Sexuality]]: An Introduction, Vol. I''.  Trans. Robert Hurley.  New York: Vintage Books, 1990.
* [[Michel Foucault|Foucault, Michel.]] ''[[Madness and Civilization|Madness and Civilization: A History of Insanity in the Age of Reason]]''. Trans. Richard Howard. New York: Vintage Books, 1988.
* Livia, Anna.  ''Pronoun Envy: [[Literary]] Uses of [[Linguistic Gender]]''. New York: Oxford UP, 2001.
* Katherine Kolb, PMLA, Vol. 120, No. 5 (Oct., 2005), pp.&nbsp;1560–1575 Published by: Modern Language Association
* Article Stable URL: http://www.jstor.org/stable/25486268
* Noble, Yvonne. "[[Castrati]], Balzac, and Barthes' ''S/Z''." ''Comparative Drama''. Kalamazoo: Spring 1997. Vol. 31, Iss. 1. pp28–42.
* Sprenger, Scott. “Mind as Ruin,” Stories of the Earth, New York/Amsterdam, Rodopi, 2008, 119-136.
* Sprenger, Scott. “Sarrasine de Balzac ou l’archéologie du moi moderne,” La Plume et la pierre: l’écrivain et le modèle archéologique au <small>XIX</small><sup>e</sup> siècle, Nîmes, Champ Social, 2007, 291-318.
* Stoltzfus, Ben. ''[[Jacques Lacan|Lacan]] and Literature : Purloined Pretexts''. SUNY Press: Albany, 1996. p145.
*Petrey,Sandy. "Castration, Speech Acts, and the Realist Difference: S/Z versus Sarrasine"Vol. 102, No. 2 (Mar., 1987), pp.&nbsp;153–165, Published by: Modern Language Association
* Bertault, Philippe. Balzac and The Human Comedy. New York: New York University Press, 1963. Print.

==External links==
* {{librivox book | title=Sarrasine | author=Honoré de BALZAC}}

{{Honoré de Balzac}}

{{Authority control}}

[[Category:Books of La Comédie humaine]]
[[Category:1830 short stories]]