{{Infobox person
| name                      = Andrew Farago
| image                     = 
| image_size                = 
| alt                       = 
| caption                   =
| birth_name                = 
| birth_date                = {{Birth date and age|1976|05|12}}
| birth_place               = 
| monuments                 = 
| residence                 = 
| nationality               = 
| other_names               = 
| ethnicity                 = <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               = American
| education                 = 
| alma_mater                = Colorado College
| occupation                = Curator and author
| years_active              = 
| employer                  = 
| organization              = 
| agent                     = 
| known_for                 = 
| notable_works             = 
| style                     = 
| home_town                 = 
| spouse                    = [[Shaenon K. Garrity]]
| parents                   = 
| relatives                 = 
| awards                    = 
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 = 
| box_width                 = 
}}

'''Andrew Farago''' (born May 12, 1976) is the curator of the [[Cartoon Art Museum]]<ref>[http://cartoonart.org/about-us/staff/ Cartoon Art Museum Staff List]</ref> in [[San Francisco]], author, chairman of the North California chapter of the [[National Cartoonists Society]],<ref>[http://www.reuben.org/chapters/ National Cartoonist Society Chapters]</ref> and husband of [[webcomic]]s author and illustrator [[Shaenon K. Garrity]].<ref name=jazma>[http://www.jazmaonline.com/interviews/interviews2009.asp?intID=298 Jazma Interview] {{webarchive |url=https://web.archive.org/web/20150402134428/http://www.jazmaonline.com/interviews/interviews2009.asp?intID=298 |date=April 2, 2015 }}</ref>

Farago began his writing career in the mid 2000s by writing for various print and online magazines. He went on to author various books on cartooning most notably 2014's ''Teenage Mutant Ninja Turtles: The Ultimate Visual History'',<ref>[http://comicsalliance.com/teenage-mutant-ninja-turtles-the-ultimate-visual-history-coming-in-june-preview/ TMNT: A Visual History preview] {{webarchive|url=https://web.archive.org/web/20150213042211/http://comicsalliance.com/teenage-mutant-ninja-turtles-the-ultimate-visual-history-coming-in-june-preview/ |date=2015-02-13 }}</ref> a book that chronicles the creative and business history of the [[Teenage Mutant Ninja Turtles]] franchise.<ref name=marketwatch>[http://www.marketwatch.com/story/teenage-mutant-ninja-turtles-the-ultimate-visual-history-reveals-the-never-before-told-story-of-the-hit-franchise-2014-05-05 Market Watch article on TMNT: A Visual History]</ref>

==Education==
Farago graduated from [[Colorado College]] with a degree in Studio Art.<ref name="wcp">[http://www.washingtoncitypaper.com/blogs/artsdesk/books/2013/08/23/meet-a-visiting-comics-curator-a-chat-with-andrew-farago/ Washington City Paper Interview]</ref>

==Personal life==
Farago is married to Shaenon K. Garrity, herself a webcomic creator.

Both Farago and Garrity have both been big supporters of [[Richard Thompson (cartoonist)|Richard Thompson]]'s collaborative project Team Cul de Sac which aims to help find a cure for [[Parkinson's disease]].<ref>[http://teamculdesac.blogspot.com/2011/07/shaenon-k-garrity-and-andrew-farago.html Team Cul de Sac blog entry]</ref>

==Awards and Honors==
Farago received an Inkpot Award for Fandom Services at the 2015 San Diego Comic-Con.

''Teenage Mutant Ninja Turtles: The Ultimate Visual History'', written by Farago, won the 2015 Harvey Award for Best Biographical, Historical, or Journalistic Presentation.

==Cartoon Art Museum==
Andrew Farago has worked for the Cartoon Art Museum in San Francisco since the summer of 2000, starting as a volunteer. It is here that he met his future wife.<ref>[http://www.sfweekly.com/sanfrancisco/best-professional-fanboy/BestOf?oid=2204899 SF Weekly "Best Professional Fanboy"]</ref> He moved from volunteer status to a paid position in 2001, and in 2005 he was formally named Curator of the Cartoon Art Museum.<ref>[http://www.fantagraphics.com/index.php?option=com_myblog&show=Cartoon-Art-Museum-Highlight-Andrew-Farago.html&Itemid=113 Fantastagraphics Interview]</ref> In his time with CAM he has curated over 100 exhibits.<ref name="wcp" />

==Writing career==
Although remaining with the Cartoon Art Museum, Farago began to channel his love of [[cartoon]]s into other venues.

===Magazines===
Farago has written for a number of magazines both online and in print related to cartoons such as [[Animation World Network]],<ref>[http://www.awn.com/users/andrew-farago/posts Animation World Network]</ref> [[The Comics Journal]],<ref name=cj>[http://www.tcj.com/the-comics-journal-285-october-2007/ Interview with Keith Knight]</ref> and [[The Comics Reporter]].<ref name=cr>[http://www.comicsreporter.com/index.php/resources/interviews/2251/ Interview with Kyle Baker]</ref> Topics covered have included the San Francisco-based [[comic book convention]] [[WonderCon]],<ref>[http://www.awn.com/animationworld/wondercon-2006-beckons-hollywood-bay-area Wondercon 2006 coverage]</ref><ref>[http://www.awn.com/animationworld/wondercon-2007-bay-area-goes-geeky Wondercon 2007 coverage]</ref> interviews with [[Patton Oswalt]],<ref>[http://www.awn.com/animationworld/ratatouille-wondercon-jar-jar-binks-patton-oswalt-talks Interview with Patton Oswald]</ref> [[Kyle Baker]],<ref name="cr" /> and [[Keith Knight (cartoonist)|Keith Knight]],<ref name="cj" /> as well as a retrospective on the 20th anniversary of [[Who Framed Roger Rabbit]]<ref>[http://www.awn.com/animationworld/roger-rabbit-turns-20 Roger Rabbit 20 year retrospective at AWN]</ref>

===Comic work===
Farago's first professional [[comic book|comic]] writing came in the mid 2000s, co-writing with his wife for multiple editions of the ''[[Marvel Entertainment|Marvel]] Holiday Special''.<ref>[http://www.shaenon.com/cv.html Shaenon Garrity's website]</ref><ref>[http://marvel.com/comics/creators/5723/andrew_farago Marvel website]</ref><ref>[http://comicbookdb.com/creator.php?ID=3543 Comicbook DB]</ref> In 2007 he launched ''The Chronicles of William Bazillion'', a webcomic surrounding the exploits of the titular character William Bazillion.<ref name="jazma" /><ref>[http://www.webcomicsnation.com/andrew/bazillion/toc.php William Bazillion webpage]</ref>

===Notable works===
''The Looney Tunes Treasury'' was the first full-length work by Farago, which told the fictional histories of the [[Looney Tunes]] characters in the voices of the characters themselves.<ref>[http://www.awn.com/animationworld/book-review-looney-tunes-treasury AWN review of Looney Tunes Treasury]</ref>

In June 2014 Insight Editions published ''Teenage Mutant Ninja Turtles: The Ultimate Visual History'', authored by Farago. Likely the first comprehensive history of the franchise, Farago "conducted over 60 interviews over the course of two years"<ref name="fog">[http://www.forcesofgeek.com/2014/07/fog-chats-with-andrew-farago-author-of.html Force of Geek interview]</ref> in the process of writing it.

The release was designed to coincide with the release of the [[Michael Bay]] produced [[Teenage Mutant Ninja Turtles (2014 film)|rebooted movie]], and was pushed back when the movie was so that it also coincided with the 30th Anniversary of the franchise.<ref>[http://www.teenagemutantninjaturtles.com/blog/author-turtles-ultimate-visual-history-andrew-farago-talks-book-new-turtles-movie/ teenagemutantninjaturtles.com interview]</ref> This book featuring art from the [[Teenage Mutant Ninja Turtles]] franchise, including the various comics, animated series, and movies, as well as interviews with the creators of the series ([[Kevin Eastman]] and [[Peter Laird]]) and numerous others who have been involved in the series over the years.<ref name="marketwatch" /> The book was met with largely favorable reaction in the press and from fans, earning a Gold Medal for Best Popular Culture Book from the Independent Publisher Book Awards <ref>[http://www.independentpublisher.com/article.php?page=1936]</ref> as well as a Harvey Award nomination for Best Biographical, Historical, or Journalistic Presentation.<ref>{{cite web|url=http://www.harveyawards.org/2015-final-ballot/ |title=Archived copy |accessdate=2015-07-16 |deadurl=yes |archiveurl=https://web.archive.org/web/20150716173831/http://www.harveyawards.org/2015-final-ballot/ |archivedate=2015-07-16 |df= }}</ref>

==Bibliography==
*Spark Generators II (2002)
*Marvel Holiday Special 2005 (2005)
*Marvel Holiday Special 2006 (2006)
*Marvel Holiday Special 2007 (2007)
*The 100 Greatest Looney Tunes (2010)
*Looney Tunes Treasury: Includes Amazing Interactive Treasures from the Warner Bros. Vault! (2010)
*G.I. Joe: Cobra H.I.S.S. Tank: Includes Light & Sound! (2014)
*Teenage Mutant Ninja Turtles: The Ultimate Visual History (2014)
*Adventure Time Bowling (2016)

==References==
{{Reflist|30em}}

==External links==
*[https://andrewfarago.wordpress.com/ Andrew Farago's WordPress]
*{{IMDb name|nm2253621}}

{{DEFAULTSORT:Farago, Andrew}}
[[Category:Colorado College alumni]]
[[Category:Comics critics]]
[[Category:1976 births]]
[[Category:American webcomic creators]]
[[Category:Living people]]
[[Category:Writers from San Francisco]]