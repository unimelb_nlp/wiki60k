<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=C.430 Rafale
 | image=Boucher devant son Caudron Rafale.jpg
 | caption=Hélène Boucher with her C.430 Rafale
}}{{Infobox Aircraft Type
 | type=Two seat touring aircraft
 | national origin=[[France]]
 | manufacturer=[[Caudron|Société des Avions Caudron]]
 | designer=Marcel Riffard
 | first flight=c.24 March 1934
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Caudron C.430 Rafale''' was a fast, two seat [[France|French]] touring [[monoplane]]. Soon after its first flight in 1933 it set an international class speed record.

==Design and development==

The C.430 Rafale was a two-seat development of the single seat [[Caudron C.362]], the winner of the 1933 [[Coupe Deutsch de la Meurthe]]. Slightly larger and heavier, though with a lower [[wing loading]], the Rafale was a low wing [[cantilever]] monoplane, wood framed and covered with a mixture of [[plywood]] and [[aircraft fabric covering|fabric]]. Its one piece, single [[spar (aviation)|spar]] wing was strongly straight tapered to elliptical tips and was [[plywood]] covered with an outer layer of fabric. There were [[flap (aircraft)|flaps]] inboard of the [[ailerons]].<ref name=Flight34/><ref name=Laero/>

Its [[fuselage]] was flat sided and fabric covered, with a deep, rounded decking running the full length. It had an air cooled {{convert|150|hp|kW|abbr=on|0|order=flip}}  inverted four cylinder {{convert|6.3|l|cuin|abbr=on|0}} [[straight engine|inline]] [[Renault 4Pei]] Bengali engine in the nose, driving a two blade, two position variable [[blade pitch|pitch]] [[propeller (aircraft)|propeller]]. The Rafale's two seats were in tandem, one over the wing and the other  behind the [[trailing edge]], under a long (about a third of the fuselage length), narrow multi-framed canopy with a blunt, vertical windscreen and sliding access. Behind the canopy a long fairing continued its profile to the straight tapered, round tipped vertical tail, which included a [[balanced rudder]] that ended at the top of the fuselage. The tapered horizontal tail, with inset [[elevator (aircraft)|elevator]]s, was mounted on the top of the fuselage largely ahead of the [[fin]]. Construction of the [[empennage]] was similar to that of the wing. The Rafale had a fixed [[landing gear|tailskid undercarriage]]. Its wheels were on vertical legs from the wings and were largely enclosed within magnesium [[aircraft fairing|spats]].<ref name=Flight34/><ref name=Laero/>

The C.430 Rafale probably flew for the first time in the last week of March 1934, though the ''Flight'' article<ref name=Flight34/> specifically refers to ''F-AMVB'' which alphabetically at least was the second of the type.<ref name=FrReg/>

==Operational history==

On 31 March 1934, only about a week after its first flight, the C.430 ''F-AMVB'' set a new International speed record of {{convert|292|km/h|mph|abbr=on|0}} over {{convert|100|km|mi|abbr=on|0|}} for aircraft with an empty weight less than {{convert|560|kg|lb|abbr=on|0}}.<ref name=Flight34/>

[[Hélène Boucher]], a prominent French pilot in the mid-1930s, died in a landing approach accident in ''F-AMVB'' on 30 November 1934.<ref name=FrReg/>

Though Caudron dominated the 1935 Coupe Deutsch de la Meurthe, the single seat '''C.430/1 Rafale''', ''F-AMVA'' re-engined with a more powerful  {{convert|180|hp|kW|abbr=on|0|order=flip}} [[Renault 438]],<ref name=afr2/>  was outclassed by the single seat [[Caudron C.460|C.450 and C.460]] machines and retired with engine problems after a few  circuits.<ref name=Flight35/>

==Variants==

;C.430: As described below; two built.
;C.430/1: ''F-AMVA'' fitted with a {{convert|180|hp|kW|abbr=on|0|order=flip}} [[Renault 438]] engine in October 1934 for the 1935 Coupe Deutsch de la Meurthe.<ref name=FrReg/><ref name=afr2/>

==Specifications (C.430) ==
{{Aircraft specs
|ref=l'Aérophile April 1933 p.114<ref name=Laero/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Two
|length m=7.1
|length note=
|span m=7.7
|span note=
|height m=1.88
|height note=<ref name=afr/>
|wing area sqm=9.0
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=480
|empty weight note=
|gross weight kg=820
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|160|l|Impgal USgal|abbr=om|1}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Renault 4Pei]]
|eng1 type={{convert|6.3|l|cuin|abbr=on|0}} 4 cylinder, air cooled, inverted [[straight engine|inline]]
|eng1 hp=150
|eng1 note=maximum
|more power=

|prop blade number=2
|prop name=[[Ratier]], metal two position variable [[blade pitch|pitch]], adjustable in flight
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=305
|max speed note=
|cruise speed kmh=260
|cruise speed note=at 75% power
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1000
|range note=at 75% power
|endurance=<!-- if range unknown -->
|ceiling m=5750
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
*'''Landing speed:''' {{convert|95|km/h|mph|abbr=on|0}}

}}

==References==

{{columns-list|2|
{{reflist|refs=

<ref name=Flight34>{{cite magazine |last= |first= |authorlink= |coauthors= |date=12 April 1934 |title= A new light plane record|magazine= [[Flight International|Flight]]|volume=XXVI |issue=1320|page=359  |url= http://www.flightglobal.com/pdfarchive/view/1934/1934%20-%200359.html  }}</ref>

<ref name=Flight35>{{cite magazine |last= |first= |authorlink= |coauthors= |date=30 May 1935 |title= A Family Affair|magazine== [[Flight International|Flight]]|volume=XXVII |issue=1324|page=1379 |url= http://www.flightglobal.com/pdfarchive/view/1935/1935%20-%201324.html  }}</ref>

<ref name=Laero>{{cite magazine |last= |first= |authorlink= |coauthors= |date=April 1933 |title=Le Biplace de Sport Caudron C.430, 130 CV (France)|magazine=L'Aérophile| volume=42|issue=4|page=114|url=http://gallica.bnf.fr/ark:/12148/bpt6k6553677b/f36.image }}</ref>

<ref name=afr>{{cite web |url=http://www.aviafrance.com/caudron-c-430-rafale--aviation-france-9840.htm|title=Caudron C.430 Rafale |author= |date= |work= |publisher= |accessdate=7 March 2015}}</ref>

<ref name=afr2>{{cite web |url=http://www.aviafrance.com/caudron-c-430-1-rafale--aviation-france-9842.htm|title=Caudron C.430/1 Rafale |author= |date= |work= |publisher= |accessdate=7 March 2015}}</ref>

<ref name=FrReg>{{cite web |url=http://www.ab-ix.co.uk/f-aaaa.pdf|title=French pre-war register |author= |date= |work= |publisher= |accessdate=7 March 2015}}</ref>

}}
}}

<!-- ==Further reading== -->
<!-- ==External links== -->

{{Caudron aircraft}}

[[Category:Caudron aircraft|C.430]]
[[Category:French sport aircraft 1930–1939]]