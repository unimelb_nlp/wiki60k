{{about|the journal|the relationship between philosophy and literature|philosophy and literature}}
{{Infobox Journal
| title = Philosophy and Literature
| cover	=	[[Image:Philosophy and literature.gif]]
| editor=               [[Denis Dutton]], [[Garry Hagberg]]
| discipline	=	[[Philosophy]], [[Literature]]
| abbreviation	=	Philos. Literature
| website	=	http://www.press.jhu.edu/journals/philosophy_and_literature/
| RSS           =       http://www.press.jhu.edu/cgi-bin/journals_syndication.cgi
| link1         =       http://muse.jhu.edu/journals/philosophy_and_literature/
| link1-name    =       Online access 
| publisher	=	[[Johns Hopkins University Press]]
| country	=	United States
| frequency     =       Biannually
| history	=	1977–present
| ISSN	        =	0190-0013
| eISSN         =       1086-329X
| OCLC          =       33895278
}}
'''''Philosophy and Literature''''' is an [[academic journal]] founded in 1977. It explores the connections between literary and philosophical studies by presenting ideas on the [[aesthetics]] of literature, [[Literary criticism|critical theory]], and the [[Philosophy and literature|philosophical interpretation of literature]]. The journal, which has been characterized as "culturally conservative,"<ref>{{Cite news | last = Butler | first = Judith | title = A 'Bad Writer' Bites Back | work = New York Times | accessdate = 2010-10-30 | date = 1999-03-20 | url = https://pantherfile.uwm.edu/wash/www/butler.htm }}</ref> aims to challenge "the cant and pretensions of academic priesthoods by publishing an assortment of lively, wide-ranging essays, notes, and reviews that are written in clear, jargon-free prose."<ref>{{Cite web| title = Philosophy and Literature| format = Johns Hopkins University Press| accessdate = 2010-10-30| year = 2000| url = http://www.press.jhu.edu/press/journals/phl/phl.html |archiveurl = https://web.archive.org/web/20000424104040/http://www.press.jhu.edu/press/journals/phl/phl.html |archivedate = 2000-04-24}}</ref> 

The journal is published twice a year in April and October by the [[Johns Hopkins University Press]]. Circulation is 823 and the average length of an issue is 224 pages. The current editors are [[Denis Dutton]] of the [[University of Canterbury]] and [[Garry Hagberg]] of [[Bard College]].

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.press.jhu.edu/journals/philosophy_and_literature/}}
*[http://muse.jhu.edu/journals/philosophy_and_literature/ ''Philosophy and Literature''] at [[Project MUSE]]
*[http://www.denisdutton.com/ Denis Dutton’s homepage]

[[Category:American literary magazines]]
[[Category:Philosophy journals]]
[[Category:Aesthetics]]
[[Category:Publications established in 1977]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:Biannual journals]]
[[Category:English-language journals]]