{{Other uses|Juno (disambiguation){{!}}Juno}}
{{good article}}
{{Infobox planet
| discovery=yes
| physical_characteristics = yes
| bgcolour=#FFFFC0
| name= 3 Juno
| symbol= [[Image:Juno symbol.svg|25px]]
| image=Juno 4 wavelengths.jpg
| caption=Juno seen at four wavelengths with a large [[Impact crater|crater]] in the dark ([[Hooker telescope]], 2003)
| discoverer=[[Karl Ludwig Harding]]
| discovered=September 1, 1804
| mp_name=(3) Juno
| alt_names=''none''
| pronounced ={{IPAc-en|ˈ|dʒ|uː|n|oʊ}}
| adjectives=Junonian {{IPAc-en|dʒ|uː|ˈ|n|oʊ|n|i|ə|n}}<ref>{{OED|Junonian}}</ref>
| named_after = [[Juno (mythology)|Juno]] ({{lang-la|Iūno}})
| mp_category=[[Main belt]] ([[Juno clump]])
| orbit_ref =<ref name="jpldata">{{cite web
 |type=2013-06-01 last obs
 |title=JPL Small-Body Database Browser: 3 Juno
 |url=http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=3
 |accessdate=2014-11-17}}</ref>
| epoch=[[Julian day|JD]] 2457000.5 (9 December 2014)
| semimajor=2.67070 AU <br>(399.725 Gm)
| perihelion=1.98847 AU <br>(297.40 Gm)
| aphelion=3.35293 [[Astronomical unit|AU]] <br>(502.050 Gm)
| eccentricity=0.25545
| period=4.36463 [[Julian year (astronomy)|yr]]<br>(1594.18 d)
| inclination=12.9817°
| asc_node=169.8712°
| arg_peri=248.4100°
| mean_anomaly=33.077°
| avg_speed=17.93&nbsp;km/s
| p_orbit_ref = <ref name="Juno-POE">{{cite web
  |title=AstDyS-2 Juno Synthetic Proper Orbital Elements
  |publisher=Department of Mathematics, University of Pisa, Italy
  |url=http://hamilton.dm.unipi.it/astdys/index.php?pc=1.1.6&n=3
  |accessdate=2011-10-01}}</ref>
| p_semimajor = 2.6693661
| p_eccentricity = 0.2335060
| p_inclination = 13.2515192°
| p_mean_motion = 82.528181
| perihelion_rate = 43.635655
| node_rate = −61.222138
| dimensions=(320×267×200)±6&nbsp;km<ref name="Baer"/><br>(233&nbsp;km)<ref name="jpldata"/>
| surface_area= 216 000 km<sup>2</sup><ref name="fact2">Calculated based on the known parameters</ref>
| volume= 8 950 000 km<sup>3</sup><ref name="fact2"/>
| mass=2.67 {{e|19}} kg<ref name="Baer">
{{cite web
  |date=2008
  |title=Recent Asteroid Mass Determinations
  |publisher=Personal Website
  |author=Jim Baer
  |url=http://home.earthlink.net/~jimbaer1/astmass.txt
  |accessdate=2008-12-03
}}</ref>
| density=3.20 ± 0.56 g/cm³<ref name="Baer"/>
| surface_grav=0.12&nbsp;m/s<sup>2</sup>
| escape_velocity=0.18&nbsp;km/s
| rotation=7.21 hr<ref name="jpldata"/> (0.3004 d)<ref name="lc">
 {{cite web
 |editor-last=Harris 
 |editor-first=A. W. 
 |editor2=Warner, B. D. 
 |editor3=Pravec, P. 
 |title=Asteroid Lightcurve Derived Data. EAR-A-5-DDR-DERIVED-LIGHTCURVE-V8.0. 
 |publisher=[[Planetary Data System|NASA Planetary Data System]] 
 |date=2006 
 |url=http://www.psi.edu/pds/resource/lc.html 
 |accessdate=2007-03-15 
 |archiveurl=http://www.webcitation.org/5mqof7MU9?url=http://www.psi.edu/pds/resource/lc%2Ehtml 
 |archivedate= January 17, 2010 
 |deadurl=no 
 |df= 
}}</ref>
| spectral_type=[[S-type asteroid]]<ref name="jpldata"/><ref name="tax">{{cite web | editor-last= Neese | editor-first= C. | title= Asteroid Taxonomy.EAR-A-5-DDR-TAXONOMY-V5.0. |  publisher= [[Planetary Data System|NASA Planetary Data System]] | date= 2005 | url= http://www.psi.edu/pds/asteroid/EAR_A_5_DDR_TAXONOMY_V5_0/data/taxonomy05.tab | accessdate= 2013-12-24}}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref> 
| rot_velocity= 31.75 m/s<ref name="fact2"/>
| magnitude = 7.4<ref name="AstDys-Juno">{{cite web
 |title=AstDys (3) Juno Ephemerides
 |publisher=Department of Mathematics, University of Pisa, Italy
 |url=http://hamilton.dm.unipi.it/astdys/index.php?pc=1.1.3.1&n=3&oc=500&y0=1983&m0=10&d0=23&h0=00&mi0=00&y1=1983&m1=10&d1=26&h1=00&mi1=00&ti=1.0&tiu=days
 |accessdate=2010-06-26}}
</ref><ref name="bright2005">{{cite web |title=Bright Minor Planets 2005 |publisher=[[Minor Planet Center]] |url=http://www.cfa.harvard.edu/iau/Ephemerides/Bright/2005 |deadurl=yes |archiveurl=https://web.archive.org/web/20100827232404/http://www.cfa.harvard.edu/iau/Ephemerides/Bright/2005/ |archivedate=2010-08-27}}</ref> to 11.55
 |abs_magnitude=5.33<ref name="jpldata"/><ref name="iras">
 {{cite web
 |editor-last=Davis 
 |editor-first=D. R. 
 |editor2=Neese, C. 
 |title=Asteroid Albedos. EAR-A-5-DDR-ALBEDOS-V1.1. 
 |publisher=[[Planetary Data System|NASA Planetary Data System]] 
 |date=2002 
 |url=http://www.psi.edu/pds/resource/albedo.html 
 |accessdate=2007-02-18 
 |archiveurl=http://www.webcitation.org/5mqof7ROl?url=http://www.psi.edu/pds/resource/albedo%2Ehtml 
 |archivedate= January 17, 2010 
 |deadurl=no 
 |df= 
}}</ref>
| albedo=0.238&nbsp;([[geometric albedo|geometric]])<ref name="jpldata"/><ref name="iras" />
| angular_size = 0.30" <!-- Horizons 2018-Oct-28 --> to 0.07"
| single_temperature=~163 [[kelvin|K]]<br>''max:'' 301 K (+28°C)<ref name="lim2005">
 {{cite journal
 | last= Lim | first= Lucy F.
 |author2= McConnochie, Timothy H.|author3= Bell, James F.|author4= Hayward, Thomas L.
 | title= Thermal infrared (8–13 µm) spectra of 29 asteroids: the Cornell Mid-Infrared Asteroid Spectroscopy (MIDAS) Survey
 | journal= Icarus | date= 2005 | volume= 173 | issue= 2| pages= 385–408
 | bibcode= 2005Icar..173..385L
 | doi= 10.1016/j.icarus.2004.08.005}}</ref>
}}

'''Juno''', [[minor-planet designation]] '''3 Juno''' in the [[Minor Planet Center]] catalogue system, is an [[asteroid]] in the [[asteroid belt]]. Juno was the third asteroid discovered, on 1 September 1804 by German astronomer [[Karl Ludwig Harding|Karl L. Harding]]. It is the [[List of exceptional asteroids|11th-largest]] asteroid, and one of the two largest stony ([[S-type asteroid|S-type]]) asteroids, along with [[15 Eunomia]]. It is estimated to contain 1% of the total mass of the asteroid belt.<ref name="Pitjeva05">
 {{cite journal
 |last=Pitjeva 
 |first=E. V. 
 |authorlink=Elena V. Pitjeva 
 |title=High-Precision Ephemerides of Planets—EPM and Determination of Some Astronomical Constants 
 |journal=Solar System Research 
 |date=2005 
 |volume=39 
 |issue=3 
 |page=176 
 |url=http://iau-comm4.jpl.nasa.gov/EPM2004.pdf 
 |format=PDF 
 |doi=10.1007/s11208-005-0033-2 
 |bibcode=2005SoSyR..39..176P 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20081031065523/http://iau-comm4.jpl.nasa.gov/EPM2004.pdf 
 |archivedate=2008-10-31 
 |df= 
}}
</ref>

==History==

===Discovery===
{{expand section|date=October 2015}}
Juno was discovered in 1804 by [[Karl Ludwig Harding]].<ref name="jpldata"/> It was the third [[asteroid]] found, but was initially considered to be a [[planet]]; it was reclassified as an asteroid and [[minor planet]] during the 1850s.<ref>{{cite web | author=Hilton, James L. |title=When did the asteroids become minor planets? |work=U.S. Naval Observatory |url=http://aa.usno.navy.mil/faq/docs/minorplanets.php |accessdate=2008-05-08 |archiveurl = https://web.archive.org/web/20080324182332/http://aa.usno.navy.mil/faq/docs/minorplanets.php |archivedate = 2008-03-24}}</ref>

===Name===
3 Juno is named after the mythological [[Juno (mythology)|Juno]], the highest Roman goddess. The adjectival form is Junonian (''jūnōnius'').

With two exceptions, 'Juno' is the international name, subject to local variation: Italian ''Giunone'', French ''Junon'', Russian ''Yunona'', etc.{{#tag:ref|The exceptions are Greek, where the name was translated to its Hellenic equivalent, [[Hera]] (3 Ήρα), as in the cases of [[1 Ceres]] and [[4 Vesta]]; and Chinese, where it is called the 'marriage-god(dess) star' (婚神星 ''hūnshénxīng''). This contrasts with the goddess Juno, for which Chinese uses the transliterated Latin name (朱諾 ''zhūnuò'').|group=lower-alpha}} Its planetary symbol is ③. An older symbol, still occasionally seen, is ⚵ ([[Image:Juno symbol.svg|30x20px|Old symbol of Juno]]).

==Characteristics==
Juno is one of the larger asteroids, perhaps tenth by size and containing approximately 1% the mass of the entire [[asteroid belt]].<ref name="Pitjeva04b">Pitjeva, E. V.; [http://journals.cambridge.org/production/action/cjoGetFulltext?fulltextid=303499 ''Precise determination of the motion of planets and some astronomical constants from modern observations''], in Kurtz, D. W. (Ed.), ''Proceedings of IAU Colloquium No. 196: Transits of Venus: New Views of the Solar System and Galaxy'', 2004</ref> It is the second-most-massive S-type asteroid after 15 Eunomia.<ref name="Baer"/> Even so, Juno has only 3% the mass of [[Ceres (dwarf planet)|Ceres]].<ref name="Baer"/>
[[Image:Moon and Asteroids 1 to 10.svg|thumb|left|Size comparison: the first 10 asteroids discovered, profiled against Earth's [[Moon]]. Juno is third from the left.]]<!-- [[Image:3AS.jpg|Artist's concept of 3 Juno|thumb|left]] -->

The orbital period of 3 Juno is 4.36578 years.<ref>{{cite web|title=Comets Asteroids|url=http://comets-asteroids.findthedata.org/l/3015/3-Juno|publisher=Find The Data.org|accessdate=14 May 2014}}</ref>

Amongst S-type asteroids, Juno is unusually reflective, which may be indicative of distinct surface properties. This high albedo explains its relatively high [[apparent magnitude]] for a small object not near the inner edge of the asteroid belt. Juno can reach +7.5 at a favourable opposition, which is brighter than [[Neptune#Observation|Neptune]] or [[Exploration of Titan|Titan]], and is the reason for it being discovered before the larger asteroids [[10 Hygiea|Hygiea]], [[52 Europa|Europa]], [[511 Davida|Davida]], and [[704 Interamnia|Interamnia]]. At most oppositions, however, Juno only reaches a magnitude of around +8.7<ref name="brightestasteroids">
 {{cite web
 |title=The Brightest Asteroids 
 |publisher=The Jordanian Astronomical Society 
 |author=Odeh, Moh'd 
 |url=http://jas.org.jo/ast.html 
 |accessdate=2008-05-21 
 |archiveurl=https://web.archive.org/web/20080511115437/http://www.jas.org.jo/ast.html 
 |archivedate=11 May 2008 
 |deadurl=yes 
 |df= 
}}
</ref>—only just visible with [[binoculars]]—and at smaller [[Elongation (astronomy)|elongation]]s a {{convert|3|in|mm|sing=on}} [[telescope]] will be required to resolve it.<ref name="telescope">
 {{cite web|date=2004 |title=What Can I See Through My Scope? |publisher=Ballauer Observatory |url=http://www.allaboutastro.com/Articlepages/whatcanisee.html |accessdate=2008-07-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20110726123615/http://www.allaboutastro.com/Articlepages/whatcanisee.html |archivedate=July 26, 2011 }} (archived)
</ref> It is the main body in the [[Juno family]].

{| class="wikitable" style="margin:1em 0.5em 1em auto; float:left;"
|-  style="background:#ccf; font-size:smaller;"
|+ Planets 1807–1845
|- style="font-size: smaller;"
| 1 || Mercury[[Image:Mercury symbol.svg|14px|☿]] 
|- style="font-size: smaller;"
|2 ||Venus[[Image:Venus symbol.svg|14px|♀]] 
|- style="font-size: smaller;"
|3 || Earth [[Image:Earth symbol.svg|14px|⊕]]
|- style="font-size: smaller;" 
|4 || Mars[[Image:Mars symbol.svg|14px|♂]] 
|- style="font-size: smaller;"
|5 || Vesta  [[Image:Vesta symbol.svg|14px]]
|- style="font-size: smaller;"
|6 || Juno [[Image:Juno symbol.svg|14px]]
|- style="font-size: smaller;"
|7 || Ceres [[Image:Ceres symbol.svg|14px]] 
|- style="font-size: smaller;"
|8 || Pallas [[Image:Pallas symbol.svg|14px]] 
|- style="font-size: smaller;"
|9 || Jupiter[[Image:Jupiter symbol.svg|14px|♃]]
|- style="font-size: smaller;"
|10 ||Saturn [[Image:Saturn symbol.svg|14px|♄]]
|- style="font-size: smaller;"
|11|| Uranus[[Image:Uranus symbol.svg|14px|♅]]
|}
Juno was originally considered a planet, along with [[Ceres (dwarf planet)|1 Ceres]], [[2 Pallas]], and [[4 Vesta]].<ref name="Hilton"/> In 1811, [[Johann Hieronymus Schröter|Schröter]] estimated Juno to be as large as 2290&nbsp;km in diameter.<ref name="Hilton">
 {{cite web
  |date=2007-11-16
  |author=Hilton, James L |authorlink=James L. Hilton
  |title=When did asteroids become minor planets?
  |work=U.S. Naval Observatory
  |url=http://aa.usno.navy.mil/faq/docs/minorplanets.php
  |accessdate=2008-06-22 |archiveurl = https://web.archive.org/web/20080324182332/http://aa.usno.navy.mil/faq/docs/minorplanets.php |archivedate = 2008-03-24}} 
</ref> All four were reclassified as asteroids as additional asteroids were discovered. Juno's small size and irregular shape preclude it from being designated a [[dwarf planet]].

Juno orbits at a slightly closer mean distance to the [[Sun]] than Ceres or Pallas. Its orbit is moderately inclined at around 12° to the [[ecliptic]], but has an extreme [[orbital eccentricity|eccentricity]], greater than that of [[Pluto]]. This high eccentricity brings Juno closer to the Sun at [[perihelion]] than Vesta and further out at [[aphelion]] than Ceres. Juno had the most eccentric orbit of any known body until [[33 Polyhymnia]] was discovered in 1854, and of asteroids over 200&nbsp;km in diameter only [[324 Bamberga]] has a more eccentric orbit.<ref name="ecc">{{cite web|title=MBA Eccentricity Screen Capture |publisher=JPL Small-Body Database Search Engine |url=http://home.comcast.net/~kpheider/3Juno-ecc.jpg |accessdate=2008-11-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20090327111705/http://home.comcast.net/~kpheider/3Juno-ecc.jpg |archivedate=March 27, 2009 }}</ref>

Juno rotates in a [[direct motion|prograde]] direction with an [[axial tilt]] of approximately 50°.<ref name="kaasalainen2002">The north pole points towards [[ecliptic coordinate system|ecliptic coordinates]] (β, λ) = (27°, 103°) within a 10° uncertainty. 
 {{cite journal
 | last= Kaasalainen | first= M.
 |author2= Torppa, J.|author3= Piironen, J.
 | title= Models of Twenty Asteroids from Photometric Data
 | journal= Icarus | volume= 159 | issue= 2 | pages= 369–395 | date= 2002
 | url= http://www.rni.helsinki.fi/~mjk/IcarPIII.pdf | format= PDF
 | doi= 10.1006/icar.2002.6907 | bibcode=2002Icar..159..369K}}
</ref> The maximum temperature on the surface, directly facing the Sun, was measured at about 293 [[Kelvin|K]] on October 2, 2001. Taking into account the [[heliocentric]] distance at the time, this gives an estimated maximum temperature of 301 K (+28&nbsp;°C) at perihelion.<ref name="lim2005"/>

Spectroscopic studies of the Junonian surface permit the conclusion that Juno could be the progenitor of [[chondrite]]s, a common type of stony [[meteorite]] composed of iron-bearing [[silicate]]s such as [[olivine]] and [[pyroxene]].<ref name="gaffey1993">
{{cite journal
 | last= Gaffey
 | first= Michael J.
 |author2=Burbine, Thomas H.|author3=Piatek, Jennifer L.|author4=Reed, Kevin L.|author5=Chaky, Damon A.|author6=Bell, Jeffrey F.|author7= Brown, R. H.
 | title= Mineralogical variations within the S-type asteroid class
 | journal= Icarus | date= 1993 | volume= 106 | issue= 2 | page= 573
 | bibcode= 1993Icar..106..573G | doi= 10.1006/icar.1993.1194}}
</ref> [[Infrared]] images reveal that Juno possesses an approximately 100&nbsp;km-wide crater or ejecta feature, the result of a geologically young impact.<ref name="harvard-pr0318">
 {{cite web
 | title= Asteroid Juno Has A Bite Out Of It
 | publisher= Harvard-Smithsonian Center for Astrophysics
 | date= 2003-08-06
 | url= http://cfa-www.harvard.edu/press/pr0318.html
 | accessdate= 2007-02-18| archiveurl= https://web.archive.org/web/20070208013152/http://cfa-www.harvard.edu/press/pr0318.html| archivedate= 8 February 2007 <!--DASHBot-->| deadurl= no}}
</ref><ref name="baliunas2003">
 {{cite journal
 | last= Baliunas | first= Sallie
 |author2=Donahue, Robert|author3= Rampino, Michael R.|author4= Gaffey, Michael J.|author5= Shelton, J. Christopher|author6= Mohanty, Subhanjoy
 | title= Multispectral analysis of asteroid 3 Juno taken with the 100-inch telescope at Mount Wilson Observatory
 | journal= Icarus | date= 2003 | volume= 163 | issue= 1 | pages= 135–141
 | url= http://pubs.giss.nasa.gov/docs/2003/2003_Baliunas_etal_1.pdf
 | format= PDF | doi= 10.1016/S0019-1035(03)00049-6 | bibcode=2003Icar..163..135B}}</ref>

==Observations==
Juno was the first asteroid for which an [[occultation]] was observed. It passed in front of a dim [[star]] ([[SAO 112328]]) on February 19, 1958. Since then, several occultations by Juno have been observed, the most fruitful being the occultation of [[SAO 115946]] on December 11, 1979, which was registered by 18 observers.<ref name="millis1981">
 {{cite journal
 | display-authors= 8
 | last= Millis | first= R. L.
 | author2= Wasserman, L. H.
 | author3= Bowell, E.
 | author4= Franz, O. G.
 | author5= White, N. M.
 | author6= Lockwood, G. W.
 | author7= Nye, R.
 | author8= Bertram, R.
 | author9= Klemola, A.
 | author10= Dunham, E.
 | author11= Morrison, D.
 | title= The diameter of Juno from its occultation of AG+0°1022
 | journal= Astronomical Journal | volume= 86 | pages= 306–313 |date=February 1981
 | bibcode= 1981AJ.....86..306M | doi= 10.1086/112889}}</ref>
Juno occulted the magnitude 11.3 star PPMX 9823370 on July 29, 2013,<ref>[http://www.asteroidoccultation.com/2013_07/0729_3_30531.htm Asteroid Occultation Updates – Jul 29, 2013]{{dead link|date=September 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> and [[2UCAC 30446947]] on July 30, 2013.<ref>[http://www.asteroidoccultation.com/2013_07/0730_3_29995.htm Asteroid Occultation Updates – Jul 30, 2013]{{dead link|date=September 2016 |bot=InternetArchiveBot |fix-attempted=yes }}.</ref>

Radio signals from spacecraft in orbit around [[Mars]] and on its surface have been used to estimate the mass of Juno from the tiny perturbations induced by it onto the motion of Mars.<ref name="Pitjeva04">
 {{cite conference
 | first=E. V. | last= Pitjeva
 | title= Estimations of masses of the largest asteroids and the main asteroid belt from ranging to planets, Mars orbiters and landers
 | booktitle= 35th COSPAR Scientific Assembly. Held 18–25 July 2004, in Paris, France
 | pages= 2014 | date= 2004
 | url= http://adsabs.harvard.edu/abs/2004cosp.meet.2014P
 | bibcode= 2004cosp.meet.2014P}}</ref> Juno's [[orbit]] appears to have changed slightly around 1839, very likely due to perturbations from a passing asteroid, whose identity has not been determined.<ref name="usno1999">{{cite journal |last=Hilton |first=James L. |title=US Naval Observatory Ephemerides of the Largest Asteroids |journal= Astronomical Journal |volume=117 |issue=2 |pages=1077–1086 |date=February 1999 |url=http://aa.usno.navy.mil/publications/reports/asteroid_ephemerides.html |doi=10.1086/300728 |bibcode=1999AJ....117.1077H |accessdate=2012-04-15}}</ref>

In 1996, Juno was imaged by the [[Hooker Telescope]] at [[Mount Wilson Observatory]] at visible and near-IR wavelengths, using [[adaptive optics]]. The images spanned a whole rotation period and revealed an irregular shape and a dark albedo feature, interpreted as a fresh impact site.<ref name="baliunas2003"/>

{| class="wikitable" style="width:480px;"
|-
|valign=top|<center>'''Animation'''<center>[[File:Juno mpl anim.gif|235px]]<br>Juno moving across background stars
|valign=top|<center>'''Star field'''<center>[[File:3Juno-LB1-apmag.jpg|250px]]<br>Juno during opposition in 2009
|valign=top|<center>'''ALMA'''<center>[[File:Animation of the asteroid Juno as imaged by ALMA.webm|250px]]<br>Video of Juno taken as part of ALMA's Long Baseline Campaign
|}

==See also==
* [[Asteroids in fiction#Juno|Juno in fiction]]
* [[Planet#Objects formerly considered planets|Former classification of planets]]

==Notes==
{{notelist}}

==References==
{{reflist|colwidth=30em}}

==External links==
{{Commons|3 Juno|3 Juno}}
* [http://ssd.jpl.nasa.gov/horizons.cgi?find_body=1&body_group=sb&sstr=3 JPL Ephemeris]
* [https://web.archive.org/web/20060627152558/http://www.cfa.harvard.edu:80/press/pr0318image.html Well resolved images from four angles] taken at [[Mount Wilson observatory]]
* [http://www.rni.helsinki.fi/~mjk/IcarPIII.pdf Shape model deduced from light curve]
* [http://www.jpl.nasa.gov/news/features.cfm?feature=2314 Asteroid Juno Grabs the Spotlight]
* {{cite web |title=Elements and Ephemeris for (3) Juno |publisher=Minor Planet Center |url=http://scully.cfa.harvard.edu/cgi-bin/returnprepeph.cgi?d=b2011&o=00003 }} (displays [[Elongation (astronomy)|Elong]] from Sun and [[apparent magnitude|V mag]] for 2011)
* {{JPL small body}}

{{Minor planets navigator|2 Pallas|number=3|4 Vesta}}
{{Small Solar System bodies}}

{{Authority control}}

{{DEFAULTSORT:000003}}
[[Category:Junonian asteroids]]
[[Category:Numbered minor planets]]
[[Category:Discoveries by Karl Harding|Juno]]
[[Category:Minor planets named from Roman mythology|Juno]]
[[Category:Named minor planets|Juno]]
[[Category:Articles containing video clips]]
[[Category:S-type asteroids (Tholen)]]
[[Category:Sk-type asteroids (SMASS)]]
[[Category:Astronomical objects discovered in 1804|18040901]]