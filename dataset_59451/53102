{{Infobox journal
| title = Violence and Victims
| cover = 
| abbreviation = Violence Vict.
| editor = Roland D. Maiuro
| discipline = [[violence|interpersonal violence]], [[victimisation|victimization]]
| publisher = [[Springer Publishing]]
| country = [[United States]]
| frequency = Bimonthly
| history = 1986–present
| openaccess =
| license =
| impact = 0.858
| impact-year = 2014
| website = http://www.springerpub.com/vv
| link1 = http://www.ingentaconnect.com/content/springer/vav
| link1-name = Online access
| JSTOR =
| OCLC = 7938261
| LCCN = 86658044
| CODEN = 
| ISSN = 0886-6708 
| eISSN = 1945-7073
}}
'''''Violence and Victims''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] covering theory, research, policy, and clinical practice in the area of interpersonal [[violence]] and [[victimisation|victimization]], touching diverse disciplines such as [[psychology]], [[sociology]], [[criminology]], [[law]], [[medicine]], [[nursing]], [[psychiatry]], and [[social work]].

The journal's scope includes [[original research]] on violence-related victimization within, and outside of, the family; the etiology and perpetration of violent behavior; health care research related to [[interpersonal violence]] and to trauma; legal issues; and implications for clinical interventions. Occasionally, there are special issues dealing with specific topics and relevant books are often reviewed. ''Violence and Victims'' is published by [[Springer Publishing Company]].

== Abstracting and indexing ==
''Violence and Victims'' is indexed or abstracted in [[PsycINFO]], [[PsycLIT]], [[EBSCO Industries|Family and Society Studies Worldwide]], [[EMBASE]], [[CSA (database company)|Sociological Abstracts]], [[CSA (database company)|Social Services and Social Work Abstracts]], [[EBSCO Industries|Violence and Abuse Abstracts]], [[National Criminal Justice Reference Service]], [[Index Medicus]], [[MEDLINE]], [[PILOTS Database]], [[PubMed]], [[Psychological Abstracts]], [[EBSCO Industries|Criminal Justice Abstracts]], and [[EBSCO Industries|Educational Administration Abstracts]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.858.<ref name=WoS>{{cite book |year=2015 |chapter=Violence and Victims |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.springerpub.com/product/08866708}}

{{DEFAULTSORT:Violence and Victims}}
[[Category:Sociology journals]]
[[Category:Publications established in 1986]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Springer Publishing academic journals]]
[[Category:Violence journals]]