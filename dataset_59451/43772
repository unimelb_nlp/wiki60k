<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = XP-13
  |image = File:Thomas Morse XP-13 Viper USAF.jpg
  |caption = Thomas-Morse XP-13 
}}{{Infobox Aircraft Type
  |type = Fighter
  |manufacturer = [[Thomas-Morse]]
  |designer = B. Douglas Thomas<ref name="fight">''The Complete Book of Fighters'' Editors: William Green & Gordon Swanborough (Barnes & Noble Books New York, 1998, ISBN 0-7607-0904-1)</ref>
  |first flight = 
  |introduced = June 1929
  |retired = 
  |status =
  |primary user = [[United States Army Air Service]]
  |more users = 
  |produced = 
  |number built = 1<ref name="fahey">"U.S. Army Aircraft 1908-1946" by [[James C. Fahey]], 1946, 64pp.</ref>
  |unit cost = 
  |variants with their own articles = 
}}
|}

The '''XP-13 Viper''' was a prototype [[biplane]] [[fighter aircraft]] designed by the [[United States|American]] company [[Thomas-Morse Aircraft Corporation]]. The airplane was delivered to the [[United States Army]] in 1929, but they did not adopt it.

==Design and development==

This aircraft was one of several [[B. Douglas Thomas]] designs built in hopes of a production contract from the Army, following the successful [[Thomas-Morse MB-3]] of 1919. Financed by the company, and named the "Viper", it was officially purchased by the Army in June 1929 and designated "XP-13".

The XP-13 [[fuselage]] had a corrugated [[aluminum]] skin built over a metal frame; the flying surfaces were also metal-framed, but covered with the traditional fabric. While designed to use the 600&nbsp;hp [[Curtiss H-1640]]-1 Chieftain engine, (a novel 12-cylinder two-row air-cooled radial with the rear cylinders directly behind the front cylinders rather than staggered as normal in a two-row radial<ref name="Ginston p46">Gunston 1986, p.46.</ref>) for which the XP-13 incorporated a complex system of baffles to direct cooling air over the engine, the engine simply would not stay cool enough, and in September 1930 it was replaced with a [[Pratt & Whitney R-1340|Pratt & Whitney SR1340C Wasp]] of 450&nbsp;hp. Ironically, the lower-power engine actually resulted in a speed increase of 15&nbsp;mph, at least partly because of the weight savings.<ref>"U.S. Fighters", by Lloyd S. Jones, (Aero Publishers, Inc. ISBN 0-8168-9200-8, 1975) pp. 46-47</ref>

In the end, the Army decided against production, Thomas-Morse was acquired by [[Consolidated Aircraft]], and the prototype was lost to an inflight fire.

==Variants==
;XP-13
:Prototype, serial number 29-453 with 600&nbsp;hp (448 kW) [[Curtiss H-1640|Curtiss H-1640-1 Chieftain]] hex engine<ref name="fahey"/>

;XP-13A
:The XP-13 modified with a 525&nbsp;hp (391 kW) [[Pratt & Whitney R-1340|Pratt & Whitney SR-1340-C]] enclosed in a [[NACA cowling]], along with a revised [[fin]] and [[rudder]]<ref>"The American Fighter", Enzo Angellucci and Peter Bowers, (Orion Books ISBN 0-517-56588-9), 1987</ref>

;XP-14
:This designation was used for a proposed [[Curtiss Aeroplane and Motor Company|Curtiss]] version of the Viper with the Curtiss H-1640-1 Chieftain hex engine

==Operators==

;{{flag|United States|1912}}
[[United States Army Air Service]]
[[File:Thomas-Morse XP-13 Viper.jpg|thumb|Side view of P&W-powered XP-13A variant showing corrugated aluminum skin.]]

==Specifications (XP-13 (Chieftain engine))==
{{aircraft specifications
|plane or copter?= plane
|jet or prop?= prop
|ref="The Complete Book of Fighters" [http://ec1.images-amazon.com/images/I/21RSY79BDHL.jpg cover] Editors: William Green & Gordon Swanborough (Barnes & Noble Books New York, 1998, ISBN 0-7607-0904-1), 608 pp.
|crew= 1
|capacity=
|length main= 23 ft 6 in
|length alt= 7.16 m
|span main= 28 ft 0 in
|span alt= 8.53 m
|height main= 8 ft 5 in
|height alt= 2.56 m
|area main= 189 ft²
|area alt= 17.6 m²
|airfoil=
|empty weight main= 2,262 lb
|empty weight alt= 1,026&nbsp;kg
|loaded weight main= 3,256 lb
|loaded weight alt= 1,477&nbsp;kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)= [[Curtiss H-1640|Curtiss H-1640-1 Chieftain]]
|type of prop= 12-cyliner two-row air-cooled [[radial engine]]
|number of props= 1
|power main= 600&nbsp;hp
|power alt= 448 kW
|power original=
|max speed main= 150 knots
|max speed alt= 172 mph,  277 km/h
|max speed more= (at sea level)
|cruise speed main= 113 knots 
|cruise speed alt= 130 mph, 209 km/h <ref name="DorrUS">Dorr and Donald 1990, p.43</ref> 
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 168 NM
|range alt= 193 mi, 312 km <ref name="DorrUS"/> 
|ceiling main=20,775 ft
|ceiling alt=6,300 m
|climb rate main= 1,700 ft/min<ref name="DorrUS"/>
|climb rate alt= 8.6 m/s
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament= none
|avionics=
}}

==References==
;Notes
{{Reflist}}
;Bibliography
* Dorr, Robert F. and David Donald. ''Fighters of the United States Air Force''. London:Temple, 1990. ISBN 0-600-55094-X.
* [[Bill Gunston|Gunston, Bill]], ''World Encyclopedia of Aero Engines''. London: Guild Publishing, 1986.

==External links==
{{commons category|Thomas-Morse XP-13 Viper}}
* [http://www.joebaugher.com/usaf_fighters/p13.html Berliner-Joyce XP-13 Viper]
* [http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=2229 National Museum of the USAF page, with photo]

{{aircontent
|related=
|similar aircraft=
|lists=
* [[List of military aircraft of the United States]]
* [[List of fighter aircraft]]
}}

{{Thomas-Morse aircraft}}
{{USAF fighters}}

{{DEFAULTSORT:Thomas-Morse Xp-13 Viper}}
[[Category:United States fighter aircraft 1920–1929|Thomas-Morse P-13 Viper]]
[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:Thomas-Morse aircraft|XP-13]]