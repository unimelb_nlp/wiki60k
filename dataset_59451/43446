{{Infobox company
| name             = Progressive Aerodyne, Inc
| logo             = File:Progressive Aerodyne Logo 2014.png
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| genre            = <!-- Only used with media and publishing companies -->
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = 1991
| founder          = Kerry Richter<br>Wayne Richter<br>Paige Lynette
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[Tavares, Florida]]
| location_country = [[United States]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = CEO: Adam Yang
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]]
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|www.searey.com/}}
| footnotes        = 
| intl             = 
}}
[[File:Progressive Aerodyne SeaRey N244H Landing 09 SNFSI FOF 15April2010 (14650248753).jpg|thumb|right|[[Progressive Aerodyne SeaRey]]]]
'''Progressive Aerodyne, Inc''' is an [[United States|American]] aircraft manufacturer based in [[Tavares, Florida]]. The company specializes in the design and manufacture of amphibious [[kit aircraft]] for the amateur-construction market.<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 66 Belvoir Publications. ISSN 0891-1851</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 70. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', pages 225-226. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 74. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==History==
The company was founded in 1991 in [[Orlando, Florida]] by Kerry Richter, Wayne Richter and Paige Lynette. The company moved to Tavares, Florida in May 2010 and since 2011 the CEO has been Adam Yang.<ref name="Aerocrafter"/><ref name="Hist">{{cite web|url = http://www.searey.com/about-us/history|title = History|accessdate = 29 August 2014|last = Progressive Aerodyne|year =  2013}}</ref>

The company's main product, the [[Progressive Aerodyne SeaRey|SeaRey]], was first flown in November 1991.<ref name="Aerocrafter" />

== Aircraft ==

{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Aircraft built by Progressive Aerodyne'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type

|-
|align=left| '''[[Progressive Aerodyne SeaRey|SeaRey]]'''
|align=center| 1991
|align=center| 480 (2011)
|align=left| two-seat amphibious kit aircraft
|-
|align=left| '''[[Progressive Aerodyne Stingray|Stingray]]'''
|align=center| 1990s
|align=center| at least nine
|align=left| single-seat amphibious kit aircraft
|-

|}

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.searey.com/}}
{{Progressive Aerodyne aircraft}}

[[Category:Aircraft manufacturers of the United States]]
[[Category:Homebuilt aircraft]]