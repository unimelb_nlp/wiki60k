{{For|the philosophy journal|Dialogue: Canadian Philosophical Review}}
{{Multiple issues|
{{refimprove|date=March 2011}}
{{cleanup|date=March 2011}}
}}

{{Infobox journal
| cover	        = <center>[[Image:DialogueJournal.jpg|150px]]</center>
| editor        = [[Boyd Jay Petersen]]
| discipline	= [[Mormon studies]]
| publisher	= Dialogue Foundation
| country	= [[United States]]
| frequency	= Quarterly
| history	= 1966-present
| website	= http://www.dialoguejournal.com/
| OCLC          = 197923057
| ISSN          = 0012-2157
| eISSN         = 1554-9399
}}

'''''Dialogue: A Journal of Mormon Thought''''' is an independent quarterly journal of "Mormon thought" that addresses a wide range of issues on [[Mormonism]] and the [[Latter Day Saint Movement]].

The journal publishes peer-reviewed<ref name="sltrib">{{cite news|url=http://www.sltrib.com/opinion/ci_4308366|title=Four decades of independent Mormon thought|last=Bergera|first=Gary James|date=2006-09-08|publisher=[[The Salt Lake Tribune]]|accessdate=2006-09-12}}<!-- Note - SL trib moves stuff to subscription only archives after about 60 days--></ref><ref>{{cite news |first=Carrie A. |last=Moore | title=Dialogue journal celebrating 40 years | work=Deseret Morning News |url=http://deseretnews.com/dn/view/0,1249,650192701,00.html |date=2006-09-22 |accessdate=2006-11-07}}</ref><ref>{{cite journal |last=Ostler |first=Blake |authorlink=Blake Ostler |journal=[[Sunstone Magazine|Sunstone]] |title=Blake Ostler Responds |url=http://www.sunstoneonline.com/Download/bom/136_Letters.pdf |date=March 2005 |accessdate=2006-11-07 |format=PDF |archiveurl = https://web.archive.org/web/20060904100533/http://www.sunstoneonline.com/Download/bom/136_Letters.pdf <!-- Bot retrieved archive --> |archivedate = 2006-09-04}}</ref><ref>{{cite web |title=Why you should subscribe to BYU Studies |last=Adams |first=Stirling | work=By Common Consent |url=http://www.bycommonconsent.com/2006/06/why-you-should-subscribe-to-byu-studies/ |date=2006-06-12 |accessdate=2006-11-07}}</ref> academic articles that run the gamut from anthropology and sociology to theology, history, and science. The journal also publishes fiction, poetry, and graphic arts. ''Dialogue'' authors regularly include both members of the Mormon community and non-Mormon scholars interested in Mormon Studies. [[Douglas Davies]] and [[Jan Shipps]] are some of the non-Mormon academics that publish in ''Dialogue''. Examples of Mormon authors are [[Eugene England]], [[Richard Bushman]], [[Claudia Bushman]], [[Gregory Prince]], and [[Mary Lythgoe Bradford]].

== History ==
''Dialogue: A Journal of Mormon Thought'', is the oldest independent journal in Mormon Studies. ''Dialogue'' was originally the creation of a group of young Mormon scholars at [[Stanford University]] led by [[Eugene England]], and G. Wesley Johnson. ''Dialogue's'' original offices were located at Stanford.  Brent Rushforth aided in ''Dialogue's'' initiation.

The first issue appeared in the spring of 1966, and during its first few years the Editorial Board and Staff came to include many notables in the subsequent history of [[The Church of Jesus Christ of Latter-day Saints]], such as [[Richard Bushman]], [[Chase Peterson]],  [[Stanford Cazier]], [[Dallin H. Oaks]], Cherry Silver, Karen Rosenbaum, and [[Laurel Thatcher Ulrich]]. ''Dialogue'' is known for publishing groundbreaking articles from respected Mormon scholars and writers such as [[Armand Mauss]], [[Hugh Nibley]], Lester Bush, and [[D. Michael Quinn]]. Two key sponsors and advisors from the beginning were [[Lowell L. Bennion]], of the [[Institute of Religion|LDS Institute]] at the [[University of Utah]], and [[Leonard J. Arrington]], later the [[Church Historian and Recorder|official historian]] of the LDS Church. ''Dialogue'' has nevertheless remained totally independent of church auspices over the years thanks to loyal readers and the generosity of its donors.<ref name=sltrib />

The founding and subsequent editorial boards have been composed mainly of scholars and lay writers who are participating members of the LDS Church. ''Dialogue'' has been the main venue over the years for the publication of articles on some of the most difficult and controversial issues in LDS history and doctrine, including the problems of race ethnicity (see [[Blacks and Mormonism]]), women's roles, religion and politics, the history of polygamy (see [[Joseph Smith, Jr. and Polygamy]]), Mormons and Masonry, the [[Book of Mormon]], the career of [[Joseph Smith]], and many other potentially difficult issues. ''Dialogue'' has constantly strived for honesty and balance in its treatment of such topics and has maintained an [[editorial independence]] that has established it as the premier scholarly journal in Mormon Studies, now emerging as a major academic field at several universities.

The [[Mormon History Association]] ("MHA") was founded in 1966, and for the first 6–7 years of its existence, MHA members published their Mormon-related studies principally in ''Dialogue''. MHA then founded the "''[[Journal of Mormon History]].'' Since then, ''Dialogue'' and ''The Journal of Mormon History,'' along with ''[[BYU Studies Quarterly]]'' have been some of the main venues for historical studies of Mormonism. Both ''[[BYU Studies Quarterly]]'' and ''[[Sunstone Magazine]]'' are periodicals that, like ''Dialogue'', contain articles that range widely across the field of Mormon Studies.<ref>[[Devery S. Anderson|Anderson, Devery S.]] “A History of Dialogue, Part One: The Early Years, 1965-1971,”  ''Dialogue: A Journal of Mormon Thought'', [http://content.lib.utah.edu/cgi-bin/docviewer.exe?CISOROOT=/dialogue&CISOPTR=7844&CISOSHOW=7690 32:2 (Summer 1999): 15-67.]</ref><ref>[[Devery S. Anderson|Anderson, Devery S.]] “A History of Dialogue, Part Two: Struggle toward Maturity, 1971-1982,” ''Dialogue: A Journal of Mormon Thought'', [http://content.lib.utah.edu/cgi-bin/docviewer.exe?CISOROOT=/dialogue&CISOPTR=11018&CISOSHOW=10808 33:2 (Sum 2000): 1-96.]</ref><ref>[[Devery S. Anderson|Anderson, Devery S.]] “A History of Dialogue, Part Three: "Coming of Age" in Utah, 1982-1987," ''Dialogue: A Journal of Mormon Thought'', [http://content.lib.utah.edu/cgi-bin/docviewer.exe?CISOROOT=/dialogue&CISOPTR=29817&CISOSHOW=29619  35: 2 (Sum 2002): 1-71.]</ref><ref>Hansen, *Ralph W. "Letter to Editor," ''Dialogue: A Journal of Mormon Thought,'' [http://content.lib.utah.edu/cgi-bin/docviewer.exe?CISOROOT=/dialogue&CISOPTR=8057 32:4: (Winter 1999)iv-vi.]</ref>

== List of editors ==
{| class="wikitable"
|-
! Name !! Term !! Notes
|-
| [[Eugene England]] and G. Wesley Johnson || 1966–1970 ||
|-
| [[Robert A. Rees]] || 1970–1976 ||
|-
| [[Mary Lythgoe Bradford]] || 1977–1982 ||
|-
| [[Linda King Newell]] and L. Jackson Newell || 1982–1986 || Husband and wife team
|-
| F. Ross Peterson and Mary Kay Peterson || 1987–1992 ||
|-
| Martha Sonntag Bradley and Allen D. Roberts || 1993–1998 ||
|-
| Neal Chandler and Rebecca Worthen Chandler || 1999–2003 ||
|-
| Karen Marguerite Moloney || 2004 || Only for [[Spring (season)|spring]] issue
|-
| [[Levi S. Peterson]] || 2004–2008 || English professor at [[Weber State]], biographer, novelist
|-
| [[Kristine Haglund]] || 2009–2015<ref>{{cite news|url=http://www.sltrib.com/faith/ci_8656334|title=Essayist will take reins as new editor of 'Dialogue'|last=Stack|first=Peggy Fletcher|date=2008-03-21|publisher=[[The Salt Lake Tribune]]|authorlink=Peggy Fletcher Stack|accessdate=2008-04-01}}<!-- Note - SL Tribune moves articles to subscription only archives after about 60 days--></ref> ||
|-
| [[Boyd Jay Petersen]] || 2016–2020<ref>"[https://www.dialoguejournal.com/2015/new-editor-found-dr-boyd-jay-petersen-named-next-editor-of-dialogue-a-journal-of-mormon-thought/ NEW EDITOR FOUND: Dr. Boyd Jay Petersen Named Next Editor of Dialogue: A Journal of Mormon Thought]," 29 Jan 2015. Accessed 10 Mar 2015.</ref><ref>{{cite web | author=Kellene Ricks Adams | title=UVU Professor to Serve Five-Year Term as Editor of Prestigious Journal | work=Press Releases | date=February 3, 2015 | publisher=Utah Valley University Marketing & COmmunications | url=http://blogs.uvu.edu/newsroom/2015/02/03/uvu-professor-to-serve-five-year-term-as-editor-of-prestigious-journal/ | accessdate=2015-06-04}}</ref> || [[Mormon Studies]] program coordinator at [[UVU]]
|}

== Web version==
In 2005 ''Dialogue'' dipped its toe into the [[Bloggernacle]] with several of its editorial and board members participating as long-term guest contributors to the Mormon blog [[By Common Consent]].<ref>{{cite web | title=Dialogue Posts | work=[[By Common Consent]] | url=http://www.bycommonconsent.com/category/dialogue-posts/ | accessdate=2014-08-30}}</ref> Then in early 2006 ''Dialogue'' introduced "''Dialogue'' Paperless" at Dialoguejournal.com, with sections for ePapers, News, Letters, and Book Reviews.  The "e-Papers" section intended to "supplement the printed journal by housing digital documents that qualify as papers...complete pieces, duly refereed and edited and hitherto unpublished."  Early e-Papers included D. Michael Quinn's "Joseph Smith’s Experience of a Methodist “Camp-Meeting” in 1820," and articles by other authors comprising a "Critique and Defense of Chiasmus in the Book of Mormon."

== Subscriber survey ==
In 2005, ''Dialogue'' received 1,332 responses to a subscriber survey. ''Dialogue'' reports the following responses:{{Citation needed|date=April 2011}}<!-- Cannot find 2005 survey results posted on the ''Dialogue'' web site; can only find 1987 -->

*The geographic location of respondents was: [[Utah]] (33%), [[California]] (17%), other Western states—Rockies to the Coast (19%), the Northeast (10-12%), elsewhere (20%). Less than 1% lived outside the U.S., though there is some indication that the availability of the on-line subscriptions may be changing the level of international readership.
*The terminal education degree of respondents was: doctoral degree (40%), Master’s (31%), Bachelor’s degree (21%), no degree (7%).
*66% of respondents attend worship service every week, another 12% attend “most weeks” (total 78%). 59% of today’s readers are returned missionaries.
* Respondents subscribe to or regularly read the following other Mormon-related publications: ''[[BYU Studies]]'' (29%), ''[[Ensign (LDS magazine)|Ensign]]'' (66%), ''[[Foundation for Ancient Research and Mormon Studies#Publications|FARMS Review of Books]]'' (13%), ''[[John Whitmer Historical Association#Periodicals|John Whitmer Historical Association Journal]]'' (9%), ''Irreantum'', the [[Association for Mormon Letters|AML]] journal (10%), ''[[Foundation for Ancient Research and Mormon Studies#Publications|Journal of Book of Mormon Studies]]'' (17%), ''[[Mormon History Association#Journal of Mormon History|Journal of Mormon History]]'' (25%), ''[[Sunstone (magazine)|Sunstone]]'' (68%), and ''Utah Historical Quarterly'' (13%).
*90% of respondents are members of the LDS Church. Around 6% of respondents described themselves as having left the LDS Church and most of these have affiliated with other denominations.
*81% of respondents either “strongly” (32%) or “somewhat” (49%) agreed that ''Dialogue'' contributes to their personal spiritual or religious enrichment.
*84% of respondents viewed the Book of Mormon as “authentic in any sense.” The way in which the Book of Mormon was viewed as authentic was: Literal Historical Record (almost 40%) Theology & Moral Teaching Authentic, Historicity Doubtful (24%) Moral Teachings Sound, Historicity & Divine Origin Doubtful (12%), 19th Century Literary Product (14%).
*Two-thirds (68%) of respondents find Dialogue's “current content and editorial tone” to be predominantly “objective and “independent,” 9% find it to be “hypercritical and negative,” 8% find the content/tone to be “bland, uncritical,” and 13% reported that it “depends on the subject.”

== Digital archive ==
In collaboration with the [[University of Utah]]'s [http://www.lib.utah.edu/ J. Willard Marriott Library], in 2004 the archive of past ''Dialogue'' issues (1966 to date) was digitally scanned. The library's [http://www.lib.utah.edu/libraryinfo/dept/spc/ Special Collections] now hosts the digital archive in pdf and text-searchable form. This digital collection is also mirrored on the [http://www.dialoguejournal.com/search ''Dialogue'' website].
At the 2005 annual conference of the [[Association for Mormon Letters]] ("AML"), the AML presented a "Special Award in Mormon Literary Studies" with this citation:
<blockquote>
Since its inception in 1966, ''Dialogue: A Journal of Mormon Thought'' has served as a central point for academic studies of Mormonism. ''Dialogue'' has promoted Mormon arts and letters by publishing hundreds of poems, short stories, personal essays, and articles of criticism. However, like many periodicals, Dialogue's treasures have generally remained only as accessible as the most recent issue or two. ...Nearly 40 years of back issues-the entire run of this seminal periodical for Mormon studies-have now been completely digitized and made accessible to the public free of charge...This collection would not be as valuable or secure for future generations were it not hosted by an academic institution committed to maintaining the accessibility and permanence of this collection. The Association for Mormon Letters commends the [http://www.lib.utah.edu/ J. Willard Marriott Library] of the [[University of Utah]] for providing a permanent home to this vital digital  collection, and by extension, for the university's commitment to the sustained and critical assessment of Mormon arts and letters.</blockquote>

== See also ==
{{Portal|Latter-day Saints}}
* [[Bloggernacle]]
* [[List of theological journals]]
* [[List of Latter Day Saint periodicals]]

== Footnotes ==
{{Reflist|2}}

== References ==
* Carrie A. Moore, "[http://www.deseretnews.com/dn/view/0,1249,650192701,00.html Dialogue Journal Celebrating 40 Years]," ''Deseret Morning News'', September 22, 2006.
*Gary James Bergera, "Four decades of independent Mormon thought," ''Salt Lake Tribune'', September 10, 2006.

==External links==
*[http://www.dialoguejournal.com/ ''Dialogue'' website]
*[http://www.mhahome.org Mormon History Association website]
*[https://www.dialoguejournal.com/archive/ Searchable index of past ''Dialogue'' issues ]
*[http://content.lib.utah.edu/cdm4/browse.php?CISOROOT=/dialogue Digital collection of 40 years of ''Dialogue'' at the University of Utah's Marriott Library]

{{LDSChurchpubs}}

{{DEFAULTSORT:Dialogue: A Journal Of Mormon Thought}}
[[Category:Latter Day Saint periodicals]]
[[Category:Publications established in 1966]]
[[Category:Mormon studies journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:1966 in Christianity]]