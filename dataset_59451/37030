{{good article}}
{{Infobox NRHP
 | name = Nathaniel Backus House
 | nrhp_type = nrhp 
 | image = Nathaniel Backus home.JPG
 | caption = 
 | location= 44 Rockwell St., [[Norwich, Connecticut]]
 | coordinates = {{coord|41|32|4|N|72|4|42|W|display=inline,title}}
| locmapin = Connecticut#USA
 | built = 1750
 | architecture = [[Greek Revival architecture|Greek Revival]], Colonial
 | added = October 6, 1970
 | area = {{convert|2|acre}}
 | governing_body = Private
 | refnum = 70000715<ref name="nris">{{NRISref|2009a}}</ref>
}}

The '''Nathaniel Backus House''' is a two-story [[Greek Revival]] clapboarded house with a [[gable]] roof in [[Norwich, Connecticut]]. The house was built around 1750 by Nathaniel Backus and served as his home, it was later moved to its current location in 1952. The house originally began as a [[Colonial architecture|Colonial]], but was greatly modified to Greek Revival around 1825, reconfiguring the central door to the left of the facade and adding two chimneys. The house is a [[historic house museum]] operated by the Faith Trumbull Chapter of the [[Daughters of the American Revolution]].

The Nathaniel Backus House was submitted to the National Register of Historic Places for its historical value in local history and as an example of Greek Revival domestic architecture. It was listed on the [[National Register of Historic Places]] (NRHP) in 1970 and was also included in the NRHP's [[Chelsea Parade Historic District]] designation in 1989.

== Nathaniel Backus ==
The Nathaniel Backus House's namesake is its builder Nathaniel Backus, a descendent of William Backus and William Backus, Jr., two of the founders of [[Norwich, Connecticut]]. Nathaniel Backus was born on April 5, 1704 and he married Hannah Baldwin in 1726. Together they would have seven children. Nathaniel Backus was also recorded to be one of six men in Norwich to own their own carriages before the [[American Revolutionary War]].<ref name="hp">{{cite web | url={{NRHP url|id=70000715}} | title=National Register of Historic Places - Nathaniel Backus House | publisher=National Park Service | date=6 October 1970 | accessdate=4 April 2014 | author=Luyster, Constance}}</ref> Nathaniel Backus died in 1773.<ref name="rev">{{cite book | url=https://books.google.com/books?id=4J8IAQAAMAAJ&q=Nathaniel+Backus+1704&dq=Nathaniel+Backus+1704&hl=en&sa=X&ei=TYlWU4zMD6u-sQSOyoHYAQ&ved=0CEwQ6AEwBg | title=Historic and memorial buildings of the Daughters of the American Revolution | publisher=National Society, Daughters of the American Revolution | year=1979 | pages=68}}</ref>

== Design ==
The Nathaniel Backus House's construction date is unknown, but it is believed to have been around 1750. The ''History of Norwich, Connecticut'' places it around 1734 and makes mention of a highway being added in 1750 specifically by Nathaniel Backus' house.<ref name="apple">{{cite book | url=https://books.google.com/books?id=d-SOZZA4leEC&pg=PA185-IA1&lpg=PA185-IA1&dq=Nathaniel+Backus+House&source=bl&ots=1VAY-X4ChJ&sig=HqxaCt_3xvrSX7DFs9FjMMjwPdc&hl=en&sa=X&ei=1Us_U6mjG7KqsQSawIHQBg&ved=0CEIQ6AEwBzgK#v=onepage&q=Nathaniel%20Backus%20House&f=false | title=History of Norwich, Connecticut | publisher=Applewood Books | date=2010 | accessdate=4 April 2014 | author=Caulkins, Francis | pages=145, 185}}</ref> In the 1970 National Historic Register of Places nomination, the Daughters of the American Revolution indicated it dates from 1750.<ref name=hp />

The house originally stood on Broadway Street in the center of [[Norwich, Connecticut]]. It was originally Colonial at its time of construction, but has been modified to Greek Revival style.<ref name=hp /> The renovation itself may date to around 1825.<ref name="dist">{{cite book | url=https://books.google.com/books?id=OpxRPBjvrUcC&lpg=PP1&pg=PA276#v=onepage&q&f=false | title=Connecticut: A Guide to Its Roads, Lore and People | publisher=North American Book Dist LLC | author=Federal Writers Project | year=2007}}</ref>{{refn|group=note|''Connecticut: A Guide to Its Roads, Lore and People'' uses 1825-1830 as its date, but records show its existence dating prior to 1750.<ref name=apple /><ref name=dist /> The date, however, approximates the substantial renovations and alterations to the house.<ref name=dist />}} The house is a white two-story clapboarded structure with its gable end facing the street. The three-bay facade faces south and the front entrance is located on the left bay. The eaves [[cornice]] is decorated with mutules that span the length of the gable ends and combines with the roof cornice to make a pediment that encloses an elliptical window in the center. The window is of the "rising sun pattern" with glass panes radiating out through two rings. The entrance on the left of the facade has a paneled door, stated to be original, that is enclosed in a rectangular frame, supported by Ionic columns and framed by fluted moldings. The frames of the 6-over-6 windows project slightly from the clapboard exterior. The foundation and steps to the house is of stone.<ref name=hp />

An alteration saw the addition of two chimneys and additional alterations to the window and door openings on the northeast and southwest sides of the house. It is believed that the central door and window alterations were done as part of the Greek Revival renovations.<ref name=hp /> Luyster states, "[f]urther investigation would undoubtedly reveal additional changes in the interior, including changes in the position of the fireplaces and their chimney connections."<ref name=hp />

== Importance ==
The Nathaniel Backus House is a [[historic house museum]] operated by the Faith Trumbull Chapter of the [[Daughters of the American Revolution]] following its completed move in 1952.<ref name=hp /><ref name="ftc">{{cite web | url=http://www.faithtrumbulldar.org/pb/wp_0317b71f/wp_0317b71f.html | title=Chapter History | publisher=Faith Trumbull Chapter of the Daughters of the American Revolution | accessdate=4 April 2014}}</ref> The Nathaniel Backus House was submitted to the National Register of Historic Places for its historical value in local history and as an example of Greek Revival domestic architecture.<ref name=hp /> Luyster writes, "The simplicity of the Backus house contrasts pleasantly with the verandahs and asymmetric forms of the surrounding buildings."<ref name=hp /> It was listed on the [[National Register of Historic Places]] in 1970.<ref name=hp /> The house was also included as part of the [[Chelsea Parade Historic District]] in 1989.<ref name="nrhpinv3_ChelseaParade">{{cite web|url={{NRHP url|id=88003215}} |title=National Register of Historic Places Inventory-Nomination: Chelsea Parade |date=June 25, 1988 |author=William Devlin and Bruce Clouette |publisher=National Park Service}} and {{NRHP url|id=88003215|title=''Accompanying 25 photos, undated but seemingly from 1988''|photos=y}}</ref>

==See also==
*[[National Register of Historic Places listings in New London County, Connecticut]]
*[[Perkins-Rockwell House]] - another historic home located next door to the Nathaniel Backus House that is owned by the Faith Trumbull Chapter of the Daughters of the American Revolution

==Notes==
{{reflist|group=note}}

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.faithtrumbulldar.org/index.html|Faith Trumbull Chapter of the Daughters of the American Revolution official website}}

{{National Register of Historic Places}}

{{DEFAULTSORT:Backus, Nathaniel, House}}
[[Category:Houses on the National Register of Historic Places in Connecticut]]
[[Category:Houses completed in 1750]]
[[Category:Houses in Norwich, Connecticut]]
[[Category:National Register of Historic Places in New London County, Connecticut]]
[[Category:Museums in New London County, Connecticut]]