<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
{|{{Infobox Aircraft Begin
 |name=Jupiter
 |image=jupiter.engine.arp.750pix.jpg
 |caption=Preserved Bristol Jupiter
}}{{Infobox Aircraft Engine
 |type=[[Piston]] [[aircraft engine]]
 |manufacturer=[[Bristol Aeroplane Company]]
 |designer=[[Roy Fedden]]
 |first run=29 October {{avyear|1918}}
 |major applications=[[Bristol Bulldog]]<br>[[Gloster Gamecock]]
 |number built = >7,100 
 |program cost = 
 |unit cost = 
 |developed from = 
 |developed into = [[Bristol Mercury]]
 |variants with their own articles = 
}}
|}

The '''Bristol Jupiter''' was a [[United Kingdom|British]] nine-cylinder single-row piston [[radial engine]] built by the [[Bristol Aeroplane Company]]. Originally designed late in [[World War I]] and known as the '''Cosmos Jupiter''', a lengthy series of upgrades and developments turned it into one of the finest engines of its era.

The Jupiter was widely used on many aircraft designs during the 1920s and 1930s. Thousands of Jupiters of all versions were produced, both by Bristol and abroad under licence. 

A [[Turbocharger|turbo-supercharged]] version of the Jupiter known as the '''Orion''' suffered development problems and only a small number were produced. The "Orion" name was later [[Bristol Orion|re-used]] by Bristol for an unrelated [[turboprop]] engine.

==Design and development==
The '''Jupiter''' was designed during [[World War I]] by [[Roy Fedden]] of [[Cosmos Engineering]]. During the rapid downscaling of military spending after the war, Cosmos became [[bankrupt]] in 1920, and was eventually purchased by the [[Bristol Aeroplane Company]] on the strengths of the Jupiter design and the encouragement of the [[Air Ministry]].<ref>Gunston 1989, p.44.</ref> The engine matured into one of the most reliable on the market. It was the first air-cooled engine to pass the Air Ministry full-throttle test, the first to be equipped with automatic boost control, and the first to be fitted to airliners.<ref>Gunston 1989, p.31.</ref>
 
The Jupiter was fairly standard in design, but featured four valves per cylinder, which was uncommon at the time. The [[Cylinder (engine)|cylinders]] were machined from steel forgings, and the cast [[cylinder head]]s were later replaced with aluminium alloy following studies by the [[Royal Aircraft Establishment|RAE]]. In 1927, a change was made to move to a forged head design due to the rejection rate of the castings. The Jupiter VII introduced a mechanically driven supercharger to the design, and the Jupiter VIII was the first to be fitted with reduction gears.<ref>Bridgman (Jane's) 1998, p.270.</ref>

In 1925, Fedden started designing a replacement for the Jupiter. Using a shorter stroke to increase the [[rpm]], and including a [[supercharger]] for added power, resulted in the [[Bristol Mercury]] of 1927. Applying the same techniques to the original Jupiter-sized engine in 1927 resulted in the [[Bristol Pegasus]]. Neither engine would fully replace the Jupiter for a few years.

In 1926 a Jupiter-engined [[Bristol Type 84 Bloodhound|Bristol Bloodhound]], G-EBGG, completed an endurance flight of 25,074 miles, during which the Jupiter ran for a total of 225 hours, 54 minutes, without part failure or replacement.<ref>http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200183.html</ref>

===Licensed production===
The Jupiter saw widespread use in licensed versions, with fourteen countries eventually producing the engine. In [[France]], [[Gnome-Rhone]] produced a version known as the '''Gnome-Rhône 9 ''Jupiter''''' which was used in several local civilian designs, as well as achieving some export success. [[Siemens-Halske]] took out a licence in Germany and produced several versions of increasing power, eventually resulting in the '''[[Bramo 323]] ''Fafnir''''', which saw use in German wartime aircraft.<ref>Gunston 1989, p.29.</ref>

In [[Japan]], the Jupiter was licence-built from 1924 by [[Nakajima Aircraft Company|Nakajima]], forming the basis of their own subsequent radial aero-engine design, the [[Nakajima Kotobuki|'''Nakajima Ha-1 ''Kotobuki''''']].<ref>Gunston 1989, p.104.</ref> It was produced in Poland as the '''PZL ''Bristol Jupiter''''', in Italy as the '''[[Alfa Romeo 125|Alfa Romeo 126-RC35]]''',<ref name="aroca-qld.com">{{cite web|url=http://www.aroca-qld.com/library_articles/alfa_romeo_aero_engines.php |title=Alfa Aero Engines |accessdate=2007-08-25 |work=aroca-qld.com |archiveurl=https://web.archive.org/web/20071008150746/http://www.aroca-qld.com/library_articles/alfa_romeo_aero_engines.php |archivedate=2007-10-08 |deadurl=yes |df= }}</ref> and in [[Czechoslovakia]] by [[Walter Engines]]. The most produced version was in the [[Soviet Union]], where their '''[[Shvetsov]] M-22''' version powered the initial Type 4 version of the [[Polikarpov I-16]] (55 units produced). Type 4 Polikarpovs can be identified by their lack of exhaust stubs, rounded NACA cowling and lack of cowling shutters, features which were introduced on the [[Shvetsov M-25]] powered Type 5 and later variants (total production 4,500+ units).<ref>[http://vvs.hobbyvista.com/Modeling/Polikarpov/I-16/Kit_Comparison/description.php]</ref><ref>Gunston 1989, p.158.</ref> Production started in 1918 and ceased in 1930.

==Variants==
The Jupiter was produced in many variants, one of which was the '''Bristol Orion''' of 1926. [[Metallurgy]] problems with this turbo-supercharged engine caused the project to be abandoned after only nine engines had been built.<ref>Lumsden 2003, p.101.</ref>
;Brazil Straker Jupiter I
:(1918) {{convert|400|hp|kW|abbr=on}}. Two built.
;Cosmos Jupiter II
:(1918) {{convert|400|hp|kW|abbr=on}}. One built.
;Bristol Jupiter II
:(1923) {{convert|400|hp|kW|abbr=on}}.
;Bristol Jupiter III
:(1923) {{convert|400|hp|kW|abbr=on}}.
[[File:BristolJupiter.JPG|thumb|right|<center>Bristol Jupiter VII on display at the [[Shuttleworth Collection]].</center>]]
;Bristol Jupiter IV
:(1926) {{convert|430|hp|kW|abbr=on}}. Variable valve timing, Bristol Triplex carburettor.
;Bristol Jupiter V
:(1925) {{convert|480|hp|kW|abbr=on}}. 
;Bristol Jupiter VI
:(1927) {{convert|520|hp|kW|abbr=on}}. Produced in both high (6.3:1) and low (5.3:1) [[compression ratio]]s. 
;Bristol Jupiter VIA
:(1927) {{convert|440|hp|kW|abbr=on}}. Civil version of Jupiter VI.
;Bristol Jupiter VIFH
:(1932) {{convert|440|hp|kW|abbr=on}}.  Equipped with gas starter motor.
;Bristol Jupiter VIFL
:(1932) {{convert|440|hp|kW|abbr=on}}.  Compression ratio 5.15:1.
;Bristol Jupiter VIFM
:(1932) {{convert|440|hp|kW|abbr=on}}.  Compression ratio 5.3:1.
;Bristol Jupiter VIFS
:(1932) {{convert|400|hp|kW|abbr=on}}.  Compression ratio 6.3:1.
;Bristol Jupiter VII
:(1928) {{convert|375|hp|kW|abbr=on}}.  Compression ratio 5.3:1, fully supercharged. Built by Gnome-Rhone as the '''9ASB'''.
;Bristol Jupiter VIIF
:(1929) {{convert|480|hp|kW|abbr=on}}. Compression ratio 5.3:1. Forged cylinder heads.
[[File:Bristol Jupiter VIIIF 2.jpg|thumb|right|<center>Preserved Bristol Jupiter VIIIF.</center>]]
;Bristol Jupiter VIIF.P
:(1930) {{convert|480|hp|kW|abbr=on}}. 'P' for pressure feed lubrication to wrist-pins.
;Bristol Jupiter VIII
:(1929) {{convert|440|hp|kW|abbr=on}}. Jupiter VI but compression ratio increased to 6.3:1.
;Bristol Jupiter VIIIF
:(1929) {{convert|460|hp|kW|abbr=on}}. Jupiter VIII with forged cylinder heads and lowered compression ratio (5.8:1).  
;Bristol Jupiter VIIIF.P
:(1929) {{convert|460|hp|kW|abbr=on}}. As Jupiter VIII with pressure feed lubrication ([[Time between overhaul|TBO]] at this stage in development was only 150 hours due to multiple failures).
;Bristol Jupiter IX 
: {{convert|480|hp|kW|abbr=on}}. Compression ratio 5.3:1. 
;Bristol Jupiter IXF
: {{convert|550|hp|kW|abbr=on}}.  Compression ratio 5.3:1. Forged cylinder heads.
;Bristol Jupiter X
: {{convert|470|hp|kW|abbr=on}}.  Compression ratio 5.3:1. 
;Bristol Jupiter XF
: {{convert|540|hp|kW|abbr=on}}.  Compression ratio 5.3:1. Forged cylinder heads.
;Bristol Jupiter XFA
: {{convert|483|hp|kW|abbr=on}}.  Compression ratio 5.3:1. 
;Bristol Jupiter XFAM
: {{convert|580|hp|kW|abbr=on}}.  
;Bristol Jupiter XFBM
: {{convert|580|hp|kW|abbr=on}}.  
;Bristol Jupiter XFS
:Fully supercharged.
;Bristol Jupiter XI
:Compression ratio 5.15:1. 
;Bristol Jupiter XIF
: {{convert|500|hp|kW|abbr=on}}.  Compression ratio 5.15:1.
;Bristol Jupiter XIFA
: {{convert|480|hp|kW|abbr=on}}.  As Jupiter XIF with 0.656:1 reduction ratio
;Bristol Jupiter XIF.P
: {{convert|525|hp|kW|abbr=on}}. As Jupiter XIF with pressure feed lubrication.
;Bristol Orion I
:(1926) Jupiter III, turbo-supercharged, abandoned programme.
;Gnome-Rhône 9A Jupiter:French licence production primarily of 9A, 9Aa, 9Ab, 9Ac, 9Akx and 9Ad variants.
;[[Siemens-Halske Sh20, Sh21 and Sh22]]: Siemens-Halske took out a licence in Germany and produced several versions of increasing power, eventually resulting in the [[Bramo 323 Fafnir]], which saw use in wartime models.
;[[Nakajima Ha-1 Kotobuki]]:In Japan, the Jupiter was licence-built from 1924 by Nakajima.
;PZL Bristol Jupiter:Polish production.
;[[Alfa Romeo Jupiter]]:Italian licence production, {{convert|420|hp|kW|abbr=on}}.
;[[Alfa 126 R.C.35]]:Alfa Romeo developed variant
;Walter Jupiter:Licence production in Czechoslovakia by Walter Engines
;Shvetsov M-22:The most produced version was in the [[Soviet Union]].
;IAM 9AD:Licence production of the Gnome-Rhône 9A in [[Yugoslavia]]

==Applications==
The Jupiter is probably best known for powering the [[Handley Page Aircraft Company|Handley Page]] [[Handley Page H.P.42|HP.42 ''Hannibal'']] airliners, which flew the [[London]]-[[Paris]] route in the 1930s. Other civilian uses included the [[de Havilland Giant Moth]] and [[de Havilland Hercules|Hercules]], the [[Junkers G 31]] (which would evolve into the famous [[Junkers Ju 52|Ju-52]]), and the huge [[Dornier Do X]] flying boat, which used no less than twelve engines.

Military uses were less common, but included the parent company's [[Bristol Bulldog]], as well as the [[Gloster Gamecock]] and [[Boulton Paul Sidestrand]]. It was also found in prototypes around the world, from [[Japan]] to [[Sweden]].

By 1929 the Bristol Jupiter had flown in 262 different aircraft types, it was noted in the French press at that year's [[Paris Air Show]] that the Jupiter and its license-built versions were powering 80% of the aircraft on display.<ref>Gunston 2006, p.126.</ref>
 
''Note:''<ref>British aircraft list from Lumsden, the Jupiter may not be the main powerplant for these types</ref>

===Cosmos Jupiter===
*[[Bristol Badger]]
*[[Bristol Bullet]]
*[[Sopwith Tabloid|Sopwith Schneider]]
*[[Westland Limousine]]

===Bristol Jupiter===
{{colbegin|3}}
*[[Aero A.32]]
*[[Airco DH.9]] 
*[[Arado Ar 64]]
*[[Avia BH-25]]
*[[Avia BH-33E]]
*[[Bernard 190]]
*[[Blériot-SPAD 51]]
*[[Blériot-SPAD S.56]]
*[[Boulton & Paul Bugle]]
*[[Boulton Paul P.32]]
*[[Boulton Paul Partridge]]
*[[Boulton Paul Sidestrand]]
*[[Blackburn Beagle]]
*[[Blackburn Sydney|Blackburn Nile]]
*[[Blackburn Ripon]]
*[[Bristol Badger]]
*[[Bristol Badminton]]
*[[Bristol Bagshot]]
*[[Bristol Boarhound|Bristol Beaver]]
*[[Bristol Type 84 Bloodhound|Bristol Bloodhound]]
*[[Bristol Boarhound]]
*[[Bristol Ten-seater|Bristol Brandon]]
*[[Bristol Bulldog]]
*[[Bristol Bullfinch]]
*[[Bristol Jupiter Fighter]]
*[[Bristol Seely]]
*[[Bristol Racer|Bristol Type 72]]
*[[Bristol Ten-seater|Bristol Type 75]]
*[[Bristol Jupiter Fighter|Bristol Type 76]]
*[[Bristol Jupiter Fighter|Bristol Type 89]]
*[[Bristol Type 92]]
*[[Bristol Type 118]]
*[[de Havilland Dormouse|de Havilland Dingo]]
*[[de Havilland DH.72]]
*[[de Havilland DH.50]]
*[[de Havilland Dormouse]]
*[[de Havilland Hercules]]
*[[de Havilland Hound]]
*[[de Havilland Giant Moth]]
*[[Gloster Survey|de Havilland Survey]]
*[[Dornier Do 11]]
*[[Dornier Do J]]
*[[Dornier Do X]]
*[[Fairey III|Fairey IIIF]]
*[[Fairey Ferret]]
*[[Fairey Flycatcher]]
*[[Fairey Hendon]]
*[[Fokker C.V]]
*[[Fokker F.VII|Fokker F.VIIA]]
*[[Fokker F.VIII]]
*[[Fokker F.IX]]
*[[Gloster Gambet]]
*[[Gloster Gamecock]]
*[[Gloster Gnatsnapper]]
*[[Gloster Goldfinch]]
*[[Gloster Goral]]
*[[Gloster Goring]]
*[[Gloster Grebe]]
*[[Gloster Mars]]
*[[Gloster Survey]]
*[[Gourdou-Leseurre GL.30|Gourdou-Leseurre LGL.32]]
*[[Handley Page Hinaidi|Handley Page Clive]]
*[[Handley Page Type W#W9a Hampstead|Handley Page Hampstead]]
*[[Handley Page Hare]]
*[[Handley Page Hinaidi]]
*[[Handley Page Type O|Handley Page HP.12]]
*[[Handley Page H.P.42]]
*[[Hawker Duiker]]
*[[Hawker Harrier]]
*[[Hawker Hart]]
*[[Hawker Hawfinch]]
*[[Hawker Hedgehog]]
*[[Hawker Heron]]
*[[Hawker Woodcock]]
*[[Junkers F.13]]
*[[Junkers G 31]]
*[[Junkers W 34]]
*[[Parnall Plover]]
*[[PZL P.7]]
*[[Saunders Medina]]
*[[Saunders Severn]]
*[[Short S.8 Calcutta|Short Calcutta]]
*[[Short Springbok|Short Chamois]]
*[[Short Gurnard]]
*[[Short Kent]]
*[[Short Rangoon]]
*[[Short Scylla]]
*[[Short Springbok]]
*[[Short S.6 Sturgeon]]
*[[Short Valetta]]
*[[Supermarine Seagull (1921)|Supermarine Seagull]]
*[[Supermarine Nanok|Supermarine Solent]]
*[[Supermarine Southampton]]
*[[Svenska Aero Jaktfalken]]
*[[Tupolev I-4]]
*[[Vickers Type 123|Vickers F.21/26]]
*[[Vickers Type 161|Vickers F.29/27]]
*[[Vickers Jockey]]
*[[Vickers Type 143]]
*[[Vickers Vanox|Vickers Type 150]]
*[[Vickers 131 Valiant|Vickers Valiant]]
*[[Vickers Vellore]]
*[[Vickers Vellore|Vickers Vellox]]
*[[Vickers Vespa]]
*[[Vickers Viastra]]
*[[Vickers Victoria]]
*[[Vickers Vildebeest]]
*[[Vickers Vimy]]
*[[Vickers Vimy|Vickers Vimy Trainer]]
*[[Vickers Wibault|Vickers Wibault Scout]]
*[[Villiers 26]]
*[[Westland Interceptor]]
*[[Westland Wapiti]]
*[[Westland Westbury]]
*[[Westland Witch]]
*[[Westland PV-3|Westland-Houston PV.3]]
{{colend}}

===Gnome-Rhône Jupiter===
*[[Bernard SIMB AB 12]]
*[[Blanchard BB-1]]
*[[Fizir F1V|Fizir F1M-Jupiter]]
*[[Latécoère 6]]
*[[Potez 29|Potez 29/4]]
*[[Wibault 220|Wibault Wib.220]]

===Shvetsov M-22===
*[[Kalinin K-5]]
*[[Polikarpov I-5]]
*[[Polikarpov I-15]]
*[[Polikarpov I-16]]
*[[Tupolev I-4]]
*[[Yakovlev AIR-7]]<ref>OKB YAKOVLEV, Yefim Gordon, Dmitriy Komissarov, Sergey Komissarov, 2005, Midland Publishing pp 28-29</ref>

==Engines on display==
A Bristol Jupiter VIIF is on static display at the [[Shuttleworth Collection]], [[Old Warden]], [[Bedfordshire]].

==Specifications (Jupiter XFA)==
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully formatted line with <li> -->
|ref=''Lumsden''<ref>Lumsden 2003, p.96.</ref>
|type=Nine-cylinder, naturally aspirated, air-cooled [[radial engine]]
|bore=5.75 in (146 mm)
|stroke=7.5 in (190 mm)
|displacement=1,753 in<sup>3</sup> (28.7 L)
|length=
|diameter=54.5 in (1,384 mm)
|width=
|height=
|weight=995 lb (451 kg)
|valvetrain=Overhead poppet valve, four valves per cylinder, two intake and two exhaust
|supercharger=Single speed, single stage
|turbocharger=
|fuelsystem=
|fueltype=73-77 [[Octane rating|Octane]] [[petrol]]
|oilsystem=
|coolingsystem=Air-cooled
|power=<br>
*550 hp (414 kW) at 2,200 rpm at 11,000 ft (3,350 m) - maximum power limited to five minutes operation.
*525 hp (391 kW) at 2,000 rpm - maximum continuous power at 11,000 ft (3,350 m)
*483 hp (360 kW) at 2,000 rpm - takeoff power 
|specpower=0.31 hp/in<sup>3</sup> (14.4 kW/L)
|compression=5.3:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=0.55 hp/lb (0.92 kW/kg)
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Alfa Romeo 125]]
*[[Bristol Mercury]]
*[[Bristol Pegasus]]
*[[Nakajima Kotobuki]]
*[[Siemens-Halske Sh 22]]
<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[BMW 132]]
*[[Pratt & Whitney R-1340]], first of the famous ''Wasp'' radial engine line
*[[Pratt & Whitney R-1690]] ''Hornet''
*[[Wright R-1820]] ''Cyclone 9''
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Bridgman, L. (ed.) ''Jane's Fighting Aircraft of World War II''. New York: Crescent Books, 1998. ISBN 0-517-67964-7
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* Gunston, Bill. ''Development of Piston Aero Engines''. Cambridge, England. Patrick Stephens Limited, 2006. ISBN 0-7509-4478-1
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
{{refend}}

==Further reading==
*Gunston, Bill. ''By Jupiter! The Life of Sir Roy Fedden''. The Johns Hopkins University Press.

==External links==
{{Commons category|Bristol Jupiter}}
*{{cite journal |date=3 July 1919 |title=The Cosmos Aero Engines |format=PDF |journal=[[Flight (magazine)|Flight]] |volume=XI |issue=27 |id=No. 549 |pages=869–871 |url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200869.html |accessdate=12 January 2011 }} Contemporary article on Cosmos Engineering's air-cooled radial engines. Photos of the Cosmos Jupiter are on [http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200870.html page 870], and a short technical description is on [http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200871.html page 871].
*[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200183.html Bristol Jupiter endurance test - ''Flight'', March 1926]
*[http://www.flightglobal.com/pdfarchive/view/1929/1929%20-%200608.html  A 1929 ''Flight'' advertisement for the Jupiter]

{{Cosmos aeroengines}}
{{BristolAeroengines}}
{{Alfa Romeo aeroengines}}
{{Gnome-Rhône aeroengines}}
{{Walter aeroengines}}

[[Category:Aircraft air-cooled radial piston engines]]
[[Category:Bristol aircraft engines|Jupiter]]
[[Category:Aircraft piston engines 1910–1919]]