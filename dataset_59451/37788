{{Infobox religious building
| building_name         =Beth Israel
| image                 =BethIsraelLakeviewNOLAFeb2010D.jpg
| image_size            =300px
| caption               =Beth Israel building on Canal Boulevard in 2010, gutted and<br/>vacant. Lines from long standing floodwaters are still visible.
| map_type              =
| map_size              =
| map_caption           =
| location              =4004 West Esplanade Avenue,<br/>[[Metairie, Louisiana]]<br/>{{flag|United States}}
| geo                   = {{coord|30.0148933|-90.178638|display=inline,title}}
| religious_affiliation =[[Orthodox Judaism]]
| rite                  =
| region                =
| province              =
| territory             =
| prefecture            =
| sector                =
| district              =
| cercle                =
| municipality          =
| consecration_year     =
| status                =
| functional_status     =Active
| heritage_designation  =
| leadership            =Rabbi: Gabriel Greenberg<ref name=Rabbi>[[#refRabbi|Our New Rabbinic Family]], Synagogue website.</ref>
| website               ={{url|bethisraelnola.com}}
| architecture          =yes
| architect             =
| architecture_type     =
| architecture_style    =
| general_contractor    =
| facade_direction      =
| groundbreaking        =
| year_completed        =2012
| construction_cost     =
| specifications        =
| capacity              =
| length                =
| width                 =
| width_nave            =
| height_max            =
| dome_quantity         =
| dome_height_outer     =
| dome_height_inner     =
| dome_dia_outer        =
| dome_dia_inner        =
| minaret_quantity      =
| minaret_height        =
| spire_quantity        =
| spire_height          =
| materials             =
| nrhp                  =
| added                 =
| refnum                =
| designated            =
}}
'''Congregation Beth Israel''' ({{lang-he|בית ישראל}}) is a [[Modern Orthodox Judaism|Modern Orthodox]] [[synagogue]] located in [[Louisiana]]. Founded in 1903 or 1904,<ref name="Founded" /> though tracing its roots back to 1857, it is the oldest Orthodox congregation in the [[New Orleans-Metairie-Hammond, LA-MS CSA|New Orleans region]].<ref name="LachoffKahn2005p24" /><ref name="Greenberg" /> Originally located on Carondelet Street in New Orleans' [[Central City, New Orleans|Central City]], it constructed and moved to a building at 7000 Canal Boulevard in [[Lakeview, New Orleans]] in 1971.<ref name="LachoffKahn2005p124" />

At one time the largest Orthodox congregation in the [[Southern United States]], its membership was over 500 families in the 1960s, but fell to under 200 by 2005.<ref name="Greenberg" /><ref name="LachoffKahn2005p124" /> That year its Canal Boulevard building was severely flooded by the [[2005 levee failures in Greater New Orleans|2005 New Orleans levee failure disaster]] during [[Hurricane Katrina]].<ref name="Smason2007" /> Despite attempts to save them,<ref name="Moore" /> all seven of its [[Torah]] scrolls were destroyed,<ref name="OU2006" /> as were over 3,000 prayer-books.<ref name="Okun" /> The building suffered further flooding damage caused by the theft of copper air-conditioning tubing in 2007.<ref name="Smason2007" />

In the wake of Katrina another 50 member families left New Orleans, including the [[rabbi]]'s.<ref name="Greenberg" /><ref name="Smason2006" /> The congregation began sharing space with Gates of Prayer, a [[Reform Judaism|Reform]] synagogue in [[Metairie, Louisiana|Metairie]], a suburb of New Orleans. In 2009, the congregation purchased land from Gates of Prayer, and by 2012 had built a new synagogue next to it at 4000 West Esplanade Avenue.<ref name="NaronChalew2009" /><ref name="TO2009p4" /><ref name="Nolan20120825" /> {{As of|2016}} the rabbi was Gabriel Greenberg.<ref name="Rabbi" />

==Early history==
Beth Israel is the oldest Orthodox congregation in the New Orleans region,<ref name="Greenberg">[[#refGreenberg2007|Greenberg (2007)]].</ref> and its most prominent.<ref name="Nolan20070913">[[#refNolan20070913|Nolan (September 13, 2007)]].</ref> Though it was founded as early as 1903,<ref name="Founded" /> it traces its roots back to much older synagogues. In the mid-19th century New Orleans had a number of small Orthodox congregations of [[Eastern Europe]]an Jews, generally "structured along nationalistic lines". These included a synagogue of [[History of the Jews in Galicia (Eastern Europe)|Galitzianer Jews]] (Chevra Thilim), and two of [[Lithuanian Jews]], (one—Chevra Mikve Israel—following the [[Nusach#Nusach Ashkenaz|non-Hassidic liturgy]], the other—[[Anshe Sfard]]—following the [[Nusach Sefard|Hassidic liturgy]]). In 1857, a congregation consisting primarily of [[Kingdom of Prussia|Prussian]] Jews from [[Province of Posen|Posen]] organized as Tememe Derech, "The Right Way". As they followed the [[History of the Jews in Poland|Polish]] rite, they were known as "The Polish Congregation".<ref name="LachoffKahn2005p8">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 8.</ref><ref name="ISJLNOLAOrthodox">[[#refISJLNOLAOrthodox|Institute of Southern Jewish Life (2010)]].</ref>

Tememe Derech built a synagogue in the 500 block of Carondelet Street in the [[Central City, New Orleans|Central City]] section of New Orleans in 1867. It was the sole Orthodox congregation to construct its own building; only a minority of New Orleans' Jews were Orthodox, and other congregations rented space or met in members' homes.<ref name="LachoffKahn2005p8" /> Tememe Derech's membership, however, never exceeded 50,<ref name="LachoffKahn2005p24">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 24.</ref> and in 1903 or 1904<ref name="Founded" /> the synagogue disbanded, and merged with a number of other small Orthodox congregations and a [[burial society]] to form Beth Israel.<ref name="LachoffKahn2005p24" /><ref name="ISJLNOLAOrthodox" /> Services were initially held in rented quarters in the same 500 block of Carondelet Street.<ref name="ISJLNOLAOrthodox" />
[[File:BethIsraelCarondeletFrontA.JPG|left|thumb|255px|Beth Israel synagogue building on Carondelet St.]]
In 1905, Beth Israel purchased the home of New Orleans' former mayor [[Joseph A. Shakspeare]] at 1610 Carondelet Street. Funds for the new acquisition came from both the Orthodox and [[Reform Judaism|Reform]] communities of New Orleans. After remodeling the building, the congregation began holding services there, in time for the 1906 [[High Holy Days]].<ref name="ISJLNOLAOrthodox" /><ref name="LachoffKahn2005p64">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 64.</ref><ref name="refAJYearBookV21p379">[[#refAJYearBookV21|''American Jewish Year Book'', Vol. 21]], p.&nbsp;379.</ref>   Membership grew quickly; by 1910 Beth Israel was the second-largest Jewish congregation in the city,<ref name="LachoffKahn2005p65">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 65.</ref> with 180 member families, and by 1914 that number had grown to 250 families.<ref name="ISJLNOLAOrthodox" /> By 1918, however, membership had fallen to 175 families. That year the synagogue's income was $6,000 (today ${{formatnum:{{inflation|US|6000|1918|r=-3}}}}).<ref name="refAJYearBookV21p379" />

Moses Hyman Goldberg was the congregation's first rabbi, but within a year he moved to Chevra Thilim.<ref name="Goldberg" /> Goldberg served as New Orleans' ''[[mohel]]'' until his death in 1940.<ref name="LachoffKahn2005p97">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 97</ref>

==1920s to early 2000s==
Beth Israel rebuilt its synagogue at the Carondelet Street location in 1924.<ref name="LachoffKahn2005p8" /> The new building was designed by Emil Weil, a noted [[Southern United States|Southern]] architect, particularly of Jewish religious buildings. He designed other New Orleans' synagogues, including the Touro Synagogue and the Anshe Sfard, as well as other non-religious buildings throughout Louisiana.<ref name="LachoffKahn2005p82">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 82.</ref> Beth Israel's new [[Byzantine Revival architecture|Byzantine Revival]] building, with its seating capacity of 1,200, reflected "the growing economic and social confidence of the membership": it had "beautiful" stained-glass windows, a "magnificent" imported European chandelier, and "hand-carved Stars of David in the ceiling".<ref name="ISJLNOLAOrthodox" /><ref name="LachoffKahn2005p67">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 67.</ref> The building was dedicated on September 12, and a Boston rabbi, Henry Raphael Gold, was a guest speaker. He was so impressed that he stayed on, becoming Beth Israel's rabbi.<ref name="ISJLNOLAOrthodox" />
[[File:BethIsraelMenorahInstituteEuterpeA.JPG|right|thumb|215px|Menorah Institute building on Euterpe St.]]
In 1926 Beth Israel built the "Menorah Institute" [[Talmud Torah]] building on Euterpe Street, adjoining the Carondelet synagogue. The school, which served as an Orthodox alternative to the existing Communal Hebrew School, comprised a [[nursery school]], a [[Hebrew school]], and a Sunday school. The building also housed Beth Israel's offices, and the "Little Shul" ("shul" is the [[Yiddish language|Yiddish]] word for synagogue), where [[Jewish services|services]] were held twice a day.<ref name="ISJLNOLAOrthodox" /><ref name="LachoffKahn2005p67" /><ref name="NOCG1938p384">[[#refNOCG2009|New Orleans City Guide (1938/2009)]], p. 384.</ref>

The congregation leased land for burials in the Chevra Thilim Cemetery on Canal Street, a site that had been used by Tememe Derech as early as 1860, and which was shared with several other congregations. In the 1930s Beth Israel purchased its own cemetery on Frenchmen Street.<ref name="LachoffKahn2005p55">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 55.</ref>

Uri Miller joined Beth Israel as rabbi in 1935, a post he would hold through the early 1940s.<ref name="Feibelman1941p93">[[#refFeibelman1941|Feibelman (1941)]], p. 93.</ref><ref name="Landman1942p172">[[#refLandman1942|Landman (1942)]], p. 172.</ref> He was president of the Hebrew Theological College Alumni from 1936 to 1938, and of its successor the [[Rabbinical Council of America]] from 1946 to 1948.<ref name="Sherman1996">[[#refSherman1996|Sherman (1996)]], p. 257.</ref>

During Miller's tenure the synagogue's neighborhood began to deteriorate. Members started moving uptown, and the congregation embarked on a search for a new location.<ref name="LachoffKahn2005p124">[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 124.</ref><ref name="LachoffKahn2005p67" /> In 1963 Beth Israel purchased a block of land at 7000 Canal Boulevard at Walker Street in the [[Lakeview, New Orleans|Lakeview neighborhood]]. It moved into its new larger building there in 1971.<ref name="LachoffKahn2005p124" /><ref name="ISJLNOLAOrthodox" />

Former [[Ku Klux Klan]] leader [[David Duke]]'s successful 1989 run for a [[Louisiana House of Representatives]] seat in nearby [[Metairie, Louisiana|Metairie]] was a cause for concern in the congregation. Then-rabbi Gavriel Newman spoke out against Duke, feeling that people were being "taken in" by "the modern guise of Klan members of Duke's ilk is the two-piece suit, the blow-dried hair and the sweet smile, which seem to serve them very well to hide the inherent racism and the propensity to violence."<ref name="Maraniss1989">[[#refMaraniss1989|Maraniss (1989)]].</ref><ref name="TulsaWorld19890213">[[#refTulsaWorld19890213|''Tulsa World'', February 13, 1989]].</ref> During the campaign an anti-Duke rally was planned to be held at Beth Israel by [[Mordechai Levy]] of the Jewish Defense Organization (JDO). It was, however, cancelled by Beth Israel, after strong objections by members of Beth Israel and the larger Jewish community, in part because it was felt that the JDO's actions would actually create more support for Duke, and in part because of Levy's statements that the JDO would not rule out violence in its efforts against Duke.<ref name="Franco1989">[[#refFranco1989|Franco (1989)]].</ref><ref name="TulsaWorld19890127">[[#refTulsaWorld19890127|''Tulsa World'', January 27, 1989]].</ref>

From 1914 through [[World War II]] Beth Israel described itself the "largest Orthodox congregation in the South",<ref name="ISJLNOLAOrthodox" /><ref name="LachoffKahn2005p65" /> and in the 1960s it had 500 member families.<ref name="LachoffKahn2005p124" /> By 2005, however, that number had been reduced to fewer than 200.<ref name="Greenberg" /> Nevertheless, Beth Israel still held services twice a day, the only synagogue in New Orleans to do so.<ref name="LachoffKahn2005p124" />

==Hurricane Katrina and aftermath==
[[File:Beth Israel sanctuary.jpg|right|thumb|Interior of the Beth Israel Congregation sanctuary after dewatering]]
As a result of [[Hurricane Katrina]] and the subsequent [[2005 levee failures in Greater New Orleans]], the congregation's building at 7000 Canal Boulevard filled with at least ten feet of water, and Beth Israel garnered national attention after attempts were made to save its [[Torah]] scrolls.<ref name="Greenberg" /> Beth Israel's rabbi, Yisroel Shiff, who had evacuated to Tennessee before Katrina hit, contacted Rabbi Isaac Leider, who had worked on [[ZAKA]] search-and-rescue teams in Israel for five years. After contacting federal officials and the [[Louisiana National Guard]], Leider hired a helicopter to fly him to within a mile of Beth Israel, met with the [[Federal Emergency Management Agency]] (FEMA) search-and-rescue team appointed to retrieve the scrolls. The group used rubber rafts to reach Beth Israel and enter it, where Leider waded into the sanctuary and rescued the Torah scrolls and their silver ornaments.<ref name="Moore">[[#refMoore2005|Moore (2005)]].</ref>

Despite Leider's efforts, all seven Torah scrolls were unsalvageable, and had to be buried. They had initially been buried in her backyard by Rebecca Heggelund, Beth Israel's non-Jewish secretary, who first received them after their rescue, and were subsequently re-buried next to the grave of Beth Israel's ''[[gabbai]]'' Meyer Lachoff.<ref name="OU2006">[[#refOU2006|Orthodox Union Department of Public Relations (March 21, 2006)]].</ref> Lachoff had died just after Katrina,<ref name="Lipman">[[#refLipman2005|Lipman (2005)]].</ref> but could not be buried in New Orleans until months later.<ref name="OU2006" />

[[File:Hayley Fields at the Beth Israel Torah Dedication Ceremony.jpg|left|thumb|Hayley Fields leading new Torah dedication ceremony at Beth Israel Congregation on August 27, 2006]]
In addition to losing all of its Torah scrolls, Beth Israel lost all its furniture, and over 3,000 ''[[siddur]]s'' and ''[[mahzor]]s'', and almost all of its members' homes were flooded, forcing them to move.<ref name="Okun">[[#refOkun2006|Okun (2006)]].</ref> The congregation did, however, receive assistance in replacing some of its assets; the [[Orthodox Union]] immediately sent Beth Israel 50 [[ArtScroll]] ''siddurs'', and Brith Shalom Beth Israel Congregation of [[Charleston, South Carolina]], and Congregation Shaare Zedek Sons of Abraham of [[Providence, Rhode Island]] each donated Torahs.<ref name="OU2006" /> Hayley Fields, a 14-year-old from [[Los Angeles]], heard of Beth Israel's difficult circumstances, and with the support of her mother, spearheaded a fund-raising drive, selling 3,500 watches. $18,000 was ultimately raised to buy a Torah, which was dedicated in August, 2006, two days before the first anniversary of the hurricane.<ref name="Simmons">[[#refSimmons2006|Simmons (2006)]].</ref> At that ceremony the [[National Council of Young Israel]] also donated 150 new Artscoll ''mahzor''s, in time for the High Holy Days.<ref name="Okun" />

In the wake of Katrina 50 families that were members of Beth Israel left New Orleans, and gave up membership in the congregation.<ref name="Greenberg" /> These included Rabbi Schiff and his family. After the flood Beth Israel's board of directors informed Schiff, who had joined they synagogue in April 2002, that it would not be able to meet its contractual obligations and pay him past December. Schiff and his family had also lost their home and much of their possessions, and had been living in Memphis. Schiff resigned effective November 1, 2005, citing this loss, and the lack of a functioning Jewish Day School in the area for his children.<ref name="Smason2006">[[#refSmason2006|Smason (2006)]].</ref><ref name="DSJV2005">[[#refDSJV2005|Deep South Jewish Voice (October 26, 2005)]].</ref>

Soon after the flood, Beth Israel received an offer to temporarily use space from Congregation Gates of Prayer, a [[Reform Judaism|Reform]] synagogue in [[Metairie, Louisiana|Metairie]], a suburb of New Orleans. The congregation began holding weekly services and renting office space there.<ref name="NaronChalew2009">[[#refNaronChalew2009|Naron Chalew (2009)]].</ref>

In 2006 it was unclear if Beth Israel, which had already been in difficult financial shape, would be allowed, or have the means, to re-build its synagogue.<ref name="Luxner">[[#refLuxner2006|Luxner (2006)]].</ref> The building suffered further flood damage in July 2007 when thieves stole the copper tubing for the main air-conditioning system. They broke the water main, and water from the second floor flooded the building for three days, to a depth of three to four feet, before it was discovered. The property was put up for sale.<ref name="Smason2007">[[#refSmason2007|Smason (2007)]].</ref>

==Recent events==

In 2007, the congregation began some joint programming with Anshe Sefard,<ref name="Nolan20070325">[[#refNolan20070325|Nolan (March 25, 2007)]].</ref> New Orleans' only other remaining Orthodox synagogue,<ref name="ISJLNOLAOrthodox" /> and in the summer hired Uri Topolosky as its new rabbi.<ref name="Fausset">[[#refFausset2007|Fausset (2007)]].</ref> A graduate of [[Yeshivat Chovevei Torah]],<ref name="Lipman2007">[[#refLipman2007|Lipman (2007)]].</ref> Topolosky had previously served as the associate rabbi of the [[Hebrew Institute of Riverdale]],<ref name="NaronChalew2007">[[#refNaronChalew2007|Naron Chalew (2007)]].</ref> and had been one of 21 rabbis arrested at the [[United Nations]] during an April 2007 [[sit-in]] demanding that [[Iran]] be expelled from the U.N.<ref name="Traiman">[[#refTraiman2007|Traiman (2007)]].</ref>

[[File:BethIsraelLakeviewNOLAFeb2010Doors.jpg|thumb|215px|The sealed front doors of the Canal Boulevard building in 2010]]
To help attract new members, in the summer Topolosky started a recruitment campaign, placing an advertisement in the New York newspaper ''[[The Jewish Week]]'',<ref name="Fausset" /> and re-designing Beth Israel's website.<ref name="Nolan20070913" /> The campaign's tagline was "If you believe in the ability to destroy, you can believe in the ability to rebuild", a saying of [[Rebbe]] [[Nachman of Breslov]],<ref name="NaronChalew2007" /> and by the end of October ten new members had joined.<ref name="Pfeffer">[[#refPfeffer2007|Pfeffer (2007)]].</ref> However, while all New Orleans synagogues lost membership after Katrina, as of 2007, Beth Israel was the only New Orleans synagogue that had not re-opened in its former location.<ref name="Smason2007" />

Beth Israel started "The Minyan Project" in 2008, an effort to attract 10 (see ''[[minyan]]'') new Orthodox families to New Orleans. The families were given "generous financial assistance", and in return had to "commit to providing community service, from maintaining the [[eruv]] enclosure that's due to be completed within the month to assisting in kosher supervision at a local supermarket." According to Topolosky, with the move of four new families to the area, New Orleans likely had proportionately the fastest growing [[Modern Orthodox Judaism|Modern Orthodox]] community in the United States.<ref name="NaronChalew2008">[[#refNaronChalew2008|Naron Chalew (2008)]].</ref> In April of that year the Orthodox Union gave the congregation $235,000 towards a new building. At the time, Beth Israel had 80 member families, and 80 "associate member" families (families who belonged to other synagogues as well).<ref name="OU2008">[[#refOU2008|Orthodox Union News (May 28, 2008)]].</ref>

By 2009, the congregation had decided to erect a new synagogue on land it had purchased from Gates of Prayer, part of the lot on which the Gates of Prayer synagogue stood.<ref name="NaronChalew2009" /><ref name="TO2009p4">[[#refTO2009|The Temple Observer, Fall 2009]], p. 4.</ref> For the 2009 High Holy Days, Topolosky planned to launch a capital campaign to raise $1 million towards the construction of the building.<ref name="Sosnik2009">[[#refSosnik2009|Sosnick (2009)]].</ref> The new building at 4004 West Esplanade Avenue was completed and occupied in August 2012. The old building, which still stood vacant, had been purchased by a surgeon for use as medical offices. All that was saved from it and brought to the new building were a ''[[Bema#Judaism|bimah]]'' (originally from the building on Carondelet Street), a ''[[Sanctuary lamp#In Jewish tradition|ner tamid]]'', two [[Menorah (Temple)#Modern Jewish use|menorahs]], a ''[[Menorah (Hanukkah)|hanukiah]]'', and "a stained-glass window and a wooden plaque honoring pioneer donor families from Carondelet Street".<ref name="Nolan20120825">[[#refNolan20120825|Nolan (2012)]].</ref> The ''hanukiah'' had previously been featured and lit at the 2010 [[White House Hanukkah Party]].<ref name="Nolan20101202">[[#refNolan20101202|Nolan (2010)]].</ref>

Topolosky and his family left Beth Israel the following year, citing "the deteriorating Jewish educational landscape in New Orleans". New Orleans' only Jewish school, which had been established in 1996, closed for a year after Hurricane Katrina, and was never able to regain its peak enrollment of over 80 students. Although by then New Orleans' Jewish population exceeded its pre-Katrina size, demographic changes negatively affected the school's population. In 2012 the school's board began recruiting non-Jewish students, and changed the name from New Orleans Jewish Day School to Community Day School, but enrollment continued to fall to 29 students, half non-Jewish, by 2013. The same demographic changes affected Beth Israel; its Saturday morning service only attracted around 40 worshipers, its Friday night service only 25, and it had no daily ''[[minyan]]''.<ref name=Heilman2013>[[#refHeilman2013|Heilman (2013)]]</ref>

In 2013 Beth Israel hired Gabriel Greenberg to succeed Topolosky as rabbi, though he could not join the synagogue until the following year. A native of New England, he, like Topolosky, received his rabbinic ordination at Yeshivat Chovevei Torah.<ref name="Rabbi" /><ref name="Heilman2013"/> {{As of|2016}}, Greenberg was Beth Israel's rabbi.<ref name="Rabbi" />

==Notes==
{{reflist|colwidth=35em|refs=
<ref name=Founded>Sources disagree on Beth Israel's founding date:
* [[#refNOCG2009|New Orleans City Guide (1938/2009)]], p. 384 states it was founded on October 25, 1903.
* [[#refHyamsonSilbermann1938|Hyamson & Silbermann (1938)]], p. 462, [[#refFeibelman1941|Feibelman (1941)]], p. 93, and [[#refLandman1942|Landman (1942)]], p. 172 give the founding year as 1903.
* [[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 24, [[#refGreenberg2007|Greenberg (2007)]], and [[#refISJLNOLAOrthodox|Institute of Southern Jewish Life (2010)]] give the founding year as 1904.</ref>
<ref name=Goldberg>[[#refLachoffKahn2005|Lachoff & Kahn (2005)]], p. 97, write that Goldberg joined Beth Israel in 1909 but left within a year to go to Chevra Thilim. [[#refNOCG2009|New Orleans City Guide (1938/2009)]], p.  384 states that Goldberg was Beth Israel's first rabbi. [[#refFeibelman1941|Feibelman (1941)]], p. 93 states that Goldberg served at Chevra Thilim "since 1906".</ref>
}}

==References==
{{refbegin|colwidth=50em}}
*<span id="refHomepage" class="citation">[http://www.bethisraelnola.com// Beth Israel website]. Accessed February 11, 2010.</span>
*<span id="refRabbinicFam" class="citation">[http://www.bethisraelnola.com/rabbinic_family.php Our New Rabbinic Family], Synagogue website. Accessed February 7, 2016.</span>
*<span id="refRabbi" class="citation">[http://bethisraelnola.com/our-rabbi/ Our Rabbi], Synagogue website. Accessed February 7, 2016.</span>
*<span id="refAJYearBookV21" class="citation">[[American Jewish Committee]]. {{cite web|url= http://www.ajcarchives.org/AJC_DATA/Files/1919_1920_5_Directories.pdf |title="Directories" }}&nbsp;{{small|(6.06&nbsp;MB)}}, ''[[American Jewish Year Book]]'', [[Jewish Publication Society]], Volume 21 (1919–1920).</span>
*<span id="refDSJV2005" class="citation">[http://www.dsjv.com/2005/10/deep-south-file-new-orleans-rabbi.html "Deep South File :: New Orleans Rabbi Moves On"], ''Deep South Jewish Voice'', October 26, 2005.</span>
*<span id="refFausset2007" class="citation">Fausset, Richard. [http://articles.latimes.com/2007/jul/19/nation/na-jewish19 "Matzo, meet gumbo - Want to help rebuild a Jewish community in a stricken city?"], ''[[Los Angeles Times]]''. July 19, 2007.</span>
*<span id="refFeibelman1941" class="citation">Feibelman, Julian Beck. ''A social and economic study of the New Orleans Jewish community'', [[University of Pennsylvania]], 1941.</span>
*<span id="refFranco1989" class="citation">Franco, Alberto S. "Synagogue Cancels Anti-Duke Rally", ''[[Associated Press]]'', January 26, 1989.
*<span id="refGreenberg2007" class="citation">Greenberg, Richard. [http://www.washingtonjewishweek.com/main.asp?Search=1&ArticleID=7290&SectionID=4&SubSectionID=4&S=1 "A new start in New Orleans"], ''[[Washington Jewish Week]]'', Jun 13, 2007.</span>
*<span id="refISJLNOLAOrthodox" class="citation">[http://www.isjl.org/history/archive/la/HistoryofOrthodoxCongregations.htm History of Orthodox Congregations in New Orleans], [[Goldring / Woldenberg Institute of Southern Jewish Life]] website. Accessed February 11, 2010.</span>
*<span id="refHeilman2013" class="citation">Heilman, Uriel. [http://www.jta.org/2013/08/26/life-religion/as-school-crumbles-new-orleans-rabbi-uri-topolosky-leaves-city "As school crumbles, New Orleans Rabbi Uri Topolosky leaves city"], ''[[Jewish Telegraphic Agency]]'', August 26, 2013.</span>
*<span id="refHyamsonSilbermann1938" class="citation">[[Albert Montefiore Hyamson|Hyamson, Albert Montefiore]]; Silbermann, Abraham Maurice. ''Vallentine's Jewish Encyclopaedia'', Shapiro, Vallentine, 1938.</span>
*<span id="refLachoffKahn2005" class="citation">Lachoff, Irwin; Kahn, Catherine C. ''The Jewish Community of New Orleans'', [[Arcadia Publishing]], 2005. ISBN 978-0-7385-1835-0</span>
*<span id="refLandman1942" class="citation">[[Isaac Landman|Landman, Isaac]]. "New Orleans", ''The Universal Jewish Encyclopedia'', Volume 8, Universal Jewish Encyclopedia Co. Inc., 1942.</span>
*<span id="refLipman2005" class="citation">Lipman, Steve. "Devastated But Safe In Wake Of Katrina", ''[[The Jewish Week]]'', September 9, 2005.</span>
*<span id="refLipman2007" class="citation">Lipman, Steve. {{cite web|url= http://www.bethisraelnola.com/media/swimmingupstream.pdf |title="Swimming Upstream" }}&nbsp;{{small|(104 KB)}}, ''[[The Jewish Week]]'', October 25, 2007.</span>
*<span id="refLuxner2006" class="citation">Luxner, Larry. [http://www.shalomdc.org/page.aspx?id=58731 "Ruined Synagogue in New Orleans Symbolizes Difficulty of Rebuilding"], ''[[Jewish Telegraphic Agency]]'', January 5, 2006.</span>
*<span id="refMaraniss1989" class="citation">[[David Maraniss|Maraniss, David]]. [https://www.washingtonpost.com/archive/politics/1989/02/14/winning-support-with-a-white-power-image/b57d3505-1257-4b74-af99-84c3ad129fd9/ "WINNING SUPPORT WITH A WHITE-POWER IMAGE"], ''[[The Washington Post]]'', February 14, 1989.</span>
*<span id="refMoore2005" class="citation">Moore, Solomon. [http://articles.latimes.com/2005/sep/14/nation/na-torah14 "Rabbi Navigates Waters to Retrieve Holy Texts"], ''[[Los Angeles Times]]'', September 14, 2005.</span>
*<span id="refNaronChalew2007" class="citation">Naron Chalew, Gail. [http://www.jstandard.com/content/item/From_recovery_to_renaissance/ "From recovery to renaissance"], ''New Jersey Jewish Standard'' (''[[Jewish Telegraphic Agency]]''), August 24, 2007.</span>
*<span id="refNaronChalew2008" class="citation">Naron Chalew, Gail. "Jewish New Orleans undergoing dramatic transformation", ''[[The Jerusalem Post]]'' (''[[Jewish Telegraphic Agency]]''), August 27, 2008.</span>
*<span id="refNaronChalew2009" class="citation">Naron Chalew, Gail. [http://jta.org/news/article/2009/08/31/1007562/unusual-reform-orthodox-partnership-born-of-katrina-blossoms "Unusual Reform-Orthodox partnership born of Katrina blossoms"], ''[[Jewish Telegraphic Agency]]'', August 31, 2009.</span>
*<span id="refNolan20070325" class="citation">Nolan, Bruce. [http://www.lsu.edu/fweil/lsukatrinasurvey/NOTP070325.htm "The Call: The local Jewish federation is launching a recruiting effort to aid New Orleans' recovery"], ''[[The Times-Picayune]]'', March 25, 2007.</span>
*<span id="refNolan20070913" class="citation">Nolan, Bruce. [http://www.bethisraelnola.com/media/orthodox_mission.pdf "Orthodox Mission: A young rabbi tries to revitalize a congregation hurt by Katrina"], ''[[The Times-Picayune]]'', September 13, 2007.</span>
*<span id="refNolan20101202" class="citation">Nolan, Bruce. [http://www.nola.com/news/index.ssf/2010/12/obama_new_orleans_rabbi_and_wh.html "White House Hanukkah ceremony features menorah salvaged from Lakeview"], ''[[The Times-Picayune]]'', December 2, 2010.</span>
*<span id="refNolan20120825" class="citation">Nolan,Bruce. [http://www.nola.com/religion/index.ssf/2012/08/congregation_beth_israel_will.html "Congregation Beth Israel ends 7 years of Hurricane Katrina-induced wandering"], ''[[The Times-Picayune]]'', August 25, 2012.</span>
*<span id="refOkun2006" class="citation">Okun, Adam.[http://www.5tjt.com/news/read.asp?Id=196 "New Orleans Jews: 'We are So Lucky'"]; ''5 Towns Jewish Times'', September 7, 2006.</span>
*<span id="refOU2006" class="citation">[http://www.ou.org/oupr/2006/katrinatorah66.htm "At Burial of Destroyed Torah Scrolls in New Orleans, OU Delivers Words of Comfort And Hears Gratitude for its Post-Katrina Role"], [[Orthodox Union]], Department of Public Relations, March 21, 2006.</span>
*<span id="refOU2008" class="citation">[http://www.ou.org/news/article/as_congregation_beth_israel_of_new_orleans_rebuilds_ou_continues_to_send_su "As Congregation Beth Israel of New Orleans Rebuilds, OU Continues To Send Support"], [[Orthodox Union]], News, May 28, 2008.</span>
*<span id="refPfeffer2007" class="citation">Pfeffer, Anshel. [http://www.bethisraelnola.com/media/haaretz.pdf "New Orleans sees resurgence of Jewish life in Hurricane Katrina aftermath"], ''[[Haaretz]]'', October 28, 2007.</span>
*<span id="refSherman1996" class="citation">Sherman, Moshe D. ''Orthodox Judaism in America: A Biographical Dictionary and Sourcebook'', [[Greenwood Publishing Group]]. 1996.  ISBN 978-0-313-24316-5</span>
*<span id="refSimmons2006" class="citation">Simmons, Ann M. [http://articles.latimes.com/2006/aug/28/nation/na-torah28 "Synagogue Gets `Gift of Renewal’"], ''[[Los Angeles Times]]'', August 28, 2006.</span>
*<span id="refSmason2006" class="citation">Smason, Alan. [http://www.clevelandjewishnews.com/articles/2006/08/25/news/local/ccover0825.txt "Hurricane Katrina: a tale of two rabbis one year later"], ''Cleveland Jewish News'', August 24, 2006.</span>
*<span id="refSmason2007" class="citation">Smason, Alan. [http://www.clevelandjewishnews.com/articles/2007/08/24/news/local/bcover0824.txt "Hurricane Katrina: 2nd anniversary no homecoming"], ''Cleveland Jewish News'', August 24, 2007.</span>
*<span id="refSosnik2009" class="citation">Sosnik, Adam. [http://www.forward.com/articles/114431/ "Public Scandals, Recession Will Be Focus of Many High Holy Day Sermons"], ''[[The Jewish Daily Forward]]'', September 18, 2009.</span>
*<span id="refTO2009" class="citation">[http://starofdavidfoundation.org/newsletters/nl1/nl1-4.html "Present at the Flood"], ''The Temple Observer'', Star of David Foundation, Volume 1, Number 1, Fall 2009.</span>
*<span id="refTraiman2007" class="citation">Traiman, Alex. [http://www.shalomriverdale.org/page.aspx?id=139148 "22 Jewish Leaders Arrested Calling for Removal of Iran at UN"], Riverdale Jewish Community Council website, April 17, 2007.</span>
*<span id="refTulsaWorld19890127" class="citation"> [http://www.tulsaworld.com/news/synagogue-cancels-rally-against-kkk-candidate/article_e5c3601e-0cb5-5f03-b191-cd71ba97507f.html "Synagogue Cancels Rally Against KKK Candidate"], ''[[Tulsa World]]'', January 27, 1989.</span>
*<span id="refTulsaWorld19890213" class="citation"> [http://www.tulsaworld.com/archives/ex-klan-chief-politicking-tulsa-born-duke-in-louisiana-house/article_1cfd3e77-a4ba-5617-a64f-0dfb139ee0eb.html "Ex-Klan chief politicking Tulsa-born Duke in Louisiana House race"], ''[[Tulsa World]]'', February 13, 1989.</span>
*<span id="refNOCG2009" class="citation">Works Progress Administration, ''New Orleans City Guide'', Garrett County Press, 2009 (originally published 1938). ISBN 978-1-891053-08-5 Full text archived [https://archive.org/stream/neworleanscitygu00federich/neworleanscitygu00federich_djvu.txt here].</span>
{{refend}}

==External links==
*[http://www.bethisraelnola.com/ Beth Israel website]

{{good article}}

{{DEFAULTSORT:Beth Israel (New Orleans, Louisiana)}}
[[Category:Buildings and structures in New Orleans]]
[[Category:Culture of New Orleans]]
[[Category:Hurricane Katrina recovery in New Orleans]]
[[Category:Modern Orthodox synagogues in the United States]]
[[Category:Places of worship in New Orleans]]
[[Category:Religious buildings completed in 2012]]
[[Category:Religious organizations established in 1903]]
[[Category:Religious organizations established in 1904]]
[[Category:Synagogues in Louisiana]]