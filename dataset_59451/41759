{|{{Infobox ship begin |infobox caption= |italic title=}} <!-- full code --><!-- caption: yes, nodab, or <caption text> -->
{{Infobox ship image
|Ship image=[[File:SAS Outeniqua A302 d.jpg|300px|SAS ''Outeniqua'' in 1994]]
|Ship caption=SAS ''Outeniqua'' in 1994
}}
{{Infobox ship career
|Hide header=
|Ship country=
|Ship flag=[[Image:Naval Ensign of South Africa.svg|44px|South African Navy ensign]]
|Ship name=*''Aleksandr Sledzyuk'' (1991–1992)
*''Yuvent'' (1991–1992)
*SAS ''Outeniqua'' (1993–2005)
*''Paarderberg'' (2005-2007)
*''Ice Maiden I'' (2007-2013)
|Ship namesake=[[Outeniqua Mountains]]
|Ship owner=
|Ship operator=[[South African Navy]] (1993–2004)
|Ship registry=
|Ship ordered=
|Ship awarded=
|Ship builder=[[Kherson Shipyard]], Ukraine
|Ship original cost=
|Ship yard number=6002
|Ship way number=
|Ship laid down=
|Ship launched=6 September 1991
|Ship sponsor=
|Ship christened=
|Ship completed=
|Ship acquired=
|Ship commissioned=8 June 1993
|Ship recommissioned=
|Ship decommissioned=30 July 2004
|Ship maiden voyage=
|Ship in service=
|Ship out of service=
|Ship renamed=
|Ship reclassified=
|Ship refit=
|Ship struck=
|Ship reinstated=
|Ship homeport=
|Ship identification={{IMO Number|9056894}}
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship honors=
|Ship captured=
|Ship fate=
|Ship status=Broken up 2013
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=
|Ship type=
|Ship tonnage=
|Ship displacement=21,025 tons full load
|Ship tons burthen=
|Ship length={{convert|166.3|m|ft}}
|Ship beam={{convert|22.6|m|ft}}
|Ship height=
|Ship draught=
|Ship draft=
|Ship hold depth=
|Ship decks=
|Ship deck clearance=
|Ship ramps=
|Ship ice class=
|Ship power={{convert|13,200|kW|bhp|abbr=on}}
|Ship propulsion=1 × MAN Burmeister & Wain 8DKRN-60/195 diesel, 1 × propeller
|Ship speed={{convert|17|kn|km/h|mi/h}}
|Ship range={{convert|8000|nmi|km}} at {{convert|15|kn|km/h}}
|Ship endurance=
|Ship test depth=
|Ship boats=
|Ship capacity=*4 × Delta 80 Landing Craft Utility
*10 × vehicles
|Ship troops=Up to 600
|Ship crew=126, including aircrew
|Ship time to activate=
|Ship sensors=2 × Racal I-band radars
|Ship EW=
|Ship armament=Fitted for 2 × [[Oerlikon 20 mm cannon]] and 6 × 12.7mm machine guns
|Ship armour=
|Ship armor=
|Ship aircraft=2 × [[Atlas Oryx]] helicopters
|Ship aircraft facilities=
|Ship notes=Data from<ref name="Janes2004_p669">Saunders (2004), p. 669</ref>
}}
|}

'''SAS ''Outeniqua'' (A 302)''' was a sealift and replenishment ship operated by the [[South African Navy]] between 1993 and 2004. During her operational career she conducted several "flag-showing" cruises to African ports and provided support for South Africa's Antarctic research program. ''Outeniqua'' was also the venue for unsuccessful peace talks between Zaire's President [[Mobuto Sese Seko]] and rebel leader [[Laurent Kabila]] in May 1997.

==Construction and acquisition==
The ship was constructed at the [[Kherson Shipyard]] in [[Ukraine]] as the second Arctic supply vessel of Project 10621, and launched as ''Aleksandr Sledzyuk'' ({{lang-ru|''Александр Следзюк''}}) on 6 September 1991.<ref name="Miramar">{{cite web|title=Yuvent|url=http://www.miramarshipindex.org.nz/ship/show?nameid=318257&shipid=192855|website=Miramar Ship Index (subscription required)|publisher=R B Haworth|accessdate=9 March 2016|location=Wellington NZ}}</ref><ref name="Fleetphoto">{{cite web|title=Class Витус Беринг, проект 10621, тип Иван Папанин|url=http://fleetphoto.ru/projects/1939/|website=Fleetphoto|publisher=Водный транспорт (Water Transport)|accessdate=9 March 2016|language=Russian}}</ref> ''Aleksandr Sledzyuk'''s [[Displacement (ship)|displacement]] was 21,025 tons full load, with dimensions {{convert|166.3|m|ft}} [[length overall]] and {{convert|22.6|m|ft}} [[Beam (nautical)|beam]].<ref name="Janes2004_p669">Saunders (2004), p. 669</ref> She was powered by a single [[MAN B&W Diesel|MAN B&W]] 8DKRN-60/195 diesel producing {{convert|13,200|kW|bhp|abbr=on}}.<ref name="Janes2004_p669" /> She was designed to be capable of breaking through {{convert|1|m|ft}} of ice while travelling at a speed of {{convert|2|kn|km/h}}. ''Aleksandr Sledzyuk'' entered service on 3 April 1992, and was renamed ''Yuvent'' ({{lang-ru|''Ювент''}}) the next day after being delivered to shipping company Aqua Limited of [[Kaliningrad]].<ref name=Janes2004_p669 /><ref name="Miramar" /><ref name="Fleetphoto" />

On 26 February 1993 ''Yuvent'' was purchased by [[Armscor (South Africa)|Armscor]] for [[South African rand|R]]40 million on behalf of the South African Navy to replace the [[SAS Tafelberg|SAS ''Tafelberg'']].<ref name=Janes2004_p669 /><ref name="Baker_149">Baker (2012), p. 149</ref><ref name="Outeniqua sails into the sunset">{{cite news|last1=Salie|title=SAS Outeniqua sails into the sunset|url=http://www.iol.co.za/news/south-africa/sas-outeniqua-sails-into-the-sunset-1.218618#.VDDsp_mSwlI|accessdate=5 October 2014|work=IOL News|date=1 August 2004}}</ref> She was commissioned into the Navy as SAS ''Outeniqua'' on 8 June 1993.<ref name="Flag-Showing Cruises">{{cite web|last1=Wessels|first1=André|title=Flag-Showing Cruises by South African Warships, 1922–2002|url=http://www.navy.mil.za/aboutus/history/ambassadors.htm|publisher=South African Navy|accessdate=5 October 2014}}</ref> In South African service the ship was primarily used to transport vehicles and other heavy equipment. Her secondary roles included acting as a [[replenishment oiler|replenishment tanker]], supporting South Africa's Antarctic research program,  providing search and rescue capabilities and responding to natural disasters.<ref name=Janes2004_p669 /> In 1997 ''Outeniqua'' was reported to be the South African Navy's largest ship.<ref>{{cite news|last1=McNeil|first1=Donald G.|title=Change Is Choppy Sea For a Navy In Africa|url=https://www.nytimes.com/1997/11/02/world/change-is-choppy-sea-for-a-navy-in-africa.html|accessdate=5 October 2014|work=The New York Times|date=2 November 1997}}</ref>

==Operational career==
After entering service, ''Outeniqua'' undertook a "flag-showing" cruise to [[Durban]], [[Majunga]] in Madagascar, [[Moroni, Comoros|Moroni]] in the Comoros, and [[Victoria, Seychelles|Victoria]] in Seychelles between 18 June and 15 July 1993.<ref name="Flag-Showing Cruises" /> On 11 August 1993 she sailed from her home port of [[Simon's Town]], and undertook a voyage in which she delivered agricultural implements to [[Mombasa]] in Kenya and a mobile hospital to [[Trieste]] in Italy. She also visited various ports in the Black Sea and eastern Mediterranean before returning to Simon's Town on 22 October.<ref name="Flag-Showing Cruises" /> During September and October 1994 ''Outeniqua'' delivered food supplies bound for Rwandan refugees to [[Dar es Salaam]] in [[Tanzania]]; this made her the first South African warship to visit the country since 1952.<ref name="Flag-Showing Cruises" /> During 1994 ''Outeniqua'' also underwent a refit in which her flight deck and hangar were modified to allow the ship to operate two [[Atlas Oryx]] helicopters, and she was fitted with [[Underway replenishment|replenishment at sea]] equipment and an armament comprising small calibre cannons and heavy machine guns.<ref name=Janes2004_p669 />

In mid-February 1995 ''Outeniqua'' provided support for a gathering of 1,200 former political prisoners at [[Robben Island]].<ref>{{cite news|title=Burying the Island ghosts|url=http://mg.co.za/article/1995-02-17-burying-the-island-ghosts|accessdate=5 October 2014|work=Mail & Guardian|date=17 February 1995}}</ref> In July that year she formed part of a South African task force of three warships and a submarine which visited [[Maputo]] in Mozambique and Dar es Salaam; during this voyage ''Outeniqua'' hosted a banquet for diplomats and senior Mozambican military officers.<ref name="Flag-Showing Cruises" /> She underwent a refit from May to September 1996.<ref name=Janes2004_p669 />
[[File:SAS Outeniqua A302 c.jpg|thumb|left|250px|''Outeniqua'' in 1994 with a helicopter embarked]]

''Outeniqua'' spent much of May 1997 at [[Pointe Noire]] in the [[Republic of the Congo]] to serve as a venue for peace talks between [[Zaire]]'s President [[Mobuto Sese Seko]] and rebel leader [[Laurent Kabila]] chaired by South African President [[Nelson Mandela]], with the intention of ending the [[First Congo War]]. While talks were held on board the ship on 4 May, Kabila withdrew from a second meeting which was planned for 14 May and little came of the discussions.<ref name="Flag-Showing Cruises" /><ref>Reyntjens (2009), pp. 128–130</ref> Between May and September 1997 she received another refit.<ref name=Janes2004_p669 />

In late 1997 and early 1998 ''Outeniqua'' conducted two voyages to resupply a [[SANAE]] weather reporting team in Antarctica. The ship also visited two Swedish bases in Antarctica, as well as [[South Thule]] and [[Zavodovski Island]] during these missions.<ref name="Flag-Showing Cruises" /> During August and September 1998 ''Outeniqua'' and two minesweepers conducted a flag-showing cruise up the east coast of Africa, and visited Maputo, Dar es Salaam and [[Zanzibar]].<ref name="Flag-Showing Cruises" /> A helicopter handling system was installed during 1998.<ref name=Janes2004_p669 /> During 2000 ''Outeniqua''{{'}}s flight deck and logistic support capabilities were upgraded.<ref name=Janes2004_p669 />

In mid-June 2001 ''Outeniqua'' and the mine hunter [[SAS Umhloti|SAS ''Umhloti'']] sailed to [[Saint Helena]] where their crews restored the graves of South African prisoners of war who had died while being held on the island during the [[Second Boer War]].<ref name="Flag-Showing Cruises" /> During August 2001 ''Outeniqua'' sailed to [[Prince Edward Islands|Marion Island]] to rescue two seriously sick weathermen. During this voyage two teenage stowaways were found on board the ship. Both were citizens of Burundi, and were handed over to immigration authorities when the ship docked at Durban.<ref>{{cite news|last1=Blumenfeld|first1=Bill|title=Marion weathermen, stowaways return to shore|url=http://www.iol.co.za/news/south-africa/marion-weathermen-stowaways-return-to-shore-1.71594?ot=inmsa.ArticlePrintPageLayout.ot|accessdate=5 October 2014|work=IOL News|date=9 August 2001}}</ref> ''Outeniqua'' departed Simon's Town on 10 September that year to participate in a naval review in Australia at the start of October, but this visit did not go ahead as the review was cancelled as a result of the [[September 11 attacks]]. Instead, she visited [[Réunion]] and exercised with the [[French Navy]] before returning to  Simon's Town on 12 October.<ref name="Flag-Showing Cruises" />

''Outeniqua'' took part in another cruise in January and February 2002 when she and the fast attack craft [[SAS Adam Kok|SAS ''Adam Kok'']] visited Dar es Salaam, [[Port of Tanga|Tanga Bay]] and Zanzibar to conduct peace-keeping exercises. During this voyage ''Outeniqua'' embarked the patrol boat [[SAS Tobie|SAS ''Tobie'']] and two [[Namacurra-class harbour patrol boat]]s.<ref name="Flag-Showing Cruises" /> In September 2002 it was reported that ''Outeniqua'' would be used to transport 200 elephants and a large number of other animals from [[Walvis Bay]] in Namibia to [[Luanda]] in Angola during June 2003. This voyage was to form part of a program called "Operation Noah's Ark" which aimed to repopulate [[Quiçama National Park]] following the conclusion of the [[Angolan Civil War]].<ref>James (2004), p. 9</ref><ref>{{cite magazine|last1=Young|first1=Emma|title=Noah's Ark' set to sail for Angola|url=http://www.newscientist.com/article/dn2776-noahs-ark-set-to-sail-for-angola.html#.VDDbKPmSwlI|accessdate=5 October 2014|magazine=New Scientist|date=10 September 2002}}</ref> It is unclear if the voyage was conducted, however. ''Outeniqua'' conducted her second visit to Saint Helena during December 2002, and also docked in Namibia before returning to Simon's Town.<ref>Wessels (2009), p. 26</ref>

During June 2003 ''Outeniqua'' took part in a three-week-long naval exercise which involved eight warships, the submarine [[SAS Assegaai|SAS ''Assegaai'']] and [[South African Air Force]] aircraft.<ref>{{cite news|last1=Steenkamp|first1=Willem|title=SA Navy can still pack a mean punch|url=http://www.iol.co.za/news/south-africa/sa-navy-can-still-pack-a-mean-punch-1.108872#.VDDwI_mSwlI|accessdate=5 October 2014|work=IOL News|date=27 June 2003}}</ref> Also in 2003, the ship's crew repaired facilities at [[Gough Island]].<ref name="SA Navy soon to dispose of SAS Outeniqua">{{cite web|title=SA Navy soon to dispose of SAS Outeniqua|url=http://www.engineeringnews.co.za/article/sa-navy-soon-to-dispose-of-sas-outeniqua-2004-05-17|website=Engineering News|accessdate=5 October 2014|date=17 May 2004}}</ref>

In May 2004 it was reported that ''Outeniqua'' was soon to be decommissioned.<ref name="SA Navy soon to dispose of SAS Outeniqua" /> The Navy stated that the ship was to be removed from service as she was becoming increasingly expensive to operate, and was not suitable for supporting the new [[Valour-class frigate]]s as she was too slow and could not provide them with enough fuel.<ref name="SA Navy soon to dispose of SAS Outeniqua" /><ref name="Navy workhorse bows out" /> ''Outeniqua'' was subsequently decommissioned on 30 July 2004 and offered for sale.<ref name="Navy workhorse bows out">{{cite news|title=Navy workhorse bows out|url=http://mg.co.za/article/2004-07-30-navy-workhorse-bows-out|accessdate=5 October 2014|work=Mail & Guardian|date=30 July 2004}}</ref> At this time, it was reported that she had spent only a third of her operational career at sea.<ref name="Outeniqua sails into the sunset" /> The ship was eventually sold for R40 million.<ref name="SANDF gets rid of surplus">{{cite news|title=SANDF gets rid of surplus|url=http://www.news24.com/SouthAfrica/News/SANDF-gets-rid-of-surplus-20051004|accessdate=6 October 2014|work=News24|date=4 October 2004}}</ref>

==References==
{{Commons category|A302 Outeniqua (ship, 1992)}}

===Citations===
{{Reflist|30em}}

===Works consulted===
*{{cite journal|last1=Baker|first1=Deane-Peter|title=The South African Navy and African Maritime Security|journal=Naval War College Review|date=Spring 2012|volume=65|issue=2|pages=145–165|url=https://www.usnwc.edu/getattachment/7f575130-bee4-466c-b9b6-d64e858d9ba8/The-South-African-Navy-and-African-Maritime-Securi}}
*{{cite book|last1=James|first1=W. Martin|title=Historical Dictionary of Angola|date=2004|publisher=Rowman & Littlefield Publishing Group|location=Lanham|isbn=0810865602}}
*{{cite book|editor1-last=Saunders|editor1-first=Stephen|title=Jane's Fighting Ships 2004–2005|date=2004|publisher=Jane's Information Group|location=Coulsdon, UK|isbn=0710626231}}
*{{cite book|last1=Reyntjens|first1=Filip|title=The Great African War: Congo and Regional Geopolitics, 1996–2006|date=2009|publisher=Cambridge University Press|location=Cambridge|isbn=0521111285}}
*{{cite web|last1=Wessels|first1=André|title=The South African Navy and its Predecessors: A Century of Interaction with Commonwealth Navies|url=http://www.navy.gov.au/sites/default/files/documents/Wessels_-_South_African_Navy_25_May_2009.pdf|publisher=Royal Australian Navy|accessdate=5 October 2014|year=2009}}

{{DEFAULTSORT:Outeniqua}}
[[Category:Ships of the South African Navy]]
[[Category:1991 ships]]
[[Category:Ships built in Ukraine]]