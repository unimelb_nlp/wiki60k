{{Infobox hiking trail
| name =Mike turtur bikeway
| embed =
| cellstyle =
| photo =
| photo_size =
| photo_alt =
| caption =
| title =
| established =
| length ={{convert|9.8|km}}<ref name="MTB"/>
| location =[[Adelaide]], [[South Australia]]
| designation =
| trailheads =[[Glenelg, South Australia|Glenelg]], [[King William Street, Adelaide]]
| use = Cycling, walking
| elev_gain_and_loss =
| elev_change = <!-- Use elev_gain_and_loss if the gain and/or loss is known.
                     Use this field when only the change is known. -->
| highest =
| lowest =
| grade =
| difficulty = Easy
| season = all
| months =
| waymark =
| sights =
| hazards =
| surface = bitumen
| ROW =
| website =
}}
The '''Mike Turtur Bikeway''' is a cycling route adjacent to the [[Glenelg Tram]] route connecting [[Glenelg, South Australia|Glenelg]] to the [[Adelaide city centre]]. It is a popular commuter route for people who live in the inner suburbs southwest of the city and work in or near the city centre.<ref name="DPTI">{{cite web|url=http://dpti.sa.gov.au/news/?a=174995 |title=The One Millionth Rider Pedals The Mike Turtur Bikeway |publisher=[[Department of Planning, Transport and Infrastructure]] |date=19 August 2015 |accessdate=14 January 2016}}</ref> 

The bikeway was named in 2010 in honour of [[Michael Turtur]], a former Olympic cycling gold medallist from Adelaide.<ref name="DPTI"/> {{As of|2016}} it ends at [[South Terrace, Adelaide]] on the edge of the city centre. The [[Government of South Australia|state government]] and [[Adelaide City Council]] are seeking to design a bikeway along [[King William Street, Adelaide|King William Street]] to extend it in to [[Victoria Square, Adelaide|Victoria Square]] in the heart of the city.<ref name="infra">{{cite web |url=http://www.infrastructure.sa.gov.au/major_projects/greenways_project/greenways_project/mike_turtur_bikeway |title=Mike Turtur Bikeway |publisher=[[Department of Planning, Transport and Infrastructure]] |accessdate=14 January 2016}}</ref>

As the bikeway is becoming more popular, a number of infrastructure improvements have been made or proposed to improve the safety and utility of the sections that were poorest. These have included an improved shared path where cyclists previously needed to use a parallel street.<ref name="MTB">{{cite web |url=http://yourviewholdfast.com/miketurturbikeway/documents/10514/download |title=Mike Turtur Bikeway and Sturt River Shared Path |publisher=[[City of Holdfast Bay]] |accessdate=14 January 2016}}</ref> There is also a better crossing of the tram tracks at South Terrace and better crossing of [[Goodwood Road, Adelaide|Goodwood Road]], along with planning to provide a grade-separated crossing of the [[Seaford railway line]] at [[Goodwood railway station, Adelaide|Goodwood railway station]].<ref name="infra"/>

==References==
{{reflist}}

[[Category:Cycling in South Australia]]
[[Category:Cycleways in South Australia]]