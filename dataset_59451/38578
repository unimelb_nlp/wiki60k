{{good article}}
{{Infobox single |
| Name           = Boys & Girls
| Cover          = Boysgirls.PNG
| Alt            = Several images of Hamasaki in one entire square canvas. A large photo of Hamasaki's face is in the centre, whilst 12 small thumbnails are placed around it.
| Artist         = [[Ayumi Hamasaki]]
| Album          = [[Loveppears]]
| Released       = {{start date|1999|7|14}}
| Format         = {{hlist|[[Gramophone record|12" vinyl]]|[[CD single]]|[[Music download|digital download]]}}
| Recorded       = 1999
| Genre          = [[Dance music|Dance]]
| Length         = 3:54
| Label          = {{hlist|[[Avex Trax]]|Avex USA|Avex Entertainment Inc.}}
| Writer         = Ayumi Hamasaki
| Producer       = [[Max Matsuura]]
| Last single    = "[[To Be (Ayumi Hamasaki song)|To Be]]"<br />(1999)
| This single    = "'''Boys & Girls'''"<br />(1999)
| Next single    = "[[A (EP)|A]]"<br />(1999)
}}

"'''Boys & Girls'''" is a song recorded by Japanese recording artist [[Ayumi Hamasaki]], serving as the fourth single for her second studio album, ''[[Loveppears]]'' (1999). It was released by [[Avex Trax]] in Japan and Hong Kong on July 14, 1999, and through Avex USA in North America in early 2001, while being re-distributed in 2003. "Boys & Girls" marks Hamasaki's first single to be made available for purchase as a [[maxi single]] with additional remixes. The track was written by the singer herself, while production was handled by long-time collaborator [[Max Matsuura]]. Musically, the song is a [[dance music|dance]] recording, a genre that heavily influences ''Loveppears''. The single's lyrical content is written in third-person perspective.

Upon its release, "Boys & Girls" received positive reviews from [[Music journalism|music critics]], with some praising the original version of the song, but also acclaiming the different selection of remixes on its physical release. Commercially, the single experienced success in Japan, being subjected to a controversial competition with the song "[[Be Together]]" by Japanese recording artist [[Ami Suzuki]], who eventually outperformed "Boys & Girls" during its first charting week on the [[Oricon Singles Chart]]. It became Hamasaki's first single to sell over one million copies, and was certified double Platinum by the [[Recording Industry Association of Japan]] (RIAJ) for shipments of 800,000 units. 

An accompanying music video was directed by [[Wataru Takeishi]], and featured Hamasaki in an orange-lit room circulated by mysterious light and furniture. In order to promote the single, it appeared on several [[Remix album|remix]] and [[greatest hits]] compilation albums and live concert torus conducted by Hamasaki. "Boys & Girls" was additionally used as the theme song for the cosmetics company Aube, which led to the singer becoming their spokeswoman. To date, the recording is one of the best-selling singles in Japanese music history and remains one of her highest-selling tracks.

==Background and release==
{{Listen
|filename=Ayumi_Hamasaki_-_Boys_&_Girls.ogg
|title="Boys & Girls" (1999)
|description=A 20-second sample of "Boys & Girls", a [[dance music|dance]] recording similar to the content of its parent album.<ref name="Time"/>}}
"Boys & Girls" was written by Hamasaki herself, while production was handled by long-time collaborator [[Max Matsuura]]. The song was composed by [[Dai Nagao]]—who used the alias D.A.I. through production credits and Aube for the maxi single notes—whilst it was arranged by Nagao and Japanese musician Naoto Suzuki. It was mixed by Dave Ford, and mastered by Shigeo Miyamoto.<ref name="albumnotes">{{cite AV media notes |title=Loveppears|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=1999|type=CD Album; Liner notes|publisher=Avex Trax|id=AVCD-11740|location=Japan}}</ref><ref name="single">{{cite AV media notes |title=Boys & Girls|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=1999|type=CD Single; Liner notes|work=Avex Trax|id=AVCD-30049|location=Japan}}</ref> Musically, "Boys & Girls" is a [[dance music|dance]] song, a genre that heavily influences its parent album, ''[[Loveppears]]'' (1999).<ref name="cdjournal"/><ref>{{cite news|author=AllMusic Staff|url=http://www.allmusic.com/album/loveppears-mw0000372208|title=Ayumi Hamasaki – Loveppears|work=[[AllMusic]]|date=April 10, 2001|accessdate=July 14, 2016}}</ref><ref name="Time">{{cite news | last=Takeuchi Cullen| first=Lisa| url=http://www.time.com/time/asia/features/ayumi_hamasaki/cover3.html |title=Empress of Pop|work=[[Time (magazine)|Time]]| accessdate=November 28, 2011 | page=3 | date=March 25, 2002 | archiveurl=https://web.archive.org/web/20020403135213/http://www.time.com/time/asia/features/ayumi_hamasaki/cover3.html | archivedate=April 3, 2002 }}</ref> The recording's instrumentation consists of synthesizers and keyboards managed by Suzuki, while electric guitar is provided by Hidetoshi Suzuki. Programming was handled by Takahiro Iida.<ref name="albumnotes"/> According to the demo sheet music published at [[Ultimate Guitar Archive]], the song is set in [[time signature]] of common time with a tempo of 130 [[beats per minute]]. Its [[chord progression]] develops in E-A-D-G-B-E.<ref>{{cite news|url=https://tabs.ultimate-guitar.com/a/ayumi_hamasaki/boys_and_girls_guitar_pro.htm|title=Boys & Girls Pro Chords|work=[[Ultimate Guitar Archive]]|accessdate=July 28, 2016}}</ref> Lyrically, "Boys & Girls" was written in third person perspective, a trait that is shared with the rest of the album's content.<ref name="specialnotes">{{cite news|author=Hamasaki , Ayumi (interviewee)|url=http://avexnet.or.jp/ayu/en/special/aballads/index2.html|title=Special Website to A Ballads|work=Ayumi Hamasaki's official website|date=March 12, 2003|accessdate=July 16, 2016}}</ref><ref>{{cite journal | title=Loveppears | journal=Beatfreak |date=November 1999 | volume=142|language=ja}}</ref>

"Boys & Girls" served as the fourth single from ''Loveppears''. It was released by [[Avex Trax]] in Japan and Hong Kong on July 14, 1999, and through Avex USA in North America in early 2001, while being re-distributed in 2003.<ref name="single"/><ref name="hongkong">{{cite AV media notes |title=Boys & Girls|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=1999|type=CD Single; Liner notes|work=Avex Trax|id=AVTCDS-190|location=Hong Kong}}</ref> Its CD edition featured a total of ten tracks, seven of which were remixes and its instrumental, plus one remix for both of Hamasaki's previous singles, "[[Love (Destiny)]]" (1999) and "[[To Be (Ayumi Hamasaki song)|To Be]]" (1999).<ref>{{cite AV media notes |title=Love (Destiny)|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=1999|type=CD Single; Liner notes|work=Avex Trax|id=AVCD-30217|location=Japan}}</ref><ref name="albumnotes"/> In early 2001, Avex USA subsequently distributed three [[Gramophone record|12" vinyl]]s; the first two of them contained remixes produced by American disc jockey [[Hex Hector]], whilst the third vinyl, released in on January 24, 2003, contained remixes by [[Junior Vasquez]].<ref name="vinyl">{{cite AV media notes |title=Boys & Girls|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2001|type=12" vinyl; Liner notes|work=Avex USA|id=AVA-1|location=North American}}</ref><ref name="vinyl3">{{cite AV media notes |title=Boys & Girls|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2003|type=12" vinyl; Liner notes|work=Avex USA|id=AVA 13|location=North American}}</ref> The artwork of the physical and digital formats was shot by Toru Kumazawa, and features several images of Hamasaki in one entire square canvas. A large photo of Hamasaki's face is placed in the center, with it being surrounded by 12 small thumbnails.<ref name="single"/>

==Reception==
[[File:Ami-Suzuki-2008.jpg|thumb|180px|left|"Boys & Girls" was commercially outperformed by [[Ami Suzuki]]'s "Be Together" during its first charting week on the [[Oricon Singles Chart]]. However, Hamasaki's single then rose to the top position.]]
Upon its release, "Boys & Girls" received positive reviews from [[Music journalism|music critics]]. A member of ''CD Journal'' was positive towards the original version of the single, but also complimented the remixes featured on its maxi release. The review concluded that each remix was able to "change different moods", and labelled the release of the recording "high-tension".<ref name="cdjournal">{{cite news|author=CD Journal Staff|url=http://artist.cdjournal.com/d/boys--girls/3199050688|title=Ayumi Hamasaki / Boys & Girls [Out of Print]|work=CD Journal|date=July 14, 1999|accessdate=July 28, 2016|language=Japanese}}</ref> [[AllMusic]]'s Alexey Eremenko, who contributed in writing Hamasaki's biography on the website, selected the track as one of her best work.<ref>{{cite news|author=Eremenko, Alexey|url=http://www.allmusic.com/artist/ayumi-hamasaki-mn0000758915/songs|title=Ayumi Hamasaki – Songs|work=[[AllMusic]]|date=2016|accessdate=July 14, 2016}}</ref> 

"Boys & Girls" was subjected to controversy by Japanese media at the time of its release, mainly due to it interspersing with the premiere of Japanese recording artist [[Ami Suzuki]]'s "[[Be Together]]", which was perceived as a direct competition between Suzuki's label, [[Sony Music]], and Hamasaki's label, Avex Trax, in order to score the highest entry on the [[Oricon Singles Chart]].<ref name="aramajapan">{{cite news|author=Arama Japan Staff|url=http://aramajapan.com/news/music/featured-artist-ami-suzuki-2/60844/|title=Featured Artist: Ami Suzuki|work=Arama Japan|date=2016|accessdate=July 28, 2016}}</ref> As a result, Suzuki's single topped the Oricon Singles Chart with 317,610 units sold, whilst Hamasaki's recording debuted at number two on that chart, selling 261,750 copies in its first week of availability.<ref name="aramajapan"/> The following week, "Boys & Girls" replaced "Be Together" at the top spot, becoming the singer's second single to peak atop after "Love (Destiny)" in May 1999.<ref name="aramajapan"/> The recording spent 17 weeks within the top 200, marking one of Hamasaki's longest-spanning songs on the chart.<ref name="week">{{cite web|url=http://www.oricon.co.jp/prof/artist/246497/products/music/45105/1/|title=Boys & Girls – Ayumi Hamasaki|work=[[Oricon Style]]|date=July 14, 1999|accessdate=July 28, 2016|language=Japanese|archiveurl=https://web.archive.org/web/20120924064108/http://www.oricon.co.jp/prof/artist/246497/products/music/45105/1/|archivedate=September 24, 2012}}</ref> Similarly, it debuted at number two on the [[Count Down TV]] chart hosted by [[Tokyo Broadcasting System]] (TBS), behind Suzuki's "Be Together".<ref>{{cite web|url=http://www.tbs.co.jp/cdtv/cddb/countdown19990724-j.html|title=CDTV PowerWeb! Ranking Singles Chart|work=[[Count Down TV]]; published through [[Tokyo Broadcasting System]] (TBS)|date=July 24, 1999|accessdate=July 28, 2016|language=Japanese|archiveurl=https://web.archive.org/web/20080628233558/http://www.tbs.co.jp/cdtv/cddb/countdown19990724-j.html|archivedate=June 28, 2008 }}</ref> The following week, it reached number one and stayed there for three consecutive weeks, with it lasting 16 weeks within the top 100.<ref name="tbs">{{cite web|url=http://www.tbs.co.jp/cdtv/songdb/song3959-j.html|title=CDTV PowerWeb! Ayumi Hamasaki – Boys & Girls|work=Count Down TV; published through Tokyo Broadcasting System (TBS)|date=July 31, 1999|accessdate=July 28, 2016|language=Japanese|archiveurl=https://web.archive.org/web/20080624234630/http://www.tbs.co.jp/cdtv/songdb/song3959-j.html|archivedate=June 24, 2008 }}</ref> In 2008, the single charted at number 33 on ''[[Billboard (magazine)|Billboard]]'''s Adult Alternative Radio chart in Japan.<ref>{{cite web | title=Japan Billboard Hot 100 | url=http://www.billboard-japan.com/charts/detail?a=adult_airplay&year=2008&month=09&day=29 | work=[[Billboard (magazine)|Billboard]] |language=Japanese | date=March 24, 2008 | accessdate=August 17, 2014}}</ref>

By the end of 1999, "Boys & Girls" had sold over 1,037,950 units in Japan, thus being ranked at number 11 on Oricon's Annual 1999 chart.<ref name="yearly">{{cite news|url=http://www.musictvprogram.com/corner-ranking-1999.html|title=Oricon Yearly Chart – Singles, Albums, and DVD Releases of 1999|work=Music TV Program Japan|date=December 1999|accessdate=July 28, 2016|language=Japanese}}</ref> Likewise, it charted at number 12 on TBS' Annual Chart.<ref name="yearly2">{{cite web|url=http://www.tbs.co.jp/cdtv/cddb/countdown1999total-j.html|title=CDTV PowerWeb! 1999 Annual Singles Chart|work=Count Down TV; published through Tokyo Broadcasting System (TBS)|date=1999|accessdate=July 16, 2016|language=Japanese|archiveurl=https://web.archive.org/web/20061009100833/http://www.tbs.co.jp/cdtv/cddb/countdown1999total-j.html|archivedate=October 9, 2006 }}</ref> In October 1999, the single was certified double Platinum by the [[Recording Industry Association of Japan]] (RIAJ) for shipments of 800,000 units.<ref name="RIAJ-oct1999">{{cite journal |title=GOLD ALBUM 他認定作品 1999年8月度 |trans_title=Gold Albums, and other certified works. August 1999 Edition |url=http://www.riaj.or.jp/issue/record/1999/199910.pdf |format=PDF | journal=The Record |type=Bulletin |language=Japanese |location=[[Chūō, Tokyo]] |publisher=[[Recording Industry Association of Japan]] |publication-date=October 10, 1999 |volume=479 |page=9 |archiveurl=https://web.archive.org/web/20131102225853/http://www.riaj.or.jp/issue/record/1999/199910.pdf |archivedate=November 2, 2013 |accessdate=January 18, 2014}}</ref> "Boys & Girls" remains the 225th best-selling single in Japanese music history, and as of July 2016 the singer's fourth highest-selling song according to [[Oricon Style]]'s database.<ref>{{cite news|url=http://www.oricon.co.jp/prof/246497/rank/single/p/3/|title=Ayumi Hamasaki Single's Ranking|work=Oricon Style|accessdate=July 16, 2016|language=Japanese}}</ref><ref>{{cite web|url=http://www.musictvprogram.com/corner-ranking-single.html|title=Best Selling Singles in Japan of All Time|work=Music TV Program Japan|accessdate=July 28, 2016|language=Japanese}}</ref>

==Music video and promotion==
An accompanying music video for the single was directed by [[Wataru Takeishi]].<ref name="video">{{cite news|author=Takeishi, Wataru (director)|url=https://www.youtube.com/watch?v=AFDg1GVY_Ug|title=Boys & Girls / Ayumi Hamasaki (short music video)|work=Avex Trax; published through [[YouTube]]|date=November 10, 2015|accessdate=July 28, 2016|language=Japanese}}</ref> It opens with Hamasaki walking into a small orange-lit room with a black object on a stall. After touching it, the entire room starts to light up, and the singer subsequently walks around singing to the song; scenes are digitally-altered and edited during the process. Following the first chorus, Hamasaki is seen wearing a white outfit while a mysterious light shines around the room. The video subsequently ends with the singer sitting on a small chair, looking in the distance.<ref name="video"/> The clip was included on several DVD compilations released by Hamasaki: ''A Clips'' (2000),<ref>{{cite AV media notes |title=A Clips|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=DVD Videos; Liner notes|publisher=Avex Trax|id=AVVD-90069|location=Japan}}</ref> ''A Complete Box Set'' (2004),<ref>{{cite AV media notes |title=A Complete Box Set|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2004|type=DVD Videos; Liner notes|publisher=Avex Trax|id=AVBD-91181~3|location=Japan}}</ref> the digital release of ''A Clips Complete'' (2014),<ref>{{cite web|url=https://itunes.apple.com/nz/album/a-clip-box-1998-2011-vol.1/id655484415|title=A Clip Box 1998-2011, Vol. 1 – Album – by Ayumi Hamasaki|work=iTunes Store (New Zealand)|date=January 1, 2012|accessdate=July 16, 2016}}</ref> and the DVD and Blu-Ray re-release edition of her 2001 compilation album, ''[[A Best]]'' (2016).<ref>{{cite AV media notes |title=A Best: 15th Anniversary Edition|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2016|type=CD + DVD + Blu-Ray; Liner notes|publisher=Avex Trax|id=AVZD-93409/B~C|location=Japan}}</ref> The visual was additionally used as the theme song for Japanese cosmetics company Aube's launch of their lipstick range, which led to Hamasaki becoming their spokeswoman and appearing in a campaign video.<ref name="cdjournal"/> 

"Boys & Girls" has been heavily promoted on compilation albums conducted by Hamasaki. It has been included on 11 of Hamasaki's remix albums, including ''[[Super Eurobeat Presents Ayu-ro Mix]]'' and ''[[Ayu-mi-x II Version Non-Stop Mega Mix]]'' (2001).<ref>The list below are the compilations that "Fly High" has been featured in;
*{{cite AV media notes |title=Super Eurobeat Presents Ayu-ro Mix|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVTCD-95326|location=Japan}}
*{{cite AV media notes |title=Ayu-mi-x II Version US+EU|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-11797|location=Japan}}
*{{cite AV media notes |title=Ayu-mi-x II Version JPN|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-11798|location=Japan}}
*{{cite AV media notes |title=Ayu-mi-x II Version Acoustic Orchestral|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-11799|location=Japan}}
*{{cite AV media notes |title=Ayu-mi-x II Version Non-Stop Mega Mix|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-11800~1|location=Japan}}
*{{cite AV media notes |title=Ayu Trance|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2001|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-17028|location=Japan}}
*{{cite AV media notes |title=Cyber Trance Presents Ayu Trance 2|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2002|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-17200|location=Japan}}
*{{cite AV media notes |title=Ayu-mi-x 4 + Selection Acoustic Orchestra Version|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2002|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-17098|location=Japan}}
*{{cite AV media notes |title=Super Eurobeat Presents Ayu-ro Mix 2|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2001|type=Remix Album; Liner notes|publisher=Avex Trax|id=AVCD-17027|location=Japan}}</ref> The single has also been featured on three of Hamasaki's [[greatest hits]] albums, ''A Best'' (2001), ''[[A Complete: All Singles]]'' (2007), and ''[[A Summer Best]]'' (2012).<ref>{{cite AV media notes |title=A Best|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2001|type=CD; Liner notes|publisher=Avex Trax|id=AVCD-11950|location=Japan}}</ref><ref>{{cite AV media notes |title=A Ballads|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2007|type=3xCD Compilation Album; Liner notes|publisher=Avex Trax|id=AVCD-23676~8|location=Japan}}</ref><ref>{{cite AV media notes |title=A Summer Best|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2012|type=2xCD; Liner notes|publisher=Avex Trax|id=AVCD-38560~1|location=Japan}}</ref> Additionally, it was specially remixed by Junior Vasquez and Hex Hector in order to be added onto her remix extended plays ''[[The Other Side One: Hex Hector]]'' (2001) and ''[[The Other Side Two: Junior Vasquez]]'' (2001).<ref>{{cite AV media notes |title=The Other Side One: Hex Hector|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2001|type=Remix EP; Liner notes|publisher=Rhythm Republic|id=RRCD-85232|location=Japan}}</ref><ref>{{cite AV media notes |title=The Other Side Two: Junior Vasquez|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2001|type=Remix EP; Liner notes|publisher=Rhythm Republic|id=RRCD-85233|location=Japan}}</ref> The song has been included on three of the singer's major concert tours, including her [[Ayumi Hamasaki Concert Tour 2000 Vol. 1|part one]] and [[Ayumi Hamasaki Concert Tour 2000 Vol. 2|part two]] of her 2000 concert tour,<ref>{{cite AV media notes |title=Ayumi Hamasaki Concert Tour 2000 Vol. 1|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Live DVD; Liner notes|publisher=Avex Trax|id=AVBD-91022|location=Japan}}</ref><ref>{{cite AV media notes |title=Ayumi Hamasaki Concert Tour 2000 Vol. 2|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Live DVD; Liner notes|publisher=Avex Trax|id=AVBD-91021|location=Japan}}</ref> her 2000–2001 Countdown live show,<ref>{{cite AV media notes |title=Countdown Live 2000-2001 A|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2000|type=Live DVD; Liner notes|publisher=Avex Trax|id=AVBD-91060|location=Japan}}</ref> her 2006 (Miss)Understood tour,<ref>{{cite AV media notes |title=Arena Tour 2006 A: (Miss)understood|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2006|type=Live DVD; Liner notes|publisher=Avex Trax|id=AVBD-91452~4|location=Japan}}</ref> and her 2011 Hotel Love Songs concert tour.<ref>{{cite AV media notes |title=Party Queen|others=Ayumi Hamasaki|first=Ayumi|last=Hamasaki|year=2012|type=2xCD + DVD; Liner notes|publisher=Avex Trax|id=AVCD-38511/B~C|location=Japan}}</ref>

==Track listing==
{{col-begin}}
{{col-2}}
*'''CD single'''<ref name="single"/><ref name="hongkong"/>
# "Boys & Girls" (Mad Filter Mix) – 6:54
# "Boys & Girls" (Aube Original Mix) – 3:56
# "Boys & Girls" (Higher Uplift Mix) – 9:50
# "Love (Destiny)" (Todd's Lovers Conversion) – 6:03
# "Boys & Girls" (HΛL's Mix) – 4:59
# "Boys & Girls" (Melt Down Dub Mix) – 4:47
# "To Be" (Bright Mix) – 5:45
# "Boys & Girls" (D-Z Psychedelic Assassin Mix) – 5:09
# "Boys & Girls" (Dub's Club Remix) – 7:26
# "Boys & Girls" (Aube Original Mix) [Instrumental] – 3:53 

*'''Hex Hector 12" vinyl (part 1)'''<ref name="vinyl"/>
# "Boys & Girls" (Hex Hector Main Club Mix) – 9:15
# "Boys & Girls" (Hex Hector Dub) – 9:07
{{col-2}}
*'''Digital download'''<ref name="itunes">{{cite web|url=https://itunes.apple.com/jp/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Japan)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
# "Boys & Girls" (Mad Filter Mix) – 6:54
# "Boys & Girls" (Aube Original Mix) – 3:56
# "Boys & Girls" (Higher Uplift Mix) – 9:50
# "Love (Destiny)" (Todd's Lovers Conversion) – 6:03
# "Boys & Girls" (HΛL's Mix) – 4:59
# "Boys & Girls" (Melt Down Dub Mix) – 4:47
# "To Be" (Bright Mix) – 5:45
# "Boys & Girls" (D-Z Psychedelic Assassin Mix) – 5:09
# "Boys & Girls" (Dub's Club Remix) – 7:26
# "Boys & Girls" (Aube Original Mix) [Instrumental] – 3:53 

*'''Junior Vasquez 12" vinyl'''<ref name="vinyl3"/>
# "Boys & Girls" (Junior's Club Mix) – 8:57
# "Boys & Girls" (Junior's Club Dub) – 8:57
# "Boys & Girls" (Junior's Boys & Girls Beats) – 6:15
{{col-end}}

==Credits and personnel==
Credits adapted from the single's CD release.<ref name="single"/>

;'''Recording'''
*Recorded at Prime Sound Studio, Studio Sound Dali, Onkio Haus, [[Tokyo, Japan]] in 1999.

;'''Technical and songwriting credits'''
*[[Ayumi Hamasaki]] – vocals, songwriting, background vocals
*[[Max Matsuura]] – production
*[[Dai Nagao]] – composing, arranging
*Naoto Suzuki – sound producing, arranging, keyboards, synthesizers
*Hidetoshi Suzuki – electric guitar
*Shigeo Miyamoto – mastering
*Takahiro Iida – programming
*[[Wataru Takeishi]] – music video director
*Dave Ford – mixing

==Charts==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable plainrowheaders"
!Chart (1999)
!Peak<br>position
|-
!scope="row"|Japan ([[Oricon]])<ref name="week"/>
| style="text-align:center;"|1
|-
!scope="row"|Japan ([[Count Down TV]])<ref name="tbs"/>
| style="text-align:center;"|1
|-
|}
{{col-2}}

===Yearly chart===
{| class="wikitable plainrowheaders"
!Chart (1999)
!Peak<br>position
|-
!scope="row"|Japan ([[Oricon]])<ref name="yearly"/>
| style="text-align:center;"|11
|-
!scope="row"|Japan ([[Count Down TV]])<ref name="yearly2"/>
| style="text-align:center;"|12
|}
{{col-end}}

==Certification and sales==
{{Certification Table Top}}
{{Certification Table Entry|region=Japan ([[Recording Industry Association of Japan|RIAJ]])|autocat=no|award=Platinum|number=2|certref=<ref name="RIAJ-oct1999"/>|salesamount=1,037,950|salesref=<ref name="OriconSales">{{cite web | title=オリコンランキング情報サービス「you大樹」 | trans_title=Oricon Ranking Information Service 'You Big Tree' | url=http://ranking.oricon.co.jp | work=[[Oricon]] |subscription=yes | accessdate=August 26, 2014}}</ref>|certyear=1999}}
{{Certification Table Bottom}}

==Release history==
{| class="wikitable plainrowheaders"
|-
!scope="col"|Region
!scope="col"|Date
!scope="col"|Format
!scope="col"|Label
|-
!scope="row"|Japan<ref name="single"/>
|rowspan="2"|July 14, 1999
|rowspan="2"|CD single
|rowspan="2"|{{hlist|Avex Trax|Avex Entertainment Inc.}}
|-
!scope="row"|Hong Kong<ref name="hongkong"/>
|-
!scope="row" rowspan="2"|North America<ref name="vinyl"/>
|rowspan="2"|January 24, 2000
|Hex Hector 12" vinyl
|rowspan="2"|Avex USA
|-
|Junior Vazquez 12" vinyl
|-
!scope="row"|Japan<ref name="itunes"/>
|rowspan="10"|September 2008{{efn|group=upper-alpha|There is currently insufficient evidence to determine what date Hamasaki's digital releases appeared on online retails. The closest source for an [[iTunes Store]] evidence is at Jame World, who confirmed Hamasaki's work was released worldwide on iTunes in September 2008.<ref>{{cite web|url=http://www.jame-world.com/us/news-32267-avex-artists-available-at-us-itunes-store.html|title=Avex Artists Available at US iTunes Store|publisher=Jame World|date=September 5, 2008|accessdate=April 1, 2016}}</ref>}}
|rowspan="10"|Digital download
|{{hlist|Avex Trax|Avex Entertainment Inc.}}
|-
!scope="row"|Australia<ref>{{cite web|url=https://itunes.apple.com/au/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Australia)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|rowspan="9"|Avex Entertainment Inc.
|-
!scope="row"|New Zealand<ref>{{cite web|url=https://itunes.apple.com/nz/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (New Zealand)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|United Kingdom<ref>{{cite web|url=https://itunes.apple.com/gb/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (United Kingdom)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|Ireland<ref>{{cite web|url=https://itunes.apple.com/ie/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Ireland)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|Germany<ref>{{cite web|url=https://itunes.apple.com/de/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Germany)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|Spain<ref>{{cite web|url=https://itunes.apple.com/es/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Spain)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|France<ref>{{cite web|url=https://itunes.apple.com/fr/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (France)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|Italy<ref>{{cite web|url=https://itunes.apple.com/it/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Italy)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
!scope="row"|Taiwan<ref>{{cite web|url=https://itunes.apple.com/tw/album/boys-girls/id75642898|title=Boys & Girls – Album – by Ayumi Hamasaki|work=iTunes Store (Taiwan)|date=July 14, 1999|accessdate=July 28, 2016}}</ref>
|-
|}

==See also==
*[[List of Oricon number-one singles|List of Oricon number-one singles of 1999]]

==Notes==
{{notelist-ua}}

==References==
{{reflist|2}}

==External links==
*[http://www.avex.jp/ayu/discography/detail.php?id=1003292 "Boys & Girls"] at Ayumi Hamasaki's official website. {{ja icon}}
*[https://web.archive.org/web/20120924064108/http://www.oricon.co.jp/prof/artist/246497/products/music/45105/1/ "Boys & Girls"] at [[Oricon Style]]. {{ja icon}}

{{Ayumi Hamasaki singles}}

{{DEFAULTSORT:Boys and Girls (Ayumi Hamasaki Song)}}
[[Category:Ayumi Hamasaki songs]]
[[Category:1999 singles]]
[[Category:Oricon Weekly number-one singles]]
[[Category:Songs written by Ayumi Hamasaki]]
[[Category:Songs written by Dai Nagao]]
[[Category:1999 songs]]
[[Category:Song recordings produced by Max Matsuura]]
[[Category:Avex Trax singles]]