'''R. Ross Holloway''' (in full, Robert Ross Holloway) <ref>[http://www.worldcat.org/wcidentities/lccn-n50-32664 Library of Congress name authority file]</ref>  (born August 15, 1934) is an American archaeologist, founder of the independent center for Mediterranean archaeology at [[Brown University]], and the Elisha Benjamin Andrews Professor Emeritus of Brown University, where he taught from 1964 to his retirement in 2006.

== Biography and education ==

Holloway graduated from the [[Roxbury Latin School]] and [[Amherst College]] (summa cum laude).  He took his Ph.D. at [[Princeton University]]  in 1960, with a thesis "The elder turtles of Aigina".<ref name=wct>[http://www.worldcat.org/title/elder-turtles-of-aigina/oclc/12873574&referer=brief_results   WorldCat]</ref>

== Academic career ==

Holloway joined  Brown University in 1964, and rose to become the Elisha Benjamin Andrews Professor Emeritus at his retirement.
His research interests include Greek and Roman numismatics, Greek art and architecture, the archaeology of ancient Rome and the history of the Early Roman Republic.

Throughout his career, Holloway has used archaeology not merely to support early published texts. but to develop and write history, whether or not it agrees   with the literary history.  As a reviewer of his work on early Rome and Latium writes
:[t]he reader will find out that this "archaeological history" of the ''primordia'' has scarcely any links with the records of Livy and of Dionysius of Halicarnassus. The "traditionalists", i.e. the ones who systematically and excessively interpret archaeological discoveries in full confidence of the global historicity of ancient records, might be somewhat upset, but in the field of history too, a face to face confrontation with the truth is much more advisable.<ref>Poucet, Jacques, Bryn Mawr Classical Review 95.03.08, http://bmcr.brynmawr.edu/1995/95.03.08.html,</ref>
Another reviewer of his work on Constantine similarly indicates that the work was based on a body of evidence different from the traditional text-based studies:
:Who was Constantine? A religious opportunist? A benevolent but autocratic patron of Christianity? An intolerant despot? All these characterizations have been cogently proposed on the basis of scholarly analysis of documentary evidence and the sifting of historical data derived from textual research. This new book looks at a different body of evidence and captures the character of the man and his era from another angle—the buildings and monuments he erected in the city of Rome.<ref>Jensen, Robin Margaret, ''Journal of Early Christian Studies,'' vol. 13, no. 3, Fall 2005: 403-405.</ref>

Holloway's field work has centered on [[Italy]] and [[Sicily]] in the [[Early Bronze Age|Early]] and [[Middle Bronze Age]].  The [[radiocarbon]] dates from his excavations led to a shift of almost five centuries in Early Bronze Age chronology in this area, while the study of the Early Bronze Age blades from [[Buccino]] (Salerno) was one of the first to document the use of arsenic as a hardening agent in   early bronze metallurgy.<ref>Buccino, The Eneolithic Necropolis of San Antonio and Other Prehistoric Discoveries made by Brown University in 1968 and 1969, De Luca (Rome) 1973.</ref>   On the island of [[Ustica]] (74&nbsp;km  north of [[Palermo]]) his excavation of the citadel, the most perfectly preserved fortification of the Bronze Age in Italy or Sicily, discovered the first evidence of native stone sculpture in the same area.<ref>Ustica II, The Results of the Excavations of the Regione Siciliana, Soprintendenza ai Beni Culturali ed Ambientali Provincia di Palermo in collaboration with Brown University in 1994 and 1999, with Susan S. Lukesh.  Archaeologia Transatlantica XIX (Providence) 2001.</ref>  At the site of La Muculufa, [[Butera]] (slightly inland from the south coast of Sicily) he discovered a federal sanctuary of the Early Bronze Age, the first to be documented.<ref>La Muculufa, The Early Bronze Age Sanctuary: The Early Bronze Age Village (Excavations of 1982 and 1983), with M. S. Joukowsky, J. Léon and S. S. Lukesh (Providence and Louvain) 1990.</ref>

A Festschrift, ''Koine : Mediterranean studies in honor of R. Ross Holloway'' was published in his honor in  2009.<ref name=wcf>Counts, Derek B., Anthony S. Tuck, and R. Ross Holloway. ''Koine: Mediterranean Studies in Honor of R. Ross Holloway.'' Oxford: Oxbow Books, 2009. ISBN 9781842173794  [http://www.worldcat.org/title/koine-mediterranean-studies-in-honor-of-r-ross-holloway/oclc/320801538&referer=brief_results   WorldCat]</ref>     The editor's preface summarizes his career:
:Holloway's recognition of the significance of traditionally underappreciated indigenous cultures within the wider milieu of trans-Italian and trans-Mediterranean contact has provided a framework within which many later studies have been situated (e.g., the contributions by Hussein, Tsakirgis, and McConnell in this volume). Today, such thinking is central to innumerable discussions of economic, demographic and social complexity of the Mediterranean. And yet, the very ubiquity of this approach today underscores its profound and revolutionary impact at a time when the idea of the classical world was far more narrowly defined than it is now.<ref>Count, Derek and Anthony Tuck, “Excavating the Labyrinth: An Archaeology of a Career," in Derek B. Counts and Anthony S. Tuck, eds., ''Koine, Mediterranean Studies in Honor of R. Ross Holloway,'' (Joukowsky Institute Publication 1), Oxford and Oakville, 2009: xxii.</ref> The introduction says that his study of numismatics
:has stressed [as well] both the skill of numismatic craft and the achievement of the craftsman for objects cast aside as mass produced and thus unable to speak to the individual.<ref>Count, Derek and Anthony Tuck, “Excavating the Labyrinth: An Archaeology of a Career, “ in Derek B. Counts and Anthony S. Tuck, eds., Koine, Mediterranean Studies in Honor of R. Ross Holloway, (Joukowsky Institute Publication 1), Oxford and Oakville, 2009: xxiii.</ref>

In his academic career at Brown Holloway was instrumental in creating an independent home for the archaeology of the classical lands of the Mediterranean in the Center for Old World Archaeology and Art (COWA), now succeeded by the [[Joukowsky Institute for Archaeology and the Ancient World]]. In 1981, together with Prof. Tony Hackens of the Catholic University of Louvain (Belgium), he founded the series Archaeologia Transatlantica which reached 22 volumes.  This was replaced in 2009, at Brown by, the Joukowsky Institute Publications.

== Awards and recognition ==

Holloway received the Gold Medal of the [[Archaeological Institute of America]] in 1995,<ref>http://www.archaeological.org/rrossholloway%E2%80%941995goldmedalawarddistinguishedarchaeologicalachievement</ref>

He holds honorary doctorates from Amherst and the [[Université catholique de Louvain|Catholic University of Louvain]] and is a corresponding member of the [[German Archaeological Institute]], an honorary member of the Royal Belgian Numismatic Society, fellow of the [[Royal Numismatic Society]] (London), fellow of the [[American Academy in Rome]], foreign member of the Italian Institute of Prehistoric and Protohistoric Studies (Florence), foreign member of the National Institute of Italic and Etruscan Studies (Florence).

== Bibliography ==

=== Archaeology of Italy and Sicily ===

*''Italy and the Aegean: 3000-700 B.C.,'' ("Archaeologia Transatlantica" ; 1), (Providence and Louvain), 1982. OCLC 8844116
*''The Archaeology of Ancient Sicily,'' Routledge (London,) 1991. ISBN 9780415019095
**Translated into Italian  as ''Archeologia della Sicilia antica'', 1995. ISBN 9788805054466
*''The Archaeology of Early Rome and Latium,'' Routledge (London), 1994. ISBN 9780415143608
*''Satrianum, The Archaeological Investigations Conducted by Brown University in 1966 and 1967,'' Brown University Press (Providence), 1970. ISBN 9780870571183
*''Buccino, The Eneolithic Necropolis of San Antonio and Other Prehistoric Discoveries made by Brown University in 1968 and 1969,'' De Luca (Rome), 1973. OCLC 1056005
*"Buccino, The Early Bronze Age Village of Tufariello," with N. P. Nabers, S. S. Lukesh, E. R. Eaton, N. B. Hartmann, G. Barker, H. McKerrall, W. L. Phippen and G. Leuci, ''Journal of Field Archaeology,'' vol. 2, 1975, pp.&nbsp;11–81.
*"La Muculufa, The Early Bronze Age Sanctuary: The Early Bronze Age Village (Excavations of 1982 and 1983)," ''Revue des Archéologues et Historiens d’Art de Louvain,'' 22, 1990: 11-67,  with M. S. Joukowsky, J. Léon and S. S. Lukesh (Providence and Louvain) 1990.
*"La Muculufa II Excavation and Survey 1988-1991 The Castelluccian Village and Other Areas," editor with T. Hackens, ("Archaeologia Transatlantica" 12), Providence and Louvain, 1995.
*"Ustica I, The Results of the Excavations of the Regione Siciliana, Soprintendenza ai Beni Culturali ed Ambientali Provincia di Palermo in collaboration with Brown University in 1990 and 1991," with Susan S. Lukesh and other contributors, ("Archaeologia Transatlantica" ; 14) Providence and Louvain, 1995. OCLC 34671592
*"Ustica II, The Results of the Excavations of the Regione Siciliana, Soprintendenza ai Beni Culturali ed Ambientali Provincia di Palermo in collaboration with Brown University in 1994 and 1999," with Susan S. Lukesh,  ("Archaeologia Transatlantica" ; 19 (Providence) 2001. OCLC 47975697

=== Ancient art ===

*''A View of Greek Art'', Brown University Press (Providence), 1973. ISBN 9780870571336 <ref>Review, by Hans Frenz, ''Gnomon,'' v50 n2 (Apr., 1978): 217-219</ref>
*''Influences and Styles in the Late Archaic and Early Classical Greek Sculpture of Sicily and Magna Graecia,'' Catholic University of Louvain, Institute of Archaeology and Art History, Monographs, 1975. OCLC 2276760
*''Constantine and Rome,'' Yale University Press, New Haven and London, 2004 ISBN 9780300100433.    According to WorldCat, the book is held in  920  libraries <ref>[http://www.worldcat.org/title/constantine-rome/oclc/52902430&referer=brief_results  WorldCat item entry]</ref>

=== Numismatics ===

*''The Thirteen-Months Coinage of Hieronymos of Syracuse, Antike Münzen und Geschnittene Steine III,'' Walter De Gruyter (Berlin), 1969.<ref>Review, by 	Konrad Kraf ''Historische Zeitschrift,'' v211 n1 (Aug., 1970): 105-106</ref>
*''Art and Coinage in Magna Graecia,'' Edizioni Arte e Moneta (Bellinzona), 1978. ISBN 9780839002062
*''Wheaton College Collection of Greek and Roman Coins, Ancient Coins in North American Collections,'' with J. D. Bishop (New York) 1981.
*''The Coinage of Terina,'' with G,. K. Jenkins, Edizioni Arte e Moneta (Bellinzona) 1982.
*''Ripostigli del Museo Archeologico di Siracusa,'' International Center for Numismatic Studies, Biblioteca vol. 2 (Naples) 1989.
*''Morgantina Studies, II, The Coins,'' with T. V. Buttrey, K. T. Erim, T. Groves, Princeton University Press, 1989.
*''Ancient Greek Coins: Catalogue of the Classical Collection, Museum of Art, Rhode Island School of Design,  ("Archaeologia  Transatlantica" 15) Providence and Louvain-la-Neuve, 1998. OCLC 43916721

== References ==

{{reflist}}
*Cova, Elisabetta "Curriculum Vitae of R. Ross Holloway," Derek B. Counts and Anthony S. Tuck, eds., ''Koine, Mediterranean Studies in Honor of R. Ross Holloway,'' (Joukowsky Institute Publication  ; 1), Oxford and Oakville, 2009, pp. xiv-xx.   (contains a full bibliography to 2008)

{{DEFAULTSORT:Holloway, R. Ross}}
[[Category:American archaeologists]]
[[Category:1934 births]]
[[Category:Living people]]
[[Category:Rome Prize winners]]
[[Category:Amherst College alumni]]
[[Category:Roxbury Latin School alumni]]