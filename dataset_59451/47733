{{Use mdy dates|date=April 2016}}
{{Infobox person
| name          = Jena Engstrom 
| image         = Jena_Engstrom.png
| caption       = Jena Engstrom as Betty in April 19, 1961 episode of [[Adventures of Ozzie and Harriet|The Adventures of Ozzie and Harriet]] titled "Selling Rick's Drums".
| birth_name    = Liana Jeanne Moon 
| birth_date    = {{birth date and age|mf=yes|1942|6|30}}
| birth_place   = [[Los Angeles, California|Los Angeles]], [[California]], USA
| death_date    = 
| death_place   = 
| height        = 
| parents       = Jean Engstrom
| occupation    = Former television actress
| years_active  = [[1960 in television|1960]] - [[1964 in television|1964]]
}}
'''Jena Engstrom ''' (born '''Liana Jeanne Moon''' on 
June 30, 1942 in [[Los Angeles, California|Los Angeles]], [[California]]) is an American former television actress. She appeared in more than three dozen episodes of various [[television series]] between 1960 and 1964.

==Career==
Jena Engstrom<ref name=Jena /> began acting after graduating from high school<ref name=Name />  and first appeared on television in the 1960 episode "Adopted" of the [[American Broadcasting Company|ABC]] [[crime drama]], ''[[The Detectives Starring Robert Taylor]]''. She appeared in some thirty-seven television programs<ref name=Jena />  before making her final appearance opposite guest star [[Robert Culp]] in the 1964 episode, "The Stallion", of [[NBC]]'s ''[[The Virginian (TV series)|The Virginian]]<ref name="Black Stallion" />''.

===Mother and daughter===

Jena Engstrom<ref name=Jena>{{cite web|title=Jena Engstrom|url=http://www.imdb.com/name/nm1226269/?ref_=fn_nm_nm_1|work=IMDb|accessdate=14 January 2013}}</ref> is the daughter of actress [[Jean Engstrom]]<ref name=Jena /><ref name=Jean>{{cite web|title=Jean Engstrom|url=http://www.imdb.com/name/nm0257761/?ref_=fn_nm_nm_2|work=IMDb|accessdate=14 January 2013}}</ref><ref name=Mother-Daughter />    (1920-1997)<ref name=Jean />  whose television career (1955-1966)<ref name=Jean /><ref name=Mother-Daughter />  overlapped her daughter’s and whose career also included movie<ref name=Jean />  and regional stage appearances.<ref name=Mother-Daughter>{{cite news|last=Vernon|first=Terry|title=Tele]Vues:  Mother-Daughter Team On TV|url=http://search.ancestry.com/browse/view.aspx?dbid=51539&iid=News-CA-IN.1962_01_16-0028&rc=588,342,745,380;2258,300,2410,338;650,625,714,658;1156,1242,1220,1275;1413,1242,1477,1275;1494,1242,1623,1275;1091,1383,1170,1416;974,1421,1040,1454;1056,1421,1204,1454;294,2600,382,2633;461,4458,523,4491;350,5300,417,5333;1138,1846,1255,1879;1431,1412,1561,1445;1553,1483,1630,1516;1441,1621,1581,1654;1358,1725,1430,1758;1329,2104,1467,2137;731,4892,793,4925;809,4892,948,4925;592,4925,656,4958;672,4925,816,4958;1729,3900,1858,3971&pid=504414236&ssrc=&fn=Jena&ln=Engstrom&st=g|accessdate=14 January 2013|newspaper=The Long Beach (California) Independent, Page C-12|date=16 January 1963}}</ref><ref name="Stage Actress">{{cite news|title=Stage Actress Stars In Graham Production|url=http://search.ancestry.com/browse/view.aspx?dbid=51369&iid=News-TE-EL_PA_HE-1965_11_27-0003&rc=960,104,1031,133;216,733,280,762;2050,679,2131,708;1548,2583,1618,2612;1470,2729,1538,2758;2075,1342,2130,1371;2144,1342,2268,1371;2210,1400,2319,1429;2332,1400,2414,1429;1733,2063,1804,2092;1822,2063,1964,2092;2161,1867,2298,1896;2314,1867,2417,1896;2130,2113,2274,2142;2662,1917,2745,1946;2635,1979,2695,2008;858,3854,954,3883&pid=506326019&ssrc=&fn=Jean&ln=Engstrom&st=g|accessdate=14 January 2013|newspaper=The El Paso (Texas) Herald-Post, Section A, Page 3|date=27 November 1965}}</ref>    Most on-line databases and this article have listed only two of the three TV shows in which they appeared together.  The first of the two shows listed is the April 1961 episode of the [[CBS]] program [[Rawhide (TV series)|Rawhide]] titled "Incident of the Lost Idol" in which they appeared as mother and daughter.  They had only one brief scene together as the story was not about their relationship. The second show listed is the January 1962 episode “To Sell Another Human Being” of ABC's ''[[The New Breed (TV Series)|The New Breed]]'', starring [[Leslie Nielsen]],<ref name=Mother-Daughter /> in which mother Jean played a wealthy woman who with her husband (played by [[Richard Arlen]]) adopt a baby through an adoption mill.  The police get involved when the baby's natural mother, played by daughter Jena, tries to get her baby back.  The third television show in which they appeared together is an unidentified 30-minute religious program presented in 1961 in which they played mother and daughter.  In this story a girl (Jena) has trouble relating to her parents until a session with teenagers at a church is recorded and the recording made available to the parents to listen to.  Appearing in this show with Jean and Jena Engstrom are [[Robert Stevenson (actor and politician)|Robert Stevenson]] as Jena’s father and [[Richard Evans (actor)|Richard Evans]] as her boyfriend.  This film, minus the opening and closing credits, has been posted on-line.<ref name=religious>{{cite web|title=F-9483|url=http://www.historicfilms.com/tapes/10790|date=1961|accessdate=4 August 2014}}</ref>

===Western series===

Twenty-one of Jena Engstrom's thirty-seven roles were in television [[Western (genre)|westerns]]. Some of her best acting opportunities came in these. She appeared three times each on ''[[Have Gun, Will Travel]]'', and ''[[Wagon Train]]'', as well as ''[[Rawhide (TV series)|Rawhide]]'', twice each on ''[[Bonanza]]'', ''[[Gunsmoke]]'', and ''[[Laramie (TV series)|Laramie]]'', and once each on ''[[The Tall Man (TV series)|The Tall Man]]'', ''[[Death Valley Days]]'', ''[[Frontier Circus]]'', and ''[[Outlaws (1960 TV series)|Outlaws]]'', as well as her final performance as an actress in ''The Virginian''.

  In “The Education of Sarah Jane” episode of [[Have Gun, Will Travel]] she appeared as Sarah Jane opposite guest star [[Duane Eddy]] in a story of the youngest members of feuding clans that have been killing one another for generations.  It is up to Paladin ([[Richard Boone]]) to bring an end to the madness.  In the “Milly” episode of ''Gunsmoke'', she played Milly Glover, an impoverished teenager hoping to break herself and her younger brother (Billy E. Hughes) from their abusive and alcoholic father ([[Malcolm Atterbury]].  She tries to escape poverty through marriage to an older man, but the three men ([[James Griffith]], [[Don Dubbins]], and Harry Swoger) to whom she proposes reject her. She plots revenge. In the episode “Chester’s Indian”, Engstrom plays Callie Dill, the repressed daughter of a storekeeper ([[Karl Swenson]]) who is helping a wrongfully-detained [[Cheyenne people|Cheyenne]] brave ([[Eddie Little Sky]]) trying to return to his village.  When Chester ([[Dennis Weaver]]) shoots and wounds the brave, Callie implores Chester to nurse him back to health. In 1962, in one of her three appearances on ''[[Rawhide (TV series)|Rawhide]]'', she had the title role of "The Child-Woman" in an episode also guest starring [[Cesar Romero]] where she played a teenager willfully in the clutches of a domineering saloonkeeper. In 1961, she portrayed Laurie Manson in "The Incident of the Lost Idol". She retired from acting in 1964 with appearances on ''[[Wagon Train]]'', ''[[Perry Mason]]'', when she played Vera Janel in "The Case of the Illicit Illusion," and in her final television appearance in "The Black Stallion" episode of ''The Virginian'', Engstrom plays a rancher’s daughter in love with an alcoholic veterinarian (Robert Culp) who has been drafted by series co-star [[Randy Boone]] to care for an abused stallion.

===Illness===

Jena Engstrom left acting in 1964, possibly because of health considerations. In 1963, she was cast in what was to have been a recurring role of Jennie in the series ''[[The Travels of Jaimie McPheeters (TV series)|The 
Travels of Jaimie McPheeters]]'', starring [[Kurt Russell]],<ref name="O'Herlihy">{{cite news|last=Russell|first=Fred H.|title=O'Herlihy Signs For Top Role In Series|url=http://search.ancestry.com/browse/view.aspx?dbid=6788&iid=NEWS-CT-TH_BR_PO.6788_1963_04_04_0035&rc=2342,108,2451,137;500,1512,539,1545;659,2483,739,2512;912,1892,965,1921;2254,1354,2310,1383;2325,1354,2452,1383;2334,2042,2477,2071&pid=505024547&ssrc=&fn=Jena&ln=Engstrom&st=g|accessdate=14 January 2013|newspaper=The Bridgeport Post, Page 35|date=4 April 1963}}</ref> but had to give up the role because of illness and was replaced by [[Donna Anderson]].<ref name=Anderson>{{cite news|last=Scheuer|first=Steven H.|title=TV Mailbag|url=http://search.ancestry.com/browse/view.aspx?dbid=7950&iid=NEWS-IN-HA_TI.1963_10_18-0033&rc=4610,121,4736,179;1979,633,2135,670;3076,1092,3163,1129;3185,1092,3380,1129&pid=505235489&ssrc=&fn=Jena&ln=Engstrom&st=g|accessdate=14 January 2013|newspaper=The Hammond (Indiana) Times, Page C-4|date=18 October 1963}}</ref><ref name=Recovers>{{cite news|title=Jena Engstrom Recovers|url=http://search.ancestry.com/browse/view.aspx?dbid=51411&iid=News-TE-EX.1963_11_24-0095&rc=83,1438,195,1488;83,1517,297,1567;157,1625,218,1650;234,1625,387,1650;288,1738,343,1763;2414,2808,2485,2833&pid=505274463&ssrc=&fn=Jena&ln=Engstrom&st=g|accessdate=14 January 2013|newspaper=The San Antonio (Texas) Express and News, Page 5|date=24 November 1963}}</ref>

In June 1964, she was replaced again because of ill health by Davey Davison in the episode "Children of Calamity" of [[Richard Crenna]]'s CBS drama series, ''[[Slattery's People]]''.<ref name=Sub>{{cite news|title=Sub for Engstrom|url=http://pqasb.pqarchiver.com/latimes/access/469843762.html?dids=469843762:469843762&FMT=CITE&FMTS=CITE:AI&type=historic&date=Jun+19%2C+1964&author=&pub=Los+Angeles+Times&desc=Sub+for+Engstrom&pqatl=google|accessdate=14 January 2013|newspaper=The Los Angeles (California) Times, Page D15|date=19 June 1964}}</ref>

===Confused credits===

The acting credits of mother Jean and daughter Jena were confused at the time they were active in the business resulting in mixed credits in newspapers and problems with pay checks.<ref name=Name>{{cite news|title=The Name's Almost the Same for Jean and Jena Engstrom|url=http://search.ancestry.com/browse/view.aspx?dbid=51411&iid=News-TE-EX.1963_01_13-0079&rc=622,75,686,100;388,696,446,717;1974,2946,2148,3025;2192,2946,2540,3025;851,3125,913,3150;571,3379,631,3404;1106,3296,1191,3321;1101,3388,1171,3413;1514,3246,1571,3271;1533,3275,1655,3300;1556,3309,1616,3334;1846,3284,1902,3309&pid=504935503&ssrc=&fn=Jena&ln=Engstrom&st=g|accessdate=14 January 2013|newspaper=San Antonio (Texas) Express and News, Page 10|date=13 January 1963}}</ref>   For a long time the credits were confused on many Internet databases, and while some errors still exist, many have been corrected.

===DVDs===

Several of the television series listed below have been released by their studios on DVDs that are available for sale or for rent.

===Television Roles<ref name=Jena /><ref name=CTVA>{{cite web|title=The Classic TV Archive|url=http://ctva.biz/index.htm|work=CTVA|accessdate=17 January 2013}}</ref><ref name="TV Guide">{{cite web|title=TV Guide|url=http://www.tvguide.com/|work=TV Guide|accessdate=17 January 2013}}</ref>   ===

Note:  "Episode" column:  2.9 indicates Season 2, Episode 9, etc.

{|class="wikitable sortable"
|-

! AIR DATE !! SHOW !! EPISODE !! EPISODE TITLE !! CHARACTER !! NOTES !! DVD
|-
| Nov. 25, 1960 || [[The Detectives Starring Robert Taylor]]  || 2.9 ||  "Adopted"<ref>
{{cite AV media
 | people = "Adopted" (1960)
 | title = The Detectives
 | medium = DVD (Disc 17)
 | publisher =magiclantern1973
 | location =
 | date =
 | url = }}
</ref>
   || Ann Martin || 1st of 2 appearances on this show. || 
|-
| Feb. 5, 1961 || [[Shirley Temple Theatre]]  || 2.19 ||  "The Fawn"<ref name="Shirley Temple -- The Fawn">{{cite news|title=The Shirley Temple Show:  "The Fawn"|url=http://search.ancestry.com/Browse/view.aspx?dbid=51548&path=1961.2.5.59&sid=&gskw=Shirley+Temple|accessdate=18 January 2013|newspaper=Pasadena (California) Independent Star-News:  TV Week}}</ref>    || Kathy || ||
|-
| Feb. 25, 1961 || [[Have Gun, Will Travel]]  || 4.23 || "Fatal Flaw" <ref name="Fatal Flaw">{{cite AV media
 | people = Fatal Flaw (1961)
 | title = Have Gun - Will Travel:  The Fourth Season:  Volume Two
 | medium = DVD (Disc One)
 | publisher = CBS Television Studios
 | location =
 | year = 2010
 | url = }}</ref>

  || Cassandra Langford || 1st of 3 appearances on this show. || Yes 
|-
| Mar. 14, 1961 || [[The Red Skelton Hour]]  || 10.17 || "Freddy and the Baby"<ref>
{{cite AV media
 | people = "Freddy and the Baby" (1961)
 | title = Red Skelton:  America's Clown Prince
 | medium = DVD (Disc 2)
 | publisher = Timeless Media Group
 | location =
 | year = 2007
 | url = }}
</ref>
  || Young Woman (Baby's Mother) || || Yes
|-
| Apr. 19, 1961 || [[The Adventures of Ozzie and Harriet]] || 9.30 || "Selling Rick's Drums"<ref>
{{cite AV media
 | people = "Selling Rick's Drums" 1961)
 | title = Ozzie and Harriet:  Best of Ricky and Dave
 | medium = DVD (Disc 3)
 | publisher = Shout! Factory LLC
 | location =
 | year = 2008
 | url = }}
</ref>
  || Betty Hamilton || || Yes
|-
| Apr. 28, 1961 || [[Rawhide (TV series)|Rawhide]]  || 3.24 || "Incident of the Lost Idol"<ref>
{{cite AV media
 | people = "Incident of the Lost Idol" (1961)
 | title = Rawhide:  The Third Season:  Volume 2
 | medium = DVD (Disc 3)
 | publisher = CBS DVD
 | location =
 | year = 2008
 | url = }}
</ref>
  || Laurie Manson || 1st of 3 appearances on this show.  Mother Jean Engstrom also appears in this episode. || Yes
|-
| Jun. 11, 1961 || [[The Asphalt Jungle (TV series)|The Asphalt Jungle]]  || 1.11 || "The Kidnapping"<ref name="Asphalt Jungle -- Kidnapping">{{cite web|title=The Classic TV Archive - US Crime Series:  "The Kidnapping"|url=http://ctva.biz/US/Crime/AsphaltJungle.htm|work=CTVA|accessdate=18 January 2013}}</ref>   || Karen Meriden || ||
|-
| Sep. 23, 1961 || [[Have Gun, Will Travel]]  || 5.2 || "The Education of Sarah Jane"<ref>
{{cite AV media
 | people = "The Education of Sarah Jane" (1961)
 | title = Have Gun - Will Travel:  The Fifth Season:  Volume One
 | medium = DVD (Disc One
 | publisher = CBS Television Studios
 | location =
 | year = 2010
 | url = }}
</ref>
    || Sarah Jane Darrow || 2nd of 3 appearances on this show. || Yes
|-
| Oct. 1, 1961 || [[Bonanza]]  || 3.2 || "Springtime"<ref>
{{cite AV media
 | people = "Springtime" (1961)
 | title = Bonanza:  The Official Third Season:  Volume 1
 | medium = DVD (Disc 1)
 | publisher = CBS DVD
 | location =
 | year = 2012
 | url = }}
</ref>
  || Ann || 1st of 2 appearances on this show. || Yes
|-
| Oct. 12, 1961 || [[My Three Sons]]  || 2.3 || "The Crush"<ref>
{{cite AV media
 | people = "The Crush" (1961)
 | title = My Three Sons:  The Second Season:  Volume One
 | medium = DVD (Disc 1)
 | publisher = CBS DVD
 | location =
 | year = 2010
 | url = }}
</ref>
  || Mary Beth Jackson || || Yes
|-
| Oct. 14, 1961 || [[The Tall Man (TV series)]]  || 2.6 || "An Item for Auction"<ref>
{{cite AV media
 | people = "An Item for Auction" (1961)
 | title = The Tall Man
 | medium =  DVD (Disc 2)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year = 2007
 | url = }}
</ref>
  || Mary Susan Naylack || || Yes
|-
| Oct. 23, 1961 || [[Death Valley Days]]  || 10.7 || "Storm Over Truckee"<ref name="Death Valley Days -- Storm Over Truckee">{{cite web|title=Episode Detail:  Storm Over Truckee - Death Valley Days|url=http://www.tvguide.com/tvshows/death-valley-days-1961/episode-5-season-10/storm-over-truckee/200859|work=TV Guide|accessdate=18 January 2013}}</ref>   || Maggie Woolf || Billed as Jenna Engstrom.  ||
|-
| Oct. 30, 1961 || [[The Everglades]]  || 1.4 || "Primer for Pioneers" <ref>
{{cite AV media
 | people = "Primer for Pioneers" (1961)
 | title = Television Classics Digital Collection:  Everglades!
 | medium = DVD (Disc 4)
 | publisher = A Magic Lantern Production
 | location =
 | date = 
 | url = }}
</ref>
 || Memory Jacks || The series is listed as "Everglades!" in the opening credits.  ||
|-
| Nov. 1, 1961 || [[Wagon Train]]  || 5.6 || "The Jenna Douglas Story"<ref>
{{cite AV media
 | people = "The Jenna Douglas Story" (1961)
 | title = Wagon Train:  The Complete Season Five
 | medium = DVD (Disc 2)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year = 2011
 | url = }}
</ref>
  || Sue Thompson || || Yes
|-
| Nov. 2, 1961 || [[Outlaws (1960 TV series)]]  || 2.5 || "The Night Riders"<ref>
{{cite AV media
 | people = "The Night Riders" (1961)
 | title = Outlaws
 | medium = DVD (Disc 8)
 | publisher = avantgarde101
 | location =
 | date =
 | url = }}
</ref>
  || Louise Nichols || ||
|-
| Nov. 25, 1961 || [[Gunsmoke]]  || 7.9 || "Milly"<ref>
{{cite AV media
 | people = "Milly" (1961)
 | title =Gunsmoke: The Seventh Season:  Volume 1
 | medium = DVD (Disc 3)
 | publisher = CBS DVD
 | location =
 | year = 2012
 | url = }}
</ref>
  || Milly Glover || 1st of 2 appearances on this show. || Yes 
|-
| Dec. 26, 1961 || [[Laramie (TV series)]]  || 3.13 || "The Lawless Seven" <ref>
{{cite AV media
 | people = "The Lawless Seven" (1961)
 | title = Laramie
 | medium = DVD (Disc 3)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year = 2008
 | url = }}
</ref>
 || Ginny Hawks || 1st of 2 appearances on this show. || Yes
|-
| Jan. 16, 1962 || [[The New Breed (TV Series)]]|| 1.16 || "To Sell Another Human Being"<ref name=Mother-Daughter />   || || Mother Jean Engstrom also appears in this episode. || 
|-
| Mar. 2, 1962 || [[The Detectives Starring Robert Taylor]]|| 3.20 || "Night Boat" <ref>
{{cite AV media
 | people = "Night Boat"
 | title = The Detectives
 | medium = DVD (Disc 9)
 | publisher = magiclantern1973
 | location =
 | date =
 | url = }}
</ref>
   || Frances || 2nd of 2 appearances on this show. || 
|-
| Mar. 14, 1962 || [[Wagon Train]]  || 5.24 || "The Amos Billings Story"<ref>
{{cite AV media
 | people = "The Amos Billings Story" (1962)
 | title = Wagon Train:  The Complete Fifth Season
 | medium = DVD (Disc 6)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year =2011
 | url = }}
</ref>
  || Loan || 2nd of 2 appearances on this show. || Yes
|-
| Mar. 17, 1962 || [[Have Gun, Will Travel]]  || 5.26 || "Alice" <ref>
{{cite AV media
 | people = "Alice" (1962)
 | title = Have Gun - Will Travel:  The Fifth Season:  Volume Two
 | medium = DVD (Disc 2)
 | publisher = CBS Television Studios
 | location =
 | year = 2011
 | url = }}
</ref>
 || Maya Ferguson || 3rd of 3 appearances on this show. ||Yes
|-
| Mar. 23, 1962 || [[Rawhide (TV series)]] || 4.24 || "The Child-Woman"<ref>
{{cite AV media
 | people = "The Child-Woman"
 | title = Rawhide:  The Fourth Season:  Volume 2
 | medium = DVD (Disc 3)
 | publisher =
 | location = CBS DVD
 | year = 2011
 | url = }}
</ref>
  || Posie Mushgrove || 2nd of 3 appearances on this show. || Yes
|-
| Apr. 5, 1962 || [[Frontier Circus]]  || 1.20 || "Mighty Like Rogues"<ref>
{{cite AV media
 | people = "Mighty Like Rogues" (1962)
 | title = Frontier Circus:   The Complete TV Series
 | medium = DVD (Disc Five)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year = 2010
 | url = }}
</ref>
  || Betsy Ross Jukes || || Yes
|-
| May 12, 1962 || [[Gunsmoke]]  || 7.32 || "Chester's Indian" <ref>
{{cite AV media
 | people = "Chester's Indian" (1962)
 | title = Gunsmoke:  The Seventh Season:  Volume 2
 | medium = DVD
 | publisher = CBS DVD
 | location =
 | year = 2013
 | url = }}
</ref>
 || Callie Dill || 2nd of 2 appearances on this show. || Yes
|-
| Oct. 26, 1962 || [[Rawhide (TV series)]]  || 5.5 || "Incident of the Four Horsemen"<ref>
{{cite AV media
 | people = "Incident of the Four Horsemen" (1962)
 | title = Rawhide:  The Fifth Season:  Volume 1
 | medium = DVD (Disc 2)
 | publisher = CBS DVD
 | location =
 | year = 2013
 | url = }}
</ref>
  || Amy Galt || 3rd of 3 appearances on this show. || Yes
|-
| Nov. 5,1962 || [[Stoney Burke (TV series)]]  || 1.6 || "A Matter of Pride"<ref>
{{cite AV media
 | people = "A Matter of Pride" (1962)
 | title = Stoney Burke:  Collector's Edition:  Vol. 1
 | medium = DVD (Disc 2)
 | publisher = Audio Tape, Inc.
 | location =
 | year = 2005
 | url = }}
</ref>
  || Meryle Hill || || Yes
|-
| Nov. 13, 1962 || [[Laramie (TV series)]]  || 4.7 || "The Sunday Shoot"<ref>
{{cite AV media
 | people = "The Sunday Shoot" (1962)
 | title = Laramie:  The Final Season
 | medium = DVD (Disc 2)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year = 2009
 | url = }}
</ref>
  || Nancy Tilford || 2nd of 2 appearances on this show. || Yes
|-
| Dec. 2, 1962 || [[Bonanza]] || 4.10 || "The Deadly Ones"<ref>
{{cite AV media
 | people = "The Deadly Ones" (1962)
 | title = Bonanza:  The Official Fourth Season:  Volume 1
 | medium = DVD (Disc 3)
 | publisher = CBS DVD
 | location =
 | year = 2012
 | url = }}
</ref>
  || Molly Reed || 2nd of 2 appearances on this show. || Yes
|-
| Jan. 11, 1963 || [[Route 66 (TV series)]]  || 3.16 || "You Can't Pick Cotton in Tahiti" <ref>
{{cite AV media
 | people = "You Can't Pick Cotton In Tahiti" (1963)
 | title = Route 66:  Complete Third Season:  Volume One
 | medium = DVD (Disc 4)
 | publisher = Roxbury Entertainment
 | location =
 | year = 2009
 | url = }}
</ref>
 || Elva Dupree || || Yes
|-
| Feb. 7, 1963 || [[Dr. Kildare]]  || 2.18 || "Good Luck Charm"<ref name="Dr. Kildare -- Gloria Swanson">{{cite news|title=Gloria Swanson with Kildare|url=https://news.google.com/newspapers?nid=1755&dat=19630207&id=qbgqAAAAIBAJ&sjid=pGUEAAAAIBAJ&pg=2337,1526232|accessdate=18 January 2013|newspaper=Google.News:  Sarasota (Florida) Herald Tribune, Page 39|date=7 February 1963}}</ref>   || Marcia Laverly || || Yes
|-
| Apr. 17, 1963 || [[The Eleventh Hour (U.S. TV series)]]  || 1.27 || "Try to Keep Alive Until Next Tuesday"  || Marian Snyder || || Yes
|-
| May 23, 1963 || [[The Doctors and the Nurses]] || 1.30 || "Bitter Pill"<ref name="The Nurses">{{cite news|title=Mothers-To-Be Warned Abouth Pill Taking|url=https://news.google.com/newspapers?nid=1144&dat=19630523&id=2kYqAAAAIBAJ&sjid=8U4EAAAAIBAJ&pg=7016,3002450|accessdate=18 January 2013|newspaper= [[Pittsburgh Press]], P 67|date=23 May 1963}}</ref>    || Jean Wheeler || The show was called [[The Nurses (CBS TV series)]] during the first season. ||
|-
| Sep 15, 1963 || [[The Travels of Jaimie McPheeters (TV series)]]  || 1.1 || "The Day of Leaving"<ref name="Jaimie McPheeters">{{cite web|title=Episode Detail:  The Day of Leaving -- The Travels of Jaimie McPheeters|url=http://www.tvguide.com/tvshows/the-travels-of-jaimie-mcpheeters-1963/episode-1-season-1/the-day-of-the-leaving/205145|work=TV Guide|accessdate=18 January 2013}}</ref>  || Jennie || Jena Engstrom was replaced after this pilot episode because of illness ||
|-
| Feb. 7, 1964 || [[77 Sunset Strip]] || 6.20 || "Queen of the Cats"<ref name="77 Sunset Strip">{{cite web|title=Episode Detail:  The Queen of Cats -- 77 Sunset Strip|url=http://www.tvguide.com/tvshows/77-sunset-strip-1964/episode-20-season-6/queen-of-the-cats/199572|work=TV Guide|accessdate=18 January 2013}}</ref>   || Marian Armstrong || Final episode of this series.[http://www.imdb.com/title/tt0051247/ 77 Sunset Strip] || 
|-
| Mar. 30, 1964 || [[Wagon Train]] || 7.20 || "The Santiago Quesada Story"<ref>
{{cite AV media
 | people = "The Santiago Quesada Story" (1964)
 | title = Wagon Train:  The Complete Color Season
 | medium = DVD (Disc 10)
 | publisher = NBC Universal:  Timeless Media Group
 | location =
 | year = 2008
 | url = }}
</ref>
  || Kim Case || 3rd of 3 appearances on this show.  || Yes
|-
| Apr. 9, 1964 || [[Perry Mason (TV series)]]  || 7.25 || "The Case of the Ilicit Illusion"<ref>
{{cite AV media
 | people = "The Case of the Illicit Illusion"
 | title = Perry Mason:  Season 7:  Volume 2
 | medium = DVD (Disc 3)
 | publisher = CBS DVD
 | location =
 | year = 2012
 | url = }}
</ref>
  || Vera Janel|| || Yes
|-
| Sep. 30, 1964 || [[The Virginian (TV series)]]  || 3.3 || "The Black Stallion"<ref name="Black Stallion">
{{cite AV media
 | people = "The Black Stallion"
 | title = The Virginian:  The Complete Third Season
 | medium = DVD (Disc 1)
 | publisher = NBC Universal"  The Media Group
 | location = 
 | year = 2010
 | url = }}
</ref>
  || Jodie Wingate || Episode also called "The Stallion"  || Yes
|}
{{Portal|California|Television}}

==References==
{{reflist|2}}

{{DEFAULTSORT:Engstrom, Jena}}
[[Category:1942 births]]
[[Category:Living people]]
[[Category:American television actresses]]
[[Category:Actresses from Los Angeles]]
[[Category:20th-century American actresses]]