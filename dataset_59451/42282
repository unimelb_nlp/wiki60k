<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Cadet
  |image = Avro Cadet.jpg
  |caption = RAAF Avro Cadets
}}{{Infobox Aircraft Type
  |type = Trainer
  |manufacturer = [[Avro]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = October [[1931 in aviation|1931]] 
  |introduced = 1932
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = 1932 - 1939
  |number built = 104
  |unit cost = 
  |developed from = [[Avro Tutor]] 
  |variants with their own articles = [[Avro 638|Avro 638 Club Cadet]] 
}}
|}

The '''Avro Cadet''' was a single-engined [[United Kingdom|British]] [[biplane]] [[Trainer (aircraft)|trainer]] designed and built by [[Avro]] in the 1930s as a smaller development of the [[Avro Tutor]] for civil use.

==Design and development==
The '''Avro 631 Cadet''' was developed in 1931 as a smaller, more economical, derivative of the [[Avro Tutor|Tutor]] military trainer, for flying club or personal use. The first prototype, ''G-ABRS'' flew in October 1931.<ref name="jackson avro">{{cite book |last= Jackson|first= A J |title=Avro Aircraft since 1908 |edition= 2nd|year= 1990 |publisher= Putnam Aeronautical Books|location= London |isbn= 0-85177-834-8}}</ref> It was publicly unveiled at the opening of Skegness airfield in May 1932, although by this time, the first orders for the type, for the Irish Army Air Corps, had already been placed and the order (for six Cadets) delivered.

The Avro 631 Cadet was replaced in production in September 1934<ref name="jacksonv1">{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 1|year= 1974|publisher= Putnam|location= London|isbn=0-370-10006-9 }}</ref>
by the improved '''Avro 643 Cadet''', which had a revised rear fuselage with a raised rear seat, retaining the 135&nbsp;hp (101&nbsp;kW) [[Armstrong Siddeley Genet Major]] 1 engine of the Avro 631. In turn, this formed the basis for the more powerful '''Avro 643 Mk II Cadet'''; it was also strengthened and had improved parachute egress. This model entered service in 1935, and was built in the largest numbers, including 34 fitted with a tailwheel for the [[Royal Australian Air Force]].<ref name="jackson avro"/>

==Operational history==
The Cadet, while smaller and more economical than the Tutor, was still more expensive to run than competing two-seat light civil aircraft and was harder to hangar because of its lack of [[folding wing]]s, so it was used mainly as a trainer for flying schools or the military. By far, the largest civil user was [[Air Service Training Ltd]], which operated 17 Avro 631s at [[Hamble-le-Rice|Hamble]], together with a further four operated by its [[Hong Kong]] subsidiary, the Far East Aviation Co. Air Service Training also operated 23 Mk II Cadets, with both these and the earlier Cadets remaining in service with Reserve Training Schools run by Air Service Training until they were impressed as ATC instructional airframes in 1941.<ref name="jackson avro"/>

The other major operator was the RAAF, which acquired 34 Mk II Cadets, delivered between November 1935 and February 1939.<ref name="jackson avro"/> These remained in service until 1946, when the surviving 16 were sold for civil use.<ref name="jacksonv1"/> Two of these were re-engined in 1963 with 220&nbsp;hp (160&nbsp;kW) [[Jacobs R-755]] engines for use as crop sprayers. In the U.K., only two Cadets survived the war.

==Variants==
;Avro 631 Cadet
:Initial version, powered by [[Armstrong Siddeley Genet Major]] I engine, 35 built.
;Avro 643 Cadet
:Raised rear seat, eight built.
;Avro 643 Cadet II
:Powered by 150&nbsp;hp (110&nbsp;kW) Genet Major 1A, 61 built.

==Operators==
[[File:Avro cadet trainer A6-23 from The Powerhouse Museum.jpg|thumb|right|250px|An [[RAAF]] Avro Mk II Cadet built in Manchester U.K. (despite the signboard) and erected in [[Australia]]]]

===Civil operators===
;{{UK}}
*[[Air Service Training Ltd]]

===Military operators===
;{{AUS}}
*[[Royal Australian Air Force]] operated 34 Avro 643 MkII Cadet.<ref name="Wilson">{{cite book|last1=Wilson|first1=Stewart|title=Military Aircraft of Australia|date=1994|publisher=Aerospace Publications|location=Weston Creek, Australia|isbn=1875671080|pages=216|accessdate=19 August 2016}}</ref>
;{{IRL}}
*[[Irish Air Corps]] operated seven Avro 631 Cadets.
;{{POR}}
*[[Portuguese Air Force]]
;{{China as ROC}}
*[[Chinese Nationalist Air Force]] - China had five Avro 631 deployed at [[Liuzhou]] Aviation School during the [[Second Sino-Japanese War]], all of which were lost due to Japanese bombing in 1939.
;{{flag|Spain|1931}}
*[[Spanish Republican Air Force]]<ref>[http://www.zi.ku.dk/personal/drnash/model/spain/did.html Aircraft that took part in the Spanish Civil War]</ref>

==Survivors==

* There are two Cadets flying in Australia (''VH-AEJ'' and '''AGH'')
* There is one in Ireland (the last of the Irish Air Corps machines, though home after a long tour of duty via the U.K. and New Zealand as ''ZK-AVR'')
* One reputed airworthy Cadet is on display in the Museu do Ar, Portugal.
* A former Australian Air Force A-25 is airworthy as NX643AV at [[Kermit Weeks|Kermit Weeks']] [[Fantasy of Flight]] in Polk City, Florida.
* A former Australian Air Force A-34, ex VH-RUO, is on static display at the RAAF Museum in Point Cook.

==Specifications (Avro 643 Mk II Cadet)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Avro Aircraft since 1908 <ref name="jackson avro"/> 
|crew=two
|capacity=
|length main= 24 ft 9 in
|length alt= 7.55 m
|span main= 30 ft 2 in
|span alt= 9.20 m
|height main= 8 ft 10 in
|height alt= 2.69 m
|area main= 262 ft²
|area alt= 24.3 m²
|airfoil=
|empty weight main= 1,286 lb
|empty weight alt= 585 kg
|loaded weight main= 2,000 lb
|loaded weight alt= 907 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Armstrong Siddeley Genet Major]] 1A 
|type of prop=seven cylinder [[radial engine|radial]]
|number of props=1
|power main= 150 hp
|power alt= 112 kW
|power original=
|max speed main= 116 mph
|max speed alt= 101 kn, 187 km/h
|cruise speed main= 100 mph
|cruise speed alt= 87 kn, 161 km/h
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main= 
|stall speed alt= 
|range main= 325 mi
|range alt= 283 nmi, 523 km
|ceiling main= 12,000 ft
|ceiling alt= 3,660 m
|climb rate main= 700 ft/min
|climb rate alt= 3.6 m/s
|loading main= 7.63 lb/ft²
|loading alt= 37.4 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.075 hp/lb
|power/mass alt= 0.12 kW/kg
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=
* [[Avro Tutor]]: Cadet developed from this
* [[Avro 638|Avro 638 Club Cadet]]: developed Cadet with folding wings
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=*[[List of Interwar military aircraft]]
*[[List of aircraft of the Spanish Republican Air Force]]<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{Reflist}}

==External links==
{{commons category|Avro 643 Cadet}}
*[https://web.archive.org/web/20070901132400/http://www.raaf.gov.au/raafmuseum/exhibitions/hangar180/avro.htm RAAF Museum]
*[http://www.britishaircraft.co.uk/aircraftpage.php?ID=254 British Aircraft Directory]
*[http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200269.html "The Avro 631 'Cadet'"] a 1932 ''Flight'' article

{{Avro aircraft}}

[[Category:British civil trainer aircraft 1930–1939]]
[[Category:Avro aircraft|Cadet]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]