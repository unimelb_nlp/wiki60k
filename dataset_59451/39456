{{Advert|date=March 2017}}

{{Infobox OS
| name          = Cosmos
| logo          = Cosmos logo.png
| logo_size     = x64px
| screenshot    = COSMOS Shell Screenshot.png
| caption       = Screenshot of an OS made with COSMOS, presenting a GUI creation possibility
| developer     = Cosmos Project
| source model  = [[Open source]]
| working state = Active
| latest preview version = Release 20150918
| latest preview date = {{start date and age|2015|09|18|df=y}}
| programmed in = [[C Sharp (programming language)|C#]], [[X Sharp|X#]]
| language      = English
| kernel_type   = [[Microkernel]]
| license       = [[BSD licenses|BSD]]
| website       = {{URL|github.com/CosmosOS}}
}}

'''C# Open Source Managed Operating System''' ('''Cosmos''') is a [[List of toolkits|toolkit]] for building [[operating system]]s, written mostly in the programming language [[C Sharp (programming language)|C#]] and small amounts of a high level [[assembly language]] named [[X Sharp|X#]].  ''Cosmos'' is a [[backronym]],<ref name="GitHub">[https://github.com/CosmosOS Cosmos website: project repository at GitHub]</ref> in that the [[acronym]] was chosen before the meaning. It is [[open-source software]] released under a [[BSD licenses|BSD license]].

Cosmos encompasses an [[AOT compiler]] named [[IL2CPU]] to translate [[Common Intermediate Language]] (CIL) into native instructions.  Cosmos compiles user-made programs and associated [[Library (computing)|libraries]] using IL2CPU to create a [[Booting|bootable]] native executable that can be run with no support.  The resulting output can be booted from a [[USB flash drive]], [[CD-ROM]], over a network via [[Preboot Execution Environment]] (PXE), or inside a [[virtual machine]]. Recent releases also allow deploying to certain [[x86]] embedded devices over Universal Serial Bus ([[USB]]).  While C# is the primary language used by developers (both on the backend and by [[end user]]s of Cosmos), many [[List of CLI languages|CLI languages]] can be used, provided they compile to pure CIL without the use of [[Platform Invocation Services]] (P/Invokes).  Cosmos is mainly intended for use with [[.NET Framework]], with [[Mono (software)|Mono]] support in the works.

{{As of|2016}}, Cosmos does not aim to become a full operating system, but rather a [[List of toolkits|toolkit]] to allow other developers to simply and easily build their own operating systems, or as one of the project leaders put it, to act as "''operating system [[Lego]]s''". It also functions as an abstraction layer, hiding much of the inner workings of the hardware from the eventual developer.

Older versions of Cosmos were released in ''Milestones'', with the last being Milestone 5 (released August, 2010).  More recently, the project switched to simply naming new releases after the latest commit number.  Releases of Cosmos are divided into two types: the ''Userkit'', and the ''Devkit''.  The Userkit"is a pre-packaged release that is updated irregularly, as new and improved features are added. Userkits are generally considered stable, but do not include recent changes and may lack features.  The Devkit refers to the [[source code]] of Cosmos, and must be built manually. The devkits are usually unstable as they may contain unfinished or untested code. The Devkit can be acquired on GitHub<ref name="GitHub" /> and uses Git as the source control management.  Most work on Cosmos is currently aimed at improving [[debugger]] functionality and [[Visual Studio]] integration.  [[Kernel (computing)|Kernel]] work is focused on implementing [[file system]]s, [[memory management]], and developing a reliable network interface. [[Syslinux]] serves as the project's [[bootloader]].

==Developing with Cosmos==
Cosmos has many facilities to improve the experience of developing operating systems with it, designed to make the process as fast and painless as possible, knowledge of the [[assembly language]] is not required to use Cosmos.

===Visual Studio integration===
A key feature of Cosmos, which separates it from other operating systems of its type, is its tight integration with [[Microsoft Visual Studio]].  Code can be written, [[compiled]], [[debugged]], and run entirely through [[Visual Studio]], with only a few key presses. Cosmos no longer supports [[Visual Studio 2013]], now it only supports [[Visual Studio 2015]].

===Debugging===
Cosmos can be seamlessly debugged through Visual Studio when running over PXE or in a virtual machine.  Many standard debugging features are present, such as breakpoints, tracing, and logging.  Additionally, debugging can be done via serial cables, if running on physical hardware.  When running in VMWare, Cosmos supports stepping and breakpoints, even while an operating system is running.

===Running===
Cosmos uses virtualization to help speed development by allowing developers to test their operating systems without having to restart their computers as often.  By default, VMWare Player is used, due to its ease of use in terms of integration with the project.  Other virtualization environments are supported as well, such as Bochs and VirtualPC.  An ISO disk image may also be generated that can be burned to a USB flash drive, CD-ROM, or similar media.

PXE booting is also supported, allowing for remote machines to run Cosmos over a network connection.

==Compile process==

===IL2CPU===
To compile .NET CIL into assembly language, Cosmos developers created an [[Ahead-of-time compilation|ahead-of-time]] compiler named IL2CPU, designed to parse CIL and output [[x86]] [[opcode]]s.

== Writing Cosmos code ==

An operating system built with Cosmos is developed in a similar fashion to any .NET C# console program. Additional references are made in the start of the program which give access to the Cosmos libraries. These libraries override the system libraries normally used in C# console programs since the resulting binary won't be running on an installation of [[Microsoft Windows]].

=== User Kit and Visual Studio ===
The Cosmos User Kit is a part of Cosmos designed to make Cosmos easier to use for developers using [[Microsoft Visual Studio]]. When installed, the user kit adds a new project type to Visual Studio, called a Cosmos Project. This is a modified version of a console application, with the Cosmos [[compiler]] and bootup stub code already added.

== Compiling a project ==
Once the code is complete, it may be compiled using .NET compiler. This converts the application from the original source code ([[C Sharp (programming language)|C#]] or otherwise) into [[Common Intermediate Language]] (CIL), the native language of .NET Framework. Next the application is run, showing the Cosmos Builder Window, which presents the developer with options which determine exactly how the project is compiled. These options include how to boot the project - via emulators such as Quick Emulator ([[QEMU]]), [[Microsoft Virtual PC|Virtual PC]], and [[VMWare]], writing to a [[disk image]] (ISO) file that can later be written to a [[CD-ROM]], or via [[Preboot Execution Environment]] (PXE) network booting - as well as debug options using Cosmos' built-in [[debugger]], and other options.

When the user has chosen their desired options, they press the Build button. This invokes the [[IL2CPU]] compiler which systematically scans through all of the applications CIL code (excluding the Cosmos compiler code), converting it into [[assembly language]] for the selected processor architecture. {{As of|2016}}, only [[x86]] is supported. Next, Cosmos invokes the selected assembler to convert this assembly language code into native [[central processing unit]] (CPU) [[opcode]]. Finally, the desired output option is activated, be this starting an emulator, starting a PXE engine, or producing an ISO [[disk image]] file from the [[binary code]] opcode.

=== Debug options ===
Cosmos offers several options as to how to deploy the resulting OS and how to [[debug]] the output.

==== Virtualization ====
[[Image:Cosmos Default_Boot.png|thumb|350px|Right|Default Cosmos boot screen as seen in QEMU.]]
Cosmos allows users to boot the [[operating system]] in an emulated environment using a [[virtual machine]]. This lets developers test the system on their own computer without having to reboot, giving the advantages of not requiring extra hardware or that developers exit their [[integrated development environment|development environment]]. Currently only VMWare is supported. [[Bochs]] support is underway. QEMU and VirtualBox are not officially supported.

==== Disk images ====
This option writes the operating system to a [[disk image]] ([[ISO image|ISO]]) file, which can be loaded into some emulators (such as [[Bochs]], [[QEMU]] or more commonly [[VMware Player|VMware]]) or written to a [[CD-ROM]] and booted on real hardware. This option also allows deploying to a USB mass storage device, such as a [[USB flash drive]], to boot on devices that may not have an optical disc drive. Because networking is not in place yet, debugging is unsupported with this deploy option.

==== PXE network boot ====
This option allows the operating system to boot on real hardware. The data is sent via a [[local area network]] (LAN) to the [[Client (computing)|client]] machine. This requires two computers: one as the client machine (on which the OS is booted) and one as the server (usually the development machine). It also requires a network connecting the two computers, a client machine with a [[network card]], and a Basic Input/Output System ([[BIOS]]) that can PXE boot. {{As of|2016}}, debugging over a network is unsupported.

=== Cosmos assembler ===
The Cosmos Project team have also created an [[assembly language#Assembler|assembler]] that is designed to eventually become the main assembler for the Cosmos system. However, the assembler is still inefficient and slow, and so the [[Netwide Assembler]] (NASM) is used instead.

== See also ==
{{Portal|Free software}}
* [[IL2CPU]]
* [[.NET Framework]]
* [[Mono (software)]]
* [[SharpOS (operating system)|SharpOS]]
* [[Singularity (operating system)|Singularity]]
* [[Phantom OS]]
* [[MOSA]]

== References ==
{{Reflist}}

== External links ==
{{Commons category|Cosmos (operating system)}}
* {{Official website|www.goCosmos.org}}
* [https://github.com/CosmosOS Project Repository at GitHub]
* [http://www.codeproject.com/KB/cs/CosmosMS5.aspx/ Cosmos Overview article at CodeProject]
* [http://tech.groups.yahoo.com/group/cosmos-dev/ Cosmos Dev Yahoo Group]
* [http://www.facebook.com/pages/Cosmos-Operating-System/10235842745 Cosmos Fan Page on Facebook]

=== News coverage ===
* [http://www.zdnet.com/blog/microsoft/cosmos-an-open-source-net-based-microkernel-os-is-born/1162 Mary Jo Foley on ZDNet - Cosmos: An open-source .Net-based microkernel OS is born ]
* [http://www.hanselman.com/blog/TheWeeklySourceCode15TinyManagedOperatingSystemEdition.aspx Scott Hanselman - Tiny Managed Operating System Edition]

[[Category:Embedded operating systems]]
[[Category:Operating system kernels]]
[[Category:Free software operating systems]]
[[Category:Beta software]]
[[Category:X86 operating systems]]
[[Category:.NET Framework]]
[[Category:Microkernel-based operating systems]]
[[Category:Microkernels]]
{{MicroKernel}}