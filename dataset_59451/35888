<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = AFVG
  |image = AFVG.jpg
  |caption = Artist's concept
}}{{Infobox Aircraft Type
  |type = Interceptor, tactical strike, reconnaissance
  |national origin = [[United Kingdom]]/[[France]]
  |manufacturer = [[British Aircraft Corporation]]/[[Dassault Aviation]]
  |designer =
  |first flight =
  |introduced =
  |retired =
  |status = Cancelled
  |primary user =
  |produced =
  |number built = None
  |unit cost =
  |developed from =
  |variants with their own articles =
  |developed into = [[Panavia Tornado]]
}}
|}

The '''AFVG''' (standing for '''Anglo-French Variable Geometry''') was a [[supersonic]] multi-role combat aircraft with a [[variable-sweep wing|variable-geometry wing]],{{#tag:ref|The term in use at the time was variable-geometry.|group=N}} being jointly developed by [[British Aircraft Corporation]] in the United Kingdom and [[Dassault Aviation]] of France.

The project was borne out of ambitions to produce a viable combat aircraft that made use of the variable-sweep wing, as well as to promote wider cooperative efforts between France and the United Kingdom. However, neither [[Dassault]] not the [[French Air Force]] were particularly keen on the AFVG; the project was further impacted by repeated specification changes and indecision for what roles that the AFVG was to be tasked with on the part of Britain. In mid 1967, British requirements settled upon adopting the AFVG for the [[Royal Air Force]] (RAF) for the strike role in the place of the cancelled [[BAC TSR-2]] strike bomber.

The project was cancelled in June 1967, when the French Government withdrew from participation. However, the cancellation was not the end of work on the proposed design. BAC modified the specification to solely satisfy [[Royal Air Force]] (RAF) needs, reconfiguring the design as the '''UKVG''' and sought out new partners to procure the aircraft, which ultimately emerged as the tri-national consortium-funded '''MRCA''' [[Panavia Tornado]], a variable-geometry wing fighter aircraft.

==Development==
===Background===
{{see also|Variable-sweep wing}}
From 1945 onwards, Britain conducted a number of studies into the properties and use of [[variable-sweep wing|variable geometry wings]].<ref name = "wood 182">Wood 1975, p. 182.</ref> The noted British engineer and inventor [[Barnes Wallis|Sir Barnes Wallis]] became exploring the concept during the [[World War II|Second World War]] and became an early pioneer and advocate for the variable geometry wing, conceiving of an aircraft consideration that lacked conventional features such as a [[vertical stabilizer|vertical stabiliser]] and [[rudder]], instead using variable geometry wings to provide primary controllability in their place. In 1946, Wallis published a paper upon this research, which was quickly hailed as being a major scientific breakthrough in the aviation industry.<ref name = "wood 182"/> Wallis proceeded to advocate for the production of an aircraft, military or civil, that would take advantage of a variable geometry wing.<ref name = "wood 182 184">Wood 1975, pp. 182, 184.</ref> The [[Ministry of Supply]] and [[Ministry of Defence]] arranged for a series of tests to demonstrate the application of the technology to [[projectile]]s, both for research purposes and a potential form of [[anti-aircraft warfare|anti-aircraft defence]]; while Wallis worked upon this research programme, he continued to promote the concept of a manned variable geometry aircraft.<ref name = "wood 184 185">Wood 1975, pp. 184–185.</ref>

In 1951, the Ministry of Supply issued [[List_of_Air_Ministry_specifications#Post_1949_Specifications_and_Air_Staff_Operational_Requirements.2FTargets_.28OR.2FASR.2FAST.29|Specification ER.110T]], which sought a piloted variable geometry aircraft that would be suitable for research flights; however, ER.110T would be cancelled without an order due to urgent demands for more conventional [[transonic]] combat aircraft.<ref name = "wood 189">Wood 1975, p. 189.</ref> At one point, Wallis examined the prospects of producing a variable geometry submission for [[List_of_Air_Ministry_specifications#Post_1949_Specifications_and_Air_Staff_Operational_Requirements.2FTargets_.28OR.2FASR.2FAST.29|Specification OR.330]], which sought a [[supersonic]] [[aerial reconnaissance]]/[[strategic bomber]] aircraft. He conceived of a large aircraft equipped with a moveable [[delta wing]] configuration, which he dubbed ''Swallow''; however, midway through scale model free-flight testing, the funding for Wallis' studies was terminated by the Ministry in June 1957.<ref name = "wood 189 191">Wood 1975, pp. 189, 191.</ref> In 1958, research efforts were revived in cooperation with the [[Mutual Weapons Development Programme]] of [[NATO]], under which all of Wallis' variable geometry research was shared with the Americans.<ref name = "wood 189 191">Wood 1975, pp. 189, 191.</ref> 

During the mid 1950s, multiple British aircraft manufacturers had become interested in harnessing variable geometry wings in their proposed designs. Amongst these design studies were a supersonic-capable derivative of the [[Folland Gnat]], and a project by [[Vickers]] to design a large variable geometry strike aircraft in response to [[List_of_Air_Ministry_specifications#Post_1949_Specifications_and_Air_Staff_Operational_Requirements.2FTargets_.28OR.2FASR.2FAST.29|Specification GOR.339]] for a [[nuclear bomb|nuclear-armed]] supersonic bomber.<ref>Wood 1975, pp. 192, 195, 197–198.</ref> In 1964, the newly formed [[British Aircraft Company]] (BAC) decided to harness Vicker's earlier variable geometry work on a new design study, designated as the BAC P.45. The conceptual BAC P.45 was designed as a 'light strike' and two-seat trainer aircraft.<ref name = "wood 199 200">Wood 1975, pp. 199, 200.</ref>{{#tag:ref|BAC ceased work on the P.45 and its follow-up P.61 project in 1965.<ref>Willox 2002, p. 35.</ref><ref>[http://www.abovetopsecret.com/forum/thread198224/pg1 "The Sepecat Jaguar and its roots."] ''Top Secret''. Retrieved: 4 February 2011.</ref>|group=N}} BAC had strongly advocated for a government order for the type to equip the [[Royal Air Force]] (RAF), being one of a number of proposed designs{{#tag:ref|The BAC P.45/P.61 joined the Folland Fo.147, Hawker Siddeley P.1173, Hunting H.155 and Vickers 593 as contenders for the AST.362 advanced fighter/trainer requirement.<ref>Hastings, David. [http://www.targetlock.org.uk/jaguar/ "SEPECAT Jaguar: Origins."] ''Target Lock,'' 2010. Retrieved: 13 February 2011.</ref>|group=N}} that were produced by several rival manufacturers to meet [[List_of_Air_Ministry_specifications#Post_1949_Specifications_and_Air_Staff_Operational_Requirements.2FTargets_.28OR.2FASR.2FAST.29|Specification AST.362]].<ref>Bowman 2007, p. 13.</ref> According to aviation author Derek Wood, in spite the P.45 design being "the obvious choice", [[Secretary of State for Defence]] [[Denis Healey]] dismissed it in favour of a prospective cooperative arrangement with [[France]] for a joint-project based on the Br.121 ECAT ("Tactical Combat Support Trainer") proposal from [[Breguet Aviation]] instead.<ref name = "wood 200 202">Wood 1975, pp. 200, 202.</ref>

===Anglo-French collaboration===
[[File:AFVG (1965).jpg|thumb|250px|Artist's concept drawing based on 1965 AFVG design]]
Starting in 1964, a series of in-depth discussions took place between the governments of France and Great Britain on prospective collaborative military aviation programs; these involved talks between Handel Davies, the co-chairman of an Anglo-French committee, and his French counterpart, Ingénieur-General Lecamus, negotiating the launch of two new military combat aircraft. According to these negotiations, the French would take the lead role in developing a new light ground-attack/trainer, while the British was to assume the leadership of a multirole fighter project.<ref name = 'guardian obit'/> This multirole aircraft was to be equipped with a variable geometry wing and was intended to perform the [[strike aircraft|strike]], reconnaissance, and [[interceptor aircraft|interceptor]] roles.<ref name = "wood 202"/>

On 17 May 1965, following on from the cancellation of the [[BAC TSR-2]] supersonic bomber, the British and French governments announced the signing of a pair of agreements to cover the two joint projects; one based on the [[Breguet Aviation]] Br.121 ECAT ("Tactical Combat Support Trainer") proposal; this would later evolve, after the cancellation of the AFVG, to become the [[SEPECAT Jaguar]]. The other was the AFVG, a larger, variable geometry [[aircraft carrier|carrier]]-capable fighter aircraft for the French Navy ([[French Naval Aviation|''Aéronavale'']]) as well as fulfilling interceptor, tactical strike and reconnaissance roles for the RAF.<ref name="dual Role.">[http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%200114.html "Anglo-French projects go ahead... The AFVG and its dual role."] ''Flight'' via ''flightglobal.com,'' 26 January 1967.</ref><ref name = "wood 202">Wood 1975, p. 202.</ref> The AFVG was to be jointly developed by BAC and [[Dassault Aviation]], the proposed M45G [[turbofan]] engine to power the aircraft was to also be jointly developed by [[SNECMA]]/[[Bristol Siddeley]].<ref name = "wood 202"/>

===Design specifications===
On 13 July 1965, the specification for the AFVG feasibility study was issued; according to Wood, the specification greatly resembled that which had been earlier issued for the cancelled TSR-2.<ref name = "wood 202 203">Wood 1975, pp. 202–203.</ref> The AFVG was to have a maximum speed of 800 knots at sea level and [[Mach number|Mach]] 2.5 at altitude. It was required to possess a minimum combat radium of 500 nautical miles, a ferry range of 3,500 nautical miles, and the nose-mounted [[airborne interception radar]] was to have a minimum range of 60 nautical miles.<ref name = "wood 203"/> Armanment was to include a pair of 30 mm [[Cannon#Aircraft_use|cannon]]s and a 2,500lb tactical [[nuclear weapon|nuclear bomb]]. However, the specification would be repeatedly re-drafted, the issuing of a definitive specification by [[Whitehall#Government_buildings|Whitehall]] was delayed until April 1966.<ref name = "wood 203"/>

Wood observed that the requirements of the specification were of a multi-role nature, akin to the [[Hawker Siddeley P.1154]] and variable geometry [[General Dynamics F-111K]].<ref name = "wood 203">Wood 1975, p. 203.</ref> In RAF service, the AFVG had originally been intended to serve as a fighter, replacing the [[English Electric Lightning]] in the interceptor mission.<ref>Gardner 1981, p. 137.</ref> However, following the decision to procure the American-built [[McDonnell Douglas F-4 Phantom II in UK service|McDonnell Douglas F-4 Phantom II]] instead, the AFVG's expected role was changed in 1966 to supplementing the F-111K{{#tag:ref|Another variable-geometry (VG) design from the US replacing the cancelled TSR-2.|group=N}} strike aircraft in replacing the [[English Electric Canberra]] and the [[V bomber]] force.<ref name="flight1967b">[http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%200359.html "AFVG Programme Details: Questions and some answers from the Commons debate on defence."] ''Flight'' via ''flightglobal.com,'' 9 March 1967. Retrieved: 29 January 2011.</ref>

The AFVG was to be powered by a pair of SNECMA/Bristol Siddeley M45G [[turbofan]]s, which were to be fed by Mirage-style half-[[shock cone]] inlets.<ref>Morris 1994, p. 137.</ref> The engine development programme contract was to be issued by the French government to a SNECMA/Bristol Siddeley [[joint venture]] company registered in France.<ref name="flight1967b"/>

===Cancellation===
For Marcel Dassault, the founder of the firm that bore his name, relinquishing leadership on a major project, essentially taking a subordinate position to BAC on the AFVG threatened his company's long-term objective of becoming a premier prime contractor for combat aircraft.<ref>Gardner 2006. pp. 214–215.</ref> After less than a year, Dassault began to actively undermine the AFVG project, working on two competing "in-house" projects: the variable geometry [[Dassault Mirage G|Mirage G]] and the [[Dassault Mirage F1|Mirage F1]].<ref>DeVore, Marc. [http://www.allacademic.com//meta/p_mla_apa_research_citation/3/1/1/3/1/pages311317/p311317-1.php "Making Collaboration Work: Examining Sub-Optimal Performance and Collaborative Combat Aircraft."] ''allacademic.com.'' Retrieved: 2 February 2011.</ref> According to Wood, both the Dassault and the French Air Force had been unenthusiastic for the project from the start, the latter wanting to pursue its own indigenous aircraft equipped with variable geometry wings, while the former had determined that the AFVG did not confirm with any of its future equipment plans.<ref name = "wood 202"/> While Britain was keen to procure a capable strike aircraft, France wanted interceptor aircraft; these design requirements of these different roles were relatively exclusive of one another.<ref name = "wood 203"/>

Britain's own set of requirements for the AFVG were complicated by the effort of trying to fit the requirements of both the RAF and the [[Royal Navy]] onto a single airframe.<ref name = "wood 203"/> Accordingly, as a measure to achieve reasonable performance, two different versions of the AFVG were called for, one being a multirole fighter equipped with [[pulse-Doppler radar]] and [[air-to-air missile]]s while the other was to be a strike aircraft with limited capability as an interceptor.<ref name = "wood 203"/> 

In June 1967, the French government announced their withdrawal from the AFVG project ostensibly on the grounds of cost.{{#tag:ref|According to aviation publication [[Flight International]], Dassault had gained valuable data on variable-geometry configurations from the AFVG programme and may have used the excuse of cost issues in order to divert funds and data to their own VG projects.<ref>[http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%200917.html "Military and Research."] ''Flight'' via ''flightglobal.com,'' 1 June 1967. Retrieved: 29 January 2011.</ref>|group=N}}<ref name = "wood 203 204">Wood 1975, pp. 203–204.</ref> The collapse of the AFVG programme was considerably troubling to the British position, having chosen to rely on Anglo-French collaboration and American-designed combat aircraft to meet its needs.<ref name = "wood 204"/>

The unilateral French decision led to a censure debate in the House of Commons.<ref>[http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%201287.html "Mr. Healey under Fire: The AFVG Censure debate."] ''Flight'' via ''flightglobal.com,'' 20 July 1967. Retrieved: 29 January 2011.</ref><ref name = "wood 203 204">Wood 1975, pp. 203–204.</ref> By 1967, when the French decided to withdraw from the AFVG programme, the [[Air Ministry]] was faced with a dilemma stemming from the imminent prospect of cancelling the F-111K, a decision that was taken in November 1967, to be formalized on 20 March 1968.<ref name="torn birt 11">Heron 2002, p. 11.</ref> Up to this point, Britain had spent £2.5 on the AFVG for practically no gains.<ref name = "wood 204">Wood 1975, p. 204.</ref> In order to justify the absence of any new strike aircraft following the failure of multiple projects to develop or procure one, Healey decided to entirely dismantle the requirement for one. Thus, in 1968, Prime Minister [[Harold Wilson]], alongside Healey, announced that British troops would be withdrawn in 1971 from major military bases in South East Asia, the [[Persian Gulf]] and the [[Maldives]], collectively known as '[[East of Suez]]'.<ref>[http://usir.salford.ac.uk/1712/1/What_Now_for_Britain.pdf "What Now for Britain?”] The State Department’s Intelligence Assessment of the “Special Relationship,” 7 February 1968 by Jonathan Colman</ref><ref name="ES-H-11">Pham P. L. [https://books.google.com/books?id=Wy343pptNB4C&pg=PP22&lpg=PP22&redir_esc=y#v=onepage&q&f=false Ending 'East of Suez': The British Decision to Withdraw from Malaysia and Singapore]</ref><ref name = "wood 204"/>

==Redesign==
With the prospect of no operational aircraft being available to fulfill the RAF's strike role, BAC decided to revamp the AFVG design, eliminating the carrier capabilities that were no longer necessary, into a larger, more strike-oriented variable geometry aircraft. Holding contracts were issued to BAC to support the project, which had been re-designated as the United Kingdom Variable Geometry (UKVG) aircraft.<ref name="torn birt 11"/><ref name = "wood 204"/> In November 1967, BAC issued a brochure on the UKVG proposal; various proposals would be issued to cover the use of multiple different engines. The quick production of a demonstrator aircraft, powered by a pair of [[Rolls-Royce/MAN Turbo RB153]] [[turbofan]] engines, was also mooted.<ref name = "wood 204"/>

While funding for the UKVG in the United Kingdom was seriously restricted, the British government sought to find partners in the form of NATO members,{{#tag:ref| Belgium, Canada, Italy, the Netherlands and West Germany were approached.<ref name = 'guardian obit'>[https://www.theguardian.com/news/2003/may/24/guardianobituaries.obituaries "Obituary: Handel Davies."] ''The Guardian,'' 24 May 2003. Retrieved: 29 January 2011.</ref>|group=N}} promoting the concept of creating and procuring a common NATO strike aircraft. In July 1968, a [[memorandum of understanding]] was signed between Britain, [[West Germany]], [[Italy]], the [[Netherlands]], [[Belgium]], and [[Canada]].<ref name = "wood 204 206">Wood 1975, pp. 204, 206.</ref> This memorandum eventually led to the launch of the multinational [[Panavia Tornado|Multi-Role Combat Aircraft]] (MRCA) project, which in turn went on to produce a variable geometry aircraft to perform strike, reconnaissance, and interception missions in the form of the Panavia Tornado.<ref name = 'guardian obit'/><ref name = "wood 206">Wood 1975, p. 206.</ref>

==Specifications==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=jet
|ref=''Project Cancelled: The Disaster of Britain's Abandoned Aircraft Projects''<ref>Wood 1986, p. 185.</ref>
|crew=Two
|capacity=
|payload main=
|payload alt=
|length main= 57.19 ft
|length alt= 17.43 m
|span main= 42.6 ft (unswept)
|span alt= 12.98 m
|height main= 17.68 ft
|height alt= 5.39 m
|area main=
|area alt=
|airfoil=
|empty weight main=
|empty weight alt=
|loaded weight main=30,000 to 50,000 lb
|loaded weight alt= 13,608 to 22,680 kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (jet)=[[SNECMA]]/[[Bristol Siddeley]] M45G
|type of jet=
|number of jets=2
|thrust main=
|thrust alt=
|thrust original=
|afterburning thrust main=
|afterburning thrust alt=
|max speed main= Mach 2.5 (1,875 mph, 3,017 km/h)
|max speed alt=
|cruise speed main=
|cruise speed alt=
|stall speed main=
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|range main= 3,500 nm (ferry)
|range alt= 6,486 km
|ceiling main=60,000 ft
|ceiling alt= 18,290 m
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
* 2× {{convert|30|mm|1|adj=on}} [[autocannon]]
* 2,500 lb (1,134 kg) tactical nuclear weapon
|avionics=
}}

==References==
===Notes===
{{reflist|group=N}}

===Citations===
{{Reflist|colwidth=30em}}

===Bibliography===
{{refbegin}}
* Bowman, Martin W. ''SEPECAT Jaguar.'' Barnsley, South Yorkshire, UK: Pen and Sword Books, 2007. ISBN 1-84415-545-5.
* Gardner, Charles. ''British Aircraft Corporation: A History''. London: B.T. Batsford Limited, 1981. ISBN 0-7134-3815-0.
* Gardner, Robert. ''From Bouncing Bombs to Concorde: The Authorised Biography of Aviation Pioneer Sir George Edwards OM.'' Stroud, Gloustershire, UK: Sutton Publishing, 2006. ISBN 0-7509-4389-0.
* Heron, Group Captain Jock. [https://web.archive.org/web/20110105085913/http://www.rafmuseum.org.uk/research/documents/Journal%2027A%20-%20Seminar%20-%20Birth%20of%20Tornado.pdf "Eroding the Requirement." ''The Birth of Tornado.''] London: Royal Air Force Historical Society, 2002. ISBN 0-9530345-0-X.
* Morris, Peter W. G. ''The Management of Projects.'' Reston, VA: American Society of Civil Engineers, 1994. ISBN 978-0-7277-1693-4.
* Wallace, William. "British External Relations and the European Community: The Changing Context of Foreign Policy-making." ''JCMS: Journal of Common Market Studies,'' Volume 12, Issue 1, September 1973, pp.&nbsp;28–52.
* Willox, Gerrie. [https://web.archive.org/web/20110105085913/http://www.rafmuseum.org.uk/research/documents/Journal%2027A%20-%20Seminar%20-%20Birth%20of%20Tornado.pdf "Tornado/MRCA: Establishing [[Collaborative partnerships|Collaborative Partnerships]] and Airframe Technology." ''The Birth of Tornado.''] London: Royal Air Force Historical Society, 2002. ISBN 0-9530345-0-X.
* Wood, Derek. ''Project Cancelled''. Macdonald and Jane's Publishers, 1975. ISBN 0-356-08109-5.
* Wood, Derek. ''Project Cancelled: The Disaster of Britain's Abandoned Aircraft Projects''. London: [[Jane's]], 2nd edition, 1986. ISBN 0-7106-0441-6.
{{refend}}

==External links==
{{Commons category|AFVG}}
* [http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%200114.html "Anglo-French projects go ahead... The AFVG and its dual role." ''Flight'', 26 January 1967]
* [http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%200113.html "Anglo-French projects go ahead... Includes an artist's concept drawing of both RAF and French Navy variants by ''Flight'' illustrator Frank Munger," ''Flight,'' 26 January 1967]

{{British Aircraft Corporation aircraft}}
{{good article}}
{{Use dmy dates|date=February 2011}}

[[Category:British Aircraft Corporation aircraft]]
[[Category:Cancelled military aircraft projects of the United Kingdom]]
[[Category:Dassault Group aircraft]]
[[Category:Cancelled military aircraft projects of France]]
[[Category:Twinjets]]
[[Category:High-wing aircraft]]
[[Category:Variable-geometry-wing aircraft]]