<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Platzer Kiebitz
 | image=Platzer Kiebitz D-MTEE.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Amateur-built aircraft]]
 | national origin=[[Germany]]
 | manufacturer=
 | designer=Michael Platzer
 | first flight=
 | introduced=
 | retired=
 | status=Plans available (2012)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]650 (plans only, 2011)
 | developed from= 
 | variants with their own articles=
}}
|}
[[File:D-MGGG 317.JPG|thumb|right|Platzer Kiebitz]]
[[File:Platzer Kiebitz B (D-MLUA) 02.jpg|thumb|right|Platzer Kiebitz B powered by a [[Volkswagen air-cooled engine]]]]
[[File:Platzer Kiebitz D-METH.jpg|thumb|right|Platzer Kiebitz]]
The '''Platzer Kiebitz''' ({{lang-en|[[Lapwing]]}}) is a [[Germany|German]] [[amateur-built aircraft]] designed by Michael Platzer and made available in the form of plans for amateur construction.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 110. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The Kiebitz features a [[strut-braced]] [[biplane]] configuration, a two-seats-in-[[tandem]] open cockpit with a small windshield, fixed [[conventional landing gear]] and a single engine in [[tractor configuration]].<ref name="WDLA11" />

The aircraft [[fuselage]] is made from welded steel tubing, with its flying surfaces covered in [[Aircraft dope|doped]] [[aircraft fabric]]. Its {{convert|7.6|m|ft|1|abbr=on}} span wing has an area of {{convert|18.3|m2|sqft|abbr=on}} and uses interplane [[Flying wires|cable bracing]]. Engines from {{convert|50|to|100|hp|kW|0|abbr=on}} can be used. The prototype used a {{convert|50|hp|kW|0|abbr=on}} [[Nissan MA engine|Nissan 12P]] automotive engine from a [[Nissan Micra]], but [[Rotax]], [[Volkswagen air-cooled engine]]s and [[Sauer]] powerplants have also been employed.<ref name="WDLA11" />

==Operational history==
Even though the aircraft can only be constructed from plans, the Kiebitz has proven popular with builders for the European [[Fédération Aéronautique Internationale]] microlight class.<ref name="WDLA11" />
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Kiebitz) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=7.6
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=18.3
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=200
|empty weight lb=
|empty weight note=
|gross weight kg=330
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|50|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Nissan MA engine|Nissan 12P]]
|eng1 type=four cylinder, liquid-cooled, [[four stroke]] automotive engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=50<!-- prop engines -->

|prop blade number=three
|prop name=composite
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=140
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=125
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=45
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=3
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=18.0
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{Commons category}}
*{{Official website|http://www.kiebitzflieger.de/}}
{{Platzer aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]