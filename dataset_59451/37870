{{good article}}
{{Infobox album| <!-- See Wikipedia:WikiProject_Albums -->
| Name        = The Big Room
| Type        = [[Album]]
| Artist      = [[M2M (band)|M2M]]
| Cover       = TheBigRoomCover.jpg
| Released    = 9 November 2001
| Recorded    = 
| Genre       = [[Pop music|Pop]], [[pop rock]]
| Length      = 36:56
| Label       = [[Atlantic Records|Atlantic]]
| Producer    = Jimmy Brolower
| Last album  = ''[[Shades of Purple]]''<br>(2000)
| This album  = '''''The Big Room'''''<br>(2001)
| Next album  = ''[[The Day You Went Away: The Best of M2M|The Day You Went Away]]'' <br />(2003)
| Misc          = {{Singles
  | Name           = The Big Room
  | Type           = studio
  | Single 1       = [[Everything (M2M song)|Everything]]
  | Single 1 date  = October 2001
  | Single 2       = What You Do About Me
  | Single 2 date  = May 2002
  }}
}}
'''''The Big Room''''' is the second and final studio album by [[Norway|Norwegian]] pop music duo [[M2M (band)|M2M]]. It was released in Asia in November 2001 and in the rest of the world in early 2002. It reached No. 16 in Norway and No. 61 in Australia. Two singles were released worldwide from the album, "[[Everything (M2M song)|Everything]]", which peaked at No. 6 in Norway and No. 27 in Australia, and "What You Do About Me", which reached No. 46 in Australia.  

To promote the album M2M appeared on an episode of the series ''[[Dawson's Creek]]'' and were selected as the opening act for [[Jewel (singer)|Jewel's]] This Way US tour. Despite critical acclaim, sales of ''The Big Room'' were considerably lower than their previous album, ''[[Shades of Purple]]'', which ''[[Verdens Gang]]'' attributed to a lacklustre marketing effort by [[Atlantic Records]]. Citing disappointing sales Atlantic chose to replace M2M as Jewel's opening act halfway through the tour, after which they returned to Norway and disbanded.

==Background and composition==
Interviewed in April 2000 following the success of their debut album ''[[Shades of Purple]]'', M2M said that while they were already writing songs for a new album, they had no plans to start recording yet.<ref name=canoe>{{cite web |archiveurl=https://web.archive.org/web/20150930181943/http://www.canoe.com/AllPopPeopleM/m2m.html |archivedate=September 30, 2015 |deadurl=no |url=http://www.canoe.com/AllPopPeopleM/m2m.html |title=M2M chats with fans on AllPop |date=3 April 2000 |work=[[Canoe.ca|Canoe.com]] |access-date=29 September 2015}}</ref> M2M either wrote or co-wrote all the songs on the ''The Big Room''.<ref name=phil>{{cite web |url=http://www.philstar.com/entertainment/142845/m2m-back-big-room |archiveurl=https://web.archive.org/web/20151006103012/http://www.philstar.com/entertainment/142845/m2m-back-big-room |archivedate= October 6, 2015 |title=M2M back with The Big Room |last=Gil |first=Baby A |date=8 December 2001 |work=[[The Philippine Star]] |accessdate= 4 October 2015 |deadurl=no}}</ref> It was recorded over six days<ref name="Singles for M2M pair">{{cite news |date=16 May 2002 |title=Singles for M2M pair |newspaper=[[Herald Sun]] |location=[[Melbourne]] |page=36 }}</ref> at the [[Bearsville Studios]] in [[New York City]]. The album takes its title from the main space at the recording facility. M2M dubbed their style "organic pop", as the album featured all real instruments, as opposed to most pop albums which use computerised instruments.<ref>{{cite news |date=9 June 2002 |title=M2M makes real music |newspaper=[[Herald Sun]] |page=F03}}</ref> Drummer [[Kenny Aronoff]] was among the artists who performed the live backing instruments.<ref name=hunter/> ''The Big Room'' took a different musical direction compared to their first album, replacing their [[bubblegum pop]] image with a more mature sound.<ref>{{cite web |url=http://www.vg.no/rampelys/musikk/musikkanmeldelser/fremdeles-uforloest/a/2099829/ |archiveurl=https://web.archive.org/web/20160304073312/http://www.vg.no/rampelys/musikk/musikkanmeldelser/fremdeles-uforloest/a/2099829/ |archivedate=March 4, 2016 |deadurl=no |title=Fremdeles uforløst |date=6 March 2002 |work=[[Verdens Gang]] |language=Norwegian |access-date=29 September 2015}}</ref>

In an interview Marion Raven said their songs were always about their experiences and thoughts, and that this album's songs were "mostly about love," though also touched on jealousy and how they see the world.<ref>{{cite web |url=http://www.girl.com.au/thebigroom.htm |archiveurl=https://web.archive.org/web/20160304211247/http://www.girl.com.au/thebigroom.htm |archivedate=March 4, 2016 |deadurl=no |title=M2M – The Big Room |work=Girl.com.au |access-date=30 September 2015}}</ref> She also said that while their first album was about finding out who they were as everything about recording was new to them, on The Big Room they knew exactly what they wanted, stating "We wanted to make a 'real' album. With a live band in the studio, with our songs and a producer we knew would listen to our ideas."<ref name=hip>{{cite web |last=Craine |first=Charlie |url=http://hiponline.com/2274/m2m-interview.html |archiveurl=https://web.archive.org/web/20151001111227/http://hiponline.com/2274/m2m-interview.html |archivedate=October 1, 2015 |deadurl=no |title=M2M – Interview |work=HipOnline |date=22 April 2002 |access-date=30 September 2015}}</ref>

The song "[[Everything (M2M song)|Everything]]" is aimed at [[Zac Hanson]], who [[Marion Raven]] dated briefly while M2M were on tour with [[Hanson (band)|Hanson]]. According to Raven, once the tour ended he never contacted her, effectively ending the relationship.<ref name=creek/> The song "Miss Popular" is about "that one popular girl in school who goes around bad-mouthing everyone" then eventually "it all comes back to her", while "Jennifer" is about wanting to dislike "the girl your boyfriend is constantly comparing you to" but not being able to as she is a nice person. Raven wrote the song "Leave Me Alone" about a guy who dumped her then wanted her back, and wrote the song "Sometimes" when she was having a really bad day and wanted to just "get it out".<ref name=hip/>

==Release and promotion==
The album was released in Asia on 9 November 2001<ref name=latest/> and M2M toured the Philippines in December to promote it.<ref name=phil/> To promote the album M2M filmed an appearance on the episode "[[List of Dawson's Creek episodes#Season 5: 2001–02|100 Light Years from Home]]" of the TV show ''[[Dawson's Creek]]'' in late February 2002.<ref>{{cite web |url=http://www.vg.no/rampelys/musikk/debut-i-bikini/a/5358313/ |archiveurl=http://archive.is/2016.12.29-021043/http://www.vg.no/rampelys/musikk/debut-i-bikini/a/5358313/ |archivedate=29 December 2016 |title=Debut i bikini |date=27 February 2002 |work=[[Verdens Gang]] |language=Norwegian |access-date=29 September 2015}}</ref> The album was released in the US on 26 February 2002 and M2M were planning to promote the album and its lead single, "Everything", starting in early March, however, they were told by [[Atlantic Records]] that all promotion in the US had been put on hold until their appearance on ''Dawson's Creek'' had aired. Frustrated, M2M returned to Norway after only one week in the US.<ref name=frustrert/> They spent three days promoting the album in Norway, four days in Spain, one day in the UK, three days in Italy, two days in Germany, two days in Sweden, one day in Denmark and one day in Finland. M2M were disappointed with the amount of time they were given to promote the album in Europe, as ideally they would have liked to spend two weeks promoting the album in the UK alone.<ref name=creek>{{cite web |url=http://www.vg.no/rampelys/tv/hver-gang-vi-moetes/her-bryter-marion-sammen-i-taarer-over-m2m-dagboken/a/10107082/ |archiveurl=https://web.archive.org/web/20160320164136/http://www.vg.no/rampelys/tv/hver-gang-vi-moetes/her-bryter-marion-sammen-i-taarer-over-m2m-dagboken/a/10107082/ |archivedate=March 20, 2016 |deadurl=no |title=Her bryter Marion sammen i tårer over M2M-dagboken |date=9 February 2013 |work=[[Verdens Gang]] |language=Norwegian |access-date=1 October 2015}}</ref> ''[[Verdens Gang]]'' attributed the lacklustre promotion of the album to Atlantic's decision to save money following the drop in record sales in the wake of the [[September 11 attacks]]. Atlantic dropped many of their artists following the attacks, and while they kept M2M, the company put little money into promoting their album.<ref>{{cite web |url=http://www.vg.no/rampelys/musikk/gaar-det-mot-opploesning/a/4119883/ |archiveurl=https://web.archive.org/web/20160304074702/http://www.vg.no/rampelys/musikk/gaar-det-mot-opploesning/a/4119883/ |archivedate=March 4, 2016 |deadurl=no |title=Går det mot oppløsning? |date=23 July 2002 |work=[[Verdens Gang]] |language=Norwegian |access-date=1 October 2015}}</ref> 

"100 Light Years from Home" aired on April 17 in the US, and M2M were scheduled to recommence promoting the single and album in the US in May, before moving onto Mexico, Australia and Japan.<ref name=frustrert>{{cite web |url=http://www.vg.no/rampelys/musikk/m2m-frustrert-over-manglende-usa-satsing/a/487933/ |archiveurl=https://web.archive.org/web/20160304080838/http://www.vg.no/rampelys/musikk/m2m-frustrert-over-manglende-usa-satsing/a/487933/ |archivedate=March 4, 2016 |deadurl=no |title=M2M frustrert over manglende USA-satsing |date=17 April 2002 |work=[[Verdens Gang]] |language=Norwegian |access-date=1 October 2015}}</ref> The album was released in Australia on 29 April;<ref name="ARIA release">{{cite news|url=http://pandora.nla.gov.au/pan/23790/20020515-0000/www.aria.com.au/Issue635.pdf|title=ARIA New Releases Albums – Week Commencing 29th April 2002|work=[[ARIA Charts]]|publisher=Australian Recording Industry Association Ltd.|accessdate=4 October 2015}}</ref> M2M arrived in Australia in mid May.<ref name="Singles for M2M pair"/> They were selected as the opening act for [[Jewel (singer)|Jewel]]'s US This Way tour, which commenced on 14 June.<ref>{{cite web | author=Barry A. Jeckell | work=[[Billboard (magazine)|Billboard]] | date= 1 March 2002 | url=http://www.billboard.com/articles/news/76600/jewel-takes-labelmate-m2m-on-the-road | title=Jewel Takes Labelmate M2M On The Road | accessdate=27 September 2015}}</ref> In mid July, M2M were removed as the opening act by Atlantic, who cited disappointing sales of ''The Big Room'', which at that time had only sold around 100,000 units in the US. Shocked and disappointed by the decision, M2M returned to Norway.<ref>{{cite web |url=http://www.vg.no/rampelys/musikk/sendt-hjem-fra-usa/a/8153400/ |archiveurl=https://web.archive.org/web/20160304094713/http://www.vg.no/rampelys/musikk/sendt-hjem-fra-usa/a/8153400/ |archivedate=March 4, 2016 |deadurl=no |title=Sendt hjem fra USA |date=21 July 2002 |work=[[Verdens Gang]] |language=Norwegian |access-date=1 October 2015}}</ref> The duo broke up later that year,<ref name="creek"/> with both Raven and Larsen pursuing solo careers.<ref>{{cite web |url=http://www.stereogum.com/1788251/30-essential-max-martin-songs/franchises/ultimate-playlist/ |archiveurl=https://web.archive.org/web/20160819012007/http://www.stereogum.com/1788251/30-essential-max-martin-songs/franchises/ultimate-playlist/ |archivedate=19 August 2016 |deadurl=no |title=30 Essential Max Martin Songs |last=Nelson |first=Michael |date=26 March 2016 |work=[[Stereogum]] |access-date=22 September 2016}}</ref>

===Singles===
"Everything" was the first single from the album. A music video for the single, directed by [[Chris Applebaum]],<ref>{{cite web |url=http://www.vh1.com/video/m2m/15166/everything.jhtml |archiveurl=https://web.archive.org/web/20151001205417/http://www.vh1.com/video/m2m/15166/everything.jhtml |archivedate=1 October 2015 |deadurl=yes |title=M2M – Music Videos |work=[[VH1]] |access-date=1 October 2015}}</ref> was filmed in [[Los Angeles]] in early September 2001. The single was released in Asia in early October 2001 and the US in January 2002.<ref name=latest>{{cite web |url=http://www.m2mmusic.com/news/ |archiveurl=https://web.archive.org/web/20020603202625/http://www.m2mmusic.com/news/ |archivedate=3 June 2002 |title=Latest News |work=M2M Music}}</ref> It reached No. 6 in Norway,<ref>{{cite web |url=http://norwegiancharts.com/search.asp?search=M2M&cat=s |archiveurl=http://archive.is/2012.06.29-131143/http://norwegiancharts.com/search.asp?search=M2M&cat=s |archivedate=June 29, 2012 |deadurl=no |title=M2M |work=norwegiancharts.com |publisher=Hung Medien |accessdate=30 September 2015}}</ref> No. 27 in Australia<ref name=oz>{{cite web |url=http://australian-charts.com/search.asp?search=M2M&cat=s |title=M2M |work=australian-charts.com |publisher=Hung Medien |accessdate=30 September 2015}}</ref> and No. 44 in New Zealand.<ref>{{cite web |url=http://charts.org.nz/search.asp?search=M2M&cat=s |archiveurl=http://archive.is/2012.06.29-131143/http://australian-charts.com/search.asp?search=M2M&cat=s |archivedate=June 29, 2012 |deadurl=no |title=M2M |work=charts.org.nz |publisher=Hung Medien|accessdate=30 September 2015}}</ref>  The second single was "What You Do About Me". It reached No. 46 in Australia.<ref name=oz/> The track "Don't" was released in the US as a promo single.<ref name=dont/>

==Critical reception==
{{Album ratings
| rev1      = [[AllMusic]]
| rev1Score = {{rating|4.5|5}}<ref name=all/>
| rev2 = ''[[Billboard (magazine)|Billboard]]''
| rev2Score = Favourable<ref name=chuck/>
| rev3      = ''[[Entertainment Weekly]]''
| rev3Score = B-<ref name=ew/>
| rev4 = Groove.no
| rev4Score = {{rating|5|7}}<ref name=groove/>
| rev5 = ''[[Herald Sun]]''
| rev5Score = Favourable<ref name=tye/>
| rev6 = ''[[The Massachusetts Daily Collegian]]''
| rev6Score = Favourable<ref name=daily/>
| rev7 = ''[[The Philippine Star]]''
| rev7Score = Favourable<ref name=phil/>
| rev8   = ''[[USA Today]]''
| rev8Score = {{rating|2.5|4}}<ref name=usa/>
| rev9 = ''[[The Village Voice]]''
| rev9Score = Favourable<ref name=hunter/>
}}
The album received a positive critical response. James Hunter of ''[[The Village Voice]]'' called the album a "pop masterpiece," praising the singing, songwriting and decision to use live backing instruments.<ref name=hunter>{{cite news |last=Hunter |first=James |date=16 April 2002 |title=Everyone Hates Miss Popular |newspaper=[[The Village Voice]] |location=[[New York City]]}}</ref> [[Chuck Taylor (writer and editor)|Chuck Taylor]] from ''[[Billboard (magazine)|Billboard]]'' said the album featured "a decidedly mature acoustic pop/rock signature, belying the tender age of the act's two singers",<ref name=chuck>{{cite journal |last=Taylor |first=Chuck |authorlink=Chuck Taylor (writer and editor) |date=4 May 2002 |title=M2M Resuscitation |url=https://books.google.com.au/books?id=5xAEAAAAMBAJ&pg=RA1-PA70 |journal=[[Billboard (magazine)|Billboard]] |page=70 |access-date=1 October 2015}}</ref> and also gave favourable reviews of both the singles "Everything"<ref>{{cite journal |last=Taylor |first=Chuck |authorlink=Chuck Taylor (writer and editor) |date=23 February 2002 |title=M2M Everyday |url=https://books.google.com.au/books?id=JBAEAAAAMBAJ&pg=PA22 |journal=[[Billboard (magazine)|Billboard]] |page=22 |access-date=1 October 2015}}</ref> and "Don't".<ref name=dont>{{cite journal |last=Taylor |first=Chuck |authorlink=Chuck Taylor (writer and editor) |date=8 June 2002 |title=M2M Everyday |url=https://books.google.com.au/books?id=-hAEAAAAMBAJ&pg=PA36&lpg=PA36 |journal=[[Billboard (magazine)|Billboard]] |page=36 |access-date=1 October 2015}}</ref> John Berge from Norwegian music website Groove.no gave the album five out of seven stars, praising the album's harmonies and saying it was guaranteed to be a hit with young people.<ref name=groove>{{cite web |url=http://www.groove.no/anmeldelse/84574241/the-big-room-m2m |archiveurl=https://web.archive.org/web/20160304050339/http://www.groove.no/anmeldelse/84574241/the-big-room-m2m |archivedate=March 4, 2016 |deadurl=no |title=The Big Room |last=Berge |first=John |work=[[:no:Groove.no|Groove.no]] |date=12 May 2002 |access-date=2 October 2015}}</ref> ''[[Entertainment Weekly]]'' gave the album a B-, favourably comparing the uptempo tracks to Hanson though stating M2M's vocal range could not handle the album's ballads.<ref name=ew>{{cite web |url=http://www.ew.com/article/2002/03/15/big-room |archiveurl=https://web.archive.org/web/20151004152059/http://www.ew.com/article/2002/03/15/big-room |archivedate=October 4, 2015 |deadurl=no |title=The Big Room |date=15 March 2002 |work=[[Entertainment Weekly]] |access-date=2 October 2015}}</ref> 

Elysa Gardner from ''[[USA Today]]'' gave the album two and a half stars out of four. Saying the duo deserved an "A for effort", she praised their strong vocals and instruments though labelled some of the lyrics as cliched teenage melodrama.<ref name=usa>{{cite web |url=http://pqasb.pqarchiver.com/USAToday/doc/408865506.html?FMT=ABS&FMTS=ABS:FT&type=current&date=Mar%2012,%202002&author=Steve%20Jones;Elysa%20Gardner;Brian%20Mansfield&pub=USA%20TODAY&edition=&startpage=D.10&desc='Full%20Moon'%20puts%20Brandy%20into%20orbit%20;%20Bobby%20McFerrin,%20M2M,%20Killa%20Beez%20also%20blast%20off |archiveurl=http://archive.is/2016.11.12-104733/http://pqasb.pqarchiver.com/USAToday/doc/408865506.html?FMT=ABS&FMTS=ABS:FT&type=current&date=Mar%2012,%202002&author=Steve%20Jones;Elysa%20Gardner;Brian%20Mansfield&pub=USA%20TODAY&edition=&startpage=D.10&desc='Full%20Moon'%20puts%20Brandy%20into%20orbit%20;%20Bobby%20McFerrin,%20M2M,%20Killa%20Beez%20also%20blast%20off |archivedate=November 12, 2016 |deadurl=no |first1=Steve |last1=Jones |first2=Elysa |last2=Gardner |first3=Brian |last3=Mansfield |title='Full Moon' puts Brandy into orbit ; Bobby McFerrin, M2M, Killa Beez also blast off |date=12 March 2002 |work=[[USA Today]] |access-date=2 October 2015}}</ref> ''[[The Philippine Star]]'' said the album "retain[ed] the same young, sparkling kind of effervescene that made the group such a huge success".<ref name=phil/> ''[[The Massachusetts Daily Collegian]]'' gave a favourable review, calling the album "a crystalline example of the heights that teen pop can accomplish".<ref name=daily>{{cite web |url=http://dailycollegian.com/2002/04/02/m2m-refreshingly-different-teen-pop/ |archiveurl=https://web.archive.org/web/20161009151353/http://dailycollegian.com/2002/04/02/m2m-refreshingly-different-teen-pop/ |archivedate=9 October 2016 |title=M2M – Refreshingly Different Teen Pop |date=2 April 2002 |work=[[The Massachusetts Daily Collegian]] |access-date=11 October 2016 |deadurl=no}}</ref>

Karen Tye spoke favourably of the album, saying they had "ditched the cutesy voices and effervescent ditties" and replaced it with acoustic pop and rock which occasionally bordered on [[Country music|country]]. She praised M2M for using "down-to-earhtunes and thoughtful lyrics" instead of revealing outfits to propel them into the limelight, unlike many of their pop counterparts.<ref name=tye>{{cite news |last=Tye |first=Karen |date=28 April 2002 |title=Pop |newspaper=[[Herald Sun]] |location=[[Melbourne]]}}</ref> Conversely, Stephen Thomas Erlewine from [[AllMusic]] was critical of M2M's new image in the album artwork. Erlewine said they had ditched their sweet and innocent look from ''Shades of Purple'', for "lots of makeup, crimped hair [and] fairly low-cut tops". He gave the album four and a half stars out of five, concluding "If M2M needs to play up their good looks to sell an album this pleasing, so be it. This is an album that deserves to find its audience, even through tactics as blatant as that."<ref name=all>{{cite web |url=http://www.allmusic.com/album/the-big-room-mw0000217419 |archiveurl=http://archive.is/2016.11.12-105015/http://www.allmusic.com/album/the-big-room-mw0000217419 |archivedate=November 12, 2016 |deadurl=no |title=AllMusic Review by Stephen Thomas Erlewine |last=Erlewine |first=Stephen Thomas |work=[[AllMusic]] |access-date=2 October 2015}}</ref>

==Track listing==
{{Tracklist
| total_length    = 36:56
| title1          = [[Everything (M2M song)|Everything]]
| writer1         = [[Marion Raven]], [[Marit Larsen]], Jimmy Bralower, Peter Zizzo
| length1         = 4:11
| title2          = Jennifer
| writer2         = Marion Raven, Marit Larsen
| length2         = 3:08
| title3          = Don't
| writer3         = Marion Raven, Marit Larsen
| length3         = 3:27
| title4          = Payphone
| writer4         = Marion Raven, Marit Larsen
| length4         = 3:16
| title5          = What You Do About Me
| writer5         = Marion Raven, Marit Larsen, Jimmy Bralower, Peter Zizzo
| length5         = 3:05
| title6          = Love Left for Me
| writer6         = Marion Raven, Marit Larsen
| length6         = 4:15
| title7          = Miss Popular
| writer7         = Marion Raven, Marit Larsen, Peter Zizzo
| length7         = 3:06
| title8          = Wanna Be Where You Are
| writer8         = Marion Raven
| length8         = 3:26
| title9          = Leave Me Alone
| writer9         = Marion Raven
| length9         = 3:08
| title10          = Sometimes
| writer10         = Marion Raven, Marit Larsen
| length10         = 2:53
| title11          = Eventually
| writer11         = Marion Raven, Marit Larsen
| length11         = 3:01
}}

==Credits and personnel==
Credits are taken from AllMusic.<ref>{{cite web |url=http://www.allmusic.com/album/the-big-room-mw0000217419/credits |archiveurl=https://web.archive.org/web/20151011224114/http://www.allmusic.com/album/the-big-room-mw0000217419/credits |archivedate=October 11, 2015 |title=M2M – The Big Room – Credits ||work=[[AllMusic]] |access-date=December 17, 2016}}</ref>

{| style="width:100%;"
! scope="col" style="text-align:left; width:25%;" | M2M
! scope="col" style="text-align:left; width:25%;" | Additional musicians
! scope="col" style="text-align:left; width:25%;" | Production
! scope="col" style="text-align:left; width:25%;" | Artwork and management
|-
| style="vertical-align:top;" |
*[[Marit Larsen]] – vocals, electric guitar, composer
*[[Marion Raven]] – vocals, acoustic guitar, composer
| style="vertical-align:top;" |
*[[Kenny Aronoff]]  – drums, percussion
*Jimmi K. Bones – acoustic guitar, electric guitar
*[[Miklós Malek (musician)|Miklós Malek]] – keyboard programming
*Dean Sharp – percussion
*[[Tom Wolk]] – bass, acoustic guitar, electric guitar, organ
| style="vertical-align:top;" |
*Larry Alexander – engineer, [[mixing engineer|mixing]], string engineer
*Jill Dell'Abate  – string contractor
*Femio Hernández – assistant engineer
*John Holbrook – engineer, mixing
*[[Ted Jensen]] – mastering
*[[Craig Kallman]] – executive producer
*[[Tom Lord-Alge]] – mixing
*Rob Mathes – string arrangements
*Nick Romei – pre-production
*Tron Syversen – engineer
*Peter Zizzo – composer, producer
| style="vertical-align:top;" |
*Aliya Best – art direction, design
*Jimmy Bralower – [[Artists and repertoire|A&R]]
*Skye Peyton – photography
*James "Big Jim" Wright  – photography
|}

==Charts==
{|class="wikitable"
!align="left"|Chart (2002)
!align="center"|Peak<br>position
|-
|align="left"|Australia ([[ARIA Charts|ARIA]])<ref name="ARIA">{{cite news|url=http://pandora.nla.gov.au/pan/23790/20020627-0000/www.aria.com.au/Issue638.pdf|title=ARIA Top 100 Albums – Week Commencing 20th May 2002|date=15 April 2002|work=[[ARIA Charts]]|publisher=Australian Recording Industry Association Ltd.|accessdate=1 October 2015}}</ref>
|align="center"|61
|-
|align="left"|Norway ([[VG-lista]])<ref name="Norway">{{cite news|url=http://norwegiancharts.com/showitem.asp?interpret=M2M&titel=The+Big+Room&cat=a |archiveurl=http://archive.is/2012.07.13-171707/http://norwegiancharts.com/showitem.asp?interpret=M2M&titel=The+Big+Room&cat=a |archivedate=July 13, 2012 |deadurl=no |title=M2M – The Big Room (Album)|work=Norwegiancharts.com|publisher=IFPI Norway|accessdate=4 October 2015}}</ref>
|align="center"|16
|}

==References==
{{Reflist|30em}}

{{M2M}}

{{DEFAULTSORT:Big Room, The}}
[[Category:2001 albums]]
[[Category:M2M (band) albums]]