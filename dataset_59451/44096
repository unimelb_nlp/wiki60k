{{other uses of|Star Air}}
{{Infobox Airline
| airline        = Star Air
| logo           = <!--Star Air logo-->
| logo_size      = 250
| fleet_size     = 12
| destinations   =
| IATA           = S6
| ICAO           = SRR
| callsign       = Whitestar
| parent         = [[Maersk]]
| company_slogan =
| founded        = 1 September 1987
| headquarters   = [[Dragør]], [[Denmark]]
| key_people     = Søren Graversen ([[CEO]])
| hubs           =
| secondary_hubs =
| focus_cities   =
| bases          = [[Cologne Bonn Airport]]
| frequent_flyer =
| lounge         =
| alliance       =
| website        = [http://www.starair.dk/ starair.dk]
}}

'''Star Air [[Aktieselskab|A/S]]''' is a [[Denmark|Danish]] [[cargo airline]] and part of Danish [[business conglomerate]] [[Maersk]]. It operates a fleet of twelve [[Boeing 767]] cargo aircraft. Several of these are on contract to [[United Parcel Service]] (UPS) and operate out of [[Cologne Bonn Airport]], [[Germany]]. Star Air is headquartered in [[Dragør]], Denmark, at the premises of [[Copenhagen Airport]]. 

The airline was established in 1987 with the purchase of Alkair's Fokker 27 operation. Originally the airline had three [[Fokker F27 Friendship]]s, which later increased to four. These were used for both passenger and cargo operations. One was involved in a fatal accident in 1988. Star Air secured a last-minute deal with UPS in 1993, allowing it to start operations out of Cologne/Bonn with [[Boeing 727]]s. Star Air became a subsidiary of now defunct [[Maersk Air]] in 1993. The Fokkers were retired in 1996—after which the airline had exclusively flown for UPS. [[Boeing 757]]s were introduced in 2001. From 2005 to 2006 the airline replaced its entire fleet, introducing the current 767s. Meanwhile, Maersk Air was sold to [[Sterling Airlines]] and ownership resumed to the Maersk Group. 

In 2013 the airline had a revenue of DKK&nbsp;813 million and a net profit of DKK&nbsp;69 million. It employed 119 pilots, 41 mechanics and 36 administrative staff.<ref>{{cite web |url=http://www.starair.dk/facts.asp |title=Facts & Numbers |publisher=Star Air |accessdate=22 September 2014}}</ref>

== History ==
===Early years===
[[File:OY-SRA Fk27 Star Air (4157761315).jpg|thumb|One of the original Star Air [[Fokker F27 Friendship]]s]]
[[File:Boeing 727-22C(QF), United Parcel Service - UPS (Star Air) AN0780472.jpg|thumb|A Star Air [[Boeing 727|Boeing 727-200]] operated for [[UPS Airlines|UPS]] in 2001]]
[[File:United Parcel Service Star Air Boeing 757-24APF.jpg|thumb|Star Air [[Boeing 757-200F]] in 2004]]
The Maersk Group entered the airline industry when it established Maersk Air in 1979.<ref>Ellemose: 50</ref> Given the nature of the mother company, Maersk Air looked at the possibilities to operate the cargo segment. The airline started operation with three F27s, mounted with cargo doors for easy conversion to cargo configuration. Oriental Air Transport Services, a cargo handling company based at Kastrup, was bought in 1971. The airline aimed at buying a [[Boeing 747]], but restrictions on freight caused these plans to be abandoned.<ref name=e54>Ellemose: 54</ref>

Until 1987 the rules in Denmark only permitted SAS to operate freight charters. The only exception was if the entire shipment had a single sender and recipient. This made filling an entire cargo plane uneconomical and resulted in Maersk abandoning its cargo plans.<ref name=e54 /> Maersk Air Cargo was founded in 1982, but only acted as at cargo division. Due to the regulations, it only acted as a ground handling agent for overseas airlines, the largest being [[Cathay Pacific]].<ref name=e100>Ellemose: 100</ref>

When the deregulation took effect in 1987, the Maersk Group immediately established Star Air as a subsidiary directly under the corporation. Incorporated on 1 September 1987, it bought an existing hangar on the south sector of Copenhagen Airport.<ref name=e100 /> The ground facilities, an organization and an [[air operator's certificate]] was taken over through the purchase of Alkair.<ref name=history>{{cite web |url=http://www.sac-crew.dk/public/index_dk.php |title=Velkommen til SAC – Star Air Cockpitassociation |publisher=Star Air Cockpitassociation |accessdate=22 September 2014 |language=Danish}}</ref>

Three Fokker F-27-600s were leased and converted to combi-freighters. These could be converted from freighter to passenger configuration in half an hour. Star Air originally had a mix of operations. One part was corporate charters, one was wet leasing to other airlines, one was charter and domestic operations for Maersk Air, and finally it conducted European hauls for freight companies, including [[Federal Express]], [[TNT N.V.|TNT]] and UPS.<ref name=e100 />

By 1990 the airline was operating four F-27s and had a revenue of DKK&nbsp;66 million. But with increased competition, the airline made a loss of DKK&nbsp;10 million in 1991. To cut costs the operations were transferred to a new legal entity, Star Air I/S, which was then placed under Maersk Air.<ref name=e114>Ellemose: 114</ref> Lack of sufficient cargo volumes resulted in Star Air carrying out passenger flights as well, on [[wet lease]] basis.<ref name=history />

In 1991 UPS announced a tender to find a European partner. They did not themselves hold the rights to fly intra-European flights and needed a European airline to fly services out of the hub at Cologne Bonn Airport. The two main contenders were Star Air and Sterling Airways, another Danish airline. Sterling had two main advantages: they already operated the Boeing 727 and they were approved by the [[Federal Aviation Administration]]. The latter would allow them to operate aircraft which were owned by UPS and registered in the United States.<ref name=e114 />

Sterling fell into financial distress in 1993 and months before the contract was to take effect its credits were cut off. UPS backed out of the deal and instead approached Star Air. An agreement was signed on 22 October 1993, with services commencing ten days later.<ref name=e114 /> This could be done because Star Air turned to Sterling employees who had been working on the preparations. People who had been employed by Sterling were instead hired by Star Air, giving them access to pilots, engineers and administrators. The initial contract involved flights to [[Milan]], [[Rome]], [[Zaragoza]] and [[Porto]].<ref name="e114"/> The operations were gradually expanded and soon the airline was operating four 727s.<ref name=history />

The same year Star Air started the process of retiring the Fokkers. Falling prices for smaller cargo aircraft made this part of the operation unprofitable. At the same time a closer integration with Maersk Air was carried out, in which the two companies received a common administration, operations center and navigational division. The Fokker F27s were retired in 1996 and since Star Air has solely flown for UPS.<ref name=history /> Star Air had a revenue of DKK&nbsp;82 million in 1997, which rose to DKK&nbsp;159 million in 2002. Its profits in this period varied between DKK&nbsp;12 and 20 million.<ref name=e285>Ellemose: 285</ref>

A total of eight 727s entered service with Star Air;<ref name=fleet>{{cite web |url=http://oy-reg.dk/register/ |title=Danish Civil Aircraft Register |publisher=OY-REG |accessdate=18 September 2014}}</ref> seven were of the larger -200 series and one, introduced in 1996, of the smaller -100 series.<ref name=oyupb>{{cite web |url=http://oy-reg.dk/register/4024.html |title=OY-UPB |publisher=OY-REG |accessdate=18 September 2014}}</ref> Two aircraft were taken into service in 1993, one more in 1994, two more in 1996, one more in 1997 and the last in 2001.<ref name=fleet /> 

===Development since the 2000s===
Four Boeing 757-200s were introduced in 2001 and 2002, and the number of 727s cut to four.<ref name=e285 />

After signing a new contract with a duration until 2015,<ref name=history /> Star Air carried out a full fleet replacement in 2005 and 2006. All the 727s and 757s were returned and instead eleven Boeing 767-200s were leased. This gave a major hike in revenue, increasing from DKK&nbsp;106 million in 2004 to DKK&nbsp;653 million in 2007. Profits increased from DKK&nbsp;7 to 58 million.<ref name=e285 /> Maersk Air was sold to Sterling Airlines in 2005. Star Air was kept out of the deal and instead it made a subsidiary directly under the Maersk Group. It was also given the responsibility for operating the Maersk Group's corporate jet, a [[Canadair Challenger 600]].<ref name=e285 /> Star Air took delivery of a Boeing 767-300 in 2014.<ref name=oysrs>{{cite web |url=http://oy-reg.dk/register/6296.html |title=OY-SRS |publisher=OY-REG |accessdate=18 September 2014}}</ref>

==Operations==
Star Air operates scheduled cargo flights on behalf of [[UPS Airlines]] out of its base at [[Cologne Bonn Airport]] as well as further freight operations on behalf of its parent [[Maersk]] and other customers on a charter basis.<ref name="officialabout">[http://www.starair.dk/about.html starair.dk - About] retrieved 4 March 2017</ref><ref>{{cite web |url=http://www.starair.dk/about.asp |title=About Us |publisher=Star Air |accessdate=22 September 2014}}</ref>

==Fleet==
===Current fleet===
[[File:Star_Air_(Maersk)_767-200F_OY-SRL.jpg|thumb|Star Air [[Boeing 767|Boeing 767-200SF]]]]
As of March 2017, the Star Air fleet consists of the following aircraft:<ref name="officialabout">[http://www.starair.dk/about.html starair.dk - About] retrieved 4 March 2017</ref><ref name="planespotters">[https://www.planespotters.net/airline/Star-Air planespotters.net - Star Air Fleet Details and History] retrieved 4 March 2017</ref>

{| class="toccolours" border="1" cellpadding="3" style="border-collapse:collapse"
|+ '''Star Air fleet'''
|- style="background:#008080;"
!<span style="color:white;">Aircraft
!<span style="color:white;">In Service
!<span style="color:white;">Orders
!<span style="color:white;">Notes
|-
|[[Boeing 767|Boeing 767-200SF]]
| style="text-align:center;"|11
| style="text-align:center;"|&mdash;
|
|-
|[[Boeing 767|Boeing 767-300SF]]
| style="text-align:center;"|1
| style="text-align:center;"|1<ref name="planespotters"/>
|
|-
!Total
!1
!1
|
|}

===Historic fleet===
{| class="wikitable plainrowheaders sortable"
|+Star Air former fleet
|-
!scope=col | Aircraft
!scope=col | Retired
!scope=col | Intro.
!scope=col | Retired
!scope=col class=unsortable | Ref.
|-
!scope=row | [[Boeing 727|Boeing 727-100]] || align=center | 8 || 1993 || 2004 || align=center | <ref name=history /><ref name=oyupb />
|-
!scope=row | [[Boeing 757|Boeing 757-200PF]] || align=center | 4 || 2001 || 2005 || align=center | <ref name=e285 />
|-
!scope=row | [[Bombardier Challenger 600 series|Bombardier Challenger 604]] || align=center | 1 || 2005 || 2015 || align=center | <ref>{{cite web |url=http://oy-reg.dk/register/3435.html |title=OY-MMM |publisher=OY-REG |accessdate=18 September 2014}}</ref>
|-
!scope=row | [[Fokker F27 Friendship|Fokker F27-100 Friendship]] ||  align=center | 5 || 1987 || 1996 || align=center | <ref name=e101 />
|}

==Accidents and incidents==
* Star Air's only hull-loss accident took place on 26 May 1988. The [[Fokker F-27]] OY-APE flew from Copenhagen to Billund, where to loaded cargo and continued onwards to [[Hannover Airport]] and [[Nuremberg Airport]]. The cargo was improperly distributed so that the aircraft became aft-heavy. Although the captain was aware of this situation, he did not relay the information to the first officer, who was pilot flying. He therefore failed to correlate for this during the landing at Hannover, having the flaps set incorrectly. When the first officer power up for a go-around, the load shifted, the aircraft pitched up and the aircraft crashed. Both pilots were killed.<ref name=e101>Ellemose: 101</ref><ref>{{cite web |url=http://aviation-safety.net/database/record.php?id=19880526-0 |title=Thursday 26 May 1988 |publisher=[[Aviation Safety Network]] |accessdate=21 September 2014}}</ref>

==References==
{{reflist}}

==Bibliography==
* {{cite book |last=Ellemose |first=Søren |title=Luftens helte |year=2009 |publisher=Jyllands-Posten Forlag |location=Aarhus |isbn=978-87-7692-197-2 |language=Danish}}

==External links==
{{commonscat-inline|Star Air (Maersk)}}
*[http://www.starair.dk/ Official website]

{{Airlines of Denmark}}
{{Maersk}}
{{UPS}}
{{Portalbar|Aviation|Companies|Denmark}}

[[Category:Airlines of Denmark]]
[[Category:Cargo airlines]]
[[Category:Airlines established in 1987]]
[[Category:1987 establishments in Denmark]]
[[Category:Companies based in Dragør Municipality|Dragor]]
[[Category:Maersk]]
[[Category:Maersk Air]]