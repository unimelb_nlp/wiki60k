{{EngvarB|date=January 2014}}
{{Use dmy dates|date=January 2014}}
{{Infobox company
 | name             = Allcargo Logistics Ltd – <br />Part of The Avvashya Group
 | logo             = [[File:Logo of Allcargo Logistics.png]]
 | type             = Public company
 |traded_as         ={{NSE|ALLCARGO}}<ref>[https://web.archive.org/web/20160304024457/http://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/GetQuote.jsp?symbol=ALLCARGO] </ref><br />{{BSE|532749}}<ref>{{cite web|url=http://www.bseindia.com/stock-share-price/allcargo-logistics-ltd/allcargo/532749/ |title=Stock Share Price allcargo logistics ltd &#124; Get Quote allcargo &#124; BSE |publisher=Bseindia.com |date= |accessdate=2015-08-11}}</ref>
 | foundation       = [[Mumbai]], India (1993)
 | founder          = [[Shashi Kiran Shetty]]
 | location_city    = [[Mumbai]], [[Maharashtra]] (India)
 | location_country = India
 | area_served      = Worldwide
 | key_people       = Shashi Kiran Shetty ([[Executive Chairman]])  
 | industry         = [[Third-party logistics|Logistics]] and transportation
 | products         = 
 | services         = [[Multimodal transport]]  
 | revenue          = INR 56.18&nbsp;billion (USD 900 million)<ref>[http://www.allcargologistics.com/investor-relations/overview.aspx.html ]{{dead link|date=August 2015}}</ref>
 | operating_income = 
 | net_income       = 
 | assets           = 
 | equity           = 
 | num_employees    = 8000 (2012)
 | parent           = 
 | divisions        =  
 | caption           = Ingenuity in motion
 | homepage         = {{URL|http://www.allcargologistics.com/}}
 | Acquisitions     = Econocaribe Consolidators (2013), ECU Line (2005–06), Hindustan Cargo Ltd. (2006), Hong Kong-based Companies (2010), MHTC Logistics Pvt. Ltd (2012)
 | footnotes        = 
 | intl             = Yes
}}

'''Allcargo Logistics Ltd''', part of the Avvashya Group, is a logistics firm headquartered in India. Its services comprise global [[multimodal transport]] operations ([[non-vessel-operating common carrier]], [[less than container load]], and [[full container load]]), pan India container [[freight station]]s, inland container depots, [[third-party logistics]], warehousing, ship owning and chartering. The company operates across 90 countries through 200 offices. As one of India’s largest publicly listed logistics companies, Allcargo Logistics has a consolidated turnover of INR 56.18&nbsp;billion (approx USD 900 million) as of March 2015.<ref>{{cite web|url=http://www.allcargologistics.com/investor-relations/overview.aspx |title=Import Services from Air Freight Forwarder in Vietnam |publisher=Allcargo Logistics |date= |accessdate=2015-08-11}}</ref><ref>{{cite web|url=http://www.moneycontrol.com/news/results-boardroom/ecu-contributed-rs-317-crore-to-topline-allcargo-global_230969.html |title=Moneycontrol.com |publisher=Moneycontrol.com |date= |accessdate=2015-08-11}}</ref><ref>{{cite web|author=N. K. Kurup |url=http://www.thehindubusinessline.com/industry-and-economy/logistics/allcargo-sees-growth-potential-in-asian-market/article4846986.ece |title=Allcargo sees growth potential in Asian market &#124; Business Line |publisher=Thehindubusinessline.com |date=2013-06-24 |accessdate=2015-08-11}}</ref><ref name="thehindubusinessline">{{cite web|author= |url=http://www.thehindubusinessline.com/industry-and-economy/logistics/allcargo-logistics-begins-corporate-recast/article2946747.ece |title=AllCargo Logistics begins corporate recast &#124; Business Line |publisher=Thehindubusinessline.com |date=2012-02-29 |accessdate=2015-08-11}}</ref><ref>{{cite web|url=http://businesstoday.intoday.in/story/indias-new-business-families-shashi-kiran-shetty-family/1/14382.html |title=Riding the tide: The Shettys of Allcargo - Business Today- Business News |publisher=Businesstoday.intoday.in |date=2011-04-17 |accessdate=2015-08-11}}</ref>

== History ==

In 1993, Chairman of Allcargo Logistics Mr. [[Shashi Kiran Shetty]] founded Allcargo Logistics as a cargo handling operator at [[JNPT|Mumbai port]]. Initially Allcargo Logistics was a shipping agency house and provided freight forwarding services.

In 1995, ECU Line, an Antwerp-based non-vessel operating common carrier (NVOCC), appointed Allcargo as their India agent. In 2003, the company started its first container freight station (CFS) at the largest Indian port, [[Jawaharlal Nehru Port Trust]] (JNPT) in Mumbai. In 2007, it started two new CFSs at [[Chennai]] (in [[Tamil Nadu]]) and [[Mundra]] (in [[Gujarat]]) ports followed by a second CFS at JNPT in the year 2012. In 2006, private equity firm New Vernon Capital Fund acquired 6.42% stake in Allcargo. In 2008, Blackstone GPV Capital Partners picked up a 6% stake in the company which was later increased to 14.99% in September 2009 by converting warrants worth USD 23 million which the global private equity fund subscribed in February 2008.

== Acquisitions ==

'''ECU Line''' (2005–06): Allcargo's first acquisition was of the Belgium-based ECU Line, spanning three stages from 2005-06. Allcargo acquired 33.8% stake in ECU Line in June 2005, increased its stake to 49.9% in Jan 2006 with an option to increase the stake further, and acquired the remainder in Jun 2006, making Allcargo Logistics one of the largest NVOCC in the world.  ECU Line’s revenues at the time of the acquisition were 4.3 times the revenues of Allcargo Logistics.<ref name="thehindubusinessline" /><ref name="articles.economictimes.indiatimes">{{cite web|author= |url=http://articles.economictimes.indiatimes.com/2012-03-02/news/31116753_1_allcargo-global-ecu-line-vessels |title=Allcargo Global to demerge two business divisions, buy new vessels - timesofindia-economictimes |publisher=Articles.economictimes.indiatimes.com |date=2012-03-02 |accessdate=2015-08-11}}</ref>
  
'''Hindustan Cargo Ltd'''. (2006): A former subsidiary of travel and forex major Thomas Cook, Hindustan Cargo is predominantly engaged in air freight forwarding and custom clearance. The company was acquired by Allcargo Logistics in 2006 for an undisclosed amount.<ref>{{cite web|url=http://www.supplychainleaders.com/freight_forwarding/hindustan-cargo-ltd-acquired-by-allcargo-logistics/822/ |title=Hindustan Cargo Ltd (Acquired by Allcargo Logistics) |publisher=Supply Chain Leaders |date= |accessdate=2015-08-11}}</ref>

'''Hong Kong-based Companies''' (2010):Allcargo acquired two Hong Kong-based companies with operations in China (one in Shanghai with 75% stake and the other in Ningbo with 100% stake) for USD 22 million.<ref>{{cite web|url=http://www.thehindu.com/business/companies/allcargo-global-logistics-acquires-two-hong-kong-firms/article1487091.ece |title=Allcargo Global Logistics acquires two Hong Kong firms |publisher=The Hindu |date=2011-02-25 |accessdate=2015-08-11}}</ref>

'''MHTC Logistics Pvt. Ltd''' (2012): Allcargo acquired 100% equity in MHTC Logistics Private Limited, engaged in project logistics and [[freight forwarding]].<ref>{{cite web|url=http://www.thehindubusinessline.com/multimedia/archive/00938/Click_here_for_pdf_938322a.pdf |format=PDF |title=Postal Ballot Notice |publisher=Thehindubusinessline.com |accessdate=2015-08-11}}</ref>
<ref>{{cite web|url=http://www.shippingherald.com/Admin/ArticleDetail/ArticleDetailsShippingNews/tabid/98/ArticleID/3282/Allcargo-Global-Logistics-to-demerge-two-business-units.aspx |title=Allcargo Global Logistics to demerge two business units |publisher=Shippingherald.com |date= |accessdate=2015-08-11}}</ref><ref>{{cite web|author= |url=http://in.finance.yahoo.com/news/allcargo-logistics-demerge-project-division-073908345.html |title=Allcargo Logistics to demerge project division of MHTC Logistics - Yahoo India Finance |publisher=In.finance.yahoo.com |date=2012-02-15 |accessdate=2015-08-11}}</ref><ref>{{cite web|author=indiainfoline.com |url=http://www.indiainfoline.com/Markets/News/Allcargo-Logistics-board-approves-merger-of-MHTC-Logistics/4450668222 |title=Allcargo Logistics' board approves merger of MHTC Logistics |publisher=Indiainfoline.com |date= |accessdate=2015-08-11}}</ref>

'''Econocaribe Consolidators'''(2013): Allcargo through its wholly owned subsidiary, ECU-LINE acquired Econocaribe Consolidators, a US based logistics company. Econocaribe specializes in freight consolidation and FCL services to Latin America, the Caribbean, Europe, the Mediterranean, the Middle East, Africa and Asia. It also offers import LCL/FCL transportation services from around the world to the United States and Puerto Rico.

<ref>{{cite web|author=P.R. Sanjai |url=http://www.livemint.com/Companies/JfIXchioQqcgMZDWvYeBvI/Allcargo-Logistics-buys-US-firm-Econocaribe-Consolidators.html |title=Allcargo Logistics buys US firm Econocaribe Consolidators |publisher=Livemint |date=2013-09-27 |accessdate=2015-08-11}}</ref><ref>{{cite web|url=http://www.business-standard.com/article/companies/allcargo-logistics-buys-econocaribe-consolidators-for-close-to-50-mln-113092700480_1.html |title=AllCargo Logistics buys Econocaribe Consolidators for close to $50 mln &#124; Business Standard News |publisher=Business-standard.com |date= |accessdate=2015-08-11}}</ref>

'''FCL Marine Agencies'''(2013): Allcargo Logistics through its step down wholly owned subsidiary company Ecuhold NV acquired majority stake in FCL Marine Agencies Rotterdam, a Netherlands-based Logistics company.

<ref>{{cite web|url=http://www.moneycontrol.com/news/announcements/allcargo-logistics-acquires-majority-stakefcl-marine-agencies-rotterdam_1010334.html |title=Allcargo Logistics acquires majority stake in FCL Marine Agencies Rotterdam |publisher=Moneycontrol.com |date= |accessdate=2015-08-11}}</ref>

== Services ==

'''Multimodal Transport Operations (MTO)'''

Allcargo’s MTO service comprises NVOCC operations related to LCL consolidation and FCL activities globally, through its wholly owned subsidiary ECU Line (based in Belgium). Initially Allcargo entered the business of LCL consolidation as agents of ECU Line. With an international network across 89 countries and 189 own offices, the division provides direct export/import and multicity consolidation services.<ref>{{cite web|url=http://www.allcargologistics.com/services/nvocc/our-services/multi-modal-transport-operations.aspx |title=Best Multimodal Transport Operators in India |publisher=Allcargo Logistics |date= |accessdate=2015-08-11}}</ref>

'''Container Freight Stations(CFS) and Inland Container Depots (ICD)'''

Allcargo commenced CFS operations in 2003 at JNPT (near Mumbai). At present it operates in all four CFSs (two at JNPT and one each in Chennai and Mundra-Gujarat). 
In 2009 Allcargo entered the business of Inland Container Depots (ICD)s. Its first ICD was at Kheda-Pithampur near Indore in the state of Madhya Pradesh, in a joint venture with Hind Terminals (part of the Samsara Group), with Allcargo’s stake at 51%. Its second ICD was started at Dadri in the national capital region, in a joint venture) with Container Corporation of India (CONCOR), with Allcargo’s stake at 51%. Its services comprise export/import handling, LCL shipments, bonded/open warehousing, first/last mile transportation, maintenance and repair of dry container, reefer monitoring and hazardous material handling. The total installed capacity across its CFSs and ICDs is around 441,000 [[Twenty-foot equivalent unit|TEUs]] per annum.<ref>{{cite web|url=http://www.allcargologistics.com/services/container-freight-stations-inland-container-depot/overview.aspx |title=Container Freight Station Facility in Chennai |publisher=Allcargo Logistics |date= |accessdate=2015-08-11}}</ref>

== Project and engineering ==

Specializes in managing end-to-end project logistics, from planning to movement of project cargo, out of gauge / over dimensional cargo, over-weight consignments on turnkey and door-to-door basis, route surveys and multimodal/location transportation. The service also includes a fleet of over 1,000 equipment and specialized vehicles. Complete range of cranes (crawler, telescopic, truck lattice and all-terrain), hydraulic axles and self-propelled modular transporters (SPMTs), strand jacks, trailers (low bed, semi low bed and high bed), fork-lifts and reach stackers.<ref>{{cite web|author= |url=http://articles.economictimes.indiatimes.com/2013-02-20/news/37200482_1_allcargo-rail-terminals-adarsh-hegde |title=Allcargo to set up rail terminal in Hyderabad - timesofindia-economictimes |publisher=Articles.economictimes.indiatimes.com |date= |accessdate=2015-08-11}}</ref><ref>{{cite web|url=http://www.nbmcw.com/equipments/rental-special-transport/27687-allcargo-logistics-exploring-value-in-heavy-lifting.html |title=Allcargo Logistics - Exploring value in Heavy Lifting |publisher=Nbmcw.com |date= |accessdate=2015-08-11}}</ref>

== Ship owning and chartering ==

The firm owns three ships:, all registered in India .<ref name="articles.economictimes.indiatimes" />
*Allcargo Laxmi, 4186 [[Gross Tonnage]]
*Allcargo Arathi, 4897 Gross Tonnage
*Allcargo Susheela, 6204 Gross Tonnage.

== Third-party logistics (3PL) and warehousing ==

Allcargo operates customized warehousing and supply through its own warehouses at [[Goa]], [[Pithampur]] (near [[Indore]] in the state of Madhya Pradesh), [[Hosur]] (in the state of Tamil Nadu) and [[Bhiwandi]] (near Mumbai in the state of Maharashtra state). It has land banks in [[Nagpur]], Indore, [[Hyderabad, India|Hyderabad]] and [[Bengaluru]] of nearly 200 acres.

== Global strategic tie-ups ==

=== Mammoet ===

A privately held Dutch company founded in 1973 specializing in the hoisting and transportation of heavy objects over water and roads, Mammoet currently has approximately 3,200 employees worldwide, of which over 850 are employed in The Netherlands. Allcargo Logistics Ltd has a strategic tie up with Mammoet in India for providing cranes above 750 Tons and self-propelled modular transporters (SPMTs).

=== Hansa Heavy Lift GmbH ===

The German, Hamburg headquartered, global specialist in transport of heavy-lift, project and break-bulk cargoes through oceans, Hansa Heavy Lift GmbH, currently operates 21 multipurpose heavy-lift freighters - the largest fleet in the heavy-lift and project market globally. Allcargo Logistics Ltd represents Hansa Heavy Lift as their exclusive agent across India.

==Current shareholding==
As on 31 March 2013, promoter and promoter group hold nearly 72.1% of the company’s shares. Private equity firms Blackstone, New Vernon and Acacia Partners hold approximately 14.4%, 3.0% and 6.0% of the company’s shares respectively. The rest is held by a mix of foreign corporate bodies, domestic corporate bodies and general public.

==Awards and recognition==
*2015 - Samudra Manthan Award- Logistics Company of the Year, 2015 from Bhandarkar Group of Publications  
*2013 - Global Indian Maritime Personalityaward from Maharashtra Chamber of Commerce, Industry and Agriculture (MACCIA)<ref>{{cite web|url=http://www.equitybulls.com/admin/news2006/news_det.asp?id=114428 |title=Shashi Kiran Shetty awarded 'Global Indian Maritime Personality' by Maharashtra Chamber of Commerce, Industry and Agriculture |publisher=EquityBulls.com |date=2013-01-10 |accessdate=2015-08-11}}</ref>
*2013 - Leadership and Innovation award from International Women Leadership Forum (IWLF)<ref>[http://www.transreporter.com/detail.php?pId=Allcargo-Logistics-Shantha-Martin-gets-International-Women-Leadership-Forum-award ]{{dead link|date=August 2015}}</ref>
*2012 - News Maker of the Year award from Maritime and Logistics Awards (MALA 2012)<ref name="mala-awards">{{cite web|url=http://mala-awards.com/MALAAwardWinners.aspx?Event_ID=22 |title=MALA 2012 |publisher=Mala-awards.com |date=2012-09-07 |accessdate=2015-08-11}}</ref>
*2012 - Heavylift Mover of the Year award from MALA 2011<ref name="mala-awards" />
*2012 - Outstanding Contribution Award from Port of Antwerp<ref>{{cite web|url=http://www.dailyshippingtimes.com/news-upload/upload/fullnews.php?fn_id=188 |title=Shashi Kiran Shetty awarded Allcargo Logistics by Port of Antwerp for outstanding contribution-Daily Shipping Times |publisher=Dailyshippingtimes.com |date=2012-02-15 |accessdate=2015-08-11}}</ref>
*2011 - LCL Consolidator of the Year award from 3rd South East CEO Conclave and Awards<ref name="moneycontrol">{{cite web|url=http://www.moneycontrol.com/company-facts/allcargogloballogistics/history/AGL02 |title=Allcargo Logistics > Company History > Transport & Logistics > Company History of Allcargo Logistics - BSE: 532749, NSE: ALLCARGO |publisher=Moneycontrol.com |date= |accessdate=2015-08-11}}</ref>
*2011 - Indian Freight Forwarder of the Year award from 1st Northern India Multimodal Logistics Awards<ref name="moneycontrol" />
*2010 - Logistics Company of the Year award from MALA 2010<ref>{{cite web|url=http://fairplayexpo.net/MALA2010/MalaAwardWinners.aspx?Event_ID=10 |title=Mala 2010 |publisher=Fairplayexpo.net |date=2010-09-30 |accessdate=2015-08-11}}</ref>
*2010 - Private Container Freight Stations (CFS) Operator in India award from Maritime Gateway Award<ref>[http://www.maritimegateway.com/mgw/index.php?option=com_contentandview=articleandid=291andItemid=136 ]{{dead link|date=August 2015}}</ref>
*2009 - Logistics Company of the Year award from NDTV Profit<ref>{{cite web|author= |url=http://www.indiantelevision.com/release/y2k9/oct/octrel77.php |title=Indian Television Dot Com &#124; NDTV Profit Announces Winners of Business Leadership Awards 2009 |publisher=Indiantelevision.com |date=2009-10-27 |accessdate=2015-08-11}}</ref><ref>{{cite web|url=http://www.daijiworld.com/news/news_disp.asp?n_id=67568andn_tit |title=D A I J I W O R L D |publisher=D A I J I W O R L D |date=2011-08-10 |accessdate=2015-08-11}}</ref>
*2008 - Best Logistics Company award from Indian Maritime Gateway Awards<ref>[http://www.maritimegateway.com/mgw/index.php?option=com_contentandview=articleandid=64andItemid=83 ]{{dead link|date=August 2015}}</ref><ref>{{cite web|url=http://www.planetearth-india.info/mgw/index.php?option=com_contentandview=articleandid=64andItemid=83 |title=Welcome to Planet Earth |publisher=Planetearth-india.info |date= |accessdate=2015-08-11}}</ref>
*2007 - Deal of the Year (for the acquisition of ECU LINE business) award from Seatrade{{cn|date=July 2015}}

==References==
{{Reflist|colwidth=30em}}{{Logistics companies of India}}
[[Category:Logistics companies of India]]