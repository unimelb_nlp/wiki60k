{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Well at the World's End
| title_orig    =
| translator    =
| image         = The Well at the Worlds End 1-2.jpg<!--prefer 1st edition-->
| caption = Covers of ''The Well at the World's End'' (1970), vols. 1–2, [[Ballantine Books]]| author        = [[William Morris]]
| illustrator   =
| cover_artist  =
| country       = England
| language      = English
| series        =
| genre         = [[Fantasy novel]]
| publisher     = [[William Morris#The Kelmscott Press|The Kelmscott Press]]
| release_date  = 1896
| english_release_date =
| media_type    = Print (hardback)
| pages         =
| preceded_by   =
| followed_by   =
}}

'''''The Well at the World's End''''' is a [[high fantasy]] novel by the British artist, poet, and author [[William Morris]]. It was first published in 1896 and has been reprinted a number of times since, most notably in two parts as the 20th and 21st volumes of the [[Ballantine Adult Fantasy series]], in August and September 1970. It is also available in one volume along with a similar Morris tale, ''[[The Wood Beyond the World]]'' (1894), in ''On the Lines of Morris' Romances: Two Books that Inspired J. R. R. Tolkien.''<ref>{{cite book |last=Morris |first=William |editor-last=Perry |editor-first=Michael W. |date=December 23, 2003 |title=On the Lines of Morris' Romances: Two Books that Inspired J. R. R. Tolkien-The Wood Beyond the World and The Well at the World's End |publisher=Inkling Books |isbn=978-1587420245}}</ref>

==Plot summary==
Using language with elements of the medieval tales which were his models, Morris tells the story of Peter, King of Upmeads, and his four sons, Blaise, Hugh, Gregory, and Ralph. These four sons decide one day that they would like to explore the world, so their father gives them permission. From that point on, the plot centers on the youngest son, Ralph.

Ralph's explorations begin at Bourton Abbas, after which he goes through the Wood Perilous. He has various adventures there, including the slaying of two men who had entrapped a woman. That woman later turns out to be the Lady of Abundance, who later becomes his lover for a short time.

In one episode Ralph is staying at a castle and inquires about the Lady of the castle (the so-called Lady of Abundance), whom he has not yet seen. Descriptions of her youth and beauty suggest to him that she has drunk from the well at the world’s end. "And now in his heart waxed the desire of that Lady, once seen, as he deemed, in such strange wise; but he wondered within himself if the devil had not sown that longing within him…." A short time later, while still at the castle, Ralph contemplates images of the Lady and "was filled with the sweetness of desire when he looked on them." Then he reads a book containing information about her, and his desire to meet the Lady of Abundance flames higher. When he goes to bed, he sleeps "for the very weariness of his longing." He fears leaving the castle because she might come while he is gone. Eventually he leaves the castle and meets the Lady of Abundance, who turns out to be the same lady he had rescued some weeks earlier from two men.

When he meets her this time, the lady is being fought over by two knights, one of whom slays the other. That knight nearly kills Ralph, but the lady intervenes and promises to become the knight’s lover if he would spare Ralph. Eventually, she leads Ralph away during the night to save Ralph’s life from this knight, since Ralph had once saved hers. She tells Ralph of her trip to the Well at the World’s End, her drinking of the water, the tales of her long life, and a maiden she thinks is especially suited to Ralph. Eventually, the knight catches up to them and kills her with his sword while Ralph is out hunting. Upon Ralph’s return, the knight charges Ralph, and Ralph puts an arrow through his head. After Ralph buries both of them, he begins a journey that will take him to the Well at the World’s End.

As he comes near the village of Whitwall, Ralph meets a group of men, which includes his brother Blaise and Blaise’s attendant, Richard. Ralph joins them, and Richard tells Ralph about having grown up in Swevenham, from which two men and one woman had once set out for the Well at the World’s End. Richard had never learned what happened to those three. Richard promises to visit Swevenham and learn what he can about the Well at the World’s End.

Ralph falls in with some merchants, led by a man named Clement, who travel to the East. Ralph is in search of the Well at the World’s End, and they are in search of trade. This journey takes him far to the east in the direction of the well, through the villages of Cheaping Knowe, Goldburg, and many other hamlets. Ralph learns that a maiden, whom the Lady of Abundance had mentioned to him, has been captured and sold as a slave. He inquires about her, calling her his sister, and he hears that she may have been sold to the Lord of Utterbol, who is a cruel, powerful, and ruthless man named Gandolf. The queen of Goldburg writes Ralph a letter of recommendation to Gandolf, and Morfinn the Minstrel, whom he also met at Goldburg, promises to guide him.

Morfinn turns out to be a traitor who delivers Ralph into the hands of the Lord of Utterbol. After some time with Gandolf and his men, Ralph escapes. Meanwhile, Ursula, Ralph’s "sister," who has been enslaved at Utterbol, escapes and by chance meets Ralph in the woods beneath the mountain, both of them desiring to reach the Well at the World’s End. Eventually their travels take them to the Sage of Swevenham, who gives them instructions for finding the Well at the World’s End.

On their journey to the well, they fall in love, especially after Ralph saves her life from a bear's attack. Eventually they make their way to the sea, on the edge of which is the Well at the World’s End. They each drink a cup of the well's water and are enlivened by it. They then backtrack along the path where they had earlier encountered, meeting the Sage and the new Lord of Utterbol, who has slain the previous evil lord and remade the city into a good city, and the pair returns the rest of the way to Upmeads.

While they experience challenges and battles along the way, the pair succeeds in all their endeavors. Their last challenge is a battle against men from the Burg of the Four Friths. These men come against Upmeads to attack it. As Ralph approaches Upmeads, he gathers supporters around him, including the Champions of the Dry Tree. After Ralph and his company stop at Wulstead, where Ralph is reunited with his parents as well as Clement Chapman, he leads a force in excess of a thousand men against the enemy and defeats them. He then brings his parents back to High House in Upmeads to restore them to their throne. As Ralph and Ursula come to the High House, Ralph's parents install Ralph and Ursula as King and Queen of Upmeads.

==Reception and influence==
On its publication, ''The Well at the World's End'' was praised by [[H. G. Wells]], who
compared the book to [[Thomas Malory|Malory]] and admired its
writing style: "all the workmanship of the book is stout oaken stuff, that must needs endure and preserve the
memory of one of the stoutest, cleanest lives that has been lived in these latter days".<ref>[[Harold Bloom]] (editor),
"William Morris" in ''Classic Fantasy Writers''. Chelsea House Publishers, 1994 ISBN 0791022048 (pg. 153).</ref>

Although the novel is relatively obscure by today's standards, it has had a significant influence on many notable fantasy authors. [[C. S. Lewis]] and [[J. R. R. Tolkien]] both seem to have found inspiration in ''The Well at the World's End'': ancient tables of stone, "King Gandolf," a "King Peter," and a quick, white horse named "Silverfax," an obvious inspiration for "Shadowfax," are only a few. Lewis was sufficiently enamored with Morris that he wrote an essay on that author, first read to an undergraduate society at [[Oxford University]] called the Martlets and later published in the collection of essays called ''Rehabilitations''.<ref>{{cite book|author=Morris, William|title=Rehabilitations|location= St. Clair Shores, Michigan|publisher= Scholarly Press, Inc|date=  1979|pages= 35–55}}</ref>

The [[Ballantine Books|Ballantine]] one-volume paperback edition has what appears to be a quotation from [[C. S. Lewis]] on the back cover: "I have been more curious about travels from Upmeads to Utterbol than about those recorded in [[Richard Hakluyt|Hokluyt]]. The magic in ''The Well at the World's End'' is that it is an image of the truth. If to love story is to love excitement, then I ought to be the greatest lover of excitement alive!" This passage is actually a pastiche of phrases from Lewis' essay "On Stories" (anthologised in several collections, including ''Of This and Other Worlds'', William Collins Sons & Co. Ltd., Glasgow, 1982: pp.&nbsp;25–45), and distorts Lewis' original meaning. He does not say that "the magic in the book" is an image of the truth, but that he is "not sure, on second thoughts, that the slow fading of the magic in ''The Well at the World's End'' is, after all, a blemish. It is an image of the truth" (p.&nbsp;45).{{Citation needed|date=September 2015}}

As for "excitement," which Lewis defines as "the alternate tension and appeasement of imagined anxiety" (p.&nbsp;29), his original point is not that he is a great lover of excitement, but that some readers, including himself, seek literary experiences other than excitement in tales: "If to love Story is to love excitement then I ought to be the greatest lover of excitement alive. But the fact is that what is said to be the most 'exciting' novel in the world, ''[[The Three Musketeers]]'', makes no appeal to me at all" (p.&nbsp;29). Lewis is thus explicitly ''not'' the "greatest lover of excitement alive."{{Citation needed|date=September 2015}}

The same title was used by Scottish writer [[Neil Gunn]] for his 1948 book.{{Citation needed|date=September 2015}}

''The Well at the World's End, Folk Tales of Scotland, retold by Norah and William Montgomerie'' was first published in 1956 by The [[Hogarth Press]]. The 2005 edition is  named ''The Folk Tales of Scotland. The Well at the World's End and Other Stories, retold by Norah and William Montgomerie'', published in 2005 by The Mercat Press.{{Citation needed|date=September 2015}}

==Notes==
{{reflist}}

==External links==
*{{isfdb title|id=871|title=The Well at the World's End}}
{{Gutenberg|no=169|name=The Well at the World's End}}
* {{librivox book | title=The Well at the World's End | author=William MORRIS}}

{{William Morris}}

{{DEFAULTSORT:Well At The World's End, The}}
[[Category:1896 novels]]
[[Category:Novels by William Morris]]
[[Category:Fantasy novels]]
[[Category:High fantasy novels]]
[[Category:19th-century British novels]]