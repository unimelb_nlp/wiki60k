{{Use Australian English|date=March 2013}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type     = suburb
| name     = Inglewood
| state    = sa
| image    = Inglewood hotel.jpg 
| caption  = Inglewood Inn
| latd  =34|latm =49 |lats  =0
| longd =138 |longm =46 |longs =0 
| pushpin_label_position = top
| lga      = Adelaide Hills Council
| postcode = 5133
| pop      = 535
| pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref name="ABS2011">{{Census 2011 AUS|id=SSC40310|name=Inglewood (State Suburb)|accessdate=3 February 2016|quick=on}}</ref>
| pop2     = 264
| pop2_year= {{CensusAU|2006}}
| pop2_footnotes = <ref name=Census2006Y />
| area     = 
| stategov = [[Electoral district of Kavel|Kavel]]
| fedgov   = [[Division of Mayo|Mayo]]
| dist1    = 24
| location1= Adelaide
| near-nw  =
| near-n   = [[One Tree Hill, South Australia|One Tree Hill]]
| near-ne  =
| near-w   = [[Upper Hermitage, South Australia|Upper Hermitage]]
| near-e   = [[Millbrook, South Australia|Millbrook]]
| near-sw  = [[Houghton, South Australia|Houghton]]
| near-s   = [[Paracombe, South Australia|Paracombe]]
| near-se  =
}}
'''Inglewood''' is a small town near [[Adelaide]], [[South Australia]]. It is located in the [[Adelaide Hills Council]] local government area, and is adjacent to [[Houghton, South Australia|Houghton]], [[Paracombe, South Australia|Paracombe]] and the rural districts of [[Upper Hermitage, South Australia|Upper Hermitage]] and [[Chain of Ponds, South Australia|Chain of Ponds]]. At the [[Census in Australia#2006|2006 census]], Inglewood had a population of 264.<ref name=Census2006Y>{{Census 2006 AUS|id=SSC43786|name=Inglewood (State Suburb) |accessdate=2 August 2011|quick=on}}</ref>

Inglewood began as a private subdivision, and was named after a town in Cumberland, England.<ref>[http://www.placenames.sa.gov.au/pno/pnores.phtml?recno=SA0018589 Placenames SA - Inglewood] accessed 16 June 2006</ref><ref name="plb">{{cite web | url=http://maps.sa.gov.au/plb/# | title=Search result(s) for Inglewood, 5133 | publisher=Government of South Australia | work=Property Location Browser | accessdate=17 February 2016}}</ref> The historic [[Inglewood Inn]] on North East Road was built by Thomas Deacon in 1858, and played a significant role in the development of the area. It survives today and is listed on the [[South Australian Heritage Register]] and the former [[Register of the National Estate]].<ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=15245 | title=Inglewood Inn | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=30 March 2016}}</ref><ref>{{cite web | url=http://www.environment.gov.au/cgi-bin/ahdb/search.pl?mode=place_detail;search=town%3Dinglewood%3Bstate%3DSA%3Bkeyword_PD%3Don%3Bkeyword_SS%3Don%3Bkeyword_PH%3Don%3Blatitude_1dir%3DS%3Blongitude_1dir%3DE%3Blongitude_2dir%3DE%3Blatitude_2dir%3DS%3Bin_region%3Dpart;place_id=6248 | title=Inglewood Inn, North East Rd, Inglewood, SA, Australia | publisher=Department of the Environment | work=Register of the National Estate | accessdate=30 March 2016}}</ref> Inglewood Post Office opened in July 1865.<ref>{{cite web | url=https://www.premierpostal.com/cgi-bin/wsProd.sh/viewpodet.w?cdpo=12364 | title=Inglewood | publisher=Premier Postal | work=Post Office Reference | accessdate=30 March 2016}}</ref> The town also has a general store, located on North East Road.<ref>{{cite web | url=http://www.paracombps.sa.edu.au/download/school_context.pdf | title=School Context Statement | publisher=Paracombe Primary School | accessdate=30 March 2016}}</ref>

The modern boundaries of Inglewood were established in October 2001 for the long established name. Its boundaries with [[Lower Hermitage, South Australia|Lower Hermitage]] were altered in October 2005 and it gained an area from [[Millbrook, South Australia|Millbrook]] in August 2015.<ref name="plb" />
[[File:Inglewood main rd.jpg|thumb|The main road]]

==Transport==
The area is not serviced by Adelaide public transport. A coach is operated from [[Tea Tree Plaza Interchange]] to Gumeracha and [[Mount Pleasant, South Australia|Mount Pleasant]] by Link SA.<ref>{{cite web | url=http://www.linksa.com.au/documents/TTMtPleasantMtTorrens-TTP31-07-15_000.pdf | title=Tea Tree Plaza - Mt Pleasant - Mt Torrens | publisher=Link SA | accessdate=30 March 2016}}</ref>

== References ==
{{Reflist}}

{{Adelaide Hills Council suburbs}}
{{Adelaide Hills}}

[[Category:Suburbs of Adelaide]]
[[Category:Towns in South Australia]]