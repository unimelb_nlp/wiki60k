'''John Blair''' [[Fellow of the Royal Society|FRS]], [[Society of Antiquaries of London|FSA]] (died 24 June 1782), was a British clergyman, and [[chronologist]].

==Life==
He was born and educated in Edinburgh. 
Leaving Scotland as a young man, he became usher of a school in Hedge Lane, London, in succession to Andrew Henderson, author of a well-known history of the rebellion of 1745.{{sfn|Grosart|1886}}

In 1754, he published, 'The Chronology and History of the World, from the Creation to the Year of Christ 1753, illustrated in fifty-six tables.' 
It was dedicated to the lord chancellor (Hardwicke), and was published by subscription. 
In the preface he acknowledged great obligations to the Earl of Bute.
The plan and scope of the work originated with Dr. Hugh Blair's scheme of chronological tables. The 'Chronology' was reprinted in 1756, 1768, and 1814. 
It was revised and enlarged 'by Willoughby Rosse in Bohn's 'Scientific Library,' 1856. 
In 1768, Blair published 'Fourteen Maps of Ancient and Modern Geography, for the illustration of the Tables of Chronology and History ; to which is prefixed a dissertation on the Rise and Progress of Geography.' The dissertation was separately republished in 1784.{{sfn|Grosart|1886}}

Blair's first book was well received. In 1755 he was elected a fellow of the Royal Society, and in its 'Transactions' appeared a paper by him on the 'Agitation of the Waters near Reading'<ref>''Phil. Trans''. x. 651, 1755</ref>{{sfn|Grosart|1886}}

He had previously obtained orders in the church of England, and in September 1757 was appointed chaplain to  [[Princess Augusta of Saxe-Gotha]] and mathematical tutor to the [[Prince Edward, Duke of York and Albany]]. 
In March 1761, on the promotion of Dr. Townshend to the deanery of Norwich, Blair was given a prebendal stall at Westminster. 
Within a week the dean and chapter of Westminster presented him to the vicarage of Hinckley. 
In the same year he was chosen fellow of the Society of Antiquaries. 
In September 1763 he left with the Duke of York on a tour on the continent, and was absent until 1764. In 1771 he was transferred, by presentation of the dean and chapter of Westminster, to the vicarage of St. Bride, London, and again to the rectory of St. John the Evangelist, Westminster, in April 1776. 
He was also rector of Horton (Milton's Horton) in Buckinghamshire. 
He died on 24 June 1782.{{sfn|Grosart|1886}}

Blair's 'Lectures on the Canons of the Old Testament, comprehending a Dissertation on the Septuagint Version,' 1785, was a posthumous publication.{{sfn|Grosart|1886}}

== References ==
{{Reflist}}
;Attribution
{{DNB |first=Alexander Balloch |last=Grosart |wstitle=Blair, John (d.1782) |volume=5|pages=162–163}}; Endnotes: 
*''Notes and Queries'', 6th series, vii. 48 ; 
*Anderson's ''Scottish Nation''; researches in Edinburgh.

==Further reading==
* {{Eminent Scotsmen|Blair, John, LL.D., chronologist|1|257-58}}

{{Authority control}}
{{DEFAULTSORT:Blair, John}}
[[Category:1782 deaths]]
[[Category:Chronologists]]
[[Category:Year of birth unknown]]
[[Category:Fellows of the Royal Society]]
[[Category:18th-century English Anglican priests]]