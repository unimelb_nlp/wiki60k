{{good article}}
{{Infobox Television episode 
| title = The Bishop Revival
| series = [[Fringe (TV series)|Fringe]]
| image =<!-- Deleted image removed:  [[File:Fringe Bishop Revival.jpg|250px]] -->
| caption =  Alfred Hoffman ([[Craig Robert Young]]) dies from an airborne toxin designed specially for him by Walter.
| season = 2
| episode = 14
| airdate = January 28, 2010
| production = 3X5113
| writer = [[Glen Whitman]]<br>[[Robert Chiappetta]]
| director = [[Adam Davidson (director)|Adam Davidson]]
| guests = *[[Craig Robert Young]] as Alfred Hoffman
*[[Clark Middleton]] as [[List of Fringe characters#Edward Markham|Edward Markham]]
* Craig Anderson as George
* Lauren Attadia as Amanda
* Diana Bang as Nora
* Aaron Brooks as Josh Staller
* Robin Douglas as Lynn Arrendt
* Magda Harout as Nana Staller
* Dan Joffre as Detective Manning
* Barbara Kottmeier as Shelley
* [[Nancy Linari]] as Eliza Staller
* John Macintyre as Young Usher
* [[Alberta Mayne]] as Young Mother
* Al Miro as Neal
* Sierra Pitkin as Jordan
* Leonard Tenisci as Harry Staller
* Max Train as Eric Franko
* Brendon Zub as David Staller
| episode_list = [[Fringe (season 2)|''Fringe'' (season 2)]]<br>[[List of Fringe episodes|List of ''Fringe'' episodes]]
| prev = [[What Lies Below]]
| next = [[Jacksonville (Fringe)|Jacksonville]]
}}

"'''The Bishop Revival'''" is the 14th [[List of Fringe episodes|episode]] of the [[Fringe (season 2)|second season]] of the American [[science fiction]] [[drama]] [[television program|television series]] ''[[Fringe (TV series)|Fringe]]''. The episode's storyline followed Nazi scientist Alfred Hoffman ([[Craig Robert Young]]) as he specially designed airborne toxins to kill only surrounding people with similar genetic traits, such as people with brown eyes.

It was written by [[Glen Whitman]] and [[Robert Chiappetta]], and directed by [[Adam Davidson (director)|Adam Davidson]]. Along with Young and a number of small guest parts, the episode featured another guest appearance by [[Clark Middleton]] as rare book seller [[List of Fringe characters#Edward Markham|Edward Markham]]. "The Bishop Revival" first aired in the United States on January&nbsp;28,&nbsp;2010 on the [[Fox Broadcasting Company|Fox network]] to an estimated 9.153&nbsp;million viewers. Critical reception for the episode ranged from positive to mixed, as reviewers were divided on the episode's villain.

==Plot==
15 people suffocate at a Jewish wedding, appearing to have asphyxiated from the inside out. When the Fringe team arrives, [[Olivia Dunham|Olivia]] ([[Anna Torv]]) identifies that all the victims were from the groom's side, whose grandmother was a [[Holocaust]] survivor -  and [[Walter Bishop (Fringe)|Walter]] ([[John Noble]]) surmises that they were all killed via their shared genetic traits. Later, a similar mass death occurs at a coffee shop, in which Walter recognizes the victims all had brown eyes, another common genetic trait. From fingerprints found at the scene, they discover the culprit is Alfred Hoffman ([[Craig Robert Young]]), a [[Nazi]] scientist apparently somehow over 100 years old. Walter realizes that the man likely worked with his own father, Robert Bishoff (a German scientist who defected to the US in 1943 and [[Anglicisation of names|anglicised]] his name), in creating a chemical agent that, once heated as a gas, could be used to target any specific trait using DNA from the target subject - especially those not of the [[master race]]. Though Walter originally had his father's files on the subject, his son [[Peter Bishop|Peter]] ([[Joshua Jackson]]), 10 years earlier, had sold them; Peter tries to recover the files but finds some have been used by an artist to create sensationalism art, causing Walter to become distraught.

They trace Hoffman to his home, finding his equipment used to create the chemical agent downstairs but no sign of Hoffman. Walter nearly suffocates from an agent left by Hoffman, but Olivia and Peter are able to save him in time. As the FBI search the premises, they find evidence that points to a convention being held to promote world equality. Olivia and Peter depart to try to find Hoffman, while Walter remains behind, examining Hoffman's equipment.

At the convention, Hoffman has replaced the heating elements for the [[chafing-dish]]es with his own. Olivia and Peter struggle with locating Hoffman before Walter and [[Astrid Farnsworth|Astrid]] ([[Jasika Nicole]]) arrive. Walter uses a fogger to distribute his own chemical agent, this time specific to Hoffman, and soon the man is found dying. As the team regroups, Walter fully admits to killing Hoffman, a crime in itself, but [[Phillip Broyles|Broyles]] ([[Lance Reddick]]) decides to let Walter go. Later, Peter has been able to recover the rest of his grandfather's work and returns it to Walter; Walter then goes through the files, finding an old photo of his father and Hoffman working together.

==Production==
"The Bishop Revival" was the third episode to be written by writing partners [[Glen Whitman]] and [[Robert Chiappetta]].<ref name=airlock>{{cite web|url=http://www.airlockalpha.com/node/7086 |title='Fringe' - The Bishop Revival |last=Ferguson|first=LaToya |publisher=[[Airlock Alpha]]|date=2010-01-29|accessdate=2011-06-23}}</ref> It was the only ''Fringe'' episode directed by [[Adam Davidson (director)|Adam Davidson]].<ref>{{cite web|url=http://www.tvguide.com/celebrities/adam-davidson/credits/188164 |title=Adam Davidson: Credits |work=[[TV Guide]]|accessdate=2011-06-26}}</ref>

"The Bishop Revival" revealed that the [[seahorse]] shown in promotions since the series began was in fact a genetically encoded "signature" created by Walter's father Dr. Robert Bishop.<ref>{{cite web|url=https://www.wired.com/underwire/2010/02/fringe-mythology/ |title='Big Answer' Looms in Fringe's Mythology-Rich Winter Finale |last=Hart|first=Hugh|work=[[Wired (magazine)|Wired]]|date=2010-02-04|accessdate=2011-02-05}}</ref><ref>{{cite web|url=http://www.newsarama.com/tv/Fringe-Post-Game-100204.html |title=Post Game TV Recap: FRINGE S2E14 "The Bishop Revival" |last=Reeder |first=Chanel |work=[[Newsarama]]|date=2010-02-04|accessdate=2011-05-25}}</ref> In an interview after the episode's broadcast, consulting producer [[Akiva Goldsman]] cited "The Bishop Revival" when discussing the use of flashbacks in ''Fringe''; he stated his disinclination to use too many flashbacks in the series, explaining "I think flashbacks are really useful and there are a couple of places where it will be useful. But fundamentally, no, I don't think we're a show that will be doing a lot of jumping back in time despite the single horde of calls for the 'Walter's Grandfather Nazi Hunting' series. I think not, but it was fun to do [in the '[[Peter (Fringe)|Peter]]' episode]."<ref>{{cite web|url=http://www.digitalspy.com/tv/s118/fringe/news/a221181/fringe-exec-unsure-about-flashbacks.html |title='Fringe' exec 'unsure about flashbacks' |last=Wightman |first=Catriona |publisher=[[Digital Spy]]|date=2010-05-21|accessdate=2011-06-23}}</ref>

Guest stars for the episode included [[Craig Robert Young]], Max Train, Sierra Pitkin, Brendon Zub, Barbara Kottmeier, John Macintyre, Lauren Attadia, Al Miro,<ref>{{cite press release|url=http://tvovermind.zap2it.com/fox/fringe/fringe-season-2-episode-13/17152 |title=Photos - Fringe 2.13 "The Bishop Revival" |publisher=[[Fox Broadcasting Company]] |date=2010-01-14 |accessdate=2011-06-26}}</ref> Aaron Brooks, Magda Harout, Leonard Tenisci, [[Alberta Mayne]], [[Nancy Linari]], and Dan Joffre.<ref>{{cite web|url=http://www.tvguide.com/tvshows/fringe/episode-14-season-2/293813 |title=Fringe Episode: "The Bishop Revival" |work=[[TV Guide]]|accessdate=2011-06-21}}</ref> [[Clark Middleton]], who was last seen in the first season episode [[Ability (Fringe)|"Ability"]], made his second guest appearance in "The Bishop Revival" as rare bookseller Edward Markham.<ref name=seattle>{{cite web|url=http://blog.seattlepi.com/tubular/2010/07/05/fringe-der-vaterland/ |title=Fringe: Der Vaterland |last=Odell |first=Therese |work=[[Seattle Post-Intelligencer]]|date=2010-06-07|accessdate=2011-06-23}}</ref>

==Cultural references==
The episode contained two pieces of music from the 19th&nbsp;century German composer [[Johannes Brahms]]: his [[Piano Quartet No. 1 (Brahms)|Piano Quartet No. 1, Op.25 in G Major: III. Andante Con Moto-Animato]] and Piano Quartet No.&nbsp;1, Op.25 in G&nbsp;Minor: II. Intermezzo: Allegro Ma Non Troppo-Trio: Animato.<ref>{{cite web|url=http://heardontv.com/tvshow/Fringe/Season+2/The+Bishop+Revival |title=Music from Fringe - The Bishop Revival |publisher=Heard On TV |accessdate=2011-06-26}}</ref> Also in the episode, someone is seen holding a [[Dharma Initiative]] tea bag, a reference to the mysterious organization on the science fiction series ''[[Lost (TV series)|Lost]]''.<ref name=airlock/>

The Nazi in this episode appears to be [[Reichsführer]] of the [[Schutzstaffel]], [[Heinrich Himmler]]. Aside from looking like the character, Himmler was also both a [[Nordicism|Nordicist]] and a [[Nazism and occultism|Nazi Occultist]].<ref>http://www.historylearningsite.co.uk/heinrich_himmler.htm</ref> At one point in the episode, a scared elderly woman points at him screaming, "It's him... it's him...!" This has a double meaning, as she could have been saying "It's him!" or she could have been trying to say "It's Himmler!"

==Reception==
===Ratings===
In a Thursday night filled mostly with repeats, Fox's airing of new episodes ''[[Bones (TV series)|Bones]]'' and ''Fringe'' finished #1 among adults aged 18–49, with an estimated 9.153&nbsp;million viewers tuning in. ''Fringe'' was up fifteen percent from the [[What Lies Below|previous week]] with a 3.0 rating, tying its highest 18–49 ratings share for the season.<ref>{{cite web|url=http://tvbythenumbers.com/2010/01/29/tv-ratings-bones-fringe-vampire-diares-up-the-deep-end-sinks-on-topsy-turvy-thursday/40481|title=TV Ratings: ''Bones'', ''Fringe'', ''Vampire Diaries'' Up; ''The Deep End'' Sinks on Topsy Turvy Thursday|last=Gorman|first=Bill|publisher=''TV by the Numbers''|date=2010-01-29|accessdate=2011-01-16}}</ref><ref>{{cite web|url=http://www.hollywoodreporter.com/blogs/live-feed/bones-fringe-score-highs-cbs-53030 |title='Bones,' 'Fringe' score highs; CBS' 'Moment' passes |last=Hibberd|first=James|publisher=''[[The Hollywood Reporter]]'' |date=2010-01-29 |accessdate=2011-02-16}}</ref> It was the second most viewed episode of the season after the season premiere "[[A New Day in the Old Town]]".<ref>{{cite web|url=http://tvovermind.zap2it.com/fox/fringe/fringe-viewership-up-almost-two-million/18164 |title='Fringe' Viewership Up Almost Two Million |last=McPherson |first=Sam|publisher=''TV Over Mind'' |date=2010-01-29|accessdate=2011-01-18}}</ref>

===Reviews===
{{Quote box
 | quote  = "I thought last night's revelation about Walter's father being a Nazi scientist/Allied spy was just too much. We didn't need that extra backstory on Walter; we didn't need to make this [[Villain of the week|monster-of-the-week]] personal. In fact making it personal, to my mind, made the episode feel cliched rather than grossout fun, which is what a monster-of-the-week should be."
 | source = – [[io9]] columnist [[Annalee Newitz]]<ref name=io9review/>
 | width  = 25em
 |bgcolor=#ADD8E6
 | align  =left
}}

The episode received mixed to positive reviews from television critics. Jane Boursaw of [[TV Squad]] wasn't sure what to think about "The Bishop Revival", but loved the plotline about Walter's dad being a German spy working for the US government.<ref>{{cite web|url=http://www.tvsquad.com/2010/01/29/review-fringe-the-bishop-revival-recap/|title=Review: Fringe - The Bishop Revival|last=Boursaw |first=Jane|publisher=[[TV Squad]] |date=2010-01-29 |accessdate=2011-01-16}}</ref> Alternately, [[Annalee Newitz]] of [[io9]] called the episode "surprisingly meh"; while appreciating "all the weird family revelations about the Bishops", she believed the revelations about Walter's father to be "too much" because "we didn't need that extra backstory".<ref name=io9review/> Newitz wished ''Fringe'' had brought back Olivia's childhood subplot and its ties with Walter and Peter.<ref name=io9review>{{cite web|url=http://io9.com/5460119/one-reveal-too-many-on-fringe |title=One Reveal Too Many On Fringe |last=Newitz |first=Annalee |publisher=[[io9]] |date=2010-01-29 |accessdate=2011-06-26}}</ref> [[The A.V. Club]] columnist Noel Murray was also critical of the episode, explaining "Plotwise, there wasn't much going on in 'The Bishop Revival.'... The FD tracked down a criminal and felled that criminal; that's really it." Murray did however praise the killer's methods and "Aryan aloofness" as "cool" and "delightfully old-school".<ref>{{cite web|url=http://www.avclub.com/articles/the-bishop-revival,37658/ |title=The Bishop Revival |last=Murray |first=Noel |publisher=[[The A.V. Club]] |date=2010-01-29 |accessdate=2011-06-26}}</ref> Like Newitz, Tim Grierson of the magazine ''[[New York (magazine)|New York]]'' believed the episode contained "stupid revelations"; for instance, the Nazi connection of Walter's father "just felt like a variation on season one's episodes in which bizarre phenomena could always magically be linked back to Walter's work for the government. Obviously, this info about Peter's grandfather was supposed to be 'shocking,' showing how the Bishop family's scientific work can so easily be perverted for evil, but by this point it just seems like a very artificial, unnecessary ploy to keep us engaged."<ref>{{cite web|url=http://nymag.com/daily/entertainment/2010/01/fringe_recap_grandpa_was_a_naz.html |title=Fringe Recap: Grandpa Was a Nazi |last=Grierson |first=Tim |work=[[New York (magazine)|New York]] |date=2010-01-29 |accessdate=2011-06-26}}</ref>

Other than a few minor complaints with the episode's logic, [[IGN]] writer Ramsey Isler thought positively about "The Bishop Revival" and the [[Nazi]] story element in particular, stating "there's a definite unique ''Fringe'' flavor that makes this story work".<ref name=ign>{{cite web|url=http://tv.ign.com/articles/106/1065019p1.html |title=''Fringe'': "The Bishop Revival" Review |last=Isler |first=Ramsey|publisher=[[IGN]] |date=2010-01-29 |accessdate=2011-01-16}}</ref> Isler disliked the unsolved mystery of Hoffman however, writing the "story really had the feel of one of those intriguing but ultimately disposable stories in the ''Fringe'' library".<ref name=ign/> Jennifer Walker from TV Fanatic called the episode "amazing" and a "heart stopper",<ref>{{cite web|url=http://www.tvfanatic.com/2010/01/fringe-review-bishop-revival/|title=Fringe Review: "The Bishop Revival"|last=Walker|first=Jennifer|publisher=TV Fanatic |date=2010-01-29|accessdate=2011-01-16}}</ref> while Andrew Hanson of the ''[[Los Angeles Times]]'' enjoyed the father-son dynamic.<ref name=la>{{cite web|url=http://latimesblogs.latimes.com/showtracker/2010/01/fringe-nazis.html |title='Fringe': Nazis! |last=Hanson|first=Andrew|work=[[Los Angeles Times]] |date=2010-01-29|accessdate=2011-01-16}}</ref> Ken Tucker from ''[[Entertainment Weekly]]'' wrote "The Bishop Revival" was "one of the series' most satisfying stand-alone episodes" because it featured a "good threat" and gave more information about the Bishop family's backstory.<ref name=ew>{{cite web|url=http://watching-tv.ew.com/2010/01/29/fringe-season-2-episode-13/ |title='Fringe' recap: Walter, who's your daddy? |last=Tucker|first=Ken|publisher=''[[Entertainment Weekly]]'' |date=2010-01-29 |accessdate=2011-01-16}}</ref> Tucker praised [[John Noble]]'s performance, as his "portrayal of Walter encompasses everything from endearing daffiness to ferocious concentration and commitment".<ref name=ew/> [[MTV]]'s Josh Wigler believed the episode was "terrific," but wished there was more of a balance between the show's three leads, and that Olivia was featured on a regular basis.<ref name=mtv>{{cite web|url=http://moviesblog.mtv.com/2010/01/28/fringe-episode-214-recap-the-bishop-revival/ |title='Fringe' Episode 2.14 Recap: 'The Bishop Revival' |last=Wigler|first=Josh |publisher=[[MTV]] |date=2010-01-28 |accessdate=2011-01-16}}</ref> He, Hanson, and other critics agreed that this and the previous week's episode gave ''Fringe'' some strong momentum heading into the winter finale.<ref name=la/><ref name=mtv/> Though normally skeptical of the series' many fringe cases, ''[[Popular Mechanics]]'' called the episode ''Fringe''{{'}}s "most plausible case yet".<ref>{{cite web|url=http://www.popularmechanics.com/technology/digital/fact-vs-fiction/4344500 |title=Fringe's Killer Biological Weapon is Rooted in Fact |last=Townsend|first=Allie |work=[[Popular Mechanics]] |date=2010-02-01 |accessdate=2011-05-25}}</ref>

===Awards and nominations===
{{See also|List of awards and nominations received by Fringe}}

At the 2011 Young Artist Awards, Sierra Pitkin received a nomination for Best Performance in a TV Series under the category "Guest Starring Young Actress Ten and Under".<ref>{{Cite web|url=http://www.youngartistawards.org/noms32.html |title=32nd Annual Young Artist Awards |publisher=Young Artist Foundation |accessdate=2012-03-01}}</ref>

==References==
{{reflist|30em}}

==External links==
{{Wikiquote|Fringe#The_Bishop_Revival_.5B2.14.5D|The Bishop Revival}}
* [http://www.fox.com/fringe/recaps/season-2/episode-14/ "The Bishop Revival"] at [[Fox Broadcasting Company|Fox]]
* {{IMDb episode|1537135}}
* {{tv.com episode|fringe/the-bishop-revival-1318766}}

{{Fringe show}}

{{DEFAULTSORT:Bishop Revival, The}}
[[Category:2010 television episodes]]
[[Category:Fringe (season 2) episodes]]