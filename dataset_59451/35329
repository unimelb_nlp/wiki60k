{{Infobox bus accident
|title=Boys in Red accident
|image= [[Image:Bathurst crash site flowers.png|300px|alt=Flowers and basketball nets behind a puddle and in front of trees.]]
|caption= Flowers at the site of the accident on October 27, 2008
|date=0:08 AST (4:08 UTC)<ref name="1yearCTV">{{cite news
 | url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20090111/crash_anniversary_090111/20090111?hub=TopStories
 | title = Bathurst, N.B., prepares to mark van crash anniversary
 | publisher = Canadian Television | date = 2009-01-11
 | accessdate = 2009-01-11
}}</ref> {{start date|2008|01|12}} 
|location= [[New Brunswick Route 8|Route 8]] near [[Bathurst, New Brunswick]]
|vehicles=[[Ford Club Wagon]], [[Mack truck]]
|pax=12
|deaths= 8 
|injuries= 4
|}}
The '''Boys in Red accident'''{{ref|ref1|[a]}} was a January 12, 2008 collision just outside the city of [[Bathurst, New Brunswick]], [[Canada]], between a [[semi-trailer truck]] and a van carrying the [[basketball]] team from [[Bathurst High School (Bathurst, New Brunswick)|Bathurst High School]]. The accident killed seven students and the wife of the coach, and injured four other occupants in the van.<ref name="BIRexpla">{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/bathurst-high-school-unveils-boys-in-red-memorial-1.737384
 | title = Bathurst high school unveils 'Boys in Red' memorial
 | publisher = Canadian Broadcasting Corporation | date = 2008-06-06
 | accessdate = 2008-10-25
}}</ref> It was the deadliest transportation accident in New Brunswick since 1989, when a [[Cormier Village hayride accident|logging truck tipped onto a hayride]] in [[Cormier Village, New Brunswick|Cormier Village]], killing 13.<ref>{{cite news
 | url = http://www.canada.com/ottawacitizen/features/crash/story.html?id=3fc75df0-1348-4c2c-939f-c613524472ca&k=45080
 | title = Canada's worst road accidents
 | work = [[Ottawa Citizen]] | date = 2006-04-19
 | accessdate = 2008-10-30
}}</ref>

The accident was followed by a national [[day of mourning]] in Canada, and a ban on all E350 [[Ford Club Wagon]] type vehicles being used for [[student transport]] in New Brunswick. Two investigations, one by the [[Royal Canadian Mounted Police]] (RCMP) and the other by [[Transport Canada]], found that the cause of the disaster was a combination of poor road conditions, lack of proper snow tires, and possible driver error. Pressure from the public and victims' families prompted the chief coroner of the province to launch an inquiry, which produced recommendations to improve student transport safety in New Brunswick. The provincial government agreed with the majority of the suggestions and has since enacted many of them.

== Accident ==
The accident occurred on [[New Brunswick Route 8]] shortly after midnight on January 12, 2008, when the team coach was driving the Bathurst High School basketball team northward back from a game in [[Moncton]] against the Moncton High School Purple Knights in an E350 [[Ford Club Wagon]]. Light [[freezing rain]] and low visibility created poor, slippery driving conditions. As the van approached the Bathurst [[city limits]], the coach lost control and veered in front of a southbound semi-trailer truck. The truck collided with the side of the van and the two vehicles came to rest on the [[shoulder (road)|shoulder]] of the southbound lane of the highway approximately {{convert|40|m|ft}} from the point of impact.<ref name="TransportCanadaReport">{{cite web |title=Special Investigation V ASF5-1210 1997 Ford E 350 Super Club XLT Van Vs. 2005 Mack CXN613 "Vision" tractor towing a 2007 Great Dane Super LT reefer semitrailer |publisher=Transport Canada Road Safety|url=http://www.cbc.ca/nb/media/pdf/transport_canada_report.pdf|format=PDF |accessdate=2008-07-30 }}</ref><ref name="CBC0112">{{cite news
 | url = http://www.cbc.ca/news/canada/small-n-b-city-in-disbelief-after-8-killed-in-crash-1.695137
 | title = Small N.B. city in disbelief after 8 killed in crash
 | publisher = Canadian Broadcasting Corporation | date = 2008-01-12
 | accessdate = 2008-01-12
}}</ref><ref name=RCMP_rpt>{{cite news|url=http://www.rcmp-grc.gc.ca/nb/news/Jul2808_223430.html |title=RCMP’s collision reconstructionist report completed into crash which claimed eight lives,, Bathurst, N.B. |publisher=Royal Canadian Mounted Police |date=2008-07-28 |accessdate=2008-10-31 |deadurl=yes |archiveurl=https://web.archive.org/web/20090524004235/http://www.rcmp-grc.gc.ca/nb/news/Jul2808_223430.html |archivedate=May 24, 2009 }}</ref>
{| style="float:right; margin:1em 1em 1em 1em; width:20em; border: 1px solid #a0a0a0; padding: 4px; background-color: #F5F5F5; text-align:left;"
|- style="text-align:center;"
|'''Deaths'''
|- style="text-align:left; font-size:x-small;"
|1. '''Javier Acevedo''', age 17
|- style="text-align:left; font-size:x-small;"
|2. '''Codey Branch''', age 17 
|- style="text-align:left; font-size:x-small;"
|3. '''Nathan Cleland''', age 17
|- style="text-align:left; font-size:x-small;"
|4. '''Justin Cormier''', age 17
|- style="text-align:left; font-size:x-small;"
|5. '''Daniel Hains''', age 17
|- style="text-align:left; font-size:x-small;"
|6. '''Nikki Kelly''', age 15
|- style="text-align:left; font-size:x-small;"
|7. '''Elizabeth "Beth" Lord''', age 51
|- style="text-align:left; font-size:x-small;"
|8. '''Nickolas Quinn''', age 16
|}

The police officer who discovered the wreck initially suspected that only the semi had gone off the road. After further investigation, he found the van and called for emergency services, whose arrival was delayed by freezing rain. The rear wall and a large portion of the right-hand side of the van, including three rows of [[Car seat|seats]], had been torn away, ejecting several occupants.<ref name="TransportCanadaReport"/> Seven players and the wife of the team coach were pronounced dead at the scene. The coach, his daughter and the two other players survived the accident and were rushed to [[Chaleur Regional Hospital]] in Bathurst. Of the injured, one was listed in critical condition, and two in stable. The fourth was released shortly after the accident. The driver of the truck was not injured.<ref name="CTVK8I7">{{cite news
 | url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20080112/nb_crash_080112/20080112?hub=TopStories
 | title = N.B. collision kills 8, including 7 teammates
 | publisher = Canadian Television | date = 2008-01-12
 | accessdate = 2008-01-12
}}</ref>

== Reaction ==
The accident was met with grief and condolences from New Brunswick and around Canada.

John McLaughlin, [[New Brunswick School District 15|School District 15]] Superintendent, stated that the community of Bathurst was in a state of shock and mourning. McLaughlin also noted that the coach held the appropriate license to drive the vehicle and that there were no laws or regulations in the province which regulated team transportation in the event of poor weather. He also added: 
<blockquote>That's really hard because you have to gauge the weather each time you have to make a decision. As for what happened last night, I can't comment. I don't have that information. But in general, our people take great care in making decisions based on the information that they have at the time.<ref>{{cite news
 | url =  http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20080113/team_travel-080113/20080113?hub=Canada
 | title = Winter team travel leaves parents nervous
 | publisher = Canadian Television| date = 2008-01-13
 | accessdate = 2008-10-30
}}</ref></blockquote> Premier [[Shawn Graham]] said "I just want to extend sympathies to the affected families. This is a tragic situation for the community of Bathurst and our province."<ref>{{cite news
 | url =  http://www.gnb.ca/cnb/news/pre/2008e0019pr.htm
 | title = Condolences on deaths of seven students and teacher (08/01/12)
 | publisher = Government of New Brunswick| date = 2008-01-12
 | accessdate = 2008-10-30
}}</ref> Prime Minister of Canada [[Stephen Harper]] said the incident had "shocked the nation" and called for a [[National day of mourning|day of mourning]].<ref name="CBC0112"/> Valery Vienneau, Bishop of Bathurst, read a message on behalf of [[Pope Benedict XVI]] stating: "[The Pope] expresses sentiments of deep sympathy and spiritual closeness to the members of their families and to all staff and students who have been touched by this tragedy. The Pope assures all concerned of his prayers for those who died and for their families."<ref>{{cite news
 | url =  http://www.thestar.com/News/Canada/article/294620
 | title = Mourners fill arena for funeral
 | work = [[Toronto Star]] | date = 2008-01-16
 | accessdate = 2008-11-01
| first=Chris
| last=Morris
}}</ref>

On January 16, all sports-related [[Extracurricular activity|extracurricular activities]] in New Brunswick were cancelled. Services were held across the country. Some schools asked students to wear red and black, the colours of Bathurst High School. The funeral for the seven deceased players was held in Bathurst at the [[K. C. Irving Regional Centre]], which was filled with 6,000 people. An additional 3,500 mourners filled the adjacent rink to watch the service on a widescreen television.<ref>{{cite news
 | url =  http://www.cbc.ca/news/canada/new-brunswick/arena-packed-as-bathurst-students-funeral-begins-1.694528
 | title = Arena packed as Bathurst students' funeral begins
 | publisher = Canadian Broadcasting Corporation| date = 2008-01-16
 | accessdate = 2008-11-01
}}</ref> Elizabeth Lord's private funeral followed the next day.<ref>{{cite news
 | url =  http://www.cbc.ca/news/canada/schools-across-canada-remember-the-boys-in-red-1.737385
 | title = Schools across Canada remember the Boys in Red
 | publisher = Canadian Broadcasting Corporation| date = 2008-01-16
 | accessdate = 2008-10-29
}}</ref>

== Investigation ==

Both the [[Royal Canadian Mounted Police]] (RCMP) and [[Transport Canada]] conducted investigations into the accident. The RCMP released its report on July 29, 2008. The report stated that the van would not have passed a safety inspection at the time of the accident because of [[rust]] in its body, worn [[Tire#All_season|all-season tires]], and faulty brakes. None of these factors could be identified as the sole cause of the accident, but the report noted that "together, they certainly contributed".<ref name=RCMP_rpt/> The report added that speed was not a factor in the crash, as the van was only travelling about {{convert|73|km/h|mph}} and the truck at approximately {{convert|80|km/h|mph}} at the time of impact, both of which were well below the posted limit of {{convert|100|km/h|mph}}. The van had been inspected on October 29, 2007, just over two months prior to the collision, and was four months away from its next inspection. The report resulted in public questioning of New Brunswick's motor vehicle inspection program.<ref name=RCMP_rpt/><ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/07/29/nb-bathurst.html
 | title = Van in Bathurst tragedy would have failed inspection: RCMP report
 | publisher = Canadian Broadcasting Corporation | date = 2008-07-29
 | accessdate = 2008-10-28
}}</ref>

Transport Canada released its report on July 30, 2008. It blamed weather conditions, but also focused on driver fatigue and driver error. It cited several breaches of provincial law regarding the operation of commercial vehicles. The coach had been on duty for sixteen hours when the collision happened, contravening the law that one cannot drive if they have been working more than fourteen consecutive hours. The report also identified inadequate pre-trip inspections and log book keeping, and the lack of a [[contingency plan]] in the event of poor weather.<ref name="TransportCanadaReport"/> Transport Canada stated that although they put an added emphasis on some factors, their report was consistent with the RCMP's. They concurred that the vehicle would have failed an inspection in its pre-collision state because of worn tires and brakes.<ref>{{cite news
 | url = http://www.canada.com/topics/news/national/story.html?id=6e91eb4d-1fdf-499f-a394-ac4721367dc9
 | title = Across Canada, schoolboards following N.B. crash report
 | publisher = Canwest Publishing Inc. | date = 2008-07-30
 | accessdate = 2008-11-03
}}</ref> 

Six of the dead were not wearing [[seat belt]]s, while a seventh was not properly restrained. Greg Sypher, a collision analyst and principal [[Transport Canada]] investigator, later suggested that seat belts most likely would not have saved all the victims' lives.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2009/05/08/nb-bathurst-inquiry-maintenance-1128.html | archive-url = https://archive.is/20130115093019/http://www.cbc.ca/news/canada/new-brunswick/story/2009/05/08/nb-bathurst-inquiry-maintenance-1128.html | dead-url = yes | archive-date = 2013-01-15 | title = Seatbelts might not have saved lives in Bathurst van crash: Transport Canada
 | work = [[National Post]] | date = 2008-07-29
 | accessdate = 2008-10-26
}} </ref> Randy Arseneau credits the fact that his son, Bradd Arseneau, was not wearing his seatbelt for Bradd's survival, saying that if Bradd had not been thrown to the floor just before the collision he may also have died.<ref>{{cite news
 | url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20080114/nb_grieving_080115/20080115/
 | title = N.B. survivor recalls crash as community mourns
 | publisher = Canadian Television | date = 2008-01-15
 | accessdate = 2008-11-02
}}</ref>

== Aftermath ==

[[File:'97-'02 Ford E-Series Van LWB.JPG|thumb|250px|right|alt=Black van parked in a parking lot|Ford E350 van similar to the one involved in the accident, only of different color.]] Following the accident New Brunswick immediately halted the use of 15-passenger vans and began a thorough review of transportation policy; many other provinces later did the same. At the time of the tragedy only [[Nova Scotia]] had a ban on 15-passenger vans, following an accident in 1984.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/01/18/graham-vans.html
 | title = Use of 15-seat vans suspended in N.B.
 | publisher = Canadian Broadcasting Corporation | date = 2008-01-18
 | accessdate = 2008-11-22
}}</ref><ref>{{cite news
 | url = http://www.canada.com/edmontonjournal/news/story.html?id=45cbda7a-9c13-4746-a5fa-2691092185d7
 | title = Edmonton public plans Sept. ban for 'death trap on wheels'
 | work = [[Edmonton Journal]] | date = 2008-01-15
 | accessdate = 2008-08-29
}}</ref><ref name=CtvNovaAc>{{cite news
 | url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20080114/bathurst_van_080114?s_name=&no_ads=
 | title = Experts question safety of van in N.B. crash
 | publisher = Canadian Television | date = 2008-01-14
 | accessdate = 2008-08-30
}}</ref>

Many at Bathurst High School speculated that its senior varsity basketball team would not be able to continue after the crash. Only five team members remained, including three students who did not go to Moncton because of illness.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/01/14/bathurst-school.html
 | title = Students find some laughter as grieving continues in Bathurst
 | publisher = CTV | date = 2008-01-14
 | accessdate = 2009-08-04
}}</ref> Despite this, the Bathurst Phantoms' basketball team defeated the Campobello Vikings 82&ndash;50 to win the Provincial AA championship a year later.<ref>{{cite news
 |url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20090221/bathurst_win_090221/20090221/?hub=TopStories
 | title = Year after tragedy, Bathurst basketball team triumphs
 | publisher = CTV | date = 2009-02-21
 | accessdate = 2009-08-04
}}</ref>

=== Recommendations ===

On August 26, 2008, a government working group presented New Brunswick Education Minister [[Kelly Lamrock]] with eight recommendations for extracurricular transportation. The recommendations included that guidelines for transporting students to school-related extracurricular activities be strengthened, that a [[school bus]] be used when transporting ten or more students, that vehicles be equipped with snow tires if travelling between October 15 and April 30, and that the district superintendent be charged with responsibility for ensuring safe transportation of students to and from extracurricular activities. Lamrock initially suggested that schools voluntarily follow the recommendations, though they were later made into enforceable policy.<ref>{{cite news
 | url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20080826/NB_transport_080826/20080826?hub=Canada
 | title = N.B. adopts new guidelines for student transport
 | publisher = Canadian Television | date = 2008-08-26
 | accessdate = 2008-08-26
}}</ref><ref>{{cite news
 | url = http://telegraphjournal.canadaeast.com/search/article/479759
 | title = Case closed on Bathurst van crash
 | work = [[Telegraph-Journal]] | date = 2008-11-13
 | accessdate = 2008-11-27
}}</ref>

One major effect of the recommendations is that, as of November 2008, it is mandatory that all vehicle operators who intend to use their vehicles to transport students between extracurricular activities be covered by a [[Liability insurance|Third Party Liability]] and Accident Benefit policy. These policies must be in the amount of no less than [[CAD$]]1 million for vehicles with a capacity of fewer than 10 passengers, and no less than CAD$5 million for vehicles with a capacity of 10 or more. This also applies to parents driving others' children to school related extracurricular activities.<ref>{{cite web |title=Guideline 513 Student Activity Vehicles |publisher=New Brunswick Department of Education|url=http://www.gnb.ca/0000/pol/e/513a.pdf|format=PDF |accessdate=2008-11-02 }}</ref>

The changes were met with critical reception in the province. Many students are now required to pay higher sports fees to cover higher transportation costs.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/08/27/nb-transportation.html
 | title = New transport guidelines present challenges for smaller schools
 | publisher = Canadian Broadcasting Corporation | date = 2008-08-27
 | accessdate = 2008-08-30
}}</ref> Some schools do not have access to approved vehicles because the ''Education Act'' prohibits schools and [[school district]]s from owning vehicles. To get around this they use corporate entities to operate vehicles for extracurricular activities. These require money to buy new vehicles and the required [[insurance]], which often requires schools to conduct extra [[fundraising]].<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/08/28/nb-guidelines.html
 | title = Athletic fees may rise, warns high school principal
 | publisher = Canadian Broadcasting Corporation | date = 2008-08-28
 | accessdate = 2008-08-29
}}</ref>

=== Responsibility ===

On November 12, 2008, the RCMP ruled out laying criminal charges in relation to the accident. It stated that the finding had been reviewed by a [[Crown attorney|Crown prosecutor]] and that no wrongdoing was found.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/11/12/nb-bathurst-crash.html
 | title = RCMP rule out charges in fatal Bathurst van crash
 | publisher = Canadian Broadcasting Corporation | date = 2008-11-13
 | accessdate = 2008-11-13
}}</ref> The families of two of the deceased expressed disapproval of the decision and indicated that they may bring a lawsuit against several of the involved parties, though others have publicly stated that they accepted the decision. The chief [[coroner]] of the province stated that a provincial [[inquest]] was possible, though it would take some time to come to any decision on the matter. No decision on whether or not to have an inquest was made for over a month.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/11/12/nb-bathurst-mothers.html
 | title = 'They just can't get away with that': Moms want charges laid in Bathurst van crash
 | publisher = Canadian Broadcasting Corporation | date = 2008-11-13
 | accessdate = 2008-11-13
}}</ref><ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/11/14/nb-bathurst-inquest.html
 | title = Father calls for coroner's inquest into Bathurst van crash
 | publisher = Canadian Broadcasting Corporation | date = 2008-11-14
 | accessdate = 2008-11-14
}}</ref> 

Two families accused the Department of Public Safety of "dragging its feet". These families stated that they wanted a "Van Angels" law that would require any driver taking students outside of their community to possess a Class 2 driver's license. They also wanted a weather law that would prevent students from travelling outside of their communities during [[severe weather]].<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/12/10/nb-bathurst-mothers.html
 | title = Mothers of 2 students killed in van crash demand inquiry, new laws
 | publisher = Canadian Broadcasting Corporation | date = 2008-12-10
 | accessdate = 2008-12-10
}}</ref><ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/12/11/nb-bathurst-mothers.html
 | title = 'I pray to God' for a coroner's inquest into Bathurst van crash: mother
 | publisher = Canadian Broadcasting Corporation | date = 2008-12-11
 | accessdate = 2008-12-11
}}</ref> Ana Acevedo and Isabelle Hains, mothers of two of the deceased, went so far as to launch a website, vanangels.ca, which is dedicated to the memory of their sons and contains a blog related to the accident formerly included a petition to hold an inquest.<ref>{{cite news
 | url = http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20081211/van_nb_081211/20081211?hub=Canada
 | title = Grieving moms want coroner's inquest into van crash
 | publisher = Canadian Television | date = 2008-12-12
 | accessdate = 2008-12-12
}}</ref>

On December 17, 2008, Greg Forestell, the province's acting chief coroner, called for an inquiry into the accident, stating that  "[t]he inquest gives us the opportunity to pull all those facts together in a comprehensive manner and look at the issues in their entirety and have the [[jury]] make recommendations for prevention."<ref name=Bathinqucall>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2008/12/18/nb-bathurst-coroner.html
 | title = N.B. coroner calls inquiry into Bathurst van tragedy
 | publisher = Canadian Broadcasting Corporation| date = 2008-12-18
 | accessdate = 2008-12-19
}}</ref> The decision was met with much approval from the victims' families.<ref name=Bathinqucall/><ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2009/02/24/nb-bathurst-inquest.html
 | title = Coroner's inquest into Bathurst van crash slated for May
 | publisher = Canadian Broadcasting Corporation| date = 2009-02-24
 | accessdate = 2009-02-24
}}</ref> The inquest began on May 4, 2009 and lasted until May 14.  It included testimony from survivors, experts, and provincial officials. The inquest did not blame anyone directly for the accident, but the jury returned 24 recommendations. Kelly Lamrock stated that the province intended to implement the "vast majority" of these recommendations and that a third of them had already been initiated.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2009/05/14/nb-bathurst-coroner-recommendations-230.html
 | title = Bathurst coroner's jury advises limiting winter travel for sports
 | publisher = Canadian Broadcasting Corporation| date = 2009-05-15
 | accessdate = 2009-07-24
}}</ref>

=== Atlantic Wholesalers Limited and Loblaws lawsuit ===

On December 22, 2009 [[Atlantic Wholesalers|Atlantic Wholesalers Limited]] and [[Loblaws]] filed a joint claim against the coach and Bathurst Van Inc. In the lawsuit the plaintiffs complained that negligence on behalf of the defendants was responsible for the crash. Atlantic Wholesalers and Loblaws argue that they are owed $40,667.86 in damages, and an additional $847.50 to cover the clean-up costs. After extensive press coverage and public backlash, Atlantic Wholesalers and Loblaws attempted to defend their action stating that this action was "normal insurance procedures follow an accident of this nature". Public outrage spread and on January 8, 2010, Loblaws dropped the suit for unspecefied reasons after enormous public backlash with numerous persons calling for a boycott of the chain.<ref>{{cite news
 | url = http://www.cbc.ca/news/canada/new-brunswick/story/2010/01/08/nb-bathurst-crash-lawsuit.html
 | title = Loblaws backs off lawsuit against Bathurst coach
 | publisher = Canadian Broadcasting Corporation| date = 2010-01-08
 | accessdate = 2010-01-08
}}</ref>

== Memorial ==
A memorial for the victims was unveiled on June 6, 2008, at Bathurst High School. An archway with a basketball net was placed in a courtyard behind the school. The memorial was funded by money donations. A temporary memorial at the crash site consisting of two basketball nets and flowers was also erected, and one of the nets featuring pictures of the players remained there as of January 2014. A permanent "Boys in Red" memorial for the victims of the van crash has been erected on an empty lot on King Avenue in the city's downtown area near the Bathurst High School. The memorial was erected in a small walking park near the school. It consists of three vertical columns of red quartz. The site has been used for a memorial space in the past as 2 small stones with plaques are placed by the st john st sidewalks of the park with 2 small trees erected as a memorial of 2 Bathurst High students who were among the class of 2001 who died in automobile accidents as well a few months before graduation.

== See also ==

{{wikinews|8 dead following road collision in New Brunswick, Canada}}
* [[List of accidents involving sports teams]]
* [[List of disasters in Canada]]
* [[List of road accidents]]
{{clear}}

== Note ==

*{{note|ref1|[a]}} The accident's name is derived from the Bathurst High School colors, red and black.

==References==
{{Coord|47|35|25|N|65|37|10|W|display=title}}
{{reflist|2}}

==External links==
*[https://web.archive.org/web/20081003075247/http://bathursthigh.nbed.nb.ca:80/birhome.html Bathurst High School Website]
*[http://www.gnb.ca/0000/pol/e/512a.pdf New Brunswick Department of Education Guideline 512]
*[http://www.vanangels.ca/ Van Angels.ca]

{{good article}}

[[Category:Road incidents in Canada]]
[[Category:2008 in Canada]]
[[Category:2008 road incidents|Bathurst Boys in Red accident]]
[[Category:Sport in Bathurst, New Brunswick]]
[[Category:Transport in Bathurst, New Brunswick]]
[[Category:2008 in Canadian sports]]
[[Category:2008 in New Brunswick]]
[[Category:Accidental deaths in New Brunswick]]