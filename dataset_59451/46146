{{Orphan|date=January 2013}}

{{Infobox scientist
| name              = Marco Amabili
| image             =Professor Marco Amabili.jpg
| image_size        =300px
| alt               = Head of man in his thirties with tousled hair sticking up and a small mustache
| caption           =
| birth_place       = [[San Benedetto del Tronto]], [[Italy]]
| residence         = [[Canada]]
| nationality       = [[Italy|Italian]]
| fields            = [[Theoretical, computational and experimental dynamics]]
| workplaces        = [[McGill University]]
| alma_mater        = [[University of Bologna]] (Ph.D.), [[University of Ancona]] (Master Eng.)
| known_for         = Nonlinear vibrations of shells, vibrations of shells with fluid-structure interaction, Shell theories
| footnotes         = He is a Professor at McGill University holding the Canada Research Chair (Tier 1)
}}
[[File:Nonlinear instability versus linear instability for a shell with internal flow.pdf|355px|"355px"|thumb|Fig.1. Comparison of numerical and experimental results for shell divergence. Nondimensional shell amplitude at {{math|''x'' {{=}} ''L'' / 2}} versus the nondimensional water-flow velocity {{math|''V ''}}  for an aluminium shell with internal water flow; {{math|''n'' {{=}} 6}} mode and pressure differential at {{math|''P''<sub>''M''</sub> {{=}} -5.8 kPa}} at {{math|''x'' {{=}} ''L'' / 2}}  ; ——, stable solutions; – –, unstable solutions. Model with {{math| 45}} dofs, {{math|''k''<sub>''a''</sub> {{=}} 1×10<sup>7</sup> N / m<sup>2</sup>}} and {{math|''k''<sub>''r''</sub> {{=}} 0.3×10<sup>3</sup> N / rad}}. ●, experimental data points.]]

'''Marco Amabili''' is a [[Professor]] who holds the [[Canada Research Chair]] (Tier 1) in Mechanical Engineering at [[McGill University]], [[Montreal]], [[Québec]], [[Canada]].<ref name="CRC">{{cite web|url=http://www.chairs-chaires.gc.ca/chairholders-titulaires/profile-eng.aspx?profileId=2595|title=Canada Research Chairs - Chairholders|accessdate=6 January 2013}}</ref>

== Biography ==

Amabili is very well known for the study of nonlinear vibrations and dynamic stability of shell and plate structures, a subject to which he has given many contributions. Professor Amabili serves as Contributing Editor for ''International Journal of Non-linear Mechanics'' (Elsevier). He is also Associate Editor of the ''Journal of Fluids and Structures'',<ref name="Amabili1999">{{cite journal|last=Amabili|first=M.|first2=F. |last2=Pellicano |first3=M.P. |last3=Païdoussis|year=1999|title=Non-linear Dynamics and Stability of Circular Cylindrical Shells Containing Flowing Fluid. Part I: Stability|journal=Journal of Sound and Vibration|volume=225|issue=4|pages=655–699|issn=0022-460X|doi=10.1006/jsvi.1999.2255}}</ref> Elsevier,<ref name="Amabili2000">{{cite journal|last=Amabili|first=M|first2=F. |last2=Pellicano |first3=M.P. |last3=Païdoussis|year=2000|title=Non-linear Dynamics and Stability of Circular Cylindrical Shells Containing Flowing Fluid. Part IV: Large-Amplitude Vibrations with Flow|journal=Journal of Sound and Vibration|volume=237|issue=4|pages=641–666|issn=0022-460X|doi=10.1006/jsvi.2000.3070}}</ref> ''[[Applied Mechanics Reviews]]'', ASME,<ref name="Amabili2002">{{cite journal|last=Amabili|first=Marco|first2=Francesco |last2=Pellicano |first3=Michael P. |last3=Paı̈doussis|year=2002|title=Non-linear dynamics and stability of circular cylindrical shells conveying flowing fluid|journal=Computers & Structures|volume=80|issue=9-10|pages=899–906|issn=0045-7949|doi=10.1016/S0045-7949(02)00055-X}}</ref> Journal of Vibration and Acoustics, ASME, ''Mechanics Based Design of Structures and Machines''.<ref name="Amabili2009">{{cite journal|last=Amabili|first=M.|first2=K. |last2=Karagiozis |first3=M.P. |last3=Païdoussis|year=2009|title=Effect of geometric imperfections on non-linear stability of circular cylindrical shells conveying fluid|journal=International Journal of Non-Linear Mechanics|volume=44|issue=3|pages=276–289|issn=0020-7462|doi=10.1016/j.ijnonlinmec.2008.11.006}}</ref> He is member of the Editorial Board of ''[[Journal of Sound and Vibration]]'', Elsevier, and ''[[International Journal of Structural Stability and Dynamics]]''. He has been the Chair of the ASME Technical Committee ''Dynamics and Control of Systems and Structures''.

Professor Amabili is working in the area of [[vibrations]], nonlinear dynamics and stability of thin-walled structures, reduced-order models and [[fluid-structure interaction]]. His research is multi-disciplinary, and it has been utilized in the design and analysis of aeronautical and aerospace structures, laminated and [[Functionally graded material|FGM]] shell structures, human blood flow problems in aorta, safety of pressure tanks and innovative flow-meters. Amabili is the author of about 400 papers (190 in referred international journals) in vibrations and dynamics and has achieved an [[h-index|''h''-Index]] 39. He is the author of the monograph ''Nonlinear Vibrations and Stability of Shells and Plates'' published by ''[[Cambridge University Press]]''.

Amabili, together with M.P. Païdoussis and F. Pellicano, has showed for the first time the strongly subcritical behavior of the stability of circular cylindrical shells conveying flow (Fig.1). A series of papers presented theoretical, numerical and experimental investigations, showing that a supported circular shell made of aluminum, plastic or rubber presents divergence for much smaller velocity than predicted by linear theory.
Amabili developed in 2014 an innovative 8-parameter thickness deformation shell theory that retains the geometric nonlinear terms in all the 8 parameters describing the shell deformation. This theory is particularly suitable to model soft tissues.

== Education ==

*PhD – [[University of Bologna]], Italy, 1996
*M.S.  – [[University of Ancona]], Italy, 1992

== International Awards ==

*[[ASME Fellow|Fellow]] of the [[ASME|American Society of Mechanical Engineers]] (ASME)
*Plenary lecture at the International Mechanical Engineering Congress and Exposition (IMECE) organized by ASME, Denver, 2011
*Christophe Pierre Research Award, 2015.
*Chair of the Thematic Session on Stability of Structures at the 2016 ICTAM Conference of IUTAM, Montreal

== Books ==

*M. Amabili, ''Nonlinear vibrations and stability of shells and plates'', [[Cambridge University Press]] (2008). ISBN 978-0-521-88329-0

== References ==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Amabili, Marco}}
[[Category:McGill University faculty]]
[[Category:Living people]]
[[Category:Fellows of the American Society of Mechanical Engineers]]