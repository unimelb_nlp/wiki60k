{{redirect|AJPS||Asian Journal of Pentecostal Studies}}
{{Use mdy dates|date=April 2014}}
{{Infobox journal
| title = American Journal of Political Science
| cover =[[File:AJPcover.jpg]]
| discipline = [[Political science]]
| abbreviation = Am. J. Polit. Sci.
| formernames = Midwest Journal of Political Science
| publisher = [[Wiley-Blackwell]] for the [[Midwest Political Science Association]]
| country = United States
| editor = [[William G. Jacoby]] ([[Michigan State University]])
| frequency = Quarterly
| history = 1956–present
| impact = 4.515
| impact-year = 2015
| openaccess =
| website = http://www.ajps.org/
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291540-5907
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291540-5907/issues
| link2-name = Online archive
| ISSN = 0092-5853
| eISSN = 1540-5907
| JSTOR = 00925853
| OCLC = 884770809
| LCCN = 73647828
}}
The '''''American Journal of Political Science''''' is a journal published by the [[Midwest Political Science Association]]. It was formerly known as the ''Midwest Journal of Political Science''. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.515, ranking it 1st out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref> According to [[SCImago Journal & Country Rank]] it ranks 3rd best in the field of Political science and Sociology.<ref>[http://www.scimagojr.com/journalrank.php?category=3312 SCImago Journal & Country Rank]</ref> Also by other authors<ref>[http://journals.cambridge.org/download.php?file=%2FPSC%2FPSC40_04%2FS1049096507071181a.pdf&code=a98b44b428a2d944d563057d88374391 Cambridge Journals]</ref> it is ranked among 5 best journals in political science. The journal publishes articles on all areas of [[political science]].<ref name=WB>{{cite web|url=http://www.wiley.com/bw/journal.asp?ref=0092-5853 |archive-url=https://web.archive.org/web/20090822120238/http://www.wiley.com:80/bw/journal.asp?ref=0092-5853 |dead-url=yes |archive-date=22 August 2009 |title=American Journal of Political Science – Journal Information |format= |publisher=[[Wiley-Blackwell]] |accessdate=29 August 2009 }}</ref>

== See also ==
* [[List of political science journals]]

== Further reading ==
* {{Cite journal | last1 = Gerber | first1 = Alan | last2 = Malhotra | first2 = Neil | title = Do statistical reporting standards affect what is published? Publication bias in two leading political science journals | journal = [[Quarterly Journal of Political Science]] | volume = 3 | issue = 3 | pages = 313–326 | publisher = Now Publishing Inc. | doi = 10.1561/100.00008024 | date = October 2008 | url = https://doi.org/10.1561/100.00008024 | ref = harv | postscript = .}}

== References ==
{{Reflist}}

==External links==
* {{Official website|1=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-5907}}

{{DEFAULTSORT:American Journal Of Political Science}}
[[Category:Political science journals]]
[[Category:Quarterly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1956]]
[[Category:Political science in the United States]]
[[Category:1956 establishments in the United States]]


{{poli-journal-stub}}