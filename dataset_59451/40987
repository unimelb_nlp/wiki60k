[[File:Cloudia Swann Headshot.jpg|thumb|Still from ''[[Real Playing Game|RPG]]'']]

'''Cloudia Swann''' is an English actress.

==Life and career==
Cloudia Swann was born in [[Leicestershire]], and spent her early years living in [[Stratford-upon-Avon]] where she attended [[Bromsgrove School]] until the age of 13. Swann later attended [[Cheltenham College]] for her A-Levels before a 3-year Acting Diploma at the [[Oxford School of Drama]].<ref>"[http://www.imdb.com/name/nm2014175/bio]",IMDB, 2011. Retrieved 2011-10-16.</ref>

Swann graduated from the Oxford School of Drama in 2005 when she landed her first screen leading role opposite Jesper Christanson as Alice in an Independent Feature Shaking Dreamland.<ref>"[http://www.eaglefilms.co.uk/Shaking%20Dream%20Land/press/cloudia_swann.html]", Eagle Films, Oct 2006. Retrieved 2011-10-16.</ref> In 2006 she landed a 3 part series Dream Team 80’s Hewland Productions for Sky TV<ref>"[http://www.harchester.net/characterProfiles/index.php?season=80&profile=121]", Dream Team 80's webpage, 2006. Retrieved 2011-10-16.</ref> before playing a semi regular role for BBC Scotlands River City.<ref>"[http://www.eveningtimes.co.uk/designer-babe-set-to-shake-up-shieldinch-1.972933]", River City - The Evening Times, 2006. Retrieved 2011-10-16.</ref> In 2007 Swann landed leading role Sophie in Shine Productions Dis/connected<ref>"[http://www.digitalspy.co.uk/tv/tubetalk/a91028/preview-new-teen-drama-disconnected.html?start=2]", Digital Spy, 03-27-2008. Retrieved 2011-10-16.</ref> directed by [[Tom Harper (film director)|Tom Harper]], as part of the BBC 3 Pilot season. Swann played Mark Addy’s daughter from the north for ITV Bike Squad directed with Guy Jenkin where she later worked with him on his comedy Fried Brain Sandwich for Sony Pictures.

[[File:RPG Press junket, Altis Prime Hotel, Lisbon.JPG|thumb|Chris Goh, [[Rutger Hauer]] and Cloudia Swann]]

Swann had a small role in an episode of Demons which screened in February 2009, and also appeared in independent film, Shoot on Sight<ref>"[http://www.apunkachoice.com/titles/sho/shoot-on-sight/mid_25492/cast_crew/]", apunkachoice film site, 2009. Retrieved 2011-10-16.</ref> directed by Jag Mundhra starring Brian Cox and Greta Saatchi about the 7/11 bombings in London.

In 2010, Swann was cast in the Award Winning [[The Great Game]] Afghanistan, [[Tricycle Theatre]]. A British series of short plays on the history of Afghanistan and foreign intervention there, from the First Anglo-Afghan War to the present day, Directed by [[Nicolas Kent]] and [[Indhu Rubasingham]].<ref>"[http://www.huffingtonpost.com/lauren-gunderson/theatre-of-the-right-now-_b_774123.html]", The Huffington Post, 10-28-2010. Retrieved 2011-10-16.</ref> Swann played six different characters from a young Afgahn girl in Black Tulips to [[Simon Stephens]] Canopy of Stars where she portrayed a wife confronting her Sergeant Husband about returning to War. The success of the US Tour resulted in a return to Washington DC early 2011 for two exclusive performances for [https://www.theguardian.com/stage/2011/mar/01/the-great-game-pentagon The Pentagon].
[[File:RPG Movie.jpg|thumb|Cloudia Swann]]
Prior to this Swann guested in many well known TV shows such as police drama [[The Bill]], Doctors and [[Hollyoaks]]. Swann appeared in the Multi Million Rowntrees Randoms Campaign playing a random primary school teacher<ref>"[https://www.youtube.com/watch?v=BPRwHxXbCZc]", Rowntrees Advert - Youtube, 2010. Retrieved 2011-10-16.</ref> which was aired across the UK. Other company commercials in which Swann has appeared include telecommunications company Orange, Vageta and the Co-op.

She starred in a stage production of Of Mice and Men at the [[Dukes Theatre]] in Lancaster.<ref>"
[http://www.thestage.co.uk/reviews/review.php/25682/of-mice-and-men]", Stage Review, 28-09-2009. Retrieved 2011-10-16.</ref> In September 2011 she completed filming Real Playing Game a thriller starring a cast from around Europe, produced by MGN Filmes [[Tino Navarro]].

==Film==
{| class="wikitable"

[[File:Ramanujan Movie.jpg|thumb|Cloudia Swann]]

|-
! Year !! Film !! Character !! Director/Producer !! Studio
|-2013 || [[Amar,Akbar n Tony]] || Nicola ||
|-
| 2013 || [[Ramanujan (film)|Ramanujan]] || Ms. Bourne || [[Gnana Rajasekaran]] || Camphor Cinema
|-
| 2012 || 7300 Days Later || Eddie || Luca Bertoluzzi || Passion Raw
|-
| 2012 || Next Exit || Jenny || Ben Goodger || Short Film
|-
| 2011 || RPG || Young Player #3 || Tino Navarro || MGN Filmes
|-
| 2007 || Shoot on Sight || Justine Miller || Jag Mundhra || Cine Boutique
|-
| 2006 || All Bar Love || Eva || James Deryshire || Te papa
|-
| 2006 || Young Hearts Leap || Face Girl || [[Alex Kalymnios]] || BFI
|-
| 2005 || Shaking Dreamland || Alice || Martina Nagel || Eagle Films
|}
[[File:RPG Movie Premier.JPG|thumb|Cloudia Swann and [[Dafne Fernández]]]]

==Television==
{| class="wikitable"
|-
! Year !! Production !! Character !! Director/Producer !! Studio
|-
| 2013 || [[Real Playing Game|RPG]] || Young Player #8 || David Rebordão and Tino Navarro || 
|-
| 2012 || Hollyoaks || Helen || Mickey Jones || [[Lime Pictures]]
|-
| 2009 || Fried Brain Sandwich || Carmel || [[Guy Jenkin]] || [[Sony Pictures]]
|-
| 2009 || Hollyoaks || Sandy || Tant Lay || Lime Pictures
|-
| 2009 || Doctors || Sally || [[Dan Wilson (playwright)|Daniel Wilson]] || [[BBC]]
|-
| 2009 || Demons || Greta || [[Tom Harper (film director)|Tom Harper]] || Shine Productions
|-
| 2008 || Dis-connected || Sophie (lead) || [[Tom Harper (film director)|Tom Harper]] || Shine Productions
|-
| 2007|| Bike Squad || Susan || Guy Jenkin || Hatrick Productions
|-
| 2007 || River City || Sophie || Various Directors || [[BBC Scotland]]
|-
| 2006 || The Bill || Sally Ayling || Robert Del Maestro || Thames TV
|-
| 2006 || Dream Team Retro || Susan || Rob McGillivery || Hewland
|}

==Theatre==

                                                  [[File:Of Mice and Men.jpg|thumb|Cloudia Swann - Of Mice and Men]]
{| class="wikitable"
|-
! Year !! Production !! Character !! Director
|-
| 2010/11 || Black Tulips (The Great Game)|| Nahid || Nicolas Kent
|-
| 2010/11 || Wood for the Fire || Karen|| Rachel Grunwald
|-
| 2010/11 || Night Is Darkest Before the Dawn || Minoo || Indhu Rubasingham
|-
| 2010/11 || On the Side of Angels || Fiona || Indhu Rubasingham
|-
| 2010/11 || Fiona Gall || Fiona || Nicolas Kent
|-
| 2010/11 || Canopy of Stars || Cheryl || Nicolas Kent
|-
| 2009 || [[Of Mice and Men (play)|Of Mice and Men]] || Curleys Wife || Kevin Dyer
|-
| 2006 || [[Othello]] || Desdemona || Kathryn Hunter, Marcello Magni
|-
| 2004 || Edmund || Glenna|| Che Walker
|}
                         [[File:Cloudia Swann and Pedro Granger.JPG|thumb|Still from set]]

==Other work==
{| class="wikitable"
|-
! Year !! Production !! Character !! Medium
|-
| 2012 || Orange Christmas Commercial || Mum || Romania (TV,Internet)
|-
| 2010 || Canopy of Stars || Cheryl || BBC 3 (Radio)
|-
| 2010 || Tantemarie Commercial || Girl || Gordan Ramsey (Viral)
|-
| 2009 || Loose Women || N/a|| BBC Lancaster (Radio)
|-
| 2009 || Rowntrees Randoms Commercial || School Teacher || UK (TV)
|-
| 2008 || Vageta Commercial || Girl || Baltic States (TV)
|-
| 2007 || Orange Commercial || Sushi Girl || Israel (Cinema and TV)
|-
| 2006 || The Co-op Commercial || Jealous Girl || Switzerland (Cinema and TV)
|}

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->
1. http://news.bbc.co.uk/local/lancashire/hi/things_to_do/newsid_8246000/8246828.stm
Of mice and Men BBC Lancashire Radio Interview and Images

2. http://www.eveningtimes.co.uk/designer-babe-set-to-shake-up-shieldinch-1.972933
River City - The Evening Times Profile on Cloudia Swann

3. http://www.denofgeek.com/television/249802/lily_allen_eyes_doctor_who_role.html
Doctor Who

4. http://www.eaglefilms.co.uk/Shaking%20Dream%20Land/press/cloudia_swann.html
Shaking Dreamland Film Press Page

5. http://www.thestage.co.uk/reviews/review.php/25682/of-mice-and-men
Of Mice and Men - The Stage Review

6. http://www.digitalspy.co.uk/tv/tubetalk/a91028/preview-new-teen-drama-disconnected.html?start=2
Dis/connected - Shine Productions Film

7. http://virtual-lancaster.blogspot.com/2009/10/in-review-of-mice-and-men.html
Of Mice and Men - Virtual Lancaster

8. http://www.berkeleyrep.org/press/photos-10gg.asp
The Great Game - Berkley Rep

9. http://outwestarts.blogspot.com/2010/12/my-ten-and-only-best-of-theater-10.html
The Great Game - 10 of the best Review

10. http://www.variety.com/review/VE1117943942?refCatId=33
The Great Game Variety Review

11. http://outwestarts.blogspot.com/2010/10/like-poppies.html
The Great Game Review

12. http://www.bbc.co.uk/programmes/b00v4m2g
BBC radio 3 Canopy of Stars

13. http://www.britishtheatreguide.info/reviews/greatgame10-rev.htm
The Great Game (review)

14. http://www.britishtheatreguide.info/reviews/greatgame10-rev.htm
The Great Game(review)

15. http://www.theatreindc.com/playdetail.php?playID=174
The Great Game (Washington DC)

16. http://www.huffingtonpost.com/lauren-gunderson/theatre-of-the-right-now-_b_774123.html
(Berkley US Review)

17. http://www.washingtonian.com/blogarticles/artsfun/afterhours/16812.html
(Washington The Great Game Review)

18. http://theater.nytimes.com/show/25491/The-Great-Game-Afghanistan-/overview
(New York Times - Overview of cast)

19. http://dctheatrescene.com/2010/09/21/the-great-game-afghanistan-part-2/
The Great Game Review

20. http://www.stagevoices.com/stage_voices/2010/12/the-great-game-afghanistan-in-new-york-review.html
(New York performance standout)

21. http://www.zimbio.com/Afghanistan/articles/eyZdZh3VS8Z/Olivier+Nominated+Great+Game+Afghanistan+Arrives
http://www.abouttheartists.com/productions/30957
(Minneapolis Programme - The Great Game)

==External links==
{{commons category}}
1. http://www.imdb.com/name/nm2014175/
(Cloudia Swann IMDB Page).

2. https://www.youtube.com/watch?v=BPRwHxXbCZc
(Cloudia Swann as a teacher in Rowntree's Randoms Advert).

{{DEFAULTSORT:Swann, Cloudia}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:People from Oadby]]
[[Category:English television actresses]]
[[Category:English stage actresses]]
[[Category:People educated at Bromsgrove School]]
[[Category:Actresses from Leicestershire]]
[[Category:People educated at Cheltenham College]]
[[Category:Alumni of the Oxford School of Drama]]