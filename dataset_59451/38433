{{good article}}{{italic title}}
{{Infobox book
| name             = ''A Book of Mediterranean Food''
| image            = File:A_Book_of_Mediterranean_Food_cover.jpg
| image_size       =
| caption          = Front cover of the first edition
| author           = [[Elizabeth David]]
| illustrator      = [[John Minton (artist)|John Minton]]
| country          = England
| subject          = [[Mediterranean cooking]]
| genre            = [[Cookery]]
| publisher        = [[John Lehmann]]
| pub_date         = 1950
| pages            =
}}
'''''A Book of Mediterranean Food''''' was an influential<ref name=Panayi191/> cookery book written by [[Elizabeth David]] in 1950, and published by [[John Lehmann]]. After years of [[rationing]] and wartime austerity, the book brought light and colour back to [[English cuisine|English cooking]], with simple fresh ingredients, from David's experience of [[Mediterranean cooking]] while living in France, Italy and Greece. The book was illustrated with quotations from famous writers.

At the time, many ingredients were scarcely obtainable, but the book was quickly recognised as serious, and within a few years it profoundly changed English cooking and eating habits.

==Context==
[[File:Britain Queues For Food- Rationing and Food Shortages in Wartime, London, England, UK, 1945 D24983.jpg|thumb|The reality of [[rationing]] and austerity: queuing for fish in London, 1945]]

Returning to postwar England, still with food [[rationing]], after years living in the Mediterranean with its wealth of fresh ingredients, [[Elizabeth David]] found life grey and daunting. The food was terrible: "There was flour and water soup seasoned solely with pepper; bread and gristle rissoles; dehydrated onions and carrots; corned beef [[toad in the hole]]. I need not go on."<ref name=ogw21>David (1986), p. 21</ref>

==Book==
Partly to earn some money, and partly from an "agonized craving for the sun", David began writing articles on [[Mediterranean cookery]].<ref name=ogw21/> Her first efforts were published in 1949 in the British magazine ''Harper's Bazaar''. From the outset, David refused to sell the copyright of her articles, and so she was able to collect and edit them for publication in book form.<ref>David (1986), p. 14</ref> Even before all the articles had been published, she had assembled them into a typescript volume and submitted it to a series of publishers, all of whom turned it down. One of them explained that a collection of unconnected recipes needed linking text.

{{Quote box |quoted=true |bgcolor=#F5F9FC |salign=center | quote = With this selection...of Mediterranean dishes, I hope to give some idea of the lovely cookery of those regions to people who do not already know them, and to stir the memories of those who have eaten this food on its native shores, and who would like sometimes to bring a flavour of those blessed lands of sun and sea and olive trees into their English kitchens. | source = [[Elizabeth David]], Preface.| align=left| width=33%}}

David took this advice, but, conscious of her inexperience as a writer, she kept her own prose short and quoted extensively from established authors whose views on the Mediterranean might carry more weight. In the published volume, the sections are linked by substantial extracts from works by writers including [[Norman Douglas]], [[Lawrence Durrell]], [[Gertrude Stein]], [[D. H. Lawrence]], [[Osbert Sitwell]], [[Compton Mackenzie]], [[Arnold Bennett]], [[Henry James]] and [[Théophile Gautier]].<ref>David (1999), p. vii</ref> She submitted the revised typescript to [[John Lehmann]], a publisher more associated with poetry than cookery, but he accepted it, agreeing to an advance payment of £100. ''A Book of Mediterranean Food'' was published in 1950.<ref name=c144>Cooper, p. 144</ref> Lehmann had suggested it be named "The Blue Train Cookery Book", since he supposed that the romance of Mediterranean countries was to be found in the exciting train journeys to reach them.<ref>{{cite journal |title=Our Summer  2000  Books |journal=The Persephone Quarterly |date=2000 |volume=6 |issue=Summer 2000 |page=4 |url=http://www.persephonebooks.co.uk/newsletters/2000-No.06-Summer.pdf}}</ref>

The book appeared when food rationing imposed during the Second World War remained fully in force in Britain. As David later put it, "almost every essential ingredient of good cooking was either rationed or unobtainable."<ref name=dix>David (1999), p. ix</ref> She therefore adapted some of the recipes she had learned during in the years when she lived in Mediterranean countries, "to make up for lack of flavour which should have been supplied by meat or stock or butter."<ref name=dix/>

===Lyricism===
The historian of food Panikos Panayi argues that with ''A Book of Mediterranean Food'', David profoundly changed English cooking. He considers the opening section to contain "perhaps the most evocative and inspirational passage in the history of British cookery writing":<ref name=Panayi191>Panayi, 2010, pp. 191–195</ref>

{{quote|The cooking of the Mediterranean shores, endowed with all the natural resources, the colour and flavour of the South, is a blend of tradition and brilliant improvisation. The Latin genius flashes from the kitchen pans. It is honest cooking too; none of the sham Grand Cuisine of the International Palace Hotel<ref name=Panayi191/>}}

David then describes the region and its perfumes:

{{quote|From Gibraltar to the Bosphorous, down the Rhone Valley, through the great seaports of Marseilles, Barcelona, and Genoa ... stretches the influence of Mediterranean cooking, conditioned naturally by variations in climate and soil and the relative industry or indolence of the inhabitants. The ever recurring themes in the food throughout these countries are the oil, the saffron, the garlic, the pungent local wines...<ref name=Panayi191/>}}

===Illustrations===
[[File:A Book of Mediterranean Cooking port scene by John Minton.jpg|thumb|upright|Mediterranean port scene, drawn by [[John Minton (artist)|John Minton]] for the book. Elizabeth David did not like the illustrations.]]
Lehmann commissioned a coloured dust-jacket painting and black and white internal illustrations from his friend the artist [[John Minton (artist)|John Minton]].  Writers including [[Cyril Ray]] and [[John Arlott]] commented that Minton's drawings  added to the attractions of the book.<ref>"Cookery", ''[[The Times Literary Supplement]]'', 9 June 1950, p. 362; Arlott, John. "From Time to Time", ''[[The Guardian]]'', 18 July 1986, p. 15; and "First Bites", ''The Guardian'', 15 March 1994, p. B5</ref> David, a woman of strong opinions,<ref name=Norrington-Davies>{{cite web|last1=Norrington-Davies|first1=Tom|title='You can smell the sea or touch the olive branch...'|url=http://www.telegraph.co.uk/foodanddrink/3323687/You-can-smell-the-sea-or-touch-the-olive-branch....html|publisher=The Telegraph|accessdate=19 April 2015|date=5 January 2006}}</ref> thought good illustration important.<ref name=c144/> Minton provided 15 decorations to give a feeling of the Mediterranean, rather than simple illustrations of dishes from David's recipes. For example, his port scene shows a sailor{{efn|The sailor resembles Minton himself.}} drinking and conversing with a young woman beside a table laden with food; in the background is a street restaurant and boats in a harbour. Although David did not like Minton's black and white drawings, she described his jacket design as "stunning".<ref name=c144/> She was especially taken with "his beautiful Mediterranean bay, his tables spread with white cloths and bright fruit" and the way that "pitchers and jugs and bottles of wine could be seen far down the street."<ref>Cooper, p. 152</ref>

===Contents===
The chapters cover in turn: soups; eggs and luncheon dishes; fish; meat; substantial dishes; poultry and game; vegetables; cold food and salads; sweets; jams, chutneys and preserves; and sauces.<ref>David (1999), p. iii</ref>

The soup chapter sets the pattern for the book, with short, simple recipes, such as soup of haricot beans – two brief paragraphs – interspersed with long, complex ones like that for Mediterranean fish soup, which covers three pages.<ref>David (1999), pp. 17 and 18–20</ref> The eggs and luncheon dish section likewise balances the concise and simple such as ''[[ratatouille]] aux oeufs'' against the detailed and discursive three-page consideration of omelettes.<ref>David (1999), 30–32, and 34</ref> Unlike many writers of cookery books, David rarely gives precise quantities or timings: in the fish chapter her suggestion for fresh tuna is:
{{quote|cut it into thick slices like a salmon steak, and sauté it in oil or butter, adding, half-way through the cooking 2 or 3 tomatoes, chopped, a handful of cut parsley, and a small glass of wine, either red or white. Serve plainly cooked potatoes with it.<ref>David (1999), p. 66</ref>|}}
The meat section begins with recipes for veal, generally unfamiliar at English tables in 1950.<ref>David (1999), pp. 76–77</ref> David also gives recipes for kid and boar.<ref>David (1999), pp. 91–93</ref> Mutton, by contrast, was more often served then than in more recent decades, and David gives four recipes for it, one of them disguising the flavour to taste like venison by long marinating and highly seasoned saucing,<ref>David (1999), pp. 80–83</ref>

[[File:Bowl of cassoulet.JPG|thumb|left|[[Cassoulet]], a southern French dish described in the book]]
The "substantial dishes" chapter discusses and illustrates the merits of [[risotto]] and [[paella]], and deals with [[polenta]] and [[spaghetti]] – both less familiar in Britain then than now – and goes on to describe [[cassoulet]].<ref>David (1999), pp. 98–108</ref> The next chapter, on poultry and game, begins with recipes for cooking chicken and duck, and goes on to  [[partridge]] – both ''à la provençale'' and in Greek style   – [[quail]] and pigeons, concluding with [[snipe]] cooked ''[[en papillote]]'' with mushrooms.<ref>David (1999), pp. 114–119</ref>

The Mediterranean theme of the book is emphasised in the section on vegetables, in which there are five [[aubergine]] recipes and only one potato dish (''[[pommes Anna]]''). Dishes from Greece and North Africa are included along with typical southern French standards such as ''tomates provençales''. The occasional non-Mediterranean dish is included, including ''[[cèpes]] à la bordelaise'' (fried in olive oil with parsley and garlic).<ref>David (1999), pp. 134–143</ref>

The cold food and salads chapter gives instructions for three dishes of cold chicken, several traditional pâtés and terrines, and another non-Mediterranean recipe, the traditional Easter dish from [[Burgundy]], ''jambon persillé de Bourgogne''. David adds to this section suggestions for [[hors d'oeuvres]], including Greek dishes then unfamiliar in Britain including [[dolma|dolmádés]] and [[Taramasalata|taramá]], as well as traditional French recipes such as ''sardines marinées à la niçoise''.<ref>David (1999), p. 148–158</ref>

In the section on sweets, David comments that throughout the Mediterranean countries, the more complicated sweets are very often bought from [[pâtisserie]]s; the few recipes she gives are for simple, traditional sweets made at home, such as ''torrijas'' (also called ''[[pain perdu]]'') and cold orange soufflé.<ref>David (1999), 166–173</ref> The jams, chutneys and preserves section includes preserved melon alongside more familiar fruits such as pears and plums.<ref>David (1999), 178–181</ref>

In the final chapter, on sauces, David includes classics like [[Béchamel sauce|béchamel]], [[Béarnaise sauce|béarnaise]], [[Hollandaise sauce|hollandaise]] and [[mayonnaise]] (which, she advises, "stir steadily but not like a maniac"). To these she adds Turkish, Greek, Italian and Egyptian sauces, the majority of them intended to go with Mediterranean fish dishes.<ref>David (1999), p. 186–195</ref>

==Reception==

===Contemporary===
''[[The Times Literary Supplement]]'' observed in 1950 that "while one might hesitate to attempt 'Lobster à la Enfant Prodigue' (with champagne, garlic, basil, lemon, chervil, mushrooms and truffles), the resourceful cook with time to explore London's more individual shops, and money, should not often be nonplussed."<ref>"Cookery", ''The Times Literary Supplement'', 9 June 1950, p. 365</ref> ''[[The Observer]]'' commented, also in 1950, that the book deserved "to become the familiar companion of all who seek uninhibited excitement in the kitchen."<ref>Chandos, John. "Southern Spells", ''The Observer'', 18 June 1950, p. 7</ref>

===Modern===
The celebrity cook [[Clarissa Dickson Wright]] comments that the book was "a breath of fresh air in the years of austerity that followed the Second World War, and [David's] espousal of excellent, well-prepared ingredients has become the hallmark of English food at its best."<ref>Dickson Wright, 2011, p. 444</ref> At that time "food was dull, vegetables were [thoroughly] stewed and olive oil was something you bought at the chemist and was marked 'for external use only'." David, on the other hand, "evoked a world of sunshine and [[lavender]], of [[bougainvillea]] and [[canna (plant)|canna]]s, and of fresh and simple food beautifully prepared."<ref>Dickson Wright, 2011, p. 450</ref>

[[File:Ratatouille.jpg|thumb|[[Ratatouille|Ratatouille niçoise]], a Mediterranean dish introduced to Britain by the book]]

John Koski, writing in the ''[[Daily Mail]]'', notes that most of the publishers at the time thought a cookery book "at best absurd" when there was so little food to cook, and "the ingredients of the Mediterranean lands – olive oil, saffron, garlic, basil, aubergines, figs, pistachio nuts – were hardly to be found in Central London, and readers had to rely on memory or imagination" to enjoy David's recipes. All the same, the recipes were "honest", "collected in Provence, Italy, Corsica, Malta and Greece", and the book was "acclaimed as a serious work". Within a few years, Koski observes, "[[paella]], [[moussaka]], [[ratatouille]], [[hummus]] and [[gazpacho]] had become familiar in home kitchens, restaurants and supermarkets throughout the country."<ref>{{cite web |last1=Koski |first1=John |title=Elizabeth David: The woman who changed the way we eat |url=http://www.dailymail.co.uk/home/you/article-1329170/Elizabeth-David-The-woman-changed-way-eat.html |publisher=Daily Mail |accessdate=18 April 2015 |date=13 November 2010}}</ref>

Rachel Cooke, writing in ''[[The Guardian]]'', quotes the chef Simon Hopkinson, who knew David in the 1980s, as believing that David's "powerful effect .. on British palates .. was as much a question of timing as anything else". She "arrived on the scene at just the right moment: the British middle classes, exhausted by austerity, were longing, even if they did not precisely know it, for the taste of sunshine."<ref>{{cite web |last1=Cooke |first1=Rachel |title=The enduring legacy of Elizabeth David, Britain's first lady of food |url=https://www.theguardian.com/lifeandstyle/2013/dec/08/elizabeth-david-first-lady-of-food |publisher=The Guardian |accessdate=18 April 2015 |date=8 December 2013}}</ref>

Caroline Stacey, writing in ''[[The Independent]]'', calls the book "her hymn of longing to the cooking around the southern shores", noting that it "changed what the British middle classes ate", and that she "ushered not only olive oil and garlic, but also [[aubergine]]s, [[courgette]]s and [[basil]] on to the stripped-pine tables of 1960s kitchens."<ref>{{cite web |last1=Stacey |first1=Caroline |title=Elizabeth David: And you thought Nigella was sexy... |url=http://www.independent.co.uk/life-style/food-and-drink/features/elizabeth-david-and-you-thought-nigella-was-sexy-522928.html |publisher=The Independent |date=14 January 2006 |accessdate=18 April 2015}}</ref>

Melanie McDonagh, writing in ''[[The Daily Telegraph|The Telegraph]]'', states that with ''A Book of Mediterranean Food'', David "introduced the Brits to the cooking of Greece, Italy and Provence in 1950 after her return from Greece, via Egypt and India". She comments that the cookery writer [[Jane Grigson]], a "devotee", said "Basil was no more than the name of bachelor uncles, courgette was printed in italics as an alien word, and few of us knew how to eat spaghetti ... Then came Elizabeth David, like sunshine." McDonagh adds that David "was one of the first and much the classiest of the personality food writers, even though she was never a telly chef: paving the way for [[Jamie Oliver|Jamie]], [[Nigella Lawson|Nigella]], [[Nigel Slater|Nigel]] and [[Hugh Fearnley-Whittingstall|Hugh F-W]]."<ref>{{cite web |last1=McDonagh |first1=Melanie |title=Elizabeth David: The writer who transformed British life |url=http://www.telegraph.co.uk/foodanddrink/10454189/Elizabeth-David-The-writer-who-transformed-British-life.html |publisher=The Daily Telegraph |accessdate=18 April 2015 |date=16 November 2015}}</ref>

Dissenting from the general acclaim, Tom Norrington-Davies, also writing in ''The Telegraph'', argues that the book "reached only a very small section of the population", but at once qualifies this, stating that these readers were "undergoing a dramatic upheaval. Educated, moderately wealthy women suddenly found themselves in their kitchens without [[servant]]s". He cites Jane Grigson's observation, introducing a collection of David's writing, that "Elizabeth didn't so much restore [middle-class women's] confidence in cooking as invent it".<ref name=Norrington-Davies/>

Joe Moran, writing in the ''[[Financial Times]]'', describes the genesis of the book as a "defining moment". It was when, "stranded by a blizzard" in a hotel in Ross-on-Wye whose restaurant served meals so dismal that they seemed to her to be "produced with a kind of bleak triumph which amounted almost to a hatred of humanity and humanity’s needs", David felt her famous "agonised craving for the sun". Furious at the joyless food, she started to draft the "sensuous descriptions" of Mediterranean food that led to ''A Book of Mediterranean Food''.<ref>{{cite web |last1=Moran |first1=Joe |title=Defining Moment: Elizabeth David reinvents middle-class British cuisine, February 1947 |url=http://www.ft.com/cms/s/0/5307c754-078e-11de-9294-000077b07658.html |publisher=Financial Times |accessdate=18 April 2015 |date=7 March 2009}}</ref>

Marian Burros, writing in the ''[[New York Times]]'', comments that David first showed "her importance" with the book. "The ration-weary English could barely buy enough to eat but they were enchanted by her descriptions of meals that included eggs, butter, [[seafood]], tomatoes, [[olives]], apricots, ingredients that were difficult, or impossible, to obtain. Foods that are taken for granted today in England – [[garlic]], [[olive oil]], [[Parmigiano Reggiano]] – were unknown and generally viewed with suspicion before Mrs. David."<ref>{{cite news|last1=Burros|first1=Marian|title=Elizabeth David Is Dead at 78; Noted British Cookbook Writer|publisher=The New York Times|date=28 May 1992}}</ref>

==Editions==
The book has appeared in the following editions since 1950, including translations into [[Danish language|Danish]] and [[Chinese language|Chinese]].<ref>{{cite web| url=http://www.worldcat.org/title/book-of-mediterranean-food/oclc/48871702/editions?start_edition=1&sd=asc&referer=di&se=yr&qt=sort_yr_asc&editionsView=true&fq= |title=A Book of Mediterranean Food | publisher=WorldCat | accessdate=18 April 2015}}</ref>

{| class="wikitable collapsible collapsed"
! Editions
|-
|<div style="-moz-column-count:3;-webkit-column-count:3; column-count:3;">
<small>
* London: Lehmann, 1950 (1st ed.)
* [[Harmondsworth]] : Penguin, 1950 (paperback)
* London: Lehmann, 1951
* [[New York City]]: Horizon, 1952
* Harmondsworth, New York : Penguin, 1955
* Harmondsworth : Penguin, 1956
* London: Macdonald, 1958 (2nd ed.)
* London: Cookery Book Club, 1958
* [[Baltimore]]: Penguin, 1958
* Baltimore: Penguin, 1960
* Harmondsworth: Penguin, 1960
* Harmondsworth: Penguin, 1965
* Harmondsworth, Baltimore : Penguin, 1966
* London: Cookery Book Club, 1968
* Harmondsworth: Penguin, 1971
* Harmondsworth: Penguin, 1975
* Harmondsworth: Penguin, 1977
* Harmondsworth: Penguin, 1983
* [[Copenhagen]]: Hans Reitzel/Gyldendals Bogklub, 1984
* Harmondsworth: Penguin, 1987
* London: Dorling Kindersley, 1988 (illus., rev.)
* [[Sydney]]: Collins, 1988
* Harmondsworth: Penguin, 1989 (2nd ed.)
* Harmondsworth: Penguin, 1991 (new intro.)
* Harmondsworth: Penguin, 1998
* New York: New York Review Books, 2002
* China: 麥田出版 / Tai Bei Shi, 2006 (地中海風味料理 / Di zhong hai feng wei liao li, by 大衛 David, Elizabeth with Da Wei; Fang-tian Huang
* London: [[Folio Society]], 2009 (intro. by [[Julian Barnes]], colour illus. by Sophie MacCarthy)<ref>{{cite web| url=http://www.foliosociety.com/book/ELD/mediterranean-food-and-other-writings |title=A Book of Mediterranean Food and Other Writings |publisher=Folio Society| date=2009}}</ref>
</small></div>
|}

==Notes==
{{notelist}}

==References==
{{reflist|28em}}

==Sources==

* {{cite book | last= Cooper | first= Artemis |year= 2000| title= Writing at the Kitchen Table – The Authorized Biography of Elizabeth David | location=London | publisher= Michael Joseph | isbn=0-7181-4224-1}}
* {{cite book | last=David | first=Elizabeth | year =1986 |origyear=1984 |edition=second | title=An Omelette and a Glass of Wine | location=Harmondsworth | publisher=Penguin | isbn=0-14-046721-1 }}
* {{cite book | last=David | first=Elizabeth | year =1999 |origyear=1950, 1951, 1955 |title=Elizabeth David Classics – Mediterranean Food; French Country Cooking; Summer Food | edition=second | location=London | publisher=Grub Street | isbn=1-902304-27-6 }}
* {{cite book | last=Dickson Wright | first=Clarissa  | year=2011 | title=A History of English Food | location=London | publisher=Random House | isbn= 978-1-905211-85-2}}     
* {{cite book | last=Panayi | first=Panikos |date=2010 |origyear=2008 | title=Spicing Up Britain | location=London | publisher=Reaktion Books|isbn=978-1-86189-373-4}}

==External links==

{{English cuisine}}

{{DEFAULTSORT:Book of Mediterranean Food, A}}
[[Category:English cuisine]]
[[Category:1950 books]]
[[Category:British cookbooks]]