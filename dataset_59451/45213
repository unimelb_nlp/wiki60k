{{more footnotes|date = June 2012}}
[[Image:Schneider von Ulm.jpg|thumb|Contemporary drawing of the flight attempt]]

'''Albrecht Ludwig Berblinger''' (24 June 1770, [[Ulm]] &ndash; 28 January 1829, Ulm), also known as '''the Tailor of Ulm''', is famous for having constructed a working [[flying machine]], presumably a [[hang glider]].

== Early life ==

Berblinger was the seventh child of a poor family. When he was 13 his father died and he was sent to an orphanage. There he was forced to become a tailor although he wanted to become a watchmaker.
He became a [[master craftsman]] at 21, but he still was interested in mechanics. In his spare time in 1808 he invented the first [[artificial limb]] with a joint.

== Flight attempts ==
{{Tone|date = June 2012}}
[[File:Stamps of Germany (DDR) 1990, MiNr 3313.jpg|thumb|Berblinger's design drawing of his glider on a GDR stamp]]
One of Berblinger's inventions was what appears to be a [[hang glider]]. He worked on it for years, improving it and watching the flight of owls. People made fun of him and he was threatened with exclusion from the [[guild]]. He was ordered to pay a large fine for his working outside of the guild. Nevertheless, he invested his whole income in his project. King [[Frederick I of Württemberg]] became interested in his work and sponsored him with 20 [[Louis (coin)|Louis]].

He tried to demonstrate the glider on the evening of 30 May 1811 in the presence of the king, his three sons and the crown prince of Bavaria. The King and a large number of citizens waited for the flight but Berblinger cancelled it, claiming that his glider was damaged. The next day he made a second attempt from a higher location - the Adlerbastei (''Eagles Bastion'').<ref>This structure no longer exists but a plaque commemorates it on the banks of the river in Neue Strasse just past the  Dreifaltigkeitskirche (''Holy Trinity Church'')</ref> The King had left by this time, but his brother Duke Heinrich and the princes stayed to watch. Berblinger waited so long for a good wind that a policemen finally gave him a push and Berblinger fell into the Donau (''Danube''). Other versions of this account have no mention of the policeman and claim that the difference in temperature over the cold Donau (Danube) limited thermal updrafts and therefore the glider failed to lift. 
He survived and was rescued by fishermen, but
his reputation was  ruined as a result and his work suffered. He was 58 years old when he died in a hospital.

The story of the tailor, who tried to fly, subsequently resulted in some fleer and allusions in publications of the 19th century. When [[Wilhelm Busch]] drew a man falling into a stream  in his picture story [[Max and Moritz]], he could count on some awareness of his readers. It was not until the end of the century that [[Otto Lilienthal]] proved the feasibility of heavier-than-air flight.

== Reception ==

* A reconstruction of Berblinger's flying device (in the form of a pair of wings) can be seen in the Ulm Rathaus (''City Hall'') suspended above the stairwell near the Standesamt (''Registrary'') where civil weddings are held. There is also another reconstruction of the glider in the ground level of building B in the Fachhochschule (''University of Applied Science'') Ulm.
* In 1986 it was proven that Berblinger's glider was capable of sustained flight, but it was almost impossible to cross the Danube even with most modern gliders.
* [[Bertolt Brecht]] wrote a ballad about Berblinger in 1934.

==See also==
* ''[[The Tailor from Ulm]]'', a 1979 German film relating the story of Berblinger's early flights.

== External links ==
* [http://www.itravelnet.com/destinations/europe/germany/ulm/adlerbastei.html Adlerbastei (Eagles Bastion) - Where Berblinger launched from. ]

== Notes ==
{{reflist}}
{{Commons category|Albrecht Ludwig Berblinger}}
*{{de icon}} [http://www.berblinger.ulm.de/ berblinger.ulm.de]

{{Authority control}}

{{DEFAULTSORT:Berblinger, Albrecht}}
[[Category:1770 births]]
[[Category:1829 deaths]]
[[Category:Aviation pioneers]]
[[Category:Aviation inventors]]
[[Category:German aviators]]
[[Category:People from Ulm]]
[[Category:Tailors]]
[[Category:German inventors]]