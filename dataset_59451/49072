{{Infobox company
|logo = Kiskalogo updated.jpg
| industry        = [[Design firm]]
| founded         = {{Start date|1990}}
| location_city   = [[Anif|Anif, Salzburg]]
| location_country = Austria 
| area_served      = Worldwide
| founder      = Gerald Kiska
| num_employees   = 200 (2016)
| type            = [[Privately held company|Private]]
| website         = {{URL|www.kiska.com}}
}}

'''KISKA GmbH''' is an award-winning [[design]] agency located in [[Anif]]-[[Salzburg]], [[Austria]].<ref>[http://www.wired.co.uk/magazine/archive/2014/08/gear/into-the-wood Wired: "Wood is good, from hi-fis to high seas"]</ref> KISKA Industrial Design was founded in 1990 as a one-man business by industrial designer Gerald Kiska.<ref>[http://www.motorcycle.com/features/the-other-k-in-ktm.html Motorcycle.com: "The other K in KTM"]</ref> In 1997, KISKA Creative Industries was formed and moved to its first office in [[Salzburg]], [[Austria]]. In 2001, KISKA Design became a [[Privately held company|Private]] Limited Company.<ref>[(http://www.ktmgroup.com/en/company/ KTM Group: "Company Section"]</ref> There are four partners in the business: Sébastien Stassin (Chief Creative Officer),
Paul Friedl (Partner and Head of Communications), Klaus Tritschler (Partner and Head of Product Design), and Julian Herget (Partner and Head of Brand and Research).

== Expansion ==
Established in 1990 as a one-man business by industrial designer Gerald Kiska, the agency has expanded to 200 people from more than 30 different countries (as of 2016). KISKA operates from a 5.900 square metre building that includes a 1.000 square metre [[transportation]] design studio.<ref>http://www.kiska.com/en/we-are/about-kiska/</ref> It moved into this building in 2009.<ref>[http://derestricted.com/design/kiska-new-office Deresticted: "New office for KISKA"]</ref> KISKA GmbH has subsidiary, KISKA Inc. that is located in [[Temecula]], [[California]] and led by  Steve Masterson.<ref>[http://www.motorcycle-usa.com/2015/02/article/kiska-opens-first-intl-studio-in-north-america/ Motorcycle USA: "KISKA opens first international studio in North America"]</ref> KISKA also has a satellite office in [[Bamberg]], [[Germany]]. In 2016, KISKA GmbH expanded to China as KISKA Brand Design [[Shanghai]].

Ostensibly a [[design]] agency, KISKA GmbH also offers services in [[brand]] consulting and communications.<ref>[http://www.motorcycle.com/features/the-other-k-in-ktm.html Motorcycle: "The other K in KTM"]</ref> The agency expanded its services over time with the aim to create a consistent brand for its clients. This approach to design is referred to by KISKA as Integrated Design Development (I.D.D.).<ref>[http://www.develop3d.com/profiles/the-fast-and-the-furious Develop3D: "The fast and the furious"]</ref>

== Work for KTM-Sportmotorcycle ==
KISKA GmbH is best known for its 23 years of work with Austrian motorcycle manufacturer [[KTM]]-Sportmotorcycle AG.<ref>[http://www.pistonheads.com/features/ph-features/ktms-gerald-kiska-ph2-meets/30655 PistonHeads: "KTM's Gerald Kiska"]</ref> KTM-Sportmotorcycle AG has a 26% stake in KISKA.<ref>[http://auto.ndtv.com/news/ktm-to-buy-back-public-shares-off-the-stock-market-1335086 Car and Bike: "KTM to Buy Back Public Shares Off the Stock Market"]</ref> In this partnership, KISKA has designed all [[motorcycles]] for KTM-Sportmotorcycle since 1992<ref>[http://lanesplitter.jalopnik.com/how-one-design-studio-took-ktm-from-bankrupt-dirtbike-c-1758099293 Lanesplitter: "How One Design Studio Took KTM From Bankrupt Dirtbike Company To Giant-Slayer"]</ref> and four versions of the KTM [[KTM X-Bow|X-BOW]]. KISKA is also responsible for the overall branding of [[KTM]]-Sportmotorcycle AG, including the design of printed material, websites, exhibits and stores.

== Clients ==
KISKA also works for other national and international brands including [[Adidas]], [[AKG Acoustics|AKG]], [[Audi]], [[Atomic Skis|Atomic]], [[Bajaj Auto|Bajaj]], CFMOTO, [[Hilti]], [[Honda]], [[Husqvarna Motorcycles]], [[Kettler]], [[OSRAM]] SIteco, [[Otto Bock|Ottobock]] and [[Carl Zeiss AG|Zeiss]] Sports Optics.<ref>http://www.kiska.com/en/our-clients/</ref>

== Awards ==

*''Autovision Ottocar 2005, 2007, 2011, 2013''<ref>[http://www.auto-vision.org/en/winners.php Autovision Winners]</ref>
*''IF Product Design Award 1998, 1999, 2000, 2004, 2005, 2007, 2008, 2009, 2010, 2011, 2013, 2014, 2016''<ref>[http://ifworlddesignguide.com/ IF Product Design Award Winners]</ref>
*''ISPO Brandnew Award 2006''<ref>[http://www.streetstepper.com/content/history ISPO Brandnew Award]</ref>
*''German Design Award 2014, 2015, 2016''<ref>[http://gallery.designpreis.de/gdagallery/list/ German Design Award]</ref>
*''Red dot design award 1999, 2008, 2009, 2010, 2011, 2013, 2014, 2015''<ref>[http://red-dot.de/pd/online-exhibition/?lang=en]</ref>
*''IDEA Award 2007, 2015''<ref>[http://www.idsa.org/awards/idea/husqvarna-motorcycles-401-vitpilen-concept IDEA Award]</ref>
*''Trio des Jahres 2013''<ref>[http://www.kiska.com/uploads/media/Seiten_aus_trio-trend-okt.2012_2__2.pdf Trio des Jahres]</ref>
*''Automotive Brand Contest 2014, 2015, 2016''<ref>[http://www.german-design-council.de/nc/designpreise/german-design-award/der-wettbewerb.html Automotive Brand Contest]</ref>
*''EY Entrepreneur of the Year Award 2014''<ref>[http://www.ey.com/AT/de/Newsroom/News-releases/EY-2014-EOY-press-release EY Entrepreneur of the Year Award]</ref>
*''Clio Award 2016''<ref>[http://www.clios.com/awards/winner/13017]</ref>

== Bibliography ==

* {{cite book |last=Kiska |first=Gerald |date=2015 |title=25 Years Designing Desire |location=Anif/Salzburg |publisher=Self-Published |isbn=9783200039315}}
* {{cite book |last=Kiska |first=Gerald |date=2015 |title=25 Years Designing Desire – Work In Progress |location=Anif/Salzburg |publisher=Self-Published |isbn=9783200044050}}
* {{cite book |last=Rossier |first=Alexandre |date=2016 |title=KTM X- BOW GT4: DESIGNED TO RACE |location=Anif/Salzburg |publisher=Self-Published |isbn=9783200045514}}

== References ==
{{reflist|30em}}

[[Category:Design companies established in 1990]]
[[Category:Industrial design firms]]