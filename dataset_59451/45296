{{Use dmy dates|date=May 2012}}
{{Use British English|date=May 2012}}
{{Infobox person
| name        = Alan Cobham
| image       = Alan Cobham (Bain Collection).jpg
| alt         = 
| caption     = 
| birth_name  = Alan John Cobham
| birth_date  = {{Birth date|1894|05|06|df=y}}
| birth_place = 
| death_date  = {{Death date and age|1973|10|21|1894|05|06|df=y}}
| death_place = 
| nationality = English
| other_names = 
| occupation  = Aviator
| known_for   = }}
'''Sir Alan John Cobham''', [[Order of the British Empire|KBE]], [[Air Force Cross (United Kingdom)|AFC]] (6 May 1894&nbsp;– 21 October 1973) was an [[England|English]] aviation pioneer.

==Early life==
As a child he attended [[Wilson's School]],<ref>{{Citation | last1 = Allport | first1 = DH | last2 = Friskney | first2 = NJ | title = A Short History of Wilson's School | publisher = Wilson's School Charitable Trust | year = 1987}}.</ref>{{Page needed |date=February 2014}} then in [[Camberwell]], [[London]]. The school relocated to the former site of Croydon Airport in 1975.

==Career==
A member of the [[Royal Flying Corps]] in [[World War I]], Alan Cobham became famous as a pioneer of long distance aviation.  After the war he became a test pilot for the [[de Havilland]] aircraft company, and was the first pilot for the newly formed de Havilland Aeroplane Hire Service.  In 1921 he  made a 5,000 mile air tour of [[Europe]], visiting 17 cities in 3 weeks. Between 16 November 1925 and 13 March 1926, he made a trip from London to Cape Town and return in his [[de Havilland DH.50]] in which he had replaced the original [[Siddeley Puma]] engine with a more powerful, air-cooled Jaguar.<ref>{{cite book|title=My flight to the Cape and back | year = 1926 |url=http://opac.navalmarinearchive.com/cgi-bin/koha/opac-detail.pl?biblionumber=32649| place = [[London]] | publisher = A. & C. Black | last = Cobham | first = Alan J. |accessdate=3 May 2016}}</ref> On 30 June 1926, he set off on a flight from Britain (from the River Medway) to [[Australia]] where 60,000 people swarmed across the grassy fields of [[Essendon Airport]], [[Melbourne]] when he landed his [[de Havilland DH.50]] [[floatplane]] (it had been converted to a wheeled undercarriage earlier, at [[Darwin, Northern Territory|Darwin]]<ref>{{Citation | title = Wonders of World Aviation | volume = 10 | year = 1938}}.{{Page needed |date=February 2014}}</ref>). During the flight to Australia, Sir Alan J. Cobham's engineer of the D.H.50 aircraft, Mr. Arthur B. Elliot, was shot and killed after they left Bagdad on 5 July 1926. The return flight was undertaken over the same route. He was knighted the same year.

[[File:Alan Cobham Ship-to-Shore Air Mail 1926.jpg|thumb|left|Cover Sir Alan Cobham had attempted to fly to New York from the RMS ''Homeric''.]]
On 25 November 1926, Cobham attempted but failed to be the first person to deliver mail to New York City by air from the east, planning to fly mail from the [[White Star Line|White Star]] ocean liner [[RMS Homeric (1922)|RMS ''Homeric'']] in a [[de Havilland DH.60 Moth]] floatplane when the ship was about 12 hours from New York harbour on a westbound crossing from Southampton. After the Moth was lowered from the ship, however, Cobham was unable to take off owing to rough water and had to be towed into port by the ship. The same year Cobham was awarded the Gold Medal by the [[Fédération Aéronautique Internationale]].<ref>{{cite book|title=Aeronautics and astronautics; an American chronology of science and technology in the exploration of space, 1915–1960 | year = 1961 |page=175|url=https://archive.org/stream/aeronauticsastro61unit#page/174/mode/2up| place = [[United States of America|US]] | publisher = National Aeronautics and Space Administration | last = Emme | first = Eugene Morlock |accessdate=3 June 2011}}</ref>

In 1927 Cobham starred as himself in the 1927 British war film ''[[The Flight Commander (film)|The Flight Commander]]'' directed by [[Maurice Elvey]]. In 1928 he flew a [[Short Singapore]] [[flying boat]] around the continent of [[Africa]] landing only in British territory. Cobham wrote his own contemporary accounts of his flights, and recalls them in his biography.<ref>{{Citation | first = Alan | last = Cobham | title = A Time to Fly | year = 1978}}.{{Page needed |date=February 2014}}</ref>  The films 'With Cobham to the Cape' (1926), 'Round Africa with Cobham' (1928) and 'With Cobham to Kivu' (1932) contain valuable footage of the flights.<ref>{{Citation | first = GH | last = Pirie | title = Cinema and British imperial civil aviation, 1919–1939 | journal = Historical Journal of Film, Radio and Television | volume = 23 | year = 2003 | pages = 117–31 | doi=10.1080/0143968032000091068}}.</ref> Recent commentaries contextualize his flights across the British Empire in the wider events and culture of the time.<ref>{{Citation | first = Colin | last = Cruddas | title = Highways to the Empire | publisher = Air Britain | year = 2006 | ISBN = 0-85130-376-5}}.</ref>{{Page needed |date=February 2014}}<ref>{{Citation | first = GH | last = Pirie | title = Air Empire: British Imperial Civil Aviation, 1919–39 | publisher = Manchester University Press | year = 2009 | chapter = 6 | ISBN = 978-0-7190-4111-2}}.</ref>{{Page needed |date=February 2014}}<ref>{{Citation | first = GH | last = Pirie | title = Cultures and Caricatures of British imperial Aviation: Passengers, Pilots, Publicity | publisher = Manchester University Press | year = 2012 | chapter = 4}}.</ref>{{Page needed |date=February 2014}}

In 1932 he started the National Aviation Day displays – a combination of [[barnstorming]] and joyriding. This consisted of a team of up to fourteen aircraft, ranging from single-seaters to modern airliners, and many skilled pilots. It toured the country, calling at hundreds of sites, some of them regular airfields and some just fields cleared for the occasion.  Generally known as "Cobham's [[Flying circus|Flying Circus]]", it was hugely popular, giving thousands of people their first experience of flying, and bringing "air-mindedness" to the population. These continued until the end of the 1935 season.<ref>{{Citation | first = Arthur | last = Ord-Hume | title = Aeroplane Monthly |date=August 1973}}.</ref>{{Page needed |date=February 2014}}<ref>{{Citation | author = Colin Cruddas | title = Those Fabulous Flying Years | publisher = Air Britain | ISBN = 0-85130-334-X}}.</ref>{{Page needed |date=February 2014}}  In the British winter of 1932–33, Cobham took his aerial circus to South Africa (with the mistaken view that it would be the first of its kind there).<ref>{{Citation | first = GH | last = Pirie | chapter = South African air shows 1932/33: 'airmindedness', ambition and anxiety | title = Kronos: Southern African Histories | year = 2009 | pages = 48–70}}.</ref>

Cobham was also one of the founding directors of [[Airspeed Ltd.|Airspeed Limited]], the aircraft manufacturing company started by Nevil Shute Norway (perhaps better known as the famous novelist, [[Nevil Shute]]), together with the designer [[Hessell Tiltman]]; who, having been discharged by the Airship Guarantee Company (a subsidiary of Vickers) after the [[R101]] disaster also caused the grounding of the more successful [[R100]], decided to found their own small aircraft business. Cobham was an early and enthusiastic recruit: indeed, it was thanks to Sir Alan – who placed early orders for two "Off Plan" aircraft (the three-engined ten seater [[Airspeed Ferry]]) for his National Aviation Day Limited company – that Airspeed managed to commence manufacturing at all.

Cobham's early experiments with in-flight refuelling were based on a specially adapted [[Airspeed Courier]]. This craft was eventually modified by Airspeed to Cobham's specification, for a non-stop flight from London to India, using in-flight refuelling to extend the aeroplane's flight duration.

In 1935 he founded a small airline, Cobham Air Routes Ltd, that flew from London [[Croydon Airport]] to the [[Channel Islands]]. Months later, after a crash that killed one of his pilots, he sold it to [[Olley Air Service Ltd]] and turned to the development of [[inflight refueling]]. Trials stopped at the outbreak of [[World War II]] until interest was successfully revived by the RAF and [[United States Army Air Forces]] in the last year of the war.

He once remarked: "It's a full time job being Alan Cobham!" He retired to the [[British Virgin Islands]], but returned to England where he died in 1973.

The company he formed is still active in the aviation industry as [[Cobham plc]].

==See also==
* [[Aerofilms]], the UK's first commercial aerial photography company.
* [[Round the Bend (1951 novel)|Round the Bend]], the novel by [[Nevil Shute]], features Cobham's National Aviation Day [[flying circus]] as an integral part of the plot.
* Philip Stenning, hero of Shute's [[Marazan]], is a character modelled upon Cobham and others.

==References==
{{reflist}}
 13. ^ In 1926 when Arthur Elliot, Cobham's engineer for the journey to Melbourne, Australia and return to London, was shot by an Arab over the desert of Iraq and died the next day in Basra, Cobham wanted to cancel the flight. However, Flight Engineer Sergeant Arthur Ward of the Royal Airforce, stationed in Basra, was asked to take Elliott's place and he agreed. The episode and full details of the flight and overcoming the problems which beset the airmen are detailed in a new book by Arthur Ward's nephew, Kenneth T Ward entitled "Long Distance Flyer, G-EBFO.  ( ISBN 978-1-78222-456-3 ) available from all good book sellers. This book details the areas where the airmen landed for re-fuelling, the stories of the problems encountered, flying blind with only a compass, the Hughes Aperiodic Compass, to guide their way and the uninhabited island where they were marooned during the worst of the monsoon. On one occasion, Ward had to climb out onto the wing when G-EBFO was in the air, to tread on a helpers hand to make him release and drop into the water.

==External links==
* {{Citation | first = Alan | last = Cobham | author-link = Alan Cobham | type = recording | contribution = To Australia and Back in Six Minutes | publisher = [[National Film and Sound Archive]] | place = [[Australia|AU]] | contribution-url = http://www.nfsa.gov.au/sounds-australia-supplement/our-heroes-air/ | title = Our Heroes of the Air}}.
* {{Citation | title = Alan Cobham | url = http://www.npg.org.uk/live/search/person.asp?LinkID=mp00938 | type = portrait | first = Frank O | last = Salisbury | author-link = Frank O. Salisbury | year = 1926 | place = London, [[England|ENG]], [[United Kingdom|UK]] | publisher = The [[National Portrait Gallery (London)|National Portrait Gallery]]}}.
* [http://www.cobham.com/80th-anniversary/cobham-the-man.aspx Biography] at Cobham Plc

{{Authority control}}

{{Cobham plc}}

{{DEFAULTSORT:Cobham, Alan}}
[[Category:1894 births]]
[[Category:People educated at Wilson's School, Wallington]]
[[Category:Royal Flying Corps officers]]
[[Category:1973 deaths]]
[[Category:English aviators]]
[[Category:Knights Commander of the Order of the British Empire]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:English test pilots]]
[[Category:Britannia Trophy winners]]