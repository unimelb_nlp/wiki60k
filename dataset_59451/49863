{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Odd Women
| title_orig    = 
| translator    = 
| image         = File:The Odd Women.jpg
| caption       = Title page of the first edition.
| author        = George Gissing
| cover_artist  = 
| country       = England
| language      = English
| genre         = 
| publisher     = Lawrence and Bullen 
| release_date  = 1893
| media_type    = 
| pages         = 
| isbn = 0-14-043379-1
| isbn_note = (Penguin Classics paperback edition, 1993)
}}

'''''The Odd Women''''' is an 1893 novel by the English novelist [[George Gissing]]. Its themes are the role of women in society, marriage, morals and the early feminist movement.<ref name="Intro">{{cite book |title=Introduction |work=The Odd Women |first=Elaine |last=Showalter |publisher=Penguin Classics |date=1993}}</ref>

==Title==
The novel's title is derived ostensibly from the notion that there was an excess of one million women over men in Victorian England. This meant there were "odd" women left over at the end of the equation when the other men and women had paired off in marriage.<ref name="Intro"/> A cross-section of women dealing with this problem are described in the book and it can be inferred that their lifestyles also set them apart as odd in the sense of strange.

==Plot summary==
The novel begins with the Madden sisters and their childhood friend in [[Clevedon]]. After various travails, the adult Alice and Virginia Madden move to [[London]] and renew their friendship with Rhoda, an unmarried [[bluestocking]].
She is living with the also unmarried Mary Barfoot, and together they run an establishment teaching secretarial skills to young middle-class women remaindered in the marriage equation.

Monica Madden, the youngest and prettiest sister, is living-in above a shop in London. She is, in modern parlance, "stalked" by a middle-aged bachelor Edmund Widdowson, and he eventually brow-beats her into marriage. His ardent love turns into jealous obsession suffocating Monica's life.

Meanwhile Mary Barfoot's rakish cousin Everard decides to court Rhoda initially as a challenge to her avowed dislike of love and marriage, but he later falls in love with her for her intellectual independence, which he finds preferable to the average uneducated woman's inanity. Despite being virulently anti-marriage, she decides to indulge him with a view to turning down any marriage proposal to show her solidarity with her "odd women". Ironically, she in turn falls for him.

Married Monica meets Bevis, a young, middle-class man who pursues her and represents for her the romantic ideal from popular novels. Crucially, Bevis lives in the same building as Everard Barfoot. Monica, determined to elope with Bevis, goes there. Unbeknownst to her, her husband has hired a detective to follow her. She hears someone follow her up the stairs and, to appear innocent, she knocks on Barfoot's door. This is reported back to Widdowson, and he feels his suspicion has been justified and informs Mary Barfoot of her cousin's blackguardly ways.

Rhoda, on the other hand, is on a holiday in [[Northumberland]], and Everard goes to see her there. He woos her and at first suggests they enter a free-union (i.e. live together out of wedlock), which would appear to be consistent with her principles. However, she gives him a conventional "womanly" response and agrees to be with him only in a legal union; Barfoot, somewhat disappointed in her surprising conventionality, proposes marriage, which she accepts. She then receives a letter from Mary telling of Everard's supposed affair with Monica. Rhoda then breaks off the engagement, after Everard proudly refuses to give an explanation but insists he is innocent. After Widdowson confronts Monica over her infidelity, she leaves him but lives at his expense and even moves, together with her sisters, to his rented house in Clevedon. Virginia has become an alcoholic (her way of dealing with being an 'odd woman'). Monica is pregnant by her husband, but her pride will not let her reunite with him. To salve her conscience, she visits Rhoda and shows her a love letter from Beavis and also exonerates Everard over the alleged affair. Then, months after they last saw each other, Everard visits Rhoda, asks her if she still believes him to be guilty, and repeats his offer of marriage. Even though Rhoda assures him that she believes him innocent, she refuses his proposal, intimating that in his professions of love he was "not quite serious," but was partially testing her principles. It is too late for them to reunite. Barfoot soon gets married to a conventionally educated young woman. Monica gives birth to a girl, then dies soon after. The novel ends with Rhoda holding the baby, crying and murmuring, "Poor little child!"

==Criticism==
[[George Orwell]] admired the book and thought it illustrated well one of the major themes of all Gissing's works—the "self-torture that goes by the name of respectability". "In ''The Odd Women'' there is not a single major character whose life is not ruined by having too little money, or by getting it too late in life, or by the pressure of social conventions which are obviously absurd but which cannot be questioned … in each case the ultimate reason for disaster lies in obeying the accepted social code, or in not having enough money to circumvent it." <ref>{{citation |title=George Gissing |work=Collected Works: It Is What I Think |pages=347–348 }}</ref>

==References==
{{Reflist}}

==Further reading==
* Chase, Karen (1984). "The Literal Heroine: A Study of Gissing's 'The Odd Women'," ''Criticism,'' Vol. XXVI, No. 3, pp.&nbsp;231–244.
* Comitini, Patricia (1995). "A Feminist Fantasy: Conflicting Ideologies in 'The Odd Women'," ''Studies in the Novel,'' Vol. XXVII, No. 4, pp.&nbsp;529–543.
* [[Austin Harrison|Harrison, Austin]] (1925). "Signposts in Fiction," ''The Contemporary Review,'' Vol. CXXVIII, pp.&nbsp;82–89.
* Lesser, Wendy (1984). "Even-Handed Oddness: George Gissing's 'The Odd Women'," ''The Hudson Review,'' Vol. XXXVII, No. 2, pp.&nbsp;209–220.
* March, Constance (2001). "Women with Ideas: Gissing's ''The Odd Women'' and the New Woman Novel." In: ''A Garland for Gissing.'' Amsterdam: Rodopi, pp.&nbsp;81–90.
* [[Annie Nathan Meyer|Meyer, Annie Nathan]] (1896). [http://babel.hathitrust.org/cgi/pt?id=mdp.39015030005477;view=1up;seq=56 "Neglected Books: Mr. Gissing's 'The Odd Women',"] ''The Bookman,'' Vol. III, pp.&nbsp;48–50.
* [[Christopher Morley|Morley, Christopher]] (1927). [http://www.unz.org/Pub/SaturdayRev-1927may14-00821a02 "A Note on George Gissing,"] ''The Saturday Review'', Vol. III, pp.&nbsp;821.
* Schmidt, Gerald (2005). "George Gissing's Psychology of 'Female Imbecility'," ''Studies in the Novel,'' Vol. XXXVII, No. 3, pp.&nbsp;329–341.

==External links==
* [https://archive.org/stream/oddwomen01giss#page/n7/mode/2up ''The Odd Women,''] [https://archive.org/stream/oddwomen02giss#page/n7/mode/2up Vol. II], [https://archive.org/stream/oddwomen03giss#page/n7/mode/2up Vol. III], at [[Internet Archive]]
* {{gutenberg|no=4313|name=The Odd Women}}
* {{librivox book | title=The Odd Women | author=George Gissing}}
* [http://catalog.hathitrust.org/Record/008973495 ''The Odd Women,''] at [[Hathi Trust]]

{{George Gissing}}

{{DEFAULTSORT:Odd Women}}
[[Category:1893 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by George Gissing]]