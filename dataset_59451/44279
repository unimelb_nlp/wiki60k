{{Refimprove|date=October 2012}}
:''Not to be confused with [[RNAS Killingholme]]''
{{Infobox airport 
| name = RAF North Killingholme
| nativename =  [[File:Ensign of the Royal Air Force.svg|90px]]
| nativename-a = 
| nativename-r = 
| image = Old Hangar on North Killingholme Airfield - geograph.org.uk - 1408076.jpg
| image-width = 240
| caption = Old hangar    
| IATA = 
| ICAO = 
| type = Military   
| owner = [[Air Ministry]]   
| operator = [[Royal Air Force]]
| city-served = 
| location = [[North Killingholme]], [[Lincolnshire]]
| built = {{Start date|1943}}
| used = 1943-{{End date|1945}}
| elevation-f = {{Convert|8|m|disp=output number only|0}}
| elevation-m = 8
| coordinates = {{coord|53|38|09|N|000|17|31|W|region:GB_type:airport|display=inline,title}}
| pushpin_map = Lincolnshire
| pushpin_label = RAF North Killingholme
| pushpin_map_caption = Location in Lincolnshire
| website = 
| r1-number = 00/00
| r1-length-f = 0
| r1-length-m = 0
| r1-surface = [[Asphalt]]   
| r2-number = 00/00
| r2-length-f = 0
| r2-length-m = 0
| r2-surface = Asphalt 
| r3-number = 00/00
| r3-length-f = 0
| r3-length-m = 0
| r3-surface = Asphalt 
| stat-year = 
| stat1-header = 
| stat1-data = 
| stat2-header = 
| stat2-data = 
}}
'''Royal Air Force North Killingholme''' or more simply '''RAF North Killingholme''' is a former [[Royal Air Force station]] located immediately west of the village of [[North Killingholme]] in [[North Lincolnshire]].

The airfield was extensively used during the [[World War II|Second World War]] by [[Avro Lancaster]] bombers.

==History==
{{see also|No. 550 Squadron RAF}}
[[File:North Killingholme Airfield - Old Hangar - geograph.org.uk - 162689.jpg|thumb|140px|left|Old hangars]]

The RAF station opened in November 1943.{{sfn|Halpenny|1981|p=146}}  and became fully operational in January 1944 when [[No. 550 Squadron RAF|550 Squadron]] moved there from [[RAF Waltham]].{{sfn|Halpenny|1981|p=147}}

The station was with [[No. 1 Group RAF]].  No. 14 Base HQ was based at the airfield between 1944 and 1945.{{citation needed|date=August 2015}}

It remained operational until October 1945. 550 squadron was the only squadron to be based at North Killingholme and flew only [[Avro Lancaster]]s.{{sfn|Halpenny|1981|p=147}}

After North Killingholme closed, the land reverted to agriculture use, but the layout of the station is very easy to see from aerial photographs.{{sfn|Halpenny|1981|p=147}}

There is the North Killingholme Industrial Estate on the site along with a large depot for Volvo construction equipment.

==See also==
* [[List of former Royal Air Force stations]]
* [[RAF Kirmington]], a nearby airfield, later became [[Humberside Airport]].

==References==

===Citations===
{{Reflist}}

===Bibliography===
{{refbegin}}
*{{wikicite|ref={{harvid|Halpenny|1981}}|reference=[[Bruce Barrymore Halpenny|Halpenny, B. B]]. ''Action Stations: Wartime Military Airfields of Lincolnshire and the East Midlands v. 2''. Cambridge, Cambridgeshire, Patrick Stephens Ltd, 1981. ISBN 0-85059-484-7.}}
{{refend}}

==External links==
{{Commons category|RAF North Killingholme}}
* [http://550squadron.wordpress.com/ RAF 550 Squadron and North Killingholme Association]
* [http://raf-lincolnshire.info/nkillingholme/nkillingholme.htm RAF Lincolnshire]
* [http://wikimapia.org/2249993/Former-RAF-North-Killingholme Former RAF North Killingholme at wikimapia.org]

{{Royal Air Force}}
{{RAF stations in Lincolnshire}}

{{DEFAULTSORT:North Killingholme}}
[[Category:Royal Air Force stations in Lincolnshire]]
[[Category:History of Lincolnshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]
[[Category:Military units and formations established in 1943]]
[[Category:Military units and formations disestablished in 1945]]