{{Refimprove|date=March 2017}}
{{Infobox school
| name=Australian Science and Mathematics School
| image=ASMSlogo.png
| established=2002
| type=[[Public school (government funded)|Public]]
| principal=Susan Hyde
| enrolment= Approx. 400
| faculty =40
| free_label=
| free_text=
| location=Flinders University,<br>Bedford Park,<br>South Australia 5042
| website=http://www.asms.sa.edu.au
}}
The '''Australian Science and Mathematics School''' (or '''ASMS''') is a [[coeducational]] public senior [[high school]] for Years 10 - 12 located in [[Adelaide]], [[South Australia]] on the campus of [[Flinders University]].<ref>https://www.asms.sa.edu.au/</ref> As the school is unzoned, it attracts students from all across the Adelaide metropolitan area as well as some regional locations, in addition to international students. The goal of the school is to prepare its students for [[university]], particularly in the fields of [[mathematics]] and [[science]]. The ASMS is unconventional in its approach to education, emphasising a love of learning in both students and teaching staff; students are given the freedom to take control of their own education. ASMS aims to make students aware of their own learning and for them to become self-directed in the way they complete academic tasks.

==Overview==
[[Image:Australian-science-and-mathematics-school.jpg|thumb|left|The Australian Science and Mathematics School]]
The Australian Science and Mathematics School was opened in 2003 and is only one of two in [[Australia]]. As the school is designed to provide an adult environment for senior school students, there is no [[school uniform]] policy, which promotes a variety of culture and social styles and structures. A key feature of the ASMS is the productive relationship between the school and the Flinders University, on which the campus is located; the ASMS shares many resources with the university, including a library, cafeteria, student services, transport, recreational areas and car parks, in addition to booked access to lecture theatres and specialist science and support facilities. Furthermore, students at the ASMS in collaboration with Flinders University's [[Science and Technology Enterprise Partnership]] (STEP) may be involved in research projects in the business, industry and university sectors.

==Curriculum==
The ASMS is a specialist science and mathematics school, however it offers a comprehensive curriculum which covers all learning areas necessary for students to achieve their [[South Australian Certificate of Education]] (SACE) qualification. If the ASMS does not offer a desired subject, the student will be transported by government-paid [[Taxicab|taxi]] to a neighbouring (or "Alliance") school for their lessons in that subject.
The Year 10/11 curriculum is organised into interdisciplinary Central Studies; these alternate every year to ensure that a student will not do the same subjects twice. The Year 12 curriculum consists of standard SACE Stage 2 subjects, such as the various Mathematics, Science, English and Humanities subjects.

==Adventure Space==
The ASMS also provides special activities for Year 10 and 11 students in the form of Adventure Space. Examples of these include Dance, Cryptography, Robotics, Aviation, Australian Space Design Competition, Paramedical Pathways, Electronics, Creative Writing, and Palaeontology. While not assessed, they do provide an opportunity to interact with university life, as well as an opportunity to take part in learning focused productive extra-curricular activities.

==External links==
*[http://www.asms.sa.edu.au ASMS website]
*[http://www.flinders.edu.au Flinders University website]

{{coord|35|1|7.99|S|138|34|30.75|E|display=title}}

{{reflist}}

{{DEFAULTSORT:Australian Science And Mathematics School}}
[[Category:High schools in South Australia]]
[[Category:Public schools in South Australia]]
[[Category:Educational institutions established in 2003]]
[[Category:Gifted education]]
[[Category:Special interest high schools in South Australia]]
[[Category:Mathematics education]]