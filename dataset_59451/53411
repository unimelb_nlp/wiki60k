{{Infobox Officeholder
| name         = Lorenzo Burrows
| image name   = Lorenzo Burroughs.png
| order        = 18th
| office       = New York State Comptroller
| term_start   = January 1, 1856 
| term_end     = December 31, 1857
| preceded     = [[James M. Cook]]
| succeeded    = [[Sanford E. Church]]
| office1      = Member of the [[United States House of Representatives|U.S. House of Representatives]] from [[New York's 34th congressional district]]
| term_start1  = March 4, 1849
| term_end1    = March 3, 1853
| preceded1    = [[Washington Hunt]]
| succeeded1   = [[Walter L. Sessions]]
| birth_date   = March 15, 1805
| birth_place  = [[Groton, Connecticut|Groton]], [[Connecticut]]
| death_date   = March 6, 1885 (age 79)
| death_place  = [[Albion (town), Orleans County, New York|Albion]], [[New York (state)|New York]]
| spouse       = 
| profession   = Clerk, politician
| religion     = 
| party        = [[Know Nothing]]<br>[[Whig Party (United States)|Whig]]
|}}

'''Lorenzo Burrows''' (March 15, 1805 in [[Groton, Connecticut|Groton]], [[New London County, Connecticut]] – March 6, 1885 in [[Albion (town), Orleans County, New York|Albion]], [[Orleans County, New York]]) was an American merchant, banker and politician.

==Life==
He attended the academies at [[Plainfield, Connecticut]] and [[Westerly, Rhode Island]]. He moved to New York and settled in Albion, N.Y., in 1824. He was employed as a clerk until 1826, when he engaged in mercantile pursuits. He assisted in establishing the Bank of Albion in 1839 and served as cashier. He was [[Treasurer]] of Orleans County in 1840 and was [[Assignee]] in [[bankruptcy]] for Orleans County in 1841. He was [[Supervisor]] of the Town of [[Barre, New York|Barre]] in 1845, and was elected as a [[USWhig|Whig]] to the [[31st United States Congress|31st]] and [[32nd United States Congress]]es, serving from March 4, 1849 to March 4, 1853.

In August 1852, he declined to be appointed [[United States Postmaster General]] by President [[Millard Fillmore]]. Instead, Fillmore (a fellow New York Whig) chose Connecticut Whig [[Samuel Dickinson Hubbard]].

He was eighteenth [[New York State Comptroller]] from 1856 to 1857, elected on the [[Know Nothing|American Party]] ticket in [[New York state election, 1855|1855]]. He won 33.98% of the vote over the Republican, and [[Barnburners and Hunkers#Hunkers|the two Democrats]].<ref>http://www.ourcampaigns.com/RaceDetail.html?RaceID=574687</ref>

He ran unsuccessfully for [[Governor of New York]] on the American Party ticket in [[New York state election, 1858|1858]]. Unlike three years previously, where he won with slightly over a third of the vote, he only narrowly got over ten percent this election while both the reunited Democratic Party and the recently established Republican Party both won over forty percent.<ref>http://www.ourcampaigns.com/RaceDetail.html?RaceID=107100</ref>

He was director of the [[Niagara Falls]] International Bridge Co. He was chosen as a [[regent]] of [[New York University]] in 1858 and appointed one of the commissioners of [[Mount Albion Cemetery]] in 1862, serving in both of these capacities until his death in 1885. He was buried at Mount Albion.

His uncle [[Daniel Burrows]] was a United States Representative from [[Connecticut]]. His brother [[Latham A. Burrows]] was a [[New York State Senate|New York State Senator]]. Both served in Congress or in the state legislature in the 1820s.

==References==
{{Reflist}}

==Sources==
{{CongBio|B001143}}
*[http://politicalgraveyard.com/bio/burrows.html] Political Graveyard
*[https://query.nytimes.com/mem/archive-free/pdf?res=9403E7DA1031E234BC4F51DFBE668389649FDE] His declination to be Postmaster General, in NYT on August 27, 1852
*[https://books.google.com/books?id=E3sFAAAAQAAJ&pg=PA34 Google Book] ''The New York Civil List'' compiled by Franklin Benjamin Hough (page 34; Weed, Parsons and Co., 1858)

{{s-start}}
{{s-par|us-hs}}
{{USRepSuccessionBox
| state= New York
| district= 34
| before= [[Washington Hunt]]
| after= [[Walter L. Sessions]]
| years= 1849–1853}}
{{s-off}}
{{succession box | title = [[New York State Comptroller]] | before = [[James M. Cook]] | after = [[Sanford E. Church]] | years = 1856–1857}}
{{s-end}}

{{NYSComptroller}}

{{Authority control}}

{{DEFAULTSORT:Burrows, Lorenzo}}
[[Category:1805 births]]
[[Category:1885 deaths]]
[[Category:People from Groton, Connecticut]]
[[Category:People from Albion, Orleans County, New York]]
[[Category:New York State Comptrollers]]
[[Category:Members of the United States House of Representatives from New York]]
[[Category:American bankers]]
[[Category:New York Whigs]]
[[Category:New York Know Nothings]]
[[Category:New York gubernatorial candidates]]
[[Category:Regents of the University of the State of New York]]
[[Category:Whig Party members of the United States House of Representatives]]
[[Category:19th-century American politicians]]