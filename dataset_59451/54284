{{Use dmy dates|date=November 2016}}
{{Use Australian English|date=November 2016}}
{{Infobox officeholder
|honorific-prefix   = Sir
|name               =  Richard Davies Hanson
|honorific-suffix   = 
|image              = Richard Davies Hanson 2.jpeg
|imagesize          = 
|smallimage         = 
|caption            = 
|order              = 4th
|office             = Premier of South Australia
|term_start         = 30 September 1857
|term_end           = 8 May 1860
|monarch          = [[Queen Victoria|Victoria]]
|governor         = [[Richard Graves MacDonnell|Sir Richard MacDonnell]]
|predecessor        = [[Robert Torrens]]
|successor          = [[Thomas Reynolds (Australian politician)|Thomas Reynolds]]
|constituency       = 
|majority           =
|birth_date   = {{birth date|1805|12|6|df=y}}
|birth_place  = London, England, UK
|death_date   = {{death date and age|1876|3|4|1805|12|6|df=y}}
|death_place  = [[Mount Lofty]], South Australia
|restingplace = 
|restingplacecoordinates = 
|birthname    = Richard Davies Hanson
|nationality  = British
|spouse       = Ann Hopgood
}}
'''Sir Richard Davies Hanson''' (6 December 1805 – 4 March 1876), was the fourth [[Premier of South Australia]], from 30 September 1857 until 8 May 1860, and was a [[Chief Judge]] from 20 November 1861 until 4 March 1876 on the [[Supreme Court of South Australia]], which is the [[Australian court hierarchy|highest ranking court]] in the Australian [[States and territories of Australia|State]] of South Australia.

==Life==
Hanson was born in London, the second son of Benjamin Hanson, a fruit merchant and importer, and was educated at a private school in Melbourn, [[Cambridgeshire]]. Admitted a solicitor in 1828, he practised for briefly in London, becoming a disciple of [[Edward Gibbon Wakefield]] in connection with his colonization schemes. Hanson joined the ''Globe'' as a political critic early in 1837. In 1838 he went with [[John Lambton, 1st Earl of Durham|Lord Durham]] to [[Canada]] as assistant commissioner of inquiry into crown lands and immigration. Hanson worked with [[Dominick Daly]] in Canada.

In 1840, on the death of Lord Durham, Hanson settled in [[Wellington]], New Zealand. He there acted as crown prosecutor, but in 1846 moved to South Australia. On his arrival in the colony of South Australia in 1846, Hanson immediately set up a legal practice. He served as Advocate-General and [[Attorney-General of South Australia|Attorney-General]] for the colony before election to the seat of [[Electoral district of City of Adelaide|City of Adelaide]] in 1857.

In 1851 Hanson was appointed advocate-general of the colony, initially as a temporary replacement for the ailing [[William Smillie]],<ref>{{cite news |url=http://nla.gov.au/nla.news-article38449569 |title=The Government Gazette |newspaper=[[South Australian Register]] |volume=XV, |issue=1484 |location=South Australia |date=18 July 1851 |accessdate=13 November 2016 |page=3 |via=National Library of Australia}}</ref> made permanent when Smillie died. He took an active share in the passing of many important measures, such as the first Education Act, the District Councils Act of 1852, and the Act of 1856 which granted constitutional government to the colony.  In 1856 he was attorney-general in the first ministry under [[Boyle Travers Finniss]]; becoming premier himself in 1857. Among the acts passed were the first patents act, an insolvency act, a partial consolidation of the criminal law, and the Torrens real property act, though he was at first opposed to this measure. He also passed an act legalizing marriage with a deceased wife's sister, the first of its kind in the Empire, but the royal assent was refused on this occasion.

After leaving parliament, Hanson replaced [[Charles Cooper (judge)|Sir Charles Cooper]] as Chief Justice of the [[Supreme Court of South Australia]] in 1861. He was knighted in 1869 by [[Victoria of the United Kingdom|Queen Victoria]] when he visited England, and was acting [[Governor of South Australia]] for 1872–73. In his spare time Hanson gave much time to theological studies. His publications include ''Law in Nature and Other Papers'' (1865), ''The Jesus of History'' (1869), ''Letters to and from Rome'' (1869), ''The Apostle Paul'', and the ''Preaching of Christianity in the Primitive Church (1875)''. He died in Australia on 4 March 1876.

==Personal life==
Freemasonry was an integral part of Hanson's personal life. He was elected as a member and initiated into the Craft on 27 November 1834 in London when The Lodge of Friendship, a Lodge especially founded to become South Australia's first Lodge, held its very first meeting. Later he was to rise in position within the Lodge, which still exists to the present day, and ultimately served as its Master.

His summer residence, Woodhouse, near [[Piccadilly, South Australia]], is today owned by the [[Scouting and Guiding in South Australia|South Australian Scout Association]], and used for Scout leader training and private functions and accommodation; the extensive grounds are used for camping and outdoor adventuring.<ref>http://www.woodhouse.org.au/</ref>

Richard's brother [[William Hanson (engineer)|William Hanson]] (1810–1875) was an architect and engineer who played a decisive role in the early history of South Australia's railways and waterworks.

==Legacy==
In 1837 Hanson Street in [[Adelaide city centre|Adelaide]] was named after him. The street was later subsumed by the expanded [[Pulteney Street]] in 1967.

The [[Parliament of South Australia]] lower house seat of [[Electoral district of Hanson|Hanson]], created in 1970 and renamed to [[Electoral district of Ashford|Ashford]] in 2002, was named after him.

The South Australian town of [[Hanson, South Australia|Hanson]] was also named after him.<ref name=burrahistory>[http://www.burrahistory.info/BurraPlaceNames.htm ''Towns and other places in the District'', www.burrahistory.info] Retrieved on 8 November 2014</ref>

==See also==
* [[Judiciary of Australia]]

== Notes ==
{{reflist}}

==References==
*{{cite DNB|wstitle=Hanson, Richard Davies|first=George Clement|last=Boase|authorlink = George Clement Boase|volume=24}} 
*'[http://www.adb.online.anu.edu.au/biogs/A040384b.htm Hanson, Sir Richard Davies (1805 - 1876)]', ''[[Australian Dictionary of Biography]]'', Volume 4, [[Melbourne University Press|MUP]], 1972, pp 336–340. Retrieved 20 January 2009
*{{Dictionary of Australian Biography|First=Richard|Last=Hanson|shortlink=0-dict-biogHa-He.html#hanson2|accessdate=2009-01-20}}
*{{EB1911}}

==External links==
*[http://www.parliament.sa.gov.au/pp/html/hanson.shtm South Australian Parliament - Hanson]

{{s-start}}
{{s-off}}
{{s-new}}
{{s-ttl|title=[[Attorney-General of South Australia]]|years= 1856{{spaced ndash}}1857}}
{{s-aft|after= [[Edward Castres Gwynne|Edward Gwynne]]}}
|-
{{s-bef|before=[[Richard Bullock Andrews|Richard Andrews]]}}
{{s-ttl|title=[[Attorney-General of South Australia]]|years= 1857{{spaced ndash}}1860}}
{{s-aft|after= [[Henry Strangways]]}}
|-
{{s-bef|before=[[Robert Torrens]]}}
{{s-ttl|title=[[Premier of South Australia]]|years=1857–1860}}
{{s-aft|after= [[Thomas Reynolds (Australian politician)|Thomas Reynolds]]}}
|-
{{s-par|au-sa}}
{{s-new|district}}
{{s-ttl | title=Member for [[Electoral district of City of Adelaide|City of Adelaide]] | years=1857&ndash;1861 | alongside=[[Robert Torrens]], [[Judah Moss Solomon|Judah Solomon]], [[Francis Dutton]], [[B. T. Finniss|Boyle Finniss]], [[John Bentham Neales|John Neales]], [[W. H. Burford & Son#W. H. Burford|William Burford]], [[William Owen (Australian politician)|William Owen]], [[Matthew Moorhouse]], [[Philip Santo]], [[Samuel Bakewell]], [[William Parkin]]}}
{{s-aft | after=[[James Boucaut]]}}
|-
{{s-legal}}
{{succession box | title= [[Supreme Court of South Australia|Chief Justice of the Supreme Court of South Australia]] | before=[[Charles Cooper (judge)|Charles Cooper]] | after=[[Samuel Way]] | years=20 November 1861 – 4 March 1876}}
{{s-end}}

{{Premiers of South Australia}}

{{Authority control}}

{{DEFAULTSORT:Hanson, Richard}}
[[Category:1805 births]]
[[Category:1876 deaths]]
[[Category:Attorneys-General of South Australia]]
[[Category:Premiers of South Australia]]
[[Category:Chief Justices of South Australia]]
[[Category:Australian people of English descent]]
[[Category:Australian Knights Bachelor]]
[[Category:Chancellors of the University of Adelaide]]
[[Category:Colony of South Australia judges]]
[[Category:19th-century Australian politicians]]