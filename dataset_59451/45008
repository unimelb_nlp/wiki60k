{{Use dmy dates|date=March 2011}}
{{Infobox Christian leader
| type = archbishop
| honorific-prefix = The Most Reverend
| name = Leonard Faulkner
| honorific-suffix =
| title = Archbishop Emeritus of Adelaide
| image =
| imagesize =
| alt =
| caption =
| church =
| archdiocese = [ Catholic Archdiocese of Adelaide]
| enthroned = 19 June 1985
| ended = 3 December 2001
| predecessor = [[James William Gleeson]]
| opposed =
| successor = [[Philip Wilson (archbishop)|Philip Wilson]]
| ordination = 1 January 1950
| consecration = 28 November 1967
| cardinal =
| rank =
| other_post = Bishop of [ Catholic Diocese of Townsville] (1967-1983)
| birth_date = {{Birth date and age|df=yes|1926|12|05}}
| birth_place = [[Booleroo Centre, South Australia]]
| death_date =
| nationality = Australian}}

'''Leonard Anthony Faulkner''' (born 5 December 1926) is an [[Australian]] [[Roman Catholic]] [[clergyman]], and was the seventh<ref name="Archdiocese of Adelaide - History">{{cite web|title=Archdiocese of Adelaide - History|url=http://www.adelaide.catholic.org.au/about-the-archdiocese/history|publisher=Archdiocese of Adelaide|accessdate=30 May 2010}}</ref> [[Archbishop]] of [[Roman Catholic Archdiocese of Adelaide|Adelaide]].  Born in rural [[South Australia]], Faulkner served as an [[Adelaide]] parish priest and Bishop of [[Roman Catholic Diocese of Townsville|Townsville]] before being appointed Archbishop of Adelaide in 1985.  He is currently [[Bishop emeritus|Archbishop Emeritus]] of Adelaide.

==Early life==
Faulkner was born in [[Booleroo Centre, South Australia]] in 1926.<ref name="Archbishop to mark 40 years as `shepherd'">{{cite news|last=Schriever|first=Jordanna|title=Archbishop to mark 40 years as `shepherd'|accessdate=30 May 2010|newspaper=The Adelaide Advertiser|date=12 November 2007}}</ref> The son of a farm labourer and the eldest of ten siblings, Faulkner did not begin to attend school until he was seven years old, as until then he was considered too young to walk the four kilometres from his house to the local school.<ref name="A love of life">{{cite news|last=Harris|first=Samela|title=A love of life|accessdate=30 May 2010|newspaper=The Adelaide Advertiser|date=6 January 2000}}</ref> Faulkner was ordained on [[New Year's Day]], 1950 in Rome, along with twelve other priests from around the world.<ref name="Golden age for a man and his church">{{cite news|last=King|first=Melissa|title=Golden age for a man and his church|accessdate=30 May 2010|newspaper=The Adelaide Advertiser|date=20 January 2000}}</ref> His first posting was to the parish of Woodville, Seaton, Royal Park and Albert Park in [[Adelaide, South Australia]]. He  served as a [[chaplain]] within the [[Young Christian Workers]] movement until his consecration as Bishop of [[Roman Catholic Diocese of Townsville|Townsville]].<ref name="A love of life"/>

==Episcopacy==
On 28 November 1967, Faulkner was consecrated as the Bishop of [[Roman Catholic Diocese of Townsville|Townsville]] in [[Queensland]].<ref name="Archbishop to mark 40 years as `shepherd'"/> In 1983 he returned to Adelaide to assist the ailing Archbishop [[James William Gleeson|James Gleeson]], and in 1985 he was installed as Gleeson's successor.<ref name="Golden age for a man and his church"/> During his tenure as Archbishop, Faulkner declined to live in the bishop's quarters, instead choosing to reside in a plain house in the Adelaide suburb of [[Netley, South Australia|Netley]].

===Controversy regarding communal confession===
In 1999, Faulkner caused controversy when he defied Vatican pressure to cease the practice of communal [[Sacrament of Penance (Catholic Church)|confession]], wherein a priest may grant [[absolution]] without hearing individual confessions.<ref name="Papal police get confession under duress">{{cite news|last=Abraham|first=Matthew|title=Papal police get confession under duress|accessdate=30 May 2010|newspaper=The Australian|date=1 March 1999}}</ref> Following a meeting with Australian bishops in late 1998, [[Pope John Paul II]] sent a letter to all Australian bishops outlining concerns with the relaxed nature of Australian Catholicism. In particular, he formally requested that the bishops eliminate the use of communal confession.<ref name="Vatican talks tough on Easter confession">{{cite news|last=Abraham|first=Matthew|title=Vatican talks tough on Easter confession|accessdate=30 May 2010|newspaper=The Australian|date=20 March 1999}}</ref> While the dioceses of most other capital cities in the country abandoned the practice, Faulkner refused, allowing communal confession during [[Lent]] of 1999.<ref name="Papal police get confession under duress"/><ref>{{cite news|last=Fowler|first=Andrew|title=The Vatican's Verdict|url=http://www.abc.net.au/4corners/stories/s20286.htm|accessdate=30 May 2010|newspaper=Four Corners|date=8 March 2009|agency=The Australian Broadcasting Corporation}}</ref> In June 1999, Faulkner sent a pastoral message to all parishes in the Archdiocese of Adelaide allowing communal confession, but requiring prior approval from the Archbishop.<ref name="Parishes told third rite is all right">{{cite news|last=Abraham|first=Matthew|title=Parishes told third rite is all right|accessdate=30 May 2010|newspaper=The Australian|date=2 June 1999}}</ref> This made Adelaide one of the few places in Australia where communal confession was still practised.<ref name="Parishes told third rite is all right"/>

===Retirement===
In November 2000, Pope John Paul II appointed the Bishop of [[Roman Catholic Diocese of Wollongong|Wollongong]], [[Philip Wilson (archbishop)|Philip Wilson]] to the position of [[Coadjutor bishop|coadjutor Archbishop]] of Adelaide, in doing so naming him as Faulkner's successor.<ref name="Pope appoints Adelaide's new Archbishop">{{cite news|last=James|first=Colin|title=Pope appoints Adelaide's new Archbishop|accessdate=28 May 2010|newspaper=The Adelaide Advertiser|date=1 December 2000}}</ref> On 3 December 2001, two days before his seventy-fifth birthday, Faulkner retired as Archbishop, and Wilson was installed as his successor.<ref name="Man of people shares welcome mass with 7000">{{cite news|last=Harris|first=Samela|title=Man of people shares welcome mass with 7000|accessdate=30 May 2010|newspaper=The Adelaide Advertiser|date=4 December 2001}}</ref> As a retired Archbishop, Faulkner retains the title of [[Bishop emeritus|Archbishop Emeritus]]. An autobiographical book based on his edited memories, [http://morningstarpublishing.net.au/product/a-listening-ministry/ ''A Listening Ministry''], appeared in 2016.<ref>M. Costigan, Review of ''A Listening Ministry'', [http://australiancatholichistoricalsociety.com.au/pdfs/2017/achs%20journal%202016%2037-2%20for%20internet.pdf ''Journal of the Australian Catholic Historical Society'' 37 (2)], 2016, 261-265.</ref>

==References==
{{reflist}}

{{Start box}}
{{s-rel|ca}}
{{Succession box|title=[[Roman Catholic Archdiocese of Adelaide|Archbishop of Adelaide]]|before=[[James William Gleeson]]|after=[[Philip Wilson (archbishop)|Philip Wilson]]|years=1985 &mdash; 2001}}
{{End box}}

{{Bishops and Archbishops of the Roman Catholic Archdiocese of Adelaide}}


{{DEFAULTSORT:Faulkner, Leonard}}
[[Category:1926 births]]
[[Category:Living people]]
[[Category:21st-century Roman Catholic archbishops]]
[[Category:Roman Catholic Bishops of Adelaide]]
[[Category:People from South Australia]]