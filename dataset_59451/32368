{{featured article}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image  = [[File:S.M. Linienschiff Mecklenburg.jpg|300px]]
| Ship caption = Lithograph of ''Mecklenburg'' from 1902
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship name = ''Mecklenburg''
| Ship namesake = [[House of Mecklenburg]]
| Ship ordered = 
| Ship builder = [[AG Vulcan Stettin]]
| Ship laid down = 15 May 1900
| Ship launched = 9 November 1901
| Ship commissioned = 25 June 1903
| Ship decommissioned = 24 January 1916
| Ship struck = 25 January 1920
| Ship fate = Scrapped in 1921
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Wittelsbach|battleship|0}} [[pre-dreadnought battleship]]
| Ship displacement = {{convert|12798|MT|LT|abbr=on|0}}
| Ship length = {{convert|126.80|m|ft|0|abbr=on}}
| Ship beam = {{convert|22.80|m|ftin|abbr=on}}
| Ship draft = {{convert|7.95|m|ftin|abbr=on}}
| Ship power =*{{convert|14000|PS|ihp kW|lk=on|abbr=on|0}}
*12 [[water-tube boiler]]s
| Ship propulsion = 3 shafts, [[triple expansion engine|triple-expansion steam engines]]
| Ship speed = {{convert|18|kn|lk=in}}
| Ship range = {{convert|5000|nmi|lk=in}}; {{convert|10|kn}}
| Ship complement =* 30 officers
* 650 enlisted men

| Ship armament =* 4 × [[24 cm SK L/40|{{convert|24|cm|in|1|abbr=on}} guns]] (40 cal.)
* 18 × {{convert|15|cm|in|1|abbr=on}} guns
* 12 × {{convert|8.8|cm|in|1|abbr=on}} guns
* 6 × {{convert|45|cm|in||abbr=on}} [[torpedo tube]]s

| Ship armor =* [[Belt armor|Belt]]:  {{convert|100|to|225|mm|in|abbr=on}}
* [[Gun turret|Turrets]]: {{convert|250|mm|in|abbr=on}}
* Deck:  {{convert|50|mm|in|abbr=on}}

| Ship notes = 
}}
|}

'''SMS ''Mecklenburg''''' ("His Majesty's Ship ''[[Mecklenburg]]''"){{efn|name=SMS|}} was the fifth ship of the {{sclass-|Wittelsbach|battleship|4}} of [[pre-dreadnought]] [[battleship]]s of the [[Kaiserliche Marine|German Imperial Navy]]. Laid down in May 1900 at the [[AG Vulcan Stettin|AG Vulcan]] shipyard in [[Stettin]], she was finished in May 1903. Her [[sister ship]]s were {{SMS|Wittelsbach||2}}, {{SMS|Zähringen||2}}, {{SMS|Wettin||2}}, and {{SMS|Schwaben||2}}; they were the first capital ships built under the [[German Naval Laws|Navy Law of 1898]], championed by [[Admiral]] [[Alfred von Tirpitz]]. ''Mecklenburg'' was armed with a main battery of four {{convert|24|cm|sp=us|adj=on}} guns and had a top speed of {{convert|18|kn}}.

''Mecklenburg'' spent the early period of her career in the I Squadron of the German fleet, participating in the peacetime routine of training cruises and exercises. After [[World War I]] began in August 1914, the ship was mobilized with her sisters as the IV Battle Squadron. She saw limited duty in the [[Baltic Sea]] against Russian naval forces, and as a [[guard ship]] in the [[North Sea]]. The German High Command withdrew the ship from active service in January 1916 due to a threat from submarines and [[naval mine]]s, together with severe shortages in personnel. For the remainder of her career, ''Mecklenburg'' served as a prison ship and as a barracks ship based in [[Kiel]]. She was stricken from the navy list in January 1920 and sold for scrapping the following year.

== Description ==
[[File:Wittelsbach class linedrawing.png|thumb|left|Line-drawing of the ''Wittelsbach'' class]]
{{main|Wittelsbach-class battleship}}

''Mecklenburg'' was {{convert|126.80|m|ft|0|abbr=on}} [[length overall|long overall]]; she had a [[beam (nautical)|beam]] of {{convert|22.80|m|ftin|abbr=on}} and a [[Draft (hull)|draft]] of {{convert|7.95|m|ftin|abbr=on}} forward. At full load, she displaced up to {{convert|12798|MT|LT|abbr=on|0}}. The ship was powered by three 3-cylinder vertical [[triple-expansion engine]]s that drove three screws. Steam was provided by six coal-fired [[Thornycroft boiler]]s and six coal-fired cylindrical boilers. ''Mecklenburg''{{'}}s powerplant was rated at {{convert|14000|PS|ihp kW|lk=on|0}}, which gave her a top speed of {{convert|18|kn|lk=in}}. She had a crew of 30&nbsp;officers and 650&nbsp;enlisted men.{{sfn|Gröner|pp=16–17}}

The ship's primary armament consisted of a main battery of four [[24 cm SK L/40|24&nbsp;cm (9.4&nbsp;in) SK L/40 guns]] in twin [[gun turret]]s,{{efn|name=gun nomenclature}} one fore and one aft of the central [[superstructure]].{{sfn|Hore|p=67}} Her secondary armament consisted of eighteen [[15 cm SK L/40 naval gun|15&nbsp;cm (5.9&nbsp;inch) SK L/40 guns]] and twelve [[8.8 cm SK L/30 naval gun|8.8&nbsp;cm (3.45&nbsp;in) SK L/30 quick-firing guns]]. Her weaponry was rounded out with six {{convert|45|cm|in|abbr=on}} torpedo tubes, all submerged in the hull; one was in the bow, one in the stern, and the other four on the [[broadside]]. Her [[armored belt]] was {{convert|225|mm|sp=us}} thick in the central portion that protected her [[Magazine (artillery)|magazines]] and machinery spaces and reduced to {{convert|100|mm|abbr=on}} on either end of the hull. The [[deck (ship)|deck]] was {{convert|50|mm|abbr=on}} thick. ''Mecklenburg''{{'}}s main battery turrets had {{convert|250|mm|abbr=on}} of armor plating.{{sfn|Gröner|p=16}}

== Service history ==
''Mecklenburg''{{'}}s keel was laid down on 15 May 1900 at [[AG Vulcan]] in [[Stettin]],{{sfn|Hildebrand Röhr & Steinmetz|p=59}} under construction number 248. She was ordered under the contract name "F", as a new unit for the fleet.{{sfn|Gröner|p=16}} The vessel was part of the first class of battleships built under the direction of State Secretary Admiral [[Alfred von Tirpitz]], according to the terms of the [[German Naval Laws|Navy Law of 1898]].{{sfn|Herwig|p=43}} ''Mecklenburg'', the last ship of her class, was launched on 9 November 1901; the ceremony was attended by [[Frederick Francis IV, Grand Duke of Mecklenburg-Schwerin]].{{sfn|Hildebrand Röhr & Steinmetz|p=59}} Fitting-out work proceeded faster on ''Mecklenburg'' than on her sister {{SMS|Schwaben||2}}, and so the former was [[ship commissioning|commissioned]] on 25 June 1903, a full year before ''Schwaben''.{{sfn|Gröner|p=17}}{{sfn|Hildebrand Röhr & Steinmetz|p=58}} ''Mecklenburg'' cost 22,329,000 [[German gold mark|marks]].{{sfn|Gröner|p=16}}

Immediately following her commissioning, ''Mecklenburg'' began [[sea trials]], which lasted until mid-December 1903.{{sfn|Hildebrand Röhr & Steinmetz|p=59}} She was assigned to the II Division of the I Squadron, alongside the battleships {{SMS|Kaiser Karl der Grosse||2}} and {{SMS|Kaiser Wilhelm II||2}}.{{sfn|"The British and German Fleets"|p=335}} ''Mecklenburg'' had to enter the drydock at [[Kaiserliche Werft Wilhelmshaven|''Kaiserliche Werft'']] (Imperial Shipyard) at [[Wilhelmshaven]] for minor improvements and repairs following her trials; this work lasted until the end of February 1904. After these modifications, ''Mecklenburg'' took part in individual and squadron training exercises, and a fleet review for the visiting British King [[Edward VII]] in June. The following month, the German fleet went on a cruise to Britain, the Netherlands, and Norway that lasted until August. ''Mecklenburg'' then participated in the annual autumn fleet exercises, which took place in late August and September, and a winter training cruise in November and December.{{sfn|Hildebrand Röhr & Steinmetz|p=59}}

Starting in mid-December 1904, ''Mecklenburg'' went into Wilhelmshaven for periodic maintenance, which lasted until the beginning of March 1905. After emerging from drydock, ''Mecklenburg'' joined her [[sister ship]] {{SMS|Wittelsbach||2}} on a cruise through the [[Skagerrak]] to Kiel. While steaming through the [[Great Belt]] on 3 March, ''Mecklenburg'' struck the [[Hatterrev|Hatter Reef]] off [[Samsø]], Denmark, and became stuck. Another sister ship, {{SMS|Wettin||2}}, and the light cruiser {{SMS|Ariadne||2}} were sent to assist ''Wittelsbach'' in pulling ''Mecklenburg'' free of the reef. ''Mecklenburg'' then steamed under her own power to Kiel, where dockyard workers discovered a large dent in her bottom. Repair work was completed at the ''Kaiserliche Werft'' in Wilhelmshaven by 20 April, allowing her to participate in the normal routine of training cruises and maneuvers with the fleet for the remainder of the year. During this period, the British [[Channel Fleet]] visited the German fleet while it was moored in [[Swinemünde]].{{sfn|Hildebrand Röhr & Steinmetz|p=59}} From mid-February to the end of March 1906, ''Mecklenburg'' was in the ''Kaiserliche Werft'' in Wilhelmshaven for her annual overhaul. The training routine continued without incident through 1907 but, in early April 1908, a major accident in one of ''Mecklenburg''{{'}}s broadside torpedo rooms nearly sank her. Water began to flood the ship and could only be stopped by sealing the torpedo tubes from the outside; repairs lasted until May.{{sfn|Hildebrand Röhr & Steinmetz|pp=59–60}}

''Mecklenburg'' participated in a training cruise to the [[Azores]] in July and August 1908. She also won the Kaiser's ''Schießpreis'' (Shooting Prize) for the most accurate gunnery in her squadron, along with the battleship {{SMS|Lothringen||2}}.{{sfn|Hildebrand Röhr & Steinmetz|p=60}}{{sfn|"German Naval Notes"|p=1052}} In mid-December, she returned once again to Wilhelmshaven for the yearly overhaul. The years 1909 and 1910 passed uneventfully for ''Mecklenburg'', with the same pattern of training cruises and maneuvers as in previous years. She began her annual overhaul on 2 December 1910 and returned to service on 7 March 1911, though only briefly. On 31 July, ''Mecklenburg'' was replaced by the new [[dreadnought battleship]] {{SMS|Ostfriesland||2}}; ''Mecklenburg'' was then decommissioned and assigned to the Reserve Division in the [[North Sea]]. On 9 May 1912, she was transferred to the Reserve Division in the Baltic. She returned briefly to active service in 1912 from 9 to 12 May to move her from the North Sea to the [[Baltic Sea|Baltic]], and again from 14 August to 28 September to participate in the fleet exercises that year. During the maneuvers, she served in the III Squadron.{{sfn|Hildebrand Röhr & Steinmetz|p=60}}

===World War I===
[[File:North and Baltic Seas, 1911.png|thumb|upright=1.4|Map of the North and Baltic Seas in 1911]]
After the outbreak of World War I in August 1914, ''Mecklenburg'' and the rest of her class were mobilized to serve in the IV Battle Squadron, under the command of Vice Admiral [[Ehrhard Schmidt (admiral)|Ehrhard Schmidt]].{{sfn|Scheer|p=15}} The squadron was initially allocated to the North Sea, but was temporarily transferred to the Baltic in September.{{sfn|Hildebrand Röhr & Steinmetz|p=60}} Starting on 3 September, the IV Squadron, assisted by the [[armored cruiser]] {{SMS|Blücher||2}}, conducted a sweep into the eastern Baltic in the direction of the [[Svenska Högarna]] islands. The operation lasted until 9 September and failed to bring Russian naval units to battle. The squadron participated in a demonstration off [[Windau]] the next day.{{sfn|Halpern|p=185}}{{sfn|Hildebrand Röhr & Steinmetz|p=60}} From 5 December to 2 April 1915, ''Mecklenburg'' and the rest of the squadron were assigned to guard duty in the North Sea, based in the mouth of the [[Elbe]].{{sfn|Hildebrand Röhr & Steinmetz|p=60}}

In May 1915, IV Squadron, including ''Mecklenburg'', was transferred to support the German Army in the Baltic Sea area.{{sfn|Scheer|pp=90–91}} ''Mecklenburg'' and her sisters were then based in [[Kiel]].{{sfn|Halpern|p=192}} From 8 to 12 May, she participated in a sweep toward [[Gotland]] and [[Bogskär]],{{sfn|Hildebrand Röhr & Steinmetz|p=60}} to support the assault on [[Liepāja|Libau]]. ''Mecklenburg'' and the other ships stood off Gotland to intercept any Russian cruisers that might try to intervene in the landings, but this did not occur. On 10 May, after the invasion force had entered Libau, the British submarines {{HMS|E1}} and {{HMS|E9}} spotted the IV Squadron, but were too far away to make an attack.{{sfn|Halpern|p=192}} After the operation, ''Mecklenburg'' and the rest of the IV Squadron returned to the Elbe for guard duties, which lasted until 4 July. The next day, ''Mecklenburg'' departed for Kiel in preparation for a major operation in the Baltic. She proceeded to Danzig, and on 11 July departed for a sweep to [[Gotska Sandön]]; another patrol to western Gotland followed on 21–22 July. ''Mecklenburg'' then steamed from Danzig to Libau on 2 August, where she joined another foray toward Gotska Sandön from 7 to 10 August.{{sfn|Hildebrand Röhr & Steinmetz|p=60}}

''Mecklenburg'' and her sisters were not included in the German fleet that [[Battle of the Gulf of Riga|assaulted the Gulf of Riga]] in August 1915, due to the scarcity of escorts. The increasingly active British submarines forced the Germans to employ more destroyers to protect the capital ships.{{sfn|Halpern|p=197}} ''Mecklenburg'' took part in two sweeps to [[Huvudskär]] on 9–11 and 21–23 September. On 17 December she ran aground in the entrance to the harbor of Libau, but was towed free without suffering any damage. She was to replace the worn-out armored cruiser {{SMS|Prinz Heinrich||2}} in the reconnaissance forces of the fleet in the Baltic, but ''Mecklenburg'' and her sisters were removed from service shortly thereafter. By this stage of the war, the German Navy was facing severe shortages of crews, which could be alleviated by the decommissioning of older, less effective warships. Furthermore, the increasing threat from British submarines and Russian mines in the Baltic by 1916, the latter of which sank the armored cruiser {{SMS|Friedrich Carl||2}}, convinced the German navy to withdraw the elderly ''Wittelsbach''-class ships from active service.{{sfn|Hildebrand Röhr & Steinmetz|p=60}}{{sfn|Herwig|p=168}} On 6 January 1916, ''Mecklenburg'' left Libau bound for Kiel, arriving the following day. She was decommissioned on 24 January and placed in reserve.{{sfn|Hildebrand Röhr & Steinmetz|p=60}}

''Mecklenburg'' was initially based in Kiel and used as a floating prison. In early 1918, she became a barracks ship for the crews of [[U-boat]]s being repaired in Kiel. The ship was briefly retained after the German defeat at the end of World War I,{{sfn|Hildebrand Röhr & Steinmetz|pp=60–61}} but was to be discarded under the terms of the [[Treaty of Versailles]], which limited the re-formed ''[[Reichsmarine]]'' to eight pre-dreadnought battleships of the {{sclass-|Deutschland|battleship|5}} and {{sclass-|Braunschweig|battleship|4}}es, of which only six could be operational at any given time.{{refn|[[s:Treaty of Versailles 1919|Treaty of Versailles]] Section II: Naval Clauses, Article 181.}} Accordingly, on 25 January 1920, ''Mecklenburg'' was stricken from the naval register. She was sold to [[Deutsche Werke]], a shipbuilder based in Kiel, on 16 August 1921 for 1,750,000&nbsp;Marks, and was [[ship breaking|broken up]] for scrap metal that year at Kiel-Nordmole.{{sfn|Gröner|p=17}}{{sfn|Hildebrand Röhr & Steinmetz|p=61}}

== Notes ==

'''Footnotes'''

{{notes
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''" ({{lang-de|link=no|His Majesty's Ship}}).
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (''Schnelladekanone'') denotes that the gun is quick firing, while the L/40 denotes the length of the gun. In this case, the L/40 gun is 40 [[Caliber (artillery)|caliber]], meaning that the gun is 40 times as long as it is in diameter. See: {{harvnb|Grießmer|p=177}}.
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==

{{portal|Battleships}}

'''Books'''

* {{cite book
 | last = Grießmer
 | first = Axel
 | year = 1999
 | language = German
 | title = Die Linienschiffe der Kaiserlichen Marine
 | trans-title=The Battleships of the Imperial Navy
 | publisher = Bernard & Graefe Verlag
 | location = Bonn
 | isbn = 978-3-7637-5985-9
 | oclc = 
 | ref = {{sfnRef|Grießmer}}
 }}
* {{cite book
 | last = Gröner
 | first = Erich
 | year = 1990
 | title = German Warships: 1815–1945
 | publisher = Naval Institute Press
 | location = Annapolis
 | isbn = 978-0-87021-790-6
 | ref = {{sfnRef|Gröner}}
 }}
* {{cite book
 | last = Halpern
 | first = Paul G.
 | year = 1995
 | title = A Naval History of World War I
 | publisher = Naval Institute Press
 | location = Annapolis
 | isbn = 978-1-55750-352-7
 | ref = {{sfnRef|Halpern}}
 }}
* {{cite book
 | last = Herwig
 | first = Holger
 | year = 1998
 | origyear = 1980
 | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
 | publisher = Humanity Books
 | location = Amherst
 | isbn = 978-1-57392-286-9
 | ref = {{sfnRef|Herwig}}
 }}
* {{cite book
 | last1 = Hildebrand
 | first1 = Hans H.
 | last2 = Röhr
 | first2 = Albert
 | last3 = Steinmetz
 | first3 = Hans-Otto
 | year = 1993
 | title = Die Deutschen Kriegsschiffe (Volume 6)
 | trans-title=The German Warships
 | publisher = Mundus Verlag
 | location = Ratingen
 |asin=B003VHSRKE |asin-tld=de
 | oclc = 
 | ref = {{sfnRef|Hildebrand Röhr & Steinmetz}}
 }}
* {{cite book
 | last = Hore
 | first = Peter
 | year = 2006
 | title = The Ironclads
 | publisher = Southwater Publishing
 | location = London
 | isbn = 978-1-84476-299-6
 | ref = {{sfnRef|Hore}}
 }}
* {{cite book
 | last = Scheer
 | first = Reinhard
 | year = 1920
 | title = Germany's High Seas Fleet in the World War
 | publisher = Cassell and Company
 | location = London
 | oclc = 2765294
 | ref = {{sfnRef|Scheer}}
 }}

'''Journals'''

* {{cite journal
 | title = German Naval Notes
 | year = 1909
 | journal = Journal of the American Society of Naval Engineers
 | volume = 21
 | pages = 1052–1056
 | publisher = American Society of Naval Engineers
 | location = Washington D.C.
 | ISSN = 0099-7056
 | oclc = 3227025
 | ref = {{sfnRef|"German Naval Notes"}}
 }}
* {{cite journal
 | title = The British and German Fleets
 | year = 1905
 | journal = The United Service
 | volume = 7
 | pages = 328–340
 | publisher = Lewis R. Hamersly & Co.
 | location = New York
 | oclc = 4031674
 | ref = {{sfnRef|"The British and German Fleets"}}
 }}

{{Wittelsbach class battleship}}


{{DEFAULTSORT:Mecklenburg}}
[[Category:Wittelsbach-class battleships]]
[[Category:Ships built in Stettin]]
[[Category:1901 ships]]
[[Category:World War I battleships of Germany]]