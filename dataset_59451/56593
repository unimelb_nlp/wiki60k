{{Infobox journal
| italic title = force
| title = Disasters
| cover = 
| editor = Sara Pantuliano, Helen Young, John Twigg
| discipline = Planning and Development
| former_names = 
| abbreviation = Disasters
| publisher = [[Wiley-Blackwell]] for the [[Overseas Development Institute]] 
| country = 
| frequency = Quarterly
| history = 1977-Present
| openaccess = 
| license = 
| impact = 0.692
| impact-year = 2011
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-7717
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-7717/issues
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-7717/issues
| link2-name = Online archive
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 0361-3666
| eISSN = 1467-7717
}}
'''''Disasters''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] for the [[Overseas Development Institute]] (ODI). ''Disasters'' is managed by the Humanitarian Policy Group at the ODI.<ref name=Wor>{{cite web|url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-7717/homepage/ProductInformation.html |title=Disasters - Overview - Wiley Online Library |publisher=Onlinelibrary.wiley.com |date= |accessdate=2013-04-11}}</ref> The journal was established in 1977 and covers aspects of disaster studies, policy and management. ''Disasters'' publishes field reports, [[case study]] articles and [[academic papers]]. It is currently edited by Sara Pantuliano (Overseas Development Institute), Helen Young ([[Tufts University]]), and John Twigg ([[University College London]]).<ref>[http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-7717/homepage/EditorialBoard.html "Editorial Board,"] journal website. Accessed: April 11, 2013.</ref> 

According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.692, ranking it 36th out of 54 journals in the category "Planning & Development".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Planning & Development|title=2011 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2012-12-05|series=Web of Science |postscript=.}}</ref>

== References ==
<references/>

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-7717}}

[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1977]]
[[Category:Quarterly journals]]
[[Category:Environmental social science journals]]
[[Category:Development studies journals]]
[[Category:Urban studies and planning journals]]