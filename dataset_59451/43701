<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
| name=Stemme S10
| image=Stemme S10 (8736172304).jpg
| caption=A Stemme S10 taking off
}}{{Infobox Aircraft Type
| type=[[motor glider]]
| national origin=[[Germany]]
| manufacturer=[[Stemme|Stemme AG]]
| designer=
| first flight=
| introduced=1984
| unit cost=$400,000<ref name=Thurber/>
| retired=
| status=
| primary user=
| number built=
| developed from=
}}|}

The '''Stemme S10''' is a [[motor glider|self-launching]] [[Glider aircraft|sailplane]] produced by [[Stemme|Stemme AG]] in [[Strausberg]] (Germany) since the 1980s. The engine is mounted amidships and it features an unusual folding propeller which is stowed inside the aircraft's nose-cone when the engine is not in use.

==Design and development==
The Stemme S10 also has several unusual features such as a tailwheel undercarriage and a side-by-side cockpit. It does not have a tow hook connection so it must self-launch. The two main wheels retract and lower electrically, though they can also be lowered manually if needed. There is an option to fold wings to reduce hangar span to {{convert|11.4|m|ftin|abbr=on|0}}. The engine restart time is 5 seconds. A solar panel can provide additional electrical power during long flights. It has a steerable tailwheel, Schempp-Hirth [[Spoiler (aeronautics)|spoilers]] and optional [[Wingtip device|winglets]]. The current variant, the S10-VT, has a variable-pitch propeller which allows more power during take off, and a new turbocharged Bombardier Rotax 914F engine in place of the earlier Limbach L2400. Most parts are made in Poland, but future production will be handled by [[Remos Aircraft]].<ref name=Thurber>{{cite web|last1=Thurber|first1=Matt|title=AirVenture Report: 2014|url=http://www.ainonline.com/aviation-news/aviation-international-news/2014-09-01/airventure-report-2014|website=ainonline.com|accessdate=24 January 2015|date=1 September 2014}}</ref>

First seen at the 1996 Berlin Air Show, the [[Stemme ASP S15|S15]] variant has a span reduced to {{convert|20.0|m|ftin|abbr=on|0}} and has two underwing hardpoints for scientific or surveillance sensor pods.  There is also an unpiloted version, the '''S-UAV''', again intended for surveillance.

==Operational history==

[[File:MWP Forschungsflugzeug S10VT.jpg|200px|thumb|right|MWP-Research Airplane Stemme S10 VT across the volcano Lanin]]
Atmospheric measurements were made with S10 VT during the [[Mountain Wave Project]] (MWP) Expedition Argentina'99 {{convert|1550|km|mi|abbr=on|0}} record flight to [[Tierra del Fuego]]<ref>Scientific TV-feature (RBB GEO-documentation) „Rodeo in the Sky - Research for greater flight safety“</ref> and during Expedition Mendoza 2006, when scientific measurements of atmospheric turbulence were made up to {{convert|12500|m|ft|abbr=on|0}} around and over the highest mountain of the Americas, [[Aconcagua]].<ref>{{Citation| last1 = Heise| first1 = Rene| title = The sailplane as research laboratory – turbulence measurements above the Andes| journal = European Meteorological Calendar 2011| publisher = [[European Meteorological Society]]| url= http://www.mountain-wave-project.com/press/DMG_Calendar_2011.pdf| year = 2011| accessdate = 20 April 2011}}</ref>
 
An S10 was flown by [[Klaus Ohlmann]] as a pure glider for a record distance of {{convert|2463|km|mi|abbr=on|0}} , in a 14-hour flight.

Two examples were used by the [[United States Air Force Academy]] between 1995 and 2002 under the designation '''TG-11A'''.

==Variants==
;S10
:Standard production variant.
;S10VC
:Surveillance variant with underwing sensor pods.
;S10-VT
:115hp Turbocharged Rotax 914F power.<ref name= sailing>{{cite journal|magazine=AOPA Pilot|title=Power Sailing|author=Thomas Horne|page=58}}</ref>
;TG-11A
:S10s operated by the U.S. Air Force Academy

==Specifications (S 10-VT)==
[[File:StemmeProp (30773204).jpg|200px|thumb|right|Retractable propeller of the Stemme S10: When the engine, which is behind the cockpit, is shut down the prop folds and the nose cone slides back, leaving a clean nose.]]
{{Aircraft specs
|ref=''Jane's All The World's Aircraft 2003–2004.''<ref name=JAWA03-04>{{cite book |title=Jane's All the World's Aircraft 2003-2004 |last= Jackson |first= Paul |year= 2004|publisher= Jane's Information Group |location=Coulsdon, Surrey, United Kingdom|isbn=0-7106-2537-5|pages=176–177}}</ref>
|prime units?=met
|crew=1 pilot
|capacity=1 passenger
|length m=8.42
|length ft=27
|length in=7
|span m=23.00
|span ft=75
|span in=6
|span note=(excluding winglets)
|height m=1.80
|height ft=5
|height in=11
|wing area sqm=18.70
|wing area sqft=201
|aspect ratio=28.3
|empty weight kg=645
|empty weight lb=1,422
|gross weight kg=850
|gross weight lb=1,874
|fuel capacity=
|eng1 number=1
|eng1 name=[[Rotax 914]] F2/S1
|eng1 type=supercharged [[flat-four]] engine
|eng1 kw=84.6
|eng1 hp=113
|prop blade number=2
|prop dia m=
|prop dia ft=
|prop dia in=
|max speed kmh=
|max speed mph=
|max speed kts=
|cruise speed kmh=259
|cruise speed mph=161
|cruise speed kts=140
|never exceed speed kmh=270
|never exceed speed mph=168
|never exceed speed kts=146
|stall speed kmh=78
|stall speed mpg=48
|stall speed kts=42
|range km=1,730
|range miles=1,075
|range nmi=934
|range note=(maximum fuel)
|ceiling m=9,140
|ceiling ft=29,990
|climb rate ms=4.0
|climb rate ftmin=787
|glide ratio=50
|min sink rate m/s=0.57
|min sink rate fpm=112
|g limits=+5.3/-2.65}}

==References==
{{Reflist}}

==External links==
{{Commons category}}
*[http://www.stemme.de/daten/e/produkte/s10/phils10.htm Manufacturer's website]
*[http://www.aetc.af.mil/library/factsheets/factsheet.asp?id=7220 USAF AETC website]
*[http://www.mountain-wave-project.com/misc/mwp_aircraft_s10vt.html Mountain Wave Project website with FAI-world records - S10 VT]
{{Stemme aircraft}}
{{US glider aircraft}}

[[Category:Stemme aircraft|S10]]
[[Category:German sailplanes 1980–1989]]
[[Category:Motor gliders]]