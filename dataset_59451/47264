{{BLP sources|date=November 2013}}
{{Infobox musical artist <!-- See Wikipedia:WikiProject_Musicians -->
| name                = Jon Cowherd
| image               = 
| caption             = Photo by Todd Chalfant
| image_size          =
| background          = non_vocal_instrumentalist
| birth_name          =
| alias               =
| birth_date          =
| birth_place         = [[Kentucky]], [[United States]]
| death_date          =
| death_place         =
| instrument          = [[Piano]]
| genre               = [[Jazz]], [[Post-bop]], [[Pop music|Pop]], [[Rock music|Rock]]
| occupation          = [[Pianist]], [[composer]], [[arranger]], [[Record producer|producer]]
| years_active        =
| label               =
| associated_acts     = [[Brian Blade]], [[Kurt Rosenwinkel]], [[Joni Mitchell]]
| website             = {{website|joncowherd.com}}
| current_members     =
| past_members        =
| notable_instruments =
}}
'''Jon Cowherd'''  is an American [[pianist]], [[composer]], [[arranger]], and [[Record producer|producer]].

Cowherd is most well known for his partnership with [[jazz drummer]] [[Brian Blade]], with whom he co-founded the Brian Blade Fellowship.<ref name="Jon Cowherd">Dog & Pony Industries [http://dogandponyindustries.com/jazz/jon-cowherd/ Dog & Pony Industries: Jon Cowherd Bio], Dog & Pony Industries, Referenced November 2013.</ref> When not recording and touring with the Fellowship, Cowherd works extensively with a broad array of players and singers from the [[jazz]], [[pop music|pop]] and [[rock music|rock]] worlds.<ref name="Jon Cowherd Official Website">Jon Cowherd, [http://joncowherd.com/bio.html Bio: Jon Cowherd], Jon Cowherd, Referenced November 2013.</ref><ref name="ArtistShare FeaturedArtists1">ArtistShare, [http://www.artistshare.com/v4/FeaturedArtists/ ArtistShare FeaturedArtists] ArtistShare.com, Retrieved November 2013.</ref>

== Biography ==
Cowherd was born and raised in [[Kentucky]]. Being a son of musicians (and music educators), Cowherd began music at an early age singing, playing the piano, [[French horn]] and [[violin]]. In 1985 Cowherd moved to [[New Orleans]] to attend [[Loyola University New Orleans|Loyola University]], where he studied jazz piano and improvisation under [[Crescent City Records|Crescent City]] legends [[Ellis Marsalis, Jr.|Ellis Marsalis]], [[John Mahoney]], Steve Masakowski, and Michael Pellera.<ref name="Jon Cowherd" /><ref name="ArtistShare Website">ArtistShare, [http://www.artistshare.com/v4/Projects/Experience/334/449/1/ Project Preview - Mercy], ArtistShare, Inc., Referenced November 2013.</ref>

It was in New Orleans that Cowherd began his partnership with drummer Brian Blade, in which they later formed the [[Brian Blade Fellowship]] together in 1997. The Fellowship consists of bassist Chris Thomas, saxophonists [[Myron Walden]] and Melvin Butler, guitarist Jeff Parker, [[Pedal steel guitar|pedal steel guitarist]] Dave Easley and guitarist [[Kurt Rosenwinkel]]. The Fellowship Band released their debut album in 1998, their second album, ''Perceptual'' in 2000 and their third album, ''Season of Changes'', in 2008. Cowherd is referred to as "the band's secondary leader",<ref name="NY Times">Chinen, Nate [https://query.nytimes.com/gst/fullpage.html?res=9B00EEDC1538F931A25756C0A96E9C8B63&pagewanted=all/ Critics' Choice-New CDs Review], NYTimes, May 12, 2008.</ref> whose playing "shows an artful touch to the keys in ways [[Thelonious Monk]] used to".<ref name="JazzReview">Gilkes, Marshall [http://jazzreview.com/cd-reviews/straight-ahead-classic-cd-reviews/lost-words-by-marshall-gilkes.html/ Lost Words By Marshall Gilkes], JazzReview, March 4, 2008.</ref> Cowherd also worked with Brian Blade in his debut singer-songwriter album ''Mama Rosa'', which was released in 2009 on [[Verve Records]].<ref name="AAJ Review">Kelman, John [http://www.allaboutjazz.com/php/article.php?id=32587#.UnRejKWPtG4/ Brian Blade: Mama Rosa] AllAboutJazz.com, April 21, 2009.</ref>

In 1993, Cowherd moved to [[New York City]], where he earned his master's degree in Jazz Studies from the [[Manhattan School of Music]].<ref name="LinkedIn">Cowherd, Jon [http://www.linkedin.com/pub/jon-cowherd/69/566/379/ Jon Cowherd LinkedIn Profile] LinkedIn.com, Referenced November 2013.</ref>
Cowherd is currently a member of [[Cassandra Wilson]]'s band, and has previously worked with [[Rosanne Cash]], [[Lizz Wright]], [[Iggy Pop]], [[Marc Cohn]], [[Mark Olson (musician)|Mark Olson]], [[Victoria Williams]], [[Daniel Lanois]], [[Joni Mitchell]], [[John Leventhal]], [[Kurt Rosenwinkel]], [[Dave Easley]], [[Jeff Parker (musician)|Jeff Parker]], [[Marcus Strickland]] and [[Jack Wilkins]].<ref name="ArtistShare FeaturedArtists2">ArtistShare [http://www.artistshare.com/v4/FeaturedArtists/ ArtistShare Featured Artists] ArtistShare.com, Referenced November 2013.</ref><ref name="Verve Music Group">Verve Music Group [http://www.vervemusicgroup.com/joncowherd/ Jon Cowherd Bio], VerveMusicGroup.com, Referenced November 2013.</ref>

As a producer, Cowherd has overseen albums by [[Lizz Wright]], [[Alyssa Graham]] and The Local NYC. He also recently served as keyboardist for and co-musical director for the all-star Joni Jazz concert at the [[Hollywood Bowl]], in honor of [[Joni Mitchell]], where he performed with [[Herbie Hancock]], [[Wayne Shorter]], [[Chaka Khan]], [[Kurt Elling]], [[Aimee Mann]], [[Glen Hansard]] and [[Cassandra Wilson]].<ref name="Jon Cowherd" /><ref name="Jon Cowherd Official Website" />

Cowherd's debut album as a leader, ''Mercy'',  was released in 2013 on [[ArtistShare]]. ''Mercy features'' [[Brian Blade]], [[John Patitucci]], and [[Bill Frisell]].<ref name="ArtistShare Website" /><ref>[http://www.wbgo.org/radar/jon-cowherd-mercy-project/ Jon Cowherd: Mercy Project | WBGO] WBGO.org, Retrieved December 2013.</ref> Reviewing the album for allaboutjazz, John Kelman said:
<blockquote> 
And the chemistry shared amongst these four players is no coincidence; instead, it's the sum total of working together in various combinations, just never in this particular one, with Blade representing a connective thread; in addition to his work with Cowherd in the Fellowship Band and his relationship with Patitucci in Shorter's quartet, Blade also toured with Frisell and the perennially undervalued Sam Yahel in 2004, in a trio that never released a commercial recording but is documented in the fourth installment of the guitarist's ongoing Live Download Series. 

With music this well-conceived and a band so telepathically connected, it may have taken Cowherd a long time to release an album under his own name, but with ''Mercy'' as the result, it's been well worth the wait; hopefully there won't be such a long one for the follow-up.<ref>{{cite web|url=http://www.allaboutjazz.com/jon-cowherd-mercy-by-john-kelman.php |title=Jon Cowherd: Mercy |publisher=Allaboutjazz.com |date=July 11, 2014 |accessdate=January 17, 2016}}</ref>
</blockquote>

== Discography ==
=== As leader ===
* ''Mercy'' (2013, [[ArtistShare]])

=== As a co-leader ===
'''With [[Brian Blade Fellowship]]'''
* ''[[Brian Blade Fellowship (album)|Brian Blade Fellowship]]'' (1998, [[Blue Note Records]])
* ''[[Perceptual (album)|Perceptual]]'' (2000, [[Blue Note Records]])
* ''[[Season of Changes]]'' (2008, [[Verve Records]])

=== As sideman ===
* Meg Okura: ''Peace In My Heart'' (1995)
* Swing: ''The Original Broadway Cast Recording'' (1999)
* Mark Olson and the Creek Dippers: ''[[Zola and the Tulip Tree]]'' (2000)
* Steve Sacks: ''Look to the Sky'' (2001)
* Ari Ambrose: ''Waiting'' (2003, [[SteepleChase Records]])
* Liz Wright: ''Salt'' (2003, [[Verve Records]])
* [[The Local]]: ''Just Show Up'' (2004, Budron Records)
* [[Marshall Gilkes]]: ''Edenderry'' (2005, Alternate Side Records)
* Kingsborough Hymns: (2005)
* [[Jack Wilkins]]: ''Until It's Time'' (2005, [[Maxjazz]])
* Metta Quintet: ''Subway Songs'' (2006, [[Sunnyside Records]])
* Mariel Larsen: ''Mariel Larsen'' (2007, Coco Jazz)
* [[Chris Tarry]]: ''Sorry to be Strange'' (2007)
* [[Nicolas Thys]]: ''Virgo'' (2008, Pirouette Records)
* [[Myron Walden]]: ''In This World'' (2008, Demi Sound Records)
* [[Marshall Gilkes]]: ''Lost Words'' (2008, Alternate Side Records)
* Pamela Luss: ''Your Eyes'' (2008, Savant)
* [[Alyssa Graham]]: ''Echo'' (2008, [[Sunnyside Records]]/Walrus)
* [[Brian Blade]]: ''Mama Rosa'' (2009, [[Verve Records]])
* Matt Lemler: ''New Orleans Revival Project'' (2009)
* [[Iggy Pop]]: ''[[Préliminaires]]'' (2009, [[EMI]])
* [[Marcus Strickland]]: ''Open Reel Deck'' (Strick Muzik, 2009)
* [[Rosanne Cash]]: ''We Three Kings'' (2010, [[Blue Note Records]])
* [[Marc Cohn]]: ''Listening Booth 1970'' (2010, Saguaro Road Records)
* Chrissi Poland: ''Songs From the Concrete'' (2010, Danben Records)
* [[Kevn Kinney|Kevin Kinney]]: ''Good Country Mile'' (2011)
* Davy Mooney: ''Perrier Street'' (2011, [[Sunnyside Records]])
* Maria Logis: ''Room for Something New'' (2012)

== References ==
{{Reflist}}

== External links ==
* [http://www.joncowherd.com/ Official Jon Cowherd Website]
* [https://www.facebook.com/pages/Jon-Cowherd/106265526118240/ Official Jon Cowherd Facebook Page]

{{DEFAULTSORT:Cowherd, Jon}}
[[Category:Living people]]
[[Category:American pianists]]
[[Category:Manhattan School of Music alumni]]
[[Category:Grammy Award winners]]
[[Category:American record producers]]
[[Category:Jazz record producers]]
[[Category:American jazz musicians]]
[[Category:Year of birth missing (living people)]]