{{Infobox single
| Name = Alive
| Artist = [[Jennifer Lopez]]
| Album = [[J to tha L–O! The Remixes]]
| A-side = "[[I'm Gonna Be Alright]]"
| Released = {{Start date|2002|05|21}}
| Format = {{flat list|
*[[CD single]]
*[[12-inch single|12"]]
}}
| Recorded = 2001 {{flat list|
*[[Sony Music Studios]]
*[[The Hit Factory]] ([[New York City]])
}}
| Genre = 
| Length = {{Duration|m=4|s=40}}
| Label = [[Epic Records|Epic]]
| Writer = {{flat list|
*Jennifer Lopez
*[[Cris Judd]]
*[[Cory Rooney]]
}}
| Producer = {{flat list|
*Cory Rooney
*[[Dan Shea (producer)|Dan Shea]]
}}
| Last single = "[[Ain't It Funny (Murder Remix)]]"<br />(2002)
| This single = "'''Alive'''"<br />(2002)
| Next single = "[[I'm Gonna Be Alright|I'm Gonna Be Alright (Track Masters Remix)]]"<br />(2002)
}}

"'''Alive'''" is a [[downtempo]] ballad recorded by American entertainer [[Jennifer Lopez]]. It was written by Lopez, [[Cris Judd]], and [[Cory Rooney]] for the [[Michael Apted]]-directed [[thriller (genre)|thriller]], ''[[Enough (film)|Enough]]'' (2002). Lopez stars in the film as Slim, a waitress who marries a wealthy contractor, and flees with their young daughter as he becomes increasingly abusive. While on the run, Slim, who discovers that her husband is following her, "trains herself for a final, violent confrontation".<ref name="Has enough in film" /> The producers of ''Enough'' wanted her to write a new song for film, however, Lopez felt as if it was not something that could be forced. In October 2001, while on their honeymoon, Judd played Lopez a melody that he had written on the piano. She thought that the melody was "really beautiful", and that it would be perfect fit for ''Enough''. She incorporated the hardships that Slim went through in the film, as well as her own personal struggles, into the song's lyrics.

"Alive" doesn't appear on the [[film score]] of ''Enough'', and instead was included on Lopez's first [[remix album]], ''[[J to tha L–O! The Remixes]]'' (2002). It was serviced to radio in the United States on May 21, 2002, by [[Epic Records]]. The song was released commercially on a [[12-inch single|12" vinyl]] as a [[A-side and B-side|double a-side]] along with "[[I'm Gonna Be Alright]]" on July 30. Music critics were divided on "Alive"; some deemed Lopez's change in musical style to be refreshing, while others felt as if she should stick to the [[dance music|dance]] arrangements of her previous work. The song's accompanying [[music video]] was directed by Jim Gable. It features Lopez playing the piano, writing the lyrics to "Alive" and recording the song, intercut between scenes from ''Enough''. While the song failed to appear on any national charts, it did reach number two on the US ''[[Billboard (magazine)|Billboard]]'' [[Hot Dance Club Songs|Dance Music/Club Play Singles]] chart. Lopez performed the song live on ''[[The Oprah Winfrey Show]]'' on May 17, 2002.

== Background ==
It was first reported in November 2000 that [[Jennifer Lopez]] was in talks to star in the [[Michael Apted]]-directed thriller ''[[Enough (film)|Enough]]''.<ref>{{cite news|last=Brodesser |first=Claude |archiveurl=http://www.webcitation.org/6EJmOLEZb?url=http%3A%2F%2Fwww.variety.com%2Farticle%2FVR1117788938%2F |archivedate=February 10, 2013 |url=http://www.variety.com/article/VR1117788938 |title=Lopez, Apted eye 'Enough' |work=[[Variety (magazine)|Variety]] |publisher=[[PMC (company)|Penske Business Media]] |date=November 7, 2000 |accessdate=February 10, 2013 |deadurl=no |df=mdy }}</ref> [[Sandra Bullock]] had originally signed on to portray the lead, but was forced to back out of the film due to scheduling conflicts.<ref name="Has enough in film" /> Lopez's casting in the film was confirmed by [[MTV News]] on May 19, 2001.<ref name="Has enough in film">{{cite web|archiveurl=http://www.webcitation.org/6EJmYVThC?url=http%3A%2F%2Fwww.mtv.com%2Fnews%2Farticles%2F1441847%2Fjennifer-lopez-has-enough-film.jhtml |archivedate=February 10, 2013 |last=Basham |first=David |title=Jennifer Lopez Has 'Enough' in Film |url=http://www.mtv.com/news/articles/1441847/jennifer-lopez-has-enough-film.jhtml |work=[[MTV News]] |publisher=[[MTV Networks]] |accessdate=February 10, 2013 |date=May 19, 2001 |deadurl=no |df=mdy }}</ref> Filming began two days later and took place on location in [[Los Angeles]], [[San Francisco]] and [[Seattle]].<ref name="Has enough in film" /> Recording music and filming ''Enough'' at the same time, Lopez became overworked and had a [[nervous breakdown]]. She suffered from a lack of sleep and froze up in her trailer. In 2008, Lopez confessed: "I was like&nbsp;– I don't want to move, I don't want to talk, I don't want to do anything."<ref name="I had a nervous breakdown">{{cite news|archiveurl=http://www.webcitation.org/6EJmkByYK?url=http%3A%2F%2Fwww.telegraph.co.uk%2Fnews%2Fcelebritynews%2F3152788%2FJennifer-Lopez-I-had-a-nervous-breakdown.html |archivedate=February 10, 2013 |last=Singh |first=Anita |title=Jennifer Lopez: I had a nervous breakdown |url=http://www.telegraph.co.uk/news/celebritynews/3152788/Jennifer-Lopez-I-had-a-nervous-breakdown.html |work=[[The Daily Telegraph]] |publisher=[[Telegraph Media Group]] |accessdate=February 10, 2013 |date=October 7, 2008 |deadurl=no |df=mdy }}</ref>

Lopez stars in ''Enough'' as Slim, a waitress whose life is severely altered after she marries Mitch, a wealthy contractor portrayed by [[Billy Campbell]]. Slim flees her increasingly abusive husband with their newborn daughter following her birth. She soon discovers that she is being followed by Mitch. While on the run, Slim "trains herself for a final, violent confrontation" with her husband.<ref name="Has enough in film" /> For the final act, which required Lopez to "become a believable lean, mean fighting machine", she thought about learning [[T'ai chi ch'uan|t'ai chi]] or [[taekwondo]], but was worried about learning it at an expert level in a short period of time.<ref name="Lopez on getting buff" /> Her personal trainer then suggested that she study [[Krav Maga]], the "official self-defense system of the [[Israeli Defense Forces]] which has recently become trendy in the States", according to MTV News.<ref name="Lopez on getting buff">{{cite web|archivedate=February 10, 2013 |archiveurl=http://www.webcitation.org/6EJmyeyH0?url=http%3A%2F%2Fwww.mtv.com%2Fnews%2Farticles%2F1454028%2Fjennifer-lopez-on-getting-buff-having-enough.jhtml |last=Downey |first=Ryan J. |title=Jennifer Lopez On Getting Buff & Having 'Enough' |url=http://www.mtv.com/news/articles/1454028/jennifer-lopez-on-getting-buff-having-enough.jhtml |work=MTV News |publisher=MTV Networks |accessdate=February 10, 2013 |date=May 16, 2002 |deadurl=no |df=mdy }}</ref> ''Enough'' was initially scheduled to be released in September, but was later pushed back to "early 2002" and eventually saw a release on May 24, 2002.<ref name="Has enough in film" />

== Writing and production ==
{{Listen|pos=right|filename=Jennifer Lopez_-_Alive.ogg|title="Alive" (2002)|description=A 25 second sample of "Alive", a [[downtempo]] "sweet, string-laden" ballad.<ref name="Slant"/><ref name="Singles Billboard"/>}}

The producers of ''Enough'' wanted Lopez to write a new song for the film. According to Lopez, writing a song is something that cannot be forced; "It kinda just has to happen".<ref name="NY Rock">{{cite web|archiveurl=http://www.webcitation.org/6EJNzg2O3?url=http%3A%2F%2Fwww.nyrock.com%2Finterviews%2F2002%2Flopez_int2.asp |archivedate=February 9, 2013 |url=http://www.nyrock.com/interviews/2002/lopez_int2.asp |title=Interview with Jennifer Lopez |publisher=NY Rock |accessdate=January 31, 2013 |deadurl=no |df=mdy }}</ref> While on their [[honeymoon]] in October 2001, [[Cris Judd]] played Lopez a melody that he had written on the piano. She thought that the melody was "really beautiful" and that it would make a "wonderful song".<ref name="Feels Alive">{{cite web|url=http://www.books.google.com/books?id=Qg8EAAAAMBAJ&pg=PA19 |page=19 |volume=114 |ISSN=0006-2510 |first=Larry |last=Flick| title=J-Lo Feels 'Alive' |work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]] |date=May 25, 2002 |accessdate=January 31, 2013}}</ref> Lopez felt that a "little bit of it sounded like the movie, a little haunting".<ref name="NY Rock"/> When she heard the melody, Lopez said to herself: "I can write something to that right now. And we can use it in ''Enough''".<ref name="NY Rock"/> She then began to think about the film and the character that she portrays and what she had been through. In addition, she also thought about the things that she had gone through in her own life.<ref name="NY Rock"/> According to Lopez, it was easy to be inspired by the melody Judd had written,<ref name="Feels Alive"/> "and just kind of that grateful feeling, to just be here [...] And keeping it simple. Because life can get so crazy and everything. And then you realize, it's all just kinda about the simplicity of being here, and being healthy. And being alive. So that's where the song came from."<ref name="NY Rock"/>

"Alive" was written by Lopez, Judd and [[Cory Rooney]].<ref name="Album credits">{{cite AV media notes |title=[[J to tha L–O! The Remixes]] |type=CD liner notes |others=[[Jennifer Lopez]] |year=2002 |publisher=[[Epic Records]], a division of [[Sony Music Entertainment]] |location=[[New York City|New York, NY]] }}</ref> The song was produced by Rooney and [[Dan Shea (record producer)|Dan Shea]], who also provided [[Programming (music)|programming]] for the song. Lopez's vocals were recorded at [[Sony Music Studios]] and [[The Hit Factory]] in [[New York City]]. Her vocals were [[Audio engineering|engineered]] by Robert Williams, with assistance from Jamie Gudewicz, Jason Dale and Kyle White. Pat Webe acted as the stage engineer for the song. Humberto Gatica provided [[audio engineering]] for the [[string instrument|strings]], which were arranged and conducted by Bill Ross. They were recorded by Bill Smith at [[Sony Pictures Studios]] in [[Culver City]], California, with assistance from Jason Lloyd, Mark Eshelman and Julianne Masted. "Alive" was mixed by [[Mick Guzauski]] at Barking Doctor Studios in [[Mount Kisco, New York|Mount Kisco]], with assistance from Tom Bender. The song was mastered by Herb Powers at Hit Factory Mastering in New York City.<ref name="Album credits"/> After the song was completed, Lopez played it for the producers of ''Enough'' and they "loved it".<ref name="Feels Alive"/>

"Alive" is not "the type of music" that Lopez is used to making. According to Lopez, she knew that there was something "inside of me that I have to offer to this ["Alive"]. And then, you know, things happen. Things have a weird way of happening naturally and organically when they're supposed to. And I feel that this song was just meant to be, for this movie. Basically, I wrote the song for me to go through all these kinds of things as an actress with this character. You draw from all different experiences in your life. From way back to the present, to whatever, to make it really true and honest".<ref name="NY Rock"/> "Alive" means "the world" to Lopez because of its positive message and the fact that it was something that she created with Judd.<ref name="Feels Alive"/>

== Release and reception ==
<!-- Deleted image removed: [[File:Chris8.jpg|thumb|left|[[Chris Cox (DJ)|Chris Cox]] (of [[Thunderpuss]]) remixed "Alive" into a "club-ready" dance track.]] -->

"Alive" was serviced to radio in the United States on May 21, 2002, as the second single from ''J to tha L–O! The Remixes''.<ref>{{cite web|archiveurl=http://www.webcitation.org/6EJr9MXsK?url=http%3A%2F%2Ftop40-charts.com%2Fnews.php%3Fnid%3D2871 |archivedate=February 10, 2013 |url=http://www.top40-charts.com/news.php?nid=2871 |title=Theme Song for 'Enough' Written and Performed by Jennifer Lopez |publisher=Top40 Charts |date=May 21, 2002 |accessdate=January 31, 2013 |deadurl=no |df=mdy }}</ref> The song was released commercially on a [[12-inch single|12" vinyl]] as a [[A-side and B-side|double a-side]] along with "[[I'm Gonna Be Alright]]" on July 30.<ref>{{cite web|url=http://www.amazon.com/Im-Gonna-Be-Alright-Alive/dp/B0000691M8|title=I'm Gonna Be Alright / Alive|publisher=[[Amazon.com]]|accessdate=May 8, 2015}}</ref> [[Chris Cox (DJ)|Chris Cox]] and Barry Harris, known collectively as [[Thunderpuss]], remixed "Alive" into a "club-ready" dance track.<ref name="Feels Alive" /> The song managed to reach number two on the US '' Billboard'' [[Hot Dance Club Songs|Dance Music/Club Play Singles]] chart, staying behind "You Gotta Believe" from Tommy Boy Silver.<ref name="BBJUN8"/> It topped the Maxi-Single Sales chart for three weeks.<ref name="BBJUN8">{{cite news|url=http://www.books.google.com/books?id=Hw0EAAAAMBAJ&pg=PA44|work=Billboard|publisher=Nielsen Business Media|date=September 14, 2002|issn=0006-2510|volume=114|issue=37|title=Maxi-Single Sales|accessdate=February 10, 2013}}</ref> The song also managed to reach number 18 on the [[Adult Contemporary (chart)|Adult Contemporary]] chart.<ref name="Allmusic"/> At the [[2003 Latin Billboard Music Awards|2003 ''Billboard'' Latin Music Awards]] the Thunderpuss Remix of "Alive" received an award for Latin Dance Single of the Year and Latin Dance Club Play Track of the Year.<ref>{{Cite web| year = 2003| title = 2003 Billboard Latin Awards Finalists| work=Billboard| publisher=Prometheus Global Media| url = http://www.billboard.com/articles/news/71168/2003-billboard-latin-music-award-winners| accessdate = November 5, 2013}}</ref><ref>{{Cite web| year = 2003| title = 2003 Billboard Latin Awards Finalists| work=Billboard| publisher=Prometheus Global Media| url = http://www.billboard.com/articles/news/72340/2003-billboard-latin-awards-finalists| accessdate = August 23, 2013}}</ref>

Music critics were divided on "Alive". Chuck Taylor of ''[[Billboard (magazine)|Billboard]]'' stated that: "Remember that 'Oh, my' feeling the first time you heard [[Madonna (entertainer)|Madonna]] sing her first ballad, '[[Crazy for You (Madonna song)|Crazy for You]]'? It's déjà vu with 'Alive'."<ref name="Singles Billboard"/> According to Taylor, Lopez instills a "vulnerable spirit of romanticism" in the track.<ref name="Singles Billboard">{{cite web|first=Chuck |last=Taylor |url=http://www.books.google.com/books?id=Qg8EAAAAMBAJ&pg=PA24 |title=Reviews and Previews: Singles&nbsp;– Jennifer Lopez Alive |work=Billboard |publisher=Prometheus Global Media |date=May 25, 2002 |page=24 |volume=114 |ISSN=0006-2510 |accessdate=January 31, 2013}}</ref> He wrote that Lopez "does her best" with the song, a "regal ballad".<ref name="Singles Billboard"/> He called "Alive" a "bold new terrain" for Lopez, "an artist who is heralded more for her videogenics than her voice."<ref name="Singles Billboard"/> Taylor wrote that "it's nice to have top 40's leading staple artist diversify her portfolio with a musical summer fling that will maintain programmers' and fans' love affair with Lopez".<ref name="Singles Billboard"/> He stated that the radio remix of "Alive" is "made more luxurious" with the addition of a "cascade of fluttering strings".<ref name="Singles Billboard"/>

Sal Cinquemani of [[Slant Magazine]] called "Alive" the "biggest surprise" of ''J to tha L–O! The Remixes''.<ref name="Slant">{{cite web|first=Sal |last=Cinquemani |archiveurl=https://web.archive.org/web/20110823090854/http://www.slantmagazine.com/music/review/jennifer-lopez-j-to-tha-l-o/113|archivedate=August 23, 2011|url=http://www.slantmagazine.com/music/review/jennifer-lopez-j-to-tha-l-o/113 |title=Jennifer Lopez: J to tha L-O &#124; Music Review |publisher=[[Slant Magazine]] |date=February 1, 2002 |accessdate=January 31, 2013}}</ref> William Ruhlmann of [[Allmusic]] gave the song a negative review, writing that her vocal limitations "come to the fore when she is spotlighted on such a song without a dance arrangement to support her".<ref name="Allmusic"/> He stated, however, that her vocal limitations are not "as big a deal" as her lack of emotional involvement: "Though she tries to fake it here and there, she still sounds like she's singing her grocery list instead of the song's clichéd lyrics".<ref name="Allmusic">{{cite web|archiveurl=http://www.webcitation.org/6EJp3Osrl?url=http%3A%2F%2Fwww.allmusic.com%2Falbum%2Fj-to-tha-l-o%2521-the-remixes-mw0000658032 |archivedate=February 10, 2013 |last=Ruhlmann |first=William |url=http://www.allmusic.com/album/j-to-tha-l-o%21-the-remixes-mw0000658032 |title=J to tha L–O!: The Remixes&nbsp;– Jennifer Lopez |work=[[AllMusic]] |publisher=[[Rovi Corporation]] |accessdate=January 31, 2013 |deadurl=no |df=mdy }}</ref> David Browne of ''[[Entertainment Weekly]]'' called the song a "syrupy ballad" that is "neither alive nor a remix".<ref name="EW"/> He stated that if you listen to the song long enough, "you may actually be conned into thinking Lopez's voice and songs are passable".<ref name="EW"/> He concluded by stating as a result of "Alive", ''J to tha L–O! The Remixes'' may be the most insidious album ever made.<ref name="EW">{{cite web|archiveurl=http://www.webcitation.org/6EJuCSvFX?url=http%3A%2F%2Fwww.ew.com%2Few%2Farticle%2F0%2C%2C251577%2C00.html |archivedate=February 10, 2013 |url=http://www.ew.com/ew/article/0,,251577,00.html |title=J to tha L–O! The Remixes Review |last=Browne |first=David |work=[[Entertainment Weekly]] |publisher=[[Time Inc.|Time Inc]] |date=March 8, 2002 |accessdate=January 31, 2013 |deadurl=no |df=mdy }}</ref>

== Promotion ==
[[File:Alive (video).png|thumb|right|Lopez writing the lyrics to "Alive" while scenes from ''Enough'' are displayed in the background.]]

The accompanying [[music video]] for "Alive" was directed by Jim Gable.<ref name="The Reel Me">{{cite AV media notes |title=[[The Reel Me]] |others=Jennifer Lopez |year=2003 |publisher=Epic Records, a division of Sony Music Entertainment |location=New York City|type=DVD liner notes}}</ref> The video opens with Lopez composing the melody and writing the lyrics for "Alive" on a piano in her living room, which overlooks a pier. Behind her, a flashing screen displays scenes from ''Enough'', along with Lopez recording the song in a studio with her band. After walking away from the piano, she lies optimistically on her sofa. She is then seen singing amid a large body of outdoor water. At the instrumental passage for "Alive", Lopez embraces the music being played by the band and changes a few chords with [[Cory Rooney]], who is seated at a piano. She then emotionally finishes recording the song during its final chorus. Throughout the entire video, clips from the film continue to be inter cut with these settings. The music video for "Alive" was included on a special edition [[DVD]] re-release of ''Enough'', as well as Lopez's first music video compilation ''[[The Reel Me]]'' (2003).<ref name="The Reel Me"/><ref>{{cite web|last=Wallis |first=J. Doyle |title=Enough&nbsp;– Special Edition (Widescreen) |url=http://www.dvdtalk.com/reviews/7622/enough-special-edition-widescreen |work=[[DVD Talk]] |publisher=[[Internet Brands]] |accessdate=February 10, 2013 |date=September 16, 2003 |archiveurl=http://www.webcitation.org/6EK1SKASV?url=http%3A%2F%2Fwww.dvdtalk.com%2Freviews%2F7622%2Fenough-special-edition-widescreen%2F |archivedate=February 10, 2013 |deadurl=no |df=mdy }}</ref> Lopez performed the song for the first and only time on ''[[The Oprah Winfrey Show]]'' on May 17, 2002.<ref>{{cite video |people=[[Oprah Winfrey]] (host) |date=May 17, 2002 |title=[[The Oprah Winfrey Show]] |medium=Television production |publisher=[[King World Productions]] |location=United States}}</ref>

== Track listings ==
{{col-begin}}
{{col-2}}

*;UK CD promo single<ref>{{cite AV media notes |title=''"Alive"'' |type=[[CD single]] liner notes| |others=Jennifer Lopez |year=2002 |publisher=[[The Sun (United Kingdom)|The Sun]]|location=[[London]], [[England]] | id=SUNW001}}</ref>
#"Alive" (Album Version)&nbsp;— 4:41
#"Alive" (Thunderpuss Radio Mix)&nbsp;— 4:12

*;US CD promo single<ref>{{cite AV media notes |title=''"Alive"'' |type=[[CD single]] liner notes| |others=Jennifer Lopez |year=2002 |publisher=[[Epic Records]], a division of [[Sony Music Entertainment]] |location=[[New York City]], [[New York (state)|New York]] | id=ESK 56903}}</ref>
#"Alive" (Radio Remix)&nbsp;— 4:20
#"Alive" (Album Version)&nbsp;— 4:41

*;US 12" promo vinyl<ref>{{cite AV media notes |title=''"Alive"'' |type=[[12-inch single]] liner notes| |others=Jennifer Lopez |year=2002 |publisher=[[Epic Records]], a division of [[Sony Music Entertainment]] |location=[[New York City]], [[New York (state)|New York]] | id=EAS 59922}}</ref>
#"Alive" (Thunderpuss Club Mix)&nbsp;— 8:51
#"Alive" (Thunderpuss Tribe-A-Pella)&nbsp;— 7:50
#"Alive" (Thunderpuss Radio Mix)&nbsp;— 4:12

{{col-2}}

*;US 12" vinyl<ref>{{cite AV media notes |title=''"[[I'm Gonna Be Alright]] / Alive"'' |type=[[CD single]] liner notes |others=Jennifer Lopez |year=2002 |publisher=[[Epic Records]], a division of [[Sony Music Entertainment]] |location=[[New York City]], [[New York (state)|New York]] | id=ESK EPC 672748 2}}</ref>
#"[[I'm Gonna Be Alright]]" (Track Masters Remix) featuring [[Nas]]&nbsp;— 2:51
#"I'm Gonna Be Alright" (Track Masters Remix)&nbsp;— 3:14
#"I'm Gonna Be Alright" (Track Masters Remix Instrumental)&nbsp;— 3:14
#"Alive" (Thunderpuss Club Mix)&nbsp;— 8:51
#"Alive" (Thunderpuss Tribe-A-Pella)&nbsp;— 7:50

{{col-end}}

== Credits and personnel ==
Credits adapted from the liner notes of ''J to tha L–O! The Remixes''.<ref name="Album credits"/>
;Locations
* Vocals recorded at [[Sony Music Studios]] and [[The Hit Factory]] in [[New York City]], New York.
* Strings recorded at [[Sony Pictures Studios]] in [[Culver City]], California.
* Mixed at Barking Doctor Studios in [[Mount Kisco, New York|Mount Kisco]], New York.
* Mastered at Hit Factory Mastering in New York City, New York.
{{Div col|cols=2}}
; Personnel
* [[Jennifer Lopez]]&nbsp;— songwriter, lead vocals and background vocals
* [[Cris Judd]]&nbsp;— songwriter
* [[Cory Rooney]]&nbsp;— producer, songwriter
* [[Dan Shea (record producer)|Dan Shea]]&nbsp;— producer, programmer
* Robert Williams&nbsp;— [[Audio engineering|engineer]]
* Jamie Gudewicz&nbsp;— assistance engineer
* Jason Dale&nbsp;— assistance engineer
* Kyle White&nbsp;— assistance engineer
* Pat Webe&nbsp;— stage engineer
* Humberto Gatica&nbsp;— string engineer
* Bill Ross&nbsp;— string engineer and conductor
* Bill Smith&nbsp;— string recorder
* Jason Lloy&nbsp;— assistant string recorder
* Mark Eshelman&nbsp;— assistant string recorder
* Julianne Masted&nbsp;— assistant string recorder
* [[Mick Guzauski]]&nbsp;— mixer
* Tom Bender&nbsp;— assistant mixer
* Herb Powers&nbsp;— mastering
{{Div col end}}

== Charts ==
{| class="wikitable sortable plainrowheaders"
|-
! Chart (2002)
! Peak<br />position
|-
{{singlechart|Billboardadultcontemporary|18|artist=Jennifer Lopez|rowheader=true|accessdate=August 11, 2013}}
|-
{{singlechart|Billboarddanceclubplay|2|artist=Jennifer Lopez|note=Thunderpuss Remix|rowheader=true|accessdate=August 11, 2013}}
|-
{{singlechart|Billboardlatintropical|36|artist=Jennifer Lopez|rowheader=true|accessdate=August 11, 2013}}
|-
|}

== References ==
{{reflist|30em}}

== External links ==
*{{MetroLyrics song|jennifer-lopez|alive}}<!-- Licensed lyrics provider -->

{{Jennifer Lopez songs}}
{{Use mdy dates|date=February 2013}}
{{good article}}

[[Category:2002 songs]]
[[Category:2002 singles]]
[[Category:Jennifer Lopez songs]]
[[Category:Songs written for films]]
[[Category:Songs written by Jennifer Lopez]]
[[Category:Songs written by Cory Rooney]]
[[Category:Song recordings produced by Cory Rooney]]
[[Category:Song recordings produced by Dan Shea (producer)]]
[[Category:Pop ballads]]