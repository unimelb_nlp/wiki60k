{{confused|The 1000 Genomes Project}}
The '''$1,000 genome''' refers to an era of predictive and [[personalized medicine]] during which the cost of full [[genome]] [[DNA sequencing|sequencing]] an individual or patient costs roughly USD $1,000.<ref>{{Cite journal 
| last1 = Mardis | first1 = E. R. 
| authorlink = Elaine Mardis
| title = Anticipating the 1,000 dollar genome 
| journal = Genome Biology 
| volume = 7 
| issue = 7 
| pages = 112 
| year = 2006 
| doi = 10.1186/gb-2006-7-7-112 
| pmid = 17224040 
| pmc = 1779559
}}</ref><ref>{{Cite journal 
| last1 = Service | first1 = R. F. 
| title = GENE SEQUENCING: The Race for the $1000 Genome 
| doi = 10.1126/science.311.5767.1544 
| journal = Science 
| volume = 311 
| issue = 5767 
| pages = 1544–1546 
| year = 2006 
| pmid = 16543431 
| pmc = 
}}</ref> It is also the title of a book by British science writer and founding editor of [[Nature Genetics]], Kevin Davies.<ref>Kevin Davies. ''The $1,000 Genome.'' (New York: Free Press, 2010). ISBN 1-4165-6959-6</ref>

== History ==
The “$1,000 genome” catchphrase was first publicly recorded in December 2001 at a scientific retreat to discuss the future of biomedical research following publication of the first draft of the HGP, convened by the [[National Human Genome Research Institute]] at Airlie House in Virginia.<ref>[http://www.genome.gov/10001294 Beyond the Beginning: The Future of Genomics. Meeting webcast. http://www.genome.gov/10001294].</ref> The phrase neatly highlighted the chasm between the actual cost of the [[Human Genome Project]], estimated at $2.7 billion over a decade, and the benchmark for routine, affordable personal genome sequencing.

On 2 October 2002, [[Craig Venter]] introduced the opening session of GSAC (The Genome Sequencing and Analysis Conference) at the Hynes Convention Center in Boston: “The Future of Sequencing: Advancing Towards the $1,000 Genome.” Speakers included [[George M. Church]] and executives from [[454 Life Sciences]], [[Solexa]], U.S. Genomics, VisiGen and [[Amersham plc]].<ref>Sylvia Pagan Westphal. [http://www.newscientist.com/article/dn2900-race-for-the-1000-genome-is-on.html "Race for the $1000 genome is on." ''New Scientist'' 12 October 2002].</ref><ref>Mark D. Uehling. [http://www.bio-itworld.com/archive/111202/genome "Wanted: The $1000 Genome." ''Bio-IT World'' November 2002].</ref> In 2003, Venter announced that his foundation would earmark $500,000 for a breakthrough leading to the $1,000 genome.<ref>[http://www.bio-itworld.com/newsitems/2005/oct2005/10-19-05-news-genome-prize "Venter raises stakes for $1,000 genome prize." ''Bio-IT World'' October 2005].</ref> That sum was subsequently rolled into the [[Archon X Prize]].

In October 2004, NHGRI introduced the first in a series of '$1,000 Genome' grants designed to advance "the development of breakthrough technologies that will enable a human-sized genome to be sequenced for $1,000 or less."<ref>NIH press release. [http://www.genome.gov/12513210 "NHGRI seeks next generation of sequencing technologies." 14 October 2004].</ref>

In a January 2006 article in [[Scientific American]] making the case for the [[Personal Genome Project]], George M. Church wrote
:“The ‘$1,000 genome’ has become shorthand for the promise of DNA-sequencing capability made so affordable that individuals might think the once-in-a-lifetime expenditure to have a full personal genome sequence read to a disk for doctors to reference is worthwhile.”:<ref>{{Cite journal 
| last1 = Church | first1 = G. M. 
| authorlink = George M. Church
| title = Genomes for all 
| journal = Scientific American 
| volume = 294 
| issue = 1 
| pages = 46–54 
| year = 2006 
| url = http://arep.med.harvard.edu/pdf/Church05s.pdf
| pmid = 16468433
 | doi=10.1038/scientificamerican0106-46
}}</ref>

In 2007, the journal [[Nature Genetics]] invited dozens of scientists to respond to its ‘[http://www.nature.com/ng/qoty/index.html Question of the Year]’:
:“The sequencing of the equivalent of an entire human genome for $1,000 has been announced as a goal for the genetics community... What would you do if [the $1,000 genome was] available immediately?”:<ref>Question of the Year. ''Nature Genetics''. http://www.nature.com/ng/qoty/index.html</ref>

In May 2007, during a ceremony held at Baylor College of Medicine, [[454 Life Sciences]] founder [[Jonathan Rothberg]] presented [[James D. Watson]] with a digital copy of his personal genome sequence on a portable hard drive. Rothberg estimated the cost of the sequence—the first personal genome produced using a [[next-generation sequencing]] platform—at $1 million.<ref>BCM press release. [http://www.bcm.edu/news/packages/watson_genome.cfm "Nobel laureate James Watson receives personal genome in ceremony at Baylor College of Medicine." 31 May 2007].</ref> Watson's genome sequence was published in 2008.<ref>{{Cite journal | last1 = Wheeler | first1 = D. A. | last2 = Srinivasan | first2 = M. | last3 = Egholm | first3 = M. | last4 = Shen | first4 = Y. | last5 = Chen | first5 = L. | last6 = McGuire | first6 = A. | last7 = He | first7 = W. | last8 = Chen | first8 = Y. J. | last9 = Makhijani | first9 = V. | last10 = Roth | first10 = G. T.| last11 = Gomes | first11 = X.| last12 = Tartaro | first12 = K.| last13 = Niazi | first13 = F.| last14 = Turcotte | first14 = C. L.| last15 = Irzyk | first15 = G. P.| last16 = Lupski | first16 = J. R.| last17 = Chinault | first17 = C.| last18 = Song | first18 = X.-Z.| last19 = Liu | first19 = Y.| last20 = Yuan | first20 = Y.| last21 = Nazareth | first21 = L.| last22 = Qin | first22 = X.| last23 = Muzny | first23 = D. M.| last24 = Margulies | first24 = M.| last25 = Weinstock | first25 = G. M.| last26 = Gibbs | first26 = R. A.| last27 = Rothberg | first27 = J. M.| doi = 10.1038/nature06884 | title = The complete genome of an individual by massively parallel DNA sequencing | journal = Nature | volume = 452 | issue = 7189 | pages = 872–876 | year = 2008 | pmid =  18421352| pmc = }}</ref>

A number of scientists have highlighted the cost of additional analysis after performing sequencing. [[Bruce R. Korf|Bruce Korf]], past president of the [[American College of Medical Genetics]], described “the $1-million interpretation.”<ref>Kevin Davies. [http://www.bio-itworld.com/2010/10/01/interpretation.html "The $1,000,000 genome interpretation." ''Bio-IT World'' October 2010].</ref>{{failed verification|date=December 2016}} Washington University’s [[Elaine Mardis]] prefers “the $100,000 analysis.”<ref>{{Cite journal | last1 = Mardis | first1 = E. R. | authorlink = Elaine Mardis| title = The $1,000 genome, the $100,000 analysis? | doi = 10.1186/gm205 | journal = Genome Medicine | volume = 2 | issue = 11 | pages = 84 | year = 2010 | pmid =  21114804| pmc =3016626 }}</ref>

== Commercial efforts ==
At the end of 2007, the biotech company Knome debuted the first direct-to-consumer genome sequencing service at an initial price of $350,000 (including analysis). One of the first clients was Dan Stoicescu, a Swiss-based biotech entrepreneur.<ref>Amy Harmon. [https://www.nytimes.com/2008/03/04/health/research/04geno.html "Gene map becomes a luxury item." ''New York Times'', March 4, 2008].</ref> As the costs of sequencing continued to plummet, in 2008, Illumina announced that it had sequenced an individual genome for $100,000 in reagent costs. Applied Biosystems countered by saying the cost on its platform was $60,000.<ref>Bernadette Tansey. [http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2008/03/12/BUTKVINOB.DTL "Applied Biosystems cuts DNA sequencing cost." SF Gate, March 13, 2008].</ref> [[Pacific Biosciences]] became the latest entrant in what the [[New York Times]] called "a heated race for the '$1,000 genome'", suggesting it would offer the 15-minute genome by 2013.<ref>Andrew Pollack. "The race to read genomes on a shoestring, relatively speaking." [https://www.nytimes.com/2008/02/09/business/09genome.html?pagewanted=all New York Times, February 9, 2008].</ref><ref>{{Cite journal 
| last1 = Wade | first1 = N. 
| title = The quest for the $1,000 human genome: DNA sequencing in the doctor's office? At birth? It may be coming closer 
| journal = The New York times 
| pages = F1, F3 
| year = 2006 
| pmid = 16874933
}}</ref>

In 2009, [[Stanford University]] professor [[Stephen Quake]] published a paper sequencing his own genome on an instrument built by [[Helicos Biosciences]] (a company he co-founded) for a reported cost in consumables of $48,000.<ref>{{Cite journal | last1 = Pushkarev | first1 = D. | last2 = Neff | first2 = N. F. | last3 = Quake | first3 = S. R. | doi = 10.1038/nbt.1561 | title = Single-molecule sequencing of an individual human genome | journal = Nature Biotechnology | volume = 27 | issue = 9 | pages = 847–850 | year = 2009 | pmid =  19668243| pmc = }}</ref> That same year, [[Complete Genomics]] debuted its proprietary [[Full genome sequencing|whole-genome sequencing]] service for researchers, charging as little as $5,000/genome for bulk orders.<ref>Emily Singer. [http://www.technologyreview.com/biomedicine/21466/ ''Technology Review''. 2008].</ref>

In 2010, [[Illumina (company)]] introduced its individual genome sequencing service for consumers, who were required to present a doctor's note. The initial price was $50,000/person. One of the first clients was former [[Solexa]] CEO John West, who had his entire family of four sequenced.<ref>{{Cite journal | last1 = Dewey | first1 = F. E. | last2 = Chen | first2 = R. | last3 = Cordero | first3 = S. P. | last4 = Ormond | first4 = K. E. | last5 = Caleshu | first5 = C. | last6 = Karczewski | first6 = K. J. | last7 = Whirl-Carrillo | first7 = M. | last8 = Wheeler | first8 = M. T. | last9 = Dudley | first9 = J. T. | last10 = Byrnes | first10 = J. K.| last11 = Cornejo | first11 = O. E.| last12 = Knowles | first12 = J. W.| last13 = Woon | first13 = M.| last14 = Sangkuhl | first14 = K.| last15 = Gong | first15 = L.| last16 = Thorn | first16 = C. F.| last17 = Hebert | first17 = J. M.| last18 = Capriotti | first18 = E.| last19 = David | first19 = S. P.| last20 = Pavlovic | first20 = A.| last21 = West | first21 = A.| last22 = Thakuria | first22 = J. V.| last23 = Ball | first23 = M. P.| last24 = Zaranek | first24 = A. W.| last25 = Rehm | first25 = H. L.| last26 = Church | first26 = G. M.| last27 = West | first27 = J. S.| last28 = Bustamante | first28 = C. D.| last29 = Snyder | first29 = M.| last30 = Altman | first30 = R. B.| last31 = Klein | first31 = T. E.| last32 = Butte | first32 = A. J.| last33 = Ashley | first33 = E. A.| title = Phased Whole-Genome Genetic Risk in a Family Quartet Using a Major Allele Reference Sequence | doi = 10.1371/journal.pgen.1002280 | journal = PLoS Genetics | volume = 7 | issue = 9 | pages = e1002280 | year = 2011 | pmid =  21935354| pmc =3174201 }}</ref>

In January 2012, [[Life Technologies (Thermo Fisher Scientific)|Life Technologies]] unveiled a new sequencing instrument, the Ion Proton Sequencer, which it said would achieve the $1,000 genome in a day within 12 months.<ref>{{Cite journal 
| last1 = Defrancesco | first1 = L. 
| title = Life Technologies promises $1,000 genome 
| doi = 10.1038/nbt0212-126a 
| journal = Nature Biotechnology 
| volume = 30 
| issue = 2 
| pages = 126 
| year = 2012 
| pmid = 22318022 
| pmc = 
}}</ref> [[Sharon Begley]] wrote: “After years of predictions that the ‘$1,000 genome’ -- a read-out of a person's complete genetic information for about the cost of a dental crown—was just around the corner, a U.S. company is announcing... that it has achieved that milestone.”<ref>Sharon Begley. [http://www.reuters.com/article/2012/01/10/us-dna-reader-idUSTRE8090B820120110 "Insight: New DNA Reader to Bring Promise." Reuters.com. 10 January 2012].</ref>

In January 2014, [[Illumina (company)]] launched its HiSeq X Ten Sequencer which delivers the first $1,000 genome at 30x coverage,<ref>[http://www.illumina.com/systems/hiseq-x-sequencing-system.ilmn HiSeq X Ten and HiSeq X Five Systems<!-- Bot generated title -->]</ref> including reagent costs ($797), instrument depreciation ($137 per genome), and sample preparation ($55–$65 per genome) amortised over 18,000 genomes sequenced per year over a four-year operational period .<ref>[http://www.nature.com/news/is-the-1-000-genome-for-real-1.14530 Is the $1,000 genome for real? : Nature News & Comment<!-- Bot generated title -->]</ref> The first three institutions in the world to purchase the X Ten machines and become capable of sequencing the $1,000 genome were the [[Garvan Institute of Medical Research]] in [[Sydney]], [[Australia]]; [[Broad Institute|The Broad Institute]] in [[Cambridge]], [[Massachusetts]], [[United States]]; Macrogen in Rockville, [[Maryland]], United States.<ref>{{Cite press release|title = Illumina Introduces the HiSeq X™ Ten Sequencing System|publisher = Illumina|date = January 14, 2014|url = http://investor.illumina.com/phoenix.zhtml?c=121127&p=irol-newsArticle&ID=1890696|access-date = 2015-07-14}}</ref>

In September 2015, Veritas Genetics (co-founded by [[George M. Church|George Church]]) announced $1000 full-genome sequencing including interpretation for participants in the [[Personal Genome Project]].<ref>{{Cite press release|title=Veritas Genetics Breaks $1,000 Whole Genome Barrier |url=http://www.prnewswire.com/news-releases/veritas-genetics-breaks-1000-whole-genome-barrier-300150585.html|publisher=PR Newswire|date=2015-09-29}}</ref><ref>{{Cite web|title=Your Full Genome Can Be Sequenced and Analyzed For Just $1,000|url=http://www.popsci.com/cost-full-genome-sequencing-drops-to-1000|publisher=PopSci|date=2015-10-01|author=Alexandra Ossola}}</ref><ref>{{Cite web|title=VERITAS GENETICS BREAKS $1,000 WHOLE GENOME BARRIER |url=http://www.veritasgenetics.com/news.html|publisher=CerconeBrown|date=2015-09-25|author=Margaret Jackson}}</ref>

==Archon Genomics X PRIZE==

It was originally announced that the revamped Archon Genomics X PRIZE presented by Medco would hold a $10-million grand prize competition in January 2013 for the team that reaches (or comes closest to reaching) the $1,000 genome. The grand prize would go to "the team(s) able to sequence 100 human genomes within 30 days to an accuracy of 1 error per 1,000,000 bases, with 98% completeness, identification of insertions, deletions and rearrangements, and a complete haplotype, at an audited total cost of $1,000 per genome."<ref>{{Cite journal | last1 = Kedes | first1 = L. | last2 = Campany | first2 = G. | doi = 10.1038/ng.988 | title = The new date, new format, new goals and new sponsor of the Archon Genomics X PRIZE Competition | journal = Nature Genetics | volume = 43 | issue = 11 | pages = 1055–1058 | year = 2011 | pmid =  22030612| pmc = }}</ref>

In August 2013 the Archon Genomics X PRIZE was cancelled, as the founders felt it had been "Outpaced by Innovation," and "was not incentivizing the technological changes" <ref>[http://www.nbcnews.com/science/10-million-genomics-x-prize-canceled-outpaced-innovation-8C10990757 nbcnews]</ref>

== References ==
{{Reflist|30em}}

== Further reading ==
* Misha Angrist. ''Here is a Human Being.'' (New York: HarperCollins, 2010). ISBN 0-06-162833-6
* Kevin Davies. ''The $1,000 Genome.'' (New York: Free Press, 2010). ISBN 1-4165-6959-6
* Lone Frank. ''My Beautiful Genome.'' (London: Oneworld, 2011). ISBN 978-1-85168-833-3

== Additional Resources ==
Webcast of James Watson personal genome presentation, 31 May 2007. http://www.bcm.edu/news/packages/watson_genome.cfm

{{DEFAULTSORT:1000 Dollar genome}}
[[Category:Articles created via the Article Wizard]]
[[Category:Genomics]]