{{other uses}}
{{Infobox book
| name = Mardi
| image = Mardi, and a Voyage Thither.jpg
| caption = First edition title page
| alt = 
| author = [[Herman Melville]]
| illustrator = 
| cover_artist = 
| country = United States, England
| language = English
| subject = 
| genre = [[Romance novel|Romance literature]]
| published = {{Plainlist|
* 1849 {{small|(New York: Harper & Brothers)}}
* 1849 {{small|(London: Richard Bentley)}}
}}
| media_type = Print
| awards = 
| isbn = 
| oclc = 
| dewey = 
| congress = 
| preceded_by = [[Omoo]]
| followed_by = [[Redburn]]
}}

'''''Mardi, and a Voyage Thither''''' is the third book by American writer [[Herman Melville]], first published in London in 1849. Beginning as a travelogue in the vein of the author's two previous efforts, the adventure story gives way to a romance story, which in its turn gives way to a philosophical quest.

==Overview==
''Mardi'' is Melville's first pure fiction work (while featuring fictional narrators; his previous novels were heavily autobiographical). It details (much like ''[[Typee]]'' and ''[[Omoo]])'' the travelings of an American sailor who abandons his whaling vessel to explore the [[Pacific Ocean|South Pacific]]. Unlike the first two, however, ''Mardi'' is highly philosophical and said to be the first work to show Melville's true potential. The tale begins as a simple narrative, but quickly focuses upon discourse between the main characters and their interactions with the different symbolic countries they encounter. While not as cohesive or lengthy as ''[[Moby-Dick]]'', it shares a similar writing style as well as many of the same themes.

As a preface to ''Mardi'', Melville wrote somewhat ironically that his first two books were nonfiction but disbelieved; by the same pattern he hoped the fiction book would be accepted as fact.

== Style ==

=== Influence of Rabelais and Swift ===
The voyage from island to island echoes [[Rabelais]]'s [[Gargantua and Pantagruel]], especially the last two books. According to scholar [[Newton Arvin]], "The praise of eating and drinking is highly Rabelaisian in intention, and so in general is all the satire on bigotry, dogmatism, and pedantry. Taji and his friends wandering about on the island of Maramma, which stands for ecclesiastical tyranny and dogmatism, are bound to recall Pantagruel and his companions wandering among the superstitious inhabitants of Papimany; and the pedantic, pseudo-philosophi of Melville's Doxodox is surely, for a reader of Rabelais, an echo of the style of Master Janotus de Bragmardo holding forth polysyllabically to Gargantua in Book I."<ref>Arvin (1950), chapter "The Enviable Isles", online, [no page numbers]</ref> Arvin also recognizes the influence of ''[[Gulliver's Travels]]'' by [[Jonathan Swift]], "there is something very Swiftian in Melville's Hooloomooloo, the Isle of Cripples, the inhabitants of which are all twisted and deformed, and whose shapeless king is horrified at the straight, strong figures of his visitors from over sea."<ref name="Arvin 1950, online">Arvin (1950), online</ref>

== Structure ==
The emotional center of the book, Arvin writes, is the relation between Taji and Yillah, the "I" and the mysterious blonde who disappears as suddenly as she appeared. Taji begins a quest for her throughout the islands without finding her. Though Arvin finds the allegory of Yillah "too tenuous and too pretty to be anything but an artistic miscarriage" in the poetic sense, he also finds it "extremely revealing" in connection with the whole Melville canon. Yillah, associated with the lily in the language of flowers, is "an embodiment of the pure, innocent, essentially sexless happiness", and Hautia, "symbolized by the dahlia", embodies "the sensual, the carnal, the engrossingly sexual". The middle portion of the book is taken up by "a series of forays in social and political satire, and by quasi-metaphysical speculations" that are, if at all, at best "only loosely and uncertainly related to the quest for Yillah". The only way to perceive any fabric holding the book together, Arvin feels, is by recognizing "a certain congruity among the various more or less frustrated quests it dramatizes--the quest for an emotional security once possessed, the quest for a just and happy sociality once too easily assumed possible, and the quest for an absolute and transcendent Truth once imagined to exist and still longed for."<ref>Arvin (1950), online [chapter "The Enviable Isles"]</ref>

== Themes ==
For Arvin, in ''Mardi'' Melville rejects not "the profounder moralities of democracy" so much as "a cluster of delusions and inessentials" that Americans have come to regard as somehow connected to the idea of democracy. Arvin recognizes three delusions to the cluster:
* "that political and social freedom is an ultimate good, however empty of content;
* that equality should be a literal fact as well as a spiritual ideal;
* that physical and moral evil are rapidly receding before the footsteps of Progress."<ref name="Arvin 1950, online"/>

The philosophical plot, Arvin believes, is furnished by the interaction between the intense longing for certainty, and the suspicion that on the great fundamental questions, "final, last thoughts you mortals have none; nor can have."<ref name="Quoted in Arvin 1950, online">Quoted in Arvin (1950), online</ref> And even while one of the characters says, "Faith is to the thoughtless, doubts to the thinker", Arvin feels that Melville struggles to avoid a brutality of what Melville himself calls "indiscriminate skepticism", and he got closest to expressing "his basic thought" in Babbalanja's speech in the dark: "Be it enough for us to know that Oro"—God--"indubitably is. My lord! my lord! sick with the spectacle of the madness of men, and broken with spontaneous doubts, I sometimes see but two things in all Mardi to believe:--that I myself exist, and that I can most happily, or least miserably exist, by the practice of righteousness."<ref name="Quoted in Arvin 1950, online"/>

==Reception==

===Contemporary reviews ===
''Mardi'' was a critical failure. One reviewer said the book contained "ideas in so thick a haze that we are unable to perceive distinctly which is which".<ref name=Miller246>Miller, Perry. ''The Raven and the Whale: The War of Words and Wits in the Era of Poe and Melville''. New York: Harvest Book, 1956: 246.</ref> Nevertheless, [[Nathaniel Parker Willis]] found the work "exquisite".<ref name=Miller246/>

[[Nathaniel Hawthorne]] found ''Mardi'' a rich book "with depths here and there that compel a man to swim for his life... so good that one scarcely pardons the writer for not having brooded long over it, so as to make it a great deal better."<ref name="Parker">{{cite book|last=Parker|first=Hershel|title=Herman Melville: A Biography, 1819-1851|year=1996|publisher=Johns Hopkins University Press|isbn=0-8018-5428-8|pages=768|url=https://books.google.com/books?id=pFB1UOHRlvYC&dq=herman+melville+hershel+parker}}</ref>

The widespread disappointment of the critics hurt Melville yet he chose to view the book's reception philosophically, as the requisite growing pains of any author with high literary ambitions. "These attacks are matters of course, and are essential to the building up of any permanent reputation—if such would ever prove to be mine... But Time, which is the solver of all riddles, will solve ''Mardi''."

=== Later critical history ===
In the description of Arvin, "the thoughts and feelings he was attempting to express in Mardi were too disparate among themselves and often too incongruous with his South Sea imagery to be capable of fusion into a satisfying artistic whole. In the rush and press of creative excitement that swept upon him in these months, Melville was trying to compose three or four books simultaneously: he failed, in the strict sense, to compose even one. Mardi has several centers, and the result is not a balanced design. There is an emotional center, an intellectual center, a social and political center, and though they are by no means utterly unrelated to each other, they do not occupy the same point in space."<ref name="Arvin 1950, online"/>

==References==
{{reflist}}

== Sources ==
* [[Newton Arvin|Arvin, Newton]] (1950). [http://andromeda.rutgers.edu/~ehrlich/361/melville_arvin-bio.html ''Herman Melville''.]

==External links==
{{wikisource}}
; Online versions
* [http://www.gutenberg.org/etext/13720 ''Mardi'' (1864 reprint, vol. 1 of 2)] at [[Project Gutenberg]]
* [http://www.gutenberg.org/ebooks/13721 ''Mardi'' (1864 reprint, vol. 2 of 2)] at [[Project Gutenberg]]
<!-- These are the original Gutenberg free files, please don't replace them with commercial mirror sites -->
* {{librivox book | title=Mardi | author=Herman Melville}}

{{Herman Melville}}

[[Category:1849 novels]]
[[Category:Novels by Herman Melville]]
[[Category:19th-century American novels]]
[[Category:Novels set in Oceania]]
[[Category:Novels republished in the Library of America]]