{{Infobox airport
| name         = Andøya Airport, Andenes
| nativename   = Andøya lufthavn, Andenes
| nativename-a =
| nativename-r =
| image        =
| image-width  =
| caption      =
| IATA         = ANX
| ICAO         = ENAN
| type         = Joint (public and military)
| owner        =
| operator     = [[Avinor]]
| city-served  =
| location     = [[Andenes]], [[Andøy]], [[Norway]]
| elevation-f  = 43
| elevation-m  = 13
| website      = [https://avinor.no/en/airport/andoya-airport/ Official website]
| coordinates  = {{coord|69|17|33|N|16|08|39|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_label          = '''ANX'''
| pushpin_label_position = bottom
| pushpin_mapsize        = 300
| pushpin_map_caption    = Map of Norway
| metric-elev  = Y
| metric-rwy   = Y
| r1-number    = 03/21
| r1-length-f  = 5,486
| r1-length-m  = 1,672
| r1-surface   = Asphalt
| r2-number    = 14/32
| r2-length-f  = 8,097
| r2-length-m  = 2,468
| r2-surface   = Asphalt
| stat-year    = 2014
| stat1-header = Passengers
| stat1-data   = 48,625
| stat2-header = Aircraft movements
| stat2-data   = 3,208
| stat3-header = Cargo (tonnes)
| stat3-data   = 1
| footnotes    = Source:<ref name=aip>{{cite web |title=ENAN – Andenes/Andøya |publisher=Avinor |url=https://www.ippc.no/norway_aip/current/AIP/AD/ENAN/EN_AD_2_ENAN_en.pdf |accessdate=9 April 2012}}</ref><ref name=pax />
}}

'''Andøya Airport, Andenes''' ({{lang-no|Andøya lufthavn, Andenes}}; {{Airport codes|ANX|ENAN|p=n}}) is a domestic airport located at [[Andenes]] in [[Andøy]], [[Norway]], on the northern tip of the island of [[Andøya]]. The airport is the civilian sector of [[Andøya Air Station]] and is operated by the state-owned [[Avinor]]. The airport consists of two runways, {{convert|2468|and|1672|m|sp=us}} long, and served 48,254 passengers in 2012. [[Widerøe]] operates [[public service obligation]] (PSO) flights to [[Bodø]], [[Tromsø]], [[Stokmarknes]] and [[Harstad]]/[[Narvik]], while [[Norwegian Air Shuttle]] operates seasonal flights to [[Oslo]].

Construction of the air station started in 1952 to host the [[No. 333 Squadron RNoAF|333 Squadron]]. Civilian operations started in 1964, when [[Scandinavian Airlines System]] (SAS) started flights to Oslo. [[Widerøe]] started serving the airport as part of the regional network in 1972, with SAS withdrawing four years later. Widerøe originally used the [[Twin Otter]], replacing it with the [[Dash 7]] from 1981 and the Dash 8 between 1993 and 1995. Routes have been subject to PSO flights since 1997; these have been operated by [[Norwegian Air Shuttle]] for part of 2003 and by [[Coast Air]] for part of 2006 og 2007, and otherwise by Widerøe.

==History==
Andøya Air Station was built with [[North Atlantic Treaty Organization]] funds as a combined [[Supreme Allied Commander Europe]] and [[Supreme Allied Commander Atlantic]] project as a base for maritime surveillance. Construction started in 1952 and all installations not required to be physically located at the air station were placed at Skarsteindalen, {{convert|12|km|sp=us|0}} away. Andøya became the base for the 333 Squadron, which initially operated the [[Grumman HU-16 Albatross]]. These were replaced by the [[Lockheed P-3 Orion]] in 1969.<ref>Arheim: 219</ref> Construction and further expansion of the air station resulted in the villages of [[Haugnes]] being [[eminent domain|expropriated]]. Construction was prohibited from 1953, but the expropriation was not carried out until 1971.<ref>{{cite web|url=http://www.digitaltfortalt.no/show_single.aspx?art_id=122262 |title=Haugnes, bygda som forsvant ut av kartet |last=Heide |first=Beate |date=9 September 2011 |publisher=Digitalt Fortalt |language=Norwegian |accessdate=27 February 2013 |archivedate=27 February 2013 |archiveurl=http://www.webcitation.org/6EkBt813M?url=http%3A%2F%2Fwww.digitaltfortalt.no%2Fshow_single.aspx%3Fart_id%3D122262 |deadurl=no |df= }}</ref>

[[File:Hangars and soldiers at Andøya.jpg|thumb|left|Soldiers in front of a hangar of [[Andøya Air Station]]]]
With the establishment of the air station, plans arose to take advantage of the infrastructure for civilian flights. The airport's location made it suitable to serve [[Vesterålen]], although it was located at the northern tip of the archipelago. SAS started serving Andenes from 1964 with their 56-seat [[Convair Metropolitan]]s, which consisted of three night flights with intermediate stops at [[Bodø Airport]] and [[Bardufoss Airport]] before continuing to [[Oslo Airport, Fornebu]]. The night flights caused difficulties corresponding with the flights as there was a limited ferry and bus service during night.<ref name=s662>Svanberg: 662</ref> The [[Sud Aviation Caravelle]] was introduced on Northern Norway flights from 1965 and later these were replaced with the [[Douglas DC-9]].<ref>Svanberg: 663</ref>

It soon proved difficult to provide sufficient patronage to keep operations profitable. With the opening of a network of [[short take-off and landing]] airports elsewhere in [[Central Hålogaland]] in 1972, Widerøe started serving the airport using the de Havilland Canada Twin Otter. SAS retained a small number of Oslo flights until 1976, when they withdrew from the service.<ref name=s662 /> At the time the military chartered aircraft for their own needs, so Andøy Mayor Johan Kleppe took initiative to coordinate the civilian and military routes, without the military supporting the proposal.<ref>Svanberg: 675</ref>

Parliament decided in 1982 that Andenes would be the base for operating helicopters [[offshore drilling|offshore]] to oil installations off [[Troms]]. However, when operations commenced operations flew out of [[Tromsø Airport]] because of the difficult weather conditions at Andenes. Operations were moved to [[Hammerfest Airport]] following the discovery of [[Snøhvit]].<ref name=gynnild>{{cite web |url=http://avinor.moses.no/index.php?seks_id=135&element=kapittel&k=2 |title=Flyplassenes og flytrafikkens historie |work=Kulturminner på norske lufthavner – Landsverneplan for Avinor |publisher=[[Avinor]] |last=Gynnild |first=Olav |year=2009 |language=Norwegian |accessdate=25 January 2012 |archivedate=25 January 2013 |archiveurl=http://www.webcitation.org/6DwQ1MXLE?url=http://avinor.moses.no/index.php?seks_id%3D135%26element%3Dkapittel%26k%3D2 |deadurl=no}}</ref> Widerøe introduced the larger, 50-seat de Havilland Canada Dash 7 in 1981,<ref>Svanberg: 677</ref> followed by the Dash 8 between 1993 and 1995.<ref name=gynnild /> The routes were made subject to public service obligations with the [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]] from 1 April 1997. Widerøe won the initial tender.<ref>{{cite news |title=Widerøe-monopol |last=Tuv |first=Kirsten |work=[[Dagens Næringsliv]] |date=2 November 1996 |page=10 |language=Norwegian}}</ref> A proposal to make Andenes an [[international airport]] was launched in 2000, which also proposed that the airport change its name to Lofoten Airport. Unlike the two airports in [[Lofoten]], [[Leknes Airport]] and [[Svolvær Airport, Helle]], Andenes is able to handle [[Jet airliner|jetliner]]s. The name change was rejected by interests in Lofoten, which accused Vesterålen of stealing the more well-known Lofoten name.<ref>{{cite news |title=Vesterålen stjeler Lofoten |last=Fonbæk |first=Dag |work=[[Verdens Gang]] |date=20 October 2000 |language=Norwegian}}</ref>

From 1 April 2003 [[Norwegian Air Shuttle]] took over the route from Andenes to Bodø and Tromsø. However, they decided to terminate their operations with the [[Fokker 50]], and thus also serving Andenes, to concentrate their efforts on becoming a [[low-cost carrier]].<ref>{{cite news |title=Kortbaneruter i nord utlyses på nytt |agency=[[Norwegian News Agency]] |date=10 April 2003 |language=Norwegian}}</ref> From 1 January 2004 the route was served by Widerøe, who won the extraordinary tender. The state paid NOK&nbsp;68 million for 27 months, up 25 percent from the Norwegian bid, including a service from Tromsø to [[Lakselv Airport, Banak]].<ref>{{cite news |title=Widerøe overtar NAS-rutene i nord |agency=[[Norwegian News Agency]] |date=15 July 2003 |language=Norwegian}}</ref> The tender for the three years starting on 1 April 2006 was won by [[Coast Air]],<ref>{{cite news |title=Coast Air "konsesjonsvinner" i ny kortbaneperiode |last=Lillesund |first=Geir  |agency=[[Norwegian News Agency]] |date=2 November 2005 |language=Norwegian}}</ref> which used an [[ATR-42]] on the route.<ref>{{cite news |title=Coast Air flyr høyere |last=Førde |first=Thomas |date=5 November 2005 |page=6 |work=[[Stavanger Aftenblad]] |language=Norwegian}}</ref> Coast Air was not able to make money on the route and abandoned the PSO contract from 1 April 2007. Widerøe won the subsequent tender and started flights from that date.<ref>{{cite news |title=Widerøe erstatter Coast Air i nord |agency=[[Norwegian News Agency]] |date=2 February 2007 |language=Norwegian}}</ref> Norwegian started irregular scheduled flights to [[Oslo Airport, Gardermoen]], using the [[Boeing 737 Next Generation|Boeing 737-800]] from June 2012.<ref name=berger>{{cite news |title=Alt klart for direkteflyet fra Oslo |last=Amundsen |first=Mette H. Berger |date=23 June 2012 |pages=4–5 |work=[[Andøyposten]] |language=Norwegian}}</ref>

==Facilities==
[[File:Snowplow at Andøya Airport.jpg|thumb|Snowplow]]

The airport is located at an elevation of 13&nbsp;meters (43&nbsp;ft). It has two asphalt runways: the main runway is {{convert|3002|by|45|m|sp=us}} and aligned 14–32, but only has a declared length of {{convert|2467|m|sp=us}}; the other is {{convert|1671|by|45|m|sp=us}} and aligned 03–21. Runways 14 and 32 are equipped with [[instrument landing system]] category I.<ref name=aip /> The airport is located two minutes' drive from Andenes. Taxis, car rental and 40 paid parking spaces are available at the airport.<ref>{{cite web |url=http://www.avinor.no/en/airport/andoya/tofromairport |title=Getting to and from the airport |publisher=[[Avinor]] |accessdate=28 February 2013}}</ref> The airport had an operating deficit of 34 million [[Norwegian krone]] in 2012.<ref>{{cite news |url=http://www.nrk.no/mr/7-av-46-flyplasser-med-overskudd-1.10970207 |title=Kun 7 av 46 flyplasser gikk med overskudd i 2012 |publisher=[[Norwegian Broadcasting Corporation]] |last=Riise |first=Ivar Lid |language=Norwegian |date=2 April 2013 |accessdate=13 January 2015}}</ref>

==Airlines and destinations==
[[Widerøe]] serves Andenes with [[Bombardier Dash 8|Dash 8]] aircraft on public service obligations with the Ministry of Transport and Communications.<ref name=ntp>{{cite web|url=http://www.avinor.no/tridionimages/NTP%202014%20-%202023%20Avinor%20Hovedrapport_tcm181-141202.pdf |title=Nasjonal transportplan 2014–2023: Framtidsrettet utvikling av lufthavnstrukturen |publisher=[[Avinor]] |date=15 February 2012 |language=Norwegian |accessdate=17 February 2013 |archivedate=28 February 2013 |deadurl=no |archiveurl=http://www.webcitation.org/6ElaccQdF?url=http%3A%2F%2Fwww.avinor.no%2Ftridionimages%2FNTP%25202014%2520-%25202023%2520Avinor%2520Hovedrapport_tcm181-141202.pdf |page=38 |df= }}</ref> These routes are mixed with commercial services and operate to Bodø, Harstad/Narvik, Stokmarknes and Tromsø.<ref>{{cite web |url=http://www.avinor.no/en/airport/andoya/timetables |title=Destinations from Andøya Airport, Andenes |publisher=[[Avinor]] |accessdate=28 February 2013}}</ref> Norwegian Air Shuttle flies irregularly to Oslo, typically during holidays and summer.<ref name=berger /> The airport handled 48,625 passengers, 3,208 aircraft movements and 1 tonne of cargo in 2014.<ref name=pax>{{cite web |url=http://www.mynewsdesk.com/material/document/42080/download?resource_type=resource_document |title=Månedsrapport |publisher=[[Avinor]] |format=XLS |accessdate=13 January 2015 |year=2015}}</ref>

{{Airport-dest-list
| [[Norwegian Air Shuttle]] | '''Seasonal:''' [[Oslo-Gardermoen]]
| [[Widerøe]] | [[Bodø Airport|Bodø]], [[Harstad/Narvik Airport, Evenes|Harstad/Narvik]], [[Stokmarknes Airport, Skagen|Stokmarknes]], [[Svolvær Airport, Helle|Svolvær]] (begins 1 April 2017),<ref name='''Airlineroute'''>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/271208/widere-adds-new-sectors-in-s17/|title=Widerøe adds new sectors in S17|publisher='''Airlineroute'''|date=3 February 2017|accessdate=3 February 2017}}</ref> [[Tromsø Airport|Tromsø]]
}}

==Accidents and incidents==
All four people on board were killed when a private [[Cessna 172]] crashed west of the airport just after take-off on 31 July 1988. The accident took place {{convert|4.5|km|sp=us}} west of the airport in a cliff, {{convert|250|m|sp=us}} altitude.<ref>{{cite news |title=Fire omkom da privatfly styrtet |last=Guhnfeldt |first=Cato |authorlink=Cato Guhnfeldt |date=1 August 1988 |page=4 |work=[[Aftenposten]] |language=Norwegian}}</ref>

==References==
{{reflist|30em}}

===Bibliography===
{{commons category|Andøya Airport, Andenes}}
* {{cite book |last=Arheim |first=Tom |last2=Hafsten |first2=Bjørn |last3=Olsen |first3=Bjørn |last4=Thuve |first4=Sverre |title=Fra Spitfire til F-16: Luftforsvaret 50 år 1944–1994 |location=Oslo |publisher=Sem & Stenersen |year=1994 |language=Norwegian |isbn=82-7046-068-0}}
* {{cite book |last=Svanberg |first=Erling |title=Langs vei og lei i Nordland: samferdsel i Nordland gjennom 3000 år |year=1990 |publisher=Nordland County Municipality |language=Norwegian |isbn=82-7416-021-5 |url=http://urn.nb.no/URN:NBN:no-nb_digibok_2010110308003}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway|Arctic}}

{{DEFAULTSORT:Andoya Airport, Andenes}}
[[Category:Airports in Nordland]]
[[Category:Airports in the Arctic]]
[[Category:Avinor airports]]
[[Category:Andøy]]
[[Category:1964 establishments in Norway]]
[[Category:Airports established in 1964]]