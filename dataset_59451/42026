{{Infobox protein family
| Symbol = RVT_1
| Name = Reverse transcriptase<br/>(RNA-dependent DNA polymerase)
| image = Reverse transcriptase 3KLF labels.png
| width =
| caption = Crystallographic structure of [[HIV]]-1 reverse transcriptase where the two subunits p51 and p66 are colored and the active sites of polymerase and nuclease are highlighted.<ref name="pmid20852643">{{PDB|3KLF}}; {{cite journal |vauthors=Tu X, Das K, Han Q, Bauman JD, Clark AD, Hou X, Frenkel YV, Gaffney BL, Jones RA, Boyer PL, Hughes SH, Sarafianos SG, Arnold E | title =Structural basis of HIV-1 resistance to AZT by excision. | journal = Nat. Struct. Mol. Biol. | volume = 17 | issue = 10 | pages = 1202–9 |date=September 2010 | pmid = 20852643 | pmc = 2987654 | doi = 10.1038/nsmb.1908 | url =  }}</ref>
| Pfam = PF00078
| Pfam_clan = CL0027
| InterPro = IPR000477
| SMART =
| PROSITE = PS50878
| SCOP = 1hmv
| TCDB =
| OPM family =
| OPM protein =
| CDD = cd00304
}}
{{enzyme
| Name = RNA-directed DNA polymerase
| EC_number = 2.7.7.49
| CAS_number = 9068-38-6
| IUBMB_EC_number = 2/7/7/49
| GO_code = 0003964
| image =
| width =
| caption =
}}

A '''reverse transcriptase''' (RT) is an [[enzyme]] used to generate [[complementary DNA]] (cDNA) from an [[RNA]] template, a process termed ''[[reverse transcription]]''. It is mainly associated with [[retrovirus]]es. However, non-retroviruses also use RT (for example, the [[hepatitis B virus]], a member of the [[Hepadnaviridae]], which are [[dsDNA-RT virus]]es, while retroviruses are ssRNA viruses). RT inhibitors are widely used as [[Management of HIV/AIDS|antiretroviral drugs]]. RT activities are also associated with the replication of chromosome ends ([[telomerase]]) and some mobile genetic elements ([[retrotransposons]]).

Retroviral RT has three sequential biochemical activities: 
*(a) RNA-dependent DNA polymerase activity, 
*(b) [[RNase H|ribonuclease H]], and 
*(c) DNA-dependent DNA polymerase activity. 
These activities are used by the retrovirus to convert single-stranded genomic RNA into double-stranded cDNA which can integrate into the host genome, potentially generating a long-term infection that can be very difficult to eradicate.  The same sequence of reactions is widely used in the laboratory to convert RNA to DNA for use in [[molecular cloning]], [[RNA sequencing]], [[polymerase chain reaction]] (PCR), or [[DNA microarray|genome analysis]].

Well studied reverse transcriptases include:
* HIV-1 reverse transcriptase from [[HIV|human immunodeficiency virus]] type 1 ({{PDB|1HMV}}) has two subunits, which have respective molecular weights of 66 and 51 [[kilodalton|kDa]].<ref>{{cite journal|last1=Ferris|first1=AL|last2=Hizi|first2=A|last3=Showalter|first3=SD|last4=Pichuantes|first4=S|last5=Babe|first5=L|last6=Craik|first6=CS|last7=Hughes|first7=SH|title=Immunologic and proteolytic analysis of HIV-1 reverse transcriptase structure.|journal=Virology|date=April 1990|volume=175|issue=2|pages=456–64|pmid=1691562|url=http://www.craiklab.ucsf.edu/docs/pub47.pdf|doi=10.1016/0042-6822(90)90430-y}}</ref>
* M-MLV reverse transcriptase from the [[murine leukemia virus|Moloney murine leukemia virus]] is a single 75 kDa monomer.<ref name="Konishi_2012">{{cite journal |vauthors=Konishi A, Yasukawa K, Inouye K | title = Improving the thermal stability of avian myeloblastosis virus reverse transcriptase α-subunit by site-directed mutagenesis | journal = Biotechnol. Lett. | volume = 34 | issue = 7 | pages = 1209–15 | year = 2012 | pmid = 22426840 | doi = 10.1007/s10529-012-0904-9 | url =  }}</ref>
* AMV reverse transcriptase from the [[Alpharetrovirus|avian myeloblastosis virus]] also has two subunits, a 63 kDa subunit and a 95 kDa subunit.<ref name="Konishi_2012"/>
* [[Telomerase reverse transcriptase]] that maintains the [[telomere]]s of [[eukaryotic]] [[chromosomes]].

== History ==
Reverse transcriptases were discovered by [[Howard Temin]] at the [[University of Wisconsin–Madison]] in [[Rous sarcoma virus|RSV virions]]<ref name="pmid4316301">{{cite journal |authors=Temin H. M., Mizutani S. | title = RNA-dependent DNA polymerase in virions of Rous sarcoma virus | journal = Nature | volume = 226 | issue = 5252 | pages = 1211–3 | date = June 1970 | pmid = 4316301 | doi = 10.1038/2261211a0 }}</ref> and independently isolated by [[David Baltimore]] in 1970 at [[MIT]] from two RNA tumour viruses: [[Murine leukemia virus|R-MLV]] and again [[Rous sarcoma virus|RSV]].<ref name="pmid4316300">{{cite journal | author = Baltimore D. | title = RNA-dependent DNA polymerase in virions of RNA tumour viruses | journal = Nature | volume = 226 | issue = 5252 | pages = 1209–11 | date = June 1970 | pmid = 4316300 | doi = 10.1038/2261209a0 }}</ref> For their achievements, both shared the 1975 [[Nobel Prize in Physiology or Medicine]] (with [[Renato Dulbecco]]).

The idea of reverse transcription was very unpopular at first, as it contradicted the [[central dogma of molecular biology]], which states that DNA is transcribed into RNA, which is then translated into proteins. However, in 1970, when the scientists [[Howard Temin]] and [[David Baltimore]] both independently discovered the [[enzyme]] responsible for reverse transcription, named reverse transcriptase, the possibility that genetic information could be passed on in this manner was finally accepted.<ref name="pmid5422595">{{cite journal | title = Central dogma reversed | journal = Nature | volume = 226 | issue = 5252 | pages = 1198–9 | date = June 1970 | pmid = 5422595 | doi = 10.1038/2261198a0 }}</ref>

== Function in viruses ==
[[File:HIV-1 Reverse Transcriptase with Active Sites.png|thumb|left|Reverse transcriptase is shown with its finger, palm, and thumb regions. The catalytic [[amino acid]]s of the [[RNase H]] active site and the [[polymerase]] active site are shown in ball-and-stick form.]]
The enzymes are encoded and used by [[reverse-transcribing virus]]es, which use the enzyme during the process of replication. Reverse-transcribing [[RNA virus]]es, such as [[retrovirus]]es, use the enzyme to reverse-transcribe their RNA [[genome]]s into DNA, which is then integrated into the host genome and replicated along with it. Reverse-transcribing [[DNA virus]]es, such as the [[hepadnavirus]]es, can allow RNA to serve as a template in assembling and making DNA strands. HIV infects humans with the use of this enzyme. Without reverse transcriptase, the viral genome would not be able to incorporate into the host cell, resulting in failure to replicate.

=== Process of reverse transcription ===
Reverse transcriptase creates single-stranded DNA from an RNA template.

In virus species with reverse transcriptase lacking DNA-dependent DNA polymerase activity, creation of double-stranded DNA can possibly be done by host-encoded [[DNA polymerase δ]], mistaking the viral DNA-RNA for a primer and synthesizing a double-stranded DNA by similar mechanism as in [[Primer (molecular biology)#Primer removal|primer removal]], where the newly synthesized DNA displaces the original RNA template.

The process of reverse transcription is extremely error-prone, and it is during this step that mutations may occur. Such mutations may cause [[Resistance to antiviral drugs|drug resistance]].

==== Retroviral reverse transcription ====
[[Image:Reverse transcription.svg|thumb|300px]]
[[Retroviruses]], also referred to as class VI [[ssRNA-RT]] viruses, are RNA reverse-transcribing viruses with a DNA intermediate. Their genomes consist of two molecules of [[sense (molecular biology)#RNA sense in viruses|positive-sense]] single-stranded RNA with a [[5' cap]] and [[Polyadenylation|3' polyadenylated tail]]. Examples of retroviruses include the human immunodeficiency virus ([[HIV]]) and the human T-lymphotropic virus ([[HTLV]]). Creation of double-stranded DNA occurs in the [[cytosol]]<ref>[http://www.bio-medicine.org/biology-definition/Retrovirus/ Bio-Medicine.org - Retrovirus] Retrieved on 17 Feb, 2009</ref> as a series of these steps:
#A specific cellular [[tRNA]] acts as a primer and hybridizes to a complementary part of the virus RNA genome called the primer binding site or PBS.
#[[Complementary DNA]] then binds to the U5 (non-coding region) and R region (a direct repeat found at both ends of the RNA molecule) of the viral RNA.
#A domain on the reverse transcriptase enzyme called [[RNAse H]] degrades the 5’ end of the RNA which removes the U5 and R region.
#The primer then "jumps" to the 3’ end of the viral genome, and the newly synthesised DNA strands hybridizes to the complementary R region on the RNA.
#The first strand of complementary DNA (cDNA) is extended, and the majority of viral RNA is degraded by RNAse H.
#Once the strand is completed, second strand synthesis is initiated from the viral RNA.
#There is then another "jump" where the PBS from the second strand hybridizes with the complementary PBS on the first strand.
#Both strands are extended further and can be incorporated into the hosts genome by the enzyme [[integrase]].

Creation of double-stranded DNA also involves ''strand transfer'', in which there is a translocation of short DNA product from initial RNA-dependent DNA synthesis to acceptor template regions at the other end of the genome, which are later reached and processed by the reverse transcriptase for its DNA-dependent DNA activity.<ref>{{cite book |authors=Telesnitsky A., Goff S. P. | editor = Skalka, M. A. |editor2=Goff, S. P. | title = Reverse transcriptase| edition = 1st | language = | publisher = Cold Spring Harbor| location = New York | year = 1993 | origyear = | chapter =  Strong-stop strand transfer during reverse transcription | page = 49| quote = | isbn =0-87969-382-7 | oclc = | doi = | url = | accessdate = }}</ref>

Retroviral RNA is arranged in 5’ terminus to 3’ terminus. The site where the [[primer (molecular biology)|primer]] is annealed to viral RNA is called the primer-binding site (PBS). The RNA 5’end to the PBS site is called U5, and the RNA 3’ end to the PBS is called the leader. The tRNA primer is unwound between 14 and 22 [[nucleotides]] and forms a base-paired duplex with the viral RNA at PBS. The fact that the PBS is located near the 5’ terminus of viral RNA is unusual because reverse transcriptase synthesize DNA from 3’ end of the primer in the 5’ to 3’  direction (with respect to the RNA template). Therefore, the primer and reverse transcriptase must be relocated to 3’ end of viral RNA. In order to accomplish this reposition, multiple steps and various enzymes including [[DNA polymerase]], ribonuclease H(RNase H) and polynucleotide unwinding are needed.<ref name="isbn0-87969-167-0">{{cite book |authors=Bernstein A., Weiss R., Tooze J. | title = Molecular Biology of Tumor Viruses | edition = 2nd| language = | publisher = Cold Spring Harbor Laboratory | location = Cold Spring Harbor, N.Y. | year = 1985 | origyear = | pages = | quote =  | isbn = | oclc = | doi = | url = | accessdate = | chapter =  RNA tumor viruses }}</ref><ref>Moelling, K; Broecker F. (2015)  The reverse transcriptase–RNase H: from viruses to antiviral defense. Ann N Y Acad Sci. 1341:126-35. doi: 10.1111/nyas.12668.</ref>

The HIV reverse transcriptase also has [[ribonuclease]] activity that degrades the viral RNA during the synthesis of cDNA, as well as [[DNA-dependent DNA polymerase]] activity that copies the [[Sense (molecular biology)|sense]] cDNA strand into an ''antisense'' DNA to form a double-stranded viral DNA intermediate (vDNA).<ref>[http://student.ccbcmd.edu/courses/bio141/lecguide/unit3/viruses/hivlc.html Doc Kaiser's Microbiology Home Page > IV. VIRUSES > F. ANIMAL VIRUS LIFE CYCLES > 3. The Life Cycle of HIV] Community College of Baltimore County. Updated: Jan 2008.</ref>

== In eukaryotes ==

Self-replicating stretches of [[eukaryotic]] genomes known as [[retrotransposon]]s utilize reverse transcriptase to move from one position in the genome to another via an RNA intermediate. They are found abundantly in the genomes of plants and animals. [[Telomerase]] is another reverse transcriptase found in many eukaryotes, including humans, which carries its own [[RNA]] template; this RNA is used as a template for [[DNA replication]].<ref name="isbn0-7167-4366-3">{{cite book |vauthors=Krieger M, Scott MP, Matsudaira PT, Lodish HF, Darnell JE, Zipursky L, Kaiser C, Berk A | title = Molecular cell biology | publisher = W.H. Freeman and CO | location = New York | year = 2004 | pages = | isbn = 0-7167-4366-3  }}</ref>

== In prokaryotes ==
Initial reports of reverse transcriptase in prokaryotes came as far back as 1971 (Beljanski et al., 1971a, 1972).  These have since been broadly described as part of bacterial [[Retron]]s, distinct sequences that code for reverse transcriptase, and are used in the synthesis of [[multicopy single-stranded DNA|msDNA]]. In order to initiate synthesis of DNA, a primer is needed. In bacteria, the primer is synthesized during replication.<ref name="pmid4333538">{{cite journal |authors=Hurwitz J., Leis J. P. | title = RNA-dependent DNA polymerase activity of RNA tumor viruses. I. Directing influence of DNA in the reaction | journal = J. Virol. | volume = 9 | issue = 1 | pages = 116–29 | date = January 1972 | pmid = 4333538 | pmc = 356270 | doi =  | url =  }}</ref>

== Evolutionary role ==
Valerian Dolja of Oregon State argues that viruses due to their diversity have played an evolutionary role in the development of cellular life, with reverse transcriptase playing a central role.<ref>{{cite news|last1=Arnold|first1=Carrie|title=Could Giant Viruses Be the Origin of Life on Earth?|url=http://news.nationalgeographic.com/news/2014/07/140716-giant-viruses-science-life-evolution-origins/|accessdate=29 May 2016|work=news.nationalgeographic.com|date=17 July 2014}}</ref>

== Structure ==

Reverse transcriptase enzymes include an RNA-dependent [[DNA polymerase]] and a DNA-dependent DNA polymerase, which work together to perform transcription. In addition to the transcription function, retroviral reverse transcriptases have a domain belonging to the [[RNase H]] family, which is vital to their replication.

== Replication fidelity ==

There are three different replication systems during the life cycle of a retrovirus. First of all, the reverse transcriptase synthesizes viral DNA from viral RNA, and then from newly made complementary DNA strand. The second replication process occurs when host cellular DNA polymerase replicates the integrated viral DNA. Lastly, RNA polymerase II transcribes the proviral DNA into RNA, which will be packed into virions. Therefore, mutation can occur during one or all of these replication steps.<ref>{{cite book |authors=Bbenek K., Kunkel A. T. | editor = Skalka, M. A. |editor2=Goff, P. S.  | title = Reverse transcriptase | edition = | language = | publisher = Cold Spring Harbor Laboratory Press | location = New York | year = 1993 | origyear = | chapter = The fidelity of retroviral reverse transcriptases | page = 85 | quote = | isbn = 0-87969-382-7 | oclc = | doi = | url = | accessdate = }}</ref>

Reverse transcriptase has a high error rate when transcribing RNA into DNA since, unlike most other [[DNA polymerase]]s, it has no [[Proofreading (biology)|proofreading]] ability. This high error rate allows [[mutation]]s to accumulate at an accelerated rate relative to proofread forms of replication. The commercially available reverse transcriptases produced by [[Promega]] are quoted by their manuals as having error rates in the range of 1 in 17,000 bases for AMV and 1 in 30,000 bases for M-MLV.<ref>[http://www.promega.com/pnotes/71/7807_22/7807_22_core.pdf Promega kit instruction manual (1999)]</ref>

Other than creating [[single-nucleotide polymorphism]]s, reverse transcriptases have also been shown to be involved in processes such as [[transcript fusion]]s, [[exon shuffling]] and creating artificial [[antisense]] transcripts.<ref name="pmid20805885">{{cite journal |authors=Houseley J., Tollervey D. | title = Apparent non-canonical trans-splicing is generated by reverse transcriptase in vitro | journal = PLoS ONE | volume = 5 | issue = 8 | pages = e12271 | year = 2010 | pmid = 20805885 | pmc = 2923612 | doi = 10.1371/journal.pone.0012271 }}</ref><ref name="pmid12044895">{{cite journal |authors=Zeng X. C., Wang S. X. | title = Evidence that BmTXK beta-BmKCT cDNA from Chinese scorpion Buthus martensii Karsch is an artifact generated in the reverse transcription process | journal = FEBS Lett. | volume = 520 | issue = 1–3 | pages = 183–4; author reply 185 | date = June 2002 | pmid = 12044895 | doi = 10.1016/S0014-5793(02)02812-0 }}</ref> It has been speculated that this '''''template switching''''' activity of reverse transcriptase, which can be demonstrated completely ''in vivo'', may have been one of the causes for finding several thousand unannotated transcripts in the genomes of model organisms.<ref>{{cite journal | title = Response to "The Reality of Pervasive Transcription" | year = 2011  |authors=van Bakel H., Nislow C., Blencowe B. J., Hughes T. R. | journal = PLoS Biology | volume = 9 | issue = 7 | pages = e1001102 | doi = 10.1371/journal.pbio.1001102 }}</ref>

==Applications==
[[Image:Zidovudine.svg|thumb|200px|The molecular structure of [[zidovudine]] (AZT), a drug used to inhibit [[HIV]] reverse transcriptase]]

=== Antiviral drugs ===
{{details|Reverse-transcriptase inhibitor}}
As [[HIV]] uses reverse transcriptase to copy its genetic material and generate new viruses (part of a retrovirus proliferation circle), specific drugs have been designed to disrupt the process and thereby suppress its growth. Collectively, these drugs are known as [[reverse-transcriptase inhibitor]]s and include the nucleoside and nucleotide analogues [[zidovudine]] (trade name Retrovir), [[lamivudine]] (Epivir) and [[tenofovir]] (Viread), as well as non-nucleoside inhibitors, such as [[nevirapine]] (Viramune).

=== Molecular biology ===
{{details|Reverse transcription polymerase chain reaction}}
Reverse transcriptase is commonly used in research to apply the [[polymerase chain reaction]] technique to [[RNA]] in a technique called [[reverse transcription polymerase chain reaction]] (RT-PCR).  The classical [[polymerase chain reaction|PCR]] technique can be applied only to [[DNA]] strands, but, with the help of reverse transcriptase, RNA can be transcribed into DNA, thus making [[Polymerase chain reaction|PCR]] analysis of RNA molecules possible. Reverse transcriptase is used also to create [[cDNA library|cDNA libraries]] from [[mRNA]]. The commercial availability of reverse transcriptase greatly improved knowledge in the area of molecular biology, as, along with other [[enzymes]], it allowed scientists to clone, sequence, and characterise RNA.

Reverse transcriptase has also been employed in [[insulin]] production. By inserting eukaryotic mRNA for insulin production along with reverse transcriptase into bacteria, the mRNA could be inserted into the prokaryote's genome. Large amounts of insulin can then be created, sidestepping the need to harvest pig pancreas and other such traditional sources. Directly inserting eukaryotic DNA into bacteria would not work because it carries [[intron]]s, so would not translate successfully using the bacterial ribosomes. Processing in the eukaryotic cell during mRNA production removes these introns to provide a suitable template. Reverse transcriptase converted this edited RNA back into DNA so it could be incorporated in the genome.

==See also==
{{Portal|Molecular and cellular biology|Viruses}}
*[[cDNA library]]
*[[DNA polymerase]]
*[[msDNA]]
*[[Reverse transcribing virus (disambiguation)|Reverse transcribing virus]]<!--intentional link to DAB page-->
*[[RNA polymerase]]
*[[Telomerase]]
*[[Retrotransposon marker]]

==References==
{{reflist|colwidth=35em}}

==External links==
* {{MeshName|RNA+Transcriptase}}
* [http://www.tibotec.com/bgdisplay.jhtml?itemname=HIV_discovery&product=none&s=2 animation of reverse transcriptase action and three reverse transcriptase inhibitors]
* [http://www.rcsb.org/pdb/static.do?p=education_discussion/molecule_of_the_month/pdb33_1.html Molecule of the month] (September 2002) at the RCSB PDB
* [https://www.youtube.com/watch?v=RO8MP3wMvqg HIV Replication 3D Medical Animation. (Nov 2008). Video by Boehringer Ingelheim.]
* {{ cite web | url= http://www.rcsb.org/pdb/101/motm.do?momID=33 | title= Molecule of the Month: Reverse Transcriptase (Sep 2002)|accessdate= 2013-01-13 | author= Goodsell DS | publisher=  Research Collaboratory for Structural Bioinformatics (RCSB) Protein Data Bank (PDB) }}
 
{{Viral proteins}}
{{Kinases}}

[[Category:Genetics]]
[[Category:EC 2.7.7]]
[[Category:Molecular biology]]
[[Category:Viral enzymes]]
[[Category:Telomeres]]