{{Use dmy dates|date=January 2013}}
{{Infobox military person
| name          = Friedrich Marnet
| image         =
| caption       =
| birth_date          = 22 January 1882 
| death_date          = {{d-da|3 October 1915|22 January 1882}}
| placeofburial_label = 
| placeofburial = 
| birth_place  = [[Metz]], [[Lorraine (region)|Lorraine]], Germany
| death_place  = [[Ochsenfurt]], Germany
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = {{flag|German Empire}}
| branch        = [[Luftstreitkräfte]]
| serviceyears  = 1902-1915
| rank          = Hauptmann
| servicenumber =
| unit          = [[Feldflieger Abteilung]] 6b
| commands      =
| battles       =
| awards        =
| relations     =
| laterwork     =
}}

'''Friedrich Marnet''' (22 January 1882 – 3 October 1915) was a German [[World War I]] officer and pilot. He also was one of the first German pilots to fly a [[Gotha G]].

==Background==
Friedrich Marnet was born into a military family in [[Metz]], one of the strongest fortress of the [[German Empire]]. After leaving school, Marnet enlisted into the [[Prussian Army]] as a ''Fahnenjunker'' (cadet officer) in 1902. He received his commission as ''Leutnant'' on 9 March 1903. On 7 March 1912, Marnet was promoted to the grade of ''Oberleutnant''. His flight training took place from July to August 1913. Marnet passed his final pilot's exam in August 1914.

==Involvement in 1st World War==
[[File:GothaGI.jpg|thumb|right|The Gotha G.I, a heavy bomber used by the Luftstreitkräfte]]
When [[World War I]] began in 1914, Marnet was immediately sent to the ''1st Ersatz Bataillon 8'', as a Platoon leader. At his own instigation, Marnet transferred to [[Feldflieger Abteilung]] 1b in May 1915. In June 1915, he participated in a flight observation training course in [[Oberschleißheim|Schleißheim]]. On 16 July 1915, Friedrich Marnet transferred to Feldflieger-Abteilung 8b. Soon after, on 9 August 1915, Marnet was promoted ''Hauptmann''. On 21 August 1915, Marnet transferred to [[Feldflieger Abteilung]] 6b, then based near [[Bühl (Baden)|Bühl]].

In October 1915, while Hauptmann Marnet was flying toward [[Strasbourg]], with a mechanic and an observer, his plane, a [[Gotha G.I]] B.14/15, crashed in a freshly tilled field, near [[Ochsenfurt]]. Hauptmann Friedrich Marnet's body was recovered from the wreckage, and he was buried with full military honors by his fellows in arms. His grave is now in [[Munich]] Waldfriedhof.

== Dates of Ranks ==
* Leutnant (Patent No.62): 9. March 1903
* Oberleutnant (Patent No.76): 7 March 1912
* Hauptmann : 9 August 1915

==Sources ==
*"Marnet, Friedrich" on [http://www.frontflieger.de/3-m-f.html frontflieger.de]

{{wwi-air}}

{{DEFAULTSORT:Marnet, Friedrich}}
[[Category:1882 births]]
[[Category:1915 deaths]]
[[Category:Aerial warfare pioneers]]
[[Category:Prussian Army personnel]]
[[Category:Luftstreitkräfte personnel]]
[[Category:Aviators killed in aviation accidents or incidents]]
[[Category:German military personnel killed in World War I]]
[[Category:People from Metz]]
[[Category:People from Alsace-Lorraine]]