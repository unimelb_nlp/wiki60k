{{Italic title}}
{{Infobox journal
| title        = Journal of Business & Securities Law
| cover        =
| editor       =
| discipline   = [[Law]]
| abbreviation = J. Bus. & Sec. L.
| publisher    = [[Michigan State University College of Law]]
| country      = [[United States]]
| frequency    = Biannually
| history      = 2005-present
| openaccess   =
| license      =
| impact       =
| impact-year  =
| website      = https://www.msujbsl.com
| link1        = https://www.msujbsl.com/content/publications
| link1-name   = Online archive
| atom         =
| JSTOR        =
| OCLC         = 62075516
| LCCN         = 2005212548
| CODEN        =
| ISSN         = 1558-609X
| eISSN        =
}}
The '''''Journal of Business & Securities Law''''' is a student-edited [[law review|law journal]] covering the areas of business and securities law and is an official journal of the [[Michigan State University College of Law]].<ref>{{cite web|url=http://news.msu.edu/story/6975/|title=Journal of Business & Securities Law receives official status|publisher=MSU|year=2000|accessdate=2010-03-29}}</ref> The Journal publishes articles on topics including [[corporation|corporate]] [[lawsuit|litigation]], [[financial transaction|commercial transaction]]s, employment, [[electronic commerce|e-commerce]], [[financial regulation|securities regulation]], and any other topic focusing on the intersection of law and business. The Journal is the sole outlet for the transcription and publication of the annual Midwest Securities Law Institute held at the Michigan State University College of Law.  It is also the national host of the annual  Elliot A. Spoon Business Law Writing Competition.<ref>{{cite web |url=https://www.msujbsl.com/spoon_competition.html|title=Elliot A. Spoon Business Law Writing Competition // ''Journal of Business & Securities Law'' |work= |accessdate=2010-04-02}}</ref> The Journal has a staff of approximately 25 law students and is headed by a board consisting of an editor-in-chief, executive editor, two managing editors of publication, and a managing editor of articles.  The Journal has been cited in various state and federal court decisions around the [[United States]].<ref>{{cite web|url=https://www.msu.edu/~jbsl/archive_In_re_Vivendi.html|title=Press Release regarding In re Vivendi|publisher=Journal of Business & Securities Law|year=2009|accessdate=2010-03-29}}</ref><ref>{{cite web|url=https://www.msu.edu/~jbsl/archive_USvFerguson.html|title=Ruling on loss calculation, victim enhancement, and restitution|publisher=Journal of Business & Securities Law|year=2009|accessdate=2010-03-29}}</ref><ref>{{cite web|url=http://www.law.msu.edu/news/2009/releases/JBSL-Dist-Ct.html|title=Journal of Business and Securities Law Cited in District Court Ruling|publisher=MSU CoL|year=2009|accessdate=2010-03-29}}</ref><ref>{{cite web|url=http://scholar.google.com/scholar_case?case=1919349285090980143|title=Grand v. Nacchio (Ariz. Court of Appeals)|publisher=Google Scholar|year=2006|accessdate=2010-03-29}}</ref>

==References==
{{reflist}}

==External links==
*{{Official website|http://www.msujbsl.com}}

{{DEFAULTSORT:Journal Of Business and Securities Law}}
[[Category:American law journals]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 2005]]
[[Category:Law journals edited by students]]
[[Category:2005 establishments in Michigan]]