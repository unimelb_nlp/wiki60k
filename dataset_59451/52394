{{Infobox journal
| title = Neurogenetics
| cover = [[File:Coverneurogenetics.jpg]]
| editor = U. Müller, M.B. Graeber, Louis J. Ptáček
| discipline = [[Neurogenetics]]
| former_names =
| abbreviation = Neurogenetics
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 1997-present
| openaccess =
| license =
| impact = 3.426
| impact-year = 2015
| website = http://www.springer.com/biomed/neuroscience/journal/10048
| link2 = http://link.springer.com/journal/volumesAndIssues/10048
| link2-name = Online archive
| JSTOR =
| OCLC = 37360267
| LCCN =
| CODEN = NEROFX
| ISSN = 1364-6745
| eISSN = 1364-6753
}}
'''''Neurogenetics''''' is a quarterly [[peer-reviewed]] [[scientific journal]] covering the field of [[neurogenetics]]. It was established in 1997 and is published quarterly by [[Springer Science+Business Media]]. The journal publishes [[review journal|review articles]], original articles, short communications, and [[Letter to the editor|letters to the editors]]. The [[editors-in-chief]] are Ulrich Müller ([[University of Giessen]]), Manuel B. Graeber ([[University of Sydney]]), and Louis J. Ptáček ([[University of California, San Francisco]]).

==Abstracting and indexing==
The journal is abstracted and indexed in [[InfoTrac|Academic OneFile]], [[Biological Abstracts]], [[BIOSIS Previews]], [[Chemical Abstracts Service]], [[ProQuest|ProQuest databases]], [[Embase]], [[Neuroscience Citation Index]], [[PASCAL (database)|PASCAL]], [[PubMed]]/[[MEDLINE]], [[Science Citation Index Expanded]], [[Scopus]], and [[VINITI Database RAS]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.426.<ref name=WoS>{{cite book |year=2016 |chapter=Neurogenetics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/biomed/neuroscience/journal/10048}}

[[Category:Neuroscience journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1997]]
[[Category:English-language journals]]
[[Category:Neurology journals]]
[[Category:Genetics journals]]