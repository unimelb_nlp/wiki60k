{{Infobox SG rail
|railroad_name     = Charlevoix Railway, ''Chemin de fer Charlevoix''
|logo_filename     = 
|logo_size         = 
|system_map        = 
|map_caption       = 
|map_size          = 
|marks             = CFC
|image             =
|image_size        = 
|image_caption     = A view of the [[Casino de Charlevoix]] and Manoir Richelieu with the railway line running along the [[Saint Lawrence River]] at lower left, 2007
|locale            = [[Charlevoix]] & [[Capitale-Nationale]] regions of [[Quebec]]
|start_year        = {{start date|1889|8|10|df=y}}
|end_year          = present
|predecessor_line  = [[Quebec, Montmorency and Charlevoix Railway]] (1881&ndash;1904)<br/>[[Quebec Railway, Light and Power Company]] (1904&ndash;1951)<br/>[[Canadian National Railway]] (1951&ndash;1994)
|successor_line    = 
|electrification   = From 1900 to 1959
|length            = {{convert|144|km|mi}}<ref name="le_massif"/>
|hq_city           = 5 rue Desbiens<br/>[[Clermont, Capitale-Nationale, Quebec|Clermont]], [[Quebec]], G4A1B8
|website           = [http://www.lemassif.com/en→ Le Massif train page]
}}

The '''Charlevoix Railway''' ({{lang-fr|'''Chemin de fer Charlevoix'''}}) {{reporting mark|CFC}} is a [[short-line railroad|short-line railway]] that operates in the [[Charlevoix]] region of [[Quebec]] [[Canada]].  From 1994 to 2009 it was a subsidiary of the [[Quebec Railway Corporation]], a short line operator.  Since April 2009 it has been owned by 
[[Train touristique de Charlevoix Inc.]],<ref>{{cite web|url=http://www.lemassif.com/en/train |title=Archived copy |accessdate=2010-05-27 |deadurl=yes |archiveurl=https://web.archive.org/web/20100123173244/http://www.lemassif.com:80/en/train |archivedate=2010-01-23 |df= }}</ref> a [[Groupe Le Massif Inc.]] (owners of [[Le Massif]]) subsidiary.<ref name="railcan">{{cite web
|url=http://www.railcan.ca/sec_rac/en_rac_member_profile.asp?id=65|title=RAC - Member Railways - Profile
|accessdate=2010-05-25}}</ref>  With a length of {{convert|144|-|148|km|mi}}<ref name="le_massif"/> it connects the city of [[Clermont, Capitale-Nationale, Quebec|Clermont]] in the Charlevoix region to a freight yard of the [[Canadian National Railway]] (CN) located in the [[La Cité-Limoilou]] borough of the city of [[Quebec City|Quebec]]. The railway runs along both the [[St. Lawrence River]] and the [[Malbaie River]] and consists of a single non-electrified track.

The railway carried passengers at its start in the 19th century, for much of the early part of the 20th century, and as part of a [[dinner train]] operation in the 1980s and again in the 1990s.  Passenger service on a [[Charlevoix tourist train|tourist train]] between Quebec City and [[La Malbaie]] began in September 2011.  As a [[freight railway]] the main commodities transported are: [[clay]], [[timber]], [[lumber]], [[cement]], [[woodchips]], [[paper]], and [[peroxide]].

== History ==
The '''Quebec, Montmorency and Charlevoix Railway Company''' (QM&C) was incorporated by an act of the Legislature of Quebec in 1881.<ref name="legislature">In 1881 Quebec had a bicameral legislature composed of the [[Legislative Council of Quebec]] and the [[Legislative Assembly of Quebec]].  For details see: [[4th Legislative Assembly of Quebec]].</ref><ref 
name="qm&c">{{cite book|url=https://archive.org/details/cihm_12271|title=Quebec, Montmorency & Charlevoix Railway: A sketch of the scheme and appendices containing various statistical and other information|year=1881|publisher=Three Rivers "La Concorde" Print|accessdate=2010-05-26}}</ref>  The railway was to be built along the Saint Lawrence River and was intended to provide service to as far east as [[Baie-Sainte-Catherine, Quebec|Baie-Sainte-Catherine]], which was in turn expected to be developed into a major seaport with ice free shipping even in winter.<ref name="qm&c"/>

[[File:Manoir Richelieu 1895.jpg|thumb|left|The first [[Manoir Richelieu]] in 1899.  Note the absence of rails along the river at the time.]]
The first part of the line between Limoilou and [[Sainte-Anne-de-Beaupré, Quebec]] went into service on Saturday 10 August 1889.<ref name="lavallee"/>  It was built at the time for the pilgrimage to the [[Basilica of Sainte-Anne-de-Beaupré]], and the railway was nicknamed the "Railway of Good Saint Anne".<ref name="lavallee">{{cite book|first=Omer S.-A.|last=Lavallée|title=Chemin de Fer de la Bonne Sainte-Anne: Une histoire des soixante-dix années de service ferroviaires sur la Côte de Beaupré|year=1958|url=http://www.angelfire.com/ca/TORONTO/history/QuebecRailwayFR.html|language=French|accessdate=2010-05-29}}</ref>  Initial operations were with [[steam locomotive]] hauled trains.
The line was electrified in 1904, when the company changed its name to '''Quebec Railway Light & Power''' (QRL&P). Between 1904 and 1959 electric trains provided an [[interurban]] type service for passengers between Quebec and Sainte-Anne-de-Beaupré.

The second part of the line, extending from Beaupré to Clermont in Charlevoix, was built between 1909 and 1919. 
[[Rodolphe Forget]] had built his large [[Manoir Richelieu]] at Pointe-au-Pic (now part of [[La Malbaie]]) in the 1890s.  At first hotel guests would get to the hotel via [[Canada Steamship Lines]] steamships.  Rodolphe Forget ran for the [[House of Commons of Canada|House of Commons]] in 1904 promising voters to extend the QM&C line, which was eventually done.  This part of the line is very scenic because it is literally wedged between [[Laurentian Mountains|mountains]] and river.  It required huge investments in time and money: there are two [[tunnel]]s and 900 [[bridge]]s and [[culvert]]s, or an average of one every {{convert|165|m|ft}}.

[[File:547 - Beaupre QC.JPG|thumb|left|The railway traverses a small bridge in front of a waterfall near [[Beauport, Quebec City|Montmorency]], 16 July 2005]]
With the line extended Manoir Richelieu hotel customers could use the QRL&P trains instead of CSL vessels to get to the hotel.  The hotel building burned down in the fall of 1928 but was rebuilt to designs by [[John Smith Archibald]] and reopened in June 1929.<ref name="fairmont">{{cite web|url=http://www.fairmont.com/EN_FA/Property/LMR/AboutUs/HotelHistory.htm|title=Hotels in Charlevoix: Le Manoir Richelieu Luxury Quebec Resort History|publisher=[[Fairmont Hotels and Resorts|Fairmont Hotels]]|accessdate=2010-05-26}}</ref>  CSL vessels continued to call on the hotel until 1966.<ref name="lavallee"/>

In 1951 CN took over as owner of the railway.  The line became known as the Murray Bay Subdivision of CN, [[Murray Bay]] being an English name for ''La Malbaie''.<ref name="racfmc">{{cite web|url=http://www.proximityissues.ca/Maps/RAC-2004-QC_sub.pdf|title=Quebec Railways Chemin de fer du Quebec|format=pdf|accessdate=2010-05-26|publisher=RAC and FCM}}</ref>
In 1959 CN terminated passenger service and dismantled the [[overhead line]]s used for electrification between Limoilou and [[Saint-Joachim, Quebec|Saint-Joachim]].

In 1984 the line saw the reintroduction of passenger service when a [[dinner train]] known as the ''[[Le Tortillard du Saint-Laurent]]'' started operating between Quebec and La Malbaie. The train ran through the 1985 season before closing.<ref name="ls20110903">{{cite news|url=http://www.lapresse.ca/le-soleil/affaires/les-regions/201109/02/01-4431327-train-touristique-une-histoire-mouvementee.php |title=Train touristique: une histoire mouvementée |work=[[Le Soleil (Quebec)|Le Soleil]] |date=September 3, 2011 |accessdate=11 October 2014 |author=Morin, Annie |language=French |archiveurl=http://www.webcitation.org/6TFmsFo4U?url=http://www.lapresse.ca/le-soleil/affaires/les-regions/201109/02/01-4431327-train-touristique-une-histoire-mouvementee.php |archivedate=11 October 2014 |deadurl=yes |df= }}</ref> {{citation needed span|In 1994 CN sold the whole line to the [[Quebec Railway Corporation]].|date=October 2014}} The ''Tortillard du Saint-Laurent'' dinner train was restarted under another company and ran again in 1995 and 1996 before declaring bankruptcy.<ref name="ls20110903" /> In 1996 the Charlevoix Railway Company created a major timber transshipment yard at Clermont, which is used by almost all timber processors of the [[Côte-Nord]].

On 7 May 2007 Nancy Belley, who had spent more than 10 years managing the Chemin de fer Charlevoix subsidiary of the Quebec Rail Corporation, was hired by Le Massif to serve as the manager of their railway project.<ref name="belley_pr">{{cite web|url=http://www.lemassif.com/en/presse/950/nancy-belley-named-general-manager-of-charlevoix-tourism-train|title=NANCY BELLEY NAMED GENERAL MANAGER OF CHARLEVOIX TOURISM TRAIN|publisher=Le Massif de Charlevoix|date=27 April 2007|accessdate=2010-05-29}}</ref><ref name="le massif apropos">{{cite web|url=http://www.lemassif.com/en/apropos|title=About us - Le Massif de Charlevoix|accessdate=2010-05-26}}</ref>  In April 2009 Le Massif de Charlevoix organization purchased the line between Quebec and La Malbaie from the Quebec Railway Corporation.  Track rehabilitation for a new tourist line began in October 2009.<ref name="le_massif">{{cite web|url=http://www.lemassif.com/en/presse/937/charlevoix-touring-train-track-rehabilitation-begins|title=Charlevoix Touring Train: TRACK REHABILITATION BEGINS - Le Massif de Charlevoix|last=Vallée|first=Isabelle|date=6 October 2009|accessdate=2010-05-25}}</ref>  [[Infrastructure Canada]] announced in November 2009 that the governments of Canada and Quebec will contribute {{CAD}}$5&nbsp;million toward the railway line upgrade.<ref name="infrastructure">{{cite web
|url=http://www.buildingcanada-chantierscanada.gc.ca/media/news-nouvelles/2009/20091126petiterivieresaintfrancois-eng.html|date=26 November 2009|publisher=[[Infrastructure Canada]]|title=Building Canada - Governments of Canada and Quebec invest in future of Charlevoix tourist train|accessdate=2010-05-25}}</ref>
CN still uses the line to haul freight between Clermont and Quebec via [[trackage rights]].  The [[Port of Quebec]] City lists the Chemin de fer Charlevoix as a rail transport provider to the port's facilities.<ref name="portquebec">{{cite web|url=http://www.portquebec.ca/index.php?option=com_content&task=view&id=5&Itemid=11&lang=en_GR|title=Port de Québec - Why Choose the Port of Quebec ?|accessdate=2010-05-29}}</ref>

Beginning in September 2011, a new [[Charlevoix tourist train|tourist train service]] began operation along the Charlevoix Railway between Quebec City and [[La Malbaie]].<ref>{{cite web|url=http://www.firsttracksonline.com/2011/08/11/charlevoix-ski-train-ready-to-roll/ |title=Charlevoix Ski Train Ready to Roll |publisher=First Tracks!! Online |date=11 August 2011 |accessdate=14 August 2011 |archiveurl=http://www.webcitation.org/60wJkrk1z?url=http://www.firsttracksonline.com/2011/08/11/charlevoix-ski-train-ready-to-roll/ |archivedate=14 August 2011 |deadurl=yes |df= }}</ref>

== Stations served ==

[[File:Montmerency falls top.jpg|thumb|View overlooking the top of [[Montmorency Falls]] where the [[Montmorency River]] flows into the [[St. Lawrence River]]. The Charlevoix Railway bridge over the Montmorency River is visible at the further end of the [[plunge pool]]. Beyond it lies a bridge carrying [[Quebec Route 138|Route 138]] and beyond that is the [[Île d'Orléans Bridge]] over the St. Lawrence, 26 April 2009.]]  
There are several stations served by the railway from Quebec running east and north to Clermont:<ref name="cn_map">{{cite web|url=http://cnplus.cn.ca/IT/Shortlines/SL_Static.nsf/shortlines/0743E49DB22F6BBF8525675A003B3881?opendocument|title=Chemin de fer Charlevoix - Context Map|date=20 May 2008|accessdate=2010-05-25}}</ref>
* [[Quebec City]]
* [[Limoilou]]
* Hedley (or Hedleyville) which has been part of [[La Cité-Limoilou]] since 1903
* [[Villeneuve (electoral district)|Villeneuve]]
* [[Sainte-Anne-de-Beaupré]]
* Donohue (just east of [[Beaupré, Quebec|Beaupré]]) the paper mill was sold to [[Abitibi-Consolidated]] in 2000, then merged into [[AbitibiBowater]] in 2007
* [[Saint-Joachim, Quebec|Saint-Joachim]] labelled "Les Caps" by CN
* Point D'Aulne (northeast of [[Petite-Rivière-Saint-François]])
* [[Baie-Saint-Paul]]
* [[La Malbaie]]
* Wieland
* [[Clermont, Capitale-Nationale, Quebec]] line ends at a former [[Donohue Inc.|Donohue]] paper mill along ''rue de la Donohue''

== Quebec Railway Light & Power Network ==
{{BS-map
|title=Q.R.L.& P.Co. route map prior to 1959
|title-color=white
|title-bg=#27404E
|width=370px
|collapsible=yes
|collapse=yes
|map=
{{BS4|KBHFa||||0 mi|St. Paul Street Station|Québec}}
{{BS4|WBRÜCKE|WASSERq|WASSERq|WASSERq||<small>St. Charles River|Swinging Bridge</small>}}
{{BS4|ABZlf|umKRZu|CONTfq||0.6 mi|Hedleyville|(Limoilou Junction)|O1=BHF}}
{{BS4|HST|uSTR|||1.2 mi|Maufils}}
{{BS4|HST|uSTR|||1.4mi|Maizerets}}
{{BS4|uexABZlf|ueKRZ|uexSTRlg|||Mastaï-Junction||(Private streetcar line)|O1=eHST}}
{{BS4|STR|uCONTf|uexKHSTe|||Mastaï Station||(Private streetcar line)|}}
{{BS4|HST||||1.9 mi|De Salaberry||(Avenue D'Estimauville)}}
{{BS4|HST|||||Monument}}
{{BS4|BHF|uCONTg|||2.8 mi|Giffard|(Beauport)}}
{{BS4|uABZlf|uABZlg|O1=STR}}
{{BS4|WBRÜCKE1|uWBRÜCKE1|WASSERq|WASSERq||<small>Beauport River</small>}}
{{BS4|STR|uHST||||St-André}}
{{BS4|HST|uSTR||||Clermont}}
{{BS4|STR|uHST||||St-Cléophas}}
{{BS4|STR|uSTRlf|uHSTq|uSTRlg||Collège}}
{{BS4|HST|||uSTR||Everell}}
{{BS4|STR|||uHST||Forget}}
{{BS4|HST|||uSTR||Les Falaises}}
{{BS4|STR|||uHST||Parc Bellevue}}
{{BS4|HST|||uSTR|5.1 mi|Paquet}}
{{BS4|STR|||uHST||Garneau}}
{{BS4|HST|||uSTR||Frontenac}}
{{BS4|STR|||uHST||L'Espérance}}
{{BS4|HST|||uSTR|5.4 mi|Lavigueur}}
{{BS4|BHF|||uSTR||Village Montmorency}}
{{BS4|HST|||uSTR|5.9 mi|Montmorency-Est||(Côte Saint-Grégoire)}}
{{BS4|STR|||uHST||Courville}}
{{BS4|BHF||FUNI|uSTR|6.3 mi|Montmorency Falls}}
{{BS4|ABZgl+l|CPICrr|CPIC|CPICll||Kenthouse|(Manoir Montmorency)|Falls Elevator||O2=KHSTr|O4=uWSLel}}
{{BS4|WBRÜCKE|WASSERq|WASSERq|WASSERq||<small>Montmorency River</small>}}
{{BS4|BHF||||7.3 mi|Boischatel}}
{{BS4|HST|||||Rivière-de-Fer}}
{{BS4|BHF||||9.6 mi|[[L'Ange-Gardien, Capitale-Nationale, Quebec|L'Ange-Gardien]]}}
{{BS4|HST||||9.9 mi|Laberge}}
{{BS4|HST|||||Dufournel}}
{{BS4|HST|||||Orléans}}
{{BS4|HST||||11.7 mi|Petit-Pré}}
{{BS4|WBRÜCKE1|WASSERq|WASSERq|WASSERq||<small>Petit-Pré River</small>}}
{{BS4|HST|||||Valin}}
{{BS4|WBRÜCKE1|WASSERq|WASSERq|WASSERq||<small>Cazeau River</small>}}
{{BS4|HST||||12.7 mi|Rivière-Cazeau}}
{{BS4|HST|||||Lemoine}}
{{BS4|WBRÜCKE2|WASSERq|WASSERq|WASSERq||Lemoyne River}}
{{BS4|HST|||||Laverdière}}
{{BS4|HST|||||Dorion}}
{{BS4|HST|||||Château-Village}}
{{BS4|HST||||15.5 mi|Giguère}}
{{BS4|BHF||||15.6 mi|Château-Richer}}
{{BS4|WBRÜCKE|WASSERq|WASSERq|WASSERq||<small>Sault-à-la-Puce River</small>}}
{{BS4|HST|||||Lefrançois}}
{{BS4|HST|||||Visitation}}
{{BS4|HST|||||Baker Inn}}
{{BS4|HST||||18.4 mi|Rivière-des-Chiens}}
{{BS4|WBRÜCKE|WASSERq|WASSERq|WASSERq||<small>Rivière-aux-Chiens</small>}}
{{BS4|HST|||||Casgrain}}
{{BS4|HST|||||Dumoulin}}
{{BS4|HST|||||Lapointe}}
{{BS4|BHF||||20.6 mi|Ste. Anne Church|(Basilique Sainte-Anne-de-Beaupré)}}
{{BS4|BHF||||21.1 mi|Ste. Anne}}
{{BS4|HST|||||Gagnon}}
{{BS4|HST|||||Pelletier}}
{{BS4|BHF||||23.0 mi|Beaupré}}
{{BS4|WBRÜCKE|WASSERq|WASSERq|WASSERq||<small>Sainte-Anne-du-Nord River</small>}}
{{BS4|HST|||||Donahue}}
{{BS4|HST|||||Queylus}}
{{BS4|ABZgl+l|STRq||||}}
{{BS4|BHF||||25.1 mi|St. Joachim}}
{{BS4|STR|O1=POINTERl|||||<small>End of electric traction</small>}}
{{BS4|HST|||||Petite-Ferme}}
{{BS4|HST|||||Cap-Tourmente}}
{{BS4|CONTf|||||<small>To Baie-Saint-Paul and Clermont</small>}}
|bottom=Compiled from www.cermc.webs.com}}

This route map shows stations served by Q.R.L.& P.Co. from 1889 to 1959, including car stops and Upper Line to Kenthouse (Manoir Montmorency).

The original stations were:
*Hedleyville (Limoilou)
*Giffard (Beauport)
*Village Montmorency
*L'Ange-Gardien
*Château-Richer
*Sainte-Anne-de-Beaupré
*Beaupré
*Saint-Joachim

== See also ==
{{Portal|Railways}}
* ''This article is based on the [[:fr:Chemin de fer Charlevoix|equivalent article]] from the [[French Wikipedia]], consulted on 25 May 2010.''
* [[Canada's grand railway hotels]]
* [[Charlevoix tourist train]]
* [[List of heritage railways in Canada]]

== References ==
{{reflist}}

== Further reading ==
* {{cite book|first=Omer S.-A.|last=Lavallée|title=Chemin de fer de la Bonne Sainte-Anne|year=1958}}

== External links ==
* {{cite web|url=http://www.lemassif.com/docs/4bf0b5d46057c/orig/The-Charlevoix-Touring-Train-15-05-2010.pdf|title=The Charlevoix Touring Train|last=Vallée|first=Isabelle|publisher=[[Le Massif]]|format=pdf|accessdate=2010-05-26}}
{{Canada railways}}
{{Canada Class 2}}

[[Category:Companies based in Quebec]]
[[Category:Quebec railways]]
[[Category:Transport in Capitale-Nationale]]
[[Category:Companies operating former Canadian National Railway lines]]
[[Category:Railway companies established in 1889]]
[[Category:1889 establishments in Quebec]]