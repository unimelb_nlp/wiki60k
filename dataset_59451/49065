{{Orphan|date=March 2017}}

{{Infobox artist
|image=Large Kinsley 12.jpg
|caption=Kinsley alongside one of her braidings
|birth_date=March 26, 1858
|birth_place=Oneida, New York
|death_date=February 10, 1938
|death_place=Oneida, New York
|nationality=American
|known_for=Braided Tapestries
|notable_works=Things Not of Man's Devising, Bewitched, Tree of Life, The Picnic
|movement=Arts and Crafts
}}

'''Jessie Catherine Kinsley''' (March 26, 1858 – February 10, 1938) was an American folk artist known for braidings - an artistic medium of her own devising which grew out of her interest in colonial-style braided rugs.

Kinsley began making braidings at the age of 50, and created many pieces for friends and family. One author described her as having "typified the domestic artist”.<ref>{{Cite journal|last=Schwartz|first=Helen|date=Winter 2000|title=Jessie Kinsley and Her Braidings|url=|journal=Heritage|publisher=New York State Historical Association Magazine|volume=16|doi=|pmid=|access-date=}}</ref> Kinsley’s works attracted attention, particularly in the [[Arts and Crafts movement|Arts and Crafts]] world. In 1917-18 alone, her creations were displayed prominently in the annual exhibit of the National Society of Craftsmen (New York), the Craftsmen’s Exhibition of the Carnegie Institute (Pittsburgh), and the Applied Arts Exhibition of the [[Art Institute of Chicago]].<ref>{{Cite book|title=The Braidings of Jessie Catherine Kinsley|last=Wonderly|first=Anthony|publisher=Oneida Community Mansion House|year=2010|isbn=|location=Oneida, New York|pages=}}</ref>

Kinsley was also a prolific diarist and correspondent. A selection of those writings and a memoir were collected into a book titled ''A Lasting Spring: Jessie Catherine Kinsley, Daughter of the Oneida Community,'' which was edited by her granddaughter Jane Kinsley Rich and historian Nelson Blake. ''A Lasting Spring'' details Kinsley's experiences growing up in a utopian community and her subsequent life.<ref>{{Cite book|title=A Lasting Spring: Jessie Catherine Kinsley, Daughter of the Oneida Community|last=Kinsley|first=Jessie|publisher=Syracuse University Press|year=1983|isbn=|editor-last=Rich|editor-first=Jane Kinsley with the assistance of Nelson M. Blake|location=Syracuse, New York|pages=59}}</ref>

== Biography ==
[[File:OneidaCommunityHomeBld.JPG|thumb|Kinsley was raised in the [[Oneida Community]], a religious utopian group. 
Pictured in a 1907 postcard is the Oneida Community's former home, the [[Oneida Community Mansion House|Mansion House.]]
|left]]
Jessie Catherine Baker was born in the [[Oneida Community]] (1848-1880),  a utopian community of religious perfectionists led by [[John Humphrey Noyes]].  In keeping with the practices of the Community, she was raised in communal style.<ref>{{Cite book|title=Paradise Now:The Story of American Utopianism|last=Jennings|first=Chris|publisher=Random House|year=2016|isbn=978-0-8129-9370-7|location=New York|pages=334}}</ref> Women in the Community held a wide variety of jobs, but they were typically responsible for the mending and sewing, which would have helped Jessie develop her skills from an early age.<ref>{{Cite journal|last=Klee-Hartzell|first=Marlyn|date=Fall 1993|title='Mingling the Sexes':The Gendered Organization of Work in the Oneida Community|url=|journal=Syracuse University Library Associates Courier|issue=2|page=67|doi=|pmid=|access-date=}}</ref> The utopian community voted to disband in 1880 when Jessie was 22.<ref>{{Cite book|title=Without Sin:The Life and Death of the Oneida Community|last=Klaw|first=Spencer|publisher=Allen Lane, the Penguin Press|year=1993|isbn=0-7139-9091-0|location=New York|pages=270}}</ref> The Community then became a joint-stock corporation, eventually evolving into [[Oneida Limited]].<ref>{{Cite book|title=Oneida: Utopian Community to Modern Corporation|last=Carden|first=Maren|publisher=Harper & Row|year=1971|isbn=|location=New York|pages=}}</ref>

Jessie married former Community member Myron Kinsley (1836-1907) in February 1880.<ref>{{Cite book|title=A Lasting Spring|last=Kinsley|first=Jessie|publisher=Syracuse University Press|year=1983|isbn=|location=|pages=59}}</ref> Myron was assigned by the newly formed company to supervise the construction and operation of the Oneida Silverplate and Canning factories in Niagara Falls, N.Y. Together the couple had three children, and Jessie also helped raise Myron’s daughter from a Community relationship. After living in Niagara Falls and Chicago, the Kinsleys returned to [[Oneida, New York|Oneida]], New York.<ref>{{Cite news|url=http://www.niagara-gazette.com/opinion/columns/niagara-discoveries-a-visit-to-the-home-of-myron-h/article_d5af40e7-fda1-5fb3-a4da-8360b151f0f1.html|title=Niagara Discoveries: A visit to the home of Myron H. Kinsley|last=Linnabery|first=Ann Marie|date=February 13, 2016|work=|access-date=|via=}}</ref>

At age 50, Jessie Catherine Kinsley began studying painting with [[Kenneth Hayes Miller]], a leading proponent of urban genre painting. Kinsley was dissatisfied with her painting, but was inspired to create braidings. She wrote:<blockquote>"So gradually, I found the fascination of this medium to my mind and fingers, and began making pictures in a wholly original way from my own designs...and unlike my painted pictures, ''braided pictures'' grew into satisfactory proportions and some large tapestries came with the years."<ref>{{Cite book|title=A Lasting Spring|last=Kinsley|first=Jessie|publisher=Syracuse University Press|year=1983|isbn=|location=|pages=104.}}</ref> </blockquote>Her work quickly grew in complexity. As Beverley Sanders noted in an article about Kinsley, she went from "chair mats to large wall hangings that resemble traditional tapestries in their subtlety of color, complexity of design and sensuous texture."<ref>{{Cite journal|last=Sanders|first=Beverly|date=May 1981|title=A Scrap Bag Palette|url=|journal=American Craft|doi=|pmid=|access-date=}}</ref> Kinsley created braidings until just before her death in 1938.

== Style ==
[[File:Street Scene 1938.jpg|thumb|''Street Scene'' (1938) by Jessie Catherine Kinsley. [[Oneida Community Mansion House]] collection.]]
Kinsley's braidings are "elaborate, exquisitely colored tapestries...that evoke a fairy-tale world" in the words of one craft historian.<ref>{{Cite journal|last=Sanders|first=Beverly|date=1981|title=A Scrap Bag Palette|url=|journal=American Craft|page=14|doi=|pmid=|access-date=}}</ref> Kinsley found inspiration for her braidings in poetry, and many of her pieces have quotations from poems.<ref>{{Cite journal|last=Schwartz|first=Helen|date=2000|title=Jessie Kinsley and her Braidings|url=|journal=Heritage|doi=|pmid=|access-date=}}</ref> A series of landscapes, ''Woodlands'' and ''To a Green Thought'' include versus from ''The Garden'' by [[Andrew Marvell]], while her piece ''Shepherd Boy'' was inspired by [[William Blake]]'s poem ''The Lamb''.<ref>{{Cite journal|last=Horton|first=Janice|date=2012|title=Braids into Brushstrokes:An Artist Finds her Medium|url=|journal=Piecework|page=|pages=44|doi=|pmid=|access-date=}}</ref>

Kinsley once stated that she approached her designs as a painter would, by first sketching out the design and then "using the braids just as a painter uses his brush--making braids take on movement-direction-perspective." She stripped cloth into bands and made her braids by attaching three bands to the back of a chair and braiding them together. Instead of using wool or cotton scraps, Kinsley generally used silks for their more vibrant colors.<ref>{{Cite journal|last=Schwartz|first=Helen|date=Winter 2000|title=Jessie Kinsley and her Braidings|url=|journal=Heritage|doi=|pmid=|access-date=}}</ref> She assembled the braids and basted them onto the pattern she had created, bending them to cover every part of the pattern before sewing them together.<ref>{{Cite journal|last=Sanders|first=Beverly|date=1981|title=A Scrap Bag Palette|url=|journal=American Craft|page=17|doi=|pmid=|access-date=}}</ref>  By the 1920s, Kinsley was creating large pieces. Her first large-scale piece, titled ''Things Not of Man's Devising,'' measured 7 feet by 7 feet. The large pieces were often divided into sections, like a [[triptych]], to make the work possible.

== Exhibitions and collections ==
Much of Kinsley’s work is housed at the [[Oneida Community Mansion House]], a museum that was once home to the Oneida Community. The museum has a permanent display of her work in an exhibit titled “The Braidings of Jessie Catherine Kinsley.”<ref>{{Cite web|url=http://www.oneidacommunity.org/visit-events/collections-exhibitions|title=Collections and Exhibitions|last=|first=|date=|website=Oneida Community Mansion House|publisher=|access-date=}}</ref>

Kinsley’s braidings have also been featured in a 2004 exhibit “Braidings and Benches: The Oneida Community and the Arts and Crafts Movement” at [[Colgate University]] and at an exhibit titled “The Braided Wall Hangings of the Oneida Community” at Fountain Elms, [[Munson-Williams-Proctor Arts Institute]] in 1978.<ref>{{Cite web|url=http://news.colgate.edu/2004/03/oneida-community-descendants-play-role-in-picker-show.html/|title=Oneida Community descendants play role in Picker show|last=|first=|date=March 29, 2004|website=news.colgate.edu|publisher=Colgate University|access-date=}}</ref> Kinsley's braiding ''The Picnic'' is in the collection at The Ruth and Elmer Wellin Museum of Art at [[Hamilton College (New York)|Hamilton College]].<ref>{{Cite web|url=http://wellin-emuseum.hamilton.edu/view/objects/asimages/artist$0040Jesse$0020Catherine%20Kinsley|title=Wellin Collections|last=|first=|date=|website=Ruth and Elmer Wellin Museum of Art, Hamilton College|publisher=|access-date=}}</ref>

Kinsley’s papers, including letters, sketches, and journals, are archived at the Oneida Community Mansion House. Other Kinsley papers are located in [[Syracuse University]]’s  Oneida Community Collection in the Special Collections at Bird Library.<ref>{{Cite web|url=http://library.syr.edu/find/scrc/collections/subjects/religious-and-utopian-communities.php|title=Special Collections: Religious and Utopian Communities|last=|first=|date=|website=library.syr.edu|publisher=Syracuse University|access-date=}}</ref>

== References ==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Kinsley, Jessie Catherine}}
[[Category:1858 births]]
[[Category:1938 deaths]]
[[Category:Folk artists]]
[[Category:American women artists]]