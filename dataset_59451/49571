{{for|the film based on the novel|The Bostonians (film)}}
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = The Bostonians
| image          = File:TheBostonians.jpg
| caption        = First edition
| author         = [[Henry James]]
| country        = United Kingdom
| language       = English
| genre          = 
| publisher      = [[Macmillan Publishers|Macmillan and Co.]], London
| release_date   = 16-Feb-1886
| media_type     = Print ([[Serial (literature)|Serial]])
| pages          = Volume one, 244; volume two, 226; volume three, 232
}}
'''''The Bostonians''''' is a novel by [[Henry James]], first published as a serial in ''[[The Century Magazine]]'' in 1885–1886 and then as a book in 1886. This bittersweet [[tragicomedy]] centers on an odd triangle of characters: Basil Ransom, a [[political conservative]] from [[Mississippi]]; Olive Chancellor, Ransom's cousin and a Boston feminist; and Verena Tarrant, a pretty, young protégée of Olive's in the [[feminist movement]]. The storyline concerns the struggle between Ransom and Olive for Verena's allegiance and affection, though the novel also includes a wide panorama of political activists, newspaper people, and quirky eccentrics.

==Plot summary==
Mississippi lawyer and [[American Civil War|Civil War]] veteran, Basil Ransom, visits his cousin Olive Chancellor in Boston. She takes him to a political meeting where Verena Tarrant delivers a feminist speech. Ransom, a strong conservative, is annoyed by the speech but fascinated with the speaker. Olive, who has never before set eyes on Verena, is equally fascinated. She persuades Verena to leave her parents' house, move in with her and study in preparation for a career in the feminist movement. Meanwhile, Ransom returns to his [[law]] practice in New York, which is not doing well. He visits Boston again and walks with Verena through the [[Harvard]] College grounds, including the impressive Civil War [[Memorial Hall (Harvard University)|Memorial Hall]]. Verena finds herself attracted to the charismatic Ransom.

Basil eventually proposes to Verena, much to Olive's dismay. Olive has arranged for Verena to speak at the [[Boston Music Hall]]. Ransom shows up at the hall just before Verena is scheduled to begin her speech. He persuades Verena to elope with him, to the discomfiture of Olive and her fellow-feminists. The final sentence of the novel shows Verena in tears&nbsp;— not to be her last, James assures us.

==Themes==
Unlike much of James' work, ''The Bostonians'' deals with explicitly political themes: feminism and the general role of women in society. James was at best ambivalent about the feminist movement, and the early chapters harshly [[satire|satirize]] Olive and her fellow ideologues. Another theme in the book, much discussed recently, is Olive's possible [[lesbianism|lesbian]] attraction to Verena. (The term [[Boston marriage]], apparently first used here by James, came to connote just such an ambiguous co-habiting long-term relationship between two women.)  James is not explicit here, partially due to the conventions of the time. But this vagueness may actually enrich the novel because it creates possible ambiguity about Olive's motives.

As Ransom gets closer to winning Verena, he seems to lose at least some of his creator's sympathy. James was rather suspicious of the winners in life who scoop up all the goodies, especially the [[human sexuality|sexual]] goodies. He becomes more sympathetic to Olive in the later chapters as she begins to lose Verena. This is especially evident in chapter 39, where Olive experiences a painful recognition of her situation somewhat similar to Isabel Archer's long nighttime meditation in chapter 42 of ''[[The Portrait of a Lady]]''.

The three central characters are surrounded by a vivid supporting cast of would-be reformers, cynical journalists, and sometimes sinister hangers-on. James shows remarkable ability to create a broad cross-section of American society, which helps refute the charge that he could only handle small, closed-off bits of life.

The title refers, not to the people of Boston in general, but to the two characters Olive and Verena, "as they appeared to the mind of Ransom, the southerner, and outsider, looking at them from New York."<ref>James, Henry. [https://books.google.com/books?id=cLMS967ykasC&pg=PA121&lpg=PA121&dq=%22meant+only+to+designate+olive+and+verena+by+it%22&source=bl&ots=mrbY29Xyzd&sig=OCJesyuMA6nI9hIIZPtAs3jgnn0&hl=en&sa=X&ei=XM2gUpOCGemmsQTBtIGgBg&ved=0CC4Q6AEwAQ#v=onepage&q=%22meant%20only%20to%20designate%20olive%20and%20verena%20by%20it%22&f=false ''Henry James: Letters'']. Edited by [[Leon Edel]]. Volume III: 1883-1895. Harvard University Press, 1980. p. 121. Retrieved 5 December 2013.</ref>

==Critical evaluation==
''The Bostonians'' was not well received by contemporary [[critic]]s, especially on the [[Anglo-America|western side of the Atlantic]].<ref>Gooder, R. D. "Note on the Text." From ''The Bostonians,'' by Henry James. Oxford, NY: Oxford University Press, 1991. xxxiv. "The novel was a success neither financially nor critically (American readers were particularly offended) and it was not included in the New York Edition of James's novels and tales (1907-9)."</ref> James himself once wrote an observation that ''The Bostonians'' had never, "even to my much-disciplined patience, received any sort of justice."<ref>James, Henry. [https://books.google.com/books?id=bjnnpsVN4isC&pg=PT159&dq=%22received+any+sort+of+justice%22&hl=en&sa=X&ei=KZufUvfTLLPNsATMjYDADQ&ved=0CEsQ6AEwBQ#v=onepage&q=%22received%20any%20sort%20of%20justice%22&f=false ''The Letters of Henry James, Vol. II'']. Published by the Library of Alexandria (date unknown). Retrieved 4 December 2013.</ref> James' portrayal of Boston reformers was denounced as inaccurate and unfair, especially because some felt James had satirized actual persons in the novel. Darrel Abel observes that when the novel was first published in ''[[Century Magazine]]'' in 1885, the people of [[Boston]] were very displeased:

<blockquote>The Bostonians resented its [[satire]] upon their intellectual and humanitary aspirations. They resented the author's evident sympathy with his reactionary [[Southern United States|Southern]] hero - a sympathy perhaps partly picked up from the [[British people|British]], who admired the Southern gentleman more than the [[Yankee]] reformer. The Bostonians considered Miss Birdseye an insulting [[caricature]] of Miss [[Elizabeth Peabody]], the sister-in-law of [[Nathaniel Hawthorne|Hawthorne]], associate of [[Amos Bronson Alcott|Alcott]], and friend of [[Ralph Waldo Emerson|Emerson]], and therefore too sacrosanct a personage to be placed in a humorous light. But probably most offensive to Boston propriety were the unmistakable indications of [[Lesbianism]] in the portrait of Olive Chancellor, which made it a violation of Boston decency and reticence.<ref>Abel, Darrel. [https://books.google.com/books?id=VvxJOS9P4P0C&pg=PA275&dq=The+Bostonians+resented+its+satire+upon+their+intellectual+and+humanitary+aspirations.&hl=en&sa=X&ei=Wp-fUvL8H6jIsASxsYKQBA&ved=0CDEQ6AEwAA#v=onepage&q=The%20Bostonians%20resented%20its%20satire%20upon%20their%20intellectual%20and%20humanitary%20aspirations.&f=false ''Classic Authors of the Gilded Age'']. [[iUniverse]], 2002. Retrieved 4 December 2013.</ref>
</blockquote>

[[Horace Elisha Scudder]] reviewed the book in 1886, calling it an unfair treatment of characters whom the author simply did not like, although James had a definite interest in them:

<blockquote>When we say that most of the characters are repellent, we are simply recording the effect which they produce upon the reader by reason of the attitude which the author of their being takes toward them. He does not love them. Why should he ask more of us? But since he is extremely interested in them, and seems never wearied of setting them in every possible light, we also accede to this interest, and if we have time enough strike up an extraordinary intimacy with all parties. It is when this interest leads Mr. James to push his characters too near the brink of nature that we step back and decline to follow.<ref>Scudder, Horace Elisha. [https://www.theatlantic.com/past/docs/unbound/classrev/thebosto.htm "The Bostonians, by Henry James"]. ''[[The Atlantic Monthly]]''. June 1886. Retrieved 5 December 2013.</ref></blockquote>

[[Mark Twain]] vowed that he would rather be damned to [[John Bunyan|John Bunyan's]] heaven than read the book.<ref>[[Mark Twain|Clemens, S.L.]]: Letter to [[William Dean Howells|W.D. Howells]], July 21, 1885, ''Mark Twain's Letters Complete'' [Ed.  [[Albert Bigelow Paine]]]</ref> The letter in which Twain wrote this remark also contains invectives against the works of [[George Eliot]] and [[Nathaniel Hawthorne]]. [[Albert Bigelow Paine]] wrote in his annotation, "It is as easy to understand Mark Twain's enjoyment of [[Indian Summer (novel)|Indian Summer]] as his revolt against [[Daniel Deronda]] and The Bostonians. He cared little for writing that did not convey its purpose in the simplest and most direct terms."<ref>{{cite web|last=Paine|first=Albert Bigelow|title=MARK TWAIN'S LETTERS 1876-1885. Volume III.|url=http://www.gutenberg.org/files/3195/3195-h/3195-h.htm|work=Gutenberg.org}}</ref>

[[Rebecca West]] described the book, in her [[biography]] of Henry James, as "a foolish song set to a good tune in the way it fails to 'come off.'" She praised the book's language and themes, but thought the book's political content was strained and unnecessary; she believed that James, by emphasizing the political aspects of the subject matter, had inadvertently distracted the reader from what he actually had set out to say: "The pioneers who wanted to raise the small silvery song of art had to tempt their audiences somehow from the big brass band of America's political movements."<ref>West, Rebecca. [http://www.gutenberg.org/files/37300/37300-h/37300-h.htm ''Henry James'']. First published in 1916. Accessed 4 December 2013.</ref>

Later critics, though uncomfortable with the novel's rather static nature and perhaps excessive length, have found more to praise in James' account of the contest for Verena and his description of the wider background of feminism and other reform movements.<ref>[http://blog.loa.org/2011/02/henry-jamess-bostonians-sparks-scandal.html "Henry James’s The Bostonians, published 125 years ago today, sparks a scandal"]. ''Reader's Almanac.'' 15 February 2011. Retrieved 5 December 2013. "Later critics developed a keener appreciation."</ref> [[Edmund Wilson]] wrote in 1938, in his book ''The Triple Thinkers: Ten Essays on Literature'', "The first hundred pages of The Bostonians, with the arrival of the young Southerner in Boston and his first contacts with the Boston reformers, is, in its way, one of the most masterly things that Henry James ever did."<ref>Wilson, Edmund. [https://books.google.com/books?id=JLjqqTS2izUC&pg=PA106&lpg=PA106&dq=%22The+first+hundred+pages+of+The+Bostonians%22+edmund+wilson&source=bl&ots=UR4pk9awx3&sig=-8JkrwC6bOERVfnCz2lhDFmbzug&hl=en&sa=X&ei=9dOgUqXOBJPgsASZwYHgAQ&ved=0CDcQ6AEwAg#v=onepage&q=%22The%20first%20hundred%20pages%20of%20The%20Bostonians%22%20edmund%20wilson&f=false ''The Triple Thinkers: Ten Essays on Literature'']. Macmillan, 1976. p. 106. Retrieved 5 December 2013.</ref> The quiet but significant struggle between Olive Chancellor and Basil Ransom does seem more pertinent and engrossing today than it might have appeared to 19th century readers, because it records the struggles of a historical period that has had, we can now see, a profound impact upon the kind of country America has become.<ref>Gooder, R. D. "Introduction." From ''The Bostonians,'' by Henry James. Oxford, NY: Oxford University Press, 1991. xvii. "''The Bostonians'' is an analysis of the metamorphosis of American idealism in the [[Gilded Age]], an account of what America had become, and was becoming."</ref>

[[F. R. Leavis]] praised the book as "one of the two most brilliant novels in the [[English language|language]]," the other being James's [[The Portrait of a Lady]].<ref>[http://www.amazon.ca/The-Portrait-Lady-Henry-James/dp/0192100386 "The Portrait of a Lady."] ''Amazon.ca.'' Excerpt from front flap of Oxford World's Classics edition. Retrieved 4 December 2013.</ref> Leavis described it as "wonderfully rich, intelligent and brilliant...It could have been written only by James, and it has an overt richness of life such as is not commonly associated with him."<ref>[[A. S. Byatt|Byatt, A. S.]]. [https://www.theguardian.com/books/2003/sep/06/classics.asbyatt "The End of Innocence"]. ''[[The Guardian]]'' 5 September 2003. Retrieved 4 December 2013.</ref>

James bemoaned the adverse effect that this novel and ''[[The Princess Casamassima]]'' (published in the same year) had on his critical fortunes. Although he didn't turn away from political themes completely, he never again gave political ideas such a prominent place in his fiction.

''The Bostonians'' is allegedly based on the novel "The Evangelist," by [[Alphonse Daudet]].<ref>[http://www.britannica.com/EBchecked/topic/74969/The-Bostonians "The Bostonians"]. ''[[Encyclopedia Britannica]]''. Retrieved 4 December 2013.</ref><ref>[http://2020ok.com/books/59/the-bostonians-16959.htm "The Bostonians"]. ''2020ok.com.'' Retrieved 4 December 2013.</ref>

==Film version==
''[[The Bostonians (film)|The Bostonians]]'' was filmed in 1984 by the [[Merchant Ivory Productions|Merchant Ivory]] team (director [[James Ivory (director)|James Ivory]], producer [[Ismail Merchant]], writer [[Ruth Prawer Jhabvala]]) with [[Christopher Reeve]], [[Vanessa Redgrave]] and [[Madeleine Potter]] in the three central roles.

The movie earned mixed reviews, with a 60% "fresh" rating on [[Rotten Tomatoes]].<ref>http://www.rottentomatoes.com/m/bostonians/</ref> Vanessa Redgrave's performance received high marks, however, as well as nominations for the 1984 [[Golden Globe Award for Best Actress - Motion Picture Drama|Golden Globe]] and [[Academy Award]] for [[Academy Award for Best Actress|Best Actress]]. Further, the movie earned other award nominations for costume design and [[cinematography]].

The [[2005 in film|2005]] [[independent film|independent]] [[drama film]] ''[[The Californians (film)|The Californians]]'' is an updated adaptation of the story.<ref>http://www.rottentomatoes.com/m/the-californians/#!reviews=dvd</ref>

==Cultural references==
* A young Olive Chancellor appears as a character in ''[[The League of Extraordinary Gentlemen]]''.

==See also==
{{portal|Novels}}
* [[Boston marriage]]
* [[Misogyny]]

==References==
<references/>
* ''A Henry James Encyclopedia'' by Robert Gale (New York: Greenwood Press, 1989) ISBN 0-313-25846-5
* ''The Novels of Henry James'' by [[Edward Wagenknecht]] (New York: Frederick Ungar Publishing Co., 1983) ISBN 0-8044-2959-6

==External links==
{{Wikisource}}
* [http://cdl.library.cornell.edu/cgi-bin/moa/pageviewer?coll=moa&root=/moa/cent/cent0029/&tif=00540.TIF&view=50&frames=1 Original magazine publication of ''The Bostonians'' (1885-86)]
* [http://www2.newpaltz.edu/~hathawar/bostonians1.html First book version of ''The Bostonians'' (1886)]
* [http://www.loa.org/volume.jsp?RequestID=57&section=notes Note on the various texts of ''The Bostonians''] at the [[Library of America]] web site
* [http://www.imdb.com/title/tt0086992/ IMDb page for the movie version of ''The Bostonians'' (1984)]
* {{librivox book | title=The Bostonians | author=Henry JAMES}}

{{Henry James}}

{{Authority control}}

{{DEFAULTSORT:Bostonians, The}}
[[Category:American novels adapted into films]]
[[Category:1886 novels]]
[[Category:Novels set in Boston]]
[[Category:Novels by Henry James]]
[[Category:Novels first published in serial form]]
[[Category:Films set in Massachusetts]]
[[Category:19th-century American novels]]
[[Category:Works originally published in The Century Magazine]]
[[Category:Boston in fiction]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]