{{DISPLAYTITLE:Partners in Crime (''Doctor Who'')}}
{{Use dmy dates|date=November 2012}}
{{Infobox Doctor Who episode
| number = 189
| serial_name = Partners in Crime
| image = [[Image:Adipose.jpg|275px]]
| caption = The [[List of Doctor Who monsters and aliens#Adipose|Adipose]], [[computer generated imagery|CGI]] aliens depicted using [[MASSIVE (software)|MASSIVE]], march through Central London towards Adipose Industries.
| show = DW
| type = episode
| doctor = [[David Tennant]] ([[Tenth Doctor]])
| companion = [[Catherine Tate]] ([[Donna Noble]])
| guests =
* [[Billie Piper]]&nbsp;– [[Rose Tyler]]
* [[Sarah Lancashire]]&nbsp;– [[List of Doctor Who villains#Matron Cofelia|Ms. Foster]]
* [[Bernard Cribbins]]&nbsp;– [[Wilfred Mott]]
* [[Jacqueline King]]&nbsp;– [[Sylvia Noble]]
* [[Verona Joseph]]&nbsp;– Penny Carter
* [[Jessica Gunning]]&nbsp;– Stacey Campbell
* [[Martin Ball]]&nbsp;– Roger Davey
* Rachid Sabitri&nbsp;– Craig Staniland
* Chandra Ruegg&nbsp;– Claire Pope
* Sue Kelvin&nbsp;– Suzette Chambers
* Jonathan Stratt&nbsp;– Taxi driver
| writer = [[Russell T Davies]]
| director = [[James Strong (director)|James Strong]]
| producer = [[Phil Collinson]]
| executive_producer = [[Russell T Davies]]<br />[[Julie Gardner]]
|composer=[[Murray Gold]]
| script_editor = Lindsey Alford
| production_code = 4.1<ref name="30reasons">{{Cite journal
 | date= 6 March 2008
 | title= 30 reasons to be excited about Series 30!
 | journal= [[Doctor Who Magazine]]
 | issue= 393
 | pages= 9–10
}}</ref>
| date = 5 April 2008
| length =  50 minutes
| preceding = "[[Voyage of the Damned (Doctor Who)|Voyage of the Damned]]"
| following = "[[The Fires of Pompeii]]"
| series = [[Doctor Who (series 4)|Series 4]]
| series_link = Series 4 (2008)
}}
"'''Partners in Crime'''" is the first episode of the [[Doctor Who (series 4)|fourth series]] of the British [[science fiction television]] series ''[[Doctor Who]]''. It was broadcast on [[BBC One]] on 5 April 2008. The episode reintroduced comedian [[Catherine Tate]] as [[Donna Noble]], who had previously appeared in "[[The Runaway Bride (Doctor Who)|The Runaway Bride]]". Donna and the [[Doctor (Doctor Who)|Doctor]] ([[David Tennant]]) meet while separately investigating Adipose Industries, a company that has created a revolutionary [[Anti-obesity medication|diet pill]]. Together, they attempt to stop the death of thousands of people in London after the head of the company, the alien [[Miss Foster]] ([[Sarah Lancashire]]), creates the [[List of Doctor Who monsters and aliens#Adipose|Adipose]], short white aliens made from human [[body fat]].

The episode's alien creatures, the Adipose, were created using the software ''[[MASSIVE (software)|MASSIVE]]'', commonly used for crowd sequences in fantasy and science fiction films.

"Partners in Crime" features the return of three supporting characters: [[Jacqueline King]] reprises her role as [[Sylvia Noble]] from "[[The Runaway Bride (Doctor Who)|The Runaway Bride]]"; [[Bernard Cribbins]] reprises his role as [[Wilfred Mott]] from "[[Voyage of the Damned (Doctor Who)|Voyage of the Damned]]", to replace the character of Geoff Noble after actor Howard Attfield died; and [[Billie Piper]] briefly reprises her role as [[Rose Tyler]] for the first time since the second series' finale "[[Doomsday (Doctor Who)|Doomsday]]", in a scene that was not included in preview showings.

The episode received many positive reviews. Most critics liked the special effects used to create the Adipose. Critics also praised Tate's subdued acting in comparison to "The Runaway Bride"; Donna was changed from a "shouting [[wikt:fishwife|fishwife]]"<ref name="noblecalling" /> to a more emotional person when she became a full-time companion. Critics' opinions were split over the episode's plot: opinion on executive producer [[Russell T Davies]]' writing ranged from "pure pleasure"<ref name="preston"/> to "the back of a [[cigarette pack|fag packet]]".<ref name="hyland"/>

==Plot==

[[Donna Noble]] has become disenchanted with her normal life for two years since meeting the Doctor in "[[The Runaway Bride (Doctor Who)|The Runaway Bride]]". She finds herself regretting her decision to decline the Doctor's invitation to travel in the [[TARDIS]]. She has started investigating conspiracy theories in the hope that she will find him again. She confides her regrets to her grandfather [[Wilfred Mott]], who met the Doctor before in "[[Voyage of the Damned (Doctor Who)|Voyage of the Damned]]".

The Doctor and Donna, neither one aware of the other's involvement, both investigate Adipose Industries, which is marketing a special [[diet pill]] to the people of London. They find that the pill's slogan, ''The Fat Just Walks Away'', is literal. The pills use latent body fat to [[parthenogenesis|parthenogenetically]] create small white aliens called Adipose that spawn at night and leave the host's body. The Doctor and Donna separately infiltrate the offices of Adipose Industries, each unaware that the other is there. Donna hides out in a toilet cubicle until they close, the Doctor stays in a storage room. As they explore the building, they suddenly encounter each other through opposite windows in an office. They are confronted by Miss Foster, an alien who is using Britain's overweight population to create the Adipose babies for the Adiposian First Family. Miss Foster pursues the Doctor and Donna around the building, finally catching them in an office. She tells the Doctor that the Adipose [[Story arcs in Doctor Who#Medusa Cascade|lost their breeding planet]] and hired Miss Foster to find a replacement. The Doctor uses Miss Foster's sonic pen and his [[sonic screwdriver]] to create a diversion and escape.

Miss Foster accelerates her plans, knowing that the Doctor will attempt to stop her. Throughout London, the Adipose begin to spawn and soon number several thousand. The Doctor and Donna prevent total emergency parthenogenesis occurring, which would have killed those who had taken the pill, and the remainder of the young Adipose make their way to Adipose Industries. The Adiposian First Family arrive in a spaceship and begin collecting their young. The Doctor tries to warn Miss Foster about her safety, but she disregards him and is killed when the Adipose drop her from their transport beam to her death, to cover their unsanctioned colonization efforts. The Doctor refrains from killing the young Adipose because they are children, to which Donna remarks that his previous companion [[Martha Jones]] made him more human.

At the end of the episode, Donna accepts the Doctor's original offer to travel in the TARDIS. The Doctor makes it clear to her that he only wants to be friends and nothing more, and she emphatically agrees. She makes a detour to leave her car keys in a litter bin, telling her mother Sylvia to collect them later. While there, she meets a blonde woman and asks her to help Sylvia find the keys. The woman turns out to be [[Rose Tyler]], who fades from view as she walks away from the area. In the final scene, Donna asks the Doctor to fly by her grandfather, Wilfred. He sees her in his telescope and celebrates on his [[Allotment (gardening)|allotment]].

== Production ==

=== Casting ===
"Partners in Crime" features several actors returning to the series. Catherine Tate was offered the opportunity to return as Donna Noble during lunch with executive producer [[Julie Gardner]]. Tate, who expected Gardner would ask about appearing in a [[Biographical film|biopic]], later admitted it was "the furthest thing from [her] mind".<ref name="pod">{{cite video
 |people    = [[David Tennant|Tennant, David]]; [[Catherine Tate|Tate, Catherine]]; [[Phil Collinson|Collinson, Phil]]
 |date      = 5 April 2008
 |title     = Partners In Crime
 |medium    = Podcast; MP3
 |publisher = BBC
}}</ref> Tate's return was controversial amongst ''Doctor Who'' fans; the criticism she received was compared to [[Daniel Craig]] after he was cast as [[James Bond (character)|James Bond]].<ref>{{cite news|url=http://entertainment.uk.msn.com/tv/galleries/gallery.aspx?cp-documentid=7349404|title=Catherine Tate&nbsp;— Are You Bovvered?|accessdate=7 April 2008|first=Lorna|last=Cooper|publisher=[[MSN]]|work=MSN UK Entertainment|location=United Kingdom| archiveurl = https://web.archive.org/web/20080409125102/http://entertainment.uk.msn.com/tv/galleries/gallery.aspx?cp-documentid=7349404| archivedate = 9 April 2008}}</ref> Howard Attfield, who appeared as Donna's father Geoff in "The Runaway Bride", filmed several scenes for this episode, but died before his scenes for the remainder of the season were completed. The producers retired his character out of respect, and dedicated him in the closing credits for the episode.<ref name="pod" /> Producer [[Phil Collinson]] suggested transferring his traits to the unrelated character Stan Mott from "[[Voyage of the Damned (Doctor Who)|Voyage of the Damned]]", and rewriting his role as Donna's grandfather. Executive producers [[Russell T Davies]] and Gardner liked the idea and recalled Bernard Cribbins to the role to re-film Attfield's scenes,<ref name="pod" /> with the character renamed as Wilfred—a name Davies favoured for Donna's grandfather—in time for the credits of "Voyage of the Damned" to be changed.<ref name="EpisodeOne">{{cite episode |title=Episode 1 |series=Doctor Who: The Commentaries |serieslink=Doctor Who: The Commentaries |network=[[BBC]] |station=[[BBC 7]] |airdate=2008-04-06 |season=1 |number=1}}</ref>

=== Writing ===
{{quote box|quote=I see her as a slightly warped Mary Poppins. She's quite austere. She's a strong woman. When I first read the script, I thought, oh, well, of course she's a baddie... but the more I read it, I thought, 'No, she's doing what she's doing for legitimate reasons.'|source=[[Sarah Lancashire]]<ref name="supernanny">{{cite journal|author = Cook, Benjamin|author2=Lancashire, Sarah |authorlink2=Sarah Lancashire |date=April 2008|title=Power Madam!|journal=[[Doctor Who Magazine]]|publisher=[[Panini Comics]]|location=[[Tunbridge Wells]], [[Kent]]|volume=394|pages=30–31|authorlink= Benjamin Cook (journalist)}}</ref>|width=400px}}

Davies took a different approach while writing the episode. David Tennant and Sarah Lancashire noted the character of Miss Foster had good intentions but was morally ambiguous.<ref name="confidential" /><ref name="supernanny" /> The premise of the Adipose pill was equally ambiguous with rare side-effects, but was a "win-win situation" for anyone involved.<ref name="confidential">{{cite episode |title=A Noble Return |series=Doctor Who Confidential |serieslink=Doctor Who Confidential |network=BBC |station=BBC Three |airdate=2008-04-05 |season=4 |number=1  }}</ref> Davies based the character of Miss Foster on ''[[Supernanny]]'' star [[Jo Frost]] and [[Argentina|Argentine]] philanthropist and politician [[Eva Perón]], and Lancashire compared her character to [[Mary Poppins (character)|Mary Poppins]].<ref name="confidential" /> The Adipose are a different style to regular ''Doctor Who'' villains; antagonists such as Lazarus in "[[The Lazarus Experiment]]" or the werewolf in "[[Tooth and Claw (Doctor Who)|Tooth and Claw]]" were singular monsters designed to scare the audience; the Adipose were written as "cute" to provide a "bizarre [and] surreal" experience.<ref name="confidential" />

Davies made some changes to Donna's character. The character was "rounded&nbsp;... out from being a shouting [[wikt:fishwife|fishwife]] to someone who's quite vulnerable and emotional".<ref name="noblecalling">{{cite journal |date=April 2008 |title=A Noble Calling |journal=[[Radio Times]] |issue=5–11 April 2008  |page=23 |publisher=BBC }}</ref> Donna was written to provide a "caustic" and "grown-up" attitude towards the Doctor, in opposition to Rose and Martha, who fell in love with him. Tate considered Donna to be more equal to the Doctor because her character did not romanticise him, allowing her to question his morality more easily.<ref name="confidential" />

==== Donna's mime ====
In this production, the script requires Catherine Tate, as Donna Noble, to reintroduce herself to the Doctor in mime.  The stage directions by writer Russell T Davies are as follows.<ref name="confidential" />

{{quote|Donna does a little mime: "I came here, trouble, read about it, internet, I thought, trouble <nowiki>=</nowiki> you! And this place is weird! Pills!  So I hid.  Back there.  Crept along.  Looked.  You.  Cos they..."|Russell T Davies|"Partners in Crime" shooting script.<ref name="confidential" />}}

Tate says Davies had suggested that she might come up with something on the day.  She improvised her [[mime]] during filming.<ref name="confidential" />

=== Filming ===
{{multiple image
 | align     = right
 | direction = horizontal
 | header    = <center>Location of Adipose Industries</center>
 | header_align = center
 | header_background =
 | footer    =
 | footer_align = left
 | footer_background =
 | width     =
 | image1    = Churchill Way, Cardiff - geograph.org.uk - 1001848.jpg
 | width1    = 182
 | alt1      =
 | caption1  = <center>Churchill Way, Cardiff</center>
 | image2    = Helmont House Cardiff.JPG
 | width2    = 106
 | alt2      =
 | caption2  = <center>Helmont House</center>
}}

The episode was in the fourth production block in the season, and was filmed in October 2007. The out-of-sequence filming allowed producers to use props to "seed" later episodes; ATMOS, a plot device in the episodes "[[The Sontaran Stratagem]]" and "[[The Poison Sky]]", is referred to by a sticker on a taxi's windscreen.<ref name="pod" /><ref>{{cite web|url=http://www.bbc.co.uk/doctorwho/s4/episodes/S4_04|title=The Sontaran Strategem: Fact File|work=Doctor Who microsite|publisher=BBC|accessdate=2 July 2008}}</ref> As the episode mostly takes place at night, many scenes were filmed in the early morning.<ref name="confidential" />

The scene where Donna and the Doctor investigate Adipose was difficult to film. The scene took thirty shots to complete, and Tennant and Tate experienced problems avoiding each other on-screen. The scene was filmed in Picture Finance's call centre on the outskirts of [[Newport, Wales|Newport]] on an early Sunday morning, with the company's telephonists serving as extras.<ref name="pod" /><ref>{{cite web|last=Sullivan|first=Shannon Patrick|year=2006a|title=Partners In Crime|series=A Brief History of Time (Travel)|publisher=ShannonSullivan.com|location=St. John's|url=http://www.shannonsullivan.com/drwho/serials/2008a.html|publication-date=2009-10-17|accessdate=17 April 2011}}</ref>

Exterior shots of Adipose Industries were filmed at the British Gas building ([[Helmont House]]) in [[Cardiff city centre|Cardiff's city centre]]. For health and safety reasons, Tennant was prohibited from performing his own stunts in the window cleaning platform. His only shot that required stunts was when he catches Miss Foster's sonic pen, a shot that took several takes to perfect.<ref name="confidential" />

=== Adipose ===
The Adipose were inspired by a stuffed toy Davies owned.<ref name="confidential" /> The name comes from the scientific name for body fat, [[adipose tissue]].<ref name="littlemonsters">{{cite journal |date=April 2008 |title=Little Monsters? |journal=[[Radio Times]] |issue=5–11 April 2008  |page=25 |publisher=BBC }}</ref> Davies' brief outlined a "cute" child-friendly creature shaped like a block of [[lard]], similar to the [[Pillsbury Doughboy]].<ref name="makingtheadipose" /><ref name="factfile">{{cite web|url=http://www.bbc.co.uk/doctorwho/s4/episodes/?episode=s4_01&action=factfile|title=Partners in Crime: Fact File|publisher=BBC|date=5 April 2008|accessdate=9 April 2008|work=Doctor Who microsite}}</ref> Further consultation with [[post-production]] team [[The Mill (post-production)|The Mill]] resulted in the ears and the singular fang each Adipose has.<ref name="makingtheadipose">{{cite video|url=http://www.bbc.co.uk/doctorwho/s4/episodes/?episode=S4_01&character=&action=videostream&playlist=/doctorwho/playlists/s4_01/video/s4_01_u_04.xml&video=1|people=McKinney, Matt|title=Making the Adipose|date=5 April 2008|accessdate=6 April 2008|publisher=BBC|format=FLV}}</ref> [[Stephen Regelous]], who won an [[Academy Award]] for his software ''[[Massive (software)|Massive]]'', flew to London to supervise the creation of the crowd special effects.<ref name="littlemonsters" /> Regelous, a ''Doctor Who'' fan, was enthusiastic about helping The Mill with special effects, stating that "When I first found out that the Mill was working on ''Doctor Who'', I was quietly hoping that Massive might be used to create hordes of Daleks or Cybermen and with series 4, I jumped at the opportunity to be involved."<ref name="sneakpeek">{{cite web|url=http://blogs.guardian.co.uk/organgrinder/2008/04/doctor_who_a_special_effects_s.html|title=Doctor Who: a special effects sneak peek|date=3 April 2008|first=Stephen|last=Brook|accessdate=6 April 2008|publisher=''[[The Guardian]]''|work=Organgrinder}}</ref> The Mill created two types of Adipose: extras with artificial intelligence and independent movement, and "hero" Adipose, which were hand-animated.<ref name="littlemonsters" />

== Broadcast and reception ==
[[Image:Billie Piper in October 2006-Edited.JPG|thumb|upright|Tennant described [[Billie Piper]]'s return as a "genuine prickle up the spine".]]

=== Broadcast and ratings ===
The episode was broadcast on 5 April 2008 at 18:20, the earliest timeslot since the show's revival in 2005. Davies criticised the BBC's scheduling department and claimed the show could lose 1.5&nbsp;million viewers.<ref>{{cite web|url=http://www.doctorwhonews.net/2008/03/davies-criticizes-time-slot_5284.html|title=Davies criticizes Time Slot|first=Russell T|last=Davies|authorlink=Russell T Davies|author2=Marcus|date=25 March 2008|accessdate=9 July 2011|publisher=''[[Ariel (newspaper)|Ariel]]'', [[The Doctor Who News Page]]}}</ref> The show retained a similar time of broadcast for a further four episodes, before returning to around 19:00. from "[[The Doctor's Daughter]]" onwards.<ref>{{cite web|url=http://www.doctorwhonews.net/2008/04/time-slot-will-change-later-in-series_6380.html|title=Time Slot will change later in Series|date=2 April 2008|accessdate=9 July 2011|first=Matt|last=Hilton|publisher=[[The Doctor Who News Page]]}}</ref><ref>Broadcast times - [http://www.digitalspy.co.uk/cult/a95244/s04e05-the-poison-sky.html "The Poison Sky"] and [http://www.digitalspy.co.uk/cult/a95290/s04e06-the-doctors-daughter.html "The Doctor's Daughter"]</ref>

The preview version of the episode supplied to the press and aired at the press launch omitted the scene that features Rose; before broadcast, only the production team, Tate, and Tennant had seen the scene.<ref name="pod" /> The scene contains Rose's departure [[motif (music)|theme]], "[[Doomsday (Doctor Who)#Music|Doomsday]]". Tennant commented "on the night of transmission&nbsp;... the ''[[Radio Times]]'' won't have told you it's coming, it'll come as a genuine&nbsp;[...] prickle up the spine".<ref name="pod" />

Overnight figures estimated the show was watched by 8.4&nbsp;million viewers, with a peak of 8.7&nbsp;million, 39.4% of the television audience. The consolidated rating was 9.1 million viewers. ''Doctor Who'' was therefore the most watched show on 5 April, although the [[Grand National]] had a higher peak with 10.1&nbsp;million viewers.<ref>{{cite web|url=http://www.doctorwhonews.net/2008/04/partners-in-crime-final-ratings_496.html|title=Partners in Crime&nbsp;— Final Ratings|date=16 April 2008|accessdate=9 July 2011|first=Matt|last=Hilton|publisher=[[The Doctor Who News Page]]}}</ref><ref name="OG">{{cite web|url=http://www.doctorwhonews.net/2008/04/partners-in-crime-overnight-ratings_7835.html|title=Partners in Crime&nbsp;— Overnight Ratings|author=Marcus|publisher=[[The Doctor Who News Page]]|date=6 April 2008|accessdate=9 July 2011}}</ref><ref name="AP">{{cite news|url=http://www.telegraph.co.uk/news/uknews/1584232/Doctor-Who-draws-in-8.4m-viewers.html|title=8.4m viewers see Doctor Who return|publisher=[[The Daily Telegraph|The Telegraph]]|location=United Kingdom|date=6 April 2008|accessdate=9 July 2011}}</ref><ref name="BBC">{{cite news|url=http://news.bbc.co.uk/1/hi/entertainment/7333321.stm|title= Doctor Who attracts 8.4m viewers|publisher=BBC|date=6 April 2008|accessdate=6 April 2008}}</ref> The episode's [[Appreciation Index]] was 88 (considered "Excellent"), the highest for any television show aired on 5 April.<ref name="PICAI">{{cite web|url=http://www.doctorwhonews.net/2008/04/partners-in-crime-ai-figure-and-ratings_7809.html |date=7 April 2008|accessdate=9 July 2011|author=Marcus|publisher=[[The Doctor Who News Page]]|title=Partners in Crime: AI figure and Ratings update}}</ref>

=== Critical reception ===
The episode received many positive reviews. John Preston, writing for ''[[The Daily Telegraph]]'', called the episode an "undiluted triumph". Opening his review, he said "last night's episode struck me as being as close to 50&nbsp;minutes of pure pleasure as you're likely to get on television". He noted the episode's clever tackling of the topical theme of obesity, and its mixture of emotion and special effects. In closing, he said "the dejected critic, denied even the smallest nit to pick, walks glumly away".<ref name="preston">{{cite news|url=http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2008/04/06/nwho206.xml|title=Doctor Who is as close to pure pleasure as you'll get on television|date=6 April 2008|accessdate=7 April 2008|first=John|last=Preston|publisher=''[[The Daily Telegraph]]''}}</ref> Scott Matthewman of ''[[The Stage]]'' lamented that the Adipose were not threatening enough. He liked the Adipose's execution of Miss Foster, a "momentary pause in mid-air, gravity only kicking in when the character looks down", comparing it to [[Wile E. Coyote]] and [[Chuck Jones]], which "[was] a nice little touch in an episode&nbsp;... full of them". He also appreciated Tate, saying that "David Tennant finally has a partner who is approaching an equal".<ref>{{cite web|url=http://blogs.thestage.co.uk/tvtoday/2008/04/doctor-who-41-partners-in-crime/|title=Doctor Who 4.1: Partners in Crime|publisher=''[[The Stage]]''|first=Scott|last=Matthewman|work=TV Today|date=6 April 2008|accessdate=7 April 2008}}</ref>  Sam Wollaston of ''[[The Guardian]]'' wrote that Tate was "not right for this role" and "too hysterical, too comedy, not cool enough", and felt her inclusion was an attempt to trade on the popularity of her own series and "broaden the appeal of [Dr Who] still further". He also found the music "a bit oppressive" but concluded that, despite these criticisms, the show was "still awfully nice television".<ref>{{cite news|url=https://www.theguardian.com/media/2008/apr/07/television.tvandradioarts
|title=The weekend's TV|date=7 April 2008|accessdate=7 April 2008|first=Sam|last=Wollaston|publisher=''[[The Guardian]]''}}</ref> Keith Watson of ''[[Metro (Associated Metro Limited)|Metro]]'' gave the episode 4 stars out of 5. He admitted that despite his dislike of Tate, "she isn't that bad". His review of the Adipose was positive, citing them as a reason of the quality of the show. Closing, he said "it split [his] sides".<ref>{{cite news|publisher=[[Associated Newspapers Ltd]]|work=[[Metro (Associated Metro Limited)|Metro]], MetroLife|page=28|title=The Weekend's TV: A right Who ha|url=http://www.metro.co.uk/metrolife/article.html?in_article_id=136391&in_page_id=9|first=Keith|last=Watson|date=7 April 2008|accessdate=7 April 2008}}</ref>

Jon Wise of ''[[The People]]'' said "Doctor Who is a super-galactic way of spending a Saturday night indoors", and appreciated that Donna was not romantically interested in the Doctor, unlike Martha or Rose.<ref>{{cite news|url=http://www.people.co.uk/showbiz/wise/tm_headline=tardis-lands-in-spot-of-bovver%26method=full%26objectid=20374671%26siteid=93463-name_page.html|title=Tardis in a spot of bovver|first=Jon|last=Wise|publisher=''[[The People]]''|date=6 April 2008|accessdate=7 April 2008}}</ref> Ben Rawson-Jones gave the episode a wholly positive review, summarising it as containing "pure fantastic family fun, delivering a winning blend of action, comedy, poignancy and one unexpected shock cameo".<ref>{{cite web|url=http://www.digitalspy.co.uk/cult/a93160/s04e01-partners-in-crime.html|title=S04E01: 'Partners In Crime'|work=Cult: Doctor Who|publisher=[[Digital Spy]]|date=April 2008|accessdate=7 April 2008|first=Ben|last=Rawson-Jones}}</ref>

The episode received several negative reviews. Andrew Billen, writing for ''[[The Times]]'', lamented that Davies had "forgotten that Doctor Who's main task is to send children [[behind the sofa|scuttling behind sofas]] while entertaining their fathers with the odd philosophical idea, the occasional classical reference, a joke or two they would probably not wish to explain and a wee bit of space totty". Billen also criticised the writing and acting, but commended Tate for a "toned down performance".<ref>{{cite news|url=http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/tv_and_radio/article3684082.ece|title=Doctor Who; Louis Theroux's African Hunting Holiday|date=7 April 2008|accessdate=7 April 2008|first=Andrew|last=Billen|publisher=''[[The Times]]''}}</ref> Alan Stanley Blair of [[SyFy Portal]] summarised it as "a runaway Saturday morning cartoon in desperate need to a solid story". Blair found flaws with the comedy and the music in the episode, but was impressed with Tate's acting and Piper's cameo.<ref>{{cite web|url=http://www.syfyportal.com/news424905.html|title=Review: 'Doctor Who' - Partners In Crime|first=Alan Stanley|last=Blair|publisher=[[SyFy Portal]]|date=5 April 2008|accessdate=7 April 2008|archiveurl = https://web.archive.org/web/20080409183841/http://www.syfyportal.com/news424905.html |archivedate = 9 April 2008|deadurl=yes}}</ref> Kevin O'Sullivan of the ''[[Sunday Mirror]]'' criticised Tate and Tennant for overacting, and had concerns about the writing: "It didn't exactly ooze tension. All we got in the way of terrifying space enemies was Sarah Lancashire hamming it up as an intergalactic super nanny, a couple of security guards with guns and lots of cute little fat babies."<ref>{{cite web|url=http://www.mirror.co.uk/lifestyle/staying-in/what-s-on-tv/not-bovvered-by-the-doctor-301530|title=Not bovvered by the Doctor|first=Kevin|last=O'Sullivan|date=6 April 2008|accessdate=7 April 2008|publisher=''[[Sunday Mirror]]''}}</ref> [[Ian Hyland]] of ''[[News of the World]]'' criticised the child-friendly storyline, comparing it to "the back of a [[cigarette pack|fag packet]]". He also criticised Tennant for appearing "jaded" and Tate for "still shouting".<ref name="hyland">{{cite web|url=http://blogs.notw.co.uk/hyland/2008/04/tates-on-board.html|title=Tate's on board, I ain't bothered|publisher=''[[News of the World]]''|first=Ian|last=Hyland|authorlink=Ian Hyland|date=6 April 2008|accessdate=7 April 2008| archiveurl = https://web.archive.org/web/20080409213203/http://blogs.notw.co.uk/hyland/2008/04/tates-on-board.html| archivedate = 9 April 2008}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
{{Wikiquote|Tenth Doctor}}
{{TardisIndexFile|Partners in Crime}}
* {{BBCDWnew | year=2008 | id=S4_01 | title=Partners in Crime }}
* {{Brief|id=2008a|title=Partners in Crime|quotes=y}}
* {{Doctor Who RG|id=who_tv33|title=Partners in Crime|quotes=y}}
* {{IMDb episode|1159991|Partners in Crime}}
* [http://www.thewriterstale.com/pdfs/Doctor%20Who%204%20Ep.1%20-%20Shooting%20Script%20-%20Yellow%20-%2016.10.07.pdf Shooting Script for "Partners in Crime"]

=== Reviews ===
* {{DWRG| id=partnerscrime | title=Partners in Crime | quotes=y}}

{{Doctor Who episodes|N4}}
{{Doctor Who episodes by Russell T Davies |state=autocollapse}}

{{featured article}}

{{DEFAULTSORT:Partners In Crime (Doctor Who)}}
[[Category:Tenth Doctor episodes|Partners in Crime]]
[[Category:2008 British television episodes]]
[[Category:Screenplays by Russell T Davies]]
[[Category:Doctor Who stories set on Earth]]