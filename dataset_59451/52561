{{Infobox journal
| title = Physical Review X
| cover = 
| editor = [[Jean-Michel Raimond and M. Cristina Marchetti]]
| discipline = [[Physics]]
| abbreviation = Phys. Rev. X
| publisher = [[American Physical Society]]
| country = United States
| frequency = Quarterly
| history = 2011-present
| openaccess = Yes
| license = [[Creative Commons Attribution 3.0 License]]
| impact = 8.385
| impact-year = 2013
| website = http://journals.aps.org/prx/
| link1 = http://journals.aps.org/prx/issues/current
| link1-name = Online access
| link2 = http://journals.aps.org/prx/issues
| link2-name = Online archive
| formernames = 
| JSTOR = 
| OCLC = 706478714
| LCCN = 2011201149
| CODEN = PRXHAE
| ISSN =
| eISSN = 2160-3308
}}
'''''Physical Review X''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] published by the [[American Physical Society]] covering all aspects of [[physics]]. It is part of the ''[[Physical Review]]'' family of journals.<ref>{{cite web |title=APS Journals |url=http://journals.aps.org/about |publisher=American Physical Society |accessdate=3 December 2014}}</ref> The lead editors are [[Jean-Michel Raimond]] ([http://www.upmc.fr/en/university.html Université de Pierre et Marie Curie]) and [http://biomaterials.syr.edu/research/marchetti/ M. Cristina Marchetti] (Syracuse University). According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 8.701.<ref name=WoS>{{cite book |year=2014 |chapter=Physical Review X |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== History ==
The journal was first announced in January 2011<ref>{{cite web |title=APS Announces ''Physical Review X'', an Open Access Journal covering Physics and its Application to Related Fields |url=http://publish.aps.org/edannounce/prx-launch |publisher=American Physical Society |accessdate=3 December 2014 |date=19 January 2011}}</ref> and began publishing papers in August 2011. An early editorial outlined the journal's publishing philosophy.<ref>{{cite web |last1=Pullin |first1=Jorge |last2=Miao |first2=Ling |title=''Physical Review X'': What does it offer? Some opening words from the editors |url=http://publish.aps.org/edannounce/prx-what-does-it-offer |publisher=American Physical Society |accessdate=3 December 2014}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.aps.org/prx/}}

[[Category:Publications established in 2011]]
[[Category:Physics journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:American Physical Society academic journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Online-only journals]]