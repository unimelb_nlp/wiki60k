{{Infobox museum
| name             = Faena Art Center
| native_name      = 
| native_name_lang = 
| image            = Faena Arts Center.jpg
| imagesize        = 
| caption          = Exterior view of the Faena Art Center building in Buenos Aires
| alt              = 
| map_type         = Argentina Buenos Aires
| map_caption      = Location within Buenos Aires
| map_alt          = 
| map_size         =
| coordinates      = {{coord|-34.6137|-58.3616|display=inline}}
| established      = 
| dissolved        = 
| location         = Aimé Paine 1169, [[Puerto Madero]], Buenos Aires
| type             = [[Arts centre]]
| collection       = 
| visitors         = 
| director         = Ximena Caminos
| president        = 
| curator          = 
| owner            = [[Faena Group]]
| publictransit    = [[Leandro N. Alem (Buenos Aires Metro)]], [[Line B (Buenos Aires Metro)]]
| car_park         = 
| network          = 
| website          = {{ URL | http://www.faenaartscenter.org/ | Faena Arts Center - Official Website }}
}}

'''Faena Art Center''' is the cultural center of the Faena District Buenos Aires, a residential and cultural community in the [[Puerto Madero]] waterfront in [[Buenos Aires]] developed by the Faena Group and opened in September 2011. [[Alan Faena]] founded the center. [[Ximena Caminos]] is the Executive Director.<ref>{{citation|url=https://www.nytimes.com/2015/03/19/arts/artsspecial/in-miami-beach-the-faena-forum-is-to-open-in-december.html|title=In Miami Beach, the Faena Forum is to Open in December|author=Ted Loos|work=New York Times|year=2015|accessdate=2015-03-17}}</ref>

==History of the Building & Renovation==
Faena Art Center was built out of one of [[Argentina]]’s first big flour mills. In what used to be the mill’s old machine room, the main exhibition space has been rebuilt to retain the original details from the 1900s. The spacious, light-filled center, which is over 4,000 m<sup>2</sup>, is housed by soaring ceilings, semicircular arches, bay windows and other hallmarks of turn-of-the-century industrial architecture.<ref>[http://www.blackbookmag.com/guides/details/faena-arts-center "Faena Arts Center"]. ''BlackBookMag.com''.</ref><ref name=inhabitat>{{cite web|last=Alperovich|first=Ana Lisa|title=Ana Lisa Alperovich Buenos Aires' Faena Arts Center Opens in a Renovated Mill With a Colorful Interactive Installation|url=http://inhabitat.com/buenos-aires-faena-arts-center-opens-in-a-recycled-mill-with-a-colorful-interactive-installation/|publisher=inhabitat.com|accessdate=February 6, 2013|date=February 14, 2012}}</ref>

==Past Exhibitions==
Faena Art Center presents works by international artists in order to create a dialogue with the Argentinean scene, as well as offering local artists the chance to debut. The inaugural exhibition featured a monumental installation by Brazilian contemporary artist [[Ernesto Neto]], curated by the Tate Modern’s Jessica Morgan.<ref name=argindep>{{cite web|last=Hay|first=Blanka|title=Ernesto Neto at Faena Art Centre|url=http://www.argentinaindependent.com/the-arts/art/ernesto-neto-at-faena-art-centre/|publisher=argentinaindependent.com|accessdate=February 6, 2013|date=October 27, 2011}}</ref><ref name=artobserved>{{cite web|last=Stone|first=A.|title=Buenos Aires: Ernesto Neto ‘Crazy Hyperculture in the Vertigo of the World’ at Faena Art Center through February 12, 2012|url=http://artobserved.com/2012/02/buenos-aires-ernesto-neto-crazy-hyperculture-in-the-vertigo-of-the-world-at-faena-art-center-through-february-12-2012/|publisher=artobserved.com|accessdate=February 6, 2013|date=February 9, 2012}}</ref> In May 2012, the Cuban duo Los Carpinteros debuted their first solo in Buenos Aires.<ref name=openingceremony>{{cite web|last=Tozer|first=Gillian|title=Los Carpinteros at Faena Arts Center|url=http://www.openingceremony.us/entry.asp?pid=5751|publisher=openingceremony.us|accessdate=February 6, 2013|date=May 23, 2012}}</ref><ref name=vernissage>{{cite web|title=Los Carpinteros at Faena Arts Center, Buenos Aires|url=http://vernissage.tv/blog/2012/05/22/los-carpinteros-at-faena-arts-center-in-buenos-aires-argentina/|publisher=vernissage.tv|accessdate=February 6, 2013|authors=Roberto Rey, Milos Deretich, Luciana Zothner, Ángel Sánchez|date=May 22, 2012}}</ref><ref name=art_in_a_mag>{{cite web|last=Wei|first=Lilly|title=Los Carpinteros|url=http://www.artinamericamagazine.com/reviews/los-carpinteros/|publisher=artinamericamagazine.com|accessdate=February 6, 2013|date=September 3, 2012}}</ref> In November 2012, German artist [[Franz Ackermann]] exhibited the results of his voyages around Buenos Aires with the largest mural of his career, also his first show in Buenos Aires.<ref>{{cite web|title=Franz Ackermann at the Faena Arts Center|url=http://whitecube.com/news/franz_ackermann_at_the_faena_arts_center/|publisher=whitecube.com|accessdate=February 6, 2013}}</ref><ref>{{cite web|last=Ross|first=Kai|title=Franz Ackermann at Faena Arts Center|url=http://insidebuenosaires.com/2012/11/20/franz-ackermann-at-faena-arts-center/|publisher=insidebuenosaires.com|accessdate=February 6, 2013|date=November 20, 2012}}</ref>

==Faena Prize for the Arts==
Faena Prize for the Arts is one of the largest art prizes in Latin America and is led by [[Ximena Caminos]]. It aims to foster artistic experimentation, encourage crossover between disciplines, and promote new explorations of the links between art, technology, and design.<ref>{{citation|url=http://artforum.com/news/id=50137|title=2015 Faena Prize for the Arts Awarded to Cayetano Ferrer |work=Artforum |year=2015 |accessdate=2015-02-25}}</ref> The competition is open to all artists all over the world, and it is also open to work in different media (installations, performances, paintings, sculptures, photographs, works that include design and architecture, film or video pieces, or combination of the aforementioned disciplines) to create a project that responds or occupies the center’s space in a significant way.<ref name=modernism_ro>{{cite web|last=Nasui|first=Cosmin|title=2012 Faena Prize for the Arts|url=http://www.modernism.ro/2012/09/28/2012-faena-prize-for-the-arts/|publisher=modernism.ro|accessdate=February 6, 2013|date=September 28, 2012}}</ref><ref>{{cite web|last=Kazalia|first=Marie|title=International Art Center Competition Prize|url=http://artistmarketingresources.com/2012/04/02/international-art-center-competition-prize/|publisher=artistmarketingresources.com|accessdate=February 6, 2013|date=April 2, 2012}}</ref>

===2012 Edition===
The 2012 Faena Prize for the Arts winner was awarded to Argentinean visual artist, Franco Dario Vico.<ref>{{cite web|title=From Foundation to Foundation: The Hack of Franco Dario Vico to the 2012 Faena Prize |url=http://faenasphere.com/en/content/foundation-foundation-hack-franco-dario-vico-2012-faena-prize#!/ |publisher=faenasphere.com |accessdate=February 6, 2013 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>{{cite web|title=Franco Darío Vico awarded the 2012 Faena Prize|url=http://www.e-flux.com/announcements/franco-dario-vico/|publisher=e-flux.com|accessdate=February 6, 2013|date=November 15, 2012}}</ref> The 2012 jury included US based Carlos Basualdo, curator of contemporary art at the Philadelphia Museum of Art, Paris-based Caroline Bourgeois, curator at the Artis/Francois Pinault Foundation, and Buenos Aires based curator Ines Katzenstein, director of the art department at the Torcuato Di Tella University, under the supervision of Ximena Caminos, Executive Director of the Faena Arts Center.<ref>{{cite web|title=Call - Faena Prize|url=http://www.artnexus.com/Notice_View.aspx?DocumentID=24413|publisher=artnexus.com|accessdate=February 6, 2013|date=May 25, 2012}}</ref>

The artist received a grant of $25,000 as well as up to $50,000 to finance the production of a site-specific project exhibited at the Faena Arts Center in 2013.<ref>[http://artforum.com/archive/id=37632 "Franco Darío Vico Wins 2012 Faena Prize"]. ''Art Forum''.</ref><ref>{{cite web|title=Franco Vico: Artista ganador del Premio Faena 2012|url=http://www.arte-online.net/Notas/Franco_Vico_Artista_ganador_del_Premio_Faena_2012|publisher=arte-online.net|accessdate=February 6, 2013|language=Spanish|date=November 15, 2012}}</ref>

===2015 Edition===
The 2015 winner of the Prize is Los Angeles-based artist Cayetano Ferrer who was awarded $75,000 in total. $50,000 is for production of his proposed site-specific work for the Sala Molinos exhibition space in the Faena Art Center.<ref>{{citation|url=http://artforum.com/news/id=50137|title=2015 Faena Prize for the Arts Awarded to Cayetano Ferrer |work=Artforum |year=2015 |accessdate=2015-02-25}}</ref><ref>{{citation|url=http://www.artnews.com/2015/02/06/cayetano-ferrer-wins-2015-faena-prize/|title=Cayetano Ferrer Wins 2015 Faena Prize|work=Artnews|year=2015|accessdate=2015-02-27}}</ref>

==References==
{{reflist|2}}

==External links==
* [http://faena.com/en#!/ Faena - Official Website]
* [http://ba.faenaart.org Faena Art Center]
* [http://www.faenaartscenter.org/premiosf Faena Prize for the Arts]
* [http://www.loscarpinteros.net// Los Carpinteros - Official Website]
* [http://www.saatchi-gallery.co.uk/artists/franz_ackermann.htm Franz Ackermann - Saatchi Gallery Artist Profile]
* [http://www.tanyabonakdargallery.com/artist.php?art_name=Ernesto%20Neto Ernesto Neto - Tanya Bonakdar Gallery]

{{Authority control}}
[[Category:Arts centres in Argentina]]
[[Category:Museums in Buenos Aires]]