{{Infobox film
| name           = Deadly Hero
| image          = Deadly Hero movie poster.png
| caption        = Original 1975 movie poster
| director       = [[Ivan Nagy (director)|Ivan Nagy]]
| producer       = Thomas J. McGrath
| writer         = George Wislocki<br/>Don Petersen<ref name="nyt980503"/>
| starring       = [[Don Murray (actor)|Don Murray]]<br/>Diahn Williams<br/>[[James Earl Jones]]<br/>[[Lilia Skala]]<br/>[[Conchata Ferrell]]
| music          = [[Brad Fiedel]]<br/>[[Tommy Mandel]]
| cinematography = [[Andrzej Bartkowiak]] 
| editing        = Susan Steinberg
| distributor    = [[AVCO Embassy Pictures]]
| released       = {{Film date|1975}} ([[Denton, Texas]]; [[Gallup, New Mexico]]).
| runtime        = 104 minutes
| country        = United States
| language       = English
}}
'''''Deadly Hero''''' is a 1975 [[thriller movie]] starring [[Don Murray (actor)|Don Murray]], Diahn Williams, [[James Earl Jones]], [[Lilia Skala]], [[Treat Williams]], and directed by [[Ivan Nagy (director)|Ivan Nagy]] from a screenplay by George Wislocki and Don Petersen. Released in limited locales in 1975 with an [[Motion Picture Association of America film rating system|R rating]] from the [[Motion Picture Association of America]], the film was distributed by [[AVCO Embassy Pictures]] and is [[Treat Williams]]' film debut. ''Deadly Hero'' opened to mixed, mostly negative reviews by critics and was seen as a commercial failure. 

==Plot==
Officer Lacy (played by [[Don Murray (actor)|Don Murray]]) is an 18-year veteran of the [[New York City Police Department]] who finds himself demoted from detective back to patrol duty for his violent tendencies and trigger-happy behavior.<ref name="nyt761016"/><ref name="bdn761105"/> Responding to a call on Manhattan's [[West Side (Manhattan)|West Side]], he finds a young musician named Sally (Diahn Williams) has been abducted by a mugger named Rabbit ([[James Earl Jones]]).<ref name="var76"/><ref name="tcmdb"/> Rabbit has Sally at knifepoint in a hostage standoff but is persuaded to release her and surrender by Officer Lacy, who kills the unarmed Rabbit anyway. A grateful Sally is convinced by Lacy to lie to detectives to make Lacy seem like a hero. She later changes her mind and tells the truth about the shooting. This drives Lacy to try to silence Sally with escalating threats and violence before his career is ruined and he's tried for Rabbit's murder.<ref name="ll771119"/>

==Cast==
*[[Don Murray (actor)|Don Murray]] ... Lacy<ref name="mn760316"/>
* Diahn Williams ... Sally
*[[James Earl Jones]] ... Rabbit
*[[Lilia Skala]] ... Mrs. Broderick
*[[Conchata Ferrell]] ... Slugger Ann
*[[George S. Irving]] ... Reilly
* Ron Weyand ... Captain Stark
*[[Treat Williams]] ... Billings
*[[Hank Garrett]] ... Buckley
*[[Dick Anthony Williams]] ... D. A. Winston

The cast also includes performances by [[Josh Mostel]] as "Victor", [[Rutanya Alda]] as "Apple Mary", [[Charles Siebert]] as "Baker", plus [[Beverly Johnson]], [[Chu Chu Malave]], [[Danny DeVito]], and an uncredited [[Deborah Harry]] as a singer.<ref name="maltin93"/> ''Deadly Hero'' is Treat Williams' first motion picture appearance.<ref name="maltin93"/><ref name="vhound"/>

==Production info==
''Deadly Hero'' was directed by [[Ivan Nagy (director)|Ivan Nagy]] from a screenplay by George Wislocki and Don Petersen.<ref name="nyt980503"/><ref name="nyt761016"/> The film was produced by Thomas J. McGrath and distributed by [[AVCO Embassy Pictures]]. The film's cinematographer was [[Andrzej Bartkowiak]], with editing by Susan Steinberg, music by Brad [[Fiedel]] and [[Tommy Mandel]], and art direction by Alan Herman.  ''Deadly Hero'' was filmed entirely on location in [[New York City]].<ref name="var76"/> The 104-minute film was released as [[R-rated]] (Restricted) from the [[Motion Picture Association of America]].<ref name="tcmdb"/>

==Reception==
===Critical reaction===
[[Gene Siskel]] wrote in the ''[[Chicago Tribune]]'' that the film is "a small triumph" but qualifying that praise by noting, "Of course, expecting nothing helps."<ref name="ct760505"/> A reviewer for the Cineman Syndicate felt that "moments of suspense" helped elevate the "thin script and moody photography".<ref name="bdn761105"/> A.H. Weiler of ''[[The New York Times]]'' described ''Deadly Hero'' as a "fairly derivative Manhattan melodrama" with the supporting cast "wasted in brief, broad portrayals".<ref name="nyt761016"/> ''[[Los Angeles Times]]'' reviewer Linda Gross called the film "gritty" and "intriguing" but ultimately found it "predictable and pessimistic".<ref name="lat771208"/> Modern critics have been kinder with ''VideoHound's Golden Movie Retriever'' describing it as both "gripping" and "chilling" while rating the film three (out of a possible four) bones.<ref name="vhound"/>

===Popular reaction===
A commercial failure, George Anderson wrote in the ''[[Pittsburgh Post-Gazette]]'' that the violent film "suffered sudden death at the box office."<ref name="ppg770111"/>

===Home video===
''Deadly Hero'' was released on [[VHS]] in the 1980s by [[Magnetic Video]] and in 1986 by [[Embassy Home Entertainment]]. The movie was released to the [[DVD]] format on August 7, 2007, by Trinity Home Entertainment but {{as of|2010|12|lc=yes}} is out of print.<ref name="amaz1"/> On December 22, 2015, it appeared on [[Blue Ray]] by Code Red Entertainment through Screen Archives Entertainment Exclusive.

==References==
{{reflist|2|refs=
<ref name="amaz1">{{cite web |publisher=[[Amazon.com]] |title=Deadly Hero (1976) |url=http://www.amazon.com/dp/B000TM1CLK |accessdate=December 3, 2010}}</ref>
<ref name="bdn761105">{{cite news |agency=Cineman Syndicate |work=[[Bangor Daily News]] |title=Mini Movie Reviews: Deadly Hero |url=https://news.google.com/newspapers?id=zqkzAAAAIBAJ&sjid=yjgHAAAAIBAJ&pg=5047,2411008&dq=deadly-hero&hl=en |date=November 5, 1976 |page=ME2 |accessdate=December 3, 2010}}</ref>
<ref name="ct760505">{{cite news |first=Gene |last=Siskel |authorlink=Gene Siskel |work=[[Chicago Tribune]] |title=Six films with no Oscar hopes--or much of anything else |url=http://pqasb.pqarchiver.com/chicagotribune/access/617473802.html?dids=617473802:617473802&FMT=ABS&FMTS=ABS:AI |date=May 5, 1976 |page=C4 |accessdate=December 3, 2010}}</ref>
<ref name="lat771208">{{cite news |first=Linda |last=Gross |work=[[Los Angeles Times]] |title=A Cop Cracks Up in 'Deadly Hero' |url=http://pqasb.pqarchiver.com/latimes/access/650239382.html?dids=650239382:650239382&FMT=CITE&FMTS=CITE:AI |date=December 8, 1977 |page=H46 |accessdate=December 3, 2010}}</ref>
<ref name="ll771119">{{cite news |work=[[Lakeland Ledger]] |title=Television: Saturday Evening |url=https://news.google.com/newspapers?id=Q11OAAAAIBAJ&sjid=_PoDAAAAIBAJ&pg=3750,5508492&dq=deadly-hero&hl=en |date=November 19, 1977 |page=5B |accessdate=December 3, 2010}}</ref>
<ref name="maltin93">{{cite book |first=Leonard |last=Maltin |authorlink=Leonard Maltin |title=[[Leonard Maltin's Movie Guide|Leonard Maltin's Movie and Video Guide 1993]] |publisher=[[Signet Books]] |location=New York, NY |date=September 1992 |isbn=0-451-17381-3 |page=285}}</ref>
<ref name="mn760316">{{cite news |first=Earl |last=Wilson |work=[[The Miami News]] |title=He thought he'd die when spotted in line |url=https://news.google.com/newspapers?id=jdQzAAAAIBAJ&sjid=hOoFAAAAIBAJ&pg=2502,2393442&dq=deadly-hero&hl=en |date=March 16, 1976 |page=3B |accessdate=December 3, 2010}}</ref>
<ref name="nyt761016">{{cite news |first=A.H. |last=Weiler |work=[[The New York Times]] |title=Screen: 'Deadly Hero,' a Manhattan Melodrama |url=https://movies.nytimes.com/movie/review?_r=1&res=9E01E0D6133FE23BA15755C1A9669D946790D6CF |date=October 16, 1976 |page=13 |accessdate=December 3, 2010}}</ref>
<ref name="nyt980503">{{cite news |first=Mel |last=Guslow |work=[[The New York Times]] |title=Don Petersen, 70, Playwright and Screenwriter |url=https://select.nytimes.com/gst/abstract.html?res=F4091EFD3A5A0C708CDDAC0894D0494D81 |date=May 3, 1988 |accessdate=December 3, 2010}}</ref>
<ref name="ppg770111">{{cite news |first=George |last=Anderson |work=[[Pittsburgh Post-Gazette]] |title=Some Coming Attractions That Never Came |url=https://news.google.com/newspapers?id=uUwNAAAAIBAJ&sjid=Um0DAAAAIBAJ&pg=6701,914503&dq=deadly-hero&hl=en |date=January 11, 1977 |page=6 |accessdate=December 3, 2010}}</ref>
<ref name="tcmdb">{{cite web |publisher=[[Turner Classic Movies]] |work=TCM Movie Database |title=Deadly Hero (1976) |url=http://www.tcm.com/tcmdb/title.jsp?stid=72573 |accessdate=December 3, 2010}}</ref>
<ref name="var76">{{cite news |author=''Variety'' Staff |work=[[Daily Variety]] |title=Film Reviews: Deadly Hero |url=http://www.variety.com/review/VE1117790292?refcatid=31 |date=May 1976 |accessdate=December 3, 2010}}</ref>
<ref name="vhound">{{cite book |title=VideoHound's Golden Movie Retriever 1996 |chapter=Bad Charleston Charlie |publisher=Visible Ink Press |location=[[Detroit, MI]] |isbn=0-7876-0626-X |year=1996 |page=327}}</ref>
}}

== External links ==
* {{imdb title|0074381}}
* {{tcmdb title|72573|Deadly Hero}}
* {{amg title|177747}}
* {{rotten_tomatoes|deadly_hero}}

{{Ivan Nagy}}

[[Category:1975 films]]
[[Category:Films directed by Ivan Nagy]]
[[Category:American thriller films]]
[[Category:American films]]