{{Use mdy dates|date=April 2012}}
{{Use British English|date=September 2011}}
{{Infobox scientist
| name              = John Alexander MacWilliam<br /><small>[[Fellow of the Royal Society|FRS]]</small>
| image             = ProfJAMpicl1.png
| image_size        = 240px
| caption           = 
| birth_date        = {{birth date|1857|07|31|df=yes}}
| birth_place       = [[Kiltarlity]], [[Inverness-shire]], Scotland
| death_date        = {{Death date and age|1937|01|13|1857|07|31|df=yes}}
| death_place       = Edinburgh, Scotland
| citizenship       = United Kingdom
| nationality       = British/Scottish
| field             = [[Physiology]], [[Cardiology]]
| alma_mater        = [[University of Aberdeen]]<br />[[University College, London]]<br />[[University of Leipzig]]
| doctoral_advisor  =
| doctoral_students =
| known_for         = Research and discoveries in field of cardiac function (heart, arteries, blood pressure).
| influences        =
| influenced        =
| prizes            = {{Plainlist|
*[[Fellow of the Royal Society|FRS]] (1916)
*Honorary LL.D award from [[University of Aberdeen]] (1927)}}
| signature         = ProfJAMsignature.jpg
}}

'''John Alexander MacWilliam''' (31 July 1857 – 13 January 1937), a physiologist at the [[University of Aberdeen]] in the late nineteenth and early twentieth centuries, was a pioneer in the field of [[cardiac electrophysiology]].  He spent many years studying [[ventricular fibrillation]], and was the first person to propose that ventricular fibrillation was the most common cause of sudden death - and that fibrillation could be terminated (and life potentially saved) by a series of induction shocks to the heart.<ref name=santos>{{cite journal | author = Dos Santos Cruz Filho FE, Chamberlain D | year = 2006 | title = Resuscitation great: John Alexander MacWilliam | url = http://www.resuscitationjournal.com/article/S0300-9572(05)00335-7/abstract | journal = Resuscitation | volume = 69 | issue = 1| pages = 3–7 | doi=10.1016/j.resuscitation.2005.08.009}}</ref> He was the first to accurately describe the condition of [[arrhythmia]] (irregular heartbeat), and he suggested [[transthoracic pacing]] to treat transient [[asystole]] (cardiac arrest).<ref name=silverman>{{cite journal | author = Silverman ME, Fye WB, Hurst JW | date = Feb 2006 | title = John A. MacWilliam: Scottish pioneer of cardiac electrophysiology | url = http://onlinelibrary.wiley.com/doi/10.1002/clc.4960290213/pdf | journal = Clin Cardiol | volume = 29 | issue = 2| pages = 90–2 | doi=10.1002/clc.4960290213}}</ref>
Although his work was recognised within his lifetime, it was not until many decades later that it laid the foundations for developments in the understanding and treatment of life-threatening heart conditions, such as in the [[artificial cardiac pacemaker]]. 
MacWilliam was appointed Regius Professor of the Institutes of Medicine (later Physiology) at the University of Aberdeen<ref name=bmj /> at the age of 29 in 1886, and remained in that post for 41 years until his retirement in 1927.

==Background==
<ref name=keith>Professor A. Keith (1937) "John Alexander MacWilliam 1857 - 1936", Obituary Notices of Fellows of the Royal Society, pp:335-338</ref>
<ref name=reid>"Miss Reid and Mrs MacWilliam", Aberdeen University Review (1939), p65</ref>

MacWilliam was born 31 July 1857 at [[Kiltarlity]], near [[Beauly]], [[Inverness-shire]] in [[Scotland]],<ref name=bmj>[http://europepmc.org/backend/ptpmcrender.fcgi?accid=PMC2088087&blobtype=pdf "J. A. MacWilliam, M.D., LL.D., FRS."] Obituary in British Medical Journal, (Jan 23, 1937), page 199</ref> where his father was farmer at Culmill farm. 
His parents were William McWilliam (1814 - 1888) and his wife Isabella Cumming (1816 - 1887) who had moved around 1850 from the neighbouring parishes of [[Inveravon]] (sometimes spelled Inveraven) and [[Knockando]] on [[Strathspey, Scotland|Speyside]] to the farm at Culmill. His mother Isabella Cumming was the youngest daughter of John and Helen Cumming, founders of the [[Cardhu]] whisky distillery on Speyside.

John Alexander had two siblings. His elder brother, William Lewis McWilliam, was born at Kiltarlity in 1855. He stayed as farmer at Culmill and married Mary Burns. They had no children. William was a respected farmer and an Inverness-shire County Councillor, who was also chairman of the local schools committee and chairman of the Parish Council for over 30 years. He died in 1936, aged 81. 
His sister, Isabella Helen McWilliam, was born 12.10.1859 and died in infanthood – at the age of just 16 months.

Around the late 1880s, he chose to alter the spelling of his name to MacWilliam rather than McWilliam. His published papers until 1889 are under the McWilliam spelling.

==Education and early work==
<ref name=keith />
<ref name=reid />

MacWilliam was educated at Kiltarlity parish school until moving on to Aberdeen University at the age of 17 in 1874. At Aberdeen he studied in the Arts faculty for two years before changing course to study medicine. He graduated M.B, C.M. in 1880, and was awarded the John Murray medal in that year for outstanding achievement.<ref name=bmj /><ref>{{cite book|title=Graduation at Aberdeen University|work=British Medical Journal|url=https://books.google.com/books?id=KU8BAAAAYAAJ&pg=PA329|year=1881|publisher=British Medical Association|pages=329–}}</ref>

After postgraduate work at the University of Edinburgh and at University College, London, MacWilliam worked with physiologists [[Hugo Kronecker]] at Bern, and [[Carl Ludwig]], director of the Physiological Institute at the [[University of Leipzig]].<ref name=santos /> In 1847 Ludwig had invented the [[Kymograph]] (a mechanical instrument to record heartbeat and other muscle contractions or movements), but of course this was still before the invention of the [[electrocardiograph]]. MacWilliam began his research with Ludwig on the hearts of cold-blooded animals such as eels, fish and frogs, and noted a phylogenic similarity between the electrical behaviour of hearts progressing from these creatures to man. He logically extended his experiments on ventricular fibrillation in lower animals to humans, surmising that this arrythmia was the cause of sudden death.
After returning from the continent, MacWilliam received his M.D. degree (with highest honours) from Aberdeen University in 1882 for his thesis: Part 1 – “On the cardiac muscular fibre in various animals”, Part 2 – “On the diaphragmatic fibre in various animals”.

==Research work and teaching==
<ref name=silverman />
<ref name=keith />
<ref name=maclean>Professor Hugh MacLean (1937) "John Alexander MacWilliam 1857 - 1936", Aberdeen University Review, pp:127-132</ref>
<ref name=desilva>{{cite journal | author = de Silva Regis A | year = 1989 | title = Evolutionary biology and sudden cardiac death | url = http://www.sciencedirect.com/science/article/pii/0735109789900417| journal = Journal of the American College of Cardiology | volume = 4 | issue = 7| pages = 1843–1849 | doi=10.1016/0735-1097(89)90041-7}}</ref>
<ref name=bruce>W.Bruce. Fye (1985) [http://circ.ahajournals.org/content/71/5/858.full.pdf Ventricular fibrillation and defibrillation: historical perspectives with emphasis on the contributions of John MacWilliam, Carl Wiggers, and William Kouwenhoven], Circulation, Journal of the American Heart Association</ref>

In the four years from 1883 till 1886 MacWilliam worked primarily at [[University College London]], as demonstrator under [[Edward Albert Sharpey-Schafer]] (previously Schäfer), and working closely with eminent contemporaries in physiology there ([[W.D. Halliburton]] and [[Ernest Starling]]), and with inspiration and guidance from [[W.H. Gaskell]] at [[Kings College, Cambridge]]. 
In 1886 MacWilliam was appointed Regius Professor of the Institutes of Medicine at the [[University of Aberdeen]] – at just 29 years old – succeeding Professor [[William Stirling (physiologist)|William Stirling]]. The role was subsequently retitled as Regius Professor of Physiology.

During his time at University College, MacWilliam studied the structure of the muscular fibres of the diaphragm and of the heart. The results of his research were communicated to the Royal Society in a paper “The structure and rhythm of the heart of the eel” – with the results of experiments extending over two years on the origin and conduction of the wave of contraction which sweeps over the eel’s heart, the different ways in which the wave could be blocked, and the manner in which its direction could be changed. From 1885 onwards, there followed three more years of systematic investigation. He extended his experiments to the hearts of mammals, most often cats,<ref>{{cite book|title=Half-yearly Compendium of Medical Science|url=https://books.google.com/books?id=Nk6gAAAAMAAJ&pg=PA22|year=1889|pages=22–}}</ref> and found that all he had observed in the heart of the (cold-blooded) eel was also to be seen in the mammalian heart.<ref name="Gilman2011">{{cite book|author=Sander L. Gilman|title=Diseases and Diagnoses: The Second Age of Biology|url=https://books.google.com/books?id=jCzwtakVbaUC&pg=PA165|date=31 December 2011|publisher=Transaction Publishers|isbn=978-1-4128-1530-7|pages=165–}}</ref> He studied the condition known then as “Herz-delirium” or “ventricular fibrillation”, which became of importance to clinicians in the opening decades of the twentieth century. He sought to explain the manner in which this incoordination in the action of the heart’s muscle was brought about and the manner in which normal contraction could be restored. He found that there was a “certain area along the septum” where inhibition of the wave of contraction could be easily produced. He observed the condition which became known as “auricular flutter”, and concluded that fibrillation was a disorder of muscle, not of nerve.<ref name="Fleming1997">{{cite book|author=P. R. Fleming|title=A Short History of Cardiology|url=https://books.google.com/books?id=-MywUScYd-4C&pg=PA121|year=1997|publisher=Rodopi|isbn=90-420-0057-0|pages=121–}}</ref> He also described the basic elements of [[cardiopulmonary resuscitation]] - commonly abbreviated as CPR - (ventilation and cardiac compression) in keeping his experimental animals alive.

His contributions to cardiac electrophysiology were not limited to ventricular fibrillation. For example, he described the technique of transthoratic pacing for transient bradycardia (abnormally slow heartbeat), and he proposed stimulating the heart during asystole (cardiac arrest) by causing “artificial excitation” with a series of induction shocks (rather than using constant strong electric currents that could trigger fibrillation). He listed several potential triggers of ventricular fibrillation in individuals with underlying cardiac disease that could lead to a “hypersensitive state” of the heart. These included exertion-related changes in blood pressure and heart rate, digitalis, [[chloroform]], and coronary obstruction.

MacWilliam made many other contributions to cardiac physiology during his career. In 1887, when chloroform anaesthesia was a cause of surgical death, his extensive animal experiments showed that chloroform could affect the heart directly and cause ventricular fibrillation. Between 1912 and 1925 he published articles analyzing the contractile properties of isolated blood vessels, the mechanism of the Korotkoff sounds, the effect of peripheral resistance on blood pressure, and blood pressure measurements in normal and pathological conditions. In 1919 he published ''The Mechanism and Control of Fibrillation in the Mammalian Heart''.

MacWilliam was the first person to describe the action of dreams on the heart rate and blood pressure, having first noticed this in dogs in the late 1800s.<ref name=desilva />   In 1923 he reported his sleep studies on humans and dogs, in which he observed that profound effects on blood pressure and heart rate sometimes occurred with “disturbed sleep” which “imposed sudden and dangerous demands on the heart”. He felt that fibrillation could be precipitated during sleep and dreaming, similar to the effects of emotional distress.

[[File:AURcentdrawing.jpg|thumb|Caricature sketch of Prof. MacWilliam in lecture mode]]
Although his research work continued throughout his career, his focus moved more towards his teaching role after taking the physiology chair in Aberdeen in 1886.,<ref name=keith /><ref name=maclean /> He retired in 1927 at the age of 70.

Amongst his students at Aberdeen were many who went on to outstanding achievements of their own, including [[John James Rickard Macleod]] (joint Nobel prize-winner in 1923 for the discovery of [[insulin]]).<ref name=keith />  In 1928 Macleod returned from Canada to take the physiology chair at Aberdeen, in succession to MacWilliam. [[Arthur Robertson Cushny]] graduated M.D. at Aberdeen in 1892 and went on to become a leading pharmacologist, and Professor in the medical faculties of the [[University of Michigan]], [[University College London]], and finally the [[University of Edinburgh]].<ref name=bruce />

==Legacy==
<ref name=silverman /> 
<ref name=keith />
<ref name=desilva />
<ref name=bruce />

It would be more than 60 years before MacWilliam’s research on arrhythmias and their treatment was translated into clinical approaches that physicians and surgeons could use in patient care. The first successful defibrillation of a human was reported in 1947 by Cleveland surgeon Claude Beck, and the first successful human transthoratic defibrillation by Boston cardiologist Paul Zoll in 1956. Cardiac pacemaker therapy would not become reality in clinical medicine until the 1950s (transthoracic and temporary pacing) and the 1960s (permanent pacing).

During the 1960s the care of patients who suffered cardiac arrest or were thought to be at risk of life-threatening arrhythmias was significantly improved by the introduction of cardiopulmonary resuscitation and the introduction of the coronary care unit.

The value of MacWilliam's work has been recognised in recent years in several published articles. In 1989, Regis de Silva of Harvard University wrote: “MacWilliam devised methods that laid the foundations for modern cardiac research and that provided the first comprehensive approach to successful cardiac resuscitation” and “MacWilliam’s basic physiologic concepts have survived for a century, greatly influencing more than three generations of research and practice in clinical cardiology”.<ref name=desilva /> In 1959 Dr [[Bernard Lown]] at Harvard invented direct current [[cardioversion]] based on the original work of MacWilliam.

In his lengthy obituary in the Aberdeen University Review<ref>Professor Hugh MacLean (1937) "John Alexander MacWilliam 1857 - 1937", Aberdeen University Review, pp:127-132</ref> Professor Hugh Maclean states that “He lived thirty years before his time” and “It is now obvious that the value of his contributions can hardly be overestimated”.

==Honours==
<ref name=keith />
<ref name=reid />

MacWilliam was elected as a [[Fellow of the Royal Society]] (FRS) in 1916, and awarded the LL.D. degree by Aberdeen University in 1927. He was a member of the University Court (governing body) of Aberdeen University from 1917 until 1921 and was a member of the University Senate (Senatus Academicus) from 1906 until his retirement. At Aberdeen he is commemorated in the naming of the MacWilliam Resuscitation Training Room in the University's Suttie Building.

At the [[National Portrait Gallery, London|National Portrait Gallery]] in London he is represented by two photographic portraits (approx. 1912 and 1925).

==Personal life==
<ref name=keith /><ref name=reid />

[[File:35 Drumsheugh Gardens, Edinburgh.JPG|thumb|35 Drumsheugh Gardens, Edinburgh, where MacWilliam died]]
In 1889 at the age of 32, MacWilliam married Edith Constance Wise, the sister of [[Berkeley Deane Wise]], a civil engineer. Edith died in November 1893 at the age of 33, of [[malaria]] contracted in the Canary Islands while her husband travelled on to South Africa.

In 1898 he was married for a second time, to Florence Edith Thomas originally from Wrexham in North Wales, who outlived him by nearly two years. She was a trained nurse who had worked at the London Temperance Hospital and then as a sister at [[St Bartholomew's Hospital|St. Bartholomew's]] in London and later as Matron of the [[Radcliffe Infirmary]] in Oxford. In Aberdeen she involved herself in a variety of social work, including caring for soldiers returning from service during [[World War I]]. For all his married life MacWilliam lived in the village of [[Cults]], now a suburb of Aberdeen.

John MacWilliam suffered for much of his life from an obscure form of dyspepsia, only diagnosed (as duodenal ulceration) and treated in later life.

[[File:ProfJAMobitpicA.jpg|thumb|Prof. J.A. MacWilliam in later life]]

==Death==
<ref name=keith />

MacWilliam died of heart failure in January 1937, in a nursing home at 35 Drumsheugh Gardens in Edinburgh. He is buried, along with his wives, at Allenvale cemetery in Aberdeen.

==Publications==

* J.A. McWilliam: On the structure and rhythm of the heart in fishes, with especial reference to the heart of the eel J Physiol, 6 (1885), pp.&nbsp;192–245
* J.A. McWilliam: Fibrillar contraction of the heart J Physiol, 8 (1887), pp.&nbsp;296–310
* J.A. McWilliam: On the rhythm of the mammalian heart Proc Royal Soc (2nd ed.), 44 (1888), pp.&nbsp;206–208
* J.A. McWilliam: Inhibition of the mammalian heart Proc Royal Soc (2nd ed.), 44 (1888), pp.&nbsp;208–213
* J.A. McWilliam: Electrical stimulation of the heart in man Br Med J, 1 (1889), pp.&nbsp;348–350 http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2154721/
* J.A. MacWilliam: Cardiac failure and sudden death Br Med J, 1 (1889), pp.&nbsp;6–8
* J.A. MacWilliam: Some applications of physiology to medicine, Ventricuar fibrillation and sudden death Br Med J, 2 (1923), pp.&nbsp;215–217
* J.A. MacWilliam: Blood pressure and heart action in sleep and dreams Br Med J, 2 (1923), pp.&nbsp;1196–1200
* John Alexander MacWilliam: Physiological Studies First Series by (pub. 1916 by Aberdeen University Press, republished by Hardpress Ltd in 2012)
* John Gray Mackendrick, and John Alexander MacWilliam: The Principles of Physiology (pub. 1928)<ref name="Silva2013">{{cite book|author=Regis A. de Silva|title=Heart Disease|url=https://books.google.com/books?id=NI0LtnwdBg0C|date=24 January 2013|publisher=ABC-CLIO|isbn=978-0-313-37607-8}}</ref>

==References==
{{reflist}}

==External links==
{{Commons category|John Alexander MacWilliam}}
*Regis de Silva, [https://books.google.com/books?id=NI0LtnwdBg0C Heart Disease (Biographies of Disease)] Hardcover, Greenwood Press, (Jun 2012)
*Mickey S. Eisenberg MD, PhD,  [http://link.springer.com/chapter/10.1385%2F1-59259-814-5%3A001 History of the Science of Cardiopulmonary Resuscitation], Springer, (2005)
*http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1036446/   W.J.O'Connor,  British Physiologists 1885-1914: A Biographical Dictionary,  Manchester University Press (1991)

{{DEFAULTSORT:MacWilliam, John Alexander}}
[[Category:1857 births]]
[[Category:1937 deaths]]
[[Category:Scottish physiologists]]
[[Category:Academics of the University of Aberdeen]]
[[Category:Alumni of the University of Aberdeen]]
[[Category:Fellows of the Royal Society]]