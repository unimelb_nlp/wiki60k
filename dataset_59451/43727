{|{{Infobox Aircraft Begin
|name = Air Yacht
|image = Supermarine Air-Yacht.jpg
|caption = 
}}{{Infobox Aircraft Type
|type = Luxury flying boat
|manufacturer = [[Supermarine]]
|designer = 
|first flight = February 1930
|introduction = 1930
|retired = 
|status =
|primary user = Private owners
|more users =
|produced = 1930
|number built = 1
|unit cost = 
|variants with their own articles =
}}
|}

The '''Supermarine Air Yacht''' was a [[United Kingdom|British]] luxury passenger-carrying [[flying boat]] [[air yacht]] designed and built by the [[Supermarine|Supermarine Aviation Works]]. A three-engined all-metal [[monoplane]], a single example was built, being destroyed in an accident in 1933.

==Development and design==

The Supermarine Air Yacht was a three-engined luxury flying boat built at [[Woolston, Hampshire|Woolston]] in 1929 for the Hon. [[Arthur Ernest Guinness]] for pleasure cruises around the [[Mediterranean]], replacing his [[Supermarine Solent]] flying boat.{{sfnp|Pegram|2016|page=64}} It was based on a 1927 design to meet the requirements of [[List of Air Ministry specifications|specification R5/27]] for a reconnaissance flying boat for the [[Royal Air Force]].<ref name="Andrews & Morgan p123-4">Andrews and Morgan 1987, pp.123-124.</ref>

The resulting design was a three-engined [[monoplane]] flying boat with hull-mounted [[sponson]]s instead of the wing-mounted floats more common on aircraft of this type. Construction was of all-metal, with  the wing was held above the fuselage on struts, with the three [[Armstrong Siddeley Jaguar]] [[radial engine]]s mounted on the leading edge of the wing. The single braced tailplane had three vertical fins and rudders. The crew were accommodated in open cockpits in the nose, with an enclosed cabin for the owner, with its own toilet, bath and bed, a separate cabin for the other five passengers and a galley beneath the wing.<ref name="Andrews & Morgan p124">Andrews and Morgan 1987, p.124.</ref><ref name="Flight2" />

The Air Yacht made its first flight in February 1930 at [[Hythe, Hampshire|Hythe, England]].<ref name="Andrews & Morgan p125">Andrews and Morgan 1987, p.125.</ref> It proved to be underpowered, with an excessive take-off run,<ref>{{cite web |url=http://aviation-safety.net/wikibase/wiki.php?id=27428 |title=ASN Wikibase Occurrence # 27428 |author= |publisher=Flight Safety Foundation |date= |format= |accessdate=July 6, 2016}}</ref> and despite re-engining with three [[Armstrong Siddeley Panther]]s, was rejected by Guinness, who purchased a [[Saro Cloud]] instead.<ref name="Andrews & Morgan p124"/>

==Operational history==
In October 1932 the Air Yacht was bought by a Mrs J.J. James and re-engined with three 525&nbsp;[[horsepower|hp]] (392&nbsp;kW) [[Armstrong Siddeley Panther|Armstrong Siddeley Panther IIA]]s. Given the name ''Windward III'' it left Woolston on a cruise to [[Egypt]] on 11 October 1932,<ref name="Jackson v3 p317">Jackson 1988, p.317.</ref><ref name="Flight1" /> but on 25 January 1933 it crashed on takeoff near [[Capri]], Italy. While there were no casualties, the Air Yacht was then scrapped.<ref name="Andrews & Morgan p128"/>

==Specifications==
{{aircraft specifications|
|plane or copter?= plane
|jet or prop?= prop
|ref= Supermarine Aircraft since 1914 <ref name="Andrews & Morgan p128">Andrews and Morgan 1987, p.128.</ref>
|crew= 4 
|capacity = 6 passengers
|length main= 66 ft 6 in
|length alt= 20.3 m
|span main= 92 ft
|span alt= 28 m
|height main= 19 ft 
|height alt= 5.8 m
|area main=1,472 ft²
|area alt=136.7 m²
|empty weight main= 16,808 lb
|empty weight alt= 7,624 kg
|loaded weight main= 23,348 [[pound (mass)|lb]]
|loaded weight alt= 10,590 kg
|engine (prop)= [[Armstrong Siddeley Panther|Armstrong Siddeley Panther IIA]]
|type of prop=14-cylinder air-cooled [[radial engine]]
|number of props= 3
|power main= 525 [[horsepower|hp]]
|power alt= 391.5 [[kilowatt|kW]]
|max speed main= 102 [[knot (unit)|kn]]
|max speed alt=117 mph, 189 km/h
|range main=  
|range alt=   
|ceiling main= 6,500 ft
|ceiling alt= 1,980 m
|climb rate main= 380 ft/min
|climb rate alt= 1.9 m/s
}}

==See also==

* [[List of seaplanes and flying boats]]

==Notes and references==
;Notes
{{reflist|refs=
<ref name="Flight1">{{cite magazine|magazine=[[Flight International|Flight]]|date=28 February 1930|pages=250–252|url=http://www.flightglobal.com/PDFArchive/View/1930/Untitled0%20-%200258.html |title=An Air Yacht De Luxe}}</ref>
<ref name="Flight2">{{cite magazine|magazine=[[Flight International|Flight]]|date=20 October 1932|page=990|title=By Air Yacht to the Mediterranean|url=http://www.flightglobal.com/PDFArchive/View/1932/1932%20-%201062.html}}</ref>
}}
;Bibliography
*Andrews, C.F. and Morgan, E.B. ''Supermarine Aircraft since 1914''. London:Putnam, 1987. ISBN 0-85177-800-3.
*Jackson, A.J. ''British Civil Aircraft 1919-1972:Volume III''. London:Putnam, 1988, ISBN 0-85177-818-6.
* {{Cite book
  |title=Beyond the Spitfire: The Unseen Designs of R.J. Mitchell 
  |last=Pegram  |first=Ralph
  |publisher=The History Press
  |year=2016
  |isbn=978-0750965156
  |ref=harv
  |page=64
}}

==Further reading==
*{{cite book | author = Shelton, John |title = Schneider Trophy to Spitfire - The Design Career of R.J. Mitchell | location = 
Sparkford | publisher = Hayes Publishing | year = 2008 | type = Hardback | isbn = 978-1-84425-530-6}}

==External links==
{{commons category|Supermarine}}
* [http://1000aircraftphotos.com/Contributions/KleinBernhard/5649.htm Photo]
* {{Cite magazine
  |title=Largest Private Plane is Flying Yacht
  |url=http://blog.modernmechanix.com/largest-private-plane-is-flying-yacht/
  |date=January 1933
  |magazine=[[Modern Mechanix]]
}}

{{Supermarine aircraft}}

[[Category:British civil utility aircraft 1930–1939]]
[[Category:Flying boats]]
[[Category:Supermarine aircraft|Air Yacht]]
[[Category:Parasol-wing aircraft]]
[[Category:Trimotors]]