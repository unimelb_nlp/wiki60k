<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name='''Linke-Hofmann R.II''' 
 | image=Linke-Hofmann RII.png||300px|Linke-Hofmann RII aircraft
 | caption=Linke-Hofmann R.II, note the size of the figures compared to the aircraft
}}{{Infobox Aircraft Type
 | type= [[Bomber]]
 | national origin=[[German Empire]]
 | manufacturer=[[Linke-Hofmann]]<ref name="Haddow">{{cite book|last=Haddow|first=G.W.|author2=Peter M. Grosz|title=The German Giants – The German R-Planes 1914–1918|publisher=Putnam & Company Ltd.|location=London|year=1988|edition=3rd|pages=146–151|isbn=0-85177-812-7}}</ref>
 | designer= [[Paul Stumpf]]<ref name="Haddow"/>
 | first flight= 1919<ref name="Haddow"/>
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built= 2<ref name="Haddow"/>
 | developed from= 
 | variants with their own articles=

}}
|}

The '''Linke-Hofmann R.II''' (''[[Riesenflugzeug]]'' – "giant aircraft") was a  bomber aircraft designed and built in [[Germany]] from 1917.<ref name="Haddow"/>

== Design and development ==
The [[Linke-Hofmann R.I]] had disappointing performance and handling, as well as structural weakness with both prototypes crashing. Linke-Hofmann took a radically different approach for their second ''Riesenflugzeug'', the Linke-Hofmann R.II. The R.II was an approximately three-fold scale-up of a conventional single-engined biplane, powered by a quartet of [[Mercedes D.IVa]] inline-six engines turning a single 6.90 meter (22&nbsp;ft 7.5 in) diameter [[Tractor configuration|tractor]] propeller, the largest single propeller ever used to propel any aircraft in aviation history.<ref name="Haddow"/> The quartet of Mercedes powerplants  were arranged in pairs in the central fuselage and drove the propeller through clutches, shafts and gearboxes. The Linke-Hofmann R.II, probably the largest single propeller driven aircraft that will ever be built, had a wing span of {{convert|41.16|m|ftin|abbr=on}}, length of {{convert|23.3|m|ftin|abbr=on}} and height of {{convert|7.1|m|ftin|abbr=on}}.<ref group="Note">As a comparison, the [[Boeing B-29 Superfortress]] heavy bomber of World War II had a wingspan of around {{convert|43|m|ftin|abbr=on}}.</ref><ref name="Haddow"/>

The airframe was constructed largely of wood, with plywood covering the forward fuselage and a steel-tube v-strut chassis main undercarriage with two wheels and a tail-skid at the aft end of the fuselage. Two examples of the R.II had been completed by the time the [[Armistice]] bore the ''[[IdFlieg]]'' German military registration numbers R.55/17 and R.58/17.<ref name="Haddow"/>

Flight testing of R 55/17 was carried out after the Armistice in {{avyear|1919}}, demonstrating acceptable performance and handling, being able to fly happily with only two engines driving the enormous propeller. Normal endurance was estimated to be 7 hours, but with adjustment of load and a cruising speed of {{convert|74|mph|0|abbr=on}} it was estimated that the R.II could stay aloft for 30 hours.<ref name="Haddow"/>

There were plans to make it a 12-passenger airliner after the war, but the restrictions of the [[Versailles Treaty]] ended further development.<ref name="Haddow"/>

==Specifications (Linke-Hofmann R.II)==
{{aerospecs
|ref=German Aircraft of the First World War<ref name="Haddow"/><ref>Gray, Peter & Thetford, Owen. "German Aircraft of the First World War". London, Putnam. ISBN 0-370-00103-6 {{Page needed|date=September 2011}}</ref>
|met or eng?=met<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew= 6+
|capacity=
|length m= 20.316
|length ft= 66
|length in= 7-7/8
|span m= 42.16
|span ft= 138
|span in= 4
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m= 7.1
|height ft=23
|height in=3-5/8
|wing area sqm= 320
|wing area sqft= 3,443
|aspect ratio=
|wing profile=
|empty weight kg= 8,000
|empty weight lb= 17,640
|gross weight kg= 12,000
|gross weight lb= 26,460
|eng1 number= 4
|eng1 type= [[Mercedes D.IVa]]
|eng1 kw=<!-- prop engines --> 193.9
|eng1 hp=<!-- prop engines --> 260
 
|max speed kmh= 130
|max speed mph= 81.25
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown --> 
|cruise speed mph=<!-- if max speed unknown --> 
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km= 
|range miles= 
|endurance h=<!-- if range unknown --> 7
|endurance min=<!-- if range unknown -->
|ceiling m= 
|ceiling ft= 
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms= 2.08
|climb rate ftmin= 410
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1= 3 x machine-guns in two dorsal and one ventral positions.
|armament2= 
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
* [[Siemens-Schuckert R.VIII]] ''Steffen'' 
|lists=<!-- related lists -->
* [[List of aircraft]]
}}

==Notes==
{{reflist|group=Note}}

==References==
;Citations
{{reflist}}
;Bibliography
{{refbegin}}
* Haddow, G.W. & Grosz, Peter M. ''The German Giants, The Story of the R-planes 1914–1919''.  London. Putnam. 1963.
* Gray, Peter & Thetford, Owen. ''German Aircraft of the First World War''. London, Putnam. ISBN 0-370-00103-6
* Wagner, Ray and Nowarra, Heinz. ''German Combat Planes: A Comprehensive Survey and History of the Development of German Military Aircraft from 1914 to 1945''. New York: Doubleday, 1971.
*{{cite journal |date=October 2, 1919 |title=The Linke-Hofmann Giant Machines |format=pdf |journal=[[Flight (magazine)|Flight]] |volume=XI |issue=40; number 562 |pages=1311–1314 |url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%201309.html |accessdate=January 13, 2011 |quote=translated from a descriptive article in ''Flugsport''}} 
{{refend}}

==External links==
* http://www.airwar.ru/enc/bww1/linker2.html
* http://wroclaw.hydral.com.pl/110884,foto.html

{{Idflieg R-class designations}}

[[Category:German bomber aircraft 1910–1919]]

[[de:Linke-Hofmann R-Typen]]