{{Infobox company
| name = IQMS
| logo= [[File:IQMS logo.png|IQMS logo|180px]]
| type= Private
| industry=  [[Software|Computer Software]]
| founded= 1989 
| founders =  Randall Flamm and Nancy Flamm      
| hq_location_city =  [[Paso Robles, California]]
| hq_location_country = United States
| area_served=  Worldwide
| key_people= Gary Nemmers, CEO, Randall Flamm, Chairman
| products= [[Enterprise resource planning]] [[software]]
| revenue= $44.6 million(2015)<ref>{{cite web|url=http://www.inc.com/profile/iqms?cid=full500016-list-3929|title=IQMS|publisher=Inc. |access-date=December 18, 2016}}</ref>
| services= [[Manufacturing]], [[accounting]], production monitoring, [[quality control]], [[supply chain management]], [[customer relationship management|CRM]], and [[electronic business|eBusiness]] solutions
| num_employees = 250<ref name= pcbt>{{cite web| url=http://www.pacbiztimes.com/author/philip-joens/|title=IQMS Growing Rapidly, Hiring More Workers in Paso Robles| publisher= Pacific Coast Business Times |first=Philip |last=Joens |date=October 2, 2015 |access-date=October 14, 2015}}</ref>
| num_employees_year = 2015<ref name=pcbt/>
| website= {{URL|http://www.iqms.com}}
}}

'''IQMS''' is a privately held, global software company based in [[Paso Robles, California]].<ref name=paso>{{cite web| url=
http://pasoroblesdailynews.com/iqms-gets-40-million-investment-and-new-ceo/39840/| title= IQMS gets $40 million investment and new CEO| author = News Staff| date= August 5, 2015|publisher= Paso Robles Daily News| access-date= August 26, 2015}}</ref> The company develops and markets manufacturing [[enterprise resource planning|ERP]] and real time [[manufacturing execution system]]s to the [[automotive industry|automotive]], [[health care industry|medical]], [[packaging and labeling|packaging]], [[Final good|consumer goods]], [[aerospace]], [[defense industry|defense]] and other [[manufacturing]] industries.<ref name=pn1>{{cite web| url=
http://www.plasticsnews.com/article/20150731/NEWS/150739975/iqms-pays-company-founders-38-6-million| title= IQMS pays company founders $38.6 million| author = Steve Toloken| date= July 31, 2015|publisher= Plastics News| access-date= August 26, 2015}}</ref><ref name=cio>{{cite web| url=
http://www.cioreview.com/magazine/IQMS-Providing-Software-Services-And-Consulting-To-Improve-Business-Efficiencies20-JUIN571755747.html| title= IQMS: Providing software, services and consulting to improve business efficiences20| date= December 5, 2013|publisher= CIO Review| access-date= August 26, 2015}}</ref> IQMS has been included on [[Inc. (magazine)|Inc. Magazine’s]] list of the 5000 fastest growing privately held U.S. companies from 2011 through 2016.<ref name = pn1/><ref>{{cite web|url=http://www.sanluisobispo.com/news/business/article99368387.html|title=6 SLO Companies Make Inc. Magazine's 5000 'Fastest-Growing' List|publisher=The Tribune|first=Danielle |last=Ames|date=September 1, 2016|access-date=December 18, 2016}}</ref> Gary Nemmers became the CEO of IQMS in 2015.<ref name = paso/>

==History==
IQMS was founded by Randy Flamm and Nancy Flamm in 1989 and began with a [[MS-DOS|Microsoft DOS]]-based offering named IQGenesis.<ref name=trib>{{cite web| url=
http://www.sanluisobispo.com/2012/08/29/2204464/mindbody-iqms-inc-magazine-rank.html| title= Mindbody, IQMS on Inc. magazine list of fastest-growing U.S. companies| author = Julie Lynem| date= August 29, 2012|publisher= The Tribune| access-date= August 26, 2015}}</ref><ref name=te1>{{cite web| url=
http://www.technologyevaluation.com/research/article/IQMS-and-IFS-Going-Strong-without-Much-Help-from-the-Cloud.html| title= IQMS and IFS Going Strong (without Much Help from the Cloud)| author = PJ Jakovljevic| date= March 14, 2011|publisher= Technology Evaluation Centers| access-date= August 26, 2015}}</ref> IQMS moved its headquarters from Southern California to Paso Robles in 1995.<ref name=pe>{{cite web|url=http://www.sanluisobispo.com/news/business/article50917730.html|title=IQMS of Paso Robles Gets New Leader, More Investor Money to Fuel Growth|publisher=The Tribune|first=Julie|last=Lynewm|date=December 21, 2015|access-date=December 18, 2016}}</ref> In 1997, the company developed EnterpriseIQ, a manufacturing ERP software for the repetitive, process, and discrete manufacturing industries.<ref name = te1/> In 2011, IQMS product EnterpriseIQ 7.8.1 won the [[Stevie Awards|Stevie Award]] for "New Product or Service of the Year - Manufacturing".<ref name=stevie>{{cite web| url=
http://www.stevieawards.com/pubs/awards/403_2646_21015.cfm| title= New product awards & product management category honorees| date= 2011|publisher= The American Business Awards| access-date= August 26, 2015}}</ref> In 2012, the company introduced  the RTStation, a touch-screen device engineered for at-machine use providing shop floor functionality.<ref name=auto>{{cite web| url=
http://www.autofieldguide.com/articles/iqms-develops-hardware-solution| title= IQMS Develops Hardware Solution| date= April 25, 2012|publisher= Automotive Design and Production| access-date= August 26, 2015}}</ref> In August 2015, Nemmers succeeded Flamm as CEO while Flamm remained chairman of the board of directors.<ref name = paso/>

In April 2016, the company announced a browser-based user interface for its EnterpriseIQ Manufacturing ERP system named WebIQ. The release allowed its clients to access the system from mobile phones, tablets, and off site computers.<ref>{{cite web|url=http://www.cioreview.com/news/enterpriseiq-manufacturing-erp-gets-a-boost-with-the-launch-of-webiq-browserbased-ui-nid-13942-cid-34.html|title=EnterpriseIQ Manufacturing ERP Gets a Boost With the Launch of WebIQ Browser-Based UI|publisher=CIO Review|date=April 12, 2016|access-date=December 18, 2016}}</ref> In September 2016, the company announced the ability for clients to automatically back up their IQMS system data in secure offsite data storage locations.<ref>{{cite web|url=http://searchmanufacturingerp.techtarget.com/news/450305003/Demonstrator-platforms-target-3D-printing-technology-for-manufacturing|title=Demonstrator Platforms Target 3D Printing Technology for Manufacturing|publisher=TechTarget|first=Jim|last=O'Donnell|date=September 26, 2016|access-date=December 18, 2016}}</ref>

==Overview==
IQMS provides real-time manufacturing, production monitoring, [[quality control]], [[supply chain management]], [[customer relationship management]] and [[electronic business|e-business]] solutions through [[Enterprise resource planning|ERP]] and other software for the [[automotive industry|automotive]], [[health care industry|medical]], [[Plastics industry|plastics]] and [[manufacturing|general manufacturing]] industries.<ref name = te1/><ref name = trib/> The company's solution can be deployed as an on-premise, or cloud-based and supports the [[Linux]], [[Microsoft Windows]] and [[Unix]] operating systems.<ref>{{cite web|url=http://erpeople.walkme.com/2016/01/29/5-erp-cloud-software-platforms-thatll-save-you-200-hours-a-year/|title=5 ERP Cloud Software Platforms That'll Save You 200 Hours a Year|publisher=ERPeople|date=January 29, 2016|access-date=December 18, 2016}}</ref><ref>{{cite web|url=http://www.enterpriseappstoday.com/erp/how-to-buy-manufacturing-erp-software.html|title=Why You Need Manufacturing ERP Software|publisher=Enterprise Apps Today|first=Drew|last=Robb|date=September 30, 2016|access-date=December 18, 2016}}</ref>

The company is headquartered in California with offices across North America, Europe, and Asia.<ref name=pac>{{cite web| url=http://www.pacbiztimes.com/2014/08/22/iqms-readies-for-big-leagues-with-first-outside-investment/| title= IQMS readies for big leagues with first outside investment| date= August 22, 2014|publisher= Pacific Coast Business Times| access-date= August 26, 2015}}</ref> The company focuses on [[Enterprise resource planning|ERP]], [[manufacturing]], [[software]], and [[supply chain management]].<ref name = cio/>

==References==
{{reflist}}

[[Category:Software companies based in California]]
[[Category:ERP software companies]]
[[Category:Companies based in San Luis Obispo County, California]]
[[Category:Paso Robles, California]]
[[Category:1989 establishments in California]]
[[Category:Software companies established in 1989]]