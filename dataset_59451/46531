{{Infobox racing driver
| name              = James Bickford
| image             = James_Bickford_(Racing_Driver).jpg
| image_size        = <!-- defaults to 180px unless a value is entered -->
| caption           = James Bickford in the 2015 Interstate Plastics/Sunrise Ford Racing suit
| nationality       = {{flag|United States|label=American}}
| birth_name        = <!-- Only if different from name -->
| birth_date        = {{Birth date and age|1998|3|16}}
| birth_place       = [[Napa, California|Napa]], [[California]]
| related to        = [[Jeff Gordon]] (cousin)
| current series    = [[NASCAR]] [[K&N Pro Series West]]
| first year        = 2014
| current team      = Sunrise Ford Racing
| current owner     = Robert Bruncati
| car number        = 6
| engine            =
| crew chief        = [[Bill Sedgwick]]
| spotter           = 
| former teams      = 
| starts            = 14
| championships     =
| wins              = 2
| poles             = 
| best finish       = 1st
| year              = 2014
| location          = [[Stateline Speedway]]
| last finish       = 5th
| prev series       = <!-- previous series with line breaks -->
| prev series years = 
| titles            = Sunoco Rookie of the Year Award
| title years       = 2014
| awards            = 
| award years       = 
| updated           = February 16, 2015
}}
'''James Bickford''' (born March 16, 1998) is an American [[stock car racing]] driver. Residing in Napa, California. He currently competes full-time in the [[NASCAR]] [[K&N Pro Series West]], driving the No. 6 [[Ford Fusion (Americas)|Ford Fusion]].<ref name=Fifteen>{{cite news|title=Interstate Plastics Sponsors Jeff Gordon's Cousin James Bickford at the NASCAR K&N Pro Series in Sonoma, CA|url=https://www.interstateplastics.com/pr-interstate-plastics-sponsors-james-bickford-nascar-kn-series-sonoma.php|publisher=PRNewswire|date=2014-06-18}}</ref>

Bickford was born in Napa, California to parents Tom and Teresa Bickford.<ref name="Right Track">{{cite news|last1=Scott|first1=Tim|title=On the Right Track – Bickford, at 9, Following Path that Led Cousin Gordon to Top|url=http://www.timesheraldonline.com/ci_8289556|publisher=Vallejo Times-Herald|date=2008-02-17}}</ref>  Currently 18 years old, James is related to four-time NASCAR Sprint Cup champion and three-time Daytona 500 champion, [[Jeff Gordon]].<ref name=two>{{cite news|last1=Lair|first1=Keith|title=Jeff Gordon’s Cousin, James Bickford, Jumps Into NASCAR West Series|url=http://jeffgordon.com/jeff-gordon-cousin-james-bickford-nascar-west-series/|publisher=San Gabriel Valley Tribune|date=2014-03-19}}</ref> Gordon is James’ paternal cousin.<ref name="Right Track" />

Throughout his career, Bickford has won over 200 main event victories and taken home titles including Rookie of the Year at Ukiah Speedway (2010), Legends of the Pacific Rookie of the Year (2011), Semi-Pro Legends champion (2011), NASCAR Whelen All-American [[ASA Late Model Series|Late Model Series]] champion (2013) and K&N Pro Series West Rookie of the Year (2014).

== Career ==

=== 2004–2010: Quarter Midgets ===
In the late 1970s and early ‘80s, Tom Bickford was making [[Quarter Midget racing|Quarter Midget]] parts for Ron Stanley race cars, and shortly after, began making parts for nephew Jeff Gordon, as Jeff and his father, John, traveled across the country to compete in Quarter Midget races.<ref name=Three>{{cite news|last1=Broussal|first1=Jeannie|title=Gordon’s Young Cousin on Same Path to Stardom|url=http://napavalleyregister.com/sports/motor-sports/gordon-s-young-cousin-on-same-path-to-stardom/article_fd3e9cba-5333-11df-b541-001cc4c03286.html|publisher=Napa Valley Register|date=2010-04-29}}</ref> James began racing in the Quarter Midgets at the age of five.  At this age, Tom referred to racing as a “fathers-sons-sport”.<ref name="Right Track" /> Bickford won 69 of 75 races entered in just one of three different Quarter Midget Divisions he raced during 2007.<ref name=Four>{{cite news|last1=Schaefer|first1=Paul|title=Bickford Steps Up at All American – Jeff Gordon’s 15-Year-Old NASCAR Cousin|url=http://hometracks.nascar.com/nwaas_feature_060513_james-bickford_steps_up_at_all-american-speedway|work=NASCAR|date=June 5, 2013|accessdate=May 10, 2015}}</ref>

By the age of nine, Bickford was racing 48 weeks out of the year.<ref name=Five>{{cite news|last1=Lawley|first1=Erin|title=Bickford Striving to Follow Cousin’s Lead – Fourth Grader Already Owner of Hundreds of Trophies|url=http://napavalleyregister.com/sports/motor-sports/bickford-striving-to-follow-cousin-s-lead/article_168bcfb8-09c9-5638-a86b-ac59607728f3.html|publisher=Napa Valley Register|date=May 29, 2008}}</ref> By 2010, the young driver, who attended St. Apollinaris Catholic school, had won 15 Quarter Midget championships: four local track championships, two Nor Cal championships, four regional championships, two California 500 championships, two Western States Mona championships and one Dirt Road National, as well as close to 200 main events.<ref name="Three"/>

Toward the end of this time, Bickford wanted to move up from Quarter Midgets to [[Bandolero racing|Bandolero cars]], but needed to raise money to purchase the necessary equipment. In order to raise sufficient funds, he held a fundraiser called the "KGA" (Kids Golf Association) at age eleven. Tom Bickford noted, “It costs a lot of money making the step from little cars to big cars. But he definitely has the desire [to race]”.<ref name=Six>{{cite news|last1=Wilcox|first1=Andy|title=NASCAR Star’s Young Cousin Hosts Golf Tourney to Help Parents Fund Racing Costs|url=http://napavalleyregister.com/calistogan/sports/nascar-star-s-young-cousin-hosts-golf-tourney-to-help/article_589ca8cf-ea28-5bc6-a6a7-dca03d78b109.html|publisher=Napa Valley Register|date=July 9, 2009}}</ref>

=== 2010: Bandoleros ===
At the beginning of his Bandolero season, James was eleven years old. In his first race at Ukiah Speedway on April 10, 2010, he won the trophy dash and came in second in the A-main. He then won the Bandolero race at All-American Speedway in Roseville, California on April 17, along with trophy dash and heat race. In late April, 2010 he raced again at Ukiah and set a new track record with a lap of 15.324 seconds.<ref name="Three"/>

Regarding the Bandolero car, James said: “The cars are fragile and they bend easy. Some drivers go out on the first day, crash into a wall, and total the car. You have to be under control.”  While driving Bandoleros, he won six main-events in just 14 starts and won Rookie of the Year at Ukiah Speedway.<ref name="Three"/>

James was required to race 40 Bandolero races before moving up to a [[Legends car racing|Legends car]].<ref name="Three"/> Before getting his Legends car, James had the opportunity to test drive a friend’s. Tom had bought James’ Bandolero car from [[Tony Stewart]]’s General Manager, Joe Custer. At the end of a conversation between the two men in 2010, Joe had invited James to visit his home in Charlotte, North Carolina and try out his son, [[Cole Custer]]’s, Legends car.<ref name="Seven">{{cite news|last1=Broussal|first1=Jeannie|title=Young Racer Bickford Makes Jump to Legend|url=http://napavalleyregister.com/sports/motor-sports/young-racer-bickford-makes-jump-to-legend/article_9afec848-6658-11e0-b6a4-001cc4c002e0.html|publisher=Napa Valley Register|date=2011-04-13}}</ref>

=== 2011–2012: Legends ===
For the 2011 season, James moved up to drive a Legends car for the Legends of the Pacific Series. According to many stock car racers, Legends cars are among the hardest and most physically challenging cars to race. Regarding Legends cars, Matt Scott, the 2010 Late Model champion and a good friend of Bickford’s, said, “If you drive a Legends car, you can drive anything,” due to the fact that it has a lot more horsepower than a Bandolero.<ref name="Seven"/>

After his first Legends experience, James said, “When I hit the throttle on the straightaway, it threw me back in my seat and that was a rush of excitement I haven’t felt since I drove a Light B in Quarter Midgets back when I was eight [years old]…It is one of those cars you have to be very smooth with and also have to have good throttle control. The tires don’t have much grip and it’s very slippery”.<ref name="Seven"/> James competed in his first Legends race on April 2 at Stockton 99 Speedway in Stockton, California. He qualified third and finished the race in eighth place.<ref name="Seven"/>

After winning the Legends Main event in late October, James clinched the Semi-Pro Legends Championship.<ref name=Eight>{{cite news|title=Bickford Wins First Legends of the Pacific Feature – 13 Year old Legends of the Pacific Rookie, James Bickford, Scored His First Legend Main Event Win Last Saturday Night at Stockton 99 Speedway.|url=http://www.redlineoil.com/news_article.aspx?id=211|publisher=Red Line Synthetic Oil Corporation|date=September 4, 2011}}</ref> James also secured Rookie of the Year for Legends of the Pacific with a commanding 41-point lead with only three races left in the season.<ref name="Eight"/> Following his move to Legends from 2011-2012, Bickford had a big setback in August 2012 that ended his Legends season and disrupted his plans for 2013. The day before he was to be fitted for a seat in a late model that was under consideration for this year, he broke his arm playing high school football (prior to his freshman year); at 5’3” and 120 pounds, James ended up at the bottom of a dog pile. Although sidelined from driving, Bickford attended the last five races of his Legends racing season.<ref name="Four"/>

=== 2013: NASCAR Whelen All-American Series Late Models ===
In the 2013 season, James moved to full-size race cars as a rookie in NASCAR Whelen All-American Late Models Series. On May 11, he won his first career NASCAR feature.<ref name=Nine>{{cite news|last1=Broussal|first1=Jeannie|title=Napa’s James Bickford, 15, Earns Ride in NASCAR K&N PRO Series West|url=http://napavalleyregister.com/sports/motor-sports/napa-s-james-bickford-earns-ride-in-nascar-k-n/article_eb6b9c32-824f-11e3-9169-0019bb2963f4.html|publisher=Napa Valley Register|date=2014-01-21}}</ref> In mid-September, Bickford took home the 2013 NASCAR Whelen All American Late Model Series championship at All American Speedway in Roseville, California.<ref name=Ten>{{cite news|title=James Bickford Takes Title in Roseville|url=http://napavalleyregister.com/sports/motor-sports/james-bickford-takes-title-in-roseville/article_a84e5c68-1f59-11e3-838f-001a4bcf887a.html|publisher=Napa Valley Register|date=September 6, 2013}}</ref> Making this the sixth win of his rookie season, Bickford became the youngest driver in the 58-year history of All-American Speedway to win a Late-Model Series race and championship.<ref name="Nine"/>

Bickford was quoted as saying, “I feel like I’m on top of the world…I came into this season just looking to get some practice, but I never imagined winning the championship”.<ref name="Ten"/>

At this point in his career, Bickford accumulated 17 championships, 15 of them in the Quarter Midgets.<ref name="Four"/>

=== 2014: NASCAR K&N Pro Series West ===
Bickford began racing in the 2014 NASCAR K&N Pro Series West driving the no. 6 Sunrise Ford/ Interstate Plastics Ford Fusion after signing a two-year contract with car owner, Bob Bruncati.<ref name="Nine"/> In the 2014 season, Bickford replaced Derek Thorn, the 2013 K&N Pro Series West champion also signed by Bruncati. Regarding James, Bruncati said, “He is the best young driver I’ve seen in years”.<ref name=Eleven>{{cite news|title=Bickford Continues to Gain in NASCAR K&N-West|url=http://www.rotoworld.com/headlines/nas/27205/bickford-continues-to-gain-in-nascar-k-and-n-west|agency=Rotoworld|publisher=Rotoworld|date=May 29, 2014}}</ref>

Tom Bickford had described James’ reaction following his induction into the NASCAR Pro Series as him “walking on air”.<ref name="Nine"/> James raced his first road course of K&N Pro Series West at 1.99-mile Sonoma Raceway in Sonoma, California on June 21, 2014. After being asked about his first road race in the K&N Pro Series West at Sonoma, Bickford said, “That is one really tough track. There are only two places you can pass without the possibility of getting into a lot of trouble, but I’ve had some good instruction.” From Jeff Gordon, who toured the track with him and told him where to brake, accelerate, shift and pass. Gordon has five victories at Sonoma in the NASCAR Cup Series—more than any other driver.<ref name="Eleven"/>

In mid-September, Bickford locked in the 2014 NASCAR K&N Pro Series West Rookie of the Year title after placing third in the Utah Grand Prix at Miller Motorsports Park in Tooele, Utah. His third-place finish gave Bickford a 32-point lead over Rich DeLong III, who had been the only remaining competitor for Rookie of the Year. Bickford’s 32-point lead exceeded the possibility for DeLong to surpass Bickford in the final two races, as ten points is the maximum number of points allotted to gain per race.<ref name=Twelve>{{cite news|author=Register Staff|title=Auto Racing: Bickford Locks Up Rookie Crown|url=http://napavalleyregister.com/sports/motor-sports/auto-racing-bickford-locks-up-rookie-crown/article_14c027cf-d89b-5738-ac15-0170d9b384f6.html|publisher=Napa Valley Register|date=September 18, 2014}}</ref> Bickford became the third consecutive driver (and fifth overall) to earn Rookie of the Year driving for Bruncati, known for providing the opportunity to young drivers to showcase their talent in the Series. Bickford joined teammates [[Dylan Lupton]] (2013), Austin Dyne (2012), Luis Martinez Jr. (2010) and Jason Bowles (2007) as drivers who have won title for Rookie under Bruncati.<ref name=Thirteen>{{cite news|last1=Tejeda|first1=John|title=Bickford Clinches Sunoco Rookie of The Year Wraps Up Rookie Honor In K&N West With Two Races Remaining|url=http://hometracks.nascar.com/nknps-east_release_091614_James-Bickford_Clinches_Sunoco_Rookie-of-The-Year|agency=NASCAR|publisher=NASCAR Media Group|date=September 7, 2014}}</ref>

Bickford was ranked fifth overall in the K&N West Pro Series. Bickford went from a 25th-place finish in his NASCAR K&N Pro Series West debut to winning his first career race and now top rookie honors in the series.<ref name="Thirteen"/> Since the beginning of his NASCAR Pro Series West season, Bickford collected more points than any K&N West regular competitor.<ref name="Twelve"/><ref name=Fourteen>{{cite news|title=James Bickford Sponsor Interstate Plastics to Raffle New Pro Series Detailing Products at NASCAR K&N West Pro Series|url=https://www.interstateplastics.com/pr-James-Bickford-Sponsor-Interstate-Plastics-to-Raffle-New-Pro-Series-Detailing-Products-at-NASCAR-KN-West-Pro-Series.php|publisher=PRNewswire|date=October 6, 2014}}</ref> He has accumulated one victory, eight top-five’s and nine top-twelve finishes. Bickford will be awarded on December 13 at the Charlotte (NC) Convention Center at the [[NASCAR Hall of Fame]]. Bickford competed at the All-American Speedway in Roseville, California on Oct. 11, 2014, and took home a second-place finish.<ref name=Sixteen>{{cite news|title=Former 49er Dan Bunz Drives for Interstate Plastics in NASCAR Media Blitz 100, October 11th in Roseville, Calif.|url=https://www.interstateplastics.com/pr-Former-49er-Dan-Bunz-Drives-for-Interstate-Plastics-in-NASCAR-Media-Blitz-100-October-11th-2014-in-Roseville-CA.php|publisher=PRNewswire|date=October 8, 2014}}</ref> The final race series will take place at the one-mile Phoenix International raceway on Nov. 6.<ref name="Thirteen"/>

=== 2015: NASCAR K&N Pro Series West ===
Bickford kicked off his 2015 NASCAR K&N Pro Series West season on March 28, 2015 at Kern County Raceway Park in Bakersfield, California. He finished fourth in the night's race. Bickford has been invited into NASCAR Next, one of two drivers from the Pro Series West to be invited into NASCAR's elite group of up-and-comings. This invitation was announced on May 5, 2015 at the NASCAR Hall of Fame in Charlotte, North Carolina.<ref>{{cite web|url=http://www.nascar.com/en_us/news-media/articles/2015/5/5/james-bickford-nascar-next-class-member-cousin-of-jeff-gordon.html|title=JAMES BICKFORD LOOKS TO CARVE HIS OWN NICHE|last=Albert|first=Zack|work=NASCAR|publisher=NASCAR Media Group|date=May 5, 2015|accessdate=May 10, 2015}}</ref>

On April 11 at [[Irwindale Event Center|Irwindale Speedway]] in Irwindale, CA, a series of events led to an unprecedented 17th-place finish during the second race of James Bickford's 2015 season, with Bickford stating that a series of bad luck was responsible for the finish, including stolen and slashed tires, a lack of radio signal during practice, and problems with the vehicle's suspension during the race.<ref>{{cite web|url=https://www.interstateplastics.com/james-bickford-blog.php#Irwindale|title=A Lap Around the Track with NASCAR’s James Bickford, Irwindale Speedway|publisher=Interstate Plastics|date=April 11, 2015|accessdate=May 13, 2015}}</ref>

Bickford's third race of the season, the NAPA Auto Parts Wildcat 150 at [[Tucson Raceway Park|Tucson Speedway]] in Tucson, AZ, ended with a 6th place.<ref>{{cite web|url=https://www.interstateplastics.com/james-bickford-blog.php#tucson|title=A Lap Around the Track with NASCAR’s James Bickford, Tucson Speedway|publisher=Interstate Plastics|date=May 2, 2015|accessdate=May 13, 2015}}</ref>

== Fundraising efforts ==

=== 2010: The Kids Golf Association, “KGA” ===
At eleven years old, Bickford was hoping to begin racing Bandolero cars, but racing was becoming a very expensive hobby. In order to move up to the bigger cars, he was in need of a [[HANS Device]] (first required after the death of 2003 Top Fuel Rookie of the Year [[Darrell Russell (dragster driver)|Darrell Russell]]), which can now cost close to $1,000, and a Bandolero car, which can cost about $5,000.<ref name="Six"/>

The fifth grader asked his parents if he could fund-raise by organizing a golf tournament, and they agreed but ensured him he’d have to do it alone. He decided to organize the golf tournament for children at Vintner’s Golf Club in Yountville, California. He originally planned a four-day event but was only approved for one day; he did all the prize funding and was “super professional the whole day around,” according to Jason Boldt, General Manager of the golf club.<ref name="Six"/>

The “KGA Tournament”—as in the Kids Golf Association—was to have four-member teams playing 18 holes in a best-ball format, with a $50 entry fee for each player. Bickford was able to get $100-hole sponsorships for each of the nine tee boxes, and 16 players (including himself and Boldt’s son, Christian). At a $50 entry-fee, Bickford took home $35 per player.<ref name="Six"/>

Along with Jeff Gordon Motorsports, Bickford Precision and Vintner’s Golf Club, the hole sponsors were The Wright Team: Coldwell Banker Brokers of the Valley, Buffalo’s Shipping, Rising World T.V., Napa Golf Course, Wine Valley Insurance Services, Inc., and KMR Builders, Inc.<ref name="Six"/>

Bickford’s team, led by Ariel Caro, won the tournament by one-stroke with a four-over-par 72. Tom added, “I told him he can’t win his own golf tournament so he donated back whatever he got.” In response to the feat of throwing the fundraiser, Tom recalled how his son used a never-say-die determination to win almost unwinnable races at the American Quarter Midget Club Nationals the previous year in Rancho Cordova, California.<ref name="Six"/>

==References==
{{Reflist}}

==External links==
* {{Racing-Reference driver|James_Bickford}}

{{DEFAULTSORT:Bickford, James}}
[[Category:1998 births]]
[[Category:21st-century American racing drivers]]
[[Category:Living people]]
[[Category:NASCAR drivers]]
[[Category:People from Napa, California]]
[[Category:Racing drivers from California]]
[[Category:Sportspeople from the San Francisco Bay Area]]