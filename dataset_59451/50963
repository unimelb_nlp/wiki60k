{{Use mdy dates|date=May 2013}}
{{Infobox journal
| title = Columbia Law Review
| cover =
| editor = Kelsey A. Ruescher <ref>http://columbialawreview.org/announcements-events/</ref>
| discipline = [[Jurisprudence]]
| abbreviation = Columbia Law Rev.
| publisher = Columbia Law Review Association, Inc.<ref>https://www.jstor.org/journal/colulawrevi</ref>
| country = United States
| frequency = 8/year
| history = 1901–present
| openaccess =
| impact = 3.070
| impact-year = 2010
| website = http://www.columbialawreview.org/
| CODEN = COLRAO
| ISSN = 0010-1958
| eISSN =
| LCCN = 29-10105
| OCLC = 01564231
| JSTOR = 00101958
}}
The '''''Columbia Law Review''''' ([[Bluebook]] abbreviation: '' Colum. L. Rev.'') is a [[law review]] edited and published by students at [[Columbia Law School]]. The journal publishes scholarly articles, [[essay]]s, and student notes.

It was established in 1901 by Joseph E. Corrigan and [[John M. Woolsey]], who served as the review's first [[editor-in-chief]] and secretary. The ''Columbia Law Review'' is one of four law reviews that publishes the ''[[Bluebook]]''.

== Impact ==
The ''Columbia Law Review'' ranked second for submissions and citations within the legal academic community, after the ''[[Harvard Law Review]]''.<ref>[http://lawlib.wlu.edu/LJ/ Law Journals: Submissions and Ranking<!-- Bot generated title -->]</ref> According to the ''[[Journal Citation Reports]]'' it has a 2009 [[impact factor]] of 3.610, ranking it third out of 116 journals in the category "Law".<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=June 26, 2011}}</ref>

== Notable alumni ==
Alumni of the ''Columbia Law Review'' include United States Supreme Court Justices [[William O. Douglas]] and [[Ruth Bader Ginsburg]], numerous judges on U.S. Courts of Appeals including [[Wilfred Feinberg]], [[Harold Leventhal]], [[Paul R. Hays]], [[Harold Medina]], [[Jerre Stockton Williams]], [[James Alger Fee]], and [[Daniel M. Friedman]]; numerous judges on U.S. District Courts including [[Jack B. Weinstein|Jack Weinstein]], [[Miriam Goldman Cedarbaum]], [[Denise Cote]], [[William Bernard Herlands]], [[Joseph Frank Bianco]], [[John S. Martin, Jr.]], [[Edmund Louis Palmieri]], [[Alexander Holtzoff]], [[Dickinson Richards Debevoise]], [[Richard Franklin Boulware II]] and [[James Edward Doyle (judge)|James Edward Doyle]]; United States Solicitors General [[Charles Fried]] and [[Donald Verrilli Jr.]]; Chairwoman of the [[Securities and Exchange Commission]], [[Mary Jo White]];  Director of the CIA [[William Colby]]; U.S. Attorney for the Southern District of New York [[Preet Bharara]];  Chairman of the [[Federal Reserve Bank of New York]] and Director of the [[United States National Economic Council|National Economic Council]], [[Stephen Friedman (PFIAB)]]; Columbia University president [[Lee C. Bollinger]], Columbia Law School deans Young B. Smith, [[Michael I. Sovern]], and [[Barbara Aronstein Black]], Columbia Law  School professors [[Herbert Wechsler]], [[Harvey Goldschmid]], [[R. Kent Greenawalt]], [[Gillian E. Metzger]] and, [[E. Allan Farnsworth]], Yale Law School professors [[Felix S. Cohen]] and [[Geoffrey C. Hazard, Jr.]], Duke University School of Law professor [[George C. Christie]], New York University Law School professor [[Samuel Estreicher]], Michigan Law School professor [[Mark D. West]], Berkeley professor and criminal law scholar [[Sanford Kadish]]. former New York Governor [[George Pataki]], two-time SEC General Counsel [[David M. Becker]], and NBA Commissioner [[David Stern]], and prominent attorneys [[Louis S. Weiss]], [[Daniel Neff]], [[Gary P. Naftalis]], [[George Davidson (attorney)|George Davidson]], [[Arthur Garfield Hays]], [[John H. Slate]], and [[Charles Rembar]], among others.{{citation needed|date=February 2015}}

== Notable articles ==
{{According to whom|date=June 2011}}
*{{cite journal |last=Cohen |first=Felix S. |authorlink=Felix S. Cohen |coauthors= |year=1935 |month= |title=Transcendental Nonsense and the Functional Approach |journal=Columbia Law Review |volume=35 |issue=6 |pages=809–849 |doi=10.2307/1116300|accessdate= |quote= |jstor=1116300}}
*{{cite journal |last=Fuller |first=Lon L. |authorlink=Lon L. Fuller |coauthors= |year=1941 |month= |title=Consideration and Form |journal=Columbia Law Review |volume=41 |issue=5 |pages=799–824 |doi=10.2307/1117840|accessdate= |quote= |jstor=1117840}}
*{{cite journal |last=Frankfurter |first=Felix |authorlink=Felix Frankfurter |coauthors= |year=1947 |month= |title=Some Reflections on the Reading of Statutes |journal=Columbia Law Review |volume=47 |issue=4 |pages=527–546 |doi=10.2307/1118049|accessdate= |quote= |jstor=1118049}}
*{{cite journal |last=Hart |first=Henry M. |authorlink=Henry M. Hart |coauthors= |year=1954 |month= |title=The Relations Between State and Federal Law |journal=Columbia Law Review |volume=54 |issue=4 |pages=489–542 |doi=10.2307/1119546|accessdate= |quote= |jstor=1119546}}
*{{cite journal |last=Wechsler |first=Herbert |authorlink=Herbert Wechsler |coauthors= |year=1954 |month= |title=The Political Safeguards of Federalism: The Role of the States in the Composition and Selection of the National Government |journal=Columbia Law Review |volume=54 |issue=4 |pages=543–560 |doi=10.2307/1119547|accessdate= |quote= |jstor=1119547}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.columbialawreview.org}}

[[Category:American law journals]]
[[Category:Columbia University publications]]
[[Category:Publications established in 1901]]
[[Category:General law journals]]
[[Category:English-language journals]]
[[Category:Law journals edited by students]]