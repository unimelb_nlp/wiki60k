{{Use dmy dates|date=May 2013}}
{{Infobox military person
|name=Ted N. Branch
|birth_date=
|death_date=
|birth_place=[[Long Beach, Mississippi]]
|death_place=
|placeofburial=
|placeofburial_label=
|image=Thumb VADM Branch, Ted.jpg
|caption=
|term_start=February 2011
|nickname=Twig
|allegiance=USA
|branch=[[File:United States Department of the Navy Seal.svg|25px]] [[United States Navy]]
|serviceyears=1979–2016
|rank=[[File:US-O9 insignia.svg|23px]] [[Vice admiral (United States)|Vice Admiral]]
|commands={{unbulleted list| Commanding Officer, [[Strike Fighter Squadron 15]] | Commanding Officer, [[USS Coronado (AFG 11)]] | Commanding Officer, [[USS Nimitz (CVN 68)]] | Commander, [[Carrier Strike Group 1 (Carl Vinson Strike Group)]] | Commander, Naval Air Force Atlantic | Director, [[Naval Intelligence]]}}
|unit=
|battles={{unbulleted list| [[Operation Southern Watch]] | [[Operation Urgent Fury]] | [[Operation Earnest Will]] | [[Operation Deliberate Force]] | [[Operation Iraqi Freedom]] | [[Operation Unified Response]]}}
|awards={{unbulleted list| [[Distinguished Service Medal]]  (1) | [[Legion of Merit]] (6) | [[Defense Meritorious Service Medal]] | [[Meritorious Service Medal (United States)|Meritorious Service Medal]] (2) | [[Air Medal]] ([[Strike/Flight numeral]] 1) | [[Navy and Marine Corps Commendation Medal]] (3, w/ [[Combat V]]) | [[Navy and Marine Corps Achievement Medal]]}}
|laterwork=
}}

'''Ted N. "Twig" Branch''' retired as a [[Vice Admiral]] in the [[United States Navy]] on October 1, 2016, after serving the last three years of his 37 year career as the Deputy Chief of Naval Operations for [[information warfare]]. In that capacity, he was the resource sponsor for the N2N6 portfolio which includes program investments for assured command and control, battlespace awareness, and integrated fires. He was the Navy’s Chief Information Officer, the Director of Navy Cybersecurity, the leader of the Information Warfare Community, and the Director of Naval Intelligence. Admiral Branch was questioned by the Department of Justice regarding the Glenn Defense Marine Asia investigation in November 2013 and his access to classified information was suspended by the Secretary of the Navy.

==Education==
* [[Bachelor of Science]], [[United States Naval Academy]], 1979
* [[Master's degree|Master]] of [[international relations]], [[Naval War College]]

==Military career==
Branch was a career [[United States Naval Aviator|naval aviator]], specializing in [[LTV A-7 Corsair II|A-7 Corsairs]] and [[McDonnell Douglas F/A-18 Hornet|F/A-18 Hornets]]. He has flown combat missions over [[Grenada]], [[Lebanon]], [[Bosnia-Herzegovina]], and [[Iraq]]. He has served as the commanding officer of [[VFA-15|VFA-15 (Strike Fighter Squadron 15)]], the {{USS|Coronado|AGF-11}}, the {{USS|Nimitz|CVN 68}}, [[Carrier Strike Group One|Carrier Strike Group One (Carl Vinson Strike Group)]], and [[Commander, Naval Air Force U.S. Atlantic Fleet|Naval Air Force Atlantic]]. He has also served on the [[Joint Staff]] at [[the Pentagon]].<ref>[http://www.navy.mil/navydata/bios/navybio.asp?bioID=426 Navy.mil Leadership Biographies<!-- Bot generated title -->]</ref>

As the commander of the {{USS|Nimitz|CVN 68}}, then-[[Captain (Navy)|Captain]] Branch was featured prominently in the Emmy award-winning documentary television series ''[[Carrier (documentary)|Carrier]]''.<ref>[http://www.pbs.org/weta/carrier/the_crew.htm CARRIER . The Crew | PBS<!-- Bot generated title -->]</ref>

In January 2010, as the Commander of CARL VINSON Strike Group, Branch led the initial US Navy response to the Haiti earthquake.

From February 2011 to July 2013 (2 years 6 months), Branch served as the Commander of Naval Air Force, U.S. Atlantic Fleet, headquartered in Norfolk, VA. In that role he was responsible for manning, training, and equipping all elements of Naval Air Force, Atlantic — over 1000 aircraft, 40 thousand personnel, and six nuclear-powered aircraft carriers. While there, he also established the Interoperability Coordination Office for the United Kingdom's new aircraft carriers and  the introduction of the F35 aircraft.

On 16 May 2013, Branch was nominated by [[United States Secretary of Defense|Secretary of Defense]] [[Chuck Hagel]] to become the Director of Naval Intelligence (DNI) and the Deputy [[Chief of Naval Operations]] (DCNO)for Information Dominance (later renamed Information Warfare). He was confirmed by the [[United States Senate]] in July 2013 and automatically promoted to [[Vice Admiral]] (O-9).<ref>[http://navintpro.net/?p=3419 Formal Nomination as DCNO for Information Dominance | Naval Intelligence Professionals<!-- Bot generated title -->]</ref>

While serving in the DCNO/DNI roles, he also took on the position of Directory of Navy Cybersecurity, becoming the Navy's leading proponent for cybersecurity. In this role, he stood up a year-long matrixed organization called Task Force Cyber Awakening to develop and implement Navy’s cybersecurity strategy. That group developed the CYBERSAFE program to design and build in cyber resiliency, and inculcate an enhanced culture of cybersecurity throughout the U.S. Navy. When the Task Force completed their efforts, he stood up the Navy Cybersecurity Division within N2N6 so the process of instituting CYBERSAFE and a culture of cybersecurity could continue.

==Corruption Scandal==
On November 8, 2013, the Navy suspended VADM Branch's access to classified information in connection with a Department of Justice investigation involving Singapore-based defense contractor, [[Glenn Marine Group|Glenn Defense Marine Asia]].<ref>{{cite web|last=Whitlock|first=Craig|title=Two admirals face probe in Navy bribery scheme|work=[[Washington Post]] |date=November 8, 2013|url=http://www.washingtonpost.com/world/national-security/two-admirals-face-probe-in-navy-bribery-scheme/2013/11/08/d2dc063a-48d8-11e3-bf0c-cebf37c6f484_story.html?clsrd|accessdate=November 8, 2013}}</ref><ref>{{cite web|last=Perry|first=Tony |title=Two admirals under investigation in Navy bribery scandal|work=[[Los Angeles Times]]|date=November 8, 2013|url=http://www.latimes.com/local/lanow/la-me-ln-navy-admirals-20131108,0,4094344.story#axzz2k6D0KX8h|accessdate=November 8, 2013}}</ref> The investigation as to Branch involves a non-criminal accusation of "inappropriate conduct" associated with his acceptance of gifts from Glenn Marine during his tour as commanding officer of USS Nimitz on a western Pacific/Persian Gulf deployment in 2005.<ref>{{cite web |last=Kenny|first=Steve |author2=Drew, Christopher |title=Contracting Case Implicates 2 Admirals |work=[[New York Times]]|date=November 8, 2013|url=https://www.nytimes.com/2013/11/09/us/bribery-case-implicates-2-admirals.html?partner=rss&emc=rss&smid=tw-nytimes|accessdate=November 9, 2013}}</ref>  Although Branch remained in his post during the long Justice Department investigation, his access to classified information remained suspended, relegating him to unclassified duties.

In September 2015, the Navy formally nominated Rear Admiral [[Elizabeth L. Train]] to succeed Branch as Director of Naval Intelligence.<ref>{{cite web|last=Larter|first=David|title=Navy nominates new intel boss after two year stalemate|work=Military Times|url=http://www.militarytimes.com/story/military/pentagon/2015/09/27/elizabeth-train-ted-branch-director-navy-intelligence-nomination-fat-leonard/72739896/|date=September 27, 2015|accessdate=November 13, 2015}}</ref> 

In January of 2016, the Washington Post reported that Branch was still functioning in his role, yet was "barred from reading, seeing or hearing classified information since November 2013", due to the suspension tied to the investigation.<ref>[https://www.washingtonpost.com/news/checkpoint/wp/2016/01/27/the-admiral-in-charge-of-navy-intelligence-has-not-been-allowed-to-see-military-secrets-for-years/ Craig Whitlock, "The admiral in charge of Navy intelligence has not been allowed to see military secrets for years", Washington Post, January 28  ]</ref>

Also in January 2016, the Daily Mail reported, "Branch is yet to be charged, but he has not been cleared either - leaving the Navy in the bizarre predicament of having an intelligence chief who is unable to read top secret documents."<ref>{{cite web| last =Gillman| first =Ollie| title =Admiral in charge of Navy intelligence has been barred from seeing military secrets| work =[[Daily Mail Online]] | date =January 28, 2016| url =http://www.dailymail.co.uk/news/article-3420979/Admiral-charge-Navy-intelligence-barred-seeing-military-secrets-2013-amid-20million-corruption-scandal.html| accessdate =January 28, 2016}}</ref>

On April 1, 2016, the ''Navy Times'' reported that the Navy had withdrawn Train's nomination to succeed Branch in favor of Vice Admiral Jan Tighe, previously commander of the Navy’s Fleet Cyber Command and Commander, U.S. Tenth Fleet.  Vice Admiral Tighe relieved Vice Admiral Branch as Deputy CNO for Information Warfare/Director of Naval Intelligence in July 2016.  Branch’s access to classified information remained suspended until he retired.

==Military Awards and Decorations==
[[File:Information dominance warfare officer device.PNG|180px]] [[Information Dominance Corps|Information Dominance Warfare Officer Badge]]
	
[[File:Naval Aviator Badge.jpg|180px]] [[Naval Aviator|Naval Aviator Wings]]

{|
|-
|{{ribbon devices|number=1|type=award-star|ribbon=Navy Distinguished Service ribbon.svg|width=80}}
|[[Distinguished Service Medal]]
|-
|{{ribbon devices|number=5|type=award-star|ribbon=Legion of Merit ribbon.svg|width=80}}
|[[Legion of Merit]] (with one silver [[award star]])
|-
|{{ribbon devices|number=0|type=oak|ribbon=Defense Meritorious Service Medal ribbon.svg|width=80}}
|[[Defense Meritorious Service Medal]]
|-
|{{ribbon devices|number=1|type=award-star|ribbon=Meritorious Service Medal ribbon.svg|width=80}}
|[[Meritorious Service Medal (United States)|Meritorious Service Medal]] (with one gold award star)
|-
|[[File:Air Medal ribbon.svg|80px]]<span style="position:relative; top: 0px; left: -22px; display: inline-block; width: 0;">[[File:Award numeral 1.png|15px]]</span>
|[[Air Medal]] with [[Strike/Flight numeral]] 1
|-
|{{ribbon devices|number=2|type=award-star|other_device=v|ribbon=Navy and Marine Corps Commendation ribbon.svg|width=80}}
|[[Navy Commendation Medal]] (with two gold award stars and [[Combat V]])
|-
|[[File:Navy and Marine Corps Achievement ribbon.svg|80px]]
|[[Navy Achievement Medal]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Joint Meritorious Unit Award-3d.svg|width=80}}
|[[Joint Meritorious Unit Award]]
|-
|{{ribbon devices|number=1|type=service-star|ribbon=Navy Unit Commendation ribbon.svg|width=80}}
|[[Navy Unit Commendation|Navy Unit Commendation Ribbon]] (with one bronze [[service star]])
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Navy Meritorious Unit Commendation ribbon.svg|width=80}}
|[[Navy Meritorious Unit Commendation|Navy Meritorious Unit Commendation Ribbon]]
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Battle Effectiveness Award ribbon, 4th award.svg|width=80}}
|[[Navy "E" Ribbon]] w/ [[Wreathed Battle E device]] (5 awards)
|-
|{{ribbon devices|number=1|type=service-star|ribbon=Navy Expeditionary Medal ribbon.svg|width=80}}
|[[Navy Expeditionary Medal]] (with one bronze service star)
|-
|{{ribbon devices|number=1|type=service-star|ribbon=National Defense Service Medal ribbon.svg|width=80}}
|[[National Defense Service Medal]] (with one bronze service star)
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Armed Forces Expeditionary Medal ribbon.svg|width=80}}
|[[Armed Forces Expeditionary Medal]]
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Southwest Asia Service Medal ribbon (1991-2016).svg|width=80}}
|[[Southwest Asia Service Medal]]
|-
|[[File:Global War on Terrorism Expeditionary ribbon.svg|border|80px]]
|[[Global War on Terrorism Expeditionary Medal]]
|-
|[[File:Global War on Terrorism Service ribbon.svg|80px]]
|[[Global War on Terrorism Service Medal]]
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Armed Forces Service Medal ribbon.svg|width=80}}
|[[Armed Forces Service Medal]]
|-
|{{ribbon devices|number=5|type=service-star|other_device=bss|ribbon=Sea Service Deployment Ribbon.svg|width=80}}
|[[Navy Sea Service Deployment Ribbon]] (with silver and bronze service star)
|-
|[[File:United States Navy Pistol Marksmanship Ribbon with expert device.svg|80px]]
|[[Navy Expert Pistol Marksmanship Medal]]
|}

[[File:Joint Chiefs of Staff seal.svg|115px]] [[Office of the Joint Chiefs of Staff Identification Badge]]

==References==
{{reflist}}

==External links==
{{commons category}}
* [http://www.navy.mil/navydata/bios/navybio.asp?bioID=426 Official Navy Biography]

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Kendall L. Card]]}}
{{s-ttl|title=Director of the [[Office of Naval Intelligence]]|years=2013–2016}}
{{s-aft|after= [[Jan E. Tighe]]}}
{{s-end}}

{{DEFAULTSORT:Branch, Ted}}
[[Category:United States Navy admirals]]
[[Category:United States Naval Academy alumni]]
[[Category:Recipients of the Legion of Merit]]
[[Category:People from Long Beach, Mississippi]]
[[Category:Living people]]
[[Category:Directors of the Office of Naval Intelligence]]