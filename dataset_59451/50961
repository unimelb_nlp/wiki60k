{{Italic title}}
{{Infobox journal
| cover        =
| editor = Chance L. Goldberg<br />(2014-15)<br />Alexander M. Weaver<br />(2015-16)
| discipline   = [[Jurisprudence]]
| abbreviation = CJEL
| publisher    = European Legal Studies Center, Columbia Law School
| country      = [[United States]]
| frequency    = Tri-annual
| history      = 1994-present
| openaccess   =
| impact       =
| impact-year  =
| website      = http://law.columbia.edu/cjel
| ISSN         = 1076-6715
| eISSN        =
}}
The '''''Columbia Journal of European Law''''' was established in 1994 and is one of the few [[legal publications]] in the United States devoted exclusively to [[European law]]. It currently ranks among the top five foreign and civil law journals in the US and is the most cited journal of European legal scholarship worldwide.<ref>[http://lawlib.wlu.edu/LJ/ Law Journals: Submissions and Ranking<!-- Bot generated title -->]</ref>
The journal is published under the auspices of the European Legal Studies Center<ref>http://www.law.columbia.edu/center_program/european_legal</ref> at [[Columbia Law School]]. Columbia students are entirely responsible for CJEL’s publication. Close collaboration is kept with the [[Université catholique de Louvain|Catholic University of Leuven]], Belgium, which keeps the journal abreast of recent developments in the case law of the [[European Court of Justice]]. CJEL publishes three issues per year—Winter, Spring, and Summer—and contains articles exploring every dimension of European law in its broadest sense. This includes the law of the [[European Union]] and law at the national or regional levels, as well as jurisprudential questions relevant to the development of law and legal institutions in Europe. Its articles cover a range of issues and are authored by leading academics and practitioners. Many articles are comparative in nature and directly relevant to the United States.

Every CJEL article is cataloged and archived on its website, although only the introduction or abstract is available online. Full articles are available in the print version, as well as on [[Westlaw]], [[LexisNexis]], and [[HeinOnline]].

== Online Supplement ==
In April 2009, CJEL launched an online edition<ref>http://cjel.net/online/</ref> to supplement the print journal. This online supplement is available on the journal’s website in its entirety. Because of the space and time limitations inherent in print publications, the online supplement was intended to allow a more rapid exploration of important case law and legislative developments, in order to reflect the ever increasing pace of the dissemination of legal information in the modern era. Although the majority of the articles published online are produced by post-graduate legal students at [[Columbia Law School]], the journal actively seeks submissions from outside of the school.

In 2015, CJEL introduced its online blog, ''Preliminary Reference''.<ref>[http://web.law.columbia.edu/journal-european-law/preliminary-reference-cjel-blog ''Preliminary Reference'']</ref>

== Merger with the ''Columbia Journal of East European Law'' ==
In Spring 2009 CJEL absorbed the ''Columbia Journal of East European Law'', and starting with its Fall 2009 issue will publish an article on [[East European]] law in each issue. The Editorial Boards of each journal believed that such a merger would best reflect the rapid eastward expansion of the [[European Union]], which has added 12 Central and East European nations in the past five years, as well as the growth of other transnational organizations, such as the [[Council of Europe]].

== Significant articles ==
*Horatia Muir Watt, Choice of Law in Integrated and Interconnected Markets: A Matter of Political Economy, 9 Colum. J. Eur. L. 383 (2003)<ref>http://www.cjel.net/print/9_3-muirwatt/</ref>
*[[Mauro Bussani]] & Ugo Mattei, The Common Core Approach to European Private Law, 3 Colum. J. Eur. L. 339 (1997) <ref>http://www.cjel.net/print/3_3-bussani/</ref>
*Vivian Grosswald Curran, Romantic Common Law, Enlightened Civil Law: Legal Uniformity and the Homogenization of the European Union, 7 Colum. J. Eur. L. 63 (2001)<ref>http://www.cjel.net/print/7_1-curran/</ref>
*Gabrielle S. Friedman & James Q. Whitman, The European Transformation of Harassment Law: Discrimination Versus Dignity, 9 Colum. J. Eur. L. 241 (2003)<ref>http://www.cjel.net/print/9_2-friedman/</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.law.columbia.edu Columbia Law School]
* [http://law.wlu.edu/library/mostcited/index.asp Washington & Lee Citation Ranking of Legal Periodicals]

{{DEFAULTSORT:Columbia Journal Of European Law}}
[[Category:American law journals]]
[[Category:Columbia University publications]]
[[Category:European law journals]]
[[Category:Law journals edited by students]]