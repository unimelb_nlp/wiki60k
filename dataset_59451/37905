{{Good article}}
{{Infobox graphic novel
<!--Wikipedia:WikiProject Comics-->
|title          = Binky Brown Meets the Holy Virgin Mary
|image          = BinkyBrownOriginalCover.jpg
|caption        = Original comic-book cover<br />[[Last Gasp]], 1972
|alt            = A comic book cover.  The Virgin Mary stands over a kneeling boy who covers his groin with his hands.
|publisher      = [[Last Gasp|Last Gasp Eco Funnies]]
|date           = March 1972
|creator        = [[Justin Green (cartoonist)|Justin Green]]
|pages          = 44
|sort           = Binky Brown Meets the Holy Virgin Mary
|single_creator = y
}}

'''''Binky Brown Meets the Holy Virgin Mary''''' is a comic-book story by American cartoonist [[Justin Green (cartoonist)|Justin Green]], published in 1972.  Green takes the persona of Binky Brown to tell of the "[[Scrupulosity|compulsive neurosis]]" with which he struggled in his youth and which he blamed on his strict [[Roman Catholic]] upbringing.  Green was later diagnosed with [[obsessive–compulsive disorder]] (OCD) and came to see his problems in that light.

In the story, sinful [[intrusive thought|thoughts that he cannot control]] torment Binky Brown; to his alarm, phallic objects become literal penises and project what he calls "pecker rays" at religious objects such as churches and statues of the [[Mary (mother of Jesus)|Virgin Mary]].  He develops an internal set of rules to obey and punishments for breaking them.  The torment does not subside, and he comes to reject the Catholic Church in defiance as the source of it.  The work combines a wide variety of visual and narrative techniques in a style that echoes the torment of its protagonist.

''Binky Brown'' had an immediate influence on contemporaries in {{not a typo|[[underground comix]]}}: such cartoonists as [[Aline Kominsky-Crumb|Aline Kominsky]], [[Robert Crumb]], and [[Art Spiegelman]] soon turned to producing similarly confessional works.  ''Binky Brown'' has gained a reputation as the first major work of autobiography in English-language comics, and many aspects of its approach have become widespread in underground and [[alternative comics]].

==Background==

[[Justin Green (cartoonist)|Justin Green]] (b.&nbsp;1945) was born to a Jewish father and Catholic mother and raised Catholic.{{sfn|Green|2010|p=56}}  As a child he at first attended a Catholic [[parochial school]], and later transferred to a school where most students were Jews.{{sfn|Hatfield|2005|p=135}}  He rejected the Catholic faith in 1958 as he believed it caused him "[[Scrupulosity|compulsive neurosis]]"{{sfn|Hatfield|2005|p=134}} that decades later was diagnosed as [[obsessive–compulsive disorder]] (OCD).{{sfn|Green|2010|p=58}}

Green was studying painting at the [[Rhode Island School of Design]] when in 1967 he discovered the work of [[Robert Crumb]] and turned to cartooning, attracted to what he called Crumb's "harsh drawing stuffed into crookedly-drawn panels".{{sfn|Chute|2010|p=17}}  He experimented with his artwork to find what he called an "inherent and automatic style as a conduit for the chimerical forms in {{interp|his|original=my}} own psyche".{{sfn|Manning|2010}}  He dropped out of an [[Master of Fine Arts|MFA]] program at [[Syracuse University]]{{sfn|Levin|1998|p=101}} when in 1968 he felt a "call to arms"{{sfn|Chute|2010|p=17}} to move to San Francisco, where the nascent {{not a typo|[[underground comix]]}} scene was blossoming amid [[Counterculture of the 1960s|the counterculture]] there.{{sfn|Chute|2010|p=17}}  That year Green introduced a religion-obsessed character in the strip "Confessions of a Mad School Boy", published in a periodical in Providence, Rhode Island, in 1968.  He named the character Binky Brown in "Binky Brown Makes up His Own Puberty Rites", published in the 17th issue of the underground comic book ''[[Yellow Dog (comics)|Yellow Dog]]'' in 1969.  "The Agony of Binky Brown" followed in the first issue of ''Laugh in the Dark'', published by Last Gasp in 1971.{{sfn|Bosky|2012|p=89}}

At the time, comic books had a reputation in the US as low-brow children's entertainment, and the public often associated them with juvenile delinquency.  Comics had little cultural capital and few American cartoonists had challenged the perception that the medium was inherently incapable of mature artistic expression.{{sfn|Gardner|2008|p=12}}

==Synopsis==

Green takes the persona of Binky Brown,{{sfn|Booker|2014|p=480}} who opens the story writing a confession of the neurosis that has tortured him since puberty.  In his childhood, he knocks over a statue of the Virgin Mary and feels intense guilt over this affront to his mother and to God.  Binky is raised a Catholic and undergoes the religious indoctrination of nuns{{sfn|Booker|2014|p=481}} at a strict Catholic parochial school that commonly employs corporal punishment.{{sfn|Green|2010|p=57}}  He forms an image of a vengeful God, which fills him with feelings of fear and guilt.{{sfn|Booker|2014|p=481}}

Binky's [[intrusive thought]]s bring him to believe his body is trying to lead him to sin and [[eternal punishment]].  He develops an internal system of rules to cope with these thoughts and punishes himself for violations.  He wards off thoughts and fantasies he cannot control and that give him guilt by silently repeating the word "noyatin" to himself, a contraction of the repentant "not a sin".{{sfn|Green|2010|p=57}}

As he approaches adolescence and becomes aware of his sexuality, he begins to see common objects as phalluses—phalluses that project unholy rays.  These objects include his fingers, his toes, and his own penis, and he obsessively tries to deflect their "pecker rays" from reaching holy items such as churches or statues of Mary.  Binky finds his anguish all-consuming as he imagines the destruction he cannot avoid, and spends hours praying to God for forgiveness.  As an adult, Binky confronts his faith and by smashing a set of statues of the Virgin Mary declares himself free of the Church and its influence on him.{{sfn|Booker|2014|p=481}}

==Composition and publication==

[[File:Ron Turner (San Francisco 2007).jpg|thumb|alt=Photo of a seated man with a long white beard|Green received financial support from [[Last Gasp]] publisher Ron Turner ''(photo from 2007)'']]

Green spent about a year working on the 44-page ''Binky Brown Meets the Holy Virgin Mary''.  He took a few months making cards of what he called "factual incidents or neurotic habits"{{sfn|Rosenkrantz|2011|p=1}} to incorporate.  During the seven months he drew the work Green received a monthly stipend of $150 from Ron Turner, the founder of underground {{not a typo|comix}} publisher [[Last Gasp|Last Gasp Eco-Funnies]].{{sfn|Rosenkrantz|2011|p=1}}  Last Gasp published the story as a one-shot comic book in 1972{{sfn|Booker|2014|p=480}}—Green's first solo title.{{sfn|Levin|1998|p=102}}  It went through two print runs of 55,000 copies each{{sfn|Green|2010|p=58}} with a "Youngsters Prohibited" label on the cover.{{sfn|Hatfield|2005|p=134}}

In 1990, Green had an essay published entitled "The Binky Brown Matter" in ''[[The Sun (magazine)|The Sun]]''.{{sfn|Levin|1998|p=103}} In the essay, he describes the OCD with which he was diagnosed years after completing ''Binky Brown''.{{sfn|Green|2010|p=58}}  Last Gasp reprinted the story in 1995 in ''The Binky Brown Sampler'', a softcover anthology of Binky Brown strips with an introduction by [[Art Spiegelman]]{{sfn|Booker|2014|p=481}} and an expanded version of "The Binky Brown Matter".{{sfn|Levin|1998|p=103}}

Green sold the original artwork to the strip in the 1970s; [[McSweeney's]] staff contacted the owner of the artwork, Christine Valenza, to make fresh scans for a standalone reprinting in 2009,{{sfn|Booker|2014|p=481}} overseen by McSweeney's editor Eli Horowitz.  It had a print run of 5,000 copies and reprints the artwork at the full size of the originals; the page reproductions mimic the actual pages, including marks, smudges, and corrections.{{sfn|Reid|2009}}  In 2011, the publisher Stara published a French translation by Harry Morgan titled ''Binky Brown rencontre la Vierge Marie'',{{sfnm|1a1=Bi|1y=2011|2a1=Norot|2y=2011}} and La Cúpula published a Spanish translation by Francisco Pérez Navarro titled ''Binky Brown conoce a la virgen María''.{{sfnm|1a1=Jiménez|1y=2011|2a1=Queipo|2y=2011}}

===Editions===

{| class="wikitable"
|-
|+ Editions of ''Binky Brown Meets the Holy Virgin Mary''
|-
! scope="col" | Year
! scope="col" | Title
! scope="col" | Publisher
! scope="col" | ISBN
! scope="col" | Format
|-
! scope="row" | 1972
| ''Binky Brown Meets the Holy Virgin Mary''
| [[Last Gasp|Last Gasp Eco Funnies]]
|
| Comic book
|-
! scope="row" | 1995
| ''The Binky Brown Sampler''
| [[Last Gasp]]
| {{ISBNT|978-0-86719-332-9}}
| Softcover collection
|-
! scope="row" | 2009
| ''Binky Brown Meets the Holy Virgin Mary''
| [[McSweeney's]]
| {{ISBNT|978-1-934-78155-5}}
| Deluxe hardcover
|}

==Style and analysis==

[[File:Justin Green (1972) Binky Brown Meets the Holy Virgin Mary splash page.jpg|thumb|alt=A comic-book panel of a naked man hanging over a sickle bound head to foot, drawing with a pen in his mouth|''Binky Brown'' makes visual its author's internal anguish.]]

The story takes the form of a guilt-ridden confession.{{sfn|Spiegelman|1995|pp=4–5}}  In the opening, the adult Binky hangs over a sickle, bound from head to toe and listening to ''[[Ave Maria (Bach/Gounod)|Ave Maria]]'' as he draws with a pen in his mouth.{{sfn|Gardner|2008|p=8}}  He declares his intention: "to purge myself of the compulsive neurosis which I have served since I officially left Catholicism on Halloween, 1958."{{sfn|Hatfield|2005|p=134}}  He justifies the work to communicate with the "many others {{interp|who}} are slaves to their neuroses" and who, despite believing themselves isolated, number so many that they "would entwine the globe many times over in a vast chain of common suffering".{{sfn|Gardner|2008|p=8}}

Though Green built ''Binky Brown'' on an autobiographical base he fabricated many scenes—such as one in which Binky is bullied by two third-graders—"to suggest or convey a whole generalized idea about some subjective feeling, such as order or fear or guilt".{{sfn|Rosenkrantz|2011|p=1}}  To critic Charles Hatfield ''Binky Brown'' displays a "radical subjectivity"{{sfn|Hatfield|2005|p=138}} that calls into question the notion of objectivity in autobiography.{{sfn|Hatfield|2005|p=138}}  The presentation is insistently subjective and non-literal in its visuals.{{sfn|Witek|2011|p=229}}

Despite the heavy tone, humor is prominent.{{sfnm|1a1=Spiegelman|1y=1995|1pp=4–5|2a1=Rosenkrantz|2y=2011|2p=1}}  The work is [[Metafiction|conscious of its own creation]]—Green's drawing of it frames the narrative proper and there are constant reminders of it throughout.{{sfn|Hatfield|2005|p=134}}  Green patterned the opening after those featuring the Crypt-Keeper in [[EC Comics]]' ''[[Tales from the Crypt (comics)|Tales from the Crypt]]'' series from the 1950s.  Green used the adult Binky as the narrator of the captions and as a way to tie together the past and present timeframes.{{sfn|Manning|2010}} There is a disconnect in that the narrator refers to his younger self as "he".{{sfn|Witek|2011|p=229}}  Other references to comics include a ''Sinstopper's Guidebook'', which alludes to [[Dick Tracy]]'s ''Crimestopper's Textbook''{{sfn|Hatfield|2005|p=138}} and a cartoon by Robert Crumb in the background.{{sfn|Hatfield|2005|p=134}}

[[File:Treasure Chest 184 cover.jpg|thumb|upright|left|alt=A comic book cover depicting two characters with halos behind their heads, title ''Treasure Chest''|Comics and Catholic images are scattered throughout ''Binky Brown'', such as the [[Parochialism|parochial]] ''[[Treasure Chest (comics)|Treasure Chest]]'' comic book.]]

Green employs numerous Catholic symbols, such as a word balloon adorned with symbols of Christ's martyrdom to represent the depth of Binky's desperation.  Catholic works such as a [[catechism]] and ''[[Treasure Chest (comics)|Treasure Chest]]'' [[Parochialism|parochial]] comics appear throughout the work.{{sfn|Hatfield|2005|p=134}}

Despite strict censorship in other media in the US, explicit sexuality was common in underground {{not a typo|comix}}.  ''Binky Brown'' was the first work of autobiographical comics to depict explicit sexuality: penises appear throughout, and Binky masturbates in one scene.{{sfn|Chute|2010|pp=18–19}}  The central symbol of the penis recurs sometimes subtly as in the images of pencils used to craft the work,{{sfn|Hatfield|2005|p=134}} and more often explicitly, as every phallic-like object Green sees because a literal "pecker ray"-projecting penis in Binky's mind.{{sfn|Rosenkrantz|2011|p=1}}

[[Art Spiegelman]] described the artwork as "quirky and ungainly".{{sfn|Spiegelman|1995|p=5}}  Though it appears awkward, Green put considerable effort into elements such as [[Perspective (graphical)|graphical perspective]], and draws attention to his craft by depicting himself drawing and by placing the drawing manuals ''Perspective'' and ''Fun With a Pencil'' in the backgrounds. In contrast to the mundane tales of [[Harvey Pekar]], another prominent early practitioner of autobiographical comics, Green makes wide use of visual metaphors.{{sfn|Hatfield|2005|p=134}}  In ''Binky Brown'' symbols become literal, as when Binky imagines himself becoming a snowball hurtling into Hell or as a fish chased by a police officer who wears a crucifix.{{sfn|Hatfield|2005|p=135}}  The work displays a wide array of visual techniques: diagrammatic arrows; mock-scholarly documentation; a great variety in panel size, composition, and layout; and a range of contrasting mechanical and organic rendering techniques, such as [[screentone]] alongside dense hand-drawn [[hatching]].{{sfn|Hatfield|2005|p=138}}  The symbolic and technical collide where the Virgin Mary becomes the [[vanishing point]] of Binky's converging "pecker rays".{{sfn|Hatfield|2005|p=134}}

[[File:Dream of the Rarebit Fiend 1905-02-25 panels 4 and 5 atacked.jpg|thumb|alt=Two panels of a comic strip of a man being buried alive|Precursors in comics to ''Binky Brown''{{'}}s unrestricted psyche include [[Winsor McCay]]'s ''[[Dream of the Rarebit Fiend]]'' (February 25, 1905).]]

Critic Joseph Witek sees the shifting between different modes of traditional comics representation at times presents a literalist view through "windowlike panels",{{sfn|Witek|2011|p=229}} and at others "representational, symbolic, allegorical, associative, and allusive", an approach analogous to "Binky Brown's massively and chaotically overdetermined subjectivity".{{sfn|Witek|2011|p=229}}  Witek finds roots for the fractured psychological landscape of ''Binky Brown'' in the comics of earlier eras: the unrestrained psyches in the dreams of [[Winsor McCay]]'s ''[[Dream of the Rarebit Fiend]]'', the irrational, shifting landscapes of [[George Herriman]]'s ''[[Krazy Kat]]'', and [[Superman]]'s obsessively contrarian nemesis [[Bizarro]].{{sfn|Witek|2011|pp=229–230}}

In ''Binky Brown'' Green blames the Catholic Church for his psychological troubles; years later, he was diagnosed with OCD, and came to see these episodes in that light rather than as the fault of the Church.{{sfn|Booker|2014|p=481}}  He nevertheless continued to blame the Church for contributing to his anxieties and maintained that religion has a magnifying influence on the condition.  He said the abandoning of both religion and recreational drugs made it easier to cope with his condition.{{sfn|Manning|2010}}  In 1990 a Catholic priest raised concerns that ''Binky Brown'' may be harmful to minors; Green countered that he believed it was the Church that was harming minors.{{sfn|Green|2010|p=57}}  Green has likened his OCD to a "split vision" which made him "both the slave to the compulsion and the detached observer".{{sfn|Gardner|2008|p=12}}

Literary scholar [[Hillary Chute]] sees the work as addressing feminist concerns of "embodiment and representation"{{sfn|Chute|2010|p=19}} as it "delves into and forcefully pictures non-normative sexuality".{{sfn|Chute|2010|p=19}}  Chute affirms that despite its brevity ''Binky Brown'' merits the label "[[graphic novel]]" as "the quality of work, its approach, parameters, and sensibility"{{sfn|Chute|2010|p=17}} mark a "seriousness of purpose".{{sfn|Chute|2010|p=17}}

==Reception and legacy==
Green recounted "a strong energy" that ''Binky Brown'' drew from his readership, the first significant response he got from his work.{{sfn|Gardner|2008|p=13}}  The story has had a wide influence on underground and [[alternative comics]],{{sfn|Booker|2014|p=480}} where its self-mocking{{sfn|Hatfield|2005|p=138}} and confessional approach has inspired numerous cartoonists to expose intimate and embarrassing details of their lives.{{sfnm|1a1=Hatfield|1y=2005|1p=138|2a1=Spiegelman|2y=1995|2p=4}}  Under the influence of ''Binky Brown'', in 1972 [[Aline Kominsky-Crumb|Aline Kominsky]] published her first strip, the autobiographical "Goldie: A Neurotic Woman"{{sfn|Chute|2010|p=34}} in ''[[Wimmen's Comix]]'' {{No.|1}}.{{sfn|Gardner|2008|p=13}}  Other contemporary underground cartoonists were soon to incorporate confessional autobiography into their work.{{sfn|Witek|2011|p=227}}  Robert Crumb followed the same year with "The Confessions of R. Crumb" and continued with numerous other such strips.{{sfn|Rosenkrantz|2011|p=1}}  Art Spiegelman, who had seen ''Binky Brown'' in mid-creation in 1971,{{sfn|Gardner|2008|p=17}} went as far as to state that "without ''Binky Brown'' there would be no ''[[Maus]]''"—Spiegelman's most prominent work.{{sfn|Spiegelman|1995|p=4}}  The same year as ''Binky Brown''{{`}}s publication, Green asked Spiegelman to contribute a three-page strip to the first issue of ''Funny <!-- This spelling is correct!!! -->{{Not a typo|Aminals}}<!-- Please do not "correct" it!!! -->'', which Green edited and was published by [[Apex Novelties]]. Spiegelman delivered the three-page "Maus" in which Nazi cats persecute Jewish mice, inspired by his father's experiences in the [[Auschwitz concentration camp]]; years later he revisited the theme in the graphic novel of the same name.{{sfn|Witek|1989|p=103}}  Comics critic Jared Gardner asserts that, while underground {{not a typo|comix}} was associated with countercultural iconoclasm, the movement's most enduring legacy was to be autobiography.{{sfn|Gardner|2008|pp=6–7}}

''Binky Brown'' went out of print for two decades after selling its initial print runs, during which time enthusiasts traded copies or photocopies.{{sfn|Spiegelman|1995|p=5}} Green made his living painting signs, and contributed occasional cartoon strips to various publications.{{sfn|Spiegelman|1995|p=5}} Green used the Binky Brown persona over the years in short strips and prose pieces that appeared in underground periodicals such as ''[[Arcade (comics magazine)|Arcade]]'' and ''[[Weirdo (comics)|Weirdo]]''.{{sfn|Levin|2005|p=88}}  "Sweet Void of Youth" in 1976 follows Binky from high school to age thirty-one, torn between cartooning and more respected forms of art.{{sfn|Levin|1998|p=102}} Aside from occasional one-off strips, his more regular cartooning appeared in the ongoing strips ''The Sign Game'', in ''[[Signs of the Times (magazine)|Signs of the Times]]'' magazine, and ''Musical Legends in America'', in ''[[Pulse! (magazine)|Pulse!]]''{{sfn|Levin|1998|p=104}}  Such later work has attracted far less attention than ''Binky Brown''.{{sfn|Levin|1998|p=107}}

[[File:Philip Roth - 1973.jpg|thumb|left|upright|alt=Photo of a middle-aged man|Green had read [[Philip Roth]] ''(pictured)'' and other writers who bared their personal lives in their work.]]
Though autobiographical elements had appeared earlier in the work of underground cartoonists such as Crumb, [[Spain Rodriguez|Spain]], and [[Kim Deitch]],{{sfn|Gardner|2008|pp=7–8}} ''Binky Brown Meets the Holy Virgin Mary'' has gained credit as the first important work of [[autobiographical comics]] in English.{{sfnm|1a1=Witek|1y=2011|1p=227|2a1=Dycus|2y=2011|2p=111}}  To Charles Hatfield ''Binky Brown'' is "the ur-example of confessional literature in comics";{{sfn|Hatfield|2005|p=131}} for [[Paul Gravett]] Green was "the first neurotic visionary to unburden his uncensored psychological troubles";{{sfn|Gravett|2005|p=22}} [[Douglas Wolk]] declared Green and his work "ahead of the memoirist curve";{{sfn|Wolk|2008|p=203}} Art Spiegelman declared: "What the [[Brontë family|Brontë sisters]] did for Gothic romance, what [[J. R. R. Tolkien|Tolkien]] did for sword-and-sorcery, Justin Green did for confessionary, autobiographical {{not a typo|comix}} {{sic}}";{{sfn|Gravett|2005|p=23}} and ''[[Publishers Weekly]]'' called the work the "[[Rosetta Stone]] of autobiographical comics".{{sfn|Publishers Weekly staff|2011}}  

''Binky Brown Meets the Holy Virgin Mary'' has appealed mostly to comics fans and cartoonists, and has gained little recognition from mainstream audiences and arts critics.  Spiegelman has speculated this neglect comes from the nature of the comics medium; in contrast to explicit works such as [[Philip Roth]]'s ''[[Portnoy's Complaint]]'', the penises in Green's work are visual.{{sfn|Spiegelman|1995|p=5}}

According to underground {{not a typo|comix}} historian Patrick Rosenkranz, Green represents a break with past convention by being "the first to openly render his personal demons and emotional conflicts within the confines of a comic".{{sfn|Rosenkrantz|2011|p=1}}  Green denied credit, calling confessional autobiography "a ''fait accompli'', a low fruit ripe for the plucking",{{sfn|Rosenkrantz|2011|p=1}} examples of which abounded in literary works he had read by [[James Joyce]], [[James&nbsp;T. Farrell]], and Philip Roth.  He has accepted credit for "anticipat{{interp|ing|original=ed}} the groundswell in literature about obsessive compulsive disorder by almost two decades",{{sfn|Rosenkrantz|2011|p=1}} for which he knew of no precedent.{{sfn|Rosenkrantz|2011|p=1}} Chute sees major themes of isolation and coping with OCD recurring in autobiographical works such as [[Howard Cruse]]'s ''[[Stuck Rubber Baby]]'' (1995) and [[Alison Bechdel]]'s ''[[Fun Home]]'' (2006).{{sfn|Green|2010|p=58}}  Hatfield sees echoes of Green's unrestrained approach to dealing with a mental condition in [[Madison Clell]]'s ''Cuckoo'' (2002)—about Clell's [[dissociative identity disorder]]—and in [[David Beauchard|David&nbsp;B.]]'s ''[[Epileptic (comics)|Epileptic]]'' (2003).{{sfn|Hatfield|2005|p=138}}

To cartoonist [[Jim Woodring]], Green's autobiographical work "has never been surpassed".{{sfn|Rosenkrantz|2011|p=3}}  Woodring's own autobiographical work in ''[[Jim (comics)|Jim]]'' draws from his dreams rather than his waking life.{{sfn|Rosenkrantz|2011|p=3}}  British-American cartoonist [[Gabrielle Bell]] sympathized with Brown's approach, which she described as "talking about his feelings or his emotional state when he was illustrating it with striking images that were sort of absurd or a weird juxtaposition".{{sfn|Rosenkrantz|2011|p=5}}  Green's influence extended overseas to cartoonists such as the Dutch [[Peter Pontiac]], who drew inspiration from ''Binky Brown'' and ''Maus'' to produce ''Kraut'' (2000), about his father who [[Collaboration with the Axis Powers during World War II|collaborated with the Nazis]] during World War&nbsp;II.{{sfn|Rosenkrantz|2011|p=3}} 

The story ranked No.&nbsp;9 on ''[[The Comics Journal]]''{{'}}s list of the best hundred English-language comics of the 20th century,{{sfn|Spurgeon|1999}} and featured as the cover artwork for the autobiographical comics issue  of the journal ''[[Biography (journal)|Biography]]'' (Vol.&nbsp;31, No.&nbsp;1).{{sfn|Witek|2011|p=227}}  Artwork to ''Binky Brown'' appeared in an exhibition of Green's work at Shake It Records in Cincinnati in 2009.{{sfn|Green|2010|p=58}}

<gallery caption="Autobiographical cartoonists inspired by ''Binky Brown Meets the Holy Virgin Mary''" mode="packed" heights="220px">

Robert Crumb 2010.jpg|alt=Photo of a bearded and bespectacled man opening a book|[[Robert Crumb]] took to putting himself on display shortly after reading ''Binky Brown''.
Art Spiegelman (2007).jpg|alt=Photo of a bespectacled man|[[Art Spiegelman]] stated there would have been no ''[[Maus]]'' without ''Binky Brown''.
JimWoodringDrawing.jpg|alt=Photo of a bearded and bespectacled man drawing|[[Jim Woodring]] stated Green's autobiographical work "has never been surpassed";{{sfn|Rosenkrantz|2011|p=3}} his own autobiographical work depicts his dreams.

</gallery>

==References==

{{Reflist|colwidth=20em}}

===Works cited===

====Books====

{{Refbegin|colwidth=40em}}

* {{cite book
|editor-last  = Booker
|editor-first = M. Keith
|title        = Comics through Time: A History of Icons, Idols, and Ideas
|year         = 2014
|publisher    = [[ABC-CLIO]]
|isbn         = 978-0-313-39751-6
|pages        = 480–482
|chapter      = Binky Brown Meets the Holy Virgin Mary
|ref          = harv}}
* {{cite book
|last          = Bosky
|first         = Bernadette
|chapter       = ''Binky Brown Sampler''
|editor1-last  = Beaty
|editor1-first = Bart
|editor2-last  = Weiner
|editor2-first = Stephen
|title         = Critical Survey of Graphic Novels: Independents and Underground Classics
|pages         = 89–93
|year          = 2012
|publisher     = [[Salem Press]]
|isbn          = 978-1-58765-950-8
|ref           = harv}}
* {{cite book
|last      = Chute
|first     = Hillary L.
|title     = Graphic Women: Life Narrative and Contemporary Comics
|year      = 2010
|publisher = [[Columbia University Press]]
|isbn      = 978-0-231-15063-7
|ref       = harv}}
* {{cite book
|last      = Dycus
|first     = D. J.
|title     = Chris Ware's Jimmy Corrigan: Honing the Hybridity of the Graphic Novel
|url       = https://books.google.com/books?id=By4sBwAAQBAJ
|year      = 2011
|publisher = Cambridge Scholars Publishing
|isbn      = 978-1-4438-3554-1
|ref       = harv}}
* {{cite book
|last       = Gravett
|first      = Paul
|authorlink = Paul Gravett
|title      = Graphic Novels: Stories to Change Your Life
|year       = 2005
|publisher  = Aurum
|isbn       = 978-1-84513-068-8
|ref        = harv}}
* {{cite book
|last         = Green
|first        = Diana
|chapter      = Binky Brown Meets the Holy Virgin Mary
|pages        = 56–58
|title        = Encyclopedia of Comic Books and Graphic Novels
|year         = 2010
|editor-last  = Booker
|editor-first = M. Keith
|publisher    = [[ABC-CLIO]]
|isbn         = 978-0-313-35747-3
|ref          = harv}}
* {{cite book
|last       = Hatfield
|first      = Charles
|authorlink = Charles Hatfield
|title      = Alternative Comics: An Emerging Literature
|year       = 2005
|publisher  = [[University Press of Mississippi]]
|isbn       = 978-1-60473-587-1
|ref        = harv}}
* {{cite book
|last         = Levin
|first        = Bob
|title        = Outlaws, Rebels, Freethinkers and Pirates
|year         = 2005
|publisher    = [[Fantagraphics Books]]
|isbn         = 978-1-56097-631-8
|ref          = harv}}
* {{cite book
|last        = Spiegelman
|first       = Art
|authorlink  = Art Spiegelman
|title       = Justin Green's Binky Brown Sampler
|year        = 1995
|publisher   = [[Last Gasp]]
|isbn        = 978-0-86719-332-9
|pages       = 4–6
|chapter     = Symptoms of Disorder/Signs of Genius: Introduction
|ref         = harv}}
* {{cite book
|last      = Witek
|first     = Joseph
|title     = Comic Books as History: The Narrative Art of Jack Jackson, Art Spiegelman, and Harvey Pekar
|publisher = [[University Press of Mississippi]]
|year      = 1989
|isbn      = 978-0-87805-406-0
|ref       = harv}}
* {{cite book
|last         = Witek
|first        = Joseph
|chapter      = Justin Green: Autobiography Meets the Comics
|title        = Graphic Subjects: Critical Essays on Autobiography and Graphic Novels
|year         = 2011
|publisher    = [[University of Wisconsin Press]]
|isbn         = 978-0-299-25103-1
|pages        = 227–230
|editor-last  = Chaney
|editor-first = Michael A.
|ref          = harv}}
* {{cite book
|last       = Wolk
|first      = Douglas
|authorlink = Douglas Wolk
|title      = Reading Comics: How Graphic Novels Work and What They Mean
|year       = 2008
|publisher  = [[Da Capo Press]]
|isbn       = 978-0-7867-2157-3
|ref        = harv}}

{{Refend}}

====Journals and magazines====

{{Refbegin|colwidth=40em}}

* {{cite journal
|last         = Gardner
|first        = Jared
|title        = Autography's Biography, 1972–2007
|journal      = [[Biography (journal)|Biography]]
|volume       = 31
|issue        = 1
|year         = 2008
|pages        = 1–26
|publisher    = [[University of Hawaii Press]]
|via          = [[Project MUSE]]
|url          = http://muse.jhu.edu/journals/biography/v031/31.1.gardner.pdf
|subscription = yes
|ref          = harv}}
* {{cite journal
|last      = Levin
|first     = Bob
|title     = Rice, Beans and Justin Greens
|date      = April 1998
|journal   = [[The Comics Journal]]
|issue     = 203
|pages      = 101–107
|publisher = [[Fantagraphics Books]]
|issn      = 0194-7869
|ref       = harv}}
* {{cite journal
|editor-last  = Spurgeon
|editor-first = Tom
|editor-link  = Tom Spurgeon
|title        = The Top 100 English-Language Comics of the Century
|journal      = [[The Comics Journal]]
|issue        = 210
|date         = February 1999
|issn         = 0194-7869
|pages        = 34–108
|publisher    = [[Fantagraphics Books]]
|ref          = harv}}

{{Refend}}

====Web====

{{Refbegin|colwidth=40em}}

* {{cite web
|last        = Bi
|first       = Jessie
|title       = Binky Brown rencontre la Vierge Marie de Justin Green
|language    = French
|date        = July 2011
|url         = http://www.du9.org/chronique/binky-brown-rencontre-la-vierge/
|accessdate  = 2015-06-28
|archiveurl  = https://web.archive.org/web/20140313164642/http://www.du9.org/chronique/binky-brown-rencontre-la-vierge/
|archivedate = 2014-03-13
|ref         = harv}}
* {{cite web
|last        = Jiménez
|first       = Jesús
|title       = 'Binky Brown conoce a la Virgen María', una obra maestra del cómic Underground
|language    = Spanish
|date        = 2011-06-17
|work        = [[RTVE]]
|url         = http://www.rtve.es/noticias/20110617/binky-brown-conoce-virgen-maria-obra-maestra-del-comic-underground/441217.shtml
|accessdate  = 2015-06-28
|archiveurl  = https://web.archive.org/web/20110625051507/http://www.rtve.es/noticias/20110617/binky-brown-conoce-virgen-maria-obra-maestra-del-comic-underground/441217.shtml
|archivedate = 2011-06-25
|ref         = harv}}
* {{cite web
|last        = Manning
|first       = Shaun
|title       = Justin Green on "Binky Brown"
|url         = http://www.comicbookresources.com/?page=article&id=24518
|publisher   = [[Comic Book Resources]]
|date        = 2010-01-22 
|accessdate  = 2011-04-18
|archiveurl  = https://web.archive.org/web/20150119062153/http://www.comicbookresources.com/?page=article&id=24518
|archivedate = 2015-01-19
|ref         = harv}}
* {{cite web
|last        = Norot
|first       = Anne-Claire
|title       = BD: "Binky Brown", un classique novateur et iconoclaste
|date        = 2011-07-25
|work        = [[Les Inrockuptibles]]
|language    = French
|url         = http://www.lesinrocks.com/2011/07/25/livres/bd-binky-brown-un-classique-novateur-et-iconoclaste-1112355/
|accessdate  = 2015-06-28
|archiveurl  = https://web.archive.org/web/20150628020253/http://www.lesinrocks.com/2011/07/25/livres/bd-binky-brown-un-classique-novateur-et-iconoclaste-1112355/
|archivedate = 2015-06-28
|ref         = harv}}
* {{cite journal
|author      = Publishers Weekly staff
|title       = Binky Brown Meets The Holy Virgin Mary 
|journal     = [[Publishers Weekly]]
|date        = December 2011
|url         = http://publishersweekly.com/978-1-934781-55-5
|accessdate  = 2015-06-29
|archiveurl  = https://web.archive.org/web/20150629111744/http://publishersweekly.com/978-1-934781-55-5
|archivedate = 2015-06-29
|ref         = harv}}
* {{cite web
|last        = Queipo
|first       = Alan
|title       = Justin Green: Binky Brown conoce a la Virgen María
|date        = 2011-07-12
|work        = Notodo
|url         = http://www.notodo.com/libros/2576_justin_green_binky_brown_conoce_a_la_virgen_mara.html
|accessdate  = 2015-06-28
|archiveurl  = https://web.archive.org/web/20150629055533/http://www.notodo.com/libros/2576_justin_green_binky_brown_conoce_a_la_virgen_mara.html
|archivedate = 2015-06-29
|ref         = harv}}
* {{cite web
|last        = Reid
|first       = Calvin
|title       = Sex, Lies and Religion: A New Edition of 'Binky Brown Meets the Holy Virgin Mary'
|work        = [[Publishers Weekly]] 
|date        = December 1, 2009
|url         = http://www.publishersweekly.com/pw/by-topic/booknews/comics/article/28212-sex-lies-and-religion-a-new-edition-of-binky-brown-meets-the-holy-virgin-mary.html
|accessdate  = 2015-01-19
|archiveurl  = https://web.archive.org/web/20150119031143/http://www.publishersweekly.com/pw/by-topic/booknews/comics/article/28212-sex-lies-and-religion-a-new-edition-of-binky-brown-meets-the-holy-virgin-mary.html
|archivedate = 2015-01-19
|ref         = harv}}
* {{cite web
|last        = Rosenkrantz
|first       = Patrick
|title       = The ABCs of Autobio Comix
|work        = [[The Comics Journal]]
|publisher   = [[Fantagraphics Books]]
|date        = 2011-03-06
|url         = http://www.tcj.com/the-abcs-of-auto-bio-comix-2/
|accessdate  = 2011-04-17
|archiveurl  = https://web.archive.org/web/20140305062916/http://www.tcj.com/the-abcs-of-auto-bio-comix-2
|archivedate = 2014-03-05
|ref         = harv}}

{{Refend}}

==Further reading==

{{Refbegin}}

* {{cite journal
|last      = Burbey
|first     = Mark
|date      = January 1986
|title     = Comics and Catholics: Mark Burbey Interviews Justin Green
|journal   = [[The Comics Journal]]
|issue     = 104
|pages     = 37–49
|publisher = [[Fantagraphics Books]]
|issn      = 0194-7869}}

{{Refend}}

{{Underground comix works}}
{{Portal bar|Christianity|Comics|Judaism|Psychology|Religion}}

<!--Categories-->
[[Category:1972 graphic novels]]
[[Category:1972 comics debuts]]
[[Category:Autobiographical comics]]
[[Category:Religious comics]]
[[Category:Comics critical of religion]]
[[Category:Last Gasp titles]]
[[Category:Obsessive–compulsive disorder in fiction]]
[[Category:Underground comix]]