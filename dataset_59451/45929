{{Infobox college soccer team
|name = 1999 [[Indiana Hoosiers men's soccer]]
|athletics_name = Indiana Hoosiers
|logo = Indiana Hoosiers Logo.svg 
|logo_size = 97px
|university = Indiana University Bloomington
|conference = Big Ten Conference
|conference_short = Big Ten
|founded = 1973
|division = 
|city = Bloomington
|stateabb = IN
|state = Indiana
|coach =  [[Jerry Yeagley]]
|tenure = 27th 
|stadium = [[Bill Armstrong Stadium]]
|capacity = 6,500
|nickname = [[Indiana Hoosiers]]
| pattern_la1= |pattern_b1= |pattern_ra1=
| leftarm1=990000 |body1=990000 |rightarm1=990000 |shorts1=990000 |socks1=990000
| pattern_la2= |pattern_b2= |pattern_ra2=
| leftarm2=FFFFFF |body2=FFFFFF |rightarm2=FFFFFF |shorts2=FFFFFF |socks2=FFFFFF
|NCAAchampion = 1999
|conference_tournament = 1999
|conference_season = 1999
}}
The 1999 [[Indiana Hoosiers men's soccer]] team represented Indiana University during the 1999 NCAA Division I soccer season, winning the [[Big Ten Conference]] regular season and tournament championships and the NCAA Championship. The team was coached by Jerry Yeagley and finished with a record of 21-3. The 1999 Hoosiers were led on offense by Aleksey Korol, Matt Fundenberger, and Yuri Lavrinenko. The defense was led by Nick Garcia, Dennis Fadeski, and T.J. Hannig. The Hoosiers played their home matches at [[Bill Armstrong Stadium]], in [[Bloomington, Indiana]].

The team's penultimate victory of the season came in the semifinals of the [[1999 NCAA Division I Men's Soccer Championship|1999 NCAA Soccer Tournament]]- also known as the [[NCAA Division I Men's Soccer Championship|College Cup]]. The Hoosiers defeated their rival, the [[UCLA Bruins]], by a score of 3-2 in 4 overtimes. The match is considered one of the most exciting in college soccer history. Two days later, Indiana defeated Santa Clara University, 1-0 to capture the national championship.

The national championship was the 2nd consecutive championship for the Hoosiers, as they won the 1998 NCAA tournament, and it was the program's 5th overall national championship. The Big Ten title that the Hoosiers captured in 1999 was the 6th consecutive conference championship for the program and was the 8th Big Ten Conference title for Indiana in the 9 years that the Big Ten had hosted men's soccer.

==Pre-Season==
The Hoosiers entered 1999 as the defending national champions and ranked #1 in the NSCAA poll. However, early on the team felt the loss of Dema Kovalenko, Lazo Alavanja, and Gino DiGuardi from the 1998 championship team and struggled to find their form. The Hoosiers struggled in the preseason exhibition schedule, losing 2-3 to St. Louis,<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/082199aaa.html
| title=1999 NCAA Soccer Preseason: IU vs SLU
| date=21 August 1999 | accessdate=29 August 2012}}</ref> squeaking past IUPUI 1-0,<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/082799aaa.html
| title=1999 NCAA Soccer Preseason: IU vs IUPUI
| date=27 August 1999 | accessdate=29 August 2012}}</ref> and then drawing with Clemson 1-1.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/082899aaa.html
| title=1999 NCAA Soccer Preseason: IU vs Clemson
| date=28 August 1999 | accessdate=29 August 2012}}</ref>

==Regular Season==
The early struggles continued into the early stages of the season, as the Hoosiers' quest to defending the crown that they'd won in 1998 began slowly. Aleksey Korol's goal lifted Indiana to a 1-0 season-opening win over fellow 1998 College Cup participant, Maryland, in front of 4,726 fans at Bill Armstrong Stadium.<ref>{{cite web |url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/090399aaa.html |title=1999 NCAA Soccer: IU vs Maryland
| date=3 September 1999 | accessdate=29 August 2012}}</ref> Indiana then dropped a 0-1 decision to St. Louis at home <ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/090599aaa.html
| title=1999 NCAA Soccer: IU vs SLU
| date=5 September 1999 | accessdate=29 August 2012}}</ref> and fell to Yale on the road, 1-2.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/091199aaa.html
| title=1999 NCAA Soccer: IU vs Yale
| date=11 September 1999 | accessdate=29 August 2012}}</ref>

The 1-2 record to start the season was a disappointment, but the Hoosiers responded and wouldn't lose again until the final regular season game of the year, stringing together 13 straight wins. The streak began with wins over Brown, Fresno State, TCU, Louisville and Michigan State.

Penn State then visited Bloomington as the #1 ranked team in the nation. And in a highly exciting match, the Hoosiers prevailed 4-2. Pat Noonan, Fundenberger, Korol and Lavrinenko each found the net in the match, and the matchup proved to be the first of three huge meetings between the squads in 1999.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/100199aaa.html |title=1999 NCAA Soccer: IU vs Penn State |date=1 October 1999 |accessdate=29 August 2012}}</ref> The win ran the Hoosiers record to 7-2-0.

Indiana's next 4 games against Butler, Evansville, Wisconsin and Northwestern were all shutout wins for the Hoosiers. IU then topped Kentucky and beat Ohio State to clinch the Regular Season Big Ten Championship with a 5-0 conference record.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/102499aaa.html
| title=1999 NCAA Soccer: IU vs Ohio State
| date=24 October 1999 | accessdate=29 August 2012}}</ref> Indiana defeated Florida International for the Hoosiers' 13th straight win, and then dropped the final game of the regular season to James Madison. IU closed the regular season with a record of 14-3-0.

==Big Ten Tournament==
A first round bye awaited the team in the Big Ten tournament in East Lansing, Michigan. In the semifinals, Indiana defeated Northwestern, 2-0 <ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/111399aaa.html
| title=1999 NCAA Soccer Big Ten Tournament: IU vs Northwestern
| date=13 November 1999 | accessdate=29 August 2012}}</ref> to set up a matchup with familiar foe Penn State, for the Big Ten championship. In the final, the Nittany Lions gained a 1-goal advantage, but the Hoosiers battled back to force overtime on a goal by Korol. In the 2nd overtime period, Korol netted the golden-goal that won the Big Ten Championship for the Hoosiers.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/111499aaa.html
| title=1999 NCAA Soccer Big Ten Tournament: IU vs Penn State
| date=14 November 1999 | accessdate=29 August 2012}}</ref> It was Indiana's 6th straight Big Ten Championship and the win extended the Hoosiers' undefeated streak against Big Ten opponents- that began in October, 1995- to 32 games (30-0-2).

==NCAA Tournament: Rounds 1-3==
The opening round match would pit the #2 seeded Hoosiers against Kentucky at Bill Armstrong Stadium, and the match was tense throughout. Despite controlling the play, Indiana could not score in regulation or the 1st overtime period. But for the second straight game, Korol was the hero in the 2nd overtime and his goal gave Indiana a 1-0 victory and a trip to the 2nd round.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/112199aaa.html
| title=1999 NCAA Soccer Tournament Round 1 Recap
| date=21 November 1999 | accessdate=29 August 2012}}</ref>

In the Round of 16, the Hoosiers shutout Washington, 2-0 at home with goals from Korol and Mack.<ref>{{cite web 
| url=http://www.soccertimes.com/ncaa/1999/nov28.htm 
| title=1999 NCAA Soccer Tournament Round 2 Recap
| date=28 November 1999 | accessdate=29 August 2012}}</ref> With a berth to the College Cup- soccer's version of the Final Four- on the line, the Hoosiers once again hosted Penn State. It was the third meeting between the teams in 1999, but it proved to be the most comfortable margin of victory for Indiana, as the Hoosiers prevailed 3-0. Pat Noonan opened the scoring, and Korol once again was unstoppable and scored 2 goals. The match was the 5th straight in which Korol had scored and the 8th time in 9 games. And the win propelled Indiana to its 13th trip to the College Cup in the program's history.<ref>{{cite web 
| url=http://www.soccertimes.com/ncaa/1999/dec05.htm 
| title=1999 NCAA Soccer Tournament Round 3 Recap
| date=5 December 1999 | accessdate=29 August 2012}}</ref>

==College Cup==
The Hoosiers were joined in the 1999 men's soccer College Cup by their long-time rival and semifinal opponent, UCLA, as well as Connecticut and Santa Clara. The UCLA Bruins had stunned Indiana in exactly the same stage of the 1997 College Cup in overtime, when the Hoosiers were 23–0 and bidding for an undefeated season. And just as 1997 provided a classic game, the 1999 matchup was even more dramatic.{{According to whom|date=September 2016}} Two second-half goals by Noonan and Lavrinenko staked the Hoosiers to a seemingly comfortable 2-0 lead, but the Bruins came storming back to tie the match. The Hoosiers surrendered their first goal of the NCAA tournament with 14 minutes left in the match, which cut their lead in half. The Bruins then knotted the match at 2–2 with 8 minutes remaining. The teams battled through the first three overtime periods, with UCLA having the better of the chances. The Bruins nearly ended the match in the third overtime when a header bounced off the post and out of the Hoosiers' goal. In the 142nd minute, and just 8 minutes from a penalty shootout, the Hoosiers finally broke through on a goal by Ryan Mack. Mack's left footed blast from the top of the 18-yard box ended the game and sent the Hoosiers to their 10th NCAA Championship match.<ref>{{cite web 
| url=http://www.soccertimes.com/ncaa/1999/dec10.htm 
| title=1999 NCAA Soccer College Cup Semifinal Recap
| date=10 December 1999 | accessdate=29 August 2012}}</ref>

A fatigued{{citation needed|date=September 2016}} Indiana squad faced off against Santa Clara, who also needed four overtimes in their semifinal match to defeat Connecticut, for the 1999 NCAA Championship. The early play between the two exhausted teams was slow and sloppy,{{According to whom|date=September 2016}} but the Hoosiers began to gain control of the match midway through the first half and tallied the first goal in the 30th minute off of a quick counter-attack. Noonan stripped a Santa Clara attacker in Indiana's 18-yard box, and he then passed to Ryan Mack. Mack carried the ball into open space before passing off to Lavrinenko at the midfield stripe. Lavrinenko dribbled through the Santa Clara midfield before passing the ball to Korol at the top of the Santa Clara box. With his back to the goal, Korol held the ball and drew several defenders. As Lavrinenko made a run off of the ball, Korol laid the ball off to the left side to the streaking Lavrinenko. Lavrinenko found himself in on goal from a bit of an angle, but was able to slide the ball underneath the onrushing Santa Clara goalkeeper for a 1-0 lead. Santa Clara nearly equalized in the 39th minute, but Hoosiers goalkeeper TJ Hannig charged out of his goal and stopped a blast from 8 yards out. The save, however, deflected high into the air and was heading towards the open net, before defender John Swann leapt up and headed the ball over the crossbar. The second half saw Santa Clara begin to seize control of the play and put more pressure on the Indiana goal.

In the 70th minute, Indiana defender Nick Garcia made a play that will go down in College Cup lore.{{According to whom|date=September 2016}} After a long throw-in by Santa Clara deep in Indiana's territory was flick-headed in the box, the ball fell perfectly for a Santa Clara attacker, who side-volleyed the ball towards goal. Hannig dove to save, but the ball got past him and was headed for the open net. Seemingly out of nowhere, Garcia dove back into the goalmouth and got enough of his head on the ball to deflect the shot up and off of the crossbar to keep it out of the net. The close calls did not end there for the Hoosiers, as a Santa Clara attacker found himself wide open about 10 yards in front of the Indiana goal following a corner kick in the 73rd minute. But luckily for Indiana, the attacker sent his attempt wide of the net. From that moment on, Indiana clamped down defensively and closed out the 1-0 win for the 1999 National Championship. It was the fifth title in the program's history (along with 1982, '83, '88, '98) and the second time the Hoosiers had gone back-to-back.<ref>{{cite web 
| url=http://www.soccertimes.com/ncaa/1999/dec12.htm 
| title=1999 NCAA Soccer College Cup Finals Recap
| date=12 December 1999 | accessdate=29 August 2012}}</ref><ref>{{cite news 
| url=http://articles.latimes.com/1999/dec/13/sports/sp-43564 
| title=Indiana Takes NCAA Title
| date=13 December 1999 | accessdate=29 August 2012
 | work=Los Angeles Times
 | first=Scott
 | last=Moe}}</ref>

==Roster==
{{Football squad start}}
{{Football squad player|no=00|pos=GK|nat=USA|name=[[Doug Warren]]}}
{{Football squad player|no=1|pos=GK|nat=USA|name=TJ Hannig}}
{{Football squad player|no=3|pos=DF|nat=USA|name=[[Nick Garcia]]}}
{{Football squad player|no=4|pos=DF|nat=USA|name=John Swann}}
{{Football squad player|no=5|pos=MF|nat=UKR|name=[[Yuri Lavrinenko]]}}
{{Football squad player|no=6|pos=DF|nat=USA|name=Ryan Hammer}}
{{Football squad player|no=7|pos=MF|nat=USA|name=Phil Presser}}
{{Football squad player|no=9|pos=DF|nat=USA|name=[[Dennis Fadeski]]}}
{{Football squad player|no=10|pos=MF|nat=USA|name=[[Ryan Mack]]}}
{{Football squad player|no=11|pos=MF|nat=USA|name=[[Pat Noonan]]}}
{{Football squad player|no=13|pos=FW|nat=UKR|name=[[Aleksey Korol]]}}
{{Football squad player|no=14|pos=MF|nat=USA|name=Justin Tauber}}
{{Football squad player|no=15|pos=MF|nat=USA|name=Marcus Chorvat}}
{{Football squad mid}}
{{Football squad player|no=16|pos=FW|nat=USA|name=Michael Bock}}
{{Football squad player|no=17|pos=MF|nat=USA|name=Tyler Hawley}}
{{Football squad player|no=18|pos=FW|nat=USA|name=Matt Fundenberger}}
{{Football squad player|no=19|pos=DF|nat=USA|name=Brandon Tauber}}
{{Football squad player|no=21|pos=MF|nat=USA|name=BJ Snow}}
{{Football squad player|no=23|pos=GK|nat=USA|name=Colin Rogers}}
{{Football squad player|no=33|pos=GK|nat=USA|name=[[Matt Reiswerg]]}}
{{Football squad player|no=|pos=MF|nat=USA|name=Nick Donaldson}}
{{Football squad player|no=|pos=DF|nat=USA|name=Nick McDevitt}}
{{Football squad player|no=|pos=MF|nat=USA|name=David Prall}}
{{Football squad player|no=|pos=MF|nat=USA|name=Drew Shinabarger}}
{{Football squad player|no=|pos=MF|nat=USA|name=Justen Spirk}}
{{Football squad player|no=|pos=MF|nat=USA|name=Cal Thomas}}
{{Football squad end}}

Head Coach: [[Jerry Yeagley]]

Assistant Coach: [[Mike Freitag]]

Assistant Coach: [[John Trask (soccer)|John Trask]]

Assistant Coach: Ernie Yarborough

==Stats==
Aleksey Korol finished the 1999 soccer season with 20 goals and 10 assists for a total of 50 points. He became the first Hoosiers soccer player to tally 50 points in a single season in a decade. His 1999 season put him at 4th on the all-time scoring list for Indiana Soccer, with 149 points. Matt Fundenberger was second on the team in 1999 with 30 points on 10 goals and 10 assists. Yuri Lavrinenko led the squad with 12 assists and added 7 goals, for a total of 26 points. His 12 assists pushed Lavrinenko to 2nd on Indiana's all time assist leaders, with 41 career assists. Ryan Mack added 19 points (5 goals, 9 assists), and Pat Noonan scored 18 points (7 goals, 4 assists) on the season.

On defense, Dennis Fadeski started all 24 matches at marking back, while Nick Garcia started each match at sweeper. The duo anchored the defense to 15 shutouts. Justin Tauber took over the defensive midfielder position for the 1999 season and started 22 matches.

In goal, TJ Hannig and Doug Warren split games throughout the regular season, before Hannig won the job permanently for the entire postseason. Hannig started 16 matches and posted a 14-2 record, with a 0.52 goals against average and 10.5 shutouts. Warren started 8 matches, posting a 7-1 record, with a 0.63 record and 4 shutouts.

==Awards and Honors==
Aleksey Korol was named the Big Ten Conference Player of the Year in 1999, as well as a [[List of NCAA Division I men's soccer First-Team All-America teams|NSCAA 1st Team All American]], and the 1999 [[Soccer America Player of the Year Award|''SoccerAmerica'' National Player of the Year]]. He was also named to the Soccer America All-Decade Team for the 1990s. Korol was a finalist for the 1999 Hermann Trophy- given to the nation's top player, and finished 4th in the voting.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/121199aaa.html 
| title=Korol Named to All-Decade Team
| date=11 December 1999 | accessdate=29 August 2012}}</ref>

Nick Garcia finished 2nd in the Hermann Trophy voting and was named the 1999 College Cup Most Outstanding Defensive Player. He was also named as a [[List of NCAA Division I men's soccer First-Team All-America teams|NSCAA 1st Team All American]].<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/121199aaa.html
| title=Garcia 2nd in Hermann Trophy Voting
| date=11 December 1999 | accessdate=29 August 2012}}</ref>

Yuri Lavrinenko was named the 1999 College Cup Most Outstanding Offensive Player and was a 2nd Team All American.

Head Coach Jerry Yeagley was named Big Ten Coach of the Year for the 5th time, and also won his 5th NSCAA National Coach of the Year award.<ref>{{cite web 
| url=http://www.iuhoosiers.com/sports/m-soccer/spec-rel/011800aaa.html 
| title=Yeagley Named Coach of the Year
| date= 18 January 2000 | accessdate=29 August 2012}}</ref>

==Hoosiers in the Pros==
The 1999 Hoosiers saw 3 players drafted in the [[2000 MLS SuperDraft]]. Garcia was selected 2nd overall by the [[Kansas City Wizards]] and Korol was drafted 5th overall by the [[Dallas Burn]]. The [[Chicago Fire Soccer Club|Chicago Fire]] selected Lavrinenko in the 3rd round, with the 32nd pick overall.<ref>{{cite web |url=http://www.mlssoccer.com/superdraft/2000 |title=2000 MLS Superdraft |date=13 January 2000 |accessdate=29 August 2012}}</ref>

In addition, nine other players from the 1999 National Champions would later go on to play professional soccer either in Major League Soccer or USL A-League (now known as USL First Division): Doug Warren, TJ Hannig, John Swann, Phil Presser, Dennis Fadeski, Ryan Mack, Pat Noonan, Matt Fundenberger, Matt Reiswerg.

==References==
{{reflist}}

*Indiana University Men's Soccer Media Guide
*Indiana Daily Student Newspaper
*Bloomington Herald Times Newspaper
*Indiana University Athletics Website- www.iuhoosiers.com

{{Indiana Hoosiers men's soccer navbox}}
{{NCAA Division I Men's Soccer Champion navbox}}

[[Category:Indiana Hoosiers men's soccer seasons|1999]]
[[Category:1999 Big Ten Conference men's soccer season|Indiana Hoosiers]] 
[[Category:American soccer clubs 1999 season|Indiana Hoosiers, Men]]
[[Category:1999 in Indiana|Indiana Hoosiers men's soccer]]
[[Category:NCAA Men's Division I Soccer Championship team seasons]]
[[Category:NCAA Men's Division I College Cup seasons]]