<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Sunny
 | image=Sunny Myx.jpg
 | caption=Sunny Sport
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[Switzerland]]
 | manufacturer=[[Dewald Leichtflugzeugbau Gmbh]]<br />[[Tandem Aircraft KG]]<br />[[Airkraft Gmbh Leichtflugzeugbau]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=250
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]21,000 (assembled, 2011)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Airkraft Sunny''' is a [[Switzerland|Swiss]] [[ultralight aircraft]] that was originally produced by the [[Dewald Leichtflugzeugbau Gmbh]] of [[Bad Schönborn]], [[Germany]], then [[Tandem Aircraft KG]] of [[Saulgau]], Germany and more recently by [[Airkraft Gmbh Leichtflugzeugbau]] of [[Beringen, Switzerland]]. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 23. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 135. Pagefast Ltd, Lancaster UK, 2003. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 23. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 266. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

==Design and development==
The aircraft complies with the [[Fédération Aéronautique Internationale]] microlight category rules. It features an unusual diamond-shaped [[biplane]], [[strut-braced]] [[closed wing]] layout, a two-seats-in-[[tandem]] enclosed or open cockpit, fixed [[tricycle landing gear]] and a single engine in [[pusher configuration]]. The upper wing is swept back, while the lower wing is straight, but mounted further aft. The two wings are joined by swept tip [[rudder]]s. The [[elevon]]s are mounted to the lower wing only.<ref name="WDLA11" /><ref name="WDLA15"/>

The aircraft is made from bolted-together [[aluminum]] tubing, with its flying surfaces covered in [[Dacron]] sailcloth. Standard engines available include many models of [[Hirth]], [[Rotax]], [[BMW]] and [[Verner 133M]] powerplants, ranging from {{convert|65|to|80|hp|kW|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA15"/>
<!-- ==Operational history== -->

==Variants==
;Sunny Light
:Version with open cockpit<ref name="WDLA11" /><ref name="WDLA15"/>
;Sunny Sport
:Version with enclosed or semi-enclosed cockpit<ref name="WDLA11" /><ref name="WDLA15"/><ref name="WDLA04" />
;Sunny Amphibian
:Version with amphibious floats<ref name="WDLA11" /><ref name="WDLA15"/>
<!-- ==Aircraft on display== -->

==Specifications (Sunny Light) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=7
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=17
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=220
|empty weight lb=
|empty weight note=
|gross weight kg=450
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|44|l}} or optionally {{convert|80|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type=twin cylinder, liquid-cooled, [[two stroke]]
|eng1 kw=48<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=145
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=100
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=55
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=4
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{commons category|Airkraft Sunny}}
*{{Official website|http://www.airkraft-gmbh.com/}}
*[https://web.archive.org/web/20100615004247/http://www.airkraft-gmbh.com/ Official website archive] on [[Archive.org]]

[[Category:Swiss ultralight aircraft 1980–1989]]
[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Closed wing aircraft]]