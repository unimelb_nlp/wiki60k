{{Infobox military person
|name= Ralph Francis Stearley
|birth_date= {{birth date|1898|7|25}}
|death_date= {{death date and age|1973|2|3|1898|7|25}}
|birth_place= [[Brazil, Indiana]], U.S.
|death_place= Brazil, Indiana, U.S.
|placeofburial=
|placeofburial_label= Place of burial
|image= Ralph Francis Stearley.JPG
|caption= General Ralph F. Stearley
|nickname=
|allegiance= {{flagicon|United States}} [[United States|United States of America]]
|serviceyears= 1918–1954
|rank= [[File:US-O8 insignia.svg|25px]] [[Major general (United States)|Major general]]
|branch= [[United States Air Force]]<br>[[United States Army]]
|commands= [[I Tactical Air Division]]<br>[[IX Tactical Air Command]]<br>[[Fourteenth Air Force]]<br>[[Twentieth Air Force]]
|unit=
|battles= [[World War I]]<br>[[World War II]]<br>[[Korean War]]
|awards= [[Air Force Distinguished Service Medal|Distinguished Service Medal]]<br>[[Legion of Merit]]<br>[[Air Medal]]<br>[[Bronze Star Medal]]<br>[[Commendation Medal#Army|Commendation Medal]]
|relations=
|laterwork=
}}
'''Ralph Francis Stearley''', [[CBE]], (July 25, 1898 - February 3, 1973) was a [[United States Army]] and [[United States Air Force|Air Force]] officer. He is best known as a general in the [[United States Army Air Forces]] during [[World War II]]. Stearly was born at [[Brazil, Indiana]], in 1898. He graduated from the [[United States Military Academy]] at [[West Point, New York]], and was commissioned a second lieutenant in the cavalry on November 1, 1918. He transferred to the Air Service in 1925 and commanded the IX Tactical Air Command during World War II, supporting the [[United States First Army]] during combat operations in Europe.

==Education==
After graduation he remained at the academy until the following July, when he made a tour of battlefields in Europe. Upon his return to the United States in October 1919, he entered the Cavalry School at [[Fort Riley]], [[Kansas]], from which he graduated the following July. He then joined the [[4th Cavalry Regiment (United States)|4th Cavalry]] at [[Fort Brown]], [[Texas]].

He entered [[Yale University]] in [[New Haven, Connecticut]], in August 1921 and after completing the communications engineering course in June 1922 was assigned to the 4th Cavalry at [[Fort McIntosh (Texas)|Fort McIntosh]], Texas. In March 1923 he joined the 1st Signal Troop of the [[1st Cavalry Regiment (United States)|1st Cavalry]] at [[Fort Bliss]], Texas.

==Early career==
In March 1925 he was transferred to the [[United States Army Air Service|Air Service]] and went to [[Brooks City-Base|Brooks Field]], Texas, for training at the Primary Flying School. In September of that year he was transferred to [[Kelly Air Force Base|Kelly Field]], Texas, where he attended the Advanced Flying School. Upon graduation in March 1926, earning his [[U.S. Air Force Aeronautical Ratings|rating of Airplane Pilot]], he remained at Kelly Field for duty with the [[3d Operations Group|3rd Attack Group.]]

He was ordered to the [[Philippines]] in July 1928 for duty with the 4th Composite Group at [[Nichols Field]]. Upon returning to the United States in July 1930, he was again assigned to the 3rd Attack Group, now at [[Fort Crockett]], Texas, where he commanded the [[13th Bombardment Squadron|13th Attack Squadron]]. In February 1934 he went to [[Chicago]], [[Illinois]], for duty with the [[Air Mail Scandal|Army Air Corps Mail Operations]], and in August of that year he entered the [[Air Corps Tactical School]] at [[Maxwell Air Force Base|Maxwell Field]], [[Alabama]]. In September 1935 he entered the [[Command and General Staff College|Command and General Staff School]] at [[Fort Leavenworth]], Kansas. He graduated the following June, at which time he returned to Maxwell Field for a two-year tour as an instructor at the Air Corps Tactical School. In July and August 1938 he attended the Chemical Warfare School at [[Aberdeen Proving Ground|Edgewood Arsenal]], [[Maryland]], after which he returned to Maxwell Field.

In May 1940 he became assistant executive officer in the Training and Operations Division of the Office of the Chief of Air Corps in Washington, which later became the Flying Training Command.

==World War II==
[[File:American World War II senior military officials, 1945.JPEG|thumb|Stearley (standing, far left) with [[SHAEF]] and [[12th Army Group]] commanders in April 1945.]]
During April 1942 he served on the Canadian-American Military Board and in June of that year was appointed chief of the Air Group of the Military Intelligence Service of the [[United States Department of War|War Department General Staff]] in [[Washington, D.C.]]. He became director of Air Support at Army Air Forces headquarters in January 1943 and the following May assumed command of the [[III Reconnaissance Command|I Air Support Command]] at [[Morris Field]], N.C., which was soon redesignated the I Tactical Air Division.

In April 1944 he joined the [[Ninth Air Force]] in the [[European Theater of Operations|European theater]] as A-3 (chief of operations). The following August he became assistant chief of staff for G-3 of the newly organized [[First Allied Airborne Army]], and in April 1945 was appointed commanding general of the [[IX Tactical Air Command]] of the Ninth Air Force, which operated in [[France]] and [[Northern Germany]]. The following September he became commander of the Air Section, [[Fifteenth United States Army|Fifteenth Army]] Theater General Board, in the European theater.

==Post-war==
He returned to Air Force headquarters in January 1946 for duty as deputy chief of the Legislative and Liaison Division of the War Department General Staff. Two years later, he was appointed director of the Legislative and Liaison Division of the Directorate of Public Relations in the Office of the [[United States Secretary of the Air Force|Secretary of the Air Force]].

He was named commanding general of the [[Fourteenth Air Force]] at Orlando Air Force Base, Fla., in July 1948 and retained that position when the 14th Air Force was moved to [[Robins Air Force Base]], [[Georgia (U.S. state)|Georgia]], in October 1949.

In July 1950 he was appointed commanding general of the [[Twentieth Air Force]] at [[Kadena Air Base]], [[Okinawa]].

==Awards and commendations==
{|
|{{ribbon devices|number=0|type=oak|ribbon=Distinguished Service Medal ribbon.svg|width=60}}
|[[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Legion of Merit ribbon.svg|width=60}}
|[[Legion of Merit]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Bronze Star ribbon.svg|width=60}}
|[[Bronze Star Medal]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Air Medal ribbon.svg|width=60}}
|[[Air Medal]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Army Commendation Medal ribbon.svg|width=60}}
|[[Commendation Medal]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Order BritEmp rib.png|width=60}}
|[[Order of the British Empire]], Degree of Commander (CBE) Honorary
|-
|{{ribbon devices|number=0|type=oak|ribbon=Legion Honneur Chevalier ribbon.svg|width=60}}
|[[Chevalier de la Légion d'honneur]] (France)
|-
|{{ribbon devices|number=0|type=oak|ribbon=Croix de guerre 1939-1945 with palm (France) - ribbon bar.png|width=60}}
|[[Croix de guerre|Croix de Guerre avec Palme]] (France)
|-
|{{ribbon devices|number=0|type=oak|ribbon=Chevalier Ordre de Leopold.png|width=60}}
|[[Order of Leopold (Belgium)]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=BEL Croix de Guerre 1944 ribbon.svg|width=60}}
|[[War Cross (Belgium)|Croix de Guerre avec Palme]] (Belgium)
|-
|{{ribbon devices|number=0|type=oak|ribbon=NLD Order of Orange-Nassau - Commander BAR.png|width=60}}
|[[Orde van Oranje-Nassau]], Degree of Commander (The Netherlands)
|}
He was rated a command pilot, combat observer and aircraft observer.

==See also==
{{Portal|United States Army|World War I|World War II}}

==References==
*{{webarchive |date=2012-12-13 |url=http://archive.is/20121213052106/http://www.af.mil/information/bios/bio.asp?bioID=7238 |title=USAF Biography "Major General Ralph Francis Stearley"}}
*[http://www.afhra.af.mil/shared/media/document/AFD-090601-137.pdf Air Force Historical Study No. 91 Fogerty, Robert P.(1953). ''Biographical Data on Air Force General Officers, 1917-1952'' Volume 2 - L thru Z: Stearley, Ralph F.]

{{DEFAULTSORT:Stearley, Ralph Francis}}
[[Category:1898 births]]
[[Category:1973 deaths]]
[[Category:American aviators]]
[[Category:American military personnel of World War I]]
[[Category:American military personnel of World War II]]
[[Category:American military personnel of the Korean War]]
[[Category:Aviators from Indiana]]
[[Category:Air Corps Tactical School alumni]]
[[Category:United States Army Command and General Staff College alumni]]
[[Category:United States Army Air Forces generals]]
[[Category:United States Air Force generals]]
[[Category:United States Military Academy alumni]]
<!-- Honours -->
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Recipients of the Croix de guerre 1939–1945 (France)]]
[[Category:Recipients of the Air Medal]]
[[Category:Recipients of the Croix de guerre (Belgium)]]
[[Category:Commanders of the Order of Orange-Nassau]]
[[Category:People from Brazil, Indiana]]