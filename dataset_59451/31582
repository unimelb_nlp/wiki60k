{{featured article}}
{{Use dmy dates|date=December 2014}}

{{Taxobox
| name = Hoopoe starling
| image = Hoopoe starling.jpg
| image_width = 250px
| image_caption = Only known life drawing, showing the natural position of the crest, by Paul Jossigny, early 1770s
| status = EX
| status_system = IUCN3.1
| status_ref = <ref>{{IUCN|id=22710840 |title=''Fregilupus varius'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2012.1 |year=2012 |accessdate=16 July 2012}}</ref>
| extinct = 1850s
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Aves]]
| ordo = [[Passeriformes]]
| familia = [[Sturnidae]]
| genus = '''''Fregilupus'''''
| genus_authority = [[René-Primevère Lesson|Lesson]], 1831
| species = '''''F. varius'''''
| binomial = ''Fregilupus varius''
| binomial_authority = ([[Pieter Boddaert|Boddaert]], 1783)
|range_map= LocationReunion.svg
|range_map_width=250px
|range_map_caption=Location of [[Réunion]] (circled)
| synonyms = {{collapsible list|bullets = true|title=<small>List</small>
|''Upupa varia'' <small>Boddaert, 1783</small>
|''Upupa capensis'' <small>[[Johann Friedrich Gmelin|Gmelin]], 1788</small>
|''Upupa madagascariensis'' <small>Shaw, 1811</small>
|''Coracia cristata'' <small>Vieillot, 1817</small>
|''Pastor upupa'' <small>Wagler, 1827</small>
|''Fregilupus capensis'' <small>Lesson, 1831</small>
|''Coracia tinouch'' <small>Hartlaub, 1861</small>
|''Fregilupus borbonicus'' <small>Vinson, 1868</small>
|''Fregilupus varia'' <small>Gray, 1870</small>
|''Sturnus capensis'' <small>Schlegel, 1872</small>
|''Lophopsarus varius'' <small>Sundeval, 1872</small>
|''Coracias tivouch'' <small>Murie, 1874</small>
}}
}}

The '''hoopoe starling''', also known as the '''Réunion starling''' or '''Bourbon crested starling''' (''Fregilupus varius''), is a species of [[starling]] which lived on the [[Mascarene island]] of [[Réunion]], and became [[extinct]] in the 1850s. Its closest relatives were the [[Rodrigues starling]] and the [[Mauritius starling]] from nearby islands, and the three apparently originated in Southeast Asia. The bird was first mentioned during the 17th century and was long thought to be related to the [[hoopoe]], from which its name is derived. Although a number of affinities have been proposed, it was confirmed as a starling in a [[DNA]] study.

The hoopoe starling was {{convert|30|cm|in|abbr=on}} in length. Its [[plumage]] was primarily white and grey, with its back, wings and tail a darker brown and grey. It had a light, mobile [[Crest (feathers)|crest]], which curled forwards. The bird is thought to have been [[Sexual dimorphism|sexually dimorphic]], with males larger and having more curved beaks. The juveniles were more brown than the adults. Little is known about hoopoe starling behaviour. Reportedly living in large flocks, it inhabited humid areas and marshes. The hoopoe starling was [[omnivorous]], feeding on plant matter and insects. Its pelvis was robust, its feet and claws large, and its jaws strong, indicating that it foraged near the ground.

The birds were hunted by settlers on Réunion, who also kept them as [[cagebirds]]. Nineteen specimens exist in museums around the world. The hoopoe starling was reported to be in decline by the early 19th century, and was probably extinct before the 1860s. A number of factors have been proposed, including competition and predation by introduced species, disease, deforestation and persecution by humans, who hunted it for food and as an alleged crop pest.

==Taxonomy==
[[Image:Hoopoe Starling.jpg|upright|left|thumb|alt=Painting of grey-and-white bird with tufted head and curved beak|[[Type illustration]] by [[François-Nicolas Martinet]], 1770s]]
The first account thought to mention the hoopoe starling is a 1658 list of birds of [[Madagascar]] written by French governor [[Étienne de Flacourt]]. Although he mentioned a black-and-grey "tivouch" or [[hoopoe]], later authors have wondered whether this referred to the hoopoe starling or the Madagascan subspecies of hoopoe (''Upupa epops marginata''), though that bird resembles the Eurasian subspecies. The hoopoe starling was first noted on the [[Mascarene island]] of [[Réunion]] (then called "Bourbon") by Père Vachet in 1669, but was not described in detail until [[Sieur Dubois]]'s 1674 account:<ref name="Hume 2014 8–14">Hume, J. P. (2014). pp. 8–14.</ref>
{{Quotation|Hoopoes or 'Calandres', having a white tuft on the head, the rest of the plumage white and grey, the bill and the feet like a bird of prey; they are a little larger than the young pigeons. This is another good game [i.e., to eat] when it is fat.<ref name="Rothschild">{{Cite book  | last = Rothschild  | first = W.  | authorlink = Walter Rothschild, 2nd Baron Rothschild  | title = Extinct Birds  | publisher = [[Hutchinson (publisher)|Hutchinson & Co]]  | year = 1907  | location = London  | pages = 3–4  | url = https://archive.org/stream/extinctbirdsatte00roth#page/2/mode/2up}}</ref>}}
Early settlers on Réunion referred to the bird as "huppe", due to the similarity of its crest and curved bill with that of the hoopoe. Little was recorded about the hoopoe starling during the next 100&nbsp;years, but specimens began to be brought to Europe during the 18th century. Although the species was first scientifically described by French naturalist [[Philippe Guéneau de Montbeillard]] in the 1779 edition of [[Comte de Buffon]]'s ''[[Histoire Naturelle]]'', it did not receive its [[scientific name]] until its designation by Dutch naturalist [[Pieter Boddaert]] for the book's 1783 edition. Boddaert named the bird ''Upupa varia''; its genus name is that of the hoopoe, and its specific name means "variegated", describing its black-and-white colour. Boddaert provided [[Linnaean taxonomy|Linnean]] [[binomial nomenclature|binomial]] names for plates in Buffon's works, so the accompanying 1770s plate of the hoopoe starling by [[François-Nicolas Martinet]] is considered the [[holotype]] or type illustration. Though the plate may have been based on a specimen in the [[National Museum of Natural History in Paris]], this is impossible to determine today; the Paris museum originally had five hoopoe starling skins, some which only arrived during the 19th century. The possibly female specimen MNHN 2000-756, one of the most-illustrated skins, has an artificially trimmed crest resulting in an unnaturally semi-circular shape, unlike its appearance in life; the type illustration has a similarly shaped crest.<ref name="Hume 2014 8–14"/>
[[File:Fregilupus varius - John Gerrard Keulemans improved.jpg|upright|thumb|left|alt=Painting of brown-and-white bird on a branch, with a shorter head tuft|1907 illustration by [[John Gerrard Keulemans]], based on a specimen in Paris with a trimmed crest]]
De Flacourt's "tivouch" led early writers to believe that variants of the bird were found on Madagascar and the [[Cape of Africa]]; they were thought to be hoopoes of the ''Upupa'' genus, which received names such as ''Upupa capensis'' and ''Upupa madagascariensis''. A number of authors also allied the bird with groups such as [[birds-of-paradise]], [[bee-eater]]s, [[cowbird]]s, [[Icteridae]], and [[chough]]s, resulting in its reassignment to other genera with new names, such as ''Coracia cristata'' and ''Pastor upupa''. In 1831, [[René-Primevère Lesson]] placed the bird in its own [[monotypic]] genus, ''Fregilipus'', a composite of ''Upupa'' and ''Fregilus'', the latter a defunct genus name of the [[chough]]. [[Auguste Vinson]] established in 1868 that the bird was restricted to the island of Réunion and proposed a new binomial, ''Fregilupus borbonicus'', referring to the former name of the island.<ref name="Hume 2014 29–44">Hume, J. P. (2014). pp. 29–44.</ref>
[[File:Fregilupus varius skeleton.jpg|thumb|alt=Drawing of bird skeleton on a branch|[[Lithograph]] of the only known skeleton, by [[Philibert Charles Berjeau]], 1874]]
[[Hermann Schlegel]] first proposed in 1857 that the species belonged to the [[starling]] family (Sturnidae), reclassifying it as part of the ''[[Sturnus]]'' genus ''S. capensis''. This reclassification was observed by other authors; [[Carl Jakob Sundevall]] proposed the new genus name ''Lophopsarus'' ("crested starling") in 1872, yet ''Fregilupus varius''—the oldest name—remains the bird's binomial, and all other scientific names are synonyms.<ref name="Hume 2014 8–14"/> In 1874, after a detailed analysis of the only known skeleton (held at the [[Cambridge University Museum of Zoology]]), James Murie agreed that it was a starling.<ref name="Murie">{{Cite journal | doi = 10.1111/j.1096-3642.1874.tb02508.x| title = On the Skeleton and Lineage of ''Fregilupus varius''| journal = Proceedings of the Zoological Society of London| url= http://www.biodiversitylibrary.org/page/28502639#page/622/mode/1up| volume = 42| pages = 474| year = 1874| last1 = Murie | first1 = D. J. }}</ref> [[Richard Bowdler Sharpe]] said in 1890 that the hoopoe starling was similar to the starling genus ''[[Basilornis]]'', but did not note any similarities other than their crests.<ref>{{Cite journal | doi = 10.1038/040177a0| title = The Extinct Starling of Réunion (''Fregilupus varius'')| journal = [[Nature (journal)|Nature]]| volume = 40| issue = 1025| pages = 177| year = 1889| last1 = Sharpe | first1 = R. B. | bibcode = 1889Natur..40..177S}}</ref> In 1941, Malcolm R. Miller found the bird's musculature similar to that of the [[common starling]] (''Sturnus vulgaris'') after he dissected a specimen preserved in spirits at the Cambridge Museum, but noted that the tissue was very degraded and the similarity did not necessarily confirm a relationship with starlings.<ref>{{Cite journal | doi = 10.2307/4078667| jstor = 4078667| title = Myology of ''Fregilupus varius'' in relation to its systematic position| journal = [[The Auk]]| volume = 58| issue = 4| pages = 586–587| year = 1941| last1 = Miller | first1 = M. R. }}</ref> In 1957, [[Andrew John Berger]] cast doubt on the bird's affinity with starlings due to subtle anatomical differences, after dissecting a spirit specimen at the [[American Museum of Natural History]].<ref name="Berger">{{cite journal |author=Berger, A. J. |author-link=Andrew John Berger |year=1957 |title=On the anatomy and relationships of ''Fregilupus varius'', an extinct starling from the Mascarene Islands |journal=[[Bulletin of the American Museum of Natural History]] |volume=113 |pages=225–272 |url=http://digitallibrary.amnh.org/dspace/bitstream/2246/1254/1/B113a03.pdf |format=PDF}}</ref> Some authors proposed a relationship with [[vanga]]s (Vangidae), but [[Hiroyuki Morioka]] rejected this in 1996, after a comparative study of skulls.<ref>{{Cite book
| last1 = Morioka | first1 = H.
| editor1-last = Yamagishi
| editor1-first = S.
| chapter = The skull of the Réunion starling ''Fregilupus varius'' and the Sickle-billed Vanga ''Falculea palliata'', with notes on their relationships and feeding habits
| title = Social evolution of birds in Madagascar, with special respect to vangas
| pages = 5–16
| year = 1996
| isbn = 978-1-4081-9049-4
| location = Osaka
| publisher = [[Osaka City University]]
}}</ref>

In 1875, British ornithologist [[Alfred Newton]] attempted to identify a black-and-white bird mentioned in an 18th-century manuscript describing a marooned sailor's stay on the Mascarene island of [[Rodrigues]] in 1726–27, hypothesising that it was related to the hoopoe starling.<ref>{{Cite journal  | last =  Newton | first =  A. | last2 =   | first2 =   | title = Additional Evidence as to the Original Fauna of Rodriguez  | journal =   Proceedings of the Zoological Society of London | series =   | volume =   | pages = 39–43  | url =  http://www.biodiversitylibrary.org/page/28501180#page/77/mode/1up | year = 1875}}</ref> [[Subfossil]] bones later found on Rodrigues were correlated with the bird in the manuscript; in 1879, these bones became the basis for a new species, ''Necropsar rodericanus'' (the [[Rodrigues starling]]), named by ornithologists [[Albert Günther]] and [[Edward Newton]]. Although they found the Rodrigues bird closely related to the hoopoe starling, Günther and Newton kept it in a separate genus due to "present ornithological practice".<ref name="Günther">{{cite journal| doi = 10.1098/rstl.1879.0043| last1 = Günther | first1 = A.| last2 = Newton | first2 = E.| year = 1879| title = The extinct birds of Rodriguez| journal = [[Philosophical Transactions of the Royal Society of London]]| volume = 168| pages = 423–437| url = https://archive.org/stream/philtrans07832595/07832595#page/n0/mode/2up| pmc = | ref = harv
}}</ref> American ornithologist [[James Greenway]] suggested in 1967 that the Rodrigues starling belonged in the same genus as the hoopoe starling, due to their close similarity.<ref name ="Greenway">{{cite book
 | last = Greenway
 | first = J. C.
 | title = Extinct and Vanishing Birds of the World
 | publisher = American Committee for International Wild Life Protection
 | series =
 | volume =
 | edition =
 | location = New York
 | year = 1967
 | pages = 129–132
 | doi =
 | isbn = 0-486-21869-4
 | mr =
 | zbl =  }}</ref> Subfossils found in 1974 confirmed that the Rodrigues bird was a distinct genus of starling; primarily, its stouter bill warrants [[generic separation]] from ''Fregilupus''.<ref name="Cowles87">{{Cite book | doi = 10.1017/CBO9780511735769.004| editor1-last = Diamond| editor1-first = A. W.| title = Studies of Mascarene Island Birds| chapter = The fossil record| pages = 90–100| year = 1987| location = Cambridge | last1 = Cowles | first1 = G. S.| isbn = 978-0-511-73576-9}}</ref><ref name="Extinct Birds">{{cite book
| last1 = Hume
| first1 = J. P.
| first2 = M.
| last2 = Walters
|pages= 271–273
|year= 2012
|location= London
|title= Extinct Birds
|publisher= [[A & C Black]]
|isbn=1-4081-5725-X}}</ref> In 2014, British palaeontologist [[Julian P. Hume]] described a new extinct species, the [[Mauritius starling]] (''Cryptopsar ischyrhynchus''), based on subfossils from [[Mauritius]], which was closer to the Rodrigues starling than to the hoopoe starling in its skull, [[sternal]], and [[humeral]] features.<ref name="Hume 2014 55–58"/>

===Evolution===
[[File:Bali starling hkg.jpg|thumb|alt=White bird with dark wing and tail feathers and a blue mask|The related Bali myna, which is similarly coloured and also has a crest]]
[[File:Hoopoe starling skeletal elements.jpg|thumb|upright|alt=Drawing of separate bird bones|Hoopoe starling skeletal elements, 1874]]
In 1943, [[Dean Amadon]] suggested that ''Sturnus''-like species could have arrived in Africa and given rise to the [[wattled starling]] (''Creatophora cinerea'') and the Mascarene starlings. According to Amadon, the Rodrigues and hoopoe starlings were related to Asiatic starlings—such as some ''Sturnus'' species—rather than to the [[glossy starlings]] (''Lamprotornis'') of Africa and the [[Madagascan starling]] (''Saroglossa aurata''), based on their colouration.<ref name="Amadon 1943">{{cite journal |author=Amadon, D. |author-link=Dean Amadon |year=1943 |title=Genera of starlings and their relationships |journal=[[American Museum Novitates]] |volume=1247 |pages=1–16 |url=http://digitallibrary.amnh.org/dspace/bitstream/2246/4757/1/N1247.pdf |format=PDF}}</ref><ref>{{cite journal |author=Amadon, D. |author-link=Dean Amadon |year=1956 |title=Remarks on the starlings, family Sturnidae |journal=[[American Museum Novitates]] |volume=1803 |pages=1–41 |url=http://digitallibrary.amnh.org/dspace/bitstream/2246/5410/1/N1803.pdf |format=PDF}}</ref> A 2008 study analysing the DNA of a variety of starlings confirmed that the hoopoe starling belonged in a [[clade]] of Southeast Asian starlings as an isolated lineage, with no close relatives. The following [[cladogram]] shows its position:<ref name="Zuccon 2008">{{Cite journal | doi = 10.1111/j.1463-6409.2008.00339.x| title = Phylogenetic relationships among Palearctic-Oriental starlings and mynas (genera ''Sturnus'' and ''Acridotheres'': Sturnidae)| journal = [[Zoologica Scripta]]| volume = 37| issue = 5| pages = 469–481| year = 2008| last1 = Zuccon | first1 = D. | last2 = Pasquet | first2 = E. | last3 = Ericson | first3 = P. G. P. }}</ref>
{{clade| style=font-size:100%; line-height:100%
|1={{clade
 |1=''[[Sturnornus albofrontatus]]'' (white-faced starling)
 |2=''[[Leucopsar rothschildi]]'' (Bali myna)
 |3={{clade
  |1='''''Fregilupus varius''''' ('''hoopoe starling''')
  |2={{clade
   |1=''[[Sturnus sinensis]]'' (white-shouldered starling)
   |2={{clade
    |1=''[[Sturnus pagodarum]]'' (brahminy starling)
    |2={{clade
     |1=''[[Sturnus erythropygia]]'' (white-headed starling)
     |2=''[[Sturnus malabaricus]]'' (chestnut-tailed starling) }} }} }} }} }} }}

An earlier attempt by another team could not extract viable hoopoe starling DNA.<ref>{{Cite journal | doi = 10.1016/j.ympev.2008.01.020| pmid = 18321732| title = A complete species-level molecular phylogeny for the "Eurasian" starlings (Sturnidae: ''Sturnus'', ''Acridotheres'', and allies): Recent diversification in a highly social and dispersive avian group| journal = [[Molecular Phylogenetics and Evolution]]| volume = 47| issue = 1| pages = 251–260| year = 2008| last1 = Lovette | first1 = I. J. | last2 = McCleery | first2 = B. V. | last3 = Talaba | first3 = A. L. | last4 = Rubenstein | first4 = D. R. }}</ref> The authors of the successful study suggested that ancestors of the hoopoe starling reached Réunion from Southeast Asia by using island chains as "stepping stones" across the Indian Ocean, a scenario also suggested for other Mascarene birds. Its lineage diverged from that of other starlings four million years ago (about two million years before Réunion emerged from the sea), so it may have first evolved on landmasses now partially submerged.<ref name="Zuccon 2008"/>

Extant relations, such as the [[Bali myna]] (''Leucopsar rothschildi'') and the [[white-headed starling]] (''Sturnia erythropygia''), have similarities in colouration and other features with the extinct Mascarene species. Since the Rodrigues and Mauritius starlings seem morphologically closer to each other than to the hoopoe starling—which appears closer to Southeast Asian starlings—there may have been two separate migrations of starlings from Asia to the Mascarenes, with the hoopoe starling the latest arrival. Except for Madagascar, the Mascarenes were the only islands in the southwestern Indian Ocean with native starlings, probably due to their isolation, varied topography, and vegetation.<ref name="Hume 2014 55–58"/>

==Description==
[[File:Bourbon Crested Starling.jpg|upright|thumb|left|alt=Painting of brown-and-white, crested bird with a long beak, on a branch under an overhanging rock|1910 illustration by Eduard de Maes]]
The hoopoe starling was {{convert|30|cm|in|abbr=on}} in length. The bird's [[Beak#Culmen|culmen]] was {{convert|41|mm|in|abbr=on}} long, its wing {{convert|147|mm|in|abbr=on}}, its tail {{convert|114|mm|in|abbr=on}}, and its [[tarsus (skeleton)|tarsus]] about {{convert|39|mm|in|abbr=on}} long.<ref name="Fuller Extinct">{{cite book
  | last = Fuller
  | first = E.
  | authorlink = Errol Fuller
  | year = 2001
  | title = Extinct Birds
  | edition = revised
  | publisher = Comstock
  | location = New York
  | pages = 364–365
  | isbn = 978-0-8014-3954-4
  | ref = harv
  }}</ref> It was the largest of the three Mascarene starlings. A presumed adult male (NHMUK 1889.5.30.15) in the Paris museum has a light ash-grey head and back of the neck (lighter on the hind-neck), with a long crest the same colour with white shafts. Its back and tail are ash-brown, its wings darker with a greyish wash, and its uppertail covert feathers and rump have a rufous wash. Its [[Flight feathers#Primaries|primary]] [[Covert feather|coverts]] are white with brown tips, although the bases (instead of the tips) are brown in other specimens. The [[superciliary stripe]], [[Lore (anatomy)|lore]], and most of the specimen's underside is white, with a pale rufous wash on the flanks and undertail coverts. The extent of light rufous on the underside varies by specimen. The beak and legs are lemon-yellow, with yellow-brown claws. It has a bare, triangular area of skin around the eye, which may have been yellow in life. Though the species' [[Iris (anatomy)|iris]] was described as bluish-brown, it has been depicted as brown, yellow or orange.<ref name="Hume 2014 14–29">Hume, J. P. (2014). pp. 14–29.</ref>

There has been confusion about which characteristics that were [[Sexual dimorphism|sexually dimorphic]] in the species. Only three specimens were sexed (all males), with age and individual variation not considered. The male is thought to have been largest with a longer, curvier beak. In 1911, Réunion resident Eugène Jacob de Cordemoy recalled his observations of the bird about 50&nbsp;years before, suggesting that only males had a white crest, but this is thought to be incorrect. A presumed female (MNHN 2000-756) in the Paris museum appears to have a smaller crest, a smaller and less-curved beak, and smaller primary coverts. A juvenile specimen has a smaller crest and primary coverts, with a brown wash instead of ash grey on the crest, lore, and superciliary stripe, and a light-brown (instead of ash-brown) back. The juveniles of some southeast Asian starlings are also browner than adults.<ref name="Hume 2014 29–44"/>
[[File:Naturalis Biodiversity Center - RMNH.AVES.110050 - Fregilupus varius (Boddaert, 1783) - Réunion Starling - specimen - video.webm|thumb|alt=Perching hoopoe starling museum specimen|thumbtime=0:00|Turnaround video of specimen RMNH 110.050, [[Naturalis Biodiversity Center]]]]
Vinson, who observed live hoopoe starlings when he lived on Réunion, described the crest as flexible, disunited and forward-curled barbs of various lengths, highest in the centre, and able to be erected at will. He compared the bird's crest to that of a [[cockatoo]] and to the tail feathers of a [[bird-of-paradise]]. Most mounted specimens have an erect crest, indicating its natural position. The only illustration of the hoopoe starling now thought to have been made from life was drawn by Paul Philippe Sauguin de Jossigny during the early 1770s. Jossigny instructed engravers under the drawing that for accuracy, they should depict the crest angled forward from the head (not straight up). Hume believes that Martinet did this when he made the type illustration, and it was derivative of Jossigny's image rather than a life drawing. Jossigny also made the only known life drawing of the now-extinct [[Newton's parakeet]] (''Psittacula exsul'') after a specimen sent to him from Rodrigues to Mauritius, so this is perhaps also where he drew the hoopoe starling. Murie suggested that only the illustrations by Martinet and [[Jacques Barraband]] were "original", since he was unaware of Jossigny's drawing, but noted a crudeness and stiffness in them which made neither appear lifelike.<ref name="Hume 2014 29–44"/><ref name="Murie"/>

==Behaviour and ecology==
[[File:Hoopoe starling Roussin.jpg|thumb|upright|left|alt=Painting of bright-eyed hoopoe starling on a tree branch|1860s illustration by Albert Roussin]]
Little is known about the behaviour of the hoopoe starling. According to [[François Levaillant]]'s 1807 account of the bird (which included observations from a Réunion resident) it was abundant, with large flocks inhabiting humid areas and marshes. In 1831, Lesson, without explanation, described its habits as similar to those of a [[crow]].<ref name="Hume 2014 29–44"/> Vinson's 1877 account relates his experiences with the bird more than 50&nbsp;years earlier:
{{Quotation|Now these daughters of the wood, when they were numerous, flew in flocks and went thus in the rain forests, while deviating little from one another, as good companions or as nymphs taking a bath: they lived on berries, seeds and insects, and the créoles, disgusted by the latter fact, held them for an impure game. Sometimes, coming from the woods to the littoral [coast], always flying and leaping from tree to tree, branch to branch, they often alighted in swarms on coffee trees in bloom, and there was in the past the testimony of an inhabitant of the Island of Bourbon, said the naturalist Levaillant, that they caused big damage in coffee trees by making the flowers fall prematurely. But it is not the white flowers of coffee that the hoopoes were searching for and thus behaving so, it was for the caterpillars and insects that devoured them; and in this they made an important service to the silviculture of the Island of Bourbon and the rich coffee plantations, with which this land was then covered, the golden age of the country!<ref name="Hume 2014 29–44"/>}}
[[File:Panorama-Cilaos-Greatoutdoors.jpg|thumb|alt=Jagged, green mountains against a blue sky|Forested mountain area on [[Réunion]], 2006]]
Like most other starlings, the hoopoe starling was [[omnivorous]], feeding on fruits, seeds, and insects. Its tongue—long, slender, sharp, and frayed—may have been able to move rapidly, helpful when feeding on fruit, nectar, pollen, and invertebrates. Its pelvic elements were robust and its feet and claws large, indicating that it foraged near the ground. Its jaws were strong; Morioka compared its skull to that of the hoopoe, and it may have foraged in a similar way, probing and opening holes in substrate by inserting and opening its beak. De Montbeillard was informed of the stomach contents of a dissected specimen, consisting of seeds and the berries of "''Pseudobuxus''" (possibly ''[[Eugenia buxifolia]]'', a bush with sweet, orange berries). He noted that the bird weighed {{convert|4|oz|g}}, and was fatter around June and July. Several accounts suggest that the hoopoe starling migrated on Réunion, spending six months in the lowlands and six months in the mountains. Food may have been easier to obtain in the lowlands during winter, with the birds breeding in the mountain forests during summer. The hoopoe starling probably nested in tree cavities. Its song was described as a "bright and cheerful whistle" and "clear notes", indicating a similarity to the songs of other starlings.<ref name="Hume 2014 29–44"/>

Many other endemic species on Réunion became extinct after the arrival of humans and the resulting disruption of the island's [[ecosystem]]. The Mascarene parrot lived with other now-extinct birds, such as the [[Réunion ibis]], the [[Mascarene parrot]], the [[Réunion parakeet]], the [[Réunion swamphen]], the [[Réunion owl]], the [[Réunion night heron]], and the [[Réunion pink pigeon]]. Extinct Réunion reptiles include the [[Réunion giant tortoise]] and an undescribed ''[[Leiolopisma]]'' [[skink]]. The [[small Mauritian flying fox]] and the snail ''[[Tropidophora carinata]]'' lived on Réunion and Mauritius before vanishing from both islands.<ref name="Lost Land">{{cite book
| last1 = Cheke | first1 = A. S.
 | first2 = J. P. | last2 = Hume
|year=2008
  |pages = 49–52
|title=Lost Land of the Dodo: an Ecological History of Mauritius, Réunion & Rodrigues
|publisher=[[T. & A. D. Poyser]]
|isbn=978-0-7136-6544-4}}</ref>

==Relationship with humans==
[[File:Barraband Fregilupus.jpg|thumb|upright|alt=Painting of tufted, brown-and-white bird with a curved beak on a branch|1807 illustration by [[Jacques Barraband]]]]
The hoopoe starling was described as tame and easily hunted. In 1704, [[Jean Feuilley]] explained how the birds were caught by humans and cats:
{{Quotation|Hoopoes and merles [''[[Hypsipetes borbonicus]]''] are the same fatness as those in France, and are of a marvellous taste, which are fat at the same time as parrots, living on the same foods. In order to catch them, hunting was done with staffs or long thin poles from six to seven feet in length, though this hunt is infrequently seen. The marrons [escaped] cats destroy many. These birds allow themselves to be approached very closely, so the cats take them without leaving their places.<ref name="Hume 2014 29–44"/>}}
[[File:Fregilupus varius.jpg|thumb|left|alt=Two hoopoe starling heads|Heads of specimens in [[Caen]] (destroyed during World War II) and Paris (with trimmed crest), 1913]]
The hoopoe starling was kept as a [[cagebird]] on Réunion and Mauritius, and although the bird was becoming scarcer, a number of specimens were obtained during the early 19th century. It is unknown whether any live specimens were ever transported from the Mascarenes. Cordemoy recalled that captive birds could be fed a wide variety of food, such as bananas, potatoes, and [[chayote]], and wild birds would never enter inhabited areas. Many individuals survived on Mauritius after escaping there, and it was thought that a feral population could be established. However, the Mauritian population lasted less than a decade; the final specimen on the island (the last definite record of a live specimen anywhere) was taken in 1836. Specimens could still be collected on Réunion during the 1830s and, possibly, the early 1840s.<ref name="Hume 2014 29–44"/>

There are 19 surviving hoopoe starling specimens in museums around the world (including one skeleton and two specimens preserved in spirit), two in the Paris museum and four in [[Troyes]]. Additional skins in Turin, Livorno, and Caen were destroyed during [[World War II]], and four skins have disappeared from Réunion and Mauritius (which now have one each). Specimens were sent to Europe beginning in the second half of the 18th century, with most collected during the first half of the 19th century. It is unclear when each specimen was acquired, and specimens were frequently moved between collections. It is also unclear which specimens were the basis for which descriptions and illustrations.<ref name="Hume 2014 29–44"/> The only known subfossil hoopoe starling specimen is a femur, discovered in 1993 in a Réunion [[grotto]].<ref name="Avifauna">{{Cite journal
| last1 = Mourer-Chauvire
| first1 = C.
| last2 =  Bour
| first2 = R.
| last3 =  Ribes
| first3 = S.
| last4 =  Moutou
| first4 = F.
| title = Avian paleontology at the close of the 20th century: The avifauna of Réunion Island (Mascarene Islands) at the time of the arrival of the first Europeans
| journal = [[Smithsonian Contributions to Paleobiology]]
| volume = 89
| pages = 8–11
| date =
| year = 1999
| url = http://hdl.handle.net/10088/2005}}</ref>

===Extinction===
[[File:FregilupusVarius.JPG|thumb|left|alt=Dark bird with white head and long, slender, curved beak|Specimen in the ornithological collection of the [[Pisa Charterhouse]], Italy]]
Several causes for the decline and sudden disappearance of the hoopoe starling have been proposed, all connected to the activities of humans on Réunion, who it survived alongside for two centuries. An oft-repeated suggestion is that the introduction of the [[common myna]] (''Acridotheres tristis'') led to competition between these two starling species. The myna was introduced to Réunion in 1759 to combat locusts, and became a pest itself. However, the hoopoe starling coexisted with the myna for nearly 100&nbsp;years and they may not have shared habitat. The [[black rat]] (''Rattus rattus'') arrived on Reunion in the 1670s, and the [[brown rat]] (''Rattus norvegicus'') in 1735, multiplying rapidly and threatening agriculture and native species. Like the hoopoe starling, the rats inhabited tree cavities and would have preyed on eggs, juveniles, and nesting birds. During the mid-19th century the [[Réunion slit-eared skink]] (''Gongylomorphus borbonicus'') became extinct due to predation by the introduced [[wolf snake]] (''Lycodon aulicum''), which may have deprived the bird of a significant food source.<ref name="Hume 2014 29–44"/> Hoopoe starlings may have contracted diseases from introduced birds, a factor known to have triggered declines and extinctions in endemic Hawaiian birds. According to ecologist Anthony S. Cheke, this was the chief cause of the hoopoe starling's extinction; the species had survived for generations despite other threats.<ref name="Cheke87">{{Cite book| last1 = Cheke | first1 = A. S. | editor1-last = Diamond| editor1-first = A. W.| doi = 10.1017/CBO9780511735769.003 | chapter = An ecological history of the Mascarene Islands, with particular reference to extinctions and introductions of land vertebrates | title = Studies of Mascarene Island Birds | pages = 5–89 | year = 1987 | isbn = 978-0-521-11331-1| location = Cambridge | publisher = Cambridge University Press }}</ref>

Beginning in the 1830s, Réunion was deforested for plantations. Former slaves joined white peasants in cultivating pristine areas after slavery was abolished in 1848, and the hoopoe starling was pushed to the edges of its former habitat. According to Hume, over-hunting was the final blow to the species; with forests more accessible, hunting by the rapidly growing human population may have driven the remaining birds to extinction. In 1821, a law mandating the extermination of grain-damaging birds was implemented, and the hoopoe starling had a reputation for damaging crops. During the 1860s, various writers noted that the bird had almost disappeared, but it was probably already extinct by this time; in 1877, Vinson lamented that the last individuals might have been killed by recent forest fires. No attempts to preserve the species in captivity seem to have been made.<ref name="Hume 2014 29–44"/><ref name=Vinson-1877>{{cite journal |last1=Vinson |first1=A. |title=Faune détruite. Les Aepiornidés et les Huppes de l'île Bourbon |journal=Bulletin Hebdomadaire de l'Association Scientifique de France |date=1877 |volume=20 |pages=327–331}}</ref> The hoopoe starling survived longer than many other extinct Mascarene species, and was the last of the Mascarene starling species to become extinct. The Rodrigues and Mauritius species probably disappeared with the arrival of rats; at least five species of ''[[Aplonis]]'' starlings have disappeared from the [[Pacific Islands]], with rats contributing to their extinction. The hoopoe starling may have survived longer due to Réunion's rugged [[topography]] and highlands, where it spent much of the year.<ref name="Hume 2014 55–58">Hume, J. P. (2014). pp. 55–58.</ref>

==References==
{{reflist|30em}}

==Works cited==
* {{Cite journal | doi = 10.11646/zootaxa.3849.1.1  | last = Hume | first =  J. P. | year = 2014 | title = Systematics, morphology, and ecological history of the Mascarene starlings (Aves: Sturnidae) with the description of a new genus and species from Mauritius | url=http://www.mapress.com/zootaxa/2014/f/z03849p075f.pdf | journal =[[Zootaxa]] | volume = 3849 | issue = 1 | pages = 1–75 |format=PDF }}

==External links==
{{Commons category-inline|Fregilupus varius}}

{{Birds|state=collapsed}}
{{portalbar|Birds|Animals|Biology|Mauritius|Madagascar|Extinction|Paleontology}}
{{taxonbar}}

[[Category:Birds described in 1783]]
[[Category:Bird extinctions since 1500]]
[[Category:Birds of Réunion]]
[[Category:Endemic fauna of Réunion]]
[[Category:Extinct birds of Indian Ocean islands]]
[[Category:Sturnidae]]
[[Category:Articles containing video clips]]