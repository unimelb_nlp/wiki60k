{{Orphan|date=October 2013}}

{{Infobox journal
| title = Journal of Coatings Technology and Research
| cover = [[File:Journal of Coatings Technology and Research.jpg]]
| editor = Mark Nichols
| discipline = [[Materials science]], [[chemistry]]
| former_names  = JCT Research
| abbreviation = J. Coating. Tech. Res.
| publisher = [[Springer Science+Business Media]] on behalf of the [[American Coatings Association]] and the [[Oil and Colour Chemists' Association]]
| country =
| frequency = Bimonthly
| history = 2004–present
| openaccess = 
| impact = 1.298
| impact-year = 2014
| website = http://www.springer.com/journal/11998/ 
| link1 = http://www.springerlink.com/content/1547-0091
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC =53149714 
| LCCN =2003215614
| CODEN = JRCEB5
| ISSN = 1547-0091 
| eISSN = 1935-3804 
}}
The '''''Journal of Coatings Technology and Research''''' is a bimonthly [[Peer review|peer-reviewed]] [[scientific journal]]. It is co-owned by the [[American Coatings Association]] and the [[Oil and Colour Chemists' Association]] and published on their behalf by [[Springer Science+Business Media]]. The [[editor-in-chief]] of the journal is Mark Nichols ([[Ford Motor Company]]).

== Scope ==
Areas of research covered in the ''Journal of Coatings Technology and Research'' include the manufacture of functional, protective and decorative coatings including [[paint]]s, [[ink]]s and related coatings and their raw materials. The journal publishes research papers describing [[chemistry]], [[physics]], [[materials science]], and [[engineering]] studies relevant to surface coatings; Applications papers on experimental solutions for technological problems in the design, formulation, manufacture, application, use and performance of surface [[coating]]s; review articles offering broad, critical overviews of advances in coatings science; and brief communications, presenting notes and letters on research topics of limited scope or immediate impact.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Advanced Polymers Abstracts]]
* [[Aerospace and High Technology Database]]
* [[Aluminum Industry Abstracts]]
* [[Ceramic Abstracts]] - [[World Ceramics Abstracts]]
* [[Compendex]]
* [[Computer and Information Systems Abstracts]]
* Copper Data Center Database
* [[Engineering Research Database]]
* [[Current Contents]]/Engineering, Computing and Technology
* [[Engineered Materials Abstracts]]
* [[Materials Science Citation Index]]
* [[METADEX]]
* [[Science Citation Index]]
* [[Scopus]]
}}
<!--
==History==
The Federation of Paint and Varnish Production Clubs (FPVPC) published their ''Official Digest - Federation of Paint and Varnish Production Clubs'' (ISSN 0097-2827; volumes 1–31) from 1919 December to 1959. From 1960 to 1965, they went under the name of the Federation of Societies for Paint Technology (FSPT) and their publication became ''Official Digest - Federation of Societies for Paint Technology'' (ISSN 0097-2835; volumes 32–37) to match. From 1966 to 1975 this publication changed to ''Journal of Paint Technology'' (''J. Paint Technol.''; ISSN 0022-3352; CODEN JPTYAX; LCCN 74645020; volumes 38–47). In 1976, they underwent another name change to the Federation of Societies for Coatings Technology (FSCT), so from 1976 to 2003 the journal went under the name ''Journal of Coatings Technology'' (''J. Coating. Tech.''; ISSN 0361-8773; E-ISSN 1935-3804; CODEN JCTEDL; LCCN 76641713; volumes 48–75). Springer was the publisher from 1997 to 2003 (volumes 69–75).

In 2004, this then split into ''JCT Coatings Tech'' (ISSN 1547-0083; CODEN JCCOBW; LCCN 2003215613) and ''JCT Research'' (ISSN 1547-0091; CODEN JRCEB5; LCCN 2003215614). In 2007, the Oil and Colour Chemists' Association ''Surface Coatings International Part B: Coatings Transactions'' (ISSN 1476-4865; E-ISSN 1742-0261; CODEN SCIPDU; LCCN 2001242086) was subsequently merged into ''JCT Research'' and it was renamed as the ''Journal of Coatings Technology and Research'' (J. Coating. Tech. Res.; ISSN 1945-9645; LCCN 2007242238).

This refactoring is likely due at least in part to the FSCT being merged into National Paint and Coatings Association (NPCA) in 2009. In January 2010, the merged NPCA then changed its name to American Coatings Association (ACA).
-->

== External links ==
*{{Official website|http://www.springer.com/11998}}

[[Category:English-language journals]]
[[Category:Materials science journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 2004]]