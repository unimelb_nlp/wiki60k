{{good article}}
{{Use mdy dates|date=March 2016}}
{{Infobox video game
| title     = Banished
| image     = Banished logo.jpg
| released  = '''Microsoft Windows'''<br />{{Video game release|WW|February 18, 2014}}
}}

'''''Banished''''' is a [[City-building game|city-building]] [[Strategy video game|strategy]] video game developed by Shining Rock Software. It was released for [[Microsoft Windows]] on February 18, 2014. The game focuses on careful resource management and survival as an isolated and growing society. Its gameplay can be compared with economic theory on sustainability and optimization. ''Banished'' received mixed reviews on release, with reviewers praising graphics and difficulty early in the game, but criticizing a lack of feedback for player actions and lower emphasis on survival as the city grows.

==Gameplay==
[[File:Banished_gameplay.jpg|thumb|left|''Banished'' gameplay without the [[user interface]].]]
''Banished'' revolves around the player guiding citizens of a remote community of outcasts to grow and maintain a settlement through a [[command economy]]. The game focuses on the player's town as a whole, with the citizens of the town acting as a resource to be managed. The player must assign citizens to various jobs such as building or fishing. The citizens then perform the job without specific direction from the player. Citizens have needs that must be met in order to keep them happy and healthy, such as ensuring enough food is available or that they have a home. Additional citizens come from two sources, the birth of children and the arrival of nomads, wandering groups of citizens that wish to join the player's [[town]]. The player's citizens will age and eventually die.<ref name=PCGUSReview/><ref name="EuroG" /><ref name="escapist"/>

The player must order the building of various structures in order to support the citizens, such as houses, blacksmith shops, hospitals, farms, and schools, while ensuring enough resources are being produced to keep the citizens stocked. The player must balance the use of resources against the town's growth.<ref name="escapist"/><ref name="RPS">{{cite web|url=http://www.rockpapershotgun.com/2013/03/22/simsettlement-banished-is-about-survival-and-life/ |title=SimSettlement: Banished Is About Survival And Life|last=Grayson|first=Nathan|date=March 22, 2013|accessdate=March 22, 2013}}</ref>

The citizens of the society act as the player's primary resource. Keeping them healthy, happy and well-fed are pivotal to making the town prosper. Hazards in the game come in the form of harsh weather conditions, fires, depression, starvation, and an ageing population.<ref name="RPS"/>

==Development and release==
Development of ''Banished'' began in August 2011 and was developed solely by Luke Hodorowicz under the studio name Shining Rock Software.<ref name="srs">{{cite web|url=http://www.shiningrocksoftware.com/?p=1262 |title=Two years ago…|publisher=Shining Rock Software|last=Hodorowicz|first=Luke|date=August 4, 2013|accessdate=October 28, 2013}}</ref><ref>{{cite web|url=http://www.rockpapershotgun.com/2014/01/10/out-of-exile-banished-releasing-in-february/|title=Out Of Exile: Banished Releasing In February|last=Grayson|first=Nathan|date=January 10, 2014|accessdate=July 1, 2015}}</ref> In a September 2013 interview, Hodorowicz said that the ''[[Anno (series)|Anno]]'' series of games was a big inspiration, which influenced his decision not to include combat in the initial release.<ref>{{cite web|url=http://www.pcgamer.com/interview-banished-developer-talks-player-choice-in-the-indie-city-builder-sandbox-game/|title=Banished interview: player choice in an indie city builder sandbox game|last=Birnbaum|first=Ian|date=September 3, 2013|accessdate=July 1, 2015}}</ref>

''PC Gamer'' interviewed Hodorowicz in December 2013, where he noted that he hoped to release the game by January 2014. Hodorowicz stated he had put over 5500 hours of development into the game, and hoped to eventually add support for [[Linux]] and [[OS X]].<ref>{{cite web|url=http://www.pcgamer.com/more-banished-details-emerge-from-developer-ama/|title=More Banished details emerge from developer AMA|last=Maiberg|first=Emanuel|date=December 18, 2013|accessdate=July 1, 2015}}</ref> On January 9, 2014, it was announced the game would be released for [[Microsoft Windows]] on February 18, 2014.<ref>{{cite web|url=http://www.pcgamer.com/banished-will-be-cast-out-into-the-wild-on-february-18th/|title=Banished will be cast out into the wild on February 18th|last=Skyes|first=Tom|date=January 10, 2014|accessdate=July 1, 2015}}</ref> Support for [[Mod (video gaming)|mods]] was announced in July 2014,<ref>{{cite web|url=http://www.rockpapershotgun.com/2014/07/10/banished-mod-support/|title=No Longer Famished: Banished Getting Mod Support|last=Grayson|first=Nathan|date=July 10, 2014|accessdate=July 1, 2015}}</ref> and was released as part of patch 1.4 in November 2014.<ref>{{cite web|url=http://www.gamestar.de/spiele/banished/news/banished,50454,3080001.html|title=Banished - Patch 1.04 mit Mod-Support veröffentlicht|first=Andre|last=Linken|language=German|date=November 5, 2014|accessdate=December 29, 2016}}</ref>

==Reception==
{{Video game reviews
| MC = 73/100<ref name="MC">{{cite web|url=http://www.metacritic.com/game/pc/banished|title=''Banished'' Metacritic Reviews|publisher=[[Metacritic]]|accessdate=July 1, 2015}}</ref>
| EuroG = 6/10<ref name="EuroG">{{cite web|url=http://www.eurogamer.net/articles/2014-02-25-banished-review |title=''Banished'' Review |publisher=Eurogamer |date=February 25, 2014|accessdate=July 1, 2015|first=Paul |last=Dean}}"</ref>
| GI = 8/10<ref name="GI">{{cite web|url=http://www.gameinformer.com/games/banished/b/pc/archive/2014/02/17/banished_2d00_review_2d00_winter_2d00_is_2d00_coming.aspx |title=Winter Is Coming - ''Banished''|work=Game Informer|date=February 17, 2014|accessdate=July 1, 2015 |first=Daniel |last=Tack}}</ref>
| GSpot = 8/10<ref name=GSpot>{{cite web|url=http://www.gamespot.com/banished/reviews/|title=''Banished'' Review|publisher=[[GameSpot]]|last=Starkey|first=Daniel|date=February 26, 2014|accessdate=July 1, 2015}}</ref>
| IGN = 8.3/10<ref name="IGN">{{cite web|url=http://www.ign.com/articles/2014/02/21/banished-review|title=''Banished'' Review - 'TIS THE SEASON FOR CITY BUILDING.|work=IGN |date=February 20, 2014|accessdate=July 1, 2015|first=Rowan|last=Kaiser}}</ref>
| PCGUS = 70/100<ref name=PCGUSReview>{{cite web|url=http://www.pcgamer.com/banished-review/|title=''Banished'' Review|last=Chalk|first=Andy|publisher=[[PC Gamer]]|date=February 20, 2014|accessdate=July 1, 2015}}</ref>
| Poly = 7.5/10<ref name="poly">{{cite web|url=http://www.polygon.com/2014/3/28/5546614/banished-review-feast-or-famine|title=''Banished'' Review: Feast or Famine|last=Hall|first=Charlie|date=March 28, 2014|accessdate=July 1, 2015}}</ref>
| rev1 = ''[[The Escapist (magazine)|The Escapist]]''
| rev1Score = {{Rating|3.5|5}}<ref name="escapist">{{cite web|url=http://www.escapistmagazine.com/articles/view/video-games/editorials/reviews/11021-Banished-Review-We-Built-This-City |title=''Banished'' Review - We Built This City|work=The Escapist|date=February 18, 2014|accessdate=July 1, 2015|first=Greg |last=Tito}}</ref>
}}

''Banished'' received "mixed or average" reviews upon release, according to review aggregator [[Metacritic]], which assigns a weighted average score out of 100 to reviews from critics and gave the game a score of 73 based on 32 reviews.<ref name="MC" />

Charlie Hall from [[Polygon (website)|Polygon]] scored the game 7.5 out of 10.0, noting that ''Banished'' felt more like a "survival simulation" than a city-building simulation. Hall also praised the games graphics and animations, but noted that the game struggled with giving players solid feedback on the results of their actions.<ref name="poly"/> Dean Paul from [[Eurogamer]] gave the game a score of 6/10, noting that the game was a "survival sandbox. It's harsh", agreeing with Hall that it was difficult to get information. Paul noted that having to build structures in a strict grid-like pattern felt generic and formulaic.<ref name="EuroG" />

''[[PC Gamer]]'''s Andy Chalk gave the game a score of 70 out of 100, stating that it is "a nice change of pace for city-builders", but noted that as the population grows, survival becomes less of a focus and city management takes over.<ref name=PCGUSReview/> Daniel Starkey of [[GameSpot]] scored the game 8/10, noting how the different mechanics of each structure interlock and provide relevance, and praising the difficulty. However, Starkey noted the game suffered from lacking a sense of progression.<ref name=GSpot/>

Daniel Tack for [[Game Informer]] rated the game with an 8/10, comparing it to games such as ''[[Dwarf Fortress]]'' and ''[[Towns (video game)|Towns]]'', but more accessible and with improved graphics.<ref name="GI" /> [[IGN]] 's Rowan Kaiser awarded an 8.3/10 score, noting that ''Banished'' suffered when the gameplay difficulty was lower, while also making the same comparison to ''Dwarf Fortress'' as Game Informer. Kaiser praised ''Banished'' for creating difficulty through the lack of a money resource, rather than using money as the primary source of difficulty.<ref name="IGN"/>

''[[The Escapist (magazine)|The Escapist]]'''s Greg Tito gave the game 3.5/5 stars, praising the simulation aspects and interface, but noting that it doesn't take long to see everything it has to offer. Tito felt the game's difficulty suffered as the town grew larger, with no proper end game goal to keep players engaged beyond earning achievements.<ref name="escapist" />

==References==
{{Reflist|30em}}

==External links==
*{{Official website|www.shiningrocksoftware.com/game/}}

{{portal bar|Video games|2010s}}
[[Category:2014 video games]]
[[Category:City-building games]]
[[Category:Indie video games]]
[[Category:Single-player-only video games]]
[[Category:Steam Workshop games]]
[[Category:Survival video games]]
[[Category:Windows games]]
[[Category:Video games with procedurally generated levels]]