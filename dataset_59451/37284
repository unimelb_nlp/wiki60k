{{about|the original masonry aqueduct|the present-day iron structure|Barton Swing Aqueduct}}
{{good article}}
{{Use dmy dates|date=April 2017}}
{{Use British English|date=April 2017}}
[[File:Barton aqueduct.jpg|thumb|right|300px|The Barton Aqueduct shortly before its demolition in 1893]]
The '''Barton Aqueduct''', opened on 17 July 1761, carried the [[Bridgewater Canal]] over the [[River Irwell]] at [[Barton-upon-Irwell]] in Greater Manchester, England. Designed largely by [[James Brindley]] under the direction of [[John Gilbert (agent)|John Gilbert]],{{sfnp|Atkinson|2002|p=14|ps=}} it was the first [[navigable aqueduct]] to be built in England, "one of the seven wonders of the canal age" according to industrial archaeologist Mike Nevell.{{sfnp|Nevell|1997|p=135|ps=}}

Construction proceeded quickly, but disaster almost struck when the aqueduct was first filled with water and one of its three arches began to buckle under the weight. Remedial work took several months, but the aqueduct was still opened to traffic only 15 months after the enabling [[Acts of Parliament in the United Kingdom|Act of Parliament]] had been passed, on 17 July 1761. It remained in use for more than 100 years, until the construction of the [[Manchester Ship Canal]] necessitated its demolition in 1893, replaced by the [[Barton Swing Aqueduct]].

==Background==

The original intention was for the [[Bridgewater Canal]] to reach [[Salford, Greater Manchester|Salford]] from the [[Francis Egerton, 3rd Duke of Bridgewater|Duke of Bridgewater]]'s coal mines in [[Worsley]], by remaining on the north bank of the [[River Irwell|Irwell]]. Work began in 1759, but it was quickly decided to alter the route by building a masonry aqueduct to carry the waterway over the Irwell at [[Barton-upon-Irwell|Barton]], and terminate instead in [[Manchester]], to the south of the river. A Bill to authorise the new route was presented to parliament on 13 November 1759, and in January the following year Brindley travelled to London to give evidence before a parliamentary committee in support of the proposal.{{sfnp|Atkinson|2002|pp=14–15|ps=}}

Although a gifted engineer Brindley had no formal education and rarely, if ever, committed his designs to paper.{{sfnp|Weale|1844|p=45|ps=}} When questioned by the parliamentary committee about the composition of the [[Puddling (engineering)|puddle]] he frequently referred to in his evidence, he had a mass of clay brought into the committee room. He then formed the clay into a trough and showed how it would only form a watertight seal if it had been worked with water to form puddle. "Thus it is" said Brindley "that I form a watertight-trunk to carry water over rivers and valleys wherever they cross the path of the canal."{{sfnp|Weale|1844|p=47|ps=}} Later, when asked to produce a drawing of the bridge or aqueduct he proposed to build, he replied that he had no representation of it on paper but would demonstrate his intention by use of a model. He then went out and bought a large round of [[Cheshire cheese]], which he divided into two equal halves saying "Here is my model". Then, to the amusement of the committee, he used the two halves of cheese to represent the semicircular arches and laid a long, rectangular object over the top to demonstrate the position of the river flowing under the aqueduct and the canal flowing over it.{{sfnp|Weale|1844|p=47|ps=}}

Although the duke had seen navigable aqueducts in use on canals when travelling abroad on his [[Grand Tour]], the idea of an such a structure carrying a canal over a river was new to England and was ridiculed by contemporary engineers.{{sfnp|Atkinson|2002|pp=14–15|ps=}}{{sfnp|Bode|2008|pp=21–24|ps=}} One brought in to review the plans, at Brindley's request, commented in a report to the Duke of Bridgewater that "I have often heard of castles in the air, but never before saw where one was to be erected."{{sfnp|Atkinson|2002|p=15|ps=}} The necessary [[Acts of Parliament in the United Kingdom|Act of Parliament]] was passed in March 1760, and was quite specific about the form the aqueduct had to take, to protect the viability of the [[Mersey & Irwell Navigation]] below. There was already a three-arch road bridge, Barton Bridge, passing over the Irwell, and the aqueduct was required not to restrict traffic on the river any more than the road bridge already did. It had to have the same number of arches, the foundations for which had to be fixed in the river bed, and the arches had to be at least as wide and high as those of the road bridge.{{sfnp|Atkinson|2002|pp=14–15|ps=}}

==Construction==
[[File:Watercolour of Barton aqueduct by G.F. Yates 1793.jpg|thumb|Watercolour, pen and ink drawing of Barton Aqueduct in 1793 by G. F. Yates]]
At about {{convert|200|yd}} long, {{convert|12|yd}} wide and {{convert|39|ft}} above the river at its highest point, the aqueduct was, for its time, an enormous construction.{{sfnp|Bode|2008|p=22|ps=}} Early illustrations show the aqueduct's piers to have been flat-faced, but an engraving of 1864 shows them to have pointed [[Starling (architecture)|cutwater]]s extending beyond the spring of the arch;{{sfnp|Atkinson|2002|p=20|ps=}} it is likely that the piers were refaced in the early 1820s.{{sfnp|Atkinson|2002|p=21|ps=}} The arches were composed of several rings of brickwork, with masonry used for decorative [[keystone (architecture)|keystone]]s. All the masonry used in the structure was coursed [[ashlar]].{{sfnp|Atkinson|2002|p=20|ps=}}

On the day it was first tested the water was allowed to flow in, but one of the arches began to buckle under the weight. Brindley, overcome with anxiety, retired to his bed at the Bishop Blaize tavern in nearby [[Stretford]]. Gilbert, realising that Brindley had placed too much weight on the sides of the arch, removed the clay and laid layers of straw and freshly puddled clay; when the water was allowed to flow in again the masonry held firm.{{sfnp|Malet|1977|p=65|ps=}} According to a statement by [[Francis Egerton, 8th Earl of Bridgewater]] printed in 1820, his uncle, the duke, had told him that there was a distortion of one of the arches, and that Gilbert had addressed the problem by placing  more weight on the crown of the arch and less on the haunches. The arch was then covered with straw and allowed to stand until the following spring, when the mortar was set and the arch had become stable, but its curve remained irregular.{{sfnp|Ruddock|2008|p=125|ps=}}

==Operation and legacy==
The aqueduct was opened to traffic on 17 July 1761, only 15 months after the enabling Act had been passed,{{sfnp|Ruddock|2008|pp=125–131|ps=}} and it was soon being used by the duke's barges to carry coal to [[Manchester, Greater Manchester|Manchester]] from his mines at Worsley.{{sfnp|Bode|2008|p=22|ps=}} The construction of the aqueduct excited great admiration, and writers of the day often remarked on the strange and novel sight afforded by the canal where it crossed the Irwell. The structure  became one of the wonders of the age and crowds came from all over the country to view it, along with the drilling of the [[sough]] for the duke's [[Worsley Navigable Levels|Worsley navigable levels]].{{sfnp|Malet|1977|pp=65–66|ps=}} Those who saw it were often struck by the advantages of still-water navigation when they saw ten or twelve men slowly hauling a single barge against the flow of the Irwell, while {{convert|40|ft}} above a horse, mule, or perhaps two men, could be seen hauling several linked barges across the still waters of the aqueduct.{{sfnp|Weale|1844|p=11|ps=}}

Although the aqueduct was {{convert|12|yd}} wide overall, the waterway it carried was only half that width. The Bridgewater Canal had been built to accommodate the [[Mersey flat]] boats then in common use, which had a beam of about {{convert|14|ft}}, making two-way traffic impossible. A signalling system was therefore installed to control access to the aqueduct. A pole {{convert|15|ft}} high in the centre of the arch at the Stretford bank supported a semaphore system with two arms on each side, operated by levers at ground level.{{sfnp|Atkinson|2002|p=21|ps=}}

Although Gilbert later had to resurface the aqueduct, the structure remained in use for more than 100 years.{{sfnp|Malet|1977|p=66|ps=}} In the first volume of his ''Lives of the Engineers'' (1862) Scottish author [[Samuel Smiles]] said of the construction that "Humble though it now appears, it was parent of the magnificent aqueducts of Rennie and Telford, and the viaducts of Stephenson and Brunel".{{sfnp|Smiles|1904|pp=212–213|ps=}}

==Replacement==
Barton Aqueduct's fate was sealed with the passage of the Manchester Ship Canal Act 1885, which allowed for the construction of a navigable waterway large enough to accommodate ocean-going vessels from the estuary of the [[River Mersey]] the {{convert|36|mi}} into Manchester, partly along the Irwell. As the arches of the aqueduct were too small to allow large ships to pass through it was demolished in 1893, and replaced by the [[Barton Swing Aqueduct]] still in use today.{{sfnp|Atkinson|2002|pp=36–37|ps=}} So solidly built was the old aqueduct that dynamite had to be used to expedite its demolition.{{sfnp|Atkinson|2002|p=21|ps=}}

Some of the stonework of Brindley's aqueduct has been preserved in the nearby Barton Memorial Arch, a monument to his "castle in the air".{{sfnp|Atkinson|2002|p=37|ps=}}

==References==
'''Citations'''
{{reflist|30em}}

'''Bibliography'''
{{refbegin}}
*{{citation |last=Atkinson |first=Glen |title=Barton's Bridges |year=2002 |publisher=Neil Richardson |isbn=978-1-85216-146-0}}
*{{citation |last=Bode |first=Harold |title=James Brindley: An Illustrated Life of James Brindley, 1716–1772 |publisher=Shire |year=2008 |isbn=978-0-85263-485-1}}
*{{citation  |last=Nevell|first=Mike |year=1997 |title=The Archaeology of Trafford |publisher=Trafford Metropolitan Borough with University of Manchester Archaeological Unit |isbn=978-1-870695-25-1}}
*{{citation |last=Malet |first=Hugh |title=Bridgewater: The Canal Duke, 1736–1803 |publisher=Manchester University Press |year=1977 |isbn=978-0-7190-0679-1}}
*{{citation |last=Ruddock |first=Ted |title=Arch Bridges and their Builders 1735–1835 |publisher=Cambridge University Press |year=2008 |isbn=978-0-521-09021-6}}
*{{citation |last=Smiles |first=Samuel |authorlink=Samuel Smiles |title=Lives of the Engineers |volume=1 |year=1904 |origyear=1862 |publisher=J. Murray |oclc=44962634 |url=https://archive.org/details/livesengineers02smilgoog}}
*{{citation |editor-last=Weale |editor-first=John |year=1844 |title=Memoir of James Brindley |work=Quarterly Papers on Engineering, Volume 1 |publisher=George Woodfall and Son}}
{{refend}}

{{coord|53.4747|-2.3522|type:landmark_region:GB|display=title}}

[[Category:Navigable aqueducts in England]]
[[Category:Demolished buildings and structures in Greater Manchester]]
[[Category:Buildings and structures completed in 1761]]
[[Category:Demolished bridges in England]]
[[Category:Buildings and structures demolished in 1893]]