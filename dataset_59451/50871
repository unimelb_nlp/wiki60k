{{Infobox journal
| title = Cell Research
| cover =
| editor = Gang Pei
| discipline = [[Cell biology]]
| abbreviation = Cell Res.
| publisher = [[Nature Publishing Group]] on behalf of the [[Shanghai Institutes for Biological Sciences]]
| frequency = Monthly
| history = 1990-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 14.812
| impact-year = 2015
| website = http://www.cell-research.com/
| link2 = http://www.nature.com/cr/archive/index.html
| link2-name = Online archive
| link3 = http://www.nature.com/cr/index.html
| link3-name = Journal page at publisher's website
| JSTOR =
| OCLC = 52535380
| LCCN = sn96039534
| CODEN = CREEB6
| ISSN = 1001-0602
| eISSN = 1748-7838
}}
'''''Cell Research''''' is a monthly [[peer-reviewed]] [[scientific journal]] covering [[cell biology]]. It is published by the [[Nature Publishing Group]] on behalf of the [[Shanghai Institutes for Biological Sciences]] ([[Chinese Academy of Sciences]]) and is affiliated with the [[Chinese Society for Cell Biology]]. It was established in 1990 and the [[editor-in-chief]] is Gang Pei (Shanghai Institutes for Biological Sciences).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index]]
* [[Current Contents]]/Life Sciences
* [[Chemical Abstracts]]
* [[BIOSIS Previews]]
* [[VINITI Database RAS]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 12.413.<ref name=WoS>{{cite book |year=2015 |chapter=Cell Research |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] }}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|https://web.archive.org/web/20100814211333/http://www.cell-research.com:80/}}
* [http://www.nature.com/cr/index.html NPG website]

{{Georg von Holtzbrinck Publishing Group}}

[[Category:Molecular and cellular biology journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1990]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]