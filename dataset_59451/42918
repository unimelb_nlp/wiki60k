<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name=XFH
|image=Hall XFH-1.jpg
|caption=
}}{{Infobox Aircraft Type
|type=[[Fighter aircraft|fighter]]
|national origin=[[United States]]
|manufacturer=[[Hall Aluminum Company]]
|designer=[[Charles W. Hall]]
|first flight={{avyear|1929}}<ref name="Angel">Angelucci, 1987. pp. 256-257.</ref>
|introduced=
|retired=
|status=
|primary user=
|number built=1
|developed from= 
|variants with their own articles=
}}
|}

The '''Hall XFH''' was an American [[fighter aircraft]] built by the [[Hall Aluminum Company]]. It was the first fighter with a [[semimonocoque]] metal fuselage.<ref name="Angel"/>

==Development==
The XFH was designed in 1927 by Charles Hall. It was a single-bay biplane with N-struts for the fabric-covered wings. Its fuselage was made of steel tubing covered with a watertight aluminum skin, enabling it to float if ditched in the ocean. Also for ditching on water or on land, the landing gear could be jettisoned. Power was provided by a [[Pratt & Whitney Wasp]] radial engine. Testing in June 1929 showed poor handling characteristics and performance. During one test flight, the upper wing separated from the aircraft. After repairs, the XFH made test flights from an [[aircraft carrier]]. Designated XFH by the [[Bureau of Aeronautics]], it was purchased not for active service, but to study new metal construction techniques.<ref name="Angel"/>

==Specifications==
{{aerospecs
|ref=Angelucci, 1987. pp. 256-257.<ref name="Angel"/>
|met or eng?=eng
|crew=one
|capacity=
|length m=6.85
|length ft=22
|length in=6
|span m=9.75
|span ft=32
|span in=0
|height m=3.35
|height ft=11
|height in=0
|wing area sqm=23.68
|wing area sqft=255
|empty weight kg=804
|empty weight lb=1,773
|gross weight kg=1,140
|gross weight lb=2,514
|eng1 number=1
|eng1 type=[[Pratt & Whitney Wasp]]
|eng1 kw=336
|eng1 hp=450
|max speed kmh=246
|max speed mph=153
|cruise speed kmh=
|cruise speed mph=
|stall speed kmh=
|stall speed mph=
|range km=443
|range miles=275
|endurance h=
|endurance min=
|ceiling m=7,711
|ceiling ft=25,300
|climb rate ms=9.07
|climb rate ftmin=1786
|armament1=2 × {{convert|.30|in|mm|2|abbr=on}} [[machine gun]]s (never installed)
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
|similar aircraft=
|lists=
}}

==References==
{{commons category|Hall aircraft}}
===Citations===
{{Reflist}}
===Bibliography===
*{{cite book |last= Angelucci |first= Enzo |title=The American Fighter from 1917 to the present |year=1987 |publisher=Orion Books |location=New York |pages= }}

{{Hall-Aluminum aircraft}}
{{USN fighters}}

[[Category:Hall aircraft|F01H]]
[[Category:United States fighter aircraft 1920–1929]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]