{{Infobox officeholder
| name = Harvey Kesselman
| image =
| caption =
| order =
| title = President of <br />[[Stockton University]]
| term_start = 
| term_end =
| predecessor = [[Herman Saatkamp]]
| successor =  
| birth_date = June 18, 1951
| birth_place = 
| death_date =
| death_place =
| alma_mater = 
*[[Stockton University]]
*[[Rowan University]]
*[[Widener University]]
| residence = 
| profession = President of Stockton University, tenured professor in Stockton’s School of Education
| religion =
| spouse = Lynne Kesselman 
| children =
| signature =
| website = [http://intraweb.stockton.edu/eyos/page.cfm?siteID=201&pageID=95 Office of the President]
| footnotes =
}}

'''Dr. Harvey Kesselman''' is the fifth president of [[Stockton University]] in [[Galloway Township, New Jersey]]<ref>{{cite web|title=How Harvey Kesselman came to oversee Stockton University|url=http://www.pressofatlanticcity.com/education/how-harvey-kesselman-came-to-oversee-stockton-university/article_c62cfed6-58e8-11e5-a21b-0b445a8d070e.html}}</ref> He is the first Stockton alumnus to become president and was a member of the first class at Stockton.  Kesselman was among those students affectionately referred to as the “Mayflower” students, because the first classes in 1971 were held at the Mayflower Hotel, located on the Atlantic City boardwalk, while construction was being completed on the first academic buildings on the main campus in Galloway.<ref>{{cite web|title=Stockton hits milestone anniversary reflecting on its past but looking to the future|url=http://www.pressofatlanticcity.com/features/stockton_40th/stockton-hits-milestone-anniversary-reflecting-on-its-past-but-looking/article_d9968926-e1c9-59b1-919d-af2889146465.html}}</ref>

==Education and Career==

Kesselman received his bachelor's degree in Political Science from [[Stockton University]], his master's degree in Student Personnel Services/Counseling from [[Rowan University]], and his doctorate in Higher Education Administration from [[Widener University]].

His previous senior leadership roles at Stockton include serving as provost and executive vice president; dean, School of Education; interim vice president for Administration and Finance; CEO of the Southern Regional Institute (SRI) and Educational Technology Training Center (ETTC); and vice president for Student Affairs.  Kesselman also is a tenured professor in Stockton’s School of Education.<ref>{{cite web|title=Stockton University|url=http://www.mycentraljersey.com/story/news/2015/09/17/equine-scholarship-4-h-students/71610366/}}</ref>

Kesselman has been appointed by New Jersey governors and state education leaders to serve as the senior college representative on a number of authorities, committees and task forces. He served on the Executive Committee of the Higher Education Student Assistance Authority (HESAA), which oversees New Jersey's $1 billion student financial aid program.<ref>{{cite web|title=Minutes Higher Education Student Assistance Authority|url=http://www.hesaa.org/BoardMeetingMinutes/BoardMinutes07.24.14.pdf}}</ref>

Kesselman remains involved in many national academic organizations. He is a frequent presenter at the American Association of State Colleges and Universities (AASCU), the American Council on Education (ACE), and the Society for College and University Planning (SCUP).  He also serves as a reviewer for the Middle States Commission on Higher Education (MSCHE).<ref>{{cite web|title=Institution Directory|url=https://www.msche.org/institutions_view.asp?idinstitution=376}}</ref>

Kesselman served on the Governor's Task Force to improve the NJ STARS program; participated on the New Jersey College and Career Readiness Task Force; advised the New Jersey State College and University Presidents' Council regarding accountability and assessment; represented the senior public colleges and universities in the NJ Committee of Experts on Campus Sexual Assault Issues; served as one of six representatives appointed by the Chancellor of Higher Education to author and develop the NJ Student Unit Record Enrollment (SURE) statewide data and monitoring system; founded and chaired the SRI & ETTC Consortium, which includes more than 90 school districts throughout New Jersey; and serves as a reviewer for the Middle States Association Commission on Higher Education.  He also serves as the senior public college representative on the New Jersey Higher Education Leadership Team (HELT), which considers policy questions and recommends strategies regarding the implementation of the Partnership for Assessment of Readiness for College and Careers (PARCC).<ref>{{cite web|title=Stockton University to Negotiate With Kesselman for Permanent Presidency|url=http://patch.com/new-jersey/oceancity/stockton-university-negotiate-kesselman-permanent-presidency}}</ref>

He was set to become president of the [[University of Southern Maine]] in the summer of 2015, but chose to remain at Stockton at the request of the Board of Trustees, following the resignation of the immediate past president, Dr. [[Herman Saatkamp]], Jr.<ref>{{cite web|title=Amid turmoil, Stockton's acting president drops plan to leave|url=http://articles.philly.com/2015-05-22/news/62465349_1_showboat-acting-president-faculty}}</ref>

Kesselman was inaugurated as the fifth president of Stockton University on September 23, 2016. <ref>{{cite web|title="Celebrating Our Fifth President"|url=https://stockton.edu/president/inauguration.html}}</ref>

==Personal life==

Kesselman is married to Lynne Kesselman, who received both her bachelor's degree in Business in 1982, and Master of Arts Degree in Instructional Technology in 2005, from Stockton.  She taught at Egg Harbor Township High School for 13 years and was chosen by the U.S. Department of Education to receive the American Star of Teaching Award for New Jersey.<ref>{{cite web|title=Stockton University Acting President Harvey Kesselman and wife, Lynne, Commit $25,000 for New Endowment at Alma Mater|url=http://patch.com/new-jersey/oceancity/stockton-university-acting-president-harvey-kesselman-wife-lynne-commit-25000-new-endowment-alma}}</ref>

Kesselman is actively involved in his community, having been elected in 2009 as a Democrat to serve on the [[Hamilton Township, Atlantic County, New Jersey]] Township Committee<ref>Landau, Joel. [http://www.pressofatlanticcity.com/news/breaking/hamilton-township-committeeman-harvey-kesselman-won-t-run-again-in/article_e4e869b2-0b3c-11e1-b30d-001cc4c002e0.html "Hamilton Township Committeeman Harvey Kesselman won’t run again in 2012"], ''[[The Press of Atlantic City]]'', November 9, 2011. Accessed November 7, 2015. "The 2012 election season already has begun in Hamilton Township. Township Committeeman Harvey Kesselman announced Wednesday he will not seek re-election when his term expires next year.... Kesselman said he felt he did not have enough time to serve on the body, noting his job as provost of The Richard Stockton College of New Jersey in Galloway Township."</ref> and has served on the Board of Education of the [[Hamilton Township Schools]], in addition to serving on the boards of numerous other community and civic organizations.

==References==
{{Reflist}}

{{DEFAULTSORT:Kesselman, Harvey}}
[[Category:1951 births]]
[[Category:Living people]]
[[Category:New Jersey city council members]]
[[Category:New Jersey Democrats]]
[[Category:People from Hamilton Township, Atlantic County, New Jersey]]
[[Category:Stockton University alumni]]
[[Category:Rowan University alumni]]
[[Category:Widener University alumni]]