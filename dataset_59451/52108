{{Infobox journal
| title = Journal of the Optical Society of America
| cover =
| editor = ''J. Opt. Soc. Am. A'': [[Franco Gori]]<br>''J. Opt. Soc. Am. B'': [[Grover Swartzlander]]
| discipline = [[Optics]]
| language =
| abbreviation = J. Opt. Soc. Am.
| publisher = [[The Optical Society]]
| country =
| frequency = Monthly
| history = 1917–present
| openaccess =
| license =
| impact = ''J. Opt. Soc. Am. A'': 1.448<br>
''J. Opt. Soc. Am. B'': 1.806
| impact-year = 2013
| website =
| link1 = http://www.osapublishing.org/josaa/
| link1-name = ''JOSA A'' website
| link2 = http://www.osapublishing.org/josab/
| link2-name = ''JOSA B'' website
| link3= http://www.osapublishing.org/josa/
| link3-name = ''JOSA'' online access
| link4=http://www.osapublishing.org/josaa/issue.cfm
| link4-name=''JOSA A'' online access
| link5=http://www.osapublishing.org/josab/issue.cfm
| link5-name=''JOSA B'' online access
| JSTOR =
| OCLC =
| LCCN =
| CODEN =
| ISSN = 1084-7529
| eISSN = 1520-8532
| ISSNlabel = ''J. Opt. Soc. Am. A''
| ISSN2 = 0740-3224
| eISSN2 = 1520-8540
| ISSN2label = ''J. Opt. Soc. Am. B''
}}
The '''''Journal of the Optical Society of America'''''  is a [[peer-reviewed]] [[scientific journal]] of [[optics]], published by [[The Optical Society]].<ref>{{cite web|url=http://www.osapublishing.org/josa/about.cfm |title=About OSA Publishing|publisher=The Optical Society |date= |accessdate=2015-04-28}}</ref> It was established in 1917<ref>{{cite web|url=http://www.osapublishing.org/josa/journal/josa/20presidents.cfm|title=JOSA History|date=|accessdate=2015-04-28}}</ref> and in 1984 was split into two parts, A and B.

== ''Journal of the Optical Society of America A'' ==
Part A covers various topics in [[optics]], [[Visual system|vision]], and [[Image processing|image science]]. The [[editor-in-chief]] is [[P. Scott Carney]] ([[University of Illinois Urbana-Champaign]]).

== Journal of the Optical Society of America ''B'' ==
Part B covers various topics in the field of [[optical physics]], such as [[guided wave]]s, [[laser spectroscopy]], [[nonlinear optics]], [[quantum optics]], [[laser]]s, organic and polymer materials for optics, and [[ultrafast phenomena]]. The editor-in-chief is [[Grover Swartzlander]] ([[Rochester Institute of Technology]]).

== References ==
{{reflist}}

== External links ==
* [http://josaa.osa.org ''Journal of the Optical Society of America A'' website]
* [http://josab.osa.org ''Journal of the Optical Society of America B'' website]

[[Category:Publications established in 1917]]
[[Category:Optical Society academic journals]]
[[Category:Optics journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]