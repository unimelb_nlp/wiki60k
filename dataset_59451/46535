{{Infobox Wrestling event
|name       = Big Dick Dudley Memorial Show
|image      = Big Dick Dudley Memorial Show 2003 poster.jpg|thumb
|caption    = Poster of the 2003 event
|tagline    =
|theme      =
|promotion  = USA Pro Wrestling
|date       = 2002-2003
|attendance =
|city       = [[Franklin Square, New York]], U.S.
|venue      = Franklin Square Firehouse
|lastevent  =
|nextevent  =
|event      =
|lastevent2 = First
|nextevent2 = [[Chris Candido Memorial Show|Chris Candido Memorial Show (2005)]]
}}
The '''Big Dick Dudley Memorial Show''' was an annual [[professional wrestling]] [[List of professional wrestling memorial shows|memorial event]] produced by the USA Pro Wrestling (USA Pro) [[professional wrestling promotion|promotion]], held between 2002 and 2003. The show was held in memory of [[Big Dick Dudley]], who died of [[kidney failure]] at his apartment in [[Copiague, New York]] on May 16, 2002, with a portion of the proceeds going to his family. A collection from the audience was taken up during the shows as well. The event also served as a reunion show for former alumni of [[Extreme Championship Wrestling]], where Dudley had spent much of his career as a member of [[The Dudley Brothers]], as many appeared at the show to pay their respects.<ref name="Vester">{{cite web |url=http://thesmartmarks.com/printer_97.shtml |title=Indy News And Notes: USA Pro Wrestling To Hold Big Dick Dudley Memorial Show |author=Vester, Byron |author2=Mike Johnson |date=May 26, 2002 |work=The Late Night Smart Marks News Update! |publisher=TheSmartMarks.com}}</ref>

Predating the [[Hardcore Homecoming]] shows of later years, ECW wrestlers that participated were generally those already actively competing in the promotion and elsewhere on the [[independent circuit]]. It was also the first of many memorial shows held for former ECW stars followed by the [[Ted Petty Invitational|Ted Petty Memorial Invitational Tournament]] (2002-2008), the [[Chris Candido Memorial Tag Team Tournament]] (2005), [[Chris Candido Memorial Show]] (2005-2006), [[Jersey J-Cup#2005|Chris Candido Memorial J-Cup]] (2005-) and the [[Pitbull/Public Enemy Memorial Cup|Pitbull/Public Enemy Tag Team Memorial Cup]] (2006).

==Show results==

===First Annual Big Dick Dudley Memorial Show (2002)===
'''June 8, 2002 in [[Franklin Square, New York]] (Franklin Square Firehouse)'''
{{Pro Wrestling results table
|results=
|times=

|match1=[[Sammy Couch|Psycho Sam Dudley]] won the first-annual Big Dick Dudley Memorial battle royal<ref group=Note>The other participants included were: Vic D. Vine, Ken Sweeney, Crazy Ivan, [[Tim Roberts|Tim Arson]], Tyler Payne, Trekkie, Wayne, Iceberg, Dickie Rodz, Brock Vendetta, Wayne, Kid USA, James Hustler, Rev. Zero, Wrecka, Jay Silva, [[Mike Tobin]], and Frankie Starz.</ref>
|stip1=20-man [[Battle royal (professional wrestling)|Big Dick Dudley Memorial Battle Royal]]
|time1=<ref name="OWW">{{cite web |url=http://www.onlineworldofwrestling.com/results/usa-pro/ |title=USA Pro Wrestling |author= |year=2006 |work=Results |publisher=OnlineWorldofWrestling.com}}</ref><ref name="NYProWrestling">{{cite web |url=http://www.nyprowrestling.com/results/2002.html |title=2002 |author=Tanabe, Hisaharu |work=Results |publisher=NYProWrestling.com}}</ref><ref name="RF Video">{{cite video | people=USA Pro Wrestling (Producer) | date=June 8, 2002 | url=http://www.rfvideo.com/usaprobigdickdudleymemorial6802.aspx | title=USA Pro Big Dick Dudley Memorial 6/8/02 | medium=DVD | publisher=[[RF Video]]}}</ref>

|match2=Masked Maniac defeated Defcon, Brother James Hustler, Dickie Rodz and Brock Vendetta
|stip2=[[Professional wrestling match types#Basic elimination matches|Five-Way Elimination match]]
|time2=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match3=[[Jerry Tuite|Malice]] defeated [[Norman Smiley]]
|stip3=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time3=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match4=Stormin’ Norman and Larry McKenny defeated [[The S.A.T.]] (Joel Maximo and Jose Maximo), [[The Bad Street Boys]] ([[Joey Mercury|Joey Matthews]] and [[Christian York]]) and Hot Commodity ([[Chris Hamrick]] and [[Julio Dinero]])
|stip4=[[Professional wrestling match types#Basic elimination matches|Four-Way]] [[Tag team#Tag team match rules|Tag Team match]]; As per the pre-match stipulation, Stormin’ Norman and Larry McKenny became number-one contenders for the USA Pro Tag Team Championship
|time4=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match5=[[Low Ki]] and [[Xavier (wrestler)|Xavier]] defeated [[Da Hit Squad]] ([[Steve Mack|Monsta Mack]] and [[Dan Lopez|Mafia]]) (c) and The Natural Born Sinners ([[Homicide (wrestler)|Homicide]] and Boogaloo)
|stip5=[[Professional wrestling match types#Basic elimination matches|Three-Way]] [[Tag team#Tag team match rules|Tag Team match]] for the USA Pro Tag Team Championship
|time5=<ref name="Vester"/><ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/><ref>{{cite web |url=http://www.solie.org/titlehistories/ttusapw.html |title=USA Pro Wrestling Tag Team Title History |author= Phil Tsakiries |author2= Brian Westcott |author3= Kriss Knights |last-author-amp= yes |year=2007 |work=Solie's Title Histories |publisher=Solie.org}}</ref>

|match6=Wayne the Convenience Store Guy, The Trekkie and The Mime defeated The Iceberg, [[Tim Roberts|Tim Arson]] and Damian Dragon
|stip6=6-man tag team match
|time6=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match7=[[Chris Chetti]] defeated [[Kevin Sullivan (wrestler)|Kevin Sullivan]] via disqualification
|stip7=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time7=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match8=[[Kid Kash]] won a [[Professional wrestling match types#Gauntlet match|gauntlet match]] involving [[Matt Striker]] (with [[Cynthia Lynch|Bobcat]]), [[Simon Diamond]], Chris Divine, Ghost Shadow, Quiet Storm, Grim Reefer, Brian XL, and [[Scoot Andrews]]
|stip8=[[Professional wrestling match types#Gauntlet match|Gauntlet match]]; As per the pre-match stipulation, Kid Kash received a title shot for the USA Pro United States Championship
|time8=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match9=[[Amazing Red]] (c) defeated Kid Kash
|stip9=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the USA Pro United States Championship
|time9=<ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match10=[[Gangrel (wrestler)|Gangrel]] (with [[Luna Vachon]]) defeated [[Crowbar (wrestler)|Crowbar]] (with Serina)
|stip10=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time10=<ref name="Vester"/><ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>

|match11=[[Balls Mahoney]] (c) defeated [[Steve Corino]]
|stip11=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the USA Pro Heavyweight Championship
|time11=<ref name="Vester"/><ref name="OWW"/><ref name="NYProWrestling"/><ref name="RF Video"/>
}}

<references group=Note/>

===Second Annual Big Dick Dudley Memorial Show (2003)===
'''May 31, 2003 in [[Franklin Square, New York]] (Franklin Square Firehouse)'''
{{Pro Wrestling results table
|results=
|times=

|match1=The Solution (Havok and Papadon) (with Miss Michelle) defeated Team Target (Devious and Tekno Heat)
|stip1=[[Tag team#Tag team match rules|Tag Team match]]
|time1=<ref name="OWW"/><ref name="NYProWrestling2">{{cite web |url=http://www.nyprowrestling.com/results/2003.html |title=2003 |author=Tanabe, Hisaharu |work=Results |publisher=NYProWrestling.com}}</ref><ref name="CageMatch">{{cite web |url=http://cagematch.de/?id=1&nr=8860&view=card |title=USA Pro Wrestling 2nd Annual Big Dick Dudley Memorial Show |author=Kreikenbohm, Philip |work=Events |publisher=CageMatch.de |language=German}}</ref><ref name="Nason">{{cite web |url=http://www.411mania.com/wrestling/news/19642 |title=411 Indies Update: Lynn/Homicide, BDD Memorial, NJ FanFest, Corino/Funk, Tons More |author=Nason, Josh |date=June 2, 2003 |work=News |publisher=411mania.com}}</ref><ref name="Martinez">{{cite web |url=http://www.pwinsider.com/article/31303/this-day-in-history-over-the-edge-do-or-die-colon-brothers-both-win-gold-dudley-memorial-and-more.html?p=1 |title=This Day In History: Over The Edge, Do Or Die, Colon Brothers Both Win Gold, Dudley Memorial And More |author=Martinez, Ryan |date=May 31, 2006 |publisher=PWInsider.com}}</ref>

|match2=Chris Caliber defeated Chewy, Javi-Air and Cody Surekill
|stip2=[[Professional wrestling match types#Basic elimination matches|Four-Way Elimination match]]
|time2=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match3=[[Monsta Mack]] defeated [[Prince Nana]]
|stip3=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time3=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match4=Grim Reefer (c) defeated Deranged
|stip4=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the USA Pro Xtreme Championship
|time4=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match5=Tony Lo defeated The Masked Maniac
|stip5=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time5=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match6=Dan Barry defeated Azrieal and Smoked Out
|stip6=[[Professional wrestling match types#Basic elimination matches|Three-Way Dance]]
|time6=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match7=James Newblood (with Mr. Big) defeated Johnny TNT
|stip7=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time7=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match8=The Boogie Knights (Danny Drake and [[Mike Tobin]]) defeated [[The Christopher Street Connection]] (Buff-E and Mace Mendoza)
|stip8=[[Tag team#Tag team match rules|Tag Team match]]
|time8=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match9=[[The S.A.T.]] (Joel Maximo and Jose Maximo) defeated The Flock ([[Mikey Whipwreck]] and Wayne)
|stip9=[[Tables, Ladders, and Chairs match]]
|time9=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match10=Louie Ramos defeated Johnny Bravado
|stip10=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time10=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match11=[[Tim Roberts|Tim Arson]] won the second-annual Big Dick Dudley Memorial battle royal<ref group=Note>The other participants included were: Adolph, Bad Boy KRACKA, Chris Caliber, Chewy, Danny Demanto, Danny Drake, Pat Gunner, Jim Hustler, J-Kronik, Javi-Air, Tony Lo, Kevin Matthews, [[Prince Nana]], Poppalicious, Psycho, Rayza, [[Norman Smiley]], Smoked Out, Vinny Stylin', Cody Surekill, Johnny TNT, [[Mike Tobin]] and Danny Yamz.</ref>
|stip11=24-man [[Battle royal (professional wrestling)|Big Dick Dudley Memorial Battle Royal]]
|time11=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match12=Skinhead Ivan (c) defeated [[Tim Roberts|Tim Arson]] (c)
|stip12=Non-title "Champion vs. Champion" match featuring the USA Pro New York State and USA Pro America's Heavyweight Champions, respectively.
|time12=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match13=[[Pat Kenney|Simon Diamond]] and [[Matt Striker]] (with [[Becky Bayless]] and [[Veronica Stevens|Simply Luscious]]) defeated The Dirty Rotten Scoundrelz (K.C. Blade and E.C. Negro) (c)
|stip13=[[Tag team#Tag team match rules|Tag Team match]] for the USA Pro Tag Team Championship
|time13=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match14=[[Billy Reil]] defeated Josh Deeley
|stip14=[[Professional wrestling match types#Variations of singles matches|Singles match]]
|time14=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match15=[[Mike Kruel]] (c) defeated [[Xavier (wrestler)|Xavier]]
|stip15=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the USA Pro United States Championship
|time15=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/>

|match16=[[Raven (wrestler)|Raven]] defeated [[Balls Mahoney]] (c)
|stip16=[[Professional wrestling match types#Variations of singles matches|Singles match]] for the USA Pro Heavyweight Championship
|time16=<ref name="OWW"/><ref name="NYProWrestling2"/><ref name="CageMatch"/><ref name="Nason"/><ref name="Martinez"/><ref>{{cite web |url=http://www.solie.org/titlehistories/whtuscw.html |title=USCW World Heavyweight Title History |year=2003 |work=Solie's Title Histories |publisher=Solie.org}}</ref>
}}

<references group=Note/>

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20030421015913/http://www.usaprowrestling.com/results060802.htm USA Pro Big Dick Dudley Memorial Show (6/8/02) results at USAProWrestling.com]
*[https://web.archive.org/web/20030813054248/http://www.usaprowrestling.com/053103results.htm USA Pro 2nd Annual Big Dick Dudley Memorial Event (5/31/03) results at USAProWrestling.com]

[[Category:Professional wrestling memorial shows]]
[[Category:2002 in professional wrestling]]
[[Category:2003 in professional wrestling]]