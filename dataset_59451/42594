<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=CLA.4
 | image=Cranwell CLA.4A AB Avn Msm EDM 16.04.08R edited-2.jpg
 | caption=The third CLA.4 exhibited after restoration at the [[Alberta Aviation Museum]] in Edmonton
}}{{Infobox Aircraft Type
 | type=Two-seat sports aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=Cranwell Light Aeroplane Club
 | designer=Nicholas Comper
 | first flight=August 1926
 | introduced=
 | retired=1934
 | status=One example preserved in museum
 | primary user=Aero clubs
 | number built=3
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Cranwell CLA.4''' was single-engined two-seat inverted [[sesquiplane]] designed and constructed for the [[Lympne light aircraft trials|1926 Lympne trials]] by an amateur group from [[RAF College Cranwell]].  Two were entered, though engine problems prevented one from taking part; the other was eliminated with a broken undercarriage.  A third aircraft was amateur built in Canada and flew until 1934.

==Design and development==
The Cranwell Light Aeroplane (CLA) club was formed in 1923 by staff and students at the RAF College Cranwell. The students came from No.4 Apprentices Wing and one of their lecturers, [[Nicholas Comper|Flt-Lt Nicholas Comper]] became chief designer of the three aircraft produced by the club as well as one, the CLA.1 that was not completed.<ref>{{Harvnb|Ord-Hume|2000|pages=310–2}}</ref>    The last of the series, the CLA.4 was designed to compete in the 1926 Lympne Light Aeroplane Trials.<ref name="OrdH">{{Harvnb|Ord-Hume|2000|pages=312–3}}</ref><ref name="AJJ">{{Harvnb|Jackson|1973|pages=296}}</ref>   Two were built for this competition, one to be powered by a [[Bristol Cherub]] engine and the other by the new [[Pobjoy P]].  Unfortunately, the latter engine failed its own trials not long before the Lympne event and only the Cherub powered aircraft took part.<ref name="OrdH"/><ref name="AJJ"/>  Since the CL.4 had been designed for the 65&nbsp;hp (48&nbsp;kW)  Pobjoy, the 36&nbsp;hp (27&nbsp;kW) Cherub left it seriously underpowered.

Many sesquiplanes have flown, the great majority of types having a smaller lower wing than upper.  The CLA.4, unusually, was an [[wing configuration#Number and position of main-planes|inverted sesquiplane]] with a smaller upper wing, joining a small group of aircraft like the [[Fiat CR.1]] of 1924 and the later [[Caproni Ca.100]], [[Caproni Ca.164]] and [[Levasseur PL.15]], all military machines.  There is a small aerodynamic penalty for this arrangement, but the advantages for a club machine are good vision, and ease of escape, from both cockpits.<ref name="OrdH"/><ref name="FlightA">[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200624.html ''Flight'' 2 September 1926  p.548]</ref>  In the case of the CLA.4, the upper wing had a span of 80% of the lower and 83% of its chord.<ref name="FlightA"/>  The wings were straight, unswept and of constant chord apart from at the rounded tips, with ailerons on the lower wings only. The CLA.4 was a single bay biplane with inward leaning single wide chord interplane struts with wide, faired roots.  Two pairs of centre section struts held the upper wing well clear of the fuselage; the absence of stagger made wing-folding easy.<ref name="OrdH"/>  Like much of the rest of the aircraft, the wings were of fabric covered wood.

The fuselage was built up on four longerons, spruce at the rear where they were linked into a tapering [[Warren girder]], and ash ahead of the rear cockpit where more traditional wire braced rectangular forms were used.<ref name="FlightA"/>  It was topped with a standard rounded decking.  The two open cockpits were placed at the leading and trailing  upper wing edges and fitted with dual controls.<ref name="FlightA"/>  The  flat twin Bristol Cherub III was mounted on a steel plate and smoothly partially cowled to a neatly pointed nose, but with the cylinder heads exposed for cooling.<ref name="FlightA"/>  At the rear of the fuselage the fin and tailplane were fabric covered metal structures, though the generous control surfaces were wood framed.<ref name="FlightA"/>  Fin and rudder together formed a shape not unlike that of many de Havilland aircraft though more rounded and unbalanced.  The main undercarriage was built from a pair of steel V-shaped tubes bearing a single axle and rubber cord shock absorbers.<ref name="FlightA"/>
[[File:Cranwell4.jpg|thumb|right|The first CLA.4]]

==Operational history==

Two CLA.4s were built for the [[Lympne light aircraft trials|Lympne trials]], the Cherub powered ''G-EBPB'', entry no.12 and the Pobjoy powered ''G-EBPC''. no.11.<ref name="AJJ"/><ref name="FlightB">[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200653.html ''Flight'' 9 September 1926  p.577]</ref>  The latter was withdrawn because of the Pobjoy problems and was one of three withdrawals before the start on 10 September, leaving 13 competitors.<ref name="FlightC">[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200674.html ''Flight'' 18 September 1926 p.528]</ref>  On the following day no.12, flown by Nick Comper<ref name="AJJ"/> suffered an undercarriage failure and was refused permission to replace it, forcing retirement.<ref name="FlightC"/> Comper went on to fly the aircraft at the Bournemouth meeting in 1927<ref name="AJJ"/> and in the King's Cup<ref name="OrdH"/> of the same year, then at Blackpool and Orly meetings in 1928<ref name="AJJ"/> without great success.  Its Cherub, normally a reliable engine (the top four aircraft at Lympne 1926 had used it) seems to have been a poor sample.<ref name="OrdH"/> The aircraft was sold into private ownership and flew until scrapped in 1933.<ref name="AJJ"/>

The second Cranwell built CLA.4, ''G-EBPC'' was also fitted with a Cherub but was destroyed in a crash in March 1927.<ref name="AJJ"/>  At least one other CLA.4 was built, but not at Cranwell.  Plans were sold to the Alberta Aero Club (now the Edmonton Flying Club) of Edmonton, Canada, which intended to build it as the Club's first aircraft.  This project was not completed and the plans, incomplete airframe, two engines and parts were sold to Alf Want of Edmonton, who had done the most work on the project. He built it at his home, fitting a [[Blackburne Thrush]].{{Citation needed|date=May 2011|reason=need Trush ref - see discussion}}   Dates are uncertain, but it seems to have flown from 1931 or 1932 until a crash due to icing near what is now the City Centre Airport in February 1934,<ref>[http://www.virtualmuseum.ca/PM.cgi?LM=Gallery&LANG=English&AP=vmc_display_static&DB=human&KEY=AACU2003-077-001 Note on Canadian CLA.4 history]</ref> making it the last CLA.4 to fly.  During this period it was re-engined with a more powerful 55&nbsp;hp  [[Viele M-5]] engine to improve its performance, perhaps to cope with the higher operating altitude of Edmonton.

In 1989 Want presented the remains to the [[Alberta Aviation Museum|Alberta Aviation Museum, Edmonton]], where it is being restored to flying condition. In 2001 it was displayed in a half-covered state so its structure could be seen and appreciated<ref>[http://albertaaviationmuseum.com/index.php?option=com_content&task=view&id=34&Itemid=41 Alberta Museum CLA.4]</ref> but by 2008 was fully covered aft of the engine firewall.  Its original Blackburne Thrush is displayed beside it and the other Thrush is now in the Reynolds-Alberta Museum collection. The aircraft has never been registered with the Canadian authorities.

==Naming==
At least some contemporary sources like ''Flight'' refer to both the Cranwell built aircraft by the number 4 (or sometimes IV), with no letter suffix.<ref name="FlightA"/><ref name="FlightB"/><ref name="FlightC"/>  The [[Civil Aviation Authority]] first registration documents<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=EBPB CAA refisttartion documents for ''G-EBPB'']</ref><ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=EBPc CAA refisttartion documents for ''G-EBPC'']</ref> refer to the Cherub powered ''G-EBPB'' as a CLA.4A and the other machine as a CLA.4, and later authors<ref name="OrdH"/><ref name="AJJ"/> use this engine based distinction.  They generally agree that ''G-EBPB'' is a CLA.4A; but some<ref name="AJJ"/> imply ''G-EBPB'' became a CLA.4A when fitted with the Cherub with which it always flew, whereas others<ref name="OrdH"/> continue to use the original CLA.4 name.

<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref={{harvnb|Jackson|1973|p=296}} 
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft=22
|length in=3.5
|span m=
|span ft=27
|span in=4
|span note=overall
|lower span ft=27
|lower span in=4
|upper span m=
|upper span ft=22
|upper span in=0
|height m=
|height ft=
|height in=
|wing area sqm=
|wing area note=<ref name="FlightA"/>
|wing area sqft=164
|airfoil=modified RAF31<ref name="OrdH"/>
|empty weight kg=
|empty weight lb=480
|gross weight kg=
|gross weight lb=874
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Bristol Cherub]] III
|eng1 type=flat twin piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|power original=
|thrust original=
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=65
|max speed kts=
|max speed note=<ref name="OrdH"/>
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|cruise speed kts=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|range km=
|range miles=
|range nmi=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|lift to drag=
|wing loading=
|disk loading=
|more performance=

|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Citations===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book |title= British Civil Aircraft 1919-72|last=Jackson|first=A.J.| year=1973|volume= 2|publisher=Putnam Publishing |location=London |isbn=0-85177-813-5|ref=harv}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6|ref=harv}}
*[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200624.html ''Flight'' 2 September 1926 THE CRANWELL LIGHT AEROPLANE IV An Interesting Training Machine Built by Amateurs]
{{refend}}
<!-- ==External links== -->
{{Comper aircraft}}

[[Category:British sport aircraft 1920–1929]]
[[Category:Sesquiplanes]]