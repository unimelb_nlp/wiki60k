{{Infobox military person
|honorific_prefix  =
|name              =Archie J. Old, Jr.
|honorific_suffix  =
|native_name       =
|native_name_lang  =
|image         =Old Archie Jr06.jpg
|image_size    =180
|alt           =
|caption       =Lieutenant General Archie J. Old, Jr.
|birth_date    ={{birth date|1906|08|01}}
|death_date    ={{Death date and age|1984|03|24|1906|08|01}}
|birth_place   =[[Farmersville, Texas]]
|death_place   =[[March AFB, California]]
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =General Old
|birth_name    =
|allegiance    ={{flag|United States|23px}}
|branch        ={{air force|United States|23px}}
|serviceyears  =1930–1965
|rank          =[[Lieutenant general (United States)|Lieutenant General]]
|servicenumber = <!--Do not use data from primary sources such as service records.-->
|unit          =
|commands      =
|battles       =[[World War II]]
|battles_label =
|awards        =[[Distinguished Service Cross (United States)|Distinguished Service Cross]]<br>[[Silver Star]] with oak leaf cluster<br>[[Legion of Merit]]<br>[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] with four oak leaf clusters<br>[[Purple Heart]]<br>[[Air Medal]] with eight oak leaf clusters
|spouse        = <!-- Add spouse if reliably sourced -->
|relations     =
|laterwork     =
|signature     =
|website       = <!-- {{URL|example.com}} -->
}}
'''Archie J. Old, Jr.''' (August 1, 1906 – March 24, 1984) was a [[General officer|General]] during World War II and a command Air Force pilot.<ref name="bio1">{{cite web|url=http://www.af.mil/bios/bio.asp?bioID=6650 |title=Lieutenant General Archie J. Old Jr. biography |accessdate=2008-06-18 |archiveurl=https://web.archive.org/web/20080613032337/http://www.af.mil/bios/bio.asp?bioID=6650 |archivedate=2008-06-13 |deadurl=no |df= }}</ref><ref name="bio2">{{cite web|url=http://www.477fg.afrc.af.mil/bios/bio_print.asp?bioID=6650&page=1 |title=Lieutenant General Archie J. Old Jr. |accessdate=2008-06-18 |archiveurl=https://web.archive.org/web/20080628042925/http://www.477fg.afrc.af.mil/bios/bio_print.asp?bioID=6650&page=1 |archivedate=2008-06-28 |deadurl=yes |df= }}</ref>

==Early life==
Old was born at [[Farmersville, Texas]] in 1906. Here he grew up and went to the normal childhood schools and graduated from high school. Old then studied [[civil engineering]] at [[Trinity University (Texas)|Trinity University]] and at the [[University of Texas]].<ref name="bio2"/>

Old enlisted as a private in the [[Texas Air National Guard|Texas National Guard]] on April 16, 1930.<ref name="bio2"/> He then attended aviation cadet training in the [[United States Army Air Corps]]. Appointed a [[flying cadet]] in February 1931, he completed his flying training at Brooks and [[Kelly Field]]s in Texas.<ref name="bio2"/>

Old was commissioned a [[second lieutenant#United States|second lieutenant]] in the Air Reserve on February 26, 1932.<ref name="bio2"/> He then was assigned to active duty with the 13th Attack Squadron at [[Fort Crockett]], Texas. There he served until February 1933.<ref name="bio2"/> Old for short periods during the following seven years was on active duty as a reserve officer.

==Military career==
On September 6, 1940 Old was ordered to extended active duty. At that time, he was assigned to the 52d Bombardment Squadron at [[MacDill Field]] in Florida. There he was an assistant armament and chemical officer.<ref name="bio2"/>

In the following February he became operations officer of the 29th Bombardment Group. The group later moved to Gowen Field, Idaho, where he followed with them. Old assumed command of the 96th Bomb Group at Walla Walla, Washington, in September 1942. In January 1943 he moved his group to the European theater. In December 1943 Old became chief of the 45th Combat Bomber Wing. On August 14, 1943, (Black Thursday) Col Old led the raid on the Schweinfurt ball bearing factories in the Fertile Myrtle III.  It is considered to be the largest aerial battle to have ever occurred. 376 aircraft (B-17s) dispatched, 291 made it over target, 60 failed to return. 5 crashed in England on landing, 12 that did land were so battle damaged that they were scrapped, never to fly again. Total loss, over 600 Flight Crew and 77 aircraft.<ref name="bio2"/>

Old returned to the United States in July 1945. There he was assigned to the Army Air Forces headquarters at Washington, D.C. In August he was transferred to Air Transport Command headquarters at [[Ronald Reagan Washington National Airport|Gravelly Point]], Virginia. Later in December of the same year he became commanding general of the Southwest Pacific Wing of Air Transport Command.<ref name="air1">[http://home.star.brisnet.org.au/~dunn/usaaf/usatc.htm United States air transport command in Australia during WW2]</ref> In January 1946 he assumed additional duty as commanding general of the China Wing.<ref name="bio2"/><ref name="air1"/>

On June 1, 1948, Old became commanding general of the 530th Air Transport Wing of Military Transport Service at [[Fairfield-Suisun Air Force Base]] in California. In July of that year he was named commander of the Atlantic Division of [[Military Air Transport Service|MATS]].<ref name="bio2"/>

Old became acting commander of the Eighth Air Force at Carswell Air Force Base, Texas, in March 1950.<ref name="bio2"/> In 1951 Old got two of SAC's important overseas jobs of commanding the 7th Air Division in England and the 5th Air Division in French Morocco.<ref name="bio2"/> In February 1953 Old was assigned as director of operations for the Strategic Air Command.<ref name="bio2"/> On August 22, 1955, Old assumed command of the Fifteenth Air Force.<ref name="bio2"/> Old retired September 1, 1965. He died March 24, 1984 at the base hospital at [[March Air Force Base]].<ref name='sun-news'>{{cite journal
 |last=Washington Post
 |title=Deaths Elsewhere
 |journal=Washington Post
 |location=Washington, D.C. 
 |date=March 30, 1984
 |page=B–16}}</ref>

==World War II==
[[File:Sac hist 005 x.jpg|thumb|Far left is Lt. Gen. '''Archie J. Old''' at [[Offutt Air Force Base]], Nebraska]]
[[File:Sac hist 018 x.jpg|thumb|A Strategic Air Command B-52 heavy bomber is refueled by a KC-135 Stratotanker]]
During World War II Old flew 43 combat missions against Germany. He led the October 14, 1943 raid against a ball-bearing works at [[Schweinfurt]], Germany.<ref name="bio1"/> He was also the general commander of the 45th Combat Bomber Wing of the Eighth Air Force.<ref name="bio1"/> Old flew the first shuttle bomb run from England to Russia in June 1944 and received the United States Army [[Distinguished Service Cross (United States)|Distinguished Service Cross]].<ref name="bio1"/>

In response to headquarters directives about his flying combat missions Old is recorded as saying, ''Every mission I make means that I'm that much more competent to advise the boys concerning their jobs. I'll go with them anywhere - Berlin, the Ruhr, and, more important, most of us will get back.''<ref name="bio1"/>

===Commands held===
* 1941 - 1942 Operations Officer, 29th Bombardment Group (Heavy), [[MacDill Field]], Florida; from 25.06.1942, Gowen Field, Idaho; Commanding Officer of the 29th Bombardment Group (Heavy), Gowen Field, Idaho
* 1943 - 1944 Commanding Officer of the 96th Bombardment Group (Heavy), Pocatello, Idaho; from 00.01.43-00.03.1943, [[Pyote Army Air Base]], Texas; from 00.05.1943, Great Saling, England; from 12.06.1943, Snetterton Heath, England; Commanding Officer of the 45th Combat Bombardment Wing (Heavy), Snetterton Heath, England; Chief of Staff of the 45th Combat Bombardment Wing (Heavy), Snetterton Heath, England
* 1944 - 1945 Commanding Officer, later Commanding General of the 45th Combat Bombardment Wing (Heavy), Snetterton Heath, England
* 1945 - 1946 Commanding General of the 20th Combat Bombardment Wing (Heavy), Snetterton Heath, England; Commanding General of the Southwest Pacific Wing, Pacific Division, Air Transport Command, Manila, Philippines
* 1946 - 1948 Commanding General of the Philippine Base Wing, Pacific Division, Air Transport Command, Manila, Philippines; Commanding General of the West Coast Wing, Pacific Division, Air Transport Command, [[Fairfield-Suisun Field]], California; Deputy Commanding General of the Pacific Division, Air Transport Command; Commanding General of the Eastern Pacific Wing, Pacific Division, Air Transport Command
* 1948 - 1950 Commanding General of the Atlantic Division, Air Transport Service, Westover AFB, Massachusetts
* 1950 - 1951 Deputy Commander of the Eighth Air Force, [[Carswell AFB]], Texas; Acting Commanding General of the Eighth Air Force, [[Carswell AFB]], Texas; Temporary duty as Commanding General of the 7th Air Division, South Ruislip, England; Temporary duty as Commanding General of the 5th Air Division, Rabat, French Morocco
* 1951 - 1953 Commanding General of the 5th Air Division, Rabat, French Morocco
* 1953 - 1955 Director of Operations, Strategic Air Command, [[Offutt AFB]], Nebraska
* 1955 - 1965 Commander of the Fifteenth Air Force, [[March AFB]], California
* 1965 Retired

==Other achievements==
[[File:Sac hist 008 x.jpg|thumb|300 px|First round-the-world nonstop flight by a jet airplane.]]
[[File:B52 Data plate.jpg|thumb|Data plate off of a B52 that has history]]

Old was the leading commander of the first non-stop round-the-world jet flight.<ref name="bio1"/><ref name="bio2"/><ref name="speed1"/><ref>{{cite web|title=World News 1956-62 |url=http://www.geocities.com/Pentagon/Bunker/2260/worldnew.html |work= |archiveurl=http://www.webcitation.org/5kmzORzXy?url=http%3A%2F%2Fwww.geocities.com%2FPentagon%2FBunker%2F2260%2Fworldnew.html |archivedate=2009-10-25 |deadurl=yes |df= }}</ref> This was done with three heavy bombers in January 1957.<ref name="fiveseven">{{cite web|url= http://www.mrhclass1957.com/news1957.html|title= Events of 1957|accessdate= 2008-06-18| archiveurl= https://web.archive.org/web/20080628060824/http://www.mrhclass1957.com/news1957.html| archivedate= 28 June 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="wild">{{cite book|url= https://books.google.com/books?id=66j8ZZg4L-oC&pg=RA1-PA384&lpg=RA1-PA384&dq=%22Archie+J.+Old+Jr%22+1957+First+round-the-world+nonstop+flight+by+a+jet+airplane|title= Beyond the wild blue: A History of the U.S. Air Force, 1947-1997|accessdate= 2008-06-18 | first=Walter J. | last=Boyne | isbn=978-0-312-18705-7 | year=1998 | publisher=St. Martin's Press}}</ref><ref name="firstfacts">Anzovin, p. 31, item # 1384</ref> The [[jet aircraft]] were refueled in flight by [[Aerial refueling|aerial tankers]].<ref name="speed1"/><ref name="firstfacts"/> Each eight engine jet carried a crew of nine.<ref name="firstfacts"/> The route was via [[Newfoundland and Labrador|Newfoundland]], [[French Morocco]], [[Saudi Arabia]], [[India]], [[Ceylon]], [[Philippines]], and [[Guam]].<ref name="firstfacts"/> This historic project was given to Fifteenth Air Force by the Strategic Air Command headquarters. It was known as [[Operation Power Flite]].<ref name="bio1"/><ref name="firstfacts"/><ref name="Jan16">{{cite web|url= http://www.centennialofflight.gov/user/fact_jan.htm|title= Aviation History Facts|accessdate= 2008-06-18| archiveurl= https://web.archive.org/web/20080628014959/http://www.centennialofflight.gov/user/fact_jan.htm| archivedate= 28 June 2008 <!--DASHBot-->| deadurl= no}}</ref>

General Old flew the lead jet airplane out of the 93d Bombardment Wing at [[Castle Air Force Base]] in California.<ref name="firstfacts"/> Old took off from there on January 16, 1957.<ref name="firstfacts"/> He circled the globe non-stop in a [[Boeing B-52 Stratofortress]] jet and completed the trip on January 18.<ref name="bio1"/><ref name="bio2"/> He landed at [[March Air Force Base]] in California some 45 hours and 19 minutes later.<ref name="bio1"/><ref name="bio2"/><ref name="speed1"/><ref name="firstfacts"/> The total distance was 24,325 miles.<ref name="bio2"/><ref name="firstfacts"/> The average speed was 525 miles per hour.<ref name="speed1">{{cite web|url= http://www.infoplease.com/ipa/A0004537.html|title= Famous Firsts in Aviation|accessdate= 2008-06-18| archiveurl= https://web.archive.org/web/20080524151512/http://www.infoplease.com/ipa/A0004537.html| archivedate= 24 May 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="firstfacts"/>

Old told reporters that the flight was ''merely a routine SAC mission. The planning and preparation that went into the aerial circumnavigation were identical to what was demanded every day in the Fifteenth Air Force and the Strategic Air Command.''<ref name="bio1"/><ref name="bio2"/>

==Military awards==
*[[File:Distinguished Service Cross ribbon.svg|60px]] &nbsp;&nbsp; [[Distinguished Service Cross (United States)|Distinguished Service Cross]]
*[[File:Silver Star ribbon.svg|60px]] &nbsp;&nbsp; [[Silver Star]] with oak leaf cluster
*[[File:Legion of Merit ribbon.svg|64px]]&nbsp;&nbsp; [[Legion of Merit]]
*[[File:Distinguished Flying Cross ribbon.svg|60 px]]&nbsp;&nbsp; [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] with four oak leaf clusters
*[[File:Purple Heart ribbon.svg|67px]]&nbsp;&nbsp; [[Purple Heart]]
*[[File:Air Medal ribbon.svg|60px]] &nbsp;&nbsp; [[Air Medal]] with eight oak leaf clusters

==Foreign decorations==
*[[French Legion of Honor]]
*[[Croix de guerre (Belgium)|Belgian Croix de guerre with Palm]]
*[[Order of Suvorov|Russian Order of Suvorov]]
*[[Croix de guerre 1939–1945 (France)|French Croix de Guerre with Palm]]
*[[Distinguished Flying Cross (United Kingdom)|British Distinguished Flying Cross]]
*French Moroccan [[Order of Ouissam Alaouite]]

==Television appearance==

General Old appeared, playing himself, in "Massacre," a 1966 episode of the television show ''[[Twelve O'Clock High (TV series)|Twelve O'Clock High]]''.<ref>http://www.imdb.com/name/nm2069731/?ref_=fn_al_nm_1</ref>

==References==
* Anzovin, Steven, ''[[Famous First Facts]]'', [[H. W. Wilson Company]], New York 2000, ISBN 0-8242-0958-3
*{{USGovernment|sourceURL=<ref>{{cite web|url=http://www.af.mil/bios/bio.asp?bioID=6650 |title=Biographies : Lieutenant General Archie J. Old Jr |accessdate=2008-06-24 |archiveurl=https://web.archive.org/web/20080613032337/http://www.af.mil/bios/bio.asp?bioID=6650 |archivedate=2008-06-13 |deadurl=no |df= }}</ref>}}
*{{USGovernment|sourceURL=<ref>{{cite web|url=http://www.477fg.afrc.af.mil/bios/bio_print.asp?bioID=6650&page=1 |title=Lieutenant General Archie J. Old Jr |accessdate=2008-06-24 |archiveurl=https://web.archive.org/web/20080628042925/http://www.477fg.afrc.af.mil/bios/bio_print.asp?bioID=6650&page=1 |archivedate=28 June 2008 |deadurl=yes |df= }} </ref>}}

==Notes==
{{Portal|Biography|United States Air Force}}
{{Reflist}}

{{DEFAULTSORT:Old, Archie J. Jr}}
[[Category:United States Air Force generals]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Legion of Merit]]
[[Category:American military personnel of World War II]]
[[Category:People from Farmersville, Texas]]
[[Category:United States Air Force officers]]
[[Category:United States Air Force airmen]]
[[Category:1906 births]]
[[Category:1984 deaths]]
[[Category:Recipients of the Silver Star]]
[[Category:Recipients of the Distinguished Service Cross (United States)]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]