{{Good article}}
{{Infobox University Boat Race
| name= 112th Boat Race
| winner = Oxford
| margin = 3 and 3/4 lengths
| winning_time= 19 minutes 12 seconds
| date= {{Start date|1966|03|26|df=y}}
| umpire = Alan Burrough<br>(Cambridge)
| prevseason= [[The Boat Race 1965|1965]]
| nextseason= [[The Boat Race 1967|1967]]
| overall =61&ndash;50
| reserve_winner =Isis
| women_winner =Cambridge
}}
The 112th [[The Boat Race|Boat Race]] took place on 26 March 1966.  Held annually, the event is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  The race was won by Oxford by three-and-three-quarter-lengths.  Isis won the reserve race while Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 24 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 24 August 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 20 August 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities, followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=24 August 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 24 August 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1965|previous year's race]] by three-and-three-quarter lengths.  Cambridge, however, held the overall lead with 61 victories to Oxford's 49 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 12 October 2014}}</ref><ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 24 August 2014}}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

Two days before the main race and in inclement weather, the Cambridge boat began to sink and was pushed into barges and tugs moored below [[Beverley Brook]].  The crew were rescued and according to their boat club president Mike Sweeney, the incident would have no impact on the Light Blues: "we shall just get into our other boat and race in that".<ref name=squall>{{Cite news | title = Squall sinks Cambridge crew | date = 25 March 1966 | page = 12 | issue = 56589 | work = [[The Times]]}}</ref>  It was the first Boat Race vessel to sink since the [[The Boat Race 1951|1951 race]].<ref name=squall/>  The Light Blues would row in the same boat in which they won the [[The Boat Race 1962|1962]] and [[The Boat Race 1964|1964 races]],<ref name=today/> while Oxford's craft was manufactured by Swiss firm [[Stämpfli Racing Boats]].<ref name=blunder/>  Both boats were German-rigged, where the number four and five row on the bow side.<ref>Dodd, p. 12</ref>  The inclement weather continued until the day of the race, with further disruption predicted and the threat of postponement a real one.<ref name=today>{{Cite news | title = Oxford favourites to win &ndash; today or tomorrow | first = Stanley | last = Baker | work = [[The Guardian]] | page = 11 | date= 26 March 1966}}</ref><ref>{{Cite news | work = [[The Guardian]] | title = Boat Race may be rowed tomorrow | date = 26 March 1966 | page = 1}}</ref>  The race was umpired by the former [[Cambridge University Boat Club]] president and rower Alan Burrough who took part in Cambridge's victory in the [[The Boat Race 1939|1939 race]].<ref>Burrell, p. 49</ref><ref>{{Cite web | url = https://www.theguardian.com/news/2002/aug/13/guardianobituaries1 | work = [[The Guardian]] | title = Alan Burrough | first = Christopher | last = Dodd | date = 13 August 2002 | accessdate = 27 September 2014}}</ref>

The Cambridge crew were coached by D. C. Christie (who rowed for Cambridge in the [[The Boat Race 1958|1958]] and [[The Boat Race 1959|1959]] races), J. G. P. Crowden (who won Blues in the [[The Boat Race 1951|1951]] and the [[The Boat Race 1952|1952 races]]), D. M. Jennens (who rowed three times between 1949 and 1951) and I. W. Welsh (who participated in the [[The Boat Race 1956|1956 race]]).<ref>Burnell, pp. 104&ndash;109</ref>  Oxford's coach was Ronnie Howard who had rowed for the Dark Blues in the [[The Boat Race 1957|1957]] and 1959 races.<ref>Burnell, p. 99</ref><ref>Burnell, 110&ndash;111</ref>

==Crews==
The Cambridge crew weighed an average of 13&nbsp;[[Stone (unit)|st]] 7&nbsp;[[Pound (mass)|lb]] (85.5&nbsp;kg), {{convert|1.6|lb|kg|1}} per rower more than their opponents.<ref name=burn81>Burnell, p. 81</ref>  Oxonians Chris and Richard Freeman became the first brothers to row in the Boat Race since [[The Boat Race 1935|1935 race]].<ref name=start>{{Cite news | work = [[Financial Times]] | title = Oxford start favourites| first = Charles | last = Dimont | issue=23886 | page = 7 | date = 26 March 1966}}</ref>  The Oxford crew contained a single former Blue, the boat club president and number two Duncan Clegg, while Cambridge saw Rodney Ward and stroke Mike Sweeeny return.<ref name=happens>{{cite news | title = Everything happens to Oxford Crew | work = [[The Times]] | issue = 56587 | date = 23 March 1966 | page = 4 }}</ref> Oxford's P. G. Tuke was following in the footsteps of his great grandfather F. E. Tuke who had rowed in the [[The Boat Race 1845|1845 race]].<ref>Burnell, p. 41</ref>  There were three non-British participants in the race, the Oxford [[Coxswain (rowing)|cox]] [[Jim Rogers]], and Cambridge rowers J. H. Ashby and P. H. Conze were all American.<ref>Burnell, p. 39</ref>

[[File:Jim-rogers-madrid-160610.jpg|right|thumb|[[Jim Rogers]] was Oxford's cox for the race.]]
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] ||R. A. D. Freeman || [[Magdalen College, Oxford|Magdalen]] || 13 st 0 lb || M. E. K. Graham || [[Lady Margaret Boat Club]] || 13 st 7 lb
|-
| 2 || R. D. Clegg (P) || [[St Edmund Hall, Oxford|St Edmund Hall]] || 13 st 3 lb || M. D. Tebay || [[Trinity College, Cambridge|1st & 3rd Trinity]] || 13 st 4 lb
|-
| 3 || F. C. Carr || [[Keble College, Oxford|Keble]] || 13 st 4 lb || J. H. Ashby || [[Trinity College, Cambridge|1st & 3rd Trinity]] || 13 st 2 lb
|-
| 4 || C. H. Freeman {{double-dagger}} || [[Keble College, Oxford|Keble]] || 14 st 3 lb || [[Patrick Delafield|P. G. R. Delafield]] || [[Jesus College, Cambridge|Jesus]] || 14 st 8 lb
|-
| 5 || J. K. Mullard || [[Keble College, Oxford|Keble]] || 13 st 7 lb || R. G. Ward || [[Queens' College, Cambridge|Queens']] || 14 st 12 lb
|-
| 6 || P. G. Tuke || [[Keble College, Oxford|Keble]] || 13 st 11 lb || P. H. Conze || [[Trinity College, Cambridge|1st & 3rd Trinity]]  || 12 st 10 lb
|-
| 7 || E. C. Meyer || [[University College, Oxford|University]] || 13 st 4 lb || L. M. Henderson || [[Selwyn College, Cambridge|Selwyn]]  || 13 st 6 lb
|-
| [[Stroke (rowing)|Stroke]] || M. S. Kennard  || [[St Edmund Hall, Oxford|St Edmund Hall]] || 12 st 11 lb || M. A. Sweeney (P) || [[Lady Margaret Boat Club]] || 12 st 10 lb
|-
| [[Coxswain (rowing)|Cox]] || [[Jim Rogers|J. B. Rogers jr.]] || [[Balliol College, Oxford|Balliol]] || 9 st 1 lb || I. A. B. Brooksby||  [[Lady Margaret Boat Club]] || 9 st 0 lb
|-
!colspan="7"|Source:<ref name=dodd342>Dodd, p. 342</ref><br>(P) &ndash; Boat club president<ref name=blunder>{{Cite news | title = Boat Race blues as Oxford blunder | first = Donald | last= Legget | work = [[The Observer]] | date = 20 March 1966 | page = 19}}</ref><ref>Burnell, pp. 51&ndash;52</ref><br>C. H. Freeman replaced C. E. Albert four days before the race.<ref name=happens/>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station.<ref name=burn81/>  Despite the prediction of poor weather and the threat of postponement,<ref name=today/> the race commenced at the planned time of 4.15pm.<ref name=burn81/>  Oxford made the better start and were half-a-length up on Cambridge, but with the advantage of the Middlesex bend, the Light Blues were one second behind at the Mile Post.<ref name=all>{{Cite news | title = Oxford lead all the way | work = [[The Observer]] | date = 27 March 1966 | first = Donald | last = Legget | page = 20}}</ref>  Oxford reacted to a Cambridge push at [[Harrods Furniture Depository]] to maintain the lead which they extended to two seconds by [[Hammersmith Bridge]].<ref name=unable>{{Cite web | title = Cambridge unable to challenge Oxford fully| work = [[The Guardian]] | first = Stanley | last = Baker | page = 18 | date = 28 March 1966}}</ref>  The Dark Blues continued to contain Cambridge's attempts to reduce the deficit and by Chiswick Steps had clear water with a three-second advantage.<ref name=unable/>  Pushing away once again, and with a two-length lead, Oxford's cox Jim Rogers steered them across the Cambridge boat to the Middlesex side, and were ten seconds ahead at [[Barnes Railway Bridge|Barnes Bridge]].<ref name=unable/>  Oxford won by three-and-three-quarter lengths in a time of 19 minutes 12 seconds.<ref name=burn81/>

In the reserve race, and after a false start,<ref name=unable/> Oxford's Isis beat Cambridge's Goldie by seventh lengths, their second consecutive victory, in a time of 19 minutes 22 seconds.<ref name=results/> In the 21st running of the [[Henley Boat Races#Women's Boat Race|Women's Boat Race]], Cambridge triumphed, their fourth consecutive victory.<ref name=results/>

==References==
'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink=Dickie Burnell| year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}

'''Notes'''
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1966}}
[[Category:1966 in English sport]]
[[Category:1966 in rowing]]
[[Category:The Boat Race]]