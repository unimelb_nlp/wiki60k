{{Infobox VG
| title         = Bubbles
| image         = [[File:Bubbles-arcade.jpg|frameless|alt=A tan, vertical rectangular poster. The poster depicts two arcade cabinets side-by-side. The left cabinet is blue and taller than the right one, which is brown. Above the cabinets read the text "Williams" and "Bubbles".]]
| caption       = ''Bubbles'' promotional flyer, showcasing the wooden upright and cocktail cabinets
| developer     = [[Williams Electronics]]
| publisher     = Williams Electronics
| designer      = John Kotlarik <br /> Tim Murphy <br /> [[Python Anghelo]] 
| release       = 1982
| genre         = [[Action game]]
| modes         = Up to 2 players, playing alternately
| cabinet       = Upright, Mini-upright, cocktail-table and Duramold cabinet
| arcade system = 
| display       = Horizontal orientation, [[raster graphics]]
| platforms     = [[Arcade game|Arcade]]
}}

'''''Bubbles''''' is an [[Arcade game|arcade]] [[video game]] developed by [[WMS Industries|Williams Electronics]] and released in 1982. The player uses a joystick to control a bubble in a kitchen sink. The object is to progress through levels by cleaning the sink while avoiding enemies. The game received a mixed reception from critics.

Development was handled by John Kotlarik and [[Python Anghelo]]. Kotlarik wanted to create a [[non-violent game]] inspired by ''[[Pac-Man]]''. Anghelo designed the game's artwork and scenario as well as a special plastic cabinet that saw limited use.  ''Bubbles'' was not ported to any contemporary systems, but was later released as a web-based version and on home consoles as part of arcade compilations.

== Gameplay ==
[[File:Bubbles Game.png|thumb|left|alt=A horizontal rectangular video game screenshot that is an overhead view of a digital representation of a blue sink with a drain in the center. Scattered in the sink are brushes, razors, ants, crumbs, and grease. A small, light blue bubble is located to the right of the drain.|The light blue bubble (center right) avoids brushes and razors to clean up ants, crumbs, and grease.]]

''Bubbles'' is an [[action game]] with [[Puzzle video game|puzzle elements]] where the player controls the protagonist, a soap bubble, from a [[top-down perspective]]. The object is to clean a kitchen sink by maneuvering over ants, crumbs, and grease spots to absorb them before they slide into the drain. As the bubble absorbs more objects, it grows in size, eventually acquiring first eyes and then a smiling mouth. At the same time, sponges and scrub brushes slowly move around the sink, cleaning it on their own in competition with the player. Touching either of these enemies costs a player one life unless the bubble is large enough to have a complete face. In this case, the enemy will be knocked away and the bubble will shrink. Sponges and brushes can be knocked into the drain for bonus points, eliminating them from play. Two other enemies in the sink are stationary razor blades and roaches that crawl out of the drain. Contact with a blade is always fatal, while the bubble can safely touch the roach only while carrying a broom, which will kill the roach with one hit. The broom can be acquired by running over a cleaning lady who appears in the sink from time to time.<ref name="CVG-49">{{cite journal| journal = [[Computer and Video Games]]| title = Arcade Action| first = Clare| last = Edgeley| publisher = [[Emap International Limited]]|date=November 1985| issue = 49| page =80}}</ref><ref name="Retro-60">{{cite magazine|magazine= [[Retro Gamer]]| publisher = [[Imagine Publishing]]| title = Retrorevival: Bubbles| first = Darran| last = Jones| page =96|date=March 2009| issue = 60}}</ref>

A level ends when all of the point-scoring objects are gone - either lost down the drain, cleaned by sponges/brushes, eaten by roaches, or absorbed by the bubble. At this point, if the bubble is large enough to have a complete face, the player moves on to the next level; otherwise, one life is lost and the level must be replayed. In addition, whenever the bubble has a face, the drain flashes green, giving the player a chance to enter it and skip the next level. Entering the drain while the bubble is too small costs one life.<ref name="CVG-49"/><ref name="Retro-60"/>

== Development ==

The game features [[monaural]] sound and [[Raster graphics|pixel graphics]] on a 19&nbsp;inch [[Cathode ray tube|CRT monitor]].<ref name="KLOV">{{cite web| url = http://www.klov.com/game_detail.php?game_id=7226| title = Bubbles - Videogame by Williams (1982)| publisher = [[Killer List of Videogames]]| accessdate = 2009-06-25}}</ref> The initial concept was conceived by John Kotlarik, who aimed to create a non-violent game. Inspired by ''[[Pac-Man]]'', he envisioned similar gameplay in an open playing field rather than in a maze. [[Python Anghelo]] furthered the concept by creating artwork and a scenario. Kotlarik designed the protagonist to have fluid movement like it was traveling on a slick surface. The control scheme allows the digital input to operate similar to an analogue one. He programmed the bubble to accelerate in the direction the joystick is held. Once the joystick returns to its neutral position, the bubble will coast as the velocity slowly decreases. Anghelo designed the artwork for the wooden cabinets as well as a new cylindrical, plastic cabinet. Gary Berge, a mechanical engineer, created the new cabinets with a rotational molding process.<ref name="Arc-Treasure">{{cite video game| title= [[Midway Arcade Treasures]]| developer= [[Backbone Entertainment|Digital Eclipse]]| publisher= [[Midway Games]]| date= 2003-11-18| platform= [[PlayStation 2]]| version= | level= The Inside Story On Bubbles}}</ref>

== Reception and legacy ==

The game received a mixed reception from critics. Author John Sellers listed it among the weirder arcade games released.<ref name="Fever">{{cite book| title = Arcade Fever: The Fan's Guide to The Golden Age of Video Games| first = John| last = Sellers| pages = 88–89| publisher = [[Running Press]]|date=August 2001| isbn = 0-7624-0937-1}}</ref> Clare Edgeley of ''[[Computer and Video Games]]'' echoed similar statements. She criticized the game, stating that the constant blue background was dull and the game lacked longevity.<ref name="CVG-49"/> ''[[Retro Gamer]]''{{'}}s Darran Jones described the game as engrossing and obscure. He also expressed disappointment that few people remember it.<ref name="Retro-60"/> Brett Alan Weiss of [[AllGame]] called ''Bubbles'' a slightly underrated game. He stated that while it lacked excitement, its gameplay was enjoyable. Weiss further commented that the control scheme was unique for its time, and that the number of on-screen objects moving smoothly was impressive.<ref>{{cite web| url = http://www.allgame.com/game.php?id=3138&tab=review| title = Bubbles - Review| publisher = [[AllGame]]| last = Weiss| first = Brett Alan| accessdate = 2009-06-25 |archiveurl=https://web.archive.org/web/20100214145943/http://allgame.com/game.php?id=3138 |archivedate=2010-02-14}}</ref>

The game was later remade for different platforms. In 2000, a web-based version of ''Bubbles'', along with nine other classic arcade games, was published on [[Adobe Shockwave|Shockwave.com]].<ref>{{cite web| url = http://www.gamespot.com/articles/midway-coming-back-at-you/1100-2565653/| title = Midway Coming Back At You| first = Sam| last = Parker| publisher = [[GameSpot]]| date = 2000-05-05| accessdate = 2009-06-25}}</ref> Four years later, [[Midway Games]]<ref group="Note">Williams Electronics purchased Midway in 1988, and later transferred its games to the [[Midway Games]] subsidiary.</ref> also launched a website featuring the Shockwave versions.<ref>{{cite web| url = http://www.gamespot.com/articles/midway-arcade-treasures-web-site-goes-live/1100-6108563/| title = Midway Arcade Treasures Web site goes live| first = Chris| last = Kohler| publisher = [[GameSpot]]| date = 2004-09-24| accessdate = 2009-06-25}}</ref> Williams Electronics included ''Bubbles'' in several of its arcade compilations: the 1996 ''[[Williams Arcade's Greatest Hits]]'', the 2000 ''[[Midway's Greatest Arcade Hits]]'' ([[Dreamcast]] version only), the 2003 ''[[Midway Arcade Treasures]]'', and the 2012 ''[[Midway Arcade Origins]]''.<ref name="Retro-60"/><ref>{{cite web| url = http://www.allgame.com/game.php?id=2356| title = Williams Arcade's Greatest Hits - Overview| publisher = [[Allgame]]| first = Brett Alan| last = Weiss| accessdate = 2009-06-25 |archiveurl=https://web.archive.org/web/20090701003623/http://www.allgame.com/game.php?id=2356 |archivedate=2009-07-01}}</ref><ref>{{cite web| url = http://www.allgame.com/game.php?id=24396| title = Midway's Greatest Arcade Hits: Vol. 1 - Overview| publisher = [[Allgame]]| accessdate = 2009-06-25 |archiveurl=https://web.archive.org/web/20090820063934/http://www.allgame.com/game.php?id=24396 |archivedate=2009-08-20 }}</ref><ref>{{Cite web| url = http://www.ign.com/articles/2003/08/11/midway-arcade-treasures-6| title = Midway Arcade Treasures| first = Craig| last = Harris| date = 2003-08-11| publisher = [[IGN]]| accessdate = 2009-06-25}}</ref><ref>{{cite web | url = http://www.ign.com/articles/2012/11/14/midway-arcade-origins-review| first = Samuel| last = Claiborn| title = Squeezing 30 fridge-sized games onto a disc proves difficult| publisher = [[IGN]] | date = 2012-11-13| accessdate = 2014-07-20}}</ref>

''Bubbles'' arcade cabinets have varying degrees of rarity. The cocktail and cabaret are the rarest, followed by the plastic 
and upright versions; the plastic models are more valuable among collectors.<ref name="PriceGuide">{{cite book| title = Official Price Guide to Classic Video Games| first = David| last = Ellis| page = 405| chapter = Arcade Classics| publisher = [[Random House]]| isbn = 0-375-72038-3| year = 2004}}</ref> Though the plastic cabinets were very durable, they would shrink over time, sometimes causing the device to become inoperable. Williams Electronics used this cabinet for only one other game, ''[[Blaster (video game)|Blaster]]''.<ref name="Arc-Treasure-2">{{cite video game| title= [[Midway Arcade Treasures]]| developer= [[Backbone Entertainment|Digital Eclipse]]| publisher= [[Midway Games]]| date= 2003-11-18| platform= [[PlayStation 2]]| version= | level= Blaster Trivia}}</ref>

== Notes ==
<references group="Note" />

== References ==
{{reflist}}

==External links== 	 
* [http://www.bubblestribute.com My Tribute to Williams Bubbles]
* [http://www.arcade-history.com/index.php?page=detail&id=347 Bubbles] at arcade-history.com

{{good article}}

[[Category:1982 video games]]
[[Category:Arcade games]]
[[Category:Action video games]]
[[Category:Williams video games]]
[[Category:Video games developed in the United States]]