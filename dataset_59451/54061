<!-- Deleted image removed: [[Image:Baseball SA Logo.PNG|200px|right]] -->

'''Baseball SA''' is the governing body of [[amateur sports|amateur]] [[baseball]] within [[South Australia]], and headquarters of the South Australian Baseball League. Baseball SA is governed by the [[Australian Baseball Federation]].

== Clubs ==
Several  clubs share their nicknames with [[Major league baseball|American Professional Baseball]] clubs.  The local clubs are not affiliated with their [[Major League Baseball]] counterparts but the names provide an instantly recognisable baseball link. 

===Division One Summer Clubs===

{| class="wikitable"
|-
! Club
! Nickname
! Location
! Home Ground
! Website
|-
| [[Adelaide Baseball Club]] 
| Angels 
| [[Plympton, South Australia]]
| Weigall Oval
| [https://web.archive.org/web/20091026233251/http://geocities.com/adelaide_baseball_club/  Adelaide]
|-
| [[East Torrens Baseball Club]] 
| Redsox
| [[Payneham, South Australia]]
| Patterson Reserve
| [http://www.easttorrens.baseball.com.au/ East Torrens]
|-
| [[Glenelg Baseball Club]]
| Tigers
| [[Glenelg, South Australia]]
| Anderson Reserve
| [http://www.glenelg.baseball.com.au/ Glenelg]
|-
| [[Golden Grove Central Districts]]
| Dodgers
| [[Surrey Downs, South Australia]]
| Illyarrie Reserve
| [http://www.ggcdbaseball.com.au/ Golden Grove]
|-
| [[Goodwood Baseball Club]]  
| Indians
| [[Colonel Light Gardens, South Australia]]
| Mortlock Park
| [http://www.goodwoodbaseball.com.au/ Goodwood]
|-
| [[Henley & Grange Baseball Club]]  
| Rams
| [[West Lakes, South Australia]]
| West Lakes Sports and Social Club
| [https://web.archive.org/web/20090330061341/http://www.henleyandgrangebaseballclub.com.au:80/ Henley & Grange]
|-
| [[Kensington Baseball Club]] 
| Cardinals 
| [[Erindale, South Australia]]
| Newland Reserve
| [https://web.archive.org/web/20060430231010/http://www.kensingtonbaseball.org.au:80/ Kensington]
|-
| [[Northern Districts Baseball Club]] 
| Reds
| [[Ingle Farm, South Australia]]
| Walkleys Park
| [http://www.northerndistricts.baseball.com.au/ Northern Districts]
|-
| [[Port Adelaide Baseball Club|Port Adelaide Magpies]]  
| Magpies
| [[Port Adelaide, South Australia]]
| War Memorial Reserve
| [http://www.padbc.com.au/ Port Adelaide]
|-
| [[Southern Districts Baseball Club]]  
| Hawks
| [[Christie Downs, South Australia]]
| Peregrine Park
| [http://www.southerndistricts.baseball.com.au/ Southern Districts]
|-
| [[Sturt Baseball Club]]  
| Saints
| [[Mitcham, South Australia]]
| Norman Reserve
| [http://www.sturtbaseballclub.org.au/ Sturt]
|-
| [[West Torrens Baseball Club]]  
| Eagles
| [[Lockleys, South Australia]]
| Lockleys Oval
| [http://www.wtbc.org.au/ West Torrens]
|-
| [[Woodville Baseball Club]]  
| Senators
| [[Findon, South Australia]]
| Findon Oval
| [http://www.woodville.baseball.com.au/ Woodville]
|-
|}

===Other Summer Clubs===

{| class="wikitable"
|-
! Club
! Nickname
! City
! Home Ground
! Website
|-
| [[Playford City Baseball Club]]
| Rebels
| [[Elizabeth, South Australia]]
| Broadmeadows Oval
| [https://web.archive.org/web/20110715093105/http://www.playfordcityrebels.com/ Playford City]
|-
| [[Flinders University]]
| 
| 
| Flinders University Ground
|-

| [[Gawler Baseball Club]]
| Rangers
| [[Willaston, South Australia]]
| Elliot Goodger Reserve (Willaston Oval)
| [http://www.gawler.baseball.com.au Gawler Rangers Baseball Club]
 
|}

===Winterball===

[[Adelaide University Baseball Club]] ("Varsity") are the 2010 Winterball Division 1 premiers. It is their seventh straight premiership win. Varsity also won the Division 3 premiership.

==South Australian Baseball League - Division 1 Premiers==

{| class="wikitable"
|-
! Season
! Premiers
|-
| 1889/90
| Post & Telegraph
|-
| 1890/91
| Adelaide
|-
| 1891/92
| Adelaide
|-
| 1892/1893
| Adelaide
|-
| 1893/94
| South Adelaide
|-
| 1894/95
| Goodwood
|-
| 1895/96
| Goodwood
|-
| 1896/97
| South Adelaide
|-

|'''In the Beginning'''
|-
| 1908
| Goodwood
|-
| 1909
| North Adelaide
|-
| 1910
| North Adelaide
|-
| 1911
| Freemason Ramblers
|-
| 1912
| Norwood
|-
| 1913
| Freemason Ramblers
|-
| 1914
| Adelaide
|-
| 1915
| Millswood
|-
|'''1916-1918'''
|'''The War Years'''
|-
| 1919
| Blue Sox
|-
| 1920
| West Torrens
|-
| 1921
| Sturt
|-
| 1922
| Freemason Ramblers
|-
| 1923
| Freemason Ramblers
|-
| 1924
| Goodwood
|-
| 1925
| East Torrens
|-
| 1926
| East Torrens
|-
| 1927
| Goodwood
|-
| 1928
| Goodwood
|-
| 1929
| Goodwood
|-
| 1930
| Kensington
|-
|'''District Baseball'''
|-
| 1931
| Goodwood
|-
| 1932
| Goodwood
|-
| 1933
| Goodwood
|-
| 1934
| University
|-
| 1935
| Goodwood
|-
| 1936
| Goodwood
|-
| 1937
| Goodwood
|-
| 1938
| Goodwood
|-
| 1939
| Kensington
|-
| 1940
| West Torrens
|-
| 1941
| Goodwood
|-
|'''1942-1945'''
|'''The War Years'''
|-
| 1946
| West Torrens
|-
| 1947
| West Torrens
|-
| 1948
| West Torrens
|-
| 1949
| University
|-
| 1950
| West Torrens
|-
| 1951
| West Torrens
|-
| 1952
| West Torrens
|-
| 1953
| Prospect
|-
| 1954
| West Torrens
|-
| 1955
| Prospect
|-
| 1956
| Kensington
|-
| 1957
| Goodwood
|-
| 1958
| Port Adelaide
|-
| 1959
| Kensington
|-
| 1960
| Adelaide
|-
| 1961
| West Torrens
|-
| 1962
| Glenelg
|-
| 1963
| West Torrens
|-
| 1964
| University
|-
| 1965
| West Torrens
|-
| 1966
| Port Adelaide
|-
| 1967
| West Torrens
|-
| 1968
| Port Adelaide
|-
| 1969
| Port Adelaide
|-
|'''Summer Baseball'''
|-
| 1968/69
| Port Adelaide
|-
| 1969/70
| Port Adelaide
|-
| 1970/71
| Port Adelaide
|-
| 1971/72
| Port Adelaide
|-
| 1972/73
| Woodville
|-
| 1973/74
| East Torrens
|-
| 1974/75
| Woodville
|-
| 1975/76
| East Torrens
|-
| 1976/77
| Glenelg
|-
| 1977/78
| Sturt
|-
| 1978/79
| Glenelg
|-
| 1979/80
| Glenelg
|-
| 1980/81
| Woodville
|-
| 1981/82
| Port Adelaide
|-
| 1982/83
| Glenelg
|-
| 1983/84
| Kensington
|-
| 1984/85
| Kensington
|-
| 1985/86
| Kensington
|-
| 1986/87
| West Torrens
|-
| 1987/88
| West Torrens
|-
| 1988/89
| East Torrens
|-
| 1989/90
| West Torrens
|-
| 1990/91
| Sturt
|-
| 1991/92
| West Torrens
|-
| 1992/93
| West Torrens
|-
| 1993/94
| West Torrens
|-
| 1994/95
| West Torrens
|-
| 1995/96
| Henley & Grange
|-
| 1996/97
| Henley & Grange
|-
| 1997/98
| East Torrens
|-
| 1998/99
| East Torrens
|-
| 1999/2000
| Goodwood
|-
| 2000/01
| Glenelg
|-
| 2001/02
| Adelaide
|-
| 2002/03
| Goodwood
|-
| 2003/04
| Kensington
|-
| 2004/05
| Glenelg
|-
| 2005/06
| Goodwood
|-
| 2006/07
| West Torrens
|-
| 2007/08
| Port Adelaide
|-
| 2008/09
| Port Adelaide
|-
| 2009/10
| Kensington
|-
| 2010/11
| Kensington
|-
| 2011/12
| Kensington
|-
| 2012/13
| Goodwood
|-
| 2013/14
| Kensington
|-
| 2014/15
| Kensington
|-
| 2015/16
| Glenelg
|-
|2016/17
|West Torrens 
|-

|}

'''*Premierships since 1908'''
* West Torrens - 22
* Goodwood - 18
* Kensington - 13
* Port Adelaide - 11
* Glenelg - 8
* East Torrens - 8 (*inc. 1 as Norwood)
* Freemason Ramblers - 4
* Adelaide - 3
* Sturt - 3
* University - 3
* Woodville - 3
* North Adelaide - 2
* Henley & Grange - 2
* Prospect - 2 (*Now Northern Districts following merger with Enfield baseball club in 1969)
* Millswood - 1
* Blue Sox - 1

==Capps Medal==

The [[Capps Medal]] is the most prestigious baseball award in South Australia and is presented to the best and fairest player in Division 1 for the SABL home and away season.  The award is determined through umpires votes from each minor round game with the best player on the day receiving 3 votes, the second best 2 votes and the third best 1 vote.

==See also==
*[[South Australian Baseball League 2006/2007]]
*[[Capps Medal]]

==External links==
* [http://www.sa.baseball.com.au/ Baseball SA]

{{SABL}}
{{Baseball in Australia}}


[[Category:Baseball governing bodies in Australia|Sout]]
[[Category:Sports governing bodies in South Australia]]