{{Infobox company
| name             = AireSpring
| logo             = 
| caption          = 
| type             = Private
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = {{Start date|2002|06|12}}
| defunct          = 
| location_city    = [[Van Nuys, California]]
| location_country = United States
| location         = 
| locations        = 
| area_served      = 
| key_people       = Avi Lonstein<br/><small>(President and CEO)</small><br/>
Arno Vigen<br/><small>(Chief Financial Officer)</small><br/>
Daniel Lonstein<br/><small>(Chief Operating Officer)</small><br/>
David Lonstein<br/><small>(EVP of Product Management)</small><br/>
Ellen Cahill<br/><small>(Senior VP of Marketing)</small><br/>
Ron McNab<br/><small>(Senior VP of Sales)</small><br/>
Vicky Weatherwax<br/><small>(Senior VP of Customer Operations)</small><br/>
Darren Sandford<br/><small>(Senior VP of Operations)</small><br/>
Kevin Griffo<br/><small>(Senior VP of Wholesale Voice)</small>
| industry         = B2B High Speed Internet, Voice and Cloud services
[[Internet service provider|Internet Service Provider]]
| products         = Data, Voice, MPLS, Cloud Hosted PBX, Cloud Contact Center, Conferencing
| services         = National provider of Local, Long Distance, Internet, MPLS, IP communications
| revenue          = 
| operating_income = 
| net_income       = 
| aum              = 
| assets           = 
| equity           = 
| owner            = Avi Lonstein
| num_employees    = 175
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|http://www.airespring.com}} 
| footnotes        = 
| intl             = 
}}

{{Advert|article|date=June 2015}}
'''AireSpring''' is a [[Competitive Local Exchange Carrier]] that provides cloud communications and managed connectivity services to businesses.  Headquartered in Van Nuys, California, the company provides voice, data, and IP services to around 10,000 small, medium-sized, and large enterprises in more than 80 major metropolitan markets across the United States.<ref>{{cite news|title=Long Distance Telephone Companies|url=http://www.la.bbb.org/business-reviews/Long-Distance-Telephone-Companies/Airespring-Inc--in-Van-Nuys-CA-13183066|accessdate=12 March 2012 |newspaper=Better Business Bureau|date=9 March 2011}}</ref>

== History ==
AireSpring was founded in 2001 by Avi, Daniel, and David Lonstein who in 1997 sold ADDTEL Communications, Inc., their privately held company who resold switched long distance, to SA Communications, Inc.<ref>{{cite news |title=Addtel merges with SA Telecommunications |url=http://www.bizjournals.com/dallas/stories/1997/01/13/daily4.html |accessdate=2 March 2012 |newspaper=Dallas Business Journal |date=14 January 1997}}</ref>   The Lonstein brothers reinvested a portion of their returns into a new company whose sole focus was on the small to medium-sized business market and called it AireSpring. The company began where they left off, selling switched long distance services.<ref>{{cite news|title=AireSpring Company Overview|url=http://www.phonedog.com/long-distance/companies/airespring/ |accessdate=12 March 2012 |newspaper=Phone Dog - Telecom News|date=21 September 2010}}</ref> Since then, AireSpring has expanded its portfolio to include local phone service, dedicated internet, [[Carrier Ethernet|ethernet]], [[MPLS VPN]], and VoIP/[[SIP trunking]] services.<ref>{{cite news |title=Company Overview of Airespring, Inc. |url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=20990623|accessdate=12 March 2012 |newspaper=Bloomberg Businessweek|date=2 March 2012}}</ref>

In 2014, AireSpring acquired hosted contact center provider, simplyCT.<ref>{{cite news|title=Introducing AireContact, Enterprise-Class, Cloud-Based Contact Center Solutions|url=http://www.prweb.com/releases/Cloud/Contact-Center/prweb11827333.htm |accessdate=23 March 2015|newspaper=PRWeb|date=7 May 2014}}</ref>

== Network ==
AireSpring is a facilities-based network provider in the United States and operates a nationwide network.<ref>{{cite news|title=AireSpring Network at a Glance|url=http://www.solveforce.com/airespring-communications|accessdate=12 March 2012|newspaper=Solveforce Carrier Reviews|date=9 March 2012}}</ref>  When AireSpring customers are located in an area that can not access the network directly, AireSpring leverages one of its wholesale agreements with [[AT&T]], [[Verizon Business]], [[XO Communications]], [[Global Crossing]] (now [[Level 3 Communications]]), [[PAETEC]] (now [[Windstream]]), or [[Qwest Communications]] (now [[CenturyLink]]), for last-mile connectivity.<ref>{{cite news|title=Regulatory Charges and Surcharges|url=http://www.airespring.com/service-terms/64-surcharges.html |accessdate=9 March 2012|newspaper=AireSpring Customer Updates|date=9 March 2012}}</ref><ref>{{cite news|title=AireSpring Launches two New Services: AirePBX Hosted IP PBX Solution and AireSpring Meshed MPLS|url=http://www.telarus.com/industry/airespring-launches-two-new-services:-airepbx-hosted-ip-pbx-solution-and-airespring-meshed-mpls.html |accessdate=14 March 2011|newspaper=Telecom Industry Updates|date=14 March 2011}}</ref> In 2010, AireSpring processed 4 billion phone calls through its network.<ref>{{cite news|title=Toshiba and AireSpring Certify Interoperability of Strata CIX VoIP Systems and AireSpring SIP Trunking|url=http://www.telecom.toshiba.com/News/Press_Releases/2010/news_pr_031610.cfm |accessdate=14 May 2010|newspaper=Telecom Industry Updates|date=14 May 2010}}</ref>

== Services ==
The company offers a broad range of services to its business and wholesale customers.  These services include:
* Local phone services
* Cloud Hosted IP PBX
* Cloud Hosted Contact Center
* Long distance voice services<ref>{{cite news|title=AireSpring Data:SIP Review|url=http://www.pbxsystems.org/airespring-datasip-review/|accessdate=12 March 2012|newspaper=Business Phone Solutions|date=22 September 2011}}</ref>
* [[T-carrier|Dedicated internet access]]
* MPLS IP-VPN<ref>{{cite news|title=AireSpring Launches MPLS-VPN Service|url=http://vartips.com/carriers/airespring/airespring-mpls-vpn-670.html |accessdate=12 March 2012|newspaper=Telecom News|date=28 September 2009}}</ref>
* SIP<ref>{{cite news|title=CEO Spotlight: AireSpring's Avi Lonstein|url=http://www.tmcnet.com/channels/sip/articles/54126-ceo-spotlight-airesprings-avi-lonste.htm |accessdate=12 March 2012|newspaper=TMCnet|date=13 April 2009}}</ref>
* [[SIP trunking]]<ref>{{cite news|title=Innovation Network Members|url=http://www.shoretel.com/partners/tech_developers/ecosystem/AireSpring.html |accessdate=12 March 2012|newspaper=Telecom News|date=21 June 2009}}</ref>
* Video conferencing
* Audio and Web conferencing
* [[Ethernet]]
* Private line service
* Hosted IP-PBX<ref>{{cite news|title=AireSpring Launches Cloud Hosted IP-PBX Solution|url=http://www.thehostingnews.com/airespring-launches-cloud-hosted-ip-pbx-solution-16716.html|accessdate=12 March 2012 |newspaper=The Hosting News|date=15 March 2011}}</ref> <ref>{{cite news|last=Holverson|first=Austin|title=AireSpring Releases Version 2.0 of Cloud Hosted IP PBX for Business|url=http://www.telarus.com/industry/airespring-releases-version-2.0-of-cloud-hosted-ip-pbx-for-business.html|accessdate=27 September 2012|newspaper=Telecom Channel Updates|date=13 September 2012}}</ref>

==Distribution channels==
AireSpring sells its services through two channels:  the [[Sales outsourcing|indirect sales channel]] and the wholesale division.  <ref>{{cite news|title=IP Communications Company AireSpring Looks to Grow Channel Manager Team|url=http://ip-communications.tmcnet.com/articles/230357-ip-communications-company-airespring-looks-grow-channel-manager.htm
|accessdate=17 October 2011|newspaper=TMCnet|date=17 October 2011}}</ref><ref>{{cite news|title=AireSpring Names Telarus 2010 Top Channel Partner|url=http://www.telarus.com/news/airespring-names-telarus-2010-top-channel-partner.html|accessdate=16 March 2011|newspaper=Telecom Industry News|date=16 March 2011}}</ref>

[[File:Airespring Award.jpg|thumb|right|upright=2.0|Ron McNab and Charles Lomond of AireSpring present the 2011 Partner Elite award to Adam Edwards of [[Telarus]], Inc.]]

==References==
{{Reflist|colwidth=30em}}

==External links==
* [http://www.airespring.com Official Company Web Site]

[[Category:Technology companies established in 2002]]
[[Category:VoIP companies of the United States]]
[[Category:Internet service providers of the United States]]
[[Category:Telecommunications companies of the United States]]
[[Category:2002 establishments in California]]