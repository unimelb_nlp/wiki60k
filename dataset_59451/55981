{{Infobox Journal
| title	=	Breast Cancer Research and Treatment
| cover	=	[[File:Breast Cancer Research and Treatment.jpg]]
| discipline	=	[[Medicine]]
| website	=	http://springerlink.metapress.com/link.asp?id=102860
| publisher	=	[[Springer Netherlands]]
| country	=	[[Netherlands]]
| abbreviation	=	Breast Cancer Res Treat
| history	=	1981–present
| impact = 3.940
| impact-year = 2014
| ISSN	=	0167-6806
}}

'''''Breast Cancer Research and Treatment''''' is a [[scientific journal]] focused on the treatment of and investigations in [[breast cancer]]. It is targeted towards a wide audience of [[clinical research]]ers, [[epidemiology|epidemiologists]], [[immunology|immunologists]], or [[cell biology|cell biologists]] interested in breast cancer.

The types of articles in this journal include original research, invited reviews, discussions on controversial issues, book reviews, meeting reports, letters to the editors, and editorials. Manuscripts are [[peer review]]ed by an international and multidisciplinary panel of advisory editors. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.940.<ref name=WoS>{{cite book |year=2015 |chapter=Breast Cancer Research |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

[[Category:Oncology journals]]
[[Category:Breast cancer]]
[[Category:Publications established in 1981]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]