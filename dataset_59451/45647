[[Image:pentagonhelicopter3.jpg|thumb|200px|&nbsp;]] 
[[Image:thompsonplane.jpg|thumb|200px|&nbsp;]]
<!-- Deleted image removed: [[Image:Comic for web best.jpg|thumb|200px|&nbsp;]] -->
'''Harold E. "Tommy" Thompson''' (1921 &ndash; October 29, 2003<ref name=PT>{{cite news |title=Hobart record setter |author=Ruth Ann Krause |newspaper=[[Post-Tribune]] |date=December 9, 2003 |url=http://www.highbeam.com/doc/1N1-1004023A90DD5A5A.html}} (Highbeam subscription required)</ref>) of [[Hobart, Indiana]], was a [[helicopter]] aviation pioneer.<ref>{{cite news|url=https://select.nytimes.com/gst/abstract.html?res=F10E15FD3B58177B93C5A9178ED85F4D8485F9|title=HELICOPTER SETS MARK; Averages 122.75 Miles an Hour Over a 100 Kilometres|publisher=The New York Times|accessdate=2008-01-23|last=|first= | date=1949-05-07}} (''New York Times'' subscription required)</ref> He was the first man to intentionally loop a helicopter, set three international helicopter speed records, and was the first man to land a helicopter in the courtyard of [[The Pentagon]].  Thompson was a veteran of 3,500 hours in single-engine propeller fixed-wing [[aircraft]] and 3000 more in helicopters.

== Life ==
Having spent two years at [[Purdue University]], Thompson was called up into the [[United States Army Air Corps]] in 1943, a week after he had married his childhood sweetheart, Carolyn Kramer.<ref name=PT/> Thompson served as a [[P-47]] instructor at [[Moore Field]] in [[Mission, Texas]] until January 1945, when he earned an assignment to the Army's first helicopter class at [[Chanute Field]], Illinois. Later, he was assigned to the [[Bridgeport, Connecticut]], plant of [[Igor Sikorsky]], who pioneered helicopters in America.

After the war, "Tommy" as he was known, got a job as one of [[Sikorsky Aircraft|Sikorsky]]'s three test pilots<ref name=PT/>—in the trial and error days.  The plant produced six helicopters a month, mostly hand built.  Engineers tinkered with new designs, and the test pilots tried them out.  Most of the early models had slow, sluggish controls - some flew as expected, but others didn't.  Thompson was also Igor Sikorsky's personal pilot.  By 1949, Thompson was an experienced helicopter pilot.  He had been through some forced landings and crashes, but had not been seriously injured.

== Records set in the S-52 ==
In 1949, Sikorsky engineers developed the [[Sikorsky S-52]] helicopter.  "It was a sharp, responsive dream," Thompson recalls.  "After trying some mild acrobatics, I figured it would loop." Until Thompson, no one had dared try to loop a helicopter.  As Sikorsky's chief test pilot [[Jimmy Viner]] pointed out, "Any of 10 things can go wrong--all fatal,be sure you know what you're doing."  Thompson did—erratically at first, then perfectly—10 loops in all, as a movie camera recorded the flight for history.

That year, he went to the [[Cleveland]] air races with the S-52, where he set the first of three international speed records that he was to achieve in the helicopters: {{convert|129.616|mph|kph}} over {{convert|3|km|mi}}.<ref>{{cite web |url=http://wrightpattcap.org/pad/index.php?s=18a900bf21380a51875a90c8012d94bb&showtopic=299&pid=328&st=0&#entry328 |title=On This Day in Air and Space History - 27 April |publisher=Wright-Patterson Sq, Civil Air Patrol |accessdate=September 14, 2012}}</ref>

== Flight instructor ==
Thompson also taught others how to fly helicopters.  His students included Admiral [[Arthur W. Radford]]; [[Pat Handy]], first woman to fly solo in a helicopter; and [[Rodman Wanamaker]], Eastern department store tycoon.

== Last flight ==
His career came to an abrupt halt on a spring day in 1950, when he took an admiral aloft at the Navy's [[Lakehurst, New Jersey]], base.  Suddenly, a shaft snapped, and the tail rotor came apart.

Thompson skillfully kept the craft from spinning around, the usual result of such an accident.  The helicopter landed hard, crushed the landing gear and tilted, while the spinning overhead rotor chewed up the ground and disintegrated.  Tommy crawled out with nothing worse than a cut cheek.  The admiral was shaken, but game:  "All in a day's work, eh, boy?" Thompson however had walked away from more than 20 forced landings and now his fifth helicopter crash.  Figuring he had stretched the law of averages too far, he replied, "Maybe for you, sir, but not for me". That night, when he got home he talked to his wife and refrained from flying again in a helicopter until 1979.

==Later life==
After his flying career came to a halt, he moved back to Hobart, Indiana, and began working with his father, delivering fuel oil for the Standard Oil Company. In 1979, Thompson visited the Tucson Convention Center where a large helicopter convention was taking place.  He immediately was recognized for his feats during the convention and given the opportunity to pilot one of Sikorsky's S-58s.

Currently he is honored at the [[Smithsonian]] [[National Air and Space Museum]] in [[Washington, D.C.]] outside the entrance to the [[Steven F. Udvar-Hazy Center]].  Thompson can be found on Panel #37, Tablet #1, Column #1.

Thompson died on October 29, 2003 in Hobart, Indiana. He was survived by his wife Carolyn.
<!--I'm writing part of this from memory and as Harold's Grandson, he has a book available that talks all about his adventures
it was written for the family, but I can get copies to those who are interested.  I also have videos showing the loop and 4 photo albums full of pictures.Thanks for everyones help-- Robtff-->

== References ==
{{reflist}}

{{DEFAULTSORT:Thompson, Harold E.}}
[[Category:1921 births]]
[[Category:2003 deaths]]
[[Category:American aviators]]
[[Category:American test pilots]]
[[Category:Aviation pioneers]]
[[Category:Flight instructors]]
[[Category:Helicopter pilots]]