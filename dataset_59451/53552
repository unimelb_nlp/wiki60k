{{Use dmy dates|date=August 2011}}
{{icelandic name|Halldór}}
{{Infobox officeholder
|name         = Halldór Ásgrímsson
|image        = Halldor Asgrimsson generalsekreterare Nordiska ministerradet (2).jpg
|office       = 22nd [[Prime Minister of Iceland]]
|president    = [[Ólafur Ragnar Grímsson]]
|term_start   = 15 September 2004
|term_end     = 15 June 2006
|predecessor  = [[Davíð Oddsson]]
|successor    = [[Geir Haarde]]
|office1      = Secretary General for the [[Nordic Council|Nordic Council of Ministers]]
|term_start1  = 1 January 2007
|term_end1    = 18 May 2015
|predecessor1 = [[Per Unckel]]
|successor1   = [[Dagfinn Høybråten]]
|birth_date   = {{birth date|1947|9|8|df=y}}
|birth_place  = [[Vopnafjörður]], [[Iceland]]
|death_date   = {{death date and age|2015|5|18|1947|9|8|df=y}}
|death_place  = [[Reykjavik]], [[Iceland]]
|party        = [[Progressive Party (Iceland)|Progressive Party]]
|spouse       = Sigurjóna Sigurðardóttir
|children     = 3
|alma_mater   = [[Bifröst University]]
}}
'''Halldór Ásgrímsson''' (pronounced {{IPA-is|ˈhalːtour ˈaːuskrimsɔn|}}; 8 September 1947 – 18 May 2015) was an [[Iceland]]ic politician, who served as [[Prime Minister of Iceland]] from 2004 to 2006 and was leader of the [[Progressive Party (Iceland)|Progressive Party]] from 1994 to 2006.<ref>{{cite web|url=https://books.google.com/books?id=uoIG6bbP32IC|title=Historical Dictionary of Iceland|first=Gudmundur|last=Halfdanarson|date=23 October 2008|publisher=Scarecrow Press|accessdate=3 September 2016|via=Google Books}}</ref>

==Education and early life==
Halldór studied at the [[Bifröst University|Co-operative College in Bifröst]], and became a certified public accountant in 1970. He later completed graduate commerce studies at the Universities of [[University of Bergen|Bergen]] and [[University of Copenhagen|Copenhagen]], and worked as a lecturer at the [[University of Iceland]] from 1973 to 1975.

==Political career==
He represented the East constituency as a member of the [[Althing]] (Icelandic Parliament) from 1974 to 1978 and from 1979 to 2003, when he was elected to represent the [[Reykjavík]] North constituency. Over the years, he has served in a large number of ministerial portfolios, namely as [[Minister of Fisheries (Iceland)|Minister of Fisheries]] from 1983 to 1991, [[Minister of Justice and Ecclesiastical Affairs (Iceland)|Minister of Justice and Ecclesiastical Affairs]] from 1988 to 1989, [[Minister for Nordic Cooperation (Iceland)|Minister for Nordic Cooperation]] from 1985 to 1987 and 1995 to 1999 and [[Minister of Foreign Affairs (Iceland)|Minister of Foreign Affairs]] from 1995 to 2004.

Halldór took over as Prime Minister on 15 September 2004, succeeding [[Independence Party (Iceland)|Independence Party]] leader [[Davíð Oddsson]], while Davíð replaced Halldór as Foreign Minister.

On 5 June 2006, following poor results in municipal elections, Halldór announced his resignation as Prime Minister and stated that he intended to step down as leader of the Progressive Party in August 2006. [[Geir H. Haarde]], the Foreign Minister of Iceland, succeeded him on 15 June 2006.

Halldór Ásgrímsson's successor as Progressive Party leader was [[Jón Sigurðsson (f. 1946)|Jón Sigurðsson]], [[Ministry of Industry, Energy and Tourism (Iceland)|Ministry of Industry, Energy and Tourism]], who was elected at the party's convention in August 2006. At the convention Halldór ended his political career with an emotional and dynamic farewell speech to the party. Halldór resigned as MP after the convention; he was the longest serving MP at the time.

On 31 October 2006, Halldór was chosen as the Secretary-General of the [[Nordic Council of Ministers]].

Halldór Ásgrímsson was an Honorary Member of [http://www.raoulwallenberg.net The International Raoul Wallenberg Foundation]. He died of a heart attack at a Reykjavik hospital in May 2015.<ref>{{cite web|url=http://icelandreview.com/news/2015/05/19/former-prime-minister-iceland-passes-away|title=Former Prime Minister of Iceland Passes Away|first=Eygló Svala|last=Arnarsdóttir|date=19 May 2015|publisher=|accessdate=3 September 2016}}</ref>

==References==
{{Reflist}}

==External links==
{{Commons category}}
*[http://eng.forsaetisraduneyti.is/minister/Speeches_HA/nr/234 Halldór's biography (since his tenure as PM)] {{en icon}}
*[http://www.norden.org Nordic Council of Ministers]

{{s-start}}
{{s-off}}
{{s-bef|before=[[Steingrímur Hermannsson]]}}
{{s-ttl|title=[[Ministry of Fisheries and Agriculture (Iceland)|Minister of Fisheries]]|years=1983–1991}}
{{s-aft|after=[[Þorsteinn Pálsson]]}}
|-
{{s-bef|before=[[Jón Sigurðsson (minister)|Jón Sigurðsson]]}}
{{s-ttl|title=[[Minister of Justice and Ecclesiastical Affairs (Iceland)|Minister of Justice and Ecclesiastical Affairs]]|years=1988–1989}}
{{s-aft|after=[[Óli Þ. Guðbjartssson|Óli Guðbjartssson]]}}
|-
{{s-bef|before=[[Jón Baldvin Hannibalsson]]}}
{{s-ttl|title=[[Minister for Foreign Affairs (Iceland)|Minister of Foreign Affairs]]|years=1995–2004}}
{{s-aft|after=[[Geir Haarde]]}}
|-
{{s-bef|before=[[Davíð Oddsson]]}}
{{s-ttl|title=[[Prime Minister of Iceland]]|years=2004–2006}}
{{s-aft|after=[[Geir Haarde]]}}
|-
{{s-ppo}}
{{s-bef|before=[[Steingrímur Hermannsson]]}}
{{s-ttl|title=Leader of the [[Progressive Party (Iceland)|Progressive Party]]|years=1994–2006}}
{{s-aft|after=[[Jón Sigurðsson (minister)|Jón Sigurðsson]]}}
{{s-end}}

{{First cabinet of Steingrimur Hermannsson}}
{{Cabinet of Thorsteinn Palsson}}
{{Second cabinet of Steingrimur Hermannsson}}
{{Third cabinet of Steingrimur Hermannsson}}
{{Second cabinet of David Oddsson}}
{{Third cabinet of David Oddsson}}
{{Fourth cabinet of David Oddsson}}
{{Cabinet of Halldor Asgrimsson}}
{{Prime Ministers of Iceland}}

{{DEFAULTSORT:Halldor Asgrimsson}}
[[Category:1947 births]]
[[Category:2015 deaths]]
[[Category:Accounting educators]]
[[Category:Disease-related deaths in Iceland]]
[[Category:Fisheries ministers of Iceland]]
[[Category:Foreign ministers of Iceland]]
[[Category:Icelandic accountants]]
[[Category:Justice ministers of Iceland]]
[[Category:Members of the Althing]]
[[Category:Prime Ministers of Iceland]]
[[Category:Progressive Party (Iceland) politicians]]
[[Category:University of Bergen alumni]]
[[Category:University of Copenhagen alumni]]
[[Category:University of Iceland faculty]]