{{Infobox journal
| title = Physics of the Solid State
| cover = [[File:PhysSolidStateCover.jpg]]
| discipline = [[Physics]]
| formernames = Soviet Physics - Solid State
| abbreviation = Phys. Solid State
| peer-reviewed =
| impact = 0.821
| impact-year = 2014
| editor = [[Alexander  A. Kaplyanskii]]
| publisher = [[Pleiades Publishing]] and [[Springer Science+Business Media|Springer]]
| country = 
| history = 
| frequency = monthly
| ISSN = 1063-7834
| eISSN = 1090-6460
| CODEN = PSOSED
| LCCN = 
| OCLC = 35364153
| website =
| link1= http://www.maik.ru/cgi-bin/journal.pl?name=physsost&page=main
| link1-name= Pleiades Publishing page
| link2= http://www.springer.com/materials/journal/11451
| link2-name= Springer page
}}
'''''Physics of the Solid State''''' is a [[peer-review]]ed [[scientific journal]] of [[solid state physics]] that publishes articles from researchers based at the [[Russian Academy of Sciences]] and other leading institutions in Russia. The journal is published by [[Pleiades Publishing]] and the online version is provided by the publisher [[Springer Science+Business Media|Springer]]. It is edited by [[Alexander  A. Kaplyanskii]].

==Abstracting and indexing==
Physics of the Solid State is abstracted and indexed in the following databases:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Academic Search]]
* [[Astrophysics Data System]]
* [[Chemical Abstracts Service]]
* [[Chemical and Earth Sciences]]
* [[Current Abstracts]]
* [[Current Contents]]/Physical
* [[EBSCO]]
* [[Gale]]
* [[INIS Atomindex]]
* [[INSPEC]]
* [[International Bibliography of Book Reviews]]
* [[International Bibliography of Periodical Literature]]
* [[Journal Citation Reports]]/Science Edition
* [[SCImago]]
* [[Scopus]]
* [[Science Citation Index]]
* [[Science Citation Index Expanded]] ([[SciSearch]])
* [[Summon by ProQuest]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.821.<ref name=WoS>{{cite book |year=2015 |chapter=Physics of the Solid State|title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

== External links ==
* [http://www.maik.ru/cgi-bin/journal.pl?name=physsost&page=main ''Physics of the Solid State''] @ Pleiades Publishing
* [http://www.springer.com/materials/journal/11451 ''Physics of the Solid State''] @ Springer

[[Category:Physics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]