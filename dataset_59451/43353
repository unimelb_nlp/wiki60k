<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name=XBQM-108A
  |image=XBQM-108A testing.jpg{{!}}border
  |size=200px
  |caption=
}}{{Infobox Aircraft Type
  |type=[[Unmanned aerial vehicle]]
  |manufacturer=[[Naval Weapons Center]]
  |designer=
  |first flight=29 September 1976
  |introduced=
  |retired=
  |status=
  |primary user=[[United States Navy]]
  |more users=
  |produced=
  |number built=1
  |unit cost=
  |developed from= [[BQM-74 Chukar]]
  |variants with their own articles=
}}
|}

The '''XBQM-108A''' was an experimental [[VTOL]] [[unmanned aerial vehicle]] developed by the [[United States Navy]] during the 1970s. Although the XBQM-108A successfully conducted tethered flight tests, the project was cancelled before any free flights could be conducted.

==Design and development==
The XBQM-108 project was initiated in 1975 by the Aviation and Surface Effects Department of the U.S. Navy's [[David W. Taylor Naval Ship Research and Development Center]].<ref name="DS"/><ref name="DTIC">Eilertson 1977</ref> The project was intended to demonstrate the practicality of the 'Vertical Attitude Take-Off and Landing' (VATOL), or '[[tailsitter]]' flight profile for [[remotely piloted vehicle]]s,<ref name="DTIC"/> as the Navy believed the increasing threat posed by [[cruise missile]]s required that aviation assets be dispersed among additional ships of the fleet, which would possess limited space for aviation operations as opposed to conventional aircraft carriers.<ref name="DTIC"/>

Based on the [[MQM-74 Chukar]] [[target drone]], the XBQM-108A was modified with a revised, [[delta wing]] with [[Canard (aeronautics)|canard]] control surfaces.<ref name="DTIC"/> The XBQM-108A was fitted with a [[thrust vectoring]] system based on the use of vanes that deflected the engine's exhaust; the aircraft retained the Chukar's recovery [[parachute]] and radio guidance system,<ref name="DS">Parsch 2002</ref> with the addition of a [[radar altimeter]] and midcourse guidance unit based on that of the [[AGM-84 Harpoon]] anti-ship missile.<ref name="NEJ">''Naval Engineers' Journal'', Volume 89, [https://books.google.com/books?ei=nxNDTaXJKIXGlQfWyLEs&ct=result&id=V3dWAAAAMAAJ&dq=%22XBQM-108A%22&q=XBQM-108A#search_anchor pp.164-169]. (1977)</ref>

==Operational history==
The XBQM-108A undertook its first, tethered flight on September 29, 1976.<ref name="DS"/><ref name="DTIC"/> The flight, using fuel supplied through the tether from {{convert|42|USgal}} drums,<ref name="NEJ"/> was successful and demonstrated that the VATOL flight profile for unmanned vehicles was practical.<ref name="DTIC"/> However, the project was cancelled before further testing could be conducted; the XBQM-108A conducted no free flights, or transitions to horizontal flight.<ref name="DS"/>

==Specifications==
{{Aircraft specs
|ref=<ref name="DS"/>
|prime units?=imp
|genhide=
|crew=None
|capacity=
|length m=3.58
|length ft=
|length in=
|length note=
|span m=
|span ft=7
|span in=3
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=560
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Teledyne CAE YJ402]]-CA-400
|eng1 type=[[turbojet]]
|eng1 kn=
|eng1 lbf=660
|eng1 note=
|thrust original=
|eng1 kn-ab=
|eng1 lbf-ab=
|more power=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=400
|max speed kts=
|max speed note=(estimated)
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=30 minutes
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent|
|related=
*[[BQM-74 Chukar]]
|similar aircraft=
* [[Ryan X-13 Vertijet]]
|lists=
*[[List of experimental aircraft]]
|see also=
}}

==References==
;Notes
{{reflist|2}}
;Bibliography
{{refbegin}}
* {{cite book |last1=Eilertson |first1=W.H. |title=The XBQM-108A Vertical Attitude Takeoff and Landing Vehicle |year=1977 |publisher=Defense Technical Information Center |location=Washington, DC |asin=B00073EVPK }}
* {{cite web |url=http://www.designation-systems.net/dusrm/m-108.html |title=NWC BQM-108 |first=Andreas |last=Parsch |year=2002 |work=Directory of U.S. Military Rockets and Missiles |publisher=designation-systems.net |accessdate=2011-01-10}}
{{refend}}

{{US missiles}}

[[Category:NWC aircraft|BQM-108]]
[[Category:United States experimental aircraft 1960–1969]]
[[Category:Tailsitter aircraft]]
[[Category:Single-engined jet aircraft]]
[[Category:Unmanned aerial vehicles of the United States]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Delta-wing aircraft]]