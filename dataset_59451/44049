<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= F121
 | image= File:F121 Fanjet.jpg
 | caption=
}}{{Infobox Aircraft Engine
 |type=[[Turbofan]]
 |manufacturer=[[Williams International]]
 |national origin=
 |first run=1984
 |major applications=[[AGM-136 Tacit Rainbow]]
 |produced=
 |number built=
 |program cost=
 |unit cost=
 |developed from=
 |variants with their own articles=
}}
|}

The '''Williams F121''' (company designation '''WR36-1'''<ref>Leyes, p. 421</ref>)
is a small turbofan engine designed for use in the [[AGM-136 Tacit Rainbow]] anti-radiation cruise missile.

==Development and Design==
The F121 engine had a rare set of design parameters as it is designed to be used only once. As a cruise missile engine, it was designed to have a long shelf life (be able to sit around unused for long periods of time) and then operate when needed for several hours.<ref name="usafm">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=887 ''Williams International F-121 Fanjet]''. National Museum of the US Air Force Fact Sheet. Accessed 24 Sept 2009.</ref> It was designed to power the [[AGM-136 Tacit Rainbow]], which was to be a stand-off anti radiation missile. Its first flight was on July 30, 1984. The AGM-136 program was canceled several years later.

Another unique feature of the engine is that it was started with an explosive cartridge because it couldn't start while still mated to its aircraft.<ref name="usafm"/>

In the late 2000s, the engine was being used by the Naval Air Warfare Center at [[Naval Air Weapons Station China Lake]] to test fuel performance and additives.<ref name="thesis">Kraemer, Nathan A. (2007). ''[http://www.dtic.mil/cgi-bin/GetTRDoc?AD=ADA476008&Location=U2&doc=GetTRDoc.pdf Development and Qualification of a Specialized Gas Turbine Test Stand to Research the Potential Benefits of Nanocatalyst Fuel Additives'']. Master's Thesis, Naval Postgraduate School.</ref>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{jetspecs
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=<ref name="usafm"/><ref name="thesis"/>
|type=Single Spool Turbofan
|length= 40 in (1.01 m)
|diameter=8.5 in (0.22 m)
|weight=49 lb (22.22 kg)
|compressor=1 stage axial fan, 6-stage axial compressor
|combustion=[[Turbomeca Piméné]]-type
|turbine=2-stage axial
|fueltype=
|oilsystem=
|power=
|thrust=70 lbf (0.31 kN)
|compression=
|bypass=1.7:1
|aircon=5 lb/s (approx)
|turbinetemp=
|fuelcon=
|specfuelcon=
|power/weight=
|thrust/weight=1.43:1
}}

==See also==
{{aircontent
<!-- first, the related articles that do not fit the specific entries: -->
|see also=

<!-- designs which were developed into or from this aircraft or engine: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Commons category}}
{{Reflist}}
* {{cite book|last=Leyes II|first=Richard A.|author2=William A. Fleming |title=The History of North American Small Gas Turbine Aircraft Engines|publisher=Smithsonian Institution|location=Washington, DC|date=1999|pages=|chapter=7|isbn=1-56347-332-1|url= }}

<!-- ==External links== -->

{{Williams aeroengines}}
{{USAF gas turbine engines}}

[[Category:Turbofan engines 1980–1989]]
[[Category:Williams aircraft engines|F121]]
[[Category:Low-bypass turbofan engines]]