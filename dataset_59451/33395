{{For|the aircraft carrier|Brazilian aircraft carrier São Paulo (A12)}}
{{Use dmy dates|date=December 2016}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:Brazilian battleship São Paulo trials.jpg|300px]]
| Ship caption = ''São Paulo'' on her [[sea trial]]s, 1910
}}
{{Infobox ship career
| Hide header = 
| Ship country = 
| Ship flag = [[File:Flag of Brazil (1889-1960).svg|100x35px|Brazilian National Ensign]]<!-- Ship infoboxes for military ships display the national ensign (and not the naval jack); in this case, is identical to the national flag. -->
| Ship name = ''São Paulo''
| Ship namesake = The state and city of [[São Paulo]]<ref name="NGB" />
| Ship ordered = 
| Ship awarded = 
| Ship builder = [[Vickers]], [[Barrow-in-Furness]], United Kingdom<ref name="Conways" />
| Ship original cost = 
| Ship yard number = 
| Ship laid down = 30 April 1907<ref name="NGB" /><ref name="Conways" />
| Ship launched = 19 April 1909<ref name="NGB" /><ref name="Conways" />
| Ship sponsor = 
| Ship christened = 
| Ship completed = 
| Ship acquired = 
| Ship commissioned = 12 July 1910<ref name="NGB" /><ref name="Conways" />
| Ship recommissioned = 
| Ship decommissioned = 
| Ship in service = 
| Ship out of service = 
| Ship renamed = 
| Ship reclassified = 
| Ship refit = 
| Ship struck = 2 August 1947<ref name="NGB" /><ref name="Whitley29" />
| Ship reinstated = 
| Ship homeport = 
| Ship identification = 
| Ship motto = Non Ducor, Duco<ref name="NGB" />{{efn-ua|The English translation of this Latin phrase is "I am not led, I lead". ''São Paulo'' shared this motto with the city the ship was named after.<ref name="NGB" />}}
| Ship nickname = 
| Ship honors = 
| Ship captured = 
| Ship fate = Sank 1951 while en route to be scrapped
| Ship status = 
| Ship notes = 
| Ship badge = 
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Minas Geraes|battleship}}
| Ship displacement =* 19,105 tons standard<ref name="Topliss249">Topliss, "The Brazilian Dreadnoughts," 249.</ref>{{efn-ua|Topliss includes specific displacement figures for ''São Paulo'' which differ from ''Minas Geraes''.}}
* 21,370 tons full load<ref name="Topliss249" />
  
| Ship length =* {{convert|500|ft|m|abbr=on}} [[length between perpendiculars|p.p]]<ref name="Topliss249" />
* {{convert|543|ft|m|abbr=on}} [[length overall|overall]]<ref name="Topliss249" />
  
| Ship beam = {{convert|83|ft|m|abbr=on}}<ref name="Topliss249" />
| Ship height = 
| Ship draft =* {{convert|24|ft|8.75|in|m|abbr=on}} [[normal displacement|normal]]<ref name="Topliss249" />
* {{convert|27|ft|3|in|abbr=on}} [[full-load displacement|full&nbsp;load]]<ref name="Topliss249" />
  
| Ship power =* 23,500 [[shaft horsepower|shp]] (17,524 kW; design)<ref name="Conways" />
* 23,400 [[indicated horsepower|ihp]] (design)<ref name="Topliss251">Topliss, "The Brazilian Dreadnoughts," 251.</ref>
* 27,500 ihp (actual)<ref name="Topliss251" />
  
| Ship propulsion =* 2-shaft reciprocating vertical triple-expansion ([[triple expansion engine|VTE]]) steam engines<ref name="Topliss250">Topliss, "The Brazilian Dreadnoughts," 250.</ref>
* 18 [[Babcock & Wilcox]] boilers<ref name="Topliss250" />
  
| Ship speed = {{convert|21.5|knots}}<ref name="Topliss251" />
| Ship range = 
| Ship endurance = 10,000&nbsp;nautical&nbsp;miles @&nbsp;10&nbsp;knots (11,500&nbsp;mi @ 11.5&nbsp;mph or 18,500&nbsp;km @ 18.5&nbsp;km/h)<ref name="Conways" />
| Ship boats = 
| Ship troops = 
| Ship complement = 
| Ship sensors = 
| Ship EW = 
| Ship armament =* 12 × [[EOC 12 inch /45 naval gun|{{convert|12|in|mm|adj=on|sigfig=4}} main guns]] (6 × 2)<ref name="Topliss249" />
* 22 × {{convert|4.7|in|mm|abbr=on}} guns<ref name="Topliss250" />
*  8 × 3-pounder guns
  
| Ship armour =* [[Belt armor|Belt]]: {{convert|9|to|3|in|mm|abbr=on}} (upper belt 9&nbsp;in)<ref name="Topliss250" />
* Upper [[deck armor|deck]]: {{convert|1.5|in|mm|abbr=on}}<ref name="Topliss250" />
* Main deck: {{convert|2|in|mm|abbr=on}}<ref name="Topliss250" />
* [[gun turret|Turrets]]: {{convert|12|in|mm|abbr=on}} face, {{convert|8|in|mm|abbr=on}} sides, {{convert|3|to|2|in|mm|abbr=on}} roofs<ref name="Topliss250" />
* [[Barbette]]s: {{convert|9|in|mm|abbr=on}}<ref name="Topliss250" />
* Conning tower: {{convert|12|in|mm|abbr=on}},{{convert|2|in|mm|abbr=on}} sides and roof<ref name="Topliss250" />
  
| Ship notes = Characteristics are as built; ''cf.'' [[Minas Geraes-class battleship#Specifications|Specifications of the ''Minas Geraes''-class battleships]]
}}
|}

'''''São Paulo''''' was a [[dreadnought]] [[battleship]] designed and built by the British companies [[Armstrong Whitworth]] and [[Vickers#Vickers, Sons & Maxim|Vickers]], respectively, for the [[Brazilian Navy]]. She was the second of two ships in the {{sclass-|Minas Geraes|battleship|4}}, and was named after the [[São Paulo (state)|state]] and [[São Paulo|city]] of São Paulo.

''São Paulo'' was [[ship naming and launching|launched]] on 19 April 1909 and [[ship commissioning|commissioned]] on 12 July 1910. Soon after, she was involved in the [[Revolt of the Lash]] (''Revolta de Chibata''), in which crews on four Brazilian warships mutinied over poor pay and harsh punishments for even minor offenses. After entering the First World War, Brazil offered to send ''São Paulo'' and her [[Sister ship|sister]] {{ship|Brazilian battleship|Minas Geraes||2}} to Britain for service with the [[Grand Fleet]], but Britain declined since both vessels were in poor condition and lacked the latest fire control technology. In June 1918 Brazil sent ''São Paulo'' to the United States for a full refit that was not completed until 7 January 1920, well after the war had ended. On 6 July 1922, ''São Paulo'' [[Fire in anger|fired her guns in anger]] for the first time when she attacked a fort that had been taken during the [[Tenente revolts]]. Two years later, mutineers took control of the ship and sailed her to [[Montevideo]] where they obtained [[Right of asylum|asylum]].

In the 1930s, ''São Paulo'' was passed over for modernization due to her poor condition—she could only reach a top speed of {{convert|10|knots}}, less than half her design speed. For the rest of her career, the ship was reduced to a reserve coastal defense role. When Brazil entered the Second World War, ''São Paulo'' sailed to the port of [[Recife]] and remained there as the port's main defense for the duration of the war. [[wikt:stricken|Stricken]] in 1947, the dreadnought remained as a [[training vessel]] until 1951, when she was taken under tow to be scrapped in the United Kingdom. The tow lines broke during a strong gale on 6 November, when the ships were {{convert|150|nmi|abbr=on}} north of the [[Azores]], and the ''São Paulo'' was lost.

== Background ==
{{main|South American dreadnought race|Minas Geraes-class battleship|l2=''Minas Geraes''-class battleship}}

Beginning in the late 1880s, Brazil's navy fell into obsolescence, a situation exacerbated by an [[Decline and fall of Pedro II of Brazil#Fall|1889 revolution]], which deposed [[Politics of the Empire of Brazil|Emperor]] [[Dom (title)|Dom]] [[Pedro II of Brazil|Pedro II]], and an [[Brazilian Naval Revolt|1893 civil war]].<ref name="Topliss240">Topliss, "The Brazilian Dreadnoughts," 240.</ref><ref name="Livermore32">Livermore, "Battleship Diplomacy," 32.</ref><ref name="Martins75">Martins, "Colossos do mares," 75.</ref>{{efn-ua|The civil war began in the southern province of [[Rio Grande do Sul]]. Later in 1893, Rear Admiral [[Custódio José de Mello]], the minister of the navy, revolted against President [[Floriano Peixoto]], bringing nearly all of the Brazilian warships currently in the country with him. Mello's forces took [[Florianópolis|Desterro]] when the governor surrendered, and began to coordinate with the revolutionaries in Rio Grande do Sul, but loyal Brazilian forces eventually overwhelmed them both. Most of the rebel naval forces were sailed to Argentina, where their crews surrendered; the flagship, {{ship|Brazilian battleship|Aquidabã||2}}, held out near Desterro until sunk by a torpedo boat.<ref>Scheina, ''Naval History'', 67–76, 352.</ref>}} Despite having nearly three times the population of Argentina and almost five times the population of Chile,<ref name="Livermore32" /><ref name="Conways403">Scheina, "Brazil," 403.</ref> by the end of the 19th century Brazil was lagging behind the Chilean and Argentine navies in quality and total tonnage.<ref name="Livermore32" /><ref name="Martins75" />{{efn-ua|Chile's naval tonnage was {{convert|36896|LT|t}}, Argentina's {{convert|34425|LT|t}}, and Brazil's {{convert|27661|LT|t}}.<ref name="Livermore32" /> For an account of the Argentinian–Chilean naval arms races, see Scheina, ''Naval History'', 45–52.}}

At the turn of the 20th century, soaring demand for [[São Paulo (state)#Early 20th century|coffee]] and [[rubber boom|rubber]] brought prosperity to the Brazilian economy.<ref name="Conways403" /> The government of Brazil used some of the extra money from this economic growth to finance a naval building program in 1904,<ref name="Conways">Scheina, "Brazil," 404.</ref> which authorized the construction of a large number of warships, including three battleships.<ref>Scheina, ''Naval History'', 80.</ref><ref>English, ''Armed Forces'', 108.</ref> The Minister of the Navy, Admiral [[Júlio César de Noronha]], signed a contract with [[Armstrong Whitworth]] for three battleships on 23 July 1906.<ref>Topliss, "The Brazilian Dreadnoughts," 240–245.</ref> The new [[dreadnought]] battleship design, which debuted in December 1906 with the completion of [[HMS Dreadnought (1906)|the namesake ship]], rendered the Brazilian ships, and all other existing capital ships, obsolete.<ref name="Topliss246">Topliss, "The Brazilian Dreadnoughts," 246.</ref> The money authorized for naval expansion was redirected by the new Minister of the Navy, Rear Admiral [[Alexandrino Fario de Alencar]], to building two dreadnoughts, with plans for a third dreadnought after the first was completed, two scout cruisers (which became the {{sclass-|Bahia|cruiser|4}}), ten destroyers (the {{sclass-|Pará|destroyer (1908)|4}}), and three submarines.<ref name="Scheina81">Scheina, ''Naval History'', 81.</ref><ref>''Journal of the American Society of Naval Engineers'', "Brazil," 883.</ref> The three battleships on which construction had just begun were scrapped beginning on 7 January 1907, and the design of the new dreadnoughts was approved by the Brazilians on 20 February 1907.<ref name="Topliss246" /> In South America, the ships came as a shock and kindled a [[South American dreadnought race|naval arms race among Brazil, Argentina, and Chile]]. The 1902 treaty between the latter two was canceled upon the Brazilian dreadnought order so both could be free to build their own dreadnoughts.<ref name="Livermore32"/>

[[File:Minas Geraes-class battleships.jpg|thumb|left|Line drawing of a ''Minas Geraes''-class battleship]]

{{ship|Brazilian battleship|Minas Geraes||2}}, the [[lead ship]], was laid down by Armstrong on 17 April 1907, while ''São Paulo'' followed thirteen days later at Vickers.<ref name="Conways" /><ref name="Topliss249" /><ref name="Appendix7">Scheina, ''Naval History'', 321.</ref> The news shocked Brazil's neighbors, especially Argentina, whose [[Ministry of Foreign Affairs, International Trade and Worship|Minister of Foreign Affairs]] remarked that either ''Minas Geraes'' or ''São Paulo'' could destroy the entire Argentine and Chilean fleets.<ref name="Martins76">Martins, "Colossos do mares," 76.</ref> In addition, Brazil's order meant that they had laid down a dreadnought before many of the other major maritime powers, such as Germany, France or Russia,{{efn-ua|Although Germany laid down {{SMS|Nassau||2}} two months after ''Minas Geraes'', ''Nassau'' was commissioned first.<ref name="Conways" /><ref>Campbell, "Germany," 145.</ref>}} and the two ships made Brazil just the third country to have dreadnoughts under construction, behind the United Kingdom and the United States.<ref name="Conways403" /><ref>Whitley, ''Battleships'', 13.</ref> Newspapers and journals around the world, particularly in Britain and Germany, speculated that Brazil was acting as a proxy for a naval power which would take possession of the two dreadnoughts soon after completion, as they did not believe that a previously insignificant geopolitical power would contract for such powerful warships.<ref name="Conways" /><ref>Martins, "Colossos do mares," 77.</ref> Despite this, the United States actively attempted to court Brazil as an ally; caught up in the spirit, U.S. naval journals began using terms like "Pan Americanism" and "Hemispheric Cooperation".<ref name="Conways" />

== Early career ==

''São Paulo'' was [[Ship naming and launching|christened]] by Mrs. Régis de Oliveira, the wife of Brazil's minister to Great Britain, and [[Ship naming and launching|launched]] at Barrow-in-Furness on 19 April 1909 with many South American diplomats and naval officers in attendance.<ref>"[https://select.nytimes.com/gst/abstract.html?res=F00A16FB3E5D12738DDDA90A94DC405B898CF1D3 Launch Brazil's Battleship]," ''[[The New York Times]]'', 20 April 1909, 5.</ref> The ship was [[ship commissioning|commissioned]] on 12 July,<ref name="NGB">"E São Paulo," ''Navios De Guerra Brasileiros''.</ref><ref name="Official">"São Paulo I," Serviço de Documentação da Marinha&nbsp;— Histórico de Navios.</ref> and after [[fitting-out]] and [[sea trial]]s,<ref name="Official" /> she left [[Greenock]] on 16 September 1910.<ref name="Whitley28">Whitley, ''Battleships'', 28.</ref> Shortly thereafter, she stopped in [[Cherbourg-Octeville|Cherbourg]], France, to embark the [[President of Brazil|Brazilian President]] [[Hermes Rodrigues da Fonseca]].<ref>"[https://query.nytimes.com/gst/abstract.html?res=9F00E2DD1F39E333A25756C2A96F9C946196D6CF French Criticise Brazil]," ''The New York Times'', 25 September 1910, C4.</ref> Departing on the 27th,<ref>"Marshal Hermes Da Fonseca," ''[[The Times]]'', 28 September 1910, 4e.</ref> ''São Paulo'' sailed to [[Lisbon]], Portugal, where Fonseca was a guest of Portugal's King [[Manuel II of Portugal|Manuel II]]. Soon after they arrived, the [[5 October 1910 revolution]] began, which caused the fall of the Portuguese monarchy.<ref name="Keeping Good Order">"[https://query.nytimes.com/gst/abstract.html?res=9D03E1DB1F39E333A2575BC0A9669D946196D6CF Keeping Good Order in New Republic]," ''The New York Times'', 8 October 1910, 1–2.</ref> Although the president offered political asylum to the king and his family, the offer was refused.<ref name="Poder">Ribeiro, "Os Dreadnoughts."</ref> A rumor that the king was on board, circulated by newspapers and reported to the Brazilian legation in Paris,<ref>"[https://news.google.com/newspapers?id=wfZUAAAAIBAJ&sjid=CZMDAAAAIBAJ&pg=4647,545388 King Manuel Takes Flight Aboard Brazilian Warship]," ''[[The Age]]'', 7 October 1910, 7.</ref><ref>"[https://news.google.com/newspapers?id=4rJCAAAAIBAJ&sjid=XqsMAAAAIBAJ&pg=1631,2206504 Europe Stirred By Lisbon News]," ''The Telegraph-Herald'', 5 October 1910, 1.</ref> led revolutionaries to attempt to search the ship, but they were denied permission. They also asked for Brazil to land marines "to help in the maintenance of order", but this request was also denied.<ref>"The Journey from Lisbon," ''The Times'', 8 October 1910, 5–6a.</ref> ''São Paulo'' left Lisbon on 7 October for Rio de Janeiro,<ref name="Keeping Good Order" /><ref>"Movements of Warships," ''The Times'', 8 October 1910, 6a.</ref> and docked there on 25 October.<ref name="Whitley28" />

=== Revolt of the Lash ===

[[File:Joao Candido.jpg|thumb|240px|[[João Cândido Felisberto]] (front row, directly to the left of the man in the dark suit) with reporters, officers and sailors onboard ''Minas Geraes'' on 26 November 1910, the last day of the Revolt of the Lash]]
{{Main article|Revolt of the Lash}}

Soon after ''São Paulo''{{'}}s arrival, a major rebellion known as the Revolt of the Lash, or ''Revolta da Chibata'', broke out on four of the newest ships in the Brazilian Navy. The initial spark was provided on 16 November 1910 when [[Afro-Brazilian]] sailor [[Marcelino Rodrigues Menezes]] was brutally flogged 250 times for insubordination.{{efn-ua|The sailor's back was later described by [[José Carlos de Carvalho]], a retired navy [[Captain (nautical)|captain]] assigned to be the Brazilian government's representative to the mutineers, as "a mullet sliced open for salting."<ref>Quoted in Morgan, "The Revolt of the Lash," 41.</ref>}} Many Afro-Brazilian sailors were sons of former slaves, or were former slaves freed under the ''[[Lei Áurea]]'' (abolition) but forced to enter the navy. They had been planning a revolt for some time, and Menezes became the catalyst. Further preparations were needed, so the rebellion was delayed until 22 November. The crewmen of ''Minas Geraes'', ''São Paulo'', the twelve-year-old {{ship|Brazilian battleship|Deodoro||2}}, and the new {{ship|Brazilian cruiser|Bahia||2}} quickly took their vessels with only a minimum of bloodshed: two officers on ''Minas Geraes'' and one each on ''São Paulo'' and ''Bahia'' were killed.<ref>Morgan, "The Revolt of the Lash," 32–38, 50.</ref>

The ships were well-supplied with foodstuffs, ammunition, and coal, and the only demand of mutineers—led by [[João Cândido Felisberto]]—was the abolition of "slavery as practiced by the Brazilian Navy". They objected to low pay, long hours, inadequate training, and punishments including ''bolo'' (being struck on the hand with a [[ferrule]]) and the use of whips or lashes (''chibata''), which eventually became a symbol of the revolt. By the 23rd, the National Congress had begun discussing the possibility of a general [[amnesty]] for the sailors. Senator [[Ruy Barbosa]], long an opponent of slavery, lent a large amount of support, and the measure unanimously passed the [[Senate of Brazil|Federal Senate]] on 24 November. The measure was then sent to the [[Chamber of Deputies of Brazil|Chamber of Deputies]].<ref>Morgan, "The Revolt of the Lash," 40–42.</ref>

Humiliated by the revolt, naval officers and the president of Brazil were staunchly opposed to amnesty, so they quickly began planning to assault the rebel ships. The officers believed such an action was necessary to restore the service's honor. The rebels, believing an attack was imminent, sailed their ships out of [[Guanabara Bay]] and spent the night of 23–24 November at sea, only returning during daylight. Late on the 24th, the President ordered the naval officers to attack the mutineers. Officers crewed some smaller warships and the cruiser {{ship|Brazilian cruiser|Rio Grande do Sul||2}}, ''Bahia''{{'}}s sister ship with ten 4.7-inch guns. They planned to attack on the morning of the 25th, when the government expected the mutineers would return to Guanabara Bay. When they did not return and the amnesty measure neared passage in the Chamber of Deputies, the order was rescinded. After the bill passed 125–23 and the president signed it into law, the mutineers stood down on the 26th.<ref>Morgan, "The Revolt of the Lash," 44–46.</ref>

During the revolt, the ships were noted by many observers to be well handled, despite a previous belief that the Brazilian Navy was incapable of effectively operating the ships even before being split by a rebellion. João Cândido Felisberto ordered all liquor thrown overboard, and discipline on the ships was recognized as exemplary. The 4.7-inch guns were often used for shots over the city, but the 12-inch guns were not, which led to a suspicion among the naval officers that the rebels were incapable of using the weapons. Later research and interviews indicate that ''Minas Geraes''{{'}} guns were fully operational, and while ''São Paulo''{{'}}s could not be turned after salt water contaminated the [[hydraulic]] system, British engineers still on board the ship after the voyage from the United Kingdom were working on the problem. Still, historians have never ascertained how well the mutineers could handle the ships.<ref>Morgan, "The Revolt of the Lash," 39–40, 48–49, 52.</ref>

[[File:USS Nebraska experimental camouflage.tiff|thumb|left|USS ''Nebraska'' in 1918. ''Nebraska'' escorted ''São Paulo'' to New York after 14 of her 18 boilers failed]]

=== First World War ===
{{See also|Brazil during World War I}}

The Brazilian government declared that the country would be neutral in the [[World War I|First World War]] on 4 August 1914. The sinking of Brazilian merchant ships by German [[U-boat]]s led them to revoke their neutrality, then declare war on 26 October 1917.<ref>Scheina, ''Latin America's Wars'', 35–36.</ref> By this time, ''São Paulo'' was no longer one of the world's most powerful battleships. Despite an identified need for more modern fire control,<ref name="NGB" /> she had not been fitted with any of the advances in that technology that had appeared since her construction, and she was in poor condition.<ref>Scheina, ''Latin America's Wars'', 37.</ref> For these reasons the [[Royal Navy]] declined a Brazilian offer to send her and ''Minas Geraes'' to serve with the [[Grand Fleet]].<ref name="Conways" /> In an attempt to bring the battleship up to international standards, Brazil sent ''São Paulo'' to the United States in June 1918 to receive a full refit. Soon after she departed the naval base in Rio de Janeiro, fourteen of the eighteen [[boiler]]s powering the dreadnought broke down.<ref name="Whitley28" /> The American battleship {{USS|Nebraska|BB-14|2}}, which was in the area after transporting the body of the late Uruguayan Minister to the United States to Montevideo,<ref>"[https://web.archive.org/web/20040314002542/http://www.history.navy.mil:80/danfs/n3/nebraska.htm Nebraska]," ''[[Dictionary of American Naval Fighting Ships]]'', [[Naval History & Heritage Command]], last modified 7 July 2010.</ref> rendered assistance in the form of temporary repairs after the ships put in at [[Salvador, Bahia|Bahia]]. Escorted by ''Nebraska'' and another American ship, {{USS|Raleigh|C-8|2}}, ''São Paulo'' made it to the [[New York Naval Yard]] after a 42-day journey.<ref name="Conways" /><ref name="Whitley28" />{{Clear}}

== Major refit and the 1920s ==
[[File:Brazilian battleship Sao Paulo.jpg|thumb|300px|right|''São Paulo'' seen at an unknown point in her career]]
In New York, ''São Paulo'' underwent a refit, beginning on 7 August 1918 and completing on 7 January 1920.<ref name="Whitley27" /> Many of her crewmen were assigned to American warships during this time for training.<ref name = "Lind">Lind, "Professional Notes," 452.</ref> She received [[Sperry Corporation|Sperry]] fire control equipment and [[Bausch and Lomb]] range-finders for the two [[Superfire|superfiring]] [[Gun turret|turrets]] [[Bow (ship)|fore]] and [[aft]]. A vertical armor [[Bulkhead (partition)|bulkhead]] was fitted inside all six main turrets, and the secondary battery of {{convert|4.7|in|mm|abbr=on}} [[casemate]] guns was reduced from twenty-two to four guns. A few modern [[Anti-aircraft warfare|AA]] guns were fitted as well: two [[3"/50 caliber gun]]s from [[Bethlehem Steel]] were added on the aft superstructure, 37&nbsp;mm guns were added near each turret, and [[Ordnance QF 3 pounder Vickers|3 pounders]] were removed from the top of turrets.<ref name="Whitley27" />

After the refit was completed, ''São Paulo'' picked up ammunition in [[Gravesend, Brooklyn|Gravesend]] and sailed to Cuba for firing trials.<ref name="Lind" /> Seven members of the United States' [[Bureau of Standards]] traveled with the ship from New York and observed the operations, which were conducted in the [[Gulf of Guacanayabo]]. After dropping the Americans off in [[Guantánamo Bay]],<ref>Scheina, ''Latin America'', 134.</ref><ref>Department of Commerce, ''Reports'', 365–66.</ref> ''São Paulo'' returned home in early 1920.<ref name = "Whitley28" /> August 1920 saw the dreadnought sailing to Belgium, where King [[Albert I of Belgium|Albert I]] and Queen [[Elisabeth of Bavaria (1876–1965)|Elisabeth]] were embarked on 1 September to bring them to Brazil.<ref name="NGB" /><ref>"[https://query.nytimes.com/gst/abstract.html?res=9C04E1DB103FE432A25752C0A96F9C946195D6CF King Albert and His Queen Sail for Brazil Today]," ''The New York Times'', 1 September 1920, 1.</ref> After bringing the royals home, ''São Paulo'' traveled to Portugal to bring the remains of the former emperor Pedro II and his wife, [[Teresa Cristina of the Two Sicilies|Teresa Cristina]], back to Brazil.<ref name="NGB" /><ref>Whitley, ''Battleships'', 28–29.</ref>{{efn-ua|''cf.'' [[Legacy of Pedro II of Brazil]].}}

In 1922, ''São Paulo'' and ''Minas Geraes'' helped to put down the first of the [[Tenente revolts]]. Soldiers seized [[Fort Copacabana]] in [[Rio de Janeiro]] on 5 July, but no other men joined them. As a result, some men deserted the rebels, and by the next morning only 200 people remained in the fort.<ref name ="Scheina123">Scheina, ''Latin America's Wars'', 128.</ref> ''São Paulo'' bombarded the fort, firing five salvos and obtaining at least two hits; the fort surrendered half an hour later.<ref>Poggio, "Um encouraçado contra o forte: 2ª Parte."</ref> The Brazilian Navy's official history reports that one of the hits opened a hole ten meters deep.<ref name= "Official"/>

Crewmen aboard ''São Paulo'' rebelled on 4 November 1924, when First [[Lieutenant (naval)|Lieutenant]] [[Hercolino Cascardo]], seven [[second lieutenant]]s and 260 others commandeered the ship.<ref>Scheina, ''Latin America's Wars'', 129.</ref><ref>"[https://news.google.com/newspapers?id=obstAAAAIBAJ&sjid=2YsFAAAAIBAJ&pg=6781,1432690 Preparing to Take Battleship Home]," [[The Gazette (Montreal)|''The Gazette'']], 12 November 1924, 11.</ref> After the boilers were fired, ''São Paulo''{{'}}s mutineers attempted to entice the crews of ''Minas Geraes'' and the other ships nearby to join.<ref name="Whitley29">Whitley, ''Battleships'', 29.</ref> They were only able to sway the crew of one old [[torpedo boat]] to the cause.<ref name="Whitley29" /> The battleship's crew, angry that ''Minas Geraes'' would not join them, fired a six-pounder at ''Minas Geraes'' that wounded a cook.<ref name="Whitley29" /><ref name="Poder" /> The mutineers then sailed out of the Rio de Janeiro's harbor, where the forts at [[Santa Cruz, Rio de Janeiro|Santa Cruz]] and Copacabana engaged her, damaging ''São Paulo''{{'}}s fire control system and funnel. The forts stopped firing soon after the battleship returned fire due to concern over possible civilian casualties.<ref name="Official" /> The crewmen aboard ''São Paulo'' attempted to join revolutionaries in [[Rio Grande do Sul]], but when they found that the rebel forces had moved inland, they set course for Montevideo, [[Uruguay]].<ref name="Official" /><ref name="Poder" /> They arrived on 10 November, where the rebellious members of the crew disembarked and were granted asylum,<ref name="Whitley29" /><ref name="Official" /> and ''Minas Geraes'', which had been pursuing ''São Paulo'', escorted the wayward ship home to Rio de Janeiro, arriving on the 21st.<ref name="Poder" />

== Late career ==
{{See also|Vargas Era}}
[[File:Monumento ao Almirante Tamandaré, Encouraçado São Paulo 1.JPG|thumb|upright|''São Paulo''{{'}}s [[ship's bell|bell]] at [[Ibirapuera Park]], in the city of [[São Paulo]]]]

In the 1930s, Brazil decided to modernize both ''São Paulo'' and ''Minas Geraes''. ''São Paulo''{{'}}s dilapidated state made this uneconomical; at the time she could sail at a maximum of {{convert|10|knots}}, less than half her design speed.<ref name="Whitley27">Whitley, ''Battleships'', 27.</ref> As a result, while ''Minas Geraes'' was thoroughly refitted from 1931 to 1938 in the [[Rio de Janeiro Naval Yard]],<ref name="Poder" /> ''São Paulo'' was employed as a coast-defense ship, a role in which she remained for the rest of her service life.<ref name="Whitley29" /> During the 1932 [[Constitutionalist Revolution]], she acted as the [[flagship]] of a naval [[blockade]] of Santos.<ref name="Poder" /> After repairs in 1934 and 1935, she returned to lead three naval training exercises. In the same year, accompanied by the Brazilian cruisers {{ship|Brazilian cruiser|Bahia||2}} and {{ship|Brazilian cruiser|Rio Grande do Sul||2}}, the Argentine battleships {{ship|ARA|Rivadavia||2}} and {{ship|ARA|Moreno||2}}, six Argentine cruisers, and a group of destroyers, ''São Paulo'' carried the Brazilian President [[Getúlio Dornelles Vargas]] up the [[Río de la Plata|River Plate]] to [[Buenos Aires]] to meet with the presidents of Argentina and Uruguay.<ref name="NGB" /><ref>"[http://www.time.com/time/magazine/article/0,9171,883398,00.html Argentina: Lobsters, Pigeons, Parades]," ''[[Time (magazine)|Time]]'', 3 June 1935.</ref>

In 1936, the crew of ''São Paulo'', as well as {{ship|Brazilian cruiser|Rio Grande do Sul||2}}'s crew, played in the Liga Carioca de Football's Open Tournament, a cup where many amateur teams had the chance to play the likes of [[Clube de Regatas do Flamengo|Flamengo]] and [[Fluminense Football Club|Fluminense]].<ref>[http://cacellain.com.br/blog/?p=41380 TORNEIO ABERTO CARIOCA – 1936] (in Portuguese)</ref>

As in the First World War, Brazil stayed neutral during the opening years of the [[World War II|Second World War]], until U-boat attacks drove the country to declare war on Germany and Italy on 21 August 1942.<ref>Scheina, ''Latin America{{'}}s Wars'', 163–164.</ref> The age and condition of ''São Paulo'' relegated her to the role of harbor defense ship; she set sail for Recife on 23 November 1942 with the escort of two American destroyers ({{USS|Badger|DD-126|2}} and {{USS|Davis|DD-395|2}}) and served as the main defense of the port for the war, only returning to Rio de Janeiro in 1945.<ref name="NGB" /><ref name="Whitley29" /><ref name="Official" /><ref name="Poder" />

Stricken from the naval register on 2 August 1947,<ref name="Whitley29" /> the ship remained as a [[training vessel]] until August 1951,<ref name="Poder" /> when she was sold to the [[Iron and Steel Corporation of Great Britain]].<ref name="Whitley29" /> After preparing from 5 to 18 September, ''São Paulo'' was given an eight-man caretaker crew and taken under tow by two tugs, ''Dexterous'' and ''Bustler'', in Rio de Janeiro on 20 September 1951 for one last voyage to [[Greenock]] and the breakers.<ref name="Whitley29" /><ref name="Time5oct">"[http://www.webcitation.org/5kh5eHn8P?url=http%3A%2F%2Fuk.geocities.com%2Felliget%40btinternet.com%2FSao_Paulo%2FTheTimes_05Oct1954_Battleship_Lost_During_Tow.png Battleship lost during tow, Inquiry after three years]," ''The Times'', 5 October 1954.</ref> When north of the [[Azores]] in early November,{{efn-ua|Whitley and the Brazilian official histories give 6 November,<ref name="Whitley29" /><ref name="Poder" /> but contemporary newspaper accounts of the sinking use 4 November.<ref name="CDT" /><ref name="CDT" /><ref name="LAT" />}} the two lines snapped during a strong storm.<ref name="Whitley29" /><ref name="Poder" /> American [[Boeing B-17 Flying Fortress|B-17 Flying Fortress]] bombers and British planes were launched to scour the Atlantic for the missing ship.<ref name="CDT">"Planes Fail to Find Warship Lost at Sea," ''Chicago Daily Tribune'', 11 November 1951, 27.</ref><ref>"Towed Warship Missing," ''The New York Times'', 9 November 1951, 49.</ref> The ship was reported as found on the 15th,<ref name="MBL">"Missing Battleship Located," ''The New York Times'', 16 November 1951, 51.</ref> but this proved to be false. The search was ended on 10 December without finding ''São Paulo'' or her crew.<ref name="LAT">"Lost Warship Hunt Given Up," ''Los Angeles Times'', 11 December 1951, 13.</ref>

== Footnotes ==
{{notelist-ua}}

== Endnotes ==
{{reflist|30em}}

== References==
{{portal | Battleships }}
{{refbegin | indent = yes }}
: "[https://books.google.com/books?id=PUVLAAAAMAAJ&pg=PA833 Brazil]." ''Journal of the American Society of Naval Engineers'' 20, no. 3 (1909): 833–836. {{issn|0099-7056}}. {{oclc|3227025}}.
: Campbell, N.J.M. "Germany." In Gardiner and Gray, ''Conway's'', 134–189.
: "[https://web.archive.org/web/20080224085803/http://www.naval.com.br/NGB/S/S031/S031.htm E São Paulo]." ''Navios De Guerra Brasileiros''. Last modified 24 February 2008.
: English, Adrian J. ''Armed Forces of Latin America''. London: Jane's Publishing Inc., 1984. ISBN 0-7106-0321-5. {{oclc|11537114}}.
: Gardiner, Robert and Randal Gray, eds. ''Conway's All the World's Fighting Ships: 1906–1921''. Annapolis: Naval Institute Press, 1984. ISBN 0-87021-907-3. {{oclc|12119866}}.
: Lind, Wallace L. "[https://books.google.com/books?id=URJLAAAAYAAJ&pg=PA452 Professional Notes]." ''Proceedings of the United States Naval Institute'' 46, no. 3 (1920): 437–486.
: [[Seward W. Livermore|Livermore, Seward W]]. "Battleship Diplomacy in South America: 1905–1925." ''The Journal of Modern History'' 16, no. 1 (1944): 31–44. {{jstor|1870986}}. {{issn|0022-2801}}. {{oclc|62219150}}.
: Martins, João Roberto, [[Filho]]. "[https://web.archive.org/web/20101102034637/http://www.revistadehistoria.com.br/v2/home/?go=detalhe&id=1307 Colossos do mares] [Colossuses of the Seas]." ''Revista de História da Biblioteca Nacional'' 3, no. 27 (2007): 74–77. {{issn|1808-4001}}. {{oclc|61697383}}.
: Morgan, Zachary R. "The Revolt of the Lash, 1910." In ''Naval Mutinies of the Twentieth Century: An International Perspective'', edited by Christopher M. Bell and Bruce A. Elleman, 32–53. Portland: Frank Cass Publishers, 2003. ISBN 0-7146-8468-6. {{oclc|464313205}}.
: Poggio, Guilherme. "[https://web.archive.org/web/20090412041258/http://naval.com.br/historia/SP_x_Copacabana/SP_x_Copacabana_p2.htm Um encouraçado contra o forte: 2ª Parte] [A Battleship against the Strong: Part 2]." n.d. Poder Naval Online. Last modified 12 April 2009.
: Ribeiro, Paulo de Oliveira. "[https://web.archive.org/web/20080608045038/http://www.naval.com.br/historia/dreadnought/dreadnought-port1.htm Os Dreadnoughts da Marinha do Brasil: Minas Geraes e São Paulo] [The Dreadnoughts of the Brazilian Navy]." Poder Naval Online. Last modified 8 June 2008.
: "[http://www.sistemas.dphdm.mar.mil.br/navios/Index.asp?codNavio=403 São Paulo I]." ''Serviço de Documentação da Marinha&nbsp;– Histórico de Navios''. Diretoria do Patrimônio Histórico e Documentação da Marinha, Departamento de História Marítima. Accessed 27 January 2015.
: Scheina, Robert L. "Brazil." In Gardiner and Gray, ''Conway's'', 403–407.
: ———. ''Latin America: A Naval History 1810–1987''. Annapolis: Naval Institute Press, 1987. ISBN 0-87021-295-8. {{oclc|15696006}}.
: ———. ''Latin America's Wars''. Washington, D.C.: Brassey's, 2003. ISBN 1-57488-452-2. {{oclc|49942250}}.
: Topliss, David. "The Brazilian Dreadnoughts, 1904–1914." ''Warship International'' 25, no. 3 (1988): 240–289. {{issn|0043-0374}}. {{oclc|1647131}}.
: [[United States Department of Commerce]]. ''Reports of the Department of Commerce''. Washington, D.C.: Government Printing Office, 1921. {{oclc|1777213}}.
: Villiers, Alan. ''Posted Missing: The Story of Ships Lost Without a Trace in Recent Years''. New York, Charles Scribners' Sons, 1956. Ch. 5: The Battleship Sao Paulo, p. 79-100.
: Whitley, M.J. ''Battleships of World War Two: An International Encyclopedia''. Annapolis: Naval Institute Press, 1998. ISBN 1-55750-184-X. {{oclc|40834665}}.
{{refend}}

== External links ==
{{Commons category multi|São Paulo (1910)|Minas Geraes class battleship}}
* {{YouTube|KQlHAAtGjOo|Slideshow of the battleship}}
* [https://books.google.com/books?id=vG7mAAAAMAAJ&pg=PA21#v=onepage&q&f=false The Brazilian Battleships] (Extensive engineering/technical details)

{{Minas Geraes battleships}}
{{South American dreadnoughts}}
{{1951 shipwrecks}}

{{Featured article}}

{{coord missing|Atlantic Ocean}}

{{DEFAULTSORT:Sao Paulo}}
[[Category:1909 ships]]
[[Category:Barrow-built ships]]
[[Category:Maritime incidents in 1951]]
[[Category:Minas Geraes-class battleships]]
[[Category:Missing ships]]
[[Category:Shipwrecks in the Atlantic Ocean]]
[[Category:Vickers]]
[[Category:Ships lost with all hands]]