{{Infobox journal
| title = Crime, Media, Culture
| cover = [[File:Crime, Media, Culture.jpg]]
| editor = Chris Greer, Mark S. Hamm
| discipline = [[Criminology]], [[media studies]]
| former_names = 
| abbreviation = Crime Media Cult.
| publisher = [[Sage Publications]]
| country =
| frequency = Triannually
| history = 2005-present
| openaccess = 
| license = 
| impact = 0.730
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201682/title
| link1 = http://cmc.sagepub.com/content/current
| link1-name = Online access
| link2 = http://cmc.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 60630237 
| LCCN = 2005206689
| CODEN = 
| ISSN = 1741-6590
| eISSN = 1741-6604
}}
'''''Crime Media Culture''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering research on the relationship between crime, criminal justice, media, and culture. The [[editors-in-chief]] are Chris Greer ([[City University London]]) and Mark S. Hamm ([[Indiana State University]]). It was established in 2005 and is currently published by [[Sage Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.730, ranking it 30th out of 52 journals in the category "Criminology & Penology"<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Criminology & Penology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref> and 79th out of 137 journals in the category "Sociology".<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Sociology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201682/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Criminology journals]]
[[Category:Media studies journals]]
[[Category:Triannual journals]]
[[Category:Publications established in 2005]]