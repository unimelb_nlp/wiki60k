{{other uses}}
{{Use American English|date=July 2014}}{{Use mdy dates|date=July 2014}}
{{Infobox video game
| title = Blek
| image = File:Blek app icon.png
| caption = App icon
| developer = Kunabi Brother
| publisher = Kunabi Brother
| engine = [[Unity (game engine)|Unity]]
| platforms = [[iOS]], [[Android (operating system)|Android]], [[Wii U]]
| released = December 2013
| genre = [[Puzzle game|Puzzle]]
| modes = [[Single-player]]
}}

'''''Blek''''' is a 2013 [[puzzle game|puzzle]] video game for [[iOS]] and [[Android (operating system)|Android]] by Kunabi Brother, a team of brothers Denis and Davor Mikan. The player draws a snakelike black line that recurs in pattern and velocity across the screen to remove colored dots and avoid black dots. It is minimalist in design, features excerpts of [[Erin Gee]], and takes inspiration from [[Golan Levin]], the [[Bauhaus]], and [[Japanese calligraphy]]. The brothers designed the game as a touchscreen adaptation to classic game ''[[Snake (video game)|Snake]]'' and worked on the game for over six months. It was released in December 2013 for [[iPad]], and was later released for other iOS devices and Android.

The game received positive reviews, and critics praised the game's degree of unrestricted play. The game reached the top of the Apple App Store charts several months after its release. It received a 2014 [[Apple Design Award]], and has sold over one million copies.

== Gameplay ==

{{multiple image |footer=Trailer and examples of puzzles |footer_align=center |width=200 |direction=vertical |image1=Blek trailer.webm |image2=Blek - Level 2 screenshot.png |image3=Blek - Level 4 screenshot.png }}

''Blek'' is a [[puzzle game]] in which the player draws a "snake-like" black line on the screen that is recorded and played back like a pattern, recurring repeatedly across the screen.<ref name="Edge review"/> The object is to draw a line that it will remove colored dot targets when it repeats across the screen without hitting a black dot.<ref name="Edge review"/> Lines that travel off the top or bottom of the screen reset the level, while lines that travel off the left or right of the screen reflect back towards the dots. Aside from repeating the player's drawn pattern, the stroke mimics the player's pace in drawing the stroke.<ref name="Eurogamer review"/> The game begins with no prompt or tutorial other than to use a finger on the screen and experiment. The first puzzles are "on open, white canvasses" where the player can solve the simple puzzles "by accident".<ref name="Edge review"/> The 80 [[Level (video gaming)|levels]]<ref name="Eurogamer review"/> progress in difficulty and require more complex solutions. Added elements include a "[[chain reaction]]" dot that launches other dots when struck. Its sound consists of a "whoosh" that accompanies the traveling stroke, a "chime" when colored dots are hit, and a human "disappointed grumble" when black dots are hit, resetting the level.<ref name="Edge review"/> The game is depicted in flat, plain colors, with no pause feature and no option menu other than [[Game Center]] achievements. Players navigate between puzzles using three small onscreen icons.<ref name="TouchArcade review"/> There are no in-app purchases or in-game advertisements.<ref name="Edge: heartening"/>

== Development ==

''Blek'' was built for iOS by brothers Denis and Davor Mikan. While both had coding experience, neither were game developers by trade. This was their first game together as Kunabi Brother. Denis had published short stories and a novel, and Davor released music on a [[Crónica Electrónica]]. Davor previously made [[Flash games]] and developed the idea for ''Blek'' from this experience. He approached Denis about converting the video game ''[[Snake (video game)|Snake]]'' for touchscreens, and Denis returned with the idea of "a line representing an idea that springs to life after it has been drawn".<ref name="Edge: heartening"/> This thought was likely inspired by the calligraphy and ink drawings in a book by Japanese poet [[Matsuo Basho]] that Denis was reading. They had several prototypes by mid-2013, when Davor joined an Parisian [[artist in residence]] program, where he felt he was treated differently when he introduced himself as a game developer instead of as a musician. This experience invigorated his interest in the game medium and led to the brothers' push to finish the game over the next six months.<ref name="Edge: heartening"/>

[[File:Blek producer Denis Mikan.jpg|thumb|left|''Blek'' producer Denis Mikan]] The Austrian<ref name=BI/> brothers' main influences were [[Golan Levin]]'s 1998 interactive ''Yellowtail'' and [[Wassily Kandinsky]]'s ''Point and Line to Plane'' book, from his time teaching at the [[Bauhaus]].<ref name="Edge: heartening"/> The sound design uses excerpts from [[Erin Gee]]'s "Yamaguchi Mouthpiece I",<ref name="Edge: heartening"/><ref name="Illinois: Gee"/> and their game design influences include [[Thatgamecompany]] and Patrick Smith of Vectorpark and ''[[Windosill]]'', though they felt that other games did not singularly influence ''Blek''{{'s}} design. They were interested in video games as toys and "as meaningful experiences".<ref name="Edge: heartening"/> The game was written in the [[Unity (game engine)|Unity game engine]] and tested by the developers' friends. Since the core [[game mechanics]] were set, their feedback pertained to the [[level design]]. As their primary interests were in a "unification of art, craft, and technology", the game had no public relations or marketing campaign and its creators expressed little interest in the app's business and marketing, though they did share the game directly with media outlets.<ref name="Edge: heartening"/>

''Blek'' was released for iPad in December 2013, and an iPhone and iPod Touch version followed on January 7, 2014.<ref name=IndieGames/> Four months after the release, they reinvested their earnings from the game into marketing. After a few YouTube campaigns, ''Blek'' was listed in Apple's App Store lists.<ref name=BI/> An [[Android (operating system)|Android]] version was released in July.<ref name="Polygon: Android"/> Kunabi Brother are not planning a sequel, though they intend to further "experiment with touchscreens".<ref name="Edge: heartening"/>

== Reception ==

{{Video game reviews
| GR = 78<!-- Please only use two digits of precision, per [[Template:Video game reviews/doc#Guidelines]] -->%<ref name=GameRankings/>
| MC = 78/100<ref name=Metacritic/>
| Edge = 8/10<ref name="Edge review"/>
| EuroG = 8/10<ref name="Eurogamer review"/>
| rev1 = ''[[TouchArcade]]''
| rev1Score = {{rating|4|5}}<ref name="TouchArcade review"/>
}}

''Blek'' received "generally favorable reviews", according to video game review score aggregator [[Metacritic]].<ref name=Metacritic/> Though the game first released in December 2013 to little fanfare, critics "widely praised" the game,<ref name="Edge: heartening"/> and it became popular in April 2014.<ref name="TouchArcade: Apple Design Awards"/> It appeared in the top ten paid App Store games chart in April,<ref name="TouchArcade: chart 4"/><ref name="TouchArcade: chart 5"/> reached the top by May,<ref name="TouchArcade: chart 6"/><ref name="TouchArcade: chart 7"/><ref name="Polygon: Tetris"/> and was listed into June.<ref name="TouchArcade: chart June 1"/><ref name="TouchArcade: chart June 2"/> ''Blek'' received a 2014 [[Apple Design Award]]<ref name="Polygon: Apple Design Awards"/> and was featured in their Indie Game Showcase.<ref name="TouchArcade: showcase"/> While it had sold 30,000 copies by February 2014,<ref name="Edge: heartening"/> upon being featured in the App Store, it sold 500,000 copies by May,<ref name=BI/> and over a million copies by June.<ref name="Pocket Gamer: twist"/> ''Edge'' compared its aesthetic to iOS puzzle game ''[[Hundreds (video game)|Hundreds]]''.<ref name="Edge review"/> Reviewers praised the game for the amount of freedom it affords its players.<ref name="Eurogamer review"/><ref name="TouchArcade review"/>

[[File:Hundreds (video game) - Level 61.png|thumb|''Edge'' compared the game's aesthetic to ''Hundreds'', another iOS puzzle game]] ''Edge'' called ''Blek'' "a thing of elegant, intuitive beauty".<ref name="Edge review"/> They compared it to a "modernist, freeform, [[touchscreen]]" ''[[Snake (video game)|Snake]]'', albeit much calmer, and described ''Blek'' as less a puzzle game than "pure intuition" and "an act of freeform creation" that privileged the process of experimentation over the goal of solving puzzles.<ref name="Edge review"/> The magazine wrote that the [[trial and error]], muddling process of refining one's stroke led to delightful discoveries that turned "maddening" complex prospects into "natural" solutions.<ref name="Edge review"/> In a piece for ''Polygon'', Rod Green compared ''Blek'' to ''[[Tetris]]'' and ''[[Threes!]]'' as a "simple premise, beautifully executed" that lends towards imitation, and added that the game would be harder to "[[video game clone|clone]]" than the others due to its handmade levels.<ref name="Polygon: Tetris"/>{{Efn|The game later was cloned, with some examples including ''Blek'' in their titles.<ref name="TouchArcade: clone"/><ref name="Gamezebo: clone"/><ref name="The Verge: clone"/>}} ''Kotaku''{{'s}} Mike Fahey called it "the most brilliant iPad game" he played in 2013.<ref name="Kotaku: brilliant"/>

Christian Donlan of ''Eurogamer'' wrote that the game is personal. He compared its core mechanics to handwriting and doodling, noting that the recurring stroke also captures the player's "speed and hesitancy".<ref name="Eurogamer review"/> Donlan wrote that the game is "lots of kinds of puzzle games" as the player may read negative spacing or try to predict the motion of a reflected stroke, and compared the later stages to mazes or minefields.<ref name="Eurogamer review"/> Shaun Musgrave of ''TouchArcade'' noted that the game's difficulty increases around level 20, where player precision is required, and considered this part a low point. He felt that the small margin of error in later levels lent towards frustration.<ref name="TouchArcade review"/> Jared Nelson of the same website wrote that the game was uniquely suited for the touchscreen.<ref name="TouchArcade: Apple Design Awards"/>

== Notes and references ==

; Notes

{{notelist}}

; References

{{reflist|25em|refs=

<ref name=BI>{{cite web |url=http://www.businessinsider.com/blek-cofounder-explains-the-apps-success-2014-5 |accessdate=July 21, 2014 |title=Blek Founder Tells Us How He Got To No.1 In The App Store For 20 Straight Days |last1=Borison |first1=Rebecca |date=May 21, 2014 |work=Business Insider |archiveurl=https://web.archive.org/web/20140522104144/http://www.businessinsider.com/blek-cofounder-explains-the-apps-success-2014-5 |archivedate=May 22, 2014 |deadurl=no }}</ref>

<ref name="Edge: heartening">{{cite web |url=http://www.edge-online.com/features/the-heartening-story-of-blek-the-ios-game-that-cut-through-app-store-cynicism-with-pure-creativity/ |accessdate=July 20, 2014 |title=The heartening story of Blek, the iOS game that cut through App Store cynicism with pure creativity |last1=Long |first1=Neil |date=February 12, 2014 |work=[[Edge (magazine)|Edge]] |publisher=[[Future plc|Future]] |archiveurl=https://web.archive.org/web/20140721051959/http://www.edge-online.com/features/the-heartening-story-of-blek-the-ios-game-that-cut-through-app-store-cynicism-with-pure-creativity/ |archivedate=July 21, 2014 |deadurl=yes }}</ref>

<ref name="Edge review">{{cite web|url=http://www.edge-online.com/review/blek-review/ |accessdate=July 20, 2014 |title=Blek review |author=Edge Staff |date=December 6, 2013 |work=[[Edge (magazine)|Edge]] |publisher=[[Future plc|Future]] |archiveurl=http://www.webcitation.org/6RDNyMeXN?url=http%3A%2F%2Fwww.edge-online.com%2Freview%2Fblek-review%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Eurogamer review">{{cite web|url=http://www.eurogamer.net/articles/2014-03-31-blek-review |accessdate=July 20, 2014 |work=[[Eurogamer]] |publisher=Gamer Network |title=Blek review |last1=Donlan |first1=Christian |date=March 24, 2014 |archiveurl=http://www.webcitation.org/6RDO5OrzZ?url=http%3A%2F%2Fwww.eurogamer.net%2Farticles%2F2014-03-31-blek-review |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name=GameRankings>{{cite web|url=http://www.gamerankings.com/iphone/739744-blek/index.html |title=Blek for iOS (iPhone/iPad) |work=[[GameRankings]] |publisher=[[CBS Interactive]] |accessdate=July 13, 2014 |archiveurl=http://www.webcitation.org/6R2qLTldw?url=http%3A%2F%2Fwww.gamerankings.com%2Fiphone%2F739744-blek%2Findex.html |archivedate=July 13, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Gamezebo: clone">{{cite web|url=http://www.gamezebo.com/2014/06/27/cloned-blek-sets-new-standard-shameless-clones/ |accessdate=July 21, 2014 |title=Cloned Blek Sets a New Standard for Shameless Clones |last1=Dotson |first1=Carter |date=June 27, 2014 |work=[[Gamezebo]] |archiveurl=http://www.webcitation.org/6RE1UuyDL?url=http%3A%2F%2Fwww.gamezebo.com%2F2014%2F06%2F27%2Fcloned-blek-sets-new-standard-shameless-clones%2F |archivedate=July 21, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Illinois: Gee">{{cite web|url=http://news.illinois.edu/news/13/1216video_game_musicErinGee.html |accessdate=July 22, 2014 |title=Video game features music by honored U. of I. composition professor |last1=Rhodes |first1=Dusty |date=December 16, 2013 |publisher=University of Illinois |archiveurl=http://www.webcitation.org/6RGy1uh6g?url=http%3A%2F%2Fnews.illinois.edu%2Fnews%2F13%2F1216video_game_musicErinGee.html |archivedate=July 22, 2014 |deadurl=no |df=mdy }}</ref>

<ref name=IndieGames>{{cite web|url=http://indiegames.com/2013/12/blek.html |accessdate=July 21, 2014 |title=Stroke of genius Blek out now for iPad, in January for iPhone |last1=Polson |first1=John |date=December 4, 2013 |work=IndieGames.com |publisher=[[UBM Tech]] |archiveurl=http://www.webcitation.org/6RE0z4vbw?url=http%3A%2F%2Findiegames.com%2F2013%2F12%2Fblek.html |archivedate=July 21, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Kotaku: brilliant">{{cite web|url=http://kotaku.com/the-most-brilliant-ipad-game-ive-played-all-year-1484414921 |accessdate=July 21, 2014 |work=[[Kotaku]] |publisher=[[Gawker Media]] |title=The Most Brilliant iPad Game I've Played All Year |last=Fahey |first=Mike |date=December 16, 2013 |archiveurl=http://www.webcitation.org/6RE1DGF0g?url=http%3A%2F%2Fkotaku.com%2Fthe-most-brilliant-ipad-game-ive-played-all-year-1484414921 |archivedate=July 21, 2014 |deadurl=no |df=mdy }}</ref>

<ref name=Metacritic>{{cite web|url=http://www.metacritic.com/game/ios/blek/critic-reviews |title=Blek Critic Reviews for iPhone/iPad |work=[[Metacritic]] |publisher=[[CBS Interactive]] |accessdate=July 13, 2014 |archiveurl=http://www.webcitation.org/6R2pEdBQC?url=http%3A%2F%2Fwww.metacritic.com%2Fgame%2Fios%2Fblek%2Fcritic-reviews |archivedate=July 13, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Pocket Gamer: twist">{{cite web|url=http://www.pocketgamer.biz/feature/59375/twist-and-shout-the-making-of-blek/ |accessdate=July 22, 2014 |title=Twist and shout: The making of Blek |last1=Mckeand |first1=Kirk |date=June 25, 2014 |work=[[Pocket Gamer]] |publisher=Steel Media |archiveurl=http://www.webcitation.org/6RGyYRlOW?url=http%3A%2F%2Fwww.pocketgamer.biz%2Ffeature%2F59375%2Ftwist-and-shout-the-making-of-blek%2F |archivedate=July 22, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Polygon: Android">{{cite web|url=http://www.polygon.com/2014/7/7/5878597/blek-android-launch |accessdate=July 21, 2014 |title=You can now play minimalist puzzle game Blek on Android&nbsp;devices |last=Farokhmanesh |first=Megan |date=July 7, 2014 |work=[[Polygon (website)|Polygon]] |publisher=[[Vox Media]] |archiveurl=http://www.webcitation.org/6RE1jzUIJ?url=http%3A%2F%2Fwww.polygon.com%2F2014%2F7%2F7%2F5878597%2Fblek-android-launch |archivedate=July 21, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Polygon: Apple Design Awards">{{cite web|url=http://www.polygon.com/2014/6/3/5776986/games-apple-design-awards-2014 |accessdate=July 13, 2014 |title=Threes!, Monument Valley and more games win Apple Design Awards |last=Tach |first=Dave |date=June 3, 2014 |work=[[Polygon (website)|Polygon]] |publisher=[[Vox Media]] |archiveurl=http://www.webcitation.org/6R2pAQQ0V?url=http%3A%2F%2Fwww.polygon.com%2F2014%2F6%2F3%2F5776986%2Fgames-apple-design-awards-2014 |archivedate=July 13, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="Polygon: Tetris">{{cite web|url=http://www.polygon.com/2014/5/23/5744678/tetris-mobile-free-to-play |accessdate=July 13, 2014 |title=The creation of Tetris would be impossible in today's mobile&nbsp;market |last=Green |first=Rod |date=May 23, 2014 |work=[[Polygon (website)|Polygon]] |publisher=[[Vox Media]] |archiveurl=http://www.webcitation.org/6R2pCeVMh?url=http%3A%2F%2Fwww.polygon.com%2F2014%2F5%2F23%2F5744678%2Ftetris-mobile-free-to-play |archivedate=July 13, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="The Verge: clone">{{cite web|url=http://www.theverge.com/2014/6/27/5849462/blek-clone-app-store |accessdate=July 21, 2014 |title=Bad clone of award-winning game 'Blek' is literally called 'Cloned Blek' |last1=Webster |first1=Andrew |date=June 27, 2014 |work=[[The Verge]] |publisher=[[Vox Media]] |archiveurl=http://www.webcitation.org/6RE1cKcRV?url=http%3A%2F%2Fwww.theverge.com%2F2014%2F6%2F27%2F5849462%2Fblek-clone-app-store |archivedate=July 21, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: Apple Design Awards">{{cite web|url=http://toucharcade.com/2014/06/03/2014-apple-design-award-winners/ |accessdate=July 20, 2014 |title='Monument Valley', 'Device 6' and 'Threes!' Among the Five Games to Win Apple Design Awards |last1=Nelson |first1=Jared |date=June 3, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDW1aLZM?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F06%2F03%2F2014-apple-design-award-winners%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: chart 4">{{cite web|url=http://toucharcade.com/2014/04/22/apples-official-weekly-top-games-chart-4/ |accessdate=July 20, 2014 |title=Apple's Official Weekly Top Games Chart: 4/14 – 4/20 – 'A Dark Room', '2048', 'Minecraft', and 'Hearthstone' Pull Ahead |last1=Hodapp |first1=Eli |date=April 22, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDUmqNUR?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F04%2F22%2Fapples-official-weekly-top-games-chart-4%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: chart 5">{{cite web|url=http://toucharcade.com/2014/04/30/apples-official-weekly-top-games-chart-5/ |accessdate=July 20, 2014 |title=Apple's Official Weekly Top Games Chart: 4/21 – 4/27 – 'A Dark Room', 'Piano Tiles', 'Minecraft', and 'FarmVille 2' |last1=Hodapp |first1=Eli |date=April 30, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDUt6ajC?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F04%2F30%2Fapples-official-weekly-top-games-chart-5%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: chart 6">{{cite web|url=http://toucharcade.com/2014/05/08/apples-official-weekly-top-games-chart-6/ |accessdate=July 20, 2014 |title=Apple's Official Weekly Top Games Chart: 4/28 – 5/4 – 'Blek', 'Piano Tiles', 'Minecraft' and '2048' |last1=Hodapp |first1=Eli |date=May 8, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDVsQv4w?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F05%2F08%2Fapples-official-weekly-top-games-chart-6%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: chart 7">{{cite web|url=http://toucharcade.com/2014/05/20/apples-official-weekly-top-games-chart-7/ |accessdate=July 20, 2014 |title=Apple's Official Weekly Top Games Chart: 5/12 – 5/18, 'Blek', '100 Balls' and 'Minecraft' Pull Ahead Again |last1=Hodapp |first1=Eli |date=May 20, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDVooTaD?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F05%2F20%2Fapples-official-weekly-top-games-chart-7%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: chart June 1">{{cite web|url=http://toucharcade.com/2014/06/10/apples-top-games-blek-twodots-minecraft-bubble-witch-saga-2/ |accessdate=July 20, 2014 |title=Apple's Official Weekly Top Games Chart: 6/2 – 6/8, 'Blek', 'TwoDots', 'Minecraft', and 'Bubble Witch Saga 2' in the Lead |last1=Hodapp |first1=Eli |date=June 10, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDWSySSa?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F06%2F10%2Fapples-top-games-blek-twodots-minecraft-bubble-witch-saga-2%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: chart June 2">{{cite web|url=http://toucharcade.com/2014/06/17/most-downloaded-iphone-games-minecrafttwodots-heads-up-angry-birds-epic/ |accessdate=July 20, 2014 |title=Apple's Official Weekly Top Games Chart: 6/9 – 6/15, 'Heads Up!', 'TwoDots', 'Minecraft' and 'Angry Birds Epic' |last1=Hodapp |first1=Eli |date=June 17, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDUvZk3R?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F06%2F17%2Fmost-downloaded-iphone-games-minecrafttwodots-heads-up-angry-birds-epic%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: clone">{{cite web|url=http://toucharcade.com/2014/07/09/blek-is-cheap-as-well-as-the-apple-design-award-winners-fire-sale-continues/ |accessdate=July 20, 2014 |title='Blek' is Cheap as Well, as the Apple Design Award Winners Fire Sale Continues |last1=Dotson |first1=Carter |date=July 9, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDXYJ6nq?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F07%2F09%2Fblek-is-cheap-as-well-as-the-apple-design-award-winners-fire-sale-continues%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade review">{{cite web|url=http://toucharcade.com/2014/05/08/blek-review/ |accessdate=July 20, 2014 |title='Blek' Review – A Unique And Challenging Puzzle Game |last1=Musgrave |first1=Shaun |date=May 8, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDT3jJkG?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F05%2F08%2Fblek-review%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

<ref name="TouchArcade: showcase">{{cite web|url=http://toucharcade.com/2014/05/29/apple-posts-new-indie-game-showcase-featuring-threes/ |accessdate=July 20, 2014 |title=Apple Posts New Indie Game Showcase Featuring 'Threes!' |last1=Hodapp |first1=Eli |date=May 29, 2014 |work=TouchArcade |archiveurl=http://www.webcitation.org/6RDVkde3j?url=http%3A%2F%2Ftoucharcade.com%2F2014%2F05%2F29%2Fapple-posts-new-indie-game-showcase-featuring-threes%2F |archivedate=July 20, 2014 |deadurl=no |df=mdy }}</ref>

}}

== External links ==

{{Commons category-inline|Blek|''Blek''}}
* {{official website|http://blekgame.com/}}

{{Portal bar|Austria|Video games|2010s|border=yes}}
{{good article}}

[[Category:2013 video games]]
[[Category:Android (operating system) games]]
[[Category:Apple Design Awards recipients]]
[[Category:Indie video games]]
[[Category:IOS games]]
[[Category:IPad games]]
[[Category:Puzzle video games]]
[[Category:Single-player-only video games]]
[[Category:Unity (game engine) games]]
[[Category:Video games developed in Austria]]
[[Category:Wii U eShop games]]
[[Category:Articles containing video clips]]