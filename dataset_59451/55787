{{Infobox Journal
| title = Australian Family Physician
| cover = 
| discipline =  [[Family medicine]]
| abbreviation = Aust. Fam. Physician
| formernames = Annals of General Practice
| publisher = [[Royal Australian College of General Practitioners]]
| country = [[Australia]]
| history = 1956–present
| frequency = Monthly
| impact = 0.759
| impact-year = 2015
| website = http://www.racgp.org.au/afp
| ISSN = 0300-8495
| OCLC = 608167033
}}
The '''''Australian Family Physician''''' is a monthly [[peer-reviewed]] [[medical journal]] published by the [[Royal Australian College of General Practitioners]]. It was established in 1956 as the ''Annals of General Practice'', obtaining its current name in 1971.<ref>Royal Australian College of General Practitioners, [https://archive.is/20120911095847/http://www.racgp.org.au/timeline History timeline], retrieved 16 February 2009.</ref> The journal is abstracted and indexed in [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/0326701 |title=Australian Family Physician |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-10-01}}</ref> and the [[Science Citation Index Expanded]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-10-01}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.759.<ref name=WoS>{{cite book |year=2016 |chapter=Australian Family Physician |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
*{{Official website|http://www.racgp.org.au/afp}}

[[Category:General medical journals|Australian Family Physician]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1956]]


{{med-journal-stub}}