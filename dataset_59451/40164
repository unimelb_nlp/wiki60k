{{Infobox Jewish leader
|name         = Bernard J. Bamberger
|image        = 
|caption      = 
|denomination = [[Reform Judaism|Judaism (Reform)]]
|birth_date   = {{birth date|1904|5|30}}
|birth_place  = [[Baltimore]], [[Maryland]]
|death_date   = {{death date and age|1980|6|14|1904|5|30}}
|death_place  = [[New York City]], [[New York (state)|New York]], U.S.
|spouse       = Ethel Kraus ("Pat") {{small|(1908–2003)}}
|children     = Henry, [[Opera Cleveland|David]]
|profession   = Rabbi
|alma_mater   = [[Johns Hopkins University]]<br>[[Hebrew Union College-Jewish Institute of Religion|Hebrew Union College (Cincinnati)]]
}}

'''Bernard Jacob Bamberger''' (May 30, 1904 – June 14, 1980) was an American rabbi, scholar, author, translator, head of major Jewish organizations, and congregational spiritual leader for over 50 years during the middle decades of the 20th century.<ref>American Jewish Archives, "An Inventory of the Bernard J. Bamberger Papers, Manuscript Collection No. 660"</ref><ref>Temple Shaaray Tefila, Program, "Temple Shaaray Tefila Honors Rev. Dr. Bernard J. Bamberger and Ethel 'Pat' Bamberger on the Occasion of his 75th Birthday," May 19, 1979</ref>

==Biography==
Bernard J. Bamberger was born May 30, 1904, in Baltimore, to which his great-grandfather Abraham had emigrated from Bavaria in 1839.<ref>Stern, Malcom H., compiler, ''First American Jewish Families,'' Third Edition, 1991, Baltimore, MD, p. 14.</ref> He was a brilliant student, rapidly completing high school and entering [[Johns Hopkins University]] in 1920, at the age of 16.  He matriculated in three years (instead of the normal four) and on June 12, 1923, just two weeks after his 19th birthday, Johns Hopkins awarded him the Bachelor of Arts degree with honors, and he was inducted into the  [[Phi Beta Kappa Society]].<ref>Johns Hopkins University, program of "Conferring of Degrees at the Close of the 47th Academic Year," June 12, 1923; ''Johns Hopkins Half-Century Directory''; Temple Shaaray Tefila, op. cit.'''</ref> That fall he entered the [[Hebrew Union College-Jewish Institute of Religion|Hebrew Union College (HUC)]] in Cincinnati, again completing his studies at an accelerated pace.  He was ordained a rabbi on May 29, 1926, the day before his 22nd birthday.  On June 1, 1929, he received from the HUC his Doctor of Divinity Degree (at the time, equivalent to a Ph.D.)  Concurrent to his doctoral courses, he served as rabbi for [[Temple Israel (Lafayette, Indiana)|Temple Israel]] of Lafayette, Indiana.  In 1929, he became Rabbi of [[Congregation Beth Emeth]] in Albany, NY, where his predecessors included  [[Isaac Mayer Wise|Isaac Mayer WIse]], the founder of the Reform movement in America.  Bamberger remained there for 15 years.<ref>Temple Shaaray Tefila, op. cit.</ref>

On June 14, 1932, he married Ethel Kraus of New York City <ref>Times-Union, Albany, NY, June 15, 1932, p. 3, "Rabbi Bamberger Wed to New York Girl"</ref> whom he nicknamed “Pat.”  They had two sons: Henry, who was ordained as a rabbi in 1961 at the [[Hebrew Union College-Jewish Institute of Religion]], and [[Opera Cleveland|David]], who wrote four textbooks for Jewish religious schools while pursuing a career as an opera producer/director.<ref>{{cite news|title=Paid Notice: Deaths BAMBERGER, ETHEL (PAT)|url=https://www.nytimes.com/2003/12/09/classified/paid-notice-deaths-bamberger-ethel-pat.html|accessdate=July 6, 2016|date=December 9, 2003|work=New York Times}}</ref>

In 1944, Bamberger became rabbi of  [[Temple Shaaray Tefila|Congregation Shaaray Tefila]], one of New York City’s oldest and most prominent Reform Jewish synagogues. During his tenure, he oversaw the congregation’s move from Manhattan’s West side to the East side.  He served as head of the congregation from 1944 to 1971, and then as Rabbi Emeritus until his death on June 14, 1980 – his 48th wedding anniversary.<ref>{{cite news|last1=Clark|first1=Alfred E.|title=Rabbi Bernard J. Bamberger, 76; Became Rabbi in 1926|url=https://query.nytimes.com/gst/abstract.html?res=9E01EED81138E232A25755C1A9609C94619FD6CF|accessdate=July 5, 2016|work=New York Times|subscription=yes|date=June 16, 1980}}</ref>

==Biblical Scholarship==

===''The Bible: A Modern Jewish Approach'' (1955)===
In 1955, Bamberger wrote for Hillel books a short introduction to scripture entitled The Bible: A Modern Jewish Approach.  Its first sentence shows the large question addressed in the text’s mere 96 pages:  “What meaning, what value does the Bible have for the modern man – in particular, for the modern Jew?” <ref>''The Bible: A Modern Jewish Approach'' (B'nai B'rith Hillel Foundations, 1955), p. 1.</ref>  The book was published in Spanish in 1967 as ''La Biblia: Un enfoque judio moderno.''

===''Tanakh: The Holy Scriptures'' (1955-1978)===
In 1955, the  [[Jewish Publication Society|Jewish Publication Society of America]], aware that the Jewish community of the United States and English-speaking Jews everywhere needed a translation of the Bible more intelligible and accurate than the 1917 Bible in general use, appointed a committee of seven scholars to prepare a new translation.  The members of the committee were “chosen for scholarly ability, broad outlook, and recognized status in the community at large.”<ref>''Notes on the New Translation of The Torah'', ed. Harry M. Orlinsky, Jewish Publication Society of America, 1969, pp. 17-18. ''</ref> Bamberger, as a member of the Central Conference of American Rabbis, was asked to represent the Reform perspective.  In addition to his work as a translator (he prepared the initial draft of the [[Book of Jeremiah]]), he presided over meetings of the committee,<ref>''Tanakh: The Holy Scriptures'', The New JPS Translation, 1985, Preface, p. xix.</ref> his objectivity, fair-mindedness, and ability to defuse tensions being important assets in dealing with divergent and sometimes passionate views of the committee members.  This project remained a priority through 1978.<ref>Bamberger’s committee translated ''The Torah'' (pub. 1962), ''The Five Megilloth and Jonah'' (1969), ''Isaiah'' (1973), ''Jeremiah'' (1974), and the second part of the Holy Scriptures, ''The Prophets (Nevi-im)'' (1978).  To expedite completion of the project, a second committee was appointed in 1966 to translate the third part of the Holy Scriptures: ''Kethubim (The Writings)''. The completed new translation was published in 1985.  See ''Tanakh'', op. cit., pp. xx-xxi.</ref>

===''The Torah: A Modern Commentary - Leviticus'' (1979)===
The [[Union for Reform Judaism|Union of American Hebrew Congregations]] (now the [[Union for Reform Judaism]]) decided to publish a commentary on the Torah that would be written from a liberal (Reform) point of view.  Bamberger was asked to participate and chose to work on the [[Book of Leviticus]].  This was published separately in 1979, though the complete commentary was not published as a single volume until 1981, the year following his death.<ref>''The Torah: A Modern Commentary'', The Union of American Hebrew Congregations, 1981, p. vii.</ref>

==Other Published Works==
His other major books indicate the breadth and depth of Bamberger’s scholarship:

===''Proselytism in the Talmudic Period'' (1939)===
This book was begun to detail the then commonly held belief that Rabbis of the Talmudic period were opposed to conversion. Bamberger discovered and proved that the opposite was true: the Rabbis were overwhelmingly pro-conversionist.<ref>''Proselytism in the Talmudic Period'', p. 297.</ref> Thus, in his first book, he revolutionized thinking on the subject.  It was republished, with a new introduction by the author, in 1968.<ref>Ibid., p. xix.</ref>

===''Fallen Angels'' (1952)===
Bamberger traced how Judaism and Christianity have tried to explain why, in the universe of a good God, there is evil in the world. Specifically, it traces the myths such as those about angels who sinned with mortals, and about the Satanic rebellion against God, and how these beliefs differed in Judaism, Christianity, and Islam.<ref>''Fallen Angels'' (Jewish Publication Society of America, 1952), p. 5.</ref>

===''The Story of Judaism'' (1957)===
This is a book for the general reader providing "a history of the inner content of Jewish life" and “a comprehensive yet popular history of Judaism” (in contrast to existing histories which focused on the Jews or of Jewish literature).<ref>''The Story of Judaism'' (Union of American Hebrew Congregations, 1957), p. ix.</ref>

===''The Search for Jewish Theology'' (1978)===
A summation of his “approach to religious thinking in general, and Jewish religious thinking in particular.”  Bamberger emphasizes that Judaism has not focused on creating a systematic theology, but rather recognizes and accepts (though perhaps reluctantly) the areas in which human concepts of the divine are limited and even contradictory.<ref>''The Search for Jewish Theology'' (Behrman House, 1978), p. 103.</ref>

==Organization Leader==
[[File:B.J.Bamberger with Truman.pdf|thumb|Bamberger (far left), as President of the Synagogue Council of America, served on the President's Committee on Religion and Welfare in the Armed Forces which here presents its report to President Truman]]
In addition to dealing with the demands of congregational leadership and scholarship, Bamberger was deeply involved in the major organizations of national and world Jewry.  Of these, the most important were:

President of the [[Synagogue Council of America]] (1950–51):  His work included participating on the President’s Committee of Religion and Welfare in the Armed Forces, along with representatives of the Catholic and Protestant Churches.<ref>Harry S. Truman Library and Museum, Daily Appointment Sheet, Tuesday, October 10, 1950.  http://www.trumanlibrary.org/calendar/main.php?currYear=1950&currMonth=10&currDay=10  Also pictured (left to right) H. E. The Most Reverend William R. Arnold, representing Cardinal Spellman, Frank L. Weil, Chairman, the President’s Committee on Religion and Welfare in the Armed Forces, and Rt. Rev. Charles W. Flint, representing Bishop Stamm, President of Federal Council of the Churches of Christ in America.</ref>

President of the [[Central Conference of American Rabbis]] (1959-1961): He oversaw landmark decisions including the implementation of the rabbinic placement system.  Additionally, he used his position to support the civil rights movement of the early 1960s.<ref>{{cite news|title=BAMBERGER HEAD OF REFORM RABBIS|url=https://query.nytimes.com/gst/abstract.html?res=950DE4D9173BEF3BBC4151DFB0668382649EDE|accessdate=July 5, 2016|work=New York Times|subscription=yes|date=June 29, 1959}}</ref><ref>American Jewish Archives, op. cit.</ref>

President of the [[World Union for Progressive Judaism]] (1970-1972): Accompanied by his wife, Bamberger traveled around the world to teach and to cement relationships with far-flung communities such as those in South Africa and Australia.<ref>http://wupj.org/about/History.asp</ref>

==References==
{{reflist|30em}}
{{authority control}}

{{DEFAULTSORT:Bamberger, Bernard J.}}
[[Category:American Reform rabbis]]
[[Category:1904 births]]
[[Category:1980 deaths]]
[[Category:American people of German-Jewish descent]]
[[Category:Religious leaders from Baltimore]]
[[Category:Hebrew Union College alumni]]
[[Category:Johns Hopkins University alumni]]
[[Category:Phi Beta Kappa members]]