{{Infobox military conflict
|conflict=Action of 13 January 1797
|partof=the [[French Revolutionary Wars]]
|image=[[File:Droits de lHomme sinking.jpg|300px|alt=An engraving in which two damaged ships are pulling away from land in high seas while in the foreground a third ship, also damaged, is overwhelmed by a huge wave.]]
|caption=''View of the wreck of the French ship Le Droits de l'Homme'',<br /> [[John Fairburn]]
|date=13–14 January 1797
|place=[[Brittany]] coastline, [[Bay of Biscay]].
|result= British victory
|combatant1={{flagcountry|Kingdom of Great Britain}}
|combatant2={{flagcountry|French First Republic}}
|commander1={{flagicon|Kingdom of Great Britain|naval}} [[Edward Pellew, 1st Viscount Exmouth|Sir Edward Pellew]]<br/>
{{flagicon|Kingdom of Great Britain|naval}} [[Robert Carthew Reynolds]]
|commander2={{flagicon|France}} [[Jean-Baptiste Raymond de Lacrosse]]
|strength1= Two [[frigate]]s:<br/>{{HMS|Indefatigable|1784|6}} (44 guns)<br />{{HMS|Amazon|1795|6}} (36 guns)
|strength2= One [[ship of the line]]:<br/>[[French ship Droits de l'Homme (1794)|''Droits de l'Homme'']] (74 guns)
|casualties1= ''Amazon'' wrecked, 3 killed, 34&nbsp;wounded, 6&nbsp;drowned, crew of ''Amazon'' taken prisoner
|casualties2= ''Droits de l'Homme'' wrecked, 103 killed, 150&nbsp;wounded, ~900&nbsp;drowned
|coordinates={{coord|47|56|29|N|4|27|16|W|region:FR_type:event|display=inline,title}}
}}
{{Campaignbox French Revolutionary Wars Naval Battles}}

The '''Action of 13 January 1797''' was a minor naval battle fought between a French [[ship of the line]] and two British [[frigate]]s off the coast of [[Brittany]] during the [[French Revolutionary Wars]]. During the action the frigates outmanoeuvred the much larger French vessel and drove it onto shore in heavy seas, resulting in the deaths of between 400 and 1,000 of the 1,300&nbsp;persons aboard. One of the British frigates was also lost in the engagement with six sailors drowned after running onto a sandbank while failing to escape a [[lee shore]].

The French 74-gun ship [[French ship Droits de l'Homme (1794)|''Droits de l'Homme'']] had been part of the ''[[Expédition d'Irlande]]'', an unsuccessful attempt by a French expeditionary force to invade Ireland. During the operation, the French fleet was beset by poor coordination and violent weather, eventually being compelled to return to France without landing a single soldier. Two British frigates, the 44-gun {{HMS|Indefatigable|1784|6}} and the 36-gun {{HMS|Amazon|1795|6}}, had been ordered to patrol the seas off [[Ushant]] in an attempt to intercept the returning French force and sighted the ''Droits de l'Homme'' on the afternoon of 13 January.

The engagement lasted for more than 15 hours, in an increasing gale and the constant presence of the rocky Breton coast. The seas were so rough that the French ship was unable to open the lower gun ports during the action and as a result could only fire the upper deck guns, significantly reducing the advantage that a ship of the line would normally have over the smaller frigates. The damage the more manoeuvrable British vessels inflicted on the French ship was so severe that as the winds increased, the French crew lost control and the ''Droits de l'Homme'' was swept onto a sandbar and destroyed.

==Background==
{{main article|Expédition d'Irlande}}
In December 1796, during the [[French Revolutionary Wars]], a French expeditionary force departed from [[Brest, France|Brest]] on an expedition to invade Ireland. This army of 18,000 French soldiers was intended to link up with the secret organisation of [[Irish nationalism|Irish nationalists]] known as the [[United Irishmen]] and provoke a widespread uprising throughout the island.<ref name="TP24">Pakenham, p. 24.</ref> It was hoped that the resulting war would force Britain to make peace with the [[First French Republic|French Republic]] or risk losing control of Ireland altogether.<ref name="TP24"/> Led by Vice-Admiral [[Justin Bonaventure Morard de Galles|Morard de Galles]], General [[Lazare Hoche]] and leader of the United Irishmen [[Wolfe Tone]], the invasion fleet included 17 ships of the line, 27 smaller warships and transports, and carried extensive [[field artillery]], cavalry and military stores to equip the Irish irregular forces they hoped to raise.<ref>James, p. 5.</ref>

===Departure from Brest===
Morard de Galles planned to sail his fleet from the French naval fortress of Brest under cover of darkness on the night of 15–16&nbsp;December.<ref name="JH21">Henderson, p. 21.</ref> The British [[Channel Fleet]] normally maintained a squadron off Brest to [[naval blockade|blockade]] the port, but its commander, Rear-Admiral [[John Colpoys]], had withdrawn his force from its usual station {{convert|20|nmi|km}} offshore to {{convert|40|nmi|km}} northwest of Brest because of severe [[Atlantic]] winter [[gale]]s.<ref name="JH21"/><ref>Woodman, p. 85.</ref><ref name="ODNBColpoys">{{cite ODNB |last=Laughton |first=J. K. |authorlink=John Knox Laughton |title=Colpoys, Sir John |id=5985 |url=http://www.oxforddnb.com/view/article/5985 |accessdate=2008-10-16}}</ref> The only British ships within sight of Brest were an inshore squadron of frigates under [[Edward Pellew, 1st Viscount Exmouth|Sir Edward Pellew]] in {{HMS|Indefatigable|1784|6}}, accompanied by {{HMS|Amazon|1795|6}}, {{HMS|Phoebe|1795|6}}, {{HMS|Révolutionnaire|1794|6}} and the [[lugger]] HMS ''Duke of York''. Pellew was already renowned, having been the first British officer of the war to capture a French frigate: the [[French frigate Cléopâtre|''Cléopâtre'']] at the [[Action of 18 June 1793]]. He later captured the frigates [[French frigate Pomone (1787)|''Pomone'']] and [[French frigate Virginie (1794)|''Virginie'']] in 1794 and 1796, and saved 500&nbsp;lives following the shipwreck of the [[East Indiaman]] ''Dutton'' in January 1796.<ref name="ODNBPellew">{{cite ODNB |last=Hall |first=Christopher D.  |title=Pellew, Edward, first Viscount Exmouth (1757–1833) |id=21808  |url=http://www.oxforddnb.com/view/article/21808 |accessdate=2008-10-16}}</ref> For these actions he had first been [[Knight Bachelor|knighted]] and then raised to a [[baronetcy]]. ''Indefatigable'' was a [[razee]], one of the largest frigates in the [[Royal Navy]], originally constructed as a 64-gun [[third rate]] and cut down to 44 guns in 1795 to make the ship fast and powerful enough to catch and fight the largest of French frigates. Armed with [[24-pounder long gun|24-pounder cannon]] on the main decks and 42-pounder [[carronade]]s on the quarter deck, she had a stronger armament than any equivalent French frigate.<ref>Woodman, p. 65.</ref>

[[File:Edward Pellew.jpg|thumb|upright|left|alt=Portrait of the head and torso of a middle aged man wearing a blue naval uniform|''[[Edward Pellew, 1st Viscount Exmouth|Sir Edward Pellew]]'' by [[Thomas Lawrence (painter)|Thomas Lawrence]], 1797]]
Observing the French fleet's departure from the harbour at dusk, Pellew immediately dispatched ''Phoebe'' to Colpoys and ''Amazon'' to the main fleet at [[Portsmouth]] with warnings, before approaching the entrance to Brest in ''Indefatigable'' with the intention of disrupting French movements.<ref name="RW84">Woodman, p. 84.</ref> Believing that the frigates in the bay must be the forerunners of a larger British force, de Galles attempted to pass his fleet through the [[Raz de Sein]]. This channel was a narrow, rocky and dangerous passage, and de Galles used [[corvettes]] as temporary [[light ships]] that shone blue lights and fired fireworks to direct his main fleet through the passage.<ref name="JH21"/> Pellew observed this, and sailed ''Indefatigable'' right through the French fleet, launching rockets and shining lights seemingly at random. This succeeded in confusing the French officers, causing the [[French ship Séduisant (1783)|''Séduisant'']] to strike the Grand Stevenent rock and sink with the loss of over 680 men from a complement of 1,300.<ref>James, p. 6.</ref> ''Séduisant''<nowiki>'</nowiki>s distress flares added to the confusion and delayed the fleet's passage until dawn.<ref name="RW84"/> His task of observing the enemy completed, Pellew took his remaining squadron to [[Falmouth, Cornwall|Falmouth]], sent a report to the [[Admiralty]] by [[semaphore line|semaphore telegraph]], and refitted his ships.<ref name="JH21"/>

===Failure of the ''Expédition d'Irlande''===
[[File:Jean-Baptiste Raymond de Lacrosse2.JPG|thumb|upright|right|alt=A black and white engraving of a man wearing a naval uniform|[[Jean-Baptiste Raymond de Lacrosse]]]]
During December 1796 and early January 1797, the French army repeatedly attempted to land in Ireland. Early in the voyage, the frigate [[French frigate Aglaé (1788)|''Fraternité'']] carrying de Galles and Hoche, was separated from the fleet and missed the rendezvous at [[Mizen Head]]. Admiral [[François Joseph Bouvet|Bouvet]] and General [[Emmanuel de Grouchy, Marquis de Grouchy|Grouchy]] decided to attempt the landing at [[Bantry Bay]] without their commanders, but severe weather made any landing impossible.<ref name="JH22">Henderson, p. 22.</ref> For more than a week the fleet waited for a break in the storm, until Bouvet abandoned the invasion on 29&nbsp;December and, after a brief and unsuccessful effort to land at the mouth of the [[River Shannon]], ordered his scattered ships to return to Brest.<ref>Regan, p. 89.</ref> During the operation and subsequent retreat a further 11&nbsp;ships were wrecked or captured, with the loss of thousands of soldiers and sailors.<ref>James, p. 10.</ref>

By 13 January most of the survivors of the fleet had limped back to France in a state of disrepair. One ship of the line that remained at sea, the 74-gun [[French ship Droits de l'Homme (1794)|''Droits de l'Homme'']], was commanded by Commodore [[Jean-Baptiste Raymond de Lacrosse]] and carried over 1,300&nbsp;men, 700–800 of them soldiers, including General [[Jean Joseph Amable Humbert|Jean Humbert]].<ref name="CNP177">Parkinson, p. 177.</ref> Detached from the main body of the fleet during the retreat from Bantry Bay, Lacrosse made his way to the mouth of the Shannon alone.<ref name="JH22"/> Recognising that the weather was still too violent for a landing to be made, Lacrosse acknowledged the failure of the operation and ordered the ship to return to France, capturing the British [[privateer]] ''Cumberland'' en route.<ref name="RW86">Woodman, p. 86.</ref>

==Chase==
Pellew too was on his way back to Brest in ''Indefatigable'', accompanied by ''Amazon'' under the command of Captain [[Robert Carthew Reynolds]]. While the rest of the Channel Fleet had been pursuing the French without success, Pellew had had his ships refitted and resupplied at Falmouth so that both frigates were at full complement, well armed and prepared for action. At 13:00 on 13&nbsp;January, the British ships were approaching the island of [[Ushant]] in a heavy fog when they spied another ship through the gloom ahead.<ref name="WJ11">James, p. 11.</ref> This ship, clearly much larger than either of the British vessels, was the ''Droits de l'Homme''. At the same time, lookouts on the French ship spotted the British, and Lacrosse was faced with the dilemma of whether or not to engage the enemy. He knew that his ship was far larger than either of his opponents, but had earlier spotted sails to westwards he believed to be British and thus considered himself outnumbered and possibly surrounded. British records show that no other British vessels were in the vicinity at the time and it is likely that Lacrosse had seen the French ships [[French ship Thésée (1790)|''Révolution'']] and ''Fraternité'' returning to Brest from Bantry Bay.<ref name="RW86"/><ref name="JH23"/> In addition, Lacrosse was concerned by the increasing gale and rocky lee shoreline, which posed a considerable threat to his over-laden vessel, which was already damaged from its winter voyage and carried a [[demi-brigade]]<!-- Number of soldiers is more typical of a battalion --> of the [[French Army]] and Humbert, neither of which could be placed at risk in an inconsequential naval action.<ref name="JH23">Henderson, p. 23.</ref>

Determined to avoid battle, Lacrosse turned southeast, hoping to use his wider spread of sail to outrun his opponent in the strong winds. Pellew, however, manoeuvred to cut the ''Droits de l'Homme'' off from the French coast, at this stage still unsure of the nature of his opponent.<ref name="WJ11"/> As the chase developed, the weather, which had been violent for the entire preceding month, worsened. An Atlantic gale swept the Ushant headland, driving a blizzard eastwards and whipping the sea into a turbulent state, making steering and aiming more difficult. At 16:15, two of ''Droits de l'Homme''<nowiki>'</nowiki>s topmasts broke in the strong winds. This dramatically slowed the French ship, and allowed Pellew, who had recognised his opponent as a French ship of the line, to close with ''Droits de l'Homme''.<ref name="RW87">Woodman, p. 87.</ref>

==Battle==
Pellew was aware that his frigate was heavily outclassed by his much larger opponent, and that ''Amazon'', which was {{convert|8|nmi|km}} distant, was not large enough to redress the balance when it did arrive. He correctly assumed, however, that the ocean was too rough to allow Lacrosse to open his lower [[gunport]]s without the risk that heavy waves would enter them and cause ''Droits de l'Homme'' to founder.<ref name="RG159">Gardiner, p. 159.</ref> In fact, the French ship was totally unable to open her lower deck gunports during the action: an unusual design feature had the ports {{convert|14|in|cm}} lower than was normal and as a result the sea poured in at any attempt to open them, preventing any gunnery at all from the lower deck and halving the ship's firepower.<ref name="WJ12">James, p. 12.</ref> Although this reduced the number of available guns on the French vessel, Lacrosse still held the advantage in terms of size, weight of shot and manpower. The French situation was worsened however by the loss of the topmasts: this caused their ship to roll so severely in the high seas that it was far more difficult both to steer the ship and to aim the guns than on the British vessels.<ref name="RW87"/>

[[File:Vaisseau-Droits-de-lHomme.jpg|thumb|left|alt=Three ships, the middle one badly damaged and flying a French flag, exchanged cannon fire in heavy seas|''Battle between the French warship ''Droits de l'Homme'' and the frigates HMS Amazon and Indefatigable, 13 & 14 January 1797'', Leopold Le Guen]]

To the surprise of Lacrosse and his officers, ''Indefatigable'' did not retreat from the ship of the line, nor did she pass the ship of the line at long-range to [[Windward and leeward|leeward]] as expected.<ref name="WJ12"/> Instead, at 17:30, Pellew closed with the stern of ''Droits de l'Homme'' and opened a [[raking fire]]. Lacrosse turned to meet the threat and opened fire with the guns on the upper deck accompanied by a heavy volley of [[musket]] fire from the soldiers on board.<ref name="CNP177"/><ref name="JH24"/> Pellew then attempted to pull ahead of ''Droits de l'Homme'' and rake her bow, to which Lacrosse responded by attempting to ram ''Indefatigable''.<ref name="RW87"/> Neither manoeuvre was successful, as ''Droits de l'Homme'' raked the British ship but caused little damage as most of her shot scattered into the ocean.<ref name="WJ12"/>

''Indefatigable'' and ''Droits de l'Homme'' manoeuvred around one another, exchanging fire when possible until 18:45, when ''Amazon'' arrived. During this exchange, one of ''Droits de l'Homme''{{'}}s [[cannon]] burst, causing heavy casualties on her packed deck.<ref name="RW88">Woodman, p. 88.</ref> Approaching the larger French ship with all sail spread, Reynolds closed to within pistol shot before raking ''Droits de l'Homme''. Lacrosse responded to this new threat by manoeuvring to bring both British ships to face the westward side of his ship, avoiding becoming trapped in a [[crossfire]].<ref name="JH24">Henderson, p. 24.</ref> The battle continued until 19:30, when both ''Amazon'' and ''Indefatigable'' pulled away from their opponent to make hasty repairs.<ref>Clowes, p. 303.</ref> By 20:30, the frigates had returned to the much slower French ship and began weaving in front of ''Droits de l'Homme''{{'}}s bow, repeatedly raking her.<ref name="JH25">Henderson, p. 25.</ref> Lacrosse's increasingly desperate attempts to ram the British ships were all unsuccessful and what little cannon fire he did manage to deploy was ineffectual, as the rolling of the ship of the line prevented reliable aiming.<ref name="WJ12"/>

By 22:30, ''Droits de l'Homme'' was in severe difficulties, with heavy casualties among her crew and passengers and the loss of her [[Mast (sailing)|mizzenmast]] to British fire. Observing the battered state of their opponent, Pellew and Reynolds closed on the stern quarters of the French ship, maintaining a high rate of fire that was sporadically returned by ''Droits de l'Homme''.<ref>James, p. 13.</ref> Having exhausted the 4,000 [[wikt:cannonball|cannonballs]] available, Lacrosse was forced to use the [[Shell (projectile)|shells]] he was carrying, which had been intended for use by the army in Ireland. In the high winds, these proved even less effective than solid shot, but did drive the frigates to longer range.<ref name="JH25"/> With their opponent almost immobilised, the British frigates were able to remain outside her arc of fire, effect repairs when necessary and secure guns that had broken loose in the heavy seas.<ref name="RW89">Woodman, p. 89.</ref> For the rest of the night the three battered ships remained locked in a close range duel, until suddenly, at 04:20 while it was still dark, land was spotted just {{convert|2|nmi|km}} to leeward by Lieutenant George Bell of the ''Indefatigable''.<ref name="RW89"/>

==Shipwrecks==
Pellew immediately turned seawards in an effort to escape the shore and signalled Reynolds to follow suit. Although both ships had suffered severe damage from the battle and weather, they were able to make the turn away from land, ''Amazon'' to the north and ''Indefatigable'', at the insistence of its [[Breton people|Breton]] [[Maritime pilot|pilot]], to the south.<ref name="RG159"/> Initially it was believed that the land spotted was the island of Ushant, which would have given the ships plenty of sea-room in which to manoeuvre. However at 06:30, with the sky lightening, it became apparent on the ''Indefatigable'' that there were breakers to the south and east, indicating that the three ships had drifted during the night into [[Audierne|Audierne Bay]].<ref>James, p. 16.</ref> On discovering his situation, Pellew was determined to bring his ship westwards, attempting to work his ship out of danger by [[Tacking (sailing)#Beating|beating against the wind]]. Hasty repairs had to be made to the damaged rigging before it was safe to alter their course.<ref>Parkinson, p. 178.</ref> Due to her northwards turn, ''Amazon'' had even less room to manoeuvre than ''Indefatigable'' and by 05:00 she had struck a sandbank.<ref name="LG"/> Although the frigate remained upright, attempts over several hours to bring her off failed; at 08:00 Reynolds ordered his men to prepare to [[wikt:abandon ship|abandon ship]].<ref name="ODNBReynolds"/>

''Droits de l'Homme'' had been more seriously damaged than the British frigates, and was closer to shore at the time land was spotted. As Lacrosse's crew made desperate efforts to turn southwards, the ship's foremast and [[bowsprit]] collapsed under the pressure of the wind. With the ship virtually unmanageable, Lacrosse ordered the anchors lowered in an attempt to hold the ship in position until repairs could be made. This effort was futile, as all but two anchors had been lost during efforts to hold position in Bantry Bay, and British gunfire had damaged one of the anchor cables and rendered it useless.<ref name="WJ17">James, p. 17.</ref> The final anchor was deployed, but it failed to restrain the ship and at 07:00 (according to the French account), the ''Droits de l'Homme'' struck a sandbank close to the town of [[Plozévet]]. This broke off the remaining mast and caused the ship to heel over onto her side.<ref name="WJ18">James, p. 18.</ref>

===''Amazon''===
As daylight broke over Audierne Bay, crowds of locals gathered on the beach. The ''Droits de l'Homme'' lay on her side directly opposite the town of Plozévet, with large waves breaking over her hull; {{convert|2|nmi|km}} to the north, ''Amazon'' stood upright on a sandbar, her crew launching boats in an effort to reach the shore, while ''Indefatigable'' was the only ship still afloat, rounding the [[Penmarck rocks]] at the southern edge of the bay at 11:00.<ref name="LG">{{londonGazette|issue=13972|startpage=53|date=17 January 1797|supp=|accessdate=2008-10-16}}</ref> On board the ''Amazon'', Reynolds maintained discipline and gave orders to launch the ship's boats in an orderly fashion and to build rafts in which to bring the entire crew safely to shore. Six men disobeyed his command, stole a [[Launch (boat)|launch]], and attempted to reach the shore alone, but were swept away by the current. Their boat was capsized by the waves, and all six drowned. The remaining crew, including those wounded in the previous night's action, were safely brought ashore by 09:00, where they were made [[prisoners of war]] by the French authorities.<ref name="JH29">Henderson, p. 29.</ref>

===''Droits de l'Homme''===
''Droits de l'Homme'' was irreparably damaged, and many of the men on board were soldiers with no training for what to do in the event of a shipwreck. Each successive wave swept more men into the water and desperate attempts to launch boats failed when the small craft were swept away by the waves and broken in the surf. Rafts were constructed, but several were swamped in attempts to carry a rope to the shore. The men on the one raft that remained upright were forced to cut the rope to prevent them from foundering in the heavy seas.<ref name="WJ18"/> Some of the men on this raft reached the beach, the first survivors of the wreck. Subsequent attempts were made by individuals to swim to shore with ropes, but they were either drowned or driven back to the ship by the force of the sea.

With no aid possible from the shore, night fell on 14&nbsp;January with most of the crew and passengers still aboard. During the night, the waves stove in (smashed in) the stern of the ship, flooding much of the interior.<ref name="WJ18"/> On the morning of 15&nbsp;January, a small boat carrying nine British prisoners (part of the crew of the ''Cumberland'', captured by ''Droits de l'Homme'' earlier in the campaign) managed to reach shore. The sight of the British-manned boat reaching shore prompted a mass launching of small rafts from the wreck in hopes of gaining the beach. However the waves increased once more, and not one of these small craft survived the passage.<ref>Pipon in Tracy, p. 169.</ref>

By the morning of 16 January, the seas were still rough, whilst hunger and panic had taken over on the wreck. When a large raft carrying the wounded, two women and six children was launched during a lull in the weather, over 120 unwounded men scrambled to board it. This severely overloaded the craft and within minutes a large wave struck the heavy raft and capsized it, drowning all aboard.<ref name="WJ19">James, p. 19.</ref> By the evening, the remaining survivors, without food or fresh water, began to succumb to exposure, and at least one officer drowned in a desperate attempt to swim to shore. Throughout the night, the survivors gathered on the less exposed parts of the hull, and, in the hope of staving off death by dehydration, drank sea water, urine, or vinegar from a small barrel that had floated up from the hold.<ref name="NT170">Pipon in Tracy, p. 170.</ref> The morning of 17&nbsp;January finally saw a reduction in the storm and the arrival of a small French naval [[brig]], the ''Arrogante''. This ship could not come close without the risk of grounding but sent her boats to the wreck in the hope of rescuing survivors.<ref name="WJ19"/> The brig was joined later in the day by the [[cutter (ship)|cutter]] ''Aiguille''.<ref>Clowes, p. 304.</ref>

On the ''Droits de l'Homme'', many survivors were too weak to reach the boats, and a number of men fell from the hull and drowned in the attempt. Many more could not find room in the small boats, and only 150 men were rescued on 17&nbsp;January.<ref name="WJ19"/> The following morning, when the boats returned, they found only 140&nbsp;survivors left, at least as many again having died during the night. The [[the captain goes down with the ship|last two people to leave the ship]] were the commanders [[Jean Joseph Amable Humbert|General Humbert]] and [[Jean-Baptiste Raymond de Lacrosse|Commodore Lacrosse]].<ref name="NT170"/> Taken to Brest, the survivors were fed and clothed and given medical treatment. All the surviving prisoners from the ''Cumberland'' were released and returned to Britain, in recognition of their efforts to save lives from the shipwreck.<ref>James, p. 20.</ref>

==Aftermath==
[[File:Plozévet-Menhir des Droits de l'Homme(3).jpg|right|thumb|Menhir commemorating the wreck of the ''Droits de l'Homme'']]Exact French casualties are difficult to calculate, but of the 1,300 aboard ''Droits de l'Homme'', 103 are known to have died in the battle and just over 300 were saved from the wreck, indicating the deaths of approximately 900&nbsp;men on the French ship between the morning of 14&nbsp;January and the morning of 18&nbsp;January.<ref>James, pp. 15–19.</ref> However, a French source suggests that up to another 500 of the crew were rescued from the wreck by the corvette ''Arrogante'' and the cutter ''Aiguille'' on 17 and 18 January.<ref>Jakez Cornou et Bruno Jonin, L'odyssée du vaisseau "Droits de l'homme" : L'expédition d'Irlande de 1796, éditions Dufa, 1 January 1988, p. 216.</ref> This would give a toll of only about 400. A [[menhir]] at Plozévet, with an inscription carved in 1840 gives a death toll of six hundred.

''Amazon'' lost three in the battle and six in her wreck, with 15 wounded, while ''Indefatigable'' did not lose a single man killed, suffering only 18&nbsp;wounded.<ref name="WJ15">James, p. 15.</ref> The discrepancy in losses during the action is likely due to the extreme difficulty the French crew had in aiming their guns given their ship's instability in heavy seas.<ref name="WJ15"/>

Reynolds and his officers were exchanged for French prisoners some weeks later, and in the routine [[court-martial]] investigating the loss of their ship were honourably acquitted "with every sentiment of the court's highest approbation."<ref name="WJ17"/>  Reynolds was subsequently appointed to the large frigate {{HMS|Pomone|1794|6}}. The senior lieutenants of each frigate were promoted to commander and head money ([[prize money]] based on the number of the enemy's crew and awarded when the defeated ship was destroyed) was distributed among the crews.<ref>{{londonGazette |issue=14089 |startpage=120 |date=6 February 1798 |supp= |accessdate=2008-10-16}}</ref> Pellew remained in command of ''Indefatigable'' off Brest for another year and seized a number of French merchant ships. He was later promoted several times, and by the end of the [[Napoleonic Wars]] in 1815 had become Lord Exmouth, Commander in Chief of the Mediterranean Fleet.<ref name="ODNBPellew"/> Reynolds did not survive the war, dying in the wreck of {{HMS|St George|1785|6}} in 1811.<ref name="ODNBReynolds">{{cite ODNB |last=Laughton |first=J. K. |authorlink=John Knox Laughton |title=Reynolds, Robert Carthew |id=23436 |url=http://www.oxforddnb.com/view/article/23436 |accessdate=2008-10-16}}</ref> Lacrosse and Humbert were not censured for the loss of their ship: the commodore was promoted to admiral and later became ambassador to Spain, while Humbert led the next and equally unsuccessful attempt to invade Ireland, surrendering at the [[Battle of Ballinamuck]] in 1798.<ref>Pakenham, p. 289.</ref>

In Britain, the action was lauded at the time and since: [[First Lord of the Admiralty]] [[George Spencer, 2nd Earl Spencer|Lord Spencer]] described the operation as "an exploit which has not I believe ever before graced our naval annals".<ref name="ODNBPellew"/> Historian James Henderson says of the action: "It was a feat of arms and seamanship such as had never been done before, and never was done again,"<ref name="JH29"/> and [[Richard Woodman]] calls it "a dazzling display of seamanship by all concerned in the alternating darkness and moonlight of a boisterous night".<ref name="RW88"/> Five decades later the battle was among the actions recognised by the [[Naval General Service Medal (1847)|Naval General Service Medal]], with clasps "Indefatigable 13 Jany. 1797" and "Amazon 13 Jany. 1797", awarded upon application to all British participants still living in 1847.<ref>{{London Gazette|issue=20939|startpage=236|endpage=245|date=26 January 1849}}</ref>

== Notes ==
{{Reflist|colwidth=30em}}

== References ==
* {{cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997 |origyear=1900
 | chapter =
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume IV
 | publisher = Chatham Publishing
 | location =
 | isbn = 1-86176-013-2
}}
* {{cite book
 | editor =Gardiner, Robert
 | year = 2001 |origyear=1996
 | title = Fleet Battle and Blockade
 | publisher = Caxton Editions
 | isbn = 1-84067-363-X
}}
* {{cite book
 | last = Henderson CBE
 | first = James
 | year = 1994 |origyear=1970
 | title = The Frigates
 | publisher = Leo Cooper
 | isbn = 0-85052-432-6
}}
* {{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002 |origyear=1827
 | title = The Naval History of Great Britain, Volume 2, 1797–1799
 | publisher = Conway Maritime Press
 | isbn = 0-85177-906-9
}}
* {{cite book
 |last= Pakenham
 |first= Thomas
 |authorlink= Thomas Pakenham (historian)
 |year= 2000
 |origyear= 1997
 |title= The Year of Liberty: The Story of the Great Irish Rebellion of 1798
 |publisher= Abacus
 |location= London
 |edition= revised
 |isbn= 978-0-349-11252-7 }}
* {{cite book
 | last = Parkinson
 | first = C. Northcote
 | authorlink = C. Northcote Parkinson
 | year = 1934
 | title = Edward Pellew Viscount Exmouth
 | publisher = Methuen & Co
 | location = London
}}
* {{cite book
 | last = Regan
 | first = Geoffrey
 | authorlink = Geoffrey Regan
 | year = 2001
 | title = Naval Blunders
 | publisher = Andre Deutsch
 | isbn = 0-233-99978-7
}}
* {{cite book
 | editor-last = Tracy
 | editor-first = Nicholas
 | year = 1998
 | chapter = Narrative of the dreadful Shipwreck of Les Droits de L'Homme, a French ship, of 74 guns, driven on shore on the 14th February 1797, after a severe Action with the Indefatigable and Amazon Frigates, under the Command of Sir Edward Pellew and Captain Reynolds
 |first= Elias, Lieutenant, 63rd Regiment
 |last= Pipon
 | title = The Naval Chronicle, Volume 1, 1793-1798
 | publisher = Chatham Publishing
 | isbn = 1-86176-091-4
}}
* {{cite book
 | last = Woodman
 | first = Richard
 | authorlink = Richard Woodman
 | year = 2001
 | title = The Sea Warriors
 | publisher = Constable Publishers
 | isbn = 1-84119-183-3
}}
{{featured article}}

[[Category:Conflicts in 1797]]
[[Category:Naval battles involving France]]
[[Category:Naval battles of the French Revolutionary Wars]]
[[Category:Naval battles involving Great Britain]]
[[Category:January 1797 events]]