<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Christavia Mk IV
 | image=PH-DYL (8321448925).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[Canada]]
 | manufacturer=[[Elmwood Aviation]]
 | designer=Ron Mason
 | first flight=
 | introduced=
 | retired=
 | status=Plans available (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=250
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]275.00 (plans only, 2013)
 | developed from= [[Christavia Mk I]]
 | variants with their own articles=
}}
|}
The '''Christavia Mk IV''' (Christ-in-Aviation) is a [[Canada|Canadian]] [[homebuilt aircraft]] that was designed by Ron Mason and produced by [[Elmwood Aviation]] of [[Frankford, Ontario]] (formerly in [[Belleville, Ontario]]). The aircraft is supplied in the form of plans for amateur construction.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 150. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="Spruce">{{cite web|url = http://www.aircraftspruce.com/catalog/kitspages/christaviaMK4.php|title = Christavia|accessdate = 14 December 2013|last = [[Aircraft Spruce & Specialty Co]]|year = 2013}}</ref>

==Design and development==
Designed for African missionary work the Mark IV is a development of the [[Christavia Mk I]], with greater wingspan, a longer [[fuselage]] and two additional seats. The aircraft features a [[strut-braced]] [[high-wing]], a four seat enclosed cabin with doors, fixed [[conventional landing gear]] and a single engine in [[tractor configuration]].<ref name="Aerocrafter" />

The aircraft fuselage is made from welded [[4130 steel]] tubing, while the wing is of all-wooden construction, with all surfaces covered with [[Aircraft dope|doped]] [[aircraft fabric]]. Later models have an [[aluminum]] [[Spar (aviation)|wing spar]]. Its {{convert|35.50|ft|m|1|abbr=on}} span wing employs a custom Mason [[airfoil]], mounts [[Flap (aircraft)|flaps]] and has a wing area of {{convert|177.3|sqft|m2|abbr=on}}. The wing is supported by "V" struts with [[jury struts]]. The standard engine used is the {{convert|150|hp|kW|0|abbr=on}} [[Lycoming O-320]] powerplant.<ref name="Aerocrafter" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 14 December 2013|last = Lednicer |first = David |year = 2010}}</ref>

The Christavia Mk IV has a typical empty weight of {{convert|1100|lb|kg|abbr=on}} and a gross weight of {{convert|2200|lb|kg|abbr=on}}, giving a useful load of {{convert|1100|lb|kg|abbr=on}}. With full fuel of {{convert|41|u.s.gal}} the payload for crew, passengers and baggage is {{convert|854|lb|kg|abbr=on}}.<ref name="Aerocrafter" />

Plans are marketed by [[Aircraft Spruce & Specialty Co]]. Ron Mason sold the rights to the Christavia series of aircraft to Aircraft Spruce and no longer supplies the plans or support.<ref>{{Cite web|title = CHRISTAVIA MK-4 from Aircraft Spruce Canada|url = https://www.aircraftspruce.ca/catalog/kitspages/christaviaMK4.php|website = www.aircraftspruce.ca|access-date = 2016-02-03}}</ref> The designer estimates the construction time from the supplied plans as 2600 hours.<ref name="Aerocrafter" /><ref name="Spruce" />

==Operational history==
By 1998 the designer reported that 250 examples were flying.<ref name="Aerocrafter" />

In December 2016 five examples were [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]] and eight with [[Transport Canada]].<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=CHRISTAVIA+MK+4&PageNo=1|title = Make / Model Inquiry Results|accessdate = 23 December 2016|last = [[Federal Aviation Administration]]|date = 23 December 2016}}</ref><ref name="TCCAR">{{cite web|url=http://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/RchSimpRes.aspx?cn=%7cCHRISTAVIA%7c&mn=%7c%7c&sn=%7c%7c&on=%7c%7c&m=%7c%7c |title=Canadian Civil Aircraft Register |accessdate=23 December 2016 |last=[[Transport Canada]] |date=23 December 2016}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Christavia Mk IV) ==
{{Aircraft specs
|ref=AeroCrafter and The Incomplete Guide to Airfoil Usage<ref name="Aerocrafter" /><ref name="Incomplete" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=three passengers
|length m=
|length ft=22
|length in=3
|length note=
|span m=
|span ft=25
|span in=6
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=177.3
|wing area note=
|aspect ratio=
|airfoil=Mason
|empty weight kg=
|empty weight lb=1100
|empty weight note=
|gross weight kg=
|gross weight lb=2200
|gross weight note=
|fuel capacity={{convert|41|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming O-320]]
|eng1 type=four cylinder, air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=150<!-- prop engines -->

|prop blade number=2
|prop name=fixed pitch
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=130
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=120
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=48
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=400
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=19000
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=800
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=12.4
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{Commons category|Christavia Mk IV}}
*{{Official website|http://www.aircraftspruce.com/catalog/kitspages/christaviaMK4.php}}
{{Elmwood Aviation aircraft}}

[[Category:Elmwood Aviation aircraft|Christavia Mk IV]]
[[Category:Canadian civil utility aircraft 1980–1989]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Homebuilt aircraft]]