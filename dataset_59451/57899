{{italictitle}}
{{Infobox Journal
| title        = Journal of Applied Electrochemistry
| cover        = [[File:Journal of Applied Electrochemistry.jpg]]
| editor       = Gerardine G. Botte
| discipline   = [[Chemistry]]
| abbreviation = J. Appl. Electrochem.
| publisher    = [[Springer Science+Business Media|Springer]]
| country      = [[Netherlands]]
| frequency    = Monthly
| history      = 1971-present
| openaccess   =
| impact       = 1.540 (2008)
| website      = http://www.springer.com/journal/10800/
| link1        = http://www.springerlink.com/content/0021-891X
| link1-name   = online access
| link2        =
| link2-name   = 
| RSS          = http://www.springerlink.com/content/0021-891X?sortorder=asc&export=rss
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 0021-891X
| eISSN        = 1572-8838 }}
The '''''Journal of Applied Electrochemistry''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Springer Science+Business Media]]<ref>[http://www.springer.com/396 Journal homepage]</ref> covering [[electrochemistry]], focusing on technologically oriented aspects. A major topic of the journal is the application of electrochemistry to technological development and practice. 
Subjects covered are cell design, electrochemical reaction engineering, [[corrosion]], [[hydrometallurgy]], the electrochemical treatment of effluents, [[molten salt]] and solid state electrochemistry, [[solar cells]], new battery systems, and surface finishing.

== Impact factor ==
The ''Journal of Applied Electrochemistry'' had a 2008 [[impact factor]] of 1.540,<ref>[http://admin.isiknowledge.com ISI Web of Knowledge]</ref> ranking it 14th out of 22 in the subject category Electrochemistry.

== Editor ==
The editor of the journal is Gerardine G. Botte.<ref>[http://www.springer.com/chemistry/physical/journal/10800?detailsPage=editorialBoard complete Editorial Board]</ref>

== References ==
{{reflist}}
== External links ==
*{{Official|http://www.springer.com/journal/10800}}


[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1971]]
[[Category:Monthly journals]]
[[Category:Electrochemistry journals]]