{{Use American English|date=August 2015}}
{{Use dmy dates|date=August 2015}}
{{Infobox military unit
|unit_name=92nd Engineer Battalion (Heavy)
|image=[[File:92nd Engineer Battalion Unit Crest.jpg|220px]]
|caption=Distinctive Unit Insignia
|country= {{USA}}
|branch= [[US Army Corps of Engineers#The Engineer Regiment|US Army Corps of Engineers]]
|garrison=[[Fort Stewart, Georgia]]
|motto=GLORIA AD CAPUT VENIRE  
"Glory in Achievement"}}

{{Infobox military unit
|unit_name=Battalion Coat of Arms 
|image=[[File:92nd Eng Bn coa.jpeg|220px]]
|caption=Coat of arms
}}

The '''92nd Engineer Battalion''' is a unit of the [[United States Army]] with a record in both peace and war; an organization that provides sustained engineer support across the full spectrum of military operations.

The 92nd Engineer Battalion Headquarters provides training and readiness assistance to six companies and two detachments located on [[Fort Stewart, Georgia]].  The battalion formation includes two horizontal construction companies, one vertical construction company, one forward support company, one route clearance company, headquarters and headquarters company, a survey detachment, and a firefighter detachment prepared to deploy in support of contingency operations worldwide.

The 92nd Engineer Battalion is one of the [[Army]]’s first modular engineer battalion headquarters. The unit’s history of service begins in 1933 with deployments to Britain, North Africa, Italy during [[World War II]], [[Vietnam]], hurricane relief efforts, Desert Storm/Shield and numerous deployments in support of the [[Global War on Terrorism]] in [[Iraq]] and [[Afghanistan]].

==Unit crest==
The [[Distinctive unit insignia (U.S. Army)|distinctive unit insignia (DUI)]] is directly related to the Battalion’s history. The four points of the diamond represent the four campaigns in which the Battalion participated during [[World War II]]. The cross is symbolic of the royal arms of Italy, the country in which the four campaigns took place. The colors of the crest, scarlet and white, are the colors of the [[United States Army Corps of Engineers|Army Corps of Engineers]]. GLORIA AD CAPUT VENIRE is the battalion motto and translated means GLORY IN ACHIEVEMENT.<ref>{The Battalion Insignia, Headquarters, History of 92D Engineer Battalion (Construction) Fort Bragg, NC - March 1967}</ref>

Approved on 19 January 1956, the [[Coat of Arms]] consists of two crossed bamboo poles, an upright sword as well as a green disk with gold star in the center, placed above the scarlet and white shield with the black diamond symbol in the center. This represents the unit’s war service in [[Vietnam]]. According to [[The Institute of Heraldry]], "The crossed bamboo poles symbolize the general support, rebuilding and construction projects of the 92d Engineer Battalion. The green disc alludes to the tropical region and the five-pointed star represents the four [[Meritorious Unit Commendations]] and the Vietnamese Civil Action Medal awarded the unit."<ref>{The Institute of Heraldry. United States Army / U.S. Army Heraldry / Unit Insignia / Distinctive Unit Insignia, Shoulder Sleeve Insignia, Coat of Arms / Engineer / 92 Engineer Battalion / Coat of Arms}</ref>

==Battalion motto==
Glory in Achievement - LTC Harry W. Lombard, Commander of the 92D Engineer Battalion, in 1967, recorded this definition in the Battalion records, of the meaning associated with the motto. "A word is shallow, a promise unfullfilled, a battle never won until the object we have sought is ours. Then there is success, and glory in achievement. This is our goal: Our words, commands; each promise is kept; the battle won. In peace and war it is not the task at hand that matters but the manner in which it is undertaken. When any group puts forth their all in a combined effort, and by so doing accomplishes its mission, it has attained the highest honor - Glory in Achievement."<ref name="lombard">{Harry W. Lombard, LTC, CE, Commanding 92D Engineer Battalion (Construction)- March 1967}</ref>

==Essayons==
''Let us Try'' - The [http://www.usace.army.mil/ U.S. Army Corps of Engineers] is rich in history; built the earth works at [[Battle of Bunker Hill|Bunker Hill]], the bridges at the [[Imjin River]] in Korea, and the port at [[Cam Ranh]] in Vietnam. "Engineer Soldiers have been shelled, bombed, starved and frozen, but, no matter how hopeless the situation seemed, the officers and Soldiers never forgot, ESSAYONS".<ref name="lombard" />
[[File:United States Army Corps of Engineers logo.svg|thumb|right|<center>United States Army Corps of Engineers Logo</center>]]
The Corps' mission is to "Deliver vital public and military engineering services; partnering in peace and war to strengthen our Nation’s security, energize the economy and reduce risks from disasters".<ref name="mission">[http://www.usace.army.mil/About/MissionandVision.aspx USACE Mission and Vision webpage]</ref>

==Unit history==
The 92nd Engineer General Service Regiment was constituted an inactive unit of the Regular Army, effective 1 October 1933, as the 51st Engineer Battalion (Separate). The unit was redesignated on 1 January 1938, as the 92nd Engineer Battalion and activated on 1 May 1941 at [[Fort Leonard Wood, Missouri]].  The enlisted cadre for the 92nd Engineer Battalion was furnished by the 41st Engineer Regiment and numbered 49 men. The officers were drawn from the Regular Army and members of the Officers Reserve Corps on active duty.<ref name="bennett">{George W. Bennett, LTC, 92nd Engineer Regiment, Commanding, Headquarters, History of 92D Engineer General Service Regiment - December 1944 to May 1945}</ref>
<!-- Deleted image removed: [[File:51st Engineer Battalion.jpg|thumb|right|1 October 1933 - Constituted as the 51st Engineer Battalion (Separate)]] -->

The unit was reorganized and redesignated, effective 22 May 1942, as the 92nd Engineer Regiment (General Service).<ref name="schut">{CPT Scott Schutzmeister, 92nd Engineer Battalion Unit History - August 1990-April 1991}</ref>  On 1 July 1942, the organization sailed aboard the [[USAT Thomas H. Barry]] from the [[New York Port of Embarkation]]. Landing in Scotland on 13 July, the regiment was assigned to Southern Base Section of SOS, ETOUSA, with station near Taunton, Somerset, in southwestern England. The regiment was the first American unit in that part of Britain. On 1 August 1942, the unit designation was changed to 92nd Engineer General Service Regiment.<ref name="bennett" />
<!-- Deleted image removed: [[File:92nd Engineer General Service Regiment.jpg|thumb|right|1 August 1942 – Designated as the 92nd Engineer General Service Regiment]] -->

The Regiment sailed from Scotland arriving at Oran, Algeria, in French North Africa on 16 February 1943, and was assigned to the Mediterranean Base Section. In April the unit was attached to the Fifth Army Invasion Training Center in preparation for the Sicily and Salerno operations.<ref name="bennett" /> 
<!-- Deleted image removed: [[File:Battalion pass and review.jpg|thumb|right|Battalion pass and review - Arkansas]] -->

===World War II===

On 14 November 1943, the unit sailed from North Africa and landed at Naples on 17 November.<ref name="bennett" /> The 92nd Engineers deployed to Italy earning four campaign streamers during World War II:

:<big>♦</big> '''Naples-Foggia''' - For reconditioning the port as well as repairing and maintaining bridges. On one occasion, the night of 12 Feb 1944, the river rose to flood height and the rising water and floating debris seriously endangered the bridge at Capua. During the night, while the fate of the bridge hung in the balance, the aggressive action of members of Company B, under 1st Lieutenant Lester M. Kostenski, cleared wreckage from the understructure, enabling the bridge to be saved and kept the Army’s main supply line intact. Two enlisted men were awarded the [[Soldiers Medal]] for their actions during that operation. During the winter when all units were fighting rain and mud, the Regiment operated five quarries and gravel pits to supply rock, for roads to hospitals, water points, dumps and other Army installations, as well as material for the main roads;<ref name="bennett" />

:<big>♦</big> '''Rome-Arno''' - In the rapid advance after the Hitler line was broken the Regiment operated in the rear of the Army area. In general, its duties were to keep the supply roads open for high speed traffic, dismantle tactical bridges, and build fixed bridges. For its work during this phase of operations, the Regiment was awarded the Fifth Army Plaque and Clasp for August 1944;<ref name="bennett" />

:<big>♦</big> '''North Apennine''' - Working 24 hours a day, dump trucks poured crushed rock onto the main supply route and Soldiers stood knee deep in mud and snow digging out drains so the water would flow off the surface. Highway 67 provided a sizeable job with a "dip" in the road 400 feet long and 7’6" deep in the center where German demolitions had cut the roadway;<ref name="bennett" />

:<big>♦</big> '''Po Valley''' - For maintaining a railhead for supply routes to the Po River. Observed artillery fire exploded in the vicinity and two men received wounds and [[Purple Heart]]s as a result, PVTs Parky Durham and Cleavon Lark. The surrender of first the German armies in Italy and then the entire German army on 8 May 1945, made little difference to the operation of the Regiment; work continued without interruption.<ref name="bennett" />

The four points of the black diamond on the unit crest represent these four campaigns. During this time the Regiment became known as the "Can Do Regiment."  
[[File:Officers of 1st BN 92d EN GS REGT WWII.jpg|21.4px|framed|right|'''Officers of 1st BN 92d EN GS REGT WWII -''' Front Row: Lt. Schellberg (Neb), Lt. Welker (Ala), Lt. Swanson (Mich), Capt. Douglas (Mo), Back Row: Capt. Green (Minn), Major Small (Ill), Capt. Wrey (Texas), Lt. McDonald (Mass), Lt. Geary (Penn), Lt. Lahaye (Texas), Lt. Cordone (Mich)]]
<!-- Deleted image removed: [[File:CPT Joel B. Hill.png|thumb|right]] -->

Due to the reduction in military strength following [[World War II]], the Regiment was deactivated in Italy 20 April 1946;<ref name="schut" /> the unit remained inactive for eight years. While still in an inactive status - Headquarters, Headquarters and Service Company, and Companies Alpha, Bravo, and Charlie were redesignated as the 92nd Engineer Battalion on 25 February 1954. The remainder of the Regiment was disbanded.<ref name="lombard" />

Reactivated on 9 February 1955, as the 92nd Engineer Battalion (Construction) at [[Fort Bragg, North Carolina]], the unit was attached to [[20th Engineer Brigade]], [[XVIII Airborne Corps]]. Reorganization added Company Delta and redesignated Company A as an Equipment and Maintenance Company.<ref name="lombard" />

[[Hurricane Connie]] and Hurricane Dianne (1955) left the beaches of South Carolina scarred and bruised, Company C was sent with its personnel and equipment to restore the beaches in the stricken areas. After 90 days of operation the unit returned to Fort Bragg.<ref name="lombard" />

On 7 June 1965, Company C reinforced plus a Battalion Headquarters Planning Section deployed to the Dominican Republic to support the Inter American Peace Force during the Dominican Republic Crisis. Within the combat area the Construction Force built a logistical complex to support all the forces in Theater. The unit returned 8 February 1966, after having been designated the most outstanding engineer unit within the Continental United States.<ref name="lombard" />

On 15 June 1965, Company D moved to [[Fort Rucker, Alabama]] to support the United States Army Aviation Center. This company deployed separately from Fort Rucker to Southeast Asia on 30 January 1967.<ref name="lombard" />

===Vietnam===
On 27 May 1967, the rest of the battalion was ordered to the Republic of Vietnam.  During the Vietnam Conflict the "Can Do Regiment" became known as the "Black Diamonds" for their distinctive unit crest. Once in country, the battalion quickly demonstrated the high levels of enthusiasm and professionalism which had characterized their actions in previous times of crisis.<ref name="schut" /> The battalion constructed warehouses, motor pool sheds, chapels, airfields, pipeline systems, barracks and a 1000-foot MLC 50 bridge. Additionally, the 92nd, maintained supply routes and operated a rock quarry. It was stationed at Long Binh under the 159th Engineer Group throughout its service in Vietnam.<ref>{Shelby L. Stanton,''Vietnam Order of Battle'' U.S. News Books. Washington, D.C. - 1981}</ref>
From 2 May 1967 through 27 July 1972, the Battalion distinguished itself in 14 campaigns:<ref name="schut" />
<!-- Deleted image removed: [[File:92nd Engineer Battalion Construction.jpg|thumb|right|<center>E5 Promotion Ceremony – December 1967
</center>]] -->
<!-- Deleted image removed: [[File:92nd Dirt mover.png|thumb|right|<center>92d’s Official Newspaper In Vietnam</center>]] -->

:1. Counteroffensive Phase II
:2. Counteroffensive Phase III
:3. Tet Counteroffensive
:4. Counteroffensive Phase IV
:5. Counteroffensive Phase V
:6. Counteroffensive Phase VI
:7. Tet 69/Counteroffensive
:8. Summer-Fall 1969
:9. Winter-Spring 1970
:10. Sanctuary Counteroffensive
:11. Counteroffensive Phase VII
:12. Consolidation I
:13. Consolidation II
:14. Cease Fire

The Battalion received four [[Meritorious Unit Commendations]] for the periods 1967, 1968–69, 1969–70, and 1970-71. The unit also received the Vietnamese Civil Action Honor Medal, First Class for the period 1967-70. On 26 July 1972, the 92nd left Vietnam and returned stateside to Fort Stewart, Georgia. Delta Company was reformed at Fort Gordon, Georgia.<ref name="schut" />

All of the engineer support for the security forces sent to Florida for the National Political Nominating Conventions was supplied by "Chargin’ Charlie Company" (C Company) at Homestead Air Force Base. In March and June, 1972, the unit received the "Bloodhound Award" in recognition of its exemplary participation in the quarterly Bloodmobile visit to the Hunter/Stewart Complex. This was the first time a unit had won three consecutive "Bloodhounds" (C Company won its first in December, 1971).<ref>{Unit Achievements - 1972}</ref> In June 1976, the unit was redesignated as the 92nd Engineer Battalion (Combat) (Heavy).<ref name="schut" />
[[File:Khobar towers. 92nd en bn 1991.jpg|thumbnail|Khobar towers. After the ground war. Preparing to return to Ft. Stewart March 1991. Bottom left to right. Sgt. Obe, Sgt. Gartin, SSG Van Zandt, SSG Licea, Pvt Martin. Top, left to right. Spc Porter, Pvt Peltier, Pvt Mott, Spc Kirby, Sgt. Kirk, Sgt. Gibson, Spc Bartholomew.]]
During the period 1 August 1987 to 31 July 1988, the Battalion supported a series of worldwide deployments both in support of Joint Service exercises and to provide humanitarian assistance.  The missions were to numerous locations, including Haiti, the Dominican Republic, Cuba, Bahamas, the jungles of South America and the deserts of East Africa. For this effort, the Battalion was awarded the Army Superior Unit Award. The Battalion was recognized again in the fall of 1988, with the National Humanitarian Award for hurricane relief provided during Hurricanes Connie, Dianne, Hugo, and Andrew.<ref name="schut" />

===[[Operation Desert Shield/Operation Desert Storm]]===

[[File:Southwest asia 530th charlie company.jpg|thumb|right|<center>Charlie Company</center>]]
[[File:Desert storm shield 92nd en bn.jpg|thumb|right|<center>92nd EN BN - Desert Shield/Storm</center>]]

====92nd Engineer Combat Battalion (Heavy) A, B, C and D Companies - October 1990 to April 1991====
On 15 October 1990, the 92nd deployed to Damman, [[Saudi Arabia]], where it subsequently went on to successfully complete 40 missions during [[Operation Desert Shield]] and 68 missions during [[Operation Desert Storm]]. Of the seven top priority construction engineer projects, as designated by both [[XVIII Airborne Corps]] and [[VII Corps (United States)|VII Corps]], the Black Diamonds were assigned and completed five in a little over six months. This included completion of 566 kilometers of road construction (Main Supply Routes) for both corps, constructing a major VII Corps logistics base (Log Base Echo), and a 4300’ asphalted airstrip for theater casualty evacuation. The Battalion relocated five times into, and out of, three countries – [[Saudi Arabia]], [[Iraq]], and [[Kuwait]]. Throughout, the unit successfully completed all assigned missions and gained the reputation of being a proactive, responsive engineer battalion.  The Battalion was awarded campaign streamers for the defense of Saudi Arabia and the Liberation and Defense of Kuwait, and received its fifth [[Meritorious Unit Commendation]]. The individuals of the battalion were awarded 48 [[Bronze Star Medals]] and 297 Army Commendation Medals from [[3rd Armored Division (United States)|3rd Armored Division]] and the 926th Engineer Group.<ref name="schut" />

===Operation Enduring Freedom===

====92nd Engineer Battalion Companies A and B====

In 2001, A and B Companies and a platoon from the 306th Engineer Company of the US Army Reserves, were deployed in support of [[10th Mountain Division]] and the  [[101st Airborne Division]] to provide rapid runway repair, build a detainee holding cell, construct administrative facilities, seven kilometers of roads, thousands of feet of survivability ditches and mine clearing operations in [[Uzbekistan]], [[Bagram]], and [[Kandahar]]. 92nd deployed to build what was to be the U.S. and coalition forces’ initial base camp, staging area and port for debarkation of humanitarian aid to [[Afghanistan]].  Prior to the Battalions arrival, the camp was an open field with no buildings, electricity, running water or adequate drainage. The Soldiers had the additional task of providing perimeter security at the camps while the infantry units were on mission, one of which was Operation Anaconda  1–18 March 2002.  Mines were also troublesome to the engineers’ day-to-day operations many of which remained from the Soviets previous occupation of the country. Between Bagram and Kandahar, the mine-clearing dozers detonated 125 mines.

Maj. Gen. Buford C. Blount III, 3rd Infantry Division, commanding general, presented 38 Soldiers with [[Bronze Star Medals]]. More than 230 Soldiers in the Battalion were awarded the Army Commendation Medal.

===Operation Iraqi Freedom===

[[File:RED HORSE - 92nd FOB Hammer.png|framed|right|Soldiers from the 92nd Engineer Battalion complete guard towers along Forward Operating Base Hammer's 14-kilometer, 10-foot-high perimeter berm in Iraq. 557th Expeditionary RED HORSE Airmen and Soldiers from the 92nd Engineer Battalion's "Black Diamonds" were tasked to bed down the Army's 3rd Brigade Combat Team, 3rd Infantry Division, from Fort Stewart, Ga., in support of the Baghdad Security Plan. (U.S. Air Force photo)]]
In April 2003, the 92nd deployed in support of [[Operation Iraqi Freedom I]].  The Battalion worked from Tallil Airbase just outside An Nasiriyah to develop living and logistics sustainment facilities along the main supply route.<ref name="3ID">{Team Stewart, 92nd Engineer Battalion History, Third Infantry Division, http://www.stewart.army.mil/}</ref>
In January 2005, the Battalion deployed to [[Baghdad]] in support of OIF 04-06 to provide general construction engineering and chemical force protection support.  92nd completed 276 construction missions, including the construction of an Iraqi tank gunnery range, a headquarters and airfield complex for the [[3rd Armored Cavalry Regiment]], numerous battalion and brigade headquarters SEA Huts and the repair of over 100 IED craters.<ref name="3ID" />

From October 2006 to October 2007, the Battalion was deployed to Balad Air Base, Cp Striker, Cp Delta, and Talil Air Base, [[Iraq]] in support of OIF 06-08. During the deployment, the Black Diamonds completed over 700 combat and construction missions worth over $50 million, earning its eighth Meritorious Unit Commendation. The Battalion consisted of over 1200 Soldiers, sailors and Airmen who executed the full spectrum of engineering operations in support of Multinational Corps-Iraq. During the surge the battalion constructed the base camp facilities for the 3/3 BCT at FOB Hammer, 4/2 BCT at Camp Taji, 4 Joint Security Stations in support of the [[25th Infantry Division (United States)|25th Infantry Division]], 3 Combat Outposts in support of 2/3 BCT and renovated an existing building to serve as the 3d Infantry Division Headquarters. 92nd along with 557th Expeditionary RED HORSE Squadron, completed an impressive list of individual construction projects at Besmaya, including erecting 223 tents in three Life Support Areas; 14 kilometers of 10-foot high, triple-strand, concertina wire topped berm; two million gallons of black and grey water lagoon capacity; an 850,000 gallon bulk fuel farm; a 6 million gallon raw water processing plant; six motor pools and two helicopter pads. As a part of the MNC-I's counter IED mission the battalion completed repair of over 700 IED craters, sanitized over 100 kilometers of Main Supply Routes, completed 8 major road repairs and installed 12 fixed bridges, enhancing mobility throughout the Corps.<ref name="3ID" />

===Operation Enduring Freedom===

====92nd Engineer Battalion - HHC, FSC, 526, 984, 554, 36th DET====
In May 2010, the Black Diamonds deployed to [[Afghanistan]] as part of the surge of 30,000 additional American Soldiers.  The battalion constructed new, and upgraded existing, combat outposts and forward operating bases in support of Coalition and Afghan units. Their efforts directly resulted in the increased combat effectiveness of 15 battalion- and brigade-level task forces across Regional Command-East at a critical period of the campaign.<ref name="3ID" />

====530th Route Clearance Company - March 2011 to March 2012====
On 12 March 2011, the 530th Bloodhounds deployed to Forward Operating Base Sarkari Karez, Afghanistan in the Helmand province to conduct route clearance.  In May 2011, the company moved to the Kandahar province and split into two elements.  First and second platoon conducted clearance operations in and around FOB Walton, while third and fourth platoon moved to FOB Spin Boldak in southern Afghanistan near the Pakistan border. The Bloodhound efforts reduced 147 IEDs in southern Afghanistan; 80,000 kilometers of improved and unimproved roads were cleared. Effectively employed multiple Mine Clearing Linear Charges and eight Antipersonnel Obstacle Breaching Systems during route and area clearance operations; cross-trained maneuver counterparts on both devices. Executed a responsible Relief In Place – Transfer Of Authority with two Mobility Augmentation Companies at two FOBs separated by over 100&nbsp;km in RC-South. 
Awarded 19 Purple Hearts and 65 Combat Action Badges during deployment; redeployed every Bloodhound from Afghanistan. The company returned to Fort Stewart on 12 March 2012.<ref>{530th Engineer Company History, 92nd Engineer Battalion, Fort Stewart - 2014}</ref>
<!-- Deleted image removed: [[File:Camp Stone Flags.png|597px|thumb|right|Camp Stone, Afghanistan]] -->

====92nd Engineer Battalion - HHC, FSC, 526, 984, 554, 36th DET - February 2013 to November 2013====
Most recently, the Black Diamonds deployed in support of Operation Enduring Freedom 13, to conduct retrograde operations and reduce the Army’s footprint in Afghanistan. As the second engineer battalion to be part of the CENTCOM Materiel Retrograde Element (CMRE) mission, the 92nd Engineer Battalion completed over fifteen engineer work requests and contributed to the closure or transfer of two Tactical Infrastructures, nine Tactical Bases, and one Operational Base with focus in Regional Command’s South, Southwest and West. Forward Support Company completed over twenty-eight Combat Logistic Patrols covering over 4,000 miles in RC South, served more than 25,000 meals to fellow Black Diamonds and mechanical support at six different locations. 526th Horizontal Construction Engineers, 554th Vertical Construction Engineers, and 984th Horizontal Construction Engineers deconstruction efforts directly contributed to the CMRE mission to retrograde with a focus to consolidate troops and equipment in Afghanistan. 526 Gladiators completed projects at five bases stretching across RC’s South and Southwest. 554 Assassins completed projects at seven bases stretching across RC’s South, Southwest and West. 984 Warriors completed projects at seven bases stretching across RC’s South and Southwest.<ref>{Unit Historian, 92nd Engineer Battalion, Fort Stewart - 2014}</ref>

==Company lineage==
<center>HHC - Headhunters, FSC - Coldsteel, 526th - Gladiators, 554th - Assassins, 530th - Bloodhounds</center>

===''526th Engineer Company''===
{| class="wikitable" style="width:100%;"
|-
! style="width:5%" |Year || style="width:10%" |Date || Event
|-
|align="center"| 1944 || 7 February || Constituted in the Army of the United States as the 526th Engineer Light Ponton Company
|-
|align="center"| 1944 || 20 April || Activated at Camp Shelby, Mississippi
|-
|align="center"| 1945 || 16 November || Reorganized and redesignated as the 526th Engineer Panel Bridge Transport Company
|-
|align="center"| 1949 || 27 May || Reorganized and redesignated as the 526th Engineer Panel Bridge Company; concurrently allotted to the Regular Army
|-
|align="center"| 1954 || 1 April || Reorganized and redesignated as the 526th Engineer Company
|-
|align="center"| 1971 || 31 March || Deactivated in Korea
|-
|align="center"| 2008 || 16 October || Activated at Fort Stewart, Georgia
|}

====Campaign participation credit====
{{col-begin|width=100%}}
{{col-break|width=50%}}

=====World War II=====
* Rhineland
* Central Europe

=====Korean War=====
* UN Offensive 
* CCF Intervention
* First UN Counteroffensive
* CCF Spring Offensive
* UN Summer-Fall Offensive
* Second Korean Winter
* Korea, Summer-Fall 1952
* Third Korean Winter
* Korea, Summer 1953
{{col-break|width=50%|heading}}
{{col-end}}
<gallery>
File:526th_Engineer_Company.png|<center>526th Horizontal Construction Engineer Company Icon</center>
<!-- Deleted image removed: File:526th_Engineer_Company_Photo.jpg|<center>526th Engineer Company Photo</center> -->
</gallery>

===984th Engineer Company===

{| class="wikitable" style="width:100%;"
|-
! style="width:5%" |Year || style="width:10%" |Date || Event
|-
|align="center"| 1943 || 26 November || Constituted in the Army of the United States as the 984th Engineer Maintenance Company. 
|-
|align="center"| 1943 || 30 December || Activated at Camp Swift, Texas.
|-
|align="center"| 1951 || 1 December || Reorganized and redesignated as the 984th Engineer Field Maintenance Company; concurrently allotted to the Regular Army.
|-
|align="center"| 1954 || 1 April || Reorganized and redesignated as the 984th Engineer Company.
|-
|align="center"| 1965 || 12 August || Deactivated in Germany.
|-
|align="center"| 1969 || 1 January || Activated in Vietnam. 
|-
|align="center"| 1971 || 26 December || Deactivated in Vietnam.
|-
|align="center"| 1975 || 21 January || Activated at Fort Stewart, Georgia.
|-
|align="center"| 1975 || 21 September || Deactivated at Fort Stewart, Georgia. 
|-
|align="center"| 2008 || 16 October || Activated at Fort Stewart, Georgia.<ref>{Department of the Army. 984th Engineer Company Lineage, U.S. Army Center of Military History. Fort Lesley J. McNair, DC}</ref>
|-
|align="center"| 2014 || 5 September || Deactivated at Fort Stewart, Georgia. 
|}

====Campaign participation credit====

{{col-begin|width=100%}}
{{col-break|width=50%}}

=====World War II=====
* Rhineland

=====Korean War=====
* First UN Counteroffensive
* CCF Spring Offensive
* UN Summer-Fall Offensive
* Second Korean Winter
* Korea, Summer-Fall 1952
* Third Korean Winter
* Korea, Summer 1953
{{col-break|width=50%|heading}}

=====Vietnam=====
* Counteroffensive Phase VI 
* Tet 69/Counteroffensive 
* Summer-Fall 1969
* Winter-Spring 1970 
* Sanctuary Counteroffensive 
* Counteroffensive Phase VII 
* Consolidation I 
* Consolidation II
{{col-end}}
<gallery>
File:984th_Horizontal_Construction_Engineer_Company.png|<center>984th Horizontal Construction Engineer Company Icon</center>
File:984th_Engineer_Company_Photo.jpg|<center>984th Engineer Company Photo</center>
</gallery>

===''554th Engineer Company''===
{| class="wikitable" style="width:100%;"
|-
! style="width:5%" |Year || style="width:10%" |Date || Event
|-
|align="center"| 1942 || 19 December || Constituted in the Army of the United States as the 554th Engineer Heavy Ponton Battalion. 
|-
|align="center"| 1943 || 25 January ||  Activated at Camp Swift, Texas.
|-
|align="center"| 1945 || 5 November ||  Redesignated as the 554th Engineer Ponton Bridge Company.
|-
|align="center"| 1949 || 30 April || Deactivated at Fort Monroe, Virginia.
|-
|align="center"| 1951 || 17 December || Redesignated as the 554th Engineer Panel Bridge Company. 
|-
|align="center"| 1951 || 19 December || Allotted to the Regular Army.  
|-
|align="center"| 1952 || 16 January || Activated at Camp McCoy, Wisconsin. 
|-
|align="center"| 1952 || 15 August || Deactivated at Camp McCoy, Wisconsin.
|-
|align="center"| 1954 || 11 October || Redesignated as the 554th Engineer Company.
|-
|align="center"| 1954 || 3 December || Activated at Fort Lewis, Washington.
|-
|align="center"| 1967 || 8 December || Deactivated in Vietnam. 
|-
|align="center"| 2008 || 16 October || Activated at Fort Stewart, Georgia.<ref>{Department of the Army. 554th Engineer Company Lineage, U.S. Army Center of Military History. Fort Lesley J. McNair, DC}</ref>
|}

====Campaign participation credit====
{{col-begin|width=100%}}
{{col-break|width=50%}}

=====World War II=====
:Northern France
:Rhineland
:Ardennes-Alsace
:Central Europe

=====Vietnam=====
:Counteroffensive, Phase II 
:Counteroffensive, Phase III

{{col-end}}
<gallery>
File:554th_Vertical_Construction_Engineer_Company.png|554th Vertical Construction Engineer Company Icon
File:554th_Engineer_Company_Photo.jpg|554th Engineer Company Photo
</gallery>

===''530th Engineer Company''===
[[File:530th reflexive fire.jpg|framed|center|<center>Reflexive Fire Stress Shoot - July 2012</center>]]

{| class="wikitable" style="width:100%;"
|-
! style="width:5%" |Year || style="width:10%" |Date || Event
|-
|align="center"| 1942 || 24 March || Constituted in Regular Army as 437th Engineer Company (Dump Truck) 
|-
|align="center"| 1942 || 20 May ||  Activated at Camp Claiborne, Louisiana
|-
|align="center"| 1943 || 25 June ||  530th Engineer Light Pontoon Company was formed at Camp Beale, California
|-
|align="center"| 1946 || 16 December ||   Deactivated at Fort Lewis, Washington
|-
|align="center"| 1947 || 30 January ||  Redesignated as 530th Engineer Dump Truck Company
|-
|align="center"| 1947 || 20 June ||  Activated in Europe
|-
|align="center"| 1947 || 1 September ||  Deactivated at Bad Nauheim, Germany
|-
|align="center"| 1954 || 29 September ||  Redesignated as 530th Engineer Company (Panel Bridge)
|-
|align="center"| 1954 || 15 November ||  Activated in Europe
|-
|align="center"| 2010 || 16 February ||   530th Route Clearance Company activated at Fort Stewart, Georgia.
|}

====Campaign participation credit====

=====World War II=====
:Sicily
:Naples-Foggia
:Rome-Arno
:North Apennines
<gallery>
File:530th_Route_Clearance_Company_Icon.png|530th Route Clearance Company Icon
<!-- Deleted image removed: File:530th_Engineer_Company.jpg|530th Engineer Company  -->
<!-- Deleted image removed: File:530th_Company_Photo_Afghanistan.jpg|Afghanistan 2011-2012 -->
File:4th_platoon_-530th_EN_CO.jpg|4th Platoon blows-in-place a found IED during a clearance mission to the dangerous Ganjitsu Kalay village, near the AF-Pak border
File:Expert_Sapper_-_530th.jpg|530th Soldiers negotiate a 400-meter buddy team poncho raft swim as part of the Expert Sapper Competition
<!-- Deleted image removed: File:530th_Engineer_Dump_Truck_Icon.jpg|530th Engineer Dump Truck Company Distinctive Unit Insignia -->
</gallery>

==Detachments==

===''36th Engineer Detachment''===

''THE CAN DO CREW''

{| class="wikitable" style="width:100%;"
|-
! style="width:5%" |Year || style="width:10%" |Date || Event
|-
|align="center"| 1945 || 26 June || Constituted in Army of the United States as the 3336th Engineer Refrigeration Maintenance Detachment  
|-
|align="center"| 1945 || 26 July ||  Activated in France
|-
|align="center"| 1945 || 12 November ||  Deactivated in France
|-
|align="center"| 1953 || 5 February || Redesignated as the 36th Engineer Water Tank Detachment and allotted to the Regular Army  
|-
|align="center"| 1953 || 10 April || Activated in Korea
|-
|align="center"| 1954 || 1 April ||  Reorganized and redesignated as the 36th Engineer Detachment
|-
|align="center"| 1955 || 20 January ||  Deactivated in Korea
|-
|align="center"| 1957 || 15 October ||  Activated in Japan
|-
|align="center"| 1959 || 25 March ||  Deactivated in Korea
|-
|align="center"| 1963 || 26 March ||  Activated at Fort Benning, Georgia
|-
|align="center"| 1969 || 17 February || Deactivated in Vietnam
|-
|align="center"| 1970 || 30 October ||  Activated at Fort Benjamin Harrison, Indiana 
|-
|align="center"| 1987 || 15 August ||  Deactivated at Fort Benjamin Harrison, Indiana 
|-
|align="center"| 2008 || 16 October ||  Activated at Fort Stewart, Georgia
|}

====Campaign participation credit====
{{col-begin|width=100%}}
{{col-break|width=50%}}

=====Korean War=====
* Third Korean Winter
* Korea, Summer 1953

=====Vietnam=====
* Defense
* Counteroffensive
* Counteroffensive Phase II
* Counteroffensive Phase III 
* Tet 69/Counteroffensive 
* Counteroffensive Phase IV
* Counteroffensive Phase V 
* Counteroffensive Phase VI

=====War on Terrorism=====
{{col-end}}

<!-- Deleted image removed: File:36th_Detachment.jpg|36th Engineer Detachment -->


===''514th Firefighter Detachment''===
{| class="wikitable" style="width:100%;"
|-
! style="width:5%" |Year || style="width:10%" |Date || Event
|-
|align="center"| 1943 || 12 August || Constituted in Army of the United States as the 1098th Engineer Utilities Detachment  
|-
|align="center"| 1943 || 14 August ||  Activated at Camp Claiborne, Louisiana
|-
|align="center"| 1946 || 13 March ||  Deactivated in France
|-
|align="center"| 1946 || 10 December || Activated in Korea
|-
|align="center"| 1947 || 30 June || Redesignated as the 514th Engineer Utilities Detachment  
|-
|align="center"| 1949 || 25 January ||  Deactivated in Korea
|-
|align="center"| 1952 || 25 August ||  Redesignated as the 514th Engineer Carbon Dioxide Generating Detachment and allotted to the Regular Army 
|-
|align="center"| 1952 || 18 September ||  Activated at Fort Leonard Wood, Missouri
|-
|align="center"| 1953 || 2 October ||  Reorganized and redesignated as the 514th Engineer Detachment
|-
|align="center"| 1971 || 15 April ||  Deactivated in Vietnam
|-
|align="center"| 1980 || 1 September || Activated at Fort Stewart, Georgia 
|-
|align="center"| 1988 || 15 July ||  Deactivated at Fort Stewart, Georgia 
|-
|align="center"| 2005 || 16 October ||  Activated at Hunter Army Airfield, Georgia
|}

====Campaign participation credit====
{{col-begin|width=100%}}
{{col-break|width=50%}}

=====World War II=====
* [[Normandy]]
* Northern France

=====Vietnam=====
* Counteroffensive Phase II
* Counteroffensive Phase III 
* Tet Counteroffensive 
* Counteroffensive Phase IV
* Counteroffensive Phase V 
* Counteroffensive Phase VI 
* Tet 69/Counteroffensive
* Summer-Fall 1969
* Winter-Spring 1970
* Sanctuary Counteroffensive
* Counteroffensive Phase VII

=====War on Terrorism=====
{{col-end}}

<gallery>
File:514th Firefighters.jpg|514th Firefighter Detachment ran in the Ft Stewart, Red Cross 5K on 1 March. SGT Rodriguez, SPC Carnes, and SPC Hardy all completed the run in turn-out gear
File:514th_firefighters_92nd.jpg|514th Firefighters at Red Cross Run
</gallery>

==See also==
* [[Coats of arms of U.S. Engineer Battalions]]
* [[Military engineering of the United States]]
* [[Combat engineer]]
* [[Military engineering]]

==References==
{{reflist}}
*Archival Research Catalog. http://arcweb.archives.gov

==External links==
*[https://www.facebook.com/#!/pages/92nd-Engineer-Battalion/184951174871488 92nd Engineer Battalion Facebook Page]

[[Category:Engineer battalions of the United States Army]]
[[Category:Military units and formations in Georgia (U.S. state)]]