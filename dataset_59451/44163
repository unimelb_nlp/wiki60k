{{Infobox airport
| name         = Cartersville Airport
| image        = Cartersville Airport July 2016.jpg
| IATA         = <!--not VPC-->
| ICAO         = KVPC
| FAA          = VPC
| type         = Public
| owner        = Cartersville Bartow Airport Authority
| operator     = 
| city-served  = [[Cartersville, Georgia]]
| location     = <!--if different than above-->
| elevation-f  = 759
| elevation-m  = 231
| coordinates  = {{coord|34|07|23|N|084|50|55|W|region:US-GA_type:airport_scale:10000}}
| website      = 
| r1-number    = 1/19
| r1-length-f  = 5,760
| r1-length-m  = 1,756
| r1-surface   = Asphalt
| stat-year    = 2009
| stat1-header = Aircraft operations
| stat1-data   = 50,500
| stat2-header = Based aircraft
| stat2-data   = 145
| footnotes    = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=VPC|use=PU|own=PU|site=03709.1*A}}. Federal Aviation Administration. Effective 29 July 2010.</ref>
}}

'''Cartersville Airport''' {{airport codes||KVPC|VPC}} is a public use [[airport]] located three nautical miles (6&nbsp;km) southwest of the [[central business district]] of [[Cartersville, Georgia|Cartersville]], in [[Bartow County, Georgia|Bartow County]], [[Georgia (U.S. state)|Georgia]], [[United States]]. It is owned by the Cartersville Bartow Airport Authority.<ref name="FAA" />

Although many U.S. airports use the same three-letter [[location identifier]] for the [[Federal Aviation Administration|FAA]] and [[International Air Transport Association|IATA]], this facility is assigned '''VPC''' by the FAA but has no designation from the IATA.<ref>{{cite web | url = http://www.gcmap.com/airport/KVPC | title = Yolo County Airport (FAA: VPC, ICAO: KVPC) | publisher = Great Circle Mapper | accessdate = 15 August 2010}}</ref>

== Facilities and aircraft ==
Cartersville Airport covers an area of {{convert|185|acre|ha}} at an [[elevation]] of 759 feet (231 m) above [[mean sea level]]. It has one [[runway]] designated 1/19 with an asphalt surface measuring 5,760 by 100 feet (1,756 x 30 m).<ref name="FAA" />

For the 12-month period ending April 23, 2009, the airport had 50,500 aircraft operations, an average of 138 per day: 89% [[general aviation]], 10% [[air taxi]], and 1% [[military aviation|military]]. At that time there were 145 aircraft based at this airport: 53% single-[[aircraft engine|engine]], 19% multi-engine, 15% [[jet aircraft|jet]], 1% [[helicopter]] and 12% [[ultralight]].<ref name="FAA" />

== References ==
<references />

== External links ==
* [http://msrmaps.com/map.aspx?t=1&s=12&lat=34.1231&lon=-84.8487&w=500&h=500&lp=---+None+--- Aerial photo] from [[USGS]] ''[[The National Map]]''
* {{FAA-procedures|VPC}}
* {{US-airport-ga|VPC|-}}

<!--Navigation box--><br />
{{Atlanta airports}}

[[Category:Airports in Georgia (U.S. state)]]
[[Category:Buildings and structures in Bartow County, Georgia]]
[[Category:Transportation in Bartow County, Georgia]]