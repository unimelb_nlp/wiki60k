{{distinguish|Prince George of Cambridge}}
{{Use British English|date=June 2011}}
{{Use dmy dates|date=March 2013}}
{{Infobox royalty
| name                 = Prince George
| image                = George 1st Kent.png
| image_size           = 229
| succession           = [[Duke of Kent]]
| successor            = [[Prince Edward, Duke of Kent|Prince Edward]]
| spouse               = {{marriage|[[Princess Marina of Greece and Denmark]]<br>|1934}}
| issue                = [[Prince Edward, Duke of Kent]]<br/>[[Princess Alexandra, The Honourable Lady Ogilvy|Princess Alexandra, The Hon. Lady Ogilvy]]<br />[[Prince Michael of Kent]]
| full name            = George Edward Alexander Edmund
| titles               = ''HRH'' The Duke of Kent<br />''HRH'' The Prince George<br />''HRH'' Prince George of Wales
| house                = [[House of Windsor|Windsor]] <small>(since 1917)</small> <br />[[House of Saxe-Coburg and Gotha|Saxe-Coburg and Gotha]] <small>(until 1917)</small>
| father               = [[George V]]
| mother               = [[Mary of Teck]]
| birth_date        = {{Birth date|1902|12|20|df=y}}
| birth_place       = [[York Cottage]], [[Sandringham, Norfolk|Sandringham]]
| date of christening  = 26 January 1903
| place of christening = [[St George's Chapel at Windsor Castle|St George's Chapel]], [[Windsor, Berkshire|Windsor]]
| death_date        = {{Death date and age|1942|8|25|1902|12|20|df=y}}
| death_place       = [[Morven, Caithness|Morven]], Scotland
| date of burial = 29 August 1942
| place of burial      = [[St George's Chapel at Windsor Castle|St George's Chapel]], [[Windsor, Berkshire|Windsor]] and later [[Royal Burial Ground, Frogmore]]
}}
'''Prince George, Duke of Kent''', {{post-nominals|country=GBR|size=100%|sep=,|KG|KT|GCMG|GCVO}} (George Edward Alexander Edmund; 20 December 1902 – 25 August 1942) was the fourth son and fifth child of [[George V|King George V]] and [[Mary of Teck|Queen Mary]].

He was the younger brother of Kings [[Edward VIII]] and [[George VI]]. He held the title of [[Duke of Kent]] from 1934 until his death in a military air-crash on 25 August 1942.

==Early life==
Prince George was born on 20 December 1902 at [[York Cottage]] on the [[Sandringham House|Sandringham Estate]] in Norfolk, England.<ref name=ppg26aug>{{cite news|title=Duke of Kent once called sailor prince|url=https://news.google.com/newspapers?id=dtkOAAAAIBAJ&sjid=C2oDAAAAIBAJ&pg=5896,1218980&dq=prince+george+duke+of+kent&hl=en|accessdate=23 March 2013|newspaper=Pittsburg Post Gazette|date=26 August 1945}}</ref> His father was George, Prince of Wales (later [[George V|King George V]]), the only surviving son of [[Edward VII|King Edward VII]] and [[Alexandra of Denmark|Queen Alexandra]]. His mother was the Princess of Wales (later [[Mary of Teck|Queen Mary]]), the daughter of the [[Prince Francis, Duke of Teck|Duke]] and [[Princess Mary Adelaide of Cambridge|Duchess of Teck]].<ref name=dexp26feb/> At the time of his birth, he was fifth in the [[Succession to the British throne|line of succession to the throne]], behind his father and three older brothers.

George was baptised in the Private Chapel at [[Windsor Castle]] on 26 January 1903 by [[Francis Paget]], [[Bishop of Oxford]].<ref name="Yvonne"/>{{refn|Unlike many previous royal baptisms, George was christened using local water, rather than water from the [[River Jordan]].<ref name="Yvonne">[http://users.uniserve.com/~canyon/christenings.htm#Christenings Yvonne's Royalty Home Page— Royal Christenings]</ref>}}

==Education and career==
Prince George received his early education from a tutor and then followed his elder brother, Prince Henry (later the [[Prince Henry, Duke of Gloucester|Duke of Gloucester]]), to [[St Peter's Court]], a [[Preparatory school (United Kingdom)|preparatory school]] at [[Broadstairs]], [[Kent]]. At the age of thirteen, like his brothers, the [[Edward VIII|Prince of Wales]] (later King Edward VIII) and [[George VI|Prince Albert]] (later King George VI), before him, he went to naval college, first at [[Osborne House|Osborne]] and, later, at [[Britannia Royal Naval College|Dartmouth]].<ref name=ppg26aug/> He remained in the [[Royal Navy]] until March 1929, serving on {{HMS|Iron Duke|1912|6}} and later {{HMS|Nelson|28|6}}.<ref name=ppg26aug/> After leaving the navy, he briefly held posts at the Foreign Office and later the Home Office, becoming the first member of the [[British Royal Family|royal family]] to work as a civil servant.<ref name=ppg26aug/>

From January to April 1931 Prince George and his elder brother the Prince of Wales travelled 18,000 miles on a tour of [[South America]]. Their outward voyage was on the [[ocean liner]] {{SS|Oropesa||2}}.<ref>{{citation |url= http://www.ecsodus.com/PSNC/fleet/O-1920.html |last=Erskine |first=Barry |title=Oropesa (II) |work=Pacific Steam Navigation Company |accessdate=15 December 2013}}</ref> In Buenos Aires they opened a British Empire Exhibition.<ref>{{cite book |last=Nicol |first=Stuart |year=2001 |title=MacQueen's Legacy; Ships of the Royal Mail Line |volume=Two |place=Brimscombe Port and Charleston, SC |publisher=[[The History Press|Tempus Publishing]] |isbn=0-7524-2119-0 |page=130}}</ref> They continued from the [[Río de la Plata|River Plate]] to [[Rio de Janeiro]] on the liner {{RMS|Alcantara|1926|2}} and returned from Brazil to Europe on the liner {{RMS|Arlanza|1912|2}}, landing at [[Lisbon]].<ref>{{cite book |last=Nicol |first=Stuart |year=2001 |title=MacQueen's Legacy; A History of the Royal Mail Line |volume=One |place=Brimscombe Port and Charleston, SC |publisher=[[The History Press|Tempus Publishing]] |isbn=0-7524-2118-2 |page=158}}</ref> The princes returned ''via'' Paris and an [[Imperial Airways]] flight from [[Paris–Le Bourget Airport]] that landed specially in Windsor Great Park.<ref>{{citation |url= http://newspapers.nl.sg/Digitised/Article/straitstimes19310430-1.2.53.aspx |title=Arrival at Windsor by Air |newspaper=[[The Straits Times]] |publisher=[[National Library, Singapore]] |date=30 April 1931 |accessdate=18 December 2013}}</ref><ref>{{citation |url= http://trove.nla.gov.au/ndp/del/article/45763837 |title=Princes Home |newspaper=[[The Advertiser (Adelaide)|The Advertiser and Register]] |publisher=[[National Library of Australia]] |date=1 May 1931 |accessdate=18 December 2013}}</ref>

In October 1938 George was appointed [[Governor General of Australia]] in succession to [[Alexander Hore-Ruthven, 1st Earl of Gowrie|Lord Gowrie]] with effect from November 1939.<ref>"The Duke of Kent: Appointment in Australia", ''The Times'' (26 October 1938): 14.</ref><ref>{{cite news|title=Marina, a tragic but well-loved Princess|url=https://news.google.com/newspapers?id=BLIpAAAAIBAJ&sjid=_OYDAAAAIBAJ&pg=3140,8817486&dq=princess+marina&hl=en|accessdate=24 July 2013|newspaper=The Sydney Morning Herald|date=28 August 1968|location=London}}</ref> On 11 September 1939 it was announced that, owing to the outbreak of the [[Second World War]], the appointment was postponed.<ref>"Duke of Kent and Australia", ''The Times'' (12 September 1939): 6.</ref>

At the start of the [[Second World War]], George returned to active military service in the rank of [[rear admiral]], briefly serving on the [[Naval Intelligence Division|Intelligence Division]] of the [[Admiralty]]. In April 1940, he transferred to the [[Royal Air Force]]. He temporarily relinquished his rank as [[air vice-marshal]] (the equivalent of rear admiral) to assume the post of staff officer at [[RAF Training Command]] in the rank of [[group captain]].

==Marriage==
[[File:Combined Coat of Arms of George and Marina, the Duke and Duchess of Kent.svg|thumb|Combined coat of arms of George and Marina, the Duke and Duchess of Kent]]
On 12 October 1934, in anticipation of his forthcoming marriage to his second cousin [[Princess Marina, Duchess of Kent|Princess Marina of Greece and Denmark]] he was created Duke of Kent, Earl of St Andrews, and Baron Downpatrick.<ref name=dexp26feb/><ref name=creation>[http://mypage.uniserve.ca/~canyon/peerage_titles.htm#Holders Yvonne's Royalty: Peerage]</ref><ref>{{London Gazette|issue=34094|date=9 October 1934|startpage=6365|city=London|accessdate=1 May 2011}}</ref> The couple married on 29 November 1934 at [[Westminster Abbey]].<ref>{{cite news|title=King and Queen|url=https://news.google.com/newspapers?id=gBRkAAAAIBAJ&sjid=BnsNAAAAIBAJ&pg=1563,3406056&dq=prince+george+duke+of+kent&hl=en|accessdate=23 March 2013|newspaper=The Calgary Daily Herald|date=29 November 1934}}</ref> They had three children:
* [[Prince Edward, Duke of Kent]] (born 9 October 1935)
* [[Princess Alexandra, The Honourable Lady Ogilvy|Princess Alexandra, The Hon. Lady Ogilvy]] (born 25 December 1936)
* [[Prince Michael of Kent]] (born 4 July 1942)

==Personal life==
[[File:The Duke and Duchess of Kent.jpg|thumb|left|The Duke and Duchess in 1934]]
{{House of Windsor|george5}}
Both before and after his marriage, Prince George had a string of affairs with both men and women, from socialites to Hollywood celebrities. The better known of his lovers included banking heiress Poppy Baring, socialite [[Margaret, Duchess of Argyll|Margaret Whigham]] (later Duchess of Argyll and involved in a notoriously scandalous divorce case), and [[Barbara Cartland]] (who believed him to be the father of her daughter [[Raine Spencer, Countess Spencer|Raine McCorquodale]]).<ref>{{cite news|last=Thornton|first=Michael|title=A drunken husband and five secret lovers: The novel Barbara Cartland never wanted you to read|url=http://www.dailymail.co.uk/femail/article-1080454/A-drunken-husband-secret-lovers-The-novel-Barbara-Cartland-wanted-read.html|accessdate=11 September 2012|newspaper=Daily Mail|date=24 October 2008}}</ref> There were "strong rumours" that he had affairs with musical star [[Jessie Matthews]]<ref name="Panton">Kenneth J. Panton [https://books.google.com/books?id=BiyyueBTpaMC&pg=PA217&lpg=PA217&dq=%22Duke+of+Kent%22+%22Jessie+Matthews%22&source=bl&ots=biK3SvjBUq&sig=Tb6SSvzsGNkDU2swduHOYc5qSok&hl=en&sa=X&ei=7CnPUbv4LIa04ASZiYDgDQ&ved=0CEEQ6AEwAzgK#v=onepage&q=%22Duke%20of%20Kent%22%20%22Jessie%20Matthews%22&f=false''Historical Dictionary of the British Monarchy''], Lanham,MD: Scarecrow Press, 2011, p.217</ref>, writer  [[Cecil Roberts]]<ref>King, Francis Henry ''Yesterday Came Suddenly'', Constable (London) 1993, p278</ref>  and [[Noël Coward]],<ref>Barry Day, ed., "The Letters of Noël Coward," (NY: Alfred A. Knopf, 2007), p. 691</ref> a relationship which Coward's long-term boyfriend, [[Graham Payn]], denied.<ref>[[Gyles Brandreth|Brandreth, Gyles]] (2004). ''Philip and Elizabeth: Portrait of a Marriage''. London: Century. ISBN 0-7126-6103-4, p. 94</ref> The security services "reported that Coward and Kent had been seen parading together through the streets of London, dressed and made up as women, and had once been arrested by the police for suspected prostitution".<ref name="Daily Mail">{{cite news|url=http://www.dailymail.co.uk/femail/article-492770/How-predatory-Noel-Cowards-tried-seduce-I-19-author-Micahel-Thornton.html#ixzz26H2L1UWM|title=How predatory Noel Coward tried to seduce me when I was 19|newspaper=The Daily Mail|date=9 November 2007|accessdate=12 September 2012|author=Thorton, Michael}}</ref>

The Duke of Kent is rumoured to have been addicted to drugs, especially [[morphine]] and [[cocaine]], a rumour which reputedly originated with his friendship with [[Kiki Preston]],<ref>Lynn Kear and John Rossman [https://books.google.com/books?id=QWRWZu2EsKAC&pg=PA28&dq=%22Duke+of+Kent%22+Morphine+Cocaine&hl=en&sa=X&ei=QyHPUaiLO_Sq4gS_tYAo&ved=0CDEQ6AEwAA#v=onepage&q=%22Duke%20of%20Kent%22%20Morphine%20Cocaine&f=false ''Kay Francis: A Passionate Life and Career''], Jefferson: NC: McFarland & Company, 2006, p. 28</ref><ref name="Farrant">Farrant, Leda (1994). ''Diana, Lady Delamere and the Lord Erroll Murder'', p. 77. Publishers Distribution Services.</ref><ref>McLeod, Kirsty. ''Battle Royal: Edward VIII & George VI, Brother Against Brother'', p. 122. Constable</ref> whom he first met in the mid-1920s. Reportedly, Prince George shared Kiki in a [[ménage à trois]] with Jorge Ferrara, the bisexual son of the [[Argentina|Argentinian]] ambassador to [[UK|Britain]].<ref>Nicholson, Stuart (1999). ''Reminiscing in Tempo: A Portrait of Duke Ellinson'', p. 146. Northeastern University Press</ref> Other alleged sexual liaisons were with the art historian and Soviet spy [[Anthony Blunt]] and [[Indira Devi|Indira Raje]], [[Maharaja|Maharani]] of [[Cooch Behar]].<ref name="Panton"/>

In his attempt to rescue his cocaine-addicted brother from the influence of Kiki, [[Edward VIII]] attempted for a while to persuade both George and Kiki to break off their contact, to no avail.<ref>Ziegler, Philip (2001). ''King Edward VIII'', p. 200. Sutton</ref> Eventually, Edward forced George to stop seeing Kiki and also forced Kiki to leave England, while she was visiting George there in the summer of 1929.<ref>[https://books.google.com/books?id=z4r3xTUVuZMC&pg=PA31&dq=kiki+preston&hl=en&ei=D3q2S5GKDJuP4ga2w5z9Dg&sa=X&oi=book_result&ct=result&resnum=6&ved=0CEcQ6AEwBTgU#v=onepage&q=kiki%20preston&f=false Williams, Susan A. (2004). ''The People's King: The True Story of the Abdication'', p. 31. New York: Palgrave Macmillan]</ref> For years afterwards, Edward feared that George might relapse to drugs if he maintained his contact with Kiki. Indeed, in 1932, Prince George ran into Kiki unexpectedly at [[Cannes]] and had to be removed almost by force.<ref>Kiste, John van Der (1991). ''George V's Children'', p. 71. A. Sutton.</ref>

It has been alleged for years that American publishing executive Michael Temple Canfield (1926&ndash;1969) was the illegitimate son of Prince George and Kiki Preston. According to various sources, both Prince George's brother, the [[Edward VIII|Duke of Windsor]] and Laura, Duchess of Marlborough, Canfield's second wife, shared this belief.<ref>Higham, Charles (1988). ''Wallis: Secret Lives of the Duchess of Windsor'', p. 392. Sidgwick & Jackson</ref><ref>Horsler, Val (2006). ''All For Love: Seven Centuries of Illicit Liaison'', p. 183. National Archives</ref><ref>Lindsay, Loelia (1961). ''Grace and Favour: The Memoirs of Loelia, Duchess of Westminster. Reynal</ref><ref>Bradford, Sarah (2000). ''America's Queen: The Life of Jacqueline Kennedy Onassis'', p. 84. Viking</ref> Canfield was the adopted son of [[Cass Canfield]], American publisher of [[Harper and Row]].<ref name="Can">[https://news.google.com/newspapers?nid=1955&dat=19670910&id=yB4rAAAAIBAJ&sjid=DKAFAAAAIBAJ&pg=3555,4750932 "The Prince's Cousin", ''Reading Eagle'', September 10, 1967]</ref> In 1953 Canfield married [[Lee Radziwill|Caroline Lee Bouvier]] the younger sister of [[Jacqueline Bouvier]] who married [[US Senator]] and future [[US president]] [[John F. Kennedy]] the same year. Canfield and  Bouvier divorced in 1959, and the marriage was annulled by the Roman Catholic Church in November 1962.<ref name=annul>{{cite news|title=Roman Catholics: The Law's Delay|url=http://www.time.com/time/magazine/article/0,9171,873839-1,00.html|publisher=[[Time-Life]]|location=New York Cit|date=February 28, 1964|accessdate=September 4, 2009}}</ref>

==RAF career==
[[File:Royal Air Force Ferry Command, 1941-1943. CH3161.jpg|thumb|right|The Duke of Kent before he crossed the Atlantic by air]]

As a young man the duke came to the opinion that the future lay in aviation. It became his passion, and in 1929 the duke earned his pilot's licence. He was the first of the Royal family to cross the [[Atlantic Ocean|Atlantic]] by air. Prior to his flying days, he entered the [[Royal Navy]], and was trained in intelligence work while stationed at [[Rosyth]].<ref>Macwhirter, Robin, 'The Tragedy at Eagle's Rock', Scotsman, 24 August 1985</ref>

In 1937, he was granted a commission in the [[Royal Air Force]] as a [[group captain]].<ref>{{London Gazette|issue=34379 |date=12 March 1937|startpage=1646|accessdate=5 June 2009}}</ref> He was also made the [[Honorary Air Commodore]] of [[No. 500 Squadron RAF|No. 500 (County of Kent) Squadron]] [[Royal Auxiliary Air Force|Auxiliary Air Force]].<ref>{{Harvnb|Hunt|1972|p=314.}}</ref> Just before war broke out he became an RAF Air Vice-Marshal (approximately equal in rank to his Rear Admiral status earlier in the Royal Navy). In a characteristic gesture,{{citation needed|date=January 2016}} he relinquished that rank in 1940 so that he would not be senior to more experienced officers, becoming a lower-ranked group captain and, in July 1941, an air commodore in the Welfare Section of the RAF Inspector General's Staff.{{citation needed|date=May 2015}}  In this role he went on official visits to RAF bases to help boost wartime morale.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1942/1942%20-%201829.html|title=Royal family; aircraft engineer; 1942|publisher=Flight Archive|accessdate=23 March 2013}}</ref>  His death while in the service of the RAF marked the first time in more than 450 years that a member of the Royal family died on active service.<ref>https://newbattleatwar.wordpress.com/category/aviation-2/</ref>

==Death==
Prince George died on 25 August 1942, at the age of 39, along with fourteen others, on board RAF [[Short Sunderland]] [[flying boat]] W4026, which crashed into a hillside near [[Dunbeath]], [[Caithness]], Scotland, while flying from [[Invergordon]], [[Ross and Cromarty]], to [[Iceland]] on non-operational duties.<ref>{{cite news|title= Duke of Kent Dies in an R.A.F. Crash on way to Iceland |url= http://query.nytimes.com/mem/archive/pdf?res=F00F1EFC3558167B93C4AB1783D85F468485F9 |accessdate= 11 September 2012 |newspaper= [[The New York Times]] |date= 26 August 1942}}</ref> [[Lynn Picknett]] and [[Clive Prince]] alleged in their book ''Double Standards'', which has been criticized for its "implausible inaccuracy",<ref>
{{cite book
| last1                 = Rubinstein
| first1                = William D.
| author-link1          = William D. Rubinstein
| chapter               = 7: The Mysteries of Rudolf Hess
| title                 = Shadow Pasts: History's Mysteries
| url                   = https://books.google.com/books?id=UPiYKsL25ZIC
| location              = Harlow, England
| publisher             = Pearson/Longman
| publication-date      = 2008
| page                  = 147
| isbn                  = 9780582505971
| accessdate            = 2017-02-18
| quote                 = ... probably the strangest book ever written on the Hess affair is ''Double Standards''... The thesis of ''Double Standards'' is that [[Rudolf Hess]] ... died in the plane crash in northern Scotland in August 1942 which also killed the Duke of Kent .... Hess was being transported to neutral Sweden (not Iceland, given in the official story as the plane's destination) to be handed over to the Germans as the first step in a settlement of the war between Britain and Germany. ... ''Double Standards'' seems breathtaking in its implausible inaccuracy.
}}
</ref> that Prince George had a briefcase full of 100-[[Swedish krona|krona]] notes, worthless in Iceland, handcuffed to his wrist, leading to speculation the flight was a military mission to Sweden, the only place where krona notes were of value.<ref>
''Double Standards'' p.424
</ref>

The Prince's body was transferred initially to [[St. George's Chapel, Windsor]], and he was buried in the [[Royal Burial Ground, Frogmore]], directly behind [[Queen Victoria]]{{'}}s mausoleum. His elder son, seven-year-old [[Prince Edward, Duke of Kent|Prince Edward]], succeeded him  as [[Duke of Kent]]. Princess Marina, his wife, had given birth to their third child, [[Prince Michael of Kent|Prince Michael]], only seven weeks before Prince George's death.

==In popular culture==
The Duke's early life is dramatised in [[Stephen Poliakoff]]'s television serial ''[[The Lost Prince]]'' (2003), a biography of the life of [[Prince John of the United Kingdom|the Duke's younger brother John]]. In the film, the teenage Prince 'Georgie' is portrayed as sensitive, intelligent, artistic and almost uniquely sympathetic to his brother's plight. He is shown as detesting his time at the Royal Naval College and as having a difficult relationship with his austere father.

Much of George's later life was outlined in the documentary film ''The Queen's Lost Uncle''.<ref>{{cite web|url=http://www.channel4.com/programmes/the-queens-lost-uncle|title=The Queen's Lost Uncle|publisher=Channel 4|accessdate=18 May 2015}}</ref> He is a recurring character in the revival of ''[[Upstairs Downstairs (2010 TV series)|Upstairs, Downstairs]]'' (2010), played by [[Blake Ritson]].<ref name=dexp26feb>{{cite news|title=Upstairs life of a royal rogue|url=http://www.express.co.uk/posts/view/304572/Upstairs-life-of-a-royal-rogue/|accessdate=23 March 2013|newspaper=Daily Express|date=26 February 2012}}</ref> He is portrayed as a caring brother, terrified of the mistakes that his family is making; later, he is portrayed as an appeaser of the German regime, but also as a supportive friend of Hallam Holland.<ref name=dexp26feb/>

George and his eldest brother the [[Edward VIII|Prince of Wales]], later King Edward VIII, are shown in [[Stephen Poliakoff]]'s BBC television serial ''[[Dancing on the Edge (TV series)|Dancing on the Edge]]'' (2013), in which they are portrayed as supporters of jazz and encouragers of Louis Lester's Jazz Band. A sexual attraction to Louis on George's part is also insinuated.<ref name=fur1feb>{{cite news|last=Furness|first=Hannah|title=New BBC drama to show the scandalous stories of the playboy Princes|url=http://www.telegraph.co.uk/culture/tvandradio/bbc/9843404/New-BBC-drama-to-show-the-scandalous-stories-of-the-playboy-Princes.html|accessdate=23 March 2013|newspaper=The Telegraph|date=1 February 2013}}</ref>

==Titles, styles, honours and arms==

===Titles and styles===
*'''20 December 1902 – 6 May 1910''': ''His Royal Highness'' Prince George of Wales{{refn|As a grandchild of a British monarch in the male line, he was styled ''His Royal Highness Prince George of Wales".}}
*'''6 May 1910 – 12 October 1934''': ''His Royal Highness'' The Prince George
*'''12 October 1934 – 25 August 1942''': ''His Royal Highness'' The Duke of Kent
**''in Scotland'' from '''May 1935''': ''His Grace'' the Lord High Commissioner

At the time of his death, Prince George's full style was ''His Royal Highness The Prince George Edward Alexander Edmund, Duke of Kent, Earl of Saint Andrews and Baron Downpatrick, Royal Knight of the Most Noble Order of the Garter, Royal Knight of the Most Ancient and Most Noble Order of the Thistle, Knight Grand Cross of the Most Distinguished Order of Saint Michael and Saint George, Knight Grand Cross of the Royal Victorian Order.''

[[Image:Coat of Arms of George, Duke of Kent.svg|220px|right|thumb|Prince George's [[coat of arms]]]]

===Honours===
'''British honours'''
*'''KG:''' [[Order of the Garter|Knight of the Garter]], 1923
*'''KT:''' [[Order of the Thistle|Knight of the Thistle]], 1935
*'''GCMG:''' [[Order of St Michael and St George|Knight Grand Cross of the Order of St Michael and St George]], 1934
*'''GCVO:''' [[Royal Victorian Order|Knight Grand Cross of the Royal Victorian Order]], 1924
*[[Royal Victorian Chain]]

====Appointments====
* '''1932''': Royal Bencher of the Honourable Society of [[Lincoln's Inn]]

===Arms===
Around the time of his elder brother Prince Henry's twenty-first birthday, Prince George was granted the use of the Royal Arms, differenced by a label argent of three points, each bearing an anchor azure.
{{Clear}}

==Ancestry==
{{Ahnentafel top|width=100%}}
<center>{{Ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Prince George, Duke of Kent'''
|2= 2. [[George V|George V of the United Kingdom]]
|3= 3. [[Mary of Teck|Princess Mary of Teck]]
|4= 4. [[Edward VII|Edward VII of the United Kingdom]]
|5= 5. [[Alexandra of Denmark|Princess Alexandra of Denmark]]
|6= 6. [[Francis, Duke of Teck]]
|7= 7. [[Princess Mary Adelaide of Cambridge]]
|8= 8. [[Albert, Prince Consort|Prince Albert of Saxe-Coburg and Gotha]]
|9= 9. [[Queen Victoria|Victoria of the United Kingdom]]
|10= 10. [[Christian IX of Denmark|Christian IX, King of Denmark]]
|11= 11. [[Louise of Hesse-Kassel|Princess Louise of Hesse-Kassel]]
|12= 12. [[Duke Alexander of Württemberg (1804–1885)|Duke Alexander of Württemberg]]
|13= 13. [[Countess Claudine Rhédey von Kis-Rhéde]]
|14= 14. [[Prince Adolphus, Duke of Cambridge]]
|15= 15. [[Princess Augusta of Hesse-Kassel]]
|16= 16. [[Ernest I, Duke of Saxe-Coburg and Gotha]]
|17= 17. [[Princess Louise of Saxe-Gotha-Altenburg (1800–1831)|Princess Louise of Saxe-Gotha-Altenburg]]
|18= 18. [[Prince Edward, Duke of Kent and Strathearn]]
|19= 19. [[Princess Victoria of Saxe-Coburg-Saalfeld]]
|20= 20. [[Friedrich Wilhelm, Duke of Schleswig-Holstein-Sonderburg-Glücksburg|Frederick William, Duke of<br>Schleswig-Holstein-Sonderburg-Glücksburg]]
|21= 21. [[Princess Louise Caroline of Hesse-Kassel]]
|22= 22. [[Prince William of Hesse-Kassel]]
|23= 23. [[Princess Louise Charlotte of Denmark]]
|24= 24. [[Duke Louis of Württemberg]]
|25= 25. [[Princess Henriette of Nassau-Weilburg (1780–1857)|Princess Henriette of Nassau-Weilburg]]
|26= 26. Count László Rhédey von Kis-Rhéde
|27= 27. Baroness Ágnes Inczédy von Nagy-Várad
|28= 28. [[George III of the United Kingdom]]
|29= 29. [[Charlotte of Mecklenburg-Strelitz|Duchess Charlotte of Mecklenburg-Strelitz]]
|30= 30. [[Landgrave Frederick of Hesse-Kassel|Landgrave Prince Frederick of Hesse-Kassel]]
|31= 31. [[Princess Caroline of Nassau-Usingen]]
}}</center>
{{Ahnentafel bottom}}

==See also==
*[[British Royal Family]]

==References==
{{Reflist|33em}}

==Further reading==
* {{cite book|last=Hunt|first=Leslie|title=Twenty-one Squadrons: History of the Royal Auxiliary Air Force, 1925–57|location=London |publisher=Garnstone Press|year=1972|ISBN=0-85511-110-0|ref=harv}}(New edition in 1992 by Crécy Publishing, ISBN 0-947554-26-2.)
* Millar, Peter. "The Other Prince". ''The Sunday Times'' (26 January 2003).
* Warwick, Christopher. ''George and Marina, Duke and Duchess of Kent''. London: Weidenfeld and Nicolson, 1988. ISBN 0-297-79453-1.

==External links==
{{commons category}}
*{{Hansard-contribs|mr-george-windsor-2|the Duke of Kent}}
*[http://www.npg.org.uk/live/search/person.asp?LinkID=mp05474 Portraits of Prince George from the National Portrait Gallery]

{{S-start}}
{{S-hou | [[House of Windsor]] |20 December|1902|25 August|1942|[[House of Wettin]]}}
{{S-npo|mason}}
{{Succession box | title=[[United Grand Lodge of England|Grand Master of the United<br />Grand Lodge of England]] | before=[[Prince Arthur, Duke of Connaught and Strathearn]] | after=[[Henry Lascelles, 6th Earl of Harewood]] | years=1939–1942}}
{{S-reg|uk}}
{{Succession box|title=[[Duke of Kent]]|before=''Office established''|after=[[Prince Edward, Duke of Kent|Prince Edward]]|years=1934–1942}}
{{S-end}}

{{British princes}}
{{Dukes of Kent}}
{{Princes of Saxe-Coburg and Gotha}}

{{Authority control}}

{{DEFAULTSORT:Kent, Prince George, Duke Of}}
[[Category:1902 births]]
[[Category:1942 deaths]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]
[[Category:Bailiffs Grand Cross of the Order of St John]]
[[Category:Bisexual royalty]]
[[Category:Burials at the Royal Burial Ground, Frogmore]]
[[Category:Civil servants in the Home Office]]
[[Category:Civil servants in the Foreign Office]]
[[Category:Dukes of Kent]]
[[Category:Grand Masters of the United Grand Lodge of England]]
[[Category:House of Windsor]]
[[Category:Knights Grand Cross of the Order of St Michael and St George]]
[[Category:Knights Grand Cross of the Royal Victorian Order]]
[[Category:Knights of the Garter]]
[[Category:Knights of the Thistle]]
[[Category:Lords High Commissioner to the General Assembly of the Church of Scotland]]
[[Category:People educated at St Peter's Court]]
[[Category:Princes of the United Kingdom]]
[[Category:Recipients of the Royal Victorian Chain]]
[[Category:Royal Air Force air marshals]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Royal Navy admirals]]
[[Category:Royal Navy personnel of World War II]]
[[Category:LGBT people from England]]
[[Category:Victims of aviation accidents or incidents in Scotland]]
[[Category:British Empire in World War II]]
[[Category:People from Sandringham, Norfolk]]
[[Category:Members of the Privy Council of the United Kingdom]]