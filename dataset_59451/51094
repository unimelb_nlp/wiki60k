{{No footnotes|date=August 2014}}
{{Infobox journal
| title         = Denver Quarterly
| cover         = 
| editor        = 
| discipline    = [[Literary journal]]
| peer-reviewed = 
| language      = English
| former_names  = The University of Denver Quarterly
| abbreviation  = 
| publisher     = [[University of Denver]]
| country       = [[United States]]
| frequency     = Quarterly
| history       = 1966-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.denverquarterly.com/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0011-8869
| eISSN         = 
| boxwidth      = 
}}

The '''''Denver Quarterly''''' (known as '''''The University of Denver Quarterly''''' until 1970) is a well-regarded, avant-garde literary journal based at the [[University of Denver]]. Founded in 1966 by novelist [[John Edward Williams]].

==''The Best American Short Stories''==
Stories from the journal have twice been included in ''[[The Best American Short Stories]]'': Margaret Shipley's "The Tea Bowl of Ninsel Nomura," in 1969, and in 1977 Baine Kerr's "Rider."  Victor Kolpacoff's "The Journey to Rutherford" received an Honorable Mention in the 1970 anthology, Walter Benesch received a similar notation for "The Double" in 1971, and John P. Fox got one for "Torchy and My Old Man" (also in 1971).

==''The Best American Essays''==
Three essays have had honorable mentions in ''[[The Best American Essays]]'': Gabriel Hudson's "The Sky Hermit" in 1986, [[Stanley Elkin]]'s "What's in a Name? Etc" in 1988, and [[Albert Goldbarth]]'s "Wind-up Sushi: With Catalogues and Instructions for Assembly" in 1990.

==''The Best American Poetry''==
*In ''[[The Best American Poetry 1990]]'' the poems "First Song/Bankei/1653/" by Stephen Berg, "Climbing Out of the Cage" by Virginia Hooper, and "Distance from Loved Ones" by [[James Tate (writer)|James Tate]]. 
*In ''[[The Best American Poetry 1992]]'' the poems "The Sudden Appearance of a Monster at a Window" by Lawrence Raab and "Lucifer in Starlight" by [[David St. John]].
*In ''[[The Best American Poetry 1997]]'' the poems "from 'A Summer Evening,' " by Geoffrey Nutter and "Helicopter Wrecked on a Hill" by Christine Hume.
*In ''[[The Best American Poetry 1998]]'' the poems "Past All Understanding" by [[Heather McHugh]] and "A Calm November. Sunday in the Fields," by [[Sidney Wade]]. 
*In ''[[The Best American Poetry 2000]]'' the poem "The Year," by Janet Bowdan.  
*In ''[[The Best American Poetry 2005]]'' the poem "In the Graveyard of Fallen Monuments," by [[Rachel Loden]].
*In ''[[The Best American Poetry 2007]]'' the poem "Dear Pearce & Pearce, Inc," by Danielle Pafunda.

==Other awards==
Stephen Berg, the founder of ''[[The American Poetry Review]]'', won the ''Denver Quarterly'' a [[Pushcart Prize]] for his poem "First Song/Bankei/1653/", which also was included in ''Best American Poetry 1990''.

In 1990, [[Joanne Greenberg]] won an [[O. Henry Award]] for her short story "Elizabeth Baird," originally published in the Fall 1989 issue of the journal.  [[Scott Bradfield]]'s essay "Why I hate Toni Morrison's 'Beloved'" was published to acclaim in 2004 (Vol 38:4).

==Notable contributors==
{{col-start}}
{{col-break}}
*[[Seth Abramson]]
*[[Jesse Ball]]
*[[Owen Barfield]]
{{col-break}}
*[[Charles Baxter (author)|Charles Baxter]]
*[[Joan Didion]]
*[[Russell Edson]]
*[[Raymond Federman]]
{{col-break}}
*[[Dana Gioia]]
*[[Brenda Hillman]]
*[[Es'kia Mphahlele]]
*[[Tim O'Brien (author)|Tim O'Brien]]
*[[Ricardo Pau-Llosa]] 
{{col-break}}
*[[Donald Revell]]
*[[Cole Swensen]]
*[[John Updike]] 
*[[Lee Upton]]
{{col-break}}
*[[Dara Wier]]
*[[Yvor Winters]]
*[[Jim Krusoe]]
{{col-end}}

==Editors==
The first editor was [[John Edward Williams]] (1965-1970).  Others have included Jim Clark, Leland Chambers (1977-1983), [[Donald Revell]] (1988-1994), [[Bin Ramke]] (1994-2011), and, currently, novelist [[Laird Hunt]] (2012-present).

==Notes and references==
{{reflist}}

http://libinfo.uark.edu/specialcollections/findingaids/williamsje.html<br />
http://www.philsp.com/homeville/anth/s101.htm<br />
https://books.google.com/books?id=1cZZAAAAMAAJ&q=%22denver+quarterly%22+best+american&dq=%22denver+quarterly%22+best+american&lr=&pgis=1<br />
http://www.du.edu/english/binramke.htm<br />
http://www.randomhouse.com/anchor/ohenry/winners/past.html<br />
http://www.bestamericanpoetry.com/archive/?id=19<br />
http://biography.jrank.org/pages/4268/Didion-Joan.html<br />
http://www.newpages.com/magazinestand/litmags/reviews_archive_2004/2004_08/default.htm<br />
http://eskiaonline.com/content/view/18/33/<br />
http://davidlavery.net/barfield/barfield_resources/Bibliographies/Bibliography.html<br />
http://www.ilab.org/db/detail.php?lang=de&membernr=2318&ordernr=007518<br />
http://www.cmmayo.com/mexico.translators.html<br />
https://books.google.com/books?id=GsdZAAAAMAAJ&q=%22denver+quarterly%22&dq=%22denver+quarterly%22&lr=&pgis=1

==External links==
* http://www.denverquarterly.com

[[Category:American literary magazines]]
[[Category:American quarterly magazines]]
[[Category:Magazines established in 1966]]
[[Category:Magazines published in Colorado]]
[[Category:Poetry literary magazines]]
[[Category:University of Denver]]
[[Category:Media in Denver]]