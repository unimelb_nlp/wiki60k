{{Infobox television episode
 |image_size = 280px
 |image = Game of Thrones-S03-E03 Jaime's hand is severed.jpg
 |caption = Jaime Lannister loses his hand.
 |title = Walk of Punishment
 |series = [[Game of Thrones]]
 |season = 3
 |episode = 3
 |director = [[David Benioff]]
 |writer = David Benioff<br />[[D. B. Weiss]]
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = Matthew Jensen
 |editor = Katie Weiland
 |production = 
 |airdate = {{Start date|2013|4|14}}
 |length = 53 minutes
 |guests =* [[Ciarán Hinds]] as Mance Rayder
* [[Robert Pugh]] as Craster
* [[Ian McElhinney]] as Barristan Selmy
* [[Gwendoline Christie]] as Brienne of Tarth
* [[Mackenzie Crook]] as Orell
* [[Paul Kaye]] as Thoros of Myr
* [[Clive Russell]] as Brynden "Blackfish" Tully
* [[Tobias Menzies]] as Edmure Tully
* [[Kristofer Hivju]] as Tormund Giantsbane
* [[Julian Glover]] as Grand Maester Pycelle
* [[Iwan Rheon]] as Boy
* [[Noah Taylor]] as Locke
* [[Mark Stanley]] as Grenn
* [[Ben Crompton]] as Edd Tollett
* [[Luke Barnes]] as Rast
* [[Burn Gorman]] as Karl
* [[Hannah Murray]] as Gilly
* [[Ben Hawkey]] as Hot Pie
* [[Esmé Bianco]] as Ros
* [[Daniel Portman]] as Podrick Payne
* [[Philip McGinley]] as Anguy
* [[Dan Hildebrand]] as Kraznys mo Nakloz
* [[Nathalie Emmanuel]] as Missandei
* Michael Shelford as Torturer
* Clifford Barry as Greizhen mo Ullhor
* [[Gary Lightbody]] as a Bolton soldier
* Kylie Harris as Genna
* [[Josephine Gillan]] as Marei
* Pixie Le Knot as Kayla
* [[Dean-Charles Chapman]] as Martyn Lannister
 |season_list =
 |prev = [[Dark Wings, Dark Words]]
 |next = [[And Now His Watch Is Ended]]
 |episode_list = [[Game of Thrones (season 3)|''Game of Thrones'' (season 3)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}

"'''Walk of Punishment'''" is the third episode of the [[Game of Thrones (season 3)|third season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 23rd episode of the series. Written by [[David Benioff]] and [[D. B. Weiss]], and directed by Benioff, it aired on April 14, 2013.

The title of the episode alludes to a place called "The Walk of Punishment" in the series, a road where slaves are crucified and displayed as examples to the slaves who thought of disobeying their masters.

The episode received a nomination for a [[Primetime Emmy Award for Outstanding Costumes for a Series]] at the [[65th Primetime Creative Arts Emmy Awards]].

==Plot==

===In King's Landing===
Tywin Lannister ([[Charles Dance]]) calls a meeting of the Small Council. He demands knowledge of Jaime's location, which the spymaster, Lord Varys ([[Conleth Hill]]), is unable to provide. Tywin then announces plans to have Lord Petyr Baelish ([[Aidan Gillen]]) wed Lysa Arryn to deprive Robb Stark of more allies in the war. Tywin names his son Tyrion ([[Peter Dinklage]]) as Baelish's replacement as Master of Coin (treasurer), despite Tyrion's pointing out his lack of expertise. Later, while retrieving the ledgers for his new appointment, Lord Baelish advises Tyrion on the job. Tyrion then rewards his squire Podrick Payne ([[Daniel Portman]]) with the services of three prostitutes for saving his life in the Battle of the Blackwater; Tyrion is later surprised to discover that all three prostitutes refuse payment due to Podrick's skill in lovemaking. While going over the books, Tyrion discovers that as treasurer Baelish has borrowed millions in gold from Tywin, as well as tens of millions from the Iron Bank of Braavos. A debt to Tywin is a problem since, regardless of his familial relationship with the king, Tyrion fears he will not forgive the debt despite the crown's dire straits. However, he is more worried about the Iron Bank, since if they are not paid back in due time they may very well begin funding the Crown's enemies.

===In the North===
Theon Greyjoy ([[Alfie Allen]]) is awakened by the cleaning boy ([[Iwan Rheon]]) and released from his bondage, given a horse, and told to ride east to his sister, Yara, at Deepwood Motte. After riding all night, Theon finds out that he is being pursued by his captors; he is soon caught and knocked off his horse. Moments before their leader attempts to rape him, he is saved by the cleaning boy, who kills the soldiers and aids him once again.

===At Dragonstone===
Stannis Baratheon ([[Stephen Dillane]]) accuses Melisandre ([[Carice van Houten]]) of trying to abandon him as she is heading to a ship bound for an unknown destination which she claims will be revealed to her by the Lord of Light. Stannis begs her to give him another son, but she says he does not have the strength. She informs him that her magic requires king's blood and that it will be necessary to acquire it from others who share Stannis' blood.

===In Astapor===
Ser Jorah Mormont ([[Iain Glen]]) and Ser Barristan Selmy ([[Ian McElhinney]]) offer Daenerys Targaryen ([[Emilia Clarke]]) their differing counsel over the choice of soldiers to use upon their return to Westeros. Ser Barristan would have them use free, loyal men, while Ser Jorah presses in favor of the Unsullied. Shortly afterwards, Daenerys declares to slaver Kraznys mo Nakloz ([[Dan Hildebrand]]) that she wishes to purchase all 8,000 Unsullied and the boys still in training to become Unsullied. When Kraznys tells her she cannot afford them (and insulting her in Old Valyrian at the same time), Daenerys offers one of her dragons, despite the counsel of both Jorah and Barristan. Kraznys insists on the largest one, to which Daenerys agrees. Before leaving, Daenerys demands Kraznys' slave translator Missandei ([[Nathalie Emmanuel]]) as a gift, and she departs with her.

===Beyond the Wall===
The wildling army, led by Mance Rayder ([[Ciarán Hinds]]) arrives at the Fist of the First Men to find a field of decapitated horses arranged in shape of a whirl by the White Walkers. Jon Snow ([[Kit Harington]]) notes that there are no dead Night's Watch brothers amongst the horses, which Orell ([[Mackenzie Crook]]) claims he had seen. Rayder tells Jon that the dead have become wights, and orders Tormund Giantsbane ([[Kristofer Hivju]]) to take 20 men, along with Jon, and climb the Wall. Rayder then says he will light the biggest fire the North has seen to signal them to attack the Night's Watch. He stipulates that if Snow doesn't prove useful, he can be thrown off the Wall.

Arriving at Craster's Keep, the remaining Night's Watch brothers seek refuge under his roof. Threatened by the desperate brothers, Craster ([[Robert Pugh]]) reluctantly allows them into his keep. After being ridiculed by Craster, Samwell Tarly ([[John Bradley-West|John Bradley]]) leaves the keep and witnesses Gilly ([[Hannah Murray]]) giving birth to a boy.

===At Riverrun===
[[File:Game of Thrones Oslo exhibition 2014 - Jaime Lannister's hand.jpg|thumb|upright|Props from the episode: Jaime Lannister's severed hand, his shackles and Locke's cleaver.]]
Lord Hoster Tully is given a [[ship burial]], attended by his family and bannermen. When his son, Edmure ([[Tobias Menzies]]), is unable to set the boat ablaze despite three arrow shots, Hoster's brother, Brynden "the Blackfish" ([[Clive Russell]]) shoves Edmure out of the way and with one arrow sets the boat on fire, shaming Edmure in front of the group. Shortly after, while in conference with King Robb ([[Richard Madden]]), Edmure is chastised for having disobeyed orders not to engage Ser Gregor Clegane. Later, Lady Catelyn Stark ([[Michelle Fairley]]) discusses her pain with Brynden. Queen Talisa ([[Oona Chaplin]]) pays a visit to Tywin's captured nephews, Martyn and Willem Lannister, and tends to their wounds.

===In the Riverlands===
Arya Stark ([[Maisie Williams]]) confronts Sandor "the Hound" Clegane ([[Rory McCann]]) about [[The Kingsroad|killing Arya's friend Mycah]] while traveling from Winterfell, but before the Hound can reply, he is taken away in a cart. Arya and Gendry ([[Joe Dempsie]]) then say goodbye to Hot Pie (Ben Hawkey), who tells them that the Brotherhood has left him at the inn as payment to the innkeeper after proving his skill as a cook.

Locke ([[Noah Taylor]]) and his men transport Ser Jaime Lannister ([[Nikolaj Coster-Waldau]]) and Brienne of Tarth ([[Gwendoline Christie]]) to Harrenhal. Along the way, Brienne and Jaime bicker about their capture, and Brienne chides Jaime for his poor swordplay. Jaime anticipates that the men will try to rape Brienne and suggests she submit to their advances or face being killed, but concedes that if he were in Brienne's position he would make the men kill him. At camp for the night, when Locke's men attempt to rape Brienne, Jaime convinces Locke that Brienne's father is rich and would pay ransom to have her back chaste and unharmed. Locke believes Jaime's story and stops the men from raping Brienne. However, Jaime then tries to secure his own release, promising Locke that his father Tywin will reward him with gold and titles if he is returned to him. Locke feigns acceptance but is actually offended by Jaime's offer, and sneers that Jaime is powerless without his father. To drive home the point, he severs Jaime's right hand.

==Production==
[[File:D. B. Weiss and David Benioff.jpg|thumb|250px|"Walk of Punishment" was written and directed by producers [[D.B. Weiss]] and [[David Benioff]].]]

===Writing===
"Walk of Punishment" was written by the show creators and executive producers David Benioff and D. B. Weiss, based on material from George R. R. Martin's novel ''[[A Storm of Swords]]''. The episode adapts parts of chapters to 15, 16, 18, 20, 22, 24, 28, 32, 34 and 36 of the book (Catelyn II, Jon II, Arya III, Tyrion III, Jaime III, Daenerys II, Daenerys III, Jaime IV, Samwell II and Catelyn IV). The writers have also included original storylines including Theon's flight, Tyrion bringing Podrick to a brothel, and Melisandre departing Dragonstone.<ref name="westeros">{{Cite web|url=http://www.westeros.org/GoT/Episodes/Entry/Walk_of_Punishment/Book_Spoilers/|title=EP303: Walk of punishment|work=westeros.org|first=Elio|last=Garcia|accessdate=April 15, 2013}}</ref>

In the scene at the brothel, Tyrion claims that the last prostitute he introduces is one of the few women in the world able to perform "the Meereenese Knot". This is an inside joke referring to the name that Martin gave (after the legendary [[Gordian Knot]]) to a complicated structural problem that he had to face while writing the fifth book of the series, ''[[A Dance with Dragons]]''. This book had to synchronize the arrival of several characters in the city of Meereen while keeping the chronology and causations in line and informing the reader of events happening in places where no [[point of view (literature)|point-of-view character]] was present. Martin worked on solving "the Meereenese Knot" from 2005 to 2011, and it was one of the main causes behind the late delivery of the book.<ref>{{Cite web|url=http://grrm.livejournal.com/92848.html|title=Guarded Optimism|work=Not a blog|first=George R.R.|last=Martin|accessdate=April 15, 2013}}</ref><ref>{{Cite web|url=http://grrm.livejournal.com/217066.html|title=Talking About the Dance|work=Not a blog|first=George R.R.|last=Martin|accessdate=April 15, 2013}}</ref>

===Directing===
The episode was directed by the writing team itself, although to comply with the rules of the [[Directors Guild of America]] only Benioff is credited for directing. For both Benioff and Weiss, it was their first direction experience though the former had previously directed an experimental short film "When the Nines Roll Over".<ref>{{Cite web|url=http://www.accesshollywood.com/game-of-thrones-producers-david-benioff-and-db-weiss-to-direct-during-season-3_article_66580?__source=rss|title=Game Of Thrones: Producers David Benioff & D.B. Weiss To Direct During Season 3 |work=[[Access Hollywood]]|first=Jolie|last=Lash|accessdate=April 15, 2013}}</ref>

===Casting===
"Walk of Punishment" introduces the Tully family at Riverrun, marking the first appearances of Lady Catelyn's uncle Brynden Tully, played by [[Clive Russell]], and her brother Edmure, played by [[Tobias Menzies]]. Edmure Tully is depicted in the show more harshly than in the books.<ref name="westeros"/> Talking about his character, Menzies described him as "as comic as ''Game of Thrones'' gets ... He's a little flawed, really."
[[Dean-Charles Chapman]] first appears in the role of Martyn Lannister in this episode. In Season 4, however, Chapman returns portraying a different character: Tommen Baratheon, which was played by [[Callum Wharry]] in previous Seasons.
<ref>{{Cite web|url=http://www.accesshollywood.com/game-of-thrones-access-countdown-to-season-3-qanda-tobias-menzies-talks-edmure-tully_article_76562|title=Game Of Thrones: Access Countdown To Season 3 Q&A — Tobias Menzies Talks Edmure Tully|work=[[Access Hollywood]]|first=Jolie|last=Lash|accessdate=April 15, 2013}}</ref>

===Filming locations===
[[File:Down (042), October 2009.JPG|thumb|The [[river Quoile]] was used to depict the surroundings of the Riverrun castle.]]
The interiors of the episode were filmed at the [[Paint Hall Studios]] in [[Belfast]], the show's base of operations. For the exterior shots the production used many other locations across Northern Ireland: the Redhall State ([[County Antrim]]) for the Inn at the Crossroads, the [[Clandeboye Estate]] ([[County Down]]) for Craster's Keep, Downhill Strand ([[County Londonderry]]) as the coast of Dragonstone, and the [[River Quoile]] (County Down) as the setting of Lord Hoster Tully's funeral.<ref>{{cite web|url=http://winteriscoming.net/2012/07/filming-continues-in-ni-peep-these-new-pics-from-the-set/|title=Day 23: Filming continues in NI, peep these new pics from the set|work=WinterIsComing.net|accessdate=April 15, 2012}}</ref>

The storylines led by Jon Snow and Daenerys Targaryen continued to be filmed in Iceland and in the Moroccan city of [[Essaouira]] respectively.

===Music===
{{main|Music of Game of Thrones}}
The band of Locke's men sing "[[The Bear and the Maiden Fair (song)|The Bear and the Maiden Fair]]", heard for the first time in the series with music composed by [[Ramin Djawadi]]. The song, a very popular song in Westeros both among the commoners and the nobility, appears often in the original novels. Singing at the head of the group is [[Snow Patrol]]'s frontman [[Gary Lightbody]], in a cameo appearance.<ref name="SFX">{{cite web|url=http://www.sfx.co.uk/2013/04/16/game-of-thrones-3-03-walk-of-punishment-review/|title=Game Of Thrones 3.03 "Walk Of Punishment" REVIEW|last=Power|first=Rob|publisher=[[SFX (magazine)|SFX]]|date=April 16, 2013|accessdate=April 16, 2013}}</ref>

The closing credits reprise the song in a new version recorded specifically for the series by the indie band [[The Hold Steady]]. The group, one of Benioff and Weiss's favourite bands, was chosen because they wanted the rendition "to be bawdy and a little sloppy – drunken musicians getting up on the table and jamming while the rowdy party continues around them".

The decision to place the song at the end of the episode, right after the amputation of Jaime's hand, was made to reinforce the surprise of the viewers: “It’s such a shocking ending and when we read the scene in the books it was so shocking to us. To really hammer home the shock of that moment you need something unexpected. There’s no version of a traditional score that would keep you as off balance as we wanted that scene to leaving you feeling.”<ref>{{cite web|url=http://insidetv.ew.com/2013/03/05/game-of-thrones-hold-steady/|title='Game of Thrones' and the Hold Steady team for sesson 3 song -- EXCLUSIVE|last=Hibberd|first=James|publisher=EW.com|date=March 5, 2013|accessdate=April 15, 2013}}</ref><ref>{{cite web|url=http://insidetv.ew.com/2013/04/14/thrones-jaime-hand/|title='Game of Thrones': Nikolaj Coster-Waldau on Jaime's big surprise|last=Hibberd|first=James|publisher=EW.com|date=April 14, 2013|accessdate=April 16, 2013}}</ref><ref>{{cite web|url=http://www.rollingstone.com/movies/news/the-hold-steady-record-song-for-game-of-thrones-20130305|title=The Hold Steady Record Song for 'Game of Thrones'|publisher=Rolling Stone|date=March 5, 2013|accessdate=April 16, 2013}}</ref>

==Reception==

===Ratings===
"Walk of Punishment"'s first airing was seen by 4.7 million viewers, setting a new viewership record  for the show. Taking into account the viewers of the later repeat the figures rose to 5.8 million.<ref>{{cite web|url=http://insidetv.ew.com/2013/04/16/game-of-thrones-ratings-hit-series-high-third-episode/|title='Game of Thrones' sets ratings record for latest episode|last=Hibberd|first=James|publisher=[[Entertainment Weekly]]|date=April 16, 2013|accessdate=April 16, 2013}}</ref> In the United Kingdom, the episode was seen by 1.173 million viewers on [[Sky Atlantic]], being the channel's highest-rated broadcast that week.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (1 - 7 April 2013)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref> In the United Kingdom, the episode was seen by 1.093 million viewers on [[Sky Atlantic]], being the channel's highest-rated broadcast that week.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (15 - 21 April 2013)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref>

===Critical reception===
The episode was praised by critics; [[review aggregator]] [[Rotten Tomatoes]] surveyed 21 reviews of the episode and judged 95% of them to be positive. The website's critical consensus reads, "A bit of well-placed levity perfectly compliments the shocking final scenes of 'Walk of Punishment', adding up to hands down the most thrilling episode of the season so far—minus one hand."<ref>{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s03/e03/|title=Walk of Punishment|work=[[Rotten Tomatoes]]|accessdate=May 3, 2016}}</ref> Matt Fowler, writing for [[IGN]], rated the episode 8.8/10, writing "A shocking chop and a rollicking rock song led us out of a strong Thrones episode."<ref name="ign">{{cite web|url=http://ca.ign.com/articles/2013/04/15/game-of-thrones-walk-of-punishment-review|title=Game of Thrones "Walk of Punishment" Review|last=Fowler|first=Matt|publisher=[[IGN]]|date=April 14, 2013|accessdate=April 15, 2013}}</ref> Writing for the [[The A.V. Club|A.V. Club]], David Sims rated the episode an A-.<ref name="av1">{{cite web|url=http://www.avclub.com/articles/walk-of-punishment-for-newbies,95986/|title="Walk of Punishment" (for newbies)|last=Sims|first=David|publisher=[[The A.V. Club]]|date=April 14, 2013|accessdate=April 15, 2013}}</ref> Also at The A.V. Club Todd VanDerWerff gave the episode another A-, praising its quickening of narrative pace.<ref name="av2">{{cite web|url=http://www.avclub.com/articles/walk-of-punishment-for-experts,95985/|title="Walk of Punishment" (for experts)|last=VanDerWerff|first=Todd|publisher=[[The A.V. Club]]|date=April 14, 2013|accessdate=April 15, 2013}}</ref> [[Time (magazine)|Time]] magazine reviewer James Poniewozik praised the episode, writing "...as a fan of fantasy fiction since I was a kid–is that it has a level of ugly realism missing from much of the genre."<ref>{{cite web|url=http://entertainment.time.com/2013/04/15/game-of-thrones-watch-thats-what-the-moneys-for/|title=Game of Thrones Watch: That's What the Money's For!|last=Poniewozik|first=James|publisher=[[Time (magazine)|Time]]|date=April 15, 2013|accessdate=April 15, 2013}}</ref>

== References ==
{{Reflist|2}}

== External links ==
{{wikiquotepar|Game_of_Thrones_(TV_series)#Walk_of_Punishment_.5B3.03.5D|Walk of Punishment}}
* [http://www.hbo.com/game-of-thrones/episodes/3/23-walk-of-punishment/index.html "Walk of Punishment"] at [[HBO.com]]
* {{IMDb episode|2178802}}
* {{tv.com episode|game-of-thrones/walk-of-punishment-2673907}}

{{Game of Thrones Episodes}}

[[Category:2013 American television episodes]]
[[Category:Game of Thrones episodes]]