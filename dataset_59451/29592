{{featured article}}
{{Use dmy dates|date=August 2016}}
{{Use British English|date=August 2016}}

'''Lazarus Leonard Aaronson''' {{post-nominals|MBE}} (18 February 1895&nbsp;– 9 December 1966), often published as '''L. Aaronson''', was a British poet and a lecturer in [[economics]]. As a young man, he belonged to a group of Jewish friends who are today known as the [[Whitechapel Boys]], many of whom later achieved fame as writers and artists. Though less radical in his use of language, he has been compared to his more renowned Whitechapel friend, [[Isaac Rosenberg]], in terms of [[diction]] and verbal energy. Aaronson's poetry is characterised as more [[Georgian Poetry|post-Georgian]] than [[Literary modernism|modernistic]], and reviewers have traced influences from both the English poet [[John Keats]], and Hebrew poets such as [[Shaul Tchernichovsky]] and [[Zalman Shneur]].

Aaronson lived most of his life in London and spent much of his working life as a [[lecturer]] in economics at the [[City of London College]].  In his twenties, he converted to Christianity and a large part of his poetry focused on his conversion and spiritual identity as a Jew and an [[English people|Englishman]]. In total, he published three collections of poetry: ''Christ in the Synagogue'' (1930), ''Poems'' (1933), and ''The Homeward Journey and Other Poems'' (1946). Although he did not achieve widespread recognition, Aaronson gained a [[cult following]] of dedicated readers. Upon retiring from teaching, he moved to [[Harpenden, Hertfordshire|Harpenden]], [[Hertfordshire]], where he died from heart failure and [[coronary heart disease]] on 9 December 1966. His poetry was not widely publicised, and he left many unpublished poems at his death.

==Life==
Aaronson was born on 18 February 1895{{refn|group=note|''The Palgrave Dictionary of Anglo-Jewish History'', and some other sources, lists Aaronson's date of birth as 24 December 1894, while the later and more comprehensive entry in the ''Oxford Dictionary of National Biography'' gives it as 18 February 1895.}} at 34 Great Pearl Street, [[Spitalfields]] in the [[East End of London]] to poor [[Orthodox Jewish]] parents who had immigrated from [[Vilna Governorate|Vilna]] in the [[Pale of Settlement]] in Eastern Europe.<ref name="ONDB">{{cite encyclopedia |last=Baker |first=William |title=Aaronson, Lazarus (1895–1966)|url=http://www.oxforddnb.com/view/article/105000 |encyclopedia=Oxford Dictionary of National Biography |publisher=Oxford University Press |date=May 2015 |accessdate=4 June 2015 |doi=10.1093/ref:odnb/105000 |format={{ODNBsub}}|isbn=978-0-19-861411-1 }}</ref><ref name="Robson">{{cite journal |last=Robson |first=Jeremy |year=1966 |title=A Survey of Anglo-Jewish Poetry |journal=[[Jewish Quarterly]]  |volume=14 |issue=2 |page=5–23 |doi=10.1080/0449010X.1966.10706502|url=http://www.tandfonline.com/doi/abs/10.1080/0449010X.1966.10706502|doi-broken-date=2017-01-31 }}</ref> His father was Louis Aaronson, a bootmaker, and his mother was Sarah Aaronson, ''née'' Kowalski. He attended Whitechapel City Boys' School and later received a scholarship to attend [[Hackney Downs School|Hackney Downs grammar school]].<ref name="ONDB"/>

His father emigrated to New York in 1905. The rest of the family followed in 1912, except for 17-year old Lazarus who remained in London. From then on, he lived with the family of Joseph Posener at 292 Commercial Road in the [[East End of London]]. At the time, the area was a hub of the Jewish [[diaspora]] and at the turn of the 20th century, a quarter of its population were Jews from Central and Eastern Europe. Growing up in the East End, Aaronson was part of a group of friends who are today referred to as the [[Whitechapel Boys]], all of whom were children of Jewish immigrants and shared literary and artistic ambitions.<ref>{{cite journal |last1=Dickson |first1= Rachel |last2=MacDougall |first2=Sarah |year=2004 |title=The Whitechapel Boys |journal=[[Jewish Quarterly]] |volume=51 |issue=3 |pages=29–34|url=http://www.tandfonline.com/doi/abs/10.1080/0449010X.2004.10706848  |doi=10.1080/0449010X.2004.10706848  |doi-broken-date= 2017-01-31 }}</ref> Others in the group who, like Aaronson, later achieved distinction included [[John Rodker]], [[Isaac Rosenberg]], [[Joseph Leftwich]], [[Samuel Winsten]], [[Clara Birnberg]], [[David Bomberg]], and the brothers [[Abraham Fineberg|Abraham]] and [[Joseph Fineberg]].<ref>{{cite book |last=Patterson |first=Ian |editor-last1=Gasiorek |editor-first1=Andrzej |editor-last2=Reeve-Tucker |editor-first2= Alice |editor-last3=Waddell |editor-first3=Nathan |year=2013 |title=Wyndham Lewis and the Cultures of Modernity |chapter=John Rodker, Julius Ratner and Wyndham Lewis: The Split-Man Writes Back |publisher=Ashgate |isbn= 978-1-4094-7901-7 |page=97}}</ref> Aaronson was also involved in the [[Young Socialist League]], where he and other Whitechapel Boys helped organise educational meetings on modern art and radical politics.<ref>{{cite book |last=Patterson |first=Ian |editor-last1=Beasley |editor-first1=Rebecca |editor-last2=Bullock |editor-first2=Philip Ross |year=2013 |title=Russia in Britain, 1880–1940: From Melodrama to Modernism |chapter=The Translation of Soviet Literature |publisher=Oxford University Press |isbn=978-0-19-966086-5 |page=189}}</ref> Aaronson remained a committed [[Socialism|socialist]] throughout adulthood.<ref>{{cite book |last=Zilboorg |first=Caroline |year=2001 |title=The Masks of Mary Renault: A Literary Biography |publisher=University of Missouri Press |isbn=0-8262-1322-7 |pages=67–70, 87}}</ref>

{{Quote box
 |title = The Stake
 |bgcolor   = #c6dbf7
 |quote  =<poem>
All that I am is staked on words.
Bless their meaning, Lord, or I become
Slave to the heavy, hollow, mindless drum.

Make me the maker of my words.
Let me renew myself in my own speech,
Till I become at last the thing I teach.

And let a taste be in my words,
That men may savour what is man in me,
And know how much I fail, how little see.

Let not my pleasure in my words
Forget the silence whence all speech has sprung,
The cell and meditation of the tongue.

And at the end, the Word of words,
Lord! make my dedication. Let me live
Towards Your patient love that can forgive

The blasphemy and pride of words
Since once You spoke. Your praise is there.
I mean it thus, even in my despair.
</poem>
 |source = —''The Homeward Journey and Other Poems'', 1946
 |width  = 30 %
 |align  = right
}}
Having been diagnosed with [[tuberculosis]] and [[diabetes]], Aaronson did not serve in the military during the [[First World War]]. Between 1913 and 1915, and again between 1926 and 1928, he studied economics at the [[London School of Economics]], but never completed his degree.<ref name="ONDB"/>

Aaronson was married three times. His first wife was the actress [[Lydia Sherwood]] (1906–1989), whom he was married to between 1924 and 1931.<ref name="Rubinstein">{{cite encyclopedia |last= |first= |author-link= |editor-first=William D. |editor-last=Rubinstein |editor-link=William Rubinstein |encyclopedia=The Palgrave Dictionary of Anglo-Jewish History |title= Aaronson, Lazarus Leonard |edition= |year=2011 |publisher=Palgrave Macmillan |volume= |isbn=978-1-4039-3910-4 |pages=2}}</ref> He filed for divorce on grounds of her adultery with the theatre producer [[Theodore Komisarjevsky]], and the much publicised suit was undefended.<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=[In the Divorce Court, London, yesterday...] |url=https://news.google.com/newspapers?id=5_NYAAAAIBAJ&sjid=YaUMAAAAIBAJ&pg=5398%2C8485573 |newspaper=[[The Glasgow Herald]] |date=30 October 1931 |page=11 }}</ref><ref>{{cite news |last=Trilling |first=Ossia |title=Lydia Sherwood [obituary] |newspaper=[[The Independent]] |date=28 April 1989 |page=35 }}</ref> His second marriage, which took place on 9 July 1938, to Dorothy Beatrice Lewer (1915–2005), also ended in divorce. On 14 January 1950, Aaronson married Margaret Olive Ireson (1920–1981), with whom he had one son, David, who was born in 1953.<ref name="ONDB"/>{{refn|group=note|Aaronson's first wife was born as Lily Shavelson, but took the name Lydia Sherwood. The second wife Dorothy Beatrice Lewer subsequently married the geriatrician Oscar Olbrich. The third wife was born as Margaret Olive Axford, but had previously been married to the French scholar John Clifford Ireson.<ref name="ONDB"/>}}

To friends and family, Lazarus Aaronson was known as Laz.<ref name="ONDB"/> He was friends with the novelist [[Stephen Hudson]], the sculptor [[Jacob Epstein]], the media mogul [[Sidney Bernstein, Baron Bernstein|Sidney Bernstein]], the artists [[Mark Gertler (artist)|Mark Gertler]] and [[Matthew Smith (artist)|Matthew Smith]] and the poets [[Harold Monro]] and [[Samuel Beckett]].<ref name="ONDB"/><ref>{{cite journal |last1=Knowlson |first1=James |year=1993 |title=Letter: Friends of Beckett |journal=[[Jewish Quarterly]] |volume=40 |issue=1 |page=72 |doi=10.1080/0449010X.1993.10705916 }}</ref> Aaronson was also close to the economist [[David Graham Hutton|Graham Hutton]], who in 1952 made a radio programme about him for the BBC.<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Portrait Sketch: Laz Aaronson by Graham Hutton |url=http://genome.ch.bbc.co.uk/98bb988e06ec49228794d2c9d51bc9de |newspaper=[[Radio Times]] |location= |date=25 January 1952 |page=16 |issue=1472}}</ref>

Around 1934, he began working as a lecturer in economics at the [[City of London College]]. Upon his retirement from the university in 1958, Aaronson was made a Member of the [[Order of the British Empire]] in the [[1959 New Year Honours]], in recognition of more than twenty-five years of service.<ref name="ONDB"/><ref>{{London Gazette |issue=41589 |date=30 December 1958 |startpage=15 |supp=yes}}</ref> The same year he moved with his family from London to [[Harpenden]] in Hertfordshire, where he later died from [[Coronary artery disease|coronary heart disease]] and heart failure on 9 December 1966, at the age of 71. He was buried in the Westfield Road Cemetery in Harpenden. His wife and young son survived him.<ref name="ONDB"/>

==Poetry==
Aaronson had literary ambitions from an early age, and by 1914 he contributed his first work for the radical literary magazine ''[[The New Age]]'' .<ref name="ONDB"/> He was often published under the name L. Aaronson.<ref>{{cite web |url=http://www.reading.ac.uk/web/FILES/library/LocationRegisterNameAuthorityListnew.pdf |title=The Names of Modern British and Irish Literature  |work=Name Authority List of the Location Register of English Literary Manuscripts and Letters |pages=1 |last1=Sutton |first1=David |author-link=David Sutton (archivist) |date=January 2015 |format=PDF |publisher=University of Reading |accessdate=26 January 2015}}</ref> In the 1920s, he [[Religious conversion|converted]] to Christianity. His first collection of poems, ''Christ in the Synagogue'', published by [[Victor Gollancz Ltd|V. Gollancz]] in 1930, dealt to a large extent with his conversion and spiritual identity as both a Jew and an Englishman. This subject would become a recurring theme in his numerous mystical poems.<ref name="Rubinstein"/><ref name="EJ"/> ''Christ in the Synagogue'' reached only a small audience and received fewer than a dozen reviews, but ''[[The Guardian|The Manchester Guardian]]'', ''[[The Nation and Athenaeum]]'', ''[[The Times Literary Supplement]]'', and ''The New Age'' wrote favourably of it.<ref>Extracts of the reviews are reprinted in {{cite book|first=L. |last=Aaronson|year=1933|title=Poems|publisher=V. Gollancz}}</ref>

Notwithstanding Aaronson's small readership, V. Gollancz published a second verse collection in 1933, titled ''Poems''. Despite being little known to the general public, Aaronson gained a [[cult following]] of dedicated readers.<ref name="Rubinstein"/> His third collection, ''The Homeward Journey and Other Poems'', was published by Christophers in 1946.<ref name="EJ">{{cite encyclopedia |editor-first=Fred |editor-last=Skolnik |editor-link=Fred Skolnik |encyclopedia=[[Encyclopaedia Judaica]] |title=Aaronson, Lazarus Leonard |edition=second |year=2007 |publisher=Gale Thomson |volume=Volume 1 |isbn=978-0-02-865928-2 |pages=224}}</ref> Some of his works also appeared in journals and anthologies such as the 1953 ''[[Faber Book of Twentieth Century Verse]]''.<ref name="ONDB"/>

Since Aaronson's poetry does not display formal innovation, literature professor William Baker, characterises him as "A [[Georgian Poetry|post-Georgian]] rather than a [[Modernist poetry in English |modernist]] [poet]".<ref name="ONDB"/> Baker further notes that Aaronson's poetry deals with several issues of his time, such as the rise of [[fascism]] and the [[World War II|Second World War]], but points out that Aaronson did not directly write about [[the Holocaust]].<ref name="ONDB"/> Upon Aaronson's death, the poet [[Arthur Chaim Jacobs]] compared him with [[Isaac Rosenberg]], the more celebrated poet of the same [[British Jews|Anglo-Jewish]] generation. According to Jacobs, Aaronson was "clearly influenced by him in terms of diction, and in a kind of verbal energy which runs through a lot of his poetry. But he was less radical than Rosenberg in his use of language, and tended towards [[John Keats|Keatsian]] luxuriance and sweetness."<ref name="Jacobs">{{cite journal |last=Jacobs |first=Arthur Chaim |title=Lazarus Aaronson: Two Unpublished Poems |year=1967  |url=http://www.tandfonline.com/doi/abs/10.1080/0449010X.1967.10703091  |journal=[[Jewish Quarterly]]  |volume=15 |issue=1–2 |page=13 |doi=10.1080/0449010X.1967.10703091 |doi-broken-date=2017-01-31 }}</ref> Although much of Aaronson's writings centred on his conversion to Christianity, Jacobs traces a continuing Hebraic mood in his poetry, and wrote that "his Christianity was hardly familiarly Anglican, and there is in his work an avowed sensuality which could in some ways be compared to that of modern Hebrew poets like [[Shaul Tchernichovsky|Tchernikowsky]] or [[Zalman Shneur|Shneur]], or later, [[Avraham Shlonsky]]."<ref name="Jacobs"/> According to his friend [[Joseph Leftwich]], Aronson himself in old age acknowledged influences from both traditional [[Judaism]] and [[Martin Buber]]'s unorthodox interpretation of [[Hasidism]].<ref>{{cite journal |last= Leftwich |first=Joseph  |authorlink=Joseph Leftwich |title=Hasidic Influences in Imaginative English Literature |year=1959  |url=http://jba.cjh.org/volumes/17/HTML/files/assets/basic-html/page9.html  |journal=Jewish Book Annual |publisher=[[Jewish Book Council]] |volume=17 |page=3 |doi=}}</ref>

Aaronson's poetry was not widely publicised, and he left over a thousand unpublished poems at his death.<ref name="ONDB"/> Little scholarly attention has been paid to his life and poetry. Upon Aaronson's death Jacobs stated that "Further assessment of his work awaits more substantial publication";<ref name="Jacobs"/> about 40 years later, Baker, who has written most extensively on Aaronson, named him among the Whitechapel intellectual writers and artists "today consigned to oblivion".<ref>{{cite journal|title=Reviewed Work: ''Whitechapel at War: Isaac Rosenberg and his Circle'' by Sarah MacDougall, Rachel Dickinson|first=William|last=Baker|journal=[[The Modern Language Review]]|volume=105|issue=3|date=July 2010|page=854}}</ref>

==Bibliography==
*''Christ in the Synagogue''. London: V. Gollancz, 1930
*''Poems''. London: V. Gollancz, 1933
*''The Homeward Journey and Other Poems''. London: Christophers, 1946

==Notes==
{{reflist|group=note}}
 
==References==
{{reflist}}

==Further reading==
{{refbegin}}
* Baker, William; Roberts Shumaker, Jeanette (2017). "Pioneers: E. O. Deutsch, B. L. Farjeon, Israel Gollancz, Leonard Merrick, and Lazarus Aaronson". ''The Literature of the Fox: A Reference and Critical Guide to Jewish Writing in the UK''. AMS Studies in Modern Literature. AMS Press. ISBN 978-0-404-65531-0.
{{refend}}

{{Authority control}}

{{DEFAULTSORT:Aaronson, Lazarus Leonard}}
[[Category:1895 births]]
[[Category:1966 deaths]]
[[Category:20th-century English poets]]
[[Category:English economists]]
[[Category:English people of Jewish descent]]
[[Category:Modernist writers]]
[[Category:People educated at Hackney Downs School]]
[[Category:People from Spitalfields]]
[[Category:Members of the Order of the British Empire]]
[[Category:English educators]]
[[Category:English male poets]]
[[Category:Whitechapel Boys]]