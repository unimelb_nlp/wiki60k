{{Infobox journal
| cover         = 
| editor        = Diane I. Doser
| discipline    = [[Seismology]]
| peer-reviewed = 
| language      = 
| former_names  = 
| abbreviation  = Bull. Seismol. Soc. Am.
| publisher     = [[Seismological Society of America]]
| country       = USA
| frequency     = Bimonthly 
| history       = 1911-present
| openaccess    = 
| license       = 
| impact        = 1.70
| impact-year   = 2012
| website       = http://www.seismosoc.org/publications/bssa/
| link1         = http://bssa.geoscienceworld.org/content/by/year
| link1-name    = Archive (1911-present)
| link2         = http://bssa.geoscienceworld.org/content/current
| link2-name    = Current issue
| JSTOR         = 
| OCLC          = 1604335
| LCCN          = 13010025 
| CODEN         = BSSAAP
| ISSN          = 1943-3573
| eISSN         = 0037-1106 
| boxwidth      = 
}}

'''''Bulletin of the Seismological Society of America''''' ('''BSSA''') is a bimonthly [[peer reviewed]] [[scientific journal]] published by the [[Seismological Society of America]]. The [[editor-in-chief]] is [[Diane I. Doser]] ([[University of Texas at El Paso]]). The journal covers [[seismology]] and related disciplines. Topical coverage includes theory and observation of [[seismic waves]], specific [[earthquakes]], the structure of the Earth, earthquake sources, hazard and risk estimation, and [[earthquake engineering]]. Publishing formats include regular papers and short notes. Publication has been continuous since 1911.

==Abstracting and indexing==
This journal is indexed by the following services:<ref name=hollis>[http://lms01.harvard.edu/F/EB6LGI59V1CXQX3VUICPIQHR8HAPVVE8QNM78YP8BPI3L61C7J-20003?func=find-b&find_code=kon&request=ocm01604335&pds_handle=GUEST Hollis Classic Library]. Harvard College. 2009</ref><ref name=cassi>[http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfOlTVLqjl685PxOTvACeoFzLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfzt4glDIzSoCsWbSRXbY7gA CAS Source Index (CASSI)] Search Result. American Chemical Society. 2013</ref><ref>[http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0037-1106 Master Journal List]. Thomson Reuters. 2013</ref>
 
* [[Science Citation Index]]
* [[Current Contents]] / Physical, Chemical & Earth Sciences
* [[Chemical Abstracts Service]]
* [[Applied Science & Technology Index]]
*  Coal Abstracts ([[International Energy Agency]])
* [[International Aerospace Abstracts]] 
* [[GeoRef]] 
* [[Computer & Control Abstracts]] 
* [[Electrical & Electronics Abstracts]] 
* [[Physics Abstracts. Science Abstracts. Series A]] 
* [[Energy Research Abstracts]]

==Notes==
* Note: An apparent alternate title is "Seismological Society of America, Bulletin" with the abbreviation "Seismol. Soc. Am., Bull." <ref name=hollis/><ref name=cassi/> 
* Note: the value of impact factor is tentative, and may be needing a reliable source. [https://archive.is/20130820060341/http://impact-factor.info/2012/b-seismol-soc-am/ Found 2012 impact factor here].

==References==
{{reflist}}

==External links==
* {{Official website|http://www.seismosoc.org/publications/bssa/}}
* [http://www.seismosoc.org/ Seismological Society of America]

[[Category:Seismology]]
[[Category:Geology journals]]
[[Category:Publications established in 1911]]
[[Category:English-language journals]]
[[Category:Earthquake engineering]]
[[Category:Engineering journals]]
[[Category:Academic journals published by learned and professional societies of the United States]]