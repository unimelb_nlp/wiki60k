{{Infobox Website
| name = NASA Star and Exoplanet Database
| url = http://nsted.ipac.caltech.edu
| type = [[Astronomy]]
| author = Operated for [[NASA]] by [[JPL]]/[[Caltech]]
| current status = Closed
}}

The '''NASA Star and Exoplanet Database''' ('''NStED''') is an on-line astronomical stellar and exoplanet catalog and data service that collates and cross-correlates [[astronomy|astronomical]] data and information on [[exoplanet]]s and their host [[star]]s. NStED is dedicated to collecting and serving important public data sets involved in the search for and characterization of exoplants and their host stars. The data include stellar parameters (such as positions, magnitudes, and temperatures), exoplanet parameters (such as masses and orbital parameters) and discovery/characterization data (such as published radial velocity curves, photometric light curves, images, and spectra).

The NStED collects and serves public data to support the search for and characterization of extra-solar planets (exoplanets) and their host stars. The data include published light curves, images, spectra and parameters, and time-series data from surveys that aim to discover transiting exoplanets. All data are validated by the NStED science staff and traced to their sources. NStED is the U.S. data portal for the CoRoT mission.

As of December 2011, SDtED is no longer in operation. Most data and services have been transferred to the [[NASA Exoplanet Archive]].

==Search characteristics==
Data are searchable either for an individual star or by stellar and planetary properties.

NStED offers direct access to frequently accessed tables:
*[http://nsted.ipac.caltech.edu/cgi-bin/bgServices/nph-bgExec?bgApp=/Sieve/nph-sieve&mission=NStED&uniqueid&viewfile=list_Planet_uniqueid&fromplanet List of all known planets]{{deadlink|date=April 2015}}
*[http://nsted.ipac.caltech.edu/cgi-bin/bgServices/nph-bgExec?bgApp=/Sieve/nph-sieve&mission=NStED&uniqueid&currentForm=goQuery&list_Multiplicity_uniqueid&listpath&frommulti List of all known planet-hosting stars]{{deadlink|date=April 2015}}

==Survey programs==
NStED also serves photometric time-series data from surveys that aim to discover transiting exoplanets such as ''[[Kepler (spacecraft)|Kepler]]'' and the [[COROT|CoRoT]].
NStED provides access to over 500,000 light curves from space and ground-based exoplanet transit survey programs, including:

* [[Kepler (spacecraft)|Kepler]]
* [[COROT|CoRoT]]
* [[HATNet Project]]
* [[Trans-Atlantic Exoplanet Survey]]
* [[South African Astronomical Observatory#KELT-South|KELT]]

The transit survey data are linked to an online periodogram service that can be used to search for periods in the transit survey data sets.  A user can also upload their own time series data to search for periods in their own data.

The planetary orbital parameters are linked to an online [https://web.archive.org/web/20110519110405/http://nsted.ipac.caltech.edu/applications/TransitSearch/exotransits.html  transit ephemeris prediction tool] which can be used to predict the probability that an exoplanet may transit and the date/time and observability of the transits.

==See also==
*[[Extrasolar Planets Encyclopaedia]]
*[[Exoplanet Data Explorer]]

==External links==
* [http://nexsci.caltech.edu NASA Exoplanet Science Institute]
* [http://www.planethunters.org Planet Hunters]
* [http://ipac.caltech.edu Infrared Processing and Analysis Center]

{{Exoplanet}}

[[Category:Astronomical databases]]
[[Category:Astronomy websites]]
[[Category:Exoplanet catalogues]]
[[Category:Stellar astronomy]]
[[Category:Computational astronomy]]
[[Category:Databases in the United States]]