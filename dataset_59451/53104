{{Infobox journal
| title = Virginia Environmental Law Journal
| cover =
| | discipline = [[Environmental Law]]
| abbreviation = Va. Environ. Law J.
| publisher = [[University of Virginia School of Law]]
| country = United States
| frequency = Triannually
| history = 1983-present
| impact = 0.92<ref>[http://lawlib.wlu.edu/LJ/index.aspx//index.aspx "Law Journals: Submission and Ranking, 2007-2014,"] Washington & Lee University. Accessed: June 28, 2015.</ref>
| website = http://www.velj.org/
| ISSN = 1045-5183
| LCCN = 90644818
| CODEN = VELJF8
| OCLC = 19769977
}}
The '''''Virginia Environmental Law Journal''''' ([[Bluebook]] abbreviation: ''Va. Envtl. L.J.'') is a [[law review]] edited by students at the [[University of Virginia School of Law]]. The journal covers research and discussion in the areas of [[environmental policy|environmental]] and [[natural resource]] law, on a broad array of topics from [[environmental justice]] to [[corporate liability]]. The Journal is perennially ranked among the top five environmental law journals in the country. <ref>http://lawlib.wlu.edu/LJ/</ref>

Established in 1980, the Virginia Environmental Law Journal is one of the oldest journals at the University of Virginia School of Law. It is entirely student-run. The Journal is closely tied to the law school’s Program in Environmental and Land Use Law, and it provides the Law School community with a forum for discussion of current environmental issues through its publication of legal scholarship related to the topic.

The Journal publishes the works of leading scholars, practitioners, and government officials, as well as exceptional student notes. 

==Annual symposia==
Through its annual symposia, the Journal encourages dialogue about environmental law and policy among students, professors, and the local Charlottesville community.

==See also==
* [[List of environmental law journals]]

==External links==
* {{Official website|http://www.velj.org/}}

==References==
{{reflist}}

{{University of Virginia}}

{{DEFAULTSORT:Virginia Environmental Law Journal}}
[[Category:University of Virginia School of Law]]
[[Category:Environmental law journals]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1983]]
[[Category:Law journals edited by students]]
[[Category:1983 establishments in Virginia]]