{{Redirect|Les Tribulations d'un Chinois en Chine|the 1965 film|Up to His Ears}}

{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
|name          = Tribulations of a Chinaman in China
|title_orig    = Les Tribulations d'un Chinois en Chine
|translator    =
|image         =<!-- Deleted image removed:  [[Image:Tribulations.jpg|200px]] -->
|caption = Sampson Low Edition
|author        = [[Jules Verne]]
|illustrator   = [[Léon Benett]]
|cover_artist  =
|country       = France
|language      = French
|series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #19
|genre         = [[Adventure novel]] 
|publisher     = [[Pierre-Jules Hetzel]]
|release_date  = 1879
|english_pub_date = 1879
|media_type    = Print ([[Hardcover|Hardback]])
|pages         =
|preceded_by   = [[The Begum's Fortune]]
|followed_by   = [[The Steam House]]
}}
'''''Tribulations of a Chinaman in China''''' ({{lang-fr|Les Tribulations d'un Chinois en Chine}}) is an [[adventure novel]] by [[Jules Verne]], first published in 1879. The story is about a rich Chinese man, Kin-Fo, who is bored with life, and after some business misfortune decides to die.

==Style==
The book is a traditional adventure, similar in style to ''[[Around the World in Eighty Days (novel)|Around the World in Eighty Days]]'', which is one of the author's more well-known books. However, it does contain more humour as well as criticism of topics such as the [[History of opium in China|British opium trade in China]].

==Plot summary==
Kin-Fo is an extremely wealthy man who certainly does not lack material possessions. However, he is terribly bored and when news reaches him about his major investment abroad, a bank in the United States, going bankrupt, Kin-Fo decides to die. He signs up for a $200,000 life insurance covering all kinds of accidents, death in war, and even [[suicide]]; Wang and Kin-Fo's fiancée are to be the beneficiaries. He rejects [[seppuku]] and [[hanging]] as means of dying, and is about to take [[opium]] laced with poison when he decides that he doesn't want to die without having ever felt a thrill in his life. Kin-Fo hires his old mentor, the philosopher Wang, to murder him before the life insurance expires.

After a while news reaches Kin-Fo that the American bank he had invested in was not bankrupt, but instead had pulled off a stock market trick and is now wealthier than ever. Unfortunately, Wang has already disappeared. Together with two body guards assigned by the insurance company, and his loyal but lazy and incompetent servant Soun, Kin-Fo travels around the country in an effort to run away from Wang and the humiliation from the affair.

One day he receives a message from Wang, stating that he can't stand the pain of having to kill one of his friends, and instead decided to take his own life while giving the task of killing Kin-Fo to a bandit he once knew. Kin-Fo, Soun and the two bodyguards now try to get to the bandit, planning to offer money in return for his life. The ship they travel with is hijacked, and they are forced to use their [[Personal flotation device|life vests]] with built-in sails to return to land.

After being kidnapped by the bandit they were looking for, they are blindfolded and returned to Kin-Fo's home, where his old friends (including Wang, who we now find out staged this entire history to teach him a lesson about how valuable life is) are waiting for him. He marries his young, beautiful fiancée after all and they live happily forever after.

== Trivia ==
A 1965 film starring [[Jean-Paul Belmondo]], ''[[Up to His Ears]]'', was loosely based on this novel.<ref>http://www.imdb.com/title/tt0059831/?ref_=fn_al_tt_1</ref> In 1990, Finnish [[auteur]] [[Aki Kaurismäki]] directed ''[[I Hired a Contract Killer]]'' with a similar plot.<ref>http://www.imdb.com/title/tt0099818/?ref_=fn_al_tt_1</ref> ''[[Bulworth]]'' (1998), by and featuring [[Warren Beatty]] and [[Halle Berry]], also seems to be inspired by the novel.<ref>http://www.imdb.com/title/tt0118798/?ref_=nv_sr_1</ref>

An English translation was serialized in [[The Leisure Hour]] in 1880.

==References==
{{Reflist}}

==External links==
{{commons category}}
{{Wikisourcelang|fr|Les Tribulations d’un Chinois en Chine}}
*[http://jv.gilead.org.il/works.html#TC E-text of ''Tribulations of a Chinaman in China'' in various languages at the '''Jules Verne Virtual Library'''.]
* {{librivox book | title=The Tribulations of a Chinaman in China | first=Jules| last=Verne}}

{{Verne}}

{{Authority control}}

[[Category:1879 novels]]
[[Category:French novels adapted into films]]
[[Category:Novels by Jules Verne]]
[[Category:Novels set in the Qing dynasty]]