{{about|journals currently published using the name Communications in Statistics|the journal previously published under the name "Communications in Statistics. Stochastic Models"|Stochastic Models}}
{{Infobox journal
| title = Communications in Statistics - Theory and Methods
| cover =
| discipline = [[Statistics]]
| abbreviation =
| publisher = [[Taylor & Francis]]
| country =
| history = 1970-present
| frequency = 20/year
| openaccess =
| impact = 0.406
| impact-year = 2009
| website = http://www.tandf.co.uk/journals/titles/03610926.asp
| link1 =
| link1-name =
| ISSN = 0361-0926
| eISSN = 1532-415X
| LCCN =
| OCLC = 48483352
| JSTOR =
}}
{{Infobox journal
| title = Communications in Statistics – Simulation and Computation
| cover =
| discipline = [[Statistics]]
| publisher = [[Taylor & Francis]]
| country =
| abbreviation =
| history = 1972-present
| frequency = Bimonthly
| openaccess =
| impact = 0.383
| impact-year = 2009
| website = http://www.tandf.co.uk/journals/titles/03610918.asp
| link1 =
| link1-name =
| ISSN = 0361-0918
| eISSN = 1532-4141
| LCCN =
| OCLC = 48483321
| JSTOR =
}}
'''''Communications in Statistics''''' is a [[peer review|peer-reviewed]] [[scientific journal]] that publishes papers related to [[statistics]]. It is published by [[Taylor & Francis]] in two series, ''Theory and Methods'' and ''Simulation and Computation''.

== ''Communications in Statistics – Theory and Methods'' ==
This series started publishing in 1970, and publishes papers related to statistical theory and methods. It publishes 20 issues each year. Based on Web of Science, the five most cited papers in the journal are:<ref>{{Cite web|url=http://www.tandfonline.com/action/showMostCitedArticles?journalCode=lsta20#.VRbVaS6nyW6|title= Communications in Statistics - Theory and Methods, Most Cited Articles |author=Taylor and Francis |accessdate=2015-03-28}}</ref>

* Kulldorff M. A spatial scan statistic, 1997, 982 cites.
* Holland PW, Welsch RE. Robust regression using iteratively reweighted least-squares, 1977, 526 cites.
* Sugiura N. Further analysts of the data by [[Akaike information criterion|Akaike's information criterion]] and the finite corrections, 1978, 490 cites.
* Hosmer DW, Lemesbow S. Goodness of fit tests for the multiple logistic regression model, 1980, 401 cites.
* Iman RL, Conover WJ. Small sample sensitivity analysis techniques for computer models.with an application to risk assessment, 1980, 312 cites.

=== Abstracting and indexing ===
''Communications in Statistics – Theory and Methods'' is indexed in the following services:
* [[Current Index to Statistics]]
* [[Science Citation Index Expanded]]
* [[Zentralblatt MATH]]

== ''Communications in Statistics – Simulation and Computation'' ==
This series started publishing in 1972, and publishes papers related to [[computational statistics]]. It publishes 6 issues each year. Based on Web of Science, the five most cited papers in the journal are:<ref>{{Cite web|url=http://www.tandfonline.com/action/showMostCitedArticles?journalCode=lssp20#.VRbXiC6nyW4|title= Communications in Statistics - Simulation and Computation, Most Cited Articles |author=Taylor and Francis |accessdate=2015-03-28}}</ref>

* Iman RL, Conover WJ. A distribution-free approach to inducing rank correlation among input variables, 1982, 519 cites.
* Wolfinger R. Covariance structure selection in general mixed models, 1993, 248 cites.
* Helland IS, On the structure of partial least squares regression, 1988, 246 cites.
* McCulloch JH. Simple consistent estimators of stable distribution parameters, 1986, 191 cites.
* Sullivan Pepe M, Anderson GL. A cautionary note on inference for marginal regression models with longitudinal data and general correlated response data, 1994, 162 cites.

=== Abstracting and indexing ===
''Communications in Statistics – Simulation and Computation'' is indexed in the following services:
* Current Index to Statistics
* Science Citation Index Expanded
* Zentralblatt MATH

==References==
{{Reflist}}

== External links ==
* [http://www.tandf.co.uk/journals/titles/03610926.asp ''Communications in Statistics - Theory and Methods'']
* [http://www.tandf.co.uk/journals/titles/03610918.asp ''Communications in Statistics - Simulation and Computation'']

{{Statistics journals}}

[[Category:Computational statistics journals]]
[[Category:Statistics journals]]
[[Category:Publications established in 1970]]
[[Category:Publications established in 1972]]
[[Category:Taylor & Francis academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]