{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Bodmin
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Experimental bomber
 | national origin=[[United Kingdom]]
 | manufacturer=[[Boulton & Paul Ltd]]
 | designer=[[John Dudley North]]
 | first flight=1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from=
 | variants with their own articles=
}}
|}
The '''Boulton & Paul P.12 Bodmin''' was an experimental [[United Kingdom|British]] twin-engined biplane bomber with its engines mounted in a fuselage engine room and with tandem pairs of tractor and pusher airscrews mounted between the wings.  The two Bodmins built flew successfully in 1924, proving the concept, but the layout was not developed to production.

==Design and development==
The Boulton & Paul Bodmin was one of the few multi-motor propeller-driven aircraft to have its engines in its fuselage.  The concept arose immediately after [[World War I]], when [[Bristol Aeroplane Company|The British & Colonial Aeroplane Co.]] (soon to be renamed Bristol Aeroplane) began thinking about large transport aircraft powered by [[steam turbine]]s mounted in an "engine room" in the fuselage and driving wing mounted propellers.  They intended to develop the idea using their large [[Bristol Braemar]] triplane bomber, initially modified to be powered by four 230&nbsp;hp (172&nbsp;kW) [[Siddeley Puma]]s and called, in anticipation of steam power the [[Bristol Tramp|Tramp]].<ref name="Barnes">{{Harvnb|Barnes|1964|pages=140–2}}</ref>   They sought and obtained [[Air Ministry]] support for this project, the Ministry appreciating the extra safety of an aircraft whose engines could be serviced in flight.  Consequentially, the Ministry also issued  [[List of Air Ministry Specifications#1920-1929|Specification 9/20]] for a smaller aircraft of the same configuration and placed orders for two prototypes with both Boulton & Paul for the twin-engined Bodmin and for the single-engined [[Parnall Possum]].<ref name="Brew">{{Harvnb|Brew|1993|pages=174–9}}</ref>  They were described as "Postal" aircraft to cover the Ministry's intents but were clearly bombers.<ref>{{Harvnb|Brew|1993|pages=177}}</ref>  All three types were built, but only the Bodmin and the Possum flew. It was recognised that the "engine room" arrangement came with a weight penalty owing to the gearing, clutches, drive shafts and supports, plus the need to strengthen wings, but [[John Dudley North]], Boulton & Paul's chief designer argued that the airframe weight would be reduced by 10% due to the all-metal construction, as pioneered in the [[Boulton Paul P.10]].<ref name="Brew"/>  This used tubes etc. produced in-house from steel sheet; the airframe was then fabric covered.

The Bodmin<ref name="Brew"/><ref name="Flight">[http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200528.html ''Flight'' 6 September 1923 pp.528-33]</ref>  was a large three bay biplane with unswept and unstaggered square tipped wings of equal span and constant chord.  These had leading-edge-balanced ailerons on both upper and lower planes and the rather rectangular fin and tailplane carried similarly balanced control surfaces.  The rudder extended below the fuselage underside.

The fuselage centre section contained the "engine room" with its two 450&nbsp;hp (336&nbsp;kW) [[Napier Lion]]s in tandem.<ref name="Brew"/><ref name="Flight"/>   One was ahead of the wing leading edge and the other at the wing centreline, with an enclosed, illuminated space between them where the engineer could stand upright and monitor and manage them.  The engines were mounted on the upper fuselage longerons, leaving a crawl space beneath.<ref name="Flight"/>  The Lions were oriented with their gearboxes away from the engineer's room and the power from each was taken to the airscrews by two drive shafts at right angles to the fuselage.  The two from the front engine drove a pair of two-bladed tractor propellers ahead of the leading edge via a pair of gearboxes half way between the wings, just beyond the first interplane struts. Their mountings extended rearwards to carry a similarly driven pair of four-bladed pusher propellers driven by the rear engine.  Port and starboard airscrews rotated in opposite directions and the fore and aft pairs did likewise, so that either engine could be shut down without any power asymmetry.  The centre section, engine drive shafts and propeller mountings were built as a unit independent of the wings, the drives having their own struts and bracing.<ref name="Brew"/>  The space between each fore and aft pair of propellers was occupied by a cylindrical petrol tank, and thin radiators extended between these tanks and the fuselage.<ref name="Flight"/>  Radiators and driveshafts were enclosed by a streamlined fairing on either side.<ref name="Flight"/>

The rest of the fuselage was conventional and of square cross section with rounded decking.<ref name="Brew"/><ref name="Flight"/>  The pilot sat well forward, behind a front gunner's position and the forward end chin shaped; the long front fuselage has been described as being "like the bow  of an inverted boat".<ref name="Brew"/>   There was also provision for a dorsal gunner just behind the wings. The main wide tracked single axle undercarriage had pneumatic springing and damping; a pair of smaller wheels further ahead and closer together served to prevent nose-overs and a standard tail skid extended below the rudder.

The first of two Bodmins flew early in 1924.<ref name="Brew"/>  It flew well, though with some drive and cooling problems and showed that despite Bristol's problems with the Tramp the fuselage mounted engine arrangement was workable. John North had been able to compensate the extra weight of this configuration by the savings of metal construction, which he estimated after the Bodmin was built to be as high as 20%, a factor of two better than his design estimate.  The safety bonus of engines that could be adjusted and mended in the air, the most important reason behind the layout was achieved.  The maximum speed and climb rate of the Bodmin were marginally better than those of the identically powered though slightly smaller metal-framed [[Boulton Paul Bugle|Boulton & Paul Bugle&nbsp;II]].<ref name="Brew"/><ref name="Brew2">{{Harvnb|Brew|1993|pages=187}}</ref>

The Bodmin could fly level on one engine and there was no asymmetry of thrust unlike a conventional twin-engine design.<ref>Kinsey, G ''Boulton & Paul Aircraft'' 1992 Terence Dalton p41</ref> The first prototype was written off due to undercarriage failure and the trials were completed with the second. The "engine room" concept did not catch on, but all-metal airframes served later aircraft well.

==Specifications==
{{Aerospecs
|ref={{harvnb|Brew|1993|page=179}}<!-- reference -->
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=3/4
|capacity=
|length m=16.27
|length ft=53
|length in=4½
|span m=21.34
|span ft=70
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=111.9
|wing area sqft=1,204
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=3,592
|empty weight lb=7,920
|gross weight kg=4,990
|gross weight lb=11,000
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=2
|eng1 type=[[Napier Lion]] water cooled W-12 piston engine
|eng1 kw=336<!-- prop engines -->
|eng1 hp=450<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=187
|max speed mph= at sea level 116
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=4,877
|ceiling ft=16,000
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=4.05
|climb rate ftmin= to 6,500 ft (1,981 m) 798
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
*[[Parnall Possum]]
*[[Bristol Tramp]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Boulton Paul}}
;Notes
{{Reflist|2}}

;Bibliography
*{{Cite book |title= Boulton Paul Aircraft since 1915 |last= Brew |first= Alec |coauthors= |edition= |year= 1993 |publisher= Putnam |location= London |isbn= 0-85177-860-7|ref=harv}}
*{{Cite book |title= Bristol Aircraft since 1910 |last= Barnes |first=C.H. |year=1964 |publisher=Putnam Publishing  |location=London |isbn= 0-370-00015-3|ref=harv}}
{{Refbegin}}
{{Refend}}

{{Boulton Paul aircraft}}

[[Category:Boulton Paul aircraft|Bodmin]]
[[Category:British bomber aircraft 1920–1929]]
[[Category:Twin-engined four-prop push-pull aircraft]]
[[Category:Biplanes]]