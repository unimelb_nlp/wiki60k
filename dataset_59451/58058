{{Infobox journal
| title = Journal of Developmental and Behavioral Pediatrics
| cover =
| editor = Suzanne D. Dixon
| discipline = [[Pediatrics]]
| former_names =
| abbreviation = J. Dev. Behav. Pediatr.
| publisher = [[Lippincott Williams & Wilkins]]
| country =
| frequency = 9/year
| history = 1980-present
| openaccess =
| license =
| impact = 1.750
| impact-year = 2012
| website = http://journals.lww.com/jrnldbp/Pages/default.aspx
| link1 = http://journals.lww.com/jrnldbp/pages/currenttoc.aspx
| link1-name = Online access
| link2 = http://journals.lww.com/jrnldbp/pages/issuelist.aspx
| link2-name = Online archive
| JSTOR =
| OCLC = 5780657
| LCCN = 80644911
| CODEN = JDBPD5
| ISSN = 0196-206X
| eISSN = 1536-7312
}}
The '''''Journal of Developmental and Behavioral Pediatrics''''' is a [[peer-reviewed]] [[medical journal]] covering [[Developmental-Behavioral Screening and Surveillance|developmental behavioral pediatrics]]. The journal was established in 1980 and is published by [[Lippincott Williams & Wilkins]]. The [[editor-in-chief]] is Suzanne Dixon ([[University of Washington]]). It is the official journal of the [[Society for Developmental and Behavioral Pediatrics]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-01-29}}</ref><ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8006933 |title=Journal of Developmental and Behavioral Pediatrics |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-01-29}}</ref>
{{columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[Social Sciences Citation Index]]
* [[Current Contents]]/Social & Behavioral Sciences
* Current Contents/Clinical Medicine
* [[BIOSIS Previews]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.750.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Developmental and Behavioral Pediatrics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.lww.com/jrnldbp/Pages/default.aspx}}
* [http://www.sdbp.org/ Society for Developmental and Behavioral Pediatrics]

[[Category:Publications established in 1980]]
[[Category:Lippincott Williams & Wilkins academic journals]]
[[Category:Pediatrics journals]]
[[Category:English-language journals]]
[[Category:Developmental psychology journals]]