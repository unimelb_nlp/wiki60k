{{good article}}
{{Use mdy dates|date=May 2015}}
{{Infobox film
| name           = Bertie's Brainstorm
| image          = Bertie's Brainstorm.jpg
| caption        = A surviving film still
| director       =
| producer       = [[Thanhouser Company]]
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = [[Motion Picture Distributing and Sales Company]]
| released       = {{film date|1911|1|17}}
| runtime        = 1 reel
| country        = United States
| language       = Silent film<br> [[English language|English]] inter-titles
}}

'''''Bertie's Brainstorm''''' is a 1911 American [[silent film|silent]] [[short film|short]] [[drama]] film produced by the [[Thanhouser Company]]. The film focuses on Bertie Fawcett, a dim-witted fop, who erroneously believes to have won the heart of May Vernon. In reality, May loves Jack and the two are set to be married, but May's father wishes he would prove his worth by earning his own living. Bertie chances upon the letter and sets off to make a living proceeds through a number of jobs with hope to claim May as his bride. The film ends with Bertie returning and finding out that May has married Jack. Little is known about the production of the film save that [[William Russell (American actor)|William Russell]] played an unknown role and that the scenario was written by [[Lloyd F. Lonergan]]. The [[fop]]pish character of Bertie may have been inspired by [[Edwin Thanhouser]]'s role as Bertie Nizril in ''Thoroughbred''. Originally conceived as a series, this ultimately singular work received praise from critics. The film is presumed [[lost film|lost]].

== Plot and production == 
An official synopsis published in the ''[[Billboard (magazine)|Billboard]]'' states, "Bertie Fawcett is a dudish chap, who believes that he has won the heart of May Vernon. May, however, regards Bertie as very much of a joke, and is in love with Jack Mace, who is her ideal of manly beauty. May's father has no objection to Jack personally, but he does not propose that the daughter he idolizes shall wed a weakling or a ne'er do well. Therefore, he tells May in a letter that if 'that young man wants to marry you, he must show his ability by earning his own living during vacation.' Unfortunately for Bertie, he sees the letter, and egotistically jumps to the conclusion that he is the person referred to. He starts out to make his own living, but soon finds that it is not as easy as it sounds. He is successfully a writer, a billposter, a village constable, and a living target in the baseball show, but fails to shine in any one sphere. And then to cap the climax, when he returns to claim his bride, he finds that May is married to Jack."<ref name=brain />
 
The only known actor in the production for [[William Russell (American actor)|William Russell]] in an unknown role.<ref name=brain /> A surviving film still appears to show Russell in the role of Jack at the climax of the film in which Bertie encounters the newly married couple.<ref>{{cite web | url=https://archive.org/details/movingpicturenew04unse | title=Moving Picture News Jan-Dec 1911 | publisher=Cinematograph Publishing Company | date=1911 | accessdate=28 May 2015 | pages=32}}</ref> The other cast credits are unknown, but many Thanhouser productions are fragmentary.<ref name="cast">{{cite web | url=http://www.thanhouser.org/tcocd/Filmography_files/ind6i4_n5.htm | title=Volume 2: Filmography - Thanhouser Filmography - 1910 | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=February 12, 2015 | author=Q. David Bowers}}</ref> In late 1910, the Thanhouser company released a list of the important personalities in their films. The list includes [[G.W. Abbe]], [[Justus D. Barnes]], [[Frank H. Crane]], [[Irene Crane]], [[Marie Eline]], [[Violet Heming]], [[Martin J. Faust]], [[Thomas Fortune]], [[George Middleton (actor)|George Middleton]], [[Grace Moore (Thanhouser actress)|Grace Moore]], [[John W. Noble (actor)|John W. Noble]], [[Anna Rosemond]], [[Mrs. George Walters]].<ref name=cast /> The scenario was written by [[Lloyd F. Lonergan]] and the character of Bertie may have been based on [[Edwin Thanhouser]]'s role of Bertie Nizril from ''Thoroughbred''.<ref name=brain /> The play was a three-act comedy by [[Ralph Lumley]] and was first produced on February 13, 1895.<ref>{{cite web | url=https://books.google.com/books?id=y9IJYrZ0ro8C&pg=PA527&dq=Thoroughbred+1895+Lumley&hl=en&sa=X&ved=0CC4Q6AEwA2oVChMIho39l7r6xgIVC2s-Ch1LrQL1#v=onepage | title=Appletons' Annual Cyclopedia and Register of Important Events: Embracing Political, Military, and Ecclesiastical Affairs; Public Documents; Biography, Statistics, Commerce, Finance, Literature, Science, Agriculture, and Mechanical Industry, Volume 40 | publisher=Appleton | date=1901 | accessdate=27 July 2015 | pages=527}}</ref> The play would come to the Garrick Theatre in New York City on August 17, 1896 and Edwin Thanhouser took over the role on August 29, 1896.<ref>{{cite web | url=http://www.thanhouser.org/tcocd/Narrative_files/c1s5.htm | title=Volume 1: Narrative History - Chapter 1: The Early Life On Tour with Thoroughbred | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=27 July 2015 | author=Q. David Bowers}}</ref> A series of Bertie films was projected, but only this work was produced. Two other announced works included ''Bertie's Bride'' and ''Bertie's Baby''.<ref>{{cite web | url=http://www.thanhouser.org/tcocd/Narrative_files/c4s3.htm | title=Volume 1: Narrative History - Chapter 4: 1911 Thanhouser Releases | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=28 May 2015 | author=Q. David Bowers}}</ref>

==Release and reception ==
The single reel drama, approximately 1,000 feet long, was released on January 17, 1911.<ref name="brain">{{cite web | url=http://www.thanhouser.org/tcocd/Filmography_files/kq__3l.htm | title=Volume 2: Filmography - Bertie's Brainstorm | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=28 May 2015 | author=Q. David Bowers}}</ref> The film received favorable reviews from ''[[Billboard (magazine)|Billboard]]'', ''[[The Moving Picture World]]'' and ''[[The New York Dramatic Mirror]]''. ''Billboard'' would write, "The adventures of the unfortunate Bertie are such as will make the usual motion picture audience chuckle with appreciation. The film is, of course, a farce essentially. The photography is well up to the Thanhouser standard."<ref name=brain /> Walton of the [[Moving Picture News]], would quip, "This beats the brainstorm in ''[[Les Miserables]]''. It is a Doré nightmare."<ref name=brain />  The comedic farce of the dim-witted [[fop]] proved to be successful if ultimately singular release of an expected series of films.<ref name=brain /> The film is presumed [[lost film|lost]] because the film is not known to be held in any archive or by any collector.<ref name="res">{{cite web | url=http://www.thanhouser.org/research.htm | title=Thanhouser Company Film Preservation, Inc. Research Center - Film Database| publisher=Thanhouser.org | date=2014 | accessdate=20 January 2015}}</ref>

== References ==
{{reflist|30em}}

{{DEFAULTSORT:Bertie's Brainstorm}}
[[Category:1911 films]]
[[Category:1910s drama films]]
[[Category:American drama films]]
[[Category:American films]]
[[Category:American silent short films]]
[[Category:American black-and-white films]]
[[Category:Thanhouser Company films]]
[[Category:Lost films]]