{{Infobox book
|name           = The Last Day of a Condemned Man
| title_orig    = Le Dernier Jour d'un Condamné
| translator    = 
| image         = HugoLastDayCondemnedMan.jpg
| caption = 1829 illustration from the<br>first edition
| author        = [[Victor Hugo]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = [[French language|French]]
| series        = 
| subject       = 
| genre         = [[Romanticism]]
| publisher     = [[Gosselin]]
| pub_date      = 1829
| english_pub_date =
| media_type    = 
| pages         =
| isbn          = 
| oclc          = 
}}

'''''The Last Day of a Condemned Man''''' ({{lang-fr|Le Dernier Jour d'un Condamné}}) is a short [[novel]] by [[Victor Hugo]] first published in [[1829 in literature|1829]]. The novel recounts the thoughts of a man condemned to die. Victor Hugo wrote this novel to express his feelings that the [[death penalty]] should be abolished.

==Genesis==
Victor Hugo saw several times the spectacle of the [[guillotine]] and was angered at the spectacle that society can make of it. It was the day after crossing the "Place de l'Hotel de Ville" where an executioner was greasing the guillotine in anticipation of a scheduled execution that Hugo began writing ''The Last Day of a Condemned Man''. He finished very quickly.<sup><ref>↑ Achevé en 3 semaines selon Victor Hugo raconté par un témoin de sa vie , chapitre L ou en un mois et demi (14 novembre 1828 - 26 décembre 1828) selon Roger Borderie (Notices sur le Dernier Jour d’un condamné - Gallimard 1970)</ref></sup> The book was published in February 1829 by Charles Gosselin without the author's name. Three years later, on 15 March 1832, Hugo completed his story with a long preface and his signature.

==Plot summary==
A man who has been condemned to death by the guillotine in 19th-century France writes down his cogitations, feelings and fears while awaiting his execution. His writing traces his change in [[psyche (psychology)|psyche]] vis-a-vis the world outside the prison cell throughout his imprisonment, and describes his life in prison, everything from what his cell looks like to the personality of the prison priest. He does not betray his name or what he has done to the reader, though he vaguely hints that he has killed someone.

On the day he is to be executed he sees his three-year-old daughter for the last time, but she no longer recognizes him, and she tells him that her father is dead.

The novel ends just after he briefly but desperately begs for pardon and curses the people of his time, the people he hears outside, screaming impatiently for the spectacle of his decapitation.

==Notes==
{{reflist}}

==External links==
{{Gutenberg|no=6838|name=Le Dernier Jour d'un Condamné}} {{fr}}
* {{librivox book | title=The Last Day of a Condemned | author=Victor HUGO}}

{{Victor Hugo}}

{{Authority control}}

{{DEFAULTSORT:Last Day of a Condemned Man, The}}
[[Category:1829 novels]]
[[Category:Novels by Victor Hugo]]


{{1820s-novel-stub}}