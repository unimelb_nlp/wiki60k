{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox University Boat Race
| name= 132nd Boat Race
| winner =Cambridge
| margin =  7 lengths
| winning_time= 17 minutes 58 seconds
| date= {{start date|1986|03|29|df=y}}
| umpire = Mike Sweeney<br>(Cambridge)
| prevseason= [[The Boat Race 1985|1985]]
| nextseason= [[The Boat Race 1987|1987]]
| overall =69&ndash;62
| reserve_winner=Isis
| women_winner =Oxford
}}

The 132nd [[The Boat Race|Boat Race]] took place on 29 March 1986.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Cambridge won by seven lengths and took their first victory in eleven years, in one of the fastest winning times in the history of the event. Cambridge was coxed for the first time by a woman, Carole Burton. Isis won the reserve race, while Oxford were victorious in the Women's Boat Race.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 17 July 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 17 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=14 July 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 14 July 2014}}</ref> Oxford went into the race as reigning champions, having beaten Cambridge by four-and-three-quarter lengths in the [[The Boat Race 1985|previous year's race]].  However Cambridge held the overall lead, with 68 victories to Oxford's 62, despite Oxford having won the previous ten races.<ref name=atlast>{{Cite news | title = Light Blues to win at last | first = Christopher | last = Dodd | work = [[The Guardian]] | page = 15 | date = 29 March 1986}}</ref> 

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

The race was sponsored by [[Ladbrokes]] for the tenth consecutive year,<ref name=stim>{{Cite newspaper | work = [[The Times]] | date = 6 April 1985 | page = 30 | issue = 62105 | title = Boat Race interest stimulated by a clash of style and contrasting motivation | first = Jim | last = Railton| first2=Simon |last2=Barnes}}</ref> estimated to be worth about £30,000 to each boat club,<ref>{{Cite news | title = Oxford's challenge carries more weight| first = Christopher | last = Dodd | work = [[The Guardian]] | date = 26 March 1986| page = 29}}</ref> and was umpired by former Cambridge rower Mike Sweeney.<ref name=spring/>

==Crews==
The Oxford crew weighed an average of over {{convert|5|lb|kg}} per rower more than Cambridge.<ref name=spring/> Cambridge's crew featured only two rowers over the age of 24 while Oxford had just three men under 25.<ref name=dom>{{Cite news | work = [[The Times]] | title = Veterans' domination leaves schoolboys with the blues|first=David | last=  Miller | page = 32 | issue = 62418}}</ref> Oxford saw three [[Blue (university sport)|Blues]] return while Cambridge welcomed back four. The Cambridge crew featured three international rowers: two Canadians (Gibson and Wilson), and the American Pew.  The Light Blue cox, Carole Burton, was the first woman to steer the Cambridge boat.<ref>{{Cite web | url = http://www.independent.co.uk/sport/boat-race-facts-and-figures-1303526.html | work = The Independent | title = Boat Race facts and figures | accessdate = 2 April 2017|date=5 April 1996}}</ref> Oxford's MacDonald was the oldest in the race at the age of 30, he was accompanied in the boat by international rowers Clark and Livingstone from the United States and Jones from Australia.<ref name=atlast/> [[Daniel Topolski|Dan Topolski]] was the Oxford coach while Cambridge relied on [[Alan Inns]] and Canadian Olympic coach [[Neil Campbell (rower)|Neil Campbell]].<ref name=spring/> Cambridge were clear favourites to win, but prior to the race, Topolski claimed his crew had "pulled themselves back into contention by sheer hard work."<ref>{{Cite news | work = [[The Financial Times]] | title = A Light Blue revival? | first = Michael | last = Donne | date = 29 March 1986| issue =29891 | page= 14}}</ref>
 
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || G R Screaton || [[Merton College, Oxford|Merton]] || 13 st 7 lb || I R Clarke || [[Fitzwilliam College, Cambridge|Fitzwilliam]] || 12 st 9 lb
|-
| 2 ||  D H M Macdonald || [[Mansfield College, Oxford|Mansfield]] || 13 st 12 lb || M Wilson|| [[Trinity Hall, Cambridge|Trinity Hall]] || 12 st 9 lb
|-
| 3 || M R Dunstan || [[Worcester College, Oxford|Worcester]] || 13 st 7 lb || J D Hughes || [[Downing College, Cambridge|Downing]] || 13 st 10 lb
|-
| 4 || G R D Jones || [[New College, Oxford|New College]] || 13 st 11 lb || J S Pew || [[First and Third Trinity Boat Club|1st and 3rd Trinity]] || 15 st 1 lb
|-
| 5 || B M Philp (P) || [[Worcester College, Oxford|Worcester]] || 15 st 9 lb || S M Peel || [[Downing College, Cambridge|Downing]] || 14 st 0 lb
|-
| 6 || C G H Clark || [[University College, Oxford|University]] || 14 st 13 lb || P M Broughton || [[Magdalene College, Cambridge|Magdalene]]  || 13 st 11 lb
|-
| 7 || G A Livingston || [[Oriel College, Oxford|Oriel]] || 14 st 1 lb || E A F Gibson || [[Churchill College, Cambridge|Churchill]] ||  13 st 4 lb
|-
| [[Stroke (rowing)|Stroke]] || A M S Thomas || [[Pembroke College, Oxford|Pembroke]] || 13 st 13 lb || [[John Pritchard (rower)|J M Pritchard]] || [[Robinson College, Cambridge|Robinson]]  || 14 st 1 lb
|-
| [[Coxswain (rowing)|Cox]] || A S Green || [[Christ Church, Oxford|Christ Church]] || 7 st 12 lb || C A Burton || [[Fitzwilliam College, Cambridge|Fitzwilliam]]  || 6 st 9 lb
|-
!colspan="7"|Source:<ref name=spring>{{Cite newspaper | work = [[The Times]] | date = 29 March 1986 | page = 40 | issue = 62417 | title = Oxford must spring to life to extend run | first = Jim | last = Railton}}</ref><br>(P) &ndash; boat club president (Cambridge had a non-rowing president in Quintus Travis).<ref name=atlast/>
|}

==Races==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]]]]
Oxford won the toss and elected to start from the Surrey station.  The predicted severe wind did not materialise and from the start, Cambridge pulled ahead.<ref name=faith>{{Cite news | title = Cambridge show their faith in steady state | first = Jim | last = Railton | work = [[The Times]] | date = 31 March 1986 | page=30 | issue = 62418 }}</ref> A half-length lead by the end of Putney boathouses became a two length lead by [[Hammersmith Bridge]], and Cambridge's cox Burton steered towards the safer Surrey side, extending Cambridge's lead to 14 seconds by Chiswick Steps.  Continuing to pull away, Cambridge passed the finishing post 21 seconds and seven lengths ahead of Oxford.  It was the sixth fastest time in the history of the race.<ref name=faith/> This was Cambridge's first victory in eleven years and took the overall record to 69&ndash;62 in favour of Cambridge.<ref name=results>{{Cite web | url = http://theboatrace.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 12 June 2014}}</ref>

In the reserve race, Oxford's Isis beat Cambridge's Goldie by six lengths, while Oxford won the 41st [[Women's Boat Race]].<ref name=results/>

==Reaction==
The trophy was presented by former Cambridge student [[Prince Edward, Earl of Wessex|Prince Edward]].<ref name=splash/> Cambridge stroke [[John Pritchard (rower)|John Pritchard]] said "After our initial start we built up for 20 strokes, steadied and then just grinded away."<ref name=faith/> He added "I was just stirring the tea while the others did the work."<ref name=spring/> Cambridge cox Carole Burton noted "I went where I wanted to go."<ref name=wait>{{Cite news | title = Cambridge's wait is over | date = 30 March 1986 | work = [[The Observer]] | first = Roger | last= Jennings | page = 49}}</ref> Following tradition, Pritchard picked her up and threw her into the river.<ref name=splash>{{Cite news | title = Splashing victory for cox Carole| work = [[The Observer]] | date = 30 March 1986 | first = John | last= Reardon | page = 1}}</ref> Oxford's Jones remarked "we were as well prepared as last year but we found no magic."<ref name=wait/> Topolski conceded that even had Oxford been at their best, they would still have lost the race.<ref>{{Cite news | title = Topolski's men toppled at last | work = [[The Guardian]] | first = Frank | last = Keating | date = 31 March 1986 | page = 17}}</ref>

==References==
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1986}}

[[Category:1986 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1986 sports events]]
[[Category:1986 in rowing]]