{{Good Article}}
{{Infobox album | <!-- See Wikipedia:WikiProject_Albums -->
| Name        = At Newport 1960
| Type        = live
| Artist      = [[Muddy Waters]]
| Cover       = Muddywatersnewport.jpg
| Released    = November 15, 1960 (US)
| Recorded    = July 3, 1960
| Genre       = [[Chicago blues]]
| Length      = 32:38
| Label       = [[Music Corporation of America|MCA]]/[[Chess Records|Chess]]
| Producer    = [[Leonard Chess]]
| Last album  = ''[[Sings Big Bill Broonzy]]''<br/>(1960)
| This album  = '''''At Newport 1960'''''<br/>(1960)
| Next album  = ''[[Folk Singer (album)|Folk Singer]]''<br/>(1964)
}}

'''''At Newport 1960''''' is a live album by [[Muddy Waters]] performed at [[Newport Jazz Festival]] in [[Newport, Rhode Island]], with his backing band, consisting of [[Otis Spann]] (piano, vocals), [[Pat Hare]] (guitar), [[James Cotton]] (harmonica), Andrew Stevens (bass) and [[Francis Clay]] (drums), on July 3. Waters's performances across Europe in the 1950s and at Newport helped popularize [[blues]] to a broader audience, especially to whites. The album is said to be one of the first live blues albums.

The album was released in the US on November 15 that year, featuring eight songs, including "I Got My Brand on You" to "Goodbye Newport Blues". In 2001, [[Chess Records]] released a remastered version, which includes three bonus tracks recorded in [[Chicago]] in June. ''At Newport 1960'' never charted, but it received critical acclaim and was influential for future bands. It was ranked on several music lists, including at number 348 on ''Rolling Stone'''s "[[Rolling Stone's 500 Greatest Albums of All Time|500 Greatest Albums of all Time]]" in 2003.

==Background==
After releasing his debut album ''[[The Best of Muddy Waters]]'' (1958), a [[greatest hits]] collection, and ''[[Sings Big Bill Broonzy]]'' (1960), a collection of [[cover (music)|covers]] of songs by the blues musician [[Big Bill Broonzy]], Waters performed at the [[Newport Jazz Festival]].{{sfn|Capace|2001|p=315}} Waters had already been a well-known blues musician across Europe and the United States in the '50s. His successful performances with his [[electric blues]] band, consisting of his half-brother [[Otis Spann]] (piano, vocals), [[Pat Hare]] (guitar), [[James Cotton]] (harmonica), Andrew Stevens (bass) and [[Francis Clay]] (drums), increasingly popularized the blues in mainstream music in the United States and Europe, especially among white audiences.{{sfn|Smith|2009|p=15–17}}

==Recording==
The gig was scheduled on July 3, Sunday afternoon. The day before, performances by [[Ray Charles]] and singing group [[Lambert, Hendricks and Ross]] were met with crowd rushes. About 300 drunken [[hipster (1940s subculture)|hipster]]s made an uproar during Charles' performance caused by poor police security. The policemen attacked with [[teargas]] and [[water hose]]s. The riots became so out of control that the [[National Guard of the United States|National Guard]] was called in at midnight to calm the crowd.  When Waters and his band arrived on the scheduled day, they intended to drive back on the next day, until driver [[James Cotton]] saw [[John Lee Hooker]] standing at a corner, his guitar on his back without a guitar case. Cotton said Hooker should get into his car to get the musicians out of harm's way. At the same time, the city council decided to cancel the concert, but concert promoter [[George Wein]] convinced them when he said that the [[United States Information Agency]] (USIA) planned to film the festival to teach American culture in other countries.{{sfn|Gordon|2003|pp=197–199}}

Before Waters' performance, his band backed [[Otis Spann]], who was the band leader, and John Lee Hooker. At about 17 p.m., Waters entered the stage, wearing black, while the rest of the band wore white formal dress. ''At Newport 1960'' opens with then-unreleased "I Got My Brand on You", which was recorded one month prior, and "[[(I'm Your) Hoochie Coochie Man]]", both written by [[Willie Dixon]]. Next are the [[Big Joe Williams]] cover "[[Baby Please Don't Go]]", [[St. Louis Jimmy Oden|Oden]]'s "Soon Forgotten", Dixon's "Tiger in Your Tank" and Broonzy's "I Feel So Good". During the latter he performed hip swings, and during "[[I've Got My Mojo Working]]", which he played a second time, he performed Elgin movements, then a [[foxtrot]] with Cotton. At the end he did a [[jitterbug]]; when he returned to the microphone and performed the move a second time, he received massive cheers from the audience. At the end of this song, every bluesman gathered at the stage to perform [[medley (music)|medley]]s of blues standards. Jazz poet and directorate of Newport [[Langston Hughes]] spontaneously wrote a finishing song, the slow "Goodbye Newport Blues", this time Spann with as singer, as Waters was too exhausted to perform.{{sfn|Smith|2009|p=15–17}}{{sfn|Gordon|2003|pp=197–199}}

The album was released in the United States in November 15, the same year they performed their concert in Newport, on the [[MCA Records|MCA]] label, and produced by [[Leonard Chess]].{{sfn|Smith|2009|p=15–17}} A [[Compact Disc|CD]] version was released in 1987, but one [[rum-running|bootleg]]er already released a different version in the early 90s. It was [[digital audio|digitally]] remastered in 2001 by MCA, with a significantly better quality in bass and singing.<ref name="Allmusic"/> The remastered version contains three bonus tracks recorded in Chicago in June 1960.<ref>{{cite web|url=http://www.worldcat.org/title/muddy-waters-at-newport-1960/oclc/046675856 |title=Muddy Waters at Newport, 1960 (Musical CD, 2001) |publisher=WorldCat |accessdate=January 11, 2011}}</ref>

==Album cover==
The album cover depicts Muddy Waters at the Newport Jazz Festival holding a [[semi-acoustic guitar]]. When the photographer, [[Burt Goldblatt]], asked him to pose for the cover, Waters left his [[Fender Telecaster]] (which he played during the concert) on the stage and instead held the semi-acoustic guitar, belonging to his friend [[John Lee Hooker]].{{sfn|Adelt|2011|p=30}}

==Critical reception==
{{Album ratings
| rev1 = [[All About Jazz]] 
| rev1Score = <small>(favorable)</small><ref name="AllAboutJazz">{{cite web|url=http://italia.allaboutjazz.com/reviews/r0701_701_it.htm |author=Maurizo Comandini |title=Lookin' 'round: Muddy Waters at Newport 1960 |publisher=[[All About Jazz]] |language=Italian |accessdate=January 11, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110707112538/http://italia.allaboutjazz.com/reviews/r0701_701_it.htm |archivedate=July 7, 2011 |df= }}</ref>
| rev2 = [[Allmusic]] 
| rev2Score = {{Rating|5|5}}<ref name = "Allmusic">{{cite web|url=http://www.allmusic.com/album/r123344|title=At Newport – Muddy Waters|work=Allmusic|publisher=Rovi Corporation|author=Cub Coda|accessdate=January 11, 2011}}</ref>
| rev3 = Blues Access
| rev3Score = <small>(favorable)</small><ref name = "Blues Access">{{cite web|url=http://www.bluesaccess.com/No_46/bluesroots.html|title=Blues & Roots|publisher=Blues Access|author=[[John Sinclair (poet)|John Sinclair]]|date=May 22–23, 2001|accessdate=January 11, 2011}}</ref>
| rev4 = ''[[fRoots]]''
| rev4Score = <small>(favorable)</small>{{sfn|Froots|1996|p=162}}
| rev5 = ''Southwest Blues''
| rev5Score = <small>(favorable)</small><ref name = "Southwest Blues">{{cite web|url=http://www.southwestblues.com/reviews/2001/cd-11-01muddy.htm|archiveurl=https://web.archive.org/web/20110716112758/http://www.southwestblues.com/reviews/2001/cd-11-01muddy.htm|archivedate=July 16, 2011|title=SW Blues CD Review|date=November 2001|author=Mark A. Cole|publisher=''Southwest Blues''|accessdate=January 11, 2011}}</ref>
}}
''At Newport 1960'' received positive critical reception. It was generally praised for the powerful and fizzy performance by Waters and his band. Cub Koda, writing for [[Allmusic]], said that Waters "lays it down tough and cool with a set that literally had [the audience] dancing in the aisles by the set close". Furthermore, he remarked that the opening track, "I Got My Brand on You", "positively burns the relatively tame".<ref name="Allmusic"/> Matthew Oshinsky, in ''1001 Albums You Must Hear Before You Die'', praised the "merciless refrain" in "Hoochie Coochie Man" and the "unvarnished moaning" in "Baby Please Don't Go". He also enjoyed Muddy's powerful [[baritone]], Cotton's harmonica playing, Spann's "pub piano"–like playing and the overall danceable music.{{sfn|Lydon|2005|p=53}} 

Chris Smith, in ''101 Albums That Changed Popular Music'', praised Waters's "growly vocal presentation, energetic stage presence, and electrifying (literally and figuratively) performances."{{sfn|Smith|2009|p=15-17}} The album was ranked number 348 on ''Rolling Stone''{{'}}s "[[Rolling Stone's 500 Greatest Albums of All Time|500 Greatest Albums of All Time]]" in 2003, in which the band's playing was described as "tough, tight and in the groove" and Cotton's harmonica jams were mentioned as "a special treat."<ref name = "RS">{{cite web|url=http://www.rollingstone.com/music/lists/500-greatest-albums-of-all-time-19691231/at-newport-1960-muddy-waters-19691231|title=348) At Newport 1960|publisher=Wenner Media|work=Rolling Stone|date=November 2003|accessdate=January 11, 2011}}</ref> In ''[[Vibe (magazine)|Vibe]]' magazine's "100 Essential Albums of the 20th Century", a critic called the album "immortal."{{sfn|Vibe|1999|p=100}} The album is mentioned in ''The Rough Guide to Blues 100 Essential CDs''.{{sfn|Ward|2000|p=177-178}}

Many musicians and bands, such as the [[The Rolling Stones|Rolling Stones]], [[Jimi Hendrix]], [[AC/DC]] and [[Led Zeppelin]], have been influenced by his electric sound and used this and his greatest hits album in creating a hard rock sound. ''At Newport 1960'' was one of the first live blues albums.{{sfn|Smith|2009|p=15–17}}

==Track listing==
# "I Got My Brand on You" ([[Willie Dixon]]) – 4:24
# "[[Hoochie Coochie Man|I'm Your Hoochie Coochie Man]]" (Dixon) – 2:50
# "[[Baby, Please Don't Go]]" ([[McKinley Morganfield]]) – 2:52
# "Soon Forgotten" ([[St. Louis Jimmy Oden|James Oden]]) – 4:08
# "Tiger in Your Tank" (Dixon) – 4:12
# "I Feel So Good" ([[Bill Broonzy]]) – 2:48
# "[[Got My Mojo Working]]" ([[Preston Foster]]) – 4:08
# "Got My Mojo Working, Part 2" (Foster) – 2:38
# "Goodbye Newport Blues" ([[Langston Hughes]], Morganfield) – 4:38

===2001 remastered issue bonus tracks===
#<li value="10">"I Got My Brand on You" (Dixon) – 2:22
# "Soon Forgotten" (Oden) – 2:41
# "Tiger in Your Tank" (Dixon) – 2:17
# "Meanest Woman" (Morganfield) – 2:18

Tracks 1, 2, 7, 8 were credited to McKinley Morganfield on the original LP.

== Personnel ==
{{col-start}}
{{col 2}}
;Original vinyl release
* Muddy Waters - guitar, vocals
* [[Otis Spann]] - piano, vocals
* [[Pat Hare]] - guitar
* [[James Cotton]] - harmonica
* Andrew Stephens - bass
* [[Francis Clay]] - drums
* Jack Tracy - liner notes
* Burt Goldblatt - photography
{{col 2}}
;Additional credits for 1987 re-release
* Mary Katherine Aldin - liner notes
* Bob Schnieders - liner notes, coordination, research
* Geary Chansley - photo research
* Mike Fink - reissue design
* Steve Hoffman - master tape restoration
* Erick Labson - digital remastering
* Andy McKaie - reissue producer
* Beth Stempel - reissue producer
* [[Vartan (art director)|Vartan]] - reissue art director
{{col-end}}

==References==
{{reflist|colwidth=30em}}

;Bibliography
{{refbegin}}
* {{cite book|last=Adelt|first=Dr. Ulrich|title=Blues Music in the Sixties: A Story in Black and White |location=New Brunswick, New Jersey|publisher=Rutgers University Press|date=August 31, 2011|isbn=978-0-8135-5174-6|oclc=752962199|ref=harv}}
* {{cite book|last=Capace|first=Nancy|url=https://books.google.com/books?id=dlLDIiQv9twC&pg=PA315&dq=at+newport+1960+muddy+waters+review&hl=de&ei=PObkTorwIMLktQbg5-2hCQ&sa=X&oi=book_result&ct=result&redir_esc=y#v=onepage&q=at%20newport%201960%20muddy%20waters%20review&f=false|title=Encyclopedia of Mississippi|location=Santa Barbara, California|publisher=Somerset Publishers|date=December 1, 2001|isbn=978-0-403-09603-9|oclc=46485566|ref=harv}}
* {{cite book|last=Smith|first=Chris|url=https://books.google.com/books?id=Jk5fPOyRIXEC&pg=PA3&dq=at+newport+1960+muddy+waters&hl=de&ei=p6HkTtOdLcrdsgbRpPzNCQ&sa=X&oi=book_result&ct=result&redir_esc=y#v=onepage&q=at%20newport%201960%20muddy%20waters&f=false|title=101 albums that changed popular music|location=New York City, New York|publisher=Oxford University Press|date=April 14, 2009|isbn=978-0-19-537371-4|oclc=259266639|ref=harv}}
* {{cite journal|url=https://books.google.com/books?id=YCgEAAAAMBAJ&pg=PA155&dq=Vibe+100+Essential+Albums+of+the+20th+Century&hl=de&ei=TCDlTveCOceOswaY7tSxCQ&sa=X&oi=book_result&ct=result&resnum=3&ved=0CEYQ6AEwAg#v=onepage&q=muddy%20waters&f=false|title=The Vibe 100 – Judgement Day|publisher=''Vibe Magazine''|volume=7|issue=10|date=December 1999 – January 2000|ref={{harvid|Vibe|1999}}}}
* {{cite journal|title=And The Rest: Muddy Waters – Live at Newport/Live (BGO)|publisher=[[fRoots]]|date=December 1996|ref={{harvid|Froots|1996}}}}
* {{cite book|last=Lydon|first=Michael|title=1001 Albums: You Must Hear Before You Die|location=London|publisher=Cassell Illustrated|date=October 31, 2005|isbn=978-1-84403-392-8|oclc=224890343|ref=harv}}
* {{cite book|last=Ward|first=Greg|title=The Rough Guide to Blues 100 Essential CDs|publisher=Rough Guides|edition=1st|date=November 23, 2000|isbn=978-1-85828-560-3|oclc=231873478|ref=harv}}
* {{cite book|last=Gordon|first=Robert|others=Keith Richards (foreword)|title=Can't Be Satisfied – The Life and Times of Muddy Waters|date=June 1, 2003|publisher=Back Bay Books|isbn=978-0-316-16494-8|ref=harv}}<!--|accessdate=January 16, 2012-->
{{refend}}

{{Muddy Waters}}

[[Category:Muddy Waters albums]]
[[Category:Albums recorded at the Newport Jazz Festival]]
[[Category:1960 live albums]]
[[Category:1960 in Rhode Island]]
[[Category:Chess Records live albums]]
[[Category:MCA Records live albums]]
[[Category:English-language live albums]]
[[Category:Albums produced by Leonard Chess]]