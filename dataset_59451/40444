'''Michele J. Gelfand''' is a [[Cultural psychology|cultural psychologist]], best known for being an expert on tightness-looseness theory,<ref name=":1">{{Cite web|url=https://www.psychologytoday.com/blog/culture-conscious/201306/some-it-tight-some-it-loose|title=Some Like It Tight, Some Like It Loose|last=White|first=Lawrence T.|last2=Jackson|first2=Steven|date=2013-07-21|website=Psychology Today|access-date=2016-06-01}}</ref> which explains variations in the strength of social [[Norm (social)|norms]] and punishments across human groups.<ref name=":2" /> She is currently a Professor and Distinguished University Scholar Teacher at the [[University of Maryland, College Park]].<ref>{{Cite web|url=http://www.gelfand.umd.edu/pages/People4.html|title=Michele J. Gelfand|website=University of Maryland Department of Psychology|access-date=2016-05-31}}</ref>

== Education and career  ==
Gelfand studied at [[Colgate University]], where she graduated with a [[Bachelor of Arts|B.A.]] in psychology in 1989. She graduated from the [[University of Illinois at Urbana–Champaign|University of Illinois, Urbana-Champaign]] in 1996 with a [[Doctor of Philosophy|PhD]] in [[social psychology]] and [[Industrial and organizational psychology|organizational psychology]].<ref name=":0">{{Cite web|url=http://www.gelfand.umd.edu/docum/Michele%20Gelfand%20CV%20Feb%202017.pdf|title=Michele J. Gelfand Curriculum Vitae|last=|first=|date=|website=University of Maryland Department of Psychology|publisher=|archive-url=|archive-date=|dead-url=|access-date=2017-03-09}}</ref> There, she studied under the mentorship of Harry Triandis, one of the founders of cross-cultural psychology.<ref>{{Cite web|url=http://www.apa.org/international/pi/2013/03/collaborate.aspx|title=Multinational collaborations in culture and psychology|last=Gelfand|first=Michele J.|date=March 2013|website=Psychology International|publisher=American Psychological Association|access-date=2016-06-01}}</ref> She was on the faculty of [[New York University]] from 1995 to 1996, and she has worked at the [[University of Maryland, College Park]] since 1996.<ref name=":0" />

Gelfand is the editor of several [[Academic publishing|academic books]] and series, including ''Advances in Culture and Psychology'', which she co-founded; ''Psychology of Conflict and Conflict Management in Organizations''; and ''The Handbook of Negotiation and Culture''.<ref>{{Cite web|url=http://academicminute.org/2014/11/michele-gelfand-university-of-maryland-american-regionalism/|title=American Regionalism|date=2014-11-04|website=The Academic Minute|publisher=WAMC Northeast Public Radio|access-date=2016-06-01}}</ref> She was the president of the International Association for Conflict Management from 2009 to 2010.<ref name=":0" /><ref>{{Cite web|url=http://www.asfee.fr/?q=node/237|title=24th Annual Conference of the International Association for Conflict Management|website=Association Française d'Economie Expérimentale|access-date=2016-06-01}}</ref>

== Research ==
Gelfand has conducted research across many cultures, using field, experimental, computational, and neuroscientific methods to understand the evolution of cultural differences and their consequences for individuals, teams, organizations, and nations.<ref>{{Cite web|url=http://gelfand.umd.edu/pages/Research.html|title=Research: Culture and Psychology|website=Michele F. Gelfand|publisher=University of Maryland Department of Psychology|access-date=2016-06-02}}</ref><ref>{{Cite journal|last=Gelfand|first=M. J.|last2=Erez|first2=M.|last3=Aycan|first3=Z.|date=2007|title=Cross-cultural organizational behavior|url=http://gelfand.umd.edu/pages/papers/Gelfandetal2007.pdf|journal=Annual Review of Psychology|volume=58|pages=479–514|doi=10.1146/annurev.psych.58.110405.085559|pmid=17044797|access-date=2016-06-06}}</ref> Gelfand has also done work on the role of culture in [[negotiation]] and conflict and the psychology of revenge and forgiveness.<ref>{{Cite journal|last=Gelfand|first=M. J.|last2=Leslie|first2=L.|last3=Keller|first3=K.|last4=De Dreu|first4=C.|date=2012|title=Conflict cultures in organizations: How leaders shape conflict cultures and their organization-level consequences|url=http://gelfand.umd.edu/papers/Gelfand%20et%20al%20(2012)b%20Conflict%20cultures.pdf|journal=Journal of Applied Psychology|volume=97|issue=6|pages=1131|doi=10.1037/a0029993|pmid=23025807|access-date=2016-06-06}}</ref><ref>{{Cite journal|last=Gelfand|first=M. J.|last2=Severance|first2=L.|last3=Lee|first3=T.|last4=Bruss|first4=C. B.|last5=et al.|date=2015|title=Getting to Yes: The linguistic signature of the deal in the U.S. and Egypt|url=|journal=Journal of Organizational Behavior|volume=36|issue=7|pages=967|doi=10.1002/job.2026|pmid=|access-date=}}</ref><ref>{{Cite journal|last=Zheng|first=Xue|last2=Fehr|first2=Ryan|last3=Tai|first3=Kenneth|last4=Narayanan|first4=Jayanth|last5=Gelfand|first5=Michele J.|date=2014|title=The Unburdening Effects of Forgiveness: Effects on Slant Perception and Jumping Height|url=http://www.gelfand.umd.edu/Social%20Psychological%20and%20Personality%20Science-2014-Zheng-1948550614564222.pdf|journal=Social Psychological and Personality Science|publisher=University of California, Davis|doi=|pmid=|access-date=}}</ref>

=== Cultural tightness-looseness ===
Gelfand is considered a pioneering researcher on the concept of tightness-looseness.<ref name=":1" /> She is credited with defining the tightness-looseness classification system, which assesses how much a culture adheres to social norms and tolerates deviance. Tight cultures are more restrictive, with stricter disciplinary measures for norm violations while loose cultures have weaker social norms and a higher tolerance for deviant behavior. Gelfand found that a history of threats, such as natural disasters, high population density, or vulnerability to infectious diseases, is associated with greater tightness. Her research has shown that tightness allows cultures to coordinate more effectively to survive threats.<ref name=":2">{{Cite journal|last=Gelfand|first=Michele J.|last2=Raver|first2=Jana L.|last3=Nishii|first3=Lisa|last4=Leslie|first4=Lisa M.|last5=Lun|first5=Janetta|last6=Lim|first6=Beng Chong|last7=et al|date=2011-05-27|title=Differences Between Tight and Loose Cultures: A 33-Nation Study|url=http://science.sciencemag.org/content/332/6033/1100|journal=Science|language=en|volume=332|issue=6033|pages=1100–1104|doi=10.1126/science.1197754|issn=0036-8075|pmid=21617077|bibcode=2011Sci...332.1100G}}</ref><ref>{{Cite journal|last=Harrington|first=Jesse R.|last2=Gelfand|first2=Michele J.|date=2014|title=Tightness–looseness across the 50 united states|url=http://www.pnas.org/content/111/22/7990.full.pdf|journal=Proceedings of the National Academy of Sciences|doi=|pmid=|access-date=}}</ref>

== Awards and honors ==

*  Carol and Ed Diener Award in Social Psychology, [[Society for Personality and Social Psychology]] (2015)<ref>{{Cite web|url=http://www.spsp.org/annualawards/diener-award-social-psychology|title=Diener Award in Social Psychology, Past Recipients|website=Society for Personality and Social Psychology|access-date=2016-06-03}}</ref>
*  William A. Owens Scholarly Achievement Award, [[Society for Industrial and Organizational Psychology]] (2014)<ref>{{Cite web|url=http://www.apa.org/about/awards/div-14-owens.aspx?tab=4|title=William A. Owens Scholarly Achievement Award, Past Recipients|website=American Psychological Association|access-date=2016-06-03}}</ref>
*  Gordon Allport Intergroup Relations Prize, [[Society for the Psychological Study of Social Issues]] (2012)<ref>{{Cite web|url=https://www.spssi.org/_data/n_0001/resources/live/Previous%20Allport%20Recipients.pdf|title=Gordon Allport Intergroup Relations Prize Winners|last=|first=|date=|website=Society for the Psychological Study of Social Issues|publisher=|access-date=2016-06-03}}</ref>
* Anneliese Maier Research Award, [[Alexander von Humboldt Foundation]] (2011)<ref>{{Cite web|url=http://www.psychologicalscience.org/index.php/publications/observer/obsonline/gelfand-receives-anneliese-maier-research-award.html|title=Gelfand Receives Anneliese Maier Research Award|date=2012-10-04|website=Association for Psychological Science|access-date=2016-06-02}}</ref>
* Best Paper Award for New Directions, [[Academy of Management]] Conflict Management Division (2009)<ref>{{Cite web|url=http://division.aomonline.org/cm/CMD-Awards.htm|title=CMD Sponsored Awards|last=|first=|date=|website=Academy of Management, Conflict Management Division|publisher=|access-date=2016-06-03}}</ref>
* Distinguished University Scholar-Teacher, [[University of Maryland, College Park]] (2009)<ref>{{Cite web|url=https://faculty.umd.edu/awards/list_dst.html|title=Distinguished Scholar Teacher Recipients|website=University of Maryland Office of Faculty Affairs|access-date=2016-06-03}}</ref>
* Outstanding Article of the Year Award, International Association for Conflict Management (2009, 2004, and 2001)<ref>{{Cite web|url=http://www.iacm-conflict.org/Awards/Outstanding_Paper|title=IACM Outstanding Article or Book Chapter Award Recipients|website=International Association of Conflict Management|access-date=2016-06-03}}</ref>
* Cummings Scholarly Achievement Award, [[Academy of Management]] Organizational Behavior Division (2002)<ref>{{Cite web|url=http://obweb.org/index.php/awards/cummings-scholarly-achievement-award/|title=Cummings Scholarly Achievement Award, Past Award Winners|website=Organizational Behavior Division of the Academy of Management|access-date=2016-06-03}}</ref>
* Distinguished Early Career Contribution Award, [[Society for Industrial and Organizational Psychology]] (2002)<ref>{{Cite web|url=http://www.siop.org/awardwinners.aspx#D|title=Past SIOP Award Recipients|website=Society for Industrial and Organizational Psychology|access-date=2016-06-03}}</ref>

== Selected publications ==
* Gelfand, Michele J.; Raver, Jana L.; Nishii, Lisa; Leslie, Lisa M.; Lun, Janetta; et al. (2011). "Differences Between Tight and Loose Cultures: A 33-Nation Study." ''[[Science (journal)|Science]],'' 332, 1100-1104. [[Digital object identifier|doi]]: 10.1126/science.1197754
* Harrington, Jesse R.; Gelfand, Michele J. (2014). "Tightness–looseness across the 50 united states." ''[[Proceedings of the National Academy of Sciences of the United States of America|Proceedings of the National Academy of Sciences]],'' 111, 7990-7995. [[Digital object identifier|doi]]: 10.1073/pnas.1317937111
* Roos, Patrick; Gelfand, Michele J.; Nau, Dana; Lun, Janetta (2015). "Societal threat and cultural variation in the strength of social norms: An evolutionary basis." ''Organizational Behavior and Human Decision Processes'',129, 14-23. [[Digital object identifier|doi:]] 10.1016/j.obhdp.2015.01.003
* Gelfand, Michele J.; Leslie, Lisa M.; Keller, Kirsten; de Dreu, Carsten (2012). "Conflict cultures in organizations: How leaders shape conflict cultures and their organizational-level consequences." ''[[Journal of Applied Psychology]],'' 97, 1131-1147. [[Digital object identifier|doi:]] 10.1037/a0029993
* Gelfand, Michele J.; Severance, Laura; Lee, Tiane; Bruss, C. Bayan; Lun, Janetta; Abdel-Latif, Abdel-Hamid; Moustafa Ahmed, Sally (2015). "Culture and getting to yes: The linguistic signature of creative agreements in the United States and Egypt." ''[[Journal of Organizational Behavior]],'' 36(7), 967-989. [[Digital object identifier|doi:]] 10.1002/job.2026
* Imai, Lynne; Gelfand, Michele J. (2010). "The culturally intelligent negotiator: The impact of cultural intelligence (CQ) on negotiation sequences and outcomes." ''Organizational Behavior and Human Decision Processes,''112, 83-98. [[Digital object identifier|doi:]] 10.1016/j.obhdp.2010.02.001
* Gelfand, Michele J.; Erez, Miriam; Aycan, Zeynep (2007). "Cross-cultural organizational behavior." ''[[Annual Reviews (publisher)|Annual Review of Psychology]],'' 58, 479-514. [[Digital object identifier|doi:]] 10.1146/annurev.psych.58.110405.085559
* Triandis, Harry C.; Gelfand, Michele J. (1998). "Converging measurement of horizontal and vertical individualism and collectivism." ''[[Journal of Personality and Social Psychology]]'', 74, 118-128. [[Digital object identifier|doi:]] 10.1037/0022-3514.74.1.118

== References ==
{{reflist}}

== External links ==

*{{Official website|http://www.gelfand.umd.edu}}

{{authority control}}

{{DEFAULTSORT:Gelfand, Michele J.}}
[[Category:American psychologists]]
[[Category:American women psychologists]]
[[Category:American women writers]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]