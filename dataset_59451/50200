{{No footnotes|date=July 2015}}
[[Image:Loo temple.jpg|thumb|250px|A ruined medieval Sadz church in modern-day Sochi.]]
The '''Sadz''' or '''Asadzwa''', also '''Jigets''', are a subethnic group of the [[Abkhazians]]. They are sometimes purported to have originated from the [[Sanigoi]] tribe mentioned by the Classic authors. In the 6th century, they formed a tribal principality, which later commingled with the [[Abasgoi]], [[Apsilae]] and [[Missimianoi]] into the [[Kingdom of Abkhazia]]. 

Until 1864 Sadz lived at the [[Black Sea]] coast north to [[Gagra]] until the [[Khosta River]] (Khamysh River). They formed the [[Sadzyn]] area, which consisted of the possessions of [[Kamysh]], [[Arydba]], [[Amarshan]] and [[Gechba]] clans, under the hegemony of [[Tsanba]] clan. The [[Ubykh people|Ubykh]] princes [[Oblagua]], [[Chizmaa]] and [[Dziash]] also originated from the Sadz. 

Some think that in the 12-14th centuries a part of the Sadz have been forced to resettle to the [[Abazinia|northern mountainside]] of [[Caucasus Major]] under the Ubykh pressure. They formed there [[Abazin]] people. This is only one of the theories explaining the migration from Abkhazia of the ancestors of what is now the Abaza people. After the [[Russian-Circassian War]] ended in 1864 the most of Sadz were forced to turn [[Muhajir (Caucasus)|muhajirs]], moving to the [[Ottoman Empire]]. Some of them settled in [[Adjara]] (then under the Ottoman possession).

Now the [[Sadz dialect]] of the [[Abkhaz language]] is spoken only in [[Turkey]]. It consists of Akhaltsys and Tswyzhy subdialects.

The Sadz, Aibga and [[Akhchipsou]] tribes of Abkhazia were the last ethnic groups to have offered the resistance to the Russian advances during the [[Caucasus War]]. The last tribes conquered by Russians were [[Ahchypsy]] and [[Aibga]], who lived in and around of what is now [[Krasnaya Polyana]].

==References==
* Анчабадзе З. В. ''Из истории средневековой Абхазии''. Сухум, 1959.
* Анчабадзе П. Д. Абаза. (К этно-культурной истории народов Северо-Западного Кавказа). КЭС, 1984, т.&nbsp;8.
*Анчабадзе Ю. Д., Волкова Н.Г. Этническая история Северного Кавказа XVI-XIX вв. // ''Материалы к серии "Народы и культуры"''. Выпуск XXVIII. «Народы Кавказа». М., 1993. Книга 1.
* Волкова Н. Г. ''Этнонимы и племенные названия Северного Кавказа''. М., 1973.
* Инал-Ипа Ш. Д. Садзы. // ''Материалы к серии «Народы и культуры»''. Выпуск XXVIII. «Народы Кавказа», М., 1995. Книга 2.
* Чирикба, В.А. Расселение абхазов в Турции. Annex to: Инал-Ипа Ш. Д. ''Садзы. // ''Материалы к серии «Народы и культуры»''. Выпуск XXVIII. «Народы Кавказа», М., 1995. Книга 2'', p.&nbsp;260-277.
* Chirikba, V.A. Sadz, an Abkhaz Dialect in Turkey. In: ''Howard A. Aronson (ed.). NSL.8. Linguistic Studies in the Non-Slavic Languages of the Commonwealth of Independent States and the Baltic Republics'', Chicago: The University of Chicago, 1996, p.&nbsp;67-81.
* Chirikba, V.A. Distribution of Abkhaz Dialects in Turkey. In: ''A. Sumru Özsoy (ed.). Proceedings of the Conference on Northwest Caucasian Linguistics, 10–12 October 1994. Studia Caucasologica III. Novus forlag - Oslo, [[Institute for Comparative Research in Human Culture|Instituttet for Sammenlignende Kulturforskning]], 1997,'' p.&nbsp;63-88.
* Chirikba, V.A. ''A Grammar of Sadz Abkhaz'' (Forthcoming).

[[Category:History of ethnic groups in Russia]]
[[Category:Ethnic groups in Turkey]]
[[Category:Abkhaz diaspora]]
[[Category:Sub-ethnic groups]]
[[Category:Abkhaz people]]