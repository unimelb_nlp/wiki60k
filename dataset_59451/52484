{{redirect|CD Plus|the Enhanced Music CD format (also known as CD Plus)|Blue Book (CD standard)}}
'''Ovid Technologies, Inc.''' (or just '''Ovid''' for short), part of the [[Wolters Kluwer]] group of companies, provides access to online [[bibliographic database]]s, [[academic journals]], and other products, chiefly in the area of [[health science]]s. The [[National Library of Medicine]]'s [[MEDLINE]] database was once its chief product but, as this is now freely available through [[PubMed]], Ovid has diversified into a wide range of other databases and other products. Ovid has its global headquarters in [[New York City]].<ref>"[http://www.ovid.com/site/offices.jsp Office Locations]", Ovid Technologies, Inc.</ref>

==History==
Ovid was founded in 1984 by Mark Nelson, who had developed an interface to [[MEDLINE]], the world's largest and oldest medical database, produced by the [[US National Library of Medicine]].  The company at that time was known as Online Research Systems, a name Nelson chose to disguise the fact that he was the only employee of the company, operating out of an apartment in Spanish Harlem, New York City.  The interface was designed to connect over the phone lines to mainframe computers of vendors, primarily [[BRS/Search|BRS Online]], which were running in-house search engines designed for Medline.  However, fearing potential competition, these vendors shut off access for the interface.  Nelson then decided to write his own Medline search engine, one of the first that was based on PCs.<ref name=quint1998>Quint, Barbara. "[http://newsbreaks.infotoday.com/nbreader.asp?ArticleID=17998 Ovid Technologies bought by Wolters Kluwer for $200 million]". ''Information Today'' October 5, 1998</ref> 

Nelson had attempted, unsuccessfully, to obtain funding for the company.  In order to fund operations, Nelson began building computers in his apartment, selling them along with the software. <ref name=quint1998>Quint, Barbara. "[http://newsbreaks.infotoday.com/nbreader.asp?ArticleID=17998 Ovid Technologies bought by Wolters Kluwer for $200 million]". ''Information Today'' October 5, 1998</ref> <ref name=nicsolution>"[http://www.nicsolution.com/partners/ovid/ovid.htm Ovid: A narrative chronology]", NIC Corporation</ref> When the first product was released in 1988, the company changed its name to CD Plus.  The product quickly became successful, fueled by innovations in search engine technology.  Most importantly, Nelson had devised algorithms that encapsulated much of the complexity of Medline's lexicon, enabling end-users to achieve a high level of search precision without the need to master Medline's intricate taxonomy.<ref name=businessweek>"[http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=334364 Ovid Technologies, Inc.]", BusinessWeek</ref> The company's first [[Microsoft Windows]] interface to MEDLINE was named Ovid and released in 1992.  Nelson, who had majored in English Literature and minored in classical languages, chose the name Ovid as a homage to the ancient Roman poet's most famous work, Metamorphoses.  Several years later, Nelson started the [[Alpheios Project]], non-profit software to facilitate the reading of ancient Greek and Latin. <ref name=nicsolution/>

The company's primary competitor continued to be [[BRS/Search|BRS Online]]. CD-Plus gained significant market share from its competitor, and in 1994 acquired the company.<ref name=nicsolution/><ref name=quint1998/> Shortly after, CD-Plus went public at $6/share, and listed on [[NASDAQ]].<ref name=nicsolution/> It then changed to its present name in 1995, reflecting the importance of its Ovid product.<ref name=nicsolution/>

In 1998, Nelson, who still retained the majority of Ovid shares, was wary of the market bubble that had been building for several years. He engaged Goldman Sachs to sell the company. <ref name=quint1998>Quint, Barbara. "[http://newsbreaks.infotoday.com/nbreader.asp?ArticleID=17998 Ovid Technologies bought by Wolters Kluwer for $200 million]". ''Information Today'' October 5, 1998</ref> Wolters Kluwer acquired the company in October, 1998, for $24.59/share. 

Ovid continued to make inroads against its competitors.  In 2001, Wolters Kluwer purchased the rival [[SilverPlatter]] company and merged it into Ovid during 2001 and early 2002.<ref>"[http://www.ovid.com/site/about/history.jsp?top=42&mid=43 Company history]", Ovid Technologies, Inc.</ref>

Ovid introduced a new database search interface called OvidSP in 2007. This has replaced the Ovid Gateway interface, which was retired in February 2008, and the SilverPlatter interface, which was retired in January 2009.

==References==
{{reflist}}

==External links==
* [http://www.ovid.com Ovid website]
* [http://www.ovid.com/site/products/tools/ovid/ovidsp_access.jsp OvidSP page] at Ovid website
* [http://www.iwr.co.uk/2216015 Wolters Kluwer Health - OvidSP] — a review of OvidSP by Davey Winder in ''Information World Review''

[[Category:Bibliographic database providers]]
[[Category:Bioinformatics]]
[[Category:Educational publishing companies]]
[[Category:Health sciences]]
[[Category:Academic publishing companies]]