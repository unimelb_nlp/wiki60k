{{Orphan|date=September 2016}}

{{Infobox journal
| title = International Journal of Energy Research
| cover = [[File:International Journal of Energy Research.gif]]
| former_name = 
| abbreviation = Int. J. Energy Res.
| discipline = Energy
| editor = Ibrahim Dincer
| publisher = [[John Wiley & Sons]]
| country = 
| history = 1977–present
| frequency = Monthly
| openaccess = [[Hybrid open access journal|Hybrid]]
| license = 
| impact = 2.737
| impact-year = 2013
| ISSN = 0363-907X
| eISSN = 1099-114X
| CODEN = IJERDN
| JSTOR = 
| LCCN = 92640055
| OCLC = 42621034
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-114X
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-114X/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-114X/issues
| link2-name = Online archive
}}
'''''International Journal of Energy Research''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by [[John Wiley & Sons]]. Its scope includes [[fossil fuel|fossil]], [[nuclear power|nuclear]], and [[renewable energy|renewable]] energy sources, and research into [[energy storage]]. It was established in 1997 and the [[editor-in-chief]] is Ibrahim Dincer ([[University of Ontario Institute of Technology]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Chemical Abstracts Service]]<ref name=cassi>[http://cassi.cas.org/search.jsp CAS Source Index (CASSI) Search Tool]{{dead link|date=April 2017 |bot=InternetArchiveBot |fix-attempted=yes }}. ISSN Search: 0363-907X. Retrieved on 2014-12-27.</ref>
* [[Academic Search]]
* [[Aquatic Sciences & Fisheries Abstracts]]
* [[COMPENDEX]]
* [[Current Contents]]/Engineering, Computing & Technology<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-27}}</ref>
* [[ProQuest|ProQuest databases]]
* [[GeoRef]]
* [[Inspec]]
* [[METADEX]]
* [[PASCAL (database)|PASCAL]]
* [[Science Citation Index Expanded]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=[http://www.elsevier.com/online-tools/scopus/content-overview Scopus coverage lists] |accessdate=2014-12-27}}</ref>
* [[VINITI Database RAS]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.737.<ref name=WoS>{{cite book |year=2014 |chapter=International Journal of Energy Research |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-114X}}

[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1977]]
[[Category:Energy and fuel journals]]
[[Category:John Wiley & Sons academic journals]]


{{sci-journal-stub}}