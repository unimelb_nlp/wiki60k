{{Infobox writer <!-- For more information see [[:Template:Infobox Writer/doc]]. --> 
| name          = Professor Norma Reid Birley 
| image         = Norma Reid Birley.jpg|thumb|Norma Reid Birley
| image_size    = 
| alt           = photograph of Professor Lady Norma Reid Birley
| caption       =  
| pseudonym     = 
| birth_name    = 
| birth_date    = {{Birth date and age|1952|04|24}}
| birth_place   = Limavady Northern Ireland
| death_date    = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place   = 
| resting_place = 
| occupation    = ex-vice-Chancellor, Witwaterstrand University<br /> Visiting Professor, Univ of Ulster, Sussex Univ
| language      = 
| nationality   = Irish
| ethnicity     = 
| citizenship   = Irish
| education     = University of Ulster, Sussex University, Plymouth University
| alma_mater    = 
| notableworks  = 
| spouse        = [[Derek Birley|Sir Derek Birley]] 
| signature_alt = 
| website       = <!--http://www.normareidbirley.com-->
| portaldisp    = 
}}

'''Norma Glasgo Reid Birley''' (born 28 April 1952 in Limavady, Northern Ireland) was  [[Vice-Chancellor]] and Principal of the [[University of the Witwatersrand]].<ref name=wits>[http://www.wits.ac.za University of Witsaterstrand]</ref> A statistician, and health service researcher, she has also held the positions of director of an academic consultancy firm,   Associate Professor of the [[University of Ulster]], and an Honorary Professor of [[Sussex University]]. She was appointed to Witwatersrand in 2000, following a 25-year career as a researcher, teacher and senior administrator in higher education.

== Education ==
Birley was educated at [[Limavady Grammar School]], in [[Northern Ireland]], and obtained a B.Sc. and a M.Sc. in mathematics at [[Sussex University]], graduating in 1974. She was awarded a D. Phil by the [[University of Ulster]] in 1983 and  was made an honorary Doctor of Science [[Sussex University]] in 2002.

== Early Appointments and Research ==
In 1974, Birley was appointed to the DHSS Health Services Research Unit at the [[University of Newcastle-upon-Tyne]]. In 1977 she was appointed lecturer in statistics at the [[London School of Economics]]. She became interested in pedagogy as a result of being asked to teach a Master’s class of sociologists, in finding new graphical ways to communicate her subject to a reluctant audience. In 1978 she  returned to Ireland, to lead an interdisciplinary study of nurse education in the clinical setting at the [[University of Ulster]]. In 1984, she became founding Director of that university's Research Centre for Applied Health Studies, and in 1986 she was promoted to a senior lectureship in mathematics. While there, she published the first evidence of the huge exodus of well-qualified school leavers from Ulster in those years.<ref>Cormack, R, Osborne, R, Reid, N G and Williamson, A. (1982) Political Arithmetic, Higher Education and Religion in Northern Ireland in ‘Religion, Education and Employment: Aspects of equal opportunity in Northern Ireland’, eds. Cormack R and Osborne, R, Appletree Press, Belfast. Cormack, R, Osborne, R, Reid, N G and Williamson, A (1981) A Continuing Haemorrhage: the other Irish Question, Times Higher Education Supplement, 20.1.81.</ref>

She was awarded a D Phil by the [[University of Ulster]] in 1983; her thesis was a statistical investigation of the relationship between the quality of nurse education in the clinical setting, and the historically used apprenticeship model of nurse education. She sat on the Royal College of Nursing’s Commission on Nursing Education (1985), and wrote a chapter in their report on nurse education.<ref>Reid, N G (1985) ''The Education of Nurses: A New Dispensation,'' Royal College of Nursing of the United Kingdom, author of chapter six and technical annexe</ref>

In 1988 she was appointed to [[Coventry University]] as Head of Department and Professor of Health Sciences in the first interdisciplinary Department of Health Sciences in the UK, comprising physiotherapy, occupational therapy and nursing. She was promoted to Dean of the Faculty of Social, Biological and Health Sciences in 1991.

During these years, she published three books, two of which became standard text books in research methods for nurses, and continued to sell well over two decades.<ref>Reid, N.G. and Boore, J. (1987) ''Research Methods and Statistics in Health Care,'' Edward Arnold, London, pp132</ref><ref>Reid, N.G. (1993) ''Health Care Research by Degrees:'' Blackwell Scientific Publications</ref>   She also published some 70 articles in refereed journals.

== Educational management ==
Birley was appointed Pro-Vice-Chancellor of [[Coventry University]] in 1993, with responsibility for academic development. She was appointed as a Quality Auditor of the national Quality Assurance Agency, in 1996, Deputy Vice-Chancellor at [[Plymouth University]], with responsibility for finance, estates, IT and human resources, and in 2000, Deputy Vice-Chancellor for Corporate Development. In these years, she played a leading role in the establishment of the [[Peninsula Medical School]]. {{citation needed |date=September 2013}}

In 2001, Birley was appointed as Vice-Chancellor and Principal of the [[University of the Witwatersrand]] in [[Johannesburg, South Africa]], the first woman and only the fourth foreigner to hold the position in its 100-year history. In post-1994 South Africa, her appointment was controversial as a female white foreigner – especially in a university  which had a turbulent history of internal and external political friction—her predecessor had resigned very early in his appointment, as had two previous Deputy Vice-chancellors.<ref>Business Day, 04/12/02, editorial, ‘South  Africa: Wits Vice-Chancellor Was Progressively Driven Out’</ref>

During her time there, student numbers and external funding reached record   levels. She led the establishment of a cultural precinct and raised £2.5 million to found a Rock Art Museum, which was opened as the Origins Centre <ref>[origins.mysouthwest.com.au]</ref> in 2006. After the death of her husband, Sir Derek Birley, in May 2002, she faced renewed challenges to her leadership, led by the Chair of the University Council, and resigned in November 2002. She sued the university for libel, receiving an out-of-court settlement of R1.1 million in 2004.<ref>http://www.sundaytimes.co.za, 7 Dec 2004</ref>

In 2003-2004, Birley led the development of an econometric/statistical model  for the South Africa Department of Labour to predict the financial and human resources impact of HIV/AIDS on major South African companies,.

On her return to the UK, she became Director of the Institute for Research in Health and Human Sciences at [[Thames Valley University]], and subsequently, its Director for Postgraduate Development. Since then she has been director of NRB Consulting, a consultancy company in health and social services.

== References ==
{{Reflist}}

== External links ==
*[http://normareidbirley.com www.normareidbirley.com]

{{Authority control}}

{{DEFAULTSORT:Birley, Norma Reid}}
[[Category:Irish women academics]]
[[Category:1952 births]]
[[Category:Living people]]
[[Category:People from Limavady]]
[[Category:People educated at Limavady Grammar School]]
[[Category:Alumni of the University of Sussex]]
[[Category:Alumni of Ulster University]]
[[Category:Academics of Newcastle University]]
[[Category:Academics of the London School of Economics]]
[[Category:Academics of Coventry University]]
[[Category:University of the Witwatersrand academics]]
[[Category:Academics of the University of Plymouth]]
[[Category:Academics of the University of West London]]