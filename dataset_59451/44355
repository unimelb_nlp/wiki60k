{{EngvarB|date=August 2015}}
{{Use dmy dates|date=May 2014}}
{{refimprove|date=February 2014}}
{{Infobox airport
| caption      = 
| image2-width =
| location     = [[Rajasansi]], [[Amritsar District|Amritsar]], India
| elevation-m  = 230
| coordinates  = {{coord|31|42|28|N|074|47|57|E|display=inline,title}}
| pushpin_map = India Punjab#India
| pushpin_label = '''ATQ'''
| r1-length-f  = 12,001
| r1-surface   = [[Asphalt]]
| name         = Sri Guru Ram Dass Jee International Airport
| image        = Amritsar Airport Entrance.jpg
| image-width  = 250
| image2       =
| caption2     =
| IATA         = ATQ
| ICAO         = VIAR
| type         = Public
| owner        =
| operator     = [[Airports Authority of India]]
| city-served  = [[Amritsar]], India
| elevation-f  = 756
| website      = [http://www.aai.aero/allAirports/amritsar_generalinfo.jsp www.aai.aero/]
| r1-number    = 16/34
| r1-length-m  = 3,658
|stat-year= 2016
|stat1-header= Passenger movements
|stat1-data= 1,501,184 {{increase}}28.6%
|stat2-header= Aircraft movements 
|stat2-data= 11,187 {{increase}}22.5%
|stat3-header= Cargo tonnage
|stat3-data= 1,187
| footnotes = Source: [[Airport Authority of India|AAI]],<ref name="201617passengers">{{cite report |url=http://www.aai.aero/traffic_news/Jan2k16annex3.pdf |title=Traffic News for the month of January 2016: Annexure III |page=3 |date=9 March 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=January 2016: 121,193 passengers; January 2015: 84,719 passengers}}
*{{cite report |url=http://www.aai.aero/traffic_news/Feb2k16annex3.pdf |title=Traffic News for the month of February 2016: Annexure III |page=3 |date=8 April 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=February 2016:  121,395 passengers; February 2015: 91,349 passengers}}
*{{cite report |url=http://www.aai.aero/traffic_news/Mar2k16annex3.pdf |title=Traffic News for the month of March 2016: Annexure III |page=3 |date=22 May 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=March 2016:  120,241 passengers; March 2015: 103,573 passengers}}
*{{cite report |url=http://www.aai.aero/traffic_news/Dec2k16annex3.pdf |title=Traffic News for the month of December 2016: Annexure III |page=4 |date=30 January 2017 |website=Airports Authority of India |access-date=1 February 2017 |quote=April–December 2016: 1,138,355 passengers; April–December 2015: 887,541 passengers}}</ref>
<ref name="201617move">{{cite report |url=http://www.aai.aero/traffic_news/Jan2k16annex2.pdf |title=Traffic News for the month of January 2016: Annexure II |page=3 |date=9 March 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=January 2016: 864 aircraft movements; January 2015: 644 aircraft movements}}
*{{cite report |url=http://www.aai.aero/traffic_news/Feb2k16annex2.pdf |title=Traffic News for the month of February 2016: Annexure II |page=3 |date=8 April 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=February 2016: 847 aircraft movements; February 2015: 647 aircraft movements}}
*{{cite report |url=http://www.aai.aero/traffic_news/Mar2k16annex2.pdf |title=Traffic News for the month of March 2016: Annexure II |page=3 |date=22 May 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=March 2016: 882 aircraft movements; March 2015: 742 aircraft movements}}
*{{cite report |url=http://www.aai.aero/traffic_news/Dec2k16annex2.pdf |title=Traffic News for the month of December 2016: Annexure II |page=3 |date=30 January 2017 |website=Airports Authority of India |access-date=1 February 2017 |quote=April–December 2016: 8,594 aircraft movements; April–December 2015: 7,102 aircraft movements}}</ref>
<ref>{{cite report |url=http://www.aai.aero/traffic_news/Jan2k16annex4.pdf |title=Traffic News for the month of January 2016: Annexure IV |page=3 |date=9 March 2016 |work=Airports Authority of India |access-date=4 April 2016 |quote=January 2016: 41 tonnes}}
*{{cite report |url=http://www.aai.aero/traffic_news/Feb2k16annex4.pdf |title=Traffic News for the month of February 2016: Annexure IV |page=3 |date=8 April 2015 |work=Airports Authority of India |access-date=4 April 2016 |quote=February 2016: 77 tonnes}}
*{{cite report |url=http://www.aai.aero/traffic_news/Mar2k16annex4.pdf |title=Traffic News for the month of March 2016: Annexure IV |page=3 |date=22 May 2015 |work=Airports Authority of India |access-date=4 April 2016 |quote=March 2016: 84 tonnes}}
*{{cite report |url=http://www.aai.aero/traffic_news/Dec2k16annex4.pdf |title=Traffic News for the month of December 2016: Annexure IV |page=4 |date=30 January 2017 |work=Airports Authority of India |access-date=1 February 2017 |quote=April–December 2016: 985 tonnes}}</ref>
}}

'''Sri Guru Ram Das Jee International Airport''' {{airport codes|ATQ|VIAR}} named after [[Guru Ram Das]], the fourth Sikh Guru and the founder of Amritsar city, is an [[international airport]] about {{convert|11|km|mi|0}} northwest of the city of [[Amritsar]], India. It is located on the Amritsar-Ajnala Road, near the village of Raja Sansi. Besides [[Amritsar]], the airport serves neighboring areas of [[Punjab, India|Punjab]], Western districts of [[Himachal Pradesh]] and Southern districts of [[Jammu and Kashmir]]. The new integrated terminal has doubled the capacity of the old terminal. There are 15 domestic and 7 international flights from this airport.

==Facilities==

===Terminal===
The arrivals section of the old terminal was inaugurated in September 2005, and the departures section was made operational in March 2006. A new integrated terminal building, built in glass and steel and equipped with modern facilities like an inline X-ray baggage inspection and conveyor system, Flight Information Display System (FIDS), Common Use Terminal Equipment (CUTE), and CCTV for surveillance among others, was inaugurated on 25 February 2009, with an area of approximately {{convert|40175|m2}} marking an improvement over the earlier {{convert|12770|m2}} facility. The new terminal building is a blend of modern and Indian designs, constructed in glass and steel with Indian style arches and colours. For the quarter ending June 30, 2016, the airport registered a 59.6% growth of international passenger traffic.<ref>{{cite web|last=Khare |first=Harish |url=http://www.tribuneindia.com/news/amritsar/59-6-increase-in-passenger-footfall-at-amritsar-airport/287470.html |title=59.6% increase in passenger footfall at Amritsar airport |publisher=Tribuneindia.com |date=2016-08-30 |accessdate=2017-03-12}}</ref>

The integrated terminal building has four aero bridges, an annual capacity of 1.46 million passengers with a peak hour capacity of 1,200 passengers. The building has 30 check-in counters, 4 X-ray scanners (for baggage), 26 immigration Counters, 10 custom counters, 12 security check booths, and 4 conveyor belts for arrivals. The apron has been extended to cater for parking of a total of 14 aircraft (8 Category 'E’, 3 Category 'D’ and 3 Category 'C’ types of aircraft) from the earlier capacity of ten aircraft and strengthened for parking of Category 'E’ type of aircraft. The departure and arrival halls operate duty-free shops. The departure hall also accommodates foreign currency exchange service, a book store, restaurants and other shops for the convenience of departing passengers.<ref>Press Release By PIB http://pib.nic.in/release/release.asp?relid=47938</ref>

===Runway===
CAT II ILS became operational on 23 December 2011 and reduced the visibility requirement for an aircraft landing at Amritsar Airport on Runway 34 from the existing 650 metres to 350 metres benefiting airlines in terms of increased safety and avoiding diversions to other airports resulting in better operational and environmental efficiency.<ref>{{cite web|url=http://www.aai.aero/misc/AAI-upadteMArch12.pdf |title=Amritsar Airport ushers in CAT II Instrument Landing System |format=PDF |date= |accessdate=2017-03-12}}</ref><ref>{{cite web|title = Airports Authority of India|url = http://www.aai.aero/allAirports/amritsar_technicalinfo.jsp|website = www.aai.aero|accessdate = 2015-12-31}}</ref> The runway is currently going through repavement and upgrade of its strength that will be completed on April 30, 2017 thereby resuming 24 x 7 operations. CAT III installation will be completed in Sep 2017.

==Airlines and destinations==
[[File:Turkmenistan Airlines EZ-A012 at Amritsar, July 2015.jpg|thumb|A [[Turkmenistan Airlines]] [[Boeing 757-200]] on the apron]]
{{Airport destination list
|[[Air India]]| [[Birmingham Airport|Birmingham]], [[Indira Gandhi International Airport|Delhi]]
<!-- -->
|[[Air India Express]]|[[Dubai International Airport|Dubai-International]]
<!-- -->
|[[IndiGo]]|[[Indira Gandhi International Airport|Delhi]], [[Chhatrapati Shivaji International Airport|Mumbai]], [[Satwari Airport|Jammu]] (begins 1 May 2017) <ref>https://www.goindigo.in/information/new-flights.html</ref>
<!-- -->
|[[Jet Airways]]|[[Indira Gandhi International Airport|Delhi]]
<!-- -->
|[[Malindo Air]]|[[Kuala Lumpur International Airport|Kuala Lumpur]]
<!-- -->
|[[Qatar Airways]]|[[Hamad International Airport|Doha]]
<!-- -->
|[[Scoot]]|[[Singapore Changi Airport|Singapore]]

|[[SpiceJet]]| [[Indira Gandhi International Airport|Delhi]], [[Dubai International Airport|Dubai-International]], [[Chhatrapati Shivaji Maharaj International Airport|Mumbai]], [[Srinagar Airport|Srinagar]]
|[[Turkmenistan Airlines]]|[[Ashgabat International Airport|Ashgabat]]
<!-- -->
|[[Uzbekistan Airways]]|[[Tashkent International Airport|Tashkent]]
<!-- -->
|[[Vistara]]|[[Indira Gandhi International Airport|Delhi]], [[Chhatrapati Shivaji Maharaj International Airport|Mumbai]]
}}

==References==
{{Reflist}}

==External links==
{{Commons-inline}}

<!--Templates-->
{{Airports in India}}

[[Category:Transport in Punjab, India]]
[[Category:Transport in Amritsar]]
[[Category:Airports in Punjab, India]]