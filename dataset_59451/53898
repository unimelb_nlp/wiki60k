'''Room acoustics''' describes how [[sound]] behaves in an enclosed space.

The way that sound behaves in a room can be broken up into roughly four different [[frequency]] zones:
*The first zone is below the frequency that has a wavelength of twice the longest length of the room. In this zone, sound behaves very much like changes in static air pressure.
*Above that zone, until the frequency is approximately 11,250([[RT60]]/V)<sup>1/2</sup> (when Volume is measured in cubic feet and 2000([[RT60]]/V)<sup>1/2</sup> when Volume is measured in cubic metres),<ref>"Sound System Engineering" 4th edition, Don Davis, Eugene Patronis, Pat Brown, June 2013, page 215</ref> [[wavelength]]s are comparable to the dimensions of the room, and so room [[resonance]]s dominate. This transition frequency is popularly known as the [[Manfred R. Schroeder|Schroder]] frequency, or the cross-over frequency and it differentiates the low frequencies which creates standing waves within small rooms from the mid and high frequencies.<ref>"Handbook of Noise and Vibration Control", Malcolm J. Crocker, 2007, page 54</ref>
*The third region which extends approximately 2 [[octave]]s is a transition to the fourth zone.
*In the fourth zone, sounds behave like rays of light bouncing around the room.

==Natural modes==
{{see also|Normal mode}}
The sound wave has reflections at the walls, floor and ceiling of the room. The incident wave then has interference with the reflected one. This action creates [[standing wave]]s that generate nodes and high pressure zones.<ref>“Acoustics”, Leo Beranek, chapter 10,  McGraw Hill Books, 1954</ref>

In 1981, in order to solve this problem, Oscar Bonello, professor at the University of Buenos Aires, formulated a modal density concept solution which used concepts from [[psychoacoustics]].<ref>"A NEW CRITERION FOR THE DISTRIBUTION OF NORMAL ROOM MODES" - Journal of the Audio Engineering Society (USA) Vol. 29, Nro. 9 - September/1981. Oscar Bonello</ref> Called "Bonello Criteria", the method analyzes the first 48 room modes and plots the number of modes in each one-third of an octave. The curve increases monotonically (each one-third of an octave must have more modes than the preceding one).<ref>''Handbook for Sound Engineers'' Glen Ballou, Howards Sams Editors, page 56.</ref> Other systems to determine correct room ratios have more recently been developed <ref>Cox, TJ, D'Antonio, P and Avis, MR 2004, "[http://www.acoustics.salford.ac.uk/acoustics_info/room_sizing/ Room sizing and optimization at low frequencies]." , Journal of the Audio Engineering Society, 52 (6) , pp. 640-651.</ref>

==Reverberation of the room==

After determining the best dimensions of the room, using the modal density criteria, the next step is to find the correct [[reverberation|reverberation time]].  The most appropriate reverberation time depends on the use of the room. Times about 1.5 to 2 seconds are needed for opera theaters and concert halls. For broadcasting and [[recording studio]]s and conference rooms, values under one second are frequently used. The recommended reverberation time is always a function of the volume of the room. Several authors give their recommendations <ref>“Acoustics”, Leo Beranek, chapter 13,  McGraw Hill Books, 1954</ref> A good approximation for Broadcasting  Studios and Conference Rooms  is:  TR[1&nbsp;kHz] = [0,4 log (V+62)] – 0,38   TR in seconds and V=volume of the room in m<sup>3</sup>  <ref>“Clases de Acústica”, Oscar Bonello, Edited CEI, Facultad de Ingeniería UBA</ref> The ideal RT60 must have the same value at all frequencies from 30 to 12,000&nbsp;Hz.  Or, at least, it is acceptable to have a linear rising from 100% at 500&nbsp;Hz to 150% down to 62&nbsp;Hz

To get the desired RT60, several acoustics materials can be used as described in several books.<ref>{{cite book|last=Rettinger|first=Michael|title=Acoustic Design and Noise Control|year=1977|publisher=Chemical Publishing|location=New York}}</ref><ref>{{cite book|last=Knudsen|first=Vern Oliver|title=Acoustical designing in Architecture|publisher=John Wiley and Sons|location=New York|authorlink=Vern Oliver Knudsen|author2=[[Cyril M. Harris]] }}</ref>  A valuable simplification of the task was proposed by Oscar Bonello in 1979 <ref>“A new computer aided method for the complete acoustical design of broadcasting and recording studios”, Oscar Bonello, 1979 IEEE [[International Conference on Acoustics, Speech and Signal Processing]], Washington</ref>  It consists of using standard acoustic panels of 1 m<sup>2</sup> hung from the walls of the room (only if the panels are parallel). These panels use a combination of  three  [[Helmholtz resonator]]s and a wooden resonant panel. This system gives a large acoustic absorption at low frequencies (under 500&nbsp;Hz) and reduces at high frequencies to compensate for the typical absorption by people, lateral surfaces, ceilings, etc.

==See also==
*[[Acoustics]]
*[[Acoustic board]]
*[[Architectural acoustics]]
*[[Anechoic room]]
*[[Digital room correction]]
*[[Reverberation]]
*[[Whispering gallery]]
*[[Acoustic space]]

==Compare==
*[[Noise control]]
*[[Sound proofing]]

==References==
{{reflist}}


{{Authority control}}
[[Category:Acoustics]]
[[Category:Building engineering]]