{{infobox organization
| name          = Lancashire Parish Register Society
| image         = 
| image_border  =
| size          = 150
| map           =
| msize         =
| mcaption      =
| alt           = <!-- alt text; see [[WP:ALT]] -->
| caption       =
| formation     = [[26 November]] [[1897]]
| type          = [[Historical Society]]
| status        = [[charitable organization|Charity]]
| registration_id = 511396
| purpose       = Historical Study and Research
| headquarters  = [[Manchester]], [[United Kingdom]]
| location      = [[Manchester Central Library]]
| region_served = [[Lancashire]] and [[Greater Manchester]]
| language      = English
| leader_title = Activities
| leader_name = Research & Publications
| leader_title2 = Collections
| leader_name2 = Library, Archives
| leader_title3  = 9th President
| leader_name3   = [[Thomas Woodcock (officer of arms)|Thomas Woodcock]]
| website       = [http://www.lprs.org.uk www.lprs.org.uk]
}}

The '''Lancashire Parish Register Society''' is a [[historical society]] and [[registered charity]]. It was founded for the 'purpose of printing the registers of the ancient parishes' in [[Lancashire]]. The society was formed at a meeting at [[Chetham's Library]] on [[26 November]] [[1897]], but the year [[1898]] was fixed as the first year of the society's existence. The society became a [[registered charity]] (No. 511396) in [[1981]].<ref>{{Cite web|url=http://apps.charitycommission.gov.uk/Showcharity/RegisterOfCharities/CharityWithoutPartB.aspx?RegisteredCharityNumber=511396&SubsidiaryNumber=0 |title=Charity Commission of England and Wales |publisher=Charity Commission of England and Wales |date=2015-12-11 |accessdate=2015-12-11}}</ref>

==Publications==
Since [[1898]], the society has published 175 printed volumes of Lancashire's [[parish registers]], as well as numerous CDs and CD-Roms.<ref>''Lancashire Parishes for the Genealogist'' (Lancashire Parish Register Society, 2007), s. 1. ISBN 1-85445-157-X.</ref> The society published its 175th volume in 2012.<ref>''The Registers of the Parish of Wigan, 1711-40'', ed. K. T. Taylor, Lancashire Parish Register Society, 175 (2012). ISBN 978-1-85445-171-2.</ref>

==Membership==
Membership is open to all individuals and institutions interested in the parish registers of the county.

==Presidents==
{{columns-list|3|
* 1897–1914 Lt-Col. Henry Fishwick
* 1920–38 Col. John William Robinson Parker
* 1938–46 Prof. [[Ernest Fraser Jacob]]
* 1946–55 Prof. [[C. R. Cheney|Christopher Robert Cheney]]
* 1955–62  Prof. [[J. M. Wallace-Hadrill| John Michael Wallace-Hadrill]]
* 1962–84 Prof. John Smith Roskell
* 1984–99 Prof. Jeffrey Howard Denton
* 1999–2003 The Worshipful John Leslie Ogilvie Holden
* 2004–present [[Thomas Woodcock (officer of arms)|Thomas Woodcock]]
}}

==General Editors==
{{columns-list|3|
* 1976–2008 Dr C. D. Rogers
* 2008–present A. A. Kenwright
}}

==Secretaries==
{{columns-list|3|
* 1897–98 Rev. W. Löwenberg
* 1898–1931 Dr Henry Brierley
* 1931–56 Archibald Sparke
* 1941–82 Dr Frank Taylor
* 1982–86 Jean M. Ayton
* 1986–89 Irene Foster
* 1989–93 R. N. Hudson
* 1993–99 The Worshipful John Leslie Ogilvie Holden
* 1999–2010 M. Mason
* 2010–present R. N. Hudson

}}
==Treasurers==
{{columns-list|3|
* 1897–1936 John Rigg Faithwaite
* 1936–48 Dr Ernest Bosdin Leech
* 1948–77 Dr Robert Dickinson
* 1971–77 Florence Dickinson
* 1977–95 J. R. Bulmer
* 1995–2005 T. O’Brien
* 2005–08 A. A. Kenwright
* 2008–present J. Roberts
}}

== See also ==
*[[Chetham Society]]
*[[Historic Society of Lancashire and Cheshire]]
*[[Lancashire and Cheshire Antiquarian Society]]
*[[Record Society of Lancashire and Cheshire]]

== External links ==
* [http://www.lprs.org.uk/ Lancashire Parish Register Society] 2017-01-26. Retrieved 2017-01-26.

== Notes and References ==

{{Reflist|colwidth=30em}}

{{DEFAULTSORT:List Of Historical Societies}}
[[Category:Learned societies of the United Kingdom]]
[[Category:History organisations based in the United Kingdom]]