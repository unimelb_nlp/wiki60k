{{COI|date=June 2015}}
{{Infobox Officeholder
| name                = Miriam Coronel-Ferrer 
| image               = 
| imagesize           = 
| smallimage          =
| caption             =

| order               = 
| president           = 
| office              = 
| term_start          = 
| term_end            =
| succeeding          = 
| predecessor         = 
| successor           =

| birth_date          = 
| birth_place         = 
| death_date          =
| death_place         =
| nationality         = Filipino
| spouse              = 
| relations           =
| children            =
| residence           = 
| alma_mater          = [[University of the Philippines]]
| occupation          = 
| profession          = Lawyer
| party               = [[Aksyon Demokratiko|Aksyon]]
| otherparty          = [[Liberal Party (Philippines)|Liberal]]
| religion            = [[Roman Catholic]]
| signature           =
| website             = 
| footnotes           =
}}

'''Prof. Miriam Coronel-Ferrer''' is a [[Filipino people|Filipino]] peace negotiator and the current chairperson of the peace panel of the [[Government of the Philippines]] to the [[Moro Islamic Liberation Front]].<ref>http://www.gmanetwork.com/news/story/285474/news/nation/up-professor-miriam-coronel-ferrer-is-new-head-of-govt-peace-panel-for-milf-talks</ref>  She is the first female chief negotiator in the world to sign a final peace accord with a rebel group<ref>http://www.rappler.com/nation/90902-miriam-coronel-ferrer-hillary-clinton-award?utm_content=bufferb1c86&utm_medium=social&utm_source=facebook.com&utm_campaign=buffer</ref>

She is also a political science professor of the [[University of the Philippines]].

== Education and Career==

Coronel-Ferrer graduated cum laude from the University of the Philippines Diliman with a degree in philosophy in 1980. She also possesses a master's degree in Southeast Asian Studies from the University of Kent at Canterbury.<ref>http://lifestyle.inquirer.net/149766/sister-act-coronel-girls-in-the-limelight</ref>

In the early 2000s, she served as the director of the UP Third World Studies Center and was a convenor of the Program on Peace, Democratization, and Human Rights of the UP Center for Integrative and Development Studies until 2005.<ref>http://www.up.edu.ph/up-prof-miriam-coronel-ferrer-is-govts-new-peace-panel-chief/</ref>

Before joining the government peace panel, Coronel-Ferrer was already involved in a number of campaigns.  She was founding co-chair of the Non-State Actors Working Group of the 1997 Nobel Peace Prize winner International Campaign to Ban Landmines, and served in this capacity from 1999-2004. In 2005, she joined 26 other Filipinas nominated among the "1,000 Women for the Nobel" peace prize in 2005.   She co-led the civil society-initiated drafting of the National Action Plan (NAP) on UN Security Council Resolution 1325. The Philippine NAP was formally adopted by the government in March 2010.

Appointed by President Benigno Simeon Aquino III to the Government Negotiating Panel in July 2010, she was assigned to her current post as Panel Chair on 7 December 2012, replacing Associate Justice Marvic Leonen.<ref>http://www.gmanetwork.com/news/story/285474/news/nation/up-professor-miriam-coronel-ferrer-is-new-head-of-govt-peace-panel-for-milf-talks</ref><ref>http://www.rappler.com/nation/17504-miriam-coronel-ferrer-is-the-new-gov-t-peace-panel-chair</ref>
  
Prof. Ferrer has published several books and journal articles on Philippine democratization, civil society, human rights and peace processes, and served as visiting professor in Hankuk University in Seoul, Hiroshima University in Japan, and Gadja Madah University in Yogjakarta, Indonesia.

Recent awards received by Coronel-Ferrer include the 2015 Hillary Rodham Clinton Award for Advancing Women in Peace and Security,  the 2015 Xavier University-Ateneo de Cagayan  Fr William F Masterson SJ Award, the 2014 United Nations Development Program N-Peace Award for Campaigning for Action, and  the 2011 Philippine Science High School Alumni Association Gawad Lagabalab for Outstanding Alumni.

As the chair of the government panel, Prof. Ferrer currently oversees the implementation of the Comprehensive Agreement on the Bangsamoro.<ref>http://www.opapp.gov.ph</ref>

==Mamasapano==
In the aftermath of the [[Mamasapano clash]], a bloody police operation which successfully managed to eliminate Malaysian terrorist [[Zulkifli Abdhir]] alias Marwan, but led to the killing of 44 police commandos by Moro rebels, Coronel-Ferrer was involved in a tense exchange with Senator [[Alan Peter Cayetano]]. Coronel-Ferrer argued that "The government policy is to negotiate with the armed groups that are fighting the government." The Senator responded by saying, "All governments around the world, the policy is not to negotiate with terrorists. If the peace panel does not know that, we’re in trouble." To which Coronel-Ferrer replied that she was "not aware of any such policy."<ref>https://anc.yahoo.com/news/cayetano-in-heated-exchange-with-ferrer-at-senate-hearing-225942216.html</ref>

Coronel-Ferrer's calls for calm in the wake of the clash have drawn criticism from many Filipino netizens. Many have accused her of treason and of rushing the peace process, at the expense of justice. Manipulated, sexually-explicit images of her have also begun to spread across the Internet and she has also received death threats. However, Coronel-Ferrer says she accepts them as "part of the job".<ref>http://www.bworldonline.com/weekender/content.php?id=105101</ref>

==External links==
* [http://polisci.upd.edu.ph/faculty/ferrer.html Coronel-Ferrer's faculty profile at the UP Diliman, Department of Political Science website]
* [http://www.inclusivesecurity.org/network-bio/miriam-coronel-ferrer/ Coronel-Ferrer's bio at The Institute for Inclusive Security]

==References==
{{Reflist}}

{{DEFAULTSORT:Coronel-Ferrer, Miriam}}
[[Category:Filipino women in politics]]
[[Category:Filipino educators]]
[[Category:University of the Philippines Diliman alumni]]
[[Category:University of the Philippines people]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Benigno Aquino III Administration personnel]]