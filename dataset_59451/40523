{{other people|Joseph Howard}}
{{Infobox person
|name          = Joseph Howard, Jr.
|image         = JoeHowardJr.jpg
|image_size    = 150px
|caption       = 
|birth_name    = 
|birth_date    = {{Birth date|1833|6|3}}
|birth_place   = [[Brooklyn, New York]], [[United States]]
|death_date    = {{death date and age|1908|3|31|1833|6|3}}
|death_place   = [[New York City]], [[New York (state)|New York]]
|death_cause   = [[Kidney failure]]
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = 
|known_for     = City editor of the [[Brooklyn Eagle]]; responsible for the "[[Civil War gold hoax|Great Civil War Gold Hoax]]". 
|education     =
|alma_mater    = [[Rensselaer Polytechnic Institute|Troy Polytechnic Institute]]
|employer      = 
|occupation    = Editor and journalist
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = [[Democratic Party (United States)|Democrat]]
|boards        = 
|religion      = [[Plymouth Church of the Pilgrims|Plymouth Church]]
|spouse        = Anna S. Gregg
|partner       = 
|children      = 4 daughters: Grace Mesnard, Susan White, Rose Jewett and Maud Beard Bush (married to [[Irving T. Bush]])
|parents       = John Tasker Howard and Susan Raymond
|relations     = [[Samuel Gregg]], father-in-law
|signature     = 
|website       = 
|footnotes     = 
}}
'''Joseph "Joe" Howard, Jr.''' (June 3, 1833 &ndash; March 31, 1908) was an American journalist, war correspondent, publicist and newspaperman. He was one of the top reporters for ''[[The New York Times]]'', city editor of the [[Brooklyn Eagle]] and longtime president of the [[New York Press Club]]. One of the most colorful reporters of the era, he was a popular lecturer and discussed journalism and his life from 1886 until shortly before his death.

During the [[American Civil War]], he and fellow reporter [[Francis A. Mallison]] were responsible in creating a forgery falsely declaring another [[conscription|conscription order]] in [[New York City]] by President [[Abraham Lincoln]]. This document was published in both the ''[[New York World]]'' and the ''[[Journal of Commerce]]'' and, less than a year after the [[New York Draft Riots]], a minor riot ensured when a mob gathered outside Journal of Commerce. Howard was eventually arrested for what became known as "Howard's Proclamation" or the "[[Civil War gold hoax|Great Civil War Gold Hoax]]" and held as a [[prisoner of war]] at [[Fort Lafayette]].

==Biography==

===Early life and journalism career===
Joseph Howard, Jr. was born in [[Brooklyn, New York]] on June 3, 1833. His family emigrated from England to [[Salem, Massachusetts]] in 1700 and remained there for generations before his grandfather Joseph Howard and father John Tasker Howard, both prominent religious leaders, moved to New York City in 1820.<ref name="NCAB">''The National Cyclopaedia of American Biography''. Vol. XV. New York: James T. White & Company, 1895. (pg. 213-214)</ref> His father was one of the founders of [[Plymouth Church (Brooklyn)|Plymouth Church]] and responsible for bringing [[Henry Ward Beecher]] to its ministry. Joseph Howard was educated at [[Farmington, Connecticut]] and graduated from the [[Rensselaer Polytechnic Institute|Troy Polytechnic Institute]] in 1857.<ref name="Heidler">Heidler, Jeanne T. and David J. Coles, ed. ''Encyclopedia Of The American Civil War: A Political, Social, and Military History''. W. W. Norton & Company, 2002. (pg. 1,007-1,008) ISBN 0-393-04758-X</ref> He also married Anna S. Gregg, daughter of noted [[homeopathist]] Dr. [[Samuel Gregg]], who together would have four daughters.<ref name="NCAB"/>

Although he had intended to become a [[civil engineer]], he was drawn into the field of journalism by what he called "a sense of adventure". Shortly after returning from a pleasure trip in California in February 1860, he visited [[Lynn, Massachusetts]] to witness a shoemakers strike "to see the fun". According to the popular story, Howard entered a local hotel and saw the name of a reporter from the ''[[New York Herald]]'' in the registry. He then signed his name in a similar fashion claiming to be a reporter from the ''[[New York Times]]''. That night, he sent a report on the strike to ''The Times'' which so impressed the editors that [[Henry J. Raymond]] personally telegraphed Howard to offer him a full-time position on the paper.<ref name="Article">"Dies In 75th Year'; Journalist and Lecturer Began His Career as a Reporter for The Times, Was A Prisoner Of War; Lectured on "Cranks" and "People I Have Met " -- Was Long President of the New York Press Club". <u>New York Times.</u> 1 Apr 1908</ref>

Howard stayed in Lynn covering the strike for the next five weeks and, for several years afterwards, became a leading journalist for ''The Times''. He came to national attention for a series of articles he published while traveling with the [[King Edward VII|Prince of Wales]] during his tour of the United States and Canada.<ref name="Heidler"/> His reports provided detailed descriptions of the reception given to the Prince and his royal escort as they visited several major cities, all of these letters signed "Howard".<ref name="NCAB"/><ref name="Article"/>

He extensively covered the [[United States presidential election, 1860|United States presidential election of 1860]] <ref name="Heidler"/> and, the following year, he wrote a false story claiming that [[Abraham Lincoln]] had traveled through [[Baltimore]] disguised in "a Scotch cap and long military cloak" while on his way to [[Washington, D.C.]] for his official inauguration <ref name="MOH">{{Cite web |url=http://www.museumofhoaxes.com/gold.html |title=The Great Civil War Gold Hoax |accessdate=12 January 2009 |author=Boese, Alex |last= |first= |authorlink= |coauthors= |date= |year=2002 |month= |work=Hoaxes: 1800-1868 |publisher=[[Museum of Hoaxes]] |location= |pages= |language= |doi= |archiveurl= |archivedate= |quote=}}</ref> Upon the outbreak of the [[American Civil War]], became a [[war correspondent]] and was present at the battels of [[First Battle of Bull Run|Bull Run]] and [[Battle of Ball's Bluff|Ball's Bluff]]. He also played a series of practical jokes such as holding open the paper's lines to telegraph the [[genealogy]] of [[Jesus]] and, in September 1862, he violated an order prohibiting journalists from attending the funeral of Brigadier General [[Philip Kearny]] by sneaking in dressed in clerical robes. This incident caused his editors to remove him as a regular columnist and he was forced to become a freelance reporter.<ref name="Heidler"/> He did continue to remain with ''The Times'' on and off for another eight years and was also a regular contributor to ''[[The Independent]]'', ''[[The Atlantic Monthly]]'', ''The Leader'', ''Noah's Sunday Times'' and other newspapers. Near the end of the war, he was also briefly the city editor of the ''[[Brooklyn Eagle]]'' and the ''[[Sunday Mercury (New York)|New York Sunday Mercury]]''.<ref name="NCAB"/><ref name="Article"/>

===The Great Civil War Gold Hoax===
{{main article|Civil War gold hoax}}

On May 18, 1864, a government proclamation was published in the ''[[New York World]]'' and the ''[[Journal of Commerce]]'' which claimed that President [[Abraham Lincoln]] had ordered the conscription of an additional 400,000 men into the [[Union Army]] due to "the situation in Virginia, the disaster at Red River, the delay at Charleston, and the general state of the country". With Confederate General [[Robert E. Lee]] on the run from General [[Ulysses S. Grant]] in Virginia, this news came as a shock to New Yorkers who believed the war was nearing its end. Predictably, the [[New York Stock Exchange]] plummeted while the value of gold immediately began to rise.<ref name="MOH"/>

After less than a year following the [[New York Draft Riots]], it was feared that another riot might result from the story. That same morning, a crowd began to gather outside of the office of the Journal of Commerce on the corner of [[Wall Street|Wall]] and Water Streets. Many of these were merchants suspicious of the story and finally General [[George B. McClellan]] arrived at the office to investigate the matter. The editors, concerned of a likely confrontation with the mob, insisted the proclamation was real and showed a dispatch from [[Associated Press]] which they had received earlier that morning.<ref name="MOH"/>

However, shortly after 11:00&nbsp;a.m., the Associated Press issued a public statement denying they had such a dispatch. An hour and a half later, a telegram was received from the [[US State Department]] in Washington in which Secretary of State [[William H. Seward]] declared the proclamation "an absolute forgery". It was soon discovered that the document was a hoax however the New York financial district, specifically the stock exchange, had suffered greatly in the meantime. President Lincoln also responded by closing down a number of newspapers and arresting the owners resulting in what would be one of the biggest controversies of his presidency.<ref name="MOH"/>

Two days after the story was published, detectives arrested Brooklyn Eagle reporter Francis A. Mallison who quickly confessed to his involvement in the hoax and implicated his editor as having organized the deception. Howard was arrested at his Brooklyn home and later made a full confession. Having an intimate knowledge of the newspaper industry, he knew that the price of gold would skyrocket if word got out of a delay in the war effort. He invested heavily in gold and, when he and Mallison used various couriers to plant the false information in the press the following morning, it was a simple matter for him to sell his shares once the price had risen enough for him to make a huge profit.<ref name="MOH"/>

Howard was held as a [[prisoner of war]] at [[Fort Lafayette]] for fourteen weeks, serving less than three months of his sentence, before being pardoned by Lincoln on August 22, 1864. The president was supposedly moved by Henry Ward Beecher, a personal friend of Howard's father, who spoke on his behalf claiming that Howard, Jr. was guilty only of "the hope of making some money". Lincoln also appointed Howard official military recorder for the Eastern Department until the end of the war.<ref name="Heidler"/><ref name="Article"/> During his time there, he was present at the trial and execution of Confederate spies [[John Yates Beall]] and [[Robert Cobb Kennedy]]. Ironically, Lincoln did in fact issue a call for 500,000 men two months later and may have been a factor in Lincoln's lenient treatment of Howard.<ref name="NCAB"/><ref name="MOH"/>

===Later years===
After his release, Howard continued to work for ''The Times'' until August 1868 when he became managing editor for [[Marcus M. Pomeroy|"Brick" Pomeroy]]'s ''The Democrat''. On January 1, 1869, he took control of the ''[[New York Star (1800s newspaper)|New York Star]]'' <ref name="Heidler"/> and remained on as editor, publisher and subsequently chief proprietor until the spring of 1875. After a year with the ''[[New York Sun]]'', he became a political writer for the ''New York Herald'' and received national attention for his accurate predictions during the [[James G. Blaine|Blaine campaign]] in 1880. He officially retired from journalism that same year but continued to send editorials and letters from time to time, popularly known as "Howard's Column", which appeared in newspapers throughout the [[Northeastern United States]] including the ''New York Press'', the ''New York Recorder'' and the ''Boston Globe''.<ref name="NCAB"/>

Among the social and political events he reported included the trial and execution of presidential assassin [[Charles J. Guiteau]], the [[Red River Rebellion]] led by [[Louis Riel]], the presidential campaigns and inaugurations of [[James A. Garfield]] and [[Grover Cleveland]], the death and funeral of Ulysses S. Grant and the opening of the [[Brooklyn Bridge]]. He also compiled a large collection of letters and telegrams sent by various influential members of the [[Republican Party (United States)|Republican Party]] to [[Stephen W. Dorsey]] and provided digests for the memoirs of Grant and Beecher in his column, which were reprinted throughout the world, and regularly corresponded with the ''[[Boston Globe]]'', the ''[[Chicago News]]'' and the ''[[United Press]]''. He later reported the trial and execution of the Chicago anarchists for the ''[[New York World]]''.<ref name="NCAB"/>

He was one of the founding members of the [[New York Press Club]], serving as its president four times, and a member of various others including the Electric Club, the Tenderloin Club, the Philadelphia Journalists, the Brockton Press Club and the Boston Press Club. He was also president of the International League of Press Clubs. In 1886, he delivered a speech entitled "Remembrances of Journalism" to raise money for the New York Press Club burial fund which received $4,500. He left the staff of ''The Herald'' shortly after and began lecturing later that year, his most memorable addresses being "Journalism", "Cranks" and "People I Have Met".<ref name="NCAB"/><ref name="Heidler"/><ref name="Article"/>

Howard was in poor health during the last year and a half of his life and died of [[kidney failure]] on the evening of March 31, 1908.<ref name="Heidler"/> Howard is interred at [[Green-Wood Cemetery]] in [[Brooklyn]], [[New York (state)|New York]]. Living at the family apartments at ''The Nevada'', his four daughters and other relatives were in attendanceLiving at the family apartments at ''The Nevada'', his four daughters and other relatives were in attendance at the time of his death.<ref name="Article"/> His eldest daughter Grace founded the first mission for [[Native Americans in the United States|Native American]] girls in the [[Dakota Territory]].<ref name="NCAB"/>

==References==
{{Reflist}}
This commentary is in reference of the mention of Joe Howard Jr. and Anne S. Gregg had four daughters but is not so they had five daughters: Grace, Kate, Susan, Rose, and Maud. Kate is my great grand mother, she was the mother of my grandfather Antonio Howard Soler. She became Kate Howard Soler when she married with whom afterwards became to be my great grand father Arturo Soler. She die in Havana, Cuba in April 20, 1940.

==Further reading==
*Johnson, Rossiter and John Howard Brown, ed. ''The Twentieth Century Biographical Dictionary of Notable Americans''. Vol. V. Boston: The Biographical Society, 1904.
*Mott, Frank Luther. ''American Journalism: A History, 1690-1960''. New York: Macmillan, 1962.
*Starr, Louis Morris. ''Bohemian Brigade: Civil War Newsmen in Action''. New York: Alfred A. Knopf, 1954.
*Wert, Jeffrey D. "The Great Civil War Gold Hoax". ''American History Illustrated'' 1980 15(1): 20-24.

{{DEFAULTSORT:Howard, Joseph Jr.}}
[[Category:1833 births]]
[[Category:1908 deaths]]
[[Category:American people of English descent]]
[[Category:American fraudsters]]
[[Category:American male journalists]]
[[Category:American war correspondents]]
[[Category:People from Brooklyn]]
[[Category:People of New York in the American Civil War]]
[[Category:Rensselaer Polytechnic Institute alumni]]
[[Category:Deaths from renal failure]]
[[Category:Journalists from New York City]]