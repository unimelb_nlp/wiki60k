{{Infobox journal
| title = Ériu
| cover = [[File:Ériu (journal).jpg]]
| editor = Liam Breatnach, Damian MacManus
| discipline = Irish studies, [[Celtic studies]]
| language = English, [[Irish language|Irish]]
| abbreviation = Ériu
| publisher = [[Royal Irish Academy]]
| country = Ireland
| frequency = Annually
| history = 1904 to present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = https://www.ria.ie/eriu
| link1 = 
| link1-name = 
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 728293147
| LCCN = 05035766
| CODEN = 
| ISSN = 0332-0758
| eISSN = 2009-0056
}}
'''''Ériu''''' is an [[academic journal]] of [[Irish language]] studies. It was established in 1904 as the journal of the [[School of Irish Learning]] in [[Dublin]].<ref name=RIA>[http://www.ria.ie/publications/journals/eriu.aspx ''Ériu''], Royal Irish Academy.</ref> When the school was incorporated into the [[Royal Irish Academy]] in 1926, the academy continued publication of the journal, in the same format and with the same title.<ref name=RIA /> Originally, the journal was published in two parts annually, together making a volume, but parts slipped further apart after Volume III.<ref name=RIA /> Articles are written in either Irish or English.

== Editors ==
Since {{when|date=August 2015}}, the [[editors-in-chief]] have been Liam Breatnach ([[Dublin Institute for Advanced Studies]]) and Damian MacManus ([[University of Dublin, Trinity College]]).

== References ==
{{reflist}}

== External links ==
* {{Official website|https://www.ria.ie/eriu}}
* [http://www.ucc.ie/celt/Eriu1-46.pdf Contents of ''Ériu'' vols. 1-46]
* [https://archive.org/search.php?query=%C3%89riu Scans of the first three volumes], at the [[Internet Archive]]

{{DEFAULTSORT:Eriu}}
[[Category:Irish language]]
[[Category:Linguistics journals]]
[[Category:Publications established in 1904]]