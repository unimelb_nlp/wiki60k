{{Orphan|date=July 2015}}

{{Infobox non-profit
| name              = Generations of Virtue
| image           = 
| type              = [[501c3]]
| tax_id             = 14-1875193
| status = Non-profit Organization
| purpose = Activism
| headquarters = [[Colorado Springs, Colorado]]
| founded_date      = 2003
| founder           = Julie Hiramine<ref>[http://www.cru.org.sg/how-to-give-the-talk/ How to give “The Talk”] at cru.org.sg; by Gail Tan; published May 29, 2012; retrieved May 13, 2015</ref>
| key_people        = Kay Hiramine<ref>[http://www.oru.edu/about_oru/board_of_trustees/board_of_trustees.php?trustee_id=29 Board of Trustees] at ORU.edu; retrieved April 20, 2015</ref>
| staff = 8
| area_served       = [[Singapore]], [[Indonesia]], [[Philippines]], [[China]], [[Thailand]], [[Malaysia]], [[Hong Kong]], [[Cambodia]], [[Taiwan]], [[UAE]], [[South Korea]], [[Myanmar]], [[South Africa]], [[Australia]], [[United States]]
| mission = Transforming culture - one family at a time.
| non-profit_slogan =
| former name       =
| homepage          = [http://www.generationsofvirtue.org/ GenerationsofVirtue.org]
}}

'''''Generations of Virtue (GOV)''''' is a [[non-profit organization]] whose focus is to equip families with resources and education on implementing lifestyles of [[Abstinence|sexual purity]], based on [[Christian values]].<ref>http://robhoskins.com/4-reasons-parents-must-talk-kids-50-shades-grey/</ref> GOV was founded in 2003 by Julie Hiramine, the [[author]] of ''Guardians of Purity'',<ref>{{cite journal |last=Ming |first=Lai Kai |last2= |first2= |date=June 2015 |title=Book Reviews |url= |journal= Impact Christian Communications |publisher= |volume= |issue= |pages= |doi= |access-date=June 11, 2015}}</ref><ref name = "license">[http://licensetoparent.org/2013/04/guardians-purity-guest-julie-hiramine/ Guardians of Purity with Guest Julie Hiramine] at License to Parent; by Tracy Embry; published April 15, 2012; retrieved April 20, 2015</ref> and is headquartered in [[Colorado Springs, Colorado]].<ref name = "cnews">[http://www.charismanews.com/opinion/33509-mom-takes-unique-stand-after-inappropriate-instagram-post Mom Takes Unique Stand After Inappropriate Instagram Post] at Charisma News; by Julie Hiramine; published May 31, 2012; retrieved April 20, 2015</ref> The organization has developed curriculum, lecture series, book series, and several products to promote purity.<ref name = "bcn">[http://www.breakingchristiannews.com/articles/display_art.html?ID=10081 GENERATIONS OF VIRTUE MINISTRY SEEKS TO BRING PURITY TO AMERICA'S CHILDREN] at Breaking Christian News; by Michael Ireland; published April 30, 2012; retrieved April 20, 2015</ref> Among their resource line are books for parents to educate their children age-appropriately about [[sexuality]].<ref name = "license"/> They share these resources and training materials through worldwide ministry trips and speaking engagements in almost every state in the U.S. and numerous other nations.

==History==
In 2003, Julie Hiramine founded ''Generations of Virtue'' in [[Colorado Springs, Colorado]].<ref>{{cite book |last= Farrel |first= Bill and Pam |date= 2013 |title= 10 Questions Kids Ask About Sex |url= |location= USA |publisher= Harvest House Publishers |page=165 |isbn=978-0736949194 }}<!--|access-date=Jun 4, 2015--></ref> Its original scope was to teach sexual integrity to families, but they have evolved to educate on a myriad of other topics related to sexual purity including interpreting media, cultural parenting, safe technology use, practically equipping youth for success, strategic family development, and raising generations of virtue.<ref>http://lamplighterbooks.com/?cat=4</ref> GOV is a [[501c3]] non-profit organization.<ref name="cnews"/>

Through the years, the organization has worked with several worldwide organizations including [[Charisma (magazine)|Charisma House Media]], the Tween Gospel Alliance, [[Every Nation]] churches, World Changers’ Summit, Hearts at Home, [[MOPS International|MOPS]], and the International Children’s Ministry Network. In 2003, Generations of Virtue was invited to the New York State Homeschool Convention. From there, they began working with moms’ groups, the homeschool arena, and churches in the U.S. Over the years, Generations of Virtue has broadened its reach to public, private, and international schools and hundreds of church networks throughout the world. Generations of Virtue has ministered in numerous denominations and church movements, including the [[Methodist]], [[Anglican]], [[Baptist]], [[Assemblies of God]], [[Evangelical Free Church]], [[Nondenominational]], [[Lutheran]], [[Presbyterian]], [[Mennonite]], [[Pentecostal]], [[Calvary Chapel]], Full Gospel Assembly, and [[Vineyard]].  Hiramine has written for [[Charisma (magazine)|Charisma Magazine]] and Charisma Publishing published her debut book in 2012.<ref name = "license"/> Hiramine is also a Director for the organization True Value of a Woman,<ref>[http://www.truevalueofawoman.com/national-directors/ True Value of a Woman Board of Directors]</ref> and frequently speaks at parenting and family conferences around the world.<ref name="bcn"/>

==Operations==
Generations of Virtue instructs leaders, educators and parents to be effective within their individual areas of influence. They accomplish this goal through strategic partnerships with key organizations both in the [[United States]] and internationally.<ref>{{cite book |last= Loh |first= Pauline |date= April 30, 2014 |title= Keep Calm and Mother On |url= |location= |publisher= Armour Publishing |page=81 |isbn=978-9814597180 }}<!--|access-date=Jun 4, 2015--></ref>

GOV has released several book series, including ''Teknon and the Champion Warriors''<ref>{{cite book |last= Gresh |first= Dannah and Bob |date= 2012 |title= Six Ways to Keep the "Good" in Your Boy On |url= |location= USA |publisher= Harvest House Publishers |page=122 |isbn=978-0736945790 }}<!--|access-date=Jun 4, 2015--></ref> and ''Beautifully Made''. In 2005, GOV introduced [[purity ring]]s to their product line.<ref name="bcn"/> In 2007, GOV started an internship program for youth ages 16–21. During the same year, they launched their international speaking tours.<ref>[http://www.breakpoint.org/features-columns/discourse/entry/15/21404 BP This Week: Protecting Purity] at Break Point; published February 1, 2013; retrieved April 20, 2015</ref>

In 2011, ''Culture Shock'' was published by Standard Publishing. Both the leader’s guide and student's guide have been distributed to over 4,000 people worldwide. In 2012, Generations of Virtue’s DVD curriculum, ''Raising a Pure Generation'', was released.

==External links==
*[http://www.generationsofvirtue.org/ Generations of Virtue Official Website]
*[https://generationsofvirtue.wordpress.com/ Generations of Virtue Wordpress Blog]
*[http://generationsofvirtue.tumblr.com/ Generations of Virtue Tumblr Blog]
*[http://www.amazon.com/Julie-Hiramine/e/B00DWIIX6K Generations of Virtue Educational Materials]
*{{Twitter}}
*{{Facebook|generationsofvirtue}}
*[http://juliehiramine.com/ Julie Hiramine Official Website]

==References==
{{reflist}}

*
*
*
*

[[Category:Charities based in Colorado|Red Cross]]
[[Category:Organizations established in 2003]]
[[Category:Evangelicalism in Colorado]]
[[Category:Organizations based in Colorado Springs, Colorado]]
[[Category:Religion in Colorado Springs, Colorado]]
[[Category:Christian charities based in the United States]]
[[Category:Christian organizations established in the 20th century]]