{{Infobox journal
| title = Journal of Bioactive and Compatible Polymers
| cover = [[File:Journal of Bioactive and Compatible Polymers.jpg]]
| editor = Kathryn Uhrich
| discipline = [[Materials science]]
| former_names = 
| abbreviation = J. Bioact. Compat. Polym.
| publisher = [[Sage Publications]]
| country = 
| frequency = Bimonthly
| history = 1986-present
| openaccess = 
| license = 
| impact = 2.500
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201575/title
| link1 = http://jbc.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jbc.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 231044699
| LCCN = 93093542 
| CODEN = JBCPEV
| ISSN = 0883-9115 
| eISSN = 1530-8030
}}
The '''''Journal of Bioactive and Compatible Polymers''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering the field of [[materials science]], especially the use of [[polymer]]s in biomedicine. its [[editor-in-chief]] is Kathryn Uhrich ([[Rutgers University]]). The journal was established in 1986 and is published by [[Sage Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 2.500, ranking it 62nd out of 165 journals in the category "Biotechnology & Applied Microbiology",<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Biotechnology & Applied Microbiology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref> 10th out of 25 journals in the category "Materials Science, Biomaterials",<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Materials Science, Biomaterials |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Science |work=[[Web of Science]] |postscript=.}}</ref> and 14th out of 79 journals in the category "Polymer Science".<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Polymer Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Science |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201575/title}}


[[Category:Materials science journals]]
[[Category:Bimonthly journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 1986]]
[[Category:English-language journals]]