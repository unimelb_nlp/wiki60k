{{Infobox journal
| title = Journal of the Chemical Society, Faraday Transactions
| formernames = Transactions of the Faraday Society; Journal of the Chemical Society, Faraday Transactions I; Journal of the Chemical Society, Faraday Transactions II
| cover = [[Image:Farad Trans cover.jpg|120 px]]
| editor = [[Rosemary Whitelock]]
| discipline = [[Chemistry]]
| former_names =
| abbreviation = J. Chem. Soc. Faraday Trans.
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| frequency =
| history = 1905–1998
| openacces =
| license =
| impact = 1.757
| impact-year = 1998
| website = http://pubs.rsc.org/en/Journals/JournalIssues/FT
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 20905692
| LCCN = 90659044
| CODEN = JCFTEV
| ISSN = 0956-5000
| eISSN =
}}
The '''''Journal of the Chemical Society, Faraday Transactions''''' was a [[Peer review|peer-reviewed]] [[scientific journal]] published from 1905 until 1998. The journal was originally published by the [[Faraday Society]] under the name '''''Transactions of the Faraday Society''''' and was renamed in 1972.

==History==
The journal began its publication in 1905 as ''Transactions of the Faraday Society''. When the society merged with the other chemistry societies of the United Kingdom to form the [[Royal Society of Chemistry]], the publication of the journal was transferred to the [[Chemical Society]] in 1972 as part of the merger negotiations. The journal was renamed ''Journal of the Chemical Society, Faraday Transactions'', and split in two (''Faraday Transactions I: Physical Chemistry in Condensed Phases'' and ''Faraday Transactions II: Molecular and Chemical Physics''). After the merger, the Royal Society carried the publication until its end. In 1990, the two journals merged into a single ''Journal of the Chemical Society, Faraday Transactions'', which continued publication until 1998 when it merged with a number of other [[physical chemistry]] journals published by different societies to form ''[[Physical Chemistry Chemical Physics]]''.

==See also==
* ''[[Faraday Discussions]]''
* ''[[Physical Chemistry Chemical Physics]]''

== External links ==
* {{Official website|http://pubs.rsc.org/en/Journals/JournalIssues/FT}}

{{Royal Society of Chemistry|state=collapsed}}
{{chem-journal-stub}}

[[Category:Physical chemistry journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 1905]]
[[Category:Publications disestablished in 1998]]
[[Category:English-language journals]]
[[Category:Biweekly journals]]