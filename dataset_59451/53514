{{Use dmy dates|date=October 2013}}
{{multiple issues|
{{cleanup|date=May 2011}}
{{globalize|date=December 2010}}
}}

{{Accounting}}

A '''financial audit''' is conducted to provide an opinion whether "[[financial statements]]" (the information being verified) are stated in accordance with specified criteria. Normally, the criteria are international accounting standards, although auditors may conduct audits of financial statements prepared using the cash basis or some other basis of accounting appropriate for the organisation. In providing an opinion whether financial statements are fairly stated in accordance with accounting standards, the auditor gathers evidence to determine whether the statements contain material errors or other misstatements.<ref name="ReferenceA">Arens, Elder, Beasley; Auditing and Assurance Services; 14th Edition; Prentice Hall; 2012</ref>

The audit opinion is intended to provide reasonable assurance, but not absolute assurance, that the financial statements are presented fairly, in all material respects, and/or give a [[true and fair]] view in accordance with the financial reporting framework. The purpose of an audit is to provide an objective independent examination of the financial statements, which increases the value and credibility of the financial statements produced by management, thus increase user confidence in the financial statement, reduce investor risk and consequently reduce the cost of capital of the preparer of the financial statements.<ref>{{Cite web|url=http://pcaobus.org/Standards/Auditing/Pages/Auditing_Standard_5.aspx|title=Auditing Standard No. 5|website=pcaobus.org|access-date=2016-04-07}}</ref>

In accordance with the [[US GAAP]], auditors must release an opinion of the overall financial statements in the [[auditor's report]]. Auditors can release three types of statements other than an unqualified/unmodified opinion. The unqualified auditor's opinion is the opinion that the financial statements are presented fairly. A qualified opinion is that the financial statements are presented fairly in all material respects in accordance with [[US GAAP]], except for a material misstatement that does not however pervasively affect the user's ability to rely on the financial statements. A qualified opinion can also be issued for a [[scope limitation]] that is of limited significance. Further the auditor can instead issue a disclaimer, because there is insufficient and appropriate evidence to form an opinion or because of lack of independence. In a disclaimer the auditor explains the reasons for withholding an opinion and explicitly indicates that no opinion is expressed. Finally, an adverse audit opinion is issued when the financial statements do not present fairly due to departure from [[US GAAP]] and the departure materially affects the financial statements overall. In an adverse auditor's report the auditor must explain the nature and size of the misstatement and must state the opinion that the financial statements do not present fairly in accordance with [[US GAAP]].<ref>{{Cite web|url=http://pcaobus.org/Standards/Auditing/Pages/AU508.aspx|title=AU 508 Reports on Audited Financial Statements|website=pcaobus.org|access-date=2016-04-07}}</ref>

Financial audits are typically performed by firms of practicing accountants who are experts in financial reporting. The financial audit is one of many [[Assurance services|assurance]] functions provided by [[accountancy|accounting]] firms. Many organizations separately employ or hire [[internal audit]]ors, who do not attest to financial reports but focus mainly on the internal controls of the organization. [[External audit staff|External auditors]] may choose to place limited reliance on the work of internal auditors. Auditing promotes transparency and accuracy in the financial disclosures made by an organization, therefore would likely reduce such corporations concealmeant of unscrupulous dealings.<ref>CALCPA. (2010, November). Report: Auditing Missteps During the Economic Crisis. News & Trends, p. 4.</ref>

Internationally, the [[International Standards on Auditing]] (ISA) issued by the [[International Auditing and Assurance Standards Board]] (IAASB) is considered as the benchmark for audit process. Almost all jurisdictions require auditors to follow the ISA or a local variation of the ISA.

== Audit overview==
Financial audits exist to add credibility to the implied assertion by an organisation's management that its financial statements fairly represent the organisation's position and performance to the firm's stakeholders. The principal stakeholders of a company are typically its shareholders, but other parties such as tax authorities, banks, regulators, suppliers, customers and employees may also have an interest in knowing that the financial statements are presented fairly, in all material aspects. An audit is not designed to provide absolute assurance, being based on sampling and not the testing of all transactions and balances; rather it is designed to reduce the risk of a material financial statement misstatement whether caused by fraud or error. A misstatement is defined in ISA 450 as an error, omitted disclosure or inappropriate accounting policy. "Material" is an error or omission that would affect the users decision.  Audits exist because they add value through easing the cost of [[information asymmetry]] and reducing information risk, not because they are required by law (note: audits are obligatory in many EU-member states and in many jurisdictions are obligatory for companies listed on public stock exchanges).
For collection and accumulation of audit evidence, certain methods and means generally adopted by auditors are:<ref>{{cite book |title=Auditing and Assurance Vol.1 |url=http://www.icai.org/ |year=2011 |publisher=The Institute of Chartered Accountants of India|location= |isbn=978-81-8441-135-5 |page=3.4 |accessdate=27 August 2012}}</ref>
# Posting checking
# Testing the existence and effectiveness of management controls that prevent financial statement misstatement
# Casting checking
# Physical examination and count 
# Confirmation
# Inquiry
# Observation
# inspection
# Year-end scrutiny
# Re-computation
# Tracing in subsequent period
# [[Bank reconciliation]]
# [[Vouching (financial auditing)|Vouching]]
# Verification of existence, ownership, title and value of assets and determination of the extent and nature of liabilities

==The Big Four==
The '''Big Four''' are the four largest international [[professional services networks]], offering [[audit]], assurance, tax, consulting, advisory, actuarial, corporate finance, and legal services. They handle the vast majority of audits for [[Public company|publicly traded companies]] as well as many [[Private company|private companies]], creating an [[oligopoly]] in auditing large companies. It is reported that the Big Four audit 99% of the companies in the [[FTSE 100 Index|FTSE 100]], and 96% of the companies in the [[FTSE 250 Index]], an [[Stock market index|index]] of the leading [[Market capitalization|mid-cap]] listing companies.<ref name="FTSE">{{cite news|url=https://www.wsj.com/article/SB10001424052748703806304576232231353594682.html |title=U.K. Auditors Criticized on Bank Crisis |date=2011-03-30 |author=Mario Christodoulou |publisher=Wall Street Journal }}</ref> The Big Four firms are shown below, with their latest publicly available data. None of the Big Four firms is a single firm; rather, they are [[professional services networks]]. Each is a network of firms, owned and managed independently, which have entered into agreements with other member firms in the network to share a common name, brand and quality standards. Each network has established an entity to co-ordinate the activities of the network. In one case (KPMG), the co-ordinating entity is Swiss, and in three cases (Deloitte Touche Tohmatsu, PricewaterhouseCoopers and Ernst & Young) the co-ordinating entity is a UK [[limited company]]. Those entities do not themselves perform external professional services, and do not own or control the member firms. They are similar to [[law firm network]]s found in the legal profession. In many cases each member firm practises in a single country, and is structured to comply with the regulatory environment in that country. In 2007 KPMG announced a merger of four member firms (in the United Kingdom, Germany, Switzerland and [[Liechtenstein]]) to form a single firm. Ernst & Young also includes separate legal entities which manage three of its four areas: Americas, EMEIA (Europe, The Middle East, India and Africa), and [[Asia-Pacific]]. (Note: the Japan area does not have a separate area management entity).  These firms coordinate services performed by local firms within their respective areas but do not perform services or hold ownership in the local entities.<ref>{{cite web|last=Ernst & Young|title=Legal statement|work=http://www.ey.com/GL/EN/home/legal}}</ref> This group was once known as the "Big Eight", and was reduced to the "Big Six" and then "Big Five" by a series of [[merger]]s. The Big Five became the Big Four after the demise of [[Arthur Andersen]] in 2002, following its involvement in the [[Enron scandal]].

== Costs ==
Costs of audit services can vary greatly dependent upon the nature of the entity, its transactions, industry, the condition of the financial records and financial statements, and the fee rates of the CPA firm.<ref>{{cite news|last=Denlinger|first=Craig|title=Audit Costs|url=http://crowdfundcpa.com/cost-estimate--calculator.html|accessdate=27 January 2014}}</ref><ref>{{cite news|title=Audit Costs|url=http://artesiancpa.com/|accessdate=15 April 2015}}</ref> A commercial decision such as the setting of audit fees is handled by companies and their auditors. Directors are responsible for setting the overall fee as well as the audit committee. The fees are set at a level that could not lead to audit quality being compromised.<ref>{{Cite web|url=http://asic.gov.au/regulatory-resources/financial-reporting-and-audit/auditors/audit-quality-the-role-of-directors-and-audit-committees/|title=Audit quality - The role of directors and audit committees|last=Commission|first=c=au;o=Australian Government;ou=Australian Government Australian Securities and Investments|website=asic.gov.au|access-date=2016-05-17}}</ref>

== History ==

=== Audit of government expenditure ===
[[File:NationalAuditOffice.svg|thumb|National Audit Office Logo
(United Kingdom)<ref>{{Cite web|url=https://www.nao.org.uk/|title=National Audit Office (NAO)|website=www.nao.org.uk|language=en-US|access-date=2016-05-17}}</ref>
]]
The earliest surviving mention of a public official charged with auditing government expenditure is a reference to the Auditor of the Exchequer in England in 1314.  The Auditors of the Imprest were established under [[Elizabeth I of England|Queen Elizabeth I]] in 1559 with formal responsibility for auditing Exchequer payments.  This system gradually lapsed and in 1780, Commissioners for Auditing the Public Accounts were appointed by statute.  From 1834, the Commissioners worked in tandem with the [[Comptroller]] of the Exchequer, who was charged with controlling the issuance of funds to the government.

As [[Chancellor of the Exchequer]], [[William Ewart Gladstone]] initiated major reforms of public finance and Parliamentary accountability. His 1866 Exchequer and Audit Departments Act required all departments, for the first time, to produce annual accounts, known as appropriation accounts. The Act also established the position of Comptroller and Auditor General (C&AG) and an Exchequer and Audit Department (E&AD) to provide supporting staff from within the civil service. The C&AG was given two main functions – to authorise the issue of public money to government from the Bank of England, having satisfied himself that this was within the limits Parliament had voted – and to audit the accounts of all Government departments and report to Parliament accordingly.

Auditing of UK government expenditure is now carried out by the [[National Audit Office (United Kingdom)|National Audit Office]]. The [[Australian National Audit Office]] conducts all financial statement audits for entities controlled by the Australian Government.<ref>{{Cite web|url=https://www.anao.gov.au/about/auditor-general-and-office|title=Auditor-General and the Office|last=Office|first=Australian National Audit|date=2016-05-12|website=www.anao.gov.au|language=en|access-date=2016-05-17}}</ref>

== Governance and oversight ==
[[File:US-GovernmentAccountabilityOffice-Seal.svg|thumb|Seal of the [[United States]] [[Government Accountability Office]]<ref>{{cite web|url=http://www.gao.gov/about/seal.html |title=U.S. GAO - Our Seal |publisher=Gao.gov |date=2008-09-29 |accessdate=2013-09-02}}</ref>]]

In the United States, the SEC has generally deferred to the accounting industry (acting through various organisations throughout the years) as to the accounting standards for financial reporting, and the [[U.S. Congress]] has deferred to the SEC.

This is also typically the case in other developed economies.  In the UK, auditing guidelines are set by the institutes (including [[Association of Chartered Certified Accountants|ACCA]], [[ICAEW]], ICAS and ICAI) of which auditing firms and individual auditors are members. While in Australia, the rules and professional code of ethics are set by The [[Institute of Chartered Accountants Australia]] (ICAA), [[CPA Australia]] (CPA) and The National Institute of Accountants (NIA). <ref>{{Cite web|url=http://archive.treasury.gov.au/documents/403/HTML/docshell.asp?URL=Ch2.asp|title=Part 2|website=archive.treasury.gov.au|access-date=2016-05-17}}</ref>

Accordingly, financial auditing standards and methods have tended to change significantly only after auditing failures.  The most recent and familiar case is that of [[Enron]]. The company succeeded in hiding some important facts, such as off-book liabilities, from banks and shareholders.<ref>{{Cite web|url=https://www.fbi.gov/news/stories/2006/december|title=A Look Back at the Enron Case|website=FBI|access-date=2016-04-07}}</ref>  Eventually, Enron filed for bankruptcy, and ({{As of|2006|lc=on}}) is in the process of being dissolved.  One result of this scandal was that [[Arthur Andersen]], then one of the five largest accountancy firms worldwide, lost their ability to audit public companies, essentially killing off the firm.

A recent trend in audits (spurred on by such [[accounting scandals]] as Enron and [[MCI Worldcom|Worldcom]]) has been an increased focus on internal control procedures, which aim to ensure the completeness, accuracy and validity of items in the accounts, and restricted access to financial systems.  This emphasis on the internal control environment is now a mandatory part of the audit of SEC-listed companies, under the auditing standards of the [[Public Company Accounting Oversight Board]] (PCAOB) set up by the [[Sarbanes-Oxley Act of 2002|Sarbanes-Oxley Act]].

Many countries have government sponsored or mandated [[List of accountancy bodies#Auditing standards-setting bodies|organizations]] who develop and maintain auditing standards, commonly referred to generally accepted auditing standards or GAAS. These standards prescribe different aspects of auditing such as the opinion, stages of an audit, and controls over work product (''i.e.'', [[working paper]]s).

Some oversight organisations require auditors and audit firms to undergo a third-party quality review periodically to ensure the applicable GAAS is followed.

== Stages of an audit ==

The following are the stages of a typical audit:<ref name="ReferenceA"/>

=== Phase I Plan and Design an Audit Approach  ===

* Accept Client and Perform Initial Planning.
* Understand the Client’s Business and Industry.
** What should auditors understand?<ref>International Standard on Auditing 315 Understanding the Entity and its Environment and Assessing the Risks of Misstatement</ref>
*** The relevant industry, regulatory, and other external factors including the applicable financial reporting framework
*** The nature of the entity
*** The entity's selection and application of accounting policies
*** The entity's objectives and strategies, and the related business risks that may result in material misstatement of the financial statements
*** The measurement and review of the entity's financial performance
*** Internal control relevant to the audit
* Assess Client’s Business Risk
* Set Materiality and Assess Accepted Audit Risk (AAR) and Inherent Risk (IR).
* Understand Internal Control and Assess Control Risk (CR).
* Develop Overall Audit Plan and Audit Program

=== Phase II Perform Test of Controls and Substantive Test of Transactions ===

* Test of Control: if the auditor plans to reduce the determined control risk, then the auditor should perform the test of control, to assess the operating effectiveness of internal controls (e.g. authorisation of transactions, account reconciliations, [[Separation of duties|segregation of duties]]) including [[ITGC|IT General Controls]].  If internal controls are assessed as effective, this will reduce (but not entirely eliminate) the amount of 'substantive' work the auditor needs to do (see below).
* Substantive test of transactions: evaluate the client’s recording of transactions by verifying the monetary amounts of transactions, a process called substantive tests of transactions. For example, the auditor might use computer software to compare the unit selling price on duplicate sales invoices with an electronic file of approved prices as a test of the accuracy objective for sales transactions. Like the test of control in the preceding paragraph, this test satisfies the accuracy transaction-related audit objective for sales. For the sake of efficiency, auditors often perform tests of controls and substantive tests of transactions at the same time.
* Assess Likelihood of Misstatement in Financial Statement.

'''Notes:'''
* At this stage, if the auditor accept the CR that has been set at the phase I and does not want to reduce the controls risk, then the auditor may not perform test of control. If so, then the auditor perform substantive test of transactions.
* This test determines the amount of work to be performed i.e. substantive testing or test of details.<ref>{{Cite web|url=https://www.icas.com/education-and-qualifications/tests-of-control-and-substantive-testing-student-blog|title=Tests of control &amp; substantive tests|last=Stef|first=Scott,|website=www.icas.com|language=English|access-date=2016-05-17}}</ref>

=== Phase III Perform Analytical Procedures and Tests of Details of Balances ===

* where internal controls are strong, auditors typically rely more on '''Substantive Analytical Procedures''' (the comparison of sets of financial information, and financial with non-financial information, to see if the numbers 'make sense' and that unexpected movements can be explained)
* where internal controls are weak, auditors typically rely more on '''Substantive Tests of Detail of Balance''' (selecting a sample of items from the major account balances, and finding hard evidence (e.g. invoices, bank statements) for those items)

'''Notes:'''
* Some audits involve a 'hard close' or 'fast close' whereby certain substantive procedures can be performed before year-end.  For example, if the year-end is 31 December, the hard close may provide the auditors with figures as at 30 November.  The auditors would audit income/expense movements between 1 January and 30 November, so that after year end, it is only necessary for them to audit the December income/expense movements and 31 December balance sheet.  In some countries and accountancy firms these are known as 'rollforward' procedures.

=== Phase IV Complete the Audit and Issue an Audit Report  ===
After the auditor has completed all procedures for each audit objective and for each financial statement account and related disclosures, it is necessary to combine the information obtained to reach an overall conclusion as to whether the financial statements are fairly presented. This highly subjective process relies heavily on the auditor’s professional judgment. When the audit is completed, the CPA must issue an audit report to accompany the client’s published financial statements.

== Responsibilities of an Auditor ==
Corporations Act 2001 requires the auditor to:
* Gives a true and fair view about whether the financial report complies with the accounting standards
* Conduct their audit in accordance with auditing standards
* Give the directors and auditor's independence declaration and meet independence requirements
* Report certain suspected contraventions to ASIC<ref>{{Cite web|url=http://asic.gov.au/regulatory-resources/financial-reporting-and-audit/auditors/audit-quality-the-role-of-directors-and-audit-committees/|title=Audit quality - The role of directors and audit committees|last=Commission|first=c=au;o=Australian Government;ou=Australian Government Australian Securities and Investments|website=asic.gov.au|access-date=2016-05-17}}</ref>

== Commercial relationships versus objectivity ==
One of the major issues faced by private auditing firms is the need to provide independent auditing services while maintaining a business relationship with the audited company.

The auditing firm's responsibility to check and confirm the reliability of financial statements may be limited by pressure from the audited company, who pays the auditing firm for the service.  The auditing firm's need to maintain a viable business through auditing revenue may be weighed against its duty to examine and verify the accuracy, relevancy, and completeness of the company's financial statements. This is done by auditor.

Numerous proposals are made to revise the current system to provide better economic incentives to auditors to perform the auditing function without having their commercial interests compromised by client relationships.  Examples are more direct incentive compensation awards and financial statement insurance approaches. See, respectively, Incentive Systems to Promote Capital Market Gatekeeper Effectiveness<ref>{{cite web|url=http://papers.ssrn.com/abstract_id=980949 |title=Carrots for Vetogates: Incentive Systems to Promote Capital Market Gatekeeper Effectiveness by Lawrence A. Cunningham :: SSRN |publisher=Papers.ssrn.com |date=2007-04-17 |accessdate=2013-09-02}}</ref> and Financial Statement Insurance.<ref>{{cite web|url=http://papers.ssrn.com/abstract_id=554863 |title=Choosing Gatekeepers: The Financial Statement Insurance Alternative to Auditor Liability by Lawrence A. Cunningham :: SSRN |publisher=Papers.ssrn.com |date=2004-06-03 |accessdate=2013-09-02}}</ref>

==Related qualifications==
* There are several related professional qualifications in the field of financial audit including Certified Internal Auditor, [[Certified General Accountant]], [[Chartered Certified Accountant]], [[Chartered Accountant]] and [[Certified Public Accountant]].

==See also==
{{colbegin}}
*[[Auditor's report]]
*[[Center for Audit Quality]] (CAQ)
*[[Comfort letter]]
*[[Comparison of accounting software]]
*[[Computer Assisted Audit Tools]]
*[[Forensic Accounting]]
*[[International Standards on Auditing]] (ISA)
*[[List of accounting topics]]
{{colend}}

==References==
{{Library resources box 
|by=no 
|onlinebooks=no 
|others=no 
|about=yes 
|label=Financial audit }}
<references />{{Authority control}}

{{DEFAULTSORT:Financial Audit}}
[[Category:Auditing]]
[[Category:Financial accounting]]
[[Category:Accounting terminology]]
[[Category:Types of auditing]]