{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Loss and Gain
| image         = Loss and Gain 1st ed.jpg
| caption = First edition title page
| author        = [[John Henry Newman]]
| country       = England
| language      = English
| genre         = [[Philosophical novel]], [[Campus novel]]
| publisher     = [[Burns & Oates]]
| pub_date      = 1848
}}
'''''Loss and Gain''''' is a [[philosophical novel]] by [[John Henry Newman]] published in 1848. It depicts the culture of [[Oxford University]] in the [[Victorian era|mid-Victorian era]] and the [[Religious conversion|conversion]] of a young student to Roman Catholicism. The novel went through nine editions during Newman's lifetime,<ref name="Hill, Intro, p. xx">Hill, Intro, p. xx</ref> and thirteen printings.<ref>Lipscombe, Intro., p. xix</ref>  It was the first work Newman published after his [[Religious conversion|conversion]] to Roman Catholicism in 1845.<ref>Hill, Intro, p. x; Lipscombe, Intro., p. xvii.</ref>

==Plot introduction==
''Loss and Gain'' describes the religious climate of [[Oxford University]] during the 1840s, a time of great contention between various factions within the [[Church of England]]. Some factions advocated [[Protestant]] doctrines, renouncing the development of doctrine through tradition and instead emphasising [[sola scriptura|private interpretation of scripture]]. Against these and other liberal religious factions, the [[Oxford Movement]], of which Newman was a leading member, advocated a Catholic interpretation of the Church of England, claiming that the Church and its traditions were authoritative. Amongst all of these thinkers, however, the Roman Catholic Church was despised as having abdicated its claim to doctrinal authority by introducing [[superstition]] into its practice. Accordingly, when Newman converted to Roman Catholicism in 1845, he met with vehement criticism. In ''Loss and Gain'', Newman's first publication after his conversion, he expressed the intellectual and emotional development that led him to Roman Catholicism and the response his conversion elicited. Newman was in his 40s and was an esteemed theologian at the time of his conversion, but in the novel he displaces his experience onto Charles Reding, a young student entering Oxford and experiencing its intellectual climate for the first time. Although Charles attempts to follow a conventional path and avoid being influenced by "parties" (i.e. cliques advocating trendy sectarian views), he soon discovers that he is inclined towards Roman Catholicism. He struggles against this inclination but eventually decides he must [[Religious conversion|convert]], a decision that causes great consternation to his family and friends but leads to personal fulfilment.

==Plot summary==
Charles Reding arrives at [[Oxford University]] planning to follow the advice and example of his father, and to submit to the teachings of the [[Church of England]] without becoming involved in any factious parties. Reding is inclined towards a form of [[Latitudinarianism]], following the maxim "Measure people by what they are, and not by what they are not."<ref>Newman, p. 16</ref> His conversations with his friend Sheffield convince him, however, that there must be right and wrong answers in doctrinal matters. To follow the right views, Reding seeks a source of Church authority, and is disappointed to find only party dissension and the [[Protestant]] doctrine of Private Judgment, which locates interpretive authority in the individual and thereby leads (in Newman's view) to the espousal of contradictory views.<ref>Ker, p. 10</ref> Furthermore, Reding begins to have doubts about the [[Thirty-nine Articles]], to which he must subscribe to take his degree. His doubts are briefly dispelled following the death of his father, but return soon afterward. In particular, several brief encounters with Willis, a former Oxford peer who converted to Roman Catholicism, greatly excite and trouble him. Suspicious of his speculations, Jennings forces Reding to live away from Oxford while studying for his exams, so as not to corrupt other students. Reding confesses his doubts to his sister Mary, who does not understand them and loses trust in her brother. When Reding finally decides he must convert, Mary, his mother, and several family friends express resentment and anger. He travels to London, on the way receiving encouragement from a Catholic priest (perhaps Newman himself),<ref name="Hill, Originality, p. 35">Hill, "Originality", p. 35</ref> the first he has ever met. While in London Reding is confronted by emissaries from various religious and philosophical sects who, hearing about his departure from the Anglican Church, want to recruit him for their own causes. Ultimately, however, Reding arrives at the [[Passionist]]s Convent, where he joins the Roman Catholic Church.

==Characters in "Loss and Gain"==
*'''Charles Reding''' – A young Oxford student inclined to submit to tradition and the authority of the Church of England. He comes to feel, in spite of himself, that the Roman Church is the true Church. He is also inclined towards celibacy. Although he is much younger than [[John Henry Newman|Newman]] himself was when he converted, Reding is commonly seen as an autobiographical figure.<ref>Hill, Intro, p. xiv</ref> Newman did, though, warn readers that the novel was not autobiographical and that no identification of characters should be made.<ref>Lipscombe, Intro. p. xxi</ref>
*'''Reverend Reding''' – Charles' father, a country clergyman who follows an intellectually conservative path.
*'''Mary''' – Reding's sister, who despite her love for her brother, is intolerant of his religious doubts and his conversion.
*'''William Sheffield''' – A friend of Reding, described as "viewy" in that he participates in religious fads at Oxford, but ultimately uncommitted to any particular religious view.
*'''Willis''' – An Oxford student who converts to Roman Catholicism, and later helps convince Reding to convert.
*'''Bateman''' – An Oxford student inclined towards [[High Church]] [[Anglicanism|Anglican]] Catholicism, particularly because of its architecture and [[vestment]]s. He unsuccessfully attempts to reconvert Willis.
*'''Mr. Malcolm''' – A friend of Rev. Reding, who condemns Charles for his conversion.
*'''Jennings''' – Vice-principal at Oxford; he suspects Reding of sympathising with Catholicism and therefore sends him away from Oxford.
*'''Campbell''' – An [[Anglicanism|Anglican]] friend of Reding, who sympathises with Reding, although he does not support his conversion. He marries Mary.
*'''Mr. Upton''' – Lecturer on the [[Thirty-nine Articles]]. He reports Reding's questions as suspicious.
*'''Mr. Vincent''' – A junior Tutor to whom Reding looks for guidance. However, he advises Reding to avoid religious factions instead of guiding him towards any positive beliefs.
*'''Freeborn''' – An [[Evangelicalism|Evangelical]] who hosts a religious conversation over tea.

==Analysis and major themes==
Ed Block has described ''Loss and Gain'' as a [[Bildungsroman]]<ref name="Block, p. 24">Block, p. 24</ref> because it describes "the mental growth of an individual... who has to choose between rival systems and loyalties which vie for his attention and support."<ref>Hill, Intro, p. xiii</ref> Reding's intellectual development towards Roman Catholicism parallels (although it is not identical to) that of Newman himself, described in his 1864 autobiography ''[[Apologia Pro Vita Sua]]''. ''Loss and Gain'' was possibly the first novel set entirely within a university milieu<ref>Hill, "Originality", p. 27</ref> and Newman included numerous locally used [[colloquialism]]s to enhance the impression of everyday life.<ref>Hill, "Originality", pp 27–33</ref> Charles' views develop during the course of daily life and in response to the fashions of Oxford at moment, expressing Newman's belief that all aspects of experience are interconnected.<ref>Hill, Intro, p. xvii</ref> The novel has an essentially "[[dialogue|dialogical]] structure" reminiscent of the dialogues of [[Plato]],<ref name="Block, p. 24"/> consisting largely of intellectual conversations Charles has with various acquaintances on religious subjects such as [[Catholicism]], the [[Thirty-nine Articles]], the [[Athanasian Creed]], and [[apostasy]]. Each character has a personal as well as an intellectual relationship with Charles, and possesses at best a partial truth from which Charles draws his own conclusions.<ref>Block, p. 32</ref> The novel has also been considered a satire because of its accounts of the inconsistencies adopted by Anglican thinkers<ref>Ker, p. 15</ref> and of the trendy religious beliefs that urge themselves on Charles in London.

==Origins==
Newman wrote ''Loss and Gain'' as a response to ''From Oxford to Rome: And how it fared with some who lately made the journey'', a novel by Miss Elizabeth Harris, originally published anonymously. Harris had converted to Roman Catholicism along with a number of former [[Oxford Movement]] leaders, but had become disillusioned and reconverted to Anglicanism. Her novel was intended to deter potential converts to Roman Catholicism, and suggested that Newman and other converts were considering returning to the [[Church of England]].<ref>Crawford, p. 415</ref>

==Publication and reception==
The novel went through nine editions in Britain during Newman's lifetime, and two editions each in America, France, and Italy during the 1850s. A German translation is recorded in 1861.<ref name="Hill, Intro, p. xx"/> The book was serialized in the ''Bengal Catholic Register'' in 1850, and a Dutch edition was published in 1882.<ref>"Lipscombe, Intro., p. xix</ref>

The first reviewers of ''Loss and Gain'' tended to judge it according to its theological principles rather than its literary merits, and accordingly were divided along denominational lines.<ref name="Hill, Originality, p. 35"/><br /> <ref>Lipscombe,Intro., p. xix-xx</ref>
[[Mary Augusta Ward|Mrs. Humphrey Ward]] referred to ''Loss and Gain'', along with ''[[Sartor Resartus]]'', ''[[The Nemesis of Faith]]'', ''[[Alton Locke]]'', and ''[[Marius the Epicurean]]'', as one of the works "to which the future student of the nineteenth century will have to look for what is deepest, most intimate, and most real in its personal experience.".<ref>Qtd. in Hill, Intro, p. vii</ref>

==Notes==
{{reflist| 2}}

==References==
*{{cite book |last=Block |first=Ed Jr. |chapter=Venture and Response: The Dialogical Strategy of Newman's ''Loss and Gain'' |title=Critical Essays on John Henry Newman |others=Ed. Ed Block Jr. |year=1992 |publisher=English Literary Studies |location=University of Victoria |pages=23–38 |isbn=0-920604-62-5}}
*{{cite journal |last=Crawford |first=Charlotte E. |publication-date=1950 |title=The Novel that Occasioned Newman's ''Loss and Gain'' |periodical=Modern Language Notes |volume=65 |issue=6 |pages=414–418 |doi=10.2307/2908756 |year=1950 |jstor=2908756 |publisher=The Johns Hopkins University Press |postscript=<!--None-->}}
*Hill, Alan G. (1986). Introduction. ''Loss and Gain'' by John Henry Newman. Oxford: Oxford UP.
*{{cite book |last=Hill |first=Alan G. |chapter=Originality and Realism in Newman's Novels |title=Newman after a Hundred Years |others=Ed. Ker and Hill |year=1990 |publisher=Clarendon Press |location=Oxford |pages=21–42 |isbn=0-19-812891-6}}
*{{cite book |last=Ker |first=Ian |chapter=Newman the Satirist |title=Newman after a Hundred Years |others=Ed. Ker and Hill |year=1990 |publisher=Clarendon Press |location=Oxford |pages=1–20 |isbn=0-19-812891-6}}
*Lipscombe, Trevor (2012). Introduction. ''Loss and Gain'' by John Henry Newman. San Francisco: Ignatius Press.
*{{cite book |last=Newman |first=John Henry |authorlink=John Henry Newman| others=Ed. Alan G. Hill |title=Loss and Gain |origyear=1848 |year=1986 |publisher=Oxford UP |location=Oxford |isbn=0-8240-1530-4}}
*{{cite book |last=Newman |first=John Henry |authorlink=John Henry Newman| others=Ed. Trevor Lipscombe |title=Loss and Gain |origyear=1848 |year=2012 |publisher=Igantius Press |location=San Francisco |isbn=978-1586177058}}

[[Category:1848 novels]]
[[Category:Victorian novels]]
[[Category:English philosophical novels]]
[[Category:Roman Catholic novels]]
[[Category:Novels set in Oxford]]
[[Category:Novels set in the 1840s]]
[[Category:Novels by John Henry Newman]]
[[Category:University of Oxford in fiction]]