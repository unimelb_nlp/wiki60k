{{Good article}}
{{Taxobox
| name = Borneo shark
| image = Carcharhinus borneensis.png
| status = EN 
| status_system = IUCN3.1
| status_ref = <ref name="iucn"/>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Chondrichthyes]]
| subclassis = [[Elasmobranchii]]
| superordo = [[Shark|Selachimorpha]]
| ordo = [[Carcharhiniformes]]
| familia = [[Requiem shark|Carcharhinidae]]
| genus = ''[[Carcharhinus]]''
| species = '''''C. borneensis'''''
| binomial = ''Carcharhinus borneensis''
| binomial_authority = ([[Pieter Bleeker|Bleeker]], 1858)
| range_map = Carcharhinus borneensis distmap.png
| range_map_caption = Present (dark blue) and possible historical (light blue) range of the Borneo shark<ref name="iucn"/><ref name="white et al"/>
| synonyms = ''Carcharias borneensis'' <small>Bleeker, 1858</small>
}}

The '''Borneo shark''' (''Carcharhinus borneensis'') is a [[species]] of [[requiem shark]], and part of the [[family (biology)|family]] Carcharhinidae. Extremely rare, it is known only from [[inshore]] waters around [[Mukah]] in northwestern [[Borneo]], though it may once have been more widely distributed. A small, gray shark reaching {{convert|65|cm|in|abbr=on}} in length, this species is the [[monotypic|only member]] of its [[genus]] with a row of enlarged pores above the corners of its mouth. It has a slender body with a long, pointed snout and a low second [[dorsal fin]] placed posterior to the [[anal fin]] origin.

Almost nothing is known about the natural history of the Borneo shark. It is [[viviparous]] like other requiem sharks; the females bear litters of six pups, which are provisioned through [[gestation]] by a [[placenta]]l connection. The [[International Union for Conservation of Nature]] last assessed this species as [[Endangered]], at which time it had not been seen since 1937. While an extant population has since been found, the Borneo shark continues to merit conservation concern given its highly limited range within heavily fished waters.

==Taxonomy and phylogeny==
Dutch [[ichthyologist]] [[Pieter Bleeker]] originally described the Borneo shark as ''Carcharias (Prionodon) borneensis'' in an 1858 issue of the [[scientific journal]] ''Acta Societatis Regiae Scientiarum Indo-Neêrlandicae''. He based his account on a newborn male {{convert|24|cm|in|abbr=on}} long, caught off [[Singkawang]] in western [[Kalimantan]], [[Borneo]].<ref name="bleeker"/> Later authors have recognized this species as belonging to the genus ''[[Carcharhinus]]''.<ref name="compagno"/> Before 2004, only five specimens of the Borneo shark were known, all of them immature and collected before 1937.<ref name="iucn"/> In April and May 2004, researchers from [[Universiti Malaysia Sabah]] discovered a number of additional specimens while surveying the [[fishery]] resources of [[Sabah]] and [[Sarawak]].<ref name="white et al"/>

The [[phylogeny|evolutionary relationship]]s of the Borneo shark are uncertain. [[Jack Garrick]], in his 1982 [[morphology (biology)|morphological]] study, did not place it close to any other member of the genus.<ref name="garrick"/> [[Leonard Compagno]] in 1988 tentatively grouped it with the [[smalltail shark]] (''C. porosus''), [[blackspot shark]] (''C. sealei''), [[spottail shark]] (''C. sorrah''), [[creek whaler]] (''C. fitzroyensis''), [[whitecheek shark]] (''C. dussumieri''), [[hardnose shark]] (''C. macloti''), and [[Pondicherry shark]] (''C. hemiodon'').<ref name="compagno2"/> The Borneo shark resembles the [[Rhizoprionodon|sharpnose sharks]] (''Rhizoprionodon'') in certain traits, for example the enlarged pores by its mouth. Nevertheless, other aspects of its morphology firmly place it within ''Carcharhinus''.<ref name="white et al"/>

==Description==
The Borneo shark is slim-bodied, with a long, pointed snout and oblique, slit-like nostrils preceded by narrow, nipple-shaped flaps of skin. The eyes are rather large and circular, and equipped with [[nictitating membrane]]s. The corners of the sizable mouth bear short, indistinct furrows, and immediately above are a series of enlarged pores that are unique within the genus. There are 25–26 upper and 23–25 lower tooth rows. The upper teeth have a single, narrow, oblique cusp with strongly serrated edges, and large cusplets on the trailing side. The lower teeth are similar, but tend to be more slender and finely serrated. The five pairs of [[gill slit]]s are short.<ref name="white et al"/><ref name="compagno"/>

The [[pectoral fin]]s are short, pointed, and falcate (sickle-shaped), while the [[pelvic fin]]s are small and triangular with a nearly straight trailing margin. The first [[dorsal fin]] is fairly large and triangular, with a blunt apex sloping down to a sinuous trailing margin; its origin lies over the free rear tips of the pectoral fins. The second dorsal fin is small and low, and originates over the middle of the [[anal fin]] base. There is no ridge between the dorsal fins. The [[caudal peduncle]] bears a deep, crescent-shaped pit at the origin of the upper [[caudal fin]] lobe. The asymmetrical caudal fin has a well-developed lower lobe and a longer, narrow upper lobe with a strong ventral notch near the tip. The [[dermal denticle]]s are small and overlapping, each with three horizontal ridges leading to marginal teeth. This species is slate-gray above, darkening towards the tips of the dorsal fins and upper caudal fin lobe; some specimens have irregular rows of small, white blotches, which may be an artifact of handling. The underside is white, which extends onto the flanks as a vague pale band. There are faint, lighter edges on the pectoral, pelvic, and anal fin trailing margins. The largest known specimen measures {{convert|65|cm|in|abbr=on}} long.<ref name="white et al"/><ref name="compagno"/>

==Distribution and habitat==
[[File:Bako National Park 2006.jpg|thumb|upright=0.8|The Borneo shark is only known to inhabit the coastal waters of Sarawak.]]
All recent specimens of the Borneo shark have been collected solely from fishery landing sites at [[Mukah]] in [[Sarawak]], despite thorough surveys across the rest of Borneo (including at the locality of the type specimen). Thus, its range may now be restricted to shallow, [[inshore]] waters in northwestern Borneo.<ref name="white et al"/><ref name="last et al"/> Of the five earlier specimens, four came from Borneo and one from [[Zhoushan Island]] in [[China]], hinting at a wider historical distribution. This species was also recorded from [[Borongan]] in the [[Philippines]] in 1895, and [[Java]] in 1933; these records cannot be substantiated and there have been no subsequent sightings from these areas.<ref name="white et al"/>

==Biology and ecology==
[[Bony fish]]es are probably the main food of the Borneo shark.<ref name="voigt and weber"/> It is [[viviparous]] like other requiem sharks, with the developing [[embryo]]s provisioned by the mother through a [[placenta]]l connection formed from the depleted [[yolk sac]]. The litter size is six, and the pups are born at close to {{convert|24|-|28|cm|in|abbr=on}} long. From the available specimens, the length at [[sexual maturity]] can be surmised to be under {{convert|55|-|58|cm|in|abbr=on}} in males and under {{convert|61|-|65|cm|in|abbr=on}} in females.<ref name="white et al"/><ref name="voigt and weber"/>

==Human interactions==
The [[International Union for Conservation of Nature]] last assessed the Borneo shark as [[Endangered]], based on 2005 data that do not include the recent specimens from Mukah. Previously, several fishery surveys within its supposed historical range had failed to find it.<ref name="iucn"/> The Borneo shark's conservation status remains precarious given its very small range in waters subjected to intensive [[artisan fishing|artisanal]] and [[commercial fishing]].<ref name="white et al"/> It is caught by line gear and used for meat, though it has minimal commercial significance.<ref name="last et al"/>

==References==
{{reflist|colwidth=30em|refs=

<ref name="bleeker">{{cite journal |author=Bleeker, P. |date=1858 |title=Twaalfde bijdrage tot de kennis der vischfauna van Borneo. Visschen van Sinkawang |journal=Acta Societatis Regiae Scientiarum Indo-Neêrlandicae |volume=5 |issue=7 |pages=1–10}}</ref>

<ref name="compagno">{{cite book |author=Compagno, L.J.V. |date=1984 |title=Sharks of the World: An Annotated and Illustrated Catalogue of Shark Species Known to Date |publisher=Food and Agricultural Organization |isbn=92-5-101384-5 |pages=458–459}}</ref>

<ref name="compagno2">{{cite book |title=Sharks of the Order Carcharhiniformes |author=Compagno, L.J.V. |publisher=Princeton University Press |year=1988 |isbn=0-691-08453-X |pages=319–320}}</ref>

<ref name="garrick">Garrick, J.A.F. (1982). Sharks of the genus ''Carcharhinus''. NOAA Technical Report, NMFS CIRC 445.</ref>

<ref name="iucn">{{IUCN |assessor=Compagno, L.J.V. |title=Carcharhinus borneensis |year=2005 |id=39367 |version=2011.2}}</ref>

<ref name="last et al">{{cite book |title=Sharks and Rays of Borneo |author=Last, P.R. |author2=White, W.T. |author3=Caire, J.N. |author4=Dharmadi |author5=Fahmi |author6=Jensen, K. |author7=Lim, A.P.K. |author8=Mabel-Matsumoto, B. |author9=Naylor, G.J.P. |author10=Pogonoski, J.J. |author11=Stevens, J.D. |author12=Yearsley, G.K. |publisher=CSIRO Publishing |year=2010 |isbn=978-1-921605-59-8 |pages=92–93}}</ref>

<ref name="voigt and weber">{{cite book |author1=Voigt, M. |author2=Weber, D. |title=Field Guide for Sharks of the Genus ''Carcharhinus'' |publisher=Verlag Dr. Friedrich Pfeil |year=2011 |isbn=978-3-89937-132-1 |pages=49–50}}</ref>

<ref name="white et al">{{cite book |chapter=Rediscovery of the rare and endangered Borneo Shark ''Carcharhinus borneensis'' (Bleeker, 1858) (Carcharhiniformes: Carcharhinidae) |author1=White, W.T. |author2=Last, P.R. |author3=Lim, A.P.K. |title=Descriptions of New Sharks and Rays from Borneo |editor=Last, P.R. |editor2=W.T. White |editor3=J.J. Pogonoski |publisher=CSIRO Marine and Atmospheric Research |year=2010 |isbn=978-1-921605-57-4 |pages=17–28}}</ref>

}}

==External links==
* [http://fishbase.org/Summary/speciesSummary.php?ID=863 ''Carcharhinus borneensis'', Borneo shark] at [http://www.fishbase.org/search.php FishBase]
* [http://www.iucnredlist.org/apps/redlist/details/39367/0 ''Carcharhinus borneensis'' (Borneo Shark)] at [http://www.iucnredlist.org/ IUCN Red List]
* [http://shark-references.com/species/view/Carcharhinus-borneensis Species Description of Carcharhinus borneensis at www.shark-references.com]

{{Carcharhinidae}}

{{taxonbar}}

[[Category:Carcharhinidae]]
[[Category:Fish of Indonesia]]
[[Category:Animals described in 1858]]