{{for|the wrestler|Eva Marie}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox musical artist
| name                = Natalie Coyle
| image               =
| background          = solo_singer
| genre               = Classical, [[classical crossover]]
| website             ={{url|nataliecoyle.com/}}
}}

'''Natalie Coyle''' is a London-based, classically trained [[soprano]].<ref>{{cite web|last=Nichols|first=Rob|title=FansOnline|url=http://fansonline.net/onegiantleap/article.php?id=1173|accessdate=5 April 2013}}</ref>

Coyle has sung in aid of many charities; on television shows and has been asked to sing at a number of sporting events.

== Early life and education ==

Coyle was born in Falkirk, Scotland, and brought up in Linlithgow until moving to Surrey at the age of 10 where she attended [[St Teresa's School]] for Girls.<ref>{{cite news|last=Russell|first=Helen|title=It's turf at the top!|newspaper=Linlithgow Gazette|date=5 April 2013}}</ref>

After graduating from [[Sheffield University]] with a degree in music, Coyle was offered a place to train with Steven Maughan at the Amsterdam Opera House and later, with [[Jesús León]] in London. Her training led Coyle to secure a place in the opera ''[[Don Giovanni]]'', performed at Woodhouse.<ref>{{cite news|last=Newbold|first=Andy|title=Natalie Coyle 'My Way'|newspaper=Time & Leisure|location=Surrey|date=<!--April-->}} {{full|date=November 2014|reason=April which year?}}</ref>

== Music career ==

At the Classic BRIT Awards in June 2011, Coyle supported [[Il Divo]] when they won the Artist of the Decade Award at the Royal Albert Hall.<ref>{{cite web|website=ClassicFM|url=http://www.classicfm.com/artists/il-divo/news/il-divo-named-artist-decade/|title=Il Divo named Artist of the Decade|accessdate=5 April 2013}}</ref> Soon after, Coyle won a place with the [[Royal Choral Society]] with whom she has performed regularly, including her second performance of 2011 at the Royal Albert Hall, ''[[Carmina Burana]]''. Additionally, Coyle performed on the live final of ''Britain's Got Talent'' with [[Jackie Evancho]] (finalist in ''America's Got Talent''), to an audience of over 15 million.<ref>{{cite web|last=McGarry|first=Lisa|title=UnrealityTV|url=http://www.unrealitytv.co.uk/britains-got-talent/britains-got-talent-2011-jackie-evancho-performed-nessun-dorma-on-final-show-video/|work=Britain's Got Talent 2011|accessdate=5 April 2013}}</ref> Throughout 2011, Coyle performed in numerous broadcast episodes of the BBC's ''Songs of Praise''.

In 2012, Coyle was selected to take part in the Associated Studios' prestigious 4-month performance course, where she studied a broad range of musical genres which has helped to build on her classical roots and move into a classical-crossover style.<ref>{{cite web|title=Associated Studios|url=http://www.associatedstudios.co.uk/musical-theatre-workshops-courses|accessdate=5 April 2013}}</ref> While on the course, Coyle took part in numerous master-classes with directors, playwrights and singers such as [[Ché Walker]], [[Graeme Danby]], [[Michael England]] and Kate Golledge. The course ended with a performance at the Arts Theatre in London's West-End. At the end of 2012, Coyle performed, for the second year running, in Duke of York Square for the Prince's Trust at their annual event to switch on the King's Road Christmas lights.<ref>{{cite web|title=Duke of York Square|url=http://www.dukeofyorksquare.com/featured/christmas-grotto/|work=Christmas Lights|accessdate=12 October 2012}}</ref> Coyle also performed at ''Attitude'' magazine's annual Christmas event at the Conran Shop, Chelsea.<ref>{{cite news|title=The Attitude Conran Shop event|newspaper=Attitude|date=<!--February-->}} {{full|date=November 2014|reason=April which year?}}</ref> From the end of 2012 and into 2013, Coyle has joined Classic BRIT Award winners Blake on their UK tour.<ref>{{cite news|last=Pratt|first=Steve|title=And then there were three...|newspaper=Northern Echo|date=7 March 2013}}</ref><ref>{{cite news|title=Classical boy band to play at Towngate|newspaper=Basildon Yellow Advertiser|date=14 February 2013}}</ref>

On 7 April 2013, Coyle made her Wembley debut, singing the national anthem for the football league Johnstone's Paint Trophy Final (Southend v Crewe Alexandra), in front of over 50,000 supporters and live on SkySports1.<ref>{{cite news|last=Kay|first=Richard|title=Rushdie girl's Britannic verses|newspaper=Daily Mail|date=3 April 2013}}</ref><ref>{{cite news|last=Shakespeare|first=Sebastian|title=Football's my game now|newspaper=London Evening Standard|date=4 April 2013}}</ref><ref>{{cite web|title=Coyle to Sing Anthem|url=http://www.football-league.co.uk/johnstonespainttrophy/news/20130328/coyle-to-sing-anthem_2293332_3124628|publisher=The Football League|accessdate=28 April 2013}}</ref>

On 27 May 2013, Coyle performed the national anthem at Wembley for the second time for the npower Championship play-off final, which saw Crystal Palace beat Watford to secure a place in the Premier League.<ref>{{cite news|last=Anisiobi|first=JJ|title=Soprano Natalie Coyle reveals the surprising two sides to her career|url=http://www.dailymail.co.uk/tvshowbiz/article-2331186/Soprano-Natalie-Coyle-reveals-surprising-sides-singing-career.html|publisher=Mail Online|accessdate=30 May 2013|location=London|date=26 May 2013}}</ref><ref>{{cite news|last=Anisiobi|first=JJ|title=Sir Elton John and David Furnish take son to first football match|url=http://www.dailymail.co.uk/tvshowbiz/article-2331678/Sir-Elton-John-David-Furnish-son-Zachary-football-match-Watford-Crystal-Palace-Play-Final.html|publisher=Mail Online|accessdate=30 May 2013|location=London|date=27 May 2013}}</ref> Coyle's performance was to a sold out crowd of 85,000.<ref>{{cite web|last=Blackmore|first=David|title=Bookham singer at Wembley|url=http://www.thisissurreytoday.co.uk/Bookham-singer-Wembley/story-19067265-detail/story.html#axzz2Um0xnU21|publisher=Surrey Today|accessdate=30 May 2013}}</ref>

On 24 August 2013, Coyle was invited back to Wembley for the third time, to perform for the rugby league Tetley's Challenge Cup Final. Coyle sang the national anthem and "Abide with Me" to a sold-out crowd of over 85,000 and live on the BBC.<ref>{{cite news|last=Eden|first=Richard|title=Mandrake|newspaper=Sunday Telegraph|date=25 August 2013}}</ref>

Coyle made her debut performance for England on 9 November 2013, where she performed the England and Fijian National Anthems for the Rugby League World Cup England v Fiji match.<ref>{{cite web|last=Falconer|first=Daniel|title=Natalie Coyle Exclusive Interview|url=http://www.femalefirst.co.uk/music/interviews/natalie-coyle-exclusive-interview-368331.html|publisher=Female First|accessdate=8 November 2013}}</ref><ref>{{cite news|last=Bell|first=Matthew|newspaper=Independent on Sunday}} {{full|date=November 2014|reason=Article title?}}</ref> Over 2 million people tuned in to watch the match which was aired live on BBC1.<ref>{{cite web|url=http://www.rlwc2013.com/rugby-league-world-news/article/1344/over-two-million-viewers-watch|title=Over Two million viewers watch England beat Fiji in RLWC2013|publisher=RLWC2013|accessdate=11 November 2013}}</ref>

== Charities ==

Coyle has performed in aid of a number of charities including the Prince's Trust, Queen Elizabeth's Foundation for Disabled People, St Thomas' Hospital, St John's Hospice, London, and the Order of Malta Volunteers. Coyle is also heavily involved with the Children's Trust and [[Marie Curie Cancer Care]]. She has recently been appointed Patron of Breathe, a partner of Guy's and St Thomas Charity, the largest NHS charity in the UK.<ref>{{cite web|title=Breathe|url=http://breatheahr.org/|accessdate=5 April 2013}}</ref>

In May 2013, Coyle performed for the [[Cystic Fibrosis Trust]] at the Nicky's Whisper Trophy Final at QPR's Loftus Road. MPs and celebrities including [[Ali Campbell|Alistair Campbell]] and [[Omid Djalili]] took part in the charity football match.<ref>{{cite web|title=Sporting celebs take on MP's in aid of Cystic Fibrosis Trust|url=https://www.cysticfibrosis.org.uk/news/latest-news/sporting-celebs-take-on-mps-in-aid-of-cystic-fibrosis-trust.aspx|publisher=Cystic Fibrosis Trust|accessdate=30 May 2013}}</ref> Later that month, Coyle also performed at Reading FC's Madejski Stadium for Celebrity Soccer Six in aid of the charity campaign 'Hearts and Goals', supported by Fabrice Muamba.<ref>{{cite web|title=Celebrity Soccer Six 2013|url=http://soccersix2013.com/info/|publisher=soccersix2013.com}}</ref><ref>{{cite web|last=Blackmore|first=David|title=Bookham singer at Wembley|url=http://www.thisissurreytoday.co.uk/Bookham-singer-Wembley/story-19067265-detail/story.html#axzz2Um0xnU21|publisher=SurreyToday|accessdate=30 May 2013}}</ref>

== References ==
{{reflist|colwidth=30em}}

==External links==
*{{Official website|http://nataliecoyle.com/}}

{{DEFAULTSORT:Coyle, Natalie}}
[[Category:English sopranos]]
[[Category:People from Falkirk]]
[[Category:Alumni of the University of Sheffield]]
[[Category:Opera crossover singers]]
[[Category:Living people]]
[[Category:Year of birth missing (living people)]]
[[Category:People educated at St Teresa's School]]