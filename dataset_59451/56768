{{Infobox magazine
| title           = Environment
| logo            = 
| logo_size       = 
| image_file      = <!-- cover.jpg (omit the "file: prefix -->
| image_size      = <!-- (defaults to user thumbnail size if no size is stated) -->
| image_alt       = 
| image_caption   = 
| editor          = Susan L. Cutter, Anthony A. Leiserowitz, Alan H. McGowan, [[Tim O'Riordan | Timothy O'Riordan]]
| editor_title    = Executive Editors
| previous_editor = 
| staff_writer    = 
| photographer    = 
| category        = [[Natural environment|Environment]], [[Environmental policy]]
| frequency       = Bimonthly
| circulation     = 3,100<ref>[http://www.heldref.org/media-kits/pdfs/2013%20VENV%20Media%20Kit.pdf "Rates and Data 2013,"] ''Environment'' magazine. Accessed: March 19, 2013.</ref>
| publisher       = 
| founder         = [[Barry Commoner]]
| founded         = 1958
| firstdate       = <!-- {{Start date|year|month|day}} -->
| company         = [[Taylor & Francis]]
| country         = 
| based           = [[Philadelphia]]
| language        = 
| website         = {{URL|www.environmentmagazine.org}}
| issn            = 0013-9157
| oclc            = 476415628
}}

'''''Environment: Science and Policy for Sustainable Development''''', commonly referred to as ''Environment'' magazine, is published bi-monthly in Philadelphia by [[Taylor & Francis]]. ''Environment'' is a hybrid, [[peer-reviewed]], popular [[environmental science]] publication and website, aimed at a broad, "smart, but uninitiated" population.<ref>''Environment'' (inside back cover), March/April 2013.</ref> Its Executive Editors are Susan L. Cutter ([[University of South Carolina]]), [[Anthony Leiserowitz|Anthony A. Leiserowitz]] ([[Yale University]]), Alan H. McGowan ([[The New School]]), and [[Tim O'Riordan]] ([[University of East Anglia]]).

== History ==
''Environment'' was founded in the late 1950s as ''Nuclear Information'', a mimeographed newsletter published by [[Barry Commoner]] at the Center for the Biology of Natural Systems, at [[Washington University in St. Louis|Washington University]], in [[St. Louis]], [[Missouri]].<ref>{{cite journal|last=McGowan|first=Alan H.|title=Remembering Barry Commoner|journal=Environment|date=March–April 2013|volume=55|issue=2|page=17|doi=10.1080/00139157.2013.765312}}</ref>

== References ==
{{Reflist}}

[[Category:1958 establishments in the United States]]
[[Category:Environmental magazines]]
[[Category:American bi-monthly magazines]]
[[Category:Magazines established in 1958]]
[[Category:Magazines published in Pennsylvania]]
[[Category:Environmental websites]]
[[Category:Magazines published in Missouri]]

{{environment-stub}}
{{biology-journal-stub}}