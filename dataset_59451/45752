{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name= Sir John Willis
|image= 
|caption= 
|birth_date= {{birth date|1937|10|27|df=y}}
|death_date= {{death date and age|2008|01|09|1937|10|27|df=y}} 
|birth_place= [[London]], England
|death_place= 
|nickname=
|allegiance= United Kingdom
|branch= [[Royal Air Force]]
|serviceyears= 1955–1997
|rank= [[Air Chief Marshal]]
|commands= [[Vice-Chief of the Defence Staff (United Kingdom)|Vice-Chief of the Defence Staff]] (1995–97)<br/>[[RAF Support Command|Support Command]] (1992–94)<br/>[[RAF Akrotiri]] (1982–84)<br/>[[No. 27 Squadron RAF|No. 27 Squadron]] (1975–77)
|unit=
|battles= 
|awards= [[Knight Grand Cross of the Order of the British Empire]]<br/>[[Knight Commander of the Order of the Bath]]
|laterwork= 
}}
[[Air Chief Marshal]] '''Sir John Frederick Willis''' {{post-nominals|country=GBR|size=100%|sep=,|GBE|KCB|FRAeS}} (27 October 1937 – 9 January 2008) was a senior [[Royal Air Force]] officer.

==Flying career==
John Frederick Willis was born in London and educated at [[Dulwich College]] and the [[RAF College]] [[Cranwell]].<ref name=tele>[http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2008/01/14/db1401.xml Telegraph.co.uk – Obituary of Air Chief Marshal Sir John Willis]</ref> Willis gained his RAF commission in 1958 and went on to fly [[Avro Vulcan|Vulcan]] bombers.<ref name=tele/> He went on to be [[Commanding Officer]] of [[No. 27 Squadron RAF|No. 27 Squadron]] at [[RAF Scampton]].<ref name=tele/> He was appointed Deputy Director of Air Staff Plans at the [[Ministry of Defence (United Kingdom)|Ministry of Defence]] in 1979, Station Commander at [[RAF Akrotiri]] on [[Cyprus]] in 1982 and Director of Air Staff Briefing & Co-ordination at the Ministry of Defence in 1985.<ref name=air>[http://www.rafweb.org/Biographies/Willis_JF.htm Air of Authority – A History of RAF Organisation – Air Chief Marshal Sir John Willis]</ref> Later that year he was made Chief of the Special Weapons Branch at Headquarters [[Supreme Headquarters Allied Powers Europe|SHAPE]].<ref name=air/> 

In 1989 he was appointed Assistant Chief of Defence Staff (Policy & Nuclear).<ref name=tele/> He went on to be one of the RAF's most senior commanders, becoming Director-General of Training in 1991, [[Air Officer Commanding]]-in Chief of [[RAF Support Command|Support Command]] in 1992 and the [[Vice-Chief of the Defence Staff (United Kingdom)|Vice-Chief of the Defence Staff]] at the [[Ministry of Defence (United Kingdom)|Ministry of Defence]] in 1995.<ref name=tele/> He retired from the Royal Air Force in 1997.<ref name=tele/>

In retirement he was a member of the Council of the [[University of Newcastle upon Tyne]] and a patron of the Second World War Experience Centre. He was also actively involved in his local [[Royal Air Forces Association]].<ref name="Northumberland Gazette Sudden death " />

==Personal life==
In 1959, while living in [[London]], Sir John met his wife Merrill, who was a nurse. They married in 1960. They had five [[children]] together, Johnathon, David, Kate, Rachel, and Rob. Sir John had eleven grandchildren: Rebecca, Micheal, Gregory, William, Joe, Ben, Finn, Ella, Millie, George and Sarah.<ref name="Northumberland Gazette Sudden death ">{{cite news|url=http://www.northumberlandgazette.co.uk/news/Sudden-death-of-outstanding-military.3659014.jp|title=Sudden death of outstanding military ace|date=10 January 2008 |publisher=Northumberland Gazette|accessdate=18 March 2009}}</ref>

==References==
{{reflist|30em}}

==External links==
*[http://www.ncl.ac.uk/press.office/newslink/honorary_fellows_oct06.doc Honorary Fellowship of Newcastle University]
*[http://www.timesonline.co.uk/tol/comment/obituaries/article3240231.ece Obituary in ''The Times'', 24 January 2008]

{{s-start}}
{{s-mil}}
|-
{{s-bef|before=[[John Thomson (RAF officer)|Sir John Thomson]]}}
{{s-ttl|title=[[RAF Support Command|Commander-in-Chief, Support Command]]|years=1992–1994}}
{{s-non|reason=Command split up into<br>[[RAF Logistics Command|Logistics Command]] under [[Michael Alcock|Sir Michael Alcock]]<br>[[RAF Personnel and Training Command|Personnel and Training Command]] under [[Andrew Wilson (RAF officer)|Sir Andrew Wilson]]}}
|-
{{succession box|title=[[Vice-Chief of the Defence Staff (United Kingdom)|Vice-Chief of the Defence Staff]]|before=[[Jock Slater|Sir Jock Slater]] |after=[[Peter Abbott|Sir Peter Abbott]]|years=1995–1997}}
{{s-end}}

{{DEFAULTSORT:Willis, John}}
[[Category:Royal Air Force air marshals]]
[[Category:Knights Grand Cross of the Order of the British Empire]]
[[Category:Knights Commander of the Order of the Bath]]
[[Category:Fellows of the Royal Aeronautical Society]]
[[Category:Graduates of the Royal Air Force College Cranwell]]
[[Category:People educated at Dulwich College]]
[[Category:People associated with Newcastle University]]
[[Category:1937 births]]
[[Category:2008 deaths]]