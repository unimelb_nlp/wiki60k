{{Infobox NCAA team season
  |Year=1919
  |Team=Alabama Crimson Tide
  |Image=
  |ImageSize=
  |Conference=Southern Intercollegiate Athletic Association
  |ShortConference=SIAA
  |Record=8–1
  |ConfRecord=6–1
  |HeadCoach=[[Xen C. Scott]]
  |HCYear = 1st
  |Captain =[[Ike Rogers]]
  |StadiumArena=[[Denny Field (Alabama)|University Field]]<br>[[Rickwood Field]]
}}
{{1919 SIAA football standings}}
The '''1919 Alabama Crimson Tide football team''' (variously "Alabama", "UA" or "Bama") represented the [[University of Alabama]] in the [[1919 college football season]]. It was the Crimson Tide's 26th overall and 23rd season as a member of the [[Southern Intercollegiate Athletic Association]] (SIAA). The team was led by head coach [[Xen C. Scott]], in his first year, and played their home games at [[Denny Field (Alabama)|University Field]] in [[Tuscaloosa, Alabama|Tuscaloosa]] and at [[Rickwood Field]] in [[Birmingham, Alabama|Birmingham]], [[Alabama]]. They finished the season with a record of eight wins and one loss (8–1 overall, 6–1 in the SIAA).

After not fielding a team for the [[1918 Alabama Crimson Tide football team|1918 season]] due to the effects of [[World War I]], in May 1919 [[Xen C. Scott]] was hired to serve as head coach of the Crimson Tide. Alabama then opened the season with four consecutive [[shutout]] victories at University Field in Tuscaloosa. After Scott defeated {{cfb link|year=1919|team=Birmingham–Southern Panthers|title=Birmingham–Southern}} in his debut as Crimson Tide head coach, the next week he defeated {{cfb link|year=1919|team=Ole Miss Rebels|title=Ole Miss}} for his first SIAA victory. After a pair of [[Blowout (sports)|blowout victories]] over both {{cfb link|year=1919|team=Howard Bulldogs|title=Howard}} and the [[Marion Military Institute]], Alabama defeated [[1919 Sewanee Tigers football team|Sewanee]] 40–0 in what was the most anticipated game of the season at Rickwood Field.

After the Sewanee win, Alabama traveled to Nashville where they lost their only game of the season against [[1919 Vanderbilt Commodores football team|Vanderbilt]] 16–12. After the loss, the Crimson Tide rebounded with wins at [[1919 LSU Tigers football team|LSU]] and [[1919 Georgia Bulldogs football team|Georgia]] and at Birmingham over [[1919 Mississippi A&M Aggies football team|Mississippi A&M]] on Thanksgiving to close the season.

==Before the season==
After the departure of [[Thomas Kelley (coach)|Thomas Kelley]] as head coach of the Crimson Tide following their [[1917 Alabama Crimson Tide football team|1917 season]], then [[athletic director]] [[B. L. Noojin]] was chosen as his successor.<ref name="Noojin1918">{{cite news |title=Noojin to coach Crimson next year |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=December 20, 1917 |page=2}}</ref> However Noojin never led the team as head coach since the [[1918 Alabama Crimson Tide football team|1918 season]] was canceled due to the effects of [[World War I]].<ref name="WWI">{{cite news |title=Intercollegiate football abandoned at University |page=1 |url=https://news.google.com/newspapers?id=olQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=2377%2C2159822 |newspaper=The Tuscaloosa News |location=Tuscaloosa, Alabama |publisher=Google News |date=August 23, 1943 |accessdate=September 17, 2013}}</ref> When football was reinstated for the next season, [[Xen C. Scott]] was hired to serve as head coach in May 1919.<ref name="ScottHire1">{{cite news |title=Xen Scott to coach Alabama football team next season |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=May 27, 1919 |page=10}}</ref> Scott had previously served as head coach of the Cleveland Naval Reserve team that upset the [[College football national championships in NCAA Division I FBS|national champion]] [[Pittsburgh Panthers football|Pittsburgh Panthers]] to close their [[1918 Pittsburgh Panthers football team|1918 season]].<ref name="ScottHire1"/> Scott had also previously served as head coach for both [[Western Reserve University]] (1910) and the [[Case Institute of Technology]] (1911–1913) in [[Cleveland]].<ref name="ScottHire1"/>

Scott opened his first fall practice on September 1.<ref name="Pre1">{{cite news |title=Alabama football team is training |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=September 6, 1919 |page=5}}</ref> At that time, ten players from previous Alabama squads returned and included T. L. Brown, Jack Hovater, Walter E. Hovater, Ralph Lee Jones, [[Mullie Lenoir]], Emmet Noland, J. T. O'Connor, [[Ike Rogers]], [[Tram Sessions]] and [[Riggs Stephenson]].<ref name="Pre1"/> After two weeks of practice, Scott divided the players into four teams in order to determine starting line-ups. At his time Scott also did not utilize a [[quarterback]], but instead would simply [[Snap (American and Canadian football)|snap]] the ball directly to the runner.<ref name="Pre2">{{cite news |title=University men getting started in football work |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=September 14, 1919 |page=9}}</ref> Before game preparation began for their game against Birmingham–Southern, Ike Rogers was selected as [[Captain (sports)|team captain]] for the season by the returning lettermen on September 25.<ref name="Pre3">{{cite news |title=Rogers is elected captain of state university team |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=September 26, 1919 |page=10}}</ref> Rogers was previously elected to serve as team captain for the 1918 season that was cancelled.<ref name="Pre3"/>

==Schedule==
{{CFB Schedule Start | time = no | rank = no | ranklink = no | rankyear = no | tv = no | attend = yes}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 4
| nonconf      = yes
| time         = no
| rank         = no
| opponent     = {{cfb link|year=1919|team=Birmingham–Southern Panthers|title=Birmingham–Southern}}
| site_stadium = [[Denny Field (Alabama)|University Field]]
| site_cityst  = [[Tuscaloosa, Alabama|Tuscaloosa, AL]]
| tv           = no
| score        = 27–0
| attend       = 2,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 11
| time         = no
| rank         = no
| opponent     = {{cfb link|year=1919|team=Ole Miss Rebels|title=Ole Miss}}
| site_stadium = University Field
| site_cityst  = Tuscaloosa, AL
| gamename     = [[Alabama–Ole Miss football rivalry|Rivalry]]
| tv           = no
| score        = 49–0
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 18
| time         = no
| rank         = no
| opponent     = {{cfb link|year=1919|team=Howard Bulldogs|title=Howard}}
| site_stadium = University Field
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 48–0
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 24
| nonconf      = yes
| time         = no
| rank         = no
| opponent     = [[Marion Military Institute]]
| site_stadium = University Field
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 61–0
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 1
| time         = no
| rank         = no
| opponent     = [[1919 Sewanee Tigers football team|Sewanee]]
| site_stadium = [[Rickwood Field]]
| site_cityst  = [[Birmingham, Alabama|Birmingham, AL]]
| tv           = no
| score        = 40–0
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = l
| date         = November 8
| away         = yes
| time         = no
| rank         = no
| opponent     = [[1919 Vanderbilt Commodores football team|Vanderbilt]]
| site_stadium = [[Vanderbilt Stadium#Old Dudley Field|Dudley Field]]
| site_cityst  = [[Nashville, Tennessee|Nashville, TN]]
| tv           = no
| score        = 16–12
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 15
| away         = yes
| time         = no
| rank         = no
| opponent     = [[1919 LSU Tigers football team|LSU]]
| site_stadium = [[State Field]]
| site_cityst  = [[Baton Rouge, Louisiana|Baton Rouge, LA]]
| gamename     = [[Alabama–LSU football rivalry|Rivalry]]
| tv           = no
| score        = 23–0
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 22
| away         = yes
| time         = no
| rank         = no
| opponent     = [[1919 Georgia Bulldogs football team|Georgia]]
| site_stadium = [[Ponce de Leon Park]]
| site_cityst  = [[Atlanta|Atlanta, GA]]
| tv           = no
| score        = 6–0
| attend       = 10,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 27
| time         = no
| rank         = no
| opponent     = [[1919 Mississippi A&M Aggies football team|Mississippi A&M]]
| site_stadium = Rickwood Field
| site_cityst  = Birmingham, AL
| gamename     = [[Alabama–Mississippi State football rivalry|Rivalry]]
| tv           = no
| score        = 14–6
| attend       = 6,000
}}
{{CFB Schedule End
| rank     = no
| timezone = [[Central Time Zone (North America)|Central Time]]
| hc       = no
}}
*<small>Source: Rolltide.com: 1919 Alabama football schedule<ref name="1919schedule">{{cite web| url=http://www.rolltide.com/sports/2016/6/10/sports-m-footbl-archive-m-footbl-archive-1919-html.aspx?id=258 |title=1919 Alabama football schedule |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics| accessdate=February 29, 2012}}</ref></small>

==Game summaries==

===Birmingham–Southern===
{{AFB game box start
|Visitor=BSC
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=6 |H2=7 |H3=7 |H4=7
|Date=October 4
|Location=University Field<br>Tuscaloosa, AL
|Attendance=2,000
}}
*'''Source:'''<ref name="BSC3">{{cite news |title=Panther warriors lose to University |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 5, 1919 |page=13}}</ref>
{{AFB game box end}}
Nearly after two years since their previous home game during the [[1917 Alabama Crimson Tide football team|1917 season]], Alabama opened the 1919 season against [[Birmingham–Southern College|Birmingham–Southern]] and [[shutout]] the [[Birmingham–Southern Panthers football|Panthers]] 27–0 in the first all-time game between the schools.<ref name="BSC3"/><ref>{{cite news |title=University team getting into shape |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 1, 1919 |page=15}}</ref><ref name=a1>1919 Season Recap</ref><ref name="BSCAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Birmingham–Southern |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=340 |accessdate=October 1, 2013}}</ref> The [[Kickoff (gridiron football)|opening kickoff]] was at 1:30 and was played in a newly expanded University Field with seating for 800 spectators.<ref>{{cite news |title=Crimson and White ready for blow-off |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 4, 1919 |page=6}}</ref> Mullie Lenoir starred in the game for Alabama as he scored a touchdown in each of the first three quarters that gave the Crimson White a 20–0 lead.<ref name="BSC3"/> [[Charles Bartlett (American football)|Charles Bartlett]] scored the final points of the game with his fourth quarter touchdown that made the final score 27–0.<ref name="BSC3"/>

===Ole Miss===
{{AFB game box start
|Visitor=Ole Miss
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=6 |H2=12 |H3=25 |H4=6
|Date=October 11
|Location=University Field<br>Tuscaloosa, AL
|Attendance=
}}
*'''Sources:'''<ref name="OM1">{{cite news |title=Mississippians swamped by the Alabama eleven |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 12, 1919 |page=10}}</ref><ref name="OM2">{{cite news |title=Ole Miss loses to Alabama 50 to 0 |publisher=NewsBank: America's Historical Newspapers |newspaper=The New Orleans Item |date=October 12, 1919 |page=4}}</ref>
{{AFB game box end}}
In their second game, Alabama [[shutout]] their SIAA [[Alabama–Ole Miss football rivalry|rival]], the [[University of Mississippi|Ole Miss]] [[Ole Miss Rebels football|Rebels]] 49–0 at Tuscaloosa.<ref name=a1/><ref name="OM1"/><ref name="OM2"/> After being held scoreless for the first ten minutes, Alabama scored their first touchdown on Mullie Lenoir run late in the quarter. A pair of second quarter touchdown runs from first [[Riggs Stephenson]] and then by Charles Bartlett that made the halftime score 18–0.<ref name="OM1"/><ref name="OM2"/> In the third quarter, the Crimson Tide scored 25 points and included a pair of long touchdown scores. The long scores came first on a 76-yard Lenoir run and the second on a 65-yard [[interception]] return by Stephenson.<ref name="OM2"/> Lenoir and Stephenson each scored another touchdown as did J. T. O'Connor and make the final score 49–0.<ref name="OM1"/><ref name="OM2"/> The victory improved Alabama's all-time record against Ole Miss to 9–2–1.<ref name="OMAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Mississippi |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2039 |accessdate=October 1, 2013}}</ref>

===Howard===
{{AFB game box start
|Visitor=Howard
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=21 |H2=6 |H3=7 |H4=14
|Date=October 18
|Location=University Field<br>Tuscaloosa, AL
|Attendance=
}}
*'''Source:'''<ref name="HOW1">{{cite news |title=Crimson White cracks Howard line at will; Wins by score of 48–0 |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 19, 1919 |page=1}}</ref>
{{AFB game box end}}
Behind a strong [[Forward pass|passing game]], Alabama defeated the Howard (now known as [[Samford University]]) [[Howard Bulldogs football|Bulldogs]] 48–0 at Tuscaloosa.<ref name=a1/><ref name="HOW1"/> Touchdowns in the game were scored twice each by Mullie Lenoir] and Riggs Stephenson and one apiece by Isaac M. Boone, [[Tram Sessions]] and [[Joe Sewell]].<ref name="HOW1"/> In the game, the Crimson offense was dominant both running and passing the ball in the victory.<ref name="HOW1"/> The victory improved Alabama's all-time record against Howard to 8–0.<ref name="HOWAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Samford (AL) |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2867 |accessdate=October 1, 2013}}</ref>
{{clear}}

===Marion Military Institute===
{{AFB game box start
|Visitor=Marion
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=7 |H2=20 |H3=2 |H4=32
|Date=October 24
|Location=University Field<br>Tuscaloosa, AL
|Attendance=
}}
*'''Source:'''<ref name="MMI1">{{cite news |title=University team romps on Marion |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 25, 1919 |page=5}}</ref>
{{AFB game box end}}
As they entered their game against the [[Marion Military Institute]], many of the Alabama supporters viewed the game against the Cadets as just a practice game before their anticipated match-up against [[Sewanee Tigers football|Sewanee]].<ref name="MMI2">{{cite news |title=Alabama priming for Sewanee Tigers |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=October 22, 1919 |page=12}}</ref> On a Friday afternoon, Alabama defeated the Cadets 61–0 at Tuscaloosa for their fourth consecutive shutout to open the season.<ref name=a1/><ref name="MMI1"/> Touchdowns in the game were scored three times by J. H. Emmett, twice each by [[Charles Bartlett (American football)|Charles Bartlett]] and Mullie Lenoir, and one apiece by Walter E. Hovater and Morgan.<ref name="MMI1"/> The victory improved Alabama's all-time record against Marion to 6–0.<ref name="MMIAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Marion Military Institute (AL) |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1874 |accessdate=October 1, 2013}}</ref>
{{clear}}

===Sewanee===
{{AFB game box start
|Visitor=Sewanee
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=7 |H2=7 |H3=12 |H4=14
|Date=November 1
|Location=Rickwood Field<br>Birmingham, AL
|Attendance=
}}
*'''Source:'''<ref name="SEW1">{{cite news |title=Alabama overwhelms Sewanee elevens and wins by score 40–0 |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=November 2, 1919 |page=1}}</ref>
{{AFB game box end}}
In what was the most anticipated game of the season, the entire University population and educators traveled to Birmingham for their game against [[Sewanee: The University of the South|Sewanee]].<ref name="SEW2">{{cite news |title=Alabama is now preparing for Sewanee |publisher=NewsBank: America's Historical Newspapers |newspaper=Columbus Enquirer-Sun |date=October 28, 1919 |page=5}}</ref>
In the game Alabama defeated the [[1919 Sewanee Tigers football team|Tigers]] 40–0 at [[Rickwood Field]], in the largest margin of victory ever for Alabama over Sewanee to date.<ref name=a1/><ref name="SEW1"/> Alabama took an early 7–0 lead in the first quarter on a 15-yard [[Riggs Stephenson]] touchdown run and then extended it to 14–0 at halftime on a 45-yard Walter E. Hovater touchdown run in the second.<ref name="SEW1"/> Alabama then closed the game with four Mullie Lenoir touchdown runs, two in the third and two in the fourth quarter.<ref name="SEW1"/> The victory improved Alabama's all-time record against Sewanee to 3–9–2.<ref name="SEWAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Sewanee (TN) |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2947 |accessdate=October 1, 2013}}</ref>
{{clear}}

===Vanderbilt===
{{AFB game box start
|Visitor=Alabama
|V1=0 |V2=0 |V3=6 |V4=6
|Host='''Vanderbilt'''
|H1=0 |H2=13 |H3=0 |H4=3
|Date=November 8
|Location=Dudley Field<br>Nashville, TN
|Attendance=
}}
*'''Source:'''<ref name="VU1">{{cite news |title=Alabama is defeated by own fumbles |publisher=NewsBank: America's Historical Newspapers |newspaper=The New Orleans Item |date=November 9, 1919}}</ref>
{{AFB game box end}}
A week after their victory over Sewanee, Alabama traveled to Nashville where they were defeated by the [[Vanderbilt University|Vanderbilt]] [[1919 Vanderbilt Commodores football team|Commodores]] 16–12 on a muddy field for their only loss of the season.<ref name=a1/><ref name="VU1"/> On their first drive of the game, Alabama took the ball to the Vanderbilt two-yard line, but then [[fumble]]d the ball that was recovered by [[Josh Cody]] of the Commodores to end the scoring threat.<ref name="VU1"/> The second Alabama fumble resulted in the first touchdown of the game. Early in the second quarter, [[Riggs Stephenson]] fumbled the ball that was recovered by [[Tom Zerfoss|Tommy Zerfoss]] and returned 35-yards for a 7–0 Vanderbilt lead.<ref name="VU1"/> They further extended their lead to 13–0 at halftime on a 20-yard [[Grailey Berryhill]] touchdown run.<ref name="VU1"/>

Alabama rallied in the second half with a pair of two-yards Stephenson touchdown runs in the third and fourth quarter that made the score 13–12.<ref name="VU1"/> Cody then provided for the final margin in the 16–12 Commodores' victory with his 30-yard [[Field goal (American and Canadian football)|field goal]] in the fourth quarter.<ref name="VU1"/> The loss brought Alabama's all-time record against Vanderbilt to 0–5.<ref name="VUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Vanderbilt (TN) |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3363 |accessdate=October 1, 2013}}</ref>

The starting lineup was J. Hovater (left end), Brown (left tackle), Johnson (left guard), Sessions (center), Jones (right guard), Rogers (right tackle), Boone (right end), W. Hovater (quarterback), Lenoir (left halfback), O'Connor (right halfback), Stephenson (fullback).<ref>{{Harvnb|Woodruff|1928|page=96}}</ref>

===LSU===
{{AFB game box start
|Visitor='''Alabama'''
|V1=0 |V2=0 |V3=3 |V4=20
|Host=LSU
|H1=0 |H2=0 |H3=0 |H4=0
|Date=November 15
|Location=State Field<br>Baton Rouge, LA
|Attendance=
}}
*'''Source:'''<ref name="LSU1">{{cite news |title=Alabama beats LSU 23–0 |publisher=NewsBank: America's Historical Newspapers |newspaper=New Orleans States |date=November 16, 1919 |page=18}}</ref>
{{AFB game box end}}
After they suffered their only loss of the season at Vanderbilt, Alabama traveled to Baton Rouge where they shutout the [[Louisiana State University|LSU]] [[1919 LSU Tigers football team|Tigers]] 23–0.<ref name=a1/><ref name="LSU1"/> After a scoreless first half that saw two drives end inside the Tigers ten-yard line due to [[fumble]]s, Alabama took a 3–0 lead in the third quarter on a 20-yard [[Joe Sewell]] [[Field goal (American and Canadian football)|field goal]].<ref name="LSU1"/> The Crimson Tide sealed the win with three touchdowns in the fourth quarter scored on short runs by Sewell, Mullie Lenoir and Riggs Stephenson.<ref name="LSU1"/> The victory improved Alabama's all-time record against LSU to 4–3.<ref name="LSUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs LSU |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1793 |accessdate=October 1, 2013}}</ref>

===Georgia===
{{AFB game box start
|Visitor='''Alabama'''
|V1=3 |V2=3 |V3=0 |V4=0
|Host=Georgia
|H1=0 |H2=0 |H3=0 |H4=0
|Date=November 22
|Location=Ponce de Leon Park<br>Atlanta, GA
|Attendance=10,000
}}
*'''Sources:'''<ref name="GA1">{{cite news |title=O'Connor's educated toe defeated Red and Blacks |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=November 23, 1919 |page=11}}</ref><ref name="GA2">{{cite news |title=O'Connor kicks two field goals, winning game for Alabama 6–0 |publisher=NewsBank: America's Historical Newspapers |newspaper=Columbus Enquirer-Sun |date=November 23, 1919 |page=1}}</ref>
{{AFB game box end}}
After their road win at LSU, Alabama traveled to Atlanta where they shutout the [[University of Georgia|Georgia]] [[1919 Georgia Bulldogs football team|Bulldogs]] 6–0.<ref name=a1/><ref name="GA1"/><ref name="GA2"/> The only points in the game came on a pair of J. T. O'Connor [[Field goal (American and Canadian football)|field goals]]. The first was from 45-yards in the first and the second from 25-yards in the second quarter.<ref name="GA1"/><ref name="GA2"/> Both teams played strong defense throughout the game, and Georgia nearly pulled out a win when [[Buck Cheves]] [[Interception|intercepted]] an Alabama pass in the final seconds of the game and made a sizable return before he was tackled by the Crimson Tide.<ref name="GA1"/><ref name="GA2"/> The victory improved Alabama's all-time record against Georgia to 4–7–3.<ref name="GAAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Georgia |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1265 |accessdate=October 1, 2013}}</ref>

The starting lineup was J. Hovater (left end), Brown (left tackle), Johnson (left guard), Sessions (center), Jones (right guard), Rogers (right tackle), Boone (right end), W. Hovater (quarterback), O'Connor (left halfback), Lenoir (right halfback), Stevenson (fullback).<ref>{{Harvnb|Woodruff|1928|page=101}}</ref>

===Mississippi A&M===
{{AFB game box start
|Visitor=Mississippi A&M
|V1=0 |V2=0 |V3=6 |V4=0
|Host='''Alabama'''
|H1=0 |H2=0 |H3=7 |H4=7
|Date=November 27
|Location=Rickwood Field<br>Birmingham, AL
|Attendance=6,000
}}
*'''Source:'''<ref name="MSA1">{{cite news |title=Alabama winner over Mississippi in annual contest |publisher=NewsBank: America's Historical Newspapers |newspaper=The Montgomery Advertiser |date=November 28, 1919 |page=1}}</ref>
{{AFB game box end}}
In their final game of the season, Alabama defeated the Mississippi A&M (now known as [[Mississippi State University]]) [[1919 Mississippi A&M Aggies football team|Aggies]] 14–6 on [[Thanksgiving]] at Rickwood Field.<ref name=a1/><ref name="MSA1"/> After a scoreless first half, H. S. Little scored the Aggies' only points of the game with his 80-yard [[Kickoff (gridiron football)|kickoff return]] that opened the third quarter.<ref name="MSA1"/> Alabama then took the lead later in the third on a short [[Riggs Stephenson]] touchdown run.<ref name="MSA1"/> They then made the final score 14–6 in the fourth after T. L. Brown blocked an A&M [[Punt (gridiron football)|punt]] that was recovered by Ike Rogers in the endzone for a touchdown.<ref name="MSA1"/> The victory improved Alabama's all-time record against Mississippi A&M to 7–4–1.<ref name="MSAAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Mississippi State |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2049 |accessdate=October 1, 2013}}</ref>

==Postseason==
Alabama won eight games in a season for the first time, and was awarded by some organizations a share of the SIAA title.<ref name=k1>Kordic, p. xiii</ref>{{#tag:ref|The SIAA did not award football championships officially by the conference. As such, any claimed SIAA titles are unofficial.<ref name="SIAA1">{{cite news |first=Roger |last=Saylor |title=Southern Intercollegiate Athletic Association |url=http://library.la84.org/SportsLibrary/CFHSN/CFHSNv06/CFHSNv06n2g.pdf |publisher=LA84 Foundation |newspaper=College Football Historical Society |date=February 1993 |format=PDF |accessdate=October 1, 2013}}</ref><ref name="SIAA2">{{cite web |url=http://homepages.cae.wisc.edu/~dwilson/rfsc/champs/Southern.txt |title=Champions of the Southern Intercollegiate Athletic Association (1895–1921) |author=David Krysakowski |accessdate=October 1, 2013}}</ref>|group="A"}} [[Fuzzy Woodruff]] recalls "Auburn claimed it. "We defeated Tech" said Auburn. "Yes, but we defeated you" said Vanderbilt. "Yes", said Alabama, "but Tech, Tulane ,and Tennessee took your measure. We defeated Gergia Tech, who tied Tulane, so we are champions...The newspapers, however, more or less generally supported the claim of Auburn..."<ref>{{Harvnb|Woodruff|1928|page=105}}</ref>

==Personnel==
{{Col-begin}}
{{Col-2}}

===Varsity letter winners===
====Line====
{|class="wikitable"
|-
{{CollegePrimaryHeader|team=Alabama Crimson Tide| Player |Hometown|Position|Games<br>started|Prep school|Height|Weight|Age}}
|-
| T. L. Brown
| [[Jasper, Alabama]]
| [[Tackle (American football)|Tackle]]
|-
| [[Dan Boone (baseball)|Alfred Morgan "Dan" Boone]]
| [[Samantha, Alabama]]
| [[End (American football)|End]]
|-
| [[Ike Boone]]
| [[Samantha, Alabama]]
| End
|
|
|6'0"
|190
|-
| J. H. Emmett
| [[Albertville, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| E. P. Hood
| [[Birmingham, Alabama]]
| Tackle
|-
| Sidney Johnston
| [[Athens, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Ralph Lee Jones
| [[Jones Mills, Alabama]]
| Guard
|-
| [[Ike Rogers]]
| [[Vina, Alabama]]
| Tackle
|
|[[University of North Alabama|Florence Normal School]]
|
|185
|-
| Harry Rowe
| [[Elba, Alabama]]
| Guard
|-
| [[Tram Sessions]]
| [[Birmingham, Alabama]]
| [[Center (American football)|Center]]
|-
| Frank Boyd Thomason
| [[Albertville, Alabama]]
| End
|-
|}

====Backfield====
{|class="wikitable"
|-
{{CollegePrimaryHeader|team=Alabama Crimson Tide| Player |Hometown|Position|Games<br>started|Prep school|Height|Weight|Age}}

|-
| [[Jack Hovater]]
| [[Russellville, Alabama]]
| Back/End
|
|
|
|190
|-
| Walter E. Hovater
| [[Russellville, Alabama]]
| Back
|-
| [[Mullie Lenoir]]
| [[Marlin, Texas]]
| [[Halfback (American football)|Halfback]]
|
|
|
|144
|-
| J. T. O'Connor
| [[St. Louis, Missouri]]
| Back
|-
| [[Joe Sewell]]
| [[Titus, Alabama]]
| Halfback
|
|
|5'6"
|155
|-
| [[Luke Sewell]]
| [[Titus, Alabama]]
| [[Quarterback]]
|
|
|5'9"
|160
|-
| [[Riggs Stephenson]]
| [[Akron, Alabama]]
| [[Fullback (American football)|Fullback]]
|
|
|5'10"
|185
|-
|}<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Tide Football Lettermen |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=127–141}}</ref>

===Coaching staff===
{|class="wikitable"
|-
{{CollegePrimaryHeader|team=Alabama Crimson Tide| Name|Position|Seasons at<br>Alabama|Alma mater}}
|-
| [[Xen C. Scott]] || [[Head coach]] || 1 || 
|-
| Alfred Morgan Boone || Assistant coach || 1 || [[Alabama Crimson Tide football|Alabama]] (1919)
|-
| [[Adrian Van de Graaff]] || Assistant coach ||1 || [[Alabama Crimson Tide football|Alabama]] (1914)
|-
|}<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Assistant Coaches |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=142–143}}</ref>

==Notes==
{{reflist|group=A}}

==References==
'''General'''
{{refbegin}}
* {{cite book |last=Kordic |first=Gregory |title=A Damn Good Yankee: Xen Scott and the Rise of the Crimson Tide |year=2007 |publisher=[[AuthorHouse]] |location=Bloomington, Indiana |isbn=978-1-4259-6018-6}}
*{{cite book|title=A History of Southern Football 1890–1928|last=Woodruff|first=Fuzzy|volume=2|year=1928|ref=harv}}
* {{cite web |url=http://grfx.cstv.com/schools/alab/graphics/docs/19-m-footbl-recaps.pdf |title=1919 Season Recap |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics |accessdate=October 1, 2013 |format=PDF}}
* {{cite web |url=http://grfx.cstv.com/photos/schools/alab/sports/m-footbl/auto_pdf/2012-13/misc_non_event/2012FootballRecordBook.pdf |title=2012 Alabama Crimson Tide Football Record Book |year=2012 |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |accessdate=October 1, 2013 |format=PDF}}
{{refend}}

'''Specific'''
{{Reflist|30em}}

{{Alabama Crimson Tide football navbox}}

[[Category:1919 Southern Intercollegiate Athletic Association football season|Alabam]]
[[Category:1919 in Alabama|Crimson Tide]]
[[Category:Alabama Crimson Tide football seasons]]