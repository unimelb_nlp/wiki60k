{{COI|date=March 2017}}
{{refimprove|date=March 2017}}
{{advert|date=March 2017}}
{{Infobox military unit
|unit_name=Gator Guard Drill Team
|image=
[[File:Gator guard crest black v2.png|220px]]
|caption=
|dates=1953 – present
|country={{flagu|United States}}
|allegiance=
|branch=
|type=[[Drill Team]]/[[Color Guard]]
|role=
|size=
|command_structure=
|garrison=[[Van Fleet Hall (Gainesville, Florida)]] 
|garrison_label=Headquarters
|nickname=Gator Guard
|patron=
|motto="All The Damn Time"
|colors=
|colors_label=
|march=
|mascot=
|equipment=
|equipment_label=
|battles=
|anniversaries=
|decorations=
|battle_honours=
<!-- Commanders -->
|commander1=Commander Gregory S. Davis
|commander1_label=Commander
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=
|identification_symbol_2_label=
}}

The '''Gator Guard Drill Team''', abbreviated '''GGDT''', is a precision [[drill team]] and military [[fraternity]] based at the [[University of Florida]]. The team is named after the [[Florida Gators]], the mascot of the University of Florida. Founded in 1953, the Gator Guard is most widely known for its annual performances in the [[New Orleans Mardi Gras]] Parades.
In 2016, the team consisted of 31 active male and female members.{{cn|date=March 2017}}
 
== Unit history and background ==
[[File:Gator guard mardi gras 2016.png|300px|thumb|left|Gator guard performing at Mardi Gras parades, 2016.]]
The Gator Guard Drill Team was founded in 1953 at the [[University of Florida]].{{cn|date=March 2017}} The drill team was formed by cadets from the [[University of Florida ROTC]] program. The Gator Guard performs annually at the University of Florida's [[Homecoming]] Parade, as well as the Krewe of Mid-City and King Rex parades at the [[New Orleans Mardi Gras]].<ref>http://www.gainesville.com/news/20160205/gator-drill-team-ready-to-head-to-tigers-den</ref><ref>https://news.google.com/newspapers?nid=1356&dat=19651128&id=Xc1NAAAAIBAJ&sjid=tg4EAAAAIBAJ&pg=2104,3600336&hl=en</ref> The Drill Team uses M1903 rifles with 8-inch bayonets  for all performances.<ref>https://thepatriotpress.wordpress.com/2015/02/</ref> In addition, the Gator Guard also performs [[Color Guard]] ceremonies for the University of Florida, the [[Southeastern Conference|SEC]], and the [[MLB]]. The Gator Guard was modeled after the [[3rd United States Infantry Regiment]], "The Old Guard." The team makes an annual trip to [[Washington D.C.]] to train with The Old Guard.

The Gator Guard absorbed and succeeded the University of Florida's Army ROTC chapter of the [[Pershing Rifles]] upon its inception. The founding class of the Gator Guard consisted of 16 cadets in 1953. The team's membership would see an all-time high of 72 members in 1957.{{cn|date=March 2017}}

In many universities across the United States, ROTC participation was compulsory. This policy would be revoked in the 1960s, following [[opposition to U.S. involvement in the Vietnam War]], in favor of voluntary programs.<ref>http://www.fsm-a.org/stacks/AP_files/APCompulsROTC.html</ref> This move from compulsory to voluntary participation in the UF ROTC program would cause a substantial decrease in enrollment of members, both in the ROTC program and in the Gator Guard Drill Team. This is largely why the team's enrollment peaked in 1957. From 1963 to 1972 the Gator Guard would hold an unprecedented nine year winning streak as the "Best Drill Unit" in the All-Florida Invitational Drill Meet, held in [[Sarasota, Fl]].

On August 18, 1976, an alumnus of the Gator Guard, Mark T. Barrett, assigned to [[United States Forces Korea]] would tragically be at the center of the [[Axe Murder Incident]]. Assigned to the [[Joint Security Area]] of the [[Demilitarized Zone]], LT. Barrett would be part of a detail sent to cut down a poplar tree that had been blocking the view of the [[United Nations]] observers. While in the process of cutting down the tree, the detail was assaulted by North Koreans and LT. Mark Barrett was killed. The incident would be recognized internationally, and had the potential to spark an escalation of conflict on the Korean peninsula. A memorial for LT Barrett currently stands in front of [[Van Fleet Hall (Gainesville, Florida)]]. The Gator Guard performs an annual changing of the Guard ceremony over the course of 24 hours over Mark T. Barrett's memorial, in similar fashion to [[3rd U.S. Infantry Regiment (The Old Guard)]] [[Tomb of the Unknown Soldier]] changing of the guard.

In the 1970s, women would gain opportunities for entrance into ROTC programs, as the Army opened up positions in [[Field Artillery]] and the [[Ordinance Corps]] to female officers.<ref>http://sill-www.army.mil/FAMAG/1978/JUL_AUG_1978/JUL_AUG_1978_PAGES_40_43.pdf</ref> Women coming into the Army through ROTC dovetailed with the elimination of the [[Women's Army Corps]]. The Gator Guard, having been an exclusively male team, would also see the entrance of female members in the coveted position of drillers, who would march and execute precision drill movements in parade as part of the team.{{cn|date=March 2017}} The first female cadet to march with the drill team was during the 1973-1974 school year was Carmen Parrot. She was allowed to carry the Guidon during parades. The first female commander of the drill team would be Nancy A. Oxer in 1979. The first female GGDT member to be commissioned was Bonnie Chandler.

Between 2010 and 2015, the Gator Guard helped to create and train teams at other Florida universities modeled after the Gator Guard Drill Team. By 2015, the Gator Guard had successfully set up the 'Osprey Guard' at the [[University of North Florida]], and the 'Seminole Guard' at [[Florida State University]]. 

In 2016, the team consisted of 31 active male and female members.{{cn|date=March 2017}}

==References==
{{Reflist}}<!--added above External links/Sources by script-assisted edit-->



{{University of Florida}}

https://orgs.studentinvolvement.ufl.edu/Organization/Gator-Guard-Drill-Team

{{DEFAULTSORT:Gator Guard Drill Team}}
[[Category:University of Florida]]