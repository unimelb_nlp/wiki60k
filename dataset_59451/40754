{{Orphan|date=June 2012}}

{{enzyme
| Name = Nucleotide pyrophosphatase/phosphodiesterase (NPP)
| EC_number = 3.6.1.9
| image = Nucleotide pyrophosphatase.png
| width = 
| caption = The overall dimeric structure of NPP in ''Xanthomonas axonopodis pv. citri str. 306'' (''Xac'').  This enzyme relies on the catalytic ability of 2 Zn<sup>2+</sup> atoms in the catalytic core, which are shown in white.<ref name="Structural and Functional Comparisons" />
}}

'''Nucleotide pyrophosphatase/phosphodiesterase''' (NPP) is a class of dimeric [[enzymes]] that catalyze the hydrolysis of [[phosphate]] [[diester]] bonds.  NPP belongs to the alkaline phosphatase (AP) superfamily of enzymes.<ref>[http://cmgm.stanford.edu/biochem/herschlag/research-protein-catalysis.html Enzyme Promiscuity, Evolution, and Phosphoryl Transfer.] Herschlag Lab. Stanford University, accessdate 1 March 2012.</ref> Humans express seven known NPP [[isoforms]],<ref>{{Cite journal
| last = Terkeltaub
| first = Robert
| date = 2006-06-01
| title = Physiologic and pathologic functions of the NPP nucleotide pyrophosphatase/phosphodiesterase family focusing on NPP1 in calcification
| url = http://link.springer.com/article/10.1007/s11302-005-5304-3
| journal = Purinergic Signalling
| language = en
| volume = 2
| issue = 2
| pages = 371–377
| doi = 10.1007/s11302-005-5304-3
| issn = 1573-9538
| pmc = 2254483
| pmid = 18404477
}}</ref> some of which prefer [[nucleotide]] [[enzyme substrate (biology)|substrates]], some of which prefer [[phospholipid]] substrates, and others of which prefer substrates that have not yet been determined.<ref>Pham, Truc Chi T.; Wanjala, Irene; Howard, Angela; Parrill, Abby L.; Baker, Daniel L. "Insights into the structure and function of lipid preferring Nucleotide PyrophosphotasePhosphodiesterase isoforms" 2011</ref> In eukaryotes, most NPPs are located in the cell membrane and hydrolyze extracellular phosphate diesters to affect a wide variety of biological processes.<ref>{{cite journal|doi=10.1016/j.tibs.2005.08.005|title=NPP-type ectophosphodiesterases: Unity in diversity|year=2005|last1=Stefan|first1=Cristiana|last2=Jansen|first2=Silvia|last3=Bollen|first3=Mathieu|journal=Trends in Biochemical Sciences|volume=30|issue=10|pages=542–50|pmid=16125936}}</ref><ref name=":3">{{cite journal|doi=10.1016/S0925-4439(03)00058-9|title=Physiological and pathophysiological functions of the ecto-nucleotide pyrophosphatase/phosphodiesterase family|year=2003|last1=Goding|first1=James W.|last2=Grobben|first2=Bert|last3=Slegers|first3=Herman|journal=Biochimica et Biophysica Acta (BBA) - Molecular Basis of Disease|volume=1638|pages=1–19}}</ref> Bacterial NPP is thought to localize to the periplasm.<ref name="Structural and Functional Comparisons">{{cite journal
| last2 = Fenn
| first2 = TD
| last3 = Brunger
| first3 = AT
| last4 = Herschlag
| first4 = D
| year = 2006
| title = Structural and functional comparisons of nucleotide pyrophosphatase/phosphodiesterase and alkaline phosphatase: Implications for mechanism and evolution
| journal = Biochemistry
| volume = 45
| issue = 32
| pages = 9788–803
| doi = 10.1021/bi060847t
| pmid = 16893180
| last1 = Zalatan
| first1 = JG
}}</ref>

[[File:NPPScheme.png|thumb|center|Net reaction scheme for Nucleotide Pyrophosphatase/Phosphodiesterase enzyme, showing a natural NTP substrate (ATP) and a chromophore-producing phosphodiester reagent used for activity assays (methyl ''para''-nitrophenyl phosphate).|560x560px]]

==Structure==
The catalytic site of NPP consists of a two-metal-ion (bimetallo) Zn<sup>2+</sup> catalytic core.  These Zn<sup>2+</sup> catalytic components are thought to stabilize the [[transition state]] of the NPP phosphoryl transfer reaction.<ref>{{cite journal|doi=10.1016/j.jmb.2011.10.040|title=High-Resolution Analysis of Zn<sup>2+</sup> Coordination in the alkaline phosphatase Superfamily by EXAFS and X-ray Crystallography|year=2012|last1=Bobyr|first1=Elena|last2=Lassila|first2=Jonathan K.|last3=Wiersma-Koch|first3=Helen I.|last4=Fenn|first4=Timothy D.|last5=Lee|first5=Jason J.|last6=Nikolic-Hughes|first6=Ivana|last7=Hodgson|first7=Keith O.|last8=Rees|first8=Douglas C.|last9=Hedman|first9=Britt|last10=Herschlag|first10=Daniel|journal=Journal of Molecular Biology|volume=415|pages=102–17|pmid=22056344|issue=1|pmc=3249517|display-authors=8}}</ref>

[[File:Nucleotide pyrophosphatase close up.png|thumb|350x350px|A closer view of the active site of ''Xac'' NPP, which is located on the surface of the subunit. The Zn<sup>2+</sup> atoms of the bimetallo catalytic site are shown by white spheres.<ref name="Structural and Functional Comparisons" />|left]]

[[File:NPPActiveSite.png|thumb|Schematic of ''Xac'' NPP active site in a transition state. Violet denotes the phosphodiester substrate. Red and blue curved arrows represent nucleophilic addition (A<sub>N</sub>) and elimination (D<sub>N</sub>) steps, respectively, of the phosphoryl transfer reaction. These two steps could occur in either order, and probably overlap. The active site's threonine nucleophile is regenerated via hydrolysis. Adapted from.<ref name="Structural and Functional Comparisons" />|alt=Active site schematic|centre|364x364px]]

==Mechanism==

=== Overview ===
NPP catalyses the [[nucleophilic substitution]] of one ester bond on a phosphodiester substrate. It has a nucleoside binding pocket that excludes phospholipid substrates from the active site.<ref>{{Cite journal
| last = Stefan
| first = C.
| last2 = Gijsbers
| first2 = R.
| last3 = Stalmans
| first3 = W.
| last4 = Bollen
| first4 = M.
| date = 1999-05-06
| title = Differential regulation of the expression of nucleotide pyrophosphatases/phosphodiesterases in rat liver
| journal = Biochimica et Biophysica Acta
| volume = 1450
| issue = 1
| pages = 45–52
| issn = 0006-3002
| pmid = 10231554
| doi=10.1016/s0167-4889(99)00031-2
}}</ref> A threonine nucleophile has been identified through site-directed mutagenesis,<ref>{{Cite journal
| last = Belli
| first = Sabina I.
| last2 = Mercuri
| first2 = Francesca A.
| last3 = Sali
| first3 = Adnan
| last4 = Goding
| first4 = James W.
| date = 1995-03-01
| title = Autophosphorylation of PC-1 (Alkaline Phosphodiesterase I/Nucleotide Pyrophosphatase) and Analysis of the Active Site
| url = http://onlinelibrary.wiley.com/doi/10.1111/j.1432-1033.1995.0669m.x/abstract
| journal = European Journal of Biochemistry
| language = en
| volume = 228
| issue = 3
| pages = 669–676
| doi = 10.1111/j.1432-1033.1995.0669m.x
| issn = 1432-1033
}}</ref><ref>{{Cite journal
| last = Lee
| first = Hoi Young
| last2 = Clair
| first2 = Timothy
| last3 = Mulvaney
| first3 = Peter T.
| last4 = Woodhouse
| first4 = Elisa C.
| last5 = Aznavoorian
| first5 = Sadie
| last6 = Liotta
| first6 = Lance A.
| last7 = Stracke
| first7 = Mary L.
| date = 1996-10-04
| title = Stimulation of Tumor Cell Motility Linked to Phosphodiesterase Catalytic Site of Autotaxin
| url = http://www.jbc.org/content/271/40/24408
| journal = Journal of Biological Chemistry
| language = en
| volume = 271
| issue = 40
| pages = 24408–24412
| doi = 10.1074/jbc.271.40.24408
| issn = 0021-9258
| pmid=8798697
}}</ref><ref>{{Cite journal
| last = Gijsbers
| first = R.
| last2 = Ceulemans
| first2 = H.
| last3 = Stalmans
| first3 = W.
| last4 = Bollen
| first4 = M.
| date = 2001-01-12
| title = Structural and catalytic similarities between nucleotide pyrophosphatases/phosphodiesterases and alkaline phosphatases
| journal = The Journal of Biological Chemistry
| volume = 276
| issue = 2
| pages = 1361–1368
| doi = 10.1074/jbc.M007552200
| issn = 0021-9258
| pmid = 11027689
}}</ref> and the reaction inverts the stereochemistry of the phosphorus center.<ref name=":2">{{Cite journal
| last = Bollen
| first = M.
| last2 = Gijsbers
| first2 = R.
| last3 = Ceulemans
| first3 = H.
| last4 = Stalmans
| first4 = W.
| last5 = Stefan
| first5 = C.
| date = 2000-01-01
| title = Nucleotide pyrophosphatases/phosphodiesterases on the move
| journal = Critical Reviews in Biochemistry and Molecular Biology
| volume = 35
| issue = 6
| pages = 393–432
| doi = 10.1080/10409230091169249
| issn = 1040-9238
| pmid = 11202013
}}</ref> The sequence of bond breakage and formation has yet to be resolved.

=== Ongoing Investigation ===
Three extreme possibilities have been proposed for the mechanism of NPP-catalyzed phosphoryl transfer. They are distinguished by the sequence in which bonds to phosphorus are made and broken. Though this phenomenon is subtle, it is important for understanding the physiological roles of AP superfamily enzymes, and also to [[Molecular dynamics|molecular dynamic]] modeling.

<u>Extreme mechanistic scenarios:</u>[[File:NPP More-OFerall-Jenck.png|thumb|'''(a)''' A 2-dimensional [[reaction coordinate]] plot, also known as a [[More-O'Ferall-Jenck diagram]], illustrating possible catalytic mechanisms for NPP. The x and y coordinates represent the reaction coordinates (i.e. progress) of the addition (A<sub>N</sub>) and elimination (D<sub>N</sub>) steps, respectively. To use the plot to visualize a [[potential energy surface]], imagine energy of the system on an axis projecting out of the screen. The shape of the potential energy surface determines the actual mechanism of the enzyme, since the system will proceed from reactants to products along the lowest-energy path.
'''(b)''' Schematics of the [[reaction intermediate]]s and [[transition state]]s produced by each hypothetical path plotted in (a). Note partial charges. 
|550x550px]]'''1)''' A two-step "dissociative" (elimination-addition or D<sub>N</sub> + A<sub>N</sub>) mechanism that proceeds via a trigonal metaphosphate intermediate.<ref name=":0">{{cite journal
| last2 = Zalatan
| first2 = JG
| last3 = Herschlag
| first3 = D
| year = 2011
| title = Biological phosphoryl-transfer reactions: Understanding mechanism and catalysis
| journal = Annual Review of Biochemistry
| volume = 80
| pages = 669–702
| doi = 10.1146/annurev-biochem-060409-092741
| pmc = 3418923
| pmid = 21513457
| last1 = Lassila
| first1 = JK
}}</ref> This mechanism is represented by the red dashed lines in the figure at right.

'''2)''' A two-step "associative" (addition-elimination or A<sub>N</sub> + D<sub>N</sub>) mechanism that proceeds via a pentavalent phosphorane intermediate.<ref name=":0" /> This is represented by the blue dashed lines in the figure at right.

'''3)''' A one-step fully synchronous mechanism analogous to [[SN2 reaction|S<sub>N</sub>2 substitution]]. Bond formation and breakage occur simultaneously and at the same rate. This is represented by the black dashed line in the figure at right.

The above three cases represent archetypes for the reaction mechanism, and the actual mechanism probably falls somewhere in between them.<ref name=":0" /><ref name=":1">{{Cite journal
| last = López-Canut
| first = Violeta
| last2 = Roca
| first2 = Maite
| last3 = Bertrán
| first3 = Juan
| last4 = Moliner
| first4 = Vicent
| last5 = Tuñón
| first5 = Iñaki
| date = 2010-05-26
| title = Theoretical study of phosphodiester hydrolysis in nucleotide pyrophosphatase/phosphodiesterase. Environmental effects on the reaction mechanism
| journal = Journal of the American Chemical Society
| volume = 132
| issue = 20
| pages = 6955–6963
| doi = 10.1021/ja908391v
| issn = 1520-5126
| pmid = 20429564
}}</ref> The red and blue dotted lines in Fig. 2a represent more realistic "concerted" mechanisms in which addition and elimination overlap, but are not fully synchronous. The difference in initial rates of the two steps implies different charge distribution in the transition state (TS).

When the addition step occurs more quickly than elimination (an A<sub>N</sub>D<sub>N</sub> mechanism),<ref name=":0" /> more positive charge develops on the nucleophile, and the transition state is said to be "tight."<ref name="Structural and Functional Comparisons" /><ref name=":1" /> Conversely, if elimination occurs more quickly than addition (D<sub>N</sub>A<sub>N</sub>), the transition state is considered "loose."

López-Canut et al. modeled substitution of a phosphodiester substrate using a hybrid quantum mechanics/molecular mechanics model.<ref name=":1" /> Notably, the model predicted an A<sub>N</sub>D<sub>N</sub> concerted mechanism in aqueous solution, but a D<sub>N</sub>A<sub>N</sub> mechanism in the active site of ''Xac'' NPP.

==Promiscuity==
Although NPP primarily catalyzes phosphodiester hydrolysis, the enzyme will also catalyze the hydrolysis of phosphate monoesters, though to a much smaller extent.  NPP preferentially hydrolyzes phosphate diesters over monoesters by factors of 10<sup>2</sup>-10<sup>6</sup>, depending on the identity of the diester substrate.  This ability to catalyze a reaction with a secondary substrate is known as enzyme promiscuity,<ref name="Structural and Functional Comparisons" /> and may have played a role in NPP’s evolutionary history.<ref>{{Cite journal
| last = O'Brien
| first = Patrick J.
| last2 = Herschlag
| first2 = Daniel
| date = 1999-04-01
| title = Catalytic promiscuity and the evolution of new enzymatic activities
| url = http://www.cell.com/article/S1074552199800337/abstract
| journal = Chemistry & Biology
| language = English
| volume = 6
| issue = 4
| pages = R91–R105
| doi = 10.1016/S1074-5521(99)80033-7
| issn = 1074-5521
| pmid = 10099128
}}</ref>

NPP’s promiscuity enables the enzyme to share substrates with [[alkaline phosphatase]] (AP), another member of the alkaline phosphate superfamily. Alkaline phosphatase primarily hydrolyzes phosphate monoester bonds, but interestingly it shows some promiscuity towards hydrolyzing phosphate diester bonds, making it a sort of opposite to NPP.  The active sites of these two enzymes show marked similarities, namely in the presence of nearly superimposable Zn<sup>2+</sup> bimetallo catalytic centers.  In addition to the bimetallo core, AP also has an Mg<sup>2+</sup> ion in its active site.<ref name="Structural and Functional Comparisons" />

== Biological Function ==
NPPs have been implicated in several biological processes, including bone mineralization, purine nucleotide and insulin signaling, and cell differentiation and motility. They are generally regulated at the transcriptional level.<ref name=":2" />

=== Mammalian Isoforms ===

==== [[Nucleotide pyrophosphatase/phosphodiesterase I|NPP1]] ====
NPP1 helps scavenge extracellular nucleotides in order to meet the high purine and pyrimidine requirements of dividing cells.<ref name=":2" /> In [[T cell|T-cells]], it may scavenge NAD<sup>+</sup> from nearby dead cells as a source of adenosine.<ref>{{Cite journal
|author1=Deterre, P. |author2=Gelman, L. |author3=Gary-Gouy, H. |author4=Arrieumerlou, C. |author5=Berthelier, V. |author6=Tixier, J.-M. |author7=Ktorza, S. |author8=Goding, J. |author9=Schmitt, C. |author10=Bismuth, G. | date = 1996
| title = Coordinated regulation in human T cells of nucleotide-hydrolyzing ecto-enzymatic activities, including CD38 and PC-1. Possible role in the recycling of nicotinamide adenine dinucleotide metabolites.
| url = 
| journal = The Journal of Immunology
| doi = 
| pmid = 8759717 | volume=157 | pages=1381–8
}}</ref>

The pyrophosphate produced by NPP1 in bone cells is thought to serve as both a phosphate source for [[calcium phosphate]] deposition and as an inhibitory modulator of [[calcification]].<ref>{{Cite journal
| last = Meyer
| first = John L.
| date = 1984-05-15
| title = Can biological calcification occur in the presence of pyrophosphate?
| url = http://www.sciencedirect.com/science/article/pii/0003986184903564
| journal = Archives of Biochemistry and Biophysics
| volume = 231
| issue = 1
| pages = 1–8
| doi = 10.1016/0003-9861(84)90356-4
}}</ref> NPP1 appears to be important for maintaining pyrophosphate/phosphate balance. Overactivity of the enzyme is associated with [[chondrocalcinosis]], while deficiency correlates to pathological calcification.<ref name=":3" />

NPP1 inhibits the [[insulin receptor]] ''in vitro''. In 2005, overexpression of the isoform was implicated in insulin resistance in mice.<ref>{{Cite journal
| last = Dong
| first = Hengjiang
| last2 = Maddux
| first2 = Betty A.
| last3 = Altomonte
| first3 = Jennifer
| last4 = Meseck
| first4 = Marcia
| last5 = Accili
| first5 = Domenico
| last6 = Terkeltaub
| first6 = Robert
| last7 = Johnson
| first7 = Kristen
| last8 = Youngren
| first8 = Jack F.
| last9 = Goldfine
| first9 = Ira D.
| date = 2005-02-01
| title = Increased Hepatic Levels of the Insulin Receptor Inhibitor, PC-1/NPP1, Induce Insulin Resistance and Glucose Intolerance
| url = http://diabetes.diabetesjournals.org/content/54/2/367
| journal = Diabetes
| language = en
| volume = 54
| issue = 2
| pages = 367–372
| doi = 10.2337/diabetes.54.2.367
| issn = 0012-1797
| pmid = 15677494
}}</ref> It has been linked to insulin resistance and [[Diabetes mellitus type 2|Type 2 diabetes]] in humans.<ref name=":2" />

==== NPP2 ====
NPP2, known in humans as [[autotaxin]], acts primarily in cell motility pathways. With its active site functioning, NPP2 promotes cellular migration at picomolar concentrations.<ref name=":2" /> Soluble splice variants of NPP2 are thought to be important to cancer [[metastasis]], and also show [[Angiogenesis|angiogenic]] properties in tumors.<ref name=":3" />

==== NPP3 ====
NPP3 is probably a major contributor to nucleotide metabolism in the intestine and liver.<ref name=":2" />

Intestinal NPP3 would be involved in hydrolyzing food-derived nucleotides.<ref>{{Cite journal
|author1=Byrd, J. C. |author2=Fearney, F. J. |author3=Kim, Y. S. | date = 1985
| title = Rat Intestinal Nucleotide-Sugar Pyrophosphatase
| url = 
| journal = Journal of Biological Chemistry
| doi = 
| pmid = 
}}</ref>

The liver releases ATP and ADP into the [[bile]] to regulate bile secretion.<ref>{{Cite journal
| vauthors = Schlenker T, Romac JM, Sharara AI, Roman RM, Kim SJ, LaRusso N, Liddle RA, Fitz JG
| date = 1997
| title = Regulation of biliary secretion through apical purinergic receptors in cultured rat cholangiocytes
| url = 
| journal = American Journal of Physiology. Gastrointestinal and Liver Physiology
| doi = 
| pmid = 
}}</ref> It subsequently reclaims adenosine via a pathway that probably contains NPP3.<ref>{{Cite journal
| last = Scott
| first = L J
| last2 = Delautier
| first2 = D
| last3 = Meerson
| first3 = N R
| last4 = Trugnan
| first4 = G
| last5 = Goding
| first5 = J W
| last6 = Maurice
| first6 = M
| date = 1997-04-01
| title = Biochemical and molecular identification of distinct forms of alkaline phosphodiesterase I expressed on the apical and basolateral plasma membrane surfaces of rat hepatocytes
| url = http://onlinelibrary.wiley.com/doi/10.1002/hep.510250434/abstract
| journal = Hepatology
| language = en
| volume = 25
| issue = 4
| pages = 995–1002
| doi = 10.1002/hep.510250434
| issn = 1527-3350
| pmid=9096610
}}</ref>

==Evolution==
NPP belongs to the alkaline phosphatase superfamily, which is a group of evolutionarily related enzymes that catalyze phosphoryl and sulfuryl transfer reactions.  This group includes phosphomonoesterases, phosphodiesterases, phosphoglycerate mutases, phosphophenomutases, and sulfatases.<ref>{{cite journal|doi=10.1021/ja908391v|title=Theoretical Study of Phosphodiester Hydrolysis in Nucleotide Pyrophosphatase/Phosphodiesterase. Environmental Effects on the Reaction Mechanism|year=2010|last1=LóPez-Canut|first1=Violeta|last2=Roca|first2=Maite|last3=Bertrán|first3=Juan|last4=Moliner|first4=Vicent|last5=Tuñón|first5=Iñaki|journal=Journal of the American Chemical Society|volume=132|issue=20|pages=6955–63|pmid=20429564}}</ref>

==References==
{{reflist|32em}}

{{DEFAULTSORT:Nucleotide Pyrophosphatase Phosphodiesterase (NPP)}}
[[Category:Protein structure]]
[[Category:Biochemistry]]
[[Category:Enzymes of known structure]]