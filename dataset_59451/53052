{{Infobox journal
| title = Training and Education in Professional Psychology
| cover = [[File:Training_and_Education_in_Professional_Psychology_Cover_Image.jpg]]
| editor = Michael C. Roberts
| discipline = [[Psychology]]
| abbreviation = Train. Educ. Prof. Psychol.
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Quarterly
| history = 2006-present
| openaccess = 
| license =
| impact = 1.733
| impact-year = 2014
| website = http://www.apa.org/pubs/journals/tep/index.aspx
| link1 = http://psycnet.apa.org/index.cfm?fa=browsePA.volumes&jcode=tep
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 237076268
| LCCN = 
| CODEN =
| ISSN = 1931-3918
| eISSN = 1931-3926
}}
'''''Training and Education in Professional Psychology''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]] on behalf of the Association of Psychology Postdoctoral and Internship Centers. It was established in 2006 and "is dedicated to enhancing supervision and training provided by psychologists."<ref>{{cite web |date=November 8, 2012 | url=http://www.apa.org/pubs/journals/tep/index.aspx |title=Training and Education in Professional Psychology |publisher=[[American Psychological Association]] |accessdate=2013-06-25}}</ref> The current [[editor-in-chief]] is Michael C. Roberts of the [[University of Kansas]].

== Abstracting and indexing ==
The journal is abstracted and indexed by the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.733, ranking it 18th out of 55 journals in the category "Psychology, Educational".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Psychology, Educational |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2015-08-26 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.apa.org/pubs/journals/tep/index.aspx}}

[[Category:American Psychological Association academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2006]]
[[Category:Quarterly journals]]
[[Category:Education journals]]