{{Infobox musical artist
| name            = A.G.Trio
| image           = Agtrio live in South Korea.jpg
| image_size      = 
| landscape       = <!-- yes, if wide image, otherwise leave blank -->
| alt             = A.G.Trio live in South Korea
| caption         = The A.G.Trio performing live at World Electronica Carnival in South Korea, August 2012
| background      = group_or_band
| alias           = Ages
| origin          = [[Linz]], [[Austria]]
| genre           = [[Electro house]]
| years_active    = {{start date|2004}}-present
| label           = Freaks like us!, Etage Noir Special
| associated_acts = 
| website         = * {{URL|theagtrio.com}}
* {{URL|ages-music.com}}
| current_members = * Roland von der Aist
* Andy Korg
* Aka Tell
| past_members    = 
}}

'''A.G.Trio''' is an [[Austria]]n [[Electro house]] band. It was founded in 2004 in [[Novi Sad]], [[Serbia]], during a combined tour of the founding members Roland von der Aist, Andy Korg & Aka Tell. The '''A.G.Trio''' is known for its excessive live-act and its remixes for popular Artists like [[Parov Stelar]].<ref>{{cite news|url=http://schaumedia.at/Action-mit-A-G-Trio.3877.0.html|title=A.G. Trio mit "Action"|publisher=schaumedia|date=June 1, 2012|language=German|accessdate=29 July 2013}}</ref> After many Singles, EPs and remixes in 2012 the '''A.G.Trio''' released a double-album called "Action" containing own productions as well es remixes.<ref>{{cite news|url=http://diepresse.com/home/kultur/popco/poep/760940/|title=Made in Austria: Neues von Zeronic, Debüt von AG Trio|date=May 25, 2012|first=Maciej |last=Palucki |publisher=Die Presse|language=German|accessdate=29 July 2013}}</ref>
After extensive touring with their debut album, the band released a compilation called "Reaction" which collects the remixes they have made over the past two years. In April 2014 the confirmed in an interview with the Austrian magazine [[The Gap (magazine)|The Gap]] that they are already working on their second album.<ref>{{Cite web|url=http://www.thegap.at/musikstories/artikel/cds-amadeus-und-edm-blase/|title=CDs, Amadeus und EDM-Blase|publisher=The Gap (Magazin)|date=2014-05-13|accessdate=2014-05-13}}</ref> In December 2014 they announced that '''A.G.Trio''' is on hiatus and presented their new project [[Ages (band)|Ages]].<ref>{{cite web|url=http://www.thegap.at/musikstories/artikel/mystisches-muehlviertel/|title=Mystisches Mühlviertel|publisher=The Gap|language=German|accessdate=19 December 2014|date=18 December 2014|last=Niederwieser|first=Stefan}}</ref>

== Discography ==

=== Albums ===

* 2012: Action (2xCD, Etage Noir Special)

=== Compilations ===

* 2014: Reaction (CD, Etage Noir Special)

=== Singles & EPs ===

* 2009: Zombies In The Disco (Digital, Etage Noir Special)
* 2009: Electro Messiah (Digital)
* 2009: Things You Wanna Play (Digital, Freaks Like Us! Entertainment)
* 2009: Replay (Digital, Freaks Like Us! Entertainment)
* 2010: Dancen (Digital, Etage Noir Special)
* 2010: Bass Effect (Digital, Etage Noir Special)
* 2010: Planet Disco (Digital, Etage Noir Special)
* 2011: Everyone With Us (Digital, Etage Noir Special)
* 2012: Everyone Withs Us (Remixes) (Digital, Etage Noir Special)
* 2012: Countably Infinite feat. M. Zahradnicek (Digital, Etage Noir Special)
* 2012: Moldance (Digital, Etage Noir Special)
* 2012: Give A Damn (Digital, Etage Noir Special)
* 2013: Duckstep (Digital, Etage Noir Special)
* 2013: Slice & Stitch EP (Digital, Etage Noir Special)

=== Remixes ===

* 2008: Hot Pants Road Club - Especially Tonight (FM Music)
* 2009: Johnny Mitchell - Errare (Freaks Like Us! Entertainment)
* 2009: Näd Mika - UFO (Drunkn Punx Rec.)
* 2010: Eriq Johnson - Boy Who Is A Girl (Dandy Kids Records)
* 2010: [[Egotronic]] - Was Solls ([[Audiolith Records]])
* 2010: Bilderbuch - Bitte, Herr Märtyrer (Schönwetter Schallplatten)
* 2010: ULTRNX - Rockstr ([[Audiolith Records]])
* 2010: Just Banks - Blockparty (Proper Nightlife)
* 2011: [[Bunny Lake]] - Army Of Lovers ([[Universal Music Group]])
* 2011: Ira Atari - Don´t Wanna Miss You ([[Audiolith Records]])
* 2011: Dead C∆T Bounce & You Killing me - Justice! (Mähtrasher)
* 2011: The Sexinvaders - LA Love (Riot Riot)
* 2012: Gorillas On Drums - We Are God (Dreieck)
* 2012: Pola-Riot – Brazza (Etage Noir Special)
* 2012: Orchestra Psychodelia - Cybertown (Etage Noir Special)
* 2012: Tits & Clits – Daedalus (Gigabeat)
* 2012: [[Parov Stelar]] – Nobody´s Fool (Etage Noir Recordings)
* 2012: Cosmic Sand – Sombra (Jet Set Trash Records)
* 2012: Hypomaniacs – There´s No Way Back (Jet Set Trash Records)
* 2013: The Sexinvaders – Empire (Pink Pong Records)
* 2013: Russkaja – Energia ([[Napalm Records]])
* 2013: Beef Theatre – Beef Is Back (Techno Changed My Life)
* 2013: Allen Alexis – Who Cares (Lamb Lane Records)

== Music videos ==

{| class="wikitable plainrowheaders" style="text-align:center;"
|+ List of music videos, showing year released and director
! scope="col" style="width:18em;" | Title
! scope="col"| Year
! scope="col" style="width:16em;" | Director(s)
|-
! scope="row"| "Dancen"
| rowspan="2"| 2010
| A.G.Trio<ref>{{cite web | title=A.G.Trio - DANCEN (official video) | url=http://www.nme.com/nme-video/youtube/id/KW0H3ieTfgY | publisher=NME| accessdate=29 July 2013}}</ref>
|-
! scope="row"| "Planet Disco"
| Luzi Katamay, Christian Dietl<ref name="musicaustria">{{cite web | title=Nominiert für den ersten Österreichischen Musikvideo Preis: "Countably Infinite" vom A.G. Trio | url=http://www.musicaustria.at/node/19137 |first=Lucia |last=Laggner |publisher=music austria| language=German|accessdate=29 July 2013}}</ref>
|-
! scope="row"| "Everyone With Us"
| 2011
| A.G.Trio<ref>{{cite web | title=Interview #39: A.G.Trio (incl. Exclusive Mixtape) | url=http://www.metalectro-music.com/2012/11/12/interview-39-a-g-trio-incl-exclusive-mixtape/ |publisher=Metalectro| accessdate=29 July 2013}}</ref>
|-
! scope="row"| "Countably Infinite feat. M. Zahradnicek"
| 2012
| Luzi Katamay<ref name="musicaustria" />
|-
|}

== Charts ==

A.G.Trio´s "Countably Infinite" feat. M. Zahradnicek entered the charts of Austrian radio station [[FM4]] on 12 May 2012 and was number one for two weeks. It stayed in the charts for seven weeks and was number 13 in the FM4 year charts of 2012, which was highest charting title by an Austrian artist or group.<ref>{{cite news|url=http://fm4.orf.at/stories/1698505/|title=Die FM4 Charts vom 12. Mai 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1698792/|title=Die FM4 Charts vom 19. Mai 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1699132/|title=Die FM4 Charts vom 26. Mai 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1699431/|title=Die FM4 Charts vom 2. Juni 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1699842/|title=Die FM4 Charts vom 9. Juni 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1700228/|title=Die FM4 Charts vom 16. Juni 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1700583/|title=Die FM4 Charts vom 23. Juni 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1709638/|title=FM4 Jahrescharts 2012|publisher=FM4|language=German|accessdate=29 July 2013}}</ref>

"Countably Infinite" also entered the German Club Charts on 25 May 2012 and remained in the charts for five weeks with best position 7.<ref>{{cite news|url=http://www.djbasis.de/2012/dcc-21-woche-2012/|title=DCC 21. Woche 2012|publisher=djbasis|language=German|accessdate=29 July 2013}}</ref>

A.G.Trio´s singles "Dancen", "Planet Disco" and "Countably Infinite", as well as their album "Action" reached position one in their respective genre charts on [[iTunes]] in [[Austria]].<ref>{{cite news|url=http://www.music-chart.info/|title=music-chart.info - iTunes Music Chart History Analysis|publisher=music-chart.info|accessdate=29 July 2013}}</ref>

The singles "Replay" and "Things You Wanna Play" reached top rankings in the Canadian and U.S. dance charts.<ref>{{cite news|url=http://www.musicexport.at/austrian-acts-at-eurosonic-a-g-trio/|title=Austrian Acts at Eurosonic 2013: A.G.Trio|publisher=Austrian Music Export|language=German|accessdate=29 July 2013}}</ref>

== Awards ==

In 2010 A.G.Trio was voted "Soundpark Band of the Year" by the listeners of the Austrian radio station [[FM4]].<ref>{{cite news|url=http://fm4.orf.at/stories/1670586/|title=Die Soundpark Band des Jahres ist ...&#124;|publisher=FM4|language=German|accessdate=29 July 2013}}</ref>

In 2013 A.G.Trio was nominated in the categories "Electronic/Dance" and "FM4 Award" at the Amadeus Austrian Music Awards.<ref>{{cite news|url=http://www.amadeusawards.at/nominierte/electronicdance/|title=Nominierte&#124;|publisher=Amadeus Austrian Music Awards|language=German|accessdate=29 July 2013}}</ref><ref>{{cite news|url=http://fm4.orf.at/stories/1714636/|title=FM4 Award 2013: Die Finalisten&#124;|publisher=FM4|language=German|accessdate=29 July 2013}}</ref> They were again nominated for the Amadeus Austrian Music Award in the category "Electronic/Dance" in 2015.<ref>{{cite news|url=http://www.heute.at/freizeit/musik/art31330,1120920|title=Amadeus Awards: Das sind alle Nominierten|publisher=Heute.at|language=German|accessdate=29 January 2015}}</ref>

== References ==

{{reflist}}

== External links ==
*[http://www.theagtrio.com Official Website]
*[http://www.ages-music.com Ages Website]
{{authority control}}
[[Category:Austrian musical groups]]
[[Category:Dance musicians]]
[[Category:Remixers]]
[[Category:Musical groups established in 2004]]