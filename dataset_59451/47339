[[File:Moravian Slovak Costumes during Jizda Kralu.jpg|thumb|The folk costumes [[kroje]], as seen in [[Vlčnov]], Moravia, during a folklore feast.]] 

'''Czech folklore''' is the folk tradition which has developed among the [[Czechs|Czech people]] over a number of centuries. Czech folklore was influenced by a mix of [[Christian]] and [[pagan]] customs. Nowadays it is preserved and kept alive by various folklore ensembles uniting members of all ages, from children to seniors, showing their talent during competitions, folklore festivals or other performances. 

The [[Czech Republic]] is divided into a number of ethnographic regions. Each of them has special folklore traditions, songs or costumes and specializes in different [[crafts]]. As a result, Czech folklore provides a diverse source of entertainment.

==Music and dance==
Music played an important part in life of common people or [[peasants]] in the Czech Republic. It offered both means of expression and a vent for their emotions. Resulting music varies not only by the region of its origin but also in the purpose of its use. Therefore, there are myriads of distinct [[folk songs]].

Music often addressed everyday issues and was passed down [[Oral tradition|orally]]. From the 19th century onward it was recorded by [[Ethnography|etnographs]]. Traditional celebrations such as '''welcome of the spring''', '''successful harvest''' are still among the occasions traditionally celebrated with songs. More lively themes were used specifically during celebrations, weddings or feasts. [[Funeral]]s and mournful occasions also had their own set of songs and tunes. 

Songs and especially dances were often linked to conscription of Czech young men to the Army; they are called [[Verbunkos#Slovácko verbuňk|“verbuňk”]] in some regions. Conscriptions usually happened during wartimes and these songs have a particular place in Czech folklore music. They are listed by [[UNESCO]] in the List of the Intangible Cultural Heritage of Humanity.<ref>{{cite web|url=http://www.unesco.org/archives/multimedia/?s=films_details&id_page=33&id_film=621|title=The Slovacko Verbunk|publisher=NULK|accessdate=19 November 2013}}</ref>

There are a number of instruments associated with Czech folk music, which add to its distinct sound – violin and the [[double bass]]; instruments specific to [[Bohemia]] and [[Moravia]] such as bagpipes [[Dudy|(bock)]], shepherd’s pipe, [[Hammered dulcimer|dulcimer]] and trumpet. 

All of them are still in active use by the folklore groups during their shows.

==Traditional Czech celebrations==
[[File:Svět knihy 2010 - Dětský folklórní soubor 1.JPG|thumb|[[Moravian Wallachia|Wallachian]] folk costumes]]
In the past, emphasis was placed on changes connected to the four seasons of the year; every season had its own specific traditions. In the [[Spring (season)|spring]], people practiced customs which would ensure health of their crops and future successful [[harvest]]. In the [[winter]], it was most important to protect households and villagers against adverse effects of cold weather and supernatural forces. 

Many Czech regions still hold traditional celebrations, which also revel in folklore music, dance and costumes. Among the most typical is Drowning of Morana, Shrovetide, erecting the maypole, [[Harvest (wine)|grape harvest]] or celebration of Easter. 

* Drowning of [[Marzanna|Morana]]: to celebrate the end of the winter, young girls in a number of villages build an effigy of a woman out of straw and branches, and dress the figure in old clothes. The woman represents a [[Slavic mythology|Slavic goddess]] of winter and is associated with death. Therefore her destruction symbolizes the beginning of new life in spring. After taking the figure  out of the village in a solemn procession, Morana is set on fire and then thrown into the water by singing girls.
* Shrovetide ([[Slavic carnival|Masopust]]): also serves to celebrate spring and ensure the fertility of future harvest. It includes a [[carnival]]-like procession of singing people dressed in masks of beasts and animals.<ref>{{cite web|last=Kuča|first=Pavel|title=Masopust - Fašank|language=Czech|url=http://czech.republic.cz/encyklopedie/objekty1.phtml?id=135067&lng=1|accessdate=19 November 2013}}</ref>
* [[Easter]] celebrations: the most distinctive customs preserved in the Czech Republic are spanking women solemnly with a whip made of thin willow branches (pomlázka); or drenching them with cold water to symbolically guarantee their good health. In exchange, men often receive traditional food, [[Easter egg|decorated eggs]] and (recently), alcoholic beverages.<ref>{{cite web|last=Kuncová|first=Romana|title=Celebrating Easter in the Czech Republic|url=http://www.czech.cz/en/Discover-CZ/Lifestyle-in-the-Czech-Republic/Tradition/Celebrating-Easter-in-the-Czech-Republic|accessdate=19 November 2013}}</ref>
* Burning of witches: also known as Walpurgis Night,<ref>{{cite web|last=Hubálková|first=Petra|title=Burning of Witches|url=http://www.czech.cz/en/Discover-CZ/Lifestyle-in-the-Czech-Republic/Tradition/Burning-of-Witches|accessdate=19 November 2013}}</ref> according to the traditional Czech stories, the night on the turn of April 30 and May 1 had a magical power. Not only was evil believed to be more powerful at this time, but also those who felt brave enough to go outside could find [[treasure]]s if they carried with them items such as wood fern flower, [[wafer]] or sanctified chalk. It was also believed that during the night [[witch]]es were flying and gathering for the [[Sabbath]]. To protect themselves, villagers burnt [[bonfire]]s on hills and set fire to brooms, which were then thrown into the air to reveal any flying witch. These celebrations are nowadays accompanied with music and traditional food and mark the opening of the tourist season.<ref>{{cite web|last=Hollý|first=Ladislav|title=Pálení čarodějnic|language=Czech|url=http://czech.republic.cz/encyklopedie/objekty1.phtml?id=120943&lng=1|accessdate=19 November 2013}}</ref>
* Erecting a Maypole: unmarried men in southern parts of the Czech Republic and in [[Slovácko|Slovacko]] cut off trees such as [[fir]], [[spruce]] or [[pine]] to make a traditional maypole out of it over night representing spring. The branches are cut off and only the top of the tree is left and decorated with ribbons and flowers. The [[maypole]] is then erected in the middle of the village. It is important to guard one’s maypole because men from other villages may try to cut it down as a form of contest.<ref>{{cite web|last=Hollý|first=Ladislav|title=Stavění MÁJE a májový čas|language=Czech|url=http://czech.republic.cz/encyklopedie/objekty1.phtml?id=120945&lng=1|accessdate=10 January 2013}}</ref>

==Traditional costumes==
[[File:Prague Pride 2013 - Moravian dress.jpg|thumb|upright|A participant of 2013 Prague Pride wearing a traditional Moravian costume ([[Haná|Hanakia]]) and a sign "Good day - [[Olomouc]] greets [[Prague]]"]]
Folklore is not merely about music. Czech folklore is characteristic for elaborate traditional costumes distinctive to each region or even village.<ref>{{cite web|title=Folk costumes|url=http://www.czech.cz/en/Discover-CZ/Lifestyle-in-the-Czech-Republic/Tradition/Folk-costumes|accessdate=19 November 2013}}</ref>

Specific aspects such as colors, [[embroidery]], themes and fragments of traditional costumes varied on the basis of social, geographical and symbolical factors as well as according to the purpose of their use. Costumes worn on special occasions were usually lavishly decorated, colorful and accompanied with a wide range of accessories (scarves, [[ribbon]]s, [[headdress]]es, hats, belts, etc.) while everyday clothes tended to be rather simple and practical. Every region had a set of amusing peculiarities. For example:

* Women in the [[Plzen Region|Plzen region]] wore up to 24 underskirts, which restricted their movement but were nevertheless a show of their fashionableness. 
* While married women in [[Haná|Hanakia]] wore plain clothes and hid their hair under bonnets and headdresses even from their own husbands, their men wore their hair long and even their clothes were flauntingly colorful and decorated.

Traditional costumes are no longer commonly worn in most parts of the Czech Republic. They may be used during traditional celebrations or festivals when worn by members of folklore ensembles. In some families, costumes are handed down from generation to generation as they wish to preserve the tradition. Likewise, many stay protected in museums and private collections.

==Folklore festivals==
Multiple folklore shows and festivals are organized in the Czech Republic throughout the year. They are a display of rich Czech traditions and folklore. At the festivals, various folklore groups demonstrate the heritage of their country. The performers are presented in traditional costumes particular to different regions of the Czech Republic. They perform [[folk dance]]s, traditional folk songs and play musical instruments. Many festivals are not only demonstrations of music and dances, but also show traditional [[Czech food|cuisine]] and crafts.

There are many folklore festivals in the Czech Republic.<ref>{{cite web|title=Festivals|url=http://www.folklornisdruzeni.cz/en/festivals|work=Folklorní sdružení ČR|accessdate=19 November 2013}}</ref><ref>{{cite web|author= |url=http://czech.republic.cz/encyklopedie/seznam.phtml?typ=44&lng=2&menu= |title=rezervační a informační systém |publisher=Czech.Republic.CZ |date= |accessdate=2013-11-20}}</ref>
* [[Vlčnov]]: probably the most famous one is a festival called “[[Ride of the Kings]]”. This festival takes place every year and can be also found on the list of UNESCO. 
* [[Strážnice]]: Another unique folklore festival. Not only does this festival present traditional Czech folklore, but also folklore groups from all over the world visit the town to show their own traditions. 
* [[Strakonice]]: The Bohemian city holds an annual bagpipe festival that attracts the locals, as well as foreign tourists. The festival is a parade and a meeting place of Czech and international bagpipe players.

==Folklore groups==
Czech Republic is abundant in folklore groups of all kinds.<ref>{{cite web|author= |url=http://czech.republic.cz/encyklopedie/seznam.phtml?typ=38,245&lng=2&menu= |title=rezervační a informační systém |publisher=Czech.Republic.CZ |date= |accessdate=2013-11-20}}</ref> Their goal is to preserve local traditions and folklore and to pass the [[Cultural heritage|heritage]] on to the following generations. Members of the folklore groups are enthusiasts of all ages: from little children through adolescents to adults. They all enjoy building up folklore spirit with dances, music and showing off beautiful folklore dresses, as well as performing in front of audiences at folklore festivals. Children are encouraged to participate in folklore ensembles from a very early age.

==Traditional arts and crafts==
Traditional arts and crafts are an inseparable part of Czech folklore. 
* [[Corn husk doll|Cornhusk dolls]]: among the most popular traditional products. These dolls made of cornhusk and a few strings are nowadays used as decorations but in the past children used them as toys. Cornhusk dolls are also great [[souvenir]]s tourists can buy in the Czech Republic if they are looking for something truly traditional. 
* Pottery: Another popular traditional artistic product. Bohemian and Moravian [[pottery]] is painted with various folklore patterns, simple or floral ornaments. The pottery serves for both decorative and practical purposes.
* [[Bobbin lace]]: also one of the popular traditional items. The most famous Czech bobbin lace products are made in [[Vamberk]], a city in Eastern Bohemia. 
* The Czech Republic is also renowned for its production of [[glass]]: crystal glasses, bowls, vases, and other products contribute to the richness of heritage of the Czech Republic. 
* [[Indigo]] print products: hand-made textile goods. Tablecloths, toys, and other accessories are made of this material.

==See also==
*[[Slavic mythology]]

==References==
{{Reflist}}

==External links==
*[http://blog.folkloreshow.cz Foklore Show Blog]
*{{cite web|last=Kábelová|first=Andrea|title=May 1st - Day For Love|url=http://www.czech.cz/en/Life-Work/Living-here/Customs-and-traditions/May-1st-–-Day-For-Love|accessdate=19 November 2013}}
*[http://www.jizni-morava.cz/?lang=2&tpl=60 South Moravia - Official Tourism Website]

[[Category:Czech folklore| ]]
[[Category:Folklore by country]]