{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name = Baila Esta Cumbia
| Cover = Bailaestacumbia.jpg
| Alt =  Selena poses with her right hand above her head and her left arm stationed, while her band are in the foreground.
| Artist = [[Selena]]
| from Album = [[Ven Conmigo (album)|Ven Conmigo]]
| Released = August 28, 1990
| Format = {{flatlist|
*[[CD single]]
*[[12-inch single|12" single]]
}}
| B-side = [[La Carcacha]]
| Recorded = 1989
| Genre = [[Mexican cumbia]]
| Length = {{duration|m=2|s=57}}
| Label = [[EMI Latin]]
| Writer = {{flatlist|
*[[A.B. Quintanilla]],
*[[Pete Astudillo]]
}}
| Producer = A.B. Quintanilla
| Last single = "Ya Ves"<br>(1990)
| This single = "'''Baila Esta Cumbia'''"<br>(1990)
| Next single = "[[Buenos Amigos]]"<br>(1991)
{{Extra music sample|filename=Selena - Baila Esta Cumbia.ogg|format=[[Ogg]] |title="Baila Esta Cumbia"|description=|type=single}}

}}
"'''Baila Esta Cumbia'''" (English: Dance to this Cumbia) is a song recorded by American [[Tejano music|Tejano]] singer [[Selena]] for her second studio album, ''[[Ven Conmigo (album)|Ven Conmigo]]'' (1990). It was released as the second single by [[EMI Latin]] on August 28, 1990, behind "Ya Ves". It was composed by her brother–producer [[A.B. Quintanilla]], and [[Selena y Los Dinos]] backup dancer, [[Pete Astudillo]]. The recording is an [[tempo|up-tempo]] [[Mexican cumbia]] song. It was well received by [[music journalist|music critics]] who enjoyed its cumbia-feel and rhythm.

The track received exposure on radio stations that predominately played cumbia music, and as a result the recording became a [[hit single]] for the singer. It was named among her best recordings in her career by music critics. "Baila Esta Cumbia" peaked at number ten on the US [[Billboard (magazine)|''Billboard'']] [[Latin Digital Songs|Regional Mexican Digital Songs]] chart. In 2005, A.B. Quintanilla recorded a remix/duet version of the track with his band [[Kumbia Kings]], which peaked at number 16 on the US ''Billboard'' [[Regional Mexican Airplay]] chart and number 44 on the [[Hot Latin Tracks]] chart. Other cover versions include recordings by Mexican singers [[Diana Reyes]] and [[Yuridia]].

== Background and release ==
"Baila Esta Cumbia" was released as the second single from ''[[Ven Conmigo (album)|Ven Conmigo]]'' (1990) in the United States<ref name=disco>{{cite journal|title=Selena > Discography|date=10 June 1995|journal=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|volume=107|issue=23|url=https://books.google.com/books?id=0QsEAAAAMBAJ&pg=PA64 |accessdate=10 October 2014}}</ref> and in Mexico.<ref name=cristina>{{cite book|last1=Castrellón|first1=Cristina|title=Selena: su vida después de su muerte|date=31 August 2007|publisher=Penguin Random House Grupo Editorial México|isbn=9786071110367|url=https://books.google.com/books?id=pT3SO9XeUbQC&pg=PT22 |accessdate=10 October 2014|quote=(Spanish, original) El motivo era celebrar que Selena había ganado su primer Disco de Oro al rabasar las 150 mil copias vendidas de su disco Baila Esta Cumbia, el primero que salio en Mexico. (English, translate) The occasion was to celebrate that Selena had won her first gold record of 150 thousand copies sold of her album Baila Esta Cumbia, who first came to Mexico.|language=Spanish}}</ref> A compilation album of the same name was released in Mexico and sold 150,000 copies.<ref name=cristina/> It was certified [[Asociación Mexicana de Productores de Fonogramas y Videogramas#Sales certificates|platinum]] by the [[Asociación Mexicana de Productores de Fonogramas y Videogramas]] (AMPROFON), denoting sales of 250,000 units.<ref>{{cite news|title=Disco de Oro y Platino a Viene de la Uno|url=http://h.elsiglodetorreon.com.mx/Repository/getFiles.asp?Style=OliveXLib:LowLevelEntityToSaveGifMSIE_ELSIGLO&Type=text/html&Locale=spanish-skin-custom&Path=EDT/1993/12/13&ChunkNum=-1&ID=Ar05010&PageLabel=50|accessdate=10 October 2014|work=[[El Siglo de Torreón]]|date=13 December 1993|language=Spanish}}</ref> "Baila Esta Cumbia" was written by Selena's brother and principal record producer, [[A.B. Quintanilla]] and [[Selena y Los Dinos]]' backup dancer, [[Pete Astudillo]].<ref name="albumnotes">{{cite AV media notes |title=Ven Conmigo |titlelink=Ven Conmigo (album) |others=Selena |year=2002 |chapter= |url= |first= |last= |authorlink= |coauthors= |page= |pages= |type=CD |publisher=EMI Latin |id=77774235921 |location=}}</ref> During an interview in 2002, A.B. said that the recording helped the band's exposure on radio stations that predominantly play [[cumbia music]] recordings.<ref name="albumnotes"/>

The song is an [[tempo|up-tempo]]<ref name=roberts>{{cite book|last1=Roberts|first1=John Storm|title=The Latin Tinge: The Impact of Latin American Music on the United States|date=21 January 1999|publisher=[[Oxford University Press]]|isbn=0195121015|edition=2nd|url=http://www.questia.com/read/100559829/the-latin-tinge-the-impact-of-latin-american-music|accessdate=10 October 2014}}</ref> [[Mexican cumbia]] [[Tejano music|Tejano]] song.<ref name=vargas>{{cite book|last1=Vargas|first1=Deborah R.|title=Dissonant divas in Chicana music : the limits of la onda|date=2012|publisher=[[University of Minnesota Press]]|isbn=0816673160|url=https://books.google.com/books?id=Qx00pQIkclMC&pg=PA188 |accessdate=10 October 2014}}</ref> It is set in [[common time]] and moves at a moderate rate of 90 [[beats per minute]]. "Baila Esta Cumbia" is written in the [[key (music)|key]] of [[C major]]. The [[vocal range]] of the melody extends from the note [[G♯ (musical note)|G{{Music|sharp}}<sub>3</sub>]] to [[A (musical note)|A<sub>4</sub>]].<ref name=music2>{{cite web|url=http://www.musicnotes.com/sheetmusic/mtdFPE.asp?ppn=MN0075339&ref=google |first1=Selena |last1=Quintanilla-Perez |first2=A.B. |last2=Quintanilla III |authorlink1=Selena |authorlink2=A.B. Quintanilla III|first3=Pete |last3=Astudillo |authorlink3=Pete Astudillo |title=Baila Esta Cumbia: Selena Digital Sheet Music|year=1990 |accessdate=10 October 2014 |work=''Musicnotes.com'' |publisher=[[EMI Music Publishing]] |format=Musicnotes|at=MN0075339 (Product Number)}}</ref>

== Reception and impact ==
The majority of contemporary reviews on "Baila Esta Cumbia" were positive. ''Billboard'' contributor Ramiro Burr praised the song for its "melodic hook".<ref name=billboardreview>{{cite journal|last1=Burr|first1=Ramiro|title=EMI Set To Honor Selena's Memory|date=15 April 1995|journal=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|volume=107|issue=15|url=https://books.google.com/books?id=7wsEAAAAMBAJ&pg=PA72&dq=selena+baila+esta+cumbia|accessdate=10 October 2014}}</ref> Burr, who wrote in ''[[The Billboard Guide to Tejano and Regional Mexican Music]]'' (1999), opined that Selena "evolved a rhythmic style" which paved way for "catchy cumbias", giving "Baila Esta Cumbia" as his example of her "increasing prowess".<ref name=billboardguide>{{cite book|last1=Burr|first1=Ramiro|title=The Billboard guide to Tejano and regional Mexican music|date=1999|publisher=[[Billboard Books]]|isbn=0823076911|edition=1st|url=https://books.google.com/books?ei=Lxk4VPHVEYKi8QGItoHgCg&id=9DPaAAAAMAAJ&dq=selena+baila+esta+cumbia|accessdate=10 October 2014|quote=Selena, had evolved a rhythmic style that demonstrated its increasing prowess for catchy cumbias such as "Baila Esta Cumbia".}}</ref> Italian essayist Gaetano Prampolini, called "Baila Esta Cumbia" a "plain cumbia dance pleasure" in his book ''The Shade of the Saguaro''.<ref name=prampolini>{{cite book|last1=Prampolini|first1=Gaetano|last2=Pinazzi|first2=Annamaria|title=The Shade of the Saguaro / La sombra del saguaro. Essays on the Literary Cultures of the American Southwest|publisher=Firenze University Press|isbn=886655393X|url=https://books.google.com/books?id=B1nWKfuvSa4C&pg=PA188&dq=selena+baila+esta+cumbia|accessdate=10 October 2014}}</ref> In a 2013 contribution to ''[[OC Weekly]]'', Marco Torres added that "Baila Esta Cumbia" is a "fun song" and noted its "lively" addictive nature.<ref name=marcorank>{{cite news|last1=Torres|first1=Marco|title=Top 10 Selena Songs of All Time|url=http://blogs.ocweekly.com/heardmentality/2013/04/top_10_selena_viceos.php?page=3|accessdate=10 October 2014|work=[[OC Weekly]]|publisher=[[Voice Media Group]]|date=16 April 2013}}</ref> John Storm Roberts wrote in his book ''The Latin Tinge'', that the recording is an "up-tempo romantic piece" for his review of Selena's live album, ''[[Selena Live!|Live]]'' (1993). Roberts added that with "[[Como la Flor]]", the two "mixes pop vocalism, some quite free scatting, and a classic banda keyboard sound."<ref name=roberts/> Federico Martinez of the San Antonio ''La Prensa'' called the recording "upbeat".<ref name=lapresna>{{cite news|last1=Martinez|first1=Federico|title=Part Two on the Legacy of Selena|url=http://www.laprensatoledo.com/Stories/2014/090514/selena.htm|accessdate=10 October 2014|work=La Prensa|publisher=Culturas Publication|date=9 September 2014}}</ref>

Deborah R. Vargas wrote in her book ''Dissonant Divas in Chicana Music: The Limits of la Onda'' (2008), that Selena reconstructed Tejano music with the additions of cumbia music, giving credit to "Baila Esta Cumbia" as an example of Selena's blended musical compositions.<ref name=vargas/> The recording became one of Selena's biggest [[hit singles]].<ref name=encyclopedia>{{cite book|last1=Chávez Candelaria|first1=Cordelia|last2=Aldama|first2=Arturo J.|last3=García|first3=Peter J.|last4=Alvarez-Smith|first4=Alma|title=Encyclopedia of Latino popular culture|date=2004|publisher=[[Greenwood Press]]|isbn=031333210X|url=https://books.google.com/books?id=STjcB_f7CVcC&pg=PA215|accessdate=10 October 2014}}</ref> It has appeared on a number of critics' "best Selena songs" lists, including ''OC Weekly'' (at number five),<ref name=marcorank/> and ''[[Latina (magazine)|Latina]]'' magazine (at number three).<ref name=latina>{{cite web|last1=Rodriguez|first1=Priscillia|title=Remembering Selena: Her Top Ten Songs|url=http://www.latina.com/music/selena-quintanilla-hit-songs-videos#3|journal=[[Latina (magazine)|Latina]]|publisher=Lauren Michaels|accessdate=10 October 2014}}</ref> It entered The TouchTunes Most Played chart on ''Billboard'' on the issue dated April 7, 2001, with 1.5 million airplay spins.<ref name=touchtunes>{{cite journal|title=The TouchTunes Most Played (7 April 2001)|url=https://books.google.com/books?id=uBMEAAAAMBAJ&pg=PA109&dq=selena+baila+esta+cumbia|date=7 April 2001|journal=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|volume=113|issue=14|accessdate=10 October 2014}}</ref> The song was included in the set list for the ''[[Selena Forever]]'' musical, which ran for one year in 2000.<ref name=musical>{{cite journal|last1=Burr|first1=Ramiro|title=Selena Forever Premiere Should Boost Catalog Sales|date=25 March 2000|journal=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|volume=112|issue=13|url=https://books.google.com/books?id=sg4EAAAAMBAJ&pg=PA89&dq=selena+baila+esta+cumbia|accessdate=10 October 2014}}</ref>

== Cover versions ==
Mexican singer [[Diana Reyes]] recorded the song for her album ''Ámame, Bésame'' (2010).<ref>{{cite web|last1=Henderson|first1=Alex|title=Amame, Besame|url=http://www.allmusic.com/album/%C3mame-b%C3%A9same-mw0001967642|website=''[[AllMusic]]''|publisher=[[Rovi Corporation]]|accessdate=10 October 2014}}</ref> Mexican singer [[Yuridia]] performed "Baila Esta Cumbia" during her tour in Mexico in 2013 to a positive reception.<ref>{{cite web|last1=Arrona Crespo|first1=Juana|title=Voz de ‘Ángel’ cautiva Palenque|url=http://www.am.com.mx/leon/espectaculos/voz-de-angel-cautiva-palenque-256137.html|website=''Am.com.mx''|accessdate=21 June 2016}}</ref>

=== Kumbia Kings' version ===
Selena's brother and principal record producer, A.B. formed [[Kumbia Kings]] in 1997. In 2005, the band remixed "Baila Esta Cumbia" into a duet version for their album ''[[Duetos (Kumbia Kings album)|Duetos]]'' under the title "Baila Esta Kumbia". Evan C. Gutierrez music reviewer for [[AllMusic]] believed that the Kumbia Kings' version is a "postmortem love letter" to Selena.<ref name=allmusic>{{cite web|last1=Gutierrez|first1=Evan C.|title=Duetos (Album review)|url=http://www.allmusic.com/album/duetos-mw0000251603|website=''[[AllMusic]]''|publisher=[[Rovi Corporation]]|accessdate=10 October 2014}}</ref> Kumbia Kings performed their version for the tribute concert ''[[Selena ¡VIVE!]]'', which premiered live on [[Univision]] on April 7, 2005.<ref name=van>{{cite news|title=Selena, a 13 años de su muerte|url=http://www.vanguardia.com.mx/selenaa13anosdesumuerte-144513.html|accessdate=10 October 2014|work=Vanguardia|publisher=[[Terra Networks]]|date=31 March 2008|language=Spanish}}</ref> The track debuted at number 35 on the US [[Billboard (magazine)|''Billboard'']] [[Regional Mexican Airplay]] chart on the issue dated April 2, 2005.<ref name=regionalapril>{{cite journal|title=Regional Mexican Airplay (2 April 2005)|journal=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=2 April 2005|volume=117|issue=14|url=https://books.google.com/books?id=_BMEAAAAMBAJ&pg=PA23&dq=selena+baila+esta+cumbia&hl=en&sa=X&ei=lw44VKnbEI6nyATjtIAI&ved=0CCcQ6AEwAg#v=onepage&q=selena%20baila%20esta%20cumbia&f=false|accessdate=10 October 2014}}</ref> It eventually peaked at number 16.<ref name=allmusic/>

{|class="wikitable plainrowheaders sortable" style="text-align:center;"
|-
! scope="col" | Chart (2005)
! scope="col" | Peak<br/>position
|-
!scope=row|US ''Billboard'' [[Hot Latin Tracks]]<ref name=allmusic/>
| style="text-align: center; "|44
|-
!scope=row|US ''Billboard'' Regional Mexican Airplay<ref name=allmusic/>
| style="text-align: center; "|16
|-
|}

== Charts ==
{|class="wikitable plainrowheaders sortable" style="text-align:center;"
|-
! scope="col" | Chart (2012–14)
! scope="col" | Peak<br/>position
|-
!scope=row|US Latin Digital Songs (''Billboard'')<ref name=latindigital>{{cite web|title=Selena > Chart history > Latin Digital Songs|url=http://www.billboard.com/artist/278615/selena/chart?f=1089|website=Billboard|accessdate=10 October 2014}}</ref>
| style="text-align: center; "|38
|-
!scope=row|US Latin Pop Digital Songs (''Billboard'')<ref name=digital>{{cite web|title=Selena > Chart history > Baila Esta Cumbia|url=http://www.billboard.com/biz/search|archivedate=10 October 2014|archiveurl=http://www.webcitation.org/6TEE6zwKU|website=Billboard.biz|accessdate=10 October 2014}}</ref>
| style="text-align: center; "|21
|-
!scope=row|US Regional Mexican Digital Songs (''Billboard'')<ref name=digital/>
| style="text-align: center; "|10
|}

==References==
{{Reflist|2}}

==External links==
* {{MetroLyrics song|selena|baila-esta-cumbia}}<!-- Licensed lyrics provider -->

{{Selena singles}}
{{Kumbia Kings}}
{{Good article}}

[[Category:1990 singles]]
[[Category:2005 singles]]
[[Category:Cumbia songs]]
[[Category:Selena songs]]
[[Category:Songs written by A.B. Quintanilla]]
[[Category:Songs written by Pete Astudillo]]
[[Category:Spanish-language songs]]
[[Category:Kumbia Kings songs]]
[[Category:1990 songs]]
[[Category:EMI Latin singles]]