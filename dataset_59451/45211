{{Other people}}
{{Infobox military person
|name= Donald Clifford Tyndall Bennett
|image= Air Marshal D C T Bennett CH13645.jpg
|caption= Bennett as an Air Vice Marshal
|nickname= 
|birth_date= {{Birth date|1910|09|14|df=yes}}
|birth_place= [[Toowoomba]], Queensland
|death_date= {{Death date and age|1986|09|15|1910|09|14|df=yes}}
|death_place= Slough, Berkshire<ref>https://www.munzinger.de/search/portrait/Donald+C+T+Bennett/0/8625.html</ref>
|placeofburial= 
|allegiance= Australia<br/>United Kingdom
|branch= [[Royal Australian Air Force]] (1930–31, 1935–36)<br/>[[Royal Air Force]] (1931–35, 1941–45)
|serviceyears= 1930–36<br/>1940–45
|rank= [[Air Vice Marshal]]
|servicenumber= 
|unit= 
|commands= [[No. 8 Group RAF|No. 8 (Pathfinder Force) Group]] (1943–45)<br/>[[Pathfinder (RAF)|Pathfinder Force]] (1942–43)<br/>[[No. 10 Squadron RAF|No. 10 Squadron]] (1942)<br/>[[No. 77 Squadron RAF|No. 77 Squadron]] (1941–42)
|battles= [[Second World War]]
|awards= [[Companion of the Order of the Bath]]<br/>[[Commander of the Order of the British Empire]]<br/>[[Distinguished Service Order]]<br/>[[King's Commendation for Valuable Service in the Air]]<br/>[[Order of Alexander Nevsky]] (USSR)
|relations= 
|laterwork= Director of British South American Airways
}}
[[Air Vice Marshal]] '''Donald Clifford Tyndall Bennett''', {{postnominals|country=GBR|size=100%|sep=,|CB|CBE|DSO}} (14&nbsp;September 1910 – 15&nbsp;September 1986) was an Australian aviation pioneer and bomber pilot who rose to be the youngest air vice marshal in the [[Royal Air Force]]. He led the [[Pathfinder (RAF)|"Pathfinder Force"]] ([[No. 8 Group RAF]]) from 1942 to the end of the Second World War in 1945. He has been described as "one of the most brilliant technical airmen of his generation: an outstanding pilot, a superb navigator who was also capable of stripping a wireless set or overhauling an engine".

== Early flights ==
Donald Bennett was born the youngest son of a grazier in [[Toowoomba]], Queensland. He attended [[Brisbane Grammar School]] and later joined the [[Royal Australian Air Force]] in 1930 <ref>https://www.awm.gov.au/people/P10676768/</ref> and transferred to the [[Royal Air Force]] a year later,<ref>https://www.thegazette.co.uk/London/issue/33751/page/5831</ref> starting with the [[flying boat]]s of [[No. 20 Squadron RAF|20 Squadron]]. Bennett developed a passion for accurate flying and precise [[navigation]] that would never leave him. After a period as an instructor at [[RAF Calshot]], he left the service in 1935 (retaining a reserve commission) to join [[Imperial Airways]]. Over the next five years, Bennett specialised in long distance flights, breaking a number of records and pioneering techniques which would later become commonplace, notably [[aerial refueling|air-to-air refuelling]]. In 1936 he wrote the first edition of his ''The complete air navigator: covering the syllabus for the flight navigator's licence'' (Pitman, London) which he updated several times up to the seventh edition in 1967. In July 1938 he piloted the ''Mercury'' part of the [[Short Mayo Composite]] flying-boat across the Atlantic; this flight earned him the [[Oswald Watt Gold Medal]] for that year.

==Second World War==
During 1940 Bennett's long-distance expertise was set to work setting up the [[Atlantic Ferry Organization]] tasked with the wartime delivery of thousands of aircraft manufactured in the United States and Canada to the United Kingdom. At that time, a transatlantic flight was a significant event, but the Atlantic Ferry project proved remarkably successful and demonstrated that with suitable training even inexperienced pilots could safely deliver new aircraft across the North Atlantic.

Bennett was recommissioned in 1941 In the [[Royal Air Force Volunteer Reserve]] as a squadron leader, his first task was to oversee the formation The Elementary Air Navigation School, Eastbourne, for the initial training of observers (later navigators). However he was promoted to [[Wing commander (rank)|wing commander]], and appointed to the command of [[No. 77 Squadron RAF|No. 77 Squadron]], based at [[RAF Leeming]] flying [[Armstrong Whitworth Whitley|Whitleys]] in 4 Group, Bomber Command, on 7 December 1941.

In April 1942, No. 77 Squadron was transferred to [[RAF Coastal Command|Coastal Command]] and Bennett was given command of [[No. 10 Squadron RAF|No. 10 Squadron]] ([[Handley Page Halifax]]) and shortly afterwards led a raid on the [[German battleship Tirpitz|German battleship ''Tirpitz'']]. Shot down during that raid, he evaded capture and escaped to Sweden, from where he was able to return to Britain; he and his copilot were awarded the [[Distinguished Service Order]] (DSO) on 16&nbsp;June 1942.<ref>''London Gazette'' 12 June 1942 Issue number: 35597 page 2649</ref>

===Pathfinder Force===
In July 1942, Bennett was appointed to command the new [[Pathfinder Force]], an elite unit tasked with improving [[RAF Bomber Command]]'s navigation. At this stage of the war, Bomber Command had begun to make night-time raids deep into Germany, but had not yet been able to cause significant damage, largely because only about a quarter of the bomb loads were delivered "on target" — and this at a time when "on target" was defined as within three miles of the aim point.

Pathfinder Force was set up to lead the [[bomber stream]] to the target areas and drop markers for the remainder of the force to aim at. Later in the war, the Pathfinder Force would be equipped with a range of newly developed and often highly effective electronic aids, but the initial object was to simply take experienced crews with standard equipment and hone their navigation skills.

Having already demonstrated that he could pass on his meticulous navigational ability to others, Bennett was an obvious choice for the role, yet nevertheless a surprising one. The [[Air Ministry]]'s Directorate of Bomber Operations had for some time been pushing to establish an elite precision bombing force, but Bomber Command AOC-in-C Air Chief Marshal [[Sir Arthur Harris, 1st Baronet|Arthur Harris]] was implacably opposed to the idea, on the grounds that an elite force would "lower the morale" of the other squadrons.

When Harris learned that Vice-Chief of the Air Staff Sir [[Wilfrid Freeman]] planned to order the change, and that the strong-willed [[Basil Embry]] would probably be given command of the new force, Harris bowed to the inevitable, but was given a "more or less free hand" in selection of the force commander and he chose to appoint Wing Commander Don Bennett without considering other candidates. Harris described Bennett as "the most efficient airman I have ever met".<ref>Maynard p83</ref> Bennett was called to Bomber Command HQ when he was on the point of leaving with his squadron for the Middle East. There he was informed by Harris that he was to lead a special force to make use of the new bombing and navigational aids then available and the more sophisticated ones that would follow. With effect of the 5 July he was promoted to group captain.<ref>Maynard p84</ref>

In 1943 Bennett was promoted with the upgrading of PFF to group status to [[air commodore]]. and then in December to [[Acting (rank)|acting]] air vice marshal <ref>https://www.thegazette.co.uk/London/issue/36323/supplement/211</ref> — the youngest officer to ever hold that rank – giving him a rank similar to those of the other commanders of groups. He remained in command of the Pathfinder Force until the end of the war, overseeing its growth to an eventual 19&nbsp;squadrons, a training flight and a meteorological flight, working relentlessly to improve its standards, and tirelessly campaigning for better equipment, in particular for more [[de Havilland Mosquito|Mosquitos]] and [[Avro Lancaster|Lancasters]] to replace the diverse assortment of often obsolete aircraft the force started with.{{Citation needed|date=July 2011}}

Bennett was not a popular leader: a personally difficult and naturally aloof man, he earned a great deal of respect from his crews but little affection. As Harris wrote, "he could not suffer fools gladly, and by his own high standards there were many fools". Nor did Bennett get on well with the other RAF group commanders: not only was he 20&nbsp;years younger, he was an Australian. Indeed, Bennett saw his own appointment in those terms: it was, he believed, a victory for the [[Gentlemen v Players|"players" over the "gentlemen"]]. There was antagonism between Bennett and Air Vice Marshal [[Ralph Cochrane]] of No. 5 Group. In 5 Group's 617 Squadron, Cochrane had his own specialist squadron pursuing high levels of accuracy.<ref>[http://www.oxforddnb.com/view/article/30945 Ralph Cochrane] Oxford Dictionary of National Biography</ref>

==After the war==
Despite the unquestioned achievements of No. 8 Group, at the end of the war Bennett was the only bomber group commander not to be knighted . He resigned his commission in the RAF,<ref>https://www.thegazette.co.uk/London/issue/37146/supplement/3318</ref> and returned to private life to pursue a variety of activities. He became a Director of [[British South American Airways]], and designed and built both cars ([[Fairthorpe Cars|Fairthorpes]]) and light aircraft.

One of his darkest hours after the war came on 12 March 1950, when an aircraft operated on charter by his airline 'Fairflight' [[Llandow air disaster|crashed at Llandow in Wales]].

Bennett became one of the shortest-serving [[Member of Parliament|Members of Parliament]] (MPs) of the 20th century when he was elected at a [[Middlesbrough West by-election, 1945|by-election in 1945]] <ref>https://www.thegazette.co.uk/London/issue/37085/page/2575</ref> as [[Liberal Party (UK)|Liberal]] MP for [[Middlesbrough West (UK Parliament constituency)|Middlesbrough West]]. He was defeated soon afterwards in the [[United Kingdom general election, 1945|1945 general election]] — his parliamentary career having lasted all of 73&nbsp;days. He had previously attempted to be selected as a [[Conservative Party (UK)|Conservative]] candidate for [[Macclesfield (UK Parliament constituency)|Macclesfield]] in February 1944. One of his fellow candidates was [[Guy Gibson]]; Gibson was selected instead.<ref name="Morris 1994 236">{{harvnb|Morris|1994|p=236}}</ref> Attempts to return to the [[House of Commons of the United Kingdom|House of Commons]] for [[Croydon North (UK Parliament constituency)|Croydon North]] at a [[Croydon North by-election, 1948|by-election in 1948]] and in [[Norwich North (UK Parliament constituency)|Norwich North]] at the [[United Kingdom general election, 1950|1950 general election]] were unsuccessful. A later attempt at the [[Nuneaton by-election, 1967|1967 Nuneaton by-election]], standing for the obscure [[National Fellowship|National Party]], resulted in his losing his [[deposit (politics)|deposit]]. He continued his support for far-right fringe parties during the 1970s as a patron of the [[National Independence Party (UK)|National Independence Party]].<ref>Martin Walker, ''The National Front'', Glasgow: Fontana Collins, 1977, p. 135</ref>

In 1958 an autobiography entitled ''Pathfinder'' detailing his experiences throughout the war was published by Frederick Muller Ltd.

Don Bennett died at the age of 76 on [[Battle of Britain|Battle of Britain Day]]: 15 September 1986.

==See also==
*[[List of United Kingdom MPs with the shortest service]]

==References==
;Notes
{{reflist|30em}}

;Bibliography
*[http://www.rafweb.org/Biographies/BennettD.htm Air of Authority – A History of RAF Organisation – AVM Bennett]
* D.C.T. Bennett, ''Pathfinder; a war autobiography'', London, Muller, 1958 (re-imp. Goodall paperback, 1988, ISBN 0-907579-57-4)
*[http://www.flywiththestars.co.uk ''Fly With the Stars, A History of British South American Airways''], 2007, ISBN 978-0-7509-4448-9
*Bramson, Alan, ''Master Airman; a biography of Air Vice-Marshal Donald Bennett, CB., CBE., DSO.'', Airlife, 1985, ISBN 0-906393-45-0
*Maynard, John ''Bennett and the Pathfinders'' Arms and Armour London 1996 ISBN 1-85409-258-8

==External links==
{{Commons category|Don Bennett}}
* [http://www.bbc.co.uk/programmes/b01ghdnl ''The Pathfinder'', BBC Radio 4, 27 April 2012]
* [http://www.iwm.org.uk/collections/item/object/80009167 Imperial War Museum Interview from 1986]

{{s-start}}
{{s-mil}}
{{s-new|reason=Force established}}
{{s-ttl|title=Officer Commanding [[Pathfinder Force]]|years=1942–1943}}
{{s-aft|after=Himself as AOC No. 8 Group}}
|-
{{s-vac|reason=Group re-established}}
{{s-ttl|title=[[Air Officer Commanding]] [[No. 8 Group RAF|No. 8 (Pathfinder Force) Group]]|years=1943–1945}}
{{s-aft|after=[[John Whitley]]}}
|-
{{s-par|uk}}
{{succession box
  | title  = [[Member of Parliament]] for [[Middlesbrough West (UK Parliament constituency)|Middlesbrough West]]
  | years  = 1945
  | before = [[Harcourt Johnstone]]
  | after  = [[Geoffrey Cooper (politician)|Geoffrey Cooper]]
}}
{{s-end}}
{{Use dmy dates|date=April 2012}}

{{Authority control}}

{{DEFAULTSORT:Bennett, Don}}
[[Category:1910 births]]
[[Category:1986 deaths]]
[[Category:Members of the Parliament of the United Kingdom for English constituencies]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Companions of the Order of the Bath]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Recipients of the Commendation for Valuable Service]]
[[Category:Recipients of the Order of Alexander Nevsky]]
[[Category:Royal Air Force air marshals of World War II]]
[[Category:Royal Australian Air Force officers]]
[[Category:Australian World War II pilots]]
[[Category:Liberal Party (UK) MPs]]
[[Category:UK MPs 1935–45]]
[[Category:Australian people of World War II]]
[[Category:Conservative Party (UK) politicians]]
[[Category:Australian emigrants to England]]