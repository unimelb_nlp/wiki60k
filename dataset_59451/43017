<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Skyboy
 | image=InterPlane Skyboy C-IDBX 01.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]] & [[Light-sport aircraft]]
 | national origin=[[Czech Republic]]
 | manufacturer=[[InterPlane Aircraft]]
 | designer=
 | first flight=1992
 | introduced=1992
 | retired=
 | status=Production completed
 | primary user=
 | number built=100<ref name="Hist">{{cite web|url = http://www.interplaneaircraft.com/about_us.htm|title = About InterPlane|accessdate = 16 March 2015|author = InterPlane Aircraft, Inc.|date = |archiveurl = https://web.archive.org/web/20130308030124/http://www.interplaneaircraft.com/about_us.htm |archivedate = 8 March 2013}}</ref>
 | developed from= 
 | variants with their own articles=
}}
|}

The '''InterPlane Skyboy''' is a two seat, side-by-side, high wing, single engine, [[pusher configuration]] [[ultralight aircraft]] that was manufactured as a completed aircraft by [[InterPlane Aircraft]] of [[Zbraslavice]], [[Czech Republic]].<ref name= "Purdy p. 181">{{harvnb|Purdy|1998|p=181}}</ref><ref name= "Cliche pp. B-101">{{harvnb|Cliche|1996| pp=B-101}}</ref><ref name="Interplane">{{cite web|url = http://www.interplaneaircraft.com/skyboy.htm |title = The Skyboy |accessdate = 2009-10-11|last = InterPlane|authorlink = |year = 2008}}</ref>

==Design and development==
[[File:InterPlane Skyboy C-IDBX 02.jpg|thumb|right|Interplane Skyboy - front view showing the unusual main landing gear]]
[[File:Interplane Skyboy at Sun n Fun.jpg|thumb|right|Interplane Skyboy - showing doors and interior accommodation]]
The Skyboy was designed in 1992 specifically for the [[Germany|German]] market as a [[Trainer (aircraft)|trainer]]. It was adapted for the US [[Ultralight aircraft (United States)|FAR 103 ''Ultralight Vehicles'']] category for use as a two-seat trainer under the FAR 103 trainer exemption. It later became available as a US [[light-sport aircraft]]. The aircraft is available in [[Canada]] as an [[Ultralight aircraft (Canada)#Advanced ultra-light aeroplane|Advanced Ultralight Aeroplane]].<ref name="Interplane" /><ref name="AULA">{{cite web|url = http://www.tc.gc.ca/eng/civilaviation/standards/general-ccarcs-advancedullist-2036.htm|title = Listing of Models Eligible to be Registered as Advanced Ultra-Light Aeroplanes (AULA)|accessdate = 2009-11-11|last = [[Transport Canada]]|authorlink = |date=November 2009}}</ref>

The Skyboy wing is built from [[aluminum]] extrusions for the [[spar (aviation)|spar]]s and wing ribs and covered with doped [[aircraft fabric]]. The wing's leading edge is [[Mylar]] covered in fabric, to increase stiffness. The wing is supported by "V" struts and utilizes [[jury struts]]. The [[fuselage]] is built upon an aluminum main tube that runs from the tail right to the rudder pedals. The wings and horizontal tail surfaces can be folded for trailering or storage. The cabin is constructed from two [[fibreglass]] shells, joined together. The rear of the cabin is covered in aircraft fabric. The optional cabin doors open upwards.<ref name= "Purdy p. 181"/><ref name= "Cliche pp. B-101"/>

Controls are conventional three-axis. The control stick is a centrally mounted "Y" stick, between the two seats that can be used from either seat.<ref name= "Cliche pp. B-101"/>

The Skyboy has a distinctive main landing gear, consisting of a trailing idler link, with suspension consisting of a coil spring mounted over a shock absorber. The company describes the main landing gear: "one of the best landing gears in the market (makes bad landings look good)".<ref name= "Cliche pp. B-101"/><ref name="Interplane" />

The available engines include the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]], {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912]], {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912S]] and the {{convert|85|hp|kW|0|abbr=on}} [[Jabiru 2200]]. The {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]] was available at the beginning of production, but did not provide adequate performance.<ref name="InterplaneSpecs">{{cite web|url = http://www.interplaneaircraft.com/skyboyspecs.htm |title = The Skyboy Specifications |accessdate = 2009-10-11|last = InterPlane|authorlink = |year = 2008}}</ref>

The Skyboy has never been available as a [[kit aircraft]], but only as a factory-complete, ready-to-fly product. Labour wage levels in the Czech Republic have meant that its price has been generally similar to buying unassembled kits for North American buyers. In 2008 the completed Skyboy base model sold for about [[United States dollar|US$]]60,000.<ref name= "Cliche pp. B-101"/><ref name="Interplane" />

==Operational history==
Aside from its [[flight training]] and recreational aircraft roles, the Skyboy has been employed in [[South Africa]] for surveillance duties, in [[Australia]] for professional aerial photography and in [[Mexico]] for tourist sightseeing flights.<ref name="Interplane" />

==Variants==
;Skyboy UL
:To stay below the exemption's {{convert|496|lb|kg|0|abbr=on}} empty weight limit the aircraft was marketed without cabin doors, [[Aircraft fairing|wheel pants]] and hydraulic brakes, using mechanical brakes instead and with a {{convert|904|lb|kg|0|abbr=on}} gross weight. The initial engine was the {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]] and later the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]].<ref name= "Cliche pp. B-101"/>
;Skyboy S
:Higher {{convert|1232|lb|kg|0|abbr=on}} gross weight version for Europe and Canada, with the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] or larger engines.<ref name= "Purdy p. 181"/><ref name="AULA" />
;Skyboy ZK
:{{convert|1000|lb|kg|0|abbr=on}} gross weight version, with the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine.<ref name="AULA" />
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Skyboy S)==
{{Aircraft specs
|ref=Purdy, Cliche & InterPlane<ref name= "Purdy p. 181"/><ref name= "Cliche pp. B-101"/><ref name="InterplaneSpecs" /><!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=20
|length in=0
|length note=
|span m=
|span ft=34
|span in=5
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=163.6
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=490
|empty weight note=
|gross weight kg=
|gross weight lb=1000
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=10 US gallons (38 litres)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type=twin cylinder [[two-stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=64<!-- prop engines -->
|eng1 note=
|power original=
|thrust original=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=90
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=68
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=40
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=128
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=200
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=9000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=7:1<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=630
|climb rate note=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=
|thrust/weight=

|more performance=

|avionics=
}}

==See also==
{{Portal|Aviation}}
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[Birdman Chinook|Birdman WT-11 Chinook]]
*[[Spectrum Beaver|Spectrum RX-28 Beaver]]
|lists=<!-- related lists -->
}}

==Bibliography==

;Notes
{{reflist|2}}
;References
{{refbegin|2}}
*{{cite book |ref=harv|last=Purdy|first=Don | authorlink = | title = AeroCrafter homebuilt aircraft sourcebook|edition=1998|year=1998| publisher = Bai Communications| isbn= 0-9636409-4-1 }} <small>- Total pages: 432 </small>
*{{cite book |ref=harv|last=Cliche|first=André | authorlink = | title = Ultralight aircraft reference guide|edition=1996|year=1996| publisher = Cybair| isbn= 0-9680628-1-4 }} <small>- Total pages: 310 </small>
{{refend}}

==External links==
{{Commons category}}
*{{Official website|http://www.interplaneaircraft.com/skyboy.htm}}

{{InterPlane Aircraft}}

[[Category:InterPlane aircraft|Skyboy]]
[[Category:Czech and Czechoslovakian ultralight aircraft 1990–1999]]
[[Category:Light-sport aircraft]]
[[Category:Pusher aircraft]]