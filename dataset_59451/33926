{{featured article}}

{{Infobox coin
| Country               = United States
| Denomination          = Three-cent silver
| Value                 = 3 cents (.03
| Unit                  = [[United States dollar|US dollars]])
| Mass                  = 1851–1853, .80 g.  Later pieces, .75
| Diameter              = 14
| Edge                  = plain
| Composition           = 1851–1853, .750 [[silver]], .250 [[copper]].  Later pieces, .900 silver, .100 copper
| Silver_troy_oz        = 1851–1853, .0193 troy oz. Later issues, .0217
| Years of Minting      = 1851–1873
| Mint marks            = [[New Orleans Mint|O]]. To the right of the Roman numeral on the reverse (1851 only). [[Philadelphia Mint]] specimens struck without mint mark.
| Obverse               = 1851-O 3CS (obv).jpg
| Obverse Image Size    = 
| Obverse Design        = Type 1
| Obverse Designer      = [[James B. Longacre]]
| Obverse Design Date   = 1851
| Obverse Discontinued  = 1853
| Obverse2               = 1858 3CS (obv).jpg
| Obverse2 Image Size    = 
| Obverse2 Design        = Type 2
| Obverse2 Designer      = [[James B. Longacre]]
| Obverse2 Design Date   = 1854
| Obverse2 Discontinued  = 1858
| Obverse3              = {{Css Image Crop|Image = 1866 3CS.jpg |bSize = 490|cWidth = 235|cHeight = 235|oTop = 6|oLeft = 6|Location = center}}
| Obverse3 Image Size   = 
| Obverse3 Design       = Type 3
| Obverse3 Designer     = [[James B. Longacre]]
| Obverse3 Design Date  = 1859
| Obverse3 Discontinued = 1873
| Reverse               = 1851-O 3CS (rev).jpg
| Reverse Image Size    = 
| Reverse Design        = Type 1
| Reverse Designer      = [[James B. Longacre]]
| Reverse Design Date   = 1851
| Reverse Discontinued  = 1853
| Reverse2               = {{Css Image Crop|Image = 1866 3CS.jpg |bSize = 490|cWidth = 235|cHeight = 235|oTop = 6|oLeft = 248|Location = center}}
| Reverse2 Image Size    = 
| Reverse2 Design        = Types 2 and 3
| Reverse2 Designer      = [[James B. Longacre]]
| Reverse2 Design Date   = 1854
| Reverse2 Discontinued  = 1873
}}

The '''three-cent silver''', also known as the '''three-cent piece in silver''' or '''trime''', was struck by the [[United States Mint|Mint of the United States]] for circulation from 1851 to 1872, and as a [[proof coin]] in 1873. Designed by the Mint's chief engraver, [[James B. Longacre]], it circulated well while other silver coinage was being hoarded and melted, but once that problem was addressed, became less used.  It was abolished by Congress with the [[Coinage Act of 1873]].

After a massive importation of gold bullion during the [[California Gold Rush]], silver could be traded for increasing amounts of gold, so U.S. silver coins were exported and melted for their metal. This, and the reduction of postage rates to three cents, prompted Congress in 1851 to authorize a coin of that denomination made of .750 fine silver, rather than the conventional .900. The three-cent silver was the first American coin to contain metal valued significantly less than its face value, and the first silver coin not to be [[legal tender]] for an unlimited amount. The coin saw heavy use until Congress acted again in 1853, making other silver coins lighter, which kept them in circulation.  Congress also lightened the three-cent silver, and increased its fineness to 900 silver.

With the return of other denominations to circulation, the three-cent silver saw less use, and its place in commerce was lost with the economic chaos of the [[American Civil War]], which led to hoarding of all gold and silver coins.  A [[three-cent nickel|three-cent piece in copper-nickel]] was struck beginning in 1865, and the three-cent silver saw low mintages for its final decade before its abolition. The series is not widely collected, and the pieces remain inexpensive relative to other U.S. coins of similar scarcity.

== Background ==
Although the [[United States Mint|Mint of the United States]] had been striking silver coins since the 1790s, they did not always circulate due to fluctuations in the price of the metal.  In 1834, for example, [[half dollar (United States coin)|half dollars]] sold on the market at a premium of one percent. The U.S. was then on a [[bimetallism|bimetallic]] standard, and though Congress had slightly overvalued silver with respect to gold, enough Mexican silver flowed into the country to produce a rough equilibrium.{{sfn|Taxay|pp=217–218}}

By early 1849, most of the silver coins in circulation were small coins of the [[Spanish colonial real]], including the "levy" (one real) and "fip" (half real).{{efn|"Levy" and "fip" were corruptions of their value expressed in early Pennsylvania currency, eleven pence for the levy and five and a half pence for the fip. See {{harvnb|Taxay|p=217 n.3}}.}} The levy and fip often passed for twelve and six cents respectively in the Eastern U.S. The mint accepted them as payment at a slightly lower figure, but even so, lost money on the transactions as many of the pieces were lightweight through wear. The odd denominations of the levy and fip were a convenience, allowing payment or change to be made without the use of [[large cent (United States coin)|cents]], which were at that time large, made of copper, and not accepted by the government as legal tender due to their lack of precious metal.{{sfn|Taxay|pp=217–218}} In the Western U.S., the levy and fip were accepted as the equivalent of the silver [[dime (United States coin)|dime]] and [[half dime]], although the Spanish pieces contained more silver.{{sfn|Carothers|pp=102–105}}

== Inception ==
Bullion from the [[California Gold Rush]] and other discoveries came to the Eastern U.S. in considerable quantities beginning in 1848.  By the following year, the price of gold relative to silver had dropped, making it profitable to export American silver coins, sell them as bullion, and use the payment in gold to buy more U.S. coins. Silver coins consequently vanished from circulation, meaning the highest-value American coin actually circulating that was worth less than the [[quarter eagle]] ($2.50 piece) was the half-dollar-sized copper cent, which saw no use in much of the country because of its lack of legal tender status. Early in 1849, Congress authorized a [[gold dollar]] to help bridge the gap.  Spanish silver coins were the bulk of what was left in commerce for small change, although there was disagreement as to the value to be assigned to them.  Additionally, they were often heavily worn, reducing their intrinsic worth at a time when Americans expected coins to contain metal worth the value assigned to them.{{sfn|Carothers|pp=102–105}}

In 1850, New York Senator [[Daniel S. Dickinson]] introduced legislation for a three-cent piece in .750 fine silver, that is, three parts silver to one part copper (American silver coins were then .900 fine).  He proposed to offer it in exchange for the Spanish silver, which would be valued at eight reals to the dollar for the purpose, higher than the going rate.  The new coin would weigh three-tenths as much as the dime, but the debasement of the silver would compensate the government for the losses it would take in redeeming the underweight, worn Spanish coins. The three-cent denomination was chosen as it coordinated well with the six and twelve cent values often assigned the fip and levy. The House of Representatives instead considered legislation to reduce the valuation of the Spanish coins to ten cents per real, and to strike a [[twenty-cent piece (United States coin)|twenty-cent piece]], of .900 silver, to facilitate the exchange.  Neil Carothers, in his book on small-denomination American money, suggests that the House's plan would have resulted in the Spanish coins staying in circulation, and any twenty-cent pieces issued being hoarded or melted. No legislation passed in 1850, which saw continued export of America's silver coinage.{{sfn|Carothers|pp=106–107}}

Impetus for the passage of a three-cent coin came when Congress, in January 1851, considered reducing postage rates from five cents to three.  In 1849, [[House Committee on Ways and Means]] chairman, [[Samuel Vinton]], had written to Mint director [[Robert M. Patterson]] that his committee was considering both reducing the postage rate and instituting a three-cent coin.  Although no legislative action was then taken, Patterson had the mint prepare experimental [[pattern coins]].{{sfn|Flynn & Zack|p=7}} The House committee proposing the 1851 bill included Dickinson's three-cent piece, and provided that it be legal tender up to 30 cents.  When the bill was debated in the House on January 13, 1851, New York Congressman [[William Duer (U.S. Congressman)|William Duer]] indicated that he felt both coin and stamp should be denominated at 2{{frac|1|2}} cents, and his fellow New Yorker, [[Orsamus Matteson]], offered an amendment to that effect.  The amendment failed, as did every other attempt to change the legislation, including Dickinson's plea, in the Senate, to restore the requirement that the new coin be used to retire some of the Spanish silver.  The bill passed both houses, and became the Act of March 3, 1851 when President [[Millard Fillmore]] signed it.{{sfn|Julian|p=53}}{{sfn|Carothers|pp=107–108}}{{sfn|Taxay|pp=218–219}}

Carothers pointed out the precedent-setting nature of the legislation, the first to authorize an American silver coin containing an amount of metal worth considerably less than its face value:

{{quote |
This almost forgotten statute is one of the most significant measures in American currency history. After resisting for sixty years every attempt to introduce any form of fiduciary silver coinage, Congress adopted a subsidiary silver coin as an adjunct to the postal service, without realizing that the first step had been taken in the relegation of silver to the status of a subordinate monetary material. The new piece was the first silver coin in the history of the United States that was not legal tender for an unlimited amount.  Subsidiary coinage had been established, but in a trivial way, by an unworkable law, and at a time when the entire silver currency was flowing out of the country.{{sfn|Carothers|p=108}}
}}

== Preparation ==
[[File:1850 P3CS Three Cent Silver (Judd-125 Original) (obv).jpg|thumb|left|[[Pattern coin]] struck to Peale's design for the [[three-cent piece]] in silver]]
In addition to striking the pattern coins in 1849, officials at the [[Philadelphia Mint]] continued to experiment with three-cent silvers.  The matter was caught up in ongoing conflict between Mint chief coiner [[Franklin Peale]], and the chief engraver, [[James B. Longacre]], who each prepared designs.  Peale produced a coin depicting a [[pileus (hat)|Liberty cap]], based on a design prepared by Longacre's late predecessor, [[Christian Gobrecht]], in 1836. Longacre prepared a design similar to the coin that was eventually released.{{sfn|Flynn & Zack|p=7}}

On March 2, 1851, the day before the legislation was passed, Longacre, with the reluctant permission of Patterson (a Peale ally) sent [[United States Secretary of the Treasury|Treasury Secretary]] [[Thomas Corwin]] samples of his proposed three-cent piece, along with a letter explaining the symbology. Patterson preferred Peale's design, but recommended Longacre's for approval on the ground that it was in lower [[relief]].{{sfn|Breen|p=271}} Patterson, writing to Corwin on March 7, indicated that if the three-cent were struck in .900 silver, it would be hoarded, but as the silver in the .750 pieces was worth only two and a half cents, the Mint would profit via [[seignorage]] from coining the new pieces. Patterson also suggested that the [[New Orleans Mint]] could be used to strike the new coin.{{sfn|Flynn & Zack|p=7}}

Despite the provision of the [[Mint Act of 1837]] entrusting Longacre, by virtue of his office, with responsibility for preparing dies, Peale prepared his own and ran off some sample three-cent pieces.  Both types of pattern coin were sent by Patterson to Corwin on March 25, 1851, with the Mint director's recommendation that the chief engraver's design be selected.  The next day, acting Treasury Secretary William L. Hodge approved Longacre's design.{{sfn|Taxay|pp=219–220}} Knowing there was a large demand for the new coins, Patterson thought it best to build up a stock of 500,000 before beginning distribution.{{sfn|Carothers|p=109}}

== Design ==

Art historian [[Cornelius Vermeule]], in his book on American coins and medals, considered the silver three-cent piece one of the ugliest U.S. coins, though it "has the redeeming feature of delicate workmanship".{{sfn|Vermeule|p=191}} Dennis Tucker, head of Whitman Publishing, in 2016 described the coin as "something of a 'Sarah plain and small'."<ref>{{cite news|url=https://www.coinworld.com/news/us-coins/2016/07/philadelphia-mint-some-ugliest-coins-ever.html?utm_medium=Email&utm_source=ExactTarget&utm_campaign=cw_editorial_digital-edition&utm_content=|title=Ugly duckling coins may mark the low points of design, but they have their charm|last=Roach|first=Steve|date=July 15, 2016|accessdate=July 17, 2016|newspaper = [[Coin World]]}}</ref> Congress had required, in the authorizing act for the three-cent silver, that the piece bear a design distinct from both the gold dollar and the other silver coins.{{sfn|Bureau of the Mint|p=38}} As Longacre wrote in his letter to Corwin of March 2, 1851,

{{ quote |
On so small a coin it is impossible that the device can be at once conspicuous and striking unless it is simple—complexity would defeat the object. For the [[Obverse and reverse|obverse]] I have therefore chosen a star (one of the heraldic elements of the National crest) bearing on its centre the shield of the Union, surrounded by the legal inscription and date. For the reverse I have devised an ornamental letter C embracing in its centre the Roman numeral III, the whole encircled by the thirteen stars.{{sfn|Flynn & Zack|p=155}}
}}

Longacre's original design for the reverse (type 1) was altered when the fineness of the coin was increased in 1854, to aid the public in distinguishing between them.  For the type 2 and type 3, (the reverses of which are identical, the only differences between the two types are on the obverse) an olive sprig, a symbol of peace, was added over the ''III'', and a bundle of three arrows, a symbol of war, below it. The arrows are bound by a ribbon.{{sfn|Flynn & Zack|pp=5, 9}}

== Production ==

=== Type 1 (1851–1853) ===
[[File:1776 Potosi 2 reales obv.jpg|thumb|right|A Spanish colonial two-reals piece ("two bits") from the Potosí Mint (today in Bolivia)]]
According to numismatic historian [[Walter Breen]], "the new 3¢ coins were minted in large quantities, went immediately into circulation, and stayed there".{{sfn|Breen|p=271}} Despite mechanical difficulties in striking so small a piece,{{sfn|Julian|p=53}} a total of 5,446,400 were struck at Philadelphia in 1851, and 720,000 at New Orleans—the latter would prove the only mintage of three-cent pieces outside Philadelphia. The coins were shipped directly to post offices for use with stamps.  Members of the public who wanted pieces were refused them by mint officials, who advised would-be purchasers to seek them at treasury depository branches.{{sfn|Flynn & Zack|p=8}}

The small size of the coins, which were dubbed "fish scales", was disliked as they were easily lost.{{sfn|Flynn & Zack|p=8}} The mint used them to redeem some of the Spanish silver, but the bulk of those foreign coins remained in circulation.{{efn|Congress eventually deprived the Spanish silver coins of legal tender status in 1857 and made them exchangeable for the [[flying eagle cent]].}}  A shopper paying for a small purchase with a gold dollar might receive fifteen or so three-cent pieces and the remainder in badly worn fips and other small silver coins.  One Philadelphia newspaper reported, derisively, that merchants were reduced to giving ladles full of three-cent pieces in change for a five-dollar banknote.{{sfn|Carothers|pp=111–112}}

Silver coins continued to flow out of the U.S. in 1852, and the three-cent silver saw its highest mintage, 18,663,500, all from Philadelphia.{{sfn|Flynn & Zack|p=8}} The value of these pieces was larger than that of all other silver coins struck by the mints in 1852.{{sfn|Carothers|p=109}} The chaotic state of commerce, with no circulating federal coin valued between the three-cent piece and gold dollar, was a source of concern, and mint officials and congressmen corresponded in 1852 concerning a reduction in weight of silver coins such as the half dime and half dollar. Congress at last responded by laws passed on February 21, 1853 and March 3, 1853.  These reduced the weight of all silver coins except for the [[Seated Liberty dollar|silver dollar]].  The three-cent piece had its weight reduced from .8 grams to .75, but its fineness increased to .900.{{sfn|Flynn & Zack|pp=8–9}} Although the other reduced-weight silver coins were given legal tender limits of five dollars, that of the three-cent piece remained at thirty cents.  Carothers theorized, "Congress, probably realizing that the 3 cent piece was a misfit at best, preferred to leave it with a discordant legal tender value".{{sfn|Carothers|pp=122–123}} Pursuant to these congressional acts, mintage of the type 1 three-cent silver stopped on March 31, 1853.{{sfn|Breen|p=271}} These changes to the silver coinage alleviated the problem of small change, as the new lightweight coins remained in circulation and were not then hoarded.{{sfn|Carothers|p=123}}

=== Type 2 (1854–1858) ===
The shift to .900 silver for the three-cent silver was intended to help drive the Spanish coins out of circulation.  Longacre made changes to both sides of the three-cent silver, engraving a triple line around the star on the obverse and adding an olive branch and bunch of arrows to the reverse.{{sfn|Goldstein|p=14}} These parallel changes were made to the other silver coins of less than a dollar to distinguish the new, lighter coins from the old.  Since it was more urgent to complete work on the modifications to silver coins such as the half dollar and [[Quarter (United States coin)|quarter]], Longacre left the three-cent piece for last, and did not complete work on the coin until late 1853. The new treasury secretary, [[James Guthrie (Kentucky)|James Guthrie]], approved the changes on November 10.{{sfn|Flynn & Zack|p=9}}

Starting with 1854, small quantities of [[proof coin]]s were struck and apparently distributed in sets with the other silver coins. Beginning in 1858, Mint director [[James Ross Snowden]] made the [[proof set]]s available to the general public.{{sfn|Flynn & Zack|p=9}}

The 1853 acts had prohibited the mint from buying silver from the public.  Since the silver dollar was heavy relative to its value, little silver was presented for striking into that piece.  As the statutes did not permit the public to deposit silver and receive it back in the form of subsidiary silver coins (the three-cent piece through half dollar), this effectively placed the U.S. on the [[gold standard]]. Despite the statutes, in 1853 and 1854, Snowden had the mint purchase large quantities of silver bullion at a fixed price, generally above the market rate, and struck it into coin.  Since the subsidiary coins were only legal tender to five dollars, and could not be redeemed for gold, this led to a glut of silver coins in commerce. This oversupply, which persisted through 1862,{{sfn|Carothers|pp=129–136}} led to lower mintages of silver coins in the mid-1850s, including the three-cent piece.{{sfn|Flynn & Zack|p=9}} The largest mintage for the type 2 three-cent silver was in 1858, when 1,603,700 were struck for circulation.{{sfn|Yeoman|p=579}}

=== Type 3 (1859–1873) ===
Although there is no archival evidence, Breen theorized that in 1858 Snowden ordered Longacre to make changes to improve striking quality, as most type 2 pieces were weakly struck.  The changes include removal of one of the outlines around the star, with smaller and more evenly spaced lettering. Breen suggested that the lettering displays the influence of Assistant Engraver Anthony C. Paquet, who likely assisted Longacre.{{sfn|Breen|p=273}} These changes only affected the obverse; the reverse was not altered.{{sfn|Flynn & Zack|p=9}}

The economic chaos of the [[American Civil War|civil war]] brought the introduction of [[Legal Tender Note|legal tender notes]], backed only by the credit of the government, and by mid-1862, gold and silver coins had vanished from circulation in much of the nation, their place taken by such makeshifts as [[fractional currency]] and [[postage stamp]]s.{{sfn|Carothers|pp=156–158}} The three-cent silver remained in circulation longer than the other silver coins, apparently because the public thought it was still made of debased silver, but by the autumn<!-- source says "fall" --> of 1862, it too was hoarded.{{sfn|Julian|p=54}} With little point to issuing coins that would not circulate, mintage of the three-cent silver dropped from 343,000 in 1862 to 21,000 in 1863, a figure that would be exceeded only once (22,000 in 1866) during the rest of the series.{{sfn|Yeoman|pp=579–581}} In March 1863, Treasury Secretary [[Salmon P. Chase]] wrote in a letter that the three-cent silver had entirely vanished from circulation, and suggested that it be issued in aluminum to avoid hoarding.{{sfn|Flynn & Zack|p=10}}

Congress began the task of restoring federal coins to circulation in 1864 with a [[Two-cent piece (United States)|two-cent piece]] in bronze, and by authorizing a [[three-cent nickel|three-cent piece in copper-nickel]] the following year.{{sfn|Flynn & Zack|p=10}} In 1866, it authorized a five-cent piece in the latter alloy, the [[nickel (United States coin)|nickel]], as it has come to be known.{{sfn|Bureau of the Mint|pp=48–49}} With the three-cent silver effectively replaced by base-metal pieces, it did not share in the increased mintage of silver coins that began in 1868.{{sfn|Flynn & Zack|p=10}} The three-cent silver was struck at the rate of a few thousand per year into the 1870s.{{sfn|Yeoman|pp=580–581}}

In 1870, Treasury Secretary [[George Boutwell]] sent Congress a draft bill to replace the outdated 1837 Mint Act and the bits of legislation passed over the years regarding the mint and coinage.  Even in the draft bill, no provision was made for the three-cent silver, though some in Congress wished to retain the coin in anticipation of the [[Specie Payment Resumption Act|resumption of specie payments]].  After much debate in Congress, President [[Ulysses S. Grant]] signed the [[Coinage Act of 1873]] on February 12 of that year.  The law abolished the two-cent piece, three-cent silver, half dime, and standard silver dollar (later restored in 1878). Carothers called the abolition of the silver three- and five-cent pieces "a necessity if the 3 cent and 5 cent nickel pieces were to be continued after the revival of silver coinage".{{sfn|Carothers|pp=226–236}} Breen deemed the decision to eliminate the silver three-cent piece and the half dime, which might have directly competed with the two copper-nickel coins, a favor to industrialist [[Joseph Wharton]], whose mines produced much of the nickel ore used in coinage.{{sfn|Breen|p=295}} According to numismatist R.&nbsp;W. Julian, the three-cent silver "had played its part well in the U.S. monetary system, but there was no longer any need for it".{{sfn|Julian|p=54}}

== Aftermath  ==
Large quantities of the three-cent silver, including much of the production from 1863 and later, were held by the treasury and were melted after the coin was deauthorized.{{sfn|Yeoman|p=575}} The three-cent nickel went the way of its silver counterpart.  After years of low mintages and decreasing popularity, it was abolished by the Act of September 26, 1890 along with the gold dollar and [[three-dollar piece]].{{sfn|Goldstein|p=18}}

The three-cent silver was made fully legal tender by the [[Coinage Act of 1965]], which proclaimed all coin and currency of the United States good to any amount for payment of public and private debt. By then, that coin had long since disappeared from circulation.{{sfn|Breen|p=243}}

== Collecting ==
According to Kevin Flynn and Winston Zack in their book on the three-cent silver, "Lower interest [in that piece] means that it is the type of coin [on] which you can find great deals on pricing, even on dates in which rarity is high."{{sfn|Flynn & Zack|p=5}} According to [[R. S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'', published in 2015, the ones that catalog the highest are the proof issues from before 1858, with the 1854 issue leading the way, listed at $35,000.{{sfn|Yeoman|pp=578–581}} Not listed is the 1851 proof three-cent silver, of which only one is known, which was last sold in 2012 for $172,500, and once formed part of the [[Louis Eliasberg]] collection. Flynn and Zack theorize that this was the specimen viewed by Hodge in approving Longacre's design, as there is no record of its return to the Philadelphia Mint.{{sfn|Flynn & Zack|p=138}}<ref>{{cite web|title=1851 3CS (Proof)|publisher=PCGS|url=http://www.pcgscoinfacts.com/Coin/Detail/3696?redir=t|accessdate=March 30, 2015}}</ref>

The 1873 coin, the final issue, was struck in proof only, and is listed at between $825 and $2,000, depending on condition. Most U.S. coins of that year were struck with a "close ''3''"{{efn|Sometimes known as a "closed ''3''"}} in the date, and, following a complaint that the ''3'' too closely resembled an eight, an "open ''3''" variety.  The three-cent silver exists only as a close ''3''. Of coins struck for circulation, Yeoman's highest listings are for the 1868, at $11,000 in near pristine [[coin grading|MS-66]] condition.{{sfn|Yeoman|pp=578–581}}{{sfn|Flynn & Zack|p=52}}

Yeoman lists all three-cent pieces before 1863 at $27 in worn [[coin grading|G-4 condition]], except the 1851-O ($40) and the 1855 ($38). All three-cent pieces from and after 1863 are less valuable in proof than in uncirculated MS-63 condition.{{sfn|Yeoman|pp=580–581}} Much of the mintage of later dates were melted by the Mint after the end of the series.{{sfn|Flynn & Zack|p=17}}

== Mintages and rarity ==
The [[mint mark]] appears on the reverse, to the right of the ''III'', in the opening of the ''C'' that encloses it.

* Blank (Philadelphia Mint in Philadelphia)
* O (New Orleans Mint in New Orleans, 1851 only)

{| class="wikitable"
! Year
! Mint mark
! Proofs{{sfn|Yeoman|pp=578–581}}
! Circulation strikes{{sfn|Yeoman|pp=578–581}}
|-
| 1851
|
| 
| 5,447,400
|-
| 1851
| O
|
| 720,000
|-
| 1852
|
|
| 18,663,500
|-
| 1853
|
|
| 11,400,000
|-
| 1854
| 
|
| 671,000
|-
| 1855
|
| 
| 139,000
|-
| 1856
| 
|
| 1,458,000
|-
| 1857
| 
|
| 1,042,000
|-
| 1858
|
| 210
| 1,603,700
|-
| 1859
| 
| 800
| 364,200
|-
| 1860
| 
| 1,000
| 286,000
|-
| 1861
|
| 1,000
| 497,000
|-
| 1862
| 
| 550
| 343,000
|-
| 1863
| 
| 460
| 21,000
|-
| 1864
|
| 470
| 12,000
|-
| 1865
| 
|500
| 8,000
|-
| 1866
| 
| 725
| 22,000
|-
| 1867
|
| 625
| 4,000
|-
| 1868
|
| 600
| 3,500
|-
| 1869
| 
| 600
| 4,500
|-
| 1870
| 
| 1,000
| 3,000
|-
| 1871
|
| 960
| 3,400
|-
| 1872
| 
| 950
| 1,000
|-
| 1873
| 
|600
|
|}

== Notes and references ==
'''Notes'''
{{notes}}

'''Citations'''
{{reflist
| colwidth = 20em
| refs =

}}

'''Bibliography'''

'''Books'''
* {{cite book
  | last = Breen
  | first = Walter
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York, NY
  | isbn = 978-0-385-14207-6
  | ref = {{sfnRef|Breen}}
  }}
* {{cite book
  | author = Bureau of the Mint
  | year = 1904
  | title = Laws of the United States Relating to the Coinage
  | publisher = United States Government Printing Office
  | location = Washington, D.C.
  | url = https://books.google.com/books?id=rmtAAAAAYAAJ
  | oclc = 8109299
  | ref = {{sfnRef|Bureau of the Mint}}
  }}
* {{cite book
  | last = Carothers
  | first = Neil
  | year = 1930
  | title = Fractional Money: A History of Small Coins and Fractional Paper Currency of the United States
  | publisher = John Wiley & Sons, Inc. (reprinted 1988 by Bowers and Merena Galleries, Inc., Wolfeboro, NH)
  | location = New York, NY
  | isbn = 0-943161-12-6
  | ref = {{sfnRef|Carothers}}
  }}
* {{cite book
  | last = Flynn
  | first = Kevin
  | last2 = Zack
  | first2 =Winston
  | year = 2010
  | title = The Authoritative Reference on Three Cent Silver Coins
  | publisher = Kyle Vick
  | location = Roswell, GA
  | oclc = 770720399
  | ref = {{sfnRef|Flynn & Zack}}
|lastauthoramp=y
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | year = 1983
  | title = The U.S. Mint and Coinage
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York, NY
  | edition = reprint of 1966
  | isbn = 978-0-915262-68-7
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, MA
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R. S.
  | authorlink = Richard S. Yeoman
  | year = 2015
  | title = [[A Guide Book of United States Coins]]
  | edition = 1st Deluxe
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-4307-6
  | ref = {{sfnRef|Yeoman}}
  }}

'''Journals'''

* {{cite journal
  | last = Goldstein
  | first = Bruce C.
  | date = June 6, 2011
  | title = The Power of 3s
  | work = Coin World
  | publisher = Amos Press, Inc.
  | location = Sidney, OH
  | pages = 4–5, 14–15, 18
  | ref = {{sfnRef|Goldstein}}
  }}
* {{cite journal
  | last = Julian
  | first = R. W.
  | journal = Coins
  | publisher =Krause Publications
  |location =Iola, WI
  | date = July 1998
  | title = The ''Trime'' of Our Lives
  | pages = 52–54
  | ref = {{sfnRef|Julian}}
  }}

{{Obsolete U.S. currency and coinage}}
{{Coinage (United States)}}

[[Category:1851 introductions]]
[[Category:Coins of the United States]]