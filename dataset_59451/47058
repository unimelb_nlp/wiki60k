{{Infobox recurring event
| name         = <!--Uses page name if omitted-->
| native_name  = 
| native_name_lang = 
| logo         = 
| logo_alt     =
| logo_caption = 
| logo_size    =
| image        = 
| image_size   =
| alt          =
| caption      = 
| status       = <!-- e.g. defunct, active, inactive ... -->
| genre        = [[Festival|Culinary festival]]<!-- e.g. natural phenomena, fairs, festivals, conferences, exhibitions ... -->
| date         = <!-- {{start date|YYYY|mm|dd}} "dates=" also works, but do not use both -->
| begins       = <!-- {{start date|YYYY|mm|dd}} -->
| ends         = <!-- {{start date|YYYY|mm|dd}} -->
| frequency    = <!-- Weekly, Monthly, Quarterly, Semi-annually, Annually, Bi-annually, 2nd Tuesday of November, etc. -->
| venue        = [[Washington Park (Cincinnati, Ohio)]]
| location     = [[Over-The-Rhine]]
| coordinates  = <!-- {{coord|LAT|LON|type:event|display=inline,title}} --> 
| country      = 
| years_active = <!-- {{age|YYYY|mm|dd}} Date of the first occurrence -->
| first        = 2014<!-- {{start date|YYYY|mm|dd}} "founded=" and "established=" also work -->
| founder_name = Donna Covrett, Courtney Tsitouris
| last         = <!-- Date of most recent event; if the event will not be held again, use {{End date|YYYY|MM|DD|df=y}} -->
| prev         = 
| next         = 
| participants = 
| attendance   = 2000 (2014)
| capacity     =
| area         = 
| budget       = 
| activity     = 
| leader_name  =
| patron       = 
| organised    = <!-- "organized=" also works -->
| filing       = 
| people       = 
| member       = 
| sponsor      =
| website      = <!-- {{URL|example.com}} -->
| footnotes    = 
}}
The '''Cincinnati Food + Wine Classic''' is an annual [[Festival|culinary festival]] in the [[Over-the-Rhine]] historic district of [[Cincinnati]], Ohio.  The event was first produced in 2014 and draws presenters and attendees from throughout the midwest and the nation.

==History==
The Food + Wine Classic was first held in 2014<ref name=cincimag1409>{{cite web|last1=Walters|first1=Amanda Boyd|title=Cincinnati Food + Wine Classic|url=http://www.cincinnatimagazine.com/forkopolisblog/cincinnati-food-wine-classic/|publisher=Cincinnati Magazine|accessdate=11 September 2015}}</ref> as a two-day event at [[Washington Park (Cincinnati, Ohio)|Washington Park]] in the Over-the-Rhine<ref name=cincimag1409 /> historic district<ref name=indystar1409 /> and expanded to three days for the 2015 event.<ref name=indrest1507 />  Former [[Cincinnati Magazine]] dining editor  Donna Covrett<ref name=indrest1507>{{cite web|last1=Crone|first1=Emily Starbuck|title=How – and Why – to Join a Festival|publisher=Independent Restaurateur|accessdate=11 September 2015}}</ref><ref name=citybeat150909 /> and local food writer Courtney Tsitouris<ref name=indrest1507 /><ref name=citybeat150909 /> developed the Classic to "showcase local and regional talent"<ref name=cincimag1409 /><ref name=indystar1409>{{cite web|last1=Petry|first1=Ashley|title=Celebrating food & wine in Cincinnati|url=http://www.indystar.com/story/life/2014/08/09/cincinnati-food-wine-travel/13821675/|publisher=Indianapolis Star|accessdate=11 September 2015}}</ref><ref name=indrest1507 /> and to "celebrate (the city's) [[History of Cincinnati#.27Porkopolis.27 and other nicknames|Porkopolis]] heritage."<ref name=cincimag1409 /><ref name=indystar1409 /> More than 80 chefs participated in 2014<ref name=indystar1409 /> and over 100 in 2015.<ref name=fox150910>{{cite web|last1=Daykin|first1=Margaret|title=Morning Show|url=http://fox8.com/2015/09/10/fox-recipe-box-baked-apple-crumble/|publisher=Fox8.com|accessdate=11 September 2015}}</ref><ref name=wvxu150826>{{cite web|last1=Heyne|first1=Mark|title=The 2nd Annual Cincinnati Food + Wine Classic Returns To Washington Park Next Month|url=http://wvxu.org/post/2nd-annual-cincinnati-food-wine-classic-returns-washington-park-next-month#stream/0|publisher=WVXU|accessdate=11 September 2015}}</ref>

==Program==
The event includes competitions, tastings, demonstrations, workshops, book signings, and classes offered by chefs, sommeliers, mixologists, winemakers, brewers, distillers, and food writers.<ref name=campbell151009 /><ref name=wxix150824>{{cite web|last1=Foxx|first1=Kara|title=Food + Wine Classic plants foodie paradise in Washington Park|url=http://www.fox19.com/story/29867614/food-wine-classic-plants-foodie-paradise-in-washington-park|publisher=WXIX|accessdate=11 September 2015}}</ref> The event kicks off each year with "Pork Chopped," a competition among chefs to create the best dish<ref name=indystar1409 /> using "the meat that made Cincinnati famous."<ref name=campbell151009 />

==Attendance==
The Food + Wine Classic draws chefs<ref name=indrest1507 /><ref name=fox150910 /><ref>{{cite web|title=Cincinnati Food & Wine Classic Preview|url=http://www.msn.com/en-in/health/medical/cincinnati-food-and-wine-classic-preview/vi-AAdPuQf?refvid=BBm47r4|publisher=MSN|accessdate=11 September 2015}}</ref><ref name=abc22.1509>{{cite web|title=Cinci Food & Wine Classic|url=http://www.abc22now.com/news/features/morning/stories/Cinci-Food-Wine-Classic-202097.shtml|publisher=ABC22now.com|accessdate=11 September 2015}}</ref><ref name=daytonbj1407>{{cite web|last1=Barrow|first1=Olivia|title=Dayton chef part of Cincinnati festival|url=http://www.bizjournals.com/dayton/blog/2014/07/dayton-chef-part-of-cincinnati-festival.html|publisher=Dayton Business Journal|accessdate=11 September 2015}}</ref> and food writers from throughout the midwest and across the country.<ref name=cincimag1409 /><ref name=indrest1507 /><ref name=campbell151009 /><ref name=wcpo150910>{{cite web|last1=Yek|first1=Grace|title=Former Saveur editor Pandolfi returns to Cincinnati roots at Food + Wine Classic|url=http://www.wcpo.com/news/insider/former-saveur-editor-keith-pandolfi-returns-to-food-wine-classic|publisher=WCPO.com|accessdate=11 September 2015}}</ref><ref name=campbell150907>{{cite web|last1=Campbell|first1=Polly|title=Festival is food and wine shoutout for Cincinnati|url=http://www.cincinnati.com/story/entertainment/dining/2015/09/07/festival-natl-foodies/71725324/|publisher=Cincinnati Enquirer|accessdate=11 September 2015}}</ref><ref name=campbell140912>{{cite web|last1=Campbell|first1=Polly|title=Cincinnati Food & Wine Classic: The must-see stars|url=http://www.cincinnati.com/story/entertainment/dining/2014/09/12/food-wine-stars-align-classic/15516553/|publisher=Cincinnati Enquirer|accessdate=11 September 2015}}</ref><ref name=cfwctalent>{{cite web|title=Who’s Coming|url=http://cincinnatifoodandwineclassic.com/talent/|website=Cincinnati Food + Wine Classic|accessdate=11 September 2015}}</ref> The 2014 inaugural event "exceeded expectations,"<ref name=campbell140917>{{cite web|last1=Campbell|first1=Polly|title=Cincinnati Food & Wine Classic crowds exceed expectations|url=http://www.cincinnati.com/story/campbellsscoop/2014/09/17/cincinnati-food-and-wine-classic-courtney-tsitouris-donna-covrett/15677119/|accessdate=11 September 2015}}</ref> drawing more than 2,000 attendees<ref name=campbell151009 /> who had paid ticket prices of $100 - $400<ref name=citybeat150909>{{cite web|last1=Ross|first1=Ilene|title=Cincinnati Food + Wine Classic 2.0|url=http://citybeat.com/cincinnati/article-33571-cincinnati_food_+_wine_classic_20.html|publisher=City Beat|accessdate=11 September 2015}}</ref> for "festival-style"<ref name=citybeat150909 /> (or all-inclusive) access to food and beverage offerings, and resulting in the event running out of food.<ref name=campbell140917 /><ref name=wcpo140924>{{cite web|last1=Yek|first1=Grace|title=What worked and what didn't at inaugural Cincinnati Food + Wine Classic: Organizers say more food will be there next year|url=http://www.wcpo.com/lifestyle/food/what-worked-and-what-didnt-at-inaugural-cincinnati-food-wine-classic|publisher=WCPO|accessdate=11 September 2015}}</ref>

==Reception==
The event "attracted an overflow crowd"<ref name=campbell151009>{{cite web|last1=Campbell|first1=Polly|title=Experience the food, wine at Classic|url=http://www.cincinnati.com/story/entertainment/dining/2015/09/09/experience-food-wine-classic/71961684/|publisher=Cincinnati Enquirer|accessdate=11 September 2015}}</ref> in 2014. Olivia Barrow, writing in ''Independent Restaurateur'' in July 2015, called it "(a culinary event) many chefs and restaurateurs are looking forward to, even though it's relatively new."<ref name=indrest1507 /> Food Network's Nathan Lyon called it "an exceptional showcase of the region’s culinary talent in addition to the all-star line up of chefs from around the country."<ref>{{cite web|title=Mission|url=http://cincinnatifoodandwineclassic.com/about/mission/|website=Cincinnati Food + Wine Classic|accessdate=11 September 2015}}</ref>

==References==
{{reflist|colwidth=30em}}



[[Category:Food and drink festivals in the United States]]