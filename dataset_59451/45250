{{Use British English|date=April 2014}}
{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name=Guy Rawstron Branch
|image=Guy Rawstron Branch.jpg
|caption=
|birth_date=1913 <!-- {{Birth date|df=yes|YYYY|MM|DD}} -->
|death_date=11 August 1940 (aged 26) <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} -->
|placeofburial_label=
|placeofburial=
|birth_place=[[London]], [[England]]
|death_place=[[English Channel]]
|placeofburial_coordinates= <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname=
|birth_name=Guy Rawstron Branch
|allegiance={{flag|United Kingdom}}
|branch={{air force|United Kingdom}}
|serviceyears=1937-1940
|rank=[[Flying Officer]]
|servicenumber=90137
|unit=[[No. 601 Squadron RAF]]<br>[[No. 145 Squadron RAF]]
|commands=
|battles=[[World War II]]
*[[Battle of Britain]]
|battles_label=
|awards=[[Empire Gallantry Medal]]
|relations=Charles Churchill Branch (Father)<br>Mary Madelaine Bernadette Branch (née Rawstron) (Mother)
|laterwork=
}}
Flying Officer '''Guy Rawstron Branch''' [[Empire Gallantry Medal|EGM]] (1913–1940) was a Royal Air Force fighter pilot, one of [[The Few]]. He was killed in action on 11 August 1940. Since his death occurred before the introduction of the [[George Cross]] his next-of-kin were not given the opportunity of exchanging the insignia of the [[Empire Gallantry Medal]] for the new award.

==Early life==
Branch was born in 1913 in [[London]], the son of Charles Churchill Branch and Mary Madelaine Bernadette Branch (née Rawstron). He was educated at [[Eton College|Eton]] and [[Balliol College, Oxford]].<ref name="tribute"/> On 7 May 1937 he was commissioned as Pilot Officer in the [[Royal Auxiliary Air Force]]. On 25 March 1939 in [[Lewes]] he married Lady Prudence Mary Pelham, daughter of the 6th [[Earl of Chichester]].

==Accident and award==
On 8 January 1938 Branch was a student pilot was flying as a passenger in a [[Hawker Demon]] with Pilot Officer Crawley when it crashed and burst into flames at [[RAF Upavon]].<ref name="award"/> Branch escaped but then returned to the aircraft to free the trapped pilot and pull him clear. For his actions he was awarded the [[Empire Gallantry Medal]] on 25 March 1938<ref name="award"/>
:<ref name="obituary"/>

{{Quotation|On 9th January, 1938 an aircraft in which Pilot Officer Branch was a passenger crashed at Upavon, Wiltshire, and immediately burst into flames. Having extricated himself from the burning aircraft this officer found that the pilot was trapped in the cockpit by his legs. Despite the danger of the petrol tank exploding, Pilot Officer Branch returned to the blazing wreckage and, whilst actually standing on burning debris, succeeded in extracting the pilot. There is little doubt that this prompt and gallant act saved the pilot's life. The aircraft was completely destroyed by fire.|London Gazette<ref name="egm"/>}}

==Battle of Britain==
During the [[Battle of Britain]] Branch by then a Flying Officer was a [[Hawker Hurricane]] pilot with [[No. 145 Squadron RAF|145 Squadron]]. On 8 August 1940 he was credited with destroying two [[Junkers Ju 87]]s. A few days later on 11 August 1940 while flying Hurricane [[United Kingdom military aircraft serials|serial number]] ''P9251'' he was killed in action over the English channel.
<ref name="obituary"/>

Branch was buried in the churchyard at [[Quiberville]], [[France]].<ref name="cwgc"/> His name recorded on the [[Battle of Britain Monument in London]]<ref name="boblondon" /> and at the [[Battle of Britain Memorial, Capel-le-Ferne]].

==References==
{{reflist|refs=
<ref name="egm">{{LondonGazette |issue=34496 |date=25 March 1938 |startpage=2003 |supp= |accessdate=25 March 2010}}</ref>

<ref name="tribute">
{{Cite newspaper The Times
|articlename=Personal Tribute - Flying Officer G.R. Branch E.G.M.
|author=
|section=Obituaries
|day_of_week=Wednesday
|date=5 March 1941
|page_number=7
|issue=48866
|column=C
}}</ref>
<ref name="award">
{{Cite newspaper The Times
|articlename=Air Pilot's Life Saved Medal Awarded To A.A.F. Officer
|author=
|section=News
|day_of_week=Saturday
|date=26 March 1938
|page_number=17
|issue=47953
|column=B
}}</ref>
<ref name="obituary">
{{Cite newspaper The Times
|articlename=Fallen Officers - Royal Air Force
|author=
|section=Obituaries
|day_of_week=Tuesday
|date=4 February 1941
|page_number=9
|issue=48841
|column=B
}}</ref>
<ref name="cwgc">
{{cite web | title= Casualty Details - BRANCH, GUY RAWSTRON | url=http://www.cwgc.org/search/casualty_details.aspx?casualty=2689061| date=| publisher=[[Commonwealth War Graves Commission]]| accessdate=25 March 2010}}</ref>
<ref name="boblondon">
{{cite web | title= List of British Pilots who took part in the Battle of Britain | url=http://www.bbm.org.uk/pilots-b.htm| year=2007 | publisher=[[Battle of Britain London Monument]]| accessdate=25 March 2010}}</ref>

}}

{{DEFAULTSORT:Branch, Guy Rawstron}}
[[Category:1913 births]]
[[Category:1940 deaths]]
[[Category:Alumni of Balliol College, Oxford]]
[[Category:The Few]]
[[Category:Royal Air Force officers]]
[[Category:People educated at Eton College]]
[[Category:Royal Air Force pilots of World War II]]
[[Category:English aviators]]
[[Category:British military personnel killed in World War II]]
[[Category:Recipients of the Empire Gallantry Medal]]