{{Infobox military person
| name          =Harold Albert Kullberg
| image         =Harold Albert Kullberg.jpg
| image_size    = 250
| caption       = Harold Albert Kullberg, 1918
| birth_date    = {{Birth date|1896|9|10|df=y}}
| death_date    = {{Death date and age|1924|8|5|1896|9|10|df=y}} 
| placeofburial_label = 
| placeofburial = 
| birth_place   =[[Somerville, Massachusetts]], USA
| death_place   =Vicinity of [[Hudson, Ohio]], USA
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = {{flag|United States|23px}}
| branch        = [[Royal Air Force]] (United Kingdom)
| serviceyears  =1917–1918
| rank          =Captain
| unit          = Royal Air Force
* [[No. 1 Squadron RAF|No. 1 Squadron RFC]]
| commands      =
| battles       = [[File:World War I Victory Medal ribbon.svg|50px]]&nbsp;[[World War I]]
| awards        =[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
| relations     =
| laterwork     = President of the Akron Aeronautical Association 
}}
Captain '''Harold Albert Kullberg''' (10 September 1896&nbsp;– 5 August 1924) was a World War I [[flying ace]] credited with 19 aerial victories. Though he scored his victories with the Royal Air Force, Kullberg was an American citizen. He was rejected for training as an American pilot because he was too short.<ref name=AA>{{cite book |editor=[[Harry Dempsey]]|title=American Aces of World War 1 |publisher=[[Osprey Publishing]] |ISBN =1-84176-375-6 |year=2001|page= 36 }}</ref> He then joined the [[Royal Flying Corps]] in Canada on 7 August 1917.<ref name=aerodrome>{{cite web |url=http://www.theaerodrome.com/aces/usa/kullberg.php |title=Harold Albert Kullberg |publisher=theaerodrome.com |accessdate=20 December 2009}}</ref>

==Biography==
He was born on 10 September 1896 in [[Somerville, Massachusetts]].<ref name=raf/>

He joined No. 1 Squadron RFC in May 1918. He was an immediate success flying the [[RAF SE.5a]], scoring pairs of victories on 27 and 28 May, 1 and 9 June. His next victory, his ninth, was over an observation balloon. He continued scoring apace through June and July. August and September brought his final five victories, all over German [[Fokker D.VII]]s. The wingmen of his final victory on 16 September 1918 pursued him and inflicted three leg wounds on him. Kullberg sat out the rest of the war.<ref name=aerodrome/> It took six months for Kullberg to heal. He served until his release from service in July 1919.<ref name=AA/><ref name=raf>{{cite web |url=http://www.rafmuseum.org.uk/research/online-exhibitions/americans-in-the-royal-air-force/americans-in-the-british-flying-services-1914-1945/captain-howard-kullberg.aspx |title=Captain Harold Kullberg |quote=Born in 1896 in Massachusetts, Howard Kullberg joined the Royal Flying Corps in Toronto in August 1917, after being told that he was too short for the US Air Services. He trained in Canada and Texas, and was sent to the UK in January, 1918. ... |publisher=[[Royal Air Force Museum London]]}}</ref>

Kullberg became involved in civil aviation.<ref name=aerodrome/> He even made the nation's first arrest for violation of air traffic rules. On 3 November 1923, Kullberg chased down someone who was stunt flying over an urban area, landed with them, and arrested them.<ref>http://www.theaerodrome.com/forum/newspaper-articles/31538-first-arrest-violator-air-traffic-rules.html Retrieved 20 December 2009.</ref>

He became president of the [[Akron Aeronautical Association]].<ref>http://www.theaerodrome.com/forum/newspaper-articles/32781-harold-kullberg-student-flier-killed-nose-dive.html Retrieved 20 December 2009.</ref>

On 5 August 1924, he died in an [[air crash]] while instructing a student pilot.<ref name=aerodrome/>

==Honors and awards==
'''Distinguished Flying Cross (DFC)'''

"This officer has destroyed six enemy aeroplanes and has taken part in seven engagements when others have been destroyed by members of his patrol. A bold and keen officer who possesses a fine fighting spirit." DFC citation, London Gazette, November 2, 1918.<ref>http://www.wwiaviation.com/aces/ace_Kullberg.html Retrieved 20 December 2009.</ref>

==See also==
{{Portal|World War I|Biography}}
* [[List of World War I flying aces from the United States]]

==References==
{{Reflist}}
{{Refbegin}}
{{Refend}}

==External links==
*{{Find a Grave|80711914}}

{{DEFAULTSORT:Kullberg, Harold Albert}}
[[Category:1896 births]]
[[Category:1924 deaths]]
[[Category:American World War I flying aces]]
[[Category:Aviators from Massachusetts]]
[[Category:Aviators killed in aviation accidents or incidents in the United States]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]