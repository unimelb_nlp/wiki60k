{{Use dmy dates|date=May 2011}}
{{Use Australian English|date=May 2011}}
{{Infobox civilian attack
| title      = 2011 Hectorville siege
| image      = 
| caption    = 
| location   = [[Hectorville, South Australia|Hectorville]], [[South Australia]], [[Australia]]
| target     = 
| date       = 29 April 2011
| time       = c. 2:30 a.m.-10:00 a.m. [[UTC+9:30]]
| timezone   = 
| type     [[hostage taking]], [[siege]]
| fatalities = 3<ref>{{cite web|url=http://www.thejakartapost.com/news/2011/04/29/3-killed-shooting-australia.html-0 |title=3 killed in shooting in Australia |date=29 April 2011 |publisher=Jakarta Post |accessdate=29 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110430190506/http://www.thejakartapost.com:80/news/2011/04/29/3-killed-shooting-australia.html-0 |archivedate=30 April 2011 |df=dmy }}</ref>
| injuries   = 3 (including 2 police officers)
| susperp    = Donato Anthony Corbo
| weapons    = [[Shotgun]]
}}

The '''2011 Hectorville siege''' took place between the hours of 2:30 a.m. and 10:30 a.m. on Friday, 29 April 2011, at the small suburb of [[Hectorville, South Australia|Hectorville]], east of [[Adelaide, Australia|Adelaide]] in the state of [[South Australia]], [[Australia]]. It began after a 39-year-old resident of the suburb, later identified as Donato Anthony Corbo, entered his neighbours' property and shot four people, killing three and severely wounding one. An eight-hour stand-off with police followed, during which time he shot and wounded two officers. The stand-off culminated in his arrest by members of the [[Special Tasks and Rescue]] unit of [[South Australia Police]].<ref>{{cite web|url=http://www.sbs.com.au/news/article/1528396/Three-people-killed-in-armed-siege|title=Adelaide gunman arrested after siege|date=29 April 2011|publisher=SBS World News|accessdate=29 April 2011}}</ref>

==Events==
The incident started at approximately 2:30 a.m. when Corbo entered his neighbours' property and shot dead the 64-year-old man, then his 65-year-old wife and their 41-year-old son-in-law.<ref>{{cite web|url=http://www.heraldsun.com.au/news/national/three-dead-another-injured-in-armed-siege-hectorville-primary-school/story-e6frf7l6-1226046689938|title=Armed siege: Suspected gunman arrested after three people killed and teen seriously injured |last=Malik|first=Sarah|author2=Liza Kappelle|date=29 April 2011|publisher=Herald Sun|accessdate=29 April 2011}}</ref> A female who was also in the house at the time rang [[South Australia Police|police]] and fled with her 14-year-old son and his 11-year-old friend, but the son was seriously wounded when Corbo shot him as he tried to flee the house.<ref>{{cite web|url=http://www.abc.net.au/news/stories/2011/04/29/3203737.htm?section=justin|title=Three dead, three hurt in Adelaide siege|date=29 April 2011|publisher=ABC News|accessdate=29 April 2011}}</ref>

A [[South Australia Police]] patrol were the first officers on the scene. It is alleged that when the officers approached the house, Corbo opened fire with a [[shotgun]]. One officer was seriously wounded when he was shot in the face and another officer was wounded in the knee, but was able to return fire and drag himself and his wounded colleague to safety. Both officers were taken to [[Royal Adelaide Hospital]] for treatment and survived.<ref>{{cite web|url=http://www.theaustralian.com.au/news/nation/sa-policeman-shot-in-adelaide-siege/story-e6frg6nf-1226046734958|title=SA policeman shot, three dead in siege |author=AAP|date=29 April 2011|publisher=The Australian|accessdate=29 April 2011}}</ref> Corbo then fled next door back to his property, where he engaged in a tense siege with heavily armed members of the [[Special Tasks and Rescue|STAR force]].<ref>{{cite web|url=http://www.themercury.com.au/article/2011/04/29/226125_todays-news.html|title=Three killed, cop shot in siege|date=29 April 2011|publisher=The Mercuary|accessdate=29 April 2011}}</ref> After an eight-hour-long siege, police finally entered the property and arrested Corbo without further casualties.<ref>{{cite web|url=http://news.smh.com.au/breaking-news-national/three-die-2-wounded-in-adelaide-shooting-20110429-1e08g.html|title=Three die, 2 wounded in Adelaide shooting |date=29 April 2011|publisher=Sydney Morning Herald|accessdate=29 April 2011}}</ref>

===Victims===
The victims were [[South African]] immigrants who arrived in Australia three years before the shooting from [[Pretoria]] in the hope of a better life for their son as they felt that South Africa was a violent place to bring a child up. The two deceased victims were on holiday visiting the family and were due to return home to South Africa two days after the shooting took place.<ref>{{cite web|url=http://www.abc.net.au/news/stories/2011/04/30/3204223.htm|title=Adelaide siege survivor 'a very brave lady'|date=1 May 2011|publisher=ABC News|accessdate=30 April 2011}}</ref> The family were deeply religious, members of the local [[Seventh-day Adventist Church]].<ref>{{cite web|url=http://www.adelaidenow.com.au/hectorville-shooting-family-fled-life-of-violence-in-south-africa/story-e6frea6u-1226047652171|title=Hectorville shooting family fled life of violence in South Africa |last=Watkins|first=Emily|date=23 March 2011|publisher=Adelaide Now|accessdate=30 April 2011}}</ref>

==Suspect==
The suspect arrested was announced as being 39-year-old Donato Anthony Corbo, and had had previous dealings with the police.<ref>{{cite web|url=http://www.theaustralian.com.au/news/nation/adelaide-gunman-turns-on-neighbours-killing-three-and-wounding-two/story-e6frg6nf-1226047249457|title=Adelaide gunman turns on neighbours, killing three and wounding two |last=Edwards|first=Verity|author2=Rebecca Puddy|date=30 April 2011|publisher=The Australian|accessdate=30 April 2011}}</ref> It is suggested that the reason for the killings may have sparked from an earlier row between the two families over Corbo's pet dog, a Staffordshire Terrier, which had recently been poisoned. It is also speculated that Corbo was suffering from [[mental health]] issues, stemming from a relationship breakup in December 2010.<ref>{{cite web|url=http://www.smh.com.au/national/dog-row-may-have-triggered-shootings-20110430-1e1zf.html|title=Dog row may have triggered shootings |last=Duff|first=Eamonn |author2=Heath Aston|date=1 May 2011|publisher=Sydney Morning Herald|accessdate=30 April 2011}}</ref> It is alleged that the weapon used during the shootings was a [[shotgun]], a Class-A category firearm.<ref>{{cite news|url=http://www.adelaidenow.com.au/doomed-familys-last-call-for-help/story-e6frea6u-1226047289612|title=Doomed family's last call for help|last=McGregor|first=Ken|date=1 May 2011|publisher=Adelaide Now|accessdate=30 April 2011}}</ref> It is not known if Corbo has a legitimate licence for the weapon. However, police would later remove three further firearms from his property.<ref>{{cite web|url=http://www.news.com.au/breaking-news/police-hunt-for-clues-after-adelaide-triple-murder/story-e6frfku0-1226047581717|title=Police hunt for clues after Adelaide triple murder |date=30 April 2011|publisher=News.com.au|accessdate=30 April 2011}}</ref>

==Aftermath==
{{Update|section|date=January 2017}}
Corbo was later charged with three counts of [[murder]], two counts of [[attempted murder]], and a string of other offences,<ref>{{cite web|url=http://www.smh.com.au/national/siege-neighbour-charged-with-triple-murder-20110429-1e022.html|title=Siege neighbour charged with triple murder |date=29 April 2011|publisher=Sydney Morning Herald|accessdate=29 April 2011}}</ref> and if convicted, he would face an automatic penalty of three consecutive sentences of life imprisonment.<ref>{{cite web|url=http://www.boston.com/news/world/asia/articles/2011/04/29/gunman_kills_3_wounds_officer_in_australia_siege/|title=Gunman kills 3, wounds officer in Australia siege|date=29 April 2011|publisher=Boston.com|accessdate=29 April 2011}}</ref> He was refused [[bail]] and scheduled to appear in court on 2 May.<ref>{{cite web|url=http://www.abc.net.au/pm/content/2011/s3203974.htm|title=Family members killed in Adelaide shooting|last=Colvin|first=Mark|author2=Jason Om|date=29 April 2011|publisher=ABC Radio, PM Program|accessdate=29 April 2011}}</ref>

A day after the shootings occurred, the police officer who was seriously wounded after being shot in the face was confirmed to be in a serious but stable condition, under an [[induced coma]] in [[Royal Adelaide Hospital]]. The 14-year-old boy who also suffered [[gunshot wounds]] was also stated to be in a similar condition.

Corbo appeared in [[Magistrates' Court of South Australia|Adelaide Magistrates Court]] on 2 May, charged with three counts of murder and two counts of attempted murder. He pleaded [[not guilty (plea)|not guilty]] to the charges.<ref>{{cite web|url=http://www.theaustralian.com.au/news/nation/triple-shooting-murder-accused-to-plead-not-guilty/story-e6frg6nf-1226048716410|title=Triple-shooting murder accused to plead not guilty |last=Puddy|first=Rebecca|date=3 May 2011|publisher=The Australian|accessdate=3 May 2011}}</ref> He was scheduled to reappear in court on 26 July.<ref>{{cite web|url=http://www.smh.com.au/national/alleged-gunman-faces-court-in-adelaide-20110502-1e4m3.html|title=Alleged gunman faces court in Adelaide |date=2 May 2011|publisher=Sydney Morning Herald|accessdate=3 May 2011}}</ref>

On 17 May 2012, Justice Michael David found Corbo not guilty due to mental incompetence of the murders of Luc Mombers, 41, and his parents-in-law Kobus, 64, and Annetjie Snyman, 65; the attempted murders of Mr Mombers' 14-year-old son Marcel and a police officer; and threatening a second police officer with a firearm. Corbo was automatically sentenced to three consecutive sentences of detention in hospital for life.<ref>[http://www.news.com.au/breaking-news/national/hectorville-killer-detained-for-life-in-sa/story-e6frfku9-1226358894519#ixzz1v5psJn48 Hectorville gunman Donato Anthony Corbo will spend the rest of his life in mental health detention]</ref> The matter was remanded until 21 June, when Justice David was to decide whether Corbo should ever be released under supervision. On 16 May, 2013, the final decision was made that Corbo would spend the rest of his life in mental health detention.<ref>[http://murderpedia.org/male.C/c/corbo-donato-anthony.htm Murderpedia]</ref>

==References==
{{reflist|40em}}

==See also==
* [[Timeline of major crimes in Australia]]
* [[Crime in Australia]]
* [[Crime in South Australia]]

{{Australian_crime}}

{{DEFAULTSORT:Hectorville Siege}}
[[Category:2011 crimes in Australia]]
[[Category:2010s in Adelaide]]
[[Category:Crime in Adelaide]]
[[Category:History of Australia since 1945]]
[[Category:Hostage taking in Australia]]
[[Category:Murder in 2011]]
[[Category:Murder in South Australia]]