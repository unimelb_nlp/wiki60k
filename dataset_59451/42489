<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Budig
 | image=Budig glider at Rhön 1921.png
 | caption= Glider at the 1921 Rhön competition
}}{{Infobox Aircraft Type
 | type=[[glider (sailplane)|glider]]/[[powered glider]]
 | national origin=[[Germany]]
 | manufacturer=
 | designer=[[Friedrich Wilhelm Budig]]
 | first flight=1921
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The unusual [[Germany|German]] '''Budig glider''' participated in the second (1921) [[Rhön]] competition, though without distinction.<ref name=Flight2/>  It was later fitted with a low power engine, making it probably the first [[powered glider]].<ref name=Flight1/>

[[File:Budig in powered flight.png|thumb|The 1923 motor glider in flight]]

==Design and development==
The Budig [[glider (sailplane)|glider]] was a single seat [[biplane]] or [[sesquiplane]] of unequal [[Wingspan|span]] and [[chord (aircraft)|chord]] with three forward surfaces supported on a pair of box section booms. The upper foreplane was claimed to provide automatic stability and carried an [[elevator (aircraft)|elevator]] behind it, though Flight remarked that "It is difficult, from the illustration, to make out exactly what this arrangement is  supposed to do."<ref name=Flight2/> The pilot sat at the upper wing [[trailing edge]], completely exposed at the front of a short central [[fuselage]] which supported a rectangular [[fin]].<ref name=Flight1/>

[[File:Budig from above.png|thumb|The motor glider]]
[[File:Budig detail.png|thumb|Foreplanes, nose and designer of the motor glider]]

By 1923 a small, {{convert|4|hp|kW|abbr=on|disp=flip|0}} [[BMW]] [[horizontally opposed engine|horizontally opposed]] [[motorcycle engine]] had been added, mounted in [[pusher configuration]] behind the pilot who now sat at the [[leading edge]] of the upper wing behind a more substantial nose. It drove a very small diameter [[propeller (aircraft)|propeller]].  To accommodate the powerplant the central rear fuselage was replaced by rearward extensions of the booms, assisted by another, finer pair from the upper wings, which carried twin rectangular fins with a second elevator surface between them.<ref name=Flight2/>

[[File:Budig engine.png|thumb|Engine and propeller]]

The little engine probably did not have enough power to get the Budig airborne but was used to maintain altitude or decrease the sink rate; launches were made with the usual [[bungee cord]] catapult. Nonetheless, Flight noted it as " probably the first glider with an auxiliary engine to fly".<ref name=Flight2/>

==Specifications ==
{{Aircraft specs
|ref=Flight, 4 January 1923<ref name=Flight2/> 
|prime units?=met
<!--
        General characteristics
-->
|genhide=Yes

|crew=One
|capacity=
|length ft=
|length in=
|length note=
|span m=
|span ft=21
|span in=4
|span note=1921 glider
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[BMW]]
|eng1 type=[[horizontally opposed]] [[motorcycle engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=4
|eng1 note=
|more power=

|prop blade number=2
|prop name=
|prop dia m=1
|prop dia note=or less. Pusher.
<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=

|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=Flight1>{{cite magazine |last= |first= |authorlink= |coauthors= |date=8 September 1921 |title=Soaring Flight in Germany|magazine= [[Flight International|Flight]]|volume=XIII |issue=36 |pages=601, 603  |url= http://www.flightglobal.com/pdfarchive/view/1921/1921%20-%200601.html  }}</ref>

<ref name=Flight2>{{cite magazine |last= |first= |authorlink= |coauthors= |date=4 January 1923 |title= Gliding, Soaring and Air-sailing|magazine= [[Flight International|Flight]]|volume=XV |issue=1 |pages=9–11  |url= http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200009.html  }}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:German sailplanes 1920–1929]]
[[Category:Motor gliders]]