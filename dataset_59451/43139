{{For|the Canadian Vickers FV-1|F6F Hellcat}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=XFV Salmon
 |image=Lockheed XFV-1 on ground bw.jpg
 |caption=<!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
 |type=Experimental [[VTOL]] [[fighter aircraft]]
 |manufacturer=[[Lockheed Corporation|Lockheed]]
 |designer=
 |first flight=16 June 1954
 |status=
 |primary user=[[United States Navy]] (intended)
 |produced=1954
 |number built=1 flying prototype plus 1 incomplete airframe 
 |unit cost=
 |variants with their own articles=
}}
|}
The [[United States|American]] '''Lockheed XFV''' (sometimes referred to as the '''Salmon''')<ref>Taylor 1999, p. 101.</ref> {{#tag:ref|The name was very obviously derived from the Lockheed Chief Test Pilot [[Herman "Fish" Salmon]]'s name. Lockheed proposed naming the aircraft "Rising Star", but this was rejected by the Navy.|group=N}} was an [[Experimental aircraft|experimental]] [[tailsitter]] [[prototype]] aircraft built by [[Lockheed Corporation|Lockheed]] in the early 1950s to demonstrate the operation of a [[VTOL|vertical takeoff and landing]] fighter for protecting convoys.

==Design and development==
The Lockheed XFV originated as a result of a proposal issued by the U.S. Navy in 1948 for an aircraft capable of vertical takeoff and landing (VTOL) aboard platforms mounted on the afterdecks of conventional ships. Both [[Convair]] and Lockheed competed for the contract but in 1950, the requirement was revised, with a call for a research aircraft capable of eventually evolving into a VTOL ship-based convoy escort fighter. On 19 April 1951, two prototypes were ordered from Lockheed under the designation '''XFO-1''' (company designation was Model 081-40-01). Soon after the contract was awarded, the project designation changed to XFV-1 when the [[1922 United States Navy aircraft designation system|Navy's code]] for Lockheed was changed from O to V.<ref>Allen 2007. p. 14.</ref>

The XFV was powered by a {{convert|5332|hp|abbr=on}} [[Allison YT40]]-A-6 [[turboprop engine]] driving three-bladed [[contra-rotating propellers]]. The tail surfaces were a reflected cruciform [[v-tail]] (forming an '''x''') that extended above and below the fuselage. The aircraft had  an ungainly appearance on the ground with a makeshift, fixed landing gear attached.<ref>Winchester 2005, p. 135.</ref> Lockheed employees derisively nicknamed the aircraft the "[[pogo stick]]" (a direct reference to the rival [[Convair XFY]]'s name).<ref>Winchester 2005, p. 134.</ref>

==Testing and evaluation==
[[File:Lockheed XFV-1 in flight with T-33 c1954.jpg|thumb|The first XFV-1 during a test flight at Edwards AFB.]]
To begin flight testing, a temporary non-retractable undercarriage with long braced V-legs was attached to the fuselage, and fixed tail wheels attached to the lower pair of fins. In this form, the aircraft was trucked to [[Edwards AFB]] in November 1953 for ground testing and taxiing trials. During one of these tests, at a time when the aft section of the large spinner had not yet been fitted, Lockheed Chief Test Pilot [[Herman "Fish" Salmon]] managed to taxi the aircraft past the liftoff speed, and the aircraft made a brief hop on 22 December 1953. The official first flight took place on 16 June 1954.

Full VTOL testing at Edwards AFB was delayed pending the availability of the 7,100 shp Allison [[Allison_T40#Allison_T54|YT54]], which never materialized. After the brief unintentional hop, the aircraft made a total of 32 flights. All further XFV-1 flights did not involve any vertical takeoffs or landings. The XFV-1 was able to make a few transitions in flight from the conventional to the vertical flight mode and back, and had briefly held in hover at altitude. Performance remained limited by the confines of the flight test regime. With the realization that the XFV's top speeds would be eclipsed by contemporary fighters and that only highly experienced pilots could fly the aircraft, the project was cancelled in June 1955.<ref>Allen 2007, p. 20.</ref>

Salmon taxied the XFV-1 on its temporary gear "from a standing start to 175 mph, and then brought it back down to a dead stop without any use of the brakes, all within a distance of one mile."<ref>''American Aviation'' 27 Sept 1954 p34</ref>

==Variants (proposed)==
*'''XFO-1/XFV-1''': (Model 081-40-01) Two prototypes built, one flown.
*'''FV-2''': Proposed production version (Model 181-43-02) was to have been powered by the T54-A-16 turboprop, incorporating a bullet-proof windshield, armor and radar in the fixed forward part of the nose spinner. The proposed armament was four 20&nbsp;mm cannon fitted in the two wingtip pods. Alternatively, 48 2{{fraction|3|4}}-inch folding-fin rockets could be fitted.

==Aircraft on display==
[[File:Prototype XFV-1 Aircraft at Sun-n-Fun Nov 2012.jpg|thumb|Vertical display of the XFV-1 Prototype at Florida Air Museum at Sun-n-Fun]]

The single flying prototype {{#tag:ref|The first prototype located at the Sun 'n Fun Campus Museum is BuNo 138657 but is currently marked as "658."|group=N}} ended up as an exhibit at the [[Sun 'n Fun]] Campus Museum at [[Lakeland Linder Regional Airport]] in [[Lakeland, Florida]]. This example was refurbished at the museum's Buehler Restoration Center and is currently on outdoor display.  The aircraft was assigned USN/USMC Bureau Number (BuNo) 138657, but was marked as 658 following restoration.<ref>[http://www.sun-n-fun.org/Museum/BuehlerCenter.aspx "Buehler Restoration Center."] ''sun-n-fun.org.'' Retrieved: 20 September 2010.</ref> The second prototype, which was never completed, is on display at [[Los Alamitos Army Airfield]] in [[California]].

==Specifications==
[[File:Lockheed XFV-1.JPG|thumb|right|The XFV-1 prototype located at Sun 'n Fun Museum, Lakeland, Florida]]

{{aircraft specifications|
|jet or prop?=prop
|plane or copter?=plane
|include 'armament' field?=no
|include 'capacity' field?=yes
|switch order of units?=no
<!-- please include units. if something doesn't apply, leave it blank. -->
|crew=1
|capacity=
|length main=36 ft 10.25 in
|length alt=11.23 m
|span main=30 ft 22 in
|span alt=8.36 m
|height main=36 ft 10.25 in
|height alt=11.23 m
|area main=246 ft²
|area alt=22.85 m²
|empty weight main=11,599 lb
|empty weight alt=5,261 kg
|loaded weight main=16,221 lb
|loaded weight alt=7,358 kg
|useful load main=
|useful load alt=
|max takeoff weight main=16,221 lb
|max takeoff weight alt=7,358 kg
|engine (jet)=
|type of jet=
|number of jets=
|thrust main=
|thrust alt=
|engine (prop)=Allison XT40-A-14 turboprop,
|type of prop= 6 blade contra-rotating
|number of props=1
|power main=5,100 shp
|power alt=
|max speed main=580 mph
|max speed alt=930 km/h
|cruise speed main=410 mph
|cruise speed alt=660 km/h
|range main=unknown
|range alt=
|ceiling main=43,300 ft
|ceiling alt=13,100 m
|climb rate main=10,820 ft/min
|climb rate alt=3,300 m/min
|loading main=65.9 lb/ft²
|loading alt=322 kg/m²
|thrust/weight=
|power/mass main=
|power/mass alt=
|armament=4 × 20 mm (.79 in) cannons ''or'' 48 × 2.75 in (70 mm) rockets
}}

''Note: Performance estimates are based on XFV with YT40-A-14 engine.''

==See also==
{{aircontent|
|related=
|similar aircraft=
* [[Convair XFY]]
* [[Focke-Wulf Triebflügel]]
* [[Heinkel Lerche]]
* [[X-13 Vertijet|Ryan X-13 Vertijet]]
|lists=
* [[List of Lockheed aircraft]]
* [[List of fighter aircraft]]
* [[List of military aircraft of the United States]]
|see also=<!-- other relevant information -->
}}

==References==

===Notes===
{{reflist|group=N}}

===Citations===
{{reflist}}

===Bibliography===
{{refbegin}}
* Allen, Francis J. "Bolt upright: Convair's and Lockheed's VTOL fighters". ''Air Enthusiast'' (Key Publishing), Volume 127,  January/February 2007, pp.&nbsp;13–20. ISSN 0143-5450.
* Green, William and Gordon Swanborough. ''The Great Book of Fighters''. St. Paul, Minnesota: MBI Publishing, 2001. ISBN 0-7603-1194-3.
* Taylor, Michael. ''The World's Strangest Aircraft''. London: Grange Books plc, 1999. ISBN 1-85627-869-7.
* Winchester, Jim. "Lockheed XFV-1 Salmon." ''Concept Aircraft: Prototypes, X-Planes and Experimental Aircraft''. Kent, UK: Grange Books plc., 2005. ISBN 978-1-84013-809-2.
{{refend}}

==External links==
{{Commons category}}
* [https://books.google.com/books?id=oN0DAAAAMBAJ&pg=PA96&dq=1954+Popular+Mechanics+January&hl=en&sa=X&ei=TV0mT-7WMoGftwfi4dizCg&ved=0CEwQ6AEwBw#v=onepage&q=1954%20Popular%20Mechanics%20January&f=true "Heads Up Fighters."] ''Popular Mechanics'', May 1954, pp.&nbsp;96–97.

{{Lockheed aircraft}}
{{USN fighters}}

[[Category:Lockheed aircraft|F01V]]
[[Category:United States fighter aircraft 1950–1959]]
[[Category:United States experimental aircraft 1950–1959|FV, Lockheed]]
[[Category:Single-engined tractor aircraft]]
[[Category:Monoplanes]]
[[Category:Tailsitter aircraft]]
[[Category:Aircraft with contra-rotating propellers]]
[[Category:Turboprop aircraft]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Cruciform tail aircraft]]