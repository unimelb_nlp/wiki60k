{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Hendon
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 1,373
| pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref name=ABS2011>{{Census 2011 AUS |id =SSC40286 |name=Hendon (State Suburb) |accessdate=5 October 2016| quick=on}}</ref>
| est                 = 1921<ref name=Manning>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/h/h3.htm#hendon |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=29 January 2012}}</ref>
| postcode            = 5014<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/hendon |title=Hendon, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=29 January 2012}}</ref>
| area                = 
| timezone            = [[UTC9:30|ACST]]
| utc                 = +9:30
| timezone-dst        = [[UTC10:30|ACDT]]
| utc-dst             = +10:30
| dist1               = 9.8
| dir1                = NE
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = [[City of Charles Sturt]]<ref name=CharlesSturt>{{cite web|url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |title=City of Charles Sturt Wards and Council Members |author= |date= |work= |publisher=City of Charles Sturt |accessdate=29 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110805135335/http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |archivedate=5 August 2011 |df=dmy-all }}</ref>
| stategov            = [[Electoral district of Cheltenham|Cheltenham]] <small>''(2011)''</small><ref name=ECSA>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=29 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy-all }}</ref>
| fedgov              = [[Division of Port Adelaide|Port Adelaide]] <small>''(2011)''</small><ref name=AEC>{{cite web|url=http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Port+Adelaide&filterby=Electorate&divid=189 |title=Find my electorate: Port Adelaide |author= |date= |work= |publisher=Australian Electoral Commission |accessdate=29 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110727073801/http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Port+Adelaide&filterby=Electorate&divid=189 |archivedate=27 July 2011 |df=dmy-all }}</ref>
| near-n              = [[Queenstown, South Australia|Queenstown]]
| near-ne             = [[Queenstown, South Australia|Queenstown]]
| near-e              = [[Albert Park, South Australia|Albert Park]]
| near-se             = [[Albert Park, South Australia|Albert Park]]
| near-s              = [[Seaton, South Australia|Seaton]]
| near-sw             = [[Seaton, South Australia|Seaton]]
| near-w              = [[Royal Park, South Australia|Royal Park]]
| near-nw             = [[Royal Park, South Australia|Royal Park]]
}}

'''Hendon''' is a north-western [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Charles Sturt]].

==History==
[[File:Hendon aerodrome plaque.JPG|thumb|Hendon aerodrome plaque]]
Initially part of [[Albert Park, South Australia|Albert Park]] in the district of Woodville, the new suburb of Hendon was laid out in 1921 by Wilkinson, Sands and Wyles Ltd, on part of the land previously owned by the aviator, [[Harry Butler (aviator)|Harry Butler]], who established the adjacent aerodrome there in 1920. Consequently, several streets were named after aircraft.<ref name=Manning/>

The site of the "Hendon" aerodrome, also known as "Captain Butler's Aerodrome", was compulsorily acquired in July 1922 by the Civil Aviation branch of the Department of Defence, and used as the first "Adelaide Airport". By 1927 the site was becoming inadequate due to the increasing density of surrounding development and the erection of powerlines around its boundaries, so aviation operations were shifted to [[Parafield Airport|Parafield]].<ref name="Marsden">Marsden, Susan (1977): ''A history of Woodville.'' Corporation of the City of Woodville. Pp. 169-176. ISBN 0 9599828 4 1</ref> The Commonwealth had originally intended to fund the development of Parafield Aerodrome through the subdivision and sale of the land at Hendon, but with the end of the 1920s economic boom, these plans lapsed and the site remained as a cow pasture.{{sfn|Marsden|1977|p=173}}

Soon after the outbreak of [[World War II]] the still vacant former Hendon aerodrome was one of three sites in South Australia to be set up as munitions factories (the others were at nearby [[Woodville North, South Australia|Finsbury]], and [[Salisbury, South Australia|Salisbury]]). The Hendon factory, used to manufacture small arms ammunition, was connected to the [[Grange railway line]] by a [[Hendon railway line|spur]],{{sfn|Marsden|1977|p=213-216}} later removed with the corridor being reused for the easternmost section of [[West Lakes Boulevard]].

By 1947 the entire site of the former munitions factory had been acquired by [[Philips#Australia and New Zealand|Philips Electrical Industries]].<ref>[http://trove.nla.gov.au/ndp/del/article/30524445 Trove collection, National Library of Australia > Hendon's new role] ''The Advertiser'', 23 April 1947. Accessed 26 March 2014.</ref> This was a major accomplishment of the [[Thomas Playford IV#Industrialisation|Playford Government]], with the Hendon site becoming "the company's Australian headquarters, and as such, the country's largest producer of electronic components, and a major centre of technological skills and research".{{sfn|Marsden|1977|pp=230-232}} At its peak in the late 1950s the factory employed almost 3,500 skilled workers, mostly women, who were valued for their dexterity and patience.{{sfn|Marsden|1977|pp=230-232}} The company's activities were progressively reduced during the 1970s, but one notable feature of the period was the collaboration of the company with the Polish-Australian artist [[Joseph Stanislaus Ostoja-Kotkowski|Stan Ostoja-Kotkowski]], a pioneer of the use of laser, sound and image technology in art.<ref>[http://www.crookedmirror.com:16080/soundandlight/ Dr Ian Macdonald: Explorer in Sound and Light, a study of the works of Joseph Stanislaw Ostoja-Kotkowski] Accessed 26 March 2014.</ref> The Philips factory was finally sold in 1980 to the Emanuel Group of Companies, becoming the "Hendon Industrial Park".<ref>[http://www.sahistorians.org.au/175/documents/susan-marsden-a-history-of-woodville-1977-1987.shtml Marsden, Susan  (1987): A History of Woodville 1977-1987], unpublished typescript, 1987; facsimile created in 2007 and published by the Professional Historians Association (SA), 2011. Accessed 27 March 2014.</ref><ref>[http://www.charlessturt.sa.gov.au/webdata/resources/files/Local_history_factsheet_-_Hendon.pdf City of Charles Sturt > Local History Factsheet: A brief history of the suburb Hendon] Accessed 26 March 2014.</ref> Tenants at the site have included the [[South Australian Film Corporation]], which operated the Hendon Studios from 1981 until production moved to new studios at [[Glenside, South Australia|Glenside]] in 2011.

===Groundwater contamination===
In 2012 concerns emerged about the pollution of groundwater beneath Hendon and surrounding suburbs, and in May 2012 residents were advised not to use bore water for any purpose. Contaminants including [[perchloroethene]] (PCE), [[trichloroethene]] (TCE), [[dichloroethene]] (DCE) and metals in groundwater and soil vapour had been detected, and noted by a consultants' report from 1992, which attributed the pollution to a former munitions manufacturing site on Philips Crescent.<ref>[http://www.abc.net.au/news/2012-05-15/hendon-ground-water-contamination/4012188 Bore water worries for Hendon residents] [[ABC News (Australia)|ABC News]], Accessed 26 March 2014.</ref><ref>[http://www.abc.net.au/news/2013-05-16/tests-back-hendon-bore-water-worries/4693684?section=sa Tests back Hendon bore water worries] [[ABC News (Australia)|ABC News]], Accessed 26 March 2014.</ref> The Environment Protection Authority is now "working with site contamination consultants to better characterise the current nature and extent of the contamination, and to ascertain potential risks".<ref>[http://www.epa.sa.gov.au/xstd_files/Corporate/Medianews/media_17dec2013.pdf Environment Protection Authority > Hendon groundwater contamination update] Media release, 17 December 2013. Accessed 26 March 2014.</ref>

==Geography==
Hendon lies between [[Port Road, Adelaide|Old Port Road]] to the north and [[West Lakes Boulevard]] on the south. Its western boundary is formed by [[Tapleys Hill Road]].<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 49th|year=2011 |publisher=UBD |location= |isbn=978-0-7319-2652-7 |page= |pages= }}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 1,117 persons in Hendon on census night. Of these, 46.1% were male and 53.9% were female.<ref name=ABS2006>{{Census 2006 AUS |id =SSC41751 |name=Hendon (State Suburb) |accessdate=29 January 2012| quick=on}}</ref>

The majority of residents (65.9%) are of Australian birth, with other common census responses being [[Italy]] (3.0%)
[[Southeast Europe|South Eastern Europe]]<!-- This link is piped to reflect any possible variations between the scope of the WP article and the scope of the ABS definition --> (2.6%), [[England]] (2.5%), [[Poland]] (2.3%) and [[Bosnia and Herzegovina]] (2.3%).<ref name=ABS2006/>

The age distribution of Hendon residents is skewed higher than the greater Australian population. 75.2% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 24.8% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Politics==

===Local government===
Hendon is part of West Woodville Ward in the [[City of Charles Sturt]] [[Local government in Australia|local government area]], being represented in that council by Tolley Wasylenko and Angela Keneally.<ref name=CharlesSturt/>

===State and federal===
Hendon lies in the state [[Electoral districts of South Australia|electoral district]] of [[Electoral district of Cheltenham|Cheltenham]]<ref name=ECSA/> and the federal [[Divisions of the Australian House of Representatives|electoral division]] of [[Division of Port Adelaide|Port Adelaide]].<ref name=AEC/> The suburb is represented in the [[South Australian House of Assembly]] by [[Premier of South Australia|Premier]] [[Jay Weatherill]] <ref name=ECSA/> and [[Australian House of Representatives|federally]] by [[Mark Butler]].<ref name=AEC/>

==Community==

===Parks===
The suburb contains a number of parks including a reserve located between Gordon Street and Circuit Drive and another on DeHaviland Avenue.<ref name=UBD/>

===Schools===
Hendon Primary School is located in the adjoining suburb of [[Royal Park, South Australia|Royal Park]].

==Transportation==

===Roads===
Hendon is serviced by [[Port Road, Adelaide|Old Port Road]], connecting the suburb to [[Adelaide city centre]], and [[Tapleys Hill Road]], which forms its western boundary.<ref name=UBD/>

===Public transport===
Hendon is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date= |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=29 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy-all }}</ref>

==See also==
{{Commons category|Hendon, South Australia}}
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.charlessturt.sa.gov.au |title=City of Charles Sturt |author= |date= |work=Official website |publisher=City of Charles Sturt |accessdate=29 January 2012}}

{{coord|34.872|S|138.515|E|format=dms|type:city_region:AU-SA|display=title}}

{{City of Charles Sturt suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1921]]