{{Infobox settlement
| name                    = Hyde Park
| native_name             = 
| native_name_lang        = <!-- ISO 639-2 code e.g. "fr" for French. If more than one, use {{lang}} instead -->
| settlement_type         = Neighborhood
| image_skyline           = File:HydeParkSign1.jpg
| image_alt               = 
| image_caption           = Hyde Park sign
| etymology               = 
| nickname                = 
| coordinates             = {{coord|29|44|50|N|95|23|41|W|display=inline,title}}
| subdivision_type        = Country
| subdivision_name        = [[United States]]
| subdivision_type1       = State
| subdivision_name1       = [[Texas]]
| subdivision_type2       = City
| subdivision_name2       = [[Houston]]
| established_title       = Established
| established_date        = 1893
| website                 = <!-- {{URL|example.com}} -->
}}

'''Hyde Park''' is a historic community located in the [[Montrose, Houston|Montrose]] neighborhood of [[Houston]], [[Texas]]. Its southeast boundary is the intersection Montrose Boulevard and [[Westheimer Road|Westheimer]]. The neighborhood was established in the late 1800s on the summer farm of the second President of the [[Republic of Texas]], Mirabeau Lamar. In the 1970s, Hyde Park became a central part of the Gay Rights Movement in Houston. Like much of Montrose, the neighborhood is now experiencing significant [[gentrification]], and is home to an abundance of restaurants, including Mexican, Italian, Greek, American, Lebanese, coffee houses, and numerous bars.

==Geography==
Hyde Park is located within the western boundary of the historic [[Fourth Ward, Houston|Fourth Ward]], and bounded by West Gray to the north, Montrose Boulevard to the east, Westheimer to the south, and Commonwealth and Yupon to the west. The neighborhood is part of the Neartown/Montrose Super Neighborhood.<ref>[http://www.houstontx.gov/superneighborhoods/profiles/SN_24.htm "Super Neighborhood 24 - Neartown / Montrose"], ''[[City of Houston]]'', Retrieved on 7 December 2014.</ref> Other [[Neartown Houston|Neartown]] neighborhoods include Cherryhurst, Courtlandt Place, Montrose, Vermont Commons, Mandell Place and Winlow Place.

When Hyde Park was established, it was considered a distant suburb on the outskirts of the City of Houston. Located less than three miles from downtown and inside [[Interstate 610 (Texas)|Loop 610]], today it is considered part of central Houston.

==History==
[[File:MapHouston1913.jpg|thumb|Hyde Park, located at G2, shown on 1913 Map of Houston Wards]]
Hyde Park was established in 1893 and was developed on land that was owned in the 1840s by [[Mirabeau B. Lamar]], the second President of the Republic of Texas.<ref>Dewan, Shaila. [http://www.houstonpress.com/1997-08-14/news/shifting-foundation/ "Shifting Foundation"], ''[[Houston Press]]'', Houston, 14 August 1997. Retrieved on 7 December 2014.</ref> When the area was annexed into the City of Houston in the early 1900s, it was owned by the Hyde Park Improvement Company, and the company's secretary and treasurer, J.C. Hooper, directed the planning and improvement of the neighborhood. In addition to an abundance of oak trees, the area was also desirable because it was of the highest elevations in the city, sitting 12 feet above downtown, and had excellent drainage to [[Buffalo Bayou]] .

According to "The Key to the City of Houston"<ref>Love, Mrs. W.J. [https://archive.org/stream/keytocityofhoust00city#page/211/mode/1up/ "Suburbs of Houston"], ''The Key to the City of Houston'', Houston, December 1908, 210-211.</ref> Hyde Park was intended to be a high-end neighborhood, but deed restrictions regulated improvement prices to ensure that the area remained attainable to more than only the wealthiest citizens. Additionally, the restrictions set a minimum lot size, limited business development, and regulated the placement of barns and outhouses.

The longest continually operating water feature in Houston, the Dolphin Fountain, is located in Hyde Park. The fountain was installed in Lamar Park, a small neighborhood park, in 1946 and still runs today. Hyde Park residents are currently undergoing efforts to restore the fountain.<ref>Meeks, Flori. [http://www.chron.com/neighborhood/heights/news/article/Hyde-Park-group-dives-into-fountain-project-4802453.php "Hyde Park Group Dives into Fountain Project"], ''Houston Chronicle'', Houston, September 10, 2013. Retrieved on Feb 26, 2015.</ref>

The neighborhood also hosted the Hyde Park Art Crawl for many years until the mid-2000s. The art crawl showcased the works of artists living in Hyde Park at a variety of locations within the neighborhood, including studios, homes, and restaurants.<ref>[http://www.yourhoustonnews.com/west_university/news/neighborhood-and-its-artists-nothing-to-hyde/article_7d36d427-4ed7-5a71-adbe-3d323f4ec0ef.html "Neighborhood and its artists nothing to Hyde "], ''[[Examiner Newspaper Group|Examiner]]'', Houston, 1 December 204.</ref>

The Hyde Park community was also an integral part of the [[Westheimer Street Festival]], which ran along Westheimer Road, the southern boundary of Hyde Park, from 1971 to 2004.

==LGBT community==
Hyde Park has been an integral part of the [[Montrose, Houston#LGBT culture|Montrose LGBT]] community since the beginning of the LGBT movement in Houston. The [[Houston Gay Pride Parade|Houston Pride Parade]] walked along the neighborhood from 1979 until the parade's location change to downtown Houston in 2015.

The historical GLBT bar, [[Mary's (Houston)|Mary's]], stood in Hyde Park for nearly 40 years. ''[[OutSmart]]'' stated that the bar "anchored" Houston’s gay community in the Montrose area.<ref name=OutSmart>[http://www.outsmartmagazine.com/2011/12/mary’s-infamous-mural-iconic-art-for-an-iconic-bar/Mary’s Infamous Mural: Iconic Art for an Iconic Bar]</ref> Another popular bar in the LGBT community, [[Chances Bar|Chances]], was located in the neighborhood until it closed in 2010.

Hyde Park is also home to Grace Lutheran Church, which became one of the first major churches in Houston to openly accept LGBT members in 1995. It also hired an LGBT pastor in 2008.<ref name="chron.com">Karkabi, Barbara. [http://www.chron.com/life/houston-belief/article/Grace-Evangelical-Lutheran-welcomes-gay-pastor-1766376.php "Grace Evangelical Lutheran welcomes gay pastor"], ''Houston Chronicle'', Houston, August 29, 2008. Retrieved on Feb 26, 2015.</ref>

Early in her career, [[Annise Parker]], the first openly gay mayor of the City of Houston, resided in Hyde Park.

==Development==
Like most of the Montrose community, Hyde Park has also seen increased demolition of homes, businesses, and churches as developers demolish entire blocks in an effort to gentrify the area with large town home and mid-rise developments.<ref>Sarnoff, Nancy. [http://blog.chron.com/primeproperty/2014/05/upscale-residential-builder-developing-mod-midrise/ "Upscale residential builder developing Mod midrise"], ''[[Houston Chronicle]]'', Houston, 20 May 2014. Retrieved on 8 December 2014.</ref> Many older and larger trees are often removed for this development. Some Montrose community advocates feel the area is at risk of losing some of these distinguishing and cherished characteristics through wholesale demolition and rebuilding.<ref>Lane, Chris. [http://blogs.houstonpress.com/artattack/2014/08/the_changing_face_of_houston_-_the_montrose_then_and_now.php "The Changing Face of Houston - The Montrose Then and Now"], ''[[Houston Press]]'', Houston, 25 August 2014. Retrieved on 8 December 2014.</ref><ref>Mulvaney, Erin. [http://www.houstonchronicle.com/business/retail/article/Montrose-at-a-critical-juncture-for-retail-5904793.php#/0 "Montrose tries to keep its vibe"], ''[[Houston Press]]'', Houston, 19 November 2014. Retrieved on 8 December 2014.</ref>

===Building protections===
Beginning in the early 1990s, many subdivisions in Hyde Park opted to renew historical deed restrictions as allowed by the Texas Property Code in an effort to maintain the unique character of the community. Many residents voluntarily opted their properties into the restrictions, which regulate land use, including the subdivision of lots, minimum building setbacks, and tree protections.<ref>[http://www.cclerk.hctx.net/real_property/Real_Estate_Information.aspx/ "Harris County Clerk Records"], Records 173-58-0017, 196-54-3791, 504-34-0860 Harris County, 1 November 1993.</ref>

Starting in the early 2000s, residents throughout Hyde Park petitioned for the implementation of Chapter 42 Minimum Lot Size (MLS) and Minimum Building Line (MBL) Ordinances. These City of Houston states that protections are intended to help neighborhoods maintain their character.<ref>[http://www.houstontx.gov/planning/Neighborhood/docs_pdfs/MLS_Brochure_2014.pdf "MLS Brochure"], ''[[City of Houston]]'', Retrieved on 4 February 2015.</ref> Through 2014, the community has been successful at implementing these protections on many streets within Hyde Park, such as Bomar, Fairview, Hyde Park, Jackson, Michigan, Welch, West Drew, and Willard.<ref>[http://www.houstontx.gov/planning/Neighborhood/prevailLotBldg.html "Minimum Lot Size (MLS)/Minimum Building Line (MBL) Ord."], ''[[City of Houston]]'', Retrieved on 8 December 2014.</ref>

==Government and infrastructure==

===Emergency services===
There is a [[Houston Police Department]] storefront at 802 Westheimer. The community shares [[Neartown Houston#Local government|Police and Fire Department]] resources with other Neartown neighborhoods.

===Representatives===
{{as of|2015|February}}, Hyde Park was represented by:
* Houston City Council, [[Houston City Council#District C|District C]]—Council Member [[Ellen Cohen]]
* Texas State House District 147—Representative [[Garnet Coleman]]
* Texas State Senate District 15—Senator [[John Whitmire]]
* Texas U.S. House of Representatives Congressional District 2--Congressman [[Ted Poe]]
* Texas U.S. Senators—Senator [[John Cornyn]] and Senator [[Ted Cruz]]

==Education==
Residents are zoned to the [[Houston Independent School District]] (HISD).<ref>"[http://hydeparkhouston.org/?page_id=5 Hyde Park Civic Association Boundaries]." Hyde Park Civic Association. Retrieved on December 19, 2016.</ref> Residents east of Waugh are zoned to [[Wharton Dual Language Academy|Wharton K-8 School]] (for elementary school),<ref>"[http://www.houstonisd.org/cms/lib2/TX01001591/Centricity/domain/32468/boundarymaps/Wharton_K8.pdf Wharton Elementary School Attendance Boundary]." [[Houston Independent School District]]. December 19, 2016.</ref> and  [[Gregory-Lincoln Education Center]] (middle school only).<ref>"[http://www.houstonisd.org/cms/lib2/TX01001591/Centricity/domain/32468/boundarymaps/Gregory-Lincoln_MS_K8.pdf Gregory-Lincoln Middle School Attendance Boundary]," [[Houston Independent School District]]. Retrieved on December 19, 2016.</ref> Those west of Waugh are zoned to Wilson K-8 School (for elementary school),<ref>"[http://www.houstonisd.org/cms/lib2/TX01001591/Centricity/domain/32468/boundarymaps/Wilson_K8.pdf Wilson Elementary School Attendance Boundary]." [[Houston Independent School District]]. December 19, 2016.</ref> and [[Lanier Middle School (Houston)|Lanier Middle School]].<ref>"[http://www.houstonisd.org/cms/lib2/TX01001591/Centricity/domain/32468/boundarymaps/Lanier_MS.pdf Lanier Middle School Attendance Boundary]," [[Houston Independent School District]]. Retrieved on December 19, 2016.</ref> All residents are zoned to [[Lamar High School (Houston)|Lamar High School]].<ref>"[http://www.houstonisd.org/cms/lib2/TX01001591/Centricity/domain/32468/boundarymaps/Lamar_HS.pdf Lamar High School Attendance Boundary] {{webarchive|url=https://web.archive.org/web/20150513135353/http://www.houstonisd.org/cms/lib2/TX01001591/Centricity/domain/32468/boundarymaps/Lamar_HS.pdf |date=2015-05-13 }}," [[Houston Independent School District]]. Retrieved on December 19, 2016.</ref>

==Points of Interest==

'''Parks & Recreation'''
*Lamar Park & Dolphin Fountain<ref>[http://houstonparksboard.org/projects/lamar_park/ "Lamar Park"], "Houston Parks Board", Retrieved on 8 December 2014.</ref>

'''Churches & Charities'''
*Grade Lutheran Church<ref name="chron.com"/>
*Assistance League of Houston<ref>[http://www.assistanceleaguehou.org/ps.aboutus.cfm?ID=3440 "Assistance League of Houston"], Retrieved on 9 December 2014.</ref>

'''Farmer's Markets & Co-ops'''
*Central City Co-op<ref>[http://www.centralcityco-op.com/faqs.html "Central City Co-op"], Retrieved on 8 December 2014.</ref>

'''Events'''
*[[National Night Out]]<ref>[http://hydeparkhouston.org/?p=781 "Annual National Night Out a Huge Success!"], "Hyde Park Civic Association",  Retrieved on 8 December 2014.</ref>
*[[Houston Gay Pride Parade|Houston Pride Parade]]

'''Health Care'''
*Legacy Community Health Services<ref>[http://www.legacycommunityhealth.org/ "Legacy Community Health"], Retrieved on 8 December 2014.</ref>

==References==
{{Reflist|30em}}

==External links==
* [http://hydeparkhouston.org Hyde Park Civic Association]
[[Category:Gay villages in Texas]]
[[Category:Parks in Texas]]
[[Category:Neighborhoods in Houston]]