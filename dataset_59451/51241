{{Infobox journal
| title = Ethnicities
| cover = [[File:Ethnicities (journal) front cover.jpg]]
| editors = Stephen May, Tarig Modood
| discipline = [[Ethnic studies]]
| formernames =
| abbreviation = Ethnicities
| publisher = [[SAGE Publications]]
| country =
| frequency = Bimonthly
| history = 2001-present
| openaccess =
| license =
| impact = 0.667
| impact-year = 2010
| website = http://www.uk.sagepub.com/journals/Journal200776
| link1 = http://etn.sagepub.com/content/current
| link1-name = Online access
| link2 = http://etn.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 635692872
| LCCN = 2001223219
| CODEN = 
| ISSN = 1468-7968
| eISSN = 
}}
'''''Ethnicities''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes research in the fields of [[sociology]] and [[politics]] concerning questions of [[ethnicity]], [[nationalism]] and related issues such as [[identity politics]] and minority rights. It was established in 2001 and is published bimonthly by [[SAGE Publications]]. The [[editors-in-chief]] are Stephen May ([[University of Auckland]]) and [[Tariq Modood]] ([[University of Bristol]]).

== History ==
''Ethnicities'' was established in 2001, initially with three issues a year. The following year it moved to four issues and in 2012 to six issues. The founding editors were Stephen May and Tariq Modood.

'''Debate Section'''
''Ethnicities'' carries a Debate section that addresses topics of debate within the subject area, including: constructivist and realist conceptions of culture (volume 1.2), critical multiculturalism (volume 1.3), cosmopolitanism (volume 3.4), feminism, ethnicity and reproductive technology (volume 5.2), whiteness (volume 6.2), [[symbolic ethnicity]] (volume 9.1), critical race theory (volume 9.2).

'''Review symposia'''
This section of ''Ethnicities'' has explored the work of people like Bhikhu Parekh (volume 1.1), Brian Barry (volume 2.2), Paul Gilroy (volume 2.4), Glen Loury (volume 3.2), Evelyn Nakano Glenn (volume 4.3), Will Kymlicka (volume 8.2), and Veit Bader (volume 9.4).

'''Special issues'''
''Ethnicities'' publishes a special issue each year, past issues include cities and ethnicities (volume 2.3), multiculturalism and identities (volume 3.3), racialization in the USA (volume 4.3), identity, culture and globalization (volume 5.3), ethnic inequalities and education (volume 7.3), women and multiculturalism (volume 8.3), Muhammad cartoons controversy (volume 9.3).

'''Invited Symposia'''
The journal commissions Invited Symposia, which discuss topical issues. In its first issue, the journal offered an academic symposium on the key issues facing the field of ethnicity at that time, with contributions from Craig Calhoun, Nira Yuval-Davis, T.K. Oommen, [[Rogers Brubaker]], Thomas Hylland Eriksen, Roger Waldinger and Will Kymlicka. In its early volumes, ''Ethnicities'' published an editorial (2.1) and an academic symposium on the reasons for, and potential consequences of, the September 11, 2001 terrorist attack on the USA (2.2), with the latter including contributions from Mahmood Mamdani, Mohammad Waseem, Ruth Rubio-Marín, Barnor Hesse, Ien Ang, and the late Iris Marion Young.

''Ethnicities'' has cross-disciplinary aims and contains articles from the disciplines of sociology, political theory, cultural geography, anthropology, cultural and media studies, history, education, social psychology and law.

== Abstracting and indexing ==
Among other databases, ''Ethnicities'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 0.667, ranking it 7th out of 13 journals in the category "Ethnic Studies".<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Ethnic Studies |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-08-25 |series=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://etn.sagepub.com/}}

<!--- Categories --->
[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 2001]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Sociology journals]]