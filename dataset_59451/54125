{{Use dmy dates|date=September 2011}}
{{Use Australian English|date=September 2011}}
{{Infobox MP
| honorific-prefix =
| name = Nick Champion
| honorific-suffix = [[Member of parliament|MP]]
| image = Nick_Champion.jpg
| constituency_MP = [[Division of Wakefield|Wakefield]]
| parliament = Australian
| majority =
| predecessor = [[David Fawcett]]
| successor =
| term_start = 24 November 2007
| term_end =
| birth_date = {{birth date and age|df=yes|1972|2|27}}
| birth_place = [[Elizabeth, South Australia]], Australia
| death_date =
| death_place =
| nationality = [[Australia]]n
| spouse = Fiona Webber
| party = [[Australian Labor Party]]
| relations =
| children =
| residence =
| alma_mater = [[University of South Australia]]
| occupation = Union official
| profession =
| religion =
| signature =
| website =
| footnotes =
}}
'''Nicholas David "Nick" Champion''' (born 27 February 1972 in [[Elizabeth, South Australia|Elizabeth]], [[South Australia]]), is an [[Australian Labor Party]] member of the [[Australian House of Representatives|House of Representatives]] seat of [[Division of Wakefield|Wakefield]] since the [[Australian federal election, 2007|2007 election]]. He is Shadow Assistant Minister for Manufacturing and Science and the Deputy Chair of the Joint Standing Committee on Foreign Affairs, Defence and Trade.

==Background==
Champion was born in [[Elizabeth, South Australia|Elizabeth]] in [[South Australia]]. He spent his early years in the rural town of [[Kapunda, South Australia|Kapunda]] and completed his secondary education at Kapunda High School while working part-time as a fruit picker. He also previously worked as a cleaner, salesman and trolley collector. He completed an Arts degree and a Graduate Diploma in Communication at the [[University of South Australia]].<ref name=alp>[http://www.alp.org.au/nick_champion Nick Champion profile: ALP]</ref>

Champion became a union official at the [[Shop, Distributive and Allied Employees Association]] (SDA) in 1994, serving as an organiser, training officer and occupational health and safety officer.<ref name=alp/> He is aligned with the [[Labor Right]].<ref>[http://www.news.com.au/adelaidenow/story/0,22606,26068078-2682,00.html Wakefield MP's push to remove 'pokies trickery': The Advertiser 13/9/2009]</ref>

Champion served as South Australian State President of the Australian Labor Party from 2005 to 2006 and was a ministerial adviser to state Labor Minister [[Michael Wright (Australian politician)|Michael Wright]].<ref>[http://www.aph.gov.au/Senators_and_Members/Parliamentarian?MPID=HW9 Nick Champion: APH]</ref>

==Parliament==
[[Image:2010 0119 Tour Down Under Murray Street Gawler (9) (19332572883).jpg|right|thumb|Labor MPs Champion, [[Mike Rann]], [[Kevin Rudd]] and [[Tony Piccolo]] in [[Gawler, South Australia|Gawler]] for the [[Tour Down Under]] in 2010.]]
Champion won his seat at the [[Australian federal election, 2007|2007 election]], defeating incumbent [[Liberal Party of Australia]] member [[David Fawcett]].<ref>{{cite news |url=http://www.abc.net.au/news/stories/2007/11/24/2100281.htm |title=Big swings against Coalition across SA |date=24 November 2007 |accessdate=2007-11-25}}</ref>

Champion became only the third Labor member ever to win [[Division of Wakefield|Wakefield]] at the [[Australian federal election, 2007|2007 election]] with a 56.6 percent two-party vote. Champion made it a safe Labor seat on paper at the [[Australian federal election, 2010|2010 election]] with a 62 percent two-party vote, and became the first Labor member to be re-elected to Wakefield. The South Australian federal redistribution in 2011 had the greatest impact on Wakefield where the Labor margin declined by 1.5 percent. Champion retained Wakefield at the [[Australian federal election, 2013|2013 election]] on a 53.4 percent two-party vote even as Labor lost government, marking the first time the non-Labor parties won government at an election without winning Wakefield. Champion increased his margin at the [[Australian federal election, 2016|2016 election]] with a 61 percent two-party vote, again making Wakefield a safe Labor seat on paper.

==References==
{{reflist}}

==External links==
*{{OpenAustralia}}
*[https://theyvoteforyou.org.au/people/representatives/wakefield/nick_champion Summary of parliamentary voting for Nick Champion MP on TheyVoteForYou.org.au]

{{S-start}}
{{s-par|au}}
{{s-bef|before=[[David Fawcett]]}}
{{s-ttl|title={{nowrap|Member for [[Division of Wakefield|Wakefield]]}}|years=2007–present}}
{{s-inc}}
{{S-end}}

{{Current South Australia Representatives}}

{{DEFAULTSORT:Champion, Nick}}
[[Category:Australian people of Cornish descent]]
[[Category:Australian Labor Party members of the Parliament of Australia]]
[[Category:Living people]]
[[Category:Members of the Australian House of Representatives]]
[[Category:Members of the Australian House of Representatives for Wakefield]]
[[Category:University of South Australia alumni]]
[[Category:1972 births]]
[[Category:Australian trade unionists]]
[[Category:21st-century Australian politicians]]


{{Australia-Labor-representative-stub}}