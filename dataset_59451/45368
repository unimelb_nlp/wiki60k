{{EngvarB|date=July 2016}}
{{Use dmy dates|date=July 2016}}
{{Infobox military person
|name= '''Ernest Derek 'Dave' Glaser'''
|birth_date=20 April 1921
|death_date=2001
|birth_place=
|death_place= 
|image=
|caption=
|nickname=Dave
|allegiance={{flag|United Kingdom}}
|serviceyears= 1939–1953
|rank= [[Squadron Leader]]
|branch= {{air force|United Kingdom}}
|commands=[[No. 234 Squadron RAF]]<br>[[No. 548 Squadron RAF]]<br>[[No. 549 Squadron RAF]]<br>[[No. 64 Squadron RAF]]
|unit=
|battles= [[Second World War]]
*[[Battle of Britain]]
|awards=[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]], [[Air Efficiency Award]], [[Queen's Commendation for Valuable Service]]
|laterwork=
}}
[[Squadron Leader]] '''Ernest Derek 'Dave' Glaser''' [[Distinguished Flying Cross (United Kingdom)|DFC]] [[Air Efficiency Award|AE]] (20 April 1921 – 2001) was a British [[Royal Air Force]] officer of the [[Battle of Britain]], and later a notable [[test pilot]].<ref>{{cite news |last= |first= |date=16 July 2001 |title=Squadron Leader Dave Glaser |url=http://www.telegraph.co.uk/news/obituaries/1334103/Squadron-Leader-Dave-Glaser.html |newspaper=The Telegraph |location=London |accessdate=8 January 2015 }}</ref><ref>[http://www.the-battle-of-britain.co.uk/GlaserD/glaserd.htm Dave Glaser profile]</ref>

Glaser was the son of a former [[Royal Flying Corps]] officer and brought up in [[Hampshire]]. He was educated at Lancing House and [[Bloxham School]], before being accepted for flying training in the [[Royal Air Force Volunteer Reserve]] in April 1939.<ref>{{cite news |last= |first= |date=16 July 2001 |title=Squadron Leader Dave Glaser |url=http://www.telegraph.co.uk/news/obituaries/1334103/Squadron-Leader-Dave-Glaser.html |newspaper=The Telegraph |location=London |accessdate=8 January 2015 }}</ref>

In 1940, Glaser was attached to [[No. 65 Squadron RAF]], where he flew [[Supermarine Spitfire]]s alongside [[Jeffrey Quill]] and [[Franciszek Gruszka]] in the [[Battle of Britain]].<ref>BBC – WW2 People's War, ''Sgt Pilot Harold Orchard RAF – Part 4'' http://www.bbc.co.uk/history/ww2peopleswar/stories/17/a6812417.shtml (Accessed 8 January 2015)</ref> His plane became known for its [[nose art]], representing The Laughing Cavalier. Glaser was promoted to Flight Lieutenant and in July 1940 transferred to [[No. 234 Squadron RAF]] as a flight commander.<ref>Peter Brown, '6 October', ''RAF Southend'' (The History Press, 1 June 2012)</ref> It was while serving with No. 234 that he was mistakenly shot down on 13 July 1940 by a [[Royal Navy]] warship off the English south coast.<ref>{{cite news |last= |first= |date=16 July 2001 |title=Squadron Leader Dave Glaser |url=http://www.telegraph.co.uk/news/obituaries/1334103/Squadron-Leader-Dave-Glaser.html |newspaper=The Telegraph |location=London |accessdate=8 January 2015 }}</ref> He was promoted to Pilot Officer in 1941.<ref>{{London Gazette |issue=35228 |date=25 July 1941 |startpage=4282 }}</ref>

He became temporary commander of the squadron in October. He was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] in August 1942.<ref>{{London Gazette |issue=35678 |date=21 August 1942 |startpage=3709 |supp=x }}</ref> In 1943, Glaser was posted to Australia to form and command [[No. 548 Squadron RAF]], a Spitfire squadron stationed at [[Darwin, Northern Territory]]. In the New Year of 1945 he received command of [[No. 549 Squadron RAF]], a Spitfire squadron similarly charged with defending Darwin against [[Empire of Japan|Japanese]] air attack.<ref>Andrew Thomas, ''Spitfire Aces of Burma and the Pacific'' (Osprey Publishing, 2009), 93.</ref> In 1946 he was awarded the [[Air Efficiency Award]]. After two years he returned home, was granted a permanent commission and posted to Linton-on-Ouse, Yorkshire. There he was flight commander of [[No. 64 Squadron RAF]], a half-strength [[de Havilland Hornet|Hornet fighter]] squadron.<ref>{{cite news |last= |first= |date=16 July 2001 |title=Squadron Leader Dave Glaser |url=http://www.telegraph.co.uk/news/obituaries/1334103/Squadron-Leader-Dave-Glaser.html |newspaper=The Telegraph |location=London |accessdate=8 January 2015 }}</ref>

In 1949 Glaser passed the [[Empire Test Pilots' School]] and became a test pilot at the [[Royal Aircraft Establishment]]. He became a test pilot with [[Vickers Armstrong]] in 1952, and was involved in testing and developing planes such as the [[Vickers Varsity]], [[Vickers Viscount]] and the [[Vickers Valiant]]. Glaser was also involved in testing the [[BAC One-Eleven]].<ref>{{cite news |last= |first= |date=16 July 2001 |title=Squadron Leader Dave Glaser |url=http://www.telegraph.co.uk/news/obituaries/1334103/Squadron-Leader-Dave-Glaser.html |newspaper=The Telegraph |location=London |accessdate=8 January 2015 }}</ref><ref>Stephen Skinner, ''BAC One-Eleven: The Whole Story'' (The History Press, 31 January 2013)</ref> In 1979 he became flight operations manager and test pilot instructor of [[Rombac]] in Romania.<ref>{{cite news |last= |first= |date=16 July 2001 |title=Squadron Leader Dave Glaser |url=http://www.telegraph.co.uk/news/obituaries/1334103/Squadron-Leader-Dave-Glaser.html |newspaper=The Telegraph |location=London |accessdate=8 January 2015 }}</ref> In 1983 he retired from [[British Aerospace]] and worked as a successful aviation consultant. He had been awarded the military [[Queen's Commendation for Valuable Service]] in 1953, and was rewarded the commendation for civil test flying in 1968.<ref>{{London Gazette |issue=39863 |date=26 May 1953 |startpage=2990 |supp=x }}</ref><ref>{{London Gazette |issue=44600 |date=31 May 1968 |startpage=6331 |supp=x }}</ref>

==References==
{{Reflist|2}}

{{DEFAULTSORT:Glaser, Dave}}
[[Category:1921 births]]
[[Category:2001 deaths]]
[[Category:British test pilots]]
[[Category:Royal Air Force pilots of World War II]]
[[Category:English aviators]]
[[Category:People educated at Bloxham School]]
[[Category:Recipients of the Commendation for Valuable Service]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:The Few]]