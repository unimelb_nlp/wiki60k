{{Good article}}
{{Use mdy dates|date=February 2017}}
{{Infobox officeholder
|name               = Ricardo Arias Calderón
|order              = 1st
|office             = Vice President of Panama
|term_start         = December 1990
|term_end           = December 17, 1992
|president          = [[Guillermo Endara]]
|predecessor        = [[Carlos Ozores|Carlos Ozores Typaldos]]
|successor          = [[Guillermo Ford]]
|birth_date         = {{Birth date|1933|5|4}}
|death_date         = {{death date and age|2017|02|13|1933|5|4}}
|birth_place        = [[Panama City]], [[Panama]]
|death_place        = Panama City, Panama
|nationality        = Panamanian
|party              = [[Christian Democratic Party of Panama]]
|otherparty         =  <!--For additional political affiliations-->
|spouse             = Teresa
|children           = 4
|alma_mater         = [[Yale]], [[Paris-Sorbonne University|Sorbonne]]
|religion           = [[Roman Catholic]]
}}

'''Ricardo Arias Calderón''' (May 4, 1933 – February 13, 2017)<ref>{{cite web |url=http://www.panamaamerica.com.pa/notas/1602844-las-raices--de-un-filosofo,-academico-y-activo-politico |title=Las raíces de un filósofo, académico y activo político |author= |date=June 22, 2013 |work=PanamaAmerica |publisher= |accessdate=October 23, 2013}}</ref> was a Panamanian politician who served as [[Vice President of Panama|First Vice President]] from 1989 to 1992. A [[Roman Catholic]] who studied at [[Yale University|Yale]] and the [[Paris-Sorbonne University|Sorbonne]], Arias returned to [[Panama]] in the 1960s to work for political reform. He went on to become the president of the [[Christian Democratic Party of Panama]] and a leading opponent of the military government of [[Manuel Noriega]]. In 1984, he ran as a candidate for Second Vice President on the ticket of three-time former president [[Arnulfo Arias]], but they were defeated by pro-Noriega candidate [[Nicolás Ardito Barletta]].

Following an [[Panamanian general election, 1989|annulled 1989 election]] and the [[US invasion of Panama]] later in the same year, Arias  Calderón was sworn in as First Vice President of Panama under President [[Guillermo Endara]]. After growing tensions in the ruling coalition, Arias resigned his position on December 17, 1992, stating that the government had not done enough to help Panama's people. He continued to be an active voice in Panamanian politics following his resignation, supporting the [[Panama Canal expansion project]] and opposing the extradition of Noriega.

== Background ==
Arias' maternal family was from Nicaragua, having left during political upheaval there before his birth. One of Arias' great uncles ran for president in Panama, while another was a supporter of the Nicaraguan revolutionary [[Sandino]], a family history that gave Arias an early interest in politics. His father, an engineer, died when Arias was two years old, and he was raised primarily by his mother, aunt, and grandmother. His mother later remarried to a Panamanian ambassador to the United States.<ref name=WP90 />

Arias studied at [[Culver Military Academy]] in [[Indiana]] in the US. He later majored in English literature at [[Yale University]] and philosophy at [[Paris-Sorbonne University]]. A [[Roman Catholic]], Arias was heavily influenced by Catholic French philosopher and ethicist [[Jacques Maritain]]. Aesthetic, publicly stiff, and accused of aloofness, Arias would later be nicknamed "Arias Cardinal Calderón" during his political career.<ref name=WP90 />

== Early political career ==
Arias returned to Panama in the early 1960s to work for political reform, soon joining the small [[Christian Democratic Party of Panama]]. In 1972, he left Panama for some time with his family, becoming a dean and later vice president at [[Florida International University]] in [[Miami]], [[Florida]] in the US. In 1980, however, he declined an offer to become provost, and instead returned to Panamanian politics.<ref name=WP90 />

During the rule of military leader [[Manuel Noriega]], Arias was an opposition leader as the president of the [[Christian Democratic Party of Panama]], a member party of the Civic Democratic Opposition Alliance (ADOC).<ref name=WP90>{{cite web |url=http://www.highbeam.com/doc/1P2-1107830.html |title=Panama's Philosopher Pol;Ricardo Arias Calderon's Leap From Exiled Academic to Vice President |author=[[Myra MacPherson]] |date=January 30, 1990 |work=The Washington Post |accessdate=August 31, 2012}} {{subscription required}}</ref><ref>{{cite news |agency=Associated Press |url=http://articles.latimes.com/1989-10-02/news/mn-402_1_opposition-members |title=Panama Said to Arrest 9 Opposition Members |date=October 2, 1989 |work=Los Angeles Times |deadurl=no|archivedate=August 31, 2012 |archiveurl=http://www.webcitation.org/6AJqcVjqk |accessdate=August 31, 2012}}</ref> He ran on the ticket of three-time former president [[Arnulfo Arias]] (no relation) in [[Panamanian general election, 1984|the 1984 election]] as the National Alliance of Opposition's candidate for Second Vice President.<ref>{{cite web |url=https://news.google.com/newspapers?id=ZbtLAAAAIBAJ&sjid=eVADAAAAIBAJ&pg=6494,1462180&dq=ricardo+arias+calder%C3%B3n&hl=en |title=Panama vote untallied; Ecuador unity pledged |agency=United Press International |date=May 7, 1984 |work=The Courier |accessdate=April 24, 2013}}</ref> Arnulfo Arias was narrowly defeated by Noriega ally [[Nicolás Ardito Barletta Vallarino]], and the opposition stated that the election had been fraudulent.<ref>{{cite web |url=https://news.google.com/newspapers?id=OKZdAAAAIBAJ&sjid=KF0NAAAAIBAJ&pg=1398,1451949&dq=ricardo+arias+calder%C3%B3n+1984&hl=en |title=Opposition cries fraud during Panama's presidential election |agency=Associated Press |date=May 8, 1989 |work=Observer-Reporter |accessdate=April 24, 2013}}</ref>

In February 1988, plainclothes police officers forced Arias Calderón and his wife onto a plane to [[Costa Rica]] at gunpoint, and the couple spent a month in exile in Miami.<ref name=WP90 /> Arias returned to Panama in March, calling openly for Noriega's ouster on arrival at the [[Omar Torrijos International Airport]].<ref>{{cite web |url=http://www.highbeam.com/doc/1P2-3877009.html |title=Rival leader returns, urges Noriega to quit |date=March 25, 1988 |work=Chicago Sun-Times |accessdate=August 31, 2012}} {{subscription required}}</ref> In Panama's [[Panamanian general election, 1989|May 1989 elections]], Arias stood as a candidate for First Vice President with the ADOC, with [[Guillermo Endara]] as the presidential candidate and [[Guillermo Ford]] as the candidate for Second Vice President. However, Noriega's government annulled the election before voting was complete. Days after the completion of voting, Endara, Arias, and Ford were attacked on camera by Noriega supporters while security forces observed and refused to intervene.<ref>{{cite web |url=https://www.nytimes.com/1990/01/05/world/noriega-s-surrender-panama-in-disorder-the-1980-s.html |title=Noriega's Surrender; Panama in Disorder: The 1980's |date=January 5, 1990 |work=The New York Times |deadurl=no|archivedate=August 31, 2012 |archiveurl=http://www.webcitation.org/6AJrhmRqV |accessdate=August 31, 2012}}</ref> In October of that year, Arias was briefly arrested for urging citizens not to pay taxes to his government.<ref>{{cite web |url=http://articles.latimes.com/1989-10-03/news/mn-441_1_arias-calderon |title=The World |date=October 3, 1989 |work=Los Angeles Times|deadurl=no |archivedate=August 29, 2012 |archiveurl=http://www.webcitation.org/6AJpfdtDd |accessdate=August 29, 2012}}</ref>

== Vice presidency ==
Following Noriega's fall in the December 1989 [[United States invasion of Panama]], Arias was certified as vice president of Panama under President Endara<ref>{{cite web |url=http://www.state.gov/r/pa/ei/bgn/2030.htm |title=Background Note: Panama |publisher=Bureau of Western Hemisphere Affairs, US State Department |date=March 26, 2012|deadurl=no |archivedate=August 29, 2012 |archiveurl=http://www.webcitation.org/6AJqNaFWR |accessdate=August 29, 2012}}</ref> and inaugurated on a US military base.<ref name=LAT318>{{cite web |url=http://articles.latimes.com/1991-05-06/news/mn-943_1_political-party |title=Panama's 3-Party Rule Turns Into 3-Ring Circus |author=Kenneth Freed |date=May 6, 1991 |work=Los Angeles Times|deadurl=no |archivedate=August 31, 2012 |archiveurl=http://www.webcitation.org/6AJuAQAxc |accessdate=August 31, 2012}}</ref>

Arias was put in charge of reforming the Panamanian police forces, putting them under civilian control. He controversially employed former members of Noriega's [[Panamanian Defense Forces]], stating that he trusted them with his own security and that it was "time to look to the future".<ref name=WP90 /> His defense of former PDF soldiers split supporters of the coalition government, and in May 1990, sparked rumors that he and the CDP were attempting a coup while Endara was out of the country. The presidential offices were occupied by Endara loyalists with submachineguns, who accidentally shot and killed one of Endara's staff members.<ref>{{cite web |url=http://www.highbeam.com/doc/1G1-9046637.html |title=Wedding imminent, dowry missing |date=May 26, 1990 |work=The Economist |accessdate=August 31, 2012}} {{subscription required}}</ref>

In early 1991, the ADOC coalition began to unravel as Endara, Arias, and Ford publicly criticized one another. On April 8, accusing Arias' Christian Democrats of not rallying to his support during an impeachment vote, Endara dismissed Arias from the cabinet.<ref name=LAT318 />

Arias resigned from the vice presidency on December 17, 1992, stating at a news conference that Endara's government "does not listen to the people, nor does it have the courage to make changes". Endara responded that Arias' resignation was "demagoguery" and "merely starting his 1994 political campaign ahead of time".<ref>{{cite web |url=http://www.highbeam.com/doc/1G1-43614268.html |title=Ricardo Arias Calderon |date=February 1, 1993 |work=Caribbean Update |accessdate=August 31, 2012}}{{subscription required}}</ref>

== Later activity ==
Arias was an opponent of the post-invasion US presence in Panama before the December 31, 1999 handover of the [[Panama Canal]] to the [[Panama Canal Authority]].<ref>{{cite web |url=http://articles.latimes.com/2000/jan/01/news/mn-49575 |title='The Canal Is Ours' Is Jubilant Cry in Panama |author=Juanita Darling |date=January 1, 2000 |work=Los Angeles Times |deadurl=no|archivedate=August 29, 2012 |archiveurl=http://www.webcitation.org/6AJpvwqPB |accessdate=August 29, 2012}}</ref> Arias was criticized in 1998 by Endara's successor, [[Ernesto Pérez Balladares]], as "immoral" for having claimed almost $100,000 in salary from his time as vice president despite having resigned. Arias subsequently challenged Pérez Balladares to a debate over the morality of the latter's plans to amend the constitution and seek a second term.<ref>{{cite web |url=http://www.critica.com.pa/archivo/061698/portada.html |title=Ricardo Arias Calderon Balladares challenges |author=Omar Wong Wood |date=June 16, 1998 |work=Critica |accessdate=April 24, 2013 |archivedate=April 24, 2013 |archiveurl=http://www.webcitation.org/6G8ti5ANN |deadurl=no}}</ref>

In 2001, Arias released a book, ''Democracy without an Army: The Panamanian Experience'', arguing that the nation must keep its security forces depoliticized.<ref>{{cite web |url=http://mensual.prensa.com/mensual/contenido/2001/06/08/hoy/portada/152943.html |title=Arias Calderón llama a una verdadera unidad nacional |language=Spanish |author=Serrano Sucre Hermes |date=June 8, 2001 |work=La Prensa |accessdate=April 24, 2013 |archivedate=April 24, 2013 |archiveurl=http://www.webcitation.org/6G8uHhAoq |deadurl=no}}</ref> That same year, he allied with [[Democratic Revolutionary Party]], the former party of Noriega. He later pressed criminal defamation charges against ''[[La Prensa (Panama City)|La Prensa]]'' cartoonist Julio Briceño for a cartoon of Arias standing besides the [[Grim Reaper]], representing the new alliance. Arias additionally asked for a million dollars in damages, stating "That cartoon made me an accomplice of a crime ... That was a defamation I could not accept or tolerate. I was the one who denounced those crimes at the time of the dictatorship."<ref>{{cite web |url=https://www.nytimes.com/2001/10/28/world/panama-is-putting-journalists-on-trial.html?pagewanted=all |title=Panama Is Putting Journalists on Trial |author=David Gonzalez |date=October 28, 2001 |work=The New York Times|deadurl=no |archivedate=August 28, 2012 |archiveurl=http://www.webcitation.org/6AGYabozD |accessdate=August 28, 2012}}</ref> In 2006, he supported [[Panama Canal expansion project|a project to widen the canal]], calling it "historical suicide" not to do so.<ref>{{cite web |url=http://articles.latimes.com/2006/sep/23/world/fg-canal23 |title=Panamanians Likely to OK Canal's Expansion Project |author=Chris Kraul |date=September 23, 2006 |work=Los Angeles Times |archivedate=August 31, 2012 |deadurl=no|archiveurl=http://www.webcitation.org/6AJqoQ97Y |accessdate=August 31, 2012}}</ref>

Arias opposed the 2011 extradition of Noriega from France to Panama, warning that the former dictator could institute a "demagogic populism" similar to that of Venezuela's [[Hugo Chávez]].<ref>{{cite web |url=http://www.abc.com.py/internacionales/arias-calderon-advierte-del-peligro-que-noriega-representa-para-la-democracia-341718.html |title=Arias Calderón warns against Noriega represents for democracy |date=December 8, 2011 |work=ABC |agency=EFE |accessdate=April 24, 2013 |archivedate=April 24, 2013 |archiveurl=http://www.webcitation.org/6G8vPi0dS |deadurl=no}}</ref>

== Personal life ==
Arias had a Cuban-born wife, Teresa, whom he married in 1964 and with whom he had four children. In the 1960s, she broke new ground for political spouses by attending political rallies and campaigning actively for her husband.{{whom| date= February 2017}} Because Endara was a widower, she also acted as Panama's First Lady until Endara remarried to [[Ana Mae Diaz Chen]] in 1990.<ref name=WP90/>

== References ==
{{Reflist|33em}}

{{s-start}}
{{s-off}}
{{succession box
|title=[[First Vice President of Panama]]
|before=[[Carlos Ozores|Carlos Ozores Typaldos]]
|after=[[Guillermo Ford]]
|years=1990&ndash;1992}}
{{s-end}}

{{authority control}}

{{DEFAULTSORT:Arias Calderon, Ricardo}}
[[Category:1933 births]]
[[Category:2017 deaths]]
[[Category:Panamanian Roman Catholics]]
[[Category:University of Paris alumni]]
[[Category:Vice Presidents of Panama]]
[[Category:Yale University alumni]]
[[Category:Place of birth missing]]
[[Category:Panamanian expatriates in the United States]]
[[Category:Panamanian expatriates in France]]