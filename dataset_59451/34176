{{Taxobox
| image = Verpa_bohemica1.jpg
| image_width = 234px
| image_caption =
| regnum = [[Fungus|Fungi]]
| divisio = [[Ascomycota]]
| classis = [[Ascomycetes]]
| ordo = [[Pezizales]]
| familia = [[Morchellaceae]]
| genus = ''[[Verpa]]''
| species = '''''V. bohemica'''''
| binomial = ''Verpa bohemica''
| binomial_authority = ([[Julius Vincenz von Krombholz|Krombh.]]) [[Joseph Schröter|J.Schröt.]] (1893)
| synonyms_ref = <ref name="urlMycoBank: Verpa bohemica"/>
| synonyms =
  {{plainlist|
*''Morchella bohemica'' <small>Krombh. (1828)</small>
*''Ptychoverpa bohemica'' <small>(Krombh.) [[Jean Louis Émile Boudier|Boud.]] (1907)</small>
  }}
}}
{{mycomorphbox
| name = ''Verpa bohemica''
| hymeniumType=smooth
| capShape = convex
| whichGills = NA
| stipeCharacter=bare
| sporePrintColor=yellow
| ecologicalType=saprotrophic
| howEdible=edible
| howEdible2=poisonous
}}

'''''Verpa bohemica''''' is a species of [[fungus]] in the family [[Morchellaceae]]. Commonly known as the '''early morel''' (or '''early false morel''') or the '''wrinkled thimble-cap''', it is one of several species known informally as a "[[false morel]]". The [[mushroom]] has a pale yellow or brown thimble-shaped [[pileus (mycology)|cap]]—{{convert|2|to|4|cm|in|1|abbr=on}} in diameter by {{convert|2|to|5|cm|in|1|abbr=on}} long—that has a surface wrinkled and ribbed with brain-like convolutions. The cap hangs from the top of a lighter-colored, brittle [[stipe (mycology)|stem]] that measures up to {{convert|12|cm|in|1|abbr=on}} long by {{convert|1|to|2.5|cm|in|1|abbr=on}} thick. Microscopically, the mushroom is distinguished by its large [[spore]]s, typically 60–80 by 15–18&nbsp;[[micrometre|µm]], and the presence of only two spores per [[ascus]].

In the field, the mushroom is reliably distinguished from the [[morel|true morel]]s on the basis of cap attachment: ''V.&nbsp;bohemica'' has a cap that hangs completely free from the stem. Although widely considered [[edible mushroom|edible]], consumption of the mushroom is generally not advised due to reports of [[mushroom poisoning|poisoning]] in susceptible individuals. Poisoning symptoms include [[wikt:gastrointestinal|gastrointestinal]] upset and lack of muscular coordination. ''V.&nbsp;bohemica'' is found in northern North America, Europe, and Asia. It fruits in early spring, growing on the ground in woods following the snowmelt, before the appearance of "true morels" (genus ''[[Morchella]]''). The [[synonym (biology)|synonym]] ''Ptychoverpa bohemica'' is often used by European mycologists.

==Taxonomy, phylogeny, and naming==
The species was first [[species description|described]] in the scientific literature by the Czech physician and mycologist [[Julius Vincenz von Krombholz]] in 1828, under the name ''Morchella bohemica''.<ref name="Krombholz 1828"/> The German naturalist [[Joseph Schröter]] transferred it to the genus ''[[Verpa]]'' in 1893.<ref name="Schroter 1893"/> ''Ptychoverpa bohemica'' is a [[synonym (taxonomy)|synonym]] that was published by Frenchman [[Jean Louis Émile Boudier]] in his 1907 treatise on the [[Discomycetes]] of Europe;<ref name="Boudier 1907"/> the name is still occasionally used, especially in European publications.<ref name="urlVerpa bohemica (MushroomExpert.Com)"/> Boudier believed that the large, curved [[ascospore]]s and the rare and short [[paraphyses]] were sufficiently distinct to warrant a new genus to contain the single species.<ref name="Boudier 1907"/><ref name="Boudier 1892"/> ''Ptychoverpa'' has also been [[biological classification|classified]] as a [[section (botany)|section]] of ''Verpa''.<ref name="Underwood 1892"/> The section is characterized by the presence of thick longitudinal ridges on the cap that can be simple or forked.<ref name="Underwood 1899"/>

The [[specific name (botany)|specific epithet]] ''bohemica'' refers to [[Bohemia]] (now a part of the [[Czech Republic]]),<ref name="Smith 1980"/> where Krombholz originally collected the species.<ref name="Krombholz 1828"/> The mushroom is [[common name|commonly]] known as the "early morel",<ref name="McKnight 1987"/> "early false morel", or the "wrinkled thimble-cap".<ref name="Ammirati 1987"/>  ''Ptychoverpa'' is derived from the [[Ancient Greek]] ''ptyx'' ([[genitive case|genitive]] form ''ptychos''), meaning "fold", layer", or "plate".<ref name="Scarborough 1992"/>

==Description==
[[File:Verpa-bohemica-Xsection.jpg|thumb|left|Stems of young fruit bodies are initially stuffed with soft, cottony tissue.]]
[[File:Verpa bohemica 84787.jpg|thumb|left|The spores are large, measuring up to 80 µm long.]]
The [[pileus (mycology)|cap]] of this fungus (known technically as an [[apothecium]]) is {{convert|2|to|4|cm|in|1|abbr=on}} in diameter by {{convert|2|to|5|cm|in|1|abbr=on}} long, with a conical or bell shape. It is folded into longitudinal ridges that often fuse together (''[[anastomose]]'') in a vein-like network. The cap is attached to the stem at the top only—hanging from the top of the [[Stipe (mycology)|stipe]], with the lobed edge free from the stem—and varies in color from yellowish brown to reddish brown; the underside of the cap is pale. The [[stipe (mycology)|stem]] is {{convert|6|to|12|cm|in|1|abbr=on}} long by {{convert|1|to|2.5|cm|in|1|abbr=on}} thick, cream-white in color, and tapers upward so that the stem is thicker at the base than at the top.<ref name="Healy 2008"/> Although the stem is initially loosely stuffed with cottony [[hypha]]e, it eventually becomes hollow in maturity; overall, the mushroom is rather fragile.<ref name="Schalkwijk-Barendsen 1991"/> The [[spore print|spore deposit]] is yellow, and the [[trama (mycology)|flesh]] is white.<ref name="RogersMushrooms"/>

Relative to other typical mushroom species, the [[spores]] of ''V.&nbsp;bohemica'' are huge, typically measuring 60–80 by 15–18&nbsp;[[micrometre|µm]]. They are elliptical, smooth, sometimes curved, and appear [[hyaline]] (translucent)  to yellowish.<ref name="Healy 2008"/> The spores, which number two (more rarely three)<ref name="Horgen 1985"/> per [[ascus]] are characteristic for this species.<ref name="McKnight 1987"/> The smooth, elliptical asci measure 275–350&nbsp;µm long by 16–23&nbsp;µm wide.<ref name="Kuo 2005"/> The British-Canadian mycologist [[Arthur Henry Reginald Buller]] determined that the asci are [[heliotropic]]—they bend toward light. As he noted, "I cut transverse sections though their pilei, examined these sections under the microscope, and at once perceived that in all the hymenial grooves and depressions the asci were curved outwards so that their opercula must have faced the strongest rays of light to which the ends of the asci has been subjected in the places where the fruit-bodies developed."<ref name="Buller 1909"/> This response to the stimulus of light is significant because it permits a fruit body to point and later discharge its asci towards open spaces, thus increasing the chances that the spores will be dispersed by wind.<ref name="Buller 1909"/> The [[paraphyses]] are thick and club-shaped, with diameters of 7–8&nbsp;µm at their tips.<ref name="Seaver 1942"/>

===Edibility===

The [[edible mushroom|edibility]] of this species is questionable; although ''Verpa bohemica'' is eaten by many, consumption of large amounts in a single sitting, or on successive days, has been reported to cause poisoning in susceptible individuals.<ref name="Orr 1979"/> Symptoms include gastrointestinal upset and lack of muscular coordination, similar to the effects reported by some individuals after consuming the false morel species ''[[Gyromitra esculenta]]''.<ref name="Horgen 1985"/> The responsible toxin in ''G.&nbsp;esculenta'' is [[gyromitrin]]; it is suspected that ''V.&nbsp;bohemica'' may be able to synthesize low levels of the toxin.<ref name="Lincoff 1977"/> Over-consumption of the mushroom has been reported to have induced a coma.<ref name="Maybrier 2011"/> Those who do wish to eat this species are often advised to [[parboil]] with copious quantities of water (discarding the water before consumption),<ref name="Hall 2003"/> to dry the specimens before eating,<ref name="Sept 2006"/> or, if eating for the first time, to restrict consumption to small portions to test their tolerance.<ref name="McKnight 1987"/> Some advocate only eating the caps and discarding the stems.<ref name="Pelouch 2008"/> Opinions on the flavor of the mushrooms vary, ranging from "strong but not on a par with true morels",<ref name="Arora 1986"/> to "pleasant",<ref name="RogersMushrooms"/> to "not distinctive".<ref name="urlVerpa bohemica (MushroomExpert.Com)"/>

===Similar species===
{{multiple image
| footer = Lookalike species include the half-free morel (left) and ''Verpa conica'' (right).
| align = right
| image1 = Morchella semilibera 41604.jpg
| width1 = {{#expr: (160 * 1616 / 2130) round 0}}
| image2 = Verpa conica 83805.jpg
| width2 = {{#expr: (160 * 2736 / 3648) round 0}}
}}
The closely related species ''[[Verpa conica]]'' typically has a smooth cap, although specimens with wrinkled caps are known. ''V.&nbsp;conica'' may be distinguished microscopically by its eight-spored asci.<ref name="Arora 1986"/> Its North American range extends much further south than ''V.&nbsp;bohemica''.<ref name="Kuo 2005"/> Another similar group of species are the "half-free" morels, ''[[Morchella semilibera]]'' and others, which have a honeycombed cap that is attached to the stalk for about half of its length, and with ridges that are darker than the pits. Additionally, a cross-sectioned stem of a specimen of ''M.&nbsp;semilibera'' is hollow, while ''V.&nbsp;bohemica'' usually has cottony wisps in the stem,<ref name="urlVerpa bohemica (MushroomExpert.Com)"/> and ''M.&nbsp;semilibera'' usually has vertical perforations near the base, while ''V.&nbsp;bohemica'' lacks them. ''Verpa bohemica'' may be reliably distinguished from all similar species by its much larger spores.<ref name="Bessette 1992"/>

==Ecology, habitat and distribution==
The fruit bodies of ''V.&nbsp;bohemica'' grow singly or scattered on the ground in woods in early spring, often before the appearance of the [[morel]], and throughout the morel season.<ref name="urlVerpa bohemica (MushroomExpert.Com)"/> It is often found along riverbanks, near [[Populus sect. Aegiros|cottonwood]]s, [[willow]]s and [[aspen]]s, often buried in plant litter.<ref name="Ammirati 1987"/> The fungus prefers to fruit in moist areas with ample sunlight.<ref name="Skirgiello 1967"/> Its minimum growth temperature is {{convert|3|C|F}}, with an optimum of {{convert|22|C|F}}, and a maximum of about {{convert|30|C|F}}.<ref name="Gilman 1916"/> A study of [[carbon 13|carbon]] and [[nitrogen 15|nitrogen]] [[isotope analysis|isotope ratios]] indicated that ''Verpa bohemica'' is [[saprobic]], that is, obtaining nutrients from decomposing [[organic matter]].<ref name="Hobbie 2001"/> It has been suggested, however, that the fungus is [[mycorrhizal]] for at least part of its [[biological life cycle|life cycle]].<ref name="Kuo 2007"/> The fungus has a wide distribution throughout northern North America;<ref name="urlVerpa bohemica (MushroomExpert.Com)"/> its range extends south to the [[Great Lakes]] in the [[Midwestern United States]], and south to northern [[California]] on the [[West Coast of the United States|West Coast]].<ref name="Kuo 2005"/> In Europe, the fungus is widely distributed, and has been collected from Austria,<ref name="url:GBIF Portal"/> the Czech Republic,<ref name="Svrcek 1981"/> Denmark,<ref name="url:GBIF Portal"/> Finland,<ref name="Granmo 1982"/> Germany,<ref name="O'Donnell 1997"/> Norway,<ref name="Often 1990"/> Poland,<ref name="Skirgiello 1960"/> Russia,<ref name="Skryabina 1975"/> Slovenia,<ref name="url:GBIF Portal"/> Spain,<ref name="url:GBIF Portal"/> Sweden,<ref name="Ryman 1978"/> and the Ukraine.<ref name="Cybertruffle"/> In Asia, it has been recorded from India<ref name="Wani 2010"/> and Turkey.<ref name="Dogan 2007"/>

A 10-year study of the distribution, time of fruiting and habitats of morel and false morel population in [[Iowa]] showed that early false morels are the first morels to fruit in the spring, appearing shortly after leaves begin to form on [[deciduous]] trees.  Narrow-head morels (''[[Morchella angusticeps]]'') fruit next, followed by the yellow or white morels (''[[Morchella esculenta]]''), then lastly ''[[Morchella crassipes]]''.<ref name="Tiffany 1998"/> The fruit bodies serve as a habitat for breeding [[dipteran]]s (flies), including ''[[Porricondyla media]]'', ''[[Pegomya geniculata]]'', and ''[[Trichocera annulata]]''.<ref name="Krivosheina 2008"/>

==References==
{{Reflist|colwidth=30em|refs=

<ref name="Ammirati 1987">{{cite book |vauthors=Ammirati JF, McKenny M, Stuntz DE |title=The New Savory Wild Mushroom |publisher=University of Washington Press |location=Seattle, Washington |year=1987 |page=214 |isbn=0-295-96480-4}}</ref>

<ref name="Arora 1986">{{cite book |author=Arora D. |title=Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi |publisher=Ten Speed Press |location=Berkeley, California |year=1986 |page=794 |isbn=0-89815-169-4}}</ref>

<ref name="Bessette 1992">{{cite book |vauthors=Bessette A, Fischer DH |title=Edible Wild Mushrooms of North America: a Field-to-Kitchen Guide |publisher=University of Texas Press |location=Austin, Texas |year=1992 |page=137 |isbn=0-292-72080-7}}</ref>

<ref name="Boudier 1892">{{cite journal |title=Note sur les ''Morchella Bohemica'' Kromb. et voisons |journal=Bulletin de la Société Mycologique de France |volume=8 |author=Boudier É. |year=1892 |pages=141–4 |language=French}}</ref>

<ref name="Boudier 1907">{{cite book |title=Histoire et Classification des Discomycètes d'Europe |year=1907 |author=Boudier JLÉ. |publisher=Klincksieck |location=Paris, France |page=34 |language=French | url=https://books.google.com/books?id=Q8MUAAAAYAAJ&pg=PA34}}</ref>

<ref name="Buller 1909">{{cite book |title=Researches on Fungi |volume=6 |author=Buller AHR. |year=1958 |location=New York, New York |publisher=Hafner Publishing |pages=323–4 |url=http://biodiversitylibrary.org/page/3641213}}</ref>

<ref name="Cybertruffle">{{cite web |url=http://www.Cybertruffle.org.uk/cgi-bin/master.pl?species=0018724 |title=''Verpa bohemica'' (Krombh.) J. Schröt. |work=Electronic Distribution Maps of Ukrainian Fungi |author=Minter DW, Hayova VP, Minter TJ, Tykhonenko YY. (eds) |accessdate=2011-05-06}}</ref>

<ref name="Dogan 2007">{{cite journal |vauthors=Dogan HH, Ozturk C, Kasik G, Aktas S |year=2007 |title=Macrofungi distribution of Mut province in Turkey |journal=Pakistan Journal of Botany |volume=39 |issue=1 |pages=293–308 |issn=0556-3321}}</ref>

<ref name="Gilman 1916">{{Cite journal |author=Gilman JC. |title=Cabbage yellow and the relation of temperature to its occurrence |journal=Annals of the Missouri Botanical Garden |year=1916 |volume=3 |pages=25–84 (see p. 55) |url=http://biodiversitylibrary.org/page/15990412 |jstor=2990072 |issue=1 |doi=10.2307/2990072}}</ref>

<ref name="Granmo 1982">{{cite journal |vauthors=Granmo A, Skifte O, Nilssen AC |year=1982 |title=''Ptychoverpa bohemica'' Pezizales in Norway and Finland |journal=Karstenia |volume=22 |issue=2 |pages=43–8 |issn=0453-3402}}</ref>

<ref name="Hall 2003">{{cite book |author=Hall IR. |title=Edible and Poisonous Mushrooms of the World |publisher=Timber Press |location=Portland, Oregon |year=2003 |pages= |isbn=0-88192-586-1}}</ref>

<ref name="Healy 2008">{{cite book |vauthors=Healy RA, Huffman DR, Tiffany LH, Knaphaus G |title=Mushrooms and Other Fungi of the Midcontinental United States (Bur Oak Guide) |publisher=University of Iowa Press |location=Iowa City, Iowa |year=2008 |page=295 |isbn=1-58729-627-6}}</ref>

<ref name="Hobbie 2001">{{cite journal|vauthors=Hobbie EA, Weber NS, Trappe JM |year=2001 |title=Mycorrhizal vs saprotrophic status of fungi: the isotopic evidence |journal=New Phytologist |volume=150 |issue= 3|pages=601–10 |doi=10.1046/j.1469-8137.2001.00134.x}}</ref>

<ref name="Horgen 1985">{{cite book |vauthors=Horgen PA, Ammirati JF, Traquair JA |title=Poisonous Mushrooms of the Northern United States and Canada |publisher=University of Minnesota Press |location=Minneapolis, Minnesota |year=1985 |page=337 |isbn=0-8166-1407-5 |url=https://books.google.com/books?id=nhWbsGB7z4cC&pg=RA1-PA337}}</ref>

<ref name="Krivosheina 2008">{{cite journal|title=Macromycete fruit bodies as a habitat for dipterans (Insecta, Diptera) |journal=Entomological Review |year=2008 |author=Krivosheina NP. |volume=88 |issue=7 |pages=778–92 |doi=10.1134/S0013873808070038}}</ref>

<ref name="Krombholz 1828">{{cite journal |author=von Krombholz JV. |title=Böhmische Morchel, ganz offene Morchel. — ''Morchella bohemica''. KRLZ. böhm. Kačenky |journal=Naturgetreue Abbildungen und Beschreibungen der essbaren, schädlichen und verdächtigen Schwämme |issue=1 |year=1831 |pages=3–5 |url=https://books.google.com/books?id=DhYWAAAAYAAJ&pg=RA1-PA3}}</ref>

<ref name="Kuo 2005">{{cite book |author=Kuo M. |title=Morels |publisher=The University of Michigan Press |location=Ann Arbor, Michigan |year=2005 |pages=184–5 |isbn=0-472-03036-1}}</ref>

<ref name="Kuo 2007">{{cite book |author=Kuo M. |title=100 Edible Mushrooms |publisher=The University of Michigan Press |location=Ann Arbor, Michigan |year=2007 |pages= |isbn=0-472-03126-0}}</ref>

<ref name="Lincoff 1977">{{cite book |vauthors=Lincoff G, Mitchel DH |title=Toxic and Hallucinogenic Mushroom Poisoning: a Handbook for Physicians and Mushroom Hunters |publisher=Van Nostrand Reinhold |location=New York, New York |page=171 |year=1977 |isbn=978-0-442-24580-1}}</ref>

<ref name="Maybrier 2011">{{cite book|author1=Maybrier M, Maybrier M.|title=Morel Hunting |year=2011 |publisher=Stackpole Books |location=Mechanicsburg, Pennsylvania |isbn=978-0-8117-0834-0 |page=18 |url=https://books.google.com/books?id=xVZ5Yme9WysC&pg=PA18}}</ref>

<ref name="McKnight 1987">{{cite book |vauthors=McKnight VB, McKnight KH |title=A Field Guide to Mushrooms, North America |publisher=Houghton Mifflin |location=Boston, Massachusetts |year=1987 |page=42 |isbn=0-395-91090-0 |url=https://books.google.com/books?id=kSdA3V7Z9WcC&pg=PA41}}</ref>

<ref name="O'Donnell 1997">{{cite journal |vauthors=O'Donnell K, Cigelnik E, Weber NS, Trappe JM |title=Phylogenetic relationships among ascomycetous truffles and the true and false morels inferred from 18S and 28S ribosomal DNA sequence analysis |journal=Mycologia |year=1997 |volume=89 |issue=1 |pages=48–65 |jstor=3761172 |url=http://www.Cybertruffle.org.uk/cyberliber/59350/0089/001/0048.htm |doi=10.2307/3761172}}</ref>

<ref name="Often 1990">{{cite journal |vauthors=Often A, Torkelsen AE |year=1990 |title=''Ptychoverpa bohemica'' new record found in South Norway |journal=Blyttia |volume=48 |issue=4 |pages=173–5 |issn=0006-5269 |language=Norwegian}}</ref>

<ref name="Orr 1979">{{cite book |vauthors=Orr DB, Orr RT |title=Mushrooms of Western North America |publisher=University of California Press |location=Berkeley, California |year=1979 |page=36 |isbn=0-520-03656-5}}</ref>

<ref name="Pelouch 2008">{{cite book |vauthors=Pelouch M, Pelouch L |title=How to Find Morels |year=2008 |publisher=University of Michigan Press |location=Ann Arbor, Michigan |isbn=978-0-472-03274-7 |page=71 |url=https://books.google.com/books?id=twuu3g4YOicC&pg=PA71}}</ref>

<ref name="RogersMushrooms">{{cite web|author=Phillips R |url=http://www.rogersmushrooms.com/gallery/DisplayBlock~bid~6911.asp |title=''Verpa bohemica'' |publisher=Rogers Plants Ltd |accessdate=2011-05-03}}</ref>

<ref name="Ryman 1978">{{cite journal |title=Swedish Pezizales of spring and early summer |journal=Svensk Botanisk Tidskrift |year=1978 |author=Ryman S. |volume=72 |issue=4 |pages=327–40 |issn=0039-646X |language=Swedish}}</ref>

<ref name="Scarborough 1992">{{cite book |author=Scarborough J. |title=Medical and Biological Terminologies: Classical Origins |year=1992 |publisher=University of Oklahoma Press |location=Norman, Oklahoma |isbn=978-0-8061-3029-3 |page=74 |url=https://books.google.com/books?id=hjwN65nZBE0C&pg=PA74}}</ref>

<ref name="Schalkwijk-Barendsen 1991">{{cite book |author=Schalkwijk-Barendsen HME. |title=Mushrooms of Western Canada |publisher=Lone Pine Publishing |location=Edmonton, Alberta |year=1991 |page=178 |isbn=0-919433-47-2}}</ref>

<ref name="Schroter 1893">{{cite book |title=Kryptogamen-Flora von Schlesien |trans_title=Cryptogamic flora of Silesia |volume=3-2(7) |author=Schröter J. |year=1893 |page=25 |publisher=J.U. Kern's Verlag |location=Breslau, Germany |language=German |url=https://archive.org/stream/kryptogamenflor00cohngoog#page/n39/mode/2up}}</ref>

<ref name="Seaver 1942">{{cite book |title=The North American cup-fungi (Operculates) |edition=Supplement |author=Seaver FJ. |year=1942 |page=244 |publisher=Self published |location=New York, New York |url=http://biodiversitylibrary.org/page/4716363}}</ref>

<ref name="Sept 2006">{{cite book |author=Sept JD. |title=Common Mushrooms of the Northwest: Alaska, Western Canada & the Northwestern United States |publisher=Calypso Publishing |location=Sechelt, British Columbia |year=2006 |page=82 |isbn=0-9739819-0-3}}</ref>

<ref name="Skirgiello 1960">{{cite journal |author=Skirgiello A. |title=Discomycètes de printemps de Bialowieza|trans_title=Spring Discomycetes of Bialowieza |journal=Monographiae Botanicae |volume=10 |issue=2 |year=1960 |pages=3–19 |language=French}}</ref>

<ref name="Skirgiello 1967">{{cite journal |title=Materiały do poznania rozmieszczenia geograficznego grzybów wyższych w Europie. II |trans_title=Contribution to the knowledge of geographical distribution of higher fungi in Europe: II |journal=Acta Mycologica |author=Skirgiello A. |year=1967 |volume=3 |pages=243–9 |language=Polish}}</ref>

<ref name="Skryabina 1975">{{cite journal |author=Skryabina AA. |year=1975 |title=Fructification of some species of edible fungi from the family Helvellaceae in the Slobodsky Roan of the Kirov Oblast |journal=Rastitel'nye Resursy |volume=11 |issue=4 |pages=552–5 |issn=0033-9946}}</ref>

<ref name="Smith 1980">{{cite book |vauthors=Smith AH, Weber NS |title=The Mushroom Hunter's Field Guide |publisher=University of Michigan Press |location=Ann Arbor, Michigan |year=1980 |page=38 |isbn=0-472-85610-3 |url=https://books.google.com/books?id=TYI4f6fqrfkC&pg=PA38}}</ref>

<ref name="Svrcek 1981">{{cite journal |author=Svrček M. |title=Katalog operkulátních diskomycetů (Pezizales) Československa II. (O-W)|trans_title=List of operculate discomycetes Pezizales recorded from Czechoslovakia 2. O-W |journal=Ceska Mykologie |year=1981 |volume=35 |issue=2 |pages=64–89 |language=Czech}}</ref>

<ref name="Tiffany 1998">{{cite journal |vauthors=Tiffany LJ, Knaphaus G, Huffman DM |year=1998 |title=Distribution and ecology of the morels and false morels of Iowa |journal=Journal of the Iowa Academy of Science |volume=105 |issue=1 |pages=1–15 |url=http://md1.csa.com/partners/viewrecord.php?requester=gs&collection=ENV&recid=4378617&q=%22Distribution+and+ecology+of+the+morels+and+false+morels+of+Iowa%22&uid=789989821&setcookie=yes |format=abstract}}</ref>

<ref name="Underwood 1892">{{cite journal |title=North American Helvellales |author=Underwood L. |year=1892 |journal=Minnesota Botanical Studies. Reports of the Survey.  Botanical series II |page=485 |url=http://biodiversitylibrary.org/page/28079872}}</ref>

<ref name="Underwood 1899">{{cite book |title=Moulds, Mildews, and Mushrooms; A Guide to the Systematic Study of the Fungi and Mycetozoa and their Literature |author=Underwood LM. |year=1899 |page=65 |publisher=H. Holt |location=New York, New York |url=http://biodiversitylibrary.org/page/1041738}}</ref>

<ref name="url:GBIF Portal">{{cite web |url=http://data.gbif.org/species/14379356?extent=-27%2B34%2B53%2B74&zoom=3&minMapLong=-27&minMapLat=34&maxMapLong=53&maxMapLat=74&c%910%93.s=20&c%910%93.p=0&c%910%93.o=14379356 |title=Species: ''Ptychoverpa bohemica'' (Krombh.) Boud. 1907 |work=[[Global Biodiversity Information Facility]] |accessdate=2011-06-17}}</ref>

<ref name="urlMycoBank: Verpa bohemica">{{cite web |url=http://www.mycobank.org/MycoTaxo.aspx?Link=T&Rec=481988 |title=''Verpa bohemica'' (Krombh.) J. Schröt. 1893 |publisher=International Mycological Association |work=MycoBank |accessdate=2010-12-27}}</ref>

<ref name="urlVerpa bohemica (MushroomExpert.Com)">{{cite web |author=Kuo M. |url=http://www.mushroomexpert.com/verpa_bohemica.html |title=''Verpa bohemica'' |date=January 2005 |work=MushroomExpert.Com |accessdate=2011-05-03}}</ref>

<ref name="Wani 2010">{{cite journal |vauthors=Wani W, Pala SA, Boda RH, Mir RA |title=Morels in Southern Kashmir Himalya |journal=Journal of Mycology and Plant Pathology |year=2010 |volume=40 |issue=4 |pages=540–6 |issn=0971-9393}}</ref>

}}

==External links==
{{Commons category|Verpa bohemica}}
*{{IndexFungorum|481988}}
*[http://www.svims.ca/council/Morels.htm Morels & False Morels of the Pacific Northwest]
*[https://www.youtube.com/watch?v=GD6h8OVFENE YouTube video on ''Verpa bohemica'']

{{featured article}}
{{Morchellaceae}}
{{taxonbar}}
{{DEFAULTSORT:Verpa Bohemica}}
[[Category:Edible fungi]]
[[Category:Fungi described in 1828]]
[[Category:Fungi of Asia]]
[[Category:Fungi of Europe]]
[[Category:Fungi of North America]]
[[Category:Morchellaceae]]
[[Category:Poisonous fungi]]