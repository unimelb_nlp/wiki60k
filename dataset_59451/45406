{{Infobox military person
|honorific_prefix  =
|name              =Marcel Anatole Hugues
|honorific_suffix  =
|native_name       =
|native_name_lang  =
|image         =
|image_size    =
|alt           =
|caption       =
|birth_date    ={{Birth date|1892|01|05|df=yes}}
|death_date    = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
|birth_place   =[[Belfort]], France
|death_place   =
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =
|birth_name    =
|allegiance    ={{flag|France|23px}}
|branch        =Infantry; aviation
|serviceyears  =1910–1933<br>1939–19xx
|rank          =Lieutenant Colonel
|servicenumber =
|unit          =[[23e Regiment d'Infanterie]]<br>[[172e Regiment d'Infanterie]]<br>[[407e Regiment d'Infanterie]]<br>[[Escadrille 22]]<br>[[Escadrille 77]]<br>[[Escadrille 81]]
|commands      =[[Escadrille 95]]<br>[[Groupe de Chasse II/5]]
|battles       =World War I<br>World War II
|battles_label =
|awards        =[[Légion d'honneur]]<br>[[Médaille militaire]]<br>[[Croix de Guerre]]<br>British [[Military Cross]]
|relations     =
|laterwork     =Returned to service as a fighter group commander in World War II
|signature     =
|website       = <!-- {{URL|example.com}} -->
}}
Lieutenant Colonel '''Marcel Anatole Hugues''' (born 5 January 1892, date of death unknown) was a French [[flying ace]] during World War I. He served before, during, and after the war, as he was a professional soldier. Later, he came out of retirement for World War II service and led Groupe de Chasse II/5 in its opening campaign against the invading Germans.<ref>{{cite book |title= ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' |page= 178&ndash;179 }}</ref>

== Infantry service ==
Hugues volunteered for a three-year enlistment in the French military on 30 September 1910. He spent most of this enlistment in the ''23e Regiment d'Infanterie''; however, on 15 April 1913, he transferred to the 172e Regiment. On 30 September 1913, he re-enlisted for two more years. On 1 April 1915, he was shifted into the 407e Regiment. On 22 September 1915, he was forwarded to the ''[[Groupe d'Aviation]]'' at [[Avord]].<ref name="ReferenceA">{{cite book |title= ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' |page= 178 }}</ref>

During his five years in the infantry, Hugues had been steadily promoted to [[Corporal#France|corporal]] on 25 April 1911, to [[sergeant]] on 28 September 1912, to [[sergent-fourrier]] on 27 February 1913, to Sergent-Major on 2 August 1914, to [[adjutant]] just 24 days later, and finally commissioned as an acting [[sous lieutenant]] on 26 April 1915.<ref name="ReferenceA"/>

== Flying service during World War I ==
Hugues's 1915 Christmas present was confirmation in his commission as an officer. On 10 February 1916, he was assigned to Escadrille MF 22 (the 'MF' denoting the squadron used [[Maurice Farman]] aircraft). On 8 May 1916, he was sent to pilot's training at [[Pau, Pyrénées-Atlantiques|Pau]]. On 17 July 1916, he was awarded his Military Pilot's Brevet, No. 1260. On 23 September 1916, he was assigned to Escadrille N77 ('N' for [[Nieuport]]).<ref name="ReferenceA"/> He scored his first victory with them, on 14 February 1917.<ref name="theaerodrome.com">http://www.theaerodrome.com/aces/france/hugues.php Retrieved on 15 July 2010.</ref> He was forwarded to Escadrille N97 on 18 March 1917. With this unit, he scored twice more; his latter win, on 4 June 1917, was shared with [[Armand Pinsard]].<ref name="theaerodrome.com"/> He was then transferred to Escadrille N81 on 30 April 1917.<ref name="ReferenceB">{{cite book |title= ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' |page= 179 }}</ref>

He became a [[balloon buster]] for his first official win for N81, when he shot down a German observation balloon on 22 July 1917. He then ran off a succession of seven shared wins, teaming with [[Adrien L. J. Leps]], [[Gabriel Guérin]], [[André Herbelin]], and several other French pilots. Hugues ended 1917 with his tenth victory on 23 December.<ref name="theaerodrome.com"/><ref name="ReferenceB"/>

On 7 March 1918, he was appointed to command of Escadrille Spa95 ('Spa' meaning [[Société Pour L'Aviation et ses Dérivés|Spad]]). He would score twice more with this command, on 11 April and 3 May 1918. He ended the war with twelve confirmed and four unconfirmed aerial victories. He had been promoted to lieutenant on 6 July 1917, and was further advanced, to Capitaine, on 28 June 1918.<ref name="ReferenceB"/>

== Post World War I service ==
Hugues kept on soldiering after the war, remaining in service until his retirement in 1933. He returned to his country's defense for World War II, commanding ''Groupe de Chasse II/5''. His group used Curtis [[Hawk 75]]s in their battle against the invading Germans, sending down at least 60 enemy planes for the loss of two French pilots.<ref name="ReferenceB"/>

Hugues finally ended his martial career as a lieutenant colonel. He was raised in rank within the ''Légion d'honneur'' to [[Commandeur]]. In addition to the ''Médaille militaire'', he had been awarded the ''Croix de Guerre'' with eight ''[[palmes]]'' and an [[étoile de vermeil]], and the British Military Cross.<ref name="ReferenceB"/>

== References ==
{{reflist}}

== Bibliography ==
* ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' Norman L. R. Franks, Frank W. Bailey. Grub Street, 1992. ISBN 0-948817-54-2, ISBN 978-0-948817-54-0.

{{DEFAULTSORT:Hugues, Marcel A.}}
[[Category:1892 births]]
[[Category:Year of death missing]]
[[Category:French aviators]]
[[Category:French military personnel of World War I]]
[[Category:French military personnel of World War II]]
[[Category:French flying aces]]