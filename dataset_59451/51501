The '''ICMJE recommendations''' (full title, '''''Recommendations for the Conduct, Reporting, Editing, and Publication of Scholarly Work in Medical Journals''''') are a set of guidelines produced by the International Committee of Medical Journal Editors for standardising the [[ethics]], preparation and formatting of [[manuscript]]s submitted for publication by [[biomedical]] journals.<ref name=icmje-recommendations>{{Citation |author=ICMJE |title=Recommendations for the Conduct, Reporting, Editing, and Publication of Scholarly Work in Medical Journals |url=http://www.icmje.org/icmje-recommendations.pdf |postscript=.|date=December 16, 2014}}</ref> Compliance with the ICMJE Recommendations is required by most leading biomedical journals. As of 2017, over ~3274 journals worldwide followed the Uniform Requirements.<ref name="ICMJEjournallist">International Committee of Medical Journal Editors. Journals that have Requested Inclusion on the List of Publications that follow the ICMJE's Uniform Requirements For Manuscripts Submitted to Biomedical Journals [homepage on the Internet]. Philadelphia: ICMJE; c2005 [updated 27 May 2006; cited 30 May 2006]. Available from: [http://www.icmje.org/journals.html http://www.icmje.org/journals.html]</ref>

The recommendations were formerly called the '''''Uniform Requirements for Manuscripts Submitted to Biomedical Journals''''' (abbreviated '''URMs''' and often shortened to '''''Uniform Requirements''''').

==International Committee of Medical Journal Editors==
The '''International Committee of Medical Journal Editors''' ('''ICMJE''') was originally known as the '''Vancouver Group''', after the location of their first meeting in [[Vancouver, British Columbia]] in [[Canada]]. Current members of the ICMJE are:<ref>{{cite web |url=http://www.icmje.org/about-icmje/faqs/icmje-membership/ |title=ICMJE Membership |publisher=International Committee of Medical Journal Editors |accessdate=5 July 2014}}</ref>
* ''[[Annals of Internal Medicine]]''
* ''[[BMJ]]
* ''[[Canadian Medical Association Journal]]''
* ''[[Chinese Medical Journal]]''
* ''Ethiopian Journal of Health Sciences''
* ''[[Journal of the American Medical Association]]'' (JAMA)
* ''[[Nederlands Tijdschrift voor Geneeskunde]]''
* ''[[New England Journal of Medicine]]''
* ''[[Public Library of Science]]''
* ''Revista Medica de Chile''
* ''[[The Lancet]]''
* ''[[The Medical Journal of Australia]]''
* ''[[The New Zealand Medical Journal]]''
* ''[[Journal of the Norwegian Medical Association|Tidsskrift for Den norske legeforening]]''
* ''[[Colombia Médica]]''
* ''[[Ugeskrift for Læger]]''

==Citation style==
{{Main|Vancouver system}}

The citation style recommended by the ICMJE Recommendations, which is also known as the [[Vancouver system]], is the style used by the [[United States National Library of Medicine]] (NLM), codified in ''[[Citing Medicine]]''.

References are numbered consecutively in order of appearance in the text&nbsp;– they are identified by [[Arabic numerals]] enclosed in [[bracket|parentheses]].

Example of a journal citation:
* Leurs R, Church MK, Taglialatela M. H<sub>1</sub>-antihistamines: inverse agonism, anti-inflammatory actions and cardiac effects. Clin Exp Allergy 2002 Apr;32(4):489-98.

==Manuscripts describing human interventional clinical trials==
URM includes a mandate for manuscripts describing human interventional trials to register a trial in a clinical trial registry (e.g., ClinicalTrials.gov) and to include the trial registration ID in the abstract of the article. The URM also requires that this registration is done prior enrolling the first participant. A study of five high impact factor journals (founders of [[ICMJE]]) showed that only 89% of published articles (articles published during   2010-2011; about trials that completed in 2008) were properly registered prior enrolling the first participant.<ref name=adherence>{{Cite journal | last1 = Huser | first1 = V. | last2 = Cimino | first2 = J. J. | title = Evaluating adherence to the International Committee of Medical Journal Editors' policy of mandatory, timely clinical trial registration | doi = 10.1136/amiajnl-2012-001501 | journal = Journal of the American Medical Informatics Association | year = 2013 | pmid =  23396544| pmc = | volume=20 | issue=e1 | pages=e169-74}}</ref>

==Disclosure of Competing Interests==
The ICMJE also developed a uniform format for disclosure of competing interests in journal articles.<ref>{{Cite journal|vauthors=Drazen JM, Van der Weyden MB, Sahni P, Rosenberg J, Marusic A, Laine C, Kotzin S, Horton R, Hébert PC, Haug C, Godlee F, Frizelle FA, de Leeuw PW, DeAngelis CD |title=Uniform format for disclosure of competing interests in ICMJE journals |journal=The New England Journal of Medicine |volume=361 |issue=19 |pages=1896–7 |date=November 2009 |pmid=19825973 |doi=10.1056/NEJMe0909052}}</ref>

==Grey literature==
The ''Uniform Requirements'' were adapted by the Grey Literature International Steering Committee [[GLISC]] for the production of scientific and technical [[reports]] included in the wider category of [[grey literature]]. These ''GLISC Guidelines for the production of scientific and technical reports'' are translated to French, German, Italian and Spanish and are available on the GLISC website [http://www.glisc.info].

==See also==
* [[IMRAD]]
* [[EASE Guidelines for Authors and Translators of Scientific Articles]]
* [[Scientific misconduct]]

==References==
{{Reflist}}

==External links==
* [http://www.icmje.org/icmje-recommendations.pdf The ''Recommendations for the Conduct, Reporting, Editing, and Publication of Scholarly work in Medical Journals'']
* [http://www.nlm.nih.gov/bsd/uniform_requirements.html National Library of Medicine&nbsp;– ''Uniform Requirements'' sample references]
* [http://www.icmje.org/journals.html ''Journals Following the Uniform Requirements for Manuscripts'']
* [http://www.glisc.info Use of ''Uniform Requirements'' for scientific and technical reports]

{{Use dmy dates|date=September 2010}}

{{DEFAULTSORT:Uniform Requirements For Manuscripts Submitted To Biomedical Journals}}
[[Category:Bibliography]]
[[Category:Style guides for technical and scientific writing]]
[[Category:Medical research]]
[[Category:Scientific documents]]
[[Category:Scientific misconduct]]