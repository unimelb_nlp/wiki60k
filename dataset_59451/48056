{{Infobox architect
| name = Abbie Galvin
| image = 
| caption =
| birth_name = 
| birth_date = 
| birth_place = [[Vancouver|Vancouver, British Columbia]], [[Canada]]
| nationality = [[Australian]]
| alma_mater = 
| spouse = 
| children = 3
| parents = 
| awards = 
| practice = [[BVN Architecture]]
| significant_buildings = 
| significant_projects = The Braggs (2013), Royal North Shore Hospital (2012), Stockland Head Office (2007)
| significant_design = 
| website = 
}}

'''Abbie Galvin''' is a Principal of Australian architecture, urban design and interior design practice BVN. She is a registered Architect in NSW, VIC, ACT and SA and is also a member of the [[Australian Architecture Association]].<ref>BVN Architects 2015, [http://www.bvn.com.au/pages/abbie_galvin.html "''People: Abbie Galvin''"] {{webarchive |url=https://web.archive.org/web/20150228021555/http://www.bvn.com.au/pages/abbie_galvin.html |date=February 28, 2015 }}, ''BVN Architects'', viewed 3 May 2015</ref>

== Early Life and Education ==
Born in [[Vancouver]], Galvin, along with her parents and two younger brothers moved to the inner suburbs of Perth when she was 6 years old. Galvin’s father was an [[Architect]] and her mother was a [[Nurse]].

Growing up Galvin was constantly around a creative environment. ''"I grew up surrounded by Architecture, discussions around Architecture; Architecture books and magazines".'' This upbringing resulted in Galvin having a ''"very visual, aesthetic interest".'' Her childhood home was an old timber framed house built by her father, often with the help of young Galvin and occasionally their neighbours.<ref name="Interview">{{cite interview |last= Galvin|first= Abbie|subject-link= |interviewer= Joel Hiller, Krystal Rawnson, Chelsea Tomasin|title= Abbie Galvin Biography Interview|url= |location= Melbourne |date= May 1, 2015|work= Skype|access-date= May 1, 2015}}</ref>

Galvin attended [[John XXIII College (Perth, Western Australia)|John XXIII College]], a private, co-education, catholic college. She studied traditional subjects at school such as French, Math, Chemistry, Physics, English Literature and Biology; she enjoyed all subjects, in particular Math and Literature.<ref name="Interview" />

Part of Galvin was determined to stray from following in her father’s footsteps. For years she researched different professions; initially thinking she might study Physiotherapy or Chemistry. It wasn’t until the end of her secondary education that she realized that Architecture was a culmination of many things she enjoyed and excelled at. Architecture was a profession that involved  ''"problem solving..."'' whilst creating ''"...beauty at the same time".''<ref name="Interview" />

After completion of high school Galvin commenced a Bachelor of Architecture at the [[University of Western Australia]] in 1988. Galvin learnt from a variety of different educators including Geoffrey London, Simon Anderson, Michael Markham, Charlie Mann, Peter Brew, [[Howard Raggatt]] and [[Ian McDougall (architect)|Ian McDougall]]. Two key influential figures outlined above were Geoffrey London and Simon Anderson they ''"…created an environment of complete experimentation".''<ref name="Interview" /> This environment exposed Galvin to various creative processes that are relative to problem solving aspects of architecture. Galvin graduated from the Bachelor of Architecture with honours in 1992 where she began her first job at Hames Sharley.

==Personal life==
In Galvin’s spare time she enjoys the company of her husband, also an architect, and three children. She enjoys knitting, sewing and weaving. Galvin became interested in weaving after a trip through Scandinavia and has since completed a weaving course under Liz Williamson. Galvin enjoys running; last year she completed a marathon in Uluru and is currently training to complete another one later this year.<ref name="Interview" />

==Career==

===Early career===
After years of working for small architectural practices, Galvin contacted the [[Museum of Sydney]] in regards to an opportunity about developing a public program focusing on architecture. Inspired by the Chicago Open, Galvin organised and coordinated the Sydney Open, which was first presented by Sydney Living Museums in 1997.<ref>Sydney Open, [https://sydneylivingmuseums.com.au/sydneyopen/about "''About the Event''"], ''Sydney Open'', viewed 2 May 2015</ref> Throughout her working career Galvin has been a dedicated contributor to an accreditation panel through the Australian Institute of Architecture, her role consisting of monitoring and accessing university courses. Galvin occasionally lectures and is a regularly a guest critic for students. She has been on the judging panel alongside [[Richard Hassell]], [[Sean Godsell]] and [[Peter Corrigan]] for the inaugural ALVA student architecture competition at the University of Western Australia.<ref>The University Of Western Australia 2010, [http://www.alva.uwa.edu.au/__data/assets/rtf_file/0008/1724966/eALVA-Issue-2---October-2010.rtf "eALVA 2010"], ''The University Of Western Australia 2010'', viewed 4 May 2015</ref>

===BVN===
After contacting [[BVN Architecture]] Galvin immediately joined the team, which lead to her current position as a Principal Architect at the international practice. She works on large-scale commercial projects ranging from hospitals, multi-residential housing, and research buildings. Galvin communicates her designs through narrative proposals to gain a client’s understanding of complex building designs.<ref name="Interview" />

==Influences==
Galvin’s architecture is influenced by numerous sources. This includes early modernists such as [[Le Corbusier]] and [[Atelier 5]]; all seven volumes of Corbusier’s Oeuvre and Atelier 5 publications were kept in the family’s bookshelf and frequented by her father as a point of reference. Furthermore, Galvin finds architects who work in primitive locations influential as there is a focus on vernacular design. She believes architecture should not form a recognisable object for self-promotion; architecture should be subjective to a specific site.
''"Architecture can never do that in isolation, it has to work in tandem. But so often the building is seen as this skin that is independent of what goes on inside it. It's not just about form making. It is definitely about the integrity and authenticity of architecture."''<ref name="Hinchy 2009, P.64">Hinchy, MH 2009,‘RIGHT NOW WHO’,Belle October/November.P.64</ref>

== Notable Projects ==
{| class="wikitable"
|-
! Building/Project !! Location !! Country !! Date
|-
| The Braggs, University of Adelaide || Adelaide, SA || Australia || 2013 
|-
| Royal North Shore Hospital || St Leonards, Sydney, NSW || Australia || 2012
|-
| Monash University Student Housing || Clayton, Melbourne, VIC || Australia || 2011
|-
| HMAS Creswell Griffiths House || Jervis Bay, NSW || Australia || 2011
|-
| Stockland's Head Office || Sydney CBD, NSW || Australia || 2007
|-
| Deutsche Bank || Sydney CBD, NSW || Australia || 2005
|}

===Stockland's Head Office, 2007 ===

Abbie Galvin was the leading architect of Stockland’s new head office. This building is a redevelopment of an existing 1980’s office block; it became Australia’s first rated 6 Star Green Energy Efficient office (Office Interiors v1.1 building).  Winning seven awards in total that included the BPN Environ Sustainability Award for Office Fit Out (2008) and Wirtschafts Woche 2008 Best Office Award (Cologne, Germany).<ref>Green Building Council Australia 2009, [http://www.gbca.org.au/events.asp?eventid=806 "''Australia’s first 6 Star Green Star- Office Interiors v1.1 building in the spotlight''"], ''Green building Council Australia'', viewed 28 April 2015</ref> Key to the design is the centralised eight storey atrium.<ref>BVN Architects 2015, [http://www.bvn.com.au/projects/stockland_head_office.html?OpenDocument&idx=Type&pcat=Workplace&tpl=ext "''Projects: Stockland Head Office''"]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, ''BVN Architects'', viewed 28 April 2015</ref>

Galvin worked closely with the needs of the manager director of Stockland and his executive team; the development company was going through significant change at the time and needed a space to allow maximum work efficiency. The design challenge was working with the eight levels of the existing structure. Galvin and her team suggested the use of voids that would create visual allowances between the staggered levels and structural bays, these voids then creating a sense of altering scales in verticality. The centralised stairs stringers are painted red to symbolise [[Enid Blyton]]’s red slippery-dip in the children’s novel ''The Folk'' and the ''Faraway Tree''.

===Royal North Shore Hospital, 2012===
In 2009 Abbie gained further recognition in the Australian Architecture Institution after being involved in one of the largest projects undertaken by BVN. This was the $1 billion dollar redevelopment of Sydney’s [[Royal North Shore Hospital]], and the redesign of the HMAS Creswell training college located in Jervis Bay.<ref name="Hinchy 2009, P.64"/>

===The Braggs, 2013===
Another notable project Galvin was involved in was the research Institute for Photonic and Advanced Sensing (IPAS)<ref>ArchDaily 2013, [http://www.archdaily.com/362650/the-braggs-bvn-architecture/ "''The Braggs / BVN Architecture''"], ''ArchDaily'', viewed 3 May 2015</ref> building at the [[University of Adelaide]], otherwise known as ‘The Braggs’. The underpinning design idea was to reflect the purpose of the building in the form and planning. For instance, the folded glass façade at each level represents a different wavelength of light.

==Career Achievements==
Galvin has won numerous design competitions at the University of Western Australia including the RAIA Travel Grant for Design Excellence. Galvin’s reputation in architecture has been steadily increasing, she has been featured in the Architecture Bulletin discussing gender equality in the Australian architecture industry. In the article she admits that there must be change in the industry; stating that current industry practices ''‘…make it extremely difficult for women who are attempting to balance a career and children, and it is not until these issues are addressed that we will see higher levels of female representation and retention.’'' (Architecture Bulletin Sep/Oct 2012, p16). In addition, Galvin has contributed in newspaper publications such as ''[[The Australian Financial Review]]'' and ''[[The Sydney Morning Herald]]''.

==Awards==
* 2006:    Deutsche Bank is awarded RAIA National Award Interior Architecture, RAIA NSW Interior Architecture Award
* 2007:    Stockland Head Office is awarded MBA NSW Interior Fit-outs $25m+
* 2008:     Milo Dunphy Award for Sustainable Architecture at the NSW Architecture Awards.
* 2008:    Stockland Head office is awarded RAIA NSW Milo Dunphy Award for Sustainable Architecture, Environ BPN Sustainability Award, IDEA08 Best Commercial Interior, IDEA08 Best Sustainability Interior, Property Council of Australian Office Developments Finalist, Property Council of Australian Sustainable Developments Finalist, RAIA NSW Interior Architecture Commendation, Winner of BOSS Space Awards, Wirtschaftswoche Best Office 2008 International Category
* 2009:    Galvin was nominated by Micky Pinkerton (Suters Architects) for IDEA (Interior Design Excellence Awards) designer of the year 2009<ref>IDEA-awards, [http://www.idea-awards.com.au/2009-round-2/abi-galvin-from-bvn-architecture/ "''Designer of the Year 2009 (individual or practice) – pe''"], ''Interior Design Excellence Awards 2015'', viewed 28 April 2015</ref>
* 2013:    The Braggs building is awarded The Jack McConnell Award for Public Architecture (BVN Media Release 2013, Braggs Wins Top Architecture Award in SA)

== References ==
<references />

== Further reading ==
* {{cite web
 | last = Parlour
 | title = Wikipedia and women in architecture
 | website = Parlour
 | url = http://archiparlour.org/
 | accessdate = 9 May 2015
 | archiveurl = http://archiparlour.org/wikipedia-and-women-in-architecture/
 | archivedate = 25 April 2015}}
* {{cite web
 | last = Rafson
 | first = Sarah
 | title = women. wikipedia. design. #wikiD: ArchiteXX’s Ongoing Campaign to Write Women Architects into One of the Internet’s Most Widely Consulted Databases
 | website = eOculus
 | url = http://main.aiany.org/eOCULUS/newsletter/#/
 | accessdate =9 May 2015
 | archiveurl = http://main.aiany.org/eOCULUS/newsletter/women-wikipedia-design-wikid-architexxs-ongoing-campaign-to-write-women-architects-into-one-of-the-internets-most-widely-consulted-databases/
 | archivedate = 25 March 2015 }}
* [http://www.afr.com/lifestyle/arts-and-entertainment/art/university-of-adelaides-house-of-light-20131107-iyywg ''University of Adelaide’s house of light,'' The Financial Review]
* Australia Architecture, (Vol. 89, Sep:Oct 2000), Tennis in the Round, p.&nbsp;56
* Australia Architecture, (Vol. 94, Nov:Dec 2005), The Future is Now, p.&nbsp;91

{{DEFAULTSORT:Galvin, Abbie}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Australian architects]]
[[Category:Australian women architects]]
[[Category:University of Western Australia alumni]]
[[Category:21st-century architects]]
[[Category:20th-century Australian architects]]
[[Category:Women architects]]
[[Category:Canadian emigrants to Australia]]