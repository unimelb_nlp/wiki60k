{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox school
| name = Cedar College
| image = [[File:Cedar College.jpg|frameless]]
| imagesize =
| logo =
| caption = Cedar College Sports Centre
| location = 215–233 Fosters Road<br />[[Northgate, South Australia]]
| country = Australia
| coordinates =
| principal = Peter Thomson<ref>{{cite web |url=http://www.cedarcollege.sa.edu.au/welcome.htm |title=Principal's Welcome |publisher=Cedar College}}</ref>
| type =
| religion = Christian
| established = 1997
| homepage = [http://www.cedarcollege.sa.edu.au/ www.cedarcollege.sa.edu.au]
}}
'''Cedar College''' is an R-12 [[Mixed-sex education|co-educational]] [[Christian]] [[Private school|school]] situated in the suburb of [[Northgate, South Australia]], 8&nbsp;km north-east of the [[Adelaide, Australia|Adelaide]] CBD.<ref>{{cite web|title=CSA Website|url=http://csa.edu.au/|publisher=Christian Schools Australia}}</ref> Encompassing both Primary School and High School, Cedar College caters for students from Reception (Foundation) through to Year 12.

==School motto and name==
A distinctly Christian school, the Cedar College Crest features the words "Growing in [[Wisdom]]" which reflects the desire of the school to encourage student to not only grow in [[academic]] [[knowledge]], but also to develop the wisdom to use this [[knowledge]] in effective and God-honouring ways. It is derived from Psalm 92v12,<ref>[http://www.biblegateway.com/passage/?search=Psalm%2092:12&version=NIV Psalm 92v12]</ref><ref>{{cite web|title=BibleGateway Website|url=http://www.biblegateway.com|publisher=Bible Gateway}}</ref> which describes the growth of a [[cedrus|cedar tree]], becoming strong, vital and fruit-bearing. In the same way, the name 'Cedar College' was chosen to reflect the purpose of the school.

In 2014, Cedar College launched its new purpose statement: '''"Preparing students for real life'''", which reflects the desire of the school to equip each student for their life ahead, by helping them to '''Discover [[Jesus]], Display Love and Develop Self'''.

Cedar College is an inter-denominational [[Christian]] school and a ministry of [http://cityreach.com.au CityReach Baptist Church (previously Oakden Baptist Church).]<ref>[http://cityreach.com.au CityReach Baptist Church]</ref><ref>{{cite web|title=CityReach Baptist Church Website|url=http://www.cedarcollege.sa.edu.au|publisher=Oakden Baptist Church}}</ref>

==History==
Discussions regarding the establishment of a [[Christian]] school in the inner north-eastern suburbs of [[Adelaide]] first took place in May 1993. After three years of planning and preparation, Cedar College was officially opened on a newly acquired ten [[acre]] property in [[Northgate, South Australia|Northgate]] in 1997. Established initially as a primary school with just 34 students, Cedar College grew rapidly and soon opened the Middle School and Senior School campuses in the years that followed.<ref>{{cite web|title=Cedar College Website|url=http://www.cedarcollege.sa.edu.au|publisher=Cedar College Inc}}</ref>

Cedar College opened a two-court sports stadium named the "'''Cedar College Sports Centre'''" in 2011. The Centre was officially opened by the [[Kate Ellis|Hon. Kate Ellis MP]], and has quickly become an important landmark and community facility in Northgate.

In 2013, the 2-storey '''Cedar College Resource Centre and Administration building''' was officially opened by the [[Christopher Pyne|Hon. Christopher Pyne MP]], Minister for Education. The Resource Centre features a distinctive triple-winged roof line, inspired by the historic [[Vickers Vimy]] aircraft flown by [[Keith Macpherson Smith|Sir Keith]] and [[Ross Macpherson Smith|Ross Smith]] from [[England to Australia flight|England to Australia in 1919]], landing in the nearby Vickers Vimy Reserve. The Resource Centre is the new technology hub of the school, and adjoins the new Science wing, which was opened in 2014.

In 2016, during the school's 20th Anniversary year, Cedar College completed a new '''2-storey Creative Arts Centre'''.  The facility was officially opened by [[David Fawcett|Senator the Hon. David Fawcett MP]] on behalf of Education Minister [[Simon Birmingham|Senator the Hon. Simon Birmingham MP]]. The Creative Arts Centre features three classrooms and seven instrumental practice rooms on the Ground Floor with a Music performance space, recording studios, Drama performance space and theory classroom on the First Floor.

==Departments==
Today, Cedar College is divided into three [[Departmentalization|departments]] – '''Primary School''' (R-6), '''Middle School''' (7–9) and '''Senior School''' (10–12).<ref>{{cite web|title=Private Schools Guide|url=http://www.privateschoolsguide.com/view-user-profile/cedar-college-northgate-sa.html|publisher=Non-Government School Guide}}</ref> The three separate departments enable the school to ensure that students develop academically and socially at an appropriate level, and provides families with a complete educational institution from Reception through to Year 12.

With now around 800 students,<ref>{{cite web|title=My School Website|url=http://www.myschool.edu.au|publisher=Australian Curriculum, Assessment and Reporting Authority (ACARA)}}</ref> Cedar College is continuing to expand its facilities and curriculum, within a strong community environment.

==Alumni and Old Scholars Association==
In 2012, Cedar College launched its official [[Alumni]] and Old Scholars Association "[http://www.cedarcollege.sa.edu.au/old-scholars Branching Out"], to celebrate 5 years of [[graduates]] from the school. Graduates and previous [[scholars]] of Cedar College may submit their current contact details and download the Alumni Newsletter<ref>[http://www.cedarcollege.sa.edu.au/oldscholars.htm www.cedarcollege.sa.edu.au]</ref> via the school Website.

==Service area==
Situated 8 kilometres from the Adelaide CBD,<ref>[http://www.southaustralia.com/regions/adelaide-city.aspx Adelaide CBD]</ref> amongst the growing suburbs of [[Lightsview, South Australia|Lightsview]] and Northgate, Cedar College is in a suitable location for students in the North-Eastern suburbs of [[Adelaide]], [[South Australia]].

==References==
{{reflist}}

==External links==
* [http://www.cedarcollege.sa.edu.au/ Official website]

{{coord| 34.854445  |S|138.634584 |E|format=dms|display=inline,title}}

[[Category:Educational institutions established in 1997]]
[[Category:High schools in South Australia]]
[[Category:Nondenominational Christian schools in Australia]]
[[Category:Primary schools in South Australia]]
[[Category:Private schools in South Australia]]
[[Category:Schools in Adelaide]]