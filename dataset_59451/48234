{{Infobox university
|name            =Government Polytechnic Panaji
|native_name     =सरकारी तंत्रनिकेतन पणजी
|latin_name      =
|image           =[[File:Govt. Polytechnic Panaji Emblem.jpg]]
|image_size      =
|caption         =
|motto           =Knowledge Is Power
|established     =1963
|type            =Public Institution
|endowment       =
|staff           =
|faculty         =100+
|president       =
|principal       =Luis R.Fernandes:<ref>http://dtegoa.gov.in/bte/instprog.htm</ref>
|vice_chancellor =
|dean            =
|head_label      =
|head            =
|students        =1000+
|part time       =
|full time       =
|doctoral        =
|city            =[[Panaji]]
|state           =[[Goa]]
|country         =[[India]]<ref>http://www.icbse.com/colleges/government-polytechnic-panaji/4273/1</ref>
|campus          =Urban, 20 acres
|free_label      =
|free            =
|colors          =
|colours         =
|mascot          =
|affiliations    =Board of Technical Education, Goa
|website         ={{URL|gpp.nic.in}}
}}

'''Government Polytechnic, Panaji''' is a polytechnic in the state of [[Goa]], India. It is also the first polytechnic college of state of Goa. Government Polytechnic, Panaji offers diploma programs in engineering and technology that cater to the changing needs of industry, business and community at large using need based curricular delivered in a dynamic learning environment. [[All India Council for Technical Education]] (AICTE) and Government of Goa<ref>https://www.goa.gov.in/pdf/GovPolytecAltinCharter.pdf</ref> approved full-time programs are offered to candidates selected as per the norms laid down by the Board of Technical Education. The polytechnic also maintains relations with accreditation bodies like All India Council for Technical Education (AICTE)<ref>http://www.aicte-india.org/downloads/ET-15-16/Goa_ET.pdf</ref> and Goa State Directorate Of Technical Education (DTE).

==Campus==
Government Polytechnic, Panaji  campus, situated on [[Altinho, Goa|Altinho]] Hill. The Institute campus spreads over an area of 74,000 square meter, on top of the Altinho hill. There are separate blocks for each department.[[File:GPP Main Building.jpg|thumb|GPP Main Building]]
Yearly total intake capacity of the institution is 360 students. The premises of this institute has good infrastructural facilities like well equipped Laboratories, Computer Center, Conference Hall, Audio-Visual Room, Community Hall, Gymnasium, Basket Ball court, Football ground etc. Accommodation for 300 boys, 150 girls is also available in the campus.
This campus will have the grid connected solar photovoltaic rooftop system to generate 200KW of energy.<ref>http://www.navhindtimes.in/10-sites-identified-for-solar-wind-based-power-generation/</ref>

==Administration==
The Principal is the administrative head of the institute. There are a numerous administrative sections working under the Principal.
The institute has a Public Information Section which carries out the activities under [[Right to Information Act, 2005]]. This section is headed by a Public Information Officer. The Establishment Section is headed by a Deputy Registrar. The Assistant Stores Officer is the head of the Stores and Purchase Section and the Assistant Accounts Officer heads the Accounts Section. All these officials report to the Principal. The Students' Section is headed by the head of Student Section and reports to the Principal as well.

==Academics==
The Academic wing of the Institute is headed by the Principal. There are thirteen (13) academic Departments, each headed by the Head of the Department. A number of Lecturers and other supporting staff are working for each department.All the Diploma Programmes are recognized by the All India Council for Technical Education (AICTE).Every Department has well equipped and well maintained laboratories, with the latest machines, equipment, trainers, gadgets, models etc.<ref>https://www.goa.gov.in/pdf/GovPolytecAltinCharter.pdf</ref>
Most of the Diploma programmes are of 3 years duration spread over 6 semesters. However the Fabrication Technology & Erection Engineering programme is of 4 years duration, spread over 8 semesters and Food Technology programme is of 3½ years duration, spread over 7 semesters.
In all the programems the first two semesters are dedicated to impart to the students the courses like Physics, Chemistry, Mathematics, Applied Mechanics, Engineering drawing and Basic Workshop Practice. The students can opt for the elective courses of their choice at the higher levels. In all the programmes the students are required to undertake and implement a Project at the final semester of their Diploma programme. At the end of every semester, examinations are conducted by the Directorate of Technical Education, Porvorim-Goa and the final Diploma is awarded by them.

==Departments and centres==

{| class="wikitable"
|-
! S.NO. !! COURSES OFFERED<ref>http://dtegoa.gov.in/diploma.html</ref> !! SANCTIONED INTAKE !! DURATION IN YEARS<ref>http://dtegoa.gov.in/diploma.html</ref> !! YEAR OF STARTING<ref>https://www.goa.gov.in/pdf/GovPolytecAltinCharter.pdf</ref>
|-
| 1 || Civil Engineering || 40 || 3 || 1963
|-
| 2 || Mechanical Engineering || 40 || 3 || 1963
|-
| 3 || Electrical Engineering || 30 || 3  || 1963
|-
| 4 || Fabrication Technology & Erection Engineering || 30 || 4  || 1973
|-
| 5 || Electronic Engineering || 40 || 3  || 1976
|-
| 6 || Food Technology || 45 || 3½ || 1976
|-
| 7 || Modern Office Practices || 20 || 3  || 1976
|-
| 8 || Electronics & Instrumentation || 34 || 3  || 1984
|-
| 9 || Architectural Engineering || 20 || 3  || 1996
|-
| 10 || Garment Technology || 30 || 3  || 1996
|-
| 11 || Computer Engineering || 30 || 3 || 2007
|}

==Admission procedure==
Since the year 1997-98 the Government has introduced a Centralized Admission System for the various Diploma courses in all the Government Polytechnics in Goa. The Directorate of Technical Education (D.T.E) is publishing a Common Information Bulletin and Application Form for Admission to the various programmes, immediately after the declaration of S.S.C results.<ref>http://dtegoa.gov.in/cad2016/prosp/dip_prosp2016.pdf</ref><ref>https://www.goa.gov.in/pdf/GovPolytecAltinCharter.pdf</ref>

==Library==
The library has a good number of ISI specifications in its stock catering to the needs of different exams.

The ground floor of the library is used as a reading room; first floor as stacking cum landing section and the second floor for reference section. The library is open from 9.00 am to 5.15 pm and entry
to the library is restricted to bonafide students having valid reading hall tickets.The library has internet facility. The library has recently installed colour C.C.T.V. system where in the library staff can keep a watch on all three floors.

==Hostel==
Government Polytechnic, Panaji, is the foremost Technical Institute in Goa. Students seek admission to this Institute from all over Goa. The Institute has 2 spacious hostels.<ref>https://www.goa.gov.in/pdf/GovPolytecAltinCharter.pdf</ref>
* Boys' Hostel
Government Polytechnic Boys' Hostel is situated in a quiet locality on the Altinho Plateau within 200 metres walking distance from the main Institute campus. It is situated just opposite the Jogger's Park.  The Hostel building consists of a Ground plus two storeys (G+2) and a basement.
* Girls' Hostel
Government Polytechnic Girls' Hostel is situated within the Institute campus at  Altinho, within 75 metres walking distance from the main Institute building. Prior to the construction of the Girls' Hostel, the students were accommodated in two E-Type staff quarters. The Girls' Hostel building was inaugurated in November 1986. It consists of a Ground plus one storey (G+2) and a basement.

==Student life==

===Students Council===
The welfare of the college is being handled by the student union body or wing known as Gymkhana Body. 
The Principal of the Institute is the President of the Gymkhana Committee and one of the senior staff member is the Vice President. The members of the Gymkhana Committee are elected by the Student Body at the beginning of the academic year. The various Secretaries are elected from the Class Representatives.<ref>http://www.heraldgoa.in/Cafe/Young-leaders-step-up/91739.html</ref> The Gymkhana is located in a separate block adjacent to the main playground.
The structure of the Gymkhana Body is shown below:
* President
* Vice-President
* General Secretary
* Cultural Secretary
* Major Games Secretary
* Minor Games Secretary

==Placement==
The Department of Training and Placement was set up in 1975 in this Institute. Campus interviews are  conducted for placement of students in various industries like Larsen & Toubro, Thermex, Godrej & Boyce, Zuari Industries, D-link, Siemens, Macdermott (Dubai) etc.

==Alumni section==
Presently the Department of Fabrication Technology & Erection Engineering has the Alumni Association and the Departments of Civil Engineering and Instrumentation & Control are in the process of forming one in the near future. The other Departments too may not lag behind in forming their own Alumni Associations.

==References==
{{Indian law copyright|https://www.goa.gov.in/pdf/GovPolytecAltinCharter.pdf}}
{{reflist}}

{{coord missing|Goa}}

[[Category:Educational institutions established in 1963]]
[[Category:Universities and colleges in Goa]]
[[Category:1963 establishments in India]]
[[Category:Education in Panaji]]
[[Category:Buildings and structures in Panaji]]