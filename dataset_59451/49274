{{EngvarB|date=October 2014}}
{{Use dmy dates|date=October 2014}}
{{Infobox architect
| name                  = Philip H. Lewis Jr.
| image                 = Philip H. Lewis Jr3.jpg
| image_size            = 
| imagesize             = 
| order                 = 
| caption               = 
| birth_date            = {{Birth date|df=yes|1925|9|4}}
| birth_place           = [[Robinson, Illinois]]
| nationality           = U.S.A.
| alma_mater            = [[Harvard University]] & [[University of Illinois]]
| spouse                = Elizabeth Alice Thompson
| children              = Philip III, Andrew, Lisa
| occupation            = [[Landscape architect]]
}}

'''Philip H. Lewis Jr.''' (Born September 4, 1925) is an ''emeritus'' professor of landscape architecture who promoted the "environmental corridor" concept.<ref>Silbernagel, Janet (2006). "[http://library.wur.nl/ojs/index.php/frontis/article/view/1108/679 Bio-regional patterns and spatial narratives for integrative landscape research and design]", in B. Tress, G. Tres, G. Fry, and P. Opdam (eds.) ''From Landscape Research to Landscape Planning: Aspects of Integration, Education and Application''.</ref> He taught for more than 40 years at the [[University of Illinois]] (1953-1963) and the [[University of Wisconsin&ndash;Madison]] (1964-1994).

== Early years ==
Lewis was born in [[Robinson, Illinois]] (20 miles from his hometown of [[Lawrenceville, Illinois]]), United States, on September 4, 1925.{{citation needed|date=December 2015}} His father, Philip Howard Lewis Sr. (1890-1971), was an attorney who was a county judge and a state's attorney (1924-1930).<ref>"[http://lawrencelore.blogspot.com/2010/10/obituary-of-philip-h-lewis-1973.html Obituary of Philip H. Lewis 1973]". Lawrence Lore, Lawrence County, Illinois Historical Society Blog.</ref> His mother, Florence Sutfin Lewis (1900-1998), was a homemaker who worked at the First National Bank in Lawrenceville, Illinois.{{citation needed|date=December 2015}} He has a younger sister, Gretchen.{{citation needed|date=December 2015}}

== Education ==
Lewis graduated from Lawrenceville Township High School in 1943.{{citation needed|date=December 2015}} He enlisted in the Air Corps upon graduation and completed a six-month training program  at [[Hondo Air Base]] in [[Hondo, Texas]], where he received navigation training.{{citation needed|date=December 2015}} He completed his service with the Air Corps in 1946. He attended the [[University of Illinois at Urbana–Champaign|University of Illinois]] on the [[G.I. bill]], receiving a bachelor's degree in landscape architecture in 1950.{{citation needed|date=December 2015}} Influential figures during his undergraduate training included [[Stanley Hart White]] and Karl B. Lohmann.{{citation needed|date=December 2015}} His education at the University of Illinois culminated in a trip to California to view contemporary landscape architecture projects with his favorite teacher, [[Hideo Sasaki]]. This trip included tours of gardens that had been designed by [[Thomas Dolliver Church]] and [[Garrett Eckbo]]. Following this trip, Lewis secured his first professional job as a landscape architect with the O. E. Goetz Nursery in [[Webster Groves, Missouri]].

After working at the Goetz Nursery, he attended [[Harvard Graduate School of Design]], (1950-1953), where he earned a master's degree in landscape architecture.{{citation needed|date=December 2015}}

Lewis completed a summer internship with the [[National Park Service]] in 1952, where he learned about governmental agency interdisciplinary efforts to maintain the landscapes under their jurisdiction. His thesis focused on the [[Everglades]] Inventory and Development Study.{{citation needed|date=December 2015}}

On June 13, 1953, Lewis married Elizabeth Alice Thompson, a botany student at [[Radcliffe College]].{{citation needed|date=December 2015}} They were married for 58 years until her death on February 1, 2012.<ref>"[http://host.madison.com/news/local/obituaries/lewis-elizabeth-libby-alice-thompson/article_230fc9ec-4f3f-11e1-898c-0019bb2963f4.html Lewis, Elizabeth “Libby” Alice (Thompson)]". ''Wisconsin State Journal'', February 5, 2012. Accessed December 12, 2015.</ref>

Lewis was awarded a Charles Eliot Traveling Fellowship in Landscape Architecture for his work on the Everglades.{{citation needed|date=December 2015}} The fellowship allowed Lewis and his newlywed to travel to Europe for a year-long honeymoon and period of discovery. They viewed the landscapes of England, Scotland, Spain, Italy, Switzerland, Germany, Denmark, Sweden and Norway. These experiences sparked Lewis's interest in museums, communicating with the public, and the creation of spaces for buying local products.{{citation needed|date=December 2015}}

== Planning and teaching career ==
After returning from Europe, Lewis worked for the University of Illinois Bureau of Community Planning from 1953 to 1963.<ref>{{cite web|title=Past and Present Tenure Line Faculty|url=http://www.urban.uiuc.edu/about/history_faculty.html|publisher=Department of Urban & Regional Planning, University of Illinois}}</ref> He taught City Planning Research with visiting professor [[Patrick Horsbrugh]] in the spring of 1957.<ref>{{cite journal|last=Wetmore|first=Louis|title=Editor|journal=Research Digest|date=May 1957|volume=4|issue=1|pages=7–8|url=https://archive.org/stream/quarterlydigesto1719541960univ#page/n249/mode/2up}}</ref> Horsbrugh and Lewis became lifelong friends and colleagues.

Lewis was the director of the Recreation and Open Space Study of Illinois from 1958 to 1961.{{citation needed|date=December 2015}} Here he identified environmental corridors and landscape personalities that were used to guide planning efforts. He was recruited by Wisconsin Governor [[Gaylord Nelson]] to serve as the Director of the State of Wisconsin Recreation Resource, Research and Design, Department of Resource Development, where he served from 1963 to 1965. Environmental corridors were again identified in the Wisconsin State Recreation Plan to help guide planning decisions in the state.<ref>{{cite book|last=Philip|first=Lewis|title=Natural Resources of Wisconsin: The Landscape Resources of wisconsin|date=1964|publisher=Department of Resource Development|pages=130–142|url=http://digital.library.wisc.edu/1711.dl/WI.WIBlueBk1964}}</ref>

Lewis became the founder and director of the Environmental Awareness Center(EAC), part of the School of Natural Resources and the [[University of Wisconsin–Madison College of Agricultural and Life Sciences|College of Agriculture and Life Sciences]].<ref>{{cite web|title=Significant Events in the History of the Department of Landscape Architecture at the University of Wisconsin-Madison |url=http://la.wisc.edu/about-us/significant-events/|publisher=University of Wisconsin, Department of Landscape Architecture}}</ref> Lewis had joint appointments with the Department of Landscape Architecture, Department of Urban & Regional Planning, and the [[University of Wisconsin–Extension]]. Lewis also served as chair of the UW Landscape Architecture Department from 1964 to 1972 and taught undergraduate and graduate design courses. Shortly before his retirement in 1995 he served as the [[Jens Jensen (landscape architect)|Jens Jensen]] Professor of Landscape Architecture.{{citation needed|date=December 2015}}

Lewis was a visiting professor in the School of Design at Harvard University from 1976 to 1977.{{citation needed|date=December 2015}}

Lewis contributed to the origins of geographic information systems technology with his application of maps using transparent overlays for environmental planning.<ref>{{cite book|last=Chrisman|first=Nick|title=Charting the Unknown: How Computer Mapping at Harvard Became GIS|date=2004|publisher=ESRI Press |isbn=1-58948-118-6 |page=4 |url=http://www.gsd.harvard.edu/gis/manual/lcgsa/HarvardBLAD_screen.pdf}}</ref> The [[Environmental Systems Research Institute]] (Esri) presented Lewis with its Lifetime Achievement Award in 2000.<ref>{{cite web|last=Batty|first=Peter|title=Jack Dangermond on GeoDesign|url=http://geothought.blogspot.com/2009/10/jack-dangermond-on-geodesign.html|publisher=Geothought}}</ref>

Lewis, along with [[Ian McHarg]] and Angus Hills, are credited with the development of the natural resource inventory approach and map overlays as a crucial part of the design process.<ref>Carl Steinitz, Paul Parker, and Lawrie Jordan. "Hand-Drawn Overlays: Their History and Prospective Uses", ''Landscape Architecture'' 66 (September 1976): 444-55</ref><ref>Ndubisi, Forster. ''Ecological Planning: A Historical and Comparative Synthesis''. Baltimore: The Johns Hopkins University Press, 2002.</ref><ref>Steiner, Frederick R. ''The Living Landscape: An Ecological Approach to Landscape''. 2nd ed. Island Press, 2008.</ref>

Lewis celebrated his retirement from the University of Wisconsin at a banquet on April 22, 1995.{{citation needed|date=December 2015}}

=== Early studies at Illinois ===
* [[Embarras River (Illinois)|Embarrass]]/Ambraw River Valley Study (1958)<ref>{{cite book|last=Lewis|first=Philip H.|title=The Changing Landscape|date=1957|publisher=Department of Landscape Architecture, University of Illinois|location=Champaign, Illinois}}</ref> - This study inventoried water, wetland, and steep topography resources, noting that these resource patterns could be used to guide regional planning and design.
* Wabash River Valley Study (1958) - Lewis employed an interdisciplinary planning team of graduate students from the University of Illinois, [[Purdue University]], and [[Indiana University]] to take inventorying and design to a larger scale.{{citation needed|date=December 2015}}
* Study of recreation and open space in Illinois, Department of City Planning and Landscape Architecture and the Bureau of Community Planning, University of Illinois (1961)<ref>Philip H.Lewis. ''Study of recreation and open space in Illinois''. Department of City Planning and Landscape Architecture and the Bureau of Community Planning, University of Illinois, 1961.</ref> - This study aimed at identifying environmental corridors and landscape personalities through the inventory and mapping of natural resources.

== Later life ==
After the death of his wife, Lewis lived at Middleton Glen in the new-urban planned community of Middleton Hills.{{citation needed|date=December 2015}} This traditional neighborhood in the City of [[Middleton, Wisconsin]] was developed by his long-time friend [[Marshall Erdman]]. A street in Middleton Hills is named after Lewis.{{citation needed|date=December 2015}} Lewis now resides at Waunakee Manor, [[Waunakee, Wisconsin]].{{citation needed|date=December 2015}}

On September 12, 2013 the [[Dane County, Wisconsin]] Board of Supervisors dedicated a Madison E-Way to Lewis, to be known as the Lewis Nine Springs E-Way. The title recognizes the work of Lewis and his wife, Elizabeth (Libby), who served for 26 years on the Dane County Parks Commission.<ref>Perreth, Katherine. "[http://middletontimes.com/articles/2013/09/25/environmental-icon Environmental Icon]". ''Middleton Times Tribune'', September 25, 2013. Accessed December 12, 2015.</ref>

The Lewis family helped establish the Friends of Dane County Parks Endowment, managed by the Madison Community Foundation.<ref>[https://www.madisoncommunityfoundation.org/sslpage.aspx?pid=200&fid=D6Ye3NMlBaM%3d&fdesc=mqFPSqx%2bxVnq6ahBCMRCu1q7rNhV6gFi4f5ixw804XicjHOJUjiLHBqiPCPzQQUp Madison Community Foundation].</ref> The fund supports activities involving education, interpretation and volunteerism within parks and the promotion of the E-Way concept.

== Awards and recognition ==
* American Society of Lancscape Architects, Honors & Awards. [[American Society of Landscape Architects Medal]], 1987 <ref>American Society of Landscape Architects, Honors & Awards, (1987), http://www.asla.org/IndividualAward.aspx?id=2454</ref>
* Wisconsin Idea Award,Center for Resource Policy Studies, University of Wisconsin - Madison, (1993).
* Distinguished Service Award, Capital Community Citizens, 1997
* [[Esri]] Lifetime Achievement Award, 2000, presented by Jack Dangermond <ref>ArcNews online, esri, (2000).http://www.esri.com/news/arcnews/fall00articles/arcgis-geontwk.html</ref>
* Conservation/Environment Award, Kiwanis Club of Downtown, Madison, WI, 2001
* Bolz Lifetime Stewardship Award, Natural Heritage Land Trust, 2004 <ref>Natural Heritage land trust, Stewardship Award Winners, (2004). http://www.nhlt.org/page.asp?page=awards</ref>
* Ten of the Best Award, 1000 Friends of Wisconsin, 2006 <ref>Landscapes, 1000 Friends of Wisconsin, (2006). http://www.1kfriends.org/1k/wp-content/uploads/2009/11/Fall-2006-Newsletter.pdf</ref>
* First recipient of the Wisconsin Department of Natural Resources/SEWRPC Environmental Corridor Ovation Award 2007<ref>2010 Environmental Corridor Ovation Award, Wisconsin Department of Natural Resources (2010), http://dnr.wi.gov/news/BreakingNews_Print.asp?id=1588,</ref>
* Lifetime Achievement Award, ASLA Wisconsin Chapter, 2009<ref>Life Achievement, Wisconsin American Society of Landscape Architects (2009), http://www.wiasla.com/awards/recognition/life-achievement/,</ref>
* Inaugural Bringing Bioneers to Wisconsin Award, Sustain Dane & the BioPharmaceutical Technology Center Institute (BTCI), 2009 <ref>Medaris, David, They're here, they're sincere, they're Bioneers!, (2009), Isthmus,http://www.isthmus.com/isthmus/article.php?article=27407,</ref>
* Lifetime Achievement Award, Keeping Greater Milwaukee Beautiful, 2013
* Excellence in Teaching Fellowship, Council of Educators in Landscape Architecture, 2014<ref>Awards, Council of Educators in Landscape Architecture (2014), http://www.thecela.org/awards.php</ref>

==References==
{{reflist}}

== External links ==
* [http://images.library.wisc.edu/WI/EFacs/WIBlueBks/BlueBks/WIBlueBk1964/reference/wi.wibluebk1964.i0008.pdf The Landscape Resources of Wisconsin] by Lewis in ''1964 Wisconsin Blue Book''
* [http://uwsp.maps.arcgis.com/apps/OnePane/basicviewer/index.html?appid=5347e18583624a7a88601e67ac188d5e 1964 Landscape Resource Inventory maps created by Lewis]

{{DEFAULTSORT:Lewis, Philip H.}}
[[Category:1925 births]]
[[Category:Living people]]
[[Category:People from Robinson, Illinois]]
[[Category:People from Lawrenceville, Illinois]]
[[Category:People from Dane County, Wisconsin]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:Harvard University alumni]]
[[Category:University of Illinois alumni]]
[[Category:University of Illinois faculty]]
[[Category:University of Wisconsin&ndash;Madison faculty]]