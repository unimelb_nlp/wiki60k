{{good article}}
{{Infobox cricket tour
|                  series_name = Bangladeshi cricket team in Australia in 2003
|                  team2_image = Flag of Bangladesh.svg
|                   team2_name = Bangladesh
|                  team1_image = Flag of Australia.svg
|                   team1_name = Australia
|                    from_date = 27 June 2003
|                      to_date = 6 August 2003
|                team2_captain = [[Khaled Mahmud]]
|                team1_captain = [[Steve Waugh]]
|                  no_of_tests = 2
|              team1_tests_won = 2
|              team2_tests_won = 0
|        team1_tests_most_runs = [[Darren Lehmann]] (287)
|        team2_tests_most_runs = [[Hannan Sarkar]] (166)
|     team1_tests_most_wickets = [[Stuart MacGill]] (17)
|     team2_tests_most_wickets = [[Mashrafe Mortaza]] (4)
|        player_of_test_series = [[Stuart MacGill]] (Australia)
|                   no_of_ODIs = 3
|               team1_ODIs_won = 3
|               team2_ODIs_won = 0
|         team1_ODIs_most_runs = [[Ricky Ponting]] (130)
|         team2_ODIs_most_runs = [[Alok Kapali]] (83)
|      team1_ODIs_most_wickets = [[Ian Harvey]] (5)<br>[[Brad Hogg]] (5)
|      team2_ODIs_most_wickets = [[Mohammad Rafique (cricketer)|Mohammad Rafique]] (3)<br>[[Mashrafe Mortaza]] (3)
|         player_of_ODI_series = [[Ricky Ponting]] (Australia)
}}
The [[Bangladesh national cricket team]] played two [[test cricket|Test match]]es and three [[One Day International]] (ODI) matches on a mid-year tour of Australia in 2003. They were [[captain (cricket)|captained]] by all-rounder [[Khaled Mahmud]]. The Australians were under a split captaincy&mdash;[[Steve Waugh]] in the Tests and [[Ricky Ponting]] in the following ODIs. The series marked the first time a Test match had been played outside an Australian state capital city; with matches played at [[Bundaberg Rum Stadium]] in [[Cairns]], and the newly upgraded [[Marrara Oval]] in [[Darwin, Northern Territory|Darwin]]. 

Australia easily won the two-match Test series. Bangladesh's performances' did not get any better during the ODI series&mdash;failing to score more than 147 in any innings&mdash;as Australia completed a clean-sweep.

==Background==
[[Bangladesh national cricket team|Bangladesh]] had struggled to come to terms with International cricket since becoming the tenth Test team in November 2000. Before their 2003 tour of Australian, Bangladesh had only one Test draw, which came against lowly ranked Zimbabwe, and not a single test win. Their One Day International (ODI) form had also been poor since their historic victory over [[Pakistan national cricket team|Pakistan]] in the [[1999 Cricket World Cup]]. The victory subsequently paved the way for their admission to Test cricket.<ref name="Test Status">{{cite news |last=Ahmed |first=Parveen |date=2000-06-27 |title=Bangladesh celebrates after ICC confirms Test status|url=http://www.independent.co.uk/sport/cricket/bangladesh-celebrates-after-icc-confirms-test-status-713284.html |newspaper=''The Independent'' |location=London |accessdate=2009-09-09}}{{dead link|date=December 2015}}</ref> In Bangladesh's last series before Australia, they were defeated by an innings in both matches by [[South Africa national cricket team|South Africa]]. Australia, however, had a 3&ndash;1 away Test victory&mdash;starting in April&mdash;against the [[West Indies cricket team|West Indies]], and a 4&ndash;3 ODI victory over the same opposition.<ref name="Haigh668&669">Haigh (2004), p. 668&ndash;669.</ref> Earlier in the year, they remained unbeaten (10 matches) in winning the [[2003 Cricket World Cup|2003 ODI Cricket World Cup]] in South Africa. Led by [[Ricky Ponting]], Australia defeated [[India national cricket team|India]] in the final, despite losing [[leg-spin]]er [[Shane Warne]]. Prior to the start of the World Cup, Warne—Australia's highest Test wicket-taker—was sent home from South Africa after a [[drug test]] during the [[Australian Tri-Series|recent one-day series in Australia]] returned a positive result for a banned [[diuretic]].<ref name="Haigh668&669"/> He was consequently banned from International and [[first-class cricket]] for a year.<ref name="Warne">{{cite news|url=http://content-uk.cricinfo.com/australia/content/story/128796.html|title=Ban will lengthen career, says Warne|publisher=Cricinfo |date=2009-04-02 |accessdate=2009-09-09}}</ref>

The day before the first Test, former Australian batsman [[David Hookes]] said Bangladesh were not worthy of Test status. He also indicated that Australia could win either Test within a day.<ref name="Haigh670">Haigh (2004), p. 670.</ref>

==Test series==
===First Test===
[[Image:SRWaugh.png|thumb|right|Australian captain [[Steve Waugh]] (pictured) scored his 31st Test century in the match. |alt=Person aged around 30 wearing a [[baggy green]] cap with the Australian coat of arms, Australian blazer, green with yellow stripes, and a cream cricket shirt. He is clean shaven and has brown hair.]]
[[Marrara Oval]] became the 89th Test venue&mdash;and the eighth in Australia. On a slow and low drop-in pitch airlifted from Melbourne a month before the match, Australia won the toss and elected to field.<ref name="Test Wisden 1">{{cite news|url=http://www.cricinfo.com/wisdenalmanack/content/story/214660.html|title=Australia v Bangladesh: First Test |publisher=Wisden Almanack|last=Lynch |first=Steven |year=2004 |accessdate=2009-09-07}}</ref> Bangladesh collapsed and were bowled out for 97 within three hours, with only Mohammad Ashraful (23) and Khaled Mahmud (21) providing resistance.<ref name="Haigh670"/>  All four Australian bowlers chimed in, as [[Glenn McGrath]] and [[Brett Lee]] took three wickets apiece. McGrath and [[Jason Gillespie]] surpassed the duo of [[Keith Miller]] and [[Ray Lindwall]] as Australia's most successful opening-bowling combination.<ref name="Test Wisden 1"/><!-- Not covered in this ref -->{{citation needed|date=September 2009}} Bangladesh's shot selection did not impress coach [[Dav Whatmore]] who said, "A few players presented their wickets and that's just the area we're trying to improve."<ref>{{cite news|url=http://www.cricinfo.com/australia/content/story/128939.html|title=Bangladesh no match for Australian fire |publisher=Cricinfo|last=McConnell|first=Lynn |date=2009-08-20 |accessdate=2009-09-07}}</ref> Australia initially struggled to come to terms with the pitch, falling to 2/43 when [[Ricky Ponting]] was dismissed for 10. [[Darren Lehmann]] started the carnage, scoring his second Test century (110). [[Steve Waugh]] then became only the second player after [[Gary Kirsten]] to score a century against all Test playing nations. Upon reaching the milestone, he promptly declared Australia's innings on 7/407. Only Bangladesh's fastest bowler, [[Mashrafe Mortaza]] proved threatening, taking three wickets for 74 runs (3/74) in 23 overs.<ref name="Bulletin 2">{{cite news|url=http://www.cricinfo.com/australia/content/story/128962.html|title=Hundreds for Waugh and Lehmann as Australia take charge |publisher=Cricinfo|last=McConnell|first=Lynn |date=2009-08-20 |accessdate=2009-09-07}}</ref> After the days play, Lehmann said, "It was hard work out there. They stuck to their guns pretty well, they put it in the right areas and made it tough to score runs." When asked about becoming only the second batsman to score a hundred against all nine Test-playing nations, Waugh said: 

{{quote|If you play long enough you are going to reach milestones and records are going to be passed and I'm sure someone down the track will beat those. But it is nice to achieve things and to score a hundred against every country is something I'm proud of. I'm not too concerned about records. I just want to go out there and play well and I've said if I don't think I can improve then I shouldn't be there.<ref name="Tough Times">{{cite news|url=http://www.cricinfo.com/australia/content/story/128967.html|title='Nice to overcome those tough times': Waugh |publisher=Cricinfo|date=2009-08-19 |accessdate=2009-09-07}}</ref>}}

The Bangladeshis&mdash;needing 310 runs to make Australia bat again&mdash;counterattacked before the conclusion of day two. Despite losing [[Javed Omar]], leg before wicket to McGrath for five, opening batsman [[Hannan Sarkar]] and their leading International run-scorer, [[Habibul Bashar]] took the score to 89. The pair scored at a run-rate of 3.98 runs per over, before the partnership was ended when Sarkar was caught behind for 35.<ref name="Bulletin 2"/> This sparked a collapse, with Bangladesh bowled out for 178. Australian [[leg spin]]ner [[Stuart MacGill]] took 5/65&mdash;his seventh Test five-wicket haul&mdash;as Australia won by an innings and 132 runs. Along with being named man of the match, Waugh overtook West Indian [[Clive Lloyd]] as the most successful captain in Test history, enjoying his 37th victory.<ref name="Test Wisden 1"/><ref name="Bulletin 3">{{cite news|url=http://www.cricinfo.com/australia/content/story/128974.html|title=Australia complete an innings victory|publisher=Cricinfo|last=McConnell |first=Lynn |date=2009-08-20 |accessdate=2009-09-07}}</ref>

{{Test match |
 date = 18 July - 20 July |
 team1 = {{cr-rt|BAN}}  |
 team2 = {{cr|AUS}} |

 score-team1-inns1 = 97 (42.2 overs) |
 runs-team1-inns1 = [[Mohammad Ashraful]] 23 (52) |
 wickets-team1-inns1 = [[Glenn McGrath]] 3/20 (13 overs) |

 score-team2-inns1 = 407/7d (117.5 overs) |
 runs-team2-inns1 = [[Darren Lehmann]] 110 (221) |
 wickets-team2-inns1 = [[Mashrafe Mortaza]] 3/74 (23 overs) |

 score-team1-inns2 = 178 (51.1 overs) |
 runs-team1-inns2 = [[Habibul Bashar]] 54 (91) |
 wickets-team1-inns2 = [[Stuart MacGill]] 5/65 (13.1 overs) |

 result = Australia won an innings and 132 runs |
 venue = [[Marrara Oval]], [[Darwin, Northern Territory|Darwin]], [[Australia]], '''Att:''' 13,862| 
 umpires = [[Rudi Koertzen]] and [[David Shepherd (umpire)|David Shepherd]] |
 motm = [[Steve Waugh]] |
 report = [http://www.cricinfo.com/db/ARCHIVE/2003/BDESH_IN_AUS/SCORECARDS/BDESH_AUS_T1_18-22JUL2003.html Scorecard] |
 rain = 
}}

===Second Test===
[[File:STUART MACGILL.jpg|thumb|right|[[Stuart MacGill]] (pictured) was named Man of the Test Series with 17 wickets.]]

[[Bundaberg Rum Stadium]] became the 90th Test venue&mdash;and the ninth in Australia. According to Wisden, " ... Rain had left question marks about the quality of the pitch, which looked green and enticing for the Australian fast bowlers." When Australia won the toss and sent Bangladesh into bat, there were fears Bangladesh would struggle to score 100 in either innings. On a surface which "played much better than expected," and was a "much faster pitch than that in Darwin," Sarkar scored 76, hitting nine boundaries in the process.<ref name="Test Wisden 2">{{cite news|url=http://www.cricinfo.com/wisdenalmanack/content/story/214661.html|title=Australia v Bangladesh: Second Test |publisher=Wisden Almanack|last=Lynch |first=Steven |year=2004 |accessdate=2009-09-07}}</ref> He and Bashar put on 108 for second wicket as the Australian bowlers lacked penetration. Omar (26), Bashar (46), [[Sanwar Hossain]] (46) and [[Khaled Mashud]] (44) all got starts, but could not continue their good work as Bangladesh were eventually bowled out for 295 early on the second morning.<ref>{{cite news|url=http://www.cricinfo.com/australia/content/story/129080.html|title=Bangladesh build respectable total at Cairns|publisher=Cricinfo|last=McConnell|first=Lynn |date=2003-07-25 |accessdate=2009-09-07}}</ref> MacGill took another five-wicket haul as the rest of the bowlers apart from Gillespie had limited success, with Waugh bowling himself for five overs in search of a breakthrough. Despite Langer (1) falling early, Hayden (50) and Ponting (59) both scored half-centuries, taking Australia to 2/105. Both Lehmann and Waugh continued with their Darwin form, scoring 177 and 156 respectively. This was Waugh's 32nd Test century, taking him ahead of [[Sachin Tendulkar]] and behind only [[Sunil Gavaskar]] (34). Lehmann's innings included 105 runs between tea and the close of play on day two. When eventually dismissed, [[Martin Love]] compiled his first Test century. With 2/128, Hossain was the only bowler who took more than one wicket. His [[Throwing (cricket)|suspect bowling action]] would later be reported to the [[International Cricket Council]].<ref name="Test Wisden 2"/><ref name="Bulletin Test 2 Day 2">{{cite news|url=http://www.cricinfo.com/australia/content/story/129095.html|title=Lehmann flourishes after Bangladesh dismissed for 295|publisher=Cricinfo|last=McConnell|first=Lynn|date=2003-07-26 |accessdate=2009-09-07}}</ref><ref name="Bulletin Test 2 Day 3">{{cite news|url=http://www.cricinfo.com/australia/content/story/129112.html|title=All's fair for Love and Waugh as Bangladesh struggle|publisher=Cricinfo|last=McConnell|first=Lynn|date=2003-07-26 |accessdate=2009-09-07}}</ref>  

Sarkar scored another half-century in Bangladesh's second innings before succumbing to a 'wild swipe' off MacGill. The leg-spinner took his third five-wicket haul in as many innings and Gillespie took three wickets in the space of eight balls.<ref name="Test Wisden 2"/> Despite being dismissed for 163, Waugh thought Bangladesh's batting was "a lot better than a lot of efforts by the West Indies in recent years and Pakistan in Sharjah." Despite two centuries from Lehmann and Waugh, MacGill was named man of the series, with 17 wickets.<ref name="Bulletin Test 2 Day 3"/> Whatmore was also pleased with the Bangladesh improvement between Tests. 

{{quote|Well we stretched the game out to a day longer than we did in Darwin. I thought there was definite improvement. To fight back and get 295 in the first innings, I thought was excellent. Playing against that quality of opposition is not easy. Maybe against other opposition in future it might be just that little bit easier and we can progress. Most people were talking about the pace battery of the Australian team but the person who won man of the series and got the most wickets was the spinner [Stuart MacGill]. I think maybe we didn't apply the same amount of effort against the slower bowler as we did against the quicks."<ref name="well on their way">{{cite news|url=http://www.cricinfo.com/bangladesh/content/story/129136.html|title='Bangladesh are well on their way': Waugh|publisher=Cricinfo|date=2003-07-28 |accessdate=2009-09-07}}</ref>}}

{{Test match |
 date = 25 July - 28 July |
 team1 = {{cr-rt|BAN}}  |
 team2 = {{cr|AUS}} |

 score-team1-inns1 = 295 (92.1 overs) |
 runs-team1-inns1 = [[Hannan Sarkar]] 76 (136) |
 wickets-team1-inns1 = [[Stuart MacGill]] 5/77 (24 overs) |

 score-team2-inns1 = 556/4d (139.2 overs) |
 runs-team2-inns1 = [[Darren Lehmann]] 177 (207) |
 wickets-team2-inns1 = [[Sanwar Hossain]] 2/128 (30 overs) |

 score-team1-inns2 = 163 (58.4 overs) |
 runs-team1-inns2 = [[Hannan Sarkar]] 55 (104) |
 wickets-team1-inns2 = [[Stuart MacGill]] 5/56 (20 overs) |

 result = Australia won an innings and 98 runs |
 venue = [[Bundaberg Rum Stadium]], [[Cairns]], [[Australia]], '''Att:''' 13,279| 
 umpires = [[Rudi Koertzen]] and [[David Shepherd (umpire)|David Shepherd]] |
 motm = [[Stuart MacGill]] |
 report = [http://www.cricinfo.com/db/ARCHIVE/2003/BDESH_IN_AUS/SCORECARDS/BDESH_AUS_T2_25-29JUL2003.html Scorecard] |
 rain = 
}}

==Test squads==
{| class="wikitable" style="text-align:center; width:60%; margin:auto"
|-
! {{cr-rt|BAN}}<ref name="BAN-test-squad">{{cite web |title=Squads - Bangladesh Test Squad |url=http://cricxpert.com/bangladesh-test-squad-for-tour-of-australia-2003 }}</ref>
! {{cr|AUS}}<ref name="AUS-test-squad">{{cite web |title=Squads - Australia Test Squad |url= http://cricxpert.com/australia-test-squad-vs-bangladesh-test-series-2003}} </ref>
|-
| [[Khaled Mahmud]] ([[Captain (cricket)|c]])
| [[Steve Waugh]] ([[Captain (cricket)|c]])
|- 
| [[Al Sahariar]]
| [[Andy Bichel]]
|-
| [[Alok Kapali]]
| [[Adam Gilchrist]] ([[Wicket-keeper|wk]])
|-
| [[Anwar Hossain Monir]] 
| [[Jason Gillespie]]
|-
| [[Habibul Bashar]]
| [[Matthew Hayden]]
|-
| [[Hannan Sarkar]]
| [[Brad Hogg]]
|-
| [[Javed Omar]]
| [[Justin Langer]]
|-
| [[Khaled Mashud]] ([[Wicket-keeper|wk]])
| [[Brett Lee]]
|-
| [[Mohammad Manjural Islam|Manjural Islam]]
| [[Darren Lehmann]]
|-
| [[Mashrafe Mortaza]]
| [[Martin Love]]
|- 
| [[Mohammad Ashraful]]
| [[Stuart MacGill]]
|-
| [[Mohammad Rafique (cricketer)|Mohammad Rafique]] 
| [[Glenn McGrath]]
|-
| [[Sanwar Hossain]]
| [[Ricky Ponting]] 
|-
| [[Tapash Baisya]]
|-
| [[Tareq Aziz]]
|}

==One Day series==
===1st ODI===
After putting Bangladesh into bat, the Australians soon took early wickets. On a pitch that was quicker than the Test strip, Bangladesh looked all at sea against the pace of Lee and Gillespie. They soon crashed to 4/19 and never recovered, eventually bowled out for 105 in the 34th over. Only three batsmen managed double figures as the pace duo took seven wickets between them. [[Andy Bichel]] also chimed in with 2/24. 

Because of Bangladesh's meagre batting performance, the Australian innings began before lunch and opener [[Adam Gilchrist]] scored a typically quick-fire 18 before he was caught behind from Mortaza. Hayden (46) and Ponting then got valuable batting practice as they shared a 74-run partnership. Ponting was eventually bowled by Rafique for 29. Damien Martyn's stay at the crease was cut short, in his first International match since breaking his finger in the [[2003 Cricket World Cup]], he faced just one ball before Australia won by eight wickets with more than 27 overs to spare. Along with the wicket of Ponting, Rafique conceded just seven runs in five overs.<ref name="ODI Bulletin 1">{{cite news|url=http://www.cricinfo.com/australia/content/story/124676.html|title=Australian pace attack too hot for Bangladesh|publisher=Cricinfo|last=McConnell|first=Lynn|date=2003-08-02 |accessdate=2009-09-07}}</ref><ref name="ODI Wisden 1">{{cite news|url=http://www.cricinfo.com/wisdenalmanack/content/story/214856.html|title=Australia v Bangladesh: First One-Day International |publisher=Wisden Almanack|last=Lynch |first=Steven |date=2003-08-02 |accessdate=2009-09-07}}</ref><ref name="ODI Partnership 1">{{cite web|url=http://www.cricinfo.com/ci/engine/current/match/64830.html?innings=1;view=fow|title=Bangladesh in Australia ODI Series - 1st ODI: Fall of wickets and partnerships |publisher=Cricinfo|date=2003-08-02 |accessdate=2009-09-07}}</ref>

{{Limited overs international
  | date = 2 August
  | team1 = {{cr-rt|BAN}} 
  | score1 = 105 (34 overs)  |
  | score2 = 107/2 (22.3 overs)  |
  | team2 = {{cr|AUS}}
  | runs1 = [[Tushar Imran]] 28 (33)  |
  | wickets1 = [[Brett Lee]] 4/25 (8 overs)  |
  | runs2 = [[Matthew Hayden]] 46* (58)  |
  | wickets2 = [[Mohammad Rafique (cricketer)|Mohammad Rafique]] 1/7 (5 overs)  |
  | result = Australia won by 8 wickets |
  | report = [http://www.cricinfo.com/db/ARCHIVE/2003/BDESH_IN_AUS/SCORECARDS/BDESH_AUS_ODI1_02AUG2003.html Scorecard] |
  | venue = [[Bundaberg Rum Stadium]], [[Cairns]], [[Australia]]|
  | umpires = [[David Shepherd (umpire)|David Shepherd]] & [[Simon Taufel]] |
  | motm = [[Brett Lee]]  |
  | rain = Australia won the toss and elected to field.
}}

===2nd ODI===
[[File:Damien martyn.jpg|thumb|150px|right|Despite returning in the series for the first time since missing the [[2003 Cricket World Cup]] because of a broken finger, [[Damien Martyn]] (pictured) scored 91.]]
The Bangladeshis won the toss and elected to bat. Their openers put on a steady opening partnership, before Sarkar (who was now playing as a wicket-keeper in the absence of Khaled Mashud) was caught behind off [[Ian Harvey]] for 19, with the score at 37 after 14 overs. His opening partner, Javed Omar, fell nine runs later, for 11. Hossain also came and went quickly, as Bangladesh were restricted to 3/52, despite surviving Lee's spell. Only Bashar (31) offered any sort of resistance, as the middle order crumbled under the spin of [[Brad Hogg]], who took 3/31. [[Alok Kapali]] provided some lower-order entertainment, striking 34 from 44 balls. Lehmann finished off the tail, collecting 3/16, as Bangladesh were dismissed for 147 in the 46th over. 

In the absence of Bangladesh's opening bowler, Mortaza, Australia opened the innings with [[Andrew Symonds]] and [[Michael Bevan]]— giving the usual middle-order batsmen time at the crease. The Symonds experiment did not last long, as he was dismissed by [[Hasibul Hossain]] for seven. Martyn joined Bevan at the crease and quickly put the bowlers on the back foot with aggressive batting. Despite returning in the series for the first time since the [[2003 Cricket World Cup]] because of a broken finger, he scored Australia's second fastest ODI half-century&mdash;22 balls&mdash;and was on track to overtake Gilchrist's and [[Allan Border]]'s record for the fastest Australian ODI century; 78 deliveries. Together they added 131 runs from 91 balls, seeing Australia home by nine wickets. Named man of the match, Martyn finished with 92 from 51 balls, dominating the partnership with Bevan, who scored 40 from 62 balls. Martyn was especially severe on Bangladeshi captain [[Khaled Mahmud]], hitting three fours and a six from successive balls in Mahmud's second over. The captain finished with the unflattering figures of 0/34 from three overs.<ref name="ODI Bulletin 2">{{cite news|url=http://www.cricinfo.com/australia/content/story/124687.html|title=Martyn pulverises ineffectual Bangladesh|publisher=Cricinfo|last=McConnell|first=Lynn|date=2003-08-03 |accessdate=2009-09-07}}</ref><ref name="ODI Wisden 2">{{cite news|url=http://www.cricinfo.com/wisdenalmanack/content/story/214857.html|title=Australia v Bangladesh: Second One-Day International |publisher=Wisden Almanack|last=Lynch |first=Steven |date=2003-08-03 |accessdate=2009-09-07}}</ref><ref name="ODI Partnership 2">{{cite web|url=http://www.cricinfo.com/ci/engine/match/64831.html?innings=1;view=fow|title=Bangladesh in Australia ODI Series - 2nd ODI: Fall of wickets and partnerships |publisher=Cricinfo|date=2003-08-03 |accessdate=2009-09-07}}</ref>

{{Limited overs international
  | date = 3 August
  | team1 = {{cr-rt|BAN}} 
  | score1 = 147 (45.1 overs)  |
  | score2 = 148/1 (20.2 overs)  |
  | team2 = {{cr|AUS}}
  | runs1 = [[Alok Kapali]] 34 (44)  |
  | wickets1 = [[Darren Lehmann]] 3/16 (4.1 overs)  |
  | runs2 = [[Damien Martyn]] 92* (51)  |
  | wickets2 = [[Hasibul Hossain]] 1/37 (6 overs)  |
  | result = Australia won by 9 wickets |
  | report = [http://www.cricinfo.com/db/ARCHIVE/2003/BDESH_IN_AUS/SCORECARDS/BDESH_AUS_ODI2_03AUG2003.html Scorecard] |
  | venue = [[Bundaberg Rum Stadium]], [[Cairns]], [[Australia]]|
  | umpires = [[David Shepherd (umpire)|David Shepherd]] & [[Steve Davis (umpire)|Steve Davis]] |
  | motm = [[Damien Martyn]]  |
  | rain = Bangladesh won the toss and elected to bat.
}}

===3rd ODI===
[[Image:Ricky Ponting.jpg|thumb|150px|right|Australian ODI captain [[Ricky Ponting]] (pictured), struck his 12th International century in 12 months.|alt=A man in a predominately yellow cricket with: a helmet, gloves, pads and a bat. He is swinging the bat as the crowd watches in the background.]]

The series finished in Darwin, with Australian skipper Ricky Ponting winning the toss and electing to bat. They were restricted to 4/114&mdash;partly due to the tight bowling of Rafique who picked up two wickets&mdash;before the Australian captain stabilised the innings. He scored a composed century, as he and Bevan put on a run-a-ball 127 run stand. Strangely, Ponting's 14th ODI century, included only two fours, despite hitting four sixes. Australia finished on 7/254 from their 50 overs. On return, Mortaza took two late innings wickets; however, the rest of the bowling (apart from Rafique and Kapali) again lacked penetration. 

Thanks to some tight bowling, Bangladesh slumped to 5/36. Both Bashar (2) and [[Mohammad Ashraful]] (4) were dismissed charging Bichel and Harvey respectively, before Kapali compiled a determined 49 in a 66 run sixth-wicket partnership with Hossain. These shots were possibly caused by tight bowling from Jason Gillespie, who took 1/16 in 10 overs. Bangladesh were bowled out for 142 in the 48th over, with [[Ian Harvey]] taking four for 16. Ponting received the man of the match and series awards, as Australia won by 112 runs.<ref name="ODI Bulletin 3">{{cite news|url=http://www.cricinfo.com/australia/content/story/124755.html|title=Australia overwhelm Bangladesh ... again|publisher=Cricinfo|last=McConnell|first=Lynn|date=2003-08-06 |accessdate=2009-09-07}}</ref><ref name="ODI Wisden 3">{{cite news|url=http://www.cricinfo.com/wisdenalmanack/content/story/214858.html|title=Australia v Bangladesh: Third One-Day International |publisher=Wisden Almanack|last=Lynch |first=Steven |date=2003-08-06 |accessdate=2009-09-07}}</ref><ref name="ODI Partnership 3">{{cite web|url=http://www.cricinfo.com/ci/engine/current/match/64832.html?innings=1;view=fowl|title=Bangladesh in Australia ODI Series - 3rd ODI: Fall of wickets and partnerships |publisher=Cricinfo|date=2003-08-06 |accessdate=2009-09-07}}</ref> According to cricket historian [[Gideon Haigh]], "Almost a quarter of the combined populations of Cairns and Darwin attended the cricket" during the Test and ODI series.<ref name="Haigh670"/>

{{Limited overs international
  | date = 6 August
  | team1 = {{cr-rt|AUS}} 
  | score1 = 254/7 (50 overs)  |
  | score2 = 142 (47.3 overs)  |
  | team2 = {{cr|BAN}}
  | runs1 = [[Ricky Ponting]] 101 (118)  |
  | wickets1 = [[Mohammad Rafique (cricketer)|Mohammad Rafique]] 2/31 (10 overs)  |
  | runs2 = [[Alok Kapali]] 49 (64)  |
  | wickets2 = [[Ian Harvey]] 4/16 (6.3 overs)  |
  | result = Australia won by 112 runs |
  | report = [http://www.cricinfo.com/db/ARCHIVE/2003/BDESH_IN_AUS/SCORECARDS/BDESH_AUS_ODI3_06AUG2003.html Scorecard] |
  | venue = [[Marrara Oval]], [[Darwin, Northern Territory|Darwin]], [[Australia]]|
  | umpires = [[David Shepherd (umpire)|David Shepherd]] & [[Simon Taufel]] |
  | motm = [[Ricky Ponting]] |
  | rain = Australia won the toss and elected to bat.
}}

==Aftermath==
[[Image:Australia vs India.jpg|thumb|right|Match one of the VB series. Australia playing India at the [[Melbourne Cricket Ground]] (MCG)]]
Australia's next assignment was a [[Zimbabwean cricket team in Australia in 2003–04|home series against Zimbabwe]] in October 2003. Australian opener [[Matthew Hayden]] scored the then highest Test score of 380 in the First Test at the [[Western Australia Cricket Association Ground]] (WACA). Australia swept the series 2–0, after comfortably winning the Second Test at the [[Sydney Cricket Ground]] (SCG) in [[Sydney]].<ref name="Haigh671">Haigh (2004), p. 671.</ref> They then drew a [[Indian cricket team in Australia in 2003–04|four-Test series]] at home against India in December and January. The Fourth Test was captain Steve Waugh's last, and it was played at his home ground the SCG. Australia won the following [[2003–04 VB Series|VB one-day series]] that included India and Zimbabwe.

==One Day International squads==
{| class="wikitable" style="text-align:center; width:60%; margin:auto"
|-
! {{cr-rt|BAN}}<ref name="BAN-ODI-squad">{{cite web|title=Squads - Bangladesh ODI Squad |url=http://pakistancrickethighlights.info/bangladesh-odi-squad-for-tour-of-australia-2003-aus-vs-ban |deadurl=yes |archiveurl=https://web.archive.org/web/20150708134326/http://pakistancrickethighlights.info/bangladesh-odi-squad-for-tour-of-australia-2003-aus-vs-ban |archivedate=2015-07-08 |df= }}</ref>
! {{cr|AUS}}<ref name="AUS-ODI-squad">{{cite web|title=Squads - Australia ODI Squad |url=http://pakistancrickethighlights.info/bangladesh-vs-australia-2003-australia-odi-squad |deadurl=yes |archiveurl=https://web.archive.org/web/20150708192441/http://pakistancrickethighlights.info/bangladesh-vs-australia-2003-australia-odi-squad |archivedate=2015-07-08 |df= }} </ref>
|-
| [[Khaled Mahmud]] ([[Captain (cricket)|c]])
| [[Ricky Ponting]] ([[Captain (cricket)|c]])
|- 
| [[Al Sahariar]]
| [[Michael Bevan]]
|-
| [[Alok Kapali]]
| [[Andy Bichel]] 
|-
| [[Anwar Hossain Monir]] 
| [[Adam Gilchrist]] ([[Wicket-keeper|wk]])
|-
| [[Habibul Bashar]]
| [[Jason Gillespie]]
|-
| [[Hannan Sarkar]]
| [[Ian Harvey]]
|-
| [[Hasibul Hossain]]
| [[Matthew Hayden]]
|-
| [[Javed Omar]] 
| [[Brad Hogg]]
|-
| [[Khaled Mashud]] ([[Wicket-keeper|wk]])
| [[Brett Lee]]
|-
| [[Mashrafe Mortaza]]
| [[Darren Lehmann]]
|- 
| [[Mohammad Ashraful]]
| [[Damien Martyn]]
|-
| [[Mohammad Rafique (cricketer)|Mohammad Rafique]] 
| [[Andrew Symonds]]
|-
| [[Sanwar Hossain]]
| [[Brad Williams (cricketer)|Brad Williams]] 
|-
| [[Tapash Baisya]]
|-
| [[Tushar Imran]]
|}

==Notes==
{{reflist|2}}

==References==
* {{Cite book |first=Gideon |last=Haigh |year=2004 |title=200 Years of Australian Cricket: 1804-2004 |publisher=Pan Macmillan Australia |isbn=1-4050-3641-9 }}

==External links==
* [http://news.bbc.co.uk/sport1/hi/cricket/statistics/2855075.stm Bangladesh in Australia 2003]

{{International cricket tours of Australia}}
{{International cricket in 2003}}

[[Category:2003 in Australian cricket]]
[[Category:2003 in Bangladeshi cricket]]
[[Category:Bangladeshi cricket tours of Australia|2003]]
[[Category:International cricket competitions in 2003]]