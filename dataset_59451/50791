{{Infobox journal
| title = Building Services Engineering Research and Technology
| italic title = force
| cover = [[Image:Building Services Engineering Research and Technology cover.jpg]]
| editor = William H. Whalley
| discipline = [[Architectural engineering]]
| former_names = 
| abbreviation = Build. Serv. Eng. Res. Technol.
| publisher = [[Sage Publications]]
| country = 
| frequency = Quarterly
| history = 1980-present
| openaccess = 
| license = 
| impact = 0.820
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201803/title
| link1 = http://bse.sagepub.com/content/current
| link1-name = Online access
| link2 = http://bse.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 7859922
| LCCN = 81645851
| CODEN = BSETDF
| ISSN = 0143-6244
| eISSN = 1477-0849
}}
'''''Building Services Engineering Research and Technology''''' is a quarterly [[peer-reviewed]] [[scientific journal]] that covers the field of [[building services engineering]]. The journal was established in 1980 and is published on behalf of the [[Chartered Institute of Building Services Engineers]].<ref>[http://www.cibse.org/ CIBSE homepage]. CIBSE. Retrieved 17 May 2014.</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.820, ranking it 29th out of 58 journals in the category "Construction & Building Technology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Construction & Building Technology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.sagepub.com/journals/Journal201803/title}}

[[Category:Quarterly journals]]
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Engineering journals]]
[[Category:Publications established in 1980]]