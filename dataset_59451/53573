{{Accounting}}
'''International Accounting Standard 37: Provisions, Contingent Liabilities and Contingent Assets''', or '''IAS 37''', is an [[International Financial Reporting Standards|international financial reporting standard]] adopted by the [[International Accounting Standards Board|International Accounting Standards Board (IASB)]]. It sets out the accounting and disclosure requirements for [[provision (accounting)|provisions]], [[contingent liabilities]] and contingent [[assets]], with several exceptions,<ref name=ias37>IFRS Foundation, 2012. [http://eifrs.ifrs.org/eifrs/bnstandards/en/2012/ias37.pdf International Accounting Standard 37: Provisions, Contingent Liabilities and Contingent Assets]. Retrieved on April 24, 2012.</ref> establishing the important principle that a provision is to be recognized only when the entity has a [[Liability (financial accounting)|liability]].<ref name=iasplus>Deloitte Global Services Limited, 2012. [http://www.iasplus.com/en/standards/standard36 IAS 37 — Provisions, Contingent Liabilities and Contingent Assets]. Retrieved on April 24, 2012.</ref>

IAS 37 was originally issued by the [[International Accounting Standards Committee]] in 1998, superseding IAS 10: Contingencies and Events Occurring after the Balance Sheet Date, and was adopted by the IASB in 2001.<ref name=ias37/> It was seen as an "important development" in accounting as it regulated the use of provisions, minimising their abuse such as in the case of [[big bath]]s.<ref name=jw>Williams J, 1999. [http://www2.accaglobal.com/archive/sa_oldarticles/30899 IAS 37, PROVISIONS, CONTINGENT LIABILITIES AND CONTINGENT ASSETS]. Retrieved on April 25, 2012.</ref>

==Overview==
===Provisions===
IAS 37 establishes the definition of a provision as a "liability of uncertain timing or amount", and requires that all the following conditions be fulfilled before a provision can be recognized:
# the entity currently has a liability as a result of a past event;
# an outflow of resources is likely to be needed to settle the liability; and
# the amount of the obligation can be estimated reliably.<ref name=tech>IASC Foundation, 2009. [http://www.iasb.org/NR/rdonlyres/94C8F2F5-FC68-43E5-86AC-211C9B701FE5/0/IAS37.pdf IAS 37 Provisions, Contingent Liabilities and Contingent Assets]. Retrieved on April 25, 2012.</ref>
The standard also details measurement methods for provisions, generally requiring that the entity recognises a best estimate of the amounts needed to settle the obligation.<ref name=pwc>PWC, 2012. [https://pwcinform.pwc.com/inform2/show?action=informContent&id=0920084403183744 Provisions, Contingent Liabilities, and Contingent Assets]. Retrieved on April 25, 2012.</ref>

Contingent assets and liabilities
IAS 37 generally defines contingent assets and liabilities as assets and liabilities that arose from past events but whose existence will only be confirmed by the occurrence of future events that are not in the entity's control. It establishes that contingent assets and liabilities are not to be recognized in the financial statements, but are to be disclosed where an inflow of economic benefits is probable (assets) or the chance of outflows of resources is not insignificant (liabilities).<ref name=tech />

==Recent and proposed amendments==
In 2010 the IASB released an exposure draft of amendments to IAS 37 and invited comments on the draft. As of April 2012, the project has been paused pending other discussions.<ref name=amend>IFRS Foundation, 2012. [http://www.ifrs.org/current+projects/iasb+projects/liabilities/liabilities.htm Liabilities - amendments to IAS 37 (Paused)]. Retrieved on April 24, 2012.</ref> The amendments were controversial for setting out rules on how entities would account for legal cases in their financial statements; it would require firms to recognize the contingent liability as a [[weighted average]] of the possible outcomes of a legal case.<ref name=aa>Christodoulou M, 2012. [http://www.accountancyage.com/aa/news/1807778/ias-rule-legal-costs-set-cause-confusion IAS 37 rule on legal costs set to cause confusion]. Retrieved on April 24, 2012.</ref>

==References==
<references />
{{International Financial Reporting Standards}}

[[Category:International Financial Reporting Standards]]
[[Category:Liability (financial accounting)]]