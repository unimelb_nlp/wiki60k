{{pressrelease|date=March 2013}}
{{Infobox film
| name      = Theresa is a Mother
| director = [[Darren Press]] and C. Fraser Press
| writer    = C. Fraser Press
| starring  = [[Edie McClurg]]<br />C. Fraser Press<br />[[Matthew Gumley]]<br />Schuyler Iona Press<br />Maeve Press<br /> [[Richard Poe]]
| producer  = [[Darren Press]]
| music     = Schuyler Iona Press
| cinematography = Alex Kornreich
| editing   = Chad Smith
| runtime   = 101 minutes
| country   = United States
| language  = English
| released = 2012
| distributor  = Garden Thieves Pictures (Worldwide - all media)
}}

'''''Theresa is a Mother''''' is a 2012 American made [[comedy]] [[drama]] [[feature film]] [[film director|directed]] by C. Fraser Press and [[Darren Press]], and starring [[Edie McClurg]], C. Fraser Press, [[Matthew Gumley]], Schuyler Iona Press, Maeve Press and [[Richard Poe]]. The [[screenplay]] is by C. Fraser Press. The film was [[film producer|produced]] by A May Sky Picture Entertainment and is being distributed worldwide by Garden Thieves Pictures.

The film has screened at numerous [[film festivals]] around the country. Theresa is a Mother has been the recipient of 16 film festival awards including seven festival wins for Best Feature Film.

==Synopsis==
Theresa McDermott has chased her “ideal” life as an urban-dwelling, punk(ish) singer-songwriter to the very end of its possible existence.  She is broke, options have run out and she happens to have a few kids she is raising on her own since their dad split a year ago.  Facing eviction and nowhere to go, Theresa packs up her children and what is left of her life and moves back to the small rural town, childhood home and parents she deliberately ran from a decade ago.  Her parents’ mutual misery and depressingly gloomy lives were a “downer” she felt had no place in her fun city life.  Yet from the moment Theresa drives back up her old driveway, it is clear that there have been some major changes. Her parents, armed with a plethora of hobbies, a hot tub and a new philosophy, are not exactly the old folks she left behind.  Theresa needs a job, her parents need their space and a painful family history needs some closure.  Old wounds, unattainable dreams, and some “other things” are exposed as a fractured family works to become whole and a woman with a few kids learns to become a mother.

==Music==
Music plays a major role in Theresa is a Mother.  The Theresa is a Mother sound track features an eclectic array of music from [[Garage Punk]] to [[Merengue music|Merengue]] to [[Crooner]] to Southern Fried [[Honky Tonk]].  The main theme, “Summer Child” written and composed by 14-year-old cast member Schuyler Iona Press<ref name="westorlandonews">{{cite web|last=Cantone|first=Michael|title=Must See: "Theresa is a Mother" Hits Orlando Film Festival|url=http://westorlandonews.com/2012/10/19/must-see-theresa-is-a-mother-hits-orlando-film-festival/|work=West Orlando News|accessdate=11 March 2013}}</ref> (plays role of Maggie McDermott), was arranged and recorded by pianist John Ferrara.  Another song written and composed by Schuyler Iona Press, “No Words,” was arranged for the guitar and recorded by student Grammy Award Band member Gabe Schnider.<ref>{{cite web|title=GRAMMY Foundation Selects Students For GRAMMY Jazz Ensemble Program|url=http://music.broadwayworld.com/article/GRAMMY-Foundation-Selects-Students-For-GRAMMY-Jazz-Ensemble-Program-20110208|work=BWW Musicworld.com|accessdate=11 March 2013}}</ref> “No Words” is used throughout Theresa is a Mother as a main part of the film score. The Theresa Is A Mother Soundtrack features original music from: The Two-Bit Terribles, Frank Lamphere, Honky Tonk Union, Rumba Con Son, The Dave Sammarco Band, Karu, and Schuyler Iona Press. An official soundtrack has yet to be released.

==Awards and Nominations==
*''WINNER Best Film - [[Amsterdam Film Festival - Van Gogh Award]]
*''WINNER [[Best Picture|Best Film]] - [[Long Island International Film Expo - Jury Award]]
*''WINNER [[Best Picture|Best Film]] - [[Chain NYC Film Festival]]
*''WINNER [[Best Picture|Best Film]] - [[Orlando Film Festival]]<ref name="orlandofilmfest">{{cite web|title=News|url=http://orlandofilmfest.com/|work=Orlando Film Festival|accessdate=13 March 2013}}</ref> 
*''WINNER Best Film - Reel Independent Film Extravaganza<ref>{{cite web|title=2012 Festival Favorite 'THERESA IS A MOTHER' Comes to QUAD CINEMA|url=http://indienyc.com/?p=1271|work=indienyc.com|accessdate=13 March 2013}}</ref> 
*''WINNER Best Film - International Film Festival Manhattan<ref>{{cite web|title=2012 International Film Festival Manhattan Announces Film Selections|url=http://www.filmfestivaltoday.com/festivals/2012-international-film-festival-manhattan-announces-film-selections|work=Film Festival Today|accessdate=13 March 2013}}</ref> 
*''WINNER [[Best Picture|Best Film - Director's Choice]] - [[Northeast Film Festival]]
*''WINNER Best Director - [[Orlando Film Festival]]<ref name="orlandofilmfest" /> 
*''WINNER [[List of common film awards categories#Best Director|Best Director]] - [[Chain NYC Film Festival]]
*''WINNER Best Screenplay - NYC Independent Film Festival<ref>{{cite web|title=And the 2012 winner are..|url=http://www.nycindiefilmfest.com/|work=NYC Independent Film Festival|accessdate=13 March 2013}}</ref> 
*''WINNER Best Actress (C. Fraser Press) - Reel Independent Film Extravaganza
*''WINNER [[Best Actress]] (C. Fraser Press) - Chain NYC Film Festival
*''WINNER Best Ensemble - Chain NYC Film Festival
*''WINNER Best Supporting Actor ([[Richard Poe]]) - Maverick Movie Awards - [[Maverick Movie Awards]]
*''WINNER Best Actor ([[Richard Poe]]) - NYC Independent Film Festival<ref>{{cite web|title=And the 2012 winners are..|url=http://www.nycindiefilmfest.com/|work=NYC Independent Film Festival|accessdate=13 March 2013}}</ref> 
*''Nominated [[Best Supporting Actor]] ([[Matthew Gumley]]) - Reel Independent Film Extravaganza
*''Nominated [[Best Supporting Actress]] (Schuyler Iona Press) - Reel Independent Film Extravaganza{{citation needed|date=March 2013}}
*''Nominated Best Film - NYC Independent Film Festival
*''Nominated Best Performer (C. Fraser Press) - [[Orlando Film Festival]]
*''Nominated Best Performer (C. Fraser Press) - [[Northeast Film Festival]]
*''Nominated Best Performer (C. Fraser Press) - [[Maverick Movie Awards]]
*''Nominated Best Score (C. Fraser Press) - [[Orlando Film Festival]]
*''Nominated Best Film - Maverick Move Awards - [[Maverick Movie Awards]]
*''Nominated Best Supporting Actor ([[Matthew Gumley]]) - [[Maverick Movie Awards]]
*''Nominated Best Supporting Actress ([[Edie McClurg]]) - [[Maverick Movie Awards]]

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

== External links ==
*[http://www.imdb.com/title/tt1989646/ Theresa is a Mother IMDB]
*[http://www.amspe.com A May Sky Picture Entertainment.]
*[http://www.facebook.com/teresaisamother?ref=hl Theresa is a Mother Official Facebook]
*[http://www.gardenthieves.com/film.php?id=51 Garden Thieves Pictures Website]

[[Category:American films]]
[[Category:2015 films]]
[[Category:Films set in New York City]]
[[Category:American comedy-drama films]]
{{DEFAULTSORT:Theresa is a Mother}}