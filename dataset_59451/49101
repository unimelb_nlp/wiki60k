{{EngvarB|date=May 2013}}
{{Use dmy dates|date=May 2013}}
# 
{{Infobox Museum
|name = Koopmans-de Wet House
|image = Koopmans-de Wet House Nov 2011.jpg
|caption = Koopmans-de Wet House in 2011
|pushpin_map = Cape Town Central
|coordinates = {{coord|-33.920941|18.421291|display=inline,title}}
|established = {{start date and age|df=yes|10 March 1914}}
|dissolved = 
|location = 35 Strand Street,<br />8001 Cape Town, South Africa
|architect = 
|accreditation = Iziko Museums
|director = 
|curator = 
|type = [[Historic site]], Museum
| publictransit       = '''Bus:''' [[MyCiTi]] 105, 101
|website = http://www.iziko.org.za/static/page/history-of-koopmans-de-wet-house
|embedded = {{infobox |child=yes
|label1 = Built
|data1 = 1701–1793
|label2 = Architecture
|data2 = [[Neoclassical architecture|Neoclassical]]
}}
}}
'''Koopmans-de Wet House''' is a former residence and current [[museum]] in Strand Street, [[Cape Town]], [[South Africa]]. The house became part of the South African Museum in 1913 and was opened to the public on 10 March 1914.<ref name="Gutsche" /><ref name=Iziko>{{cite web|title=History of Koopmans-de Wet House|url=http://www.iziko.org.za/static/page/history-of-koopmans-de-wet-house|publisher=Iziko Museums of Cape Town|accessdate=11 November 2011}}</ref> It was declared a National Monument under National Monuments Council legislation on 1 November 1940.<ref name=SAHRA>{{cite web|title=Koopmans De Wet House 35 Strand Street Cape Town|url=http://196.35.231.29/sahra/HeritageSitesDetail.aspx?id=13526|publisher=South African Heritage Resources Agency|accessdate=16 November 2011}}</ref> It is the oldest house museum in South Africa.<ref name="Iziko" />

==Strand Street==
[[File:Cape Town 1785.jpg|thumb|280px|Cape Town in 1785]]
Strand Street is one of the oldest and widest streets in Cape Town. Between 1664 and 1702 Strand Street was spoken of as Zee Straat. A [[Dutch East India Company]] (VOC) record of 1704 refers to it as Breete Strand Straat, while another calls it Breete Opgaande Straat no. I. In 1790 the matter was settled and name-boards with ''Strand Straat'' affixed to the corner houses.<ref name="Picard" />

For two centuries Strand Street was home to the residences of citizens of the [[Cape Colony]]. The first house was occupied on 8 February 1664 by the baker [[Thomas Christoffel Mulder]].<ref name=Picard>{{cite book|last=Picard|first=Hymen W. J.|title=Gentleman's Walk: The Romantic Story of Cape Town's Oldest Streets, Lanes and Squares|year=1968|publisher=Struik|location=Cape Town}}</ref> Another resident was the wealthy butcher [[Henning Huysing]], who built one of the first two-storeyed houses in the Cape Colony in the street. The Dutch East India Company granted erven (plots) to these employees, who would later play important roles as citizens of the colony. Huysing became a Vryburgher (or Vrijburgher) on 2 January 1684, a status in which an employee of the VOC was released from their contractual obligations to the company and permitted to farm, become a tradesman, or work for others.<ref>{{cite web|last=Robertson|first=Delia A.|title=Henningh Hùijsing|url=http://www.e-family.co.za/ffy/g6/p6962.htm|accessdate=11 November 2011}}</ref> Ironically, Huysing would be instrumental in getting [[Willem Adriaan van der Stel]], the Governor of the Cape Colony, recalled on charges of corruption.<ref name=Viney>{{cite book|last=Viney|first=Graham|title=Colonial houses of South Africa|year=1987|publisher=Struik|location=Cape Town|isbn=978-0-947430-05-4|url=https://books.google.co.za/books?id=fbU5Ea8q3OIC&dq=Colonial+houses+of+South+Africa&source=gbs_navlinks_s}}</ref>
[[File:Strand Street, Cape Town 1832.jpg|thumb|Strand Street depicted in 1832]]
The Company had imposed a grid of streets on the settlement, which divided it into blocks. Block J was bordered by Strand Street, Long Street, Castle Street and Burg Street and was divided into 10 erven (plots). Erven 7 and 8, on the Strand Street side, were granted by Governor Willem Adriaan van der Stel to Reijnier Smedinga in 1699 and 1701 respectively. Erf 8 is the site of the Koopmans-de Wet House.<ref name="Picard" />

==Occupants==
The early dwelling, now substantially extended and altered, was built in 1701 by Reijnier Smedinga, silversmith, goldsmith, jeweller and joint assayer to the Dutch East India Company.<ref name=Fransen>{{cite book|last=Fransen|first=Hans|title=The Old Buildings of the Cape|year=2004|publisher=Jonathan Ball Publishers|location=Cape Town|isbn=1-86842-191-0|pages=46–47}}</ref> In 1722, Anthonij Hoesemans, lessee of a Company's wine license, took ownership of the house and erf 8. His enjoyment of the property was brief, for in 1723 the minutes of the Council of Policy at the Cape of Good Hope begin to refer to Claas van Donselaar,<ref>{{cite web|title=C. 66, pp. 33–37|url=http://databases.tanap.net/cgh/main.cfm?artikelid=22404|work=Resolutions of the Council of Policy of Cape of Good Hope|publisher=Cape Town Archives Repository, South Africa|accessdate=11 November 2011}}</ref> a soldier who had been released from his contract on 4 May 1723, as the lessee of the wine license.<ref>{{cite web|title=C. 67, pp. 77–82|url=http://databases.tanap.net/cgh/main.cfm?artikelid=22418|work=Resolutions of the Council of Policy of Cape of Good Hope|publisher=Cape Town Archives Repository, South Africa|accessdate=11 November 2011}}</ref><ref>{{cite web|title=C. 68, pp. 41–50|url=http://databases.tanap.net/cgh/main.cfm?artikelid=22424|work=Resolutions of the Council of Policy of Cape of Good Hope|publisher=Cape Town Archives Repository, South Africa|accessdate=11 November 2011}}</ref> Both Hoesemans and his wife, Rijkje van Donselaar, had died earlier that year.<ref>{{cite web|title=C. 72, pp. 110–123|url=http://databases.tanap.net/cgh/main.cfm?artikelid=22533|work=Resolutions of the Council of Policy of Cape of Good Hope|publisher=Cape Town Archives Repository, South Africa|accessdate=11 November 2011}}</ref><ref>{{cite journal|title=MOOC 7/1/3 nos. 61, 72, 73|work=Inventories of the Orphan Chamber Cape Town Archives Repository, South Africa|publisher=Cape Town Archives Repository, South Africa|accessdate=7 November 2011}}</ref> Claas van Donselaar was uncle to Rijkje van Donselaar and was made executor of the estate, along with Daniel Thibault and Jan Smit.<ref>{{cite web|title=C. 78, pp. 9–14|url=http://databases.tanap.net/cgh/main.cfm?artikelid=22682|work=Resolutions of the Council of Policy of Cape of Good Hope|publisher=Cape Town Archives Repository, South Africa|accessdate=11 November 2011}}</ref> The property was transferred to Jacob Leever in 1724, to Hendrik van Aarde in 1730, and from him to Willem Pool (c.1744).<ref name="Iziko" />

A German carpenter, Johan Fredrik Willem Böttiger, the owner from 1748 to 1771 increased the area of the property and enlarged the house. Böttiger was a Burgher Councillor and is thus distinguished for being a member of the very first town council in South Africa.<ref name="Picard" /><ref name="Fransen" />

Pieter Malet, the owner from 1771 to 1793, and his wife, Catharina Kruins, added to the property by installing the slave-quarters over a coach-house at the back, building a second rear wing and adding uppers stories to both wings. It is also in his time that the current façade was added.<ref name="Iziko" /> When Malet died, Hendrik Vos bought the house from his widow.

Vos and his wife, Maria Anna Colyn, lived in the house from 1796 to 1806.<ref name="Iziko" /> They had four children while living in there.<ref>{{cite web|last=Unknown|title=Maria Anna COLYN|url=http://awt.ancestrylibrary.com/cgi-bin/igm.cgi?op=GET&db=flakey&id=I101704339&style=TABLE&ti=5542|accessdate=18 November 2011}}</ref>

Margaretha Jacoba Smuts, the widow of the President of the Burgher Council, [[Hendrik Justinus de Wet]], acquired the property in 1806. Some time after her husband died in 1802, she had sold their house on the corner of Heerengracht ([[Adderley Street]]) and Castle Street and moved with their five children and her stepson.

De Wet left a large estate, including slaves.<ref>{{cite web|title=MOOC 8/23.40 1/2|url=http://databases.tanap.net/mooc/main_article.cfm?id=MOOC8%2F23%2E40%201%2F2|work=Inventories of the Orphan Chamber Cape Town Archives Repository, South Africa|publisher=Cape Town Archives Repository, South Africa|accessdate=20 November 2011}}</ref> Margaretha had retained seven slaves: Jonas van de Caab, a cooper; Citie, his "wife"; Hector and Jacob, their children; Theresia; Kito van Mosambique, a cook; and July, a houseboy. By 1816, ten slaves were registered to the widow Smuts. July is not listed, but the new slaves were: Lafleur, a woodcutter; Lendor, a woodcutter, who in a later document is reported to have died on 31 December 1822; Kado (alias Bejoen), a tailor; Nancy, a girl, aged about 4.<ref>SO 6/34: the Slave Registers, 1816–1834, vol. W</ref>

The brothers Johannes, Fredrick and Petrus de Wet inherited the house after her death in 1840, and Johannes decided to buy the others out. He married Adriana Dorothea Horak, a granddaughter of [[Martin Melck]], and the daughter of [[Jan Andries Horak]], whose ancestor had built the Smedinga house.<ref name="Picard" /> They had two daughters [[Maria Koopmans-de Wet|Maria (Marie)]] and Margaretha. Marie had been born in the house on 18 March 1834.
[[File:Marie Koopmans-de Wet.jpg|thumb|Marie de Wet as a young woman]]
Marie de Wet married Johan Koopmans in 1864 and it is this union that gave rise to the current name of the house. The death of Johan Koopmans in 1880, sent Maria into an extended period of mourning, during which she travelled abroad, meeting [[King William III]] of the Netherlands. On her return she and her sister lived in Koopmans-de Wet House and turned it into a [[Salon (gathering)|salon]], and a transit point for supplies donated to Boer prisoners of war during the [[Second Boer War|South African War]].<ref name=Barnard>{{cite book|last=Barnard|first=Madeleine|title=Cape Town Stories: Local Flavours from the Peninsula|year=2007|publisher=Struik|location=Cape Town|isbn=978-1-86872-940-1|pages=28–30|url=https://books.google.co.za/books?id=tW3M_wz6pAAC&dq=Cape+Town+Stories&source=gbs_navlinks_s}}</ref>

==The Sale of 1913==

Marie died on 2 August 1906 and when Margaretha de Wet died on 18 October 1911, the event was anticipated by a group of people interested in the preservation of cultural objects, calling themselves the [[National Society (South Africa)|National Society]]. ''Ons Land'', a Dutch language newspaper, had begun the campaign for the preservation of the Koopmans-de Wet House as an historical monument.<ref name=Gutsche>{{cite book|last=Gutsche|first=Thelma|title=No Ordinary Woman: The Life and Times of Florence Philips|year=1966|publisher=Howard Timmins|location=Cape Town|url=https://books.google.co.za/books?id=La4ZAAAAYAAJ&q=No+Ordinary+Woman&dq=No+Ordinary+Woman&hl=en&ei=IBzBTrvmDNSz8QOTpvSXBA&sa=X&oi=book_result&ct=result&resnum=3&ved=0CDwQ6AEwAg}}</ref> On 28 October, the National Society weighed in, adding that its content was a treasury of antiques and should be preserved intact. The Municipal Council passed a similar motion on 7 November. Architect [[Herbert Baker]] wrote supportingly from Johannesburg, as did his partner, [[Frank Masey]], from [[Salisbury]].<ref name="Gutsche" />

The wills of the De Wet sisters were ruled legally impossible to execute and the house (27 Strand Street, now no. 35), with all its content, was to be put up for auction. At the beginning of 1913 influential cultural figures [[Dora Fairbridge]], [[Edward Roworth]], [[William Frederick Purcell]], [[Major William Jardine]], [[Franklin Kaye Kendall]], and [[Fred Glennie]] were driving the campaign for government purchase of the Koopmans-de Wet Collection. [[Florence, Lady Phillips|Florence]] and her husband, the [[Randlord]] [[Lionel Phillips]], threw their considerable influence behind the movement from Johannesburg.

Under the patronage of Aletta Johanna, Lady de Villiers, wife of Chief Justice [[John Henry de Villiers]], a meeting was held in the [[Cape Town City Hall]] Library on 27 January 1913. Chaired by Annie Botha, wife of [[Louis Botha]], the meeting was attended by powerful personalities: [[Harry Hands]] (the Mayor), J. R. Finch (the Town Clerk), Sir [[James Rose-Innes]], Sir William Thorne, Sir [[Ernest Kilpin]], [[Olive Schreiner]], Dr. [[F.V. Engelenburg]], [[L. Péringuey]] (Director of the South African Museum), [[Louis Mansergh]], Anna Purcell, Mrs. Marloth (wife of botanist [[Rudolph Marloth]]), Dora Fairbridge, Sir [[Meiring Beck]] and Mrs. [[Beaumont Rawbone]].

Lionel Philipps moved that the house and contents be preserved for the nation and that General and Executive Committees be formed to engage in fundraising and other necessary activities. He went so far as to propose the members, with Annie Botha and Dora Fairbridge to chair the committees. [[Florence, Lady Phillips|Florence Phillips]] was to serve on both. The Rev. [[A. I. Steytler]] seconded the motion in Dutch and the proposal was accepted unanimously.<ref name="Gutsche" />

To drum up publicity, [[Olga Racster]] began contributing a series of articles on the house and its contents to the [[Cape Times]]. Mayor Harry Hands sent letters appealing for donations to all municipalities throughout the Union. Most notably, Johannesburg refused.

On the night of 27 February 1913, Florence Phillips accompanied by Mrs. Grace Douglas Pennant, Mrs. Marloth, and Dr. Purcell, went with Lionel in a deputation to appeal to the Cape Town City Council itself. Lionel and Senator Sir Meiring Beck<ref name=Scully>{{cite book|last=Scully|first=William Charles|title=Sir J. H. Meiring Beck: A Memoir|year=1921|publisher=T Maskew Miller|location=Cape Town|isbn=978-1-116-17613-1|pages=68–69|url=http://ia600305.us.archive.org/9/items/sirjhmeiringbeck00scul/sirjhmeiringbeck00scul.pdf}}</ref>  addressed the Council with the result of £1000 being voted (later reduced to £500 by a meeting of ratepayers). Amid bitter political division, the House of Assembly united briefly behind a national issue and allocated £3000 for the purchase of furniture.<ref name="Gutsche" />

Dr. Purcell, Mr. J. R. Finch, Mr. A. E. F. Gore and Florence Phillips were selected to choose the furniture and objects which were to be retained for the Collection. The painter [[Edward Roworth]], who was in 1941 to be made Director of the [[South African National Gallery]], assisted in the selection of the pictures. After inspecting 2089 lots, it was agreed to purchase 356. The General Committee designated Dr. Purcell to bid.<ref name="Gutsche" />

The sale, conducted by J. J. Hofmeyer & Son, started on 17 March 1913 and was to last ten days. The first eight days took place at the Good Hope Hall, Cape Town; the last two at the house in Strand Street. Viewing only started on 15 March, and viewers were not allowed to touch any of the pieces. A sparse catalogue and less than ideal viewing conditions put the Committee, which had been able to inspect at their leisure, at a considerable advantage.<ref name="Woodward" />

The sale was controversial. William R. Morrison, an Africana dealer and collector, wrote ten highly critical reports in the Cape Times, and on the evening of the first day the Government allocation of public funds came under heavy attack in the House and had to be defended by Lionel Philips and [[Abraham Fischer]].<ref name="Gutsche" />

Overvaluations abounded: a [[brass]] flower-bowl sold for £8 5s, prompting Morrison to quip that brass is counted among the [[precious metals]] in Cape Town. Another brass bowl sold for £9 10s. Bargains included a [[Louis XVI]] [[ormolu]] box, with [[mother of pearl]] lid, and inlaid enamel, by Vervain, sold for 26 shillings. A [[Louis XV]] silver [[cruet]], with cut glass bottles and castors sold for £30.<ref name="Woodward" />

Dr. Purcell was able to make his allotted purchases. He secured 374 lots, which he estimated to be 21% of the total. There were 69 pieces of furniture, 39 works of copper and brass, 28 pieces of [[Sheffield plate]], 41 silver items, 110 pieces of glass and 196 ceramic articles. The Sheffield plate included the [[Breda service]] consisting of 3 branched [[candelabra]], 5 [[entrée]] dishes, 4 covered dishes and 2 sauce-[[tureens]]. It was bought in London in the 1834 for [[Michel van Breda]] of Oranjezicht, with slave emancipation money. It set the Committee back £500 10s.<ref name="Woodward" />

The most important silver items were Dutch, of which an engraved ''[[konvoor]]'' and kettle were notable.<ref name="Woodward" /> Among the glass was a tumbler, engraved with a portrait of Louis XVI, presented by him to a member of the [[de Lettre]] family.<ref name="Woodward" />

A total of £4032 was realised.<ref name="Gutsche" />

The Koopmans-de Wet House would be auctioned on 8 April 1913. Lionel Phillips and his committee met in the night of 7 April, to formally resolve to buy the house and then hand it over to the South African Museum. The next day the public Committee bought the house for £2800 and formally donated it, along with Dr. Purcell's purchases, to the State.<ref name="Gutsche" /><ref name="Woodward" />

===The fate of the library===
Dr. Purcell was not the only successful bidder in 1913. William R. Morrison was asked by the Kimberley Library to purchase a number of rare South African books. The Kimberley Public Library Annual Report, submitted at the Annual General Meeting held on 4 March 1914 records that he was "successful in securing at very modest prices a considerable number of excessively rare books for our African collection". Morrison had spent £19 6/6 in total.<ref name=Holloway>{{cite book|last=Holloway|first=Rosemary Jean|title=The History and Development of the Kimberley Africana Library and Its Relationship with the Kimberley Public Library|year=2009|publisher=University of South Africa|location=Pretoria|url=http://uir.unisa.ac.za/bitstream/handle/10500/3699/dissertation_holloway_r.pdf?sequence=1}}</ref>

Amongst the rarities acquired were the 21 volumes of ''Historische Beschryving der Reizen of nieuwe en volkoome Verzameling van de aller waardigste en zeldsaamste zee- en landtogten'' printed in 1747 by [[Pieter de Hondt]] in [['s-Gravenhage]]. [[Sebastian Franck]]'s ''Dat wereltboek, spiegel ende Beeltnisse des gheleelen Aertbodems, in vier boecken (te wetenim Asiam, Africam, Europam ende Americam) gealtelt ende afgedeylt'', written in 1531 and published in 1562 and 1563, was sold bound with his ''Chronica, zytboeck en geschiet bibel van aenbegin tot MDXXXIII'', written in 1534.<ref name="Holloway" />

Another rarity from the Koopmans-de Wet House library was [[Abraham Josias Sluysken]]'s handwritten report on the capitulation of the Cape to the British forces under Admiral Sir [[George Elphinstone]] on 10 June 1795. This manuscript was later purchased by the Kimberley Public Library (through Morrison) with the aid of a donation by [[De Beers]].<ref name="Holloway" />

===The fate of the ceramics collection===
Woodward<ref name=Woodward>{{cite book|last=Woodward|first=C. S.|title=Oriental Ceramics at the Cape of Good Hope 1652–1795|year=1974|publisher=A. A. Balkema|location=Cape Town|isbn=0-86961-056-2}}</ref> believes Marie Koopmans-de Wet amassed the first important ceramics collection in South Africa. [[Lady Charlotte Guest]], herself a knowledgeable collector of ceramics, visited Marie and Margaretha at home on 10 December 1883 and wrote in her journal of "a great deal of good china".<ref name=Schreiber>{{cite book|last=Schreiber|first=Charlotte|title=Lady Charlotte Schreiber's Journals|year=1911|publisher=Johan Lane, the Bodley Head|location=London|url=http://ia600202.us.archive.org/1/items/ladycharlottesch02schruoft/ladycharlottesch02schruoft.pdf}}</ref>

The sale of 1913 included 147 pieces of [[Chinese porcelain]], 20 of [[Japanese porcelain]] and 29 of Delft and other earthenware. Purcell noted 142 pieces of blue-and-white Chinese porcelain of a type exported in quantity from China.

[[Nankin]] porcelain came to the Cape in large quantities and Marie Koopmans-de Wet was a collector. She owned an intact Nankin service, still on display at Koopmans-de Wet House.<ref name="Woodward"/> The sale of 1913 contained 43 lots of Nankin, fetching over £88.<ref name="Woodward" /> Purcell acquired so many that Morrison was to decry the purchases as ''Kitchen Nankin'' on the 7th day of sale.<ref name="Woodward" /> For whatever reason, Purcell chose to purchase ceramics typical of the prosperous town house of the early nineteenth century Cape, and not necessarily of the original collection.<ref name="Woodward" />

One ceramic piece of major importance was bought in 1913 for £53. It is a bottle shaped vase, enamelled in ''[[famille rose]]'' with sprays of fruiting peach, bearing the [[Qianlong Emperor|Ch'ien Lung]] seal mark and of the period.<ref name="Woodward" />
[[File:Koopmans-de Wet House.jpg|thumb|left|280px|Koopmans-de Wet House in 1920]]

==First restorations (1913–1914)==

Restorations were led by Dr. Purcell, a zoologist and biologist, who brought a scientific rigour to the process, documenting every stage meticulously.

Having stripped the plaster from the exterior walls, to expose the bricks, Purcell was able to conclude that the house was built in different stages between 1701 and 1793.<ref name="Picard" /> The Smedinga dwelling would have been one of no more than 150 houses in the Cape Colony. It would have had a low pitched roof thatched with [[Elegia tectorum]] (Cape Thatching Reed or Dekriet), the typical examples described by [[Carl Peter Thunberg]] being of brick and [[Whitewash|white-washed]].<ref name="Thunberg" />

Fire being a constant danger, thatched roofs were ordered raised to at least 8&nbsp;ft. above the ground in 1698. Due to this hazard, along with the wind, flat-roofed dwellings became fashionable at the Cape. The first flat-roofed house in the Cape was erected in 1732; whale oil was used to prevent it from leaking.<ref name="Picard" />

Purcell found lime-concrete on the roof, which would have been quarried on [[Robben Island]].<ref name=Thunberg>{{cite book|last=Thunberg|first=Carl Peter|title=Travels at the Cape of Good Hope 1772–1775|year=1986|publisher=Van Riebeeck Society|location=Cape Town|isbn=978-0-620-10981-9|url=https://books.google.co.za/books?id=ocliNOUc20oC&vq=robben+island&source=gbs_navlinks_s}}</ref> The lime was replaced with boards and [[bitumen]]. A number of recent structural additions were also removed.<ref name="Iziko" />

Dr. Purcell discovered extensive murals, but was unable to pursue their restoration due to lack of funds. Indeed, funds had been completely depleted by the opening on 10 March 1914, and the Mayor John Parker was moved to appeal for more.<ref name="Gutsche" /> Further work would not be done until 1979.<ref name="Iziko" />

When Purcell died on 3 October 1919, Florence Phillips contributed £10 toward a memorial tablet, designed by architect [[Joseph Michael Solomon]], with a likeness in bas-relief modelled by [[Moses Kottler]]. It was unveiled in January 1922.<ref name="Gutsche" />

==The House and furnishings==
[[File:Koopmans-de Wet House Facade.jpg|thumb|left|Architectural Drawing by G. E. Pearse (1933)<ref>Pearse, G. E. Eighteenth Century Architecture in South Africa. Cape Town: A. A. Balkema, 1933.</ref>]]
The façade of the building speaks of strong [[Neoclassical architecture|Neoclassical]] influence, with four [[pilasters]], the inner ones topped by a [[pediment]]. It dates from slightly before 1793 and has been attributed to the French architect [[Louis Michel Thibault]], although this attribution is unfounded.<ref name=Linscheid>{{cite journal|last=Linscheid|first=Klaus F.|title=The Use of Golden Proportions|journal=RESTORICA|date=October 1988|pages=12–15|url=http://architektur-und-medien.de/DE/downloads/KoopmansdeWetHouse.pdf|accessdate=10 November 2011}}</ref><ref name=Fairbridge>{{cite book|last=Fairbridge|first=Dorothea|title=Historic Houses of South Africa|year=1922|publisher=Maskew Miller|location=Cape Town|isbn=978-1-152-49521-0|url=https://archive.org/stream/cu31924014905834#page/n9/mode/2up}}</ref> Linscheid<ref name="Linscheid" /> argues that Koopmans-de Wet House has the only façade known to carry the proportions of the [[golden ratio]] through to every detail.

Early Cape floors and beams were of [[Podocarpus]] wood, but by the time Thunberg visited the Cape (1772–1775) supplies were dwindling and inland stores such as Olifantsbosch and Grootvadersbosch were near exhaustion.<ref name="Thunberg" /> Later additions to Cape houses typically have wood imported from Europe or the [[East Indies]], Koopmans-de Wet House having many examples. The portal has a [[teak]] and plaster [[architrave]], a shaped teak [[transom (architectural)|transom]] and projecting lantern.<ref name=Obholzer>{{cite book|last=Obholzer|first=Anton|title=The Cape House and its Interior|year=1985|publisher=Stellenbosch Museum|location=Stellenbosch|isbn=0-620-04258-3}}</ref> Banisters are of teak with [[Celtis africana|stinkwood]] returns.<ref name="Obholzer" /> The ceilings are of teak throughout.<ref name="Iziko" />

===Drawing room===
[[File:Bureau cabinet (1750-1760).jpg|thumb|270px|Bureau (1750–1760)]]
The late 18th century [[drawing room]] is furnished with examples of Cape furniture. A [[Chest of drawers|bureau]] cabinet of stinkwood and [[chestnut]] (height 252&nbsp;cm, width 133&nbsp;cm, depth 70&nbsp;cm) was made in the Cape of Good Hope (1750–1760) with Cape silver keyplates dating from circa 1800.<ref name="Fairbridge" /><ref name="Waanders" />

There are miniatures above the fireplace depicting unknown persons, but also known Cape personalities: [[Petrus Borchardus Borcherds]], who lived at what was no. 7 Strand Street until 1845,<ref name=Borcherds>{{cite book|last=Borcherds|first=Petrus Borchardus|title=An auto-biographical memoir of Petrus Borchardus Borcherds, Esq.|year=1861|publisher=Ayer Publishing|isbn=978-0-8369-9135-2|url=https://books.google.com/books?id=gRFwusfrGMMC&source=gbs_navlinks_s}}</ref> Susanna van der Poel (1743–1840) and Anna Geertruida Wykerd.<ref name="Iziko" />

The room includes a convex mirror with gilded frame (1810–1820) in the French [[Empire style]], a sofa from the Cape Orphan Chamber, and a pair of Sheffield plate [[candelabra]] in the [[neoclassicism|neoclassical]] style. The gabled display cabinet of stinkwood contains a service made c.1800 for Rudolph Cloete of Constantia. The Koopmans-de Wet collection has about 50 pieces of this service, of which the rest is at the Alphen Hotel.<ref name="Woodward" />

===Dining room===

A Cape buffet from 1780–1800, with its zinc basin, fold-out leaves and folding shelves stands here. The display cabinet on stand dates from 1775–1800 and houses a selection of late 18th and early 19th century silver tableware, including the Van Breda service.<ref name="Iziko" />

===Lower Hall===

A [[sedan chair]] which belonged to [[Maria Margaretha Horak]], Marie Koopmans-de Wet's maternal grandmother, is kept here.<ref name="Iziko" /> The brass chandelier bears the name of [[Martinus Lourens Smith]] and dates from c.1780. It is one of a pair made for the [[Lutheran Church (Cape Town)|Lutheran Church]]. The other chandelier hangs at Kersefontein.<ref name="Fransen" />

===Small sitting room===

A Cape 18th century day-bed and a box of [[Pterocarpus indicus|amboyna]] and [[ebony]], said to have been the shrough box, in which Maria Margaretha Horak kept her burial clothes, can be seen in this room.<ref name="Iziko" />

===Music room===

The music room with its square piano from around 1830 is notable for its painted friezes and a medallion painted above the fireplace. In one corner stands a stinkwood Cape gabled corner cupboard with silver escutcheon plates.<ref name="Cape" /> Japanese Imari porcelain garniture is set on top of the cornice.<ref name="Woodward" />

Important ceramics in this room include a covered baluster jar which dates from the 17th century, which is one of the earliest pieces in the house.<ref name="Iziko" /> Armorial porcelain, part of a service made between 1740 and 1755,<ref name="Woodward" /> can be viewed in the flat pedimented display cabinet.<ref name="Cape" /> There are two plates bearing the arms of John White, an Englishman who came to the Cape in 1700. He married into a Dutch family and [[Batavianization|Batavianized]] his name to Jan de Wit. He became a prominent Cape citizen and held the position of Burgher Councillor on several occasions. De Wit died in 1755, at the age of 77. The plates might be the earliest examples of armorial porcelain in South Africa.<ref name="Woodward" />

The Louis XV settee of [[Andaman Padauk]] (Pterocarpus dalbergioides), possibly of Eastern origin<ref name="Cape" /> was once used in the Wale Street office of the civil commissioner of the Cape, prior to that in the office of the Council of Policy at the [[Castle of Good Hope|Castle]], and presented by Sir [[Frederick de Waal]].<ref name="Iziko" />

===Morning room and kitchen===

A [[Jakarta|Batavian]]-styled ebony and [[cane]] armchair (height 88.5&nbsp;cm, seat height 39&nbsp;cm, width 57&nbsp;cm, depth 46&nbsp;cm) was probably made in the Cape between 1680 and 1700.<ref name="Waanders" /> There is a 19th-century [[Frisians|Frisian]] tail clock or ''Staartklok'' from the 19th century.<ref name="Iziko" />

A transitional [[Tulbagh]] armchair<ref name=Cape>{{cite book|last=Obholzer|first=Anton|title=Cape Antique Furniture: a Comprehensive Pictorial Guide to Cape Furniture|year=2004|publisher=Struik|location=Cape Town|isbn=978-1-86872-939-5|url=https://books.google.co.za/books?id=iMy9DmOk_RAC&dq=Cape+furniture&source=gbs_navlinks_s}}</ref> (height 103&nbsp;cm, seat height 41&nbsp;cm, width 62&nbsp;cm, depth 50&nbsp;cm), with its combination of old and new styles was of Cape origin (1690–1740) is an example of the style that would be typical of the Cape chair in the early 18th century.<ref name="Waanders" />

[[File:Armoire of Stinkwood.jpg|thumb|Armoire of stinkwood (1780–1790)]]

===Main bedroom===

The house contains several [[armoire]]s, of which Dorothea Fairbridge,<ref name="Fairbridge" /> believes the finest is a [[rococo]] cabinet (height 270&nbsp;cm, width 220&nbsp;cm, depth 72&nbsp;cm) of [[Celtis africana|stinkwood]] with [[Pterocarpus indicus|amboyna]]. It is gable-topped with flat spaces on which would have been placed blue Nankin or [[Delftware]] garnitures. It rests on claw feet and retains its original silver [[escutcheon (furniture)|escutcheon]]s and handles by [[Daniel Heinrich Schmidt]], a Cape silversmith. The handles date the cabinet to between 1780 and 1790.<ref name=Waanders>{{cite book|last= Eliëns|first=Titus M. ed.|title=Domestic Interiors at the Cape and Batavia 1602–1795|year=2002|publisher=Waanders Uitgevers|location=Zwolle|isbn=90-400-8715-6}}</ref> This cabinet might be one of three that belonged to Hendrik de Wet at the time of his death in 1802,<ref name="Waanders" /> but it was certainly in its present upstairs bedroom location when Margaretha de Wet died.<ref name="Fairbridge" /> The armoire, listed in the 1913 sale catalogue as lot 2308, was purchased for £100.<ref name="Woodward" />

A pair of rare [[corner chair]]s,<ref name="Fairbridge" /><ref name="Cape" /> along with the only known illustration of a square corner chair,<ref name="Waanders" /> in the family portrait of merchant [[Joachim Wernich]], his wife Anna Margaretha van Reenen and their daughter (67 x 90&nbsp;cm; dated 1754), by Peter Willem Regnault,<ref name=Gordon-Brown>{{cite book|last=Gordon-Brown|first=Alfred|title=Pictorial Africana|year=1975|publisher=A.A. Balkema|location=Cape Town|isbn=0-86961-070-8|pages=210}}</ref> further distinguishes this room. The chairs are of stinkwood and cane with panel-and frame construction, camfered edges, twist-turned legs and stretchers.<ref name="Waanders" />

===Second bedroom===

The [[cheval mirror]] (1810–1820), in the French Empire style, was reputedly part of the cargo intended for [[Napoleon Bonaparte]]'s friend, [[Henri Gatien Bertrand]], who stayed with him when he was banished to [[St. Helena]]. The islanders prevented the cargo from being off-loaded and sent it on to the Cape, where it was auctioned on the quay and the mirror bought by Marie Koopmans-de Wet's maternal grandfather, Jan Andries Horak.<ref name="Iziko" />

==See also==
* [[List of Castles and Fortifications in South Africa]]

==References==
{{reflist|2}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

==External links==
*{{SAHRA site| 920180198| name= Koopmans De Wet House, 35 Strand Street, Cape Town}}

{{Iziko Museums navbox|state=uncollapsed}}
{{Cape Town}}

[[Category:History of Cape Town]]
[[Category:Buildings and structures in Cape Town]]
[[Category:Tourist attractions in Cape Town]]
[[Category:Historic sites in South Africa]]
[[Category:South African heritage sites]]
[[Category:1914 establishments in South Africa]]
[[Category:Museums established in 1914]]

[[af:Marie Koopmans-De Wet#Die Koopmans-De Wet-huis]]