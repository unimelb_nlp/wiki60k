{{Infobox journal
| title        = Physica
| cover        = 
| editor       =
| discipline   = [[Physics]]
| language     = English
| abbreviation = 
| publisher    = [[Elsevier]]/[[North-Holland]]
| country      = [[Netherlands]]
| frequency    = 
| history      = ''Physica'':<br>(1934–1974),<br>''Physica A'':<br>(1975–present)<br>''Physica B'':<br>(1975–present)<br>''Physica C'':<br>(1975–present)<br>''Physica D'':<br>(1980–present)<br>''Physica E'':<br>(1998–present)
| openaccess   = 
| impact       = ''Physica A'': 1.785<br>''Physica B'': 1.352<br>''Physica C'': 0.835<br>''Physica D'': 1.579<br>''Physica E'': 1.904
| impact-year  = 2014
| website      =
| link1        =
| link1-name   =
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         = 
| LCCN         =
| CODEN        =
| ISSNlabel    = Physica A
| ISSN        = 0378-4371
| eISSN       =
| ISSN2label   = Physica B
| ISSN2        = 0921-4526
| eISSN2       =
| ISSN3label   = Physica C
| ISSN3        = 0921-4534
| eISSN3       =
| ISSN4label   = Physica D
| ISSN4        = 0167-2789
| eISSN4       =
| ISSN5label   = Physica E
| ISSN5        = 1386-9477
| eISSN5       =
}}

'''''Physica''''' is a [[Netherlands|Dutch]] series of [[peer-review]]ed,  [[scientific journal]]s of [[physics]] by [[Elsevier]]. It was founded in 1934 as a single journal  entitled ''Physica'' and was split in a three-part series in 1975 (''Physica A'', ''Physica B'', ''Physica C''). ''Physica D'' was created in 1980, and ''Physica E'' in 1998. It was published in [[Utrecht (city)|Utrecht]] until 2007, and is now published in [[Amsterdam]] by [[Elsevier]].

==''Physica A: Statistical Mechanics and its Applications''==
''Physica A'' was created in 1975 as a result of the splitting of ''Physica'' in 1975. It is concerned with [[statistical mechanics]] and its applications, particularly [[random system]]s, [[fluid]]s and [[soft condensed matter]], [[dynamical process]]es, [[theoretical biology]], [[econophysics]], [[complex system]]s, and [[network theory]].

''Physica A'' is published by [[Elsevier]]/[[North-Holland]] on a bimonthly basis (24 times per year).

==''Physica B: Condensed Matter''==
''Physica B'' was created in 1975 as a result of the splitting of ''Physica'' in 1975. It is concerned with [[condensed matter physics]] and its applications, particularly [[solid-state physics|solid state]] and [[cryogenics|low-temperature]] physics. Some conference proceedings are published in special editions of ''Physica B''.

''Physica B'' is published by [[Elsevier]]/[[North-Holland]] on an at-least monthly basis (12 times per year, sometimes more).

==''Physica C: Superconductivity and its Applications''==
''Physica C'' created in 1975 as a result of the splitting of ''Physica'' in 1975. It is a "rapid communications" type of journal, concerned with the topic [[superconductivity]], [[superconductive material]]s, and connected phenomena.

''Physica C'' is published by [[Elsevier]]/[[North-Holland]], two or three times per month.

==''Physica D: Nonlinear Phenomena''==
''Physica D'' was created in 1980 as an expansion of the ''Physica'' series. It is concerned with [[nonlinear physics]] and [[nonlinear phenomena]] in general.

''Physica D'' is published by [[Elsevier]]/[[North-Holland]] on a bimonthly basis (24 times a year).

==''Physica E: Low-dimensional Systems and Nanostructures''==

''Physica E'' was created in 1998 as an expansion of the ''Physica'' series. It is concerned with [[nanostructure]]s and [[thin film]]s research, particularly the properties of [[quantum dot]]s, [[quantum well]]s, [[quantum wire]]s, and both bilayer and multilayer thin-film devices.

''Physica E'' is published by [[Elsevier]]/[[North-Holland]] 10 times a year.

==External links==
* [http://www.elsevier.com/locate/physa ''Physica A'' website]
* [http://www.elsevier.com/locate/physb ''Physica B'' website]
* [http://www.elsevier.com/locate/physc ''Physica C'' website]
* [http://www.elsevier.com/locate/physd ''Physica D'' website]
* [http://www.elsevier.com/locate/physe ''Physica E'' website]

[[Category:Elsevier academic journals]]
[[Category:Physics journals]]
[[Category:Publications established in 1934]]