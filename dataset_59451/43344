<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name = MQ-8C Fire Scout
 |image = File:131031-N-SW486-022.JPG
 |caption = MQ-8C
}}{{Infobox aircraft type
 |type = UAV [[helicopter]]
 |manufacturer = [[Northrop Grumman]] <br> [[Bell Helicopter]]
 |designer = <!-- only appropriate for single designers, not project leaders or companies -->
 |first flight = 2013  
 |introduction = 2016 (planned)
 |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
 |status = Development testing
 |primary user = [[United States Navy]]
 |more users = 
 |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
 |number built = 96 (planned as of 2015)
 |program cost= $3,060.6m<ref name=GAO-15-342SP /> <small>(FY15)</small> (includes MQ-8B)
 |unit cost =  $18.0 million<ref name=GAO-15-342SP>{{cite web | url=http://www.gao.gov/assets/670/668986.pdf#page=125 | pages=117–8 | title= GAO-15-342SP DEFENSE ACQUISITIONS Assessments of Selected Weapon Programs | publisher=US Government Accountability Office | date=March 2015 | accessdate=15 July 2015 }}$1726.1m remains to be spent on procurement with 96 airframes to be acquired which will all be MQ-8C, the total budget mixes in cheaper MQ-8B's.</ref> <small>(FY15)</small> (ex R&D)
 |developed from = [[Bell 407]] <br> [[Northrop Grumman MQ-8 Fire Scout]]
 |variants with their own articles = 
 |developed into= 
}}
|}

The '''Northrop Grumman MQ-8C Fire Scout''' (known as the '''Fire-X''' during development) is an [[UAV|unmanned]] autonomous [[helicopter]] developed by [[Northrop Grumman]] for use by the [[United States Navy]]. The MQ-8C is designed to provide reconnaissance, [[situation awareness|situational awareness]], aerial fire support and precision targeting support for ground, air and sea forces. The MQ-8C airframe is based on the [[Bell 407]], while the avionics and other systems are developed from those used on the [[Northrop Grumman MQ-8 Fire Scout|MQ-8B Fire Scout]]. It first flew in October 2013 and is currently in testing, with production scheduled to start in March 2016.<ref name=GAO-15-342SP />

==Design and development==
[[File:Northrop Grumman MQ-8C Fire Scout on display at the 2015 Australian International Airshow.jpg|thumb|MQ-8C Fire Scout on display at the [[Australian International Airshow|2015 Australian International Airshow]] ]]

On 3 May 2010, Northrop announced plans to fly a [[Bell 407]] helicopter modified with autonomous controls from the MQ-8B.  Named Fire-X, it was to demonstrate an unmanned cargo resupply capability to the US Navy.<ref>[http://www.flightglobal.com/news/articles/pictures-northrop-offers-fire-x-concept-for-unmanned-341471/ "Northrop offers Fire-X concept for unmanned resupply"]. Flightglobal.com, May 4, 2010.</ref> The unmanned Fire-X completed its first flight at [[Yuma Proving Ground]] in [[Arizona]] on 20 December 2010.<ref>[http://www.flightglobal.com/news/articles/fire-x-first-flight-revives-teams-bid-for-cargo-uas-market-351009/ "Fire-X first flight revives team’s bid for cargo UAS market"]. Flightglobal.com, December 16, 2010.</ref> On 23 April 2012, Northrop received a $262.3 million contract from the Navy to build the newly designated MQ-8C Fire Scout; the work included two developmental aircraft and six low-rate production aircraft initially. The Navy wants 28 MQ-8Cs for naval special operations forces.<ref>[http://www.flightglobal.com/news/articles/northrop-contracted-to-build-new-firescout-variant-371014/ "Northrop contracted to build new Firescout variant"]. Flightglobal.com, April 24, 2012.</ref> In March 2013, the Navy incorporated the [[Allison Model 250|Rolls-Royce 250-C47E]] engine into the MQ-8C for a 5 percent increase in "hot and high" power, 2 percent reduced fuel consumption, 8 percent increase in rated takeoff power, and better reliability.<ref>[http://www.rolls-royce.com/news/press_releases/2013/04032013_military_helicopters.jsp Rolls-Royce Launches Latest M250 Engine Variant] - Rolls-Royce.com, March 4, 2013</ref> The Bell 407-based MQ-8C has an endurance of 12 hours, a range of {{convert|150|nmi|mi km|abbr=on}}, and a payload capacity of about {{convert|318|kg|lb|abbr=on}};<ref>[http://www.flightglobal.com/news/articles/video-mq-8c-flies-from-usn-destroyer-407449/ VIDEO: MQ-8C flies from USN destroyer] - Flightglobal.com, December 24, 2014</ref> it has twice the endurance and three times the payload as the MQ-8B.<ref name="navyreco23dec14">[http://www.navyrecognition.com/index.php?option=com_content&task=view&id=2285 Northrop Grumman MQ-8C Fire Scout VTOL UAV completes first ship-based test period with US Navy] - Navyrecognition.com, 23 December 2014</ref>

In early July 2013, Northrop Grumman delivered the first MQ-8C to the Navy. Ground testing was done to ensure that the systems worked properly and communicated with the ground control station prior to conducting the first flight. The MQ-8C shares software, avionics, payloads, and ship ancillary equipment with the MQ-8B.<ref>[http://www.northropgrumman.com/MediaResources/Pages/NewsArticle.aspx?art=http://www.irconnect.com/noc/press/xml/nitf.html?d=10041122 First Upgraded MQ-8C Fire Scout Delivered to U.S. Navy] - Northrop Grumman press release, July 19, 2013</ref> The MQ-8C was expected to fly in early October 2013, and be deployed in late 2014. The [[APKWS]] II will be added to the C-model sometime after 2016.<ref name="shortly">[http://www.flightglobal.com/news/articles/auvsi-northrop-close-to-completing-firescout-weapon-tests-389492/ Northrop close to completing Firescout weapon tests] - Flightglobal.com, 14 August 2013</ref><ref name=Fire_Scout_Afghan_ends>[http://www.militarytimes.com/article/20130816/NEWS04/308160001/Fire-Scout-ends-Afghan-mission-future-includes-new-variant-LCS-work Fire Scout ends Afghan mission; future includes new variant, LCS work] - Militarytimes.com, August 16, 2013</ref> On 24 September 2013, the MQ-8C Fire-X delivered to the Navy turned on its engines for 10 minutes in preparation for first flight. A second MQ-8C was to be delivered on 30 September. First flight was scheduled for early to mid-October, although the exact date was not determined, as such tests are often delayed by minor system problems. The MQ-8C flight test regime is to last six months.<ref>[http://www.flightglobal.com/news/articles/northrop-grumman-mq-8c-makes-first-engine-runs-390870/ Northrop Grumman MQ-8C makes first engine runs] - Flightglobal.com, September 24, 2013</ref>

The MQ-8C Fire Scout first flew on 31 October 2013. It flew for 7 minutes in restricted airspace using autonomous controls at [[Naval Base Ventura County]]. It flew a second time hours later that day to an altitude of 500&nbsp;ft. The MQ-8C was jointly operated by Northrop Grumman and the Navy.<ref>[http://www.flightglobal.com/news/articles/video-mq-8c-fire-scout-completes-first-flight-392469/ VIDEO: MQ-8C Fire Scout completes first flight] - Flightglobal.com, November 1, 2013</ref> Northrop Grumman delivered the second MQ-8C on 25 November 2013. They are under contract to build 14 helicopters.<ref>[http://www.flightglobal.com/news/articles/navy-receives-second-mq-8c-393491/ Navy receives second MQ-8C] - Flightglobal.com, November 25, 2013</ref> The second MQ-8C flew on 12 February 2014.  The aircraft had flown 66 hours by February 2014.<ref>[http://www.navyrecognition.com/index.php?option=com_content&task=view&id=1565 Second Northrop Grumman MQ-8C Fire Scout VTOL UAV takes flight] - Navyrecognition.com, February 13, 2014</ref> On 10 March 2014, the MQ-8C reached 100 flight hours.<ref>[http://www.suasnews.com/2014/03/28075/mq-8c-fire-scout-reaches-100-flight-hours/ MQ-8C Fire Scout reaches 100 flight hours] - sUASNews.com, March 17, 2014</ref> 19 C-model Fire Scouts are on order with two in flight testing;<ref name="flightglobal4april14">[http://www.flightglobal.com/news/articles/navy-orders-five-more-mq-8cs-397859/ Navy orders five more MQ-8Cs] - Flightglobal.com, April 4, 2014.</ref> the first deployment on an LCS is scheduled for 2015.<ref name="CNOPR2014">{{cite web | format=pdf | url=http://www.navy.mil/cno/docs/141104_PositionReport.pdf | title=CNO's Position Report: 2014 | publisher=US Navy | date=November 4, 2014 | accessdate=November 26, 2014}}</ref> The MQ-8C began testing aboard the [[destroyer]] {{USS|Jason Dunham|DDG-109|2}}  on 16 December 2014, executing 22 landings and recoveries in less than four hours.<ref name="ndm17dec14">[http://www.nationaldefensemagazine.org/blog/Lists/Posts/Post.aspx?ID=1702 Navy to Start Competition for New Fire Scout Radar] - Nationaldefensemagazine.org, 17 December 2014</ref> Testing was completed on 19 December, executing 32 takeoffs and recoveries over three flights.<ref name="navyreco23dec14"/><ref>[http://www.flightglobal.com/news/articles/video-mq-8c-flies-from-usn-destroyer-407449/ Video]</ref>

Northrop Grumman flew the MQ-8C demonstrator installed with their [[STARLite Radar|AN/ZPY-1 STARLite Radar]], although there was no requirement for an MQ-8C radar at the time;<ref name="an/zpy-4">[http://www.ainonline.com/aviation-news/ain-defense-perspective/2013-01-25/surveillance-radar-selected-unmanned-mq-8b-fire-scouts Surveillance Radar Selected for Unmanned MQ-8B Fire Scouts] - Ainonline.com, 25 January 2013</ref> the Navy began seeking information for a radar for the MQ-8C in July 2014 with surface search, [[synthetic aperture radar]], [[inverse synthetic aperture radar|inverse SAR]], and weather mode capabilities.<ref>[http://www.flightglobal.com/news/articles/new-radar-sought-for-mq-8c-401845/ New radar sought for MQ-8C] - Flightglobal.com, July 22, 2014</ref> Although the AN/ZPY-4 has been installed on some B-model Fire Scouts, the larger C-model can accommodate a larger and more powerful radar.<ref>[http://breakingdefense.com/2014/12/fire-scout-grows-up-drone-getting-radar-rockets-2016-ioc/ Fire Scout Grows Up: Drone Getting Radar, Rockets, 2016 IOC] - Breakingdefense.com, December 17, 2014</ref> The MQ-8C will be ready to perform surface warfare missions in 2018 and mine countermeasure missions in 2020.<ref>[http://www.shephardmedia.com/news/uv-online/avalon-2015-rocket-firings-planned-fire-scout/ Rocket firings planned for Fire Scout] - Shephardmedia.com, February 24, 2015</ref>  The aircraft's first deployment is expected for 2016 to give [[Littoral Combat Ship]]s a {{convert|50|nmi|mi km|abbr=on}}-radius [[ISTAR|ISR]] capability.  The Fire Scout program office is considering whether to equip the airframe itself to perform more missions or focus on manned-unmanned teaming with larger [[MH-60 Seahawk|MH-60S/R Seahawk]] helicopters.<ref>[https://www.flightglobal.com/news/articles/navy-seeks-maritime-radar-for-northrop-mq-8c-fire-sc-416435/ Navy seeks maritime radar for Northrop MQ-8C Fire Scout] - Flightglobal.com, 4 September 2015</ref> On 26 May 2016, NAVAIR signed a contract for the Osprey 30 lightweight AESA radar for the MQ-8C, the first system of its type to offer full spherical coverage with no moving parts;<ref>[https://www.flightglobal.com/news/articles/osprey-radar-selected-for-mq-8c-fire-scout-426250/ Osprey radar selected for MQ-8C Fire Scout] - Flightglobal.com, 10 June 2016</ref> the 360-degree, {{convert|50|kg|lb|abbr=on}} airborne radar uses fixed panels distributed around the body of aircraft, mounting antennas weighing just over {{convert|11|kg|lb|abbr=on}} each.<ref>[http://www.defensenews.com/story/defense/naval/naval-aviation/2016/06/10/fire-scout-drone-uav-radar/85690866/ Italian Radar To Equip US Navy's Fire Scout Drones] - Defensenews.com, 10 June 2016</ref>

The first operational MQ-8C was delivered to the US Navy in December 2014.<ref name=navdrn-8Cdeliver>{{cite web |title=Northrop Grumman Delivers First Operational MQ-8C Fire Scout to the US Navy |url=http://www.navaldrones.com/Fire-X.html |website=navaldrones.com |accessdate=April 13, 2015 |date=December 3, 2014}}</ref> Its final developmental flight was completed on 29 April 2015, after 450 hours completed in 327 flights.<ref>[http://www.navair.navy.mil/index.cfm?fuseaction=home.NAVAIRNewsStory&id=5902 Photo release: MQ-8C Fire Scout completes developmental flight test] - NAVAIR.Navy.mil, 4 May 2015</ref> In August 2015, Northrop Grumman demonstrated the MQ-8C's endurance with an 11-hour flight.<ref>[http://www.deagel.com/news/US-Navy-MQ-8C-Fire-Scout-Demonstrates-Flight-Endurance-of-More-Than-Ten-Hours_n000014386.aspx US Navy MQ-8C Fire Scout Demonstrates Flight Endurance of More Than Ten Hours] - Deagel.com, 25 August 2015</ref> On 20 November 2015, the MQ-8C completed a 3-week operational assessment period to assess system performance, endurance, and reliability of the unmanned helicopter over 83.4 hours in 11 flights.  To date, the aircraft logged 730 flight hours over 427 flights.  Ship-based testing is expected to begin in 2017.<ref>[http://www.navyrecognition.com/index.php?option=com_content&task=view&id=3291 U.S. Navy MQ-8C Fire Scout VTOL UAV Completes Operational Assessment] - Navyrecognition.com, 1 December 2015</ref>

==Operators==
;{{USA}}
*[[United States Navy]]

==Specifications==
[[File:Differences between the MQ-8B and MQ-8C.jpg|thumb|Size and performance differences between the Fire Scout and Fire-X drones]]

{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[WT:Air]] -->
|jet or prop?=prop
|plane or copter?=copter
|ref=Northrop Grumman,<ref>[http://www.northropgrumman.com/Capabilities/FireX/Documents/Fire-X_Brochure.pdf "MQ-8C Fire-X Data Sheet"] ''Northrop Grumman''. Retrieved 22 April 2015.</ref>
|crew=0
|payload main= 2,950 lb
|payload alt= 1,338 kg
|length main= 34.7 ft
|length alt= 10.6 m
|height main= 10.9 ft
|height alt= 3.3 m
|span main= 36.6 ft
|span alt= 11.2 m
|empty weight main= 
|empty weight alt= 
|max takeoff weight main= 6,000 lb
|max takeoff weight alt= 2,721 kg

|engine (prop)= [[Allison Model 250|Rolls-Royce 250-C47B]]
|number of props= 1
|power main= 813 shp
|power alt= 606 kW
|max speed main= {{convert|140|kn|km/h}}
|cruise speed main= 
|combat radius main= 
|combat radius alt= 
|combat radius more= 
|endurance= 15 hours maximum
|ceiling main= 20,000 ft
|ceiling alt= 6,100 m
}}

==See also==
{{aircontent
|see also=
|related=
* [[Bell 407]]
* [[Northrop Grumman MQ-8 Fire Scout]]
|similar aircraft=
|lists=
*[[List of active United States military aircraft]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Northrop Grumman MQ-8 Fire Scout}}
* [http://www.northropgrumman.com/Capabilities/FireScout/Pages/default.aspx MQ-8C Fire Scout page on northropgrumman.com]
* [http://www.northropgrumman.com/Capabilities/FireX/Pages/default.aspx MQ-8C Fire-X page on northropgrumman.com]
* [http://www.navair.navy.mil/index.cfm?fuseaction=home.display&key=8250AFBA-DF2B-4999-9EF3-0B0E46144D03 US Navy MQ-8 Fire Scout page]

{{Grumman aircraft}}
{{Bell Aircraft}}
{{US unmanned aircraft}}

{{DEFAULTSORT:MQ-08 Fire Scout}}
[[Category:Unmanned helicopters]]
[[Category:Unmanned aerial vehicles of the United States]]
[[Category:United States military reconnaissance aircraft 1990–1999|Northrop Grumman MQ-8 Fire Scout]]
[[Category:Airborne military robots]]
[[Category:Northrop Grumman aircraft|Q-008 Fire Scout]]
[[Category:United States helicopters 2000–2009]]
[[Category:Single-turbine helicopters]]