{{Use dmy dates|date=October 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= H160
 |image= File:H160 mockup at Dubai Airshow 2015.JPG
 |caption=H160 mockup at Dubai Airshow 2015
}}{{Infobox aircraft type
 |type= Medium [[utility helicopter]]
 |national origin = Multinational
 |manufacturer= [[Airbus Helicopters]]
 |designer=
 |first flight= 13 June 2015
 |introduced= 
 |retired=
 |status= Under development
 |primary user= 
 |more users= 
 |produced=
 |number built=
 |unit cost= 
 |developed from=
 |variants with their own articles= 
}}
|}

The '''Airbus Helicopters H160''' (formerly '''X4''') is a medium utility helicopter being developed by [[Airbus Helicopters]]. Formally launched at [[Heli-Expo]] in [[Orlando, Florida]] on 3 March 2015, it is intended to replace the [[Eurocopter AS365 Dauphin|AS365]] and [[Eurocopter EC155|EC155]] models in the firm's lineup. In June 2015, the first test flight took place; the first customer deliveries were expected in 2018<ref name=osb1>{{cite news |url=http://aviationweek.com/business-aviation/airbus-removes-veil-h160-project |title=Airbus Removes Veil From H160 Project  |work=[[Aviation Week & Space Technology]] |first=Tony |last=Osborne |date=3 March 2015 |accessdate=4 March 2015 |archiveurl=https://web.archive.org/web/20150304154118/http://aviationweek.com/business-aviation/airbus-removes-veil-h160-project |archivedate=4 March 2015 |deadurl=no}}</ref> and are now slipped to 2019.<ref>[http://www.verticalmag.com/news/development-h160-performance-airbus-says/ In-development H160 is on performance, Airbus says]</ref>

==Development==
The Airbus Helicopters H160 was first revealed to the public in 2011, at which point it was referred to by company representatives by the designation X4 – a designation which implied it to be a follow-on from the [[Eurocopter X3]], a high-speed hybrid helicopter technology demonstrator. Speaking in early 2011, Eurocopter (later renamed as Airbus Helicopters) chief Lutz Bertling declared that the X4 would be a "game changer", contrasting significance of the innovations it would feature with Airbus' development of [[fly-by-wire]] controls.<ref name = "x4 early prom">Thisdell, Dan. [http://www.flightglobal.com/news/articles/eurocopter-x4-promises-technical-tour-de-force-353663/ "Eurocopter X4 promises technical tour-de-force."] ''Flight International'', 1 March 2011.</ref> Early features alluded to included ''Blue Edge'' active tracking rotor blades, advanced pilot assistance functionality, and reduced vibration to "near-jet" levels of smoothness.<ref name = "x4 early prom"/> The X4 was also described as having a "radically different" cockpit, Bertling stating that "The cockpit as we know it today will not be there". It was also announced that the X4 would be introduced in two stages: an interim model in 2017 with some of the advanced features absent, and a more advanced model following in 2020.<ref>Quick, Darren. [http://www.gizmag.com/eurocopter-x4/21447/ "Eurocopter plans next-generation X4 helicopter."] ''gizmag'', 13 February 2012.</ref>

The development program for the X4 cost €1&nbsp;billion ($1.12&nbsp;billion), even after numerous cutting-edge features, including proposed highly advanced control systems, were toned down or eliminated as too risky or costly.<ref name=osb1/><ref name = "alpha"/> On 3 March 2015, the X4 was formally unveiled under the H160 designation. It has been marketed as a successor to the company's existing [[Eurocopter AS365 Dauphin]] and competes with the [[AgustaWestland AW139]], [[Sikorsky S-76]] and [[Bell 412]];<ref name=osb1/><ref name = "ain unveil"/> Guillaume Faury, Airbus Helicopters Chief Executive, referred to the H160 as being "the AW139 killer".<ref name = "fg target aw139">Perry, Dominic. [http://www.flightglobal.com/news/articles/airbus-helicopters-targets-aw139-with-new-h160-409630/ "Airbus Helicopters targets AW139 with new H160"] ''Flight International'', 3 March 2015.</ref><ref name = "alpha">[http://seekingalpha.com/article/3553336-airbus-helicopters-h160-the-evolutionary-successor "Airbus Helicopters H160: The Evolutionary Successor."] ''Seeking Alpha'', 6 October 2015.</ref> The H160 began Airbus Helicopter's re-branded naming convention; starting 1 January 2016, helicopters in the same range shall bear the 'H' designation, resembling how Airbus names their commercial aircraft.<ref name="shephard3march15"/>

A design emphasis was placed on using Airbus' production and support techniques as used for advanced fixed-wing aircraft such as the [[Airbus A350]] passenger jet;<ref>{{citation |first=Dominic |last=Perry |url=http://www.flightglobal.com/news/articles/revolution-underpins-new-h160-helicopter-409631/ |title= Revolution underpins new H160 helicopter |work=[[Flightglobal]] |publisher=Reed Business Information |date=3 March 2015 |accessdate=4 March 2015 |archive-url= |archive-date= |deadurl=no}}</ref> accordingly, a pair of test rigs were built to separately test the dynamic and system elements respectively to speed up the design process.<ref name = "alpha"/><ref name = "ain unveil"/> On 29 May 2015, the first H160 prototype was unveiled at Airbus Helicopters' [[Marignane]] facility in France.<ref>[http://www.verticalmag.com/news/article/AirbusHelicoptersunveilsH160prototype "Airbus Helicopters unveils H160 prototype."] ''Vertical Magazine'', 1 July 2015.</ref> On 13 June 2015, the first prototype performed its first flight from Marignane.<ref>{{cite journal|journal=[[Flight International]]|title="Airbus Helicopters lifted by debut as H160 flight test get off the ground|date=23 June 2015|issn=0015-3710}}</ref> By November 2015, the flight envelope of the prototype had been progressively expanded, having attained a maximum altitude of {{convert|15,000|ft}} and a maximum speed of {{convert|175|kn}} so far.<ref>[http://aviationweek.com/dubai-air-show-2015/airbus-h160-makes-dubai-show-debut "Airbus H160 Makes Dubai Show Debut."] ''Aviation Week'', 8 November 2015.</ref> On 18 December 2015, the second prototype, the first H160 to be equipped with Turbomeca Arrano engines, performed its initial ground run; its maiden flight took place in 27 January 2016.<ref>[http://www.emergency-live.com/en/hotnews/airbus-helicopters-second-h160-prototype-takes-off/ "Airbus Helicopters’ second H160 prototype takes off."] ''Emergency Live'', 27 January 2015.</ref><ref>[http://www.aeronewstv.com/en/industry/helicopteres/3120-h160-helicopter-flight-test-programme-moves-up-a-notch.html "Video - H160 helicopter flight test programme moves up a notch."] ''aeronewstv.com'', 5 February 2016.</ref>

In November 2015, it was announced that certification of the H160 and its entry into service with civil operators were both scheduled to take place in 2018; a military-orientated variant, designated H160M, is aimed to enter service in 2022.<ref name = "nov 2017">Perry, Dominic. [https://www.flightglobal.com/news/articles/dubai-airbus-helicopters-speeds-progress-on-h160-418882/ "DUBAI: Airbus Helicopters speeds progress on H160."] ''Flight International'', 10 November 2015.</ref> In March 2016, H160 program chief Bernard Fujarski stated that nearly all milestones set for 2015 had been achieved, save for the second prototype's first flight having being delayed to January 2016, and that the H160’s aerodynamic configuration had been validated.<ref name = "test prog march16">Dubois, Thierry. [http://www.ainonline.com/aviation-news/aerospace/2016-03-01/h160-flight-testing-benefitting-new-tools "H160 Flight Testing Benefitting From New Tools."] ''AIN Online'', 1 March 2016.</ref> Testing had revealed a need to relocate some electronics systems to the rotorcraft's nose from the rear portion of the main fuselage for [[center of gravity]] reasons, while the fenestron shrouded tail rotor had reportedly exceeded performance expectations.<ref name = "test prog march16"/>

In 2015, Airbus estimated that there was a market for 120–150 airframes annually; the company intends to initially ramp up the manufacturing rate to roughly 50 aircraft per year.<ref name="shephard3march15"/><ref name = "nov 2017"/> In July 2016, it was announced that the aeromechanical configuration of the rotorcraft had been frozen.<ref name = "design freeze">Perry, Dominic. [https://www.flightglobal.com/news/articles/h160-configuration-frozen-but-first-delivery-may-sl-427968/ "H160 configuration frozen, but first delivery may slip."] ''Flight International'', 28 July 2016.</ref> Manufacturing of the first production H160 is scheduled for 2017; the first delivery may take place in late 2018 or early 2019.<ref name = "design freeze"/> A new production scheme, drawing inspiration from the automotive industry, is to be implemented in the H160's construction; it is a stated aim for the final assembly lead time of the new rotorcraft to be half of that of the preceding Dauphin.<ref name = "test prog march16"/>

In March 2017, French defence minister [[Jean-Yves Le Drian]] announced at Marignane that the H160 was selected as the basis for its tri-service light rotorcraft replacement programme, the hélicoptère interarmées léger (HIL), with between 160 and 190 required from 2024 to replace 420 aircraft : [[French navy]]’s [[Alouette III]]s, [[SA 365 Dauphin]] and [[Eurocopter AS565 Panther|AS565 Panther]], the [[French Air Force]]’s [[Eurocopter Fennec|AS555 Fennec]] and [[SA330 Puma]]s; and the [[French army]] Pumas, [[Aérospatiale Gazelle|SA341/SA342 Gazelle]]s and Fennec.<ref>{{cite news |url= https://www.flightglobal.com/news/articles/france-selects-h160-for-light-helicopter-replacement-434790/ |title= France selects H160 for light helicopter replacement programme |date= 3 March 2017 |work= Flight Global}}</ref>

==Design==
[[File:H160 Tail.JPG|thumb|H160 tail showing biplane stabiliser, canted fenestron and rotor tip]]
[[File:H160 rotor hub and exhaust.JPG|thumb|H160 rotor hub and exhaust]]
The Airbus Helicopters H160 takes advantage of several advanced manufacturing technologies and materials in order to produce a lighter, more efficient design. One weight-saving measure was the replacement of conventional [[hydraulic]] [[landing gear]] and brakes with electrical counterparts, the first helicopter in the world to do so; according to Airbus the elimination of hydraulic components makes the rotorcraft both lighter and safer.<ref name = "alpha"/><ref name = "ain unveil"/> In 2015, Airbus claimed that the all-composite H160 would deliver the same basic performance as the rival [[AgustaWestland AW139]] while being a ton lighter, having a lower fuel consumption and offering 15–20% lower direct operating costs.<ref name=osb1/> The composite fuselage also provided for greater design freedom of the rotorcraft's external styling.<ref name = "alpha"/> During the aircraft's development, features such as full [[De-ice#Electric systems|de-icing]] equipment and a [[fly-by-wire]] control system, were deemed too heavy or costly for the benefits involved and were eliminated.<ref name=osb1/>

The H160 is the first rotorcraft to feature the Blue Edge five-bladed main rotor. This incorporates a double-swept shape that reduces the noise generation of blade-[[vortex]] interactions (BVI), a phenomenon which occurs when the blade impacts a vortex created at its tip, resulting in a 3–4&nbsp;dB noise reduction and raising the effective payload by {{convert|100|kg|lb|abbr=on}}.<ref name = "ain unveil"/><ref name="shephard3march15">Skinner, Tony and Beth Maundrill. [http://www.shephardmedia.com/news/rotorhub/heli-expo-2015-introducing-h160/ "Heli-Expo 2015: Introducing the H160."] ''Shephard'', 3 March 2015.</ref> Aerodynamic innovations include a [[biplane]] [[tailplane]] stabiliser for greater low speed stability, and a quieter canted [[fenestron]] (originated on the 1967 Aerospatiale SA.340) which combined produce an extra 80&nbsp;kg of lift.<ref name=osb1/> The H160 is the first civilian helicopter to utilise a canted fenestron anti-torque tail rotor.<ref name = "alpha"/> The H160 will be powered by two [[Turbomeca Arrano]] [[turboshaft]] engines; a second engine, the [[Pratt & Whitney Canada PW200|Pratt & Whitney Canada PW210E]], was to be offered as an alternative option, but this was eliminated due to insuffient power output and to reduce design complexity.<ref>Perry, Dominic. [http://www.flightglobal.com/news/articles/airbus-helicopters-drops-pw210-engine-on-39repositioned39-409180/ "Airbus Helicopters drops PW210 engine on 'repositioned' X4."] ''Flight International'', 18 February 2015.</ref><ref name = "alpha"/> A redundant backup for the [[gearbox]] lubrication system enables in excess of five hours of flight following a primary failure without causing mechanical damage.<ref>{{citation |first=Dominic |last=Perry |url=http://www.flightglobal.com/news/articles/new-h160-helicopter-is-first-of-the-39h-generation39-409632/ |title= New H160 helicopter is first of the 'H generation' |work=[[Flightglobal]] |publisher=Reed Business Information |date=3 March 2015 |accessdate=4 March 2015 |deadurl=no}}</ref>

In early 2015, Airbus claimed that the H160 is intended to have a "day one" availability rate expectation exceeding 95 percent. The H160 features the Helionix avionics suite, the cockpit being equipped with a total of four {{convert|6|x|8|in|cm|abbr=on}} multifunctional displays. Airbus Helicopters collaborated with [[CMC Electronics|Esterline CMC]] to develop portions of the avionics, such as the CMA-9000 Flight Management System and the CMA-5024 [[GPS]] landing system sensor, to automate the landing process.<ref>Bellamy III, Woodrow. [http://www.aviationtoday.com/av/topstories/Airbus-Esterline-CMC-Avionics-Power-New-H160_84398.html#.VvmMYG_2aUk "Airbus, Esterline CMC Avionics Power New H160."] ''Avionics Magazine'', 5 March 2015.</ref> A full flight simulator has also been developed for the H160, in partnership with Helisim and [[Thales Group]].<ref>[https://www.thalesgroup.com/en/worldwide/aerospace/press-release/airbus-helicopters-selects-thales-and-helisim-its-h160-full-flight "Airbus Helicopters selects Thales and Helisim for its H160 full flight simulator deployment."] ''Thales Group'', 10 February 2016.</ref> The avionics provide a level of commonality with the company's earlier [[Eurocopter EC145|Eurocopter EC145 T2]] and [[Eurocopter EC175]] helicopters. Commentators have stated that this commonality to be similar to AgustaWestland's rotorcraft family concept.<ref name="shephard3march15"/><ref name = "nov 2017"/>

==Variants==
;H160
:Base civil-orientated model.<ref name = "nov 2017"/>
;H160M
:Base military-orientated model.<ref name = "nov 2017"/>

==Specifications (H160)==
[[File:H160 cockpit mockup.JPG|thumb|H160 cockpit mockup]]
{{Aircraft specs
|ref=Data from ''Shephard Media'',<ref name="shephard3march15"/> AIN Online<ref name = "ain unveil">Dubois, Thierry. [http://www.ainonline.com/aviation-news/business-aviation/2015-03-03/airbus-helicopters-unveils-h160-medium-twin "Airbus Helicopters Unveils H160 Medium Twin."] ''AIN Online'', 3 March 2015.</ref><!-- reference -->
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=12
|length m=
|length ft=
|length in=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|empty weight kg=4000
|empty weight lb=
|gross weight kg=5500
|gross weight note=to {{convert|6000|kg|abbr=on}}

|max takeoff weight kg=6000
|max takeoff weight lb=
|more general=, 1,100 kg max payload
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Turbomeca Arrano]]
|eng1 type=turboshaft engines
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|more power=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=175
|cruise speed kmh=296
|cruise speed mph=<!-- if max speed unknown -->
|cruise speed kts=
|range km=
|range miles=
|range nmi= 450 <!-- 120NMI MTOW, 450 search and rescue configuration with a 20 minute reserve -->
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|lift to drag=
|disk loading kg/m2
|disk loading lb/sqft=
|more performance=

|avionics=
}}

==See also==
{{Portal|Aviation}}
{{Aircontent
|see also=<!-- other relevant information -->
|similar aircraft=<!-- similar or comparable aircraft -->
* [[AgustaWestland AW139]]
* [[Sikorsky S-76]]
* [[Ka-60]]
|lists=<!-- related lists -->
}}

==References==
{{Reflist}}

==External links==
* [http://www.airbushelicopters.com/website/en/ref/H160_204.html Airbus Helicopters H160 official page]

{{Airbus Helicopters aircraft}}

[[Category:Airbus Helicopters aircraft]]
[[Category:French helicopters 2010–2019]]
[[Category:Twin-turbine helicopters]]