{{Superleague Formula club team
| team_name = Al Ain SF Racing Team
| file_name = Al-Ain-FC_logo.gif
| founded = 2008
| country = {{UAE}}
| drivers = [[Andreas Zuber]] <small>(2008)</small> <br> [[Bertrand Baguette]] <small>(2008)</small> <br> [[Paul Meijer]] <small>(2008)</small> <br> [[Dominick Muermans]] <small>(2008)</small> <br> [[Miguel Molina]] <small>(2009)</small> <br> [[Esteban Guerrieri]] <small>(2009)</small>
| first_race = [[2008 Donington Park Superleague Formula round|2008 Donington]]
| last_race = [[2009 Zolder Superleague Formula round|2009 Zolder]]
| events = 8
| championships = 0
| total_points = 379 <small>(including Super Finals)</small>
| current_season = 2010
| current_position = n/a
| current_points = n/a
| races = 16
| poles = 0
| wins = 2
| podiums = 2
| fls = 0
}}

'''Al Ain Superleague Formula team''' were a motor racing team representing the UAE's [[Al Ain Club]] in the [[Superleague Formula]] championship.

==2008 and 2009 seasons==
They finished 12th in the [[2008 Superleague Formula season|inaugural championship]] with drivers [[Andreas Zuber]], [[Bertrand Baguette]], [[Paul Meijer]] and [[Dominick Muermans]]. It was initially announced that [[Giorgio Pantano]] would drive for the team in the [[2009 Superleague Formula season|second SF season]], but it was then announced they would not return. However, days later, they were added to the official entry list once again and participated in the first 2 rounds of the season, represented by Spain's [[Miguel Molina]] in round 1 and Argentina's [[Esteban Guerrieri]] in round 2. However, despite winning the [[2009 Zolder Superleague Formula round|2nd race at Zolder]] with series debutant Guerrieri, they were replaced by [[Sevilla FC (Superleague Formula team)|Sevilla FC]] from round 3 of the championship. [[Ultimate Motorsport]] will service the returning Sevilla entry.

==Record==
([[Template:Superleague Formula driver results legend|key]])

===2008===
{| class="wikitable" style="font-size: 85%"
!rowspan=2| Operator(s)
!rowspan=2| Driver(s)
!colspan=2| 1
!colspan=2| 2
!colspan=2| 3
!colspan=2| 4
!colspan=2| 5
!colspan=2| 6
!rowspan=2| Points
!rowspan=2| Rank
|-
!colspan=2| [[2008 Donington Park Superleague Formula round|DON]]
!colspan=2| [[2008 Nürburgring Superleague Formula round|NÜR]]
!colspan=2| [[2008 Zolder Superleague Formula round|ZOL]]
!colspan=2| [[2008 Estoril Superleague Formula round|EST]]
!colspan=2| [[2008 Vallelunga Superleague Formula round|VAL]]
!colspan=2| [[2008 Jerez Superleague Formula round|JER]]
|-
|rowspan=4 align=center| [[Azerti Motorsport]]
| {{flagicon|UAE}} [[Andreas Zuber]]
|style="background:#dfffdf;"| 6
|style="background:#efcfff;"| 15
|style="background:#dfffdf;"| 11
|style="background:#dfffdf;"| 11
|
|
|
|
|
|
|
|
!rowspan=4| 244
!rowspan=4| 12th
|-
| {{flagicon|BEL}} [[Bertrand Baguette]]
|
|
|
|
|style="background:#dfffdf;"| 11
|style="background:#dfffdf;"| 10
|
|
|
|
|style="background:#dfffdf;"| 10
|style="background:#dfffdf;"| 7
|-
| {{flagicon|NED}} [[Paul Meijer]]
|
|
|
|
|
|
|style="background:#dfffdf;"| 12
|style="background:#ffffbf;"| 1
|
|
|
|
|-
| {{flagicon|NED}} [[Dominick Muermans]]
|
|
|
|
|
|
|
|
|style="background:#dfffdf;"| 14
|style="background:#dfffdf;"| 8
|
|
|}

===2009===
*Super Final results in 2009 did not count for points towards the main championship.
{| class="wikitable" style="font-size: 85%"
!rowspan=2| Operator(s)
!rowspan=2| Driver(s)
!colspan=3| 1
!colspan=3| 2
!colspan=3| 3
!colspan=3| 4
!colspan=3| 5
!colspan=3| 6
!rowspan=2| Points
!rowspan=2| Rank
|-
!colspan=3| [[2009 Magny-Cours Superleague Formula round|MAG]]
!colspan=3| [[2009 Zolder Superleague Formula round|ZOL]]
!colspan=3| [[2009 Donington Park Superleague Formula round|DON]]
!colspan=3| [[2009 Estoril Superleague Formula round|EST]]
!colspan=3| [[2009 Monza Superleague Formula round|MOZ]]
!colspan=3| [[2009 Jarama Superleague Formula round|JAR]]
|-
|rowspan=2 align=center| [[Ultimate Motorsport]]
| {{flagicon|ESP}} [[Miguel Molina]]
|style="background:#dfffdf;"| 9
|style="background:#dfffdf;"| 4
|style="background:#ffcfcf;"| X
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
!rowspan=2| 135
!rowspan=2| 19th
|-
| {{flagicon|ARG}} [[Esteban Guerrieri]]
|
|
|
|style="background:#dfffdf;"| 6
|style="background:#ffffbf;"| 1
|–
|
|
|
|
|
|
|
|
|
|
|
|
|}

==References==
{{Reflist}}

==External links==
* [http://www.superleagueformula.com/superleague/Clubs/Al-Ain Al Ain Superleague Formula team minisite]
* [http://www.alainclub.com/ Official Al Ain football club website]

{{Superleague Formula Al Ain}}
{{Superleague Formula football clubs}}
{{Al Ain FC}}

[[Category:Al Ain FC]]
[[Category:Superleague Formula club teams]]
[[Category:2008 establishments in the United Arab Emirates]]


{{UnitedArabEmirates-sport-stub}}