{{Infobox journal
| title = International Studies in Philosophy
| cover = [[File:instphilcover.gif]]
| editor = Stephen David Ross
| discipline = [[Philosophy]]
| language = English (some Italian, French, German)
| formernames = Studi Internazionali di Filosofia
| abbreviation = Int. Stud. Philos.
| publisher = [[Philosophy Documentation Center]]
| country = United States
| frequency = Quarterly
| history = 1969–present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.pdcnet.org/intstudphil
| link2 = http://www.pdcnet.org/intstudphil/toc
| link2-name = Online access
| link3 = http://www.pdcnet.org/studintfil/toc
| link3-name = Journal content, 1969–1973
| JSTOR = 
| OCLC = 2515233
| LCCN = 90-641577
| CODEN = 
| ISSN = 0270-5664
| eISSN = 2154-1809
}}
'''''International Studies in Philosophy''''' is a [[Peer review|peer-reviewed]] [[academic journal]], formerly published by the Center for Interdisciplinary Studies in Philosophy, Interpretation, and Culture, at [[Binghamton University]]. The journal began publishing under this title with volume 6 in 1974. Notable contributors included Joseph Agassi, Bruce Aune, Milton Fisk, Véronique Fóti, Rom Harré, Kathleen Higgins, François Lapointe, Alexander Nehemas, Kai Nielson, and Richard Schacht. The journal was established in 1969 by Augusto Guzzo and Giorgio Tonelli, and its first five volumes were published as ''Studi Internazionali di Filosofia''.

Publication of ''International Studies in Philosophy'' was suspended by the editor in 2008 with the completion of volume 40, issue 2. The journal was acquired by the [[Philosophy Documentation Center]] in 2011, and is being reorganized under its auspices. All published issues of the journal (under both titles) are available online.

== Abstracting and indexing ==
''International Studies in Philosophy'' has been abstracted and indexed in L'Année philologique, [[Arts & Humanities Citation Index]], [[Current Contents]]/Arts & Humanities, [[FRANCIS]], [[International Bibliography of Book Reviews of Scholarly Literature]], [[International Bibliography of Periodical Literature]], [[International Philosophical Bibliography]], [[Philosopher's Index]], [[Philosophy Research Index]], [[PhilPapers]], [[Scopus]], and [[TOC Premier]].

== See also ==
* [[List of philosophy journals]]

== External links ==
* {{Official website|http://www.pdcnet.org/intstudphil/International-Studies-in-Philosophy}}
* [http://www.pdcnet.org/pdc/bvdb.nsf/journal?openform&journal=pdc_studintfil ''Studi Internazionali di Filosofia'' online]

[[Category:Philosophy journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1969]]
[[Category:Multilingual journals]]
[[Category:Philosophy Documentation Center academic journals]]