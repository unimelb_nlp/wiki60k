{{featured article}}
{{Infobox video game
| title = Gravity Bone
| image = Gravity Bone cover.png
| developer = [[Blendo Games]]
| publisher = Blendo Games
| engine = [[Quake II engine|KMQuake2]]
| released = {{Video game release|WW|August 28, 2008}}
| designer = Brendon Chung
| composer = [[Xavier Cugat]]
| genre = [[Adventure game|Adventure]]
| modes = [[Single-player video game|Single-player]]
| platforms = [[Microsoft Windows]]
}}

'''''Gravity Bone''''' is a [[freeware]] [[First-person (video games)|first-person]] [[Adventure game|adventure]] [[video game]] developed by [[Blendo Games]], and released on August 28, 2008. The game employs a modified version of [[id Software]]'s [[id Tech 2]] engine—originally used for ''[[Quake 2]]''—and incorporates music from films by [[film director|director]] [[Wong Kar-wai]], which were originally performed by [[Xavier Cugat]]. Four incarnations of the game were produced during its one-year development; the first featured more common [[first-person shooter]] elements than the released version. Subsequent versions shifted in a new direction, with the inclusion of more spy-oriented gameplay.

''Gravity Bone'' received critical acclaim from [[Video game journalism|video game journalists]]. It was called "a pleasure to experience" by Charles Onyett from [[IGN]], and received comparisons to games such as ''[[Team Fortress 2]]'' and ''[[Portal (video game)|Portal]]''. The game was praised for its cohesive story, atmosphere and its ability to catch the player's interest over a very short time span without feeling rushed or incomplete. It received the "Best Arthouse Game" award in Game Tunnel's Special Awards of 2008. A sequel, ''[[Thirty Flights of Loving]]'', was released in 2012.

==Gameplay==
[[File:Gravity Bone (3).jpg|thumbnail|right|''Gravity Bone'' uses an interface with no [[HUD (video gaming)|heads-up display]] and provides all objectives and guidance through interactions with objects and environments.]]

''Gravity Bone'' is a [[First-person (video games)|first-person]] [[Adventure game|adventure]] video game that lasts around 20 minutes, and is set in the fictional city of Nuevos Aires. The player controls an unnamed spy, and is tasked with accomplishing several missions across the game's two [[level (video gaming)|stages]].<ref name="TIG"/> At the end of the game, the player-controlled spy is killed by an unknown woman after chasing her through the last half of the second level. The game was designed to leave the player without a clear idea of how the game's story evolves.<ref name="destructoid"/>

During these missions, objectives and guidance are provided through the player's interactions with objects and environments in the game. The tutorial system used to demonstrate routine gameplay elements such as object interaction and movement is disguised as the first level of ''Gravity Bone''. Here, the player is tasked with the delivery of a contaminated drink to an unspecified [[non-player character]]. After the first level is completed, the player is sent to the second level of the game, which follows the pattern of the first;<ref name=ign1/> the player is assigned a set of actions and goals involving [[Platforming game|platforming sequences]].<ref name="GamePlayer"/>

==Development==
[[File:Brendon Chung at GDC 2012.jpg|thumbnail|left|Brendon Chung, developer of ''Gravity Bone'', revealed that several versions of the game were made during one year of development.]]
''Gravity Bone'' was developed by Brendon Chung under his video game studio [[Blendo Games]]. Chung, who worked as a [[level designer]] for [[Pandemic Studios]], has contributed to the development of titles such as ''[[Full Spectrum Warrior]]'' and ''[[Lord of the Rings: Conquest]]''. Four incarnations of ''Gravity Bone'' were produced during its one-year development. Chung commented during an interview with FidGit that "''Gravity Bone'' started out very different from what it was and ended up getting scrapped ... so on and so forth until this version came out."<ref name="Dev1"/> The first version of ''Gravity Bone'' featured more typical first-person shooter elements than the released version, and was based on a series of ''[[Quake 2]]'' maps entitled Citizen Abel. He elaborated that the first version of the game had the player running around with a gun, "shoot[ing] things and stuff explodes." Development shifted in a new direction, and ''Gravity Bone'' was transformed; the player would act as a [[computer hacker]], "hacking stuff all the time."<ref name="Dev1"/>

Most of the original first-person shooter elements were removed by the third revision of the game, which incorporated a more spy-oriented style of gameplay, with the player "trying to quietly take out enemies and not be seen." Chung commented that he reworked the game several more times to fit his vision: "It kept on just changing and changing and changing until it got into a more story-oriented direction."<ref name="Dev1"/> He stated that he did not feel comfortable developing ''Gravity Bone'' as a first-person shooter game, and kept adding "bits and bits of more and more unconventional" elements as a result.<ref name="Dev1"/> He explained that he "got stuck on this idea of the hero never fires a gun, but he just has a bunch of tools on his belt, like a power drill or a can of pressurized [[Freon]], a [[screwdriver]]. I thought that was kind of funny and interesting."<ref name="Dev1"/>

''Gravity Bone'' was developed using a modified version of [[id Software]]'s [[id Tech 2]], the [[game engine|graphics engine]] for ''Quake 2''.<ref name="Dev1"/> Chung acknowledged that although he has worked with newer, "powerful and flexible" engines, he preferred the older engine because it was released as an [[open source platform]], "so you can redistribute it for free."<ref name="Dev1"/>  The voice work featured in the briefings in ''Gravity Bone'' was produced using [[text-to-speech]] programs, and the game incorporates three songs by [[Xavier Cugat|Xavier Cugat and His Orchesetra]]: "Maria Elena", "Brazil", and "Perfidia". These versions of "Maria Elena" and "Perfidia" were both previously used by [[film director]] [[Wong Kar-wai]] in the 1990 film ''[[Days of Being Wild]]''. Chung declared that his passion for Wong's films were an important factor in the selection of Wong's music: "He makes these really beautiful films and I've always wanted to use the same music in a videogame."<ref name="Dev1"/> He said that Wong's films had a strong influence on the development of the game.<ref name="Dev1"/>
{{clear}}

==Reception==
Charles Onyett from [[IGN]] applauded ''Gravity Bone'', saying that it is "a game that appears to toy with the notions of heroism and villainy, and the ways the player identifies with, and is directed toward, both roles."<ref name="ign1"/> He praised all aspects of the game, commenting, "the cohesiveness of its striking visual presentation, soundtrack and effects, and almost entirely incomprehensible story combine to create an atmosphere of peculiar strength."<ref name="ign1"/> Onyett concluded his preview of the game by stating, "it's a pleasure to experience, and never ceases to delight and surprise over its short run."<ref name="ign1"/> Anthony Burch from [[Destructoid]] gave a positive review, stating that it "is so stylistically unified, so consistently cool and weird and imaginative, that it's damn near impossible not to fall in love with—even as the game ends and you're wondering what the hell happened, and why."<ref name="destructoid"/> He also applauded several technical and design aspects of the game, expressing appreciation for the game's "stylistic choices", as well as the "nigh unbelievable" [[bloom (shader effect)|bloom effects]] featured in the game. Burch concluded that ''Gravity Bone'' is "a great ride", and that the "atmosphere and style alone will barrel you through to the journey's end, which comes all too soon."<ref name="destructoid"/>

Derek Yu from The Indie Games Source compared the game with ''[[Portal (video game)|Portal]]'' and stated that Chung was able to develop "an impeccable flair for graphic design" while manufacturing ''Gravity Bone''. He concluded that the game is "bursting with delicious color, and features blocky-headed characters that are infinitely more interesting to look at and interact with than the frightening Realdolls game players are often forced to contend with in modern FPS's."<ref name="TIG"/> Yu elaborated that it had "enough panache in its two levels to make it somewhat of an indie sleeper hit of the end of 2008."<ref name="TIG"/> An editor from The Refined Geek was pleased with ''Gravity Bone'' and its sequel, ''Thirty Flights of Loving'', awarding them each a score of 8 out of 10 and stating, "the enjoyment from these games comes from noticing all the subtle environmental clues and then using your imagination to draw the connecting dots."<ref name="Geek"/> The editor commented that both games highlight story elements over graphics and technical innovations, saying each game's "true strength comes from its ability to tell a story in the extremely short time frame."<ref name="Geek"/>

Kirk Hamilton of [[Kotaku]] praised the game, writing, "if you own a PC, you owe it to yourself to play ''Gravity Bone''." He said the game was "one of the coolest things I've played on PC lately."<ref name="Kotaku"/>  Kieron Gillen from [[Rock Paper Shotgun]] considered ''Gravity Bone'' to be an intellectual mix of ''[[Hitman (video game series)|Hitman]]'', ''[[The Operative: No One Lives Forever|No-one Lives Forever]]'', and ''[[Team Fortress 2]]'', stating that it is the "wittiest game" he has played since ''[[World of Goo]]''. Gillen applauded every aspect of the game, stating that ''Gravity Bone'' was an "indie [[art game]] whose main effect is to delight you at every turn."<ref name="Shotgun"/> It received the "Best Arthouse Game" award in Game Tunnel's Special Awards of 2008.<ref name=gametunnel/>

==Sequel==
{{main article|Thirty Flights of Loving}}
A sequel to ''Gravity Bone'', ''[[Thirty Flights of Loving]]'', was announced as a reward for contributing to the [[Idle Thumbs]] podcast revival [[Kickstarter]].<ref name="Hinkle"/><ref name="Hamilton"/> The game was released to Kickstarter backers in July 2012, and later offered as a purchasable title on Steam, which included ''Gravity Bone'' as an additional feature.<ref name="Idle16"/><ref name="steam release"/> The game, though not a direct sequel in story to ''Gravity Bone'', follows the main character in a heist with two other characters that goes very wrong. The title was critically acclaimed by reviewers, who called the very short but non-linear storytelling of ''Thirty Flights'' a novel use of the video game medium.<ref name="TTOL IGM"/>

== Legacy ==
Due to the [[source code]] availability under the [[GPL]] the game was later [[Source port|ported]] by game enthusiasts to other platforms, like [[Linux]] and the [[OpenPandora]] [[Handheld game console|handheld]].<ref>[https://github.com/ptitSeb/gravitybone-pandora gravitybone-pandora] on [[github.com]]</ref>

==References==
{{Reflist|colwidth=30em|refs=
<ref name="TTOL IGM">{{cite web|last=A.M.|first=Petey|title=The Why of Indie Games: ''Thirty Flights of Loving'' and ''Gravity Bone''|url=http://www.indiegamemag.com/the-why-of-indie-games-thirty-flights-of-loving-and-gravity-bone/|archiveurl=https://web.archive.org/web/20130325130446/http://www.indiegamemag.com/the-why-of-indie-games-thirty-flights-of-loving-and-gravity-bone/|archivedate=2013-03-25|work=Indie Game Magazine|publisher=Newton Publishing Limited|accessdate=January 22, 2013|date=October 2012}}</ref>

<ref name="steam release">{{cite web | url = http://www.polygon.com/gaming/2012/8/21/3257862/thirty-flights-of-loving-now-available-gravity-bone | title = ''Thirty Flights of Loving'' now available, includes ''Gravity Bone'' | work = [[Polygon (website)|Polygon]]|publisher=[[Vox Media]] | date = August 21, 2012 | accessdate  =January 30, 2013| first = Samit | last =  Sarkar }}</ref>

<ref name="Idle16">{{cite web | url = http://www.kickstarter.com/projects/idlethumbs/idle-thumbs-video-game-podcast/posts/266152 | title = Idle Thumbs Progresscast #16 | date = July 13, 2012 | accessdate = February 8, 2013 | publisher = [[Kickstarter]]}}</ref>

<ref name="Hamilton">{{cite web|author=Hamilton, Kirk |url=http://kotaku.com/5889082/indie-darling-gravity-bone-gets-a-sequel |title=Indie Darling ''Gravity Bone'' Gets a Sequel |work=[[Kotaku]]|publisher=Allure Media|date=February 28, 2012 |accessdate=March 8, 2012}}</ref>

<ref name="Hinkle">{{cite web|author=Hinkle, David |url=http://www.joystiq.com/2012/02/20/idle-thumbs-kickstarter-includes-exclusive-game-neat-artwork/ |archiveurl=https://web.archive.org/web/20121120071652/http://www.joystiq.com/2012/02/20/idle-thumbs-kickstarter-includes-exclusive-game-neat-artwork/ |archivedate=2012-11-20 |title=Idle Thumbs Kickstarter includes exclusive game, neat artwork |publisher=Joystiq |date=February 20, 2012 |accessdate=March 8, 2012}}</ref>

<!--<ref name="tigdb">{{cite web|url=http://db.tigsource.com/games/gravity-bone|title=''Gravity Bone'' - TIGdb|publisher=The Indie Game Database|accessdate=December 6, 2012}}</ref>-->

<ref name=ign1>{{cite web|last=Onyett|first=Charles|date=January 28, 2009|url=http://www.ign.com/articles/2009/01/29/gravity-bone-impressions|title=''Gravity Bone'' Impressions|work=[[IGN]]|publisher=[[News Corporation]]|accessdate=March 7, 2012}}</ref>

<ref name="TIG">{{cite web|last=Yu|first=Derek|date=January 2, 2009|url=http://www.tigsource.com/2009/01/02/gravity-bone/ |title=''Gravity Bone''|publisher=The Indie Games Source|accessdate=March 7, 2012}}</ref>

<ref name="gametunnel">{{cite web|last=Scarpelli|first=Michael|url=http://www.gametunnel.com/2008-special-awards-article.php|archiveurl=https://web.archive.org/web/20100407081827/http://www.gametunnel.com/2008-special-awards-article.php|archivedate=2010-04-07|title=2008 Special Awards|publisher=Game Tunnel|accessdate=March 4, 2012|date=December 28, 2008}}</ref>

<ref name="destructoid">{{cite web|last=Burch|first=Anthony|title=Indie Nation #45: ''Gravity Bone''|url=http://www.destructoid.com/indie-nation-45-gravity-bone-116838.phtml|work=[[Destructoid]]|publisher=ModernMethod|accessdate=November 3, 2012|date=January 4, 2009}}</ref>

<ref name="Geek">{{cite web|title=''Gravity Bone'' and ''Thirty Flights of Loving'': Hyper-Accelerated Story Telling|url=http://www.therefinedgeek.com.au/index.php/2012/08/24/gravity-bone-and-thirty-flights-of-loving-hyper-accelerated-story-telling/|publisher=The Refined Geek|accessdate=November 3, 2012|date=August 24, 2012}}</ref>

<ref name="Kotaku">{{cite web|last=Hamilton|first=Kirk|title=Take 15 Minutes To Play This Awesome Free PC Game|url=http://www.kotaku.com.au/2012/08/take-15-minutes-to-play-this-awesome-free-pc-game/|work=[[Kotaku]]|publisher=Allure Media|date=August 23, 2012|accessdate=November 3, 2012}}</ref>

<ref name="Dev1">{{cite web|last=Chick|first=Tom|title=The man behind the strange wonderful world of ''Gravity Bone''|url=http://fidgit.com/archives/2009/01/the-man-behind-the-strange-won.php|work=FidGit|publisher=Sci Fi|accessdate=November 15, 2012|archiveurl=https://web.archive.org/web/20090227075049/http://fidgit.com/archives/2009/01/the-man-behind-the-strange-won.php|archivedate=February 27, 2009|date=January 1, 2009}}</ref>

<ref name="Shotgun">{{cite web|last=Gillen|first=Kieron|title=We Are Spies, We Will Thrill You: ''Gravity Bone''|url=http://www.rockpapershotgun.com/2009/01/06/|publisher=[[Rock, Paper, Shotgun]]|accessdate=November 16, 2012|archiveurl=https://web.archive.org/web/20090210170240/http://www.rockpapershotgun.com/2009/01/06/we-are-spies-we-will-thrill-you-gravity-bone/|archivedate=February 10, 2009|date=January 6, 2009}}</ref>

<ref name="GamePlayer">{{cite web|last=Armstrong|first=Gary|title=''Gravity Bone''|url=http://www.game-and-player.com/articles/2009/02/gravity_bone.html|publisher=Game and Player|accessdate=November 15, 2012|archiveurl=https://web.archive.org/web/20090204002720/http://www.game-and-player.com/articles/2009/02/gravity_bone.html|archivedate=February 4, 2009|date=February 2, 2009}}</ref>
}}

==External links==
* [http://blendogames.com/older.htm Officially Gravity Bones page] on Blendo Games
{{Use mdy dates|date=March 2014}}

{{Blendo Games}}

[[Category:2008 video games]]
[[Category:Adventure games]]
[[Category:Art games]]
[[Category:Freeware games]]
[[Category:Single-player-only video games]]
[[Category:Video games developed in the United States]]
[[Category:Windows games]]
[[Category:Open-source video games]]