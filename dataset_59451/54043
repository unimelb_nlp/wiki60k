{{Infobox school
|name              = Annesley Junior School
|logo             = [[File:Annesley Junior School logo.jpg|220px]]
|motto_translation = Values Matter<ref name=Principal>{{cite web|url=http://www.annesley.sa.edu.au/admin/principal.htm |title=The Principal |accessdate=2008-02-10 |work=Administration |publisher=Annesley Junior School |deadurl=yes |archiveurl=https://web.archive.org/web/20071009224606/http://www.annesley.sa.edu.au/admin/principal.htm |archivedate=October 9, 2007 }}</ref>
|established       = 1902
|type              = [[Independent school|Independent]], [[Day school|Day]], Primary school, Early learning centre (Long daycare centre / child care centre / pre-school)
|denomination = [[Uniting Church in Australia|Uniting Church]]<ref name=UnitingChurch>{{cite web|url=http://www.sa.uca.org.au/site/page.cfm?u=886&print=1 |title=Annesley College |accessdate=2008-02-10 |year=2007 |work=UnitingCare South Australia |publisher=Uniting Church in Australia |deadurl=yes |archiveurl=https://web.archive.org/web/20080215204404/http://www.sa.uca.org.au/site/page.cfm?u=886 |archivedate=February 15, 2008 }} </ref>
|key_people        = [http://www.annesley.sa.edu.au/council.html Bruce Spangler] (Chair) [http://www.annesley.sa.edu.au/our-staff.html Cherylyn Skewes] (Principal)<ref>[http://www.annesley.sa.edu.au/our-staff.html Annesley Junior School - Our Staff<!-- Bot generated title -->]</ref>
|city              = [[Wayville, South Australia|Wayville]]
|state             = [[South Australia]]
|country           = Australia
|coordinates       = {{coord|34|56|31.37|S|138|35|54.22|E|type:edu_region:AU|display=inline,title}}
|enrolment         = 103<ref name=Mess0111>{{cite news|title=Annesley student numbers plummet|url=http://city-messenger.whereilive.com.au/news/story/annesley-numbers-plummet/|accessdate=19 January 2011|newspaper=City Messenger|date=18 January 2011}}</ref>
|num_employ        = 
|colours           = Maroon, navy and white<br>{{color box|#800000}} {{color box|#0B0B61}} {{color box|#FFFFFF}}
|website           = {{URL|http://www.annesley.sa.edu.au/}}
}}
'''Annesley Junior School''' is an [[Independent school|independent]] day school for girls and boys aged from two years old to year 6, located in [[Wayville, South Australia|Wayville]], a suburb of [[Adelaide, South Australia|Adelaide]], [[South Australia]].

''Annesley Junior School'' has a [[co-educational]] early learning centre for children between the ages of two and five, and a primary school for reception to year 6.<ref name=ABSA>{{cite web|url=http://www.boarding.org.au/site/school_detail.cfm?schID=48 |title=Annesley College |accessdate=2008-02-10 |year=2007 |work=Schools – South Australia |publisher=Australian Boarding Schools Association |archiveurl=https://web.archive.org/web/20071117110925/http://www.boarding.org.au/site/school_detail.cfm?schID=48 |archivedate=2007-11-17 |deadurl=no |df= }}</ref> The early learning centre operates as a long day care centre (child care centre), open 49 weeks of the year.<ref>[http://www.annesley.sa.edu.au/elc.html Annesley Junior School - ELC<!-- Bot generated title -->]</ref> The early learning centre (kindergarten) and the primary school's before and after school care programs are Child Care Benefit–approved.<ref>http://www.annesley.sa.edu.au/pdf/Letter%20advising%20of%20Child%20Care%20Rebate%20Approval%2011%20January%202012.pdf</ref>

Annesley Junior School is affiliated with
<!-- 
Editor's note: Blank comments inserted to improve readability of the raw text.
 -->
the Association of Independent Schools of South Australia,<ref name=AofISofSA>{{cite web |url=http://www.ais.sa.edu.au/html/about_schools_display.asp |title=Annesley College |accessdate=2008-02-10 |year =2007 |work=About Our Schools |publisher=Association of Independent Schools of South Australia }} {{Dead link|date=October 2010|bot=H3llBot}}</ref> and the [[Junior School Heads Association of Australia]] (JSHAA).<ref name=jshaa>{{cite web|url=http://www.jshaa.asn.au/southaustralia/directory/index.asp |title=JSHAA South Australian Directory of Members |accessdate=2008-02-10 |year=2007 |work=South Australia Branch |publisher=Junior School Heads' Association of Australia |deadurl=yes |archiveurl=https://web.archive.org/web/20080406045335/http://www.jshaa.asn.au:80/southaustralia/directory/index.asp |archivedate=2008-04-06 |df= }}</ref>

Annesley has been an IB World School since December 2005.<ref name=IB>{{cite web |url=http://www.ibo.org/school/002644/ |title=Annesley College Junior School |accessdate=2008-02-10 |work=IB World Schools |publisher=International Baccalaureate Organisation}}</ref> The school has a music program and children from reception to year 6 learn French as a second language.

==Campus and curriculum==
Annesley Junior School is located on a single [[campus]] in Wayville, opposite the [[Adelaide Parklands]], 500 metres from the Adelaide CBD.<ref name=Location>{{cite web|url=http://www.annesley.sa.edu.au/about/location.htm |title=Location |accessdate=2008-02-10 |work=About |publisher=Annesley College |deadurl=yes |archiveurl=https://web.archive.org/web/20070918222413/http://www.annesley.sa.edu.au/about/location.htm |archivedate=September 18, 2007 }}</ref> Notable facilities include the historic 'Gillingham Hall' seating the whole school and a [[Chapel]].<ref name=Facilities>{{cite web|url=http://www.annesley.sa.edu.au/about/facilities.htm |title=Facilities |accessdate=2008-02-10 |work=About |publisher=Annesley Junior School |deadurl=yes |archiveurl=https://web.archive.org/web/20070828215804/http://www.annesley.sa.edu.au/about/facilities.htm |archivedate=August 28, 2007 }}</ref>

Annesley Junior School's academic programs include the [[IB Primary Years Programme|Primary Years Program]] of the [[International Baccalaureate]] (IBPYP).

There are two intakes into reception each year, in terms one and three. This will continue from 2014 when South Australian government schools will move to a single intake.

==History==

[[Image:AnnesleyColl1903.jpg|thumb|left|Methodist Ladies' College, 1903]]
<!-- Deleted image removed: [[File:Annesley Junior School sign.jpg|thumb|right|Annesley Junior School 2012]] -->
Annesley Junior School was founded in 1902 as Methodist Ladies' College (MLC), at the site of the former Malvern Grammar School,<ref>The Malvern Grammar School site in the suburb know known as [[Highgate, South Australia|Highgate]] was occupied by [[Concordia College, Adelaide|Concordia College]] in 1905.</ref> with 26 students enrolled. In 1903, the school was moved to the site of the former [[Way College]] for boys on Greenhill Road at Wayville.<ref name=History>{{cite web|url=http://www.annesley.sa.edu.au/about/history.htm |title=History of Annesley College |accessdate=2008-02-10 |work=About |publisher=Annesley College |deadurl=yes |archiveurl=https://web.archive.org/web/20070828215721/http://www.annesley.sa.edu.au/about/history.htm |archivedate=August 28, 2007 }}</ref>

In 1977, a [[Basis of Union (Uniting Church in Australia)|Union]] of the [[Congregational church|Congregational]], [[Methodist]] and [[Presbyterian]] Churches took place, forming the [[Uniting Church in Australia]]. MLC subsequently took the name Annesley College, the maiden name of [[Susanna Wesley]], the mother of [[John Wesley]], the founder of the Methodist Church.

===2010-2012: Merger talks, council resignation, transition to Junior School===
In October 2010 Annesley College considerered merging with another school due to declining enrolments over the previous seven years.<ref>{{cite news|url=http://www.abc.net.au/news/stories/2010/10/14/3038804.htm?section=business|title=Adelaide college seeks merger to survive|publisher=ABC News|date=15 October 2010}}</ref>  The Uniting Church stated it would guarantee the continuity of the school for the following two years and that no merger would proceed.<ref>{{cite news|title=Annesley secures two-year lifeline|url=http://www.abc.net.au/news/stories/2010/10/25/3047410.htm|newspaper=ABC News|date=25 October 2010}}</ref>
The school appointed former [[Melbourne Girls Grammar School]] principal Christine Briggs as its new principal<ref>{{cite news|url=http://www.abc.net.au/news/stories/2010/10/28/3050733.htm|title=Troubled Annesley names new principal|publisher=ABC News|date =28 October 2010}}</ref>  but she withdrew from the appointment days later.<ref>{{cite news|title=New principal quits troubled Adelaide college|url=http://www.abc.net.au/news/stories/2010/11/02/3054509.htm?section=justin|newspaper=ABC News|date=3 November 2010}}</ref>

In the face of growing uncertainty the school said it was seeking formal discussion with [[Pulteney Grammar School]] regarding a merger,<ref>{{cite news|title=Annesley starts formal talks with Pulteney|url=http://www.abc.net.au/news/stories/2010/11/05/3058129.htm|newspaper=ABC News|date=5 November 2010}}</ref>
but the Uniting Church shortly thereafter withdrew in favour of "the co-operation of another Uniting Church School".<ref>{{cite news|title=Annesley-Pulteney merger talks off|url=http://www.abc.net.au/news/stories/2010/11/12/3064745.htm|accessdate=12 November 2010|newspaper=ABC News|date=12 November 2010| archiveurl= https://web.archive.org/web/20101130040622/http://www.abc.net.au/news/stories/2010/11/12/3064745.htm| archivedate= 30 November 2010 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite web|title=Annesley College media update |url=http://www.sa.uca.org.au/media-releases/1948-annesley-college-media-update.html |publisher=Uniting Church SA Synod |accessdate=12 November 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20110331065543/http://sa.uca.org.au/media-releases/1948-annesley-college-media-update.html |archivedate=March 31, 2011 }}</ref> It subsequently stated that [[Scotch College, Adelaide|Scotch College]] would take over its management.
The existing school council resigned, stating that amalgamation would have provided a better outcome.<ref name=scotch>{{cite news|title=Scotch College to take over management of Annesley College|url=http://www.adelaidenow.com.au/news/in-depth/annesley-doomed-as-council-quits/story-fn3o6nna-1225954217922|accessdate=16 November 2010|newspaper=Adelaide Advertiser|date=16 November 2010}}</ref>

By January 2011, 108 students remained enrolled, down from 466 students mid-2010,<ref name="Mess0111"/>  and by June 2011 the school announced it would relaunch in 2012 as the Annesley Learning Community, offering a school for boys and girls from Reception to Year 6 (from 2012) and a women's college for Years 10 to 12 (from 2013),<ref name=Adv0611>{{cite news|url=http://www.adelaidenow.com.au/news/south-australia/rescue-mission-for-girls-college/story-e6frea83-1226083135764|publisher= Adelaide Advertiser|date=28 June 2011|accessdate=28 June 2011|title=Mixed reaction to rescue mission for Annesley College}}</ref> with a commensurate drop in staff numbers from 29 to 13.<ref>{{cite news|title=Annesley College closes to seniors next year|url=http://www.abc.net.au/local/stories/2011/06/28/3255215.htm|accessdate=28 June 2011|newspaper=ABC|date=28 June 2011}}</ref>
Year 10 to 12 tuition was not sustained and the college was renamed Annesley Junior School. In August 2012 there were 100 students enrolled.<ref>http://www.annesley.sa.edu.au/annesley-news/2012-T3-WK05/index.html Annesley News</ref>

==Notable alumnae==
Annesley has an Old Scholars Association which began in 1905 as the MLC Guild. The first meetings were [[literary]] and [[music]] evenings with girls writing [[essay]]s for discussion.<ref name=OSA>{{cite web |url=http://osa.annesley.sa.edu.au/ |title=About the Old Scholars Association |accessdate=2008-02-10 |work=MLC / Annesley College Old Scholars Association Inc. |publisher=Annesley College}}</ref> Some notable Annesley/MLC Old Scholars include:
{{Expand list|date=August 2008}}
<!-- Please do not add alumnae without providing a source and/or link to a valid Wikipedia article -->
<!-- Please add alumnae in alphabetical order -->

*Simone Annan – R&B singer with a degree in biomedical science.<ref>[http://www.adelaidenow.com.au/simone-annan-goes-from-top-student-to-pop-star/story-e6frea6u-1226114362776 Simone Annan goes from top student to pop star | adelaidenow<!-- Bot generated title -->]</ref>
*[[Jennifer Jane Batrouney]] – [[Senior Counsel]], Victoria (also attended [[Methodist Ladies' College, Melbourne]])<ref name=JJBatrouney>{{cite encyclopedia| editor = Suzannah Pearce| encyclopedia = Who's Who in Australia Live!| title = BATROUNEY Jennifer Jane| date = 2006-11-17| publisher = Crown Content Pty Ltd| location = North Melbourne, Vic}}</ref>
*[[Roxy Byrne]] (née Sims) – actress and hockey player (Head Prefect, Dux and Captain of Hockey; Class of 1929)<ref name=RByrne>{{cite web|url= http://www.womenaustralia.info/biogs/AWE0844b.htm|title= Byrne, Roxy (1912–2004)|accessdate= 2008-02-10 |last= Heywood |first= Anne|author2= Secomb, Robin |author3=Henningham, Nikki|date= 2004-04-06|work= Australian Women Biographical Entry|publisher= National Foundation for Australian Women}}</ref>
*[[Mary Campbell Dawbarn|Mary Campbell (Mollie) Dawbarn]] (1902-1982), biochemist and nutritional physiologist<ref>[http://adb.anu.edu.au/biography/dawbarn-mary-campbell-mollie-12409 Biography - Mary Campbell (Mollie) Dawbarn - Australian Dictionary of Biography<!-- Bot generated title -->]</ref>
*Sarah Carver, cricketer<ref>http://www.cricketsa.com.au/teams/ProfileView.aspx?p=996&id=34</ref>
*Kimberley Clayfield (‘94), listed in Australia’s Most Inspiring Young Engineers from 2010. She is Executive Manager of CSIRO Space Sciences and Technology.<ref>[http://www.adelaide.edu.au/lumen/issues/42361/news42386.html lumen - Kimberley's Stellar Career<!-- Bot generated title -->]</ref>
*[[Sara Douglass]] – author<ref name=SDouglass>{{cite web|url= http://www.austlit.edu.au/run?ex=ShowAgent&agentId=A(j%5B|title= Douglass, Sara |accessdate= 2008-02-10|work= Agent Details |publisher=AustLit}}</ref>
*[[Amy Gillett]] – rower and cyclist
*[[Gerda Jezuchowski]] – reporter for ''[[Ten News]]''
*[[Sally Newmarch]] – Olympic rower<ref>[http://www.annesley.sa.edu.au/news/2004/No3Term2.pdf Annesley News, 2004, Number 3, p65.<!-- Human deciphered Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20070530225105/http://www.annesley.sa.edu.au/news/2004/No3Term2.pdf |date=May 30, 2007 }}</ref>
*Jesse Scales - Dancer<ref>[http://www.adelaidenow.com.au/entertainment/confidential/adelaide-dancer-jesse-scales-home-to-perform-in-2-one-another/story-e6fredqc-1226638879736]</ref>
*Dorothy Catherine Somerville (1897-1992) – solicitor<ref>http://trove.nla.gov.au/people/765094?c=people</ref>
*Petra Starke – writer, journalist and PR manager. <ref>http://www.techly.com.au/author/petra-starke/</ref>
* [[Jessica Trengove]] - Australian representative to the 2012 and 2016 Olympics in Athletics<ref>{{cite web |url=http://rio2016.olympics.com.au/athlete/jessica-trengove1 |title=rio 2016 - Jessica Trengove Athlete Profile |accessdate=15 August 2016}}</ref>
*Sesca Ross Zelling, OBE, LLB (1918 - 2001) - lawyer, vice-president of the NCW of Australia (1954-1957)<ref>http://www.womenaustralia.info/biogs/IMP0062b.htm</ref>

==See also==
*[[List of schools in South Australia]]

==References==
{{reflist|2}}

==External links==
*[http://www.annesley.sa.edu.au/ Annesley Junior School website]

{{UCA Schools}}
{{IGSSA Schools}}

[[Category:Private schools in South Australia]]
[[Category:Educational institutions established in 1902]]
[[Category:Junior School Heads Association of Australia Member Schools]]
[[Category:International Baccalaureate schools in Australia]]
[[Category:Methodist schools in Australia]]
[[Category:Uniting Church schools in Australia]]
[[Category:Primary schools in South Australia]]