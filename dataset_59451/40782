'''Roberts Bishop''' "'''Bob'''" '''Owen''' (February 11, 1926 – March 10, 2016) was an American lawyer and diplomat. He served as [[Legal Adviser of the Department of State]] from 1979 to 1981 and acted as a mediator and arbitrator in several international disputes.<ref>{{cite news|last1=Smith|first1=Harrison|title=Roberts B. Owen, lawyer and adviser during Iran and Bosnian crises, dies at 90|url=https://www.washingtonpost.com/local/obituaries/roberts-b-owen-lawyer-and-adviser-during-iran-and-bosnian-crises-dies-at-90/2016/03/30/e910151e-f5be-11e5-a3ce-f06b5ba21f33_story.html|accessdate=3 May 2016|publisher=Washington Post|date=March 30, 2016}}</ref>

==Early life==
Owen was born in Boston and attended [[Phillips Exeter Academy]]. He served in the U.S. Navy during World War II before receiving undergraduate and law degrees at [[Harvard University]] in 1948 and 1951, respectively. He then attended [[Queens' College, Cambridge]] on a [[Fulbright Scholarship]].<ref>Smith.</ref><ref>{{cite web|title=Jimmy Carter: Department of State Nomination of Roberts Bishop Owen To Be Legal Adviser, Sept. 20, 1979|url=http://www.presidency.ucsb.edu/ws/?pid=31383|publisher=The American Presidency Project, Univ. of California, Santa Barbara|accessdate=3 May 2016}}</ref>

Owen began practicing law at [[Covington & Burling]] in Washington, D.C., in 1952, later becoming a partner of that firm.<ref>Carter.</ref><ref>{{cite book|last1=Westwood|first1=Howard C.|title=Covington & Burling: 1919-1984|date=1986|page=387}}</ref>

==Iran Hostage Crisis and Algiers Accords==

President [[Jimmy Carter]] appointed Owen to be the U.S. State Department Legal Adviser in 1979.<ref>Carter.</ref> A few weeks after Owen took office, the [[Iran hostage crisis]] began in November 1979.<ref>Smith.</ref> In 1980, Owen argued on behalf of the United States in its case against [[Iran]] before the [[International Court of Justice]] at [[The Hague]].<ref>Westwood, p. 148.</ref><ref>{{cite web|title=United States Diplomatic and Consular Staff in Tehran (United States of America v. Iran), Oral Arguments, Minutes of the Public sittings held from 18 to 20 March 1980 and on 24 May 1980|url=http://www.icj-cij.org/docket/files/64/13068.pdf|publisher=International Court of Justice|accessdate=3 May 2016}}</ref> Owen also participated in negotiations with Iranian representatives in [[Algiers]] in late 1980 and early 1981 as a member of the team led by Deputy Secretary of State [[Warren Christopher]].<ref>{{cite book|last1=Kreisberg|first1=Paul (ed.)|title=American Hostages in Iran: The Conduct of a Crisis|date=1985|publisher=Yale Univ. Press|location=New Haven|isbn=978-0300032338|pages=301–306}}</ref><ref>{{cite book|last1=Christopher|first1=Warren|title=Chances of a Lifetime: A Memoir|date=2001|publisher=Scribner|location=New York|isbn=978-0684830247|pages=113, 119}}</ref><ref>{{cite news|last1=Oberdorfer|first1=Don|title=Christopher Flies to Algiers in Latest Negotiating Drive|url=https://www.washingtonpost.com/archive/politics/1981/01/08/christopher-flies-to-algiers-in-latest-negotiating-drive/a6db082c-fdaf-4123-a57e-dbfe8d431caf/|accessdate=3 May 2016|publisher=Washington Post|date=January 8, 1981}}</ref> Owen played a role in drafting the [[Algiers Accords]] that led to the January 1981 release of the U.S. embassy personnel being held captive in Tehran.<ref>Christopher, p. 113.</ref><ref>Smith.</ref>

At the end of the Carter Administration, Owen returned to the practice of law at Covington & Burling, from which he retired in 1996.<ref>Smith.</ref>

==Balkans Conflict and Dayton Accords==

In 1995, then-Secretary of State Warren Christopher asked Owen to assist negotiator [[Richard Holbrooke]] in efforts to resolve conflicts arising from the [[Breakup of Yugoslavia]]. As part of Holbrooke’s delegation during negotiations with the combatants, Owen acted as a mediator and the draftsman of proposals that led to the [[Dayton Agreement]].<ref>Smith.</ref><ref>{{cite book|last1=Holbrooke|first1=Richard|title=To End a War|date=1998|publisher=Random House|location=New York|isbn=978-0375753602|pages=110–111, 129}}</ref><ref>{{cite book|last1=Hill|first1=Christopher R.|title=Outpost: Life on the Frontlines of American Diplomacy|date=2014|publisher=Simon & Schuster|location=New York|isbn=978-1-4516-8591-6|pages=91–92}}</ref> According to Holbrooke, after one occasion in which the “normally dignified” Owen expressed his frustration with the behavior of one Bosnian negotiator by slamming his fist against a wall, his American colleagues gave him the sobriquet “Mad Dog.” <ref>Holbrooke, p. 197</ref><ref>{{cite book|last1=Chollet|first1=Derek|last2=Power|first2=Samantha|title=The Unquiet American: Richard Holbrooke in the World|date=2011|publisher=Public Affairs|location=New York|isbn=978-1-61039-078-1|page=228}}</ref>

In 1996, the president of the International Court of Justice appointed Owen to act as presiding arbitrator of the Arbitral Tribunal for the Dispute Over the Inter-Entity Boundary in the Brcko Area, a special ICJ tribunal established to determine the status of the [[Brcko District]], which had not been resolved by the Dayton Agreement.<ref>{{cite book|last1=Farrand|first1=Robert William|title=Reconstruction and Peace Building in the Balkans: The Brcko Experience|date=2011|publisher=Rowman & Littlefield|location=Lanham, Md.|isbn=978-1442212350|page=5}}</ref> As presiding arbitrator, Owen ruled that the Brcko District would constitute a [[Condominium (international law)]] under the direct jurisdiction of the federal authorities of [[Bosnia and Herzegovina]] and outside the jurisdiction of either the [[Republika Srpska]] or the [[Federation of Bosnia and Herzegovina]].<ref>{{cite web|title=Final Award, Arbitral Tribunal for Dispute Over Inter-Entity Boundary in Brcko Area, 5 March 1999|url=http://www.ohr.int/?ohr_archive=arbitral-tribunal-for-dispute-over-inter-entity-boundary-in-brcko-area-final-award&lang=en&print=pdf|website=Office of the High Representative|accessdate=3 May 2016}}</ref><ref>{{cite book|last1=Liotta|first1=P.H.|title=Dismembering the State: The Death of Yugoslavia and Why It Matters|date=2001|publisher=Lexington Books|location=Lanham, Md.|isbn=978-0739102121|page=279}}</ref> As the result of Owen's decision, the Brcko District became largely self-governing.<ref>{{cite news|last1=Geohegan|first1=Peter|title=Welcome to Brčko, Europe’s only free city and a law unto itself|url=https://www.theguardian.com/cities/2014/may/14/brcko-bosnia-europe-only-free-city|accessdate=3 May 2016|publisher=The Guardian|date=May 14, 2014}}</ref>

While some aspects of Owen's arbitral decision in the Brcko Arbitration have been characterized as controversial,<ref>Geohegan</ref><ref>{{cite book|last1=Moore|first1=Adam|title=Peacebuilding in Practice: Local Experience in Two Bosnian Towns|date=2013|publisher=Cornell Univ. Press|location=Ithaca, N.Y.|isbn=978-0-8014-5199-7|pages=137–138}}</ref> one former American diplomat opined that Owen's "solution to the impasse over Brcko's political status will surely go down in the annals of peacemaking as one of its finer moments."<ref>Farrand, p. xiv.</ref>

In another role as international arbitrator, the Republic of [[Venezuela]] appointed Owen in 1996 as one of three arbitrators for an proceeding administered by the [[International Centre for Settlement of Investment Disputes]] to resolve an investor-state dispute against the Republic.<ref>{{cite web|title=ICSID, Final Award, Fedax N.V. v. The Republic of Venezuela, March 9, 1998|url=http://www.italaw.com/sites/default/files/case-documents/ita0316_0.pdf|website=Italaw|accessdate=3 May 2016}}</ref>

Owen died at his home in Washington, D.C., on March 10, 2016.<ref>Smith.</ref> As the Presiding Arbitrator of the Arbitral Tribunal for Dispute over the Inter-Entity Boundary in [[Brčko District|Brčko Area]], he was succeeded by [[John Clint Williamson]] (appointment made by [[International Court of Justice]] President [[Ronny Abraham]])[http://www.ohr.int/?p=96797]

==References==
{{reflist}}

{{s-start}}
{{s-legal}}
{{succession box
| title=[[Legal Adviser of the Department of State]]
| before=Herbert J. Hansell
| after=Davis Rowland Robinson
| years=October 4, 1979 – February 16, 1981}}
{{s-end}}

*
*
*
*

{{DEFAULTSORT:Owen, Roberts B.}}
[[Category:2016 deaths]]
[[Category:Carter administration personnel]]
[[Category:American diplomats]]
[[Category:Lawyers who have represented the United States government]]
[[Category:Harvard University alumni]]
[[Category:Fulbright Scholars]]
[[Category:1926 births]]
[[Category:United States Department of State officials]]
[[Category:American lawyers]]
[[Category:Phillips Exeter Academy alumni]]
[[Category:Alumni of Queens' College, Cambridge]]
[[Category:American naval personnel of World War II]]