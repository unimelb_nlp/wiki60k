{{About|the narrative genre|the Christian evangelistic book|Wordless Book}}
{{featured article}}
{{Use Canadian English|date=August 2013}}

[[File:Frans Masereel - 25 Images of a Man's Passion - final four plates.jpg|400px|thumb|alt=Four high-contrast black-and-white images in sequence.  In the first, a man, facing left with his right arm aloft, marches with a crowd towards a group of gun-wielding figures.  In the second, uniformed figures are taking the man away amongst a crowd.  In the third, the man is seen from behind at the bottom, with a group seated behind a bench in the distance near the top in an apparent courtroom.  A crucifix hangs prominently above the bench, bathed in light in the darkened room.  In the fourth, the man has his back to a wall, hands bound behind him, with another figure lying apparently dead at his feet.  He faces right, apparently awaiting his execution by gunfire.|Wordless novels flourished in Germany in the 1920s and typically were made using [[woodcut]] or similar techniques in an [[Expressionism|Expressionist]] style. ([[Frans Masereel]], ''[[25 Images of a Man's Passion]]'', 1918)]]

The '''wordless novel''' is a narrative genre that uses sequences of captionless pictures to tell a story.  As artists have often made such books using [[woodcut]] and other [[relief printing]] techniques, the terms '''woodcut novel''' or '''novel in woodcuts''' are also used.  The genre flourished primarily in the 1920s and 1930s and was most popular in Germany.

The wordless novel has its origin in the [[German Expressionism|German Expressionist]] movement of the early 20th century.  The typically [[Socialism|socialist]] work drew inspiration from mediaeval woodcuts and used the awkward look of that medium to express [[angst]] and frustration at [[social injustice]].  The first such book was the Belgian [[Frans Masereel]]'s ''[[25 Images of a Man's Passion]]'', published in 1918.  The German [[Otto Nückel]] and other artists followed Masereel's example.  [[Lynd Ward]] brought the genre to the United States in 1929 when he produced {{not a typo|''[[Gods' Man]]''}}, which inspired other American wordless novels and a parody in 1930 by cartoonist [[Milt Gross]] with ''[[He Done Her Wrong]]''.  Following an early-1930s peak in production and popularity, the genre waned in the face of competition from [[sound film]]s and anti-socialist [[censorship in Nazi Germany]] and the US.

Following World War&nbsp;II, new examples of wordless novels became increasingly rare, and early works went out of print.  Interest began to revive in the 1960s when the American comics [[fandom]] subculture came to see wordless novels as prototypical book-length comics.  In the 1970s, the example of the wordless novel inspired cartoonists such as [[Will Eisner]] and [[Art Spiegelman]] to create book-length non-genre comics—"[[graphic novel]]s".  Cartoonists such as [[Eric Drooker]] and [[Peter Kuper]] took direct inspiration from wordless novels to create wordless graphic novels.

{{TOC limit|3}}

==Characteristics==

[[File:CABINET DES DR CALIGARI 01.jpg|thumb|alt=A black-and-white film still.  Three figures stand facing each other on a heavily stylized street set.|[[German Expressionism|Expressionist film]] and graphics inspired early wordless novels. (''[[The Cabinet of Dr. Caligari]]'', 1920)]]

Wordless novels use sequences of expressive images to tell a story.{{sfn|Willett|2005|p=112}}  [[Socialism|Socialist]] themes of struggle against [[capitalism]] are common; scholar Perry Willett calls these themes "a unifying element of the genre's aesthetic".{{sfn|Willett|2005|p=114}}  In both formal and moral aspects, they draw from [[Expressionism|Expressionist]] graphics, [[Expressionism (theatre)|theatre]], and [[German Expressionism|film]].{{sfn|Willett|2005|p=126}}  Wordless novelists such as [[Frans Masereel]] appropriated the awkward aesthetic of mediaeval woodcuts to express their anguish and revolutionary political ideas{{sfn|Willett|2005|p=126}} and used simple, traditional iconography.  Text is restricted to title and chapter pages, except where text is a part of the scene, such as in signs.{{sfn|Cohen|1977|p=181}}

The storytelling tends to be melodramatic,{{sfn|Willett|2005|p=114}} and the stories tend to focus on struggles against social oppression in which characters are silenced by economic, political, and other social forces. The characters are clearly delineated as good or evil—the good drawn sympathetically and the evil with the contempt of the artist's moral indignation.{{sfn|Willett|2005|pp=130–131}}

Most wordless novelists were not prolific; few besides Masereel and [[Lynd Ward]] produced more than a single book.{{sfn|Willett|2005|p=131}}  The books were designed to be mass-produced for a popular audience, in contrast to similar but shorter portfolios by artists such as [[Otto Dix]], [[George Grosz]], and [[Käthe Kollwitz]], which were produced in limited editions for collectors.  These portfolios of typically from eight to ten prints also were meant to be viewed in sequence.  Wordless novels were longer, had more complex narratives, and were printed in sizes and dimensions comparable to those of novels.{{sfn|Willett|2005|p=128}}  A large influence was the most popular silent visual medium of the time: [[silent film]]s.  Panning, zooming, slapstick, and other filmic techniques are found in the books; Ward said that in creating a wordless novel, he first had to visualize it in his head as a silent film.{{sfn|Willett|2005|pp=128–129}}

[[File:Prelude to a Million Years, plate 29, by Lynd Ward.jpg|thumb|left|alt=Photograph of an engraved piece of wood, on which is an image of a man kneeling.|Wordless novelists favoured [[relief printing]] such as in this [[wood engraving]] from [[Lynd Ward|Ward]]'s ''[[Prelude to a Million Years]]'' (1933).]]

Typically, wordless novels used [[relief printing]] techniques such as [[woodcut]]s, [[wood engraving]], [[metalcut]]s, or [[linocut]]s.  One of the oldest printing techniques, relief printing has its origins in 8th-century China and was introduced to Europe in the 15th century.  It requires an artist to draw or transfer an image to a printing block; the areas not to be printed (the white areas) are cut away, leaving raised areas to which ink is applied to make prints.{{sfn|Walker|2007|p=15}}  The monochrome prints were usually in black ink, and occasionally in a different colour such as [[sienna]] or orange.{{sfn|Cohen|1977|p=193}}  Relief printing is an inexpensive but labour-intensive printing technique; it was accessible to socially conscious artists who wanted to tell wordless stories of the working classes.{{sfn|Walker|2007|p=16}}

==History==

In 15th-century{{sfn|Cohen|1977|p=175}} mediaeval Europe, woodcut [[block book]]s were printed as religious guides; particularly popular was the ''{{lang|la|[[Ars moriendi]]}}''.  The early 16th century saw block books disappear in favour of books printed with the [[movable type]] of [[Johannes Gutenberg|Gutenberg]]'s presses.{{sfn|Cohen|1977|pp=172–173, 175}}  Woodcut printing persisted into the 16th century under artists such as [[Albrecht Dürer|Dürer]], [[Hans Holbein the Younger|Holbein]], and [[Jost Amman|Amman]],{{sfn|Cohen|1977|p=175}} after which engraving techniques superseded woodcuts.  Pioneered by [[Thomas Bewick]], wood engraving enjoyed popularity beginning in the 18th century, until the method gave way by the 19th century to more advanced printing methods such as [[lithography]].{{sfn|Cohen|1977|pp=176–177}}

[[File:Biblia pauperum, Nordisk familjebok.png|thumb|alt=A monochromatic woodcut print of a page from a mediaeval book depicting a bible scene.|[[Expressionism|Expressionist]] woodcut artists expressed angst using the awkward look of mediaeval woodcuts such as this {{lang|la|''[[Biblia pauperum]]''}}.]]

[[Post-Impressionism|Post-impressionist]] artist [[Paul Gauguin]] revived woodcut printing in the late-19th century, favouring it for its [[Primitivism|primitivist]] effect.{{sfn|Cohen|1977|p=177}}  Early in the 20th century, woodcut artists such as Käthe Kollwitz (1867–1945) and [[Max Klinger]] (1857–1920) published portfolios of woodcuts, thematically linked by themes of social injustice.{{sfn|Willett|2005|pp=127–128}}  Expressionist graphic artists such as [[Max Beckmann]] (1884–1950), Otto Dix (1891–1969), Kollwitz, and [[Karl Schmidt-Rottluff]] (1884–1976) were inspired by an early-20th-century revival of interest in mediaeval graphic arts—in particular Biblical woodcut prints such as the ''{{lang|la|[[Biblia pauperum]]}}''.  These artists used the awkward look of woodcut images to express feelings of anguish.{{sfn|Willett|2005|p=126}}

===In Europe===

The wordless novel grew out of the Expressionist movement.{{sfn|Willett|2005|p=111}}  The Belgian Frans Masereel (1889–1972) created the earliest example, ''[[25 Images of a Man's Passion]]'',{{efn|{{lang-fr|25 images de la passion d'un homme}} }} in 1918.{{sfn|Willett|2005|p=112}}  It was a commercial success{{sfn|Walker|2007|p=17}} and was followed by ''[[Passionate Journey]]'',{{efn|{{lang-fr|Mon livre d'heures|links=no}}; the book was translated into English as ''My Book of Hours'' (1919),{{sfn|Willett|2005|p=114}} }} which at 167 images was Masereel's longest book.  It was also the most commercially successful,{{sfn|Willett|2005|p=114}} particularly in Germany, where copies of his books sold in the hundreds of thousands throughout the 1920s and had introductions by writers such as [[Max Brod]], [[Hermann Hesse]], and [[Thomas Mann]].  Masereel's books drew strongly on Expressionist theatre and film{{sfn|Willett|2005|p=112}} in their exaggerated but representational artwork with strong contrasts of black and white.{{sfn|Cohen|1977|p=180}}

Masereel's commercial success led other artists to try their hands at the genre;{{sfn|Willett|2005|p=116}} themes of oppression under capitalism were prominent, a pattern set early by Masereel.{{sfn|Willett|2005|p=112}} At age thirteen, Polish-French artist [[Balthus]] drew a wordless story about his cat; it was published in 1921 with an introduction by poet [[Rainer Maria Rilke]].{{sfnm|1a1=Rewald|1y=1984|1pp=12–13|2a1=Walker|2y=2007|2p=11}}  In ''[[Destiny (wordless novel)|Destiny]]'' (1926),{{efn|{{lang-de|Schicksal}}{{sfn|Smart|2011|pp=22–23}} }} [[Otto Nückel]] (1888–1955) produced a work with greater nuance and atmosphere than Masereel's bombastic works;{{sfn|Smart|2011|pp=22–23}} where Masereel told tales of Man's struggle against Society, Nückel told of the life of an individual woman.{{sfn|Bi|2009}}  ''Destiny'' appeared in a US edition in 1930{{sfn|Ward|Beronä|2005|p=v}} and sold well there.{{sfn|Cohen|1977|p=191}}

[[Clément Moreau]] (1903–1988) first tried his hand at the genre with the six-plate ''Youth Without Means''{{efn|{{lang-de|Erwerbslose Jugend|links=no}} }} in 1928.{{sfn|Walker|2007|p=23}}  [[István Szegedi-Szüts]] (1892–1959), a Hungarian immigrant to England, made a wordless book in brush and ink called ''My War'' (1931).  In simple artwork reminiscent of [[Sumi-e|Japanese brush painting]], Szegedi-Szüts told of a Hungarian cavalryman disillusioned by his World War&nbsp;I experiences.{{sfn|Beronä|2008|p=177}}  [[Helena Bochořáková-Dittrichová]] (1894–1980) was the first woman to produce a wordless novel, ''[[Childhood (wordless novel)|Childhood]]'' (1931),{{sfn|Walker|2007|p=21}} which presented middle-class life, rather than the working-class struggle found in the works of Masereel or Nückel.{{sfn|Beronä|2008|p=115}}  Bochořáková described her books as "cycles" rather than novels.{{sfn|Walker|2007|p=21}}  [[Surrealism|Surrealist]] artist [[Max Ernst]] made the silent collage novel ''[[Une semaine de bonté]]'' in 1934.{{sfn|Beronä|1999|pp=2–3}}  Following World War&nbsp;II, {{Interlanguage link multi|Werner Gothein|de}} (1890–1968), a member of the German Expressionist group [[Die Brücke]], produced ''The Tightrope Walker and the Clown''{{efn|{{lang-de|Die Seiltänzerin und ihr Clown|links=no}} }} (1949).{{sfn|Walker|2007|p=21}}

===In North America===

In 1926, the American Lynd Ward (1905–1985) moved to Leipzig to study graphic arts; while there, he discovered the works of Masereel and Otto Nückel.{{sfn|Beronä|2001|pp=20–21}}  He produced six such works of his own;{{sfn|Willett|2005|p=126}} he preferred to call them "pictorial narratives".{{sfn|Beronä|2001|pp=19–21}} The first, {{not a typo|''[[Gods' Man]]''}} (1929), was his most popular.{{sfn|Willett|2005|p=126}}  Ward used wood engraving rather than woodcutting{{sfn|Beronä|2001|pp=19–21}} and varied image sizes from page to page.{{sfn|Cohen|1977|p=191}}  {{not a typo|''Gods' Man''}} sold 20,000 copies, and other American artists followed up on this success with their own wordless novels in the 1930s.{{sfn|Beronä|2001|pp=19–21}}

[[File:He Done Her Wrong - Gross does Ward.jpg|thumb|left|alt=A black-and-white drawing of woman opening a window.|In ''[[He Done Her Wrong]]'' (1930), [[Milt Gross]] parodied [[Lynd Ward]]'s {{not a typo|''[[Gods' Man]]''}} (1929).]]

Cartoonist [[Milt Gross]]'s ''[[He Done Her Wrong]]'' (1930) was a parody of the genre; the book uses varying panel designs akin to those of comics: the action sometimes takes place outside the panel borders{{sfn|Beronä|2001|pp=21–22, 24}} and "dialogue balloons" show in images what the characters are saying.{{sfn|Beronä|2008|p=158}}  Cartoonist and illustrator [[William Gropper]]'s ''Alay-oop'' (1930) tells of three entertainers' disappointed dreams.{{sfn|Beronä|2008|p=136}}  In ''Abraham Lincoln: Biography in Woodcuts'' (1933) Charles Turzak documented [[Abraham Lincoln|the American president]].{{sfn|Ward|Beronä|2005|p=vi}}  Animator [[Myron Waldman]] (1908–2006) wrote a wordless tale of a plump young woman looking for a glamorous husband.  The book, ''Eve'' (1943), also uses "picture balloons" as ''He Done Her Wrong'' does.{{sfn|Beronä|2008|p=170}}

Inspired by mediaeval religious block books and working in an [[Art Deco]] style, American illustrator James Reid (1907–1989) produced one wordless novel, ''The Life of Christ'' (1930);{{sfn|Walker|2007|pp=25, 27}} due to the book's religious content, the [[Soviet Union]] barred its importation under its [[Religion in the Soviet Union|policies on religion]].{{sfn|Walker|2007|p=17}}

In 1938, Italian-American [[Giacomo Patri]] (1898–1978) produced his only wordless novel, the linocut ''White Collar''.  It chronicles the aftermath of the 1929 stock market crash and was intended to motivate [[white-collar worker]]s to unionize.{{sfn|Walker|2007|pp=25, 27}}  It also deals with controversial topics such as abortion, accessibility of health care for the poor, and loss of Christian faith.{{sfn|Beronä|2008|p=177}}  From 1948 to 1951, Canadian [[Laurence Hyde (artist)|Laurence Hyde]] (1914–1987) produced his single wordless novel, the woodcut ''[[Southern Cross (wordless novel)|Southern Cross]]'', in response to the American [[Nuclear testing at Bikini Atoll|atomic tests in the Bikini Atoll]].{{sfn|Walker|2007|p=31}}  The work tells of an American evacuation of an island for nuclear tests, where one family is left behind.{{sfn|Cohen|1977|p=195}}  Polish-American [[Si Lewen]]'s (1918–&nbsp;) first book, ''The Parade: A Story in 55 Drawings'' (1957), won praise from [[Albert Einstein]] for its anti-war message.{{sfnm|1a1=Beronä|1y=1999|1pp=2–3|2a1=Walker|2y=2007|2p=10}}  Canadian George Kuthan's ''Aphrodite's Cup'' (1964) is an erotic book drawn in an ancient Greek style.{{sfn|Beronä|1999|pp=2–3}}  In the early 21st century, Canadian [[George Walker (printmaker)|George Walker]] made wordless woodcut novels, beginning with ''Book of Hours'' (2010), about the lives of those in the [[World Trade Center (1973–2001)|World Trade Center]] complex just before the [[September 11 attacks]].{{sfn|Smart|2011|p=55}}

===Decline===

The popularity of wordless novels peaked around 1929 to 1931, when "[[Sound film|talkies]]" were introduced and began to supersede silent films.{{sfn|Willett|2005|pp=129–130}}  In the 1930s the Nazis in Germany suppressed and detained many printmakers and banned Masereel's works{{sfn|Walker|2007|p=17}} as "[[degenerate art]]".{{sfn|Walker|2007|p=21}}  Following World War&nbsp;II, US censors suppressed books with socialist views, including the works of Lynd Ward, on whom the [[Federal Bureau of Investigation|FBI]] kept files over his socialist sympathies; this censorship has made early editions of wordless novels scarce collectors' items in the US.{{sfn|Walker|2007|p=17}}

By the 1940s, most artists had given up on the genre.  The most devoted practitioners, Masereel and Ward, moved on to other work for which they became better known; Masereel's obituary did not even mention his wordless novels.{{sfn|Willett|2005|p=131}}  Many wordless novels remained out of print until the rise of the graphic novel revived interest amongst readers and publishers in the early 21st century.{{sfn|Beronä|2008|p=225}}

==Relation to comics and graphic novels==
{{Quote
|text   = "...&nbsp;Ward's roots were not in comics, though his work is part of the same large family tree&nbsp;..."
|sign   = [[Art Spiegelman]]
|source = ''[[The Paris Review]]'', 2010{{sfn|Spiegelman|2010}}
}}

There have been sporadic examples of textless comics throughout the medium's history.  In the US, there were comic strips such as [[Otto Soglow]]'s ''[[The Little King]]'', begun in 1931, and [[Carl Thomas Anderson|Carl Anderson]]'s ''[[Henry (comics)|Henry]]'', begun in 1932.{{sfn|Beronä|1999|pp=2–3}}  German cartoonist [[E.&nbsp;O. Plauen]]'s wordless domestic comic strip ''[[Father and Son (comics)|Father and Son]]''{{efn|{{lang-de|Vater und Sohn|links=no}} }} (1934–37) was popular in Germany, and was collected in three volumes.{{sfn|Beronä|2008|p=153}}  [[Antonio Prohías]]'s textless ''[[Mad (magazine)|Mad]]'' magazine feature ''[[Spy vs. Spy]]'' began in 1961.{{sfn|Beronä|1999|pp=2–3}}

{{multiple image
|total_width = 400
|image1 = Will Eisner2.jpg
|width1 = 509 | height1 = 682
|alt1   = An bald elderly man in a suit and tie, seated before a meal, raising his right hand slightly and look to the right of the picture.

|image2 = Art Spiegelman (2007).jpg
|width2 = 205 | height2 = 270
|alt2    = A late-middle-aged man with glasses, seated, wearing a black leather jacket, smiles at the camera.

|footer = [[Lynd Ward]]'s work inspired cartoonists [[Will Eisner]] ''(left)'' and [[Art Spiegelman]] ''(right)'' to create [[graphic novel]]s.}}

Cartoonist [[Will Eisner]] (1917–2005) first came upon the work of Lynd Ward in 1938.  Eisner was an early pioneer in the [[American comic book]] industry and saw in Ward's work a greater potential for comics.  Eisner's ambitions were rebuffed by his peers, who saw comics as no more than low-status entertainment.  Eisner withdrew from the commercial comics industry in the early 1950s to do government and educational work.  He returned in the 1970s when the atmosphere had changed and his readers and peers seemed more receptive to his ambitions.  In 1978, he began a career of creating book-length comics, the first of which was ''[[A Contract with God]]''; the book was marketed as a "graphic novel", a term that became standard towards the end of the 20th century.{{sfn|Kaplan|2010|p=153}}  Eisner called Ward "perhaps the most provocative graphic storyteller"{{sfn|Chute|2012|p=410}} of the 20th century.  He wrote that Ward's ''[[Vertigo (wordless novel)|Vertigo]]'' (1937) required considerable investment from readers in order to fill in the story between images.{{sfn|Chute|2012|p=410}}

Interest in the wordless novel revived with the rise of the graphic novel.{{sfn|Beronä|2001|p=20}}  Comics fans discussed the works of Masereel and others in fanzines, and the discussions turned to talk of the [[Great American Novel]] being made in comics.  These discussions inspired cartoonist [[Art Spiegelman]] (b.&nbsp;1948),{{sfn|Kaplan|2010|p=171}} who in 1973 made a four-page strip, "Prisoner on the Hell Planet", in an Expressionist style inspired by Ward's work.  Spiegelman later incorporated the strip into his graphic novel ''[[Maus]]'' (1992).{{sfn|Witek|2004|p=100}}

While graphic novels generally use captions and dialogue,{{sfn|Willett|2005|p=131}} cartoonists such as [[Eric Drooker]], [[Peter Kuper]], [https://fr.wikipedia.org/wiki/Thomas_Ott Thomas Ott], [[Brian Ralph]], [[Masashi Tanaka]], [[Lewis Trondheim]], and Billy Simms have made wordless graphic novels.{{sfn|Beronä|2001|p=20}}  As Gross did in ''He Done Her Wrong'', {{Interlanguage link multi|Hendrik Dorgathen|de}}'s wordless ''oeuvre'' uses textless [[Speech balloon|word balloons]] containing symbols, icons, and other images.{{sfn|Beronä|2001|pp=27–28}}  The influence of the wordless novel is prominent in Drooker's ''Flood'' (1992) and Kuper's ''The System'' (1997), both metaphorical stories that focus on social themes.{{sfn|Beronä|2008|p=225}}  Since 2011, the [[Pennsylvania State University Libraries]] and the [[Pennsylvania Center for the Book]] have awarded the annual Lynd Ward Prize for Graphic Novel, a cash prize established by Ward's daughters to highlight their father's influence on the development of the graphic novel.{{sfn|Reid|2014}}

==See also==

* ''[[Frank (comics)|Frank]]'', wordless comics by [[Jim Woodring]]

{{Portal bar|Books|Comics|Novels|Socialism|Visual arts}}

==Notes==

{{Notelist|colwidth=40em}}

==References==

===Footnotes===

{{Reflist|colwidth=20em}}

===Works cited===

====Books====

{{Refbegin|colwidth=40em}}
* {{cite book
|last          = Beronä
|first         = David A.
|chapter       = Pictures Speak in Comics Without Words
|title         = The Language of Comics: Word and Image
|editor1-last  = Varnum
|editor1-first = Robin
|editor2-last  = Gibbons
|editor2-first = Christina T.
|url           = https://books.google.com/books?id=j_S6QHAov1kC&pg=PA19
|year          = 2001
|publisher     = [[University Press of Mississippi]]
|isbn          = 978-1-60473-903-9
|pages         = 19–39
|ref           = harv}}
* {{cite book
|last1       = Ward
|first1      = Lynd
|authorlink1 = Lynd Ward
|last2       = Beronä
|first2      = David
|title       = Mad Man's Drum: A Novel in Woodcuts
|year        = 2005
|publisher   = [[Dover Publications]]
|isbn        = 978-0-486-44500-7
|chapter     = Introduction
|pages       = iii–vi
|ref         = harv}}
* {{cite book |last = Beronä |first = David A. |title = Wordless Books: The Original Graphic Novels |url = https://books.google.com/books?id=YVh2QgAACAAJ |year = 2008 |publisher = [[Abrams Books]] |isbn = 978-0-8109-9469-0 |ref = harv }}<!-- Beronä 2008 -->
* {{cite book
|last          = Chute
|first         = Hillary
|chapter       = Graphic Narrative
|editor1-last  = Bray
|editor1-first = Joe
|editor2-last  = Gibbons
|editor2-first = Alison
|editor3-last  = McHale
|editor3-first = Brian
|title         = The Routledge Companion to Experimental Literature
|url           = https://books.google.com/books?id=Z3S2uf3Zj8AC&pg=PA407
|year          = 2012
|publisher     = [[Routledge]]
|isbn          = 978-0-415-57000-8
|pages         = 407–419
|ref           = harv}}
* {{cite book |last = Kaplan |first = Arie |title = From Krakow to Krypton: Jews and Comic Books |url = https://books.google.com/books?id=8aH3H7DC6BQC |year = 2010 |publisher = [[Jewish Publication Society]] |isbn = 978-0-8276-1043-9 |ref = harv }}<!-- Kaplan 2010 -->
* {{cite book |last = Rewald |first = Sabine |title = Balthus: Catalog of an Exhibition at the Metropolitan Museum of Art, Feb. 29, 1984 to May 13, 1984 |url = https://books.google.com/books?id=UO_HTonZ31cC |year = 1984 |publisher = [[Metropolitan Museum of Art]] |isbn = 978-0-8109-0738-6 |ref = harv }}<!-- Rewald 1984 -->
* {{cite book |editor-last = Walker |editor-first = George |title = Graphic Witness: Four Wordless Graphic Novels |url = https://books.google.com/books?id=yqQNBAAACAAJ |year = 2007 |publisher = Firefly Books |isbn = 978-1-55407-270-5 |ref = harv }}<!-- Walker 2007 -->
* {{cite book
|last         = Willett
|first        = Perry
|title        = A Companion to the Literature of German Expressionism
|editor-last  = Donahue
|editor-first = Neil H.
|url          = https://books.google.com/books?id=zjvV48n-ngUC&pg=PA111
|year         = 2005
|publisher    = [[Camden House Publishing]]
|isbn         = 978-1-57113-175-1
|pages        = 111–134
|chapter      = The Cutting Edge of German Expressionism: The Woodcut Novel of Frans Masereel and Its Influences
|ref          = harv}}

{{Refend}}

====Magazines and journals====

{{Refbegin|colwidth=40em}}
* {{cite journal
|last        = Beronä
|first       = David A.
|title       = An Introduction to 'Beyond Words: A Wordless Comic Anthology'
|journal     = Sunburn
|issue       = 11
|date        = Summer 1999
|pages       = 2–3
|url         = http://jupiter.plymouth.edu/~daberona/intro.htm
|accessdate  = 2013-03-17
|archiveurl  = https://web.archive.org/web/20150122003708/http://jupiter.plymouth.edu/~daberona/intro.htm
|archivedate = 2015-01-22
|ref         = harv}}
* {{cite journal
|last      = Cohen
|first     = Martin S.
|title     = The Novel in Woodcuts: A Handbook
|journal   = [[Journal of Modern Literature]]
|volume    = 6
|issue     = 2
|date      = April 1977
|pages     = 171–195
|publisher = [[Indiana University Press]]
|jstor     = 3831165
|ref       = harv}}
* {{cite journal
|last      = Smart
|first     = Tom
|title     = A Suite of Engravings from ''The Mysterious Death of Tom Thomson''
|pages     = 10–37
|journal   = Devil's Artisan
|url       = https://books.google.com/books?id=fIgKsV3BXacC
|volume    = 68
|date      = Spring 2011
|publisher = [[The Porcupine's Quill]]
|ref       = harv}}
* {{cite journal
|title       = The Woodcuts of Lynd Ward
|first       = Art
|last        = Spiegelman
|authorlink  = Art Spiegelman
|journal     = [[The Paris Review]]
|date        = 2010-10-13
|url         = http://www.theparisreview.org/blog/2010/10/13/the-woodcuts-of-lynd-ward/
|accessdate  = 2013-03-18
|archiveurl  = https://web.archive.org/web/20141129042624/http://www.theparisreview.org/blog/2010/10/13/the-woodcuts-of-lynd-ward/
|archivedate = 2014-11-29
|ref         = harv}}
* {{cite journal
|last        = Witek
|first       = Joseph
|title       = Imagetext, or, Why Art Spiegelman Doesn't Draw Comics
|journal     = ImageTexT: Interdisciplinary Comics Studies
|volume      = 1
|issue       = 1
|year        = 2004
|issn        = 1549-6732
|publisher   = [[University of Florida]]
|url         = http://www.english.ufl.edu/imagetext/archives/v1_1/witek/
|accessdate  = 2012-04-16
|archiveurl  = https://web.archive.org/web/20130729110501/http://www.english.ufl.edu/imagetext/archives/v1_1/witek/
|archivedate = 2013-07-29
|ref         = harv}}

{{Refend}}

====Web====

{{Refbegin|colwidth=40em}}
* {{cite web
|last        = Bi
|first       = Jessie
|title       = ''Destin'' de Otto Nückel
|url         = http://www.du9.org/chronique/destin/
|work        = du9
|date        = May 2009
|language    = French
|accessdate  = 2013-03-18
|archiveurl  = https://web.archive.org/web/20140315013824/http://www.du9.org/chronique/destin/
|archivedate = 2014-03-15
|ref         = harv}}
* {{cite web
|last        = Reid
|first       = Calvin
|title       = 'Fran' Wins Lynd Ward Graphic Novel Prize
|work        = [[Publishers Weekly]]
|date        = 2014-04-25
|url         = http://www.publishersweekly.com/pw/by-topic/industry-news/comics/article/62038-fran-wins-lynd-ward-graphic-novel-prize.html
|accessdate  = 2015-01-22
|archiveurl  = https://web.archive.org/web/20150122005435/http://www.publishersweekly.com/pw/by-topic/industry-news/comics/article/62038-fran-wins-lynd-ward-graphic-novel-prize.html
|archivedate = 2015-01-22
|ref         = harv}}

{{Refend}}

==Further reading==

{{Refbegin}}
* {{cite book
<!-- |ref          = harv -->
|last      = Willett
|first     = Perry
|title     = The Silent Shout: Frans Masereel, Lynd Ward, and the Novel in Woodcuts
|year      = 1997
|publisher = [[Indiana University Libraries]]
|oclc      = 36526265}}

{{Refend}}

==External links==
* {{cite web
|title      = La bande dessinée muette
|url        = http://www.du9.org/dossier/bande-dessinee-muette-1-la/
|language   = French
|first      = Jessie
|last       = Bi
|work       = du9
|date       = June 2006
|accessdate = 2013-04-19}}
* [http://www.frans-masereel.de/ Frans Masereel Foundation] internet portal.  Includes a [http://www.frans-masereel.de/15331_Graphic_Novels.html graphic novels] page with online versions of Masereel's books.
* {{cite web
|title      = Stories Without Words: A Bibliography with Annotations
|date       = 2003-07-23
|first1     = Mike
|last1      = Rhode
|first2     = Tom
|last2      = Furtwangler
|first3     = David
|last3      = Wybenga
|publisher  = Michigan State University Libraries
|url        = http://comics.lib.msu.edu/rhode/wordless.htm
|accessdate = 2013-04-19}}

{{Wordless novels}}

[[Category:Expressionism]]
[[Category:History of comics]]
[[Category:Narrative forms]]
[[Category:Woodcuts]]
[[Category:Wordless novels| ]]