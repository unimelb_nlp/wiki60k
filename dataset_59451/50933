{{Infobox journal
| title = Clinical Infectious Diseases
| cover = <!-- Deleted image removed: [[Image:CIDcover.gif]] -->
| discipline = [[Infectious disease]]s
| abbreviation = Clin. Infect. Dis.
| editor = Sherwood L. Gorbach
| publisher = [[Oxford University Press]]
| country =
| frequency = Biweekly
| history = 1979-present
| impact = 8.886
| impact-year = 2014
| website = http://www.oxfordjournals.org/our_journals/cid/
| link1 = http://cid.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://cid.oxfordjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 1058-4838
| eISSN = 1537-6591
| CODEN = CIDIEL
| OCLC = 24308833
}}
'''''Clinical Infectious Diseases''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by [[Oxford University Press]] covering research on the [[pathogenesis]], clinical investigation, medical [[microbiology]], [[diagnosis]], [[immune]] mechanisms, and treatment of diseases caused by infectious agents. It includes articles on [[antimicrobial resistance]], [[bioterrorism]], emerging [[infections]], [[food safety]], hospital [[epidemiology]], and [[HIV/AIDS]]. It also features highly focused brief reports, review articles, editorials, commentaries, and supplements. The journal is published on behalf of the [[Infectious Diseases Society of America]].

According to the ''[[Journal Citation Reports]]'', the journal had a 2014 [[impact factor]] of 8.886, ranking it 9th out of 148 journals in the category "Immunology",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Immunology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> second out of 78 journals in the category "Infectious Diseases"<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Infectious Diseases |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |series=Web of Science}}</ref> and ninth out of 119 journals in the category "Microbiology".<ref name=WoS3>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Microbiology |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |series=Web of Science}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.oxfordjournals.org/our_journals/cid/}}

[[Category:Oxford University Press academic journals]]
[[Category:Microbiology journals]]
[[Category:Biweekly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1979]]