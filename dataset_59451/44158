{{Use Australian English|date=May 2014}}
{{Use dmy dates|date=May 2014}}
{{Infobox airport
| name = Canberra Airport
| nativename-a = 
| nativename-r =
| image = Canberra Airport logo.svg
| image-width = 150
| image2 = Canberra Airport terminal (5).jpg
| image2-width = 250
| IATA = CBR
| ICAO = YSCB
| type = Public
| owner =
| operator = Capital Airport Group Pty Ltd<ref name=CAG>{{cite web|url=http://www.canberraairport.com.au/air_aboutAirport/board.cfm|title=Board of Directors|publisher=Canberra Airport|archiveurl=https://web.archive.org/web/20110717182252/http://www.canberraairport.com.au/air_aboutAirport/board.cfm|archivedate=17 July 2011}}</ref><br> Executive Chairman: [[Terry Snow]]
| city-served = Canberra
| elevation-f = 1,886
| coordinates = {{coord|35|18|25|S|149|11|42|E|region:AU-ACT|display=inline,title}}
| pushpin_map = Australian Capital Territory#Australia
| pushpin_label = CBR
| website = [http://www.canberraairport.com.au/ canberraairport.com.au]
| metric-rwy = Yes
| r1-number = 17/35
| r1-length-m = 3,283
| r1-surface = [[Asphalt]]
| r2-number = 12/30
| r2-length-m = 1,679
| r2-surface = [[Asphalt]]
| stat-year = 2011
| stat1-header = Passengers
| stat1-data = 3,206,103
| stat2-header = Aircraftmovements
| stat2-data = 43,316
| footnotes = Sources: Australian [[Aeronautical Information Publication|AIP]] and aerodrome chart<ref name="AIP">{{AIP AU|YSCB|name=Canberra}}, [http://www.airservicesaustralia.com/aip/current/dap/SCBAD01-130.pdf Aeronautical Chart]</ref><br>passenger and aircraftmovements from the [[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics (BITRE)]]<ref name="dit">{{cite web|url=http://www.bitre.gov.au/publications/ongoing/airport_traffic_data.aspx|title=Airport traffic data|publisher=[[Department of Infrastructure and Regional Development#Bureau of Infrastructure, Transport and Regional Economics|Bureau of Infrastructure, Transport and Regional Economics (BITRE)]]}}</ref>
}}

'''Canberra Airport''' {{Airport codes|CBR|YSCB}}, is the airport serving Australia's capital city, [[Canberra]], the nearby city of [[Queanbeyan|Queanbeyan, NSW]] and the surrounding regional area of South-Eastern [[New South Wales]]. Located at the eastern edge of North Canberra,<ref>{{cite web|url=http://canberra-cbr.airports-guides.com/ |title=Canberra Airport (CBR) Information: Airport in Canberra Area, ACT, Australia, AU |publisher=Canberra-cbr.airports-guides.com |date=16 May 2011|accessdate=30 May 2011}}</ref> it is the eighth [[List of busiest airports in Australia|busiest airport in Australia]].

The airport serves flights to the state capital cities of Australia, to Newcastle, [[Gold Coast, Queensland|Gold Coast]], Wellington (New Zealand) and Singapore. Canberra Airport handled 3,240,848 passengers in financial year 2011.<ref name="airtraf"/><ref>1 July to 30 June</ref> In 2009 Canberra Airport underwent a major redevelopment in which the old terminal was demolished and in its place a new concourse was constructed which fully opened in 2013.<ref name="ReferenceA">{{cite web|url=http://www.canberraairport.com.au/air_newTerminal/new_terminal.cfm |title=Canberra's new terminal |publisher=Canberra Airport |accessdate=30 May 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110518122816/http://www.canberraairport.com.au/air_newTerminal/new_terminal.cfm |archivedate=18 May 2011 }}</ref>

==Location==
The airport is located at the intersection of Canberra's main east-west artery ([[Parkes Way]]/Pialligo Avenue) and eastern ring road ([[Monaro Highway]]/[[Majura Parkway]]) near the semi-rural suburb of [[Pialligo]] about an 8 minutes drive from the city centre, 15 minutes from Gungahlin and 10 minutes from [[Queanbeyan]] at non-peak times; travel times can sometimes be much longer at peak times due to [[traffic congestion]].

The land is currently divided into four areas:
*The passenger terminal and general aviation facility are on the western side of the main runway.
*The Brindabella Business Park is adjacent to the passenger terminal.<ref name=BBP>[http://www.brindabellabusinesspark.com.au/ Brindabella Business Park], brindabellabusinesspark.com.au</ref>
*The ex-air force base area, called [[Fairbairn, Canberra|Fairbairn]], is on the eastern side of the main runway. Fairbairn is home to [[No. 34 Squadron RAAF]], which is responsible for the operations of the [[Royal Australian Air Force VIP aircraft]] and the area is regularly used by visiting [[heads of state]] and [[military aircraft]] in transit.
*A retail and mixed use section on Majura Road which has been named Majura Park.<ref name=MajPk>[http://www.majurapark.com.au/ Majura Park] (retail precinct), majurapark.com.au</ref> Located in or near Majura Park are [[Costco]], an [[IKEA]] store, a small shopping centre and some office buildings.

==Corporate management==
The airport’s controlling entity, is Capital Property Finance Pty Ltd,<ref>{{cite news|url= https://www.commercialrealestate.com.au/news/terry-snows-2-6b-canberra-airport-bonanza/ |title=Terry Snow’s $2.6b Canberra Airport bonanza |author=Stensholt, John}}</ref> which had a 2014-15 income of $405 million.<ref>{{cite news|url= http://www.canberratimes.com.au/act-news/canberra-airport-owners-appear-on-ato-corporate-tax-transparency-report-20161210-gt8avq.html |title=Canberra Airport owners appear on ATO corporate tax transparency report |author=Jeffery, Stephen}}</ref>   The airport is managed and operated by the Canberra Airport Group Pty Ltd. [[Terry Snow]] is the airport's executive chairman and his son Stephen Byron is the managing director.<ref name=CAG/>

==History==
===Early years===
[[File:Fairbane-Canberra.jpg|thumb|The hangars and [[control tower|air traffic control tower]] of [[Defence Establishment Fairbairn]], viewed from the main runway.]]
The airport was built up from an old airstrip that was first laid down in the 1920s, not long after the National Capital site was decided. In 1939 it was taken over by the RAAF, with an area leased out for [[civil aviation]].

On 13 August 1940, in what became known as the [[1940 Canberra air disaster|Canberra air disaster]], a RAAF [[Lockheed Hudson]] flying from Melbourne crashed into a small hill to the east of the airport. Four crew and six passengers, including the Chief of the General staff and three Federal Government ministers, were killed in the accident. [[James Fairbairn]], Minister for Air and Civil Aviation, was one of those killed and Fairbairn Airbase, the eastern component of the airport, was subsequently named after him. In 1962 the military side of the airport was renamed RAAF Base Fairbairn. The North-East quadrant of the airport still retains the Fairbairn name.

The lease to the site was sold to Canberra International Airport Pty Ltd<ref name=cia>{{cite web|url=http://www.canberraairport.com.au/air_aboutAirport/board.cfm |title=Canberra International Airport Pty Ltd |publisher=Canberraairport.com.au |accessdate=30 May 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110717182252/http://www.canberraairport.com.au/air_aboutAirport/board.cfm |archivedate=17 July 2011 |df=dmy }}</ref> in 1998, and the RAAF area was sub-leased back to the Department of Defence. It was decommissioned as a RAAF base in 2003, (although No. 34 Squadron RAAF remains based there), and the RAAF area was renamed [[Defence Establishment Fairbairn]].

Before the airport redevelopment in 2009 there was one building made up two terminals. The former Qantas Terminal at Canberra Airport was located on the western side of the building. All [[Qantas]] and [[QantasLink]] flights and related services such as lounges now operate from the new Southern Concourse Terminal. The former terminal was demolished in 2011 to make way for the building of the second Western Concourse Terminal.<ref name="ReferenceA"/>

The former Common User Terminal was located on the far eastern side of the building. The terminal served [[Virgin Australia]] and briefily [[Tiger Airways]]. Also until 2001 the terminal was the home of [[Ansett Australia]]'s operations from the airport.<ref>{{cite web |url=http://www.canberraairport.com.au/air_terminal/directory.cfm |title=Terminal map and directory |publisher=Canberra Airport |accessdate=30 May 2011}}</ref> However, after the construction of the new Southern Concourse, only the terminal's departure lounge and gates 5 and 6 were in use. The Common User terminal was demolished in June 2013 after the opening of new Southern Concourse.<ref>{{cite web|url=http://www.canberratimes.com.au/act-news/airport-reborn-as-old-arch-foe-meets-its-end-20130619-2ohs7.html|title=Airport reborn as old arch foe meets its end|work=Canberra Times|accessdate=7 June 2015}}</ref>

===Redevelopment===
In 2008, Canberra International Airport launched an advertising campaign advocating the idea of having Canberra considered as Sydney's Second Airport. The slogan used was "Is the solution to Sydney's second airport still 20 years away? Less than 3 hours actually". This point of view was presented at "Canberra is the Only Serious Solution to Sydney's Air Traffic Problems."<ref>[http://www.canberraairport.com.au/ne_sy.htm "Canberra is the Only Serious Solution to Sydney's Air Traffic Problems."] {{webarchive |url=https://web.archive.org/web/20070829235019/http://www.canberraairport.com.au/ne_sy.htm |date=29 August 2007 }}</ref>
[[File:Canberra Airport terminal Gilbert.jpg|thumb|left|The now demolished old Canberra Airport terminal.]]

The Federal Transport Minister [[Anthony Albanese]] rejected Canberra International Airport's draft master plan in November 2008, on the grounds that it did not provide enough detail on the proposal to develop the airport into a freight hub; and that the airport's community consultation had been insufficient.<ref>{{cite news|url=http://www.abc.net.au/news/stories/2008/11/22/2427019.htm|title=Airport plan lacked detail: Albanese|date=22 November 2008|publisher=ABC News|accessdate=22 November 2008}}</ref> The Airport's 2005 master plan was also criticised by the then-[[Howard Government]] for not providing enough information.<ref name="CT_22Nov08">{{cite news|url=http://www.canberratimes.com.au/news/local/news/general/feds-bring-airports-247-ambitions-back-down-to-earth/1367273.aspx?page=0 |title=Feds bring airport's 24/7 ambitions back down to earth |last=McLennan |first=David |date=22 November 2008 |publisher=The Canberra Times |accessdate=22 November 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20091027132234/http://www.canberratimes.com.au/news/local/news/general/feds-bring-airports-247-ambitions-back-down-to-earth/1367273.aspx?page=0 |archivedate=27 October 2009 }}</ref>
[[File:Tiger Airlines loading at Canberra.jpg|thumb|[[Tiger Airways Australia]] jet at Canberra in 2008]]

In the second half of 2008, Canberra International Airport Pty Ltd started referring to itself as "Canberra Airport".<ref>For example, the [http://www.canberraairport.com.au/PDF/hub/Hub45.pdf Issue 45] of "The Hub", dated July 2008, uses the "Canberra International Airport" logo, whereas [http://www.canberraairport.com.au/PDF/hub/Hub46.pdf Issue 46], dated November 2008, uses a "Canberra Airport" logo.</ref>

In early December 2007, plans were announced to construct a new terminal, with works commencing in July 2008, and completion set for September 2010.<ref name="ReferenceB">{{cite web|url=http://www.canberraairport.com.au/air_newTerminal/terminal.cfm |title=Canberra's new terminal |publisher=Canberra Airport |accessdate=30 May 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110501054157/http://www.canberraairport.com.au/air_newTerminal/terminal.cfm |archivedate=1 May 2011 }}</ref> When completed, the terminal would have six aerobridges (an increase of two), 32 check-in counters, (twice the current number), 2,500 car parking spaces (doubled), three times the baggage belt capacity, and the floor area of the lounge facilities would be quadrupled.<ref name=Hub43>[http://www.canberraairport.com.au/air_media/hub08.cfm ''The Hub Newsletter''], Issue 43, January 2008.</ref><ref name="canberraairport.com.au">[http://www.canberraairport.com.au/changes Information and updates about changes to the airport], canberraairport.com.au</ref>

These plans were placed on hold in late 2008 as a result of the [[Financial crisis of 2007–2008|Global economic crisis]].<ref name="CT_22Nov08"/>

In April 2009, Canberra Airport announced that it would spend $350&nbsp;million on a number of infrastructure projects:<ref name=AirV-facts>[http://www.canberraairport.com.au/air_newTerminal/thefacts.cfm "Project key facts"], AirVolution project, Canberra Airport Website. Retrieved on 11 April 2009. {{webarchive |url=https://web.archive.org/web/20090913145858/http://www.canberraairport.com.au/air_newTerminal/thefacts.cfm |date=13 September 2009 }}</ref>
* three new jet aircraft parking positions – under construction
* Two Structured Car Parks (each containing 1,000 parking spaces and an additional 450 spaces in two open air car parks) – Both completed 
* A new Southern concourse Terminal – Completed in late 2010
* A Western concourse Terminal – Partially Opened in March 2013 and to be completed November 2013
Changes to the terminal included:<ref name=AirV-facts/>
*International capability with dedicated customs, immigration and quarantine facilities
*More than double the number of check-in counters (from 17 to 44)
*A quadrupling of baggage capacity
*A quadrupling of Airline Club Lounge areas
*A two-storey roadside drop off and pick up system – departures on the upper level and arrivals on the lower level
*An indoor [[taxi rank]] waiting area (still under construction) – a first for an Australian airport
It placed a 4.5-minute animated video of the planned finished product on its website.<ref>[http://www.canberraairport.com.au/air_newTerminal/images/NewTerminalAnimation.wmv Animated video], planned airport changes, Canberra Airport website. Retrieved on 11 April 2009.</ref>

The project was given the go ahead by Canberra International Airport executive chairman Terry Snow, to start late 2009. It was approved by the Australian Government in February 2008. The new terminal increased space by 65%. Completed as part of the redevelopment were 10 airbridges; two four-level car parks; and a still under construction under-cover taxi rank.<ref>{{cite web|url=http://www.canberraairport.com.au/travellers/the-new-terminal/car-parks/|title=Still to come - Canberra Airport|publisher=|accessdate=7 June 2015}}</ref> Space will be made for the future requirements of international flights.<ref>[http://www.canberraairport.com.au/air_newTerminal/new_terminal.cfm "The Air Volution"], Information about Canberra's (planned) new air terminal, Canberra Airport website. Retrieved on 11 April 2009. {{webarchive |url=https://web.archive.org/web/20090228052714/http://www.canberraairport.com.au/air_newTerminal/new_terminal.cfm |date=28 February 2009 }}</ref>

In 2010, 8 Brindabella Circuit, a building located in the administration area of the Airport precinct, won the 5 Green Stars Australian Excellence Award.<ref>{{cite web|url=http://www.canberraairport.com.au/air_environment/greenStar.cfm |title=5 Green Star 'Australian Excellence' Award |publisher=Canberra Airport |accessdate=30 May 2011}}</ref>

In November 2012, a national petition was started by 10-year-old Eve Cogan to name the new extensions after [[David Warren (inventor)|David Warren]], inventor of the blackbox.<ref>{{cite news |url=http://www.news.com.au/travel/campaign-to-honour-aussie-black-box-inventor/story-e6frfq7r-1226522228613 |title=Girl, 10, campaigns to honour black box inventor |author=Emily Watkins |work=[[News Ltd]] |date= 22 November 2012|accessdate=30 November 2012}}</ref><ref>{{cite news |url=http://blogs.crikey.com.au/planetalking/2013/01/05/the-link-between-unsung-hero-david-warren-and-qf32/ |title=The link between unsung hero David Warren and QF32 |author=Ben Sandilands |work=[[Crikey]] |date= 5 January 2013}}</ref> The petition has been supported by [[Chesley Sullenberger|Captain C.B. "Sully" Sullenberger]].<ref>{{cite news |url=http://www.dailytelegraph.com.au/travel/news/heroic-pilot-backs-little-aussie-girls-campaign/story-e6frezi0-1226560177790 |title=Heroic pilot backs little Aussie girl's campaign |author=Staff writers |work=[[News Ltd]] |date=23 January 2013|accessdate=23 January 2013}}</ref>

===Future===
====International flights====
[[File:9V-SRM taxiing at Canberra Airport September 2016.jpg|thumb|A Singapore Airlines Boeing 777 at Canberra Airport in September 2016]]
In January 2016, [[Singapore Airlines]] announced four weekly flights from [[Singapore Changi Airport|Singapore]] to [[Wellington International Airport|Wellington]] via Canberra with a [[Boeing 777-200]] aircraft. Dubbed "Capital Express" it is the first regular international service to Canberra in years and began on 21 September 2016.<ref name="sq">Flynn, David (20 January 2016). [http://www.ausbt.com.au/singapore-airlines-to-launch-international-flights-from-canberra-airport "Singapore Airlines to launch Singapore-Canberra-Wellington flights"]. ''Australian Business Traveller''. Retrieved 20 January 2016.</ref>

The ACT Government and Canberra Airport had been attempting for years to attract international airlines such as [[Air Asia X]],<ref>{{cite web|last1=Ironside|first1=Robyn|title=AirAsia X eyeing off Avalon, Brisbane and Canberra|url=http://www.perthnow.com.au/travel/travel-news/airasia-x-eyeing-off-avalon-brisbane-and-canberra/story-fnjjv9zm-1227347098144|publisher=Perth Now|accessdate=2015-05-09}}</ref> [[Air New Zealand]], and [[Emirates (airline)|Emirates]] or persuade Qantas or Virgin Australia to commence international flights from Canberra.<ref name="Air New Zealand Canberra flights not happening">{{cite news|last1=McIlroy|first1=Tom|title=Air New Zealand Canberra flights not happening, despite Barr plea|url=http://www.canberratimes.com.au/business/aviation/air-new-zealand-canberra-flights-not-happening-despite-barr-plea-20150305-13u1ap.html|accessdate=6 March 2015|work=The Canberra Times|date=6 March 2015}}</ref><ref>{{cite web|url=http://australianaviation.com.au/2013/11/canberra-woos-singapore-airlines/|title=Canberra woos Singapore Airlines|publisher=|accessdate=7 June 2015}}</ref> The airport argues there is a strong business case for flights to New Zealand. Canberra Airport managing director Stephen Byron said he believed there was a case to support about three flights a week to the capital of Wellington and another three to Auckland.<ref>{{cite web|url=http://www.canberratimes.com.au/act-news/push-for-overseas-flights-into-canberra-20120325-1vsun.html#ixzz2miF7Nt47|title=Push for overseas flights into Canberra|work=Canberra Times|accessdate=7 June 2015}}</ref> In addition, the airport believes in the viability of a direct daily flight to an Asian Hub airport (such as Singapore or Hong Kong) to accommodate one-stop flights to onward destinations in Asia, the Middle East and Europe.<ref name="m.canberratimes.com.au">{{cite news|url=http://www.canberratimes.com.au/act-news/ikea-helps-build-case-for-international-flights-for-canberra-airport-20150119-12tg7a.html|title=IKEA helps build case for international flights for Canberra Airport|publisher=[[The Canberra Times]]|accessdate=7 June 2015}}</ref> Canberra has a population of 900,000 in its catchment area (approximately 75% of that of [[Adelaide]] which has 42 weekly international services from its airport). Its status as Australia's capital city and the above average income of residents in the surrounding area provide more arguments in favour of international services at the airport.<ref name="m.canberratimes.com.au"/>

[[Qatar Airways]] has announced plans to fly into Canberra in either 2017 or 2018.<ref>{{cite news|url=http://www.canberratimes.com.au/act-news/qatar-airlines-announces-flights-to-canberra-with-its-list-of-new-201718-destinations-20161128-gszilh.html  |title=Qatar Airways announces flights to Canberra with its list of new 2017-18 destinations |author=Burgess, Katie|date=29 November 2016|work=[[The Canberra Times]]|accessdate=1 April 2017}}</ref>

====Future traffic trends====
The projected traffic trends for the airport are on the decline,<ref>{{cite web|url=http://centreforaviation.com/news/canberra-airport-the-only-australian-airport-to-record-reduction-in-pax-traffic-for-fy201011-146227|title=Canberra Airport the only Australian airport to record reduction in pax traffic for FY2010/11|publisher=|accessdate=7 June 2015}}</ref> as Federal Government cuts take effect in an effort to reduce costs, and with more employment opportunities in Melbourne and Sydney the ACT is experiencing the largest fall in full-time positions in 2014 than any other state or territory.<ref>{{cite web|url=http://www.canberratimes.com.au/act-news/federal-government-public-service-job-cuts-hurting-act-20141117-11oc0y.html|title=Federal government public service job cuts 'hurting ACT'|work=Canberra Times|accessdate=7 June 2015}}</ref>   Qantas is also downsizing operations at the airport.<ref>{{cite web|url=http://www.smh.com.au/business/aviation/qantas-job-losses-at-canberra-airport-20140515-zrd5c.html|title=Qantas job losses at Canberra Airport|work=The Sydney Morning Herald|accessdate=7 June 2015}}</ref> However managing director of the airport Stephen Byron believes that the airport can grow with the increase in tourism for Canberra and the surrounding area, the establishment of nearby commercial and retail precincts and the potential for the airport to become a freight hub.<ref>{{cite web|url=http://m.canberratimes.com.au/act-news/traffic-at-canberra-airport-down-for-the-fourth-year-in-a-row-20150117-12nb2w.html|title=Traffic at Canberra Airport down for the fourth year in a row|publisher=|accessdate=7 June 2015}}</ref>

====High-speed rail link proposal====
[[File:Canberra Airport viewed from Mount Ainslie June 2014.jpg|thumb|View of Canberra Airport from [[Mount Ainslie]] in June 2014]]
On 10 February 2009, Canberra Airport released its preliminary draft master plan which announced that a [[High-speed rail in Australia|high-speed rail]] link between Sydney, Canberra and Melbourne was being considered. The plan was shortlisted in December 2008 by [[Infrastructure Australia]] for further consideration, however it was the most expensive project shortlisted, and has not attracted any funding from any government. The decision to build the [[Second Sydney Airport]] at [[Badgerys Creek]] has made a fast rail link to Canberra Airport unlikely in the foreseeable future.<ref>Australian Government, [https://www.infrastructure.gov.au/infrastructure/western_sydney,''Western Sydney airport''], Infrastructure Australia, 4 December 2014, accessed 23 December 2014.</ref>

==Facilities==
[[File:Atrium interior at Canberra International Airport.jpg|thumb|Atrium interior looking out on to the tarmac]]
[[File:Canberra Airport terminal (10).jpg|thumb|Part of the Southern Concourse]]
The building's two wings, the Southern Concourse and the Western Concourse are separated by an Atrium, the centrepiece of the terminal.<ref>{{cite web|url=http://www.canberraairport.com.au/travellers/the-new-terminal/terminal/|title=New Terminal - Canberra Airport|publisher=|accessdate=7 June 2015}}</ref>

===Southern Concourse===
Construction of the Southern Concourse was completed in late 2010 and came into service on 14 November.<ref>{{cite web|url=http://www.canberraairport.com.au/air_newTerminal/new_terminal.cfm |title=Canberra's new terminal |deadurl=yes |archiveurl=https://web.archive.org/web/20090228052714/http://www.canberraairport.com.au/air_newTerminal/new_terminal.cfm |archivedate=28 February 2009 }}</ref> Qantas uses its check-in counters and departure gates.<ref name="ReferenceA"/> The Southern Concourse also includes [[Qantas#The Qantas Club|The Qantas Club]], The Qantas Business Class Lounge and The Qantas chairman's Lounge.

===Western Concourse ===
The Western Concourse opened in March 2013 and conjoins onto the Southern Concourse Terminal. Virgin Australia uses its check-in counters and departure gates.<ref>{{cite web|url=http://www.abc.net.au/news/2013-03-13/airport-opens-new-gateway-to-canberra/4569420|title=Airport opens new gateway to Canberra|work=ABC News|accessdate=7 June 2015}}</ref> The Western Concourse also includes the 300 seat Virgin Lounge and Virgin's invitation-only The Club.<ref>{{cite web|url=http://www.ausbt.com.au/virgin-australia-opens-new-canberra-airport-lounge|title=Virgin Australia opens new Canberra Airport lounge|work=Australian Business Traveller|accessdate=7 June 2015}}</ref>

The western concourse was built with space for customs, immigration and quarantine facilities next to the Virgin lounge on the upper floor and on the ground floor. These areas were fitted out and opened when [[Singapore Airlines]] began its Canberra services to [[Wellington International Airport|Wellington]] and [[Singapore Changi Airport|Singapore]].<ref>{{cite web|url=http://www.ausbt.com.au/canberra-airport-opens-new-virgin-australia-terminal-lounges-this-week|title=Canberra Airport opens new Virgin Australia terminal, lounges this week|work=Australian Business Traveller|accessdate=7 June 2015}}</ref> International flights arrive and depart from gate 5.

===General Aviation Terminal===
The General Aviation Terminal in Canberra Airport is a small separate building located on the far west side of the Terminal Precinct.<ref>{{cite web|url=http://www.canberraairport.com.au/PDF/masterplan/approved/7_GeneralAviationAndMilitaryOperations.pdf |title=Microsoft Word - FINAL Canberra Airport 2009 Master Plan, Approved 28.09.09.doc |format=PDF |accessdate=30 May 2011}}</ref><ref>{{cite web|url=http://www.canberraairport.com.au/corporate/about/awards/|title=Awards - Canberra Airport|publisher=|accessdate=7 June 2015}}</ref>

===Brindabella Business Park===
Over a dozen office buildings have also been built on airport land at Brindabella Business Park<ref name=BBP/> and [[Fairbairn (Business Park)|Fairbairn]].<ref name=FB>{{cite web|url=http://www.fairbairn.com.au/ |title=Fairbairn|publisher=Canberra Airport}}</ref> A retail precinct called Majura Park has been established on airport land along Majura Road.<ref name=MajPk/>

The Canberra Spatial Plan released by the [[Australian Capital Territory Legislative Assembly|ACT Government]] in March 2004 identified the airport and surrounding areas as being an important centre for future industrial and related development.<ref>[http://apps.actpla.act.gov.au/spatialplan/ Canberra Spatial Plan], March 2004, ACT Government.</ref>

[[Brindabella Airlines]] had its head office on the airport property prior to the airline's collapse in 2013.<ref>{{cite web|url=http://www.alternativeairlines.com/brindabella-airlines |title=Brindabella Airlines |publisher=alternativeairlines.com |date=10 January 2000|accessdate=30 May 2011}}</ref><ref>"[http://www.brindabellaairlines.com.au/ Home]." [[Brindabella Airlines]]. Retrieved on 19 November 2013. "Brindabella Airlines Pty Ltd, 5 Rayner Road Canberra Airport PO Box 1542"</ref>

==Airlines and destinations==
{{Airport-dest-list
<!-- -->
| [[FlyPelican]]| [[Dubbo Airport|Dubbo]],<ref>{{cite news |url=http://www.dailyliberal.com.au/story/4330335/new-air-links-to-canberra/ | title= Flights between Dubbo and Canberra will start from January 30 next year | publisher=Daily Liberal |date= 2 December 2016}}</ref> [[Newcastle Airport (New South Wales)|Newcastle]]<ref>{{cite news |url=http://australianaviation.com.au/2015/05/pelican-gets-green-light-to-start-newcastle-canberra/ |title=Pelican gets green light to start Newcastle-Canberra route |publisher=Australian Aviation |date=27 May 2015}}</ref>
<!-- -->
| [[Qantas]] | [[Adelaide Airport|Adelaide]], [[Brisbane Airport|Brisbane]], [[Melbourne Airport|Melbourne]], [[Perth Airport|Perth]], [[Sydney Airport|Sydney]]
<!-- -->
| [[QantasLink]]<br>operated by [[Cobham Aviation Services Australia|Cobham]] | [[Brisbane Airport|Brisbane]], [[Melbourne Airport|Melbourne]], [[Sydney Airport|Sydney]] 
<!-- -->
| [[QantasLink]]<br>operated by [[Eastern Australia Airlines]] | [[Melbourne Airport|Melbourne]], [[Sydney Airport|Sydney]]
<!-- -->
| [[QantasLink]]<br>operated by [[Sunstate Airlines]] | [[Brisbane Airport|Brisbane]]
<!-- -->
<!--Do not add the service until exact start date is found the year is not sufficient see talk page! [[Qatar Airways]] | [[Hamad International Airport|Doha]] (begins 2017) <ref name="mightytravels.com">[http://www.mightytravels.com/2016/11/expedia-cybermonday-90-off-club-carlson-30-off-transatlantic-business-class-under-1000-35-off-priority-pass-qatar-adds-dublin-las-vegas-santiago-de-chile/  Qatar adds eight new destinations - MightyTravels]</ref>-->
<!-- -->
| [[Singapore Airlines]] | [[Singapore Changi Airport|Singapore]], [[Wellington International Airport|Wellington]]<ref name="sq"/>
<!-- -->
| [[Tigerair Australia]] | [[Melbourne Airport|Melbourne]]<ref>http://www.canberratimes.com.au/act-news/cheap-flights-to-melbourne-return-tigerair-announces-daily-route-20160822-gqxz8d.html</ref>
<!-- -->
| [[Virgin Australia]] | [[Adelaide Airport|Adelaide]], [[Brisbane Airport|Brisbane]], [[Gold Coast Airport|Gold Coast]], [[Melbourne Airport|Melbourne]], [[Sydney Airport|Sydney]]
<!-- -->}}

==Statistics==

===Total passengers and aircraft movements===
{|class="wikitable sortable"
|-
! Year
! Actual<br />passengers<ref name="airtraf">[https://bitre.gov.au/publications/ongoing/airport_traffic_data.aspx Airport Traffic Data]</ref>
! 2003<br />forecast<ref name="Forecast03">[http://www.canberraairport.com.au/corporate/planning-environment/masterplan/ 2005 Canberra Airport Master Plan] pp.24–25</ref><!-- These 2003 forecast - projections are sus.  Refer Talk Page -->>
! Total<br>movements<ref name="airtraf"/>
! 2003<br>forecast<ref name=Forecast03/>
|-
| 1997–98 || 1,824,515 || || 38,446 ||
|-
| 1998–99 || 1,820,757 || || 38,077 ||
|-
| 1999–00 || 1,969,221 || || 41,025 ||
|-
| 2000–01 || 2,107,219 || || 51,867 ||
|-
| 2001–02 || 1,841,302 || || 39,716 || 90,281
|-
| 2002–03 || 1,916,351 || 2,176,603 || 35,986 || 93,296
|-
| 2003–04 || 2,303,422 || || 39,418 ||
|-
| 2004–05 || 2,478,705 || 2,280,557 || 38,512 ||
|-
| 2005–06 || 2,550,129 || || 38,182 ||
|-
| 2006–07 || 2,687,336 || || 38,257 ||
|-
| 2007–08 || 2,853,480 || || 41,177 ||
|-
| 2008–09 || 3,061,859 || 2,829,882 || 45,191 ||
|-
| 2009–10 || 3,258,396 || || 44,201 ||
|-
| 2010–11 || 3,240,848 || || 43,280 ||
|-
|2011–12
|3,158,685
|
|42,938
|
|-
|2012–13
|3,013,960
|
|41,816
|
|-
|2013–14
|2,857,618
|
|40,498
|
|-
| 2014–15 || 2,803,989
|| 3,476,797 || 38,718
|| 116,072
|-
|2015–16
|2,814,717
|
|37,147
|
|-
| 2019–20 || || 4,270,094 || ||
|-
| 2024–25 || || 5,212,007 || || 146,159
|}

===Busiest domestic routes===
{| class="wikitable sortable" width= align=
|+ Busiest domestic routes into and out of Canberra Airport (year end 2013)<ref name="airtraf"/>
|- style="background:lightgrey;"
! Rank|| Airport || Passengers handled|| %  <!---  presumably '2012-2013' --> Change
|-
|1||{{Sort|04| [[Sydney Airport|Sydney]]}} New South Wales||1,027,600||{{decrease}}{{Sort|01|2.4}}
|-
|2||{{Sort|03| [[Melbourne Airport|Melbourne]]}} Victoria||994,500||{{decrease}}{{Sort|02|0.9}}
|-
|3||{{Sort|02| [[Brisbane Airport|Brisbane]]}} Queensland||583,000||{{decrease}}{{Sort|03|3.7}}
|-
|4||{{Sort|01| [[Adelaide Airport|Adelaide]]}} South Australia||182,200||{{increase}}{{Sort|04|4.1}}
|}

==Advertising==
While billboards have been barred in Canberra since the 1930s, an amendment of the [[National Capital Authority|National Capital Plan]] in 2000 allowed them to be displayed at Canberra Airport.<ref>{{cite news|url= http://www.canberratimes.com.au/act-news/act-may-relax-its-ban-on-billboards-20170125-gtyc7g.html |title=ACT may relax its ban on billboards |author=Burgess, Kate|date=25 January 2017|work=[[Canberra Times]]|accessdate=12 April 2017}}</ref>  Subsequently the airport has hosted advertisements promoting defence harware.  A community group said the airport should not be promoting [[weapons manufacturers]].<ref>{{cite news|url=http://www.canberratimes.com.au/act-news/campaign-to-remove-weapons-advertisements-at-canberra-airport-20150824-gj69ad.html  |title=Campaign to remove weapons advertisements at Canberra airport| work=[[The Canberra Times]]|accessdate=12 April 2017}}</ref>  The airport defended the ads and said the airport would continue to accept defence industry advertising.<ref>{{cite news|url=http://www.canberratimes.com.au/act-news/canberra-airport-to-be-presented-with-petition-criticising-defence-advertising-20151027-gkk09y.html  |title=Canberra Airport to be presented with petition criticising defence advertising |  work=[[The Canberra Times]]| accessdate=12 April 2017}}</ref>  In 2015 the airport was lit up in [[Rainbow flag (LGBT movement)|rainbow colors]],<ref>{{cite news|url=http://www.abc.net.au/news/2015-08-10/canberra-airport-lights-up-for-marriage-equality/6684238  |title=Canberra Airport puts support for marriage equality up in lights |author=|date=10 August 2015|work=ABC|accessdate=12 April 2017}}</ref> and in 2017 electronic and 3D message boards, were used to support [[marriage equality]].<ref>{{cite news|url= http://www.passengerterminaltoday.com/viewnews.php?NewsID=84463 |title=Canberra Airport to promote marriage equality to traveling politicians |author=Pickering, Karstie|date=20 March 2017|work=Passenger Terminal Today|accessdate=12 April 2017}}</ref>

==Environment==
Approach and departure corridors lie over largely rural and industrial areas, although the instrument approach path (from the south) passes near the New South Wales suburb of [[Jerrabomberra]], the city of Queanbeyan, and the [[Royal Australian Navy]] base, [[HMAS Harman]], which has some barracks and housing.

Proposals have been made to the NSW Planning Minister by various developers to approve housing estates that are under the southern flight paths in New South Wales. Canberra International Airport Pty Ltd<ref name=cia/> has been vigorous in advertising its opposition to these plans on the basis of a general increase in noise levels over a wide corridor which is currently free of aircraft noise,<ref>This is referred to as "Noise Sharing". See {{cite web|url=http://www.canberraairport.com.au/air_noise/noise_planning.cfm|title=Aircraft Noise – Land Use Planning document|publisher=Canberra International Airport Pty Ltd|accessdate=28 October 2007}} and [http://www.canberraairport.com.au/air_noise/noise_sharing.cfm Noise Sharing, Canberra International Airport Pty Ltd] for an explanation of their rationale.</ref> and concern that this will lead to the imposition of a curfew on the hours-of-operation of the airport.<ref name=Hub40pg4>[http://www.canberraairport.com.au/air_media/hub07.cfm "Judge's Ruling says noise will be a problem at Tralee"], ''The Hub'', Issue 40 (September 2007), pg4. Canberra Airport Newsletter.</ref>

==Road traffic and road traffic congestion==
The road system around Canberra Airport and the road between Civic and Canberra Airport was being duplicated as at July 2008, partly funded by Canberra Airport and the ACT Government. Federal Labor had also committed to further road improvements in the area through the extension of the Monaro Highway.<ref>{{cite news |last= |author2= |title=Labor party media release |url=http://www.alp.org.au/media/1007/msats120.php |publisher=|date=12 October 2008|accessdate=18 June 2008|archiveurl = https://web.archive.org/web/20080330010159/http://www.alp.org.au/media/1007/msats120.php |archivedate = 30 March 2008}}</ref><ref name=Hub45>{{cite news |last= |author2= |title=The Hub |url=http://www.canberraairport.com.au/air_media/hub08.cfm |work=Issue 45 |publisher=Canberra International Airport Pty Ltd |date=July 2008 |accessdate=18 June 2008}}</ref>

The Chief Minister of the ACT Government, [[Jon Stanhope]], initially blamed the Commonwealth for the increased traffic congestion around the airport, which he claimed had occurred due to the construction of office buildings on airport land,<ref>{{cite news |last= |author2= |title=Stanhope blames Commonwealth for airport congestion |url=http://www.abc.net.au/news/stories/2007/03/27/1882913.htm |publisher=[[ABC News (Australia)|ABC News]] |date=7 March 2007|accessdate=18 August 2007}}</ref> however, Mr Stanhope later stated that while he accepted the development of the airport added to the level of traffic on the roads, it was not the cause of the congestion during peak periods.<ref>{{cite news|last= |author2= |title=Letter from Mr Jon Stanhope to Mr Stephen Byron, 15 January 2007, contained in submission to the National Capital Authority Inquiry |url=http://www.aph.gov.au/house/committee/ncet/natcapauth/subs/sub070.pdf |publisher= |date=15 January 2007 |accessdate=18 June 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20110320083621/http://www.aph.gov.au/house/committee/ncet/natcapauth/subs/sub070.pdf |archivedate=20 March 2011 }}</ref> The ACT Government established a roundtable working group to examine the roads around the Airport and identify solutions to the road congestion through the Majura Valley.<ref name=roundtable>{{cite news |last= |author2= |title=Airport Roads Roundtable|url=http://www.chiefminister.act.gov.au/media.asp?media=1372&section=54&title=Media%20Release&id=54 |publisher=Jon Stanhope Media Release |accessdate=19 August 2007}}</ref> The roundtable identified that the cause of the road traffic was increased traffic from [[Gungahlin]];, the expansion of the airport; and Queanbeyan's growing population.<ref name="WGinterim">{{cite news|url=http://www.tams.act.gov.au/__data/assets/word_doc/0010/39943/Canberra_Airport_Roads_Working_Group_Interim_Report.doc|title=Canberra Airport Roads Working Group – Interim Report|last=|date=1 June 2006|publisher=[[ACT Government]]|author2=}}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref><ref name=boost>{{cite news |last= |author2= |title=Media Release: $15&nbsp;million to Boost Road Access to Airport |url=http://www.treasury.act.gov.au/budget/budget_2007/files/press/61_press.pdf |publisher=[[ACT Government]] |date=1 October 2006|accessdate=5 June 2007}}</ref> The working group recommended a staged approach to solving the traffic congestion, with Stage 1 including the duplication of Pialligo Avenue, Morshead Drive and Fairbairn Avenue.<ref name="WGfinal">{{cite news|url=http://www.tams.act.gov.au/__data/assets/word_doc/0010/39943/Canberra_Airport_Roads_Working_Group_Final_Report.doc|title=Canberra Airport Roads Working Group – Final Report|last=|date=1 October 2006|publisher=[[ACT Government]]|author2=|accessdate=19 August 2007}}</ref>

== See also ==
{{Portal|Aviation|Australian Capital Territory|World War II}}
* [[Transportation in Australia]]
* [[List of airports in territories of Australia]]
{{clear}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category inline|Canberra International Airport}}
* [http://www.canberraairport.com.au/ Official website]

{{Airports in Australia}}
{{Canberra landmarks}}

[[Category:Airfields of the United States Army Air Forces in Australia]]
[[Category:Airports in the Australian Capital Territory]]
[[Category:Buildings and structures in Canberra]]