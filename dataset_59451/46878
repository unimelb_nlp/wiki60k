[[File:Christian Caryl in 2016.jpg|thumb|Christian Caryl at the Legatum Institute]]

'''Christian Caryl''' is an American journalist who is widely published in international politics and foreign affairs. Currently, he is a senior fellow at the [[Legatum Institute]] in London and a Contributing Editor at [[Foreign Policy magazine]]. He edits [http://democracylab.foreignpolicy.com/ Foreign Policy’s Democracy Lab], an online project developed in conjunction with Legatum to provide coverage of countries that are trying to make the transition to democracy.<ref>{{cite web|title=Democracy Lab|url=http://www.li.com/programmes/democracy-lab|work=Foreign Policy|publisher=THE FP GROUP|accessdate=10 April 2013}}</ref>

== Early life ==
A native of [[Midland, Texas|Midland]], [[Texas]], Caryl currently resides in [[Bethesda, Maryland|Bethesda]], [[Maryland]]. From 1978 to 1980, he attended [[Deerfield Academy]] in [[Deerfield, Massachusetts|Deerfield]], [[Massachusetts]].

Caryl received a [[Bachelor of Arts]] in Literature, cum laude, from [[Yale College]] in 1984. Caryl has also studied foreign languages extensively: French language study at L’Institut Catholique, Paris, France; Russian language study, Pushkin Russian Language Institute, Moscow, Russia; and Japanese language study, [[Middlebury College]], Middlebury, Vermont.

== Career ==
After his graduation from [[Yale College]] in 1984, Caryl traveled to [[Germany]] for a year of study at the [[University of Constance]] on a scholarship from the West German government.<ref name="Ocala News Article">{{cite news|title=Yale Scholar Has Family In McIntosh|url=https://news.google.com/newspapers?nid=1356&dat=19840627&id=ZWExAAAAIBAJ&sjid=VgYEAAAAIBAJ&pg=6204,6183265|accessdate=10 April 2013|newspaper=Ocala Star Banner|date=June 27, 1984}}</ref>  After finishing his studies, he moved to [[Berlin]], where he began working as a freelance translator and copywriter. He got his start as a journalist in 1989, when he began assisting foreign correspondents covering the collapse of [[East Germany]] and the fall of the [[Berlin Wall]].<ref name="Who Brought Down the Berlin Wall?">{{cite web|last=Caryl|first=Christian|title=Who Brought Down the Berlin Wall?|url=https://foreignpolicy.com/articles/2009/11/06/who_brought_down_the_berlin_wall?page=0,2|work=Foreign Policy|publisher=The FP Group|accessdate=13 April 2013}}</ref>  He then went on to do his own writing for publications including [[The Wall Street Journal]], [[The New Republic]], [[The Spectator]] and [[Der Spiegel]].

In 1991, Caryl traveled to the Soviet republic of [[Kazakhstan]], where he became the deputy director of a new state-sponsored institution of higher education, the Kazakhstan Institute of Management, Economics, and Strategic Research.<ref name=Diplomaatia>{{cite web|title=Christian Caryl|url=http://www.diplomaatia.ee/en/translation-policy/authors/christian-caryl/|work=Diplomaatia|publisher=International Centre for Defence Studies & SA Kultuurileht|accessdate=13 April 2013}}</ref>  Kazakhstan achieved its independence from the USSR shortly after his arrival. By the time of his departure at the end of 1992, he had experienced Kazakhstan’s first full year as a sovereign state.

From 1997 to 2000, Caryl served as [[Moscow]] bureau chief for ''[[U.S. News & World Report]]''.  From 2000 to 2004, Caryl served as [[Newsweek]]’s Moscow Bureau Chief.<ref name="Diplomaatia" />

After the [[September 11 attacks]], as part of Newsweek's reporting on the [[War on Terror]], he carried out assignments in [[Iraq]] and [[Afghanistan]].<ref name="MIT Fellows">{{cite web|title=MIT Fellows|url=http://web.mit.edu/cis/dir_senior_fellows.html|work=MIT|accessdate=13 April 2013}}</ref> 
 
From 2004 to March 2009, he led the [[Tokyo]] Bureau of Newsweek.<ref name="Diplomaatia" />  In that capacity he was in charge of the magazine’s reporting on [[North Korea]], which he visited on several occasions.<ref name="Hermit Kingdom">{{cite web|last=Caryl|first=Christian|title=The Hermit Kingdom|url=https://foreignpolicy.com/articles/2009/10/19/the_hermit_kingdom?page=0,0|work=Foreign Policy|publisher=The FP Group|accessdate=13 April 2013}}</ref>

From 2009 to 2010, he served as the [[Washington, D.C.]] Chief Editor for [[Radio Free Europe / Radio Liberty]].<ref name="Radio Free Europe / Radio Liberty">{{cite web|title=Christian Caryl Named Head of RFE Washington Bureau|url=http://www.rferl.org/content/_Christian_Caryl_press_release_press_room/2177464.html|work=Radio Free Europe / Radio Liberty|publisher=Radio Free Europe / Radio Liberty|accessdate=13 April 2013}}</ref>  In addition to his current posts, he is also a Senior Fellow at the Center for International Studies at the [[Massachusetts Institute of Technology]] and a regular contributor to [[The New York Review of Books]].<ref name="MIT Fellows" />

Caryl speaks English, Russian, and German. During his journalistic career, he has reported from some 50 countries.

Following the [[Boston Marathon bombing]], Caryl was the [http://www.nybooks.com/blogs/nyrblog/2013/apr/28/tamerlan-tsarnaev-misha-speaks/ first to interview "Misha"], who had been accused of radicalizing [[Tamerlan Tsarnaev]].

Caryl’s first book, [http://www.amazon.com/Strange-Rebels-1979-Birth-Century/dp/0465018386 ''Strange Rebels''], was published on April 30, 2013 by [[Basic Books]]. This non-fiction book looks closely at the year 1979 and the lasting impact it has had on foreign affairs and economics. ''Strange Rebels'' received a positive review from [http://www.economist.com/news/books-and-arts/21576067-why-1979-was-about-so-much-more-margaret-thatchers-election-victory-when-world The Economist].

== Awards ==
* 2010 [[Overseas Press Club]] award for “Best Online Commentary.”<ref name="Overseas Press Club of America">{{cite web|title=Best Online Commentary 2010|url=https://www.opcofamerica.org/awards/best-online-commentary-2010|work=Overseas Press Club of America|accessdate=13 April 2013}}</ref>  
* 2004 Member of reporting team that won [[National Magazine Awards]] prize to Newsweek for “[http://www.thedailybeast.com/newsweek/2003/11/02/the-87-billion-money-pit.html The $87 Billion Money Pit],” a Nov. 3, 2003 cover story on the misuse of government funds in the [[Iraq War]].<ref name="Radio Free Europe / Radio Liberty" />  
* 1999 Finalist in the [[International Consortium of Investigative Journalists]] Award for Outstanding International Investigative Reporting (with co-author David Kaplan) for “Dirty Diamonds,” an expose of Russian diamond smuggling in the August 3, 1998 issue of ''[[U.S. News & World Report]]''.<ref name="MIT Fellows" />

== Bibliography ==
''[http://www.amazon.com/Strange-Rebels-1979-Birth-Century/dp/0465018386 Strange Rebels]'', Christian Caryl (Basic Books, 2012). ISBN 978-0465018383

== TV/radio appearances ==
Caryl has provided commentary and analysis for [[National Public Radio]], [[Public Radio International]], [[CNN]], and the [[Young Turks]].<ref name="NPR Citation 2">{{cite web|title=Foreign Policy: Burma's Nuclear Ambitions|url=http://www.unz.org/Pub/NPR-2010jun-01123|work=UNZ|accessdate=14 April 2013}}</ref><ref name=Takeaway>{{cite web|last=Weinberger|first=Jillian|title=1979: The Birth of the Modern Age|url=http://www.thetakeaway.org/2013/apr/30/1979-birth-modern-age/|work=The Takeaway|publisher=Public Radio International|accessdate=1 May 2013}}</ref><ref name="Young Turks">{{cite web|title=TYT interview Christian from Newsweek|url=http://www.unz.org/Pub/YoungTurks-2006-00188|work=UNZ|accessdate=14 April 2013}}</ref>

Following his scoop on "Misha," he appeared on [[On the Record w/ Greta Van Susteren]]<ref name=Greta>{{cite web|last=Van Susteren|first=Greta|title=Boston terror probe: Who is the mysterious 'Misha'?|url=http://www.foxnews.com/on-air/on-the-record/index.html#http://video.foxnews.com/v/2338904976001/boston-terror-probe-who-is-the-mysterious-misha/?playlist_id=86925|work=On the Record w/ Greta Van Susteren|publisher=Fox News|accessdate=1 May 2013}}</ref> and [[Erin Burnett OutFront]].<ref name="Out Front">{{cite web|last=Burnett|first=Erin|title='Misha' denies radicalizing Boston bombing suspects|url=http://outfront.blogs.cnn.com/2013/04/29/misha-denies-radicalizing-boston-bombing-suspects/|work=Out Front|publisher=CNN|accessdate=1 May 2013}}</ref>

== Published works ==
* [http://www.nybooks.com/blogs/nyrblog/2013/apr/28/tamerlan-tsarnaev-misha-speaks/ 'Misha' Speaks: An Interview with the Alleged Boston Bomber's 'Svengali'] (New York Review of Books; April 28, 2013)
* [http://www.nybooks.com/articles/archives/2012/jul/12/burmese-days/ Burmese Days] (New York Review of Books; July 12, 2012)
* [http://www.nybooks.com/articles/archives/2007/jan/11/what-about-the-iraqis/ What About the Iraqis?] (New York Review of Books; Jan. 11, 2007)

== References ==
{{reflist}}

==External links==
{{Commons category}}
* {{C-SPAN|christiancaryl}}

{{Authority control}}

{{DEFAULTSORT:Caryl, Christian}}
[[Category:American male journalists]]
[[Category:American foreign policy writers]]
[[Category:American magazine editors]]
[[Category:International relations scholars]]
[[Category:American male writers]]
[[Category:Living people]]
[[Category:Year of birth missing (living people)]]
[[Category:Yale University alumni]]