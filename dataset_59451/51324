{{About|the journal "Feminist Theory"|feminist theory the subject|feminist theory}}

{{Infobox journal
| title = Feminist Theory
| cover = [[File:Feminist Theory.jpg]]
| editor = Stacy Gillis, Celia Roberts, Carolyn Pedwell, Sarah Kember 
| discipline = [[Gender studies]]
| former_names = 
| abbreviation = 
| publisher = [[Sage Publications]]
| country = 
| frequency = Triannually 
| history = 2000-present
| openaccess = 
| license = 
| impact = 1.268
| impact-year = 2015
| website = http://www.uk.sagepub.com/journals/Journal200796?siteId=sage-uk&prodTypes=any&q=Feminist+Theory&fs=1| link1 = http://fty.sagepub.com/content/current
| link1-name = Online access
| link2 = http://fty.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1464-7001
| eISSN =1741-2773
| OCLC = 439137540
| LCCN = 00233878
}}
'''''Feminist Theory''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[women’s studies]]. The journal's [[editors-in-chief]] are Stacy Gillis ([[Newcastle University]]), Celia Roberts ([[Lancaster University]]), Carolyn Pedwell ([[Newcastle University]]), and  Sarah Kember ([[Goldsmith's College]]). It was established in 2000 and is currently published by [[Sage Publications]].

== Abstracting and indexing ==
''Feminist Theory'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2015 [[impact factor]] is 1.268, ranking it 12th out of 40 journals in the category "Women's Studies".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science |postscript=.}}</ref>

== See also ==
* [[List of women's studies journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://fty.sagepub.com/}}

[[Category:English-language journals]]
[[Category:Feminist journals]]
[[Category:Publications established in 2000]]
[[Category:SAGE Publications academic journals]]
[[Category:Triannual journals]]
[[Category:Women's studies journals]]


{{Womens-journal-stub}}