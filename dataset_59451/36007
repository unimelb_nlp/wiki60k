{{Good article}}{{Infobox album | <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Alapalooza
| Type        = [[Album]]
| Artist      = [["Weird Al" Yankovic]]
| Cover       = Weird Al Alapalooza.jpg
| Alt         = Front cover of the Alapalooza album. A skeletal tyrannosaurs with the head of "Weird Al" Yankovic is framed by a yellow circle with a shadowy jungle and a red border across the entire scene. The name of the artist and the album appear in white letters above a pure black background. 
| Released    = October 5, 1993
| Recorded    = November 1992&nbsp;– August 1993
| Genre       = [[Comedy music|Comedy]], [[parody music|parody]]
| Length      = 44:34
| Label       = [[Rock 'n Roll Records]]<br>[[Scotti Brothers Records|Scotti Brothers]]
| Producer    = "Weird Al" Yankovic
| Last album  = ''[[The Food Album]]'' <br /> (1993)
| This album  = '''''Alapalooza''''' <br /> (1993)
| Next album  = ''[[Permanent Record: Al in the Box]]'' <br /> (1994)
| Misc        = {{Singles
| Name        = Alapalooza
| Type        = studio
| single 1    = [[Jurassic Park (song)|Jurassic Park]]
| single 2    = [[Bedrock Anthem]]
| single 3    = [[Achy Breaky Song]]
| single 1 date = July 1, 1993
| single 2 date = November 16, 1993
| single 3 date = December 7, 1993
}}
}}

'''''Alapalooza''''' is the eighth [[studio album]] by [["Weird Al" Yankovic]], released in [[1993 in music|1993]]. By the completion of his previous album, ''[[Off the Deep End]]'', Yankovic had already written all of the original songs that he planned to use on his next release. This new album, which would eventually be titled ''Alapalooza'' in reference to the music festival [[Lollapalooza]], consisted of seven original songs and five parodies. It produced three parody singles: "[[Jurassic Park (song)|Jurassic Park]]", "[[Bedrock Anthem]]", and "[[Achy Breaky Song]]". "Jurassic Park" was a top five hit on the Canadian magazine ''[[The Record (magazine)|The Record]]''{{'}}s single chart.

Among the album's original creations were "Talk Soup", a tune originally intended to replace the theme song of the [[Talk Soup|television show of the same name]], and "Harvey the Wonder Hamster", an oft-requested jingle from one of Yankovic's [[Al TV]] specials. A music video compilation, entitled ''Alapalooza: the Videos'', was released the following year and contained four videos, only two of which were from its eponymous album. One of the videos, the one for "Jurassic Park", was animated entirely in the style of [[clay animation|claymation]] and received a nomination for the [[Grammy Award for Best Short Form Music Video]] at the [[37th Grammy Awards]], losing to "[[Love Is Strong]]" from [[The Rolling Stones]].

''Alapalooza'' met with average to negative reception upon its release, with some critics commenting that the album seemed hurried and out of touch with contemporary music. The video offering received a similarly lukewarm response. Nonetheless the album was certified "gold" in the United States by the [[Recording Industry Association of America]] by the end of the year, peaking at number 46 on the [[Billboard 200|''Billboard'' 200]], and went "double platinum" in Canada.

==Production==

===Background===
Yankovic's 1992 album ''[[Off the Deep End]]'', his best-selling album since 1984's ''[["Weird Al" Yankovic in 3-D]]'', had revived his career and displayed his "credibility as an evolving artist"<ref name="DeepEnd">{{Cite web|last = Weber|first = Barry|title = Off the Deep End – Weird Al Yankovic : Songs, Reviews, Credits, Awards : AllMusic|publisher = [[AllMusic]]|url = {{Allmusic|class=album|id=r123458|pure_url=yes}}|accessdate = June 14, 2012}}</ref> after the commercial failures of his 1986 work ''[[Polka Party!]]'' and his feature film ''[[UHF (film)|UHF]]''.<ref name="DeepEnd"/> By the time production for ''Off the Deep End'' was nearing completion, Yankovic had already written all of the original songs that would be eventually included on ''Alapalooza''. Fearing that his track "I Was Only Kidding" would be outdated by the time of his next album, he rearranged ''Off the Deep End'' to allow for the song to be released with the album, saving "Waffle King" for ''Alapalooza''. Nevertheless, "Waffle King" was released as a B-side to ''Off the Deep End''{{'}}s "[[Smells Like Nirvana]]" single, "just in case there wasn't going to be a next album."<ref name="AlMar96">{{cite web|last = Yankovic|first = Al|authorlink = "Weird Al" Yankovic|title = Midnight Star 'Ask Al' Q&As for March/April, 1996|work = Ask Al Archive|publisher = weirdal.com|date = April 1996|url = http://weirdal.com/archives/miscellaneous/ask-al/#0396|accessdate = June 14, 2012}}</ref> He recorded all of the album's original songs, except "Talk Soup" and "Harvey the Wonder Hamster", by the end of 1992 and, in July 1993, recorded all of ''Alapalooza''{{'}}s remaining tracks, aside from "[[Livin' In The Fridge]]".<ref name="Recording">{{cite web|last = Yankovic|first = Al|authorlink = "Weird Al" Yankovic|title = Recording Dates|publisher = weirdal.com|year = 2012|url = http://weirdal.com/archives/miscellaneous/recording-dates/|accessdate = June 14, 2012}}</ref> Yankovic eventually decided to title his new album ''Alapalooza'', a reference to the [[Lollapalooza]] music festival.<ref name="History">{{cite web|last = Khanna|first = Vish|title = 'Weird Al' Yankovic Alpocalypse Now… and Then|url = http://exclaim.ca/Features/Timeline/weird_al_yankovic-alpocalypse_now8230_then/Page/7|accessdate = June 14, 2012}}</ref> The Yankovic dinosaur in the album's booklet was designed by David Peters, who had worked previously with the singer on the "[[Dare to Be Stupid (song)|Dare to Be Stupid]]" video.<ref name="Albox">{{cite AV media notes|title = Permanent Record: Al in the Box|others = [["Weird Al" Yankovic]]|year = 1994|url = http://dmdb.org/al/booklet.html|first = Barret|last = Hansen|authorlink = Dr. Demento|type = liner|publisher = [[Scotti Brothers Records]]|location = [[California]], [[United States]]}}</ref>

''Alapalooza'' was released on October 5, 1993 in the United States. Globally, some versions included a notice distinguishing it from the [[Jurassic Park (film score)|official ''Jurassic Park'' film soundtrack]], as the two cover designs were similar.<ref name="Not">{{cite web|title = Canada Alapalooza CD|publisher = Allthingsyank.com|date = June 14, 2012|url = http://www.allthingsyank.com/disc/canadaalapaloozacd.jpg|accessdate = June 14, 2012}}</ref> The [[Japan]]ese edition contained a bonus track of Yankovic singing "Jurassic Park" in Japanese.<ref name="Albox"/> A music video compilation for the album, entitled ''Alapalooza: the Videos'', was released in February 1994 and contained four videos, only two of which ("[[Jurassic Park (song)|Jurassic Park]]" and "[[Bedrock Anthem]]") were from ''Alapalooza''.<ref name="Entertainment">{{Cite web|url = http://www.ew.com/ew/article/0,,301506,00.html|last = Kenny|first = Glenn|title = Alapalooza Review|publisher = [[Entertainment Weekly]]|date = March 18, 1994|accessdate = June 14, 2012}}</ref>

===Originals===
''Alapalooza'' contains seven original songs among its twelve tracks, although "Young, Dumb & Ugly" and "Frank's 2000" TV" were meant to be stylistic parodies of [[AC/DC]] and the early work of [[R.E.M.]], respectively. For the former, Yankovic wanted to parody the [[heavy metal music]] genre while at the same time avoiding a repetition of what had already been done by [[Spinal Tap (band)|Spinal Tap]].  He ended up disliking the final product because he sang it "in a [[Register (music)|register]] that was really too high for [his] singing voice".<ref name="AVClub">{{cite web|last = Rabin|first = Nathan|title = Set List: 'Weird Al' Yankovic|url = http://www.avclub.com/articles/weird-al-yankovic,58244/|publisher = [[The Onion]]|work=[[The A.V. Club]]|accessdate = June 14, 2012}}</ref> The latter was a song about consumerism and modern electronics that described the neighborhood's envy of the eponymous character's new television.<ref name="AVClub"/>

The song "Talk Soup", which is about a man who desires to go on television to tell the world about his strange life, was originally commissioned as a new theme for the [[E!|E! Entertainment Television]] [[Talk Soup|show of the same name]]. Although the producers approved the lyrics and enjoyed the final result, they decided against using it.<ref name="AlSep95">{{cite web|last = Yankovic|first = Al|authorlink = "Weird Al" Yankovic|title = Midnight Star 'Ask Al' Q&As for September, 1995|work = Ask Al Archive|publisher = weirdal.com|date = September 1995|url = http://weirdal.com/archives/miscellaneous/ask-al/#0995|accessdate = June 14, 2012}}</ref> "Waffle King", the track that had been intended for ''Off the Deep End'', was written as "a song about a guy who becomes incredibly famous for doing something kinda stupid, and then starts taking himself way too seriously".<ref name="AlJan97">{{cite web|last = Yankovic|first = Al|authorlink = "Weird Al" Yankovic|title = Midnight Star 'Ask Al' Q&As for January/February, 1997|work = Ask Al Archive|publisher = weirdal.com|date = February 1997|url = http://weirdal.com/archives/miscellaneous/ask-al/#0197|accessdate = June 14, 2012}}</ref> Yankovic included "Harvey the Wonder Hamster", a short tune from one of his [[Al TV]] appearances, after receiving numerous requests to include it on an album.<ref name="Albox"/>

===Parodies===
[[File:JimmyWebb 2005 Crop.jpg|thumb|''Alapalooza'' lead single "Jurassic Park" is a parody of "[[MacArthur Park (song)|MacArthur Park]]" by Jimmy Webb.]]
Yankovic's first single from ''Alapalooza'' was "[[Jurassic Park (song)|Jurassic Park]]", a parody of the [[Jimmy Webb]] song "[[MacArthur Park (song)|MacArthur Park]]" that was first performed by [[Richard Harris]] in 1968.<ref name="MacArthur">{{cite news|last = Boucher|first = Geoff |url = http://www.latimes.com/entertainment/la-ca-socal10jun10,0,6518022.story|title=The SoCal Songbook: 'MacArthur Park,' Jimmy Webb, 1968|newspaper = [[Los Angeles Times]]|publisher = [[Tribune Company]]|date = June 10, 2007|accessdate = June 14, 2012}}</ref> After hearing "[[Lola (song)|Lola]]" by [[The Kinks]] on the radio and recalling how much he had enjoyed his previous pairing of a contemporary film with a classic song (1985's "[[Yoda (song)|Yoda]]"), Yankovic came up with the idea for a tune based around the recently released [[Jurassic Park (film)|''Jurassic Park'' film]].  He received permission from Webb, ''[[Jurassic Park (novel)|Jurassic Park]]'' author [[Michael Crichton]], and director [[Steven Spielberg]] to produce the track. For the music video Yankovic collaborated with animators [[Mark Osborne (filmmaker)|Mark Osborne]] and Scott Nordlund to produce a [[clay animation|claymation]] feature that parodied scenes from the movie;<ref name="Albox"/> the song itself was a comedic retelling of the film's plot interspersed with the gripes about his visit to the park.<ref name="Polka">{{cite news|last = Jenkins|first = Mark|title = POP RECORDINGS;Pearl Jam's Second: More R.E.M. Than Grunge|url = http://www.highbeam.com/doc/1P2-971372.html|newspaper = [[The Washington Post]]|date = October 24, 1993|accessdate = June 14, 2012}} {{subscription required|via=[[HighBeam Research|HighBeam]]}}</ref> The music video was directed by Osborne and Nordlund, while Yankovic came up with the original concept and ideas for some of the shots; Osborne said that the directors "came up with about half the ideas in collaboration" with Yankovic.<ref name="Mania">{{cite web|last=Biodrowski |first=Steve |title=MORE: A Little Film on a Big, Big Screen |publisher=Mania |date=November 28, 1999 |url=http://www.mania.com/more-little-film-big-big-screen_article_2108.html |accessdate=June 26, 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20131017224249/http://www.mania.com/more-little-film-big-big-screen_article_2108.html |archivedate=October 17, 2013 |df= }}</ref>

Having always wanted to write a tribute to ''[[The Flintstones]]'', Yankovic next focused his energy on creating a song that he hoped would be current with the impending release of [[The Flintstones (film)|''The Flintstones'' live action film]] in 1994. In order to collect sound bites and animation and "re-familiarize" himself with the characters, Yankovic watched over 100 episodes of the original show. A parody of both [[Red Hot Chili Peppers]]'s "[[Under the Bridge]]" and "[[Give It Away (Red Hot Chili Peppers song)|Give It Away]]", the resulting song was a comedic tribute to the program. It ended up becoming the second single released from ''Alapalooza''.<ref name="Albox"/> Yankovic directed the video for the single, which featured scenes of band members playing the song in [[Bedrock (The Flintstones)|Bedrock]] dressed as characters from the show.<ref name="Entertainment"/> In the third and final single, "[[Achy Breaky Song]]", a parody of [[Billy Ray Cyrus]]' "[[Achy Breaky Heart]]", Yankovic lists things he would rather experience than having to listen to the original track. The parody received radio play on [[country music]] stations in the United States.<ref name="Albox"/> The proceeds from the track were donated to [[United Cerebral Palsy]], as both [[Don Von Tress]] (the songwriter of "Achy Breaky Heart") and Yankovic felt that the parody was "a little bit [...] mean-spirited".<ref name="AlMay99">{{cite web|last = Yankovic|first = Al|authorlink = "Weird Al" Yankovic|title = 'Ask Al' Q&As for May, 1999|work = Ask Al Archive|publisher = weirdal.com|date = May 1999|url = http://weirdal.com/archives/miscellaneous/ask-al/#0599|accessdate = June 14, 2012}}</ref>

"Livin' in the Fridge", a parody of [[Aerosmith]]'s "[[Livin' on the Edge]]" that discusses leftovers that have grown sentient in the refrigerator, was the last song to be recorded for the album. With a deadline looming, Yankovic sent requests to several artists to do parodies of their songs. He ultimately went with Aerosmith because they replied first.<ref name="Albox"/> It was recorded a month after the rest of the tracks had been finalized and less than two months prior to the album's release.<ref name="Recording"/> The album includes a [[List of "Weird Al" Yankovic polka medleys|polka medley]], a staple of Yankovic's albums,<ref name="Medley">{{cite web|last = Berman|first = Judy|title = A Brief History of Weird Al’s Polka Medleys|work = Music|publisher = Flavorwire|date = June 15, 2011|url = http://www.flavorwire.com/187436/a-brief-history-of-weird-als-polka-medleys|accessdate = June 26, 2012}}</ref> called "[[List of "Weird Al" Yankovic polka medleys#.22Bohemian Polka.22|Bohemian Polka]]". Unlike previous medleys, which had featured portions of multiple songs,<ref name="Medley"/> "Bohemian Polka" contains only one tune, [[Queen (band)|Queen's]] "[[Bohemian Rhapsody]]", and is a rearrangement of the entire song as a [[polka]].<ref name="Polka"/>

==Reception==

===Critical reception===
{{Album ratings
| rev1 =  [[Allmusic]]
| rev1Score =  {{Rating|2|5}}<ref name="allmusic">{{Cite web|last = Weber|first = Barry|title = Alapalooza – Weird Al Yankovic: Songs, Reviews, Credits, Awards : AllMusic|publisher = [[Allmusic]]|url = http://www.allmusic.com/album/alapalooza-mw0000102769|accessdate = 14 June 2012}}</ref>
| rev2 =  ''[[Entertainment Weekly]]''
| rev2Score =  C<ref name="Entertainment"/>
| rev3 =  ''[[Rolling Stone]]''
| rev3Score =  {{Rating|2.5|5}}<ref name="Stone">{{Cite book|url = https://books.google.com/books?id=t9eocwUfoSoC&pg=PA893&lpg=PA893&dq=rolling+stone+weird+al+yankovic+alapalooza+review&source=bl&ots=BiKjoj_Q-4&sig=RJMw0SNqDBegd5zbGtfs91pICj8&hl=en&sa=X&ei=Anx3T-O1K-P40gGEsIGwDQ&ved=0CFAQ6AEwBg#v=onepage&q&f=false|last = Brackett|first = Nathan|author2= Christian Hoard|title = The Rolling Stone Album Guide|publisher = Simon and Schuster|year = 2004|location = [[New York City]], [[New York (state)|New York]]|page = 893|isbn = 0-7432-0169-8}}</ref>
| rev4 =  ''[[The Buffalo News]]''
| rev4Score = {{Rating|3|5}}<ref name="Buffalo"/>
}}

Critical response to ''Alapalooza'' ranged from average to negative. In [[The Rolling Stone Album Guide]] ''Alapalooza'' earned 2.5 stars out of 5, which ranked it somewhere between "mediocre" and "good".<ref name="Stone"/> Anthony {{Not a typo|Violanti}} of ''[[The Buffalo News]]'' gave the album three stars out of five, claiming that "[o]nce again, Weird Al gets the last laugh on rock 'n' roll".<ref name="Buffalo">{{cite news|last = Violanti|first = Anthony|title = Big Time Moving Into the Mainstream, Mr. Big Shows Its Tender Side|url = http://www.highbeam.com/doc/1P2-22538923.html|newspaper = [[The Buffalo News]]|date = October 8, 1993|accessdate = June 14, 2012}} {{subscription required|via=HighBeam}}</ref> Barry Weber of [[Allmusic]], on the other hand, criticized the album for failing to engage contemporary musical trends and said it "sounds sloppy and mostly like a compilation of old B-sides".<ref name="allmusic"/> Christopher Thelen of The Daily Vault agreed, calling the album "rushed" and "an incredible disappointment",<ref name="Vault">{{Cite web|last = Thelen|first = Christopher|title = Alapalooza|publisher = Daily Vault|date = September 27, 2002|url = http://dailyvault.com/toc.php5?review=1977|accessdate = June 14, 2012}}</ref> and said it was one of the lows of Yankovic's career. Thelen's limited praise is directed at only two songs on the album: "Livin' In The Fridge" and "Achy Breaky Song", both of which he claimed "have [their] moments".<ref name="Vault"/> In reference to the album's polka tune, Mark Jenkins of ''[[The Washington Post]]'' wrote that it "doesn't sound all that different" from the original.<ref name="Polka"/>

''[[Entertainment Weekly]]'' felt that overall ''Alapalooza: the Videos'' was "amusing", but referred to the [[clay animation|claymation]] video for the "Jurassic Park" as "clever but toothless". The magazine gave the collection an overall grade of "C" and argued that Yankovic's parodies did not satirize the original material, but instead transposed new elements on top of them.<ref name="Entertainment"/> The video for "Jurassic Park" was nominated for a [[Grammy Award for Best Short Form Music Video]] at the [[37th Grammy Awards]],<ref>{{cite news|url = http://articles.latimes.com/1995-01-06/entertainment/ca-17089_1_vocal-performance/8|title = The 37th Grammy Nominations|page = 16|date = January 6, 1995|accessdate = June 14, 2012|newspaper= [[Los Angeles Times]]|publisher = [[Tribune Company]]}}</ref> but lost to the video for "[[Love Is Strong]]" by [[The Rolling Stones]].<ref name="Grammy">{{cite web|title = 1994 Best Music Video, Short Form|work = Past Winners Search|publisher = [[National Academy of Recording Arts and Sciences]]|year = 1995|url = http://www.grammy.com/nominees/search?artist=&title=&year=1994&genre=18|accessdate = June 14, 2012}}</ref> Nonetheless, it received attention in animation festivals worldwide for its use of claymation effects.<ref name="Albox"/>

===Commercial performance===
Released in October 1993, ''Alapalooza'' was certified gold by the [[Recording Industry Association of America]] on December 23, 1993, representing sales of at least 500,000 units. The video compilation, released on February 1, 1994, went gold in the United States on August 14, 1995, representing sales of at least 50,000 units.<ref name="riaa">{{Cite web|title = Gold & Platinum – Search Results: Alapalooza|publisher = [[Recording Industry Association of America]]|url = https://www.riaa.com/gold-platinum/?tab_active=default-award&se=Alapalooza#search_section|accessdate = October 6, 2016}}</ref> In Canada the album went gold on November 16, 1993, platinum on January 31, 1994, and double platinum on February 12, 1998, representing sales of 50,000, 100,000, and 200,000 units respectively.<ref name="MusicCanada">{{cite web|title = Gold Platinum Database|publisher = [[Music Canada]]|year = 2012|url = http://musiccanada.com/gold-platinum/?fwp_gp_search=Yankovic|accessdate = February 23, 2015}}</ref> The album peaked at number 46 on the United States' [[Billboard 200|''Billboard'' 200]] chart on October 30, 1993, but produced no charting singles.<ref name="AllMusicChart">{{Cite web|title = Weird Al Yankovic – Awards : AllMusic|publisher = [[AllMusic]]|url = {{Allmusic|class=artist|id=p140212/charts-awards/billboard-singles|pure_url=yes}}|accessdate = June 14, 2012}}</ref> In Canada, however, "Jurassic Park" was a top five hit on the ''[[The Record (magazine)|The Record]]''{{'}}s single chart.<ref name=recordcanada/>

==Track listing==
The following is adapted from the album liner notes.<ref name="liner">{{cite AV media notes|title = Alapalooza|others = [["Weird Al" Yankovic]]|year = 1993|type = liner|publisher = [[Scotti Brothers Records]]}}</ref>
{{Track listing
| collapsed       = 
| extra_column    = Parody of
| writing_credits = yes

| title1          = [[Jurassic Park (song)|Jurassic Park]]
| writer1         = [[Jimmy Webb]], [["Weird Al" Yankovic|Alfred Yankovic]]
| extra1          = "[[MacArthur Park (song)|MacArthur Park]]" by [[Richard Harris]]
| length1         = 3:55

| title2          = Young, Dumb & Ugly
| writer2         = Yankovic
| extra2          = Style parody of [[AC/DC]]<ref name="AVClub"/>
| length2         = 4:24

| title3          =  [[Bedrock Anthem]]
| writer3         = [[Anthony Kiedis]], [[John Frusciante]], [[Flea (musician)|Michael Balzary]], [[Chad Smith|Chadwick Smith]], Yankovic
| extra3          = "[[Under the Bridge]]" and "[[Give It Away (Red Hot Chili Peppers song)|Give It Away]]" by the [[Red Hot Chili Peppers]]
| length3         = 3:43	

| title4          =  Frank's 2000" TV
| writer4         = Yankovic
| extra4          = Style parody of [[R.E.M.]]'s early work<ref name="AVClub"/>
| length4         = 4:07	

| title5          = [[Achy Breaky Song]]
| writer5         =  [[Don Von Tress]], Yankovic
| extra5          = "[[Achy Breaky Heart]]" by [[Billy Ray Cyrus]]
| length5         = 3:23

| title6          = Traffic Jam
| writer6         = Yankovic
| extra6          = Original<!-- DON'T ADD A BAND/GENRE HERE UNLESS YOU HAVE A RELIABLE SOURCE TO BACK IT UP -->
| length6         = 4:01

| title7          = Talk Soup
| writer7         = Yankovic
| extra7          = Original<!-- DON'T ADD A BAND/GENRE HERE UNLESS YOU HAVE A RELIABLE SOURCE TO BACK IT UP -->
| length7         = 4:25

| title8          = Livin' in the Fridge
| writer8         = [[Steven Tyler]], [[Joe Perry (musician)|Anthony Pereira]], [[Mark Hudson (musician)|Mark Hudson]], Yankovic
| extra8          = "[[Livin' on the Edge]]" by [[Aerosmith]]
| length8         = 3:55

| title9          = She Never Told Me She Was a Mime
| writer9         = Yankovic
| extra9          = Original<!-- DON'T ADD A BAND/GENRE HERE UNLESS YOU HAVE A RELIABLE SOURCE TO BACK IT UP -->
| length9         = 4:54

| title10         = Harvey the Wonder Hamster
| writer10        = Yankovic
| extra10         = Original<!-- DON'T ADD A BAND/GENRE HERE UNLESS YOU HAVE A RELIABLE SOURCE TO BACK IT UP -->
| length10         = 0:21

| title11         = Waffle King
| writer11        = Yankovic
| extra11         = Style parody of [[Peter Gabriel]]<ref name="Waffle">{{cite news|last = McCall|first = Tris|authorlink=Tris McCall|title = Song of the Day: 'Dare to Be Stupid,' 'Weird Al' Yankovic|newspaper = [[The Star-Ledger]]|date = May 17, 2011|url = http://www.nj.com/entertainment/music/index.ssf/2011/05/song_of_the_day_dare_to_be_stu.html|accessdate = July 28, 2012}}</ref>
| length11        = 4:25

| title12         = [[List of "Weird Al" Yankovic polka medleys#"Bohemian Polka"|Bohemian Polka]]
| writer12        = [[Freddie Mercury]]
| extra12         = Polka version of "[[Bohemian Rhapsody]]" by [[Queen (band)|Queen]]
| length12        = 3:39	
}}

==Credits and personnel==
{{col-begin}}
{{col-break}}
;Band members<ref name="liner"/>
*"Weird Al" Yankovic – lead and background vocals, [[keyboard instrument|keyboards]], and [[accordion]]
*[[Jim West (guitarist)|Jim West]] – [[guitar]]s, [[banjo]], [[mandolin]], and background vocals
*[[Steve Jay]] – [[bass guitar]] and background vocals
*[[Jon Schwartz (drummer)|Jon "Bermuda" Schwartz]] – [[percussion instrument|percussion]] and [[Drum kit|drums]]

;Production<ref name="liner"/>
*Tony Papa – engineering and mixing
*Colin Sauers – assistant engineering
*Jamie Dell – assistant engineering
*[[Bernie Grundman]] – mastering
*Jay Levey – management
*Spencer Proffer – executive produced on "Talk Soup"
*Doug Haverty – art direction
*Command A Studios – design
*David Peters – dinosaur imagery
*David Westwood – logo design
*[[Rocky Schenck]] – inside photography
{{col-break}}

;Other personnel<ref name="liner"/>
*[[Rubén Valtierra]] – keyboards
*[[Brad Buxer]] – keyboards, orchestral arrangements and programming on "Jurassic Park"
*Warren Luening – [[trumpet]]
*Joel Peskin – [[clarinet]] and [[baritone saxophone]]
*[[Tommy Johnson (tubist)|Tommy Johnson]] – [[tuba]]
*Julia Waters – background vocals
*Maxine Waters – background vocals
*Sandy Berman – dinosaur sound effects
*"Musical Mike" Kieffer – musical hands
*[[Alan Reed]] – voice of [[Fred Flintstone]]
*[[Mel Blanc]] – voice of Barney Rubble and Dino
{{col-break}}
{{col-end}}

==Charts and certifications==
{{col-begin}}
{{col-2}}

=== Charts ===
{| class="wikitable sortable"
|-
!Chart
!Peak<br />Position
|-
|[[Billboard 200|US ''Billboard'' 200]]
| style="text-align:center;"|46<ref name="AllMusicChart"/>
|}
{{col-2}}

=== Certifications ===
{| class="wikitable sortable"
|-
!Country
![[Music recording sales certifications|Certification]]<br /><small>([[List of music recording sales certifications|sales thresholds]])</small>
|-
|[[Recording Industry Association of America|United States]]
| style="text-align:center;"|Gold<ref name="riaa"/>
|-
|[[Music Canada|Canada]]
| style="text-align:center;"|Double Platinum<ref name="MusicCanada"/>
|}
{{col-end}}

===Singles===
{|class="wikitable"
|-
!rowspan="2"| Year
!rowspan="2"| Song
!colspan="1"| Peak positions
|- style="font-size:85%;line-height:1.3;vertical-align:top"
!style="width:4em"| [[The Record (magazine)|CAN<br>Record]]<br><ref name=recordcanada>{{cite journal|title=Hits of the World|journal=[[Billboard (magazine)|Billboard]]|date=February 19, 1994|volume=106|issue=8|accessdate=April 28, 2013|url=https://books.google.com/books?id=KQgEAAAAMBAJ&pg=PA46&dq=jurassic+park+weird+al+canada+billboard&hl=en&sa=X&ei=UY59UaXhMM7y2gXep4G4Cw&ved=0CDkQ6AEwAg#v=onepage&q=jurassic%20park%20weird%20al%20canada%20billboard&f=false}}</ref>
|-
| 1994
| "[[Jurassic Park (song)|Jurassic Park]]"
| style="text-align:center;"| 5
|}

==References==
{{reflist|2}}

{{"Weird Al" Yankovic}}

[[Category:"Weird Al" Yankovic albums]]
[[Category:1993 albums]]
[[Category:English-language albums]]