*

'''CaseIT''' is an international undergraduate business [[case competition]] focused in [[management information systems]]. Held annually in [[Vancouver, British Columbia]], this student-organized event focuses on information technology case analysis.<ref>{{cite web|url=http://www.ciocan.ca/chapters/vancouver/chapterprojects/ |title=Archived copy |accessdate=2011-08-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20110729001043/http://www.ciocan.ca:80/chapters/vancouver/chapterprojects/ |archivedate=2011-07-29 |df= }}</ref> Over a span of four days in February, 20 university teams compete in a 24-hour case deliberation. First round presentations are held in SFU's [[Harbour Centre]] campus in Vancouver, while final-round presentations are held in the [[Segal Graduate School of Business]], followed by an Awards Banquet where the top three universities are announced.<ref>{{cite web|url=http://universitychallenges.com/caseit-2011 |title=Archived copy |accessdate=2011-11-14 |deadurl=yes |archiveurl=https://web.archive.org/web/20111107010247/http://universitychallenges.com:80/caseit-2011 |archivedate=2011-11-07 |df= }}</ref>

== History ==

Co-founded in 2004 by Paul Cyr and Zephaniah Wong, CaseIT began as a competition between undergraduate teams within [[Simon Fraser University]]. Jenny Chao expanded the competition to incorporate teams from Western Canada and eventually from all across Canada, under the leadership of Andrew Kumar. In 2007, Somnath Suresh included teams from the United States and Denmark and in the following year, Rey Lim changed the format into a 24-hour case competition with international teams from Africa and Asia. Steven Chia stabilized the growth of CaseIT in 2009 by formalizing and institutionalizing many processes. Furthering the progression of CaseIT, Chair Nima Sarhangpour played an integral role in the incorporation of the CaseIT Foundation. CaseIT 2012 also saw the first University from Australia take part in the event. CaseIT 2014 saw the addition of the Wildcard Round through Chair Jillian-Joy Marlinga. Gwendoline Wong increased the number of teams competing to 19 team for CaseIT 2015 and combined the Discussion Panel and the Corporate Evening events. In addition, Gwendoline launched InTech, a case competition held parallel to the international competition exclusively for students at Simon Fraser University.<ref>http://caseit.org/about/</ref>

== Competition ==

=== Organizing Committee ===

Since its inception, CaseIT has been a student-organized event. Originally consisting of a committee of 16 members, it has since grown to 30 members. The Organizing Committee consists of the Chairs, Directors, Executives, Team Executives, and Team Hosts.

[[File:CaseIT 2013 OC.jpg|thumb|CaseIT 2013 Organizing Committee]]

{| class="wikitable" style="text-align: center; width: 400px; height: 500px;"
! Year
! Chair
! Vice-Chair
|-
| 2017 || Flora Yang || Zachary Chua & Gianna Wu
|-
| 2016 || Moses To || Tiffany Fong & Sunny Kim
|-
| 2015 || Gwendoline Wong || Cheryl Chan
|-
| 2014 || Jillian-Joy Marlinga || Nicholas Heng
|-
| 2013 || Peter Lew || Maggie Lee
|-
| 2012 || Nima Sarhangpour || Gordon Swenson
|-
| 2011 || Brian Luong || Ryan Torio
|-
| 2010 || May Yu || Stephen Lee
|-
| 2009 || Steven Chia || Amanda Yeo
|-
| 2008 || Rey Lim || Paulina Siu
|-
| 2007 || Somnath Suresh || Gloria Ching
|-
| 2006 || Andrew Kumar || Tabitha Mui
|-
| 2005 || Jenny Chao || Zephaniah Wong
|-
| 2004 || Paul Cyr || Zephaniah Wong
|-
|}

=== Participants ===

Since the competition opened to schools from around the world, CaseIT has hosted university teams from Africa, Asia, Europe, and North America.<ref>{{cite web|url=http://financialstoryteller.com/?p%3D84 |title=Archived copy |accessdate=2011-11-14 |deadurl=yes |archiveurl=https://web.archive.org/web/20120425162139/http://financialstoryteller.com/?p=84 |archivedate=2012-04-25 |df= }}</ref> Some notable participating schools in the past include:
Bocconi University School of Economics 
Tepper School of Business, Carnegie Mellon University
Carlson School of Management, University of Minnesota
Corvinus University of Budapest
Copenhagen Business School
HEC Montreal
John Molson School of Business, Concordia University
Haskane School of Business, University of Calgary
Hong Kong University of Technology and Science
Kelley School of Business, Indiana University
Kwantlen Polytechnic University
Maastricht University
National University of Singapore
Queen's School of Business
Queensland University of Technology
Richard Ivey School of Business, University of Western Ontario
Royal Roads University
Ryerson University
Sauder School of Business, University of British Columbia
Singapore Management University
University of Alberta
University of Vermont

=== Results ===

Twenty teams are chosen through an invitational process to participate in the 24-hour case deliberation held in Vancouver. Participants have 24 hours to solve and present their team’s solution to a judging panel composed of industry professionals from CaseIT sponsor companies which range in a variety of firms.<ref>http://caseit.org/competition/</ref> From these teams, one finalist is chosen from each tier after the first round of presentations. After the final round of presentations, the top three teams are selected by the judging panel. The past results of CaseIT are shown below with the corresponding theme for each year.<ref>http://www.techvibes.com/blog/canadian-schools-give-strong-showing-at-caseit-2011-queens-takes-first-place-2011-02-19</ref><ref>http://msiskelley.blogspot.com/2011/02/kelley-school-of-business-team-places.html</ref>

{| class="wikitable" style="text-align: center; width: 900px; height: 350px;"
! Year
! Theme
! 1st Place
! 2nd Place
! 3rd Place
|-
|2017
|Augmented Reality: The Future at Your Fingertips
|[[Kelley School of Business]] <br /> (Indiana University)
|[[Queen's University]]
|[[Haskayne School of Business]] <br /> (University of Calgary)
|-
|2016
|Cognitive Analytics: 
Adapting Machine for Thought
|[[Queen's University]]
|[[Copenhagen Business School]]
|[[Corvinus University of Budapest]] 
|-
| 2015 || The Internet of Things: <br /> Challenges of the Future || [[University of Manchester]] || [[Queen's University]] || [[Indiana University]]
|-
| 2014 || Big Data: <br /> Mining Decisions || [[University of British Columbia]] || [[Queensland University of Technology]] || [[Queen's University]]
|-
| 2013 || Mobility: <br /> World Within Reach || [[Kwantlen Polytechnic University]] || [[Queen's University]] || [[Ryerson University]]
|-
| 2012 || Security: <br /> Safeguarding Success || [[Kwantlen Polytechnic University]] || [[National University of Singapore]] || [[Kelley School of Business]] <br /> (Indiana University)
|-
| 2011 || Analytics: <br /> Insight into the Future  || [[Queen's University]] || [[Kelley School of Business]] <br /> (Indiana University) || [[National University of Singapore]]
|-
| 2010 || Cloud Computing: <br /> Opportunity is in the Forecast || [[Bocconi University]] || [[Kelley School of Business]] <br /> (Indiana University) || [[National University of Singapore]]
|-
| 2009 || Green IT:<br /> Sustaining Success || [[National University of Singapore]] || [[Ryerson University]] || [[Kwantlen Polytechnic University]]
|-
| 2008 || Hello World:<br /> The Value of Social Networks || [[Simon Fraser University]] || [[Singapore Management University]] || [[Haskayne School of Business]] <br /> (University of Calgary)
|-
| 2007 || Driving Innovation ||[[Haskayne School of Business]] <br /> (University of Calgary) || [[Royal Roads University]] || [[Copenhagen Business School]]
|-
| 2006 || Triumphs and Trainwrecks:<br /> Getting It Right || [[Haskayne School of Business]] <br /> (University of Calgary)|| [[Royal Roads University]] || [[Queen’s University]]
|-
| 2005 || - || [[Royal Roads University]] || [[Kwantlen Polytechnic University]] || [[Haskayne School of Business]] <br /> (University of Calgary)
|-
|}

=== Venues ===

''Venues for CaseIT may change from year-to-year. The following venues are where CaseIT was held in 2013.''

'''Morris J. Wosk Centre for Dialogue'''

Comfortable in its arrangement as an open environment, the hall is home for interaction and dialogue and the site for CaseIT's Discussion Panel. Asia Pacific Hall has hosted heads of states, political leaders, deliberations, and private negotiations since opening in September 2000.<ref>http://www.sfu.ca/mecs/wosk+dialogue+centre/about.html</ref>

'''Vancouver Aquarium'''

[[File:Aurora and baby 2 - vancouver aquarium.jpg|thumbnail|right|Vancouver Aquarium]]

The Corporate Function is held at the Arctic Canada Gallery at the [[Vancouver Aquarium]]. Located in Vancouver's [[Stanley Park]], the Vancouver Aquarium was Canada's first public aquarium when it opened in 1956. The Aquarium is dedicated to effecting the conservation of aquatic life through display and interpretation, education, research and direct action.<ref>http://www.vanaqua.org/about</ref> Participants network with one another while watching beluga whales swim by.

'''Harbour Centre'''

Simon Fraser University’s downtown campus blends history with state-of-the-art educational facilities in one of Vancouver’s most famous landmark buildings.<ref>http://harbourcentre.com/</ref> After the 24-hour deliberation period, 21 teams present their first round presentations here showcasing their analytical and presentation skills to the CaseIT Judging Panel.

'''Segal Graduate School of Business'''

The [[Segal Graduate School of Business]], located in Vancouver’s business district, delivers graduate management education and executive programs in a restored heritage building that facilitates discussion and interaction amongst professionals, academics, and students.<ref>{{cite web|url=http://business.sfu.ca/support/priorities/segal/ |title=Archived copy |accessdate=2011-08-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20110318051453/http://business.sfu.ca:80/support/priorities/segal/ |archivedate=2011-03-18 |df= }}</ref> The top four universities are announced here and present their recommendations for a second time to over 150 attendees.

== Media ==
* September 19, 2014: [http://poetsandquantsforundergrads.com/2014/09/19/why-you-should-go-to-a-business-school-that-does-case-competitions/ "Business School Case Tournaments: Why You Should Compete"]
* January 30, 2013: [http://www.mngt.waikato.ac.nz/Newsroom/Alumni%20News/Article3282.aspx "WMS students win place at international 24hr IT case comp"]
* February 12, 2012: [http://beedie.sfu.ca/blog/2012/02/beedie-hosted-caseit-competition-draws-global-class/ "Beedie School of Business" on CaseIT 2012 and the global presence in Vancouver] 
* February 19, 2011: [http://www.techvibes.com/blog/canadian-schools-give-strong-showing-at-caseit-2011-queens-takes-first-place-2011-02-19 "Techvibes" on the strong showing of Canadian universities at CaseIT 2011]
* May 11, 2010: [http://files.caseit.org/media/BIV-CaseIT2010.pdf "Business in Vancouver" on how CaseIT helped a competitor get his foot in the door with a career in IT]
* April 19, 2010: [http://www.viasarfatti25.unibocconi.eu/notizia.php?idArt=5613 "The Bocconi Online Newsmagazine" on the success of its students in CaseIT 2010]
* April 8, 2010: [http://beedie.sfu.ca/news/post.php?id=1484 "Beedie School of Business" on CaseIT and how it is planned]
* February 2, 2009: [http://www.sfu.ca/archive-pamr/media_releases/media_releases_archives/media_02020902.html "SFU Public Affairs and Media Relations" on the CaseIT 2009, Green IT]
* June 11, 2008: [http://www.asiaone.com/News/Education/Story/A1Story20080609-69843.html "AsiaOne News" on the success of the team from Singapore Management University in CaseIT 2008]

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.caseit.org CaseIT]

[[Category:Organizations based in British Columbia]]