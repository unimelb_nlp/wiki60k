{{Infobox file format
| name          = XBAP
| icon          = 
| extension     = <tt>.xbap</tt>
| mime          = application/x-ms-xbap
| type code     =
| uniform type  =
| owner         =
| genre         = [[Package management system]], [[file archive]]
| container for = [[software package (installation)|Software package]]
| contained by  =
| extended from = [[ZIP (file format)|ZIP]]
| extended to   =
| standard      =
}}

'''XAML Browser Applications''' ('''XBAP''', pronounced "ex-bap") are [[Windows Presentation Foundation]] (.xbap) applications that are hosted and run inside a [[web browser]] such as [[Mozilla Firefox|Firefox]] or [[Internet Explorer]]. Hosted applications run in a partial trust [[Sandbox (computer security)|sandbox environment]] and are not given full access to the computer's resources like opening a new network connection or saving a file to the computer disk and not all [[Windows Presentation Foundation|WPF]] functionality is available. The hosted environment is intended to protect the computer from [[Malware|malicious applications]]; however it can also run in full trust mode by the client changing the permission. Starting an XBAP from an [[HTML]] page is seamless (with no security or installation prompt). Although one perceives the application running in the browser, it actually runs in an out-of-process executable (PresentationHost.exe) managed by a [[Common Language Runtime|virtual machine]]. In the initial release of [[.NET Framework|.NET Framework 3.0]], XBAPs only ran in Internet Explorer. With the release of [[.NET Framework|.NET Framework 3.5 SP1]], which includes an XBAP extension, they also run in Mozilla Firefox.<ref>{{cite web
| url         = http://www.xbap.org/
| title       = What is XBAP?
| publisher   = XBap.org
| page        = Home page
| accessdate  = 2011-02-16
| quote       = XBAP (XAML Browser Application) is a new Windows technology used for creating [[Rich Internet Application]]s with a file extension .xbap to be run inside the Internet Explorer. They are run within a security sandbox to prevent untrusted applications from controlling local system resources.
}}
</ref>

==XBAP limitations==
XBAP applications have certain restrictions on what [[.NET Framework|.NET]] features they can use. Since they run in partial trust, they are restricted to the same set of permission granted to any InternetZone application. Nearly all standard WPF functionality, however, around 99%, is available to an XBAP application. Therefore, most of the WPF [[User Interface|UI]] features are available.<ref>
{{cite web
| url         = https://msdn.microsoft.com/en-us/library/aa970910.aspx
| title       = WPF Partial Trust Security
| publisher   = [[MSDN]]
| accessdate  = 2011-02-16
| quote       = For XBAP applications, code that exceeds the default permission set will have different behavior depending on the security zone. In some cases, the user will receive a warning when they attempt to install it. The user can choose to continue or cancel the installation. The following table describes the behavior of the application for each security zone and what you have to do for the application to receive full trust.
}}</ref>

Starting in February 2009, XBAP applications no longer function when run from the Internet.<ref>https://blogs.msdn.microsoft.com/ieinternals/2011/02/11/ie9-rc-minor-changes-list/</ref> Attempting to run the XBAP will cause the browser to present a generic error message.<ref>https://blogs.msdn.microsoft.com/ieinternals/2011/03/09/ie9-xbaps-disabled-in-the-internet-zone/</ref> An option exists in Internet Explorer 9 that can be used to allow the applications to run,<ref>http://stackoverflow.com/a/6474872/12597</ref> but this must be done with care as it increases the potential attack surface - and there have been security vulnerabilities in XBAP.<ref>https://technet.microsoft.com/library/security/ms13-004?f=255&MSPPError=-2147217396</ref>

===Permitted===
*2D drawing
*3D
*Animation
*Audio

===Not permitted===
*Access to OS [[drag-and-drop]]
*Bitmap effects (these are deprecated in .NET 3.5 SP1)
*Direct database communication (unless the application is fully trusted)
*Interoperability with Windows controls or [[ActiveX]] controls
*Most standard dialogs
*Shader effects
*Stand-alone Windows

==See also==
* [[Extensible Application Markup Language]] (XAML)
* [[ClickOnce]]
* [[Google Native Client]] (NaCl)
* [[HTML Application]] (HTA)
* [[Microsoft Silverlight]]
* [[WebAssembly]]
* [[Windows Runtime XAML Framework]]

==References==
{{reflist}}

==External links==
* [https://msdn.microsoft.com/en-us/library/aa480229.aspx Windows Presentation Foundation Security Sandbox]
* [http://channel9.msdn.com/Blogs/Charles/WPF-XBAP Channel 9 WPF XBAP Video]
*[http://supersuperclass.appspot.com/xbap-in-browsers.html Run XBAP in IE, Firefox, Chrome, Opera and Maxthon]

{{.NET Framework}}
{{Web interfaces}}

[[Category:.NET Framework]]
[[Category:Microsoft application programming interfaces]]


{{web-software-stub}}