{{Use dmy dates|date=January 2012}}
{{Use Australian English|date=January 2012}}
{{coord|-34.992597|138.632355|display=title}}
{{Infobox rail line
| name          = Belair railway line
| color         = {{Adelaide Metro color|Belair}}
| image         = AdelaideRail 9.jpg
| image_alt     = 
| caption       = 
| system        = 
| status        = 
| locale        = [[Adelaide, South Australia]]
| start         = [[Adelaide railway station|Adelaide]]
| end           = [[Belair railway station|Belair]]
| stations      = {{Plainlist|
* [[Goodwood railway station|Goodwood]]
* [[Mitcham railway station, Adelaide|Mitcham]]
* [[Blackwood railway station|Blackwood]]
}}
| open          = 1883
| routes        = {{Plainlist|
* Every 20–30 mins (peak)
* Every 30–40 mins (weekday)
* Every 60 mins (weekend)
* Every 60 mins (night)
 }}
| event1label   = Re-sleepered ([[concrete sleeper|concrete]])
| event1        = April–August 2009
| event2label   = Electrified
| event2        = 
| close         = <!-- {{End date|YYYY|MM|DD|df=y}} -->
| owner         = 
| operator      = 
| depot         = 
| stock         = {{Plainlist|
* [[3000 class railcar|3000/3100 class]]
 }}
| linelength_km = 21.5
| tracks        = Quadruple track to Goodwood<br>Single track to Belair
| gauge         = {{Track gauge|1,600 mm|allk=on}}
| map           = 
| map_state     = collapsed
}}
The '''Belair railway line''' is a suburban rail commuter route in the city of [[Adelaide]], [[South Australia]], that runs from the [[Adelaide railway station|Adelaide station]] to [[Belair railway station|Belair]] in the [[Adelaide Hills]] via the [[Adelaide-Wolseley railway line|Adelaide-Wolseley line]].

==History==
The [[Adelaide-Wolseley railway line|Adelaide-Wolseley]] from Adelaide to [[Belair railway station|Belair]] and [[Bridgewater railway station|Bridgewater]] opened in 1883.

In 1919, a new alignment was built around [[Sleeps Hill railway station|Sleeps Hill]] as part of the duplication of the line. This involved a new double track tunnel being built to replace two tunnels and two viaducts.<ref>[http://www.mitchamcouncil.sa.gov.au/webdata/resources/files/Sleeps_Hill_Reserve.pdf Sleep's Hill Reserve] City of Mitcham</ref> The new alignment was also 400 metres shorter. On 18 June 1928, the line was duplicated from [[Eden Hills railway station|Eden Hills]] to [[Blackwood railway station|Blackwood]] and on to Belair on 24 June 1928.<ref name=WHCallaghan>{{cite book|last1=Callaghan|first1=WH|title=The Overland Railway|date=1992|publisher=Australian Railway Historical Society|location=Sydney|isbn=0 909650 29 2|pages=124, 217, 239}}</ref>

Passenger services were curtailed beyond Belair on 23 September 1987.<ref name=WHCallaghan/>

In 1995, the track used by [[Adelaide railway station|Adelaide]] bound services was converted to [[standard gauge]] as part of the [[Adelaide-Wolseley railway line|Adelaide to Melbourne standardisation project]]. The broad gauge passenger services were thus restricted to one track with crossing loops located at [[Mitcham railway station, Adelaide|Mitcham]], [[Blackwood railway station|Blackwood]], [[Eden Hills railway station|Eden Hills]] and [[Sleeps Hill railway station|Sleeps Hill]]. At the same time, the stations at [[Millswood railway station|Millswood]], [[Hawthorn railway station, Adelaide|Hawthorn]] and [[Clapham railway station, Adelaide|Clapham]] were closed to speed up services. Millswood reopened on 12 October 2014.<ref>[http://www.abc.net.au/news/2014-10-13/millswood-station-reopens-after-almost-20-years/5808618 Adelaide's Millswood Station reopens after almost 20 years] ''[[Australian Broadcasting Corporation|ABC News]]'' 13 October 2014</ref>

==Services==
All services are operated by [[Adelaide Metro]]'s [[3000 class railcar]]s. Until June 2007, some services on weekends were operated by a [[2000 class railcar]]  modified to incorporate increased bike capacity.  In 2005, trains ran the route every 30 minutes on weekdays (hourly after 7pm) and every 60 minutes on weekends and public holidays. From 2006, because of the single line, this was downgraded to every 36/24 minutes on weekdays.

The standard gauge track is used by [[Great Southern Rail (Australia)|Great Southern Rail's]] twice-weekly ''[[The Overland|Overland]]'' service to [[Southern Cross railway station|Melbourne]] and freight trains operated by [[Aurizon]], [[Genesee & Wyoming Australia]], [[Pacific National]] and [[SCT Logistics]].

==Resleepering==
In 2008, the [[Government of South Australia|State Government]] announced a plan to rebuild the Belair line.<ref>[http://www.treasury.sa.gov.au/budget/previous-budgets/2008-09 2008/09 State Budget] South Australian Department of Treasury & Finance June 2008</ref>

This work saw the track removed, with the track bed and track renewed. [[Dual gauge]] [[Railroad tie|sleepers]] were laid to allow for the line to be converted to [[standard gauge]] at a future date. Unlike other lines is not planned in the immediate future due to extra engineering work and complications with the standard gauge line. The line closed on 26 April 2009 and reopened on 23 August 2009 with buses replacing trains.<ref>[https://web.archive.org/web/20091226183859/http://www.adelaidemetro.com.au/better/belair_renewal.html Belair Line Renewal] Adelaide Metro</ref>

==2013 Closure==
The Belair line was closed from 1 January 2013 to 14 July 2013 to allow for electrification of the line from Adelaide to [[Goodwood railway station|Goodwood]] and construction of a [[grade separation]] at Goodwood Junction with relatively work performed on the Belair line itself.<ref>[http://www.infrastructure.sa.gov.au/RR/rail_revitalisation/goodwood_junction Goodwood Junction upgrade] Department of Planning, Transport & Infrastructure</ref><ref>[http://www.yorkcivil.com.au/project/goodwood-junction-rail-grade-separation-2/ Goodwood Junction Rail Grade Separation] York Civil</ref>

==Gallery==
<gallery>
File:Mitcham railway station, Adelaide.jpg|[[Mitcham railway station, Adelaide|Mitcham station]]
File:Redhen.jpg|Two [[Redhen railcar|Redhen]] railcars working a Belair to Adelaide local train during the late 1980s approaching [[Lynton railway station|Lynton]]
File:EdenHillsRailwayStationAdelaide.jpg|[[Eden Hills railway station|Eden Hills station]]
File:BlackwoodRailwayStationAdelaide.jpg|[[Blackwood railway station|Blackwood station]] in 2007; bus Interchange can be seen on the left
File:Belair station.jpg|[[Belair railway station|Belair station]], services to [[Bridgewater railway station|Bridgewater]] once departed from these platforms. (Now the [[Australian Rail Track Corporation]]'s [[Adelaide-Wolseley railway line|Melbourne]] line tracks)
File:AdelaideRail 6.jpg|[[Pacific National]] freight train passing through [[Belair railway station|Belair]]
</gallery>

{{Belair railway line}}
''<small>{{note|1|1}} West Terrace tram stop will become Royal Adelaide Hospital when Hospital is constructed''</small>.

Interchange Key:
{|class="wikitable" style="font-size: 90%" width= align=
|-
!Image
!Service
!Operator(s)
!Where
|-align="center"
|[[File:25 railtransportation.svg|10px|link=Adelaide Metro]]
|Suburban Trains Transfers
|[[Adelaide Metro]]
|[[Adelaide railway station|Adelaide]], [[Goodwood railway station|Goodwood]]
|- align="center"
|[[File:BSicon TRAM.svg|15px|link=Adelaide Metro]]
|Lightrail Transfers
|[[Adelaide Metro]]
|[[Adelaide railway station|Adelaide]]
|-align="center"
|[[File:BSicon BUS.svg|20px|link=List of bus routes in Adelaide]]
|Suburban Bus Transfers
|[[Torrens Transit]]<br>[[SouthLink]]
|''see [[List of bus routes in Adelaide|Bus Routes]]''
|-align="center"
|[[File:BSicon LDER.svg|20px|link=Great Southern Rail (Australia)|Great Southern Rail]]
|Interstate Trains Transfers
|[[Great Southern Railway (Australia)|Great Southern]]
|[[Adelaide Showground railway station|Adelaide Showground]]
|-align="center"
|[[File:BSicon INT.svg|20px|link=Interchange station]]
|Bus or Rail Interchange
|n/a
|[[Adelaide railway station|Adelaide]], [[Goodwood railway station|Goodwood]], [[Blackwood railway station|Blackwood]]
|}

==References==
{{Reflist}}

{{Railway lines of Adelaide}}

[[Category:Railway lines in South Australia]]
[[Category:Railway lines opened in 1883]]
[[Category:Transport in Adelaide]]
[[Category:5 ft 3 in gauge railways in Australia]]