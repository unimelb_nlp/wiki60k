{{Infobox journal
| title         = Black Denim Lit
| cover    = [[File:Black Denim Lit, February, 2014.jpg|150px]]
| caption = Cover for an issue of ''Black Denim Lit''
| editor        = Christopher T Garry
| discipline    = [[Literary journal]]
| peer-reviewed = 
| language      = [[English language|English]]
| former_names  = 
| abbreviation  = BDLit
| publisher     = Black Denim Lit
| country       = United States
| frequency     = Online Monthly; Print Twice-Annually
| history       = 2014-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.bdlit.com/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 2333-9977
| eISSN         = 
| boxwidth      = 
}}

'''''Black Denim Lit''''' is an American, web-based [[literary magazine]] and print anthology dedicated to compositions having unique and lasting artistic merit from new and established writers. Content selected for inclusion favor the General non-genre literary category as well as [[Science Fiction]] and [[Fantasy]], although any genre work may appear. It publishes [[flash fiction]], [[short stories]], [[novella|novelettes]], reviews, interviews, and items of interest to those interested in creative writing. There is a focus on the electronic publishing, making sure that all content is also available on almost all eBook storefronts worldwide, free where ever possible. Black Denim Lit was founded by Christopher T. Garry in [[Seattle, Washington]]. He is the managing editor.<ref>{{cite web|url=http://www.ctgarry.com |title=Welcome to |publisher=CTGarry.com |date=2014-02-04 |accessdate=2014-05-13}}</ref>

[[VIDA Count]] - The magazine is not part of the VIDA count, but they track and report metric themselves. Their proportion of female to male writers in the acceptances sent through April 9, 2014 is 16%.<ref>{{cite web|url=http://www.bdlit.com/odds--ends.html |title=Odds & Ends - This month, 70-pg issue, free for eReaders |publisher=Bdlit.com |date=2014-05-05 |accessdate=2014-05-13}}</ref>

== Issues ==

One key element to Black Denim Lit is that there are no prescribed themes during the submission phase. Instead, each issue develops its own theme through submissions. Recent issues and their resulting themes:

July, 2014: [[Sex]] <ref>{{cite web|url=http://www.bdlit.com/july-2014.html |title=July 2014 - This month, 134-pg issue, free for eReaders |publisher=Bdlit.com |date= |accessdate=2014-07-10}}</ref><br/>
June, 2014: [[Inner Demon]] <ref>{{cite web|url=http://www.bdlit.com/june-2014.html |title=June 2014 - This month, 100-pg issue, free for eReaders |publisher=Bdlit.com |date= |accessdate=2014-06-12}}</ref><br/>
May, 2014: [[Avatar]] as Self  <ref>{{cite web|url=http://www.bdlit.com/may-2014.html |title=May 2014 - This month, 68-pg issue, free for eReaders |publisher=Bdlit.com |date= |accessdate=2014-05-13}}</ref><br/>
April, 2014: [[Death|Mortality]] <ref>{{cite web|url=http://www.bdlit.com/april-2014.html |title=April 2014 - This month, 60-pg issue, free for eReaders |publisher=Bdlit.com |date= |accessdate=2014-05-13}}</ref><br />
March, 2014: [[Dark Fantasy]] <ref>{{cite web|url=http://www.bdlit.com/march-2014.html |title=March 2014 - This month, 50-pg issue, free for eReaders |publisher=Bdlit.com |date=2014-03-02 |accessdate=2014-05-13}}</ref><br />
February, 2014: [[Terraforming]] <ref>{{cite web|url=http://www.bdlit.com/february-2014.html |title=February 2014 - This month, 10-pg issue, free for eReaders |publisher=Bdlit.com |date= |accessdate=2014-05-13}}</ref>

== Notable contributors ==

Since the first issue went live, Black Denim Lit has featured the work of several authors and artists from different countries. Contributors include:
{{Col-start}}
{{col-break}}
* [[Phil Richardson]],<ref>{{cite web|url=http://philrichardsonstories.com |title=Home Page &#124; Writing and Living |publisher=Philrichardsonstories.com |date=2013-08-03 |accessdate=2014-05-13}}</ref> USA
* [[Oscar Windsor-Smith]],<ref>{{cite web|url=http://uk.linkedin.com/in/oscarwindsorsmith/ |title=Oscar Windsor-Smith - United Kingdom &#124; LinkedIn |publisher=Uk.linkedin.com |date= |accessdate=2014-05-13}}</ref> UK
* [[Steven Crandell]],<ref>{{cite web|url=http://www.huffingtonpost.com/steven-crandell/ |title=Steven Crandell |publisher=Huffingtonpost.com |date= |accessdate=2014-05-13}}</ref> USA
* [[Chad Greene]],<ref>{{cite web|url=https://twitter.com/TheShortCourse |title=Chad Greene (TheShortCourse) on Twitter |publisher=Twitter.com |date= |accessdate=2014-05-13}}</ref> USA
* [[Michael Fontana]],<ref>{{cite web|url=http://www.pw.org/content/michael_fontana |title=Michael Fontana &#124; Directory of Writers &#124; Poets & Writers |publisher=Pw.org |date=2014-03-29 |accessdate=2014-05-13}}</ref> USA
* [[Bob Carlton]],<ref>{{cite web|url=http://bobcarlton3.weebly.com |title=Bob Carlton - Bob Carlton |publisher=Bobcarlton3.weebly.com |date=2014-05-09 |accessdate=2014-05-13}}</ref> USA
{{col-break}}
* [[David W. Landrum]],<ref>http://LucidRhythms.com</ref> USA
* [[Ted Morrissey]],<ref>{{cite web|last=Morrissey |first=Ted |url=http://TedMorrissey.com |title='The world does not beckon, nor does it greatly reward.' — William H. Gass |publisher=Ted Morrissey |date=2010-04-28 |accessdate=2014-05-13}}</ref> USA
* [[Sean Monaghan]],<ref>{{cite web|url=http://SeanMonaghan.com |title=writer, and Venus Vulture - ambient music |publisher=Sean Monaghan |date= |accessdate=2014-05-13}}</ref> New Zealand
* [[Alan Bray]] USA
* [[Lela Marie De La Garza]] USA
* [[Dave Morehouse]] USA
{{col-break}}
* [[Ken Poyner]] USA
* [[Konstantine Paradias]] Greece
* [[Janet Slike]] USA
* [[Susan Sage]] USA
* Michael Reilly USA
{{col-end}}

== Masthead ==

* Christopher T. Garry - Founding and Managing Editor
* Hedwika Cox - Assistant Editor
* Heather Brown - Assistant Editor

== See also ==

*[[List of literary magazines]]
*[[List of art magazines]]

== References ==

{{Reflist}}

== External links ==
* [http://www.bdlit.com/ Black Denim Lit]
* [https://duotrope.com/market_14164.aspx Duotrope Digest]
* [http://thegrinder.diabolicalplots.com/Market.aspx?mid=3096 The Grinder]

{{DEFAULTSORT:Black Denim Lit}}
[[Category:2014 establishments in the United States]]
[[Category:American literary magazines]]
[[Category:American monthly magazines]]
[[Category:American online magazines]]
[[Category:American science fiction magazines]]
[[Category:Magazines established in 2014]]
[[Category:Media in Seattle]]
[[Category:Science fiction digests]]
[[Category:Biannual magazines]]
[[Category:Magazines published in Washington (state)]]


{{US-lit-mag-stub}}