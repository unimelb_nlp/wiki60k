{{Infobox Simpsons episode
|episode_name=Another Simpsons Clip Show
|image=
|image_caption=
|episode_no=106
|prod_code=2F33
|airdate=September 25, 1994
|show runner=[[David Mirkin]]
|writer=
[[Jon Vitti|Penny Wise]]{{sfn|Richmond & Coffman|1997 |p=151}}
|director=
[[David Silverman (animator)|David Silverman]]{{sfn|Richmond & Coffman|1997 |p=151}}
|blackboard=
"I will not use abbrev."{{sfn|Richmond & Coffman|1997 |p=151}}
|couch_gag=
The Simpsons sit on the couch and get crushed by the paper cut-out foot from ''[[Monty Python's Flying Circus]]''.<ref name="BBC"/>
|guest_star=
[[Albert Brooks]] as Jacques<br /> [[Sara Gilbert]] as Laura Powers<br /> [[Kelsey Grammer]] as Sideshow Bob<br /> [[Jon Lovitz]] as Artie Ziff<br /> [[Michelle Pfeiffer]] as Mindy Simmons<br /> [[Phil Hartman]] as Troy McClure<br />(all in clips only)
|commentary=[[Matt Groening]]<br />David Mirkin<br />David Silverman
|season=6
}}

"'''Another Simpsons Clip Show'''" is the third episode of ''[[The Simpsons]]''<nowiki>'</nowiki> [[The Simpsons (season 6)|sixth season]]. It originally aired on the [[Fox Broadcasting Company|Fox network]] in the United States on September 25, 1994. In the episode, Marge reads a romance novel in bed, and it prompts her to have a family meeting, where the [[Simpson family]] recall their past loves in form of clips from previous episodes.

The episode was written by [[Jon Vitti]] (credited as "Penny Wise") and directed by [[David Silverman (animator)|David Silverman]]. It is the second ''The Simpsons'' episode featuring a [[clip show]] format and uses clips from all the previous five seasons. The episode features cultural references to the 1992 book ''[[The Bridges of Madison County]]'' and the 1967 film ''[[The Graduate]]''. The episode has received rather negative reviews, since clip shows tend to be the least favorite episodes among fans.<ref name="Groening"/> It acquired a [[Nielsen rating]] of 8.7 and was the fourth highest rated show on the Fox network that week.

==Plot==
[[Marge Simpson|Marge]] is reading a book one night and wakes up [[Homer Simpson|Homer]] to ask if he thinks the [[Romantic love|romance]] has gone out of their [[marriage]]. Homer ignores her and tosses the book into the fireplace.

In the morning, Marge gets the family together to discuss romance, but they can only come up with vignettes from their failed relationships in the form of clips from previous episodes. Homer, however, saves the day when he brings up how he and Marge got together. Ultimately, the kids do not care for this one and wind up watching ''[[Itchy & Scratchy]]'' while Homer and Marge share a moment together.

==Production==
[[File:David Silverman in 2007-cropped.JPG|upright|thumb|left|[[David Silverman (animator)|David Silverman]] directed the episode. |alt=A man with a cowboy hat on his back.]]
As the title of the episode suggests, it is the second clip show episode of ''The Simpsons'' after "[[So It's Come to This: A Simpsons Clip Show]]", the 18th episode of the [[The Simpsons (season 4)|fourth season]]. It was written by [[Jon Vitti]], who used the [[pseudonym]] Penny Wise in the [[closing credits]] because he did not want to be credited for writing a clip show, and it was directed by [[David Silverman (animator)|David Silverman]].<ref name="Groening"/> The episode also includes contributions from [[John Swartzwelder]], [[Frank Mula]], [[David Richardson (writer)|David Richardson]], [[Jeff Martin (writer)|Jeff Martin]], [[Bill Oakley]], [[Josh Weinstein]], [[Matt Groening]], [[Sam Simon]], [[Al Jean]], [[Mike Reiss]], [[Jay Kogen]], [[Wallace Wolodarsky]], [[Nell Scovell]], [[David M. Stern]], [[George Meyer]], [[Conan O'Brien]], [[Robert Cohen (writer)|Robert Cohen]], [[Bill Canterbury]], and [[Dan McGrath]].<ref>Alberti (2004), pp 316.</ref>

During the early years of the show, the staff was forced by the Fox network into doing clip shows to save money.<ref name="Mirkin">{{cite video |people=Mirkin, David |date=2005 |title=The Simpsons The Complete Sixth Season DVD commentary for the episode "Another Simpsons Clip Show" |medium=DVD |publisher=20th Century Fox}}</ref> There was originally intense pressure on the producers of the show to create extra episodes in each season, and the plan was to make four clip shows per season to meet that limit. Writers and producers, however, felt that this many clip shows would alienate fans of the series.<ref name="Groening">{{cite video |people=Groening, Matt |date=2005 |title=The Simpsons The Complete Sixth Season DVD commentary for the episode "Another Simpsons Clip Show" |medium=DVD |publisher=20th Century Fox}}</ref> The Fox network's reasoning was that clip shows cost half of what a normal episode costs to produce, but they could sell [[broadcast syndication|syndication]] rights at full price.<ref name="Groening2">{{cite video |people=Groening, Matt |date=2004 |title=The Simpsons The Complete Fourth Season DVD commentary for the episode "So It's Come to This: A Simpsons Clip Show" |medium=DVD |publisher=20th Century Fox}}</ref>

===Referenced clips===
This flashback episode uses clips from episodes released during the [[List of The Simpsons episodes|first five seasons]]:{{sfn|Richmond & Coffman|1997 |p=151}}<ref name=bbc>Martyn (2000)</ref>
{|class="wikitable"
! Episode
! colspan="2"| Season
! Description
|-
| "[[New Kid on the Block]]"
|style="width: 5px; background-color:#0067AA; "|
|style="text-align: center; "| [[#Season 4 (1992–1993)|4]]
| Homer searches for his [[hot dog]] while lounging in a wading pool.
|-
| "[[Dog of Death]]"
|style="background-color: #D3A7CB; "|
|style="text-align: center; "| [[#Season 3 (1991-1992)|3]]
| Homer tosses Marge's book into the fireplace.
|-
| "[[Krusty Gets Busted]]"
|style="background-color: #DEDDE2; "|
|style="text-align: center; "| [[#Season 1 (1989-1990)|1]]
| The children watch an ''[[Itchy & Scratchy]]'' episode.
|-
| "[[Homer the Heretic]]"
|style="background-color: #0067AA; "|
|style="text-align: center; "| [[#Season 4 (1992-1993)|4]]
| The ''Itchy & Scratchy'' episode "Flay Me to the Moon".
|-
| "[[Bart's Friend Falls in Love]]"
|style="background-color: #D3A7CB; "|
|style="text-align: center; "| [[#Season 3 (1991-1992)|3]]
| The students in Bart's class watch ''Fuzzy Bunny's Guide to [[Human sexual behavior|You-Know-What]]''.
|-
| "[[I Love Lisa]]"
|style="background-color: #0067AA; "|
|style="text-align: center; "| [[#Season 4 (1992-1993)|4]]
| [[Ned Flanders|Ned]] serenading [[Maude Flanders|Maude]].
|-
| "[[Marge Gets a Job]]"
|style="background-color: #0067AA; "|
|style="text-align: center; "| [[#Season 4 (1992-1993)|4]]
| [[Waylon Smithers|Smithers]] dreams about [[Charles Montgomery Burns|Mr. Burns]] flying in through the window.
|-
| ''Montage sequence''
|style="background-color: #FADA00; "|
|style="text-align: center; "| 1 – 4
| [[Prank call]]s to [[Moe Szyslak|Moe]].
|-
| ''Montage sequence''
|style="background-color: #FADA00; "|
|style="text-align: center; "| 3 – 5
| Homer's "Mmm..." lines.
|-
| "[[Homer Loves Flanders]]"
|style="background-color: #D3212D; "|
|style="text-align: center; "| [[#Season 5 (1993-1994)|5]]
| Homer kissing Ned repeatedly at a local [[American football|football]] game.
|-
| "[[Life on the Fast Lane]]"
|style="background-color: #DEDDE2; "|
|style="text-align: center; "| [[#Season 1 (1989-1990)|1]]
| Marge tells her story of how she almost fell in love with a French bowler.
|-
| "[[The Last Temptation of Homer]]"
|style="background-color: #D3212D; "|
|style="text-align: center; "| [[#Season 5 (1993-1994)|5]]
| Homer tells the story of how he almost cheated on Marge with [[Mindy Simmons]].
|-
| "[[I Love Lisa]]"
|style="background-color: #0067AA; "|
|style="text-align: center; "| [[#Season 4 (1992-1993)|4]]
| Lisa tells the story of [[Ralph Wiggum]]'s crush on her which ended in him being heartbroken.
|-
| "[[New Kid on the Block]]"
|style="width: 5px; background-color:#0067AA; "|
|style="text-align: center; "| [[#Season 4 (1992-1993)|4]]
| Bart tells the story of how he fell for Laura Powers, the only girl he ever loved.
|-
| "[[Black Widower]]"
|style="background-color: #D3A7CB; "|
|style="text-align: center; "| [[#Season 3 (1991-1992)|3]]
| Marge recalls [[Patty and Selma|Selma]]'s marriage to [[Sideshow Bob]].
|-
| "[[Lady Bouvier's Lover]]"
|style="background-color: #D3212D; "|
|style="text-align: center; "| [[#Season 5 (1993-1994)|5]]
| Marge recalls the love triangle between [[Grampa Simpson]], Jacqueline Bouvier, and Mr. Burns.
|-
| "[[The Way We Was]]"
|style="background-color:#D1EDF1; "|
|style="text-align: center; "| [[#Season 2 (1990-1991)|2]]
| Homer finds a love story that does not end in heartbreak: his relationship with Marge.
|-
| ''Montage sequence''
|style="background-color: #FADA00; "|
|style="text-align: center; "| 1 – 5
| Homer and Marge kissing.
|}

===Cultural references===
When Bart, Lisa, and Maggie are watching ''Itchy & Scratchy'', Marge says they watch the same episodes all the time, while Lisa says that the ''Itchy & Scratchy'' cartoons are just pasted together from pieces of old episodes. This comment is a joke about the construction of this episode; the blackboard and couch gags are taken from other episodes, there are clips from past episodes, and the interstitials are actually clips from past episodes that feature the family members talking in the kitchen. These three aspects support the idea of this episode being a clip show to the extreme.{{sfn|Turner|2004|pp=69–70, 416}}

In the clip from "Lady Bouvier's Lover", Grampa tries to stop the wedding between Jacqueline Bouvier and Mr. Burns by banging on the window while shouting "Mrs. Bouvier!", which is a reference to the 1967 film ''[[The Graduate]]''.<ref name="calendar">{{cite book |last=Groening |first=Matt |title=The Trivial Simpsons 2008 366-Day Calendar |year=2007 |publisher=Harper Collins Publishers |isbn=0-06-123130-4 |pages=}}</ref><ref name=emol>{{cite web |url=http://emol.org/emclub/?q=season6 |title=A Special Simpsons Season Six Clip Show |publisher=Entertainment Magazine |date=2005-12-10 |author=J Floyd King |accessdate=2008-12-10}}</ref> Marge is seen reading the 1992 book ''The Bridges of Madison County'' by [[Robert James Waller]].<ref name=BBC>{{cite book |last=Martyn |first=Warren |author2=Adrian Wood |title=I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide |publisher=Virgin Books |year=2000 |url=http://www.bbc.co.uk/cult/simpsons/episodeguide/season6/page3.shtml}}</ref>

==Reception==
In its original broadcast, "Another Simpsons Clip Show" finished 68th in the ratings for the week of September 19 to September 25, 1994, with a [[Nielsen rating]] of 8.7.<ref name=ratings>{{cite news |title=How They Rate |date=September 30, 1994 |page=11 |publisher=''[[St. Petersburg Times]]'' |author=}} Retrieved on December 10, 2008.</ref> The episode was the fourth highest rated show on the Fox network that week, beaten only by ''[[Beverly Hills, 90210]]'', ''[[The X-Files]]'', and ''[[Married... with Children]]''.<ref name=ratings/>

The episode received rather negative reviews, since clip shows tend to be the least favorite episodes among fans.<ref name="Groening"/> The episode has been described as "framed in such a way as to still make [it] worth watching,[...] like a slideshow that's not quite so boring",<ref>{{cite web |last=Jun |first=Posted |url=http://www.tvsquad.com/2006/06/15/the-simpsons-another-simpsons-clip-show/ |title=The Simpsons: Another Simpsons Clip Show |publisher=Tvsquad.com |date=2006-06-15 |accessdate=2008-12-10}}</ref> "another clip show, although not the worst of them",<ref>{{cite web |url=http://www.currentfilm.com/dvdreviews7/simpsonss6dvd.html |title=DVD Review: Simpsons&nbsp;— Season 6 |publisher=Currentfilm.com |accessdate=2008-12-10 |archiveurl=https://web.archive.org/web/20081122001927/http://www.currentfilm.com/dvdreviews7/simpsonss6dvd.html |archivedate=22 November 2008 <!--DASHBot--> |deadurl=no}}</ref> and "the episode title pretty much says it all".<ref>{{cite web |url=http://www.dvdtalk.com/reviews/17122/simpsons-the-complete-sixth-season-the/ |title=The Simpsons&nbsp;— The Complete Sixth Season : DVD Talk Review of the DVD Video |publisher=Dvdtalk.com |date=2005-09-09 |author=Ian Jane |accessdate=2008-12-10}}</ref> Colin Jacobson of DVD Movie Guide said in a review: "The romance related storyline fizzles. That leaves us with a good collection of clips, but since we can already watch them in their original episodes, why bother with this cheap excuse for product?"<ref name=dvdmg>{{cite web |url=http://www.dvdmg.com/simpsonsseasonsix.shtml |title=The Simpsons: The Complete Sixth Season (1994) |accessdate=2008-10-08 |author=Jacobson, Colin |year=2003 |publisher=DVD Movie Guide |archiveurl=https://web.archive.org/web/20081012044003/http://www.dvdmg.com/simpsonsseasonsix.shtml |archivedate=12 October 2008 <!--DASHBot--> |deadurl=no}}</ref>

Lisa's comments&nbsp;— "romance is dead, it was acquired in a hostile takeover by Hallmark and Disney, homogenized, and sold off piece-by-piece"&nbsp;— have been used in case studies of the cultural representations of organizations.<ref>{{cite journal |first=H. M. |last=Benshoff |year=1992 |title=Heigh-ho, heigh=ho, is Disney high or low? From silly postmodern politics |url=http://www.animationjournal.com/abstracts/essays/Benshoff.html |journal=Animation Journal |issue=Fall |pages=62–85}}</ref><ref>{{cite book |title=Management and Organization Paradoxes |first=Stewart |last=Clegg |publisher=John Benjamins Publishing Company |year=2002 |isbn=90-272-3307-1 |chapter=6 |page=119}}</ref>

Scottish indie-rock band [[Arab Strap (band)|Arab Strap]] referred to this episode in the lyrics of their debut single "[[The First Big Weekend]]" ("Sunday afternoon we go up to John's with a lot of beer in time to watch ''The Simpsons'' - it was a really good episode about love always ending in tragedy except, of course, for Marge and Homer. It was quite moving at the end and to tell you the truth my eyes were a bit damp").<ref>{{cite web |url=http://www.musictimes.com/articles/9110/20140822/7-artists-influenced-by-the-simpsons-fall-out-boy-les-claypool-and-more.htm |title=7 Artists Influenced By The Simpsons: Fall Out Boy, Les Claypool, And More |last1=DeGroot |first1=Joey|date=22 August 2014 |website=Music Times |access-date=21 July 2015}}</ref>

==References==
{{reflist|30em}}

==Bibliography==
{{refbegin}}
*{{cite book |last=Alberti |first=John |title=[[Leaving Springfield|Leaving Springfield: The Simpsons and the Possibility of Oppositional Culture]] |publisher=Wayne State University Press |year=2004 |isbn=0-8143-2849-0 |page=316}}
*{{cite book |last=Gimple |first=Scott M. |title=[[The Simpsons Forever!: A Complete Guide to Our Favorite Family ...Continued]] |publisher=[[HarperCollins]] |date=December 1, 1999 |isbn=978-0-06-098763-3 |page=24 |oclc=42857430}}
*{{cite book |last=Groening |first=Matt |authorlink=Matt Groening |editor1-first=Ray |editor1-last=Richmond |editor1-link=Ray Richmond |editor2-first=Antonia |editor2-last=Coffman |title=[[The Simpsons episode guides#The Simpsons: A Complete Guide to Our Favorite Family|The Simpsons: A Complete Guide to Our Favorite Family]]  |edition=1st |year=1997 |location=New York |publisher=[[HarperPerennial]] |lccn=98141857 |ol=433519M |oclc=37796735 |isbn=978-0-06-095252-5 |ref={{harvid|Richmond & Coffman|1997}}}}
*{{cite book |last=Turner |first=Chris |authorlink=Chris Turner (author) |title=[[Planet Simpson|Planet Simpson: How a Cartoon Masterpiece Documented an Era and Defined a Generation]] |others=Foreword by [[Douglas Coupland]]. |edition=1st |year=2004 |location=Toronto |publisher=[[Random House Canada]] |oclc=55682258 |isbn=978-0-679-31318-2|ref=harv}}
{{refend}}

==External links==
{{Wikiquote|The_Simpsons/Season_6#Another_Simpsons_Clip_Show|Another Simpson Clip Show}}
{{Portal|The Simpsons}}
*[http://www.thesimpsons.com/#/recaps/season-6_episode-3 "Another Simpsons Clip Show"] at The Simpsons.com
* {{Snpp capsule|2F33}}
*{{tv.com episode|the-simpsons/another-simpsons-clip-show-1391}}
*{{imdb episode|0701053}}

{{The Simpsons episodes|6}}

{{good article}}

[[Category:The Simpsons (season 6) episodes]]
[[Category:1994 American television episodes]]
[[Category:Clip shows]]