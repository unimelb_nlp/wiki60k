{{ad|date=December 2016}}
{{Infobox company |
 name = BDO USA, LLP |
 logo = [[Image: Bdo logo wikipedia.jpg]] |
 type = Limited Liability Partnership (LLP) |
 foundation = 1910, as Seidman and Seidman |
 location = [[One Prudential Plaza]]<br>[[Chicago, Illinois]] |
 num_employees = 6,057 <ref>https://www.bdo.com/about/why-bdo/facts-figures</ref> |
 industry = [[Accounting]]<BR>[[professional services|Professional Services]]<BR>[[Tax]]<BR>[[Consultant|Consulting]] |
 products = Professional Services |
 revenue = $1.29 billion [[U.S. dollar|USD]] (FY 2016) |
|
 Chairman of the Board = Wayne Kolins|
 homepage = [http://www.bdo.com bdo.com]
}}

'''BDO USA, LLP''' is the United States Member Firm of [[BDO International]], a global [[accounting network]]. The company is headquartered in [[Chicago]].

==History==
BDO USA, LLP was founded as '''Seidman and Seidman''' in [[New York City]] in 1910 by three immigrant brothers:  Maximillian L. Seidman, Francis E. Seidman, and Jacob S. Seidman. At that time the accounting profession was in its infancy, with fewer than 2,200 practicing CPAs in the United States. Shortly thereafter in 1913, the [[16th Amendment to the United States Constitution]] was ratified, followed by the [[Revenue Act of 1913]] with new impositions of [[Income tax in the United States|U.S. Federal income tax]] enacted by Congress in that year. M. L. Seidman  saw the potential of the accountant's role to provide tax services to individuals. By 1917, Congress enacted the first revenue bills and the U.S. entry into [[World War I]] created the need for corporate income and excess profit taxes. At the same time, federal spending rose to $18.9 billion with 58 percent of the federal revenues provided from income taxes. M.L. Seidman and his siblings, who joined him in his new accounting firm, seized the opportunity to provide tax services to businesses in addition to individuals.

===Expansion===
An era of expansion began. Fostered by the federal government's conversion of furniture and woodworking companies to aircraft production for the war effort, the firm opened an office in Grand Rapids, Michigan in 1917. Seidman and Seidman quickly established itself as a leader in serving the furniture industry by developing the first effective furniture plant costing system. Today, BDO Seidman's Furniture Industry practice remains in the industry.

In 1925, the firm rapidly expanded, opening offices in [[Jamestown, Illinois]] and [[Rockford, Illinois]], followed by [[Chicago]] in 1921, and [[Gardner, Massachusetts]] in 1924. In 1922, J. S. Seidman joined the firm as a founding member.

The 1930s brought another new beginning for the accounting profession. In 1933, Congress passed the Securities Act, requiring public corporations to have financial statements included in registration statements and periodic reports reported on by independent CPAs. A year later, the Securities and Exchange Commission was created to administer the new legislation.

In 1950, [[L. William Seidman]] joined the firm and ultimately became its managing partner before leaving for government service, most notably as Chair of the FDIC.<ref name=bloom>[https://www.bloomberg.com/apps/news?pid=newsarchive&sid=aL39kRBP7IhU William Seidman, Who Led Cleanup of S&L Crisis, Dies], ''Bloomberg'', May 13, 2009</ref>

===Development of the national firm===
The firm continued to grow and by the 1960s, truly became a national firm. On April 1, 1968 the firm was converted into a national general partnership. This marked the beginning of a new era of expansion. Over the years to the present, the firm established many offices throughout the United States.  Founding Partner Keith Beresheim was instrumental in helping BDO grow its practice and become a national tier firm.

==Present==

Today, BDO USA, LLP has more than 60 offices and more than 400 independent Alliance firm locations nationwide.  During this timeframe, [[BDO International]] was created and has grown to become the fifth largest accounting and consulting network in the world with over 1,400 Member Firm offices in 135 countries.

In February 2009, BDO USA, LLP launched the firm’s first-ever national advertising campaign: "PEOPLE WHO KNOW, KNOW BDO”.

In June 2012, the Monitor (Joseph A. Smith, Jr.) of the National Mortgage Settlement announced he engaged BDO USA, LLP as his primary professional firm.  BDO will join the Monitor and his team for a period of three and a half years as they oversee implementation of the historic National Mortgage Settlement involving 49 states, the United States government and five of the nation’s largest banks.

==Controversies==
BDO has been involved in a number of engagements which resulted in major litigation and press attention.

===Washington, D.C. tax scandal (November 2007–10)===
A group of DC government employees had embezzled funds by fraudulently issuing real estate tax refund checks to their friends.  BDO Seidman was the independent auditor of the DC government's financial records. On November 21, 2007 Reuters wrote “Washington, D.C., officials should fix problems identified in an audit that found the city mishandled millions of dollars, according to Delegate Eleanor Holmes Norton, who represents the U.S. capital in Congress…. The audit, conducted by accounting firm BDO Seidman, LLP, found that financial reporting for the city's school system and Medicaid programs did not match their financial statements, making both areas vulnerable to fraud….Altogether, auditors found more than 160 instances where the city failed to comply with conditions for spending federal money, primarily by misreporting funds and mismanaging cash." As of November 2007, it was estimated that over $30 million were scammed by [[Washington, DC]] tax officials since 1999. At the height of the scam, half of the [[property tax]] refund checks were fraudulent. The average legitimate check was less than $10,000 while the average scam check was over $300,000. "The scandal may also singe BDO Seidman, a private audit firm that gave [Washington DC CFO Natwar] Gandhi its seal of approval earlier this year. Its auditors were paid millions to go over the city’s books and gave Gandhi’s office an unqualified opinion."{{citation needed|date=December 2015}} In 2010, a mentally ill woman, who was not a DC employee was able to exploit a security loophole in DC's financial systems to file tax returns claiming $19.1 million in refunds, but these were stopped.{{relevance-inline|date=December 2015}}<ref>{{cite news|url=http://www.washingtonexaminer.com/woman-worms-into-d.c.-taxpayer-accounts/article/17483|title=Woman worms into D.C. taxpayer accounts|first=Michael |last=Neibauer|date=February 5, 2010|accessdate=2010-12-30|work=Washington Examiner}}</ref><!-- this is not the blacklisted examiner.com -->

===New York Law School and Madoff scandal (December 2008)===
On December 16, 2008, in connection with the [[Bernard Madoff]] scandal, the [[New York Law School]] filed a lawsuit against [[J. Ezra Merkin]], [[Ascot Partners]], and its auditor BDO Seidman, after losing its $3 million investment in Ascot. The lawsuit charged Merkin with recklessness, gross negligence and breach of fiduciary duties.<ref>{{cite news|url=http://www.time.com/time/business/article/0,8599,1867092,00.html|title=The Madoff Fraud: How Culpable Were the Auditors?|first=Stephen|last=Gandel|date=December 17, 2008|work=TIME magazine|accessdate=2010-12-30}}</ref>

==References==
{{Reflist}}

==External links==
*[http://www.bdo.com/ BDO Seidman, LLP]
*[http://www.bdointernational.com/ BDO International]

[[Category:BDO]]
[[Category:Accounting firms of the United States]]
[[Category:Companies based in Chicago]]