{{Primary sources|date=July 2015}}
{{Infobox concert tour|
|concert_tour_name = Just Push Play Tour
|image = Aerosmith Just Push Play Tour Poster.jpg
|artist = [[Aerosmith]]
|album = ''[[Just Push Play]]''
|start_date = June 6, 2001
|end_date = February 3, 2002
|number_of_legs = 5
|number_of_shows = 92 (scheduled); 77 (played)
| last_tour             = [[Roar of the Dragon Tour]] <br />(1999–2000)
| this_tour             = Just Push Play Tour <br />(2001–2002)
| next_tour             = [[Girls of Summer Tour]] <br />(2002)
}}

The '''Just Push Play Tour''' was a concert tour headlined by [[Aerosmith]] that took the band to dozens of shows across North America and Japan.  The tour was put on in support of their 2001 release ''[[Just Push Play]]'' and ran from June 2001 to February 2002.  [[Alternative rock]]ers [[Fuel (band)|Fuel]] opened the show for much of the tour.  [[The Cult]] served as the opening act on later dates.

The tour received much success with the only major problems on the tour being cancellations. Three of the cancellations were due in part to the [[September 11 terrorist attacks]]; two of these dates were made up on the tour. An earlier show in Irvine was canceled due to a scheduling conflict with the recording of the music video for the single "[[Sunshine (Aerosmith song)|Sunshine]]". Eleven shows were canceled later on in the tour due to illness of one of the band members.
{{TOC left}}{{Clear}}

==Tour dates==
<ref>[http://www.aeroforceone.com/index.cfm?pid=804223 http://www.aeroforceone.com/index.cfm?pid=804223]</ref>
{| class="wikitable" style="text-align:center;"
! width="175"| Date
! width="150"| City
! width="150"| Country
! width="300"| Venue
|-
! colspan="4"|North America Leg I
|-
| June 6, 2001 || [[Hartford, Connecticut|Hartford]] || rowspan="12"|United States || [[Meadows Music Theater]]
|-
| June 8, 2001 || [[Saratoga Springs, New York|Saratoga Springs]] || [[Saratoga Performing Arts Center]]
|-
| June 10, 2001 || rowspan="2"|[[Holmdel, New Jersey|Holmdel]] || rowspan="2"|[[PNC Bank Arts Center]]
|-
| June 12, 2001
|-
| June 16, 2001 || rowspan="3"|[[Wantagh, New York|Wantagh]] || rowspan="3"|[[Jones Beach Amphitheater]]
|-
| June 18, 2001
|-
| June 20, 2001
|-
| June 22, 2001 || [[Hershey, Pennsylvania|Hershey]] || [[Hersheypark Stadium]]
|-
| June 24, 2001 || [[Bristow, Virginia|Bristow]] || [[Nissan Pavilion]]
|-
| June 26, 2001 || rowspan="2"|[[Mansfield, Massachusetts|Mansfield]] || rowspan="2"|[[Tweeter Center Boston|Tweeter Center]]
|-
| June 28, 2001
|-
| June 30, 2001 || [[Burgettstown, Pennsylvania|Burgettstown]] || [[Post-Gazette Pavilion]]
|-
| July 2, 2001 || [[Toronto]] || Canada || [[Molson Amphitheatre]]
|-
| July 5, 2001 || [[Tinley Park, Illinois|Tinley Park]] || rowspan="10"|United States || [[Tweeter Center Chicago|Tweeter Center]]
|-
| July 7, 2001 || [[East Troy, Wisconsin|East Troy]] || [[Alpine Valley Music Theatre]]
|-
| July 9, 2001 || [[Noblesville, Indiana|Noblesville]] || [[Verizon Wireless Music Center (Indiana)|Verizon Wireless Music Center]]
|-
| July 11, 2001 || [[Columbus, Ohio|Columbus]] || [[Polaris Amphitheater]]
|-
| July 13, 2001 || [[Clarkston, Michigan|Clarkston]] || [[DTE Energy Music Theatre]]
|-
| July 15, 2001 || [[Corfu, New York|Corfu]] || [[Darien Lake Performing Arts Center]]
|-
| July 17, 2001 || [[Cuyahoga Falls, Ohio|Cuyahoga Falls]] || [[Blossom Music Center]]
|-
| July 19. 2001 || [[Maryland Heights, Missouri|Maryland Heights]] || [[Riverport Amphitheater]]
|-
| July 21, 2001 || [[Bonner Springs, Kansas|Bonner Springs]] || [[Sandstone Amphitheater]]
|-
| July 23, 2001 || [[Greenwood Village, Colorado|Greenwood Village]] || [[Fiddler's Green Amphitheatre (Greenwood Village, Colorado)|Fiddler's Green Amphitheatre]]
|-
! colspan="4"|North America Leg II
|-
| August 8, 2001 || [[Mountain View, California|Mountain View]] || rowspan="26"|United States || [[Shoreline Amphitheatre]]
|-
| August 10, 2001 || [[George, Washington|George]] || [[Gorge Amphitheater]]
|-
| August 12, 2001 || [[Sacramento, California|Sacramento]] || Sacramento Valley Amphitheater
|-
| August 14. 2001 || [[Concord, California|Concord]] || [[Chronicle Pavilion]]
|-
| August 16, 2001 || [[Chula Vista, California|Chula Vista]] || [[Coors Amphitheatre (San Diego)|Coors Amphitheater]]
|-
| August 18, 2001 || [[Paradise, Nevada|Paradise]] || [[MGM Grand Garden Arena]]
|-
| August 20, 2001 || rowspan="2"|[[Irvine, California|Irvine]] || [[Verizon Wireless Amphitheatre (Irvine)|Verizon Wireless Amphitheater]]
|-
| August 22, 2001 || <s>Verizon Wireless Amphitheater</s> '''CANCELED'''
|-
| August 24, 2001 || [[San Bernardino, California|San Bernardino]] || [[Hyundai Pavilion]]
|-
| August 26, 2001 || [[Phoenix, Arizona|Phoenix]] || [[Desert Sky Pavilion]]
|-
| August 28, 2001 || [[Selma, Texas|Selma]] || [[Verizon Wireless Amphitheater Selma|Verizon Wireless Amphitheater]]
|-
| August 30, 2001 || [[The Woodlands, Texas|The Woodlands]] || [[Cynthia Woods Mitchell Pavilion]]
|-
| September 1, 2001 || [[Dallas]] || [[Smirnoff Music Centre]]
|-
| September 3, 2001 || [[New Orleans]] || [[New Orleans Arena]]
|-
| September 5, 2001 || [[Memphis, Tennessee|Memphis]] || [[Pyramid Arena]]
|-
| September 7, 2001 || [[Cincinnati]] || [[Riverbend Music Center]]
|-
| September 9, 2001 || [[Charlotte, North Carolina|Charlotte]] || [[Verizon Wireless Amphitheatre Charlotte|Verizon Wireless Amphitheater]]
|-
| September 11, 2001 || [[Virginia Beach, Virginia|Virginia Beach]] || <s>[[GTE Virginia Beach Amphitheater]]</s> '''CANCELED'''
|-
| September 13, 2001 || [[Camden, New Jersey|Camden]] || <s>[[Tweeter Center at the Waterfront|Tweeter Waterfront Center]]</s> '''CANCELED'''
|-
| September 15, 2001 || [[Columbia, Maryland|Columbia]] || <s>[[Merriweather Post Pavilion]]</s> '''CANCELED'''
|-
| September 17, 2001 || [[Atlanta]] || [[HiFi Buys Amphitheatre]]
|-
| September 19, 2001 || [[Nashville, Tennessee|Nashville]] || [[AmSouth Amphitheater]]
|-
| September 21, 2001 || [[Raleigh, North Carolina|Raleigh]] || [[Alltel Pavilion at Walnut Creek]]
|-
| September 23, 2001 || [[West Palm Beach, Florida|West Palm Beach]] ||[[Sound Advice Amphitheater]]
|-
| September 25, 2001 || [[Bristow, Virginia|Bristow]] ||[[Nissan Pavilion]] '''MAKE-UP FOR CANCELED COLUMBIA, Maryland SHOW'''
|-
| September 27, 2001 || [[Camden, New Jersey|Camden]] ||[[Tweeter Center at the Waterfront|Tweeter Waterfront Center]] '''RESCHEDULED'''
|-
! colspan="4"|North America Leg III
|-
| October 11, 2001 || [[Calgary]] || rowspan="2"|Canada || [[Pengrowth Saddledome]]
|-
| October 13, 2001 || [[Edmonton]] || [[Skyreach Centre]]
|-
| October 15, 2001 || [[Minneapolis]] || rowspan="7"|United States || [[Target Center]]
|-
| October 17, 2001 || [[Grand Forks, North Dakota|Grand Forks]] || [[Alerus Center]]
|-
| October 19, 2001 || [[Ames, Iowa|Ames]] || [[Hilton Coliseum]]
|-
| October 21, 2001 || [[Indianapolis]] || [[Conseco Fieldhouse]]
|-
| October 23, 2001 || [[Rosemont, Illinois|Rosemont]] || [[Allstate Arena]]
|-
| October 25, 2001 || [[Auburn Hills, Michigan|Auburn Hills]] || [[The Palace of Auburn Hills]]
|-
| October 27, 2001 || [[Pittsburgh]] || [[Mellon Arena]]</s> '''CANCELED'''
|-
| October 29, 2001 || Toronto || rowspan="2"|Canada || [[Air Canada Centre]]</s> '''CANCELED'''
|-
| October 31, 2001 || [[Montreal]] || [[Molson Centre]]
|-
| November 2, 2001 || [[Fairborn, Ohio|Fairborn]] || rowspan="21"|United States || [[Nutter Center]]</s> '''CANCELED'''
|-
| November 4, 2001 || [[Boston]] || [[FleetCenter (Boston)|FleetCenter]]</s> '''CANCELED'''
|-
| November 6, 2001 || [[Providence, Rhode Island|Providence]] || [[Dunkin' Donuts Center]]</s> '''CANCELED'''
|-
| November 8, 2001 || [[Philadelphia]] || [[First Union Center]]</s> '''CANCELED'''
|-
| November 10, 2001 || [[Lexington, Kentucky|Lexington]] || [[Rupp Arena]]</s> '''CANCELED'''
|-
| November 12, 2001 || New York City || [[Madison Square Garden]]
|-
| November 15, 2001 || [[East Rutherford, New Jersey|East Rutherford]] || [[Continental Airlines Arena]]
|-
| November 17, 2001 || [[Manchester, New Hampshire|Manchester]] || [[Verizon Wireless Arena]]
|-
| November 19, 2001 || [[Uncasville, Connecticut|Uncasville]] || [[Mohegan Sun Arena]]
|-
| November 25, 2001 || [[Greensboro, North Carolina|Greensboro]] || [[Greensboro Coliseum]]</s> '''CANCELED'''
|-
| November 27, 2001 || [[Tampa, Florida|Tampa]] || [[Ice Palace (Tampa arena)|Ice Palace]]
|-
| November 29, 2001 || [[Fort Lauderdale, Florida|Fort Lauderdale]] || [[National Car Rental Center]]
|-
| December 1, 2001 || [[Birmingham, Alabama|Birmingham]] || [[BJCC Arena]]</s> '''CANCELED'''
|-
| December 3, 2001 || [[Champaign, Illinois|Champaign]] || [[Assembly Hall (Champaign)|Assembly Hall]]
|-
| December 5, 2001 || Dallas || [[Reunion Arena]]
|-
| December 7, 2001 || [[North Little Rock, Arkansas|North Little Rock]] || [[Alltel Arena]]
|-
| December 9, 2001 || [[Oklahoma City]] || [[Myriad Convention Center]]
|-
| December 11, 2001 || [[St. Louis]] || [[Savvis Center]]</s> '''CANCELED'''
|-
| December 13, 2001 || [[Kansas City, Missouri|Kansas City]] || [[Kemper Arena]]</s> '''CANCELED'''
|-
| December 15, 2001 || [[Moline, Illinois|Moline]] || [[The Mark of the Quad Cities]]</s> '''CANCELED'''
|-
| December 17, 2001 || [[Cleveland, Ohio|Cleveland]] || [[Gund Arena]]</s>'''CANCELED'''
|-
! colspan="4"| North America Leg IV
|-
| January 5, 2002 || [[Denver]] || rowspan="7"|United States || [[Pepsi Center]]
|-
| January 7, 2002 || [[Salt Lake City]] || [[Delta Center]]
|-
| January 9, 2002 || [[San Jose, California|San Jose]] || [[San Jose Arena]]
|-
| January 11, 2002 || [[Las Vegas Valley|Las Vegas]] || [[Hard Rock Hotel and Casino (Las Vegas)|Hard Rock Hotel]]
|-
| January 13, 2002 || [[Inglewood, California|Inglewood]] || [[Great Western Forum]]
|-
| January 15, 2002 || [[Fresno, California|Fresno]] || [[Selland Arena]]
|-
| January 17, 2002 || [[San Diego]] || [[San Diego Sports Arena]]
|-
! colspan="4"| Asia
|-
| January 25, 2002 || rowspan="2"|[[Osaka]] || rowspan="6"|Japan || rowspan="2"|[[Osaka Dome]]
|-
| January 27, 2002
|-
| January 29, 2002 || [[Fukuoka]] || [[Fukuoka Dome]]
|-
| January 31, 2002 || [[Nagoya]] || [[Nagoya Dome]]
|-
| February 2, 2002 || rowspan="2"|Tokyo || rowspan="2"|[[Tokyo Dome]]
|-
| February 3, 2002
|}

==Stage setup==
The stage for the tour had a very modern look, resembling the moderness of the band's new album and its cover.  Most striking was the silver and white colors, as well as two curving staircases which met at a platform at the top, where some of the most exciting moments of each concert took place, including the entrance of [[Steven Tyler]] and [[Joe Perry (musician)|Joe Perry]] at the beginning of the show, as well as [[Steven Tyler]] singing the eerie lyrics to the beginning of "[[Seasons of Wither]]"

Additionally, the band set up a second smaller stage in the rear of the outdoor pavilions to play for those in the lawn section.  During the middle of the show, the band members would walk under very heavy security to this stage to do a three-song set from this stage.

Steven Tyler jokingly referred to this tour as the "Back on the Grass Tour" which was a reference to the auxiliary stage set up on the lawn at many outdoor venues, and at the same time a jab at those who had claimed Aerosmith was using drugs again. Tyler especially targeted former manager [[Tim Collins (manager)|Tim Collins]] with these jokes, who had accused Aerosmith of relapsing into drug use before the band fired him in 1996. "Back On The Grass" was never an official name for the tour, just a joke Tyler repeated in several interviews.

==Setlist==
The setlist was quite long, featuring as many as 25 songs at some shows.  It varied show to show, as most Aerosmith setlists do, but it usually included about half a dozen songs from ''Just Push Play'' as well a fair balance between their 70s rock classics and their 80s and 90s pop-rock hits.

==Success==
The tour came on the heels of the band's platinum album ''[[Just Push Play]]''.  Aerosmith was at their peak popularity at this time, having played the [[Super Bowl XXXV]] Halftime Show, been inducted into the Rock 'n' Roll Hall of Fame, and scored a Top 10 hit all within the first half of the year. Just prior to the start of the tour, Steven Tyler sang the [[The Star-Spangled Banner|National Anthem]] at the [[2001 Indianapolis 500|Indianapolis 500]], and the team sponsored a car in the race.

As a result, many shows sold out and the band seemed to endlessly add arena dates through the fall and winter, even after their highly successful summer amphitheater tour.

The tour was ranked as the 8th highest grossing of 2001.
*Total Gross: $43,578,874.
*Total Attendance: 937,609. 56 shows. 15 sellouts.

==Problems==
In the wake of the [[September 11 terrorist attacks]], the band canceled the three shows after that (Virginia Beach, Camden, New Jersey, and Columbia, Maryland), which all also happened to be on the [[Eastern Seaboard]], where the attacks had occurred. These shows were later rescheduled.

Additionally, the band decided to cancel a 2nd show at Irvine, California earlier in the tour, due to a scheduling conflict with the filming of the video for the single "Sunshine."

==''United We Stand''==
The band decided to play the [[United We Stand: What More Can I Give]] benefit concert (for September 11 victims) at [[RFK Stadium]] in Washington, D.C. on October 21, 2001, alongside [[Michael Jackson]], [[Mariah Carey]], and several other pop stars.  The band had been uncertain about whether to play the show due to scheduling conflicts, and made the decision almost at the last minute.  The band took the stage in the afternoon, playing about a 5-song set and then flew back to [[Indianapolis]] for a concert that same night.

==''Rockin' the Joint''==
In January 2002, the band played ''[[The Joint (music venue)|The Joint]]'', a 2,000 seat venue within the [[Hard Rock Hotel and Casino (Las Vegas)|Hard Rock Hotel and Casino]].  This show was recorded and parts of it released as the band's fifth live album, a Dual Disc CD/DVD entitled [[Rockin' the Joint]] which was released in 2005.

==References==
{{reflist}}

==External links==
*[http://www.aerosmith.com Aerosmith]
*[http://www.aeroforceone.com Aero Force One]

{{Aerosmith}}

[[Category:Aerosmith concert tours]]
[[Category:2001 concert tours]]
[[Category:2002 concert tours]]