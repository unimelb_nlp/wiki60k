{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Kenilworth
| image        = Kenilworth 1st ed.jpg
| caption      = First edition title page.
| author       = Sir [[Walter Scott]]
| cover_artist = 
| country      = United Kingdom
| language     = English
| series       = [[Waverley Novels]]
| genre        = [[Historical novel]]
| publisher    = Constable and Co.
| release_date = 8 January 1821
| media_type   = Print ([[Hardcover|Hardback]])
| pages        = 
| isbn         = 
| preceded_by  = 
| followed_by  = 
}}
'''''Kenilworth. A Romance''''' is a [[historical novel]] by  Sir [[Walter Scott]], first published on 8 January 1821.

==Plot introduction==
''Kenilworth'' is apparently set in 1575, and centers on the secret marriage of [[Robert Dudley, 1st Earl of Leicester]], and [[Amy Robsart]], daughter of Sir Hugh Robsart. The tragic series of events begins when Amy flees her father and her betrothed, Tressilian, to marry the Earl. Amy passionately loves her husband, and the Earl loves her in return, but he is driven by ambition. He is courting the favour of [[Queen Elizabeth I]], and only by keeping his marriage to Amy secret can he hope to rise to the height of power that he desires. At the end of the book, the queen finally discovers the truth, to the shame of the Earl. But the disclosure has come too late, for Amy has been murdered by the Earl's even more ambitious steward, Varney.

===Explanation of the novel's title===
The title refers to Dudley's [[Kenilworth Castle]] in [[Kenilworth, Warwickshire]]. The novel opens, however, at [[Cumnor Place]], near [[Abingdon, Oxfordshire|Abingdon]] in [[Berkshire]] (now [[Oxfordshire]]).

==Plot summary==
Giles Gosling, the innkeeper, had just welcomed his scape-grace nephew Michael Lambourne on his return from [[Flanders]]. He invited the Cornishman, Tressilian, and other guests to drink with them. Lambourne made a wager he would obtain an introduction to a certain young lady under the steward Foster's charge at [[Cumnor Place]], seat of the Earl of Leicester, and the Cornish stranger begged permission to accompany him. On arriving there Tressilian found that this lady was his former lady-love, Amy. He would have carried back to her home, but she refused; and as he was leaving he quarrelled with Richard Varney, the earl's squire, and might have taken his life had not Lambourne intervened. Amy was soothed in her seclusion by costly presents from the earl, and during his next visit she pleaded that she might inform her father of their marriage, but he was afraid of Elizabeth's resentment.

[[Image:Kenilworth Castle 02.jpg|thumb|right|[[Kenilworth Castle]]'s 16th-century gatehouse, built by [[Robert Dudley, 1st Earl of Leicester|Robert Dudley]]]]
Warned by his host against the squire, and having confided to him how Amy had been entrapped, Tressilian left Cumnor by night, and, after several adventures by the way, reached the residence of Sir Hugh Robsart, Amy's father, to assist him in laying his daughter's case before the queen. Returning to London, Tressilian's servant, Wayland Smith, cured the Earl of Sussex of a dangerous illness. On hearing about this from [[Walter Raleigh]], Elizabeth at once set out to visit Leicester's rival, and it was in this way that Tressilian's petition, in Amy's behalf, was handed to her. The queen was agitated to learn of this secret marriage. Varney was accordingly summoned to the royal presence, but he boldly declared that Amy was his wife, and Leicester was restored to the queen's favour.

Tressilian's servant then gained access to the secret countess Amy as a pedlar, and, having hinted that Elizabeth would shortly marry the earl, sold her a cure for the heartache, warning her attendant Janet at the same time that there might be an attempt to poison her mistress. Meanwhile, Leicester was preparing to entertain the queen at Kenilworth, where she had commanded that Amy should be introduced to her, and Varney was, accordingly, despatched with a letter begging the countess to appear at the revels pretending to be Varney's bride. Having indignantly refused to do so, and having recovered from the effects of a cordial which had been prepared for her by the astrologer Alasco, she escaped, with the help of her maid, from Cumnor, and started for Kenilworth, escorted by Wayland Smith.

Travelling thither as brother and sister, they joined a party of [[mummer]]s, and then, to avoid the crowd of people thronging the principal approaches, proceeded by circuitous by-paths to the castle. Having, with Dickie Sludge's help, passed into the courtyard, they were shown into a room, where Amy was waiting while her attendant carried a note to the earl, when she was startled by the entrance of Tressilian, whom she entreated not to interfere until after the expiration of twenty-four hours. On entering the park, Elizabeth was received by her favourite attended by a numerous cavalcade bearing waxen torches, and a variety of entertainments followed. During the evening she enquired for Varney's wife, and was told she was too ill to be present. Tressilian offered to lose his head if within twenty-four hours he did not prove the statement to be false. Nevertheless, the ostensible bridegroom was knighted by the queen.

[[Image:Kenilworth Castle, Elizabethan Gardens.jpg|thumb|right|The Elizabethan Gardens at Kenilworth]]
Receiving no reply to her note, which Wayland had lost, Amy found her way the next morning to a grotto in the gardens, where she was discovered by Elizabeth, who had just told her host that "she must be the wife and mother of England alone." Falling on her knees the countess besought protection against Varney, who she declared was not her husband, and added that the Earl of Leicester knew all. The earl was instantly summoned to the royal presence, and would have been committed to the Tower, had not Amy recalled her words, when she was consigned to Lord Hunsdon's care as bereft of her reason, Varney coming forward and pretending that she had just escaped from a special treatment for her madness. Leicester insisted on an interview with her, when she implored him to confess their marriage to Elizabeth, and then, with a broken heart, told him that she would not long darken his brighter prospects. Varney, however, succeeded in persuading him that Amy had acted in connivance with Tressilian, and in obtaining medical sanction for her custody as mentally disordered, asking only for the earl's signet-ring as his authority. The next day a duel between Tressilian and the earl was interrupted by Dickie, who produced the countess's note, and, convinced of her innocence, Leicester confessed that she was his wife. With the queen's permission he at once deputed his rival and Sir Walter Raleigh to proceed to Cumnor, whither he had already despatched Lambourne, to stay his squire's further proceedings.

Varney, however, had shot the messenger on receiving his instructions, and had caused Amy to be conducted by Foster to an apartment reached by a long flight of stairs and a narrow wooden bridge. The following evening the tread of a horse was heard in the courtyard, and a whistle like the earl's signal, upon which she rushed from the room, and the instant she stepped on the bridge, it parted in the middle, and she fell to her death. Her murderer poisoned himself, and the skeleton of his accomplice was found, many years afterwards, in a cell where he secreted his money. The news of the countess's fate put an end to the revels at Kenilworth: Leicester retired for a time from Court, and Sir Hugh Robsart, who died very soon after his daughter, settled his estate on Tressilian. Leicester pressed for an impartial inquiry. Though the jury found that Amy's death was an accident (concluding that Lady Dudley, staying alone "in a certain chamber", had fallen down the adjoining stairs, sustaining two head injuries and breaking her neck), it was widely suspected that Leicester had arranged his wife's death to be able to marry the Queen..

==Characters==
[[File:Robert Dudley Earl of Leicester drawing by Zuccaro 1575.jpg|thumb|Robert Dudley, Earl of Leicester, in 1575]]
* Giles Gosling, host of the "Black Bear" at Cumnor
* Michael Lambourne, his nephew
* '''Edmund Tressilian''', a Cornish gentleman, Amy's former lover
* '''Wayland Smith''', his servant
* '''[[Thomas Radclyffe, 3rd Earl of Sussex]]'''
* '''Sir Nicholas Blount''', master of house to the Earl of Sussex
* '''[[Walter Raleigh|Sir Walter Raleigh]]''', a gentleman in the household of the Earl of Sussex
* '''[[Robert Dudley, 1st Earl of Leicester]]'''
* '''Richard Varney''', his squire
* Anthony Foster, steward of Cumnor Place
* Master Erasmus Holiday, a village pedagogue
* '''Dickie Sludge''', alias Flibbertigibbet, one of his pupils
* Doctor Doboobie, alias Alasco, an astrologer
* Sir Hugh Robsart, of Lidcote Hall, Devonshire
* '''[[Amy Robsart]]''', his daughter
* Janet Foster, her attendant at Cumnor
* '''[[Elizabeth I of England|Queen Elizabeth]]''', at Kenilworth
* ''In attendance on the Queen''
** [[Henry Carey, 1st Baron Hunsdon|Lord Hunsdon]]
** [[William Cecil, 1st Baron Burghley|Lord Burleigh]]
**

==Themes==
''Kenilworth'' is a novel of selfishness versus selflessness and ambition versus love. Amy and the Earl both struggle internally with selfishness and love, while Varney and Tressilian each typify the extremes of the two qualities. Perhaps the finest point of this work is its [[characterization]]. The Earl is shown as an ambition-driven man who will stoop to deceit and almost anything else in order to attain his goals, but with one saving grace—he loves Amy, and in the end gives up his pride and ambition to confess their marriage. Amy Robsart is a pretty, spoiled child whose tragic circumstances teach her maturity and determination, although such lessons come too late to save her. Tressilian is the serious, steadfast lover of Amy, and continues to try to save her from herself throughout the book and finally dies of a broken heart. Varney is the chief villain of the work. His greed and ambition know no bounds. It is he that pushes the Earl beyond what he would normally do to secure power, and it is he that finally murders Amy Robsart.

==Historical inaccuracies==
Much of the novel gives a fair depiction of the Elizabethan court, although the circumstances of Amy Robsart's death from a fall are greatly altered, and also many other events are a product of Scott's imagination. The death of Amy Robsart had been the subject of speculation for more than 200 years, and in 1810 [[Cumnor Place]] was pulled down, it was said, solely in order to lay her ghost to rest.

The reception at Kenilworth which provides the backdrop to the novel took place in 1575, and frequent references to how many years have passed since other events such as the Queen's accession, the deposition of [[Mary, Queen of Scots]], and so forth, indicate that the novel is set in that year—but Amy Robsart died on 8 September 1560. Leicester's first marriage was not, in fact, a closely guarded secret; it was his secret marriage in September 1578 to [[Lettice Knollys]] (with whom he had flirted in 1565) that caused the Queen's anger in 1579.

[[William Shakespeare]], who was not even born until 1564, is mentioned in Chapter 17 as an adult and as being known at court, rubbing shoulders with [[Edmund Spenser]], whose first major work ''[[The Shepheardes Calender]]'' was not published until 1579; and in Chapter 16, Queen Elizabeth even quotes from ''[[Troilus and Cressida]]'', which was written around 1602.

The character of Sir Nicolas Blount seems to be broadly based on [[Christopher Blount|Sir Christopher Blount]], who was in fact an official in the household of the Earl of Essex.

==References in other works, etc==
In 1822, the young [[Victor Hugo]] agreed with the elder [[Alexandre Soumet]] to write a five-act drama based on Scott's novel. Soumet had outlined the plot and was to write the last two acts, while Hugo was to write the first three. When they confronted the written materials, they agreed that their principles were incompatible. Each completed his own drama. Soumet's earnest ''Emilia'' was produced at the [[Odéon]] in 1823 with [[Mademoiselle Mars]], while Hugo's ''Amy Robsart'', freely mingling tragedy with comedy, didn't see the stage until 1828.

[[Gaetano Donizetti]]'s 1829 opera ''[[Elisabetta al castello di Kenilworth]]'' is loosely based on Scott's novel.

In 1957 the novel was adapted into a six-part [[BBC]] television series ''[[Kenilworth (TV series)|Kenilworth]]'', now believed to be [[lost film|lost]].

In keeping with the city's many Walter Scott references, [[Rose Street, Edinburgh|Rose Street]] in [[Edinburgh]] has a pub called The Kenilworth, which was established in 1904.

[[Portland, Oregon]], has a neighborhood called [[Creston-Kenilworth, Portland, Oregon|Creston-Kenilworth]], the latter part of which is named for the novel.
 
The novel was translated into French by [[Jacques-Théodore Parisot]] (1783-1840) under the title [http://www.livre-rare-book.com/search/current.seam?maximumPrice=0.0&keywords=&firstResult=0&faceted=true&ISBN=&century=ALL&quicksearch=&l=fr&bookType=ALL&reference=&matchTypeList=ALL&author=&title=le+chateau+de+kenilworth+++tome+troisieme&description=&minimumPrice=0.0&sorting=RELEVANCE&minimumYear=0&ageFilter=ALL&keycodes=&maximumYear=0&cid=10935197 ''Le château de Kenilworth''].

==External links==
{{Gutenberg|no=1606|name=Kenilworth}}
* {{librivox book | title=Kenilworth | author=Sir Walter SCOTT}}
*[http://www.walterscott.lib.ed.ac.uk/works/novels/kenilw.html ''Kenilworth''] at [http://www.walterscott.lib.ed.ac.uk/ Walter Scott Digital Archive], the [[University of Edinburgh]] library
{{Waverley Grey}}

{{Walter Scott}}

[[Category:1821 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by Walter Scott]]
[[Category:Historical novels]]
[[Category:Novels set in Tudor England]]
[[Category:Novels set in Warwickshire]]
[[Category:Three-volume novels]]
[[Category:Novels adapted into television programs]]