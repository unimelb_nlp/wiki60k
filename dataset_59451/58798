{{italictitle}}
{{Infobox journal
| title         =  Luso-Brazilian Review
| cover         = 
| editor        = Severino J. Albuquerque, Peter M. Beattie, Luís Madureira, Kathryn Bishop-Sánchez
| discipline    = [[literature]], [[Latin American studies]], [[history]], [[social sciences]]
| language      = [[English language|English]], [[Portuguese language|Portuguese]]
| formernames   = 
| abbreviation  = 
| publisher     = [[University of Wisconsin Press]]
| country       = [[United States]]
| frequency     = Biannually
| history       = 1964-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://lbr.uwpress.org
| link1         = 
| link1-name    = 
| link2         = http://muse.jhu.edu/journals/luso-brazilian_review/
| link2-name    = Content at [[Project MUSE]]
| RSS           = 
| atom          = 
| JSTOR         = 00247413
| OCLC          = 51321212
| LCCN          = 2004-212182
| CODEN         = 
| ISSN          = 0024-7413
| eISSN         = 1548-9957
| boxwidth      = 
}}
The '''''Luso-Brazilian Review''''' is a [[Peer review|peer-reviewed]] [[academic journal]] which publishes [[interdisciplinarity|interdisciplinary]] scholarship on the [[Portugal|Portuguese]], [[Brazil|Brazilian]], and [[Lusophone]] [[Africa]]n cultures, with an emphasis on [[literature]], [[history]], and the [[social sciences]]. Each issue of the ''Luso-Brazilian Review'' contains articles and book reviews, written in either [[English language|English]] or [[Portuguese language|Portuguese]].<ref>{{Cite web | title = JSTOR | url=http://www.jstor.org/journal/lusobrazrevi | accessdate = 23 February 2010 }} </ref><ref> {{Cite web | title = Project MUSE | url=http://muse.jhu.edu/journals/luso-brazilian_review/ | accessdate = 23 February 2010 }}</ref>

The ''Luso-Brazilian Review'' was founded in 1964 at the [[University of Wisconsin-Madison]]. The founding editor was Alberto Machado da Rosa.<ref>{{Cite journal
  | authorlink = Alberto Machado da Rosa
  | title = A Statement From the Editor
  | journal = Luso-Brazilian Review
  | volume = 1
  | pages = 3–4
  | date = June 1946}}
</ref>

== Indexing ==
The journal is indexed and abstracted in America: History and Life, Behavioural Abstracts, Hispanic American Periodicals Index, MLA International Bibliography of Books and Articles on the Modern Languages and Literatures, Multicultural Education Abstracts, Periodicals Index Online, [[Scopus]], Social Planning, Policy and Development Abstracts, and Sociology of Education Abstracts.<ref>
{{Cite web 
| title = University of Wisconsin Press, Indices and Abstracts 
| url=http://uwpress.wisc.edu/journals/indandab.html
| accessdate = 23 February 2010 }}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://uwpress.wisc.edu/journals/journals/lbr.html}}
* [http://lbr.uwpress.org/ Luso-Brazilian Review, online edition]
* [http://uwpress.wisc.edu/journals University of Wisconsin Press, Journals Division]

[[Category:Lusophone culture]]
[[Category:University of Wisconsin–Madison academic journals]]
[[Category:Cultural journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 1964]]
[[Category:Multilingual journals]]