{{no footnotes|date=July 2013}}
[[File:Lutizenbund.PNG|thumb|320px|Territory of Lutici federation after 983, beyond the eastern border of the German kingdom (outlined in yellow)]]
The '''Great Slav Rising''' in 983 was an uprising of the [[Polabian Slavs]] ([[Wends]]), mainly [[Lutici]] and [[Obotrites|Obotrite]] tribes living east of the [[Elbe]] River in modern north-east [[Germany]]. They were revolting against [[Christianization]] and their subjugation to the [[Kingdom of Germany|German]] (former [[East Francia|East Frankish]]) realm of the [[Holy Roman Emperor]].

== Background ==
The Slavic peoples between the Elbe and the [[Baltic Sea|Baltic]] coast had been conquered and nominally converted to Christianity by the campaigns of the German king [[Henry the Fowler]] and his son [[Otto I, Holy Roman Emperor|Otto I]], who in 962 was crowned Holy Roman Emperor. Otto had most recently defeated an alliance of Obotrite and [[Circipania|Circipani]] tribes at the 955 [[Battle on the Raxa]]. The conquered area east of the German [[Duchy of Saxony]] was initially organized within the vast [[Marca Geronis|Saxon Eastern March]] under Margrave [[Gero]], but divided into smaller marches upon his death in 965.

In order to stabilize his rule, Otto promoted the conversion of the Slavic population, establishing the bishoprics of [[Bishopric of Havelberg|Havelberg]] and [[Bishopric of Brandenburg|Brandenburg]] in 948, followed by the [[Archbishopric of Magdeburg]] in 968, which in particular carried out active missionary work.

== Uprising ==
In 981 Archbishop [[Adalbert (archbishop of Magdeburg)|Adalbert of Magdeburg]], the "Apostle of the Slavs", died and his successor [[Gisilher (archbishop of Magdeburg)|Gisilher]] had to struggle with the resistance by the Magdeburg chapter. He was backed by Emperor [[Otto II, Holy Roman Emperor|Otto II]], who, however, was on campaign in [[Kingdom of Italy (Holy Roman Empire)|Italy]], where he suffered a disastrous defeat against the Sicilian [[Kalbids]] in the 982 [[Battle of Stilo]] and died the next year without having returned to Germany, leaving his minor son [[Otto III, Holy Roman Emperor|Otto III]] under the tutelage of the Empress consorts [[Theophanu]] and [[Adelaide of Italy|Adelaide of Burgundy]].

While there was internal dissention in the Holy Roman Empire, Slavic forces led by the Lutici revolted and drove out the political and religious representatives of the Empire. Starting from the Slavic sanctuary at [[Rethra]], the bishops' seat of Havelberg on 29 June 983 was occupied and plundered, followed by Brandenburg three days later and numerous settlements up to the [[Tanger (river)|Tanger]] River in the west. According to the contemporary chronicler [[Thietmar of Merseburg]], the Obotrites joined the Lutici, devastated a St Lawrence monastery in [[Kalbe]], the bishopric of [[Oldenburg in Holstein|Oldenburg]] and even assaulted [[Hamburg]].

A hastily assembled Saxon army was only able to retain the Slavs behind the Elbe. The [[Northern March]] and the [[Billung March|March of the Billungs]] were lost. The [[March of Lusatia]] as well as the adjacent marches of [[March of Zeitz|Zeitz]], [[Merseburg]] and the [[Margraviate of Meissen|Meissen]] in the south did not take part in the uprising.

== Aftermath ==
From 985, several [[Princes of the Holy Roman Empire|Princes of the Empire]] carried out annual campaigns together with the Christian [[History of Poland during the Piast dynasty|Polish]] princes [[Mieszko I of Poland|Mieszko I]] and [[Bolesław I Chrobry]] to subjugate the area, however these campaigns were unsuccessful. In 1003 King [[Henry II, Holy Roman Emperor|Henry II of Germany]] tried a different approach: he allied himself with the Lutici and waged war against his previous ally Prince Bolesław of Poland. This stabilized the independence of the Lutici and ensured that the area remained ruled by Polabian Slavs and unchristianized into the 12th century.

The immediate consequences of the uprising were an almost complete stop on further [[Ostsiedlung|German eastward expansion]] (''Ostsiedlung'') for the next 200 years. For most of the time, the dioceses of Brandenburg and Havelberg existed in [[Titular see|titular]] form only, with the bishops residing at the royal court. Only in the 12th century after the [[Wendish Crusade]] of 1147 and the establishment of the [[Margraviate of Brandenburg]] under the [[House of Ascania|Ascanian]] prince [[Albert the Bear]] in 1157, the settlements east of the Elbe were resumed; followed by the northern lands of [[Mecklenburg]], where after several years of fighting against the Obotrite prince [[Niklot]], his son [[Pribislav of Mecklenburg|Pribislav]] in 1167 declared himself a vassal of the Saxon Duke [[Henry the Lion]].

== Sources ==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->
* Wolfgang Fritze: ''Der slawische Aufstand von 983 - eine Schicksalswende in der Geschichte Mitteleuropas''. In: Eckart Henning, Werner Vogel (ed.): ''Festschrift der landesgeschichtlichen Vereinigung für die Mark Brandenburg zu ihrem hundertjährigen Bestehen 1884–1984''. Berlin 1984, pp.&nbsp;9–55.
* [[Herbert Ludat]]: ''An Elbe und Oder um das Jahr 1000. Skizzen zur Politik des Ottonenreiches und der slawischen Mächte in Mitteleuropa''. Cologne 1971, ISBN 3-412-07271-0.
* [[Christian Lübke]]: ''Slavenaufstand''. In: ''[[Lexikon des Mittelalters]]''. vol. 7, col. 2003f.
* [[Lutz Partenheimer]]: ''Die Entstehung der Mark Brandenburg. Mit einem lateinisch-deutschen Quellenanhang''. Cologne/Weimar/Vienna 2007 (with sources material on the Slav Rising pp.&nbsp;98–103), ISBN 3-412-17106-9.

[[Category:West Slavic history]]
[[Category:Medieval rebellions in Europe]]
[[Category:Polabian Slavs]]
[[Category:980s conflicts]]
[[Category:983 in Germany]]
[[Category:Conflicts in 983]]