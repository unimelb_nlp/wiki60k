{{Infobox journal
| title = Australian Economic History Review
| cover = [[File:Australian Economic History Review cover image.png|200px]]
| editor = Stephen Morgan, John Singleton, Martin Shanahan, Lionel Frost
| discipline = [[History]], [[economics]]
| frequency = Triannually
| abbreviation = Aust. Econ. Hist. Rev.
| publisher = [[Wiley-Blackwell]] on behalf of the [[Economic History Society of Australia and New Zealand]]
| country =
| impact = 0.355
| impact-year = 2012
| history = 1961–present
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8446
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8446/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8446/issues
| link2-name = Online archive
| JSTOR =
| ISSN = 0004-8992
| eISSN = 1467-8446
| OCLC = 02257994
| LCCN =
}}
The '''''Australian Economic History Review: An Asia-Pacific Journal of Economic, Business, & Social History''''' is a [[Peer review|peer-reviewed]] [[academic journal]] with social-scientific analyses, principally of Pacific-Asian [[economic history]]. It is published three times a year by [[Wiley-Blackwell]] on behalf of the [[Economic History Society of Australia and New Zealand]].<ref>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8446 |title=Australian Economic History Review: An Asia-Pacific Journal of Economic, Business & Social History |publisher=Wiley-Blackwell |work= |accessdate=2012-11-20}}</ref> It was established in 1961 and is [[Editor-in-chief|edited]] by Stephen Morgan, John Singleton, Martin Shanahan, and Lionel Frost.

== Indexing and abstracting ==
The journal is indexed and abstracted in [[ProQuest]], [[CSA (database company)|CSA Environmental Sciences & Pollution Management Database]], [[Historical Abstracts]], [[International Bibliography of the Social Sciences]], ''[[Journal of Economic Literature]]''/[[EconLit]], [[Public Affairs Information Service]], [[RePEc]], [[Scopus]], [[Social Sciences Citation Index]], and [[Worldwide Political Sciences Abstracts]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.355, ranking it 18th out of 33 journals in the category "History of Social Sciences" and 260th out of 332 in the category "Economics".<ref>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8446/homepage/ProductInformation.html |title=Australian Economic History Review: An Asia-Pacific Journal of Economic, Business & Social History - Overview |publisher=Wiley-Blackwell |work= |accessdate=2013-07-01}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-8446}}
* [http://economichistorysociety.wordpress.com/ The Economic History Society of Australia and New Zealand]

{{DEFAULTSORT:Australian Economic History Review}}
[[Category:Economic history journals]]
[[Category:Publications established in 1961]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Triannual journals]]