{{Infobox journal
| title = Journal of Marketing
| image_file = 
| cover = 
| editor = [[V. Kumar (professor)|V. Kumar]]
| discipline = [[Marketing]]
| former_names = 
| abbreviation = J. Marketing
| publisher =  [[American Marketing Association]]
| country = United States
| frequency = Bimonthly
| history = 1936-present
| openaccess = 
| license = 
| impact = 3.819
| impact-year = 2013
| website = https://www.ama.org/publications/JournalOfMarketing/Pages/About.aspx
| link1 = https://www.ama.org/publications/JournalOfMarketing/Pages/Current-Issue.aspx
| link1-name = Online access
| link2 = https://www.ama.org/publications/JournalOfMarketing/Archive/Pages/default.aspx
| link2-name = Online archive
| JSTOR = 
| OCLC = 1782320
| LCCN = 
| CODEN = 
| ISSN = 0022-2429
| eISSN = 
}}
The '''''Journal of Marketing''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering the field of [[marketing]]. It was established in 1936 and is published by the [[American Marketing Association]]. The [[editor-in-chief]] is [[V. Kumar (professor)|V. Kumar]] ([[J. Mack Robinson College of Business]]).

"In 1934, the American Marketing Society established the American Marketing Journal with Frank M. Surface as editor. The following summer the [National Association of Teachers of Marketing & Advertising published] National Marketing Review issued its first number. These two publications were very similar in character and purpose. Their contributors were often the same. Some of the readers belonged to both organizations, and practically all of them had the same interests. Also their editorial boards overlapped. So it was inevitable that the two should be combined into one larger and richer publication. That was accomplished with the July, 1936, issue, which was given a new dress and a new name, The Journal of Marketing, with Dr. [Paul H.] Nystrom as editor."<ref>{{cite journal |last1=Agnew |first1=Hugh |title=The History of the American Marketing Association |journal=Journal of Marketing |date=April 1941 |volume=5 |issue=4 |pages=374-379}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-05-15}}</ref> and the [[Social Sciences Citation Index]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-05-15}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 3.819, ranking it 11th out of 239 journals in the category "Business".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Business |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|https://www.ama.org/publications/JournalOfMarketing/Pages/About.aspx}}

[[Category:English-language journals]]
[[Category:Marketing journals]]
[[Category:Publications established in 1936]]
[[Category:Bimonthly journals]]
[[Category:Academic journals published by learned and professional societies]]