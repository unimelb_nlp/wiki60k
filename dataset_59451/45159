{{About|a U.S. Air Force general|the United States Marine Corps general|John R. Allen|other persons|John Allen (disambiguation){{!}}John Allen}}
{{Infobox military person
| name          = John R. Allen, Jr.
| image         =
| caption       =
| birth_date          = {{birth year and age|1935}}<!-- {{Birth date and age|YYYY|MM|DD}} -->
| death_date          = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| placeofburial_label =
| placeofburial =
| birth_place  = [[Louisville, Kentucky]]
| death_place  =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = [[United States|United States of America]]
| branch        = [[United States Air Force]]
| serviceyears  = 1959–1990
| rank          = [[Brigadier general (United States)|Brigadier General]]
| unit          =
| commands      = [[46th Bomb Squadron|46th Bombardment Squadron]]<br />[[92nd Bombardment Wing]]<br />[[45th Air Division]]
| battles       = [[Vietnam War]]
| awards        = [[Defense Superior Service Medal]]<br />[[Legion of Merit]]<br />[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]
| relations     =
| laterwork     =National Association of Installation Developers
}}
'''John R. Allen, Jr.''' is a retired [[United States Air Force]] brigadier general. Allen served on active duty for 31 years, until his retirement in 1990. He is a highly decorated command pilot with over 6,000 flying hours In B-47s, FB-111s, B-52s, KC-135s and T-39s.

==Biography==

===Early years and education===
John R. Allen, Jr. was born in [[Louisville, Kentucky]], in 1935.  He graduated from high school in [[Spartanburg, South Carolina]], in 1954. He received a [[Bachelor of Arts]] degree from the [[University of Nebraska]] in 1975 and a [[Master of Arts]] degree in Management and Supervision from [[Central Michigan University]] in 1979. He completed Squadron Officer School in 1966, Industrial College of the Armed Forces in 1980 and the program for senior officials in national security at the [[John F. Kennedy School of Government]], [[Harvard University]], in 1985.

==Career==

===Military career===
Allen was commissioned in December 1959 and assigned to Little Rock Air Force Base, Arkansas, as a B-47 co-pilot. In June 1964, he transferred to the 393rd Bombardment Squadron, Pease Air Force Base, New Hampshire, as a B-47 commander. In April 1966, he entered [[B-52]] training and upon completion was assigned to the 20th Bombardment Squadron, [[Carswell Air Force Base]], Texas, as a B-52 commander, later serving as chief of the Standardization Division. Between 1969 and 1971, he flew 150 B-52 combat missions during three temporary duty tours in [[Southeast Asia]] and served as chief of the Tactical Evaluation Division, 8th Air Force, [[Andersen Air Force Base]], Guam. Allen then served as the Arc Light liaison officer at Headquarters U.S. Air Force, Washington, D.C., from April through September 1972.

Allen was an air operations staff officer at Headquarters [[Strategic Air Command]] (SAC), Offutt Air Force Base, Nebraska, until March 1974. His varied duties included the certification of Navy sea mines for use on B-52D's and feasibility tests of B-52 high-altitude laser-guided bomb delivery. He then became chief of the Aircraft and Weapons Test Branch and, later, executive officer to the deputy chief of staff for operations, also at SAC headquarters.

Allen assumed command of the 46th Bombardment Squadron, Grand Forks Air Force Base, North Dakota, in February 1977. He was assigned to the Air Staff at Air Force headquarters in August 1978 as one of two action officers who presented Air Force positions regarding the [[Strategic Arms Limitation Talks]] to the Joint Staff. After completing the Industrial College of the Armed Forces in June 1980, Allen was assigned as chief of the Nuclear Contingency Branch, Strategic Operations Division, Directorate of Operations, Organization of the Joint Chiefs of Staff, Washington, D.C.

He transferred to the 92nd Bombardment Wing, Fairchild Air Force Base, Washington, as vice commander in January 1982 and in August 1982 assumed command of the wing. In addition to his accomplishments while wing commander, he developed Fairchild's Year 2000 Plan, a far-reaching plan to replace many old buildings and modernize the base.

In September 1984, Allen was assigned as the senior military adviser to the director, U.S. Arms Control and Disarmament Agency, Washington, D.C., where he provided high-level liaison with the Office of the Secretary of Defense and the Joint Chiefs of Staff in policy formulation, arms control negotiations and agency operations.

In June 1986, he was assigned as commander of SAC's 45th Air Division, consisting of [[FB-111]]s at Pease Air Force Base, and Plattsburgh Air Force Base, New York; and [[B-52G]]'s at [[Loring Air Force Base]], Maine.

In September 1987, Allen became vice commander, Sacramento Air Logistics Center, Air Force Logistics Command, McClellan Air Force Base, California.

In August 1989, he became the vice commander of [[Oklahoma City Air Logistics Center]], Air Force Logistics Command, [[Tinker Air Force Base]], Oklahoma. He retired after 31 years of service on October 1, 1990.

===Military awards and decorations===
Allen's military decorations and awards include the [[Defense Superior Service Medal]], [[Legion of Merit]], [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]], [[Defense Meritorious Service Medal]], [[Meritorious Service Medal (United States)|Meritorious Service Medal]] with oak leaf cluster, [[Air Medal]] with six oak leaf clusters, [[Air Force Commendation Medal]] with oak leaf cluster and [[Combat Readiness Medal]].

*[[File:US Defense Superior Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Defense Superior Service Medal]]
*[[File:Legion of Merit ribbon.svg|60px]]&nbsp;&nbsp;[[Legion of Merit]]
*[[File:Distinguished Flying Cross ribbon.svg|60px]]&nbsp;&nbsp;[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]
*[[File:Defense Meritorious Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Defense Meritorious Service Medal]]
*[[File:Meritorious Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Meritorious Service Medal (United States)|Meritorious Service Medal]] with oak leaf cluster
*[[File:Air Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Air Medal]] with six oak leaf clusters
*[[File:Air Force Commendation ribbon.svg|60px]]&nbsp;&nbsp;[[Air Force Commendation Medal]] with oak leaf cluster
*[[File:Combat Readiness Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Combat Readiness Medal]]

===Post-military career===
After retiring from the U.S. Air Force, BGen Allen became the first executive director of the National Association of Installation Developers, which assists communities through the effects of the closure of a military base in their area.

==References==
:''This article incorporates text in the [[public domain]] from the United States Air Force.''
*{{cite web|accessdate=6 January 2009
|url=http://www.af.mil/bios/bio.asp?bioID=4506
|title=Biographies: Brigadier General John R. Allen, Jr.
|work=Air Force Link
|publisher=United States Air Force
|date=October 1989 |archiveurl = https://web.archive.org/web/20080709164633/http://www.af.mil/bios/bio.asp?bioID=4506 <!-- Bot retrieved archive --> |archivedate = 9 July 2008}}

==External links==
{{Portal|United States Air Force}}
*{{cite web|accessdate=6 January 2009
|url=http://belobog.si.umich.edu/clair/corpora/corpora/us_congressional_record/101/101.ext.19890801.021.html
|title=In Honor of Brig. Gen. John R. Allen, Jr.
|author=Fazio, Congressman Vic
|date=August 1, 1989 |publisher=Library of Congress}} Recognition of BGen Allen's accomplishments from 1987 to 1989 as vice commander of the Air Logistics Center at McClellan Air Force Base.
*{{cite web|accessdate=6 January 2009 |url=http://siscorpdc.com/associates.htm |title=SISCORP Associates |publisher=SISCORP |deadurl=yes |archiveurl=https://web.archive.org/web/20110827083442/http://siscorpdc.com/associates.htm |archivedate=August 27, 2011 }}

{{DEFAULTSORT:Allen, John R. Jr.}}
[[Category:1935 births]]
[[Category:Living people]]
[[Category:American aviators]]
[[Category:Aviators from Kentucky]]
[[Category:People from Louisville, Kentucky]]
[[Category:American military personnel of the Vietnam War]]
[[Category:Recipients of the Legion of Merit]]
[[Category:United States Air Force generals]]
[[Category:University of Nebraska alumni]]
[[Category:John F. Kennedy School of Government alumni]]
[[Category:Recipients of the Air Medal]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Defense Superior Service Medal]]