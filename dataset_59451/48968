{{multiple issues|
{{Advert|date=February 2011}}
{{Unreliable sources|date=February 2011}}
}}
{{Infobox Software
| name                   = Kajona
| logo                   =
| screenshot             =
| caption                = 
| developer              = [http://www.kajona.de Kajona Community]
| status                 = Active
| latest release version = <!-- If you update this, remember to also update [[Comparison of web application frameworks]]--> 5.0
| latest release date    = {{release date|2016|05|25}}<ref name="kajona_release">{{cite web | url=https://www.kajona.de/en/News/newsdetails.Kajona-V5-0-phartastic-released.newsDetail.967f9a6573cc50692fa1.html | title=Kajona V5.0 "phartastic" available | publisher=kajona.de | date=25 May 2016 | accessdate=22 June 2016}}</ref>
| programming_language   = [[PHP]] [[PHP5]] [[JavaScript]] [[HTML]] [[CSS]] [[SQL]] [[Less (stylesheet language)|LESS]]
| operating system       = [[Cross-platform]]
| genre                  = [[Content management system]]
| license                = [[GNU Lesser General Public License]]
| website                = http://www.kajona.de/
}}
'''Kajona''' is a [[PHP5]] based [[content management]] framework, released and developed as an open source project using the [[LGPL]]-licence.
The system requires a relational database system such as [[MySQL]], [[PostgreSQL]] or [[SQLite]]. Due to the abstraction of the database provided by Kajonas database-layer, nearly all relational database systems can be connected. Kajona uses [[UTF-8]] to store its content, resulting in a system suitable for international websites.
Since the system is written as a framework, external developers are able to enrich the system with new functionalities using one of the many hooks / plugin concepts Kajona provides. There are HotSpots for nearly every aspect such as for widgets, search plugins, elements or modules.

==Features==
Kajona ships with a set of modules and page-elements by default, including a comprehensive page-management (including a [[WYSIWYG]] editor for in-site-editing), navigation management, a search-module and image-elements including the support of on-the-fly image manipulations such as resizing or cropping images. Due to the extensibility, additional modules and elements may be added or removed from existing installations. A complete list of modules may be obtained from the projects website.<ref>http://www.kajona.de/features.xml_utf8_cms...en.html</ref> Besides English and German, the backend is also available in Russian, Portuguese, Swedish and Bulgarian.
A review of the functions and the system was also published on the (German) content manager portal contentmanager.de.<ref>http://www.contentmanager.de/magazin/artikel_2243_kajona_systemvorstellung.html</ref>

==Usability==
The system itself is separated into two main areas: The administration backend and the portal. While the backend is used to maintain the website, the portal is used to show the contents to the websites' visitors.
Compared to other CMS, Kajona is based on a page-centric approach. This means that content is placed on a page directly instead of using a complex article management linking the articles to a single page.

Editing content is possible either via the backend or via the portal-editor.

==Portal-Editor==
The portal-editor is shown to users with edit-permissions when browsing the website (the portal). The editor is integrated into two separate ways: 
In-page-editing: Accessible via a link when hovering an editable element: The portal-editor is shown as an overlay, on top of the portal-page. The editor allows to change, edit and delete the element using the functionality of the backend while still being shown in the portal.
In-site-editing: Accessible directly on the page. The user is able to click into the element (e.g. a paragraph) and start to change the content right in the final layout. Markup-toolbars appear as soon as the edit-process starts. The changes are saved on the fly, so the user may focus on changing the contents completely.

==Architecture==
Kajona is separated in several layers, providing a separation of concerns.
The database-layer can be used with nearly every relational database-system. By default, the system ships with drivers for [[MySQL]], [[MariaDB]] (mysql, mysqli), [[PostgreSQL]], [[Oracle Database|Oracle]] and [[SQLite]] (as on 01/13).

The business-logic layer consists of a number of business objects, each representing a single entity within the system, e.g. a page or an image. The layer provides the logic to handle those objects including [[Create, read, update and delete|CRUD]]-operations (create, read, update, delete). The system handles the lifecycle of each object including the logic to update or insert new object and synchronizing the objects with the database-layer.

Since the presentation-layer only makes use of the business objects, there is absolutely no database-knowledge required when working with the presentation. In addition, the presentation-layer contains the controller, triggering all further actions within the framework.

Kajona uses a [[Template engine (web)|template engine]] in order to render the layout. The engine provides a way to separate layout from content and differentiates between page- and template-elements. This results in a flexible way to create layouts and provides a way to reuse templates. 
The templates are enriched with placeholders, later being filled with the contents provided by the business objects.
Since all generated content is cached, the system delivers the pages out of the cache after the initial generation.
Templates can be used for the frontend and the (administrative) backend.

Permissions are granted using a hierarchical structure, providing the possibility to inherit the settings from a parent-node.

Since Version 4, all modules and templates are distributed as packages. The code-files of different modules are no longer merged into common folders, instead a new filesystem-layout separates each package and provides a virtual filesystem in order to overwrite or redefine files shipped with the packages without having to modify the original files. Therefore, updates can be enrolled without the danger of breaking former modification.

Starting with version 4.3, the framework-aspect is now way more present. Many modules are now fully decoupled. This results in independent backend- and portal-parts, making the backend usable for rapid web application development.<ref>http://www.kajona.de/en/News/newsdetails.Kajona-as-an-application-framework.newsDetail.0ea31a1526a21f07479d.html</ref>

==History==
In 2004 Kajona was built in its initial version reflecting a shared list of scripts often used by a few web developers. Those scripts were combined by introducing interfaces in order to provide easier interaction. Resulting in a first script library the idea of a framework was born and released as version 1.0.
Version 2 was released in 2005, followed by the version 2.1 in 2006. Since the project was still a rather unstructured list of independent scripts, a complete rewrite was done for version 3, released in January 2007.<ref>http://www.kajona.de/newsdetails.Kajona-V3-released.newsDetail.d31d98045a039b9a232c.de.html</ref> The codebase was reorganized to be fully object-oriented, providing a strict separation of concerns and a full division of logic and layout.<ref>http://www.kajona.de/kajona.open_source_content_management_framework...en.html</ref>
As of now (Jan 2011), Kajona has grown to a comprehensive, flexible and robust framework providing a large number of predefined modules and elements for a wide range of capabilities.<ref>http://www.cmsmatrix.org/matrix/cms-matrix/kajona-content-management-framework</ref><ref>http://www.kajona.de/features.xml_utf8_cms...en.html</ref>
The framework is used by public institutes such as the [[University of Kassel]], the [[ETH Zürich]] and a lot of small and middle-sized companies around Europe and all over the world as in South-Africa.<ref>http://www.kajona.de/referenzen.projects...en.html</ref>
The release 3.3 was also featured on Heise Open <ref>http://web.archive.org/web/20131208034944/http://www.h-online.com/open/news/item/Version-3-3-of-the-Kajona-CMS-released-983923.html</ref> and other websites.<ref>http://www.developer.com/daily_news/article.php/398864/Open-Source-Content-Management-System-Kajona-Version-33-Is-Available.htm</ref>
The project is now working on a new major release, version 4.<ref>http://www.kajona.de/newsdetails.Start-of-the-V4-development.newsDetail.c613cb34ec197abbd45b.en.html</ref> The development-progress may be followed in a blog created for the v4-development.<ref>http://www.kajona.de/v4-devblog.en.html</ref> Since v4 will introduce fundamental changes such as a new filesystem-layout,<ref>http://www.kajona.de/newsdetails.The-new-filesystem-layout.newsDetail.d5601834ee76b5b2409b.en.html</ref> the release is not yet scheduled. In addition, the feature-development of the v3-branch was stopped, only security-fixes will be deployed and released.

==Derivatives==
There are a few forks of Kajona such as Sycon being developed non-public.
In addition, the linux distribution [[Kajonix]]<ref>http://www.stefanbongartz.de/kajonix/index_en.php</ref> provides a live-cd containing the latest Kajona release.

== External links ==
* [http://www.kajona.de/ Official project website ]
* [http://www.kajonabase.net/ KajonaBase User Community website]
* [http://board.kajona.de/ Community board]
* [http://trace.kajona.de/ Public bugtracker]
* [http://www.pro-linux.de/news/1/16097/content-management-system-kajona3-in-version-331-erschienen.html Kajona 3.3.1 release article on ProLinux]

==References==
{{reflist}}

{{br}}

{{Application frameworks}}

[[Category:Free content management systems]]
[[Category:Web frameworks]]
[[Category:Free software programmed in PHP]]
[[Category:PHP frameworks]]