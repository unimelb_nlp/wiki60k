{{Infobox journal
| title = Journal of Mathematical Psychology
| cover =
| editor = Philip L. Smith
| discipline = [[Mathematical psychology]], [[theoretical psychology]]
| abbreviation = J. Math. Psychol.
| publisher = [[Elsevier]]
| country =
| frequency = Bimonthly
| history = 1964–present
| openaccess =
| impact = 1.622
| impact-year = 2012
| website = http://www.mathpsych.org/index.php?option=com_content&view=article&id=23&Itemid=40
| link1 = http://www.elsevier.com/wps/find/journaldescription.cws_home/622887/description#description
| link1-name = Journal page at publisher's website
| link2 = http://www.sciencedirect.com/science/journal/00222496
| link2-name = Online access
| JSTOR =
| OCLC = 01783082
| LCCN =
| CODEN = JMTPAJ
| ISSN = 0022-2496
| eISSN =
}}
The '''''Journal of Mathematical Psychology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] established in 1964. It covers all areas of [[mathematical psychology|mathematical]] and [[theoretical psychology]], including [[sense|sensation]] and [[perception]], [[psychophysics]], learning and memory, problem solving, judgment and decision-making, and [[motivation]]. It is the official journal of the [[Society for Mathematical Psychology]] and is published on their behalf by [[Elsevier]].

== Abstracting and indexing ==
The journal is abstracted and indexed by:
{{columns-list|colwidth=30em|
* [[ACM Guide to Computing Literature]]
* [[Biological Abstracts]]
* [[CompuMath Citation Index]]
* [[Computing Reviews]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[Current Index to Statistics]]
* [[Mathematical Reviews]]
* [[PsycINFO]]/[[Psychological Abstracts]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.622..<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Mathematical Psychology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.mathpsych.org/index.php?option=com_content&view=article&id=23&Itemid=40}}

{{DEFAULTSORT:JOURNAL OF MATHEMATICAL PSYCHOLOGY}}
[[Category:Publications established in 1964]]
[[Category:Mathematical and statistical psychology journals]]
[[Category:Elsevier academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]