{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox Australian road
| type         = road
| road_name    = Cross Road
| state        = sa
| length       = 8
| direction_a  = East
| direction_b  = West
| end_a        = {{AUshield|N|M1}} [[South Eastern Freeway]], [[Glen Osmond, South Australia|Glen Osmond]]
| end_b        = {{AUshield|SA|A5}} [[Anzac Highway]], [[Plympton, South Australia|Plympton]]
| est          =
| through      = {{SAcity|Black Forest| Clarence Gardens| Clarence Park| Edwardstown| Glandore| Hawthorn| Plympton}}
| route        = {{AUshield|SA|A3}} A3
| exits        = {{plainlist|
*{{AUshield|N|A17}} [[Portrush Road, Adelaide|Portrush Road]]
*{{AUshield|SA|A1}} [[Glen Osmond Road, Adelaide|Glen Osmond Road]]
*{{AUshield|SA|A13}} [[South Road, Adelaide|South Road]]
*{{AUshield|SA|A14}} [[Marion Road]]
}}
<!--
|latd_a=34 |latm_a=58 |lats_a=06
|longd_a=138 |longm_a=32 |longs_a=53
|latd_b=34 |latm_b=57 |lats_b=48
|longd_b=138 |longb_m=38 |longb_s=38
|alternative_location_map = Australia Greater Adelaide
-->
|coordinates={{coord|34|58|06|S|138|32|53|E|region:AU-SA_type:landmark|display=inline,title}}
}}

[[File:Cross rd urrbrae.jpg|right|thumb|250px|The eastern end of Cross Road. [[Urrbrae, South Australia|Urrbrae]] is on the left]]
'''Cross Road''' is a major arterial road that travels east–west through the inner southern suburbs of the [[Australia]]n city of [[Adelaide]].<ref>{{cite book|title=2003 Adelaide Street Directory, 41st Edition |publisher=UBD (A Division of Universal Press Pty Ltd) |year=2003 |isbn=0-7319-1441-4}}</ref> Its western terminus is at [[Anzac Highway, Adelaide|Anzac Highway]], travelling east and ending at [[Glen Osmond, South Australia|Glen Osmond]] and the [[Adelaide Hills]], joining the junction of [[Glen Osmond Road, Adelaide|Glen Osmond Road]], [[Portrush Road, Adelaide|Portrush Road]], and [[South Eastern Freeway]].

==Route description==
There are three railway level crossings along Cross Road: the [[Glenelg Tram]] in [[Plympton, South Australia|Plympton]],  the [[Seaford railway line]] at ''[[Emerson Crossing]]'' and the [[Belair railway line]] and [[Adelaide-Melbourne railway]] in [[Unley Park, South Australia|Unley Park]]. [[South Road, Adelaide|South Road]] passes over the Emerson level crossing on a large overpass. It was built between 1982 and 1984 to reduce the traffic congestion caused by the junction and level crossing. All other junctions are at-grade, with traffic lights at main roads.

There are a number of suburbs which Cross Road passes through and borders.  These are:
{{columns-list|colwidth=15em|
* [[Black Forest, South Australia|Black Forest]]
* [[Clarence Gardens, South Australia|Clarence Gardens]]
* [[Clarence Park, South Australia|Clarence Park]]
* [[Cumberland Park, South Australia|Cumberland Park]]
* [[Edwardstown, South Australia|Edwardstown]]
* [[Glandore, South Australia|Glandore]]
* [[Glen Osmond, South Australia|Glen Osmond]]
* [[Hawthorn, South Australia|Hawthorn]]
* [[Highgate, South Australia|Highgate]]
* [[Kings Park, South Australia|Kings Park]]
* [[Kingswood, South Australia|Kingswood]]
* [[Malvern, South Australia|Malvern]]
* [[Myrtle Bank, South Australia|Myrtle Bank]]
* [[Netherby, South Australia|Netherby]]
* [[Plympton, South Australia|Plympton]]
* [[Plympton Park, South Australia|Plympton Park]]
* [[South Plympton, South Australia|South Plympton]]
* [[Unley Park, South Australia|Unley Park]]
* [[Urrbrae, South Australia|Urrbrae]]
* [[Westbourne Park, South Australia|Westbourne Park]]
}}

==History==
In a 1949 street directory, Cross Road had its current route, but was named "Cross Roads" (plural) and is shown as the aggregation of a number of local street names including:<ref>{{cite web |url=http://www.osmaustralia.org/gregorysmaps.php |title=Gregory's Street Directory of Adelaide and Suburbs |via=Open Street Map Australia |edition=1949 |accessdate=10 July 2016}}</ref>
*High Terrace (between Fullarton and Unley Roads)
*Napier Terrace (Between Unley and Goodwood Roads)
*South Terrace (between Goodwood and South Roads)
*Glen Osmond Road (between Goodwood and Marion Road)
*Plympton Terrace (between Marion Road and Anzac Highway)
==Major intersections==
{{AUSinttop |dest_ref=<ref name="gmap">{{google maps |url=https://www.google.com.au/maps/dir/-34.9633949,138.6441263/-34.9685069,138.5479619/@-34.9678605,138.5495067,16z/data=!4m2!4m1!3e0?hl=en |accessdate=9 July 2016}}</ref> |location_ref=<ref>{{cite web |url=http://maps.sa.gov.au/plb/ |title=Property Location Browser |publisher=[[Government of South Australia]] |accessdate=9 July 2016}}</ref> |length_ref=<ref name="gmap"/>}}
{{SAint
|LGA=[[City of Unley]], [[City of Mitcham]]
|LGAspan=5
|location_special={{SAcity|Glen Osmond|Myrtle Bank|Urrbrae}}
|type=
|km=0
|road={{plainlist|
* {{AUshield|SA|A1}} [[Glen Osmond Road, Adelaide|Glen Osmond Road]] – [[Adelaide city centre]]
*{{AUshield|N|A17}} [[Portrush Road, Adelaide|Portrush Road]] – [[Payneham, South Australia|Payneham]]
* {{AUshield|N|M1}} [[South Eastern Freeway]] – [[Murray Bridge, South Australia|Murray Bridge]]
}}
|notes=
}}
{{SAint
|location_special={{SAcity|Myrtle Bank|Urrbrae|Highgate|Netherby}}
|type=
|km=1.6
|road=[[Fullarton Road, Adelaide|Fullarton Road]] – [[Eastwood, South Australia|Eastwood]], [[Adelaide city centre]] / [[Mitcham, South Australia|Mitcham]], [[Belair, South Australia|Belair]]
|notes=
}}
{{SAint
|location_special={{SAcity|Malvern|Kingswood|Unley Park|Hawthorn}}
|type=
|km=3.3
|road=[[Unley Road, Adelaide|Unley Road]] – [[Adelaide city centre]] / [[Belair Road, Adelaide|Belair Road]] – [[Blackwood, South Australia|Blackwood]]
|notes=
}}
{{SAint
|location_special={{SAcity|Unley Park|Hawthorn|Kings Park|Westbourne Park}}
|type=
|km=4.3
|place=[[Belair railway line]] and [[Adelaide-Melbourne railway line]]
|notes=
}}
{{SAint
|location_special={{SAcity|Kings Park|Westbourne Park|Clarence Park|Cumberland Park}}
|type=
|km=4.9
|road=[[Goodwood Road, Adelaide|Goodwood Road]] – [[Adelaide city centre]] / [[Darlington, South Australia|Darlington]]
|notes=
}}
{{SAint
|LGA=[[City of Unley]], [[City of Mitcham]], [[City of Marion]]
|LGAspan=
|location_special={{SAcity|Black Forest|Clarence Park|Clarence Gardens|Glandore|Edwardstown}}
|type=
|km=6.5
|road= {{AUshield|SA|A13}} [[Main South Road]] – [[Hindmarsh, South Australia|Hindmarsh]] / [[Darlington, South Australia|Darlington]]
|notes=[[Emerson Crossing]] – South Road goes over a bridge, and [[Seaford railway line]] is a level crossing under the bridge, between the junction ramps
}}
{{SAint
|LGAC=Marion
|LGAspan=
|location_special={{SAcity|South Plympton|Plympton Park}}
|type=
|km=8.1
|road={{AUshield|SA|A14}} [[Marion Road]] – [[Adelaide Airport]]
|notes=
}}
{{SAint
|LGA=[[City of Marion]], [[City of West Torrens]]
|LGAspan=
|location_special={{SAcity|Plympton|Plympton Park}}
|lspan=
|type=
|km=8.3
|place=[[Glenelg tram]]
|notes=
}}
{{SAint
|LGAC=West Torrens
|LGAspan=
|location=Plympton
|lspan=
|type=
|km=8.8
|road={{AUshield|SA|A5}} [[Anzac Highway]] – [[Glenelg, South Australia|Glenelg]]
|notes=
}}
{{Jctbtm}}

== References ==
{{reflist}}

{{Road infrastructure in Adelaide}}

[[Category:Roads in Adelaide]]