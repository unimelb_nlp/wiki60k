{{Underlinked|date=April 2013}}

'''Wellness Recovery Action Plan''' ('''WRAP''') is a [[Recovery approach|recovery model]] developed by a group of people in northern Vermont in 1997 in a workshop on mental health recovery led by [[Mary Ellen Copeland]]. It has been extensively studied and is now an evidence-based practice, listed in the National Registry of Evidence Based Programs and Practices.<ref>{{cite book |title=Facilitator Training Manual: Mental Health Recovery Including WRAP |first=Mary Ellen |last=Copeland |year=2012}}</ref>

WRAP focuses on a person's strengths, rather than perceived deficits.  WRAP is voluntary and trauma informed. People develop their own WRAP.<ref>{{cite web |url= http://nrepp.samhsa.gov/ViewIntervention.aspx?id=208 |title=Intervention Summary: Wellness Recovery Action Plan (WRAP) |first= |last= |work=nrepp.samhsa.gov |year=2013 |accessdate=January 23, 2013}}</ref>

== See also ==
* [[Tidal Model]], developed by Professor Phil Barker, Poppy Buchanan-Barker and others.
* [[Recovery approach]], an overview of the recovery approach to mental health.
* [https://web.archive.org/web/20100617193606/http://www.psychminded.co.uk/news/news2010/jan10/Get-radical-again-in-mental-health004.html? Thrive model] developed by Dr. Mike Smith and Marion Aslan.

== References ==
{{reflist}}

== External links ==
* [http://mentalhealthrecovery.com/ mentalhealthrecovery.com] - Official website
* [http://www.samhsa.gov/index.aspx SAMHSA] - The Substance Abuse and Mental Health Services Administration website
* [http://www.bu.edu/cpr/repository/articles/pdf/deegan1996.pdf Recovery as a Journey of the Heart] (PDF)

[[Category:Addiction psychiatry]]
[[Category:Psychosocial rehabilitation]]
[[Category:Social work]]
[[Category:Psychiatric services]]
[[Category:Psychiatric treatments]]
[[Category:Drug rehabilitation]]