{{Infobox journal
| title         = The Found Poetry Review
| cover         = 
| editor        = Jenni B. Baker
| discipline    = [[Literary magazine|literary journal]]
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = 
| country       = United States
| frequency     = Biannually
| history       = 2011-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.foundpoetryreview.com/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 
| eISSN         = 
| boxwidth      = 
}}

'''The Found Poetry Review''' is a biannual American [[literary magazine]] dedicated exclusively to publishing [[erasure (artform)]], [[cut-up]] and other forms of [[found poetry]].<ref>[https://books.google.com/books?id=rkrcAwAAQBAJ&pg=PT149&dq=%22the+found+poetry+review%22&hl=en&sa=X&ei=w-G5U4a9J-mpsAT2-oH4Dw&ved=0CCYQ6AEwAQ#v=onepage&q=%22the%20found%20poetry%20review%22&f=false/ Poem Central: Word Journeys with Readers and Writers]</ref> The journal has published seven volumes of found poetry since its inception in 2011, initially launching online<ref>[http://www.poetryfoundation.org/harriet/2011/07/introducing-the-found-poetry-review/ Poetry Foundation: Introducing the Found Poetry Review]</ref> and migrating to print in mid-2012. It is based in [[Bethesda, Maryland]].<ref>[http://www.gazette.net/article/20110803/ENTERTAINMENT/708039914/1148/bethesda-resident-creates-first-journal-dedicated-to-found-&template=gazette/ Gazette: Bethesda resident creates first journal dedicated to found poetry]</ref>

== Background ==

The mission of The Found Poetry Review is to "celebrate the poetry in the existing and the everyday."<ref>[http://www.foundpoetryreview.com/about-the-found-poetry-review/ About The Found Poetry Review]</ref> The journal publishes found poetry sourced from both traditional and nontraditional texts, including newspaper articles, instruction booklets, dictionaries, toothpaste boxes, biographies, [[Craigslist]] posts, speeches, existing poems and other sources. Approximately nine percent of all submissions are accepted for publication; to date, the journal has published the work of more than 150 poets.

== Issues ==

Since 2011, the Found Poetry Review has published seven volumes of work. Each volume of the journal features contributions from 25-30 poets. Additionally, the journal publishes special online issues of found poetry, including a 2013 issue dedicated to [[David Foster Wallace]] and a 2014 edition celebrating [[Bloomsday]].

Since 2012, the Found Poetry Review has engaged 70–80 poets per year in [[National Poetry Month]]-related projects. In February 2014, editors were invited to speak on project-driven publicity at the [[Association of Writers & Writing Programs]] annual conference in [[Seattle, Washington]].

=== Oulipost ===

In 2014, the journal selected 78 poets from seven countries to participate in Oulipost. The project—whose name is a mash-up of [[Oulipo]] and "the post"—guided poets to apply [[experimental literature]] techniques to text source from poets' daily newspapers. Participating poets were profiled in the [[Fairbanks Daily News-Miner]],<ref>[http://www.newsminer.com/news/local_news/poetry-in-the-newspaper-local-poet-participates-in-experimental-writing/article_c83934b6-c3a6-11e3-b647-0017a43b2370.html Poetry in the newspaper: Local poet participates in experimental writing project]</ref> [[The Salt Lake Tribune]],<ref>[http://www.sltrib.com/sltrib/blogsmoviecricket/57854794-66/hopkinson-poetry-oulipost-tribune.html.csp Provo poet finds inspiration in the Tribune  ]</ref> [[What Weekly]]<ref>[http://whatweekly.com/2014/04/23/national-poetry-month-carla-jean-valluzzis-oulipost/ National Poetry Month: Carla Jean Valluzzi’s Oulipost]</ref> and the Hebden Bridge Times,<ref>[http://www.hebdenbridgetimes.co.uk/what-s-on/arts-culture-and-entertainment/winston-s-under-way-1-6554678 Winston's Under Way! ]</ref> among other publications. The project resulted in the creation of 2,300 new poems.

=== Pulitzer Remix ===

In April 2013, the Found Poetry Review hosted Pulitzer Remix, an initiative that united 85 poets to create found poetry from the (then) 85 [[Pulitzer Prize for Fiction]]-winning texts. Poets, each of whom created new works from a specific novel, collectively created 2,550 new poems by month's end. The resulting poems garnered more than 12,200 comments and nearly 180,000 page views during the month. News coverage on the project was widespread, with profiles appearing in venues such as ''[[The Phoenix (newspaper)|The Phoenix]]'',<ref>[http://thephoenix.com/boston/news/153501-reworking-pulitzer-masterpieces/ Reworking Pulitzer Masterpieces]</ref> [[WORT]],<ref>[http://www.wortfm.org/found-poetry-review-brings-you-pulitzer-remixes/ Found Poetry Review Brings You Pulitzer Remixes]</ref> the ''[[York Daily Record]]'',<ref>[http://www.ydr.com/ci_22980588/west-york-teacher-finds-poetry West York teacher 'finds' poetry in Pulitzer Remix project]</ref> and many others.

=== The Found Poetry Project ===

On the heels of a successfully funded [[Kickstarter]] campaign,<ref>[https://www.kickstarter.com/projects/jennibbaker/the-found-poetry-project The Found Poetry Project on Kickstarter]</ref> the Found Poetry Review distributed 250 found poetry kits in public spaces across America. The kits instructed finders to create a found poem from the kit contents and upload the results on the (now-defunct) project website.

== Masthead ==

* '''Editor-in-Chief''': Jenni B. Baker
* '''Senior Poetry Editor / Web Manager''': Beth Ayer
* '''Book Reviews Editor''': Douglas J. Luman
* '''News and Resources Editor''': Martin Elwell
* '''Poetry Editors''': E. Kristin Anderson, Justin Bond, Deborah Hauser, Vicki Hudson, S.E. Ingraham, Sonja Johanson and Amanda Papenfus.

== References ==

{{reflist}}

== External links ==
*[http://www.foundpoetryreview.com/ The Found Poetry Review]
*[http://www.pulitzerremix.com/ Pulitzer Remix: A 2013 National Poetry Month Project]

{{DEFAULTSORT:Found Poetry Review}}
[[Category:American literary magazines]]
[[Category:Magazines established in 2011]]
[[Category:Poetry literary magazines]]
[[Category:Magazines published in Maryland]]
[[Category:Biannual magazines]]