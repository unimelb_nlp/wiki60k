{{about||the South African field hockey player|Iain Evans (field hockey)|people of a similar name|Ian Evans (disambiguation)}}
{{Use Australian English|date=August 2016}}
{{Use dmy dates|date=January 2015}}

{{Infobox Officeholder
|honorific-prefix = [[The Honourable]]
|name             = Iain Evans
|honorific-suffix = 
|image            =

|office           = 38th [[Leader of the Opposition (South Australia)|Leader of the Opposition (SA)]]
|deputy           = [[Vickie Chapman]]
|term_start       = 30 March 2006
|term_end         = 12 April 2007
|predecessor      = [[Rob Kerin]]
|successor        = [[Martin Hamilton-Smith]]

|office2          = Deputy [[Leader of the Opposition (South Australia)|Leader of the Opposition (SA)]]
|term_start2      = 21 November 2005
|term_end2        = 29 March 2006
|predecessor2     = [[Dean Brown]]
|successor2       = [[Vickie Chapman]]

|office3          = Minister for Environment and Heritage
|term_start3      = 14 February 2000
|term_end3        = 5 March 2002
|premier3         = [[John Olsen]] (2000–2001)<br />[[Rob Kerin]] (2001–2002)
|predecessor3     = [[Dorothy Kotz]]
|successor3       = [[John Hill (Australian politician)|John Hill]]

|office4          = Minister for Industry and Trade
|term_start4      = 8 October 1998
|term_end4        = 4 December 2001
|premier4         = [[John Olsen]] (1998–2001)<br />[[Rob Kerin]] (2001)
|predecessor4     = [[John Olsen]]
|successor4       = [[Rob Lucas]]

|office5          = Minister for Recreation, Sport & Racing
|term_start5      = 8 October 1998
|term_end5        = 4 December 2001
|premier5         = [[John Olsen]] (1998–2001)<br />[[Rob Kerin]] (2001)
|predecessor5     = Himself (as Delegate Minister)
|successor5       = [[Dorothy Kotz]]

|constituency_MP6 = [[Electoral district of Davenport|Davenport]]
|parliament6      = South Australian
|term_start6      = 11 December 1993
|term_end6        = 30 October 2014
|predecessor6     = [[Stan Evans]]
|successor6       = [[Sam Duluk]]

|birthname        = Iain Frederick Evans
|birth_date       = {{birth date and age|df=yes|1959|4|18}}
|birth_place      = [[Darlington, South Australia]]
|nationality      = Australian
|death_date       =
|death_place      =
|party            = [[Liberal Party of Australia (South Australian Division)|Liberal Party of Australia (SA)]]
|spouse           = 
|relations        = 
|alma_mater       =
|profession       = 
}}
'''Iain Frederick Evans''' (born 18 April 1959)<ref>[http://www.parliament.curriculum.edu.au/parl.php3?srch=parl&list_params=gender%3D%26offices_held%3D%26party%3D%26parliament%3D%26surname%3Devans%26electorate%3D%26former_occupations%3D&ID=421]</ref> is a former Australian politician. He was leader of the [[Liberal Party of Australia (South Australian Division)|South Australian Division of the Liberal Party of Australia]] from 2006 to 2007.

==Early life==
He studied at Heathfield High School and gained a bachelor's degree for Building Technology from the SA Institute of Technology (now [[University of South Australia]]). Prior to entering politics he managed a family-owned building and retailing business.

==Parliament==
He was elected in the 1993 [[South Australian state election, 1993|election landslide]] for the safe conservative seat of [[Electoral district of Davenport|Davenport]] (following the retirement of his father, [[Stan Evans]]). Evans held various portfolios in the [[John Olsen|Olsen]] and [[Rob Kerin|Kerin]] governments, including Correctional Services, [[Department for Environment and Heritage|Environment & Heritage]], Racing and Volunteers. He gained the Deputy Leadership in November 2005 and with the resignation of Kerin after the [[South Australian state election, 2006|electoral defeat of 2006]] became Leader of the South Australian Liberal Party in a joint leadership ticket with [[Vickie Chapman]].

The following is a quote from pollbludger.com:<ref>[http://www.pollbludger.com/sa2006/davenport.htm The Poll Bludger – Australian State and Federal Elections]</ref>

:''Evans Senior decided to pull the pin altogether and make way for his son at the 1993 election. Evans Junior arrived just in time for a Liberal government and quickly established himself, making it to cabinet in December 1997. He has served in the police, industry, trade and environment portfolios in government, and in planning and industrial relations in opposition. Widely rated as the Liberal Party's best performer in parliament, Evans was discussed as a potential successor when John Olsen was obliged to resign over the [[Motorola affair]] in October 2001. It rarely escapes notice that his factional rival to succeed Rob Kerin as leader is Vickie Chapman, daughter of the aforementioned Ted Chapman – perpetuating a rivalry which now spans three decades. Evans achieved a major victory when he defeated Chapman in the deputy leadership vote after Dean Brown's resignation in November 2005, by 15 votes to five.''

By February 2007, financial woes within the state Liberal party as well as contests between the party president and opposition leadership had come to a head.<ref>{{cite news|url=http://www.theaustralian.news.com.au/story/0,20867,21275309-5006787,00.html|title=Lib leaders at loggerheads|work=The Australian|date=24 February 2007|accessdate=2007-02-28}}</ref> At a meeting in Norwood, Evans reportedly commented that "when we lose the federal election at the end of the year, the Liberal Party will be in dire straits and we have got to plan to deal with that". The alleged gaffe drew a rebuke from one federal Liberal MP who labelled Evans and his state parliamentary team "hopeless".<ref>{{cite news|title=Evans remark draws defeatist tag|work=The Advertiser|date=21 February 2007|pages=2}}</ref> Party president Christopher Moriarty accused Evans of being "piss-weak and gutless" for not backing a business plan aimed at assisting the party out of its parlous financial situation.<ref name=gutless>{{cite news|last=Bildstien|first=Craig|title=Libs chief attacks Evans as 'gutless'|work=Adelaide Advertiser|pages=3|date=22 February 2007}}</ref> High-ranking party members were canvassing support for an urgent no-confidence motion in Mr Moriarty, with one senior figure quoted as saying that "Moriarty is to the Liberal Party what Mark Latham was to Labor",<ref name=gutless/> but others counselled Evans against challenging Moriarty due to the high chance of failure. By late February, speculation over the opposition leader's future was being reported in the media, although he was expected to survive in the short term due to lack of options with 15 MPs remaining in the lower house.<ref>{{cite news|last=Henderson|first=Nick|title=Evans has the aura of a defeated man|work=The Advertiser|pages=11|date=24 February 2007}}</ref><ref>{{cite news|last=Naughton|first=Kevin|title=Drums beat for Mr Who|work=Sunday Mail|pages=9|date=25 February 2007}}</ref><ref>{{cite news|last=Bildstien|first=Craig|title=Evans warned of coup backlash|work=The Advertiser|pages=2|date=27 February 2007}}</ref> Initial reports suggested previous aspirant [[Martin Hamilton-Smith]] might challenge for the position, with [[Isobel Redmond]] as deputy.<ref>{{cite news|last=Kelton|first=Greg|title=Strong support for Hamilton-Smith as leader Evans will not last the year|work=The Advertiser|pages=13|date=30 March 2007}}</ref><ref>{{cite news|title=Do SA a big favour, Iain – step down (Editorial)|work=The Advertiser|pages=39|date=1 April 2007}}</ref>

In early April, Hamilton-Smith announced his leadership intentions to challenge Evans.<ref>[http://www.news.com.au/adelaidenow/story/0,22606,21534553-5006301,00.html Lib leader Iain Evans faces spill] ''The Advertiser'', 10 April 2007.  Retrieved on 10 April 2007.</ref> Former Liberal Premier Kerin offered his opinion that Hamilton-Smith should and would fail,<ref>[http://www.theaustralian.news.com.au/story/0,20867,21538129-601,00.html Party must back Evans, says former Liberal Premier] ''The Australian'', 11 April 2007.  Retrieved on 11 April 2007.</ref> however on 11 April 2007, Hamilton-Smith defeated Evans on 13 votes to 10, becoming the new Liberal opposition leader after deputy leader Vickie Chapman offered her support to him.

Upon former Liberal leader [[Alexander Downer]]'s retirement from federal politics, Evans announced his nomination for Liberal preselection in the [[Mayo by-election, 2008|2008 Mayo by-election]].<ref>[http://www.abc.net.au/news/stories/2008/07/02/2291667.htm Key SA Lib considers run for Mayo – ABC News (Australian Broadcasting Corporation)]</ref><ref>[http://www.news.com.au/adelaidenow/story/0,22606,23987886-2682,00.html AdelaideNow... State MP Iain Evans to run in Alexander Downer's seat of Mayo]</ref> The preselection plebiscite was held to be held on 22 July,<ref>[http://news.smh.com.au/national/nine-seek-liberal-preselection-in-mayo-20080709-3cfv.html Nine seek Liberal preselection in Mayo – Breaking News – National – Breaking News]</ref> but was brought forward to 20 July, with former [[John Howard|Howard]] staffer chiefly for [[WorkChoices]], [[Jamie Briggs]], winning preselection, with Evans coming second.<ref>[http://www.theage.com.au/national/howard-adviser-in-mayo-20080720-3i97.html Howard adviser in Mayo: The Age 21/7/2008]</ref> [[Bob Day]], who held membership of the Liberal Party for 20 years, and the endorsed Liberal candidate for [[Division of Makin|Makin]] in 2007, quit the party after failing in his bid to win Mayo preselection, citing a "manipulated" preselection process.<ref>[http://www.theaustralian.news.com.au/story/0,25197,24087075-2702,00.html Loyal Lib quits over Mayo: The Australian 28/7/2008]</ref> Evans agreed to some extent.<ref>[http://www.theaustralian.news.com.au/story/0,25197,24093560-5013871,00.html Liberal chief Iain Evans admits to Mayo concerns: The Australian 29/7/2008]</ref>

Remarkably on 30 March 2010 exactly four years after he had been elected Liberal leader, Evans stood for the deputy's job but was defeated by the man who toppled him for the leadership in 2007, Hamilton-Smith. Evans was once again defeated by Hamilton-Smith for a leadership position in a rematch between the two former leaders. Evans was overlooked for the deputy's job despite getting the support of Redmond. Redmond had made it known that Hamilton-Smith was not her preferred deputy and a party room meeting was called for 6 April to reconsider the deputy's job. Hamilton-Smith stood aside but Evans did not renominate for the job and it went instead to Mitch Williams.

On 31 January 2013 Redmond resigned as leader. Steven Marshall, who had by this point replaced Williams as deputy leader, became acting leader and formally appointed leader on 4 February. Evans, once again contested the now vacant deputy's job but was defeated by his former deputy Vickie Chapman.

Evans suffered a 2.8-point [[two-party-preferred|two-party]] swing against him, reduced to a margin of 8.1 points in Davenport at the [[South Australian state election, 2014|2014 state election]], with two-party swings against him of up to 8 points in some booths, including the traditionally Liberal-voting booth of Belair which Labor won by three votes.<ref>[http://www.ecsa.sa.gov.au/elections/2014-state-election-results-summary/house-of-assembly-district-results/pollingboothsummary/709 2014 Davenport booth results: ECSA]</ref><ref>[http://www.ecsa.sa.gov.au/elections/state-elections/past-state-election-results/7609?view=result 2010 Davenport booth results: ECSA]</ref> On 6 June 2014 he announced he would stand down from the shadow ministry and parliament within a year and before the [[South Australian state election, 2018|next election]]. There was speculation that Evans was asked to delay his resignation and the by-election for a year due to federal Liberal government budget cuts and that there could be a "super Saturday" of by-elections in up to five Liberal-held seats.<ref>[http://www.abc.net.au/news/2014-06-09/davenport-by-election-labor-greens-parnell/5509984 Davenport by-election – Labor lacking confidence, accuse Greens: ABC 10 June 2014]</ref><ref>[http://www.adelaidenow.com.au/news/south-australia/liberals-eye-super-byelection-to-oust-deadwood-mps-after-martin-hamiltonsmiths-defection/story-fni6uo1m-1226936588579 Liberals eye super by-election to oust ‘deadwood’ MPs after Martin Hamilton-Smith’s defection: The Advertiser 30 May 2014]</ref>

Evans resigned from parliament on 30 October 2014. A [[Davenport state by-election, 2015|2015 Davenport by-election]] occurred.<ref>[http://www.abc.net.au/news/2014-06-06/iain-evans-liberal-to-quit-politics-by-election/5505964 Iain Evans to quit, Senior Liberal will retire from SA politics and force by-election: ABC 6 June 2014]</ref><ref>[http://www.abc.net.au/news/2014-10-30/departing-liberal-iain-evans-takes-final-swipe/5855740 Departing SA Liberal Iain Evans takes final swipe at parliamentary colleagues: ABC 30 October 2014]</ref>

==References==
{{reflist|2}}

==External links==
*[http://www2.parliament.sa.gov.au/Internet/DesktopModules/memberdrill.aspx?pid=563 Parliamentary Profile: SA Parliament website]
*[http://www.abc.net.au/stateline/sa/content/2006/s1602037.htm Stateline (ABC TV) 24-Mar-2006: Can Liberals heal rifts?]

{{s-start}}
{{s-off}}
{{s-bef|before=[[Rob Kerin]]}}
{{s-ttl|title=[[Leader of the Opposition (South Australia)|Leader of the Opposition]] in South Australia|years=2006{{spaced ndash}}2007}}
{{s-aft|after= [[Martin Hamilton-Smith]]}}
{{s-bef|before=[[Dean Brown]]}}
{{s-ttl|title=Deputy [[Leader of the Opposition (South Australia)|Leader of the Opposition]] in South Australia|years=2005{{spaced ndash}}2006}}
{{s-aft|after= [[Vickie Chapman]]}}
|-
{{s-par|au-sa}}
{{s-bef|before=[[Stan Evans]]}}
{{s-ttl | title=Member for [[Electoral district of Davenport|Davenport]] | years=1993–2014}}
{{s-aft|after= [[Sam Duluk]]}}
|-
{{s-ppo}}
{{s-bef|before=[[Rob Kerin]]}}
{{s-ttl|title=Leader of the [[Liberal Party of Australia (South Australian Division)]]|years=2006{{spaced ndash}}2007}}
{{s-aft|after= [[Martin Hamilton-Smith]]}}
{{s-end}}

{{Leaders of the Liberal Party of Australia (SA division)}}
{{SACurrentMHAs}}

{{Authority control}}

{{DEFAULTSORT:Evans, Iain}}
[[Category:1959 births]]
[[Category:Living people]]
[[Category:Liberal Party of Australia members of the Parliament of South Australia]]
[[Category:Members of the South Australian House of Assembly]]
[[Category:University of South Australia alumni]]
[[Category:Leaders of the Opposition in South Australia]]
[[Category:21st-century Australian politicians]]