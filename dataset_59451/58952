{{Infobox journal
| title = Modern Physics Letters B
| cover = [[image:MPLBcover.jpg|200px]]
| discipline = [[Physics]]
| abbreviation = Mod. Phys. Lett. B
| publisher = [[World Scientific]]
| country =
| frequency = 32/year
| history = 1987-present
| impact = 0.687
| impact-year = 2013
| website = http://www.worldscientific.com/worldscinet/mplb
| link2 = http://www.worldscientific.com/loi/mplb
| link2-name = Online archive
| ISSN = 0217-9849
| eISSN = 1793-6640
| OCLC = 49229274
| LCCN = 88640639
}}
'''''Modern Physics Letters B''''' is a [[peer-reviewed]] [[scientific journal]] on [[physics]], especially the areas of condensed matter, statistical and applied physics, and high-Tc superconductivity. It was established in 1987 and is published by [[World Scientific]].

== Related journals ==
* ''[[Modern Physics Letters A]]''

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Science Citation Index]]
* [[Materials Science Citation Index]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[Astrophysics Data System]]
* [[Mathematical Reviews]]
* [[Inspec]]
* CSA Meteorological & Geoastrophysical Abstracts
* [[Scopus]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.687.<ref name=WoS>{{cite book |year=2014 |chapter=Modern Physics Letters B |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.worldscientific.com/worldscinet/mplb}}

[[Category:World Scientific academic journals]]
[[Category:Physics journals]]
[[Category:Publications established in 1987]]
[[Category:English-language journals]]