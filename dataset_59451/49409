[[File:Euan London Skyline.png|alt=Photo of Euan MacDonald.|thumb|Euan MacDonald MBE, co-founder of Euan's Guide, the disabled access review website and app.]]
Euan MacDonald MBE is a Scottish entrepreneur. He studied at [[St Andrews University]] and [[Edinburgh University]].<ref>{{cite web|title=Interview with Euan|url=http://www.euansguide.com/about-us/interview-with-euan/|website=Euan's Guide|accessdate=30 January 2015}}</ref> MacDonald was diagnosed with [[motor neurone disease|Motor Neurone Disease]](MND) in October 2003.<ref>{{cite web|last1=Donnelly|first1=Brian|title=Hotel chain's founder gives cash for motor neurone centre|url=http://www.heraldscotland.com/hotel-chain-s-founder-gives-cash-for-motor-neurone-centre-1.860435|publisher=The Hearld|accessdate=30 January 2015}}</ref>

== Biography ==
MacDonald established [[the Euan MacDonald Centre]] for Motor Neurone Disease Research in 2007 in partnership with the University of Edinburgh.<ref>{{cite web|last1=Swanson|first1=Brian|title=Businessman's plight to save his son|url=http://www.express.co.uk/news/uk/11393/Businessman-s-plight-to-save-his-son|publisher=The Express|accessdate=30 January 2015}}</ref> With the Informatics Department at the University of Edinburgh, MacDonald also helped establish The Voicebank Study which enables people who are at risk of losing their voice through illness to preserve it.<ref>{{cite web|title=Voicebank wants more from lads…|website=http://www.mndscotland.org.uk/2011/12/voicebank-wants-more-from-lads/|publisher=MND Scotland|accessdate=30 January 2015}}</ref> In 2009, he was awarded an [[Order of the British Empire|MBE]] in the Queen's [[Birthday Honours]] list in recognition of his contribution to services for people with motor neurone disease.<ref>{{cite web|title=Queen's birthday honours list: MBEs|url=https://www.theguardian.com/uk/2009/jun/13/queens-birthday-honours-list|publisher=The Guardian|accessdate=30 January 2015}}</ref>

In 2013, MacDonald co-founded the disabled access review website and app, [[Euan's Guide]].<ref>{{cite web|last1=Tracey|first1=Emma|title=Family support disabled man with accessibility guide|url=http://www.bbc.co.uk/news/blogs-ouch-28130785|publisher=BBC|accessdate=30 January 2015}}</ref>

In November 2014, MacDonald was named as one of the most influential disabled people in the UK.<ref>{{cite web|title=Britain's most influential people with a disability or impairment|url=http://edition.pagesuite-professional.co.uk/Launch.aspx?EID=969986fe-a9d1-47ff-a1a7-2b059410a43e|publisher=Shaw Trust|accessdate=30 January 2015}}</ref> MacDonald lives in Edinburgh with his wife and his two children.<ref>{{cite web|title=MND sufferer raises wheelchair consumer issues|url=http://www.edinburghnews.scotsman.com/news/health/mnd-sufferer-raises-wheelchair-consumer-issues-1-3284004|publisher=Edinburgh News|accessdate=30 January 2015}}</ref>

== Euan's Guide ==
[[Euan's Guide|Euan’s Guide]] is the disabled access review website and app where people can review the disabled access of all kinds of places from post offices and shops, to restaurants, hotels and more.

== Awards and Shortlists ==
[https://www.pointsoflight.gov.uk/promoting-disabled-access/ Points of Light Award]

The Power 100 List 2016

Commended in the Unsung Hero category at the Herald Society Awards 2015

The Power 100 List 2015

MBE for services to people with MND in Scotland, [https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/416586/BD_2009.csv/preview Birthday Honours 2009]

==References==
{{reflist}}

{{DEFAULTSORT:MacDonald, Euan}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Alumni of the University of St Andrews]]
[[Category:21st-century Scottish businesspeople]]
[[Category:People with motor neurone disease]]