{{Infobox journal
| title = Brain Research Bulletin
| cover = [[File:BRBcover.gif]]
| editor = Andres Buonanno
| discipline = [[Neuroscience]]
| former_names =
| abbreviation = Brain Res. Bull.
| publisher = [[Elsevier]]
| country =
| frequency = 18/year
| history = 1976–present
| openaccess =
| license =
| impact = 2.498
| impact-year = 2010
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/525456/description#description
| link1 = http://www.sciencedirect.com/science/journal/03619230
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 02134161
| LCCN = 76644756
| CODEN = BRBUDU
| ISSN = 0361-9230
| eISSN = 1873-2747
}}
'''''Brain Research Bulletin''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] of [[neuroscience]]. It was established in 1976 with [[Matthew J. Wayner]] (then at [[Syracuse University]]) as founding [[editor in chief]]. Later it was edited by [[Stephen Dunnett]] ([[Cardiff University]]). He was succeeded in 2010 by Andres Buonanno ([[National Institute of Child Health and Human Development]]). It is published 18 times per year by [[Elsevier]].
It was an official journal of the [[International Behavioral Neuroscience Society]].<ref>{{cite book |title=Companion to Clinical Neurology |first1=William |last1=Pryse-Phillips |publisher=Oxford University Press |year=2009 |url=https://books.google.com.br/books?id=mBGB7FOFJMoC&lpg=PA131&ots=j6TkAt00WB&dq=%22Brain%20Research%20Bulletin%22%20%22International%20Behavioral%20Neuroscience%20Society%22&pg=PA131#v=onepage&q=%22Brain%20Research%20Bulletin%22%20%22International%20Behavioral%20Neuroscience%20Society%22&f=false}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[BIOSIS Previews]], [[Current Contents]]/Life Sciences, [[EMBASE]], [[Elsevier BIOBASE|BIOBASE]], [[MEDLINE]], [[PsycINFO]], [[PsycLIT]], [[Science Citation Index]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', it has a 2010 [[impact factor]] of 2.498, ranking it 137th among 237 journals in the category "Neuroscience".<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2011-06-30}}</ref>

== References ==
{{Reflist}}

==External links ==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/525456/description#description}}

[[Category:Neuroscience journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1976]]