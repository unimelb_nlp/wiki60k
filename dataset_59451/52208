{{Infobox journal
| cover = [[File:MRE Cover5609.jpg]]
| discipline = [[Polymer science|Polymer reaction engineering]]
| abbreviation = Macromol. React. Eng.
| editor = Stefan Spiegel
| publisher = [[John Wiley & Sons|Wiley-VCH]]
| country =
| history = 2007-present
| frequency = Monthly
| impact = 1.543
| impact-year = 2013
| website = http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291862-8338
| link1 = http://onlinelibrary.wiley.com/doi/10.1002/mren.v4:8/issuetoc
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291862-8338/issues
| link2-name = Online archive
| ISSN = 1862-832X
| eISSN = 1862-8338
| OCLC = 80014454
}}
'''''Macromolecular Reaction Engineering''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published monthly by [[John Wiley & Sons|Wiley-VCH]]. The journal covers academic and industrial research in the field of polymer reaction engineering, which includes [[polymer science]]. It emerged from a section that was part of ''[[Macromolecular Materials and Engineering]]''. The journal publishes reviews, feature articles, communications, and full papers in the entire field of [[polymer]] reaction engineering, including polymer reaction modeling, reactor optimization, and control. Its 2013 [[impact factor]] is 1.543.

The journal also produces special issues. The 2009 and 2010 topics included "New Frontiers in Polymer Engineering" and "Controlled Radical Polymerization".<ref name=home>''Macromolecular Reaction Engineering.'' [http://www.wiley-vch.de/publish/en/journals/alphabeticIndex/2465/ Home Page]. Wiley-VCH, July 2010.</ref><ref name=special>{{Cite journal
  | editor = Jose R. Leiza, Diana A. Estenoz
  | title = Special Issue: New Frontiers in Polymer Engineering
  | journal = Macromolecular Reaction Engineering
  | volume = 4
  | issue = 6-7
  | publisher = Wiley-VCH 
  | date = June 25, 2010 
  | url = http://onlinelibrary.wiley.com/doi/10.1002/mren.v4:6/7/issuetoc
  | doi=10.1002/mren.v4:6}}</ref>

==Aims and scope==
''Macromolecular Reaction Engineering'' is intended for polymer scientists, [[chemist]]s, [[list of physicists|physicists]], [[materials science|materials scientists]], [[theoreticians]], and [[chemical engineer]]s. The journal covers recent and significant results of academic and industrial research in the field of interest, encompassing all related topics - this includes polymer reaction modeling, reactor optimization and control,  [[polyolefin]]s, [[polymer production]], [[sensor]]s, [[process control]], [[polymer]]s, [[macromolecular materials]], polymer reaction engineering, modelling, [[reactor optimization]], [[polymeric materials]], and [[polymer engineering]].<ref name=overview>{{Cite web
  | title = Overview, Aims & scope, Abstracting and indexing 
  | publisher = Wiley-VCH 
  | date = July 2010 
  | url = http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291862-8338/homepage/ProductInformation.html
  | accessdate =2010-08-12}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in [[Chemical Abstracts Service]], [[Science Citation Index|Chemistry Citation Index]], [[Compendex]], [[Current Contents]]/Engineering, Computing & Technology, Current Contents/Physical, Chemical & Earth Sciences, [[Inspec]], [[Journal Citation Reports]]/Science Edition, [[Materials Science Citation Index]], and the [[Science Citation Index|Science Citation Index Expanded]].<ref name=overview/>

== References ==
{{Reflist}}

== External links ==
{{Official website|http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291862-8338}}

[[Category:Chemistry journals]]
[[Category:Materials science journals]]
[[Category:Publications established in 2007]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]