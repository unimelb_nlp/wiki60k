{{Infobox journal
| title        = Nuclear Physics
| cover        =
| editor       =
| discipline   = [[Nuclear physics]]
| abbreviation = Nucl. Phys.<br>Nucl. Phys. A<br>Nucl. Phys. B<br>{{nowrap|Nucl. Phys. B (Proc. Suppl.)}}
| publisher    = [[Elsevier]]
| country =
| frequency    =
| history      = 1956–present
| impact       = 1.986  (''Nucl. Phys. A'')<br>4.642 (''Nucl. Phys. B'')
| impact-year  = 2010
| openaccess   =
| website      = 
| ISSN        = 0375-9474
| ISSNlabel    = ''Nucl. Phys. A''
| ISSN2        = 0550-3213
| ISSN2label   = ''Nucl. Phys. B''
| ISSN3        = 0920-5632
| ISSN3label   = ''Nucl. Phys. B (Proc. Suppl.)''
}}

'''''Nuclear Physics''''' is a [[peer-reviewed]] [[scientific journal]] published by [[Elsevier]]. The journal was established in 1956, and then split into '''''Nuclear Physics A''''' and '''''Nuclear Physics B''''' in 1967.<ref>{{cite web|url=http://www.sciencedirect.com/science/journal/00295582 |title=Nuclear Physics |publisher=ScienceDirect.com |date= |accessdate=2015-02-17}}</ref> A supplement series to ''Nuclear Physics B'', called '''''Nuclear Physics B: Proceedings Supplements''''' has been published from 1987 onwards.<ref>{{cite web|url=http://www.sciencedirect.com/science/journal/09205632 |title=Nuclear Physics B - Proceedings Supplements |publisher=ScienceDirect.com |date=2014-03-22 |accessdate=2015-02-17}}</ref>

''Nuclear Physics B'' is part of the [[SCOAP3|SCOAP<sup>3</sup>]] initiative.<ref>
{{cite web
 |title=SCOAP<sup>3</sup> Journals: APCs and articles
 |url=http://scoap3.org/scoap3journals/journals-apc
 |publisher=[[SCOAP3|SCOAP<sup>3</sup>]]
 |accessdate=2014-09-15
}}</ref>

==Abstracting and indexing==

===''Nuclear Physics A''===
* [[Current Contents]]/Physics, Chemical, & Earth Sciences
* [[Scopus]]
* [[Zentralblatt MATH]]

===''Nuclear Physics B''===
* [[Chemical Abstracts]]
* Current Contents/Physics, Chemical, & Earth Sciences
* Scopus
* Zentralblatt MATH

==References==
{{Reflist}}

==External links==
* [http://www.sciencedirect.com/science/journal/00295582 ''Nuclear Physics'']
* [http://www.elsevier.com/wps/find/journaldescription.cws_home/505715/description ''Nuclear Physics A'']
* [http://www.elsevier.com/wps/find/journaldescription.cws_home/505716/description ''Nuclear Physics B'']
* [http://www.elsevier.com/wps/find/journaldescription.cws_home/505717/description ''Nuclear Physics B: Proceedings Supplements'']

[[Category:Elsevier academic journals]]
[[Category:Nuclear physics journals]]
[[Category:Publications established in 1956]]
[[Category:English-language journals]]


{{physics-journal-stub}}