{{Other uses|Botanical garden (disambiguation)}}
{{italic title}}
[[File:DarwinBotanicGardenTitle.jpg|thumb|right|Title page from ''The Botanic Garden'' (1791)]]

'''''The Botanic Garden''''' (1791) is a set of two poems, ''The Economy of Vegetation'' and ''The Loves of the Plants'', by the British poet and [[naturalist]] [[Erasmus Darwin]]. ''The Economy of Vegetation'' celebrates technological innovation, scientific discovery and offers theories concerning contemporary scientific questions, such as the [[Timeline of cosmology|history of the cosmos]]. The more popular ''Loves of the Plants'' promotes, revises and illustrates [[Linnaeus|Linnaeus's]] [[Linnaean taxonomy|classification scheme]] for plants.

One of the first popular science books, the intent of ''The Botanic Garden'' is to pique readers' interest in science while educating them at the same time. By embracing Linnaeus's sexualized language, which [[wikt:anthropomorphizes|anthropomorphizes]] plants, Darwin makes [[botany]] interesting and relevant to his readers, but his reliance on conventional images of women when describing plants and flowers reinforces traditional gender stereotypes.  Darwin emphasizes the connections between humanity and plants, arguing that they are all part of the same natural world and that sexual reproduction is at the heart of [[evolution]] (ideas that his grandson, [[Charles Darwin]], would later turn into a full-fledged theory of evolution). This evolutionary theme continues in ''The Economy of Vegetation'' which contends that scientific progress is part of evolution and urges its readers to celebrate inventors and scientific discoveries in a language usually reserved for heroes or artistic geniuses.

Because amateur botany was popular in Britain during the second half of the eighteenth century, ''The Botanic Garden'', despite its high cost, was a bestseller. Nevertheless, the poem's radical political elements, such as its support of the [[French revolution]] and its criticism of [[slavery]], angered conservative British readers.

Darwin's attempt to popularize science and to convey the wonders of scientific discovery and technological innovation through poetry helped initiate a tradition of popular science writing that continues to the present day.

==Historical background==
[[File:Portrait of Erasmus Darwin by Joseph Wright of Derby (1792).jpg|alt=Painting of a man wearing 18th century clothes and holding a quill pen.|thumb|right|[[Erasmus Darwin]] in 1792 by [[Joseph Wright of Derby]]]]
In the 1760s and 1770s, [[Botany#History|botany]] became increasingly popular in Britain because of the translation of [[Carl Linnaeus#Bibliography|Linnaeus's works]] into English. One of the most prominent books about botany was [[William Withering|William Withering's]] ''Botanical Arrangement of all the Vegetables Naturally Growing in Great Britain'' (1776), which used Linnaeus's system for classifying plants. Withering's book went through multiple editions and became the standard text on British plants for a generation. The book delighted and intrigued experts, amateurs, and children alike.<ref name=Shteir>Shteir, pp. 18—28.</ref>

One of the effects of Withering's book was that it provoked a debate over the translation of Linnaeus's works. Withering aimed for an Anglicized translation of Linnaeus's Latin that also stripped the nomenclature of its sexualized language. Although he wanted to make botany widely available, he believed that women readers should be protected from any mention of sexuality.<ref name=Shteir/> In his preface he writes: "from an apprehension that botany in an English dress would become a favourite amusement with the ladies, . . . it was thought proper to drop the sexual distinctions in the titles to the Classes and Orders."<ref>Quoted in Shteir, p. 23.</ref>

[[Erasmus Darwin|Darwin]] held the opposite position; he maintained that Linnaeus's works should be translated as literally as possible and that the sexual references in the nomenclature should be retained. In 1783 and 1787, the [[Botanical Society of Lichfield]], founded by Darwin and several of his friends specifically to translate Linnaeus's works, issued their own English translation, ''A System of Vegetables'', that categorized over 1400 plants. Assisted by [[Samuel Johnson]], they coined over fifty new botanical words; it is this work, along with the group's ''The Families of Plants'' that introduced the words ''[[stamen]]'' and ''[[pistil]]'' into the English language, for example. By 1796 their translation had prevailed and Withering was forced to adopt their vocabulary in later editions of his work.<ref>Shteir, pp. 18—28; Browne, pp. 600—602.</ref>

===Linnaean system===
The reliability and usefulness of the Linnaean system was a subject of much debate when Darwin was composing ''The Loves of the Plants'', leading scholars to conclude that one of his intentions in publishing the poem was to defend the [[Linnaean taxonomy|Linnaean classification scheme]]. Linnaeus had proposed that, like humans, plants are male and female and reproduce sexually; he also described his system using highly sexualized language. Therefore, as scholar Janet Browne writes, “to be a Linnaean taxonomist was to believe in the sex life of flowers.”<ref>Browne, p. 597.</ref> In his poem, Darwin not only embraced Linnaeus's classification scheme but also his metaphors. At the same time that he was defending Linnaeus's system, however, Darwin was also refining it. Linnaeus classified plants solely on the number of reproductive organs they had, but Darwin's poem also emphasized “proportion, length, and arrangement of the [sexual] organs”.<ref>Browne, pp. 594; 596—602.</ref>

==Writing and publication==
[[File:DarwinGardenFrontispiece.jpg|alt=Front piece shows a Grecian woman lounging with nymphs about her, one of them holding a mirror.|thumb|left|The frontispiece to ''The Botanic Garden'', designed by [[Henry Fuseli]]]]
Inspired by his enjoyment of his own [[botanical garden]] but primarily by [[Anna Seward|Anna Seward's]] poem “Verses Written in Dr. Darwin's Botanic Garden” (1778), Darwin decided to compose a poem that would embody Linnaeus's ideas.<ref>Browne, pp. 599—601; Coffey, 143.</ref> (Darwin would later include an edited version of Seward's poem in ''The Loves of the Plants'' without her permission and without acknowledgment. Seward was rankled by this treatment and complained of Darwin's inattention to her authorial rights in her ''Memoirs of Erasmus Darwin''.<ref>Coffey, 143.</ref>) According to Seward, Darwin said that “the Linnean System is unexplored poetic ground, and an [sic] happy subject for the muse. It affords fine scope for poetic landscape; it suggests metamorphoses of the Ovidian kind, though reversed.”<ref>Quoted in Browne, p. 601.</ref> Darwin may have also thought of ''The Love of the Plants'' “as a kind of love song” to Elizabeth Pole, a woman with whom he was in love and would eventually marry.<ref>Browne, p. 608; Shteir, p. 242, n. 25.</ref> Concerned about his scientific reputation and curious to see if there would be an audience for his more demanding poem ''The Economy of Vegetation'', he published ''The Loves of the Plants'' anonymously in 1789 (see [[1789 in poetry]]). He was stunned at its success and therefore published both ''Loves of the Plants'' and ''Economy of Vegetation'' together as ''The Botanic Garden'' in [[1791 in poetry|1791]]. [[Joseph Johnson (publisher)|Joseph Johnson]], his publisher, eventually bought the copyright for ''The Botanic Garden'' from him for the staggering sum of [[pound sterling|₤]]800.<ref>Coffey, p. 144; Browne, p. 595.</ref>

When Johnson published ''The Botanic Garden'' in 1791, he charged twenty-one [[shillings]] for it, a hefty price at the time. Seward wrote that "the immense price which the bookseller gave for this work, was doubtless owing to considerations which inspired his trust in its popularity. Botany was, at that time, and still continues a very fashionable study."<ref>Quoted in Shteir, p. 26.</ref> However, the high price would also have discouraged government prosecution for a book that contained radical political views. Any subversive ideas that the poem contained were therefore limited to an audience of the educated elite who could afford to purchase the book.<ref>Shteir, pp. 18—28; Teute, p. 330.</ref>

==''The Loves of the Plants''==

===Structure and poetic style===
Suggesting the passing of a single day, ''The Loves of the Plants'' is divided into four [[Poetry#Form|canto]]s, all written in [[heroic couplet]]s. A preface to the poem outlines the basics of the Linnaean classification system. Guiding the reader through the garden is a “Botanic Muse” who is described as Linnaeus's inspiration. Interspersed between the cantos are dialogues on poetic theory between the poet and his bookseller. The poem is not a narrative; instead, reminiscent of the [[picaresque]] tradition, it consists of discrete descriptions of eighty-three separate species which are accompanied by extensive explanatory footnotes.<ref>Browne, pp. 604—606; 616; Page, p. 149; Shteir, pp. 22—28.</ref>

In ''The Loves of the Plants'', Darwin claims "to Inlist Imagination under the banner of Science". A believer in [[Age of Enlightenment|Enlightenment]] ideals, he wanted not only to participate in scientific discovery but also to disseminate its new knowledge in an accessible format. As Darwin scholar Michael Page has written, “Darwin sought to do for Linnaeus . . . what [[Alexander Pope|Pope]] had done for [[Isaac Newton|Newton]] and celestial mechanics in the ''[[Essay on Man]]''”<ref>Page, p. 150.</ref>

====Personification====
In one of the interludes of ''The Loves of the Plants'', the voice of the Poet, which would seem to be Darwin's voice as well, argues that poetry is meant to appeal to the senses, particularly vision. Darwin's primary tool for accomplishing this was [[personification]]. Darwin's personifications were often based on the classical allusions embedded with Linnaeus's own naming system. However, they were not meant to conjure up images of gods or heroes; rather, the anthropomorphized images of the plants depict more ordinary images. They also stimulate the readers' imaginations to assist them in learning the material and allow Darwin to argue that the plants he is discussing are animate, living things—just like humans. Darwin's use of personification suggests that plants are more akin to humans than the reader might at first assume; his emphasis on the continuities between mankind and plantkind contributes to the evolutionary theme that runs throughout the poem.<ref>Browne, pp. 606—607; 615; Packham, pp. 197—198.</ref>

''The Loves of the Plants'' argues that human emotion is rooted in physiology rather than Christian theology. Darwin would take his [[materialism]] even further in ''The Economy of Vegetation'' and ''[[The Temple of Nature]]'', works that have been called [[atheistic]]. In describing plants through the language of love and sex, Darwin hoped to convey the idea that humans and human sexuality are simply another part of the natural world. Darwin writes that his poem will reverse [[Ovid]] who “did, by art poetic, transmute Men, Women, and even Gods and Goddesses, into trees and Flowers; I have undertaken, by similar art, to restore some of them to their original animality”<ref>Quoted in Teute, p. 325; Browne, p. 614; Teute, p. 323.</ref>

===Themes===
[[File:DarwinGardenAmaryllis.jpg|alt=Engraving of a stalk with a flower on the end.|right|thumb|[[William Blake|William Blake's]] engraving of ''[[Amaryllis]]''<!--is this the right link?-->
<poem>
"When heaven's high vault condensing clouds deform,
Fair Amaryllis flies the incumbent storm,
Seeks with unsteady step the shelter'd vale,
And turns her blushing beauties from the gale.
''Six'' rival youths, with soft concern impress'd,
Calm all her fears, and charm her cares to rest." (I.151-156)
</poem>]]

====Evolution====
In his ''Phytologia'' (1800), Darwin wrote “from the sexual, or amatorial generation of plants new varieties, or improvements, are frequently obtained”.<ref>Quoted in Browne, p. 603, n. 35.</ref> He insisted in ''The Loves of the Plants'' that sexual reproduction was at the heart of evolutionary change and progress, in humans as well as plants.<ref>Browne, p. 603; Page, p. 151.</ref> Browne writes that the poem may be seen as "an early study in what was to be Darwin's lifelong commitment to the idea of transmutation.”<ref>Browne, p. 604.</ref> Darwin illustrated not only organic change, but social and political change as well. Throughout ''The Botanic Garden'', Darwin endorses the ideals of the [[American revolution|American]] and [[French revolution]]s and criticizes [[slavery]]. His celebration of technological progress in ''The Economy of Vegetation'' suggests that social and scientific progress are part of a single evolutionary process. Humanity was improving, moving towards perfection, as evidenced by [[abolitionism]] and the broadening of political rights.<ref>Coffey, p. 162.</ref>

====Gender====
''The Love of the Plants'', however, while opening up the world of botany to the non-specialist and to women in particular, reinforced conventional gender stereotypes. Darwin's images “remained deeply polarized between the chaste, blushing virgin and the seductive predatory woman, the modest shepherdess and the powerful queen.”<ref>Browne, p. 618; Shteir, p. 27; Coffey, p. 149.</ref>

Although Darwin gives plant-women the central role in each vignette (a reversal of Linnaeus's classification scheme, which focuses on the male), few of the representations stray from stereotypical images of women. When the female and male reproductive organs are in a 1:1 ratio in a plant, Darwin represents traditional couplings. The women are “playful”, “chaste”, “gentle” and “blooming”. When the ratio is 1:2-4, the female becomes a “helpmate” or “associate” to the males, who have separate bonds to their “brothers”. Once he reaches 1:5-6, however, Darwin presents women as “seductive or wanton” or, at the other extreme, “needing protection”. By 1:8+, he presents “unambiguous metaphors of power and command, [with the woman] being pictured as a saint, a reigning sovereign, a sorceress, a proto-industrialist . . . a priestess”.<ref>Browne, pp. 611—612.</ref> The images also present a largely positive view of the relationship between the sexes; there is no rape or sexual violence of any kind, elements central to much of Ovid and Linnaeus. There is also no representation of the marriage market, divorce or adultery (with one exception); the poem is largely [[pastoral poetry|pastoral]]. There are also no representations of intelligent women or women writers, although Darwin knew quite a few. The exception is the “Botanic Muse”, who has the botanical knowledge that the poem imparts; however, as Browne argues, few readers in the eighteenth century would have seen this as a liberating image for women since they would have been skeptical that a woman could have written the poem and inhabited the voice of the muse (they would have assumed that the anonymous writer was a man).<ref>Browne, pp. 607; 611; 617; Coffey, p. 149.</ref>

Despite its traditional gender associations, some scholars have argued that the poem provides “both a language and models for critiquing sexual mores and social institutions” and encourages women to engage in scientific pursuits.<ref>Teute, p. 327.</ref>

==''The Economy of Vegetation''==
While ''The Loves of the Plants'' celebrates the natural world, ''The Economy of Vegetation'' celebrates scientific progress and technological innovation, such as the forging of [[steel]], the invention of the [[steam engine]] and the improvements to [[gunpowder]]. It also advances several scientific hypotheses regarding the formation of the cosmos, the moon and the earth. Moreover, Darwin's poem represents the scientists and inventors, such as [[Benjamin Franklin]], responsible for this progress as the heroes of a new age; he “mythologizes” them.<ref>Page, p. 152.</ref> Although the two poems seem separated, they both endorse an evolutionary view of the world. Darwin did not see a distinction between nature and culture; industrialization and technological progress were part of a single evolutionary process.<ref>Coffey, pp. 142; 145—147; Page, p. 163.</ref>

Much of ''The Economy of Vegetation'' deals with mining and the use of minerals.<ref>Coffey, p. 159.</ref> For example, Darwin describes the great mining capability of the steam engine:
<blockquote>
<poem>
The Giant-Power [that] forms earth's remotest caves
Lifts with strong arm her dark reluctant waves;
Each cavern'd rock, and hidden den explores,
Drags her dark Coals, and digs her shining ores.<ref>Quoted in Coffey, 160.</ref>
</poem>
</blockquote>
As such examples demonstrate, ''The Economy of Vegetation'' is part of an [[Age of Enlightenment|Enlightenment]] paradigm of progress while ''The Loves of the Plants'', with its focus on an integrated natural world, is more of an early [[Romanticism|Romantic]] work.<ref>Coffey, p. 146.</ref>

Darwin also connected scientific progress to political progress; “for Darwin the spread of revolution meant that reason and equity vanquished political tyranny and religious superstition.”<ref>Teute, p. 335.</ref> Criticizing British imperialism and slavery, he writes:
[[File:BLAKE10.JPG|alt=Man in chains on one knee, pleading.|thumb|left|Medallion of the British Anti-Slavery Society, designed by Darwin's friend and fellow [[Lunar Society]] member, [[Josiah Wedgwood]]]]
<blockquote>
<poem>
When Avarice, shrouded in Religion's robe,
Sail'd to the West, and slaughter'd half the globe:
While Superstition, stalking by his side,
Mock'd the loud groan, and lap'd the bloody tide;
For sacred truths announced her frenzied dreams,
And turn'd to night the sun's meridian beams.—
Hear, Oh Britannia! potent Queen of isles,
On whom fair Art, and meek Religion smiles,
Now Afric's coasts thy craftier sons invade,
And Theft and Murder take the garb of Trade!
—The Slave, in chains, on supplicating knee,
Spreads his wide arms, and lifts his eyes to Thee;
With hunger pale, with wounds and toil oppress'd,
'Are we not Brethren?' sorrow choaks the rest;
—Air! bear to heaven upon thy azure flood
Their innocent cries!--Earth! cover not their blood! (I.ii.414-430)
</poem>
</blockquote>

==Reception and legacy==
''The Botanic Garden'' was reissued repeatedly in Britain, Ireland and the United States throughout the 1790s.  Until the publication of [[William Wordsworth]] and [[Samuel Taylor Coleridge|Samuel Taylor Coleridge's]] ''[[Lyrical Ballads]]'' in 1798, Darwin was considered one of England's preeminent poets. His poems, with their “dynamic vision of change and transformation”, resonated with the ideals of the [[French revolution]].<ref>Page, p. 151; Teute, 332.</ref> However, when the revolution entered its more radical and bloody phase, scientific progress became associated with what many started to see as a failed revolution. [[Anti-Jacobin]]s, who were opposed to the French revolution, denounced the sexual freedom gaining ground in France and linked it to the scientific projects of men like Darwin. George Canning and John Frere published a parody of ''The Loves of the Plants'' in the ''[[Anti-Jacobin Review]]'' in [[1798 in poetry|1798]] titled “Loves of the Triangles”, suggesting just these connections.<ref>Teute, pp. 337—338; Coffey, p. 162.</ref>

Darwin's poems were not published during the first two decades of the nineteenth century as conservative reaction solidified in Britain,<ref>Teute, p. 342.</ref> although [[bowdlerized]] and [[sentimentality|sentimentalized]] poems imitating Darwin's became increasingly popular.  The analogy between plants and humans lasted well into the nineteenth century; ''[[Alice in Wonderland]]'' was one of the many books to employ the image.<ref>Browne, p. 620.</ref>

Darwin's unique poetic style impressed some while it revolted others. Wordsworth called it “dazzling" while Coleridge said, “I absolutely nauseate Darwin's poem”.<ref>Quoted in Browne, p. 604.</ref> Darwin's “visionary temperament” is similar in some ways to modern popular science writing.<ref>Page, p. 148.</ref>
{{clear}}

==See also==
*''[[The Unsex'd Females]]''
*[[Transmutation of species]]
*[[History of evolutionary thought]]

==Works cited==
*{{cite journal |author=Browne, J. |year=1989 |title=Botany for Gentlemen: Erasmus Darwin and "The Loves of the Plants" |journal=Isis |volume=80 |issue=4 |pages=592–621 |jstor=234174 |doi=10.2307/234174}}
*Coffey, Donna (2002). "Protecting the Botanic Garden: Seward, Darwin, and Coalbrookdale." ''Women's Studies'' 31: 141—164.
*Packham, Catherine (2004). "The Science and Poetry of Animation: Personification, Analogy, and Erasmus Darwin's ''Loves of the Plants''." ''Romanticism'' 10.2: 191—208.
*Page, Michael (2005). "The Darwin Before Darwin: Erasmus Darwin, Visionary Science, and Romantic Poetry." ''Papers on Language and Literature'' 41.2: 146—169.
*{{cite book|last1=Shteir|first1=Ann B.|title=Cultivating women, cultivating science : Flora's daughters and botany in England, 1760-1860|date=1996|publisher=Johns Hopkins University Press|location=Baltimore|isbn=0-8018-6175-6|url=https://books.google.ca/books?id=TtzaAAAAMAAJ|accessdate=18 February 2015}}
*Teute, Fredrika J.(2000). "The Loves of the Plants; or, the Cross-Fertilization of Science and Desire at the End of the Eighteenth Century." ''The Huntington Library Quarterly'' 63.3: 319—345.

==Notes==
{{reflist|2}}

==Further reading==
*Anonymous. "Loves of the Triangles." ''Anti-Jacobin; or, Weekly Examiner'' (16 April, 23 April, and 7 May 1798).
*Bewell, Alan. "'Jacobin Plants': Botany as Social Theory in the 1790s." ''Wordsworth Circle'' 20 (1989): 132-39.
*King-Hele, Desmond. ''Doctor of Revolution: The Life and Genius of Erasmus Darwin''. London: Faber and Faber, 1977. ISBN 0-571-10781-8.
*King-Hele, Desmond. ''Erasmus Darwin and the Romantic Poets''. London: Macmillan, 1986. ISBN 0-312-25796-1.
*Kilngender, Francis D. ''Art and the Industrial Revolution''. Chatham: Evans, Adams & Mackay, 1968. 
*Logan, James Venable. "The Poetry and Aesthetics of Erasmus Darwin." ''Princeton Studies in English'' 15 (1936): 46-92.
*McGann, Jerome. ''The Poetics of Sensibility: A Revolution in Literary Style''. Oxford: Oxford University Press, 1996. ISBN 0-19-818370-4.
*McNeil, Maureen. ''Under the Banner of Science: Erasmus Darwin and His Age''. Manchester: Manchester University Press, 1987. ISBN 0-7190-1492-1.
*McNeil, Maureen. "The Scientific Muse: The Poetry of Erasmus Darwin." ''Languages of Nature: Critical Essays on Science and Literature''. Ed. [[Ludmilla Jordanova|L. J. Jordanova]]. London: [[Free Association Books]], 1986. ISBN 0-946960-36-4.
*Schiebinger, Londa. "The Private Life of Plants: Sexual Politics in Carl Linnaeus and Erasmus Darwin." ''Science and Sensibility: Gender and Scientific Enquiry, 1780–1945''. Ed. Marina Benjamin. Oxford: Blackwell, 1991. ISBN 0-631-16649-1.
*Schiebinger, Londa. ''Nature's Body: Gender in the Making of Modern Science''. Boston: Beacon Press, 1993. ISBN 0-8070-8900-1.
*[[Anna Seward|Seward, Anna]]. ''Memoirs of the Life of Dr. Darwin''. Philadelphia: Wm. Poyntell, 1804.
*[[Robert Southey|Southey, Robert]]. "Review of Chalmers's ''English Poets''. ''Quarterly Review'' 12 (1814): 60-90.

==External links==
{{Commons category}}
*[https://books.google.com/books?id=Tn8gAAAAMAAJ&pg=PA6&dq=botanic+garden+darwin&as_brr=1#PPA1,M1 Full text of ''The Botanic Garden''] at [[google books]]
*[http://www.gutenberg.org/browse/authors/d#a3099 Full text of ''The Botanic Garden''] at [[Project Gutenberg]]

{{good article}}

{{DEFAULTSORT:Botanic Garden, The}}
[[Category:British poems]]
[[Category:18th-century books]]
[[Category:1791 books]]
[[Category:1791 in science]]