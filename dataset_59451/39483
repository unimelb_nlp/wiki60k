<!-- FAIR USE of The Europeans.JPG: see image description page at http://en.wikipedia.org/wiki/Image:The Europeans.JPG for rationale --> 
: ''This page is about the book. For other entries on The Europeans, see [[The Europeans (disambiguation)]].''

{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = The Europeans
| image          =<!-- Deleted image removed:  [[Image:The Europeans.JPG|200px]] --><!--prefer 1st edition-->
| caption  = Cover of 1964 Penguin Modern Classics edition of ''The Europeans''
| author         = [[Henry James]]
| country        = United Kingdom, United States
| language       = English
| genre          = Novel
| publisher      = [[Macmillan Publishers|Macmillan and Co.]], [[London]]<br>Houghton, Osgood and Company, [[Boston]]
| release_date   = Macmillan: 18-Sept-1878<br>Houghton: 12-Oct-1878
| media_type     = Print
| pages          = Macmillan: volume one, 255; volume two, 272<br>Houghton: 281, Penguin: 205
}}
'''''The Europeans: A sketch''''' is a short novel by [[Henry James]], published in 1878. It is essentially a [[comedy]] contrasting the behaviour and attitudes of two visitors from Europe with those of their relatives living in the 'new' world of [[New England]]. The novel first appeared as a serial in ''[[The Atlantic Monthly]]'' for July–October 1878. James made numerous minor revisions for the first book publication.

==Plot introduction==
The tale opens in [[Boston]] and [[New England]] in the middle of the 19th century, and describes the experiences of two European [[siblings]] shifting from the [[old world|old]] to the [[new world]]. The two protagonists are Eugenia Münster and Felix Young, who since their early childhood have lived in Europe, moving from France to Italy and from Spain to Germany. In this last place, Eugenia entered into a [[Morganatic marriage]] with Prince Adolf of Silberstadt-Schreckenstein, the younger brother of the reigning prince who is now being urged by his family to dissolve the marriage for political reasons. Because of this, Eugenia and Felix decide to travel to America to meet their distant cousins, so that Eugenia may "seek her fortune" in the form of a wealthy American husband.

All the cousins live in the countryside around Boston and spend a lot of time together. The first encounter with them corresponds to the first visit of Felix to his family. Mr Wentworth’s family is a [[puritanical]] one, far from the Europeans' habits. Felix is fascinated by the [[patriarchal]] Mr Wentworth, his son, Clifford, and two daughters, Gertrude and Charlotte. They spend a lot of time together with Mr. Robert Acton and his sister Lizzie, their neighbours.

Eugenia’s reaction after this first approach differs from Felix’s. She is not really interested in sharing her time with this circle. She doesn’t like the Wentworth ladies and does not want to visit them frequently. In contrast, her brother is very happy to share his spare time with Charlotte and Gertrude, spending hours in their piazza or garden creating portraits of the two ladies.

==Plot summary==
Eugenia and her brother Felix arrive in [[Boston]]. The next day Felix visits their cousins. He first meets Gertrude, who is shirking attendance at church. He stays over for dinner. The next day Eugenia visits them. Three days later their uncle Mr Wentworth suggests they stay in a little house close to theirs. Felix suggests making a portrait of his uncle. When Mr. Wentworth refuses, he makes plans to do a painting of Gertrude instead. The latter walks into Mr Brand again and bursts out crying when he asserts that he still loves her. She then sits for Felix to do his painting of her, and he reproaches his American relatives for being very [[Puritans|puritanical]].

Eugenia is talking and flirting with Robert Acton; she says she will divorce her husband. She visits Mrs Acton and says a white lie - that her son has been talking about her a lot - which comes across as a terrible faux-pas. Later, Mr Wentworth tells Felix that Clifford got suspended from [[Harvard]] owing to his drinking problem, and that he is improperly in love with Lizzie Acton - Felix suggests fixing him up with Eugenia instead. Later still, Gertrude tells him her father wants her to marry Mr Brand, though she doesn't love him. Mr Brand then criticizes Felix. Gertrude emotionally blackmails Charlotte into keeping him from talking to her, lest she tell him Charlotte likes him. Clifford then visits Eugenia. Robert Acton goes to the Wentworths' but Eugenia is not in their house; he goes into hers and asks her about the divorce note and going to see the [[Niagara Falls]] with him. Clifford comes out of his hiding place; the two men get back together.

Felix tells Eugenia he wants to marry Gertrude; she admits to being unsure of Robert. Mr Brand then visits Felix, who tells him Charlotte likes him. Eugenia gives her farewell to Mrs Acton as she prepares to move back to Europe. She walks into Robert, who says he loves her - she has sent the divorce letter; he will have to join her in Europe. Later, Felix asks Charlotte to tell her father he would be a good prospective husband for Gertrude. He then meets with his beloved again, and she says she would leave her family with him.

Three days later, Felix decides to visit his uncle and tell him he wants to marry Gertrude. The latter turns up and tells her father the same thing. Mr Brand asks for Mr Wentworth's consent to marry Gertrude and Felix - he agrees. Mr Brand and Charlotte later marry. Clifford has proposed to Lizzie Acton; Eugenia, however, has repudiated Robert Acton, not actually signed the divorce note, and is traveling back to Europe. Years later, after his mother's funeral, Robert would find a 'nice young girl'...

==Characters==
*'''Baroness Eugenia-Camilla-Dolores Münster'''.
*'''Felix Young''', 28 years old, Eugenia's brother. Felix is an artist. He does [[portrait]]s and likes music and the theatre.
*'''The Reigning Prince''', the brother of Eugenia's husband.
*'''Prince Adolf, of Silberstadt-Schreckenstein''', Eugenia's husband in a [[morganatic marriage]], whose family demands their divorce.
*'''Mr Wentworth''', Eugenia and Felix's uncle. He went to [[Harvard]].
*'''Gertrude Wentworth''', 22 or 23 years old. Her father wants her to marry Mr Brand, but instead she ends up marrying Felix.
*'''Charlotte Wentworth''', Gertrude's older sister. She ends up marrying Mr Brand.
*'''Clifford Wentworth''', in his 20s. He got suspended from [[Harvard]] because of a drinking problem. He ends up marrying Lizzie.
*'''Mr Brand''', a [[Unitarianism|Unitarian]] minister. He helps Clifford remain abstemious. Mr Wentworth wants him to marry Gertrude.
*'''Mr Robert Acton''', a young man who increased his fortune in [[China]]. He went to [[Harvard]].
*'''Miss Lizzie Acton''', Robert's sister. She ends marrying Clifford.
*'''Mrs Acton''', their mother, an invalid. She is 55 years old when Eugenia meets her.
*'''Augustine''', Eugenia's maid.
*'''Mrs Whiteside''', Eugenia and Felix's mother.
*'''Mr Adolphus Young''', Eugenia and Felix's father.
*'''Mr Broderip''', a friend of Mr Wentworth's. He went to [[Harvard]].

==Allusions to other works==
* The authors and works of fiction mentioned are: [[John Keats]], The ''[[Arabian Nights]]'', the [[Bible]] ([[Queen of Sheba]], [[Solomon]]), [[Madame de Stael]], [[Madame Recamier]], [[Charles Dickens]]'s ''[[The Life and Adventures of Nicholas Nickleby]]'', [[Ralph Waldo Emerson]].
* Mr Wentworth reads the ''[[North American Review]]'' and the ''[[Boston Daily Advertiser]]''.
* The visual arts are alluded to with [[Raphael]].

==Allusions to actual history==
*Eugenia says that her "father used to tell [her]" of General [[George Washington]]. Meanwhile, Gertrude imagines that Eugenia will be like the lithograph of [[Joséphine de Beauharnais|Empress Josephine]] hung in the Wentworth's parlor.

==Major themes==
One of the most important themes of the novel is the comparison between European and American women, which James stresses through the great difference existing between Eugenia and the Wentworth ladies. Madame Münster is independent, modern, and displays hauteur. Gertrude and Charlotte lack this self-possession. For example, they tend to comply with their father's suggestions. When Mr. Wentworth tries to arrange a marriage between Mr. Brand and Gertrude to which she objects, it is difficult for her to clearly express her preference. The sisters spend most of their free time with the family, until the moment in which Gertrude discovers herself to be in love with Felix. Her love encourages her to take a new perspective on the world and to resist the presumptions of her family. She reacts against her father's decision regarding Mr. Brand, explaining that she will never marry a man she does not love. Instead, Gertrude will marry Felix and leave America.<ref>{{cite book|last=Wagenknecht|first=Edward|title=The Novels of Henry James|year=1983|publisher=Frederick Ungar Publishing Co.|location=New York|isbn=0-8044-2959-6|quote=She [Gertrude] does not "care for the great questions of life," at least in the sense in which they appeal to Mr. Brand...if she is to live at all, she must be herself, developing her own potentialities and realizing that not all questions are closed questions...|page=61}}</ref>

The difference between Europeans and New Englanders manifests itself in particular in the expression of feelings and emotions, which are very sensitive for the former: love is more important than money. Moreover, American people are more straitlaced and they have closer links with tradition. The most important thing in life for those living in the 'New World' is, ironically, respecting old traditions and accepting the rules of a good morality. Mr Wentworth is profoundly surprised and fascinated by Eugenia's marriage experience as well as Robert Acton's; in the Americans' eyes, Eugenia is a perplexing woman.<ref>{{cite book|last=Tuttleton|first=James|title=A Companion to Henry James Studies|year=1993|editor=Daniel Mark Fogel|publisher=Greenwood Press|location=Westport, Connecticut|isbn=0-313-25792-2|quote=And insofar as Eugenia may rectify what James believed to be active deficiencies in the culture of New England, she is a positive energy in the novel. She brings color, civility, charm and conscious art to a community that is plain, dull, and devoid of sophistication. But Eugenia is no ideal heroine. She fails to make her fortune in the New World because she is not above using devious means to realize her financial and matrimonial ambitions.|page=115}}</ref>

==Literary significance and criticism==
The omniscient narrator uses a very fine and cultivated language, sometimes he prefers [[Latin]] diction; preferring to introduce very long, detailed descriptions of the setting and of the characters, from both a psychological and a physical point of view. In addition to the contributions of the narrator, dialogue helps the author to introduce his characters and to show their reactions to unfolding events. Critic Robert Gale credited James with a "specifically well-delineated New England" in the book, which he found "charming".<ref>{{cite book|last=Gale|first=Robert|title=A Henry James Encyclopedia|year=1989|publisher=Greenwood Press|location=Westport, Connecticut|isbn=0-313-25846-5|page=214}}</ref>

[[F.R. Leavis]], the influential English [[literary critic]], had a high opinion of this brief work, claiming:

:"The Europeans, the visiting cousins, are there mainly to provide a foil for the American family, a study of the New England ethos being James's essential purpose.... Nevertheless James's irony is far from being unkind; he sees too much he admires in the ethos he criticizes to condemn it.... James is not condemning or endorsing either New England or Europe.... This small book, written so early in James's career, is a masterpiece of major quality."<ref>''The Great Tradition'' by F.R. Leavis (London: Chatto and Windus 1948)</ref>

Others, most notably the author's brother [[William James]], faulted the novel's "slightness." Henry James replied in a November 14, 1878 letter that he somewhat agreed with the criticism:

:"I was much depressed on reading your letter by your painful reflections on ''The Europeans'', but now, an hour having elapsed, I am beginning to hold up my head a little; the more so as I think I myself estimate the book very justly & am aware of its extreme slightness. I think you take these things too rigidly and unimaginatively—too much as if an artistic experiment were a piece of conduct, to which one's life were somehow committed; but I think you're quite right in pronouncing the book 'thin' & empty."<ref>''The Correspondence of William James Vol.1: William and Henry 1861-1884'' edited by Ignas Skrupskelis and Elizabeth Berkeley, p.308 (Charlottesville: University of Virginia Press 1994) ISBN 0-8139-1338-1</ref>

James excluded the novel from the ''[[New York Edition]]'' of his fiction (1907–09). Among others speculating on the reasons for this exclusion, critic Oscar Cargill commented that "the intimate contemporary judgment and misfortune may have been a lingering decisive factor in James' mind."<ref>{{cite book|last=Cargill|first=Oscar|title=The Novels of Henry James|year=1961|publisher=Macmillan Company|location=New York|page=63}}</ref>

It has also been suggested by one author that Felix's rootless [[Bohemia]]n origin, as well as his "eternal gaiety", were signifiers of his covert homosexuality.<ref>Eric Haralson, ''Henry James and Queer Modernity'', Cambridge University Press, 2003, pages 50-51</ref>

== Film adaptation ==
{{main article|The Europeans (film)}}
The 1979 [[Merchant Ivory Productions]] film ''[[The Europeans (film)|The Europeans]]'' starred [[Lee Remick]] as Eugenia and received six award nominations.<ref>{{IMDb title|0079123}}</ref> The movie received a number of lukewarm reviews as a somewhat over-literary adaptation. Critic Chris Elliot noted the movie's "opaque refinements and elusive intentions, a predilection for intricacies of language and manners." [[Roger Ebert]] similarly demurred that the movie was "a classroom version of James, a film with no juice or life of its own."<ref>{{cite web|url=http://movies.toptenreviews.com/reviews/mr100867.htm|title=The Europeans (1979)|publisher=Top Ten Reviews|accessdate=2007-06-25}}</ref>

==References ==
{{reflist|2}}

== External links ==
*{{wikisource-inline|The Europeans}}
*{{Commonscat-inline}}
*[http://cdl.library.cornell.edu/cgi-bin/moa/pageviewer?ammem/coll=moa&root=/moa/atla/atla0042/&tif=00058.TIF&view=50&frames=1 Original magazine text of ''The Europeans'' (1878)]
*[http://www2.newpaltz.edu/~hathawar/european.html First American book version of ''The Europeans'' (1878)]
*[http://www.loa.org/volume.jsp?RequestID=56&section=notes Note on the various versions of ''The Europeans''] at the 
* {{librivox book | title=The Europeans | author=Henry JAMES}}

[[Library of America]] web site

{{Henry James}}

{{DEFAULTSORT:Europeans, The}}
[[Category:1878 novels]]
[[Category:Novels by Henry James]]
[[Category:19th-century American novels]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in The Atlantic (magazine)]]
[[Category:Macmillan Publishers books]]
[[Category:American novels adapted into films]]
[[Category:British novels adapted into films]]
[[Category:19th-century British novels]]

[[eu:Europarrak (eleberria)]]