{{featured article}}
{{Other ships|HMS Collingwood}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Collingwood.jpg|300px]]
|Ship caption=''Collingwood'' at anchor, 1912
}}
{{Infobox ship career
|Hide header= 
|Ship country=United Kingdom
|Ship flag={{shipboxflag|UK|naval}}
|Ship name=''Collingwood''
|Ship namesake=[[Vice-Admiral]] [[Cuthbert Collingwood, 1st Baron Collingwood]]
|Ship ordered=26 October 1907
|Ship awarded=
|Ship builder=[[HMNB Devonport|Devonport Royal Dockyard]]
|Ship original cost=
|Ship yard number=
|Ship way number=
|Ship laid down=3 February 1908
|Ship launched=7 November 1908
|Ship commissioned=April 1910
|Ship decommissioned=
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate=Sold for [[ship breaking|scrap]], 12 December 1922
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(as built)
|Ship class={{sclass-|St Vincent|battleship|0}} [[dreadnought battleship]]
|Ship displacement= {{convert|19700|LT|t|lk=in}} (normal)
|Ship length={{convert|536|ft|m|abbr=on|1}} ([[Length overall|o/a]])
|Ship beam={{convert|84|ft|2|in|m|abbr=on|1}}
|Ship draught={{convert|28|ft|m|abbr=on|1}}
|Ship power=*{{convert|24500|shp|lk=in|abbr=on}}
*18 × [[Yarrow boiler]]s
|Ship propulsion=4 × shafts; 2 × [[steam turbine]] sets
|Ship speed={{convert|21|kn|lk=in}}
|Ship range={{convert|6900|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
|Ship complement=758
|Ship armament=* 5 × twin [[BL 12 inch Mk XI - XII naval gun|12-inch (305&nbsp;mm) guns]]
* 20 × single [[BL 4 inch naval gun Mk VII|4-inch (102&nbsp;mm) guns]]
* 3 × [[British 18 inch torpedo|18-inch (450&nbsp;mm)]] [[torpedo tube]]s 
|Ship armour=* [[Belt armor|Belt]]: {{convert|8|-|10|in|mm|0}}
* [[Bulkhead (partition)|Bulkhead]]s: {{convert|5|-|8|in|mm|0}}
* [[Deck (ship)|Deck]]: {{convert|.75|-|3|in|mm|0}}
* [[Turret]]s: {{convert|11|in|mm|0}}
* [[Barbette]]s: {{convert|5|-|10|in|mm|0}}
*[[Conning tower]]: {{convert|8|-|11|in|mm|0|abbr=on}} 
|Ship notes=
}}
|}
'''HMS ''Collingwood''''' was a {{sclass-|St Vincent|battleship|0}} [[dreadnought battleship]] built for the [[Royal Navy]] in the first decade of the 20th century. She spent her whole career assigned to the [[Home Fleet|Home]] and [[Grand Fleet]]s and often served as a [[flagship]]. Prince Albert (later [[King George VI]]) spent several years aboard the ship before and during [[Naval warfare of World War I|World War I]]. At the [[Battle of Jutland]] in May 1916, ''Collingwood'' was in the middle of the [[battleline]] and lightly damaged a German [[battlecruiser]]. Other than that battle, and the inconclusive [[Action of 19 August 1916|Action of 19 August]], her service during the war generally consisted of routine patrols and training in the [[North Sea]]. The ship was deemed obsolete after the war; she was reduced to [[Reserve fleet|reserve]] and used as a [[training ship]] before being sold for [[ship breaking|scrap]] in 1922.

==Design and description==
The design of the ''St Vincent'' class was derived from that of the previous {{sclass-|Bellerophon|battleship|4}}, with a slight increase in size, armour and more powerful guns, among other more minor changes. ''Collingwood'' had an [[length overall|overall length]] of {{convert|536|ft|m|1}}, a [[Beam (nautical)|beam]] of {{convert|84|ft|2|in|m|1}},<ref>Burt, pp. 75–76</ref> and a normal [[Draft (hull)|draught]] of {{convert|28|ft|m|1}}.<ref name=p5>Preston, p. 125</ref> She [[Displacement (ship)|displaced]] {{convert|19700|LT|t|lk=in}} at normal load and {{convert|22800|LT|t}} at [[deep load]]. In 1911 her crew numbered 758 officers and [[naval rating|ratings]].<ref name="B_76">Burt, p. 76</ref>

[[File:1stGenBritishBBs.tiff|thumb|left|Right elevation and plan of the first generation of British dreadnoughts from the 1912 edition of ''[[Brassey's Naval Annual]]'']]
''Collingwood'' was powered by two sets of [[Parsons Marine Steam Turbine Company|Parsons]] [[direct-drive]] [[steam turbine]]s, each driving two shafts, using steam from eighteen [[Yarrow boiler]]s. The turbines were rated at {{convert|24500|shp|abbr=on}} and were intended to give the ship a maximum speed of {{convert|21|kn|lk=in}}. During her full-power, eight-hour [[sea trial]]s on 17 January 1910, she only reached a top speed of {{convert|20.62|kn}} from {{convert|26789|shp|abbr=on}}. ''Collingwood'' had a range of {{convert|6900|nmi|lk=in}} at a cruising speed of {{convert|10|kn}}.<ref>Burt, pp. 76, 80</ref>

===Armament and armour===
The ''St Vincent'' class was equipped with ten [[List of British ordnance terms#BL|breech-loading]] (BL) [[BL 12 inch Mk XI - XII naval gun|12-inch (305&nbsp;mm) Mk XI guns]] in five twin-[[gun turret]]s, three along the centreline and the remaining two as [[wing turret]]s. The secondary, or anti-[[torpedo boat]], armament comprised twenty [[BL 4 inch naval gun Mk VII|BL 4-inch (102&nbsp;mm) Mk VII guns]]. Two of these guns were each installed on the roofs of the fore and aft centreline turrets and the wing turrets in unshielded mounts, and the other ten were positioned in the superstructure. All guns were in single mounts.<ref name=p3/>{{refn|Sources disagree on the number, type and composition of the secondary armament. Burt gives only eighteen 4-inch guns and claims that they were the older [[List of British ordnance terms#QF|quick-firing]] QF Mark III guns. In addition, he lists a 12-pounder (three-inch (76&nbsp;mm)) gun.<ref name="B_76"/> Preston concurs on the number of 4 inchers, but does not list the 12 pounder.<ref name=p5/> Parkes says twenty 4-inch guns; while not identifying the type, he does say that they were 50-calibre guns<ref name=p3/> and Gardiner & Gray agree.<ref>Gardiner & Gray, p. 23</ref> Friedman shows the QF Mark III as a 40-calibre gun and states that the 50-calibre BL Mark VII gun armed all of the early dreadnoughts.<ref>Friedman, pp. 97–98</ref>|group=Note}} The ships were also fitted with three [[British 18 inch torpedo|18-inch (450&nbsp;mm)]] [[torpedo tube]]s, one on each [[broadside]] and the third in the [[stern]].<ref name=p5/>

The ''St Vincent''-class ships had a [[Belt armor|waterline belt]] of [[Krupp cemented armour]] (KC) that was {{convert|10|in|0}} thick between the fore and aftmost [[barbette]]s, reducing to a thickness of {{convert|2|in|0}} before it reached the ships' ends. Above this was a [[strake]] of armour {{convert|8|in|0}} thick. Transverse [[bulkhead (partition)|bulkhead]]s {{convert|5|and|8|in|0}} inches thick terminated the thickest parts of the waterline and upper armour belts once they reached the outer portions of the endmost barbettes.<ref>Burt, pp. 76, 78; Parkes, p. 503</ref>

The three centreline barbettes were protected by armour {{convert|9|in|0}} thick above the main [[deck (ship)|deck]] that thinned to {{convert|5|in|0}} below it. The wing barbettes were similar except that they had 10 inches of armour on their outer faces. The gun turrets had {{convert|11|in|0|adj=on}} faces and sides with {{convert|3|in|0|adj=on}} roofs. The three armoured decks ranged in thicknesses from {{convert|.75|to|3|in}}. The front and sides of the forward [[conning tower]] were protected by 11-inch plates, although the rear and roof were 8 inches and 3 inches thick, respectively.<ref>Burt, pp. 76, 78; Parkes, p. 504</ref>

===Alterations===
The guns on the forward turret roof were removed in 1911–1912 and the upper forward pair of guns in the superstructure were removed in 1913–1914. In addition, [[gun shield]]s were fitted to all guns in the superstructure and the [[bridge (nautical)|bridge]] structure was enlarged around the base of the forward tripod mast. During the first year of the war, a [[fire-control director]] was installed high on the forward tripod mast. Around the same time, the base of the forward superstructure was rebuilt to house eight 4-inch guns and the turret-top guns were removed, which reduced her secondary armament to a total of fourteen guns. In addition, a pair of 3-inch [[anti-aircraft gun|anti-aircraft (AA) guns]] were added.<ref name=b81>Burt, p. 81</ref>

By April 1917, ''Collingwood'' mounted thirteen 4-inch anti-torpedo boat guns as well as single 4-inch and 3-inch AA guns. Approximately {{convert|50|LT|t}} of additional deck armour had been added after the Battle of Jutland. Before the end of the war the AA guns were moved from the deckhouse between the aft turrets to the stern and the stern torpedo tube was removed. In 1918, a high-angle [[rangefinder]] was fitted and [[Flight deck#Early flight decks|flying-off platforms]] were installed on the roofs of the fore and aft turrets.<ref name=b81/>

==Construction and career==
[[File:British Battleships of the First World War; HMS Collingwood Q21101.jpg|thumb|left|''Collingwood'' shortly after completion]]
''Collingwood'', named after [[Vice-Admiral]] [[Cuthbert Collingwood, 1st Baron Collingwood|Cuthbert Collingwood]],<ref>Silverstone, p. 223</ref> was ordered on 26 October 1907.<ref name="B_86"/> She was [[laid down]] at [[HMNB Devonport|Devonport Royal Dockyard]] on 3 February 1908, [[Ship naming and launching|launched]] on 7 November 1908 and completed in April 1910. Including her armament, the ship's cost is quoted at £1,680,888<ref name="B_76"/> or £1,731,640.<ref name=p3>Parkes, p. 503</ref>

On 19 April 1910, ''Collingwood'' was [[Ship commissioning|commissioned]] and assigned to the 1st Division of the [[Home Fleet]] under the command of [[Captain (Royal Navy)|Captain]] [[William Pakenham (Royal Navy officer)|William Pakenham]].<ref name=b29>Brady, Part One, p. 29</ref> She joined other members of the fleet in regular peacetime exercises, and on 11 February 1911 damaged her bottom plating on an uncharted rock off [[Ferrol, Spain|Ferrol]]. On 24 June the ship was present at the [[Coronation Fleet Review]] for [[King George V]] at [[Spithead]].<ref name="B_86">Burt, p. 86</ref> Pakenham was relieved by Captain [[Charles Vaughan-Lee]] on 1 December.<ref name=b29/> On 1 May 1912, the 1st Division was renamed the [[1st Battle Squadron (United Kingdom)|1st Battle Squadron]].<ref name="B_86"/> On 22 June, Vaughan-Lee was transferred to the battleship {{HMS|Bellerophon|1907|2}} and Captain [[James Ley (Royal Navy officer)|James Ley]] assumed command; Vice-Admiral [[Stanley Colville]] hoisted his flag in ''Collingwood'' as commander of the 1st Battle Squadron.<ref name="b29"/> The ship participated in the Parliamentary Naval Review on 9 July at Spithead before beginning a refit late in the year. In March 1913, ''Collingwood'' and the 1st Battle Squadron undertook a port visit to [[Cherbourg]], France.<ref name=b88>Burt, p. 88</ref> [[Midshipman]] Prince Albert (later [[King George VI]]) was assigned to the ship on 15 September 1913.<ref>Judd, p. 28</ref> ''Collingwood'' hosted Albert's older brother, [[Edward VIII|Edward]], [[Prince of Wales]], during a short cruise on 18 April 1914. She became a [[private ship]] when Colville hauled down his flag on 22 June.<ref name=b88/>

===World War I===
Between 17 and 20 July, ''Collingwood'' took part in a test [[mobilisation]] and fleet review as part of the British response to the [[July Crisis]]. Arriving in [[Isle of Portland|Portland]] on 27 July, she was ordered to proceed with the rest of the Home Fleet to [[Scapa Flow]] two days later<ref name=b88/> to safeguard the fleet from a possible [[Imperial German Navy|German]] surprise attack.<ref>Massie, pp. 15–20</ref> In August 1914, following the outbreak of [[World War I]], the Home Fleet was reorganised as the [[Grand Fleet]], and placed under the command of Admiral [[John Jellicoe, 1st Earl Jellicoe|John Jellicoe]].<ref>Gardiner & Gray, p. 32</ref> Most of it was briefly based (22 October to 3 November) at [[Lough Swilly]], Ireland, while the defences at Scapa were strengthened. On the evening of 22 November 1914, the Grand Fleet conducted a fruitless sweep in the southern half of the [[North Sea]]; ''Collingwood'' stood with the main body in support of Vice-Admiral [[David Beatty, 1st Earl Beatty|David Beatty]]'s [[1st Battlecruiser Squadron]]. The fleet was back in port in Scapa Flow by 27 November.<ref>Jellicoe, pp. 163–65</ref> The 1st Battle Squadron cruised north-west of the [[Shetland Islands]] and conducted gunnery practice on 8–12 December. Four days later, the Grand Fleet [[sortie]]d during the German [[raid on Scarborough, Hartlepool and Whitby]], but failed to make contact with the [[High Seas Fleet]]. ''Collingwood'' and the rest of the Grand Fleet conducted another sweep of the North Sea on 25–27 December.<ref>Brady, Part One, p. 32; Jellicoe, pp. 172, 179, 183–84</ref>

[[File:First battle squadron in the North Sea (April 1915).jpg|thumb|upright=2|The 1st Battle Squadron at sea, April 1915]]
The Grand Fleet, including ''Collingwood'', conducted gunnery drills on 10–13 January 1915 west of the [[Orkneys]] and Shetlands. On the evening of 23 January, the bulk of the Grand Fleet sailed in support of Beatty's [[battlecruiser]]s, but ''Collingwood'' and the rest of the fleet did not participate in the ensuing [[Battle of Dogger Bank (1915)|Battle of Dogger Bank]] the following day.<ref>Jellicoe, pp. 190, 194–96</ref> The ship sailed for [[Portsmouth Royal Dockyard]] on 2 February to begin a brief refit and returned on 18 February.<ref>Brady, Part One, p. 32</ref> On 7–10 March, the Grand Fleet conducted a sweep in the northern North Sea, during which it conducted training manoeuvres. Another such cruise took place on 16–19 March.<ref>Jellicoe, p. 206</ref> From 25 March to 14 April 1915, [[Rear-Admiral]] [[Hugh Evan-Thomas]] temporarily hoisted his flag aboard ''Collingwood''.<ref name=b3>Brady, Part One, p. 33</ref> On 11 April, the Grand Fleet conducted a patrol in the central North Sea and returned to port on 14 April; another patrol in the area took place on 17–19 April, followed by gunnery drills off the Shetlands on 20–21 April.<ref>Jellicoe, pp. 211–12</ref>

The Grand Fleet conducted sweeps into the central North Sea on 17–19 May and 29–31 May without encountering any German vessels. During 11–14 June the fleet conducted gunnery practice and battle exercises west of the Shetlands.<ref>Jellicoe, pp. 217, 218–19, 221–22</ref> ''Collingwood'' was briefly docked at [[Invergordon]] from 23 to 25 June. King George V inspected the ship on 8 July,<ref name=b3/> and the Grand Fleet conducted training off the Shetlands beginning three days later.<ref>Jellicoe, p. 228</ref> Rear-Admiral [[Ernest Gaunt]] temporarily used ''Collingwood'' as his flagship from 24 August to 24 September and from 10 December to 16 January 1916.<ref>Brady, Part One, pp. 33–34</ref> On 2–5 September 1915, the fleet went on another cruise in the northern end of the North Sea and conducted gunnery drills. Throughout the rest of the month, the Grand Fleet conducted numerous training exercises. The ship, together with the majority of the Grand Fleet, conducted another sweep into the North Sea from 13 to 15 October. Almost three weeks later, ''Collingwood'' participated in another fleet training operation west of Orkney during 2–5 November.<ref>Jellicoe, pp. 243, 246, 250, 253</ref> On 21 November, she sailed for Devonport Royal Dockyard for a minor [[wikt:overhaul|overhaul]] and arrived back at Scapa on 9 December.<ref name=b4>Brady, Part One, p. 34</ref>

The Grand Fleet departed for a cruise in the North Sea on 26 February 1916; Jellicoe had intended to use the [[Harwich Force]] to sweep the [[Heligoland Bight]], but bad weather prevented operations in the southern North Sea. As a result, the operation was confined to the northern end of the sea. Another sweep began on 6 March, but had to be abandoned the following day as the weather grew too severe for the escorting [[destroyer]]s. On the night of 25 March, ''Collingwood'' and the rest of the fleet sailed from Scapa Flow to support Beatty's battlecruisers and other light forces raiding the German [[Zeppelin]] base at [[Tondern]]. By the time the Grand Fleet approached the area on 26 March, the British and German forces had already disengaged and a strong [[gale]] threatened the light craft, so the fleet was ordered to return to base. On 21 April, the Grand Fleet conducted a demonstration off [[Horns Reef]] to distract the Germans while the [[Imperial Russian Navy|Russian Navy]] relaid its defensive [[naval mine|minefields]] in the [[Baltic Sea]]. The fleet returned to Scapa Flow on 24 April and refuelled before proceeding south in response to intelligence reports that the Germans were about to launch a [[Bombardment of Yarmouth and Lowestoft|raid on Lowestoft]]. The Grand Fleet arrived in the area after the Germans had withdrawn. On 2–4 May, the fleet conducted another demonstration off Horns Reef to keep German attention focused on the North Sea.<ref>Jellicoe, pp. 271, 275, 279–80, 284, 286–90</ref>

====Battle of Jutland====
[[File:Map of the Battle of Jutland, 1916.svg|thumb|upright=1.7|Maps showing the manoeuvres of the British (blue) and German (red) fleets on 31 May&nbsp;– 1 June 1916|alt=The British fleet sailed from northern Britain to the east while the Germans sailed from Germany in the south; the opposing fleets met off the Danish coast]]
{{Main|Battle of Jutland}}
In an attempt to lure out and destroy a portion of the Grand Fleet, the German High Seas Fleet, composed of 16 dreadnoughts, 6 [[Pre-dreadnought battleship|pre-dreadnoughts]], 6 [[light cruiser]]s, and 31 [[torpedo boat]]s, departed the [[Jade Bight]] early on the morning of 31 May. The fleet sailed in concert with Rear Admiral [[Franz von Hipper]]'s five battlecruisers and supporting cruisers and torpedo boats. The Royal Navy's [[Room 40]] had [[Signals intelligence|intercepted and decrypted German radio traffic]] containing plans of the operation. In response, the Admiralty ordered the Grand Fleet, totalling some 28 dreadnoughts and 9 battlecruisers, to sortie the night before to cut off and destroy the High Seas Fleet.<ref>Tarrant, pp. 54–55, 57–58</ref> ''Collingwood'' was the eighteenth ship from the head of the battle line after the Grand Fleet deployed for battle.<ref name=b88/>

The initial action was fought primarily by the British and German battlecruiser formations in the afternoon, but by 18:00,<ref group=Note>The times used in this section are in [[Universal Time|UT]], one hour behind [[Central European Time|CET]], which is often used in German works.</ref> the Grand Fleet approached the scene. Fifteen minutes later, Jellicoe gave the order to turn and deploy the fleet for action. The transition from cruising formation caused congestion with the rear divisions, forcing many ships to reduce speed to {{convert|8|kn}} to avoid colliding with each other. During the first stage of the general engagement, ''Collingwood'' fired eight [[salvo]]s from her main guns at the crippled [[light cruiser]] {{SMS|Wiesbaden}} from 18:32, although the number of hits made, if any, is unknown. Her secondary armament then engaged the destroyer {{SMS|G42}}, which was attempting to come to ''Wiesbaden''{{'}}s assistance, but failed to hit her. At 19:15 ''Collingwood'' fired two salvoes of [[Explosive material#High explosives|high explosive]] (HE) shells at the battlecruiser {{SMS|Derfflinger}}, hitting her target once before she disappeared into the mist. The shell detonated in the German ship's [[sickbay]] and damaged the surrounding superstructure. Shortly afterwards, during the attack of the German destroyers around 19:20, the ship fired her main armament at a damaged destroyer without success and dodged two torpedoes that missed by {{convert|10|yd}} behind and {{convert|30|yd}} in front. This was the last time she fired her guns during the battle.<ref>Campbell, pp. 37, 116, 146, 157, 205, 208, 212, 214, 229–30</ref>

Following the German destroyer attack, the High Seas Fleet disengaged, and ''Collingwood'' and the rest of the Grand Fleet saw no further action in the battle. This was, in part, due to confusion aboard the fleet flagship over the exact location and course of the German fleet; without this information, Jellicoe could not bring his fleet to action. At 21:30, the Grand Fleet began to reorganise into its night-time cruising formation. Early on the morning of 1 June, the Grand Fleet combed the area, looking for damaged German ships, but after spending several hours searching, they found none. ''Collingwood'' fired a total of 52 [[Armor-piercing shot and shell|armour-piercing, capped]] and 32 HE shells from her main armament and 35 four-inch shells during the battle.<ref>Campbell, pp. 256, 274, 309–10, 346, 348, 358</ref> Prince Albert was a [[sub-lieutenant]] commanding the forward turret during the battle and sat in the open on the turret roof during a lull in the action.<ref>Gordon, pp. 454, 459</ref>

====Subsequent activity====
After the battle the ship was transferred to the [[4th Battle Squadron (United Kingdom)|4th Battle Squadron]] under the command of Vice-Admiral Sir [[Doveton Sturdee]], who inspected ''Collingwood'' on 8 August 1916.<ref>Brady, Part Two, p. 19</ref> The Grand Fleet sortied on 18 August to ambush the High Seas Fleet while it advanced into the southern North Sea, but a series of miscommunications and mistakes prevented Jellicoe from intercepting the German fleet before it returned to port. Two light cruisers were sunk by German [[U-boat]]s during the operation, prompting Jellicoe to decide to not risk the major units of the fleet south of 55° 30' North due to the prevalence of German submarines and [[naval mine|mines]]. The [[Admiralty]] concurred and stipulated that the Grand Fleet would not sortie unless the German fleet was attempting an invasion of Britain or there was a strong possibility it could be forced into an engagement under suitable conditions.<ref>Halpern, pp. 330–32</ref>

''Collingwood'' received a brief refit at [[Rosyth]] in early September before rejoining the Grand Fleet. On 29 October Sturdee came aboard to present the ship with her [[battle honour]], "Jutland 1916". Captain [[Wilmot Nicholson]] briefly assumed command on 1 December before transferring to the new battlecruiser {{HMS|Glorious||2}} upon his relief by Captain [[Cole Fowler]] on 26 March 1917. Together with the rest of the 4th Battle Squadron, ''Collingwood'' put to sea for tactical exercises for a few days in February 1917. The ship was present at Scapa Flow when her [[sister ship]] {{HMS|Vanguard|1909|2}}{{'}}s [[magazine (artillery)|magazines]] exploded on 9 July and her crew recovered the bodies of three men killed in the explosion. In January 1918, ''Collingwood'' and other of the older dreadnoughts cruised off the coast of Norway for several days, possibly to provide distant cover for a convoy to Norway.<ref name=b22>Brady, Part Two, pp. 19–22</ref> Along with the rest of the Grand Fleet, she sortied on the afternoon of 23 April after radio transmissions revealed that the High Seas Fleet was at sea after a failed attempt to intercept the regular British convoy to Norway. The Germans were too far ahead of the British, and no shots were fired.<ref>Massie, p. 748</ref> By early November, ''Collingwood'' was at Invergordon to receive a brief refit in the floating dock based there, and missed the surrender of the High Seas Fleet on the 21st. She was slightly damaged on 23 November while attempting to come alongside the [[Replenishment oiler|oiler]] [[Royal Fleet Auxiliary|RFA]] [[RFA Ebonol|''Ebonol'']].<ref name=b24>Brady, Part Two, pp. 23–24</ref>

In January 1919, ''Collingwood'' was transferred to Devonport and assigned to the Reserve Fleet. Upon the dissolution of the Grand Fleet on 18 March, the Reserve Fleet was redesignated the Third Fleet and ''Collingwood'' became its flagship. She became a [[Ship's tender|tender]] to [[HMS Vivid (shore establishment 1890)|HMS ''Vivid'']] on 1 October and served as a gunnery and [[wireless telegraphy]] (W/T) training ship. The W/T school was transferred to ''Glorious'' on 1 June 1920 and the gunnery duties followed in early August; ''Collingwood'' returned to the reserve. She became a [[Boy seaman|boys']] training ship on 22 September 1921 until she was [[Ship decommissioning|paid off]] on 31 March 1922. ''Collingwood'' was sold to [[John Cashmore Ltd]] for scrap on 12 December and arrived at [[Newport, Wales]], on 3 March 1923 to be broken up.<ref name=b24/>

==Relics==
[[Battle ensign]]s flown by the ship during Jutland survive at the [[shore establishment]] of the [[HMS Collingwood (shore establishment)|same name]] and at the [[Roedean School]] in [[East Sussex]].<ref>Gordon, p. 417</ref>

==Footnotes==
{{Reflist|group=Note}}

==Citations==
{{reflist|30em}}

==Bibliography==
* {{cite book|last=Burt|first=R. A.|title=British Battleships of World War One|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1986|isbn=0-87021-863-8}}
*{{cite journal|last=Brady|first=Mark|date=May 2014|title=HMS Collingwood War Record |others=Part One|journal=Warship|publisher=World Ship Society|location=London|issue=176|pages=29–35|issn=0966-6958}}
*{{cite journal|last=Brady|first=Mark|date=September 2014|title=HMS Collingwood War Record |others=Part Two|journal=Warship|publisher=World Ship Society|location=London|issue=177|pages=19–24|issn=0966-6958}}
*{{Cite book|last=Campbell|first=N. J. M.|title=Jutland: An Analysis of the Fighting|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1986|isbn=0-87021-324-5}}
* {{cite book|last=Friedman|first=Norman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, UK|year=2011|isbn=978-1-84832-100-7}} 
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis|publisher=Naval Institute Press|isbn=0-87021-907-3|last-author-amp=yes}}
* {{cite book|last=Gordon|first=Andrew|title=The Rules of the Game: Jutland and British Naval Command|date=2012|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=978-1-59114-336-9}}
* {{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=1-55750-352-4}}
* {{cite book |last=Jellicoe|first=John|authorlink=John Jellicoe, 1st Earl Jellicoe|title=The Grand Fleet, 1914–1916: Its Creation, Development, and Work|year=1919|location=New York|publisher=George H. Doran Company|oclc=13614571}}
*{{Cite book|first=Denis|last=Judd|year=1982|title=King George VI: 1895–1952|publisher=Michael Joseph|location=London|isbn=0-7181-2184-8}}
* {{cite book|title=[[Castles of Steel: Britain, Germany, and the Winning of the Great War at Sea]] |last=Massie|first=Robert K.|authorlink=Robert K. Massie|publisher=Random House|year=2003|location= New York|isbn=0-679-45671-6}}
* {{cite book|last=Parkes|first=Oscar|title=British Battleships|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1990|orig-year=1957|edition=reprint|isbn=1-55750-075-4}}
* {{cite book|last=Preston|first=Antony|title=Battleships of World War I: An Illustrated Encyclopedia of the Battleships of All Nations 1914–1918|publisher=Galahad Books|location=New York|year=1972|isbn=0-88365-300-1}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
* {{cite book|last=Tarrant|first=V. E.|title=Jutland: The German Perspective: A New View of the Great Battle, 31 May 1916|publisher=Brockhampton Press|location=London|year=1999|orig-year=1995|edition=reprint|isbn=1-86019-917-8}}

==External links==
{{Portal|Battleships}}
{{Commons category|HMS Collingwood (ship, 1908)}}
* [http://www.maritimequest.com/warship_directory/great_britain/battleships/collingwood/hms_collingwood.htm Maritimequest HMS ''Collingwood'' Photo Gallery]

{{St Vincent class battleship}}

{{DEFAULTSORT:Collingwood (1908)}}
[[Category:St. Vincent-class battleships]]
[[Category:World War I battleships of the United Kingdom]]
[[Category:1908 ships]]
[[Category:Plymouth-built ships]]