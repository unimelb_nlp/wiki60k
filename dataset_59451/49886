{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Spectacles
| title_orig    = 
| translator    = 
| author        = [[Edgar Allan Poe]]
| country       = United States
| language      = English
| series        = 
| genre         = [[Comedy]]<br>[[Short story]]
| published_in   = ''Philadelphia Dollar Newspaper''
| publisher     = 
| media_type    = Print ([[Periodical]])
| pub_date  = March 1844
| english_pub_date = 
| preceded_by   = 
| followed_by   = 
}}
"'''The Spectacles'''" is a [[short story]] by [[Edgar Allan Poe]], published in 1844. It is one of Poe's [[comedy]] tales.

==Plot summary==
[[File:Poe the spectacles byam shaw.JPG|thumb|Illustration by [[Byam Shaw]] for a [[London]] edition dated 1909]]
The [[narrator]], 22-year-old Napoleon Bonaparte Froissart, changes his last name to "Simpson" as a requirement to inherit a large sum from a distant cousin, Adolphus Simpson. At the [[opera]] he sees a beautiful woman in the audience and falls in love instantly. He describes her beauty at length, despite not being able to see her well; he requires [[spectacles]] but, in his [[vanity]], "resolutely refused to employ them". His companion Talbot identifies the woman as Madame Eugenie Lalande, a wealthy widow, and promises to introduce the two. He courts her and proposes marriage; she makes him promise that, on their wedding night, he will wear his spectacles.

When he puts on the spectacles, he sees that she is a toothless old woman. He expresses horror at her appearance, and even more so when he learns she is 82 years old. She begins a rant about a very foolish descendant of hers, one Napoleon Bonaparte Froissart. He realizes that she is his great-great-grandmother. Madame Lalande, who is also Mrs. Simpson, had come to America to meet her husband's heir. She was accompanied by a much younger relative, Madame Stephanie Lalande. Whenever the narrator spoke of "Madame Lalande", everyone assumed he meant the younger woman. When the elder Madame Lalande discovered that he had mistaken her for a young woman because of his eyesight, and that he had been openly courting her instead of being civil to a relative, she decided to play a trick on him with the help of Talbot and another confederate. Their wedding was a fake. He ends by marrying Madame Stephanie and vows to "never be met without SPECTACLES" — having acquired a pair of his own at last.

==Publication history and response==
"The Spectacles" was first published in the ''[[Philadelphia Dollar Newspaper]]'' in the March 27, 1844 issue.<ref>Quinn, Arthur Hobson. ''Edgar Allan Poe: A Critical Biography''. Baltimore: The Johns Hopkins University Press, 1998: 400. ISBN 0-8018-5730-9</ref> Critics suggested that the piece was paid by the word, hence its relatively high length, especially for a work of humor. Upon its reprinting in the ''[[Broadway Journal]]'' in March 1845, Poe himself acknowledged he was "not aware of the great length of "The Spectacles" until too late to remedy the evil."

The editor of the ''Dollar Newspaper'' printed "The Spectacles" with the comment that "it is one of the best from [Poe's] chaste and able pen and second only to the popular prize production, '[[The Gold-Bug]].'"<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 455–456. ISBN 0-8161-8734-7</ref> Editor John Stephenson Du Solle reprinted the story in his daily newspaper ''The Spirit of the Times'' in Philadelphia, saying, "Poe's Story of 'The Spectacles' is alone worth double the price of the paper."<ref>Thomas, Dwight & David K. Jackson. ''The Poe Log: A Documentary Life of Edgar Allan Poe, 1809–1849''. Boston: G. K. Hall & Co., 1987: 456. ISBN 0-8161-8734-7</ref> It was first published overseas in the May 3, 1845, issue of London-based ''Lloyd's Entertaining Journal''.<ref>Frank, Frederick S. and Anthony Magistrale. ''The Poe Encyclopedia''. Westport, CT: Greenwood Press, 1997: 200. ISBN 978-0-313-27768-9</ref>

==Major themes==
Besides warning readers to obey their eye doctors, Poe seems to be addressing the concept of "love at first sight" — in fact, the first line of the story points out that "it was the fashion to ridicule the idea." Yet, the story is presented to "add another to the already almost innumerable instances of the truth of the position" that love at first sight does exist. The [[irony]] is that the narrator does not have a "first sight" of the woman he falls in love with, due to his lack of spectacles.

Additionally, the story is based around [[vanity]]. The narrator changes his name, with "much [[wikt:repugnance|repugnance]]", from Froissart to Simpson, "a rather usual and [[plebeian]]" name in order to collect inheritance. His original [[patronym]], he says, elicited in him "a very pardonable pride". This same pride kept him from wearing spectacles. Madame Lalande admits that she was teaching him a lesson.

The name of "Napoleon Bonoparte" makes obvious reference to the Corsican general [[Napoleon]]. The story also has very strong Oedipal tones.{{Citation needed|date=April 2012}}

Scholar Carmen Trammell Skaggs noted that the story, though intended to be humorous, nevertheless showed Poe's awareness of the opera. He references the soprano singer [[Maria Malibran]] and the [[San Carlo Opera Company|San Carlo]] and he also describes vocal technique in a way that implies a close knowledge of the subject.<ref>Skaggs, Carmen Trammell. ''Overtones of Opera in American Literature from Whitman to Wharton''. Baton Rouge: Louisiana State University Press, 2010: 35. ISBN 9780807136751</ref> Skaggs also emphasizes Poe's role as a music critic for the ''[[New-York Mirror|New York Evening Mirror]]'' and, later, the ''[[Broadway Journal]]''.<ref>Skaggs, Carmen Trammell. ''Overtones of Opera in American Literature from Whitman to Wharton''. Baton Rouge: Louisiana State University Press, 2010: 35–36. ISBN 9780807136751</ref>

==Notes==
{{reflist}}

==References==
*{{cite book | title=''Edgar Allan Poe: A to Z. | last=Sova | first=Dawn B. |  publisher=Checkmark Books | location= | edition= | year=2001 | id= }}

==External links==
{{Wikisource|The Spectacles}}
*[http://www.eapoe.org/works/info/pt047.htm Publication history of "The Spectacles"] at the Edgar Allan Poe Society of Baltimore
*[http://norman.hrc.utexas.edu/poedc/details.cfm?id=37 Original manuscript of "The Spectacles"] at the [[Harry Ransom Center]], University of Texas, Austin
* {{librivox book | title=The Works of Edgar Allan Poe, Raven Edition, Volume 3 | author=Edgar Allan POE}}

{{Edgar Allan Poe}}

{{DEFAULTSORT:Spectacles, The}}
[[Category:1844 short stories]]
[[Category:Short stories by Edgar Allan Poe]]
[[Category:Comic short stories]]
[[Category:Works originally published in American newspapers]]
[[Category:Works set in theatres and opera houses]]