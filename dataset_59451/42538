{|{{Infobox Aircraft Begin
  |name = Cessna 205, 206 & 207
  |image = cessna.206h.stationair2.arp.jpg
  |caption = 2000 model Cessna 206H Stationair
}}{{Infobox Aircraft Type
  |type = [[Light aircraft]]
  |manufacturer = [[Cessna]]
  |designer =
  |first flight = 
  |introduced = 1962 (206)
  |retired =
  |status = In production
  |primary user = [[Yute Air Alaska]]
  |more users = [[Argentine Army]]
  |produced = 1962-1986 and 1998-present
  |number built = over 8509 (as of approx. 2006)
  |unit cost = [[United States dollar|US$]]645,000 (T206H, 2016)<ref>George 2016, p. 85</ref>
  |developed from = [[Cessna 210]]
  |variants with their own articles = 
}}
|}

The '''Cessna 205''', '''206''', and '''207''', known variously as the '''Super Skywagon''', '''Skywagon''', '''Stationair''', and '''Super Skylane''' are a family of single-engined, general aviation aircraft with fixed landing gear, used in commercial air service and also for personal use. The family was originally developed from the popular retractable-gear [[Cessna 210]] and is produced by [[Cessna]].

The line's combination of a powerful engine, rugged construction and a large cabin has made these aircraft popular [[bush plane]]s.  Cessna describes the 206 as "the sport-utility vehicle of the air."  These airplanes are also used for aerial photography, skydiving and other utility purposes.  They can also be equipped with floats, amphibious floats and skis.  Alternatively, they can be fitted with luxury appointments for use as a personal air transport.

From 1962 to 2006 Cessna produced 8,509 aircraft in the 205, 206 and 207 variants.

==Development==
[[File:Cessna205C-GHOR03.jpg|thumb|A Cessna 205, showing its distinctive cowling]]
[[File:CessnaT2105A.jpg|thumb|Cessna 205A]]
[[File:Cessna 206 (3011851458).jpg|thumb|Civil Air Patrol Cessna 206]]
[[File:Instrument Panel of a Cessna in Tasmania, jjron, 9.4.2005.jpg|thumb|right|1960s era Cessna U206 instrument panel in-flight]]
[[File:7PANEL 03 01 08.jpg|thumb|2001 Cessna T206H amphibian instrument panel]]
[[File:Bay City Seaplanes Cessna 206H Stationair Creek.jpg|thumb|Cessna 206H Stationair on amphibious floats at [[Eastern Beach, Geelong]] (2006).]]
[[File:Cessna 207-A PH-ZZF.jpg|thumb|Cessna 207A with Soloy turbine engine conversion]]
[[File:Cessna206HStationair03.jpg|thumb|2005 model Cessna T206H Stationair, showing its large "clamshell" cargo doors]]
[[File:Cessna206HStationairG1000InstrumentPanel.jpg|thumb|2005 model Cessna T206H Stationair instrument panel with the [[Garmin G1000]] [[glass cockpit|"glass" cockpit]]]]
[[File:Cessna 207 VH-FIF Flinders MBW 30.01.71 edited-3.jpg|thumb|right|Cessna 207]]

===Cessna 205===
[[File:Cessna 205A (1963) 092.jpg|right|thumb|1963 Cessna 210-5A]]
The Cessna 205 was introduced late in 1962 as a 1963 [[model year]].  The six-seat aircraft was essentially a Cessna 210 with fixed landing gear and with changes to the crew and passenger door arrangement, being officially designated by Cessna as a "Model 210-5".<ref name="3A21">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/1c8849e2325c35988625756e006da735/$FILE/3A21%20Rev%2047.pdf |title = TYPE CERTIFICATE DATA SHEET NO. 3A21 Revision 47|accessdate = 20 March 2010|last = [[Federal Aviation Administration]]|authorlink = |date=February 2009}}</ref>  The 205 retained the early 210’s engine [[cowling]] bulge, originally where the 210 stowed its nosewheel on retraction. This distinctive cowling was made more streamlined on the later Cessna 206.

The 205 is powered by a [[Continental O-470|Continental IO-470-S]] engine producing {{convert|260|hp|abbr=on}}.<ref name="Bluebook">Aircraft Bluebook Spring 2006 Edition Penton Media, Overland Park, KS USA</ref>

The 205 was produced in only two model years - 1963 and 1964 before being replaced in production by the Cessna 206.  A total of 576 Cessna 205s were produced.<ref name="Bluebook"/>

===Cessna 206===
The six-seat Model 206 was introduced as a 1964 model and was built until 1986, when Cessna halted production of its single-engined product line.  It was then re-introduced in 1998 and remains in production in 2013.<ref name="Bluebook"/>

There were many sub-variants, including the U206, P206 all certified to CAR3 standards and later 206H certified to [[Federal Aviation Regulations|FAR]] Part 23.

The total Model 206 production between 1964 and 2004 was 6,581 aircraft.<ref name="Bluebook"/>

====Cessna U206====
The original 1964 model was the U206, powered by a {{convert|285|hp|abbr=on}} [[Continental O-520|Continental IO-520-A]]. The “U” designation indicated “utility” and this model was equipped with a pilot side door and large clamshell rear door serving the back two rows of seats, allowing easy loading of oversized cargo.<ref name="Bluebook"/>

There was a TU206 turbocharged version powered by the [[Continental O-520|Continental TSIO-520-C]] engine producing {{convert|285|hp|abbr=on}}. After 1967 the turbo TU206 was powered by a TSIO-520-F of {{convert|300|hp|kW|abbr=on}}. The extra {{convert|15|hp|abbr=on}} was obtained by turning the engine at a higher rpm, and was allowed for only five minutes.<ref name="Bluebook"/> Due to the large propeller diameter, the additional engine speed meant that the propeller tips were pushed to transonic speeds, which required much more power.

From 1964 to 1969 the U206 was known as the “Super Skywagon”.  From 1970 it was named the “Stationair”, a contraction of “Station Wagon of the Air”, which is a good description of the aircraft's intended role. Sub-variants were designated U206 to U206G.<ref name="Bluebook"/>

In 1977 the U206 had its engine upgraded to a Continental IO-520-F of {{convert|300|hp|kW|abbr=on}} (continuous rating, obtained at a lower speed than the previous IO-520-F) and the TU206 powerplant was changed to the TSIO-520-M producing {{convert|310|hp|abbr=on}}.<ref name="Bluebook"/>

Production of all versions of the U206 was halted in 1986 when Cessna stopped manufacturing all piston-engined aircraft.  A total of 5,208 U206s had been produced.<ref name="Bluebook"/>

====Cessna P206====
The P206 was added to the line in 1965.  In this case the “P” stood for “people”, as the P206 had passenger doors similar to the Cessna 210 from which it was derived, on both sides.<ref name="Bluebook"/>

The P206 was produced from 1965 to 1970 and was powered by a Continental IO-520-A of {{convert|285|hp|abbr=on}}.  There was a turbocharged model designated TP206 which was powered by a Continental TSIO-520-A also of {{convert|285|hp|abbr=on}}.<ref name="Bluebook"/>

647 P206s were produced under the name “Super Skylane” which made it sound like a version of the [[Cessna 182]], which it was not.  Sub-variants were designated P206 to P206E.<ref name="Bluebook"/>

====Cessna 206H====
After a production hiatus of twelve years, Cessna started manufacturing a new version of the venerable 206 in 1998, with the introduction of the newly certified 206H.<ref name="Bluebook"/> The “H” model is generally similar to the previous U206 configuration, with a pilot entry door and a rear double clamshell door for access to the middle and back seats.  The "H" is marketed under the name "Stationair".

The 206H is powered by a [[Lycoming O-540|Lycoming IO-540-AC1A]] powerplant producing {{convert|300|hp|abbr=on}}. The turbocharged T206H is powered by a [[Lycoming O-540|Lycoming TSIO-540-AJ1A]] engine of {{convert|310|hp|abbr=on}}.<ref name="Bluebook"/>

Even though the Cessna 206H is certified as a six-seat aircraft in its country of origin, the Canadian aviation regulator, [[Transport Canada]] has certified it to carry only five people in [[Canada]].  This is due to concerns about passenger egress through the rear clamshell door with the flaps extended.  Cessna addressed one part of this problem early on, after a flight-test aircraft was damaged when the pilot extended the flaps while taxiing, and his passenger had the clamshell door open (for ventilation; it was a hot summer day).  A switch was added to the flap actuation circuit which disabled the flaps when the doors were open.  The other part of the problem is that if the flaps are already down, the passenger must perform the complicated procedure of opening the front part as far as possible (about {{convert|2|in|cm|abbr=on}}) then open the rear door and restow the rear door handle. This then gives enough clearance to open the rear part of the door.

Both the 206H and the T206H remain in production in 2013. By the end of 2004 Cessna had produced 221 206Hs and 505 T206Hs, for a total production of 726 "H" models.<ref name="Bluebook"/>

Cessna has indicated that they do not intend to produce a P206-configuration aircraft in the future, due to lack of market demand.

===Cessna 207===
The Model 207 was a seven- and later eight-seat development of the 206, achieved by stretching the design further by {{convert|45|in|cm|0}}<ref name= "Jane's 1971 p. 280">Taylor  1971, p. 280.</ref> to allow space for more seats. The nose section was extended {{convert|18|in|cm}} by adding a constant-section nose baggage compartment between the passenger compartment and the engine firewall; the aft section was extended by {{convert|27|in|cm|0}}<ref name= "Simpson p. 86">Simpson 2005, p. 86.</ref> by inserting a constant-area section in the fuselage area just aft of the aft wing attach point. Thus the propeller's ground clearance was unaffected by the change (the nosewheel had moved forward the same distance as the propeller), but the tail moved aft relative to the mainwheel position, which made landing (without striking the tailskid on the runway) a greater challenge.  The move gave that airplane a larger turning radius, since the distance between mainwheels and nosewheel increased by {{convert|18|in|cm|0}} but the nosewheel's maximum allowed deflection was not increased.

The 207 was introduced as a 1969 model featuring a Continental IO-520-F engine of {{convert|300|hp|abbr=on}}.  A turbocharged version was equipped with a TSIO-520-G of the same output.<ref name="Bluebook"/>

At the beginning of production the model was called a Cessna 207 “Skywagon”, but in 1977 the name was changed to “Stationair 7”.  1977 also saw a change of engine on the turbocharged version to a Continental TSIO-520-M producing {{convert|310|hp|abbr=on}} – the same engine used in the TU206 of the same vintage.<ref name="Bluebook"/>

The 207 added a seat in 1980 and was then known as the “Stationair 8”.  Production of the 207 was completed in 1984, just two years before U206 production halted.  A total of 626 Cessna 207s were manufactured.<ref name="Bluebook"/>

The Cessna Model 207 has been popular with air taxi companies, particularly on short runs where its full seating capacity could be used.  Very few of these aircraft have seen private use.

===Modifications===
In April 2007 [[Thielert]] announced that the [[European Aviation Safety Agency]] had granted a [[Supplemental Type Certificate]] (STC) for conversion of Cessna 206s to the Thielert V-8 diesel powerplant. The STC allows conversion of the following models: U206F and TU206F with the {{convert|300|hp|abbr=on}} powerplant, and the U206G, TU206G, 206H and T206H with the {{convert|310|hp|abbr=on}} version.<ref>{{cite web|url = http://www.dieselair.com/2006/05/thielert-announces-major-new-products.html|title =  Thielert announces major new products|accessdate = 2007-11-14|last = du Cros|first = Andre Teissier|authorlink = |date=May 2006}}</ref> This modification does not require any changes to the engine cowling. In May 2008, Thielert entered insolvency proceedings, so the future availability of this diesel conversion is uncertain.<ref name="Avweb26Apr08">{{cite web|url = http://www.avweb.com/avwebflash/news/ThielertAircraftEnginesFilesForInsolvency_197735-1.html|title = Thielert Aircraft Engines Files For Insolvency|accessdate = 2008-04-26|last = Niles|first = Russ |authorlink = |date=April 2008}}</ref>

Soloy Aviation Solutions offers a [[turboprop]] conversion for some 206/207 models based on the {{convert|418|shp|abbr=on}} Allison C20S engine/gearbox package. However, extensive engine cowl modifications are required.<ref>{{cite web|url = http://soloy.portal.acrosonic.com/Products/Fixed+Wing+Aircraft/Turbine+Cessna+206+Mark+1/default.aspx|title =  Turbine Cessna 206 Mark 1 Conversion|accessdate = 2007-11-14|last = Soloy Aviation Solutions|first =|authorlink = |year = n.d.}}</ref>

Atlantic Aero offers an FAA [[Supplemental Type Certificate|STC]] conversion to the [[Continental IO-550]] powerplant. No cowl modifications are required.<ref>{{cite web|url = http://www.atlantic-aero.com/main/docs/cessna_206_550_stationair.pdf|title =  550 Stationair|accessdate = 2007-11-14|last = Atlantic Aero|first =|authorlink = |year = n.d.}}</ref>

Both Kenmore Air ([[Edo floats]]) and Wipaire (Wipline floats) offer seaplane conversions.<ref>{{cite web|url =http://www.kenmoreair.com/parts/EDOfloats/floatModels/floatmodels.html|title =  Float Models|accessdate = 2010-07-07|last = Kenmore Air|first =|authorlink = |year = n.d.}}</ref><ref>{{cite web|url = http://www.wipaire.com/wipline/3450.php|title =   Wipline 3450 Floats|accessdate = 2007-11-14|last = Wipaire|first =|authorlink = |year = 2007}}</ref>

==Variants==
[[File:Cessna205C-GHOR02.jpg|thumb|Cessna 205]]
;205 (Model 210-5)
:Original 205 model, six seats, powered by a [[Continental O-470|Continental IO-470-S]] of {{convert|260|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3300|lb|kg|0|abbr=on}} landplane and certified on 14 June 1962 as a variant of the [[Cessna 210]].<ref name="3A21" />
;205A (Model 210-5A)
:Six seats, powered by a [[Continental O-470|Continental IO-470-S]] of {{convert|260|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3300|lb|kg|0|abbr=on}} landplane and certified on 19 July 1963 as a variant of the Cessna 210.<ref name="3A21" />
;206
:Original 206 model, six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3300|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane and certified on 19 July 1963.<ref name="A4CE">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/a9b9134bcf093c9f8625756e0070519e/$FILE/A4CE%20Rev%2047.pdf |title = TYPE CERTIFICATE DATA SHEET NO. A4CE Revision 47|accessdate = 20 March 2010|last = [[Federal Aviation Administration]]|authorlink = |date=February 2009}}</ref>

;U206 Super Skywagon
:First U206 model, six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3300|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane and certified on 8 October 1964.<ref name="A4CE" />
;P206
:First P206 model, six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3300|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane and certified on 8 October 1964.<ref name="A4CE" />
[[File:CessnaU206A.jpg|thumb|right|Cessna U206A]]
;U206A
:Six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 24 September 1965.<ref name="A4CE" />
;P206A
:Six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 24 September 1965.<ref name="A4CE" />
;P206B
:Six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 3 August 1966.<ref name="A4CE" />
;TU206A
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 20 December 1965.<ref name="A4CE" />
;TU206B
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 3 August 1966.<ref name="A4CE" />
;TP206A
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 20 December 1965.<ref name="A4CE" />
;TP206B
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 3 August 1966.<ref name="A4CE" />
;U206B
:Six seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 3 August 1966.<ref name="A4CE" />
;P206C
:Six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 20 July 1967.<ref name="A4CE" />
;TP206C
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 20 July 1967.<ref name="A4CE" />
;P206D
:Six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 18 September 1968.<ref name="A4CE" />
;TP206D
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 18 September 1968.<ref name="A4CE" />
;P206E
:Six seats, powered by a [[Continental O-520|Continental IO-520-A]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 28 July 1969.<ref name="A4CE" />
;TP206E
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 28 July 1969.<ref name="A4CE" />
;{{Visible anchor|U206C}}
:Six seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 20 July 1967.<ref name="A4CE" />
;TU206C
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 20 July 1967.<ref name="A4CE" />
;{{visible anchor|U206D}}
:Six seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 18 September 1968.<ref name="A4CE" />
;TU206D
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 18 September 1968.<ref name="A4CE" />
;U206E
:Six seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 28 July 1969.<ref name="A4CE" />
;TU206E
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 28 July 1969.<ref name="A4CE" />
;{{Visible anchor|U206F}}
:Six seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 26 October 1971.<ref name="A4CE" />
;TU206F
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-C]] of {{convert|285|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3300|lb|kg|0|abbr=on}} skiplane and certified on 26 October 1971.<ref name="A4CE" />
;{{Visible anchor|U206G}}
:Six seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3500|lb|kg|0|abbr=on}} seaplane and certified on 21 June 1976.<ref name="A4CE" />
;TU206G
:Six seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-M]] of {{convert|310|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane, {{convert|3600|lb|kg|0|abbr=on}} seaplane, {{convert|3600|lb|kg|0|abbr=on}} amphibian and certified on 21 June 1976.<ref name="A4CE" />
;206H
:Six seats, powered by a [[Lycoming O-540|Lycoming IO-540-AC1A5]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane and certified on 26 November 1997.<ref name="A4CE" />
[[File:M-AXIM-Cessna-T206H-Popham-4361.jpg|thumb|Cessna T206H]]
;T206H
:Six seats, powered by a turbocharged [[Lycoming O-540|Lycoming TIO-540-AJ1A]] of {{convert|310|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3600|lb|kg|0|abbr=on}} landplane and certified on 1  October 1998.<ref name="A4CE" />
;{{visible anchor|207 Skywagon}}
:Original 207 model, seven seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3800|lb|kg|0|abbr=on}} landplane and certified on 31 December 1968.<ref name="A16CE">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/e07f8687774465828625756e00707c40/$FILE/A16CE%20Rev%2022.pdf |title = TYPE CERTIFICATE DATA SHEET NO. A16CE Revision 22|accessdate = 20 March 2010|last = [[Federal Aviation Administration]]|authorlink = |date=February 2009}}</ref>
;T207 Turbo Skywagon
:Seven seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-G]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3800|lb|kg|0|abbr=on}} landplane and certified on 31 December 1968.<ref name="A16CE" />
;{{visible anchor|207A Skywagon/Stationair 8}}
:Seven seats, powered by a [[Continental O-520|Continental IO-520-F]] of {{convert|300|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3800|lb|kg|0|abbr=on}} landplane and certified on 12 July 1976. Certified for eight seats on 11 September 1979.<ref name="A16CE" />
;T207A Turbo Skywagon/Turbo Stationair 8
:Seven seats, powered by a turbocharged [[Continental O-520|Continental TSIO-520-M]] of {{convert|310|hp|kW|0|abbr=on}}, with a gross weight of {{convert|3800|lb|kg|0|abbr=on}} landplane and certified on 12 July 1976. Certified for eight seats on 11 September 1979.<ref name="A16CE" />

==Operators==

===Civil===
The aircraft is popular with air charter companies and small cargo air carriers, and is operated by private individuals and companies. One of the largest Cessna 207 operators was [[Flight Alaska|Yute Air Alaska]], which had a fleet of 12 aircraft.<ref name="Avweb23Aug10">{{cite web|url = http://www.avweb.com/podcast/podcast/MattSullivan_YuteAirAlaska_30000HoursOnGravelStrips_203170-1.html?kw=AVwebAudio|title = 30,000 Hours on Gravel|accessdate = 14 August 2010|last = Niles|first = Russ|authorlink = |date=August 2010}}</ref>

===Government===
{{CAN}}
*[[Ottawa Police Service]] - one U206G in the [[surveillance aircraft]] role<ref name="TCCAR">{{cite web|url = http://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/Menu.aspx|title = Canadian Civil Aircraft Register|accessdate = 3 June 2015|last = [[Transport Canada]]|date = 3 June 2015}}</ref>
{{USA}}
*[[Nebraska Highway Patrol]] 
*[[North Dakota Highway Patrol]] 
*[[North Dakota Fish and Game]] 
*[[South Dakota Highway Patrol]]
*[[Maricopa County Sheriff's Office]]

===Military===
;{{ARG}}
*[[Argentine Army]], six × T207<ref name="andrade13">Andrade 1982, p. 13</ref>
;{{BOL}}
*[[Bolivian Air Force]], two × U206C and 7 × TU206G<ref name="andrade27">Andrade 1982, p. 27</ref>
;{{COL}}
;{{CHI}}
*[[Chilean Air Force]], Operated between 1974 and 1980 the very first Cessna 206 (c/n 0001) as FACh 415, then sold it to civilian market.<ref>{{cite web|url=http://modocharlie.com/2008/06/el-primero-de-muchos/ |title=El primero de muchos |publisher=ModoCharlie |date=2014-03-09 |accessdate=2014-03-23}}</ref>
;{{DJI}}
*[[Djibouti Air Force]], one × U206G<ref name="andrade55">Andrade 1982, p. 55</ref>
;{{DOM}}
;{{ECU}}
*[[Ecuadorian Air Force]]<ref>[http://www.scramble.nl/ec.htm Ecuador Air Arms - Fuerza Aérea Ecuatoriana] {{webarchive |url=https://web.archive.org/web/20100731153318/http://www.scramble.nl/ec.htm |date=July 31, 2010 }}</ref>
;{{GUY}}
;{{IND}}
;{{ISR}}
*[[Israeli Air Force]]
;{{MEX}}
*[[Mexican Air Force]]<ref>Flores 2001, p. 301.</ref>
;{{MYS}}
;{{PAR}}
* [[Paraguayan Air Force]] 5 U206G
* [[Paraguayan Naval Aviation]]  4 U206A/C
* [[Paraguayan Army Aviation]]  2 U206G
;{{PER}}
;{{PHI}}
* [[Philippine Army]]
;{{URY}}

==Accidents==
*December 4, 1971 – Eastern Airlines Flight 898 (a [[McDonnell Douglas DC-9|Douglas DC-9-31]]) collided with a Cessna 206 (''N2110F'') while landing at [[Raleigh-Durham International Airport]]. The Cessna crashed, killing two people on the plane, but the DC-9 landed safely.<ref name="NYC72AN074">{{cite web|url = http://www.ntsb.gov/aviationquery/brief.aspx?ev_id=62879&key=0 |title = NTSB Identification: NYC72AN074 |accessdate = 2014-06-18 |last = [[National Transportation Safety Board]] |authorlink = |date=December 1971}}</ref><ref name="NYC72AN074a">{{cite web|url = http://www.ntsb.gov/aviationquery/brief.aspx?ev_id=62880&key=0|title = NTSB Identification: NYC72AN074|accessdate = 2014-06-18|last = [[National Transportation Safety Board]] |authorlink = |date=December 1971}}</ref>
*July 24, 1972 near [[Aspen, Colorado]], a 27-year-old student pilot with a total of 39 hours of flying time flew into a blind canyon and stalled the aircraft while trying to turn around, killing all four people on board. Among the passengers was wealthy playboy, entrepreneur, racing driver and developer [[Lance Reventlow]], who was a  Woolworth heir, son of [[Barbara Hutton|Barbara Woolworth Hutton]] and the husband of former Mouseketeer and actress [[Cheryl Holdridge]].<ref>{{cite web|url=http://www.ntsb.gov/aviationquery/brief.aspx?ev_id=65716&key=0 |title=DEN73AD003 |publisher=Ntsb.gov |accessdate=2013-04-24}}</ref>

==Specifications (206H Stationair)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's All The World's Aircraft 2003–2004<ref name="JAWA03 p590-4">Jackson 2003, pp. 590–591.</ref>
|crew=one
|capacity=five passengers
|payload main=
|payload alt=
|length main= 28 ft 3 in
|length alt= 8.61 m
|span main= 36 ft 0 in
|span alt= 10.97 m
|height main= 9 ft 3½ in
|height alt= 2.83 m
|area main= 175.5 ft²
|area alt= 16.30 m²
|airfoil=NACA 2412 (modified)
|empty weight main= 2,176 lb
|empty weight alt= 987 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main=
|useful load alt= 
|max takeoff weight main= 3,600 lb
|max takeoff weight alt= 1,632 kg
|more general=
|engine (prop)= [[Lycoming IO-540]]-AC1A
|type of prop=air-cooled [[flat-six]] engine
|number of props=1
|power main= 300 hp
|power alt= 224 kW
|power original=
|max speed main=174 mph
|max speed alt=151 knots, 280 km/h
|max speed more=at sea level
|cruise speed main=163 mph
|cruise speed alt=142 knots, 263 km/h
|cruise speed more=at 6,200 ft (1,890 m)
|stall speed main=63 mph 
|stall speed alt= 54 knots, 100 km/h
|never exceed speed main= 
|never exceed speed alt= 
|range main= 840 mi
|range alt=730 [[nautical mile|nmi]], 1,352 km
|ceiling main= 15,700 ft
|ceiling alt= 4,785 m
|climb rate main=  988 ft/min
|climb rate alt= 5.0 m/s
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent|
|related=
* [[Cessna 182]]
* [[Cessna 210]]
|similar aircraft=
* [[Piper Saratoga]]
|lists=
|see also=
}}

==References==
{{Reflist|30em}}

== Bibliography ==
* [https://web.archive.org/web/20061114141840/http://stationair.cessna.com:80/spec_perf.chtml Cessna 206H Specifications]
* [https://web.archive.org/web/20070705015153/http://info.thielert.com:80/centurion/main/news_start.php?newsid=343 Thielert Aircraft Engines GmbH Press release about Cessna 206 engine development]
* [http://www.soloy.com/206specs.html Cessna 206 Soloy Specification]
*{{cite book |last= Andrade|first= John|authorlink= |title= Militair 1982|year= 1982|publisher= Aviation Press Limited|location= London|isbn=0-907898-01-7}}
*{{cite magazine|last=Flores|first=Santiago A.|title=From Cavalry to Close Air Support|magazine=[[Air International]]| date=May 2001| volume=60 |issue=5| pages=298–303|issn=0306-5634}}
* {{cite magazine|last=George |first=Fred |url= http://www.sellajet.com/adpages/BCA-2016.pdf |title=2016 Business Airplanes Purchase Planning Handbook |magazine=Business & Commercial Aviation |pages=72-102 |publisher=Penton |date=May 2016 |access-date=16 February 2017}}
*{{cite book|last=Taylor |first=John W.R. |title= Jane's All The World's Aircraft 1971–1972 |year=1971 |publisher= Jane's Yearbooks|location=London, UK |isbn= }}
*{{cite book|last=Jackson |first=Paul |title= Jane's All The World's Aircraft 2003–2004 |year=2003 |publisher= Jane's Information Group|location=Coulsdon, UK |isbn= 0-7106-2537-5}}
*{{cite book|last=Simpson |first=Rod |title= The General Aviation Handbook |year=2005 |publisher= Midland Publishing|location=Hinckley, UK |isbn= 1-85780-222-5}}

==External links==
{{commons category|Cessna 206}}
{{commons category|Cessna 207}}
* [http://stationair.cessna.com/ Cessna's Stationair webpage]
* [http://www.globalsecurity.org/military/systems/aircraft/cessna-206.htm Cessna 206] at GlobalSecurity.org

<!-- Banners -->
{{Cessna}}
{{US utility aircraft}}

[[Category:Cessna aircraft|206]]
[[Category:United States civil utility aircraft 1960–1969]]
[[Category:1962 introductions]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]