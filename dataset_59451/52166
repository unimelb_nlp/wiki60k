{{Infobox publisher
| image        = 
| parent       = 
| status       = 
| founded      = 1936
| founder      = 
| successor    = 
| country      = [[UK]]
| headquarters = [[London]]
| distribution = 
| keypeople    = 
| publications = [[Book]]s, [[academic journal]]s
| topics       = 
| genre        = 
| imprints     = 
| revenue      = 
| numemployees = 
| nasdaq       = 
| url          = {{URL|http://www.lwbooks.co.uk}}
}}

'''Lawrence & Wishart''' is a British publishing company formerly associated with the [[Communist Party of Great Britain]]. It was formed in 1936, through the merger of Martin Lawrence, the [[Communist Party of the Soviet Union|Communist Party]]'s press, and Wishart Ltd, a family-owned liberal and anti-fascist publisher.<ref name="Lawrence and Wishart About Us">[http://www.lwbooks.co.uk/about.html Lawrence and Wishart About Us]</ref>

It publishes the journals ''[[New Formations]]'', ''[[Anarchist Studies]]'', ''[[Renewal (magazine)|Renewal]]'', ''Twentieth Century Communism'', ''Socialist History'' and ''[[Soundings (journal)|Soundings]]''.<ref>[http://www.lwbooks.co.uk/journals.html Lawrence and Wishart Journals]</ref>

==History==

Founded in 1936, Lawrence & Wishart initially became involved with the political and cultural life of the [[Popular Front]], publishing literature, drama and poetry, as well as political economy, working-class history and the classics of [[Marxism]].

Post-war, the company published work from the [[Communist Party Historians Group|CPGB's History Group]], including early work by [[Eric Hobsbawm]]. Later in the century Lawrence & Wishart began its project of commissioning translations of selected writings by [[Antonio Gramsci]] whose work on the relationship between politics and culture has ongoing significance for the company. The first volume dedicated to Gramsci, ''Selections from the Prison Notebooks'',<ref>{{Cite web|url=https://www.lwbooks.co.uk/book/selections-prison-notebooks|title=Selections from the Prison Notebooks|date=2005-01-11|access-date=2016-07-19}}</ref> was published in 1971, and re-published in 2005.

The mid-1980s saw the publication of works by writers who brought the insights of cultural studies to bear on more traditional political concerns with ideology, politics and power, a field of research that ultimately led to the range of journals that Lawrence and Wishart now publish.<ref name="Lawrence and Wishart About Us"/>

==Lawrence and Wishart today==

Continuing to publish the journals ''[[New Formations]]'', ''[[Anarchist Studies]]'', ''[[Renewal (magazine)|Renewal]]'', ''Twentieth Century Communism'', and ''[[Soundings (journal)|Soundings]]'', Lawrence and Wishart are also developing their work with online books. Recent online publications have included ''Regeneration'', on generational politics,<ref>[http://www.lwbooks.co.uk/ebooks/Regeneration.html Regeneration]</ref> and the Soundings collection ''The Neoliberal Crisis''.<ref>[http://www.lwbooks.co.uk/ebooks/NeoliberalCrisis.html ''The Neoliberal Crisis'']</ref> The company has also published books with a number of partners, including [[Compass (think tank)|Compass]],<ref>[http://www.compassonline.org.uk/publications/item.asp?d=214  ''A New Political Economy'']</ref> the [[International Brigade Memorial Trust]],<ref>[http://www.international-brigades.org.uk/merchandise.htm ''Antifacista: British & Irish Volunteers in the Spanish Civil War'']</ref> the [[Marx Memorial Library]],<ref>[http://www.lwbooks.co.uk/books/archive/marxinlondon.html ''Marx In London'']</ref> the [[Socialist History Society]],<ref>[http://www.lwbooks.co.uk/books/archive/John_Saville.html ''John Saville: Commitment and History'']</ref>  [[UNISON]]<ref>[http://www.lwbooks.co.uk/books/archive/leadership.html ''Leadership and Democracy: A History of the National Union of Public Employees Vol 2 1928-1993'']</ref> and [[Unite the Union]].<ref>[http://www.lwbooks.co.uk/books/archive/t&gstory.html ''The T&G Story: A History of the Transport and General Workers Union'']</ref>

In August 2016 Lawrence & Wishart moved premises from [[Central Books]] in Hackney Wick, to a new premises in Chadwell Heath. The move was a result of a combination of increased rents within Hackney Wick due to [[gentrification]] and regeneration of the area due to the construction or the [[Queen Elizabeth Olympic Park]]; and the convenience of the company to remain close to Central Books, who are also Lawrence & Wishart's distributor.

==Copyright issue on Marx/Engels Collected Works==
The [[Marx/Engels Collected Works]] were published by Lawrence and Wishart, in collaboration with others, between 1975 and 2005 in 50 volumes, and is the most complete attempt at rendering their work in English.

According to Andy Blunden, a volunteer at the [[Marxists Internet Archive]], L&W allowed the website to publish material from the MECW around 2005, but it was always an option of the publishers to revoke permission.<ref name="Howard">Jennifer Howard [http://chronicle.com/article/Readers-of-MarxEngels/146251/ "Readers of Marx and Engels Decry Publisher’s Assertion of Copyright"], ''Chronicle of Higher Education'', 29 April 2014</ref> This finally happened at the end of April 2014 when the publishers asked via email for the deletion of 1,662 files from the website threatening legal action if MIA did not comply.<ref name="Howard"/><ref name="Cohen">Noam Cohen [https://www.nytimes.com/2014/05/01/arts/claiming-a-copyright-on-marx-how-uncomradely.html?hpw&rref=books&_r=0 "Claiming a Copyright on Marx? How Uncomradely"], ''New York Times'', 30 April 2014</ref><ref name="Tobar">Hector Tobar  [http://www.latimes.com/books/jacketcopy/la-et-jc-radicals-fight-over-a-karl-marx-copyright-20140429,0,303004.story#axzz30PWRhD34 "Radicals fight over a Karl Marx copyright"], ''Los Angeles Times'', 29 April 2014</ref> Lawrence and Wishart has argued that the free reprints on MIA will affect its plans to issue MECW in a digital version in the near future, and its action is purely to ensure that it remains in business.<ref name="Howard"/>

By the end of April 2014, more than 4,500 individuals had signed a petition objecting to Lawrence and Wishart's decision to take action against MIA.<ref name="Cohen"/> In response to the criticism, Lawrence and Wishart in a statement said it had been "subject to a campaign of online abuse."<ref name="Tobar"/>

==References==

<references />

==External links==
* [http://www.lwbooks.co.uk/about.html About Lawrence and Wishart]

[[Category:Book publishing companies of the United Kingdom]]
[[Category:Political book publishing companies]]