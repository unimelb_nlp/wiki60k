{{Infobox journal
| title         = Icarus
| cover         = 
| editor        = [[Philip D. Nicholson]]
| discipline    = [[Planetary science]]
| peer-reviewed = 
| language      = [[English language|English]]
| abbreviation  = Icarus
| publisher     = [[Elsevier]]
| country       = [[United States]]
| frequency     = Monthly
| history       = 1962–present
| openaccess    = 
| license       = 
| impact        = 3.161
| impact-year   = 2012
| website       =http://www.journals.elsevier.com/icarus/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 1752499
| LCCN          = 
| CODEN         = 
| ISSN          = 0019-1035
| eISSN         = 
| boxwidth      = 
}}
'''''Icarus''''' is a [[scientific journal]] dedicated to the field of [[planetary science]].  Its longtime owner and publisher was [[Academic Press]], which was then purchased by [[Elsevier]]. It is published under the auspices of the [[American Astronomical Society]]'s [[Division for Planetary Sciences]] (DPS). The journal contains articles discussing the results of new research on [[astronomy]], [[geology]], [[meteorology]], [[physics]], [[chemistry]], [[biology]], and other scientific aspects of the [[Solar System]] or [[Extrasolar planet|extrasolar systems]].

The journal was founded in 1962, and became affiliated with the DPS in 1974. [[Carl Sagan]] served as editor of the journal from 1968 to 1979.  He was succeeded by [[Joseph A. Burns]] (1980–1997) and [[Phil Nicholson|Philip D. Nicholson]] (1998–present).<ref>
{{cite journal
 |author=C. Sagan
 |date=1980
 |title=Editorial
 |journal=Icarus
 |volume=41 |issue=1 |page=iii
 |bibcode=1980Icar...41D...3S
 |doi=10.1016/0019-1035(80)90155-4
}}</ref><ref>
{{cite journal
 |author=J. A. Burns
 |date=1997
 |title=Thanks for the Memories
 |journal=Icarus
 |volume=130 |issue=2 |pages=225
 |bibcode=1997Icar..130..225B
 |doi=10.1006/icar.1997.5859
}}</ref>

The journal is named for the mythical [[Icarus]], and the frontispiece of every issue contains an extended quotation from [[Arthur Stanley Eddington|Sir Arthur Eddington]] equating Icarus' adventurousness with the scientific investigator who "strains his theories to the breaking-point till the weak joints gape."<ref>[http://www.journals.elsevier.com/icarus/ Journal Homepage]</ref>

==Abstracting and indexing==
This journal is indexed by the following services:<ref name=hvd-hollis>
{{Cite web
  | author = Online catalog
  | title = Icarus
  | work = Hollis Classic Library
  | publisher = Harvard University
  | date = 
  | url =http://lms01.harvard.edu/F/896C3P7UA7NUVV7EX48RTIY411D1J13MHSMQJAXKNT92CH1JQX-44378?func=find-acc&acc_sequence=022642552
  | format =via World Cat 
  | doi = 
  | accessdate =2012-08-12}}</ref><ref name=trml>
{{Cite web
  | author = 
  | title = Master Journal List
  | work = 
  | publisher = Thomson Reuters
  | date = 
  | url =http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0019-1035
  | format = 
  | doi = 
  | accessdate =2012-08-12}}</ref>
* [[Science Citation Index]]
* [[Current Contents]] /Physical, Chemical & Earth Sciences
* [[Inspec#Print counterparts|Computer & control abstracts]]  
* [[Inspec#Print counterparts|Electrical & electronics abstracts]]   
* [[Inspec#Print counterparts|Physics abstracts. Science abstracts. Series A]] 
* [[GeoRef]] 
* [[Chemical Abstracts Service]] 
* [[CSA (database company)|International aerospace abstracts]] 
* [[CSA (database company)|Energy research abstracts]]

==References==
{{reflist}}

==External links==
* {{official website|http://www.journals.elsevier.com/icarus/}}

[[Category:Planetary science]]
[[Category:Astronomy journals]]
[[Category:Publications established in 1962]]
[[Category:Elsevier academic journals]]
[[Category:American Astronomical Society academic journals]]