{{no footnotes|date=July 2014}}
{{Infobox Journal
| cover	=	[[Image:Biographyjournalcover.gif]]
| discipline	=	[[literature]]
| abbreviation	=	''Bio''
| frequency	=	quarterly
| website	=	http://www.uhpress.hawaii.edu/journals/bio/
| publisher	=	[[University of Hawaii Press]]
| country	=	[[United States|USA]]
| history	=	1978–present
| ISSN	=	0162-4962
| eISSN	=	1529-1456
}}
'''''Biography: An Interdisciplinary Quarterly''''' is an international, academic journal that provides a forum for biographical scholarship. Its articles explore the theoretical, generic, historical, and cultural dimensions of life-writing; and the integration of literature, history, the arts, and the social sciences as they relate to biography. It also offers reviews, concise excerpts of reviews published elsewhere, an annual bibliography of works about biography, and listings of upcoming events, calls for papers, and news from the field.

The journal was founded in 1978 by [[George Simson]], a professor of literature at the [[University of Hawaii]]. Simson also founded the nonprofit [[Biographical Research Center]] (BRC) and the University's Center for Biographical Research (CBR).
The journal is owned by the BRC and published by the [[University of Hawaii Press]]. For several years Professor [[Donald James Winslow]] of the [[University of Boston]] was the magazine's bibliographer. In 1994, the editorship passed to Craig Howes at the University of Hawaii, who gradually expanded the content and instigated a redesign in 1999. The journal's first electronic edition appeared in 2000 on [[Project MUSE]].

''Biography'' appears quarterly, in Winter, Spring, Summer, and Fall issues. From volume 22 (1999), the first issue of each year has contained a collection of articles on a particular theme, the first one serving as a [[Festschrift]] for George Simson. The final issue of each year contains an annual bibliography of works on life-writing.

==External links==
* [http://www.hawaii.edu/biograph/ Sponsor homepage]
* [http://www.uhpress.hawaii.edu/journals/bio/ Publisher homepage]
* [http://muse.jhu.edu/journals/bio/ MUSE homepage]

[[Category:Biography journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1978]]
[[Category:1978 establishments in Hawaii]]