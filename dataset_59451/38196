{{Good article}}
{{Use British English|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox University Boat Race
| name= 22nd Boat Race
| winner = Oxford
| margin = 4 lengths
| winning_time= 21 minutes 24 seconds
| overall = 10–12
| umpire = [[Joseph William Chitty]]<br>(Oxford)
| date= {{Start date|1865|4|8|df=y}}
| prevseason= [[The Boat Race 1864|1864]]
| nextseason= [[The Boat Race 1866|1866]]
| reserve_winner = 
| women_winner = 
}}

The '''22nd Boat Race''' between crews from the [[University of Oxford]] and the [[University of Cambridge]] took place on the [[River Thames]] on 8 April 1865.  Oxford won by four lengths in a time of 21 minutes 24 seconds.  The race, described as "one of the most sensational races in this history" thus far, was umpired by [[Joseph William Chitty]]. It was the first time that a crew had won the Boat Race having been behind at [[Hammersmith Bridge]].
==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1864|1864 race]] by nine lengths, with Oxford leading overall with eleven victories to Cambridge's ten.<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 20 August 2014}}</ref>

Cambridge were coached by their non-rowing president J. G. Chambers (who had rowed in the [[The Boat Race 1862|1862]] and [[The Boat Race 1863|1863 races]]),<ref>Burnell, p. 110</ref> while Oxford's coach was G. Morrison (who had rowed for Oxford three times as well as being a non-rowing president in 1862).<ref>Burnell, p. 111</ref>  Both university boats were constructed by [[Salters Steamers|J. & S. Salter of Oxford]] using cedar wood.<ref>MacMichael, pp. 318&ndash;319</ref>  The race was umpired by [[Joseph William Chitty]] who had rowed for Oxford twice in 1849 (in the [[The Boat Race 1849 (March)|March]] and [[The Boat Race 1849 (December)|December races]]) and the [[The Boat Race 1852|1852 race]], while the starter was Edward Searle.<ref>Burnell, pp. 49, 97</ref>

==Crews==
The Oxford crew weighed an average of 11&nbsp;[[Stone (unit)|st]] 11.25&nbsp;[[Pound (mass)|lb]] (74.8&nbsp;kg), {{convert|1.25|lb|kg|1}} per rower more than their Light Blue opposition.<ref name=mac314/>  Cambridge saw the return of five former [[Blue (university sport)|Blues]], including the [[Coxswain (rowing)|cox]] Francis Archer and number four [[Robert Kinglake]], both of whom had represented the university twice before.<ref name=burn58>Burnell, p. 58</ref>  Three of Oxford's crew had rowed in the previous year's race.<ref name=burn58/> 
[[File:Lawes CB Vanity Fair 1883-05-12.jpg|right|upright|thumb|[[Charles Bennett Lawes]] rowed at [[Stroke (rowing)|stroke]] for Cambridge.]]
{| class=wikitable
|-
! rowspan="2" |Seat
! colspan="3" |Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
! colspan="3" |Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
|-
| Name
| College
| Weight
| Name
| College
| Weight
|-
| [[Bow (rowing)|Bow]] || H. Watney || [[Lady Margaret Boat Club]] || 11 st 1 lb || R. T. Raikes || [[Merton College, Oxford|Merton]] || 11 st 0 lb
|-
| 2 || M. H. L. Beebee || [[Lady Margaret Boat Club]] || 10 st 12 lb || H. P. Senhouse || [[Christ Church, Oxford|Christ Church]] || 11 st 1 lb
|-  
| 3 || E. V. Pigott || [[Corpus Christi College, Cambridge|Corpus Christi]] || 11 st 12 lb || E. F. Henley || [[Oriel College, Oxford|Oriel]] || 12 st 13 lb
|-
| 4 || [[Robert Kinglake|R. A. Kinglake]] || [[Trinity College, Cambridge|3rd Trinity]] || 12 st 8 lb || G. C. Coventry || [[Pembroke College, Oxford|Pembroke]] || 11 st 12 lb
|- 
| 5 || D. F. Steavenson || [[Trinity Hall, Cambridge|Trinity Hall]] || 12 st 4 lb || A. Morrison (P) || [[Balliol College, Oxford|Balliol]] || 12 st 6 lb
|-
| 6 || G. Borthwick ||  [[Trinity College, Cambridge|1st Trinity]] || 11 st 13 lb || T. Wood || [[Pembroke College, Oxford|Pembroke]] || 12 st 2 lb
|-
| 7 || W. R. Griffiths ||  [[Trinity College, Cambridge|3rd Trinity]] || 11 st 8.5 lb || H. Schneider || [[Trinity College, Oxford|Trinity]] || 11 st 10 lb
|-
| [[Stroke (rowing)|Stroke]] || [[Charles Bennett Lawes|C. B. Lawes]] ||  [[Trinity College, Cambridge|3rd Trinity]] || 11 st 7 lb || M. Brown || [[Trinity College, Oxford|Trinity]] || 11 st 4 lb
|-
| [[Coxswain (rowing)|Cox]] || F. H. Archer || [[Corpus Christi College, Cambridge|Corpus Christi]] || 7 st 3 lb || C. R. W. Tottenham || [[Christ Church, Oxford|Christ Church]] || 7 st 13 lb
|-
!colspan="7"|Source:<ref name=mac314>MacMichael, p. 314</ref><br>(P) &ndash; boat club president (J. G. Chambers was the non-rowing president of [[Cambridge University Boat Club]])<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Both crews had initially returned to the boathouses upon the instruction of their boat club president's who had demanded the [[Steamboat|steamers]] stay behind the start.<ref name=burn7>Burnell, p. 7</ref>  Oxford won the [[coin flipping|toss]] and elected to start from the Middlesex station, handing the Surrey side of the river to Cambridge.  Following an indifferent start from both crews, Cambridge took the lead, and were a length ahead by Bishop's Creek.  To avoid the steamers pressing from behind, the Light Blue [[Coxswain (rowing)|cox]] Archer steered his boat to the middle to the river, while his counterpart, Charles Tottenham, manoeuvred too close to the bank, resulting in a two-length lead for Cambridge by [[Craven Cottage]].<ref name=burn58>Burnell, p. 58</ref><ref>MacMichael, pp. 314&ndash;315</ref>  By the Crab Tree pub, the lead was three lengths.  Despite dominating the race, by the time the crews passed below [[Hammersmith Bridge]], Oxford had reduced the lead to three-quarters of a length, and at The Doves pub, the lead was down to half a length.<ref name=mac315>MacMichael, p. 315</ref>  

Soon after the Oxford boat overlapped their opponents yet Cambridge pushed away again before [[Chiswick Eyot]] where the Dark Blues once began to overlap the Light Blues' stern.  Cambridge's [[stroke (rowing)|stroke]] Lawes reacted and pushed to keep the lead, but by Chiswick Church the crews were level.  Oxford's better line in the river saw them draw away rapidly from Cambridge, whereupon the Light Blues were regarded as "falling to pieces".<ref>MacMichael, p. 316</ref>  Oxford's lead at [[Barnes Railway Bridge|Barnes Bridge]] was around three lengths,<ref>MacMIchael, p. 317</ref> and by the time they passed the finishing line they had won by four lengths in a time of 21 minutes 24 seconds, securing the Dark Blue's fifth consecutive win and taking the overall record to 12&ndash;10 in their favour.<ref name=results/>  Drinkwater suggested that, of the 22 races conducted thus far, it was "one of the most sensational races in this history", while Burnell described it as a "splendid race".<ref name=burn7/><ref name=drink55>Drinkwater, p. 55</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}
*{{Cite book | url = https://books.google.com/books?id=uJ8CAAAAYAAJ&dq=%22boat%20race%22%20oxford%20cambridge&pg=PA37 | title = The Oxford and Cambridge Boat Races: From A.D. 1829 to 1869| first = William Fisher | last = MacMichael | publisher = Deighton | date = 1870}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1865}}
[[Category:1865 in English sport]]
[[Category:The Boat Race]]
[[Category:April 1865 sports events]]