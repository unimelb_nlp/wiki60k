<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Airbike & Tandem Airbike
 | image=ISON Airbike Photo 1.jpg
 | caption=An Airbike at [[Sun 'n Fun]] 2004
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[TEAM Aircraft]]<br />[[ISON Aircraft]]<br />[[Jordan Lake Aero]]
 | designer=[[Wayne Ison]]
 | first flight=1994
 | introduced=1994
 | retired=
 | status=In production (2013)
 | primary user=
 | number built=127 (December 1999)<ref name="KitplanesDec1998">Kitplanes Staff: ''1999 Kit Aircraft Directory'', Kitplanes, Volume 15, Number 12, December 1998, page 70. Primedia Publications. IPM 0462012</ref>
 | developed from= 
 | variants with their own articles=
}}
|}

The '''ISON Airbike''' and '''Tandem Airbike''' are a family  of [[United States|American]] high-wing, [[tractor configuration]] [[ultralight aircraft]], that were available in kit form. The single-seat Airbike was introduced in 1994 and the two-seat Tandem Airbike was unveiled in 1996.<ref name="KitplanesDec1998" /><ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, pages B-3 & B-68. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref>

Originally produced by TEAM Aircraft of [[Bradyville, Tennessee]], manufacturing passed to ISON Aircraft, also of Bradyville, before the end of kit production. Starting circa 2009 kits became once again available once again from [[Jordan Lake Aero]].<ref name="JLA">{{cite web|url = http://jordanlakeaero.com/models.html|title = Airbike Models|accessdate = 13 February 2013|last = Jordan Lake Aero|year = 2009}}</ref>

==Development==

The single seat Airbike was designed to meet the requirements of the United States [[Ultralight aircraft (United States)|FAR 103 ''Ultralight Vehicles'']], including the maximum {{convert|254|lb|kg|0|abbr=on}} empty weight. The tandem-seat model was intended to be licensed as an ultralight trainer or an [[homebuilt aircraft|amateur-built aircraft]].<ref name="Cliche" />

The single-seater can achieve an empty weight as low as {{convert|251|lb|kg|0|abbr=on}} with the use of a light-weight engine, such as the {{convert|28|hp|kW|0|abbr=on}} [[Rotax 277]] or the {{convert|22|hp|kW|0|abbr=on}} [[Zenoah G-25]].<ref name="Cliche" />

The name ''Airbike'' was chosen for the aircraft because it has a narrow [[fuselage]] and the pilot's feet rest on rudder pedals that are on the outside of the aircraft, in a similar manner to a motorcycle.<ref name="Cliche" />

The Tandem Airbike retains all of the single-seater's features and has a stretched fuselage to accommodate the second seat. It uses a wing of {{convert|33.8|ft|m|1|abbr=on}} [[Wingspan|span]] with an area of 152 sq ft (14.14 sq m).<ref name="Cliche" />

==Design==
[[File:ISON Airbike Photo 2.jpg||thumb|right|An Airbike at [[Sun 'n Fun]] 2004 showing the external rudder pedals]]
Both variants feature a [[parasol wing]] constructed from wood and covered with aircraft fabric. The wing has full-span [[ailerons]] or, in the case of the two-seater, optional electrically-actuated [[flaperon]]s. All controls are cable-operated. The [[elevator (aircraft)|elevator]] and [[rudder]] are conventional.<ref name="Cliche" />

The fuselage is made from welded [[41xx steel|4130 steel]] tube and the aircraft has [[conventional landing gear]] with tail wheel steering connected to the rudder pedals. The main landing gear utilises sprung-tubes for suspension and absorbing landing loads.<ref name="Cliche" />

The Airbike was sold as an assembly [[homebuilt aircraft|kit]]. The kit included a pre-welded fuselage and tail, pre-built main wing spars and ribs, all brackets and fittings, landing gear, engine, propeller, instruments and a five gallon fuel tank. The company estimated the time to complete the aircraft at 150 hours for the single-seater. The price in 2001 for the single-seat Airbike was [[United States Dollar|US$]]7195<ref name="Cliche" />

The Tandem Airbike had a factory estimated construction time of 200–300 hours or 100–150 hours if the ''quick-built kit option'' was purchased. In 2001 the kits price was US$8000 without engine or propeller.<ref name="Cliche" />

==Operational history==

In December 1998 the company reported that 127 single-seaters were flying, the majority as US unregistered ultralights and 23 tandem-seaters. In July 2009 there were 45 Airbikes registered as [[homebuilt aircraft|experimental amateur-builts]] or [[light sport aircraft]] in the USA.<ref name="KitplanesDec1998" /><ref name="FAA">{{cite web|url = http://registry.faa.gov/aircraftinquiry/acftinqSQL.asp?striptxt=Airbike&mfrtxt=&cmndfind.x=0&cmndfind.y=0&cmndfind=submit&modeltxt=Airbike|title = Make / Model Inquiry Results|accessdate = 27 July 2009|last = [[Federal Aviation Administration]]|authorlink = |date=July 2009}}</ref>

==Variants==
;Airbike
:Single seat aircraft designed for the US ultralight category. Engine options were the {{convert|28|hp|kW|0|abbr=on}} [[Rotax 277]], {{convert|40|hp|kW|0|abbr=on}} [[Rotax 447]] or {{convert|22|hp|kW|0|abbr=on}} [[Zenoah G-25]].<ref name="KitplanesDec1998" /><ref name="Cliche" />
;Tandem Airbike
:Two-seat aircraft designed as an ultralight trainer or amateur-built. Standard engine was the {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]].<ref name="KitplanesDec1998" /><ref name="Cliche" />

==Specifications (Airbike with Rotax 447) ==

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref= KitPlanes<ref name="KitplanesDec1998" /> & Cliche<ref name="Cliche" /><!-- the source(s) for the information -->
|crew=one
|capacity= no passengers<!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 16 ft 0 in
|length alt=4.88 m
|span main= 26 ft 0 in
|span alt=7.93 m
|height main=5 ft 6 in
|height alt=1.68 m
|area main= 118 sq ft
|area alt= 10.98 sq m
|airfoil=
|empty weight main= 257 lb
|empty weight alt= 116 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 303 lb
|useful load alt= 137 kg
|max takeoff weight main= 560 lb
|max takeoff weight alt= 254 kg
|max takeoff weight more=
|more general=
|engine (jet)=
|type of jet=
|number of jets=
|thrust main= 
|thrust alt= 
|thrust original=
|afterburning thrust main=
|afterburning thrust alt= 
|thrust more=
|engine (prop)=[[Rotax 447]]
|type of prop= fixed pitch<!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 40 hp
|power alt=30 kW
|power original=
|power more=
|propeller or rotor?=propeller<!-- options: propeller/rotor -->
|propellers=1
|number of propellers per engine= 1
|propeller diameter main=
|propeller diameter alt= 
|max speed main= 80 mph
|max speed alt=130 km/h
|max speed more= 
|cruise speed main= 63 mph
|cruise speed alt=102 km/h
|cruise speed more 
|stall speed main= 30 mph
|stall speed alt= 49 km/h
|stall speed more=
|never exceed speed main=
|never exceed speed alt= 
|range main= 150 nautical miles
|range alt=279 km
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 
|ceiling alt= 
|climb rate main= 1000 ft/min
|climb rate alt= 5.1 m/s
|loading main=4.75 lb/sq ft
|loading alt=23.1 kg/sq m
|thrust/weight=
|power/mass main=14 lb/hp
|power/mass alt=0.12 kW/kg
|more performance=
|armament=<!-- if you want to use the following specific parameters, do not use this line at all-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[Aero-Works Aerolite 103]]
*[[Avid Champion]]
*[[Beaujon Enduro]]
*[[Birdman Chinook]]
*[[Capella Javelin]]
*[[Freebird I]]
*[[Kolb Firefly]]
*[[Kolb Firestar]]
*[[Lockwood Drifter]]
*[[RagWing RW9 Motor Bipe]]
*[[Spectrum Beaver]]
*[[Wings of Freedom Flitplane]]
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{Commons category}}

[[Category:United States ultralight aircraft 1990–1999]]