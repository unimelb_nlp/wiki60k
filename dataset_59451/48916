{{BLP refimprove|date=June 2015}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox person
| name = Dan Jones
| image = Dan Jones, historian and journalist.jpg
| caption =  Dan Jones in 2012
| birth_name = Daniel Gwynne Jones
| birth_date = {{Birth date and age|1981|7|27|df=y}}
| birth_place = [[Reading, Berkshire|Reading]], England
| death_date =
| death_place =
| other_names =
| occupation = Historian and journalist
| known_for =
| spouse =
}}

'''Dan Jones''' (born 27 July 1981) is an English writer, historian, TV presenter and journalist.

==Personal life==
Jones was born in [[Reading, Berkshire|Reading]], England, in 1981 to Welsh parents.{{Citation needed|date=March 2012}} He was educated at The [[Royal Latin School]], a state grammar school in [[Buckingham]], before attending [[Pembroke College, Cambridge|Pembroke College]], [[University of Cambridge]], where he was taught by [[David Starkey]].<ref name="observer">{{cite news|author=Oliver Marre |url=https://www.theguardian.com/books/2009/jun/28/history-starkey-historians-writers |title=They're too cool for school: meet the new history boys and girls &#124; The Observer |work=The Guardian |accessdate=11 February 2012 |location=London |date=28 June 2009}}</ref> He got a First in History in 2002.<ref>{{cite web|url=http://www.summerofblood.com |title=The Peasants Revolt, Medieval History – History Book by Dan Jones |publisher=Summer of blood |accessdate=11 February 2012}}</ref> He is a historian, a newspaper columnist, and a magazine editor. He lives in [[Battersea]], London, with his wife and two daughters.

==Historian==
Dan Jones' first history book was a popular narrative history of the English [[Peasants' Revolt]] of 1381, titled ''Summer of Blood: The Peasants’ Revolt of 1381'', which was published in 2009.<ref name="telegraph">{{cite news|author=Book Reviews |url=http://www.telegraph.co.uk/culture/books/bookreviews/5407473/Summer-of-Blood-the-Peasants-Revolt-of-1381-by-Dan-Jones-review.html |title=Summer of Blood: the Peasants’ Revolt of 1381 by Dan Jones: review |work=The Daily Telegraph |accessdate=11 February 2012 |location=London |date=30 May 2009}}</ref> 
His second book, ''The Plantagenets: The Kings Who Made England'', was published in 2012 in the United Kingdom and a year later in the United States,<ref>{{cite web|url=http://www.us.penguingroup.com/nf/Book/BookDisplay/0,,9780670026654,00.html |title=The Plantagenets – Books by Dan Jones – Penguin Group (USA) |publisher=Us.penguingroup.com |date=2013-04-18}}</ref> where it became a ''[[New York Times]]'' bestseller.<ref>{{cite news|url=https://www.nytimes.com/best-sellers-books/2013-05-05/hardcover-nonfiction/list.html |title=The New York Times – bestseller list | date=2013-05-05 | publisher=nytimes.com }}</ref> It is a family portrait of the [[House of Plantagenet|Plantagenet]] kings from [[Henry II of England|Henry II]] to [[Richard II of England|Richard II]].

Jones' third book, ''The Hollow Crown: The Wars of the Roses and the Rise of the Tudors'', published in 2014, picks up where ''The Plantagenets'' leaves off and covers the period 1420–1541, from the death of [[Henry V of England|Henry V]] to the arrival of [[the Tudors]].<ref>{{cite web|last=Heilbrun|first=Margaret|title=Before Richard III: Author Interview with Dan Jones, The Plantagenets|url=http://reviews.libraryjournal.com/2013/02/in-the-bookroom/authors/before-richard-iii-author-interview-with-dan-jones-the-plantagenets/#_|work=Library Journal|accessdate=13 February 2013}}</ref>

His fourth book is titled "[[Magna Carta]]: The Making and Legacy of the Great Charter" and was published in 2014.<ref>{{cite web|author= |url=http://www.amazon.co.uk/Magna-Carta-Dan-Jones/dp/1781858853/ |title=Magna Carta: The Making and Legacy of the Great Charter: Amazon.co.uk: Dan Jones: 9781781858851: Books |publisher=Amazon.co.uk |date= |accessdate=2015-07-01}}</ref>

==TV presenter==
In 2014, Jones's book ''The Plantagenets'' was adapted for television as a four-part series on [[Channel 5 (UK)]] entitled ''[[Britain's Bloodiest Dynasty|Britain's Bloodiest Dynasty: The Plantagenets]]''.<ref>{{cite web|url=http://www.channel5.com/shows/britains-bloodiest-dynasty |title=Britain's Bloodiest Dynasty |publisher=Channel5.com |date= |accessdate=2015-07-01}}</ref>

Jones has also made a twelve-part series for [[Channel 5 (UK)]] entitled ''[[Secrets of Great British Castles]]''.<ref>{{cite web|url=http://dcdrights.com/catalogue/programme/great-british-castles |title=Catalogue |publisher=Dcdrights.com |date= |accessdate=2015-07-01}}</ref>

In April 2016, he co-wrote and co-presented, with Dr. Suzannah Lipscomb, ''[[Henry VIII and His Six Wives (TV Mini-Series)|Henry VIII and His Six Wives]]''<ref>http://www.channel5.com/show/henry-viii-and-his-six-wives</ref> which was shown on Channel 5.<ref>http://www.channel5.com</ref>

==Journalist==
Jones is a journalist. He is a columnist at the [[Evening Standard|''London Evening Standard'']], where he writes regularly about sport.<ref name="Evening Standard">{{cite web |url= http://www.standard.co.uk/sport/sport-comment/dan-jones-a-scare-could-be-just-what-the-lions-needed-8663466.html |title=A scare could be just what the Lions needed |first=Dan |last=Jones |work=London Evening Standard |date=18 June 2013 |accessdate=23 July 2014}}</ref> He has written for ''The Times'',<ref>{{cite web|author=Dan Jones |url=http://www.thetimes.co.uk/tto/arts/books/article3509136.ece |title=The Watchers: A Secret History of the Reign of Elizabeth I by Stephen Alford |work=The Times |date=2012-08-18}}</ref><ref>{{cite web|author=Dan Jones Last updated at 12:01AM, 25 September 2012 |url=http://www.thetimes.co.uk/tto/arts/books/article3548425.ece |title=The history of Britain (in 15 minutes): from Stonehenge to the credit crunch |work=The Times |date=2012-09-25}}</ref><ref>{{cite web|author=Dan Jones  |url=http://www.thetimes.co.uk/tto/arts/books/non-fiction/article3359290.ece |title=Thomas Becket: Warrior, Priest, Rebel, Victim by John Guy |work=The Times |date=2012-03-24}}</ref> the ''Sunday Times'',<ref>{{cite web|author=Dan Jones |url=http://www.thesundaytimes.co.uk/sto/culture/film_and_tv/film/article1029468.ece |title=Rise of the Plantagenets |work=The Sunday Times |date=2012-05-06}}</ref><ref>{{cite web|author=Dan Jones |url=http://www.thesundaytimes.co.uk/sto/culture/books/non_fiction/article1133722.ece |title=Blood Sisters: The Hidden Lives of the Women Behind the Wars of the Roses by Sarah Gristwood |work=The Sunday Times |date=2012-09-30}}</ref><ref>{{cite web|author=Dan Jones |url=http://www.thesundaytimes.co.uk/sto/culture/arts/theatre/article1144587.ece |title=A cavalier, with facts |work=The Sunday Times |date=2012-10-14}}</ref> ''The Telegraph'',<ref>{{cite news|author=Book Reviews |url=http://www.telegraph.co.uk/culture/books/bookreviews/8896707/Books-of-the-Year-2011-History-Books.html |title=Books of the Year 2011: History Books |work=The Daily Telegraph |accessdate=11 February 2012 |location=London |date=21 November 2011}}</ref><ref>{{cite news|last=Jones |first=Dan |url=http://www.telegraph.co.uk/topics/christmas/9696152/Christmas-2012-History-books-of-the-year.html |title=Christmas 2012: History books of the year |work=The Daily Telegraph |date=2012-11-29 |location=London}}</ref><ref>{{cite news|author=Hay Festival |url=http://www.telegraph.co.uk/culture/hay-festival/9307450/Hay-Festival-2012-Dan-Jones-on-Freedom-of-Speech.html |title=Hay Festival 2012: Dan Jones on Freedom of Speech |work=The Daily Telegraph |location=London |date=5 June 2012}}</ref><ref>{{cite news|author=Christmas |url=http://www.telegraph.co.uk/topics/christmas/9764194/The-modern-joys-of-Christmas-past.html |title=The modern joys of Christmas past |work=The Daily Telegraph |date=2012-12-23 |location=London}}</ref> and for ''The Spectator'',<ref>{{cite web|url=http://www.spectator.co.uk/search/author/?searchString=Dan%20Jones |title=Dan Jones &#124; Spectator Magazine |work=The Spectator |accessdate=11 February 2012}}</ref> ''The Daily Beast'' and ''Newsweek'',<ref>{{cite web|url=http://www.thedailybeast.com/contributors/dan-jones.html |title=Dan Jones |work=Thedailybeast|accessdate=2015-07-01 }}</ref> ''The Literary Review'', ''The New Statesman'',<ref>{{cite web|last=Jones |first=Dan |url=http://www.newstatesman.com/books/2011/08/smith-sport-game-cricket |title=The Following Game |work=New Statesman |accessdate=11 February 2012}}</ref> ''GQ'', ''BBC History Magazine'' and ''History Today''.

==Family==
Dan Jones is the great-nephew of British politician and journalist [[Alun Gwynne Jones, Baron Chalfont]].{{Citation needed|date=March 2012}}

==Publications==
* ''Magna Carta: The Making And Legacy Of The Great Charter'', London, Head of Zeus, 2014, ISBN 978-1-781-85885-1.
* ''The Wars of the Roses: The Fall of the Plantagenets and the Rise of the Tudors'', New York, Viking, 2014, ISBN 978-0-670-02667-8. (Known in the UK as ''The Hollow Crown: The Wars of the Roses and the Rise of the Tudors'', London, 2014, ISBN 978-0-571-28807-6.)
* ''The Plantagenets: The Warrior Kings and Queens Who Made England'', London, HarperPress, 2012, ISBN 978-0-00-721392-4 <ref name="HarperCollins.co.uk">{{cite web|url=http://www.harpercollins.co.uk/Titles/34118 |title=HarperCollins |publisher=Harpercollins.co.uk |accessdate=10 February 2012 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* ''Summer of Blood: The Peasants’ Revolt of 1381'', London, HarperPress, 2009, ISBN 978-0-00-721391-7.

==References==
{{Reflist|colwidth=30em}}


{{DEFAULTSORT:Jones, Dan}}
[[Category:1981 births]]
[[Category:Living people]]
[[Category:People from Reading, Berkshire]]
[[Category:Alumni of Pembroke College, Cambridge]]
[[Category:People educated at the Royal Latin School]]
[[Category:English historians]]
[[Category:English male journalists]]
[[Category:English biographers]]
[[Category:Historians of England]]
[[Category:English television presenters]]
[[Category:Writers of historical fiction set in the Middle Ages]]