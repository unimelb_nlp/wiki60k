{{Orphan|date=March 2016}}

'''Grinding wheel wear''' is an important measured factor of [[Grinding (abrasive cutting)|grinding]] in the [[Manufacturing engineering|manufacturing]] process of engineered parts and tools. Grinding involves the removal process of material and modifying the surface of a workpiece to some desired finish which might otherwise be unachievable through conventional machining processes.<ref name=":5">{{cite book|last1=Schmid|first1=Serope Kalpakjian, Steven|title=Manufacturing processes for engineering materials|date=2007|publisher=Prentice Hall|location=Harlow|isbn=978-0132272711|pages=chapter 9|edition=5th}}</ref> The grinding process itself has been compared to machining operations which employ multipoint cutting tools. The abrasive grains which make up the entire geometry of  wheel act as independent small [[Cutting tool (machining)|cutting tools]]. The quality, characteristics, and rate of grinding wheel wear can be affected by contributions of the characteristics of the material of the workpiece, the temperature increase of the workpiece, and the rate of wear of the grinding wheel itself. Moderate wear rate allows for more consistent material size. Maintaining stable grinding forces is preferred rather than high wheel wear rate which can decrease the effectiveness of material removal from the workpiece.<ref>{{cite book|last1=Rowe|first1=W. Brian|title=Principles of modern grinding technology|date=2009|publisher=William Andrew|location=Norwich, NY|isbn=0815520182|edition=1st}}</ref>

== Mechanisms of wheel wear==
[[File:GrindingWheel.jpg|thumb|Industrial Grinding Gear|300x300px]]
Some common attributing factors to wheel can be caused by grain fracture, which can be advantages, is the results of a portion of each of the individual grains on the wheel surface breaking apart and leaving the remaining grain bonded to the wheel. The fractured grain is left with newly exposed sharp edges which attribute the self-sharpening characteristic of [[grinding wheel]]s and [[Cutting tool (machining)|cutting tools]] in general. Attritious wear or progressive wear which is typically undesirable<ref name=":1">{{cite book|last1=editors|first1=Mark J. Jackson, J. Paulo Davim,|title=Machining with abrasives|date=2011|publisher=Springer|location=New York|isbn=978-1441973016|pages=47–51|edition=1.}}</ref> leads to the grains dulling by developing flat spots and rounded edges on the wheel which can deteriorate the wheels ability to remove material. Flat spots also can lead to excessive heat generation with the added surface contact which and in turn enable Bond fractures or the brittle fracture of the adhesive bonds between the grains. The removal of these worn grains from the adhesive bonds restores the wheels cutting ability once more.<ref name=":3">{{cite book|last1=Marinov|first1=Valery|title=Manufacturing Process Design.|date=2012|publisher=Kendall Hunt Pub Co|isbn=1465202560|edition=2}}</ref> Grinding wheels can also be characterized by the grains increased capacity to fracture according to a level of higher value of [[friability]]'''.'''  Different bonding materials are used depending on the intended use of the grinding wheel. The bonding material is classified by its individual strength called its wheel grade.<ref>{{cite book|last1=Groover|first1=Mikell P.|title=Fundamentals of modern manufacturing : materials, processes, and systems|date=2010|publisher=J. Wiley & Sons|location=Hoboken, NJ|isbn=978-0470467008|page=613|edition=4th|url=https://futureingscientist.files.wordpress.com/2014/01/fundamentals-of-modern-manufacturing-4th-edition-by-mikell-p-groover.pdf|accessdate=17 February 2016}}</ref>

== Grinding forces ==
[[File:DresserWear1.jpg|thumb|300x300px|Grinding Wheel Wear Curve:  Diameter of wear (mm) as a function of time (td), Three stages of grinding, and dressing/tool life determined in stage II (Td).]]
The longevity and cutting ability of a grinding wheel can be affected by the grinding forces generated while in use. Experimental investigation has revealed a direct relationship between cutting speed, wheel geometry, chip geometry, and the grinding forces namely the resultant normal force component (''F<sub>n</sub> ),'' the tangential force component (''F<sub>t</sub> ),'' and their ratio when in contact with a workpiece.<ref name=":1">{{cite book|last1=editors|first1=Mark J. Jackson, J. Paulo Davim,|title=Machining with abrasives|date=2011|publisher=Springer|location=New York|isbn=978-1441973016|pages=47–51|edition=1.}}</ref>

Stage I: As a workpiece enters the grinding zone the initial contact forces are unstable and rise abruptly short period of time and a small wear spot is formed. The overall performance of a wheel during this moment of unstable rising forces can be minimized with proper [[Grinding dresser|dressing conditions]] prior to use and can help effect the high peak and steady state forces which should normally be contained within a short period of time.

Stage II: During the steady state wear stage reaction forces are constant and the flow of heat generation in both the work piece and wheel remain in equilibrium. The measurable data in this stage presents itself as a linear rate of wear as a function of the working duration of the [[Grinding dresser|dresser application]] or tool life (''T<sub>d</sub>'').<ref name=":2">{{cite book|last1=al.]|first1=Ioan D. Marinescu ... [et|title=Tribology of abrasive machining processes|date=2012|publisher=William Andrew|location=Norwich, N.Y.|isbn=978-1437734676|pages=356–359|edition=2nd}}</ref> The tool life corresponds to the grinding wheels ability to maintain the initial shape give to it during the dressing prior to use. As the workpiece stays full contact with the grinding zone in the steady state of constant forces the flow of heat generation in the work piece and the wheel maintains equilibrium. This phase usually does not produce temperatures that would coincide with bond fracture however the material properties of the bond strength can determine the maximum applied force the wheel grit can sustain prior to fracture.<ref name=":1">{{cite book|last1=editors|first1=Mark J. Jackson, J. Paulo Davim,|title=Machining with abrasives|date=2011|publisher=Springer|location=New York|isbn=978-1441973016|pages=47–51|edition=1.}}</ref>

Stage III: Wear rates on the workpiece become detrimental while the rate of change in reaction forces decreases. The end of tool-life represent the initial dressing condition are no longer effective. The rate of change of forces generated in this stage are minimal and wear on the workpiece as an exponential tendency.<ref name=":2">{{cite book|last1=al.]|first1=Ioan D. Marinescu ... [et|title=Tribology of abrasive machining processes|date=2012|publisher=William Andrew|location=Norwich, N.Y.|isbn=978-1437734676|pages=356–359|edition=2nd}}</ref>

== Effects of cutting temperature ==
[[File:Scheibe_im_eingriff.jpg|300x300px|Grinding with the use of lubricant|thumb]]The lifespan of the grinding wheel and final surface properties of the workpiece are directly affected by the operating cutting temperature.  Heat generated during grinding penetrates the grinding wheel and the workpiece which can cause dimensional errors due to thermal expansion <ref name=":3" />

Several adverse effects of a high cutting temperature are as follows: 
* [[Tempering (metallurgy)|Tempering]]
* [http://www.abrasiveengineering.com/therm.htm Burning]
* [[Thermal shock|Thermal cracks]]
* [[Residual stress]]es 
[[File:Heat flux dist.png|thumb|300x300px|The heat flux distribution at the grinding wheel-workpiece interface]]

The addition of grinding fluids can effectively control cutting temperatures to reduce heat induced surface effects on the wheel and workpiece.<ref name=":3" />

High heat flux density may result in the grinding wheel melting and consequently, increased wear.  The heat flux (Φ) penetrating the grinding wheel and workpiece depends mainly on the cutting speed (v<sub>s</sub>) and cutting force (F<sub>c</sub>).  The temperature of the grinding wheel is related to the density of heat flux (φ = dΦ/dA) generated (which also is directly proportional to the feed rate).<ref name=":4">{{Cite journal|last = Kaczmarek|first = Jozef|date = 2008|title = The effect of abrasive cutting on the temperature of grinding wheel and its relative efficiency|url =|journal = Archives of Civil and Mechanical Engineering|doi =|pmid =|access-date =}}</ref> An approximate value of the heat flux can be calculated as follows:  Φ = F<sub>c</sub> • v<sub>s</sub>

== Grinding wheel types ==

Grinding wheels can be made with a variety of materials depending on the desired abrasive quality required during use. Abrasive material, natural of synthetic, used in grinding include some common types of grinding wheel geometry.<ref name=":0">{{cite book|title=Fundamentals of Modern Manufacturing : Materials, Processes, and Systems.|date=2012|publisher=Wiley & Sons Canada, Limited, John|isbn=978-1118393673|pages=604–624|url=https://futureingscientist.files.wordpress.com/2014/01/fundamentals-of-modern-manufacturing-4th-edition-by-mikell-p-groover.pdf|accessdate=13 February 2016}}</ref>
# Straight
# Cylinder
# Straight Cup
# A variety of material types

== Abrasive grains ==
The process of grinding requires an abrasive component with material properties harder than the workpiece. Most common grinders employ a rotating surface being brought in contact with a work surface. The wheel component of grinder itself is generally composed of abrasive grains held together by a bond structure which contain some amount of porosity.<ref name=":0">{{cite book|title=Fundamentals of Modern Manufacturing : Materials, Processes, and Systems.|date=2012|publisher=Wiley & Sons Canada, Limited, John|isbn=978-1118393673|pages=604–624|url=https://futureingscientist.files.wordpress.com/2014/01/fundamentals-of-modern-manufacturing-4th-edition-by-mikell-p-groover.pdf|accessdate=13 February 2016}}</ref>

== Wheel speed ==
[[File:Typical ranges and speeds for abrasive processes.png|thumb|600x600px|Typical ranges and speeds for abrasive processes]]The grinding wheel typically operates at high rotational speeds.<ref name=":3" />  The wheel speed depends several factors, some of which included the grindability of the wheel, the shape of the part, and the material of the workpiece.  These properties will affect important parameters such as the surface finish, surface integrity, and wheel wear.<ref name=":5" />  Likewise, the grinding wheel speed will depend on which abrasive process is needed and which finishing process is desired.

== Dressing ==
A worn grinding wheel can be dressed to restore its grinding properties. [[Grinding dresser|Dressing]] a grinding wheel causes new grains to be produced on a glazed or loaded grinding wheel. A glazed grinding wheel is the result of high attritious wear causing the grains to become dull. A loaded grinding wheel is a result of chips clogging the grains on the grinding wheel due to the grinding of soft materials, improper grinding wheel selection processing parameters. In addition to sharpening a grinding wheel dressing can also be used to true a grinding wheel that is out of round or to shape the profile of a grinding wheel to produce specific features on the workpiece.

== See also ==
* [[Grinding wheel]]
* [[Grinding dresser]]
* [[Manufacturing]] 
* [[Grinding machine]] 
* [[Metalworking]]

==References==
{{reflist}}

[[Category:Grinding and lapping]]