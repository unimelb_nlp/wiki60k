{{good article}}
{{Infobox album
| Name        = The Allman Brothers Band
| Type        = [[Album]]
| Artist      = [[The Allman Brothers Band]]
| Cover       = TheAllmanBrothersBandTheAllmanBrothersBand.jpg
| Released    = November 4, 1969
| Recorded    = August 3–12, 1969<br/>[[Atlantic Studios]]<br/><small>([[New York City]])</small>
| Genre       = [[Southern rock]], [[blues]], [[blues rock]]
| Length      = 33:18
| Label       = [[Atco Records|Atco]], [[Capricorn Records|Capricorn]]
| Producer    = [[Adrian Barber]]
| Last album  =
| This album  = '''''The Allman Brothers Band'''''<br />(1969)
| Next album  = ''[[Idlewild South]]''<br />(1970)
  | Misc        = {{Singles
  | Name           = The Allman Brothers Band
  | Type           = studio
  | single 1       = Black Hearted Woman/Every Hungry Woman
  | single 1 date  = March 1970
  }}
}}

'''''The Allman Brothers Band''''' is the debut [[studio album]] by American [[Rock music|rock]] band [[the Allman Brothers Band]]. It was released in the United States by [[Atco Records]] and [[Capricorn Records]] on November 4, 1969 and [[Record producer|produced]] by [[Adrian Barber]]. Formed in 1969, the Allman Brothers Band came together following various musical pursuits by each individual member. Following his session work in [[Muscle Shoals, Alabama|Muscle Shoals]], [[Alabama]], [[Duane Allman]] moved to [[Jacksonville, Florida|Jacksonville]], [[Florida]] where he led large jam sessions with his new band, one he had envisioned as having two guitarists and two drummers. After rounding out the lineup with the addition of his brother, [[Gregg Allman]], the band played free shows in public parks and moved to [[Macon, Georgia|Macon]], [[Georgia (U.S. state)|Georgia]], where they were to be one of the premiere acts on Capricorn.

The album was recorded and mixed in two weeks at [[Atlantic Studios]] in [[New York City]]. Much of the material presented was premiered live over the preceding months and combines [[blues]], [[jazz]] and [[country music]] to varying degrees. It includes re-workings of "[[Trouble No More (song)|Trouble No More]]" and "Don't Want You No More," as well as notable originals such as "Dreams", which highlighted the band's jazz influence, and "[[Whipping Post (song)|Whipping Post]]", which soon became a crowd favorite. Although the group was arranged to work with producer [[Tom Dowd]] (whose credits included [[Cream (band)|Cream]] and [[John Coltrane]]), he was unavailable, and they instead recorded with house engineer [[Adrian Barber]]. The album's artwork was photographed at various places in Macon and surrounding areas.

The record initially received a poor commercial response, charting in the lower levels of ''[[Billboard (magazine)|Billboard]]''{{'s}} [[Billboard 200|Top 200 Pop Albums]] chart. Despite this, the album received critical acclaim from publications such as ''[[Rolling Stone]]'', who called it "consistently [...] subtle, and honest, and moving." Following the release of the album, the band remained on the road for an extended period of time. They chose to remain in Macon, despite suggestions from label executives to move to larger cities for a better shot at commercial acceptance.

==Background==
The Allman Brothers Band was formed in March 1969, during large jam sessions with various musicians in [[Jacksonville, Florida|Jacksonville]], [[Florida]]. [[Duane Allman]] and [[Jai Johanny Johanson]] (Jaimoe) had recently moved from [[Muscle Shoals, Alabama|Muscle Shoals]], where Duane participated in session work at [[FAME Studios]] for artists such as [[Aretha Franklin]], [[King Curtis]], and [[Wilson Pickett]], with whom he recorded a cover of [[the Beatles]]' "[[Hey Jude]]" that went to number 23 on the national charts.{{sfn|Paul|2014|p=6}} Duane began to put together a new band, and invited bassist [[Berry Oakley]] to jam with the new group; the pair had met in a [[Jacksonville, Florida|Jacksonville]], [[Florida]] club some time earlier, and became quick friends.{{sfn|Paul|2014|p=15}} The group had immediate chemistry, and Duane's vision for a "different" band — one with two lead guitarists and two drummers — began evolving.{{sfn|Paul|2014|p=15}} Meanwhile, [[Phil Walden]], the manager of the late [[Otis Redding]] and several other R&B acts, was looking to expand into rock acts.{{sfn|Paul|2014|p=9}} Rick Hall became frustrated with the group’s recording methods, and offered the tracks recorded and their contract to Walden and [[Jerry Wexler]] of [[Atlantic Records]], who purchased them for $10,000.{{sfn|Paul|2014|p=11}} Walden intended the upcoming group to be the centerpiece of his new Atlantic-distributed label, [[Capricorn Records|Capricorn]].{{sfn|Paul|2014|p=13}}

After the duo moved to Jacksonville, they began to put together large jam sessions.{{sfn|Paul|2014|p=19}} [[Dickey Betts]] had played in Oakley’s previous band, the Second Coming, and became the group’s second lead guitarist, while [[Butch Trucks]], with whom Duane and Gregg had cut a demo less than a year prior, fulfilled the role of the second drummer.{{sfn|Paul|2014|p=17}} The Second Coming’s Reese Wynans played keyboards, and Duane, Oakley and Betts all shared vocal duties.{{sfn|Paul|2014|p=17}} The unnamed group began to perform free shows in Willow Branch Park in Jacksonville, with an ever-changing, rotating cast of musicians.{{sfn|Paul|2014|p=24}} Duane felt strongly his brother should be the vocalist of the new group (which effectively eliminated Wynans' position, as Gregg also played keyboards).{{sfn|Paul|2014|p=25}} Gregg accepted the invitation and entered rehearsal on March 26, 1969, when the group was rehearsing "[[Trouble No More (song)|Trouble No More]]" by [[Muddy Waters]].{{sfn|Paul|2014|p=28}} Although initially intimidated by the musicians, Duane pressured his brother "into singing [his] guts out."{{sfn|Paul|2014|p=29}} Four days later, the group made their début at the Jacksonville Armory.{{sfn|Paul|2014|p=29}} Although many names were suggested including [[Beelzebub]], the six-piece eventually decided on the Allman Brothers Band.{{sfn|Paul|2014|p=30}}

The group moved to [[Macon, Georgia|Macon]], [[Georgia (U.S. state)|Georgia]] by May 1, where Walden was establishing Capricorn Records.{{sfn|Paul|2014|p=32}} The band performed locally, as well as eighty miles north in [[Atlanta]]'s [[Piedmont Park]], and practiced at the newly minted Capricorn nearly each day.{{sfn|Paul|2014|p=36}} The group forged a strong brotherhood, spending countless hours rehearsing, consuming psychedelic drugs, and hanging out in [[Rose Hill Cemetery (Macon, Georgia)|Rose Hill Cemetery]], where they would write songs.{{sfn|Paul|2014|p=46}} Their first performances outside the [[Southern United States|South]] came on May 30 and 31 in [[Boston]], opening for [[the Velvet Underground]].{{sfn|Paul|2014|p=46}} In need of more material, the group remade old blues numbers like “Trouble No More” and “[[One Way Out (song)|One Way Out]]”, in addition to improvised jams such as “[[Mountain Jam]].”{{sfn|Paul|2014|p=41}} Gregg, who had struggled to write in the past, became the band’s sole songwriter, composing songs such as “[[Whipping Post (song)|Whipping Post]]" and “Black-Hearted Woman.”{{sfn|Paul|2014|p=42}} Much of the material collected on ''The Allman Brothers Band'' was written between the period of May to August 1969 and premiered live. According to Johanson, the group gauged crowd reaction to the numbers and adjusted the songs accordingly.{{sfn|Paul|2014|p=52}} "Before we went into the studio, we had a very clear idea of what we were all trying to do musically and that it was unique, totally different from anything else that anyone was playing," said Betts. "From the earliest rehearsals, we all had the same mind-set."{{sfn|Freeman|1996|p=58}}

==Recording and production==
{{Quote box
 |quote  = The whole experience of making the first album was absolutely wonderful. I felt comfortable in the studio, having recorded a bunch before, as did we all, and the music was great. We had played these songs so much and we were all just busting to get them down on record.
 |source = [[Butch Trucks]], 2014{{sfn|Paul|2014|p=52}}
 |quoted = 1
 |width  = 25%
 |align  = right
}}
The band set off from Macon for [[New York City]] in August 1969, and faced setbacks along the way, such as their equipment truck breaking down in [[South Carolina]].{{sfn|Paul|2014|p=51}} In addition, they had arranged to work with [[Cream (band)|Cream]] producer [[Tom Dowd]], who was unavailable; [[Atlantic Records]] house engineer [[Adrian Barber]] recorded the sessions instead, and was credited as producer.{{sfn|Paul|2014|p=51}} Recalled Dowd, "I was supposed to have done the first album with the band up in New York, but some way or other I got detoured. [[Jerry Wexler]] made a deal to keep them in the studio for three or four days when they were supposed to be with me."{{sfn|Poe|2008|p=124}} The band had no commercial success in mind, having had troublesome experiences individually in the past with producers and labels that pushed for radio hits. The band felt that with time they would develop a small, devoted following and be strong enough to collect $3–4,000 dollars per night.{{sfn|Freeman|1996|p=58}}

''The Allman Brothers Band'' was recorded and mixed in two weeks, and according to biographer Alan Paul, "virtually no outtakes exist from the sessions."{{sfn|Paul|2014|p=51}} The band had performed their songs countless times in the preceding months and "[had] them down cold."{{sfn|Paul|2014|p=51}} Numerous artists, including [[Ray Charles]], had recorded on the studio's house [[Hammond organ]], but Gregg Allman set up his own instead, feeling unable to play on the same instrument as Charles.{{sfn|Allman|Light|2012|p=139}} A red light on the recording board would go on when the band began recording, and it made Gregg Allman nervous; in order to perform takes as needed, he unscrewed the light.{{sfn|Allman|Light|2012|p=139}} The two-week booking was initially designed for laying down basic tracks, with [[overdubs]] following later,{{sfn|Paul|2014|p=52}} but the group ended up cutting the entire record in six non-consecutive days.{{sfn|Poe|2008|p=126}} They first entered Atlantic Studios that Sunday night (August 3) to "get sounds";{{sfn|Paul|2014|p=52}} the band laid down the album's openers, "Don’t Want You No More" and "It's Not My Cross to Bear", as well as "Dreams," which the band set aside.{{sfn|Poe|2008|p=124}} "Dreams" had previously been recorded as a demo at Macon’s new Capiricorn Studios in April.{{sfn|Poe|2008|p=125}}

On August 5, the band cut "Black Hearted Woman" and "Trouble No More", and the group completed "Whipping Post" after another day off on August 7 (it took the entirety of that day’s session to complete the recording, despite the fact that they had performed the number countless times).{{sfn|Poe|2008|p=125}}{{sfn|Paul|2014|p=51}} The next day, the band attempted to record "[[Statesboro Blues]]," which was the song that influenced Duane Allman to begin slide playing. Unable to achieve the same energy as it would performed live, the band scrapped the recording and session for the day.{{sfn|Poe|2008|p=125}} "Every Hungry Woman" was recorded on August 11, and their last day in the studio on Tuesday, August 12 produced a final version of "Dreams".{{sfn|Poe|2008|p=125}} Johnson remembered the process as only taking four days; "We went in there, played our asses off, and that was it; we were done in four days and they spent the rest of the time mixing," said Johnson.{{sfn|Paul|2014|p=52}}

Although Butch Trucks recalls the entire ensemble as comfortable with studio recording,{{sfn|Paul|2014|p=52}} another source claims that Johnson, Betts and Oakley were unfamiliar with studio recording, but nevertheless not intimidated.{{sfn|Paul|2014|p=52}} "They were out of their element in New York, hustled by a chap with an English accent," said Dowd of Barber. He spoke of Barber's direction as "perhaps intimidating, or push-push, shove-shove. 'Do what the guy says and let’s get out of here.'"{{sfn|Poe|2008|p=126}} "Dreams", which later gained regard among band members as the high point of the record, was the only song in which the group got stuck, due to Duane Allman's displeasure with his [[guitar solo]]. The performance captured on record came when Duane instructed the other members to turn off all the lights in the studio after the day's session, and sat in a corner beside his amp and baffle.{{sfn|Paul|2014|p=53}} Allman played [[slide guitar]] (which was not employed in previous attempts) and improvised the overdubbed performance, bringing all of the members to tears. "It was unbelievable," recalled Trucks. "It was just magic. It’s always been that the greatest music we played was from out of nowhere, that it wasn’t practiced, planned, or discussed."{{sfn|Paul|2014|p=53}}

During their tenure in New York, the group made their debut over three non-consecutive nights at Ungano's in [[Manhattan]],{{sfn|Allman|Light|2012|p=139}} a club that would eventually become regarded within the ensemble as their "second home."{{sfn|Paul|2014|p=51}} Gregg Allman felt the band had rushed through their debut recording and was later unhappy with his vocal sound on the record; "They were recorded with the regular old [[tape echo]] "[[Heartbreak Hotel]]" setting," he recalled.{{sfn|Allman|Light|2012|p=140}} Barber disagreed with this assessment, and, not wanting to cause any quarrels, Allman backed away.{{sfn|Allman|Light|2012|p=140}}

==Composition==
{{Listen|filename=DreamsAllmanBros.ogg|title="Dreams"|description="Dreams" contains [[slide guitar]] sections from [[Duane Allman]], and also contains hints of influence from [[jazz]].}}
The songs on the album were largely arranged after Gregg Allman joined the band in [[Jacksonville, Florida|Jacksonville]], [[Florida]] in March 1969. Most of the songs were devised from longer, impromptu [[jam session]]s.{{sfn|Paul|2014|p=56}} The group's style evolved from a mix of [[jazz]], [[country music]], [[blues]] and [[rock music|rock]], which was the result of each individual member turning the others onto their particular interests.{{sfn|Paul|2014|p=60}} Trucks introduced Johnson to [[the Grateful Dead]] and [[the Rolling Stones]]; Johnson likewise introduced the group to jazz musicians such as [[Miles Davis]] and [[John Coltrane]], and Betts did the same with country music and [[Chuck Berry]].{{sfn|Paul|2014|p=60}} Duane Allman had previously listened to Davis and Coltrane before Johnson's suggestion, and his two favorite songs — Coltrane's version of "[[My Favorite Things (song)|My Favorite Things]]" and Miles Davis' "[[All Blues]]" — were the basis for the majority of the band's modal jamming, "without a lot of chord changes."{{sfn|Paul|2014|p=61}}

The album opens with an instrumental, a cover of [[Spencer Davis]]' "[[With Their New Face On|Don't Want You No More]]," which had previously been employed on set lists of the Second Coming, Oakley and Betts' former band.{{sfn|Freeman|1996|p=59}} Allman and Betts' guitars perform in unison on a five-note melody while Johnson concentrates on his [[Hi-hat (instrument)|hi-hat]], and the song includes an [[organ (music)|organ]] solo.{{sfn|Freeman|1996|p=59}} The song contains two guitar solos, with the latter "[coming] in behind the first one for a darting buildup that sound[s] like something taken from [[Brahms]]."{{sfn|Freeman|1996|p=60}} It segues into a "lazy blues shuffle" titled "It's Not My Cross to Bear," which Allman had written in Los Angeles for a former lover.{{sfn|Freeman|1996|p=60}} "Black Hearted Woman," also penned on the same subject, follows, and the album returns to a blues-based sound with a cover of "Trouble No More," featuring Duane’s debut bottleneck guitar performance.{{sfn|Freeman|1996|p=60}} Songs such as "Black Hearted Woman" and "Every Hungry Woman" were written about Allman's experiences with a girl named Stacy in [[Los Angeles]].{{sfn|Allman|Light|2012|p=111}}

Among the most changed were two songs that would become the basis for two of the Allman Brothers' most famed epic concert numbers: "Dreams" and "[[Whipping Post (song)|Whipping Post]]". Oakley "played a huge role in the band’s arrangements," changing numbers such as "Whipping Post" from a [[ballad]] structure to a more hard-rocking song.{{sfn|Paul|2014|p=55}} "Dreams" developed from a jam in which the band toyed with the theme to the film ''[[2001: A Space Odyssey]]'', and has been referred to by Johnson as Coltrane's "My Favorite Things" with lyrics.{{sfn|Paul|2014|p=56}} Johnson's drum fills were pulled from [[Jimmy Cobb]]'s performance on "All Blues"; he later commented that he "did a lot of copying, but only from the best."{{sfn|Paul|2014|p=56}} "Dreams" begins with "intricate, subdued drums playing under a soft organ with only the hint of guitars before Gregg begins singing about disillusionment and broken dreams."{{sfn|Freeman|1996|p=60}}

The final song on the record, "Whipping Post," was written shortly after Allman returned to Jacksonville.{{sfn|Freeman|1996|p=60}} The song came to him shortly before bed, but he was unable to acquire a pencil and paper to write down his ideas, as there was a child asleep in the room and he could not turn on the lights. Turning to his next best alternative, he struck two kitchen matches (one for light and one, later blown out, as a [[charcoal]] writing utensil) and wrote down his lyrics on a bedside [[ironing board]].{{sfn|Freeman|1996|p=61}} "Whipping Post" was similar in composition to "Dreams" in its first incarnation, with Oakley later creating the heavy [[bassline]] that starts off the track.{{sfn|Paul|2014|p=56}} Duane and Betts take quick solos before the track builds to an "anguished climax," leading to Gregg Allman's solo voice, singing the song's refrain: "Good Lord, I feel like I'm dyin'."{{sfn|Freeman|1996|p=61}} Allman had no idea the intro was written in 11/4 time — "I just saw it as three sets of three, and then two to jump on the next three sets with" — until his brother pointed it out for him.{{sfn|Poe|2008|p=125}} "My brother told me — I guess the day I wrote it — he said, 'That's good, man. I didn’t know you understood 11/4.' Of course I said something intelligent like, 'What's 11/4?' Duane just said, 'Okay, dumbass, I'll try to draw it up on paper for you.'"{{sfn|Poe|2008|p=125}}

[[Gregg Allman]]'s lyrical contributions to the band's debut album have been called "remarkably mature lyrical conceptions for such a young man, expertly executed in a [[minimalism|minimalist]], almost [[haiku]] style."{{sfn|Paul|2014|p=55}} Allman's inspiration came from his time in [[Los Angeles]] as a part of [[Hour Glass (band)|Hour Glass]], "getting fucked by different land sharks in the business," experiencing great frustration among fierce competition.{{sfn|Paul|2014|p=55}} The traditional blues songs were, likewise, regarded as "songs that were so good they couldn’t be left off the album."{{sfn|Paul|2014|p=55}} On the writing of the record, Allman wrote in his memoir ''My Cross to Bear'', "I wrote most of that whole first record in that one week. I had total peace of mind. L.A. and all its changes didn’t even cross my mind. I felt like I was starting all over, which I was."{{sfn|Allman|Light|2012|p=113}} Most of the songs on the album were written at [[Rose Hill Cemetery (Macon, Georgia)|Rose Hill Cemetery]].{{sfn|Allman|Light|2012|p=127}}

==Artwork==
[[Image:Allman Brothers gatefold 1969.jpg|thumb|The gatefold album sleeve features the band posing nude in a brook.]]
The cover for the album was taken by photographer [[Stephen Paley]]. Paley had gotten to know Duane Allman during photo shoots for Atlantic.{{sfn|Paul|2014|p=58}} Paley stayed for "about a week" in Macon with the band, partying with the group.{{sfn|Paul|2014|p=58}} They approached any areas about the town that appeared photogenic, such as "fields, old houses, railroad tracks, [and] the cemetery."{{sfn|Paul|2014|p=58}} The front album cover photo was taken at the entrance of the College House (now owned by [[Mercer University]]) next door at 315 College Street. The back cover photo of the album was taken at the Bond Tomb at [[Rose Hill Cemetery (Macon, Georgia)|Rose Hill Cemetery]], located at 1091 Riverside Drive in Macon.{{sfn|Allman|Light|2012|p=127}}

The gatefold cover of the [[gramophone record|vinyl LP]] features the band posing nude in a brook. The shot was original manager [[Phil Walden]]'s idea, and the brook was on his brother [[Alan Walden|Alan's]] property.{{sfn|Paul|2014|p=58}} Alan later recalled, "The [inner sleeve] photo was taken in [[Jones County, Georgia|Round Oak]], [[Georgia (U.S. state)|Georgia]], down behind my log cabin there, which is also the back of [[Otis Redding]]'s Big O Ranch".{{sfn|Poe|2008|p=128}} ''[[Rolling Stone]]'' editor [[Jann Wenner]] was present with [[Boz Scaggs]], whom he was producing at the time.{{sfn|Paul|2014|p=58}} They brought bubbles to cover themselves up, but the bubbles were washed away by the stream.{{sfn|Paul|2014|p=59}} Trucks had sliced his leg open earlier that day, requiring thirteen stitches, and was unable to get in the water; he is standing behind Oakley in the shot.{{sfn|Paul|2014|p=59}} Walden suggested the band take a few shots standing full-frontal; the band was reluctant but he assured them they would never see the light of day. At their first performance at the [[Fillmore East]] that December, Trucks discovered the full-frontal shots were printed in a broadsheet alternative newspaper.{{sfn|Paul|2014|p=59}}

Paley later said of the cover shoot, "I never liked a band more. I was one of them. It was like being a rock star. I hung out with a lot of rock stars but no one ever did that to the same extent. There was just an ease to the whole thing. They really were the kindest, most fun band I ever worked with."{{sfn|Paul|2014|p=58}}

==Release and reception==
{{Album ratings
| rev1      = [[AllMusic]]
| rev1Score = {{Rating|4.5|5}} <ref>{{Allmusic |class=album |id=r387 |tab=review |first=Bruce |last=Eder |label=''The Allman Brothers Band''}}</ref>
| rev2      = ''[[The Rolling Stone Album Guide]]''
| rev2Score = {{Rating|4|5}}<ref>[http://www.rollingstone.com/music/artists/the-allman-brothers-band/albumguide The Allman Brothers Band Album Guide], ''Rolling Stone''</ref>
}}
''The Allman Brothers Band'' was released in November 1969 through [[Atco Records|Atco]] and [[Capricorn Records]].{{sfn|Freeman|1996|p=59}} Atco was a subsidiary label of [[Atlantic Records]], and Walden had not even created a logo for Capricorn Records; instead, the LP featured an Atco label with a "barely noticeable" line reading "Capricorn Records Series."{{sfn|Poe|2008|p=129}} The record received a poor commercial response, selling fewer than 35,000 copies upon initial release.{{sfn|Paul|2014|p=64}} Executives suggested to Walden that he relocate the band to New York or Los Angeles to "acclimate" them to the industry. "They wanted us to act "like a rock band" and we just told them to fuck themselves," remembered Trucks.{{sfn|Paul|2014|p=65}} For their part, the members of the band remained optimistic, electing to stay in [[Southern United States|the South]]. "Everyone told us we’d fall by the wayside down there," said Gregg Allman,{{sfn|Paul|2014|p=65}} but the collaboration between the band and Capricorn Records "transformed Macon from this sleepy little town into a very hip, wild, and crazy place filled with bikers and rockers."{{sfn|Paul|2014|p=66}}

The band played shows along the [[East Coast of the United States|East Coast]] in December 1969, attempting to kick-start the record onto ''[[Billboard (magazine)|Billboard]]''{{'s}} [[Billboard 200|Top 200 Pop Albums]] chart.{{sfn|Poe|2008|p=137}} In January, the band performed at the [[Electric Factory]] in [[Philadelphia]] and the [[Fillmore West]] in [[San Francisco]] before debuting at the [[Whisky a Go Go]] in Los Angeles, representing the Allmans' first engagement there since their days in the Hour Glass.{{sfn|Poe|2008|p=137}} On the third day of their Whisky residency, the album logged at number 188 on the top 200, "enough to prove that the ABB was more than a regional outfit."{{sfn|Poe|2008|p=137}} Capricorn issued "Black Hearted Woman" as the album’s single, edited down to nearly two minutes shorter in an effort to place on top 40 radio.{{sfn|Poe|2008|p=138}} Despite Ed Och's rave review in ''Billboard'', the single failed to register on pop radio.{{sfn|Poe|2008|p=138}} Rather than employing the standard cover shot for the advertising campaign, marketing rather emphasized the nude group shot, alongside a quote from Och's ''Billboard'' review, describing the group as a "bad bunch of electric Southern longhairs."{{sfn|Poe|2008|p=128}}

''[[Rolling Stone]]''{{'s}} Lester Bangs called the album "consistently [...] subtle, and honest, and moving," describing the band as "a white group who've transcended their schooling to produce a volatile blues-rock sound of pure energy, inspiration and love."<ref name=rs>{{cite journal|author=Lester Bangs| date =February 21, 1970| title =Review: ''The Allman Brothers Band''| journal =[[Rolling Stone (magazine)|Rolling Stone]]| volume = | issue =52 | page =52 | publisher =[[Straight Arrow Press|Straight Arrow Publishers, Inc.]]| location =[[New York City]] | issn =0035-791X }}</ref> A retrospective review from Bruce Eder at [[Allmusic]] stated it "might be the best debut album ever delivered by an American blues band, a bold, powerful, hard-edged, soulful essay in electric blues with a native Southern ambience."<ref name="am">{{cite web|author=Bruce Eder|url=http://www.allmusic.com/album/the-allman-brothers-band-mw0000200423|title=Review: ''The Allman Brothers Band''|publisher=[[Allmusic]]|date=|accessdate=March 23, 2014}}</ref>

==Track listing==
All songs written by [[Gregg Allman]], except where noted.
{{tracklist
| headline = Side 1
| title1 = Don't Want You No More
| note1 = [[Spencer Davis]], Edward Hardin
| length1 = 2:25
| title2 = It's Not My Cross to Bear
| length2 = 5:02
| title3 = Black Hearted Woman
| length3 = 5:08
| title4 = [[Trouble No More (song)|Trouble No More]]
| note4 = [[Muddy Waters]]
| length4 = 3:45
}}
{{tracklist
| headline = Side 2
| title5 = Every Hungry Woman
| length5 = 4:13
| title6 = Dreams
| length6 = 7:18
| title7 = [[Whipping Post (song)|Whipping Post]]
| length7 = 5:17
}}

==Personnel==
All credits adapted from liner notes.<ref name="linernotes">{{cite AV media notes | title=The Allman Brothers Band | year=1969 | others=[[The Allman Brothers Band]] | type=liner notes | publisher=[[Atco Records|Atco]] | location=[[United States|US]] | id=SD 33-308}}</ref>
{{col-start}}
{{col-2}}
;The Allman Brothers Band
*[[Duane Allman]]&nbsp;– [[slide guitar|slide]] and [[lead guitar|lead]] guitars
*[[Dickey Betts]]&nbsp;– lead guitar
*[[Gregg Allman]]&nbsp;– [[organ (music)|organ]], [[lead vocals]]
*[[Berry Oakley]]&nbsp;– [[bass guitar]], [[backing vocals]]
*[[Jai Johanny Johanson]]&nbsp;– [[drum kit|drums]], [[congas]]
*[[Butch Trucks]]&nbsp;– drums, [[percussion]]
{{col-2}}
;Production
*[[Adrian Barber]]&nbsp;– [[Sound recording and reproduction|production]], [[Audio engineer|engineer]]
* Robert Kingsbury&nbsp;– design
* [[Stephen Paley]]&nbsp;– [[photography]]
{{col-end}}

==Charts==

===Weekly charts===
{|class="wikitable"
|-
! Chart (1969)
! Peak<br />position
|-
| US [[Billboard 200|Top 200 Pop Albums]] (''[[Billboard (magazine)|Billboard]]'')
| style="text-align:center;"| 188{{sfn|Poe|2008|p=137}}
|}

==Notes==
{{Reflist|colwidth=20em}}

==References==
* {{cite book
|ref       = harv
|last      = Paul
|first     = Alan
|title     = One Way Out: The Inside History of the Allman Brothers Band
|year      = 2014
|publisher = St. Martin's Press
|isbn      = 978-1250040497}}
* {{cite book
|ref       = harv
|last      = Freeman
|first     = Scott
|title     = Midnight Riders: The Story of the Allman Brothers Band
|year      = 1996
|publisher = Little, Brown and Company
|isbn      = 978-0316294522}}
* {{cite book
|ref       = harv
|last      = Poe
|first     = Randy
|title     = Skydog: The Duane Allman Story
|year      = 2008
|publisher = Backbeat Books
|isbn      = 978-0879309398}}
* {{cite book
|ref       = harv
|last1      = Allman
|first1     = Gregg
|last2      = Light
|first2     = Alan
|title     = My Cross to Bear
|year      = 2012
|publisher = William Morrow
|isbn      = 978-0062112033}}

==External links==
<!-- This is a licensed stream for the album, which is allowed under Wikipedia polices -->
* [http://www.last.fm/music/The+Allman+Brothers+Band/The+Allman+Brothers+Band ''The Allman Brothers Band''] at [[Last.fm]]
{{Allman Brothers Band}}

{{DEFAULTSORT:Allman Brothers Band, The}}
[[Category:The Allman Brothers Band albums]]
[[Category:1969 debut albums]]
[[Category:Atco Records albums]]
[[Category:Capricorn Records albums]]
[[Category:English-language albums]]