'''''The Perfect Moment''''' was the most comprehensive retrospective of works by New York photographer [[Robert Mapplethorpe]]. The show spanned twenty-five years of his career, featuring [[celebrity portrait]]s, [[self-portrait]]s, [[interracial]] figure studies, floral still lifes, [[homoerotic]] images, and collages. The exhibition, organized by [[Janet Kardon]] of the [[Institute of Contemporary Art, Philadelphia|Institute of Contemporary Arts in Philadelphia]], opened in the winter of 1988 just months before Mapplethorpe’s death from [[AIDS]] complications on March 9, 1989.  On tour, in the summer of 1989, the exhibition became the centerpiece of a controversy concerning federal funding of the arts and censorship.

== Exhibition ==
=== Background ===
''The Perfect Moment'' covered all aspects of the photographer’s career from the late 1960s to 1988. The traveling exhibition had been scheduled to appear at five other museums in various regions of the country during the next year and a half. It included more than 150 images. Despite the controversial character of some of the [[photograph]]s, critical response was enthusiastic and attendance was robust throughout the show's [[Philadelphia]] run (from December 1988 through January 1989).

''The Perfect Moment'' traveled to the [[Museum of Contemporary Art, Chicago|Museum of Contemporary Art in Chicago]]. Again, it generated no unfavorable public or critical attention. In June 1989, after the cancellation of the exhibition by the [[Corcoran Gallery of Art]] in [[Washington D.C.]], two and a half weeks before it was to open there, ''The Perfect Moment'' unexpectedly provoked national controversy and ICA became a key player in the congressional debate over what public funds should and should not fund. The issues of censorship and artistic freedom that the show raised occupied the forefront of the debates between conservatives and liberals during the [[Ronald Reagan]] era and in its aftermath. Members of the religious right, especially, criticized academics and artists for what they regarded as their indecent, subversive and blasphemous works. 

=== Layout and works displayed ===
''The Perfect Moment'' grouped photos into three categories: 
*Rigorously conceived portraits and figure studies;
*Dramatically lit flower arrangements in color, and in black and white;
*Photographs of gay [[sadomasochism]] (S&M) that left nothing to the imagination.

Robert Mapplethorpe’s XYZ portfolios, explored three subjects: homosexual [[sadomasochism]] (X); flower still lifes (Y); and nude portraits of [[African American]] men (Z). The extremely graphic S&M photos from Mapplethorpe’s X Portfolio were displayed in a separate, age-restricted area during at each venue of the exhibition. The portfolios were displayed with a series of poems by poet and singer [[Patti Smith]]. The poems echo Mapplethorpe's X,Y,Z trope.

{{quote|“Y is the symbol of the covenant which exists between the artist and his creator/ Y is the consummation of this idea thru the projection of the perfect shot. ”Please use the poetry as sandwich quotes; I want them to be obvious.” (Patti Smith)}}

The images that sparked the most controversy include:
*Jim and Tom, Sausalito, 1977
*Man in a Polyester Suit, 1980
*Jesse McBride, 1976
*Rosie (Honey), 1976
 
Rosie, a black and white portrait of a very young girl crouched down on a bench outdoors with part of her dress lifted, exposing her genitals, generated controversy because of the subject's age and the issue of consent. This photograph was deemed [[child pornography]] by Mapplethorpe's detractors, and it is surprising that it was featured in the exhibition catalogue, considering that the printer had refused to print it for the Mapplethorpe retrospective at the [[Whitney Museum of Art]] in 1988.<ref>{{cite book|last1=Danto|first1=Arthur|title=Playing with the Edge: The Photographic Achievement of Robert Mapplethorpe|date=1996|publisher=Berkeley: University of California Press}}</ref> There was one other photograph of a naked child, "Jessie McBride" which also contributed to the controversy created by "Rosie". 
 
A 55-minute videotape of a [[BBC]] interview with the photographer accompanied the exhibition.

== Reception ==
The art critics in Philadelphia, the show's first venue, critiqued Mapplethorpe's work along formalist lines, without commenting on the provocative content of the X portfolio photographs. Overall, the exhibition was met enthusiastically by critics both Philadelphia and in Chicago, where the show appeared at the [[Museum of Contemporary Art, Chicago|Museum of Contemporary Art]].

However, a campaign launched by the [[American Family Association]], a conservative watchdog group, to censor what they considered “indecent” art changed the climate of reception. In June 12, 1989 [[Christina Orr-Cahall]], the director of the [[Corcoran Gallery of Art]], cancelled “The Perfect Moment,” which was scheduled to open there on June 30. Orr-Cahall feared that ''The Perfect Moment'' would endanger [[National Endowment for the Arts]] (NEA) appropriations in the [[United States Congress]].

The exhibition set off one of the fiercest episodes of America's "[[culture war]]s" —  and sparked a recurring debate about state-funded cultural production and the support of [[sexually explicit]] or [[sacrilegious]] art by public funds.

=== Debate about National Endowment for the Arts ===
The canceling of the ''Perfect Moment'' provoked a [[censorship]] battle about national funding for the arts that was front page news for the next year.

On June 30, 1989, protesters angered by the cancellation of the show by the Corcoran Gallery projected slides of Mapplethorpe’s photographs on the facade of the museum. Seven hundred people rallied to express their outrage about the Corcoran's decision. [[Michael Brenson]], art critic for the ''[[New York Times]]'' wrote, “This exhibit should be seen. It is extremely unfortunate that the Corcoran Gallery of Art canceled it last month in the hope of averting a political outcry…. As much as he has been made out to be a renegade and outlaw, Mapplethorpe is an utterly mainstream artist. He loved freshness and glamor and was obsessed with the moment, which his photographs always reflect. In his restricted spaces and his feeling for abstraction and attentiveness to every shape, edge and texture, Mapplethorpe is a child of the Formalism of the 1960’s."<ref name="New York Times">{{cite news|last1=Brenson|first1=Michael|title=The Many Roles of Mapplethorpe, Acted out in Ever Shifting Images,|date=July 22, 1989}}</ref> The Washington Project for the Arts (WPA) stepped in to host the show, and on July 22, 1989, “The Perfect Moment” opened at this alternative art space. No incidents marred the show's run at the Washington Project for the Arts.

However, Senator [[Jesse Helms]] introduced legislation that would stop the NEA from funding artwork he considered “obscene.” The legislation subsequently required any recipients of NEA funds to sign an oath that declared they would not promote obscenity. The oath provoked protests from artists and arts organizations. When, during the next grant cycle, in this climate of fear, applications for support equaling hundreds of thousands of dollars were rejected. Outraged artists filed lawsuits against the agency. A compromise was reached in Congress. Although the radically restrictive Helms amendment did not pass, restrictions were placed on NEA funding procedures.

=== Censorship ===
In March 1990, anti-pornography advocates in [[Cincinnati Ohio]], the [[Citizens for Community Values]], launched a campaign to pressure the [[Contemporary Arts Center]] (CAC) to cancel “The Perfect Moment.” Cincinnati law enforcement ordered four hundred visitors from the museum to leave while they videotaped Mapplethorpe’s photographs as evidence to support obscenity charges brought against [[Dennis Barrie]], the director of the CAC, and against the CAC. This was the first time a museum in the [[United States]] faced prosecution for the art it displayed.

On October 5, 1990, Dennis Barrie and the CAC were acquitted in the [[obscenity]] case. The prosecution failed to convince the jury that Mapplethorpe’s photographs lacked [[artistic merit]]. In Cincinnati, more than 80,000 people saw the show. The censorship proceedings doubtless brought more attention to Mapplethorpe's work than it would have otherwise received.

=== Legacy ===
Since the CAC debacle, images from ''The Perfect Moment'', including the X, Y, and Z portfolios, have circulated to in hundreds of exhibitions internationally. Mapplethorpe's work is currently represented by 12 galleries worldwide, with the largest collection of his works at the [[Solomon R. Guggenheim Museum]]. These photographs and history of their attempted censorship during the 1990s have influenced artists, inspired [[LGBTQ]] advocates, and generated much scholarship.

==References==
{{Reflist}}

=== Bibliography ===
{{colbegin|2}}
*Altabe, Joan. "Robert Mapplethorpe's 'The Perfect Moment'" Examiner.com. 9 Sept. 2012. Web. 18 Oct. 2014.
*Atkins, Robert. “A Censorship Time Line.” College Art Association, Art Journal, Vol. 50, No. 3, Censorship I (Autumn, 1991), pp.&nbsp;33–37. JSTOR. Web. 20 Oct. 2014.
*Artner, Alan G. "Robert Mapplethorpe, Daring Photographer." Chicago Tribune. Chicago Tribune, 10 Mar. 1989. Web. 21 Oct. 2014.
*Barbara Gamarekian, Corcoran, to Foil Dispute, Drops Mapplethorpe Show, June 14, 1989, http://www.nytimes.com/1989/06/14/arts/corcoran-to-foil-dispute-drops-mapplethorpe-show.html
*Barrie, Dennis R. "The Scene Of The Crime." Art†Journal†50.(1991): 29-32. Art†Source. Web. 20 Oct. 2014.
*Cash, Stephanie. "Everyone’s a Critic (or Curator)." Art in America 99. no. 5 (2011):40      
*Cembalest, Robin. Who Does it Shock? Why Does it Shock?”, Artnews (March 1992):32-33.
*Champion, Daryl. "Twenty Years Later: Mapplethorpe, Artand Politics." Twenty Years Later. Web. 18 Oct. 2014.
*Cynthia L. Ernst, The Robert Mapplethorpe Obscenity Trial (1990): Selected Links & Bibliography, http://law2.umkc.edu/faculty/projects/ftrials/mapplethorpelinks.html
*Danto, Arthur C. Playing with the Edge: The Photographic Achievement of Robert Mapplethorpe. Berkeley: University of California Press, 1996. Print.
*Deborah A. Levinson, Robert Mapplethorpe's extraordinary vision, August 31, 1990, http://tech.mit.edu/V110/N31/mapple.31a.html
*Demaline, Jackie. "Mapplethorpe Battle Changed Art World." Enquirer. The Cincinnati Enquirer, 21 May 2000. Web. 20 Oct. 2014.
*Eccles, Tom. "Michael Ward Stout." Art Review (November 2013): 70-72.
*Encyclopædia Britannica. Britannica. Last accessed October 17, 2014. http://www.britannica.com/EBchecked/topic/363603/Robert-Mapplethorpe#ref41553.
*Ernst, Cynthia L. "Robert Mapplethorpe Obscenity Trial (1990): Selected Links and Bibliography." Robert Mapplethorpe Obscenity Trial (1990): Selected Links and Bibliography. N.p., n.d. Web. 20 Oct. 2014.
* Fabre, Jan, and Robert Mapplethorpe. The Power of Theatrical Madness. London: International distributor, Institute of Contemporary Arts, 1986. Print
*Fox, John. "What Was It like 10 Years Ago? What Does It Mean Today?" CoverStory: Then and Now: Mapplethorpe CAC. Citybeat, 30 Mar. 2000. Web. 16 Oct.2014.
*Frank, Priscilla. "Robert Mapplethorpe's 'Saint And Sinners' Reveals The Power Of Black-And-White Photography." The Huffington Post. TheHuffingtonPost.com, 23 Dec. 2013. Web. 17 Oct. 2014.
*Fritscher, Jack. “What Happened When: Censorship, Gay History, and Mapplethorpe.” Censorship: A World Enclylcopedia. Jack Fritscher, 2002. Web. 20 Oct. 2014.
*Gamarekian, Barbara. "Corcoran, to Foil Dispute, Drops Mapplethorpe Show." The New 	York Times. The New York Times, 13 June 1989. Web. 21 Oct. 2014.
*Gefter, Philip. “Two Robert Mapplethorpe Symposia.” Apeture no. 197 (2009):82-85
*Great Contemporary Nudes, 1978-1990: Selected Works by Robert Mapplethorpe, Herb Ritts, Bruce Weber : [C2 Gallery, April 28-June 11, 1990]. C2 Gallery, Yasushi Inaida, 1990.
*Gurstein, Rochelle. Current Debate: High Art or Hard-Core? Misjudging Mapplethorpe: the Art Scene and the Obscene. Tikkun (November–December 1991): 70-80.
*Hatt, Michael. "Exposure Time." Oxford†Art†Journal†Vol. 17.No. 2 (1994): P. 132137. Jstor. Web. 18 Oct. 20144. <jstor.org>.
*Hatt, M. (n.d.). The Homoerotic Photograph: Male Images from Durieu/Delacroix to Mapplethorpe by Allen Ellenzweig.
*Hood, William. “On Getting Better” Art Journal 56, no. 2 (1997): 4-5.
*Hudson, Suzanne Perling. "Beauty and the Status of Contemporary Criticism." October, Vol. 104, (Spring, 2003): 115-130.
*Ischar, Doug. "Endangered Alibis." Afterimage 17.(1990): 8-11. Art Source. Web. 20 Oct. 2014.
*Kardon, Janet, David Joselit, and Kay Larson. Robert Mapplethorpe: The Perfect Moment. Philadelphia: University of Pennsylvania, Institute of Contemporary Art. 1998.
*Kester, Grant H. "A Town Called Malice." Afterimage 17.(1990): 2. Art Source. Web. 21 Oct. 2014.
*Kidd, Dustin. "Mapplethorpe and the New Obscenity." Afterimage 30 no. 5 (2003):6-7
*Lacayo, Richard. "Shock Snap." TIME's Annual Journey:1989.
*Levinson, Deborah A. “Robert Mapplethorpe's extraordinary vision.” http://tech.mit.edu. NA. Friday, August 31, 1990. NA.
*Light, Judy. Jury Acquits Museum in Landmark Art Trial. Dancemagazine (December 1990):12-13.
*McClean, Daniel. The Trials of Art. London: Ridinghouse, 2007. Print.
*Meyer, Richard. The Jesse Helms Theory of Art.The MIT Press. October, Vol. 104, (Spring, 2003), pp.&nbsp;131–148.
*Meyer, Richard. "Mapplethorpe's Living Room: Photography And The Furnishing Of Desire." Art History 24.2 (2001): 292-311. Art Source. Web. 21 Oct. 2014.
*Marshall, Richard, Richard Howard, and Ingrid Sischy. Robert Mapplethorpe. New York: Whitney Museum of American Art in association with New York Graphic Society Books, 1988. Print
*Mercer, Kobena. "Reading Racial Fetishism: The Photographs of Robert Mapplethorpe." 	Welcome to the Jungle (London, Routledge, 1994):174-219.
*Morrisroe, Patricia. Mapplethorpe: A Biography. New York: Random House, 1995. Print.
*Phelan, Peggy. "Serrano, Mapplethorpe, the NEA, and You: “Money Talks”: October 1989" TDR 34, no.1 (1990):4-15.
*Quigley, Magaret. "Welcome to the Online Archive of the Old PublicEye.Org Website." PublicEye.org. Web. 17 Oct. 2014.
*Quigley, Margaret. "The Mapplethorpe Censorship Controversy: Chronology of Events, The 1989--1991 Battles." PublicEye.org. Political Research Associates, 1 Jan. 2013. Web. 18 Oct. 2014.
*Rizzo, Frank. "Keeping Distance From and Redefining Robert Mapplethorpe." - Behind the Curtain. Courant, 26 Mar. 2010. Web. 16 Oct. 2014.
*"Robert Mapplethorpe: The Perfect Moment." Catalyst RSS. Washington Project for the Arts, n.d. Web. 19 Oct. 2014. <http://wpadc.org/catalyst/?p=2845>.
*Ruhrberg, K. (1998). Art of the 20th century (p.&nbsp;767). Köln: Taschen.
*Schenchner, Richard. "Bon Voyage, NEA." TDR (1988-), Vol. 40, No. 1 (Spring 1996):7-9.
*Smith, Nathan. "Op-ed: After 25 Years, Mapplethorpe's Photos Still Crack the Bullwhip." Advocate.com. Advocate.com, 17 Oct. 2014. Web. 20 Oct. 2014.
*Storr, Robert. “Art, Censorship, and the First Amendment: This Is Not a Test.” College Art Association, Art Journal, Vol. 50, No. 3, Censorship I (Autumn, 1991), pp.&nbsp;12–28. JSTOR. Web. 20 Oct. 2014.
*Suddath, Claire. "Top 10 Persecuted Artists." Time. Time Inc., 05 Apr. 2011. Web. 20 Oct. 2014.
*Tannenbaum, Judith. “Robert Mapplethorpe: The Philadelphia Story” Art Journal 50, no. 4 (1991):71-76.
*Taylor, C. (n.d.). Robert Mapplethorpe: Body, form and function.
*Vartanian, Hrag. "Imperfect Moments: Mapplethorpe & Censorship Twenty Years Later” at ICA in Philadelphia." Art21 18 Feb. 2009. Print.
*Weston, Edward, Mark Johnstone, Jonathan Green, Irene M. Borger, and Robert Mapplethorpe.The Garden of Earthly Delights: Photographs by Edward Weston and Robert Mapplethorpe. Riverside: UCR/California Museum of Photography, University of California, 1995. Print.
*Yúdice, George. "The Privatization of Culture." Social Text, No. 59 (Summer 1999):17-34.
{{colend}}

{{DEFAULTSORT:Perfect Moment}}
[[Category:Art exhibitions in the United States]]
[[Category:Censorship in the arts]]
[[Category:Controversies in the United States]]
[[Category:Obscenity controversies]]