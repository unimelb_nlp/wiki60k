{{infobox book
| name          = A Lineage of Grace
| title_orig    =
| translator    =
| image         =
| caption       = 2004 edition cover
| author        = [[Francine Rivers]]
| cover_artist  =
| country       = [[United States]]
| language      = [[English language|English]]
| series        = A Lineage of Grace
| genre         = [[Christian novel]]
| publisher     = [[Tyndale House Publishers]]
| release_date  = 2002 (first edition)
| media_type    = Print, Audio, e-book
| pages         = 516 pp (first edition, paperback)
| isbn          = 0-8423-7110-9
| dewey         = 813/.54 21
| congress      =
| oclc          =
| preceded_by   =
| followed_by   =
}}

<!--- 'Francine Rivers' links here --->
'''A Lineage of Grace''' is a series of five historical fiction [[novellas]] written by the American author [[Francine Rivers]]. Each novella details the story of a woman in the lineage of [[Jesus Christ]] described in the New Testament - [[Tamar (Genesis)|Tamar]], [[Rahab]], [[Ruth (biblical figure)|Ruth]], [[Bathsheba]], and [[Mary (mother of Jesus)|Mary]]. The book was released in 2002 by [[Tyndale House Publishers]].<ref name="Description - Rivers">Rivers, Francine. [http://francinerivers.com/books/lineage-grace ''A Lineage of Grace - Description''] {{webarchive |url=https://web.archive.org/web/20140327085025/http://francinerivers.com/books/lineage-grace |date=March 27, 2014 }}. Retrieved on 13 May 2014</ref><ref name="Description - Tyndale">[http://www.tyndale.com/A-Lineage-of-Grace/9780842356329#.U4MYZvmSx8E ''A Lineage of Grace - Details'']. Retrieved on 13 May 2014</ref>  It has been published in hardcover, paperback, audio and e-book versions<ref name="Editions">[http://www.goodreads.com/work/editions/92167-a-lineage-of-grace ''A Lineage of Grace - Editions''] Retrieved on 23 May 2014</ref> in several languages.<ref name="International editions">Rivers, Francine. [http://francinerivers.com/books/91/international-editions ''A Lineage of Grace - International Editions''] {{webarchive |url=https://web.archive.org/web/20140629202645/http://francinerivers.com/books/91/international-editions |date=June 29, 2014 }} Retrieved on 23 May 2014</ref>  It won the Retailer's Choice Award in 2009<ref name="retailer's choice awards">[http://www.goodreads.com/award/show/201-retailer-s-choice-award ''Retailer's Choice Award winners''] Retrieved on 25 May 2014</ref>

== References ==
{{reflist}}

== External links ==
* [http://www.francinerivers.com/ Francine Rivers Official Website]

{{DEFAULTSORT:Lineage of Grace}}
[[Category:2002 American novels]]
[[Category:American historical novels]]
[[Category:Christian novels]]