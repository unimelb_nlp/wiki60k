{{Infobox journal
| title = Bone & Joint Research
| cover = [[File:Cover_image_for_Bone_&_Joint_Research.jpg|centre]]
| former_name = 
| discipline = [[Orthopaedics]]
| abbreviation = Bone Joint Res.
| editor = Hamish Simpson
| publisher = The British Editorial Society of Bone & Joint Surgery
| country = 
| history = 2012-present
| frequency = Monthly
| openaccess = Yes
| license = CC BY-NC 3.0<ref>{{cite web|url=https://creativecommons.org/licenses/by-nc/3.0/|title=Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)|work=Creative Commons|accessdate=18 February 2016}}</ref>
| impact = 2.425
| impact-year = 2015
| website = http://www.bjr.boneandjoint.org.uk/
| link1 = http://www.bjr.boneandjoint.org.uk/content/by/year
| link1-name = Online Archive
| link2 = http://www.bjr.boneandjoint.org.uk/content/editorial-board
| link2-name = Editorial Board
| ISSN = 
| eISSN = 2046-3758
}}
'''''Bone & Joint Research''''' (BJR) is an orthopaedic journal covering the whole spectrum of the musculoskeletal sciences, published by The British Editorial Society of Bone & Joint Surgery, a registered charity in the UK (No. 209299). BJR is a gold open access journal and hence articles published in the journal are available online to anyone, free of charge. Articles are published in a continuous publication model, and collated into monthly issues. First published in 2012, BJR is part of the Bone & Joint series of journals, which also includes ''Bone & Joint 360'' and the flagship journal ''[[The Bone & Joint Journal]]'' (first published in 1948 as ''The Journal of Bone & Joint Surgery (British Volume)'').<ref>{{cite web|url=http://www.boneandjoint.org.uk/|title=Bone & Joint - Home|work=The British Editorial Society of Bone & Joint Surgery|accessdate=17 February 2016}}</ref>

According to the ''[[Journal Citation Reports]]'', BJR received achieved a 2015 [[Impact Factor]] Impact Factor of 2.425.<ref>{{cite book |year=2016 |chapter=Bone & Joint Research |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> The journal is also indexed in [[MEDLINE]].

== References ==
{{Reflist}}

== External links ==
*[http://www.bjr.boneandjoint.org.uk ''Bone & Joint Research'']
*[http://www.bjj.boneandjoint.org.uk ''The Bone & Joint Journal'']
*[http://www.bj360.boneandjoint.org.uk ''Bone & Joint 360'']

{{DEFAULTSORT:Bone and Joint Research}}
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Orthopedics journals]]