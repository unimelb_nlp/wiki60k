'''Samson Lane Faison''' (November 29, 1860 &ndash; October 17, 1940) was a brigadier general in the United States Army who commanded the [[30th Infantry Division (United States)|30th Infantry Division]] at various times during World War I.  He received the [[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]] for his significant role in the breaking of the German’s [[Hindenburg Line]].
{{Infobox military person
|name=Samson Lane Faison
|birth_date    = {{birth date|1860|11|29}}
|death_date    = {{death date and age|1940|10|17|1860|11|29}}
|birth_place   = [[Faison, North Carolina]] 
|death_place   = [[Baltimore, Maryland]]
|image=General Faison.jpg
|caption=
|allegiance    = {{flagicon|United States}}[[United States|United States of America]]
|branch        = [[File:United States Department of the Army Seal.svg|20px|United States Army seal]] [[United States Army]]
|serviceyears  = 1883-1922
|rank          = [[File:US-O7 insignia.svg|20px]] [[Brigadier General (United States)|Brigadier  General]]
|servicenumber =  
|commands      = [[30th Infantry Division (United States)|30th Infantry Division]]
|unit          = 
|battles       = [[Apache Wars]]<br/>[[Philippine–American War]]<br/>[[World War I]]
|awards        = [[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]]
|relations     = LCDR [[S. Lane Faison]] (son)
}}

==Personal life==
Samson L. Faison was born in [[Faison, North Carolina]] (Duplin County) to Elias James Faison and Elizabeth Maria Lane.<ref>{{cite book|last=Davis, Jr.|first=Henry Blaine|title=Generals in Khaki|publisher=Pentland Press, Inc.|year= 1998
|ISBN= 1571970886|oclc=40298151|page=123 }}</ref>  He had four brothers and two sisters.  A North Carolina state historical marker stands today in the town bearing his name, birthplace, and accomplishments.<ref>{{cite web|url=http://www.ncmarkers.com/Markers.aspx?sp=Markers&k=Markers&sv=F-29 |title=Marker: F-29 |publisher=Ncmarkers.com |date= |accessdate=2015-02-27}}</ref>  Faison married Eleanor Sowers in 1906 and had two children, [[S. Lane Faison]], Jr. and Eleanor.  His son Lane had a distinguished career as an art historian, member of the OSS’s Art Looting Investigation Unit in World War II, and long-time faculty member at Williams College.

==Career highlights==
[[File:USMA Cadet SL Faison 1883.jpg|thumb|right|Faison's 1883 West Point Graduation photo]]

===Apache Wars and Geronimo Campaign===
Faison graduated from West Point in 1883 and served in the Arizona Territory with the 1st Infantry from 1883-1886 during the final years of the [[Apache Wars]].   In 1885, Lt. Faison was selected by General [[George Crook]] to lead Apache Scout Companies into Mexico to help track down the Apache war leader, [[Geronimo]].  Faison was in the field for 11 months during 1885&ndash;1886 and was one of the few officers present at the council between Crook and Geronimo in March 1886, famously documented by photographer [[C. S. Fly]].  General Crook later recognized Faison and six other officers "for bearing uncomplainingly the almost incredible fatigues and privations as well as the dangers incident to their operations" while they "commanded expeditions or Indian Scouts in Mexico."<ref>General Orders and Circulars, Adjutant General’s Office, 1891. Washington, Government Printing Office</ref>  In 1898 Faison wrote a memoir of his experience in the 1885-1886 Geronimo Campaign, which was published posthumously in 2012 in the ''Journal of the Southwest.''<ref>{{cite web|url=http://www.highstead.net/pdfs/Faison%20Memoir.pdf |format=PDF |title= Lieutenant Faison’s Account of the Geronimo Campaign|author=Edward K. Faison|publisher=Highstead.net |accessdate=2015-02-27}}</ref>
[[File:Council between General Crook and Geronimo.jpg|thumb|right|Council Between Gen. Crook and Geronimo in Mexico, March 1886. Lt. Faison is seated far left in foreground.]]

===West Point and the Philippines===
From 1896-1899, Lt. Faison was senior instructor of infantry tactics at the United States Military Academy.<ref>{{cite book|last=Davis, Jr.|first=Henry Blaine|title=Generals in Khaki|publisher=Pentland Press, Inc.|year= 1998
|ISBN= 1571970886|oclc=40298151|page=123}}</ref>  In 1899, Captain Faison left West Point to participate in the [[Philippine-American War]] where he served in several combat operations against the Philippine insurrection with the 13th Infantry.  He also served as adjutant general and later as judge advocate of military commissions and as judge of the provost court in the Philippines until 1902.<ref>{{cite book|last=Davis, Jr.|first=Henry Blaine|title=Generals in Khaki|publisher=Pentland Press, Inc.|year= 1998
|ISBN= 1571970886|oclc=40298151|page=123 }}</ref>  That fall he returned to the US and assumed command of [[Fort Mason]] in San Francisco.

===Army War College===
Major Faison attended the [[United States Army War College]] from 1910 to 1911. Upon graduation, he served briefly as an instructor and administrator.

===World War I===
After the United States entered the war, Colonel Faison accepted a wartime promotion of brigadier general in August 1917 and command of the 60th Brigade of the 30th Infantry Division.  General Faison commanded the Division for several periods during both its training phase in South Carolina and while in France.  During the 2nd Somme offensive in September–October 1918, Faison’s 60th brigade is said by at least one source to have been the first Americans to break through the German Hindenburg Line at Bellicourt, France.<ref>{{cite web|last=Ingram |first=Charles M. |url=http://ncpedia.org/biography/faison-samson-lane |title=Faison, Samson Lane |publisher=NCpedia |date= |accessdate=2015-02-27}}</ref>  Faison's Distinguished Service Medal citation reads as follows:

General Faison commanded with great credit the 60th Infantry Brigade, 30th Division, in the breaking of the enemy's Hindenburg line at Bellicourt, France, and in subsequent operations in which important captures were made, all marking him as a military commander of great energy and determination.<ref>{{cite web|url=http://digital-library.usma.edu/libmedia/archives/aogreports/V1941.PDF |accessdate=March 23, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20130318004007/http://digital-library.usma.edu/libmedia/archives/aogreports/V1941.PDF |archivedate=March 18, 2013 }}</ref>

[[File:Samson L. Faison.jpg|thumb|right|Brigadier General Faison in Belgium 1918]]

==Later career and life==
After the world war, Faison returned to his permanent rank of colonel, but was officially promoted to brigadier general in 1922.  He retired from active duty later that same year because of physical disabilities.  Faison died in [[Baltimore]], [[Maryland]] on October 17, 1940 at the age of 79 and is buried at [[Arlington National Cemetery]].

==Awards and honors==

Here is the ribbon bar of Brigadier General Faison:

<center>
{|
|{{Ribbon devices|number=0|type=oak|ribbon=Distinguished Service Medal ribbon.svg|width=106}}
|{{Ribbon devices|number=0|type=oak|ribbon=Indian Campaign Medal ribbon.svg|alt=}}
|{{Ribbon devices|number=0|type=oak|ribbon=Philippine Campaign Medal ribbon.svg|alt=}}
|-
|{{Ribbon devices|number=3|type=service-star|ribbon=World War I Victory Medal ribbon.svg|alt=}}
|{{Ribbon devices|number=0|type=service-star|ribbon=Legion Honneur Officier ribbon.svg|alt=}}
|{{Ribbon devices|number=0|type=oak|ribbon=Croix de guerre 1914-1918 with palm.jpg|alt=}}
|}
</center>
<center>
{| class="wikitable"
|-
!1st Row
|colspan="4" align="center" |[[Distinguished Service Medal (U.S. Army)|Army Distinguished Service Medal]]
|colspan="4" align="center" |[[Indian Campaign Medal]]
|colspan="4" align="center" |[[Philippine Campaign Medal]]
|-
!2nd Row
|colspan="4" align="center" |[[World War I Victory Medal (United States)|World War I Victory Medal]] with three battle Clasps
|colspan="4" align="center" |[[Legion of Honour|Officer of the Legion of Honour]] ([[France]])
|colspan="4" align="center" |[[Croix de guerre 1914–1918 (France)|Croix de guerre 1914–1918 with Palm]] ([[France]])
|-
|}
</center>

==Bibliography==
*''Strategy and Tactics''<ref>{{cite web|last=Ingram |first=Charles M. |url=http://ncpedia.org/biography/faison-samson-lane |title=Faison, Samson Lane |publisher=NCpedia |date= |accessdate=2015-02-27}}</ref>

==References==
{{Reflist}}

==External links==
*[https://select.nytimes.com/gst/abstract.html?res=F1081EF73B5A11728DDDA10994D8415B8088F1D3&action=click&module=Search&region=searchResults%230&version=&url=http%3A%2F%2Fquery.nytimes.com%2Fsearch%2Fsitesearch%2F%3Faction%3Dclick%26region%3DMasthead%26pgtype%3DHomepage%26module%3DSearchSubmit%26contentCollection%3DHomepage%26t%3Dqry490%23%2Fgeneral%2Bsamson%2Bfaison%2F New York Times Obituary]
*[http://1-22infantry.org/commanders/faisonpers.htm 1-22infantry.org]

{{DEFAULTSORT:Faison, Samson L.}}
[[Category:1860 births]]
[[Category:1940 deaths]]
[[Category:People from North Carolina]]
[[Category:United States Army generals]]
[[Category:United States Military Academy alumni]]
[[Category:United States Army War College alumni]]
[[Category:American military personnel of the Indian Wars]]
[[Category:American military personnel of the Philippine–American War]]
[[Category:American military personnel of World War I]]
[[Category:United States Army generals of World War I]]
[[Category:American people of the Indian Wars]]
[[Category:Apache Wars]]
[[Category:Recipients of the Distinguished Service Medal (US Army)]]
[[Category:Officiers of the Légion d'honneur]]
[[Category:Recipients of the Croix de guerre 1914–1918 (France)]]
[[Category:Burials at Arlington National Cemetery]]