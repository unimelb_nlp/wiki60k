{{Use Australian English|date=June 2014}}
{{Use dmy dates|date=January 2014}}
'''Henry Edward Downer''' (22 March 1836 – 4 August 1905) was a South Australian politician. He was a brother of Sir [[John Downer]] and [[George Downer]], and a noted lawyer and businessman.

Henry Edward Downer was born in [[Portsmouth]], England and emigrated to Australia in 1838 with his parents Henry (ca.1812 – 25 September 1870) and Jane (ca.1808 – 4 Jan 1861), arriving in Adelaide in June 1838. His father ran a grocery store in [[Hindley Street, Adelaide|Hindley Street]] (at that time Adelaide's premier shopping strip), in 1848 took on as partner Thomas Graves, who bought him out two years later,<ref>{{cite news |url=http://nla.gov.au/nla.news-article54449242 |title=The Late Mr. T. Graves |newspaper=[[South Australian Register]] |location=Adelaide |date=13 August 1900 |accessdate=16 October 2014 |page=6 |publisher=National Library of Australia}}</ref> later was landlord of the Blenheim Hotel on the same street; they lived first at Hindley Street, followed by [[South Terrace, Adelaide|South Terrace]], then at "St. Bernard's", [[Magill, South Australia|Magill]], where he died after a long illness.

Henry received an education at Francis Haire's academy and was articled to the legal firm of Wigley & Richman, working for a time as law clerk. He was called to the Bar in 1859.<ref name=death>{{cite news |url=http://nla.gov.au/nla.news-article4948549 |title=Death of Mr. H. E. Downer |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=5 August 1905 |accessdate=14 November 2012 |page=8 |publisher=National Library of Australia}}</ref>

He was appointed Commissioner for Insolvency in 1865 and filled that position until 1881 when he resigned in order to contest the [[South Australian House of Assembly]] for the seat of [[Electoral district of Encounter Bay|Encounter Bay]]. He entered Parliament in 1881 and held the seat until 1896, for two of those terms in conjunction with [[Simpson Newland]].

He was appointed [[Attorney-General of South Australia]] in May 1890 in the [[John Cockburn (Australian politician)|Cockburn]] ministry, succeeding [[Frederick Turner (politician)|F. F. Turner]] (solicitor to the Lands Titles Office; a temporary appointment of April that year following the defeat at the polls of [[Beaumont Arnold Moulden|B. A. Moulden]]),<ref>{{cite news |url=http://nla.gov.au/nla.news-article47242651 |title=A Political Retrospect |newspaper=[[South Australian Register]] |location=Adelaide |date=28 April 1890 |accessdate=14 November 2012 |page=4 |publisher=National Library of Australia}}</ref> and lost the post with the swearing-in of the [[Thomas Playford II|Playford]] government the following year. His political achievements included:
*conversion of the [[Victor Harbor railway line|Strathalbyn to Victor Harbor horse tramway]] to steam locomotive.
*amendments to Insolvency laws
*amendments to Rent laws in 1883 to protect tenants

He was an active Freemason and office-holder of the Anglican Church. He was a keen horseman and twice appointed master of the Adelaide Hunt Club.

==Family==
H. E. Downer married Maria Martin Haggar ( – 11 January 1912) on 13 August 1859. They had a home "Lyndhurst" on South Terrace, Adelaide. Their children included:
*Ada Louisa (23 November 1860 – 1941) married Otto Heinrich Schomburgk (1857–1938), son of [[Moritz Richard Schomburgk]], on 17 September 1892
*Alice Maria (12 June 1862 – ) married Charles Herbert Warren (ca.1856 – 6 November 1917) on 7 February 1891.
:*[[Constance Jean Bonython]] (1891–1977) was a daughter.<ref>Joyce Gibberd, [http://adb.anu.edu.au/biography/bonython-constance-jean-9539/text16799 'Bonython, Constance Jean (1891–1977)'], ''Australian Dictionary of Biography'', National Centre of Biography, Australian National University, accessed 14 November 2012</ref>
:She married again, to the widower [[Samuel Bruce Rudall]] (7 March 1859 – 3 January 1945), on 6 October 1923.
*Frank Hagger Downer (18 October 1863 – 19 March 1938) married Charlotte Mary Murray (died 15 September 1920), youngest daughter of [[A. B. Murray]] on 8 August 1900
*(Harold) Charles (24 August 1865 – 27 December 1921) married Bertha Law Smith ( – 11 July 1947) on 17 February 1909. He was manager of Melton station.

He died at the home of his son-in-law Otto Schomburgk.

== References ==
{{Reflist}}

{{DEFAULTSORT:Downer, Henry Edward}}
[[Category:Members of the South Australian House of Assembly]]
[[Category:Attorneys-General of South Australia]]
[[Category:Australian lawyers]]
[[Category:1836 births]]
[[Category:1905 deaths]]
[[Category:19th-century Australian politicians]]