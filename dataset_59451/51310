{{Distinguish-redirect|F1000|Formula 1000}}
{{Notability|Companies|date = March 2015}}{{Infobox publisher
| name         = F1000 (Faculty of 1000)
| image        = [[File:Facultyof1000.jpg|200px|Facultyof1000]]
| caption      =
| parent       = Science Navigation Group 
| status       = 
| traded_as    =
| predecessor  =
| founded      = 2000
| founder      = [[Vitek Tracz]]
| successor    = 
| country      = United Kingdom
| headquarters = London
| distribution = 
| keypeople    = 
| publications = reviews, [[scientific journal]]s, posters, slides
| topics       = Science
| genre        = 
| imprints     = 
| revenue      = 
| owner        =
| numemployees =
| url          = {{URL|http://f1000.com}}
}}

'''Faculty of 1000''' (abbreviated '''F1000''') is a publisher of services for [[life scientist]]s and [[clinical researchers]].

== Services ==

===F1000Prime===

'''''F1000Prime''''' publishes recommendations of articles in [[biology]] and [[medicine]] from a "faculty" of around 6,000 scientists and clinical researchers and 5,000 more junior "associate" faculty. The service covers 32 disciplines and around 3,700 journals.<ref>{{cite journal| last                  =Wets | first                 =Kathleen | last2                 =Weedon | first2                =Dave| last3                 =Velterop | first3                =Jan | date                  =1 October 2003 | title                 =Post-publication filtering and evaluation: Faculty of 1000| url                   =http://www.ingentaconnect.com/content/alpsp/lp/2003/00000016/00000004/art00002 | journal               =Learned Publishing | publisher             =ALPSP | volume                =16 | issue                 =4 | pages                 =249–258 | doi                   =10.1087/095315103322421982 | name-list-format      = vanc }}</ref> It previously existed as two sister sites, ''F1000 Biology'', launched in 2002, and ''F1000 Medicine'', launched in 2006. In 2010, these services were combined as ''F1000.com''. The service obtained its current name in 2012.<ref>{{cite web |url=http://charleston.publisher.ingentaconnect.com/content/charleston/chadv/2014/00000015/00000003/art00009 |title=The Charleston Advisor F1000Prime |work=[[The Charleston Advisor]] |publisher=The Charleston Company |date=2014-01-01 |accessdate=2014-02-28}}</ref> When Faculty Members recommend an article for F1000Prime, they rate it as 'Good'. F1000Prime uses the individual scores to calculate the total scores for each article, which are used to rank the articles in each discipline. The F1000Prime score is an [[Article level metrics|article-level metric]], or [[Altmetrics|altmetric]],<ref>{{cite web |url=http://scholar.cci.utk.edu/beyond-downloads/publications/users-narcissism-and-control-%E2%80%93-tracking-impact-scholarly-publications |title=Users, narcissism and control – tracking the impact of scholarly publications in the 21st century |publisher=SURF foundation |date=2012 |accessdate=2015-06-26}}</ref><ref>{{cite journal|last =Eyre-Walker |first =Adam |last2 =Stoletzki |first2 =Nina|date  =8 October 2013 |title =The Assessment of Science: The Relative Merits of Post-Publication Review, the Impact Factor, and the Number of Citations|url =http://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001675 |journal =PLOS Biology |volume =11 |issue =10 |pages =e1001675 |doi =10.1371/journal.pbio.1001675 |name-list-format = vanc }}</ref> and is a potential indicator of the scientific impact of individual papers.<ref>{{cite web|title=F1000 evaluations are an indicator of future citation impact |url=http://www.mrc.ac.uk/Achievementsimpact/MRCe-Val2009/Publications/index.htm#P123_12359 |work=Medical Research Council |accessdate=3 March 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20140304105317/http://www.mrc.ac.uk/Achievementsimpact/MRCe-Val2009/Publications/index.htm |archivedate=4 March 2014 |df= }}</ref><ref>{{cite arXiv |last=Waltman |first=Ludo |author-link= |eprint=1303.3875 |title=F1000 recommendations as a new data source for research evaluation: A comparison with citations |class=cs.DL |date=15 March 2013 }}</ref>

===F1000Research===
'''''F1000Research'''''  is an [[open access]], open peer-review scientific publishing platform covering the [[life sciences]]. Articles are published first and peer reviewed after publication by invited referees. The [[peer review]]er's names and comments are visible on the site. As part of its [[open science]] model, the data behind each article are also published and are downloadable. ''F1000Research'' publishes multiple article types including traditional research articles, single findings, [[case report]]s, protocols, replications and null or negative results.<ref>{{cite web |url=http://f1000research.com/about |title=About |publisher=F1000Research |date= |accessdate=2014-02-28}}</ref> The journal has been criticized for unclear peer-review standards in relation to its inclusion in PubMed, but has since clarified how articles are indexed in the PubMed and PubMed Central databases.<ref>{{cite web |last=Anderson |first=Kent |title=PubMed and F1000 Research — Unclear Standards Applied Unevenly |url=http://scholarlykitchen.sspnet.org/2013/01/15/pubmed-and-f1000-research-unclear-standards-applied-unevenly/ |work=[[Scholarly Kitchen]] |accessdate=14 August 2013}}</ref><ref>{{cite web |last=Lawrence|first=Rebecca|title=F1000Research now visible on PubMed and PubMed Central |url=http://blog.f1000research.com/2013/12/12/f1000research-now-visible-on-pubmed-and-pubmed-central/ |work=F1000Research blog |accessdate=14 April 2014}}</ref> F1000Research also publishes posters and slide presentations in biology and medicine.
In October 2014, F1000Research's Managing Director Rebecca Lawrence took part in a Reddit Science AMA (Ask Me Anything) as part of Open Access Week, to answer questions about the F1000Research publication format and about open science in general.<ref>{{cite web |last=Lawrence|first=Rebecca|title=Science AMA Series: I'm Rebecca Lawrence, Managing Director of F1000Research, an Open Science publishing platform designed to turn traditional publishing models on their head. The journal is dead – discuss, and AMA |url=http://www.reddit.com/r/science/comments/2jvpsj/science_ama_series_im_rebecca_lawrence_managing/ |work=[[Reddit]] |accessdate=26 June 2015}}</ref>

===F1000Workspace===
'''''F1000Workspace''''' is a suite of tools to help scientists with writing, collaborating, reference management and preparation for publishing scientific papers.<ref>{{cite web |title=Work smart with Workspace, our new platform for writing papers  |url=http://blog.f1000.com/2015/05/06/work-smart-with-workspace-our-new-platform-for-writing-papers/ |accessdate=26 June 2015}}</ref> The service was launched in May 2015.<ref>{{cite web |title=Faculty of 1000 Workspace |url=http://news.hsl.virginia.edu/?p=6583 |work=Claude Moore Health Sciences Library, University of Virginia |accessdate=26 June 2015}}</ref> It includes a Word plug-in, from which you're able to directly search PubMed, as well as save references or articles with just one click. F1000Workspace also allows you to directly annotate articles online, as well as share these notes - and whole projects - with co-authors.

===F1000 Specialists===
'''F1000 Specialists''' – An affiliate program aimed at experienced users and advocates of F1000 services. F1000 Specialists receive in-kind rewards from F1000 in exchange for being a local representative and contact for one or more F1000 services at their organization.

== History ==
Faculty of 1000 was founded by publishing entrepreneur [[Vitek Tracz]] in 2000.<ref>{{cite journal |doi=10.1126/science.342.6154.66 |title=The Seer of Science Publishing |year=2013 |last1=Rabesandratana |first1=T. |journal=[[Science (journal)|Science]] |volume=342 |issue=6154 |pages=66–7 |pmid=24092726}}</ref> The company is part of the [http://sciencenavigation.com/ Science Navigation Group], which also owns [[Web of Stories]].

In March 2015, F1000's chairman Vitek Tracz, a scientific publishing pioneer and founder of the BioMed Central and Current Opinions journals, was featured in an article in Library Journal, titled "F1000’s Vitek Tracz: Redefining Scientific Communication." <ref>{{cite journal |author=Henrietta Verma |title=	
F1000’s Vitek Tracz: Redefining Scientific Communication |url=http://reviews.libraryjournal.com/2015/03/reference/f1000s-vitek-tracz-redefining-scientific-communication/ |journal=Library Journal |accessdate=26 June 2015}}</ref> Vitek discussed the problems with peer review and why he thinks it's "mortally sick".

Vitek Tracz also published an editorial piece in F1000Research in May 2015 about the "five deadly sins of science publishing", detailing the problems with the process of preparing and publishing research findings, and judging their veracity and significance. He explains how F1000 is starting to tackle these ‘deadly sins’.<ref>{{cite journal|last =Tracz |first =Vitek |date =11 May 2015 |title =The five deadly sins of science publishing [v1; ref status: not peer reviewed, http://f1000r.es/5de]|url =http://f1000research.com/articles/4-112/v1 |journal =F1000Research  |doi =10.12688/f1000research.6488.1 |name-list-format = vanc }}</ref>

== See also ==
* [[The Winnower]]
* [[Scholarly peer review]]

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://f1000.com/}}

{{DEFAULTSORT:Faculty Of 1000}}
[[Category:Science websites]]
[[Category:Review websites]]
[[Category:Open access publishers]]