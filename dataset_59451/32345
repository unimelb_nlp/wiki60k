{{Use mdy dates|date=October 2011}}
{{Infobox Officeholder
|birthname        = James Bennett McCreary
|image       = James-B-McCreary.jpg
|caption     = McCreary in 1914
|order       = 37th
|office      = Governor of Kentucky
|term_start  = December 12, 1911
|term_end    = December 7, 1915
|lieutenant  = [[Edward J. McDermott]]
|predecessor = [[Augustus E. Willson]]
|successor   = [[Augustus Owsley Stanley|Augustus O. Stanley]]
|jr/sr2      = [[United States Senate|U.S. Senator]]
|state2      = [[Kentucky]]
|term_start2 = March 4, 1903
|term_end2    = March 3, 1909
|predecessor2 = [[William Joseph Deboe|William J. Deboe]]
|successor2   = [[William O'Connell Bradley|William O. Bradley]]
|district3    = [[Kentucky's 8th congressional district|8th]]
|state3       = [[Kentucky]]
|term_start3  = March 4, 1885
|term_end3    = March 3, 1897
|predecessor3 = [[Philip B. Thompson, Jr.]]
|successor3   = [[George M. Davison]]
|order4       = 27th
|office4      = Governor of Kentucky 
|term_start4  = August 31, 1875
|term_end4    = September 2, 1879
|predecessor4 = [[Preston H. Leslie]]
|successor4   = [[Luke P. Blackburn]]
|lieutenant4  = [[John C. Underwood]]
|office5      = Member of the [[Kentucky House of Representatives]]
|term5        = 1869–1875
|birth_date  = {{birth date|1838|7|8|mf=y}}
|birth_place = [[Richmond, Kentucky]]
|death_date  = {{death date and age|1918|10|8|1838|7|8|mf=y}}
|death_place = [[Richmond, Kentucky]]
|restingplace= Richmond Cemetery
|party       = [[Democratic Party (United States)|Democrat]]
|spouse      = Katherine Hughes
|alma_mater  = [[Centre College]]<br/>[[Cumberland University]]
|profession  = Lawyer
|religion    = [[Presbyterianism|Presbyterian]]
|branch={{army|CSA}}
|allegiance= {{flagicon|CSA|variant=1861b}} [[Confederate States of America]]
|unit={{flagicon|Kentucky}} [[11th Kentucky Cavalry]]
|rank=[[File:Confederate States of America Lieutenant Colonel.png|35px]] [[Lieutenant colonel (United States)|Lieutenant Colonel]]
|battles=[[American Civil War]]
}}

'''James Bennett McCreary''' (July 8, 1838 – October 8, 1918) was a lawyer and politician from the US state of [[Kentucky]]. He represented the state in both houses of the [[United States Congress|U.S. Congress]] and served as its [[List of Governors of Kentucky|27th and 37th]] [[Governor of Kentucky|governor]]. Shortly after graduating from law school, he was commissioned as the only [[Major (United States)|major]] in the [[11th Kentucky Cavalry]], serving under [[Confederate States of America|Confederate]] Brigadier General [[John Hunt Morgan]] during the [[American Civil War]]. He returned to his legal practice after the war. In 1869, he was elected to the [[Kentucky House of Representatives]] where he served until 1875; he was twice chosen [[Speaker of the Kentucky House of Representatives|Speaker of the House]]. At their 1875 nominating convention, state [[Democratic Party (United States)|Democrats]] chose McCreary as their nominee for governor, and he won an easy victory over [[Republican Party (United States)|Republican]] [[John Marshall Harlan]]. With the state still feeling the effects of the [[Panic of 1873]], most of McCreary's actions as governor were aimed at easing the plight of the state's poor farmers.

In 1884, McCreary was elected to the first of six consecutive terms in the [[United States House of Representatives|U.S. House of Representatives]]. As a legislator, he was an advocate of [[free silver]] and a champion of the state's agricultural interests. After two failed bids for election to the [[United States Senate|Senate]], McCreary secured the support of Governor [[J. C. W. Beckham]], and in 1902, the General Assembly elected him to the Senate. He served one largely undistinguished term, and Beckham successfully challenged him for his Senate seat in 1908. The divide between McCreary and Beckham was short-lived, however, and Beckham supported McCreary's election to a second term as governor in 1911.

Campaigning on a platform of [[Progressivism in the United States|progressive]] reforms, McCreary defeated Republican [[Edward C. O'Rear]] in the general election. During this second term, he became the first inhabitant of the state's second (and current) [[Kentucky Governor's Mansion|governor's mansion]]; he is also the only governor to have inhabited both the [[Old Governor's Mansion (Frankfort, Kentucky)|old]] and new mansions. During his second term, he succeeded in convincing the legislature to make [[Women's suffrage in the United States|women eligible to vote]] in school board elections, to mandate direct [[primary election]]s, to create a state [[public utilities commission]], and to allow the state's counties to hold [[local option]] elections to decide whether or not to adopt [[Prohibition in the United States|prohibition]]. He also realized substantial increases in education spending and won passage of reforms such as a mandatory school attendance law, but was unable to secure passage of laws restricting lobbying in the legislative chambers and providing for a [[workers' compensation]] program. McCreary was one of five commissioners charged with overseeing construction of the new governor's mansion and exerted considerable influence on the construction plans. His term expired in 1915, and he died three years later. [[McCreary County, Kentucky|McCreary County]] was formed during McCreary's second term in office and was named in his honor.

==Early life==
James Bennett McCreary was born in [[Richmond, Kentucky]], on July 8, 1838.<ref name=bioguide>"McCreary, James Bennett". ''Biographical Directory of the United States Congress''</ref> He was the son of Edmund R. and Sabrina (Bennett) McCreary.<ref name=kye>Harrison in ''The Kentucky Encyclopedia'', p. 597</ref> He obtained his early education in the region's common schools, then matriculated to [[Centre College]] in [[Danville, Kentucky]], where he earned a bachelor's degree in 1857.<ref name=bioguide /><ref name=kygovs105>Burckel in ''Kentucky's Governors'', p. 105</ref> Immediately thereafter, he enrolled at [[Cumberland University]] in [[Lebanon, Tennessee]], to study law.<ref name=mcafee118>McAfee, p. 118</ref> In 1859, he earned a [[Bachelor of Laws]] from Cumberland and was [[valedictorian]] of his class of forty-seven students; he was admitted to the [[Bar (law)|bar]] and commenced practice at Richmond.<ref name=bioguide /><ref name=mcafee119>McAfee, p. 119</ref>

Shortly after the [[Battle of Richmond]] on August 29, 1862, [[David Waller Chenault]], a Confederate sympathizer from [[Madison County, Kentucky|Madison County]], came to Richmond to raise a Confederate regiment. On September 10, 1862, Chenault was commissioned as a [[Colonel (United States)|colonel]] and given command of the regiment, dubbed the [[11th Kentucky Cavalry]]. McCreary joined the regiment and was commissioned as a [[Major (United States)|major]], the only one in the unit. The 11th Kentucky Cavalry was pressed into immediate service, conducting [[reconnaissance]] and fighting [[bushwhacker]]s. Just three months after its muster, they helped the [[Confederate States Army|Confederate Army]] secure a victory at the [[Battle of Hartsville]]. In 1863, the unit joined [[John Hunt Morgan]] for his [[Morgan's Raid|raid into Ohio]]. Colonel Chenault was killed as the Confederates tried to capture the [[Green River (Kentucky)|Green River]] Bridge at the July 4, 1863, [[Battle of Tebbs Bend]]. McCreary assumed command of the unit after Chenault's death. Following the battle, he was promoted to the rank of lieutenant colonel on the recommendation of [[John C. Breckinridge]].<ref>Johnson, pp. 793–794</ref>

Most of the 11th Kentucky Cavalry was captured by [[Union Army|Union forces]] at the [[Battle of Buffington Island]] on July 17, 1863. Approximately two hundred men, commanded by McCreary, mounted a charge and escaped their captors, but they were surrounded the next day and surrendered. McCreary was taken to Ninth Street Prison in [[Cincinnati]], Ohio, but was later transferred to [[Fort Delaware]] and eventually to [[Morris Island]], [[South Carolina]], where he remained a prisoner through July and most of August 1863. In late August, he was released as part of a [[prisoner exchange]] and taken to [[Richmond, Virginia]]. He was granted a thirty-day [[furlough]] before being put in command of a [[battalion]] of Kentucky and South Carolina troops. He commanded this unit, primarily on scouting missions, until the end of the war.<ref name=johnson794>Johnson, p. 794</ref>

Following the war, McCreary resumed his legal practice.<ref name=powell62>Powell, p. 62</ref> On June 12, 1867, McCreary married Katherine Hughes, the only daughter of a wealthy [[Fayette County, Kentucky|Fayette County]] farmer.<ref name=powell62 /> The couple had one son.<ref name=kye />

==Early political career==
McCreary was nominated to serve as a [[United States Electoral College|presidential elector]] for the ticket of Democrat [[Horatio Seymour]] in 1868; though he declined to serve, he attended the [[1868 Democratic National Convention|national convention]] as a delegate.<ref name=johnson794 /> His political career began in earnest in 1869 when he was elected to the [[Kentucky House of Representatives]].<ref name=bioguide />

In 1871, McCreary was re-elected to the state House without opposition.<ref name=mcafee119 /> In the upcoming legislative session, the major question was expected to be the [[Cincinnati, New Orleans and Texas Pacific Railway|Cincinnati Southern Railway]]'s request for authorization to build a track connecting [[Cincinnati]], Ohio, with either [[Knoxville, Tennessee|Knoxville]] or [[Chattanooga, Tennessee]], through [[Central Kentucky]].<ref name=tapp54>Tapp and Klotter, p. 54</ref> The action was opposed by the [[Louisville and Nashville Railroad]], a bitter rival of the Cincinnati line.<ref name=tapp54 /> Appeals to the [[Kentucky General Assembly|General Assembly]] to oppose the bill on grounds that an out-of-state corporation should not be granted a charter in the state were successful in 1869 and 1870, and an attempt by the federal Congress to grant the charter was defeated by [[states' rights]] legislators there.<ref>Tapp and Klotter, pp. 55–56</ref> Moreover, newly elected governor [[Preston Leslie]] had opposed a bill granting Cincinnati Southern's request when he was in the [[Kentucky Senate|state Senate]] in 1869.<ref name=tapp57>Tapp and Klotter, p. 57</ref> In the lead-up to the 1871 session, frustrated Central Kentuckians threatened to defect from the Democratic Party in future elections if the bill were not passed in the session.<ref name=tapp57 /> Supporters of Cincinnati Southern won a victory when McCreary, a staunch supporter of the bill to grant the line's request, was elected [[Speaker of the Kentucky House of Representatives|Speaker of the House]].<ref name=tapp57 /> After approval of a series of amendments designed to give Kentucky courts some jurisdiction in cases involving the line and the Kentucky General Assembly some measure of control over the line's activities, the bill passed the House by a vote of 59–38.<ref name=tapp57 /> The vote in the Senate resulted in a 19–19 tie; [[President pro tempore of the Kentucky Senate|President Pro Tem]] [[John G. Carlisle]]—a native of [[Covington, Kentucky|Covington]], through which the proposed line would pass—cast the deciding vote in favor of approving Cincinnati Southern's request.<ref name=tapp58>Tapp and Klotter, p. 58</ref> With the will of the people clearly expressed through the legislature, Governor Leslie did not employ his gubernatorial veto.<ref name=tapp58 /> McCreary was again returned to the House without opposition in 1873 and was again chosen Speaker of the House during his term.<ref name=mcafee119 />

==First term as governor==
In 1875, McCreary was one of four men, all former Confederate soldiers, who sought the Democratic gubernatorial nomination—the others being [[John Stuart Williams]], J. Stoddard Johnson, and George B. Hodge.<ref name=johnson794 /> Williams was considered the favorite for the nomination at the outset of the Democratic nominating convention, despite attacks on his character by newspapers in the western part of the state.<ref name=tapp132>Tapp and Klotter, p. 132</ref> However, McCreary defeated Williams on the fourth ballot.<ref name=tapp132 />

[[File:JudgeJMHarlan.jpg|thumb|left|upright|alt=A balding man in his fifties wearing black judicial robes|John Marshall Harlan, McCreary's opponent in the 1875 gubernatorial contest]]
The Republicans nominated [[John Marshall Harlan]], who had served in the Union Army.<ref name=tapp137>Tapp and Klotter, p. 137</ref> In joint debates across the state, McCreary stressed what many Kentuckians felt were abuses of power by Republican President [[Ulysses S. Grant]] during the [[Reconstruction era of the United States|Reconstruction Era]].<ref name=kye /> Harlan countered by faulting the state's Democratic politicians for continuing to dwell on war issues almost a decade after the war's end.<ref name=tapp137 /> He also attacked what he perceived as Democratic financial extravagance and the high number of [[clemency|pardons]] granted by sitting Democratic governor Preston Leslie.<ref name=tapp137 /> Harlan claimed these as evidence of widespread corruption in the Democratic Party.<ref name=tapp137 /> McCreary received solid support from the state's newspapers, nearly all of which had Democratic sympathies.<ref name=tapp137 /> Despite a late infusion of cash and stump speakers in favor of his opponent, McCreary won the general election by a vote of 130,026 to 94,236.<ref name=kye /><ref name=powell62 />

At the time of McCreary's election, his wife Kate was the youngest first lady in the Commonwealth's history. Due to the near completion of an annex to the state capitol building by the time of McCreary's inauguration, he was able to move the official governor's office out of the [[Old Governor's Mansion (Frankfort, Kentucky)|governor's mansion]], freeing his family from the intrusion of public business into their private quarters. McCreary's receipt of the executive journal and Great Seal of the Commonwealth from outgoing Governor Leslie in the mansion's office is believed to be the last official act performed by a governor there.<ref name=clark55>Clark and Lane, p. 55</ref>

In the wake of the [[Panic of 1873]], the electorate was primarily concerned with economic issues.<ref name=tapp145>Tapp and Klotter, p. 145</ref> In his first address to the General Assembly, McCreary focused on economic issues to the near exclusion of providing any leadership or direction in the area of government reforms.<ref name=tapp145 /> (In later years, McCreary's unwillingness to take a definite stand on key issues of reform would earn him the nicknames "Bothsides" McCreary and "Oily Jeems".)<ref name=klotter218>Klotter, p. 218</ref> In response to McCreary's address, legislators from the rural, agrarian areas of the state proposed lowering the maximum legal interest rate from ten percent to six percent.<ref name=tapp146>Tapp and Klotter, p. 146</ref> The proposed legislation drew the ire of bankers and capitalists; it was also widely panned in the press, notably by ''[[Louisville Courier-Journal]]'' editor [[Henry Watterson]].<ref name=tapp146 /> Ultimately, the Assembly compromised on a legal interest rate of eight percent.<ref name=tapp146 /> Another bill to lower the [[property tax]] rate from 45 to 40 cents per 100 dollars of taxable property encountered far less resistance and passed easily.<ref name=tapp146 /> Few bills passed during the session had statewide impact, despite McCreary's insistence that the legislature prefer general bills over bills of local impact.<ref name=kygovs106>Burckel in ''Kentucky's Governors'', p. 106</ref> This fact, too, was widely criticized by the state's newspapers.<ref name=tapp146 />

The issue of improving navigation along the [[Kentucky River]] was raised numerous times by Representative James Blue during the 1876 legislative session. Despite Blue's promises of manifold benefits to the state from such an investment, parsimonious legislators defeated a bill allocating funds for the improvements. The issue gained traction with some voters during the biennial legislative elections, however, which brought it back to the floor in the 1878 session. Prompted by recommendations from the Kentucky River Navigation Convention in 1877, McCreary abandoned his typical fiscal conservatism and joined the calls for improvements along the river. In response, legislators passed a largely ineffective bill providing that, if funds could be raised through special taxes in districts along the river, the state would provide the funds to maintain the improvements.<ref name=tapp156>Tapp and Klotter, pp. 146, 154–156</ref>

Also in the 1878 session, tax assessments for railroad property were raised to match those of other property.<ref name=kygovs106 /> Agrarian interests were pleased that the legal interest rate was again lowered, now reaching the six percent they had proposed in the previous session.<ref name=tapp159>Tapp and Klotter, p. 159</ref> Non-economic reforms included the separation of Kentucky Agricultural and Mechanical College (later the [[University of Kentucky]]) from Kentucky University (later [[Transylvania University]]) and the establishment of a state board of health.<ref name=kygovs106 /> Bills of local import again dominated the session, representing 90 percent of the acts and resolutions passed by the Assembly.<ref name=tapp160>Tapp and Klotter, p. 160</ref>

Along with Democrats John Stuart Williams, [[William Lindsay (Kentucky)|William Lindsay]], and [[J. Proctor Knott]], and Republican Robert Boyd, McCreary was nominated for a [[United States Senate|U.S. Senate]] seat in 1878. Democrats were divided by sectionalism and initially unable to unite behind one of their four candidates. After more than a week of [[caucus]]ing among Democratic legislators, the nominations of McCreary, Knott, and Lindsay were withdrawn, and Williams was elected over Boyd. Historian Hambleton Tapp opined that the withdrawals were likely a part of some kind of deal among legislators, although the details of the deal, if it ever existed, were not made public.<ref name=tapp157>Tapp and Klotter, p. 157</ref>

==Service in Congress==
Following his term as governor, McCreary returned to his legal practice.<ref name=kye /> In 1884, he sought election to Congress from [[Kentucky's 8th congressional district|Kentucky's Eighth District]].<ref name=mcafee120>McAfee, p. 120</ref> His opponents for the Democratic nomination were [[Milton J. Durham]] and [[Philip B. Thompson, Jr.]], both of whom had held the district's seat previously.<ref name=mcafee120 /> McCreary bested both men, and in the general election in November, defeated Republican James Sebastian by a margin of 2,146 votes.<ref name=mcafee120 /> It was the largest margin of victory by a Democrat in the Eighth District.<ref name=mcafee120 />

During his tenure, McCreary represented Kentucky's agricultural interests, introducing a bill to create the [[United States Department of Agriculture]]. A bill containing most of the same provisions as the one McCreary authored was passed later in the session. He also proposed a successful amendment to the [[Wilson–Gorman Tariff Act]] that excluded farm implements and machinery from the tariff. An advocate of [[free silver]], he was appointed by President [[Benjamin Harrison]] to be a delegate to the [[International Monetary Conference]] held in [[Brussels, Belgium]], in 1892. As chairman of the [[United States House Committee on Foreign Affairs|House Committee on Foreign Affairs]], he authored a bill to establish a court that would settle disputed land claims stemming from the [[Gadsden Purchase]] and the [[Treaty of Guadalupe-Hidalgo]]. He advocated the creation of a railroad linking Canada, the United States, and Mexico. In 1890, he sponsored a bill authorizing the first [[Pan-American Conference]] and was an advocate of the Pan-American Medical Conference that met in Washington, D.C., in 1893. He authored a report declaring American hostility to European ownership of a canal connecting the Atlantic and Pacific Oceans, and sponsored legislation authorizing the U.S. president to retaliate against foreign vessels that harassed American fishing boats.<ref name=johnson795>Johnson, p. 795</ref>

[[File:J. C. W. Beckham.jpg|thumb|right|upright|alt=Cleanshaven man with drooping eyelids, aged about 40. He is wearing a black bowler hat, white shirt, tie and dark overcoat.|J. C. W. Beckham, McCreary's sometime ally, succeeded him in the Senate.]]
In 1890, McCreary's name was again placed in nomination for a U.S. Senate seat to succeed [[James B. Beck]], who died in office.<ref name=tapp251>Tapp and Klotter, p. 251</ref> John G. Carlisle, J. Proctor Knott, William Lindsay, [[Laban T. Moore]], and [[Evan E. Settle]] were also nominated by various factions of the Democratic Party; Republicans nominated [[Silas Adams]].<ref>Tapp and Klotter, pp. 251–252</ref> Carlisle was elected on the ninth ballot.<ref name=tapp252>Tapp and Klotter, p. 252</ref> McCreary continued his service in the House until 1896, when he was defeated in his bid for a seventh consecutive nomination for the seat.<ref name=bioguide /> In that same year, his was among a myriad of names put forward for election to the Senate, but he never received more than 13 votes.<ref name=tapp356>Tapp and Klotter, p. 356</ref> Following these defeats, he resumed his law practice in Richmond.<ref name=bioguide />

McCreary campaigned for Democrat [[William Goebel]] during the controversial [[Kentucky gubernatorial election, 1899|1899 gubernatorial campaign]].<ref name=tapp429>Tapp and Klotter, p. 429</ref> Between 1900 and 1912, he represented Kentucky at four consecutive [[Democratic National Convention]]s.<ref name=powell62 /> Governor J. C. W. Beckham and his well-established [[political machine]] supported McCreary's nomination to the Senate in 1902.<ref name=klotter205>Klotter, p. 205</ref> His opponent, incumbent [[William J. Deboe]], had been elected as a compromise candidate six years earlier, becoming Kentucky's first-ever Republican senator.<ref name=klotter205 /> Deboe had done little to secure support from legislators since his election, however, and McCreary was easily elected by a vote of 95–30.<ref name=klotter205 /> Following his election to the Senate, McCreary supported Beckham's gubernatorial re-election bid in 1903.<ref name=klotter206>Klotter, p. 206</ref> In a largely undistinguished term as a senator, he continued to advocate the free coinage of silver and tried to advance the state's agricultural interests.<ref name=kye />

McCreary's senate term was set to expire in 1908, the same year as Beckham's second term as governor. Desiring election to the Senate following his gubernatorial term, Beckham persuaded his Democratic allies to choose the party's nominees for governor and senator by a [[primary election]] held in 1906 – a year before the gubernatorial election and two years before the senatorial election. This ensured that the primary would occur during his term as governor, when he still wielded significant influence within the party. McCreary now allied himself with [[J. C. S. Blackburn]], Henry Watterson, and other Beckham opponents, and sought to defend his seat in the primary. During the primary campaign, he pointed to his record of dealing with national issues, contrasting it with Beckham's youth and inexperience at the national level. Beckham countered by citing his strong stand in favor of [[Prohibition in the United States|Prohibition]], as opposed to McCreary's more moderate position, and by touting his support of a primary election instead of a nominating convention, which he said gave the voters a choice in who would represent them in the Senate. Ultimately, Beckham prevailed in the primary by an 11,000-vote margin, rendering McCreary a [[Lame duck (politics)|lame duck]] with two years still left in his term.<ref>Klotter, pp. 210–211</ref>

==Second term as governor and death==
Despite Beckham's move to unseat McCreary in the Senate, the two were once again allies by 1911, when Beckham supported the aging McCreary for the party's gubernatorial nomination.<ref name=klotter218 /> It is unclear whether McCreary sought the reconciliation in order to secure the gubernatorial nomination or Beckham made amends with McCreary because he thought he could control McCreary's actions as governor.<ref name=klotter218 /> In the Democratic primary, McCreary defeated William Adams by a majority of 25,000 votes.<ref name=burckel298>Burckel in ''Register'', p. 298</ref>

Republicans nominated Judge [[Edward C. O'Rear]] to oppose McCreary.<ref name=kye /> There were few differences between the two men's stands on the issues. Both supported [[Progressivism in the United States|progressive]] reforms such as the [[direct election]] of senators, a non-partisan judiciary, and the creation of a [[public utilities commission]].<ref>Klotter, pp. 218–219</ref> McCreary also changed his stance on the liquor question, now agreeing with Beckham's prohibitionist position; this also matched the Republican position.<ref name=klotter219>Klotter, p. 219</ref> O'Rear claimed that Democrats should have already enacted the reforms their party platform advocated, but his only ready line of attack against McCreary himself was that he would be a pawn of Beckham and his allies.<ref name=klotter219 /><ref name=burckel299>Burckel in ''Register'', p. 299</ref>

McCreary pointed out that O'Rear had been nominated at a party nominating convention instead of winning a primary, though O'Rear claimed to support primary elections.<ref name=burckel299 /> He also criticized O'Rear for continuing to receive his salary as a judge while running for governor.<ref name=klotter219 /> McCreary cited what he called the Republicans' record of "assassination, bloodshed, and disregard of law", an allusion to the assassination of William Goebel in the aftermath of the 1899 gubernatorial contest.<ref name=klotter219 /> [[Caleb Powers]], convicted three times of being an accessory to Goebel's murder, had been pardoned by Republican governor [[Augustus Willson]] and had recently been elected to Congress.<ref name=klotter219 /> He further attacked the tariff policies of Republican President [[William H. Taft]].<ref name=klotter219 /> In the general election, McCreary won a decisive victory, garnering 226,771 votes to O'Rear's 195,435.<ref name=klotter219 /> Several other minor party candidates also received votes, including [[Socialist Party of America|Socialist]] candidate Walter Lanfersiek, who claimed 8,718 votes (2 percent of the total).<ref name=powell62 /><ref name=klotter219 />

===Construction of the new governor's mansion===
[[File:KY Governors Mansion.png|thumb|240px|alt=A pillared, two-story, gray marble building with several flower gardens in front|The current Kentucky Governor's Mansion was constructed during McCreary's second gubernatorial term]]
One of McCreary's first acts as governor was signing a bill appropriating $75,000 for the construction of a new [[Kentucky Governor's Mansion|governor's mansion]]. The legislature appointed a commission of five, including McCreary, to oversee the mansion's construction. The governor exercised a good deal of influence over the process, including the replacement of a conservatory with a ballroom in the construction plan and the selection of a contractor from his hometown of Richmond as assistant superintendent of construction. Changing societal trends also affected construction. A hastily constructed stable to house horse-drawn carriages was soon abandoned in favor of a garage for automobiles.<ref>Clark and Lane, pp. 89, 93</ref>

The mansion was completed in 1914. Because McCreary was widowed before his second term in office, his granddaughter, Harriet Newberry McCreary, served as the mansion's first hostess during her summer vacations from her studies at [[Wellesley College]]. When Harriet McCreary was away at college, McCreary's housekeeper, Jennie Shaw, served as hostess. McCreary authorized the state to sell the old mansion at auction, but the final bid of $13,600 was rejected as unfair by the mansion commission.<ref>Clark and Lane, pp. 80–81, 84</ref>

===Progressive reforms===
Among the progressive reforms advocated by McCreary and passed in the 1912 legislative session were making [[Women's suffrage in the United States|women eligible to vote]] in school board elections, mandating direct [[primary election]]s, and allowing the state's counties to hold [[local option]] elections to decide whether or not to adopt prohibition.<ref name=kye /> McCreary appointed a tax commission to study the revenue system, and the Board of Assessments and Valuation made a more realistic appraisal of corporate property.<ref name=kye /> McCreary created executive departments to oversee state banking and highways, and a bipartisan vote in the General Assembly established the [[Kentucky Public Service Commission]].<ref name=kye /><ref name=burckel302>Burckel in ''Register'', p. 302</ref> Near the close of the session, [[McCreary County, Kentucky|McCreary County]] was created and named in the governor's honor.<ref name=powell62 /> It was the last of [[List of counties in Kentucky|Kentucky's 120 counties]] to be constituted.<ref name=klotter220>Klotter, p. 220</ref>

McCreary was not as successful in securing reforms during the 1914 legislative session. He advocated a comprehensive [[workmen's compensation]] law, but the law that was passed in the 1914 General Assembly was later declared unconstitutional.<ref name=kye /><ref name=klotter224>Klotter, p. 224</ref> He also recommended a requirement for full disclosure of campaign contributions and expenditures, but the majority of legislators in the House of Representatives voted to send it back to the Suffrage and Elections Committee, from whence it was never recalled.<ref name=burckel301>Burckel in ''Register'', p. 301</ref> Although they did not pass a law regulating [[Lobbying in the United States|lobbying]] at the [[Kentucky State Capitol|capitol]] – a law that McCreary supported – legislators showed responsiveness to McCreary's desire for this reform by putting stricter regulations on who could be in the legislative chambers while the legislature was in session.<ref name=kygovs108>Burckel in ''Kentucky's Governors'', p. 108</ref> Some reforms were made in the area of education. The school year was lengthened, school attendance for children was mandated, and the legislature created a Text Book Commission to assist local school boards in adopting textbooks.<ref>Burckel in ''Register'', pp. 303–304</ref> Public schools expenditures were increased by 25 percent.<ref name=kye />

Part of the reason for the inefficacy of the 1914 session was that McCreary was engaged in a three-way primary race for the Democratic nomination to the U.S. Senate. The other major candidates were former Governor Beckham and [[Augustus O. Stanley]]; a fourth candidate, David H. Smith, withdrew early from the race. McCreary ran a mostly positive campaign, touting his own accomplishments and speaking cordially about his opponents. Beckham and Stanley, however, were bitter political and personal enemies, and the campaign reflected their animosity. Without the support of Beckham's political machine that had helped him in the gubernatorial contest, McCreary never had a realistic chance to win the nomination. Beckham secured the nomination with 72,677 votes to Stanley's 65,871 and McCreary's 20,257.<ref>Klotter, pp. 224–225</ref>

Following the expiration of his term as governor, McCreary continued to practice as a private attorney until his death on October 8, 1918.<ref name=kygovs109>Burckel in ''Kentucky's Governors'', p. 109</ref> He was buried in Richmond Cemetery.<ref name=bioguide />

==References==
{{Reflist|colwidth=20em}}
{{commons category}}

===Bibliography===
*{{cite journal |last=Burckel |first=Nicholas C. |title=From Beckham to McCreary: The Progressive Record of Kentucky Governors |journal=Register of the Kentucky Historical Society |volume=76 |date=October 1978 |pages=285–306}}
*{{cite book |last=Burckel |first=Nicholas C. |title=Kentucky's Governors |chapter=James B. McCreary |editor=[[Lowell H. Harrison]] |publisher=The University Press of Kentucky |location=Lexington, Kentucky |year=2004 |isbn=0-8131-2326-7}}
*{{cite book |last=Clark |first=Thomas D. |authorlink=Thomas D. Clark |author2=Margaret A. Lane  |title=The People's House: Governor's Mansions of Kentucky |publisher=The University Press of Kentucky |location=Lexington, Kentucky |year=2002 |isbn=0-8131-2253-8}}
*{{cite book |last=Harrison |first=Lowell H. |authorlink=Lowell H. Harrison |chapter=McCreary, James Bennett |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], Lowell H. Harrison, and [[James C. Klotter]] |title=The Kentucky Encyclopedia |year=1992 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-1772-0 |url=http://www.kyenc.org/entry/m/McCRE01.html |accessdate=2010-10-24}}
*{{cite book |last=Johnson |first=E. Polk |title=A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities |publisher=Lewis Publishing Company |location=Chicago, Illinois |year=1912 |url=https://books.google.com/books?id=FXQUAAAAYAAJ |accessdate=2008-11-10}}
*{{cite book |authorlink=James C. Klotter |last=Klotter |first=James C. |title=Kentucky: Portraits in Paradox, 1900–1950 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |year=1996 |isbn=0-916968-24-3 |url=https://books.google.com/?id=o58mJavC4msC |accessdate=2009-06-26}}
*{{cite book |title=Kentucky politicians : sketches of representative Corncrackers and other miscellany |last=McAfee |first=John J. |publisher=Press of the Courier-Journal job printing company |location=Louisville, Kentucky |year=1886 |url=http://kdl.kyvl.org/cgi/t/text/text-idx?c=kyetexts;cc=kyetexts;sid=7c3b4143decaf4c8af264312f1bb5cd1;idno=b92-77-27211894;view=toc}}
*{{cite web |title=McCreary, James Bennett |work=Biographical Directory of the United States Congress |url=http://bioguide.congress.gov/scripts/biodisplay.pl?index=M000382 |accessdate=2011-05-27}}
*{{cite book |last=Powell |first=Robert A. |title=Kentucky Governors |publisher=Bluegrass Printing Company |location=Danville, Kentucky |year=1976 |oclc=2690774}}
*{{cite book |last=Tapp |first=Hambleton |author2=James C. Klotter  |title=Kentucky: decades of discord, 1865–1900 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |year=1977 |isbn=0-916968-05-7 |url=https://books.google.com/?id=n7JIP_B_vQMC |accessdate=2009-06-26|authorlink2=James C. Klotter }}

==Further reading==
*{{cite book |last=Gorin-Smith |first=Betty Jane |authorlink=Betty Jane Gorin-Smith |title=Morgan Is Coming!: Confederate Raiders in the Heartland of Kentucky |publisher=Harmony House Publishers |location=Louisville, Kentucky |year=2006 |isbn=978-1-56469-134-7}}
*{{cite book |last=Malone |first=Dumas |title=Dictionary of American Biography |volume=12 |chapter=James Bennett McCreary |publisher=Charles Scribner's Sons |location=New York City, New York |year=1937}}
*{{cite journal |last=McCreary |first=James B. |title=The Journal of My Soldier Life |journal=The Register of the Kentucky State Historical Society |volume=33 |issue=103 |date=April 1935 |pages=97–117}}
*{{cite book |last=McCreary |first=James B. |title=Progress in Arbitration |publisher=Peace and Arbitration League |location=Washington, D. C. |year=1909}}

==External links==
*[https://archive.org/stream/menofmarkinamerica00gate#page/136/mode/2up Men of Mark in America] Biography & Portrait
*[http://www.nga.org/portal/site/nga/menuitem.29fab9fb4add37305ddcbeeb501010a0/?vgnextoid=cd1737a59b066010VgnVCM1000001a01010aRCRD&vgnextchannel=e449a0ca9e3f1010VgnVCM1000001a01010aRCRD McCreary's biography from the National Governors Association]

{{s-start}}
{{s-off}}
{{succession box
 | title = [[Governor of Kentucky]]
 | before = [[Preston H. Leslie]]
 | after = [[Luke P. Blackburn]]
 | years = 1875–1879
}}
{{succession box
 | title = [[Governor of Kentucky]]
 | before = [[Augustus E. Willson]]
 | after = [[Augustus Owsley Stanley|Augustus O. Stanley]]
 | years = 1911–1915
}}
{{s-par|us-hs}}
{{succession box
 | title = [[US Congressional Delegations from Kentucky|United States Representative (District 8) from Kentucky]]
 | before = [[Philip B. Thompson, Jr.]]
 | after = [[George M. Davison]]
 | years = 1885–1897
}}
{{s-par|us-sen}}
{{U.S. Senator box
 |state=Kentucky
 |class=3
 |before=[[William Joseph Deboe|William J. Deboe]]
 |after=[[William O'Connell Bradley|William O. Bradley]]
 |alongside=[[Joseph Clay Stiles Blackburn|Joseph C. S. Blackburn]], [[Thomas H. Paynter]]
 |years=1903–1909
}}
{{s-end}}

{{Governors of Kentucky}}
{{USSenKY}}
{{US House Foreign Affairs chairs}}
{{featured article}}

{{Authority control}}

{{DEFAULTSORT:McCreary, James B.}}
[[Category:1838 births]]
[[Category:1918 deaths]]
[[Category:American Civil War prisoners of war]]
[[Category:American Presbyterians]]
[[Category:Centre College alumni]]
[[Category:Confederate States Army officers]]
[[Category:Cumberland University alumni]]
[[Category:Governors of Kentucky]]
[[Category:Kentucky Democrats]]
[[Category:Kentucky lawyers]]
[[Category:Members of the Kentucky House of Representatives]]
[[Category:Members of the United States House of Representatives from Kentucky]]
[[Category:People from Richmond, Kentucky]]
[[Category:People of Kentucky in the American Civil War]]
[[Category:United States Senators from Kentucky]]
[[Category:Democratic Party United States Senators]]
[[Category:Democratic Party members of the United States House of Representatives]]
[[Category:Democratic Party state governors of the United States]]
[[Category:19th-century American politicians]]