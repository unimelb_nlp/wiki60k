<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 | name=Wee Bee
 | image=BWeeB.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Two seater sports plane
 | national origin=[[United Kingdom]]
 | manufacturer=[[William Beardmore and Company|William Beardmore &Co,,Ltd]], Dalmuir, Glasgow, Dumbartonshire
 | designer=W.S. Shackleton
 | first flight=mid 1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Beardmore Wee Bee''' was a single-engined [[monoplane]] built only once and specifically for the [[Lympne light aircraft trials|Lympne two-seat light aircraft trials]] held in the [[United Kingdom]] in 1924. This plane won the major prize.

==Design and development==
The Beardmore W.B.XXIV Wee Bee I<ref group=note>It often appears under this name, though W.B.XXIV was a constructor's number referring to that airframe; and the I after Bee turned out to be optimistic as only one was built and there were no variants</ref> was the company's winning entrant to the [[Lympne Airport|Lympne Aerodrome]] light aeroplane trials of 1924.  The competition rules were framed to encourage more robust designs than those that had competed as "motor-gliders" at Lympne the previous year; they were to be single-engined two-seaters, with engine capacities up to 1,1000 cc allowed. The total prize money was £3,600.;<ref>{{Harvnb|Barnes|1964| pages=189}}</ref> the first prize £2,000.<ref>''Flight'' 9 October 1924 p650</ref>

The Wee Bee<ref name="Flight">''Flight'' 25 September 1924 p591-594</ref><ref name="AJJ">{{Harvnb|Jackson|1959|pages=421}}</ref> was a high-wing monoplane, its aerodynamically thick wing divided at the centre and braced, close to the fuselage, by pairs of parallel struts to the lower longerons.  The wings were of two spar construction with plywood skinning between the two spars out as far as the bracing; outboard, only the leading edges were plywood covered, with fabric elsewhere.<ref name="Flight"/>  The outboard ailerons were mounted on [[Spar (aviation)#False spars|false spars]] as usual. In plan, the wings were almost rectangular, with an [[aspect ratio]] of about 5.5.

The fuselage was built up on six spruce longerons, with bulkheads ([[former]]s) of spruce and three-ply.<ref name="Flight593">''Flight'' 25 September 1924 p593</ref>  Rather than the usual half round deck on the top of the fuselage, the Wee Bee's decking was concave as it reached the flat fuselage sides, making for a better view from the two tandem cockpits.  These were positioned at the leading edge and just behind mid-chord, fitted with dual controls.<ref name="Flight593"/>  Both cockpits were semi-enclosed and faired neatly into the upper fuselage/ wing surface.  The front one was entered by lifting up a small hinged part of the leading edge, which was locked down for flight; this gave excellent visibility with the help of the "hollow ground" decking.<ref name="FlightA">''Flight'' 2 October 1924 [http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200639.html p.639 fig.2]</ref>  The aft cockpit had a roof hatch with a transparent celluloid window in the upper wings, with good views though openings in the sides below the wing.<ref name="Flight593"/><ref name="FlightB">''Flight'' 2 October 1924  [http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200639.html p.639 fig.5]</ref><ref name="FlightD">''Flight'' 9 October 1924  [http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200654.html p.654]</ref>  The 32&nbsp;hp (24&nbsp;kW) [[Bristol Cherub]] flat twin  engine was hung from a duralumin sheet bracket fixed to the engine bulkhead and positioned below with a pair of duralumin tubes, themselves braced to the lower longerons with steel tubes.<ref name="Flight594">''Flight'' 25 September 1924 p594</ref>  It was enclosed in a smooth, streamlined cowling, with the upper cylinder heads protruding for cooling.

The tail unit of the Wee Bee was fairly conventional.  Fin and tailplane were both integral with the fuselage and narrow in chord, carrying wider control surfaces.  Both horizontal and vertical surfaces were rather rectangular, but the rudder had a noticeable horn balance.<ref name="Flight594"/>   The main undercarriage was very simple, with two wheels mounted on a bent chrome-nickel alloy tube which passed through the fuselage bottom.<ref name="Flight"/>  This placed the wheels well clear of the fuselage horizontally - the track was 3&nbsp;ft 9 in (1.14 m) - but left the Wee Bee sitting close to the ground.<ref name="FlightE">''Flight'' 9 October 1924 [http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200655.html p.655]</ref>   Landing loads were absorbed by elastic axle bending.

==Operational history==

The only Wee Bee first flew in the late summer of 1924, ready for the Lympne competition.  The preliminary trial for this were held at the weekend of 27–8 September, where, surprisingly only seven of the initial nineteen entrants did enough to go to the competition proper.<ref name="FlightC">''Flight'' 2 October 1924 [http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200631.html p.631]</ref><ref group=note>''Flight'' says six, but in a later note the following week noted one of the Parnall Pixies had ''not'' dropped out with engine trouble</ref>  As well as the Wee Bee, the other competitors were the [[Bristol Brownie]], two [[Hawker Cygnet]]s, the [[Parnall Pixie]], the [[Cranwell CLA.2]] and the [[Westland Woodpigeon]].<ref name="FlightG">''Flight'' 9 October 1924 [http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200650.html p.650]</ref>  The Wee Bee was flown by Maurice Piercey.<ref group=note>Jackson calls him "Pearcy"</ref>

The aircraft were given marks for four tasks: speed over two sets of five laps, slow speed flying and take off and landing distances.<ref name="FlightL">''Flight'' 9 October 1924 pp.650-9</ref> Only the Wee Bee and the Brownie, both Cherub engined, had the reliability to complete the speed tests, in which the Wee Bee achieved 70.1&nbsp;mph (112.8&nbsp;km/h), about 5&nbsp;mph faster than the Bristol.  It flew at just under 40&nbsp;mph (64&nbsp;km/h) and took off to clear a 6&nbsp;ft (1.83 m) barrier with a run of 705&nbsp;ft (215 m).  Given the Wee Bee's clean aerodynamics it was not surprising to find it took to greatest distance to pull up at landing; wheel brakes were not fitted.  Despite the long landing, the Wee Bee ended the competition well ahead in marks of the Brownie, its nearest rival, winning the Air Ministry first prize of £2,000.<ref name="Flight"/>

At Lympne in 1924 the Wee Bee flew as "No.4",<ref name="FlightC"/> but at the [[Royal Aero Club]] meeting of  August 1925,<ref>[http://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200497.html "RAeC 1925 August Meeting at Lympne"] ''Flight'' 6 August 1925 pp.497-510</ref> also held at Lympne, it appeared as ''G-EBJJ''.<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=EBJJ CAA record for G-EBJJ]</ref> At that meeting, with an aluminium front cockpit hatch in place of the earlier wooden one, it won the light two-seater race and came fourth in the [[Grosvenor Cup]], both times piloted by A. N. Kingswill. In December 1933 it was sold to Percy Parker of Warrnambool, Victoria,  Australia, where it was registered as ''VH-URJ''. Mr. Parker sold it prior to 1939.<ref name="AJJ"/>  It flew until 1939 and was then stored, but probably flew again a few years after the end of [[World War II]].,<ref name="AJJ"/><ref>[http://www.flightglobal.com/pdfarchive/view/1949/1949%20-%201694.html "Indestructibility Plus !"] ''Flight'' 6 October 1949 p. 458]</ref> though exact dates are uncertain.
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==

{{aerospecs
|ref=''Flight'' 25 September 1924 p.592<!-- reference -->
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=2
|capacity=
|length m=6.76
|length ft=22
|length in=2
|span m=11.58
|span ft=38
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.18
|height ft=7
|height in=2
|wing area sqm=19.37
|wing area sqft=including ailerons 208.5
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=211
|empty weight lb=<ref name="AJJ"/> 465
|gross weight kg=381
|gross weight lb=<ref name="AJJ"/> 840
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Bristol Cherub]] I<ref>{{Harvnb|Ord-Hume|2000|pp=266–7}}</ref> air cooled flat twin
|eng1 kw=24<!-- prop engines -->
|eng1 hp=32<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=140
|max speed mph=<ref name="AJJ"/> 87
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Notes and citations===
;Notes
{{reflist |group="note"}}
;Citations
{{reflist|2}}

===Bibliography===
{{commons category|Beardmore Wee Bee}}
*{{cite book |title= Bristol Aircraft since 1910|last= Barnes|first=C.H. |year=1964 |publisher=Putnam Publishing  |location=London |isbn= 0-370-00015-3|ref=harv}}
*{{cite book |title= British Civil Aircraft 1919-59|last=Jackson|first=A.J.| year=1959|volume= 1|publisher=Putnam Publishing |location=London |isbn=|ref=harv}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6|ref=harv}}
*{{citation |url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200591.html |title=The Beardmore "Wee Bee I" Light Monoplane (No. 4) |journal=Flight |date=25 September 1924 |pages=591–4}}
*{{citation |url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200650.html |title=Two-Seater Light Planes Competitions at Lympne |journal=Flight |date=9 October 1924 |pages=650–9}}
{{refbegin}}
{{refend}}

<!-- ==External links== -->
{{Beardmore aircraft}}

[[Category:Beardmore aircraft|Wee Bee]]
[[Category:British sport aircraft 1920–1929]]