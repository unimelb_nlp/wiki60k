{{BLP sources|date=February 2015}}


'''Antonio Colinas Lobato''' is a Spanish writer (poet, novelist, journalist, translator, essayist) and intellectual who was born in [[La Bañeza]], [[León, Spain]] on January 30, 1946.<ref>{{cite book |last=Alonso |first=Luis Miguel |year=2000 |title=Antonio Colinas, un clásico del siglo XXI |publisher=Ediciones Universidad de León |page=14 |isbn=8477198640}}</ref>  He has published a variety of works, but is considered to be above all a [[poet]].  He won Spain's [[National Prize for Literature (Spain)|National Prize for Literature]] in 1982, among several other honors and awards.

== Prizes and Honors ==

*1976 - Premio de la Crítica de poesía castellana for ''Sepulcro en Tarquinia''
*1982 - Premio Nacional de Literatura ([[National Prize for Literature (Spain)]]) for the anthology ''Poesía, 1967-1980''<ref>{{cite book |last=Amell |first=Samuel |year=1990 |title=Literature, the arts, and democracy: Spain in the eighties |publisher=[[Fairleigh Dickinson University Press]] |page=75 |isbn=9780838633731}}</ref>
*1996 - Mención Especial del Premio Internacional Jovellanos de Ensayo for ''Sobre la Vida Nueva''
*1998 - Premio Castilla y León de las Letras
*1999 - Premio Internacional Carlo Betocchi for his work as a translator and scholar of Italian literature
*2001 - Premio de la Academia Castellana y Leonesa de Poesía
*2005 - Premio Nacional de Traducción, given by the Ministerio de Asuntos Exteriores de Italia, for his translation of the complete poetry of Nobel-Prize winner Quasimodo
*2006 - "Alubia de Oro", award giving him the title of "Personaje Bañezano del Año 2006", created by the weekly publication ''El Adelanto Bañezano''
*2006 - Premio Leonés del Año 2006 (given by Cadena Ser)
*2008 - Pregonero Vitalicio de la Feria del Libro de Salamanca
*2011 - Hijo Adoptivo de Salamanca
*2012 - X Premio de la Crítica de Castilla y León
*2014 - [http://www.leonoticias.com/frontend/leonoticias/Grande-Antonio-Colinas-vn156291-vst240 Premio de las Letras Teresa de Ávila]
*2016 - [http://www.abc.es/cultura/libros/abci-antonio-colinas-premio-reina-sofia-poesia-iberoamericana-201605181407_noticia.html XXV Premio Reina Sofía de Poesía Iberoamericana]

== Individual Books of Poetry ==

*''Junto al lago''. Salamanca: Cuadernos para Lisa, 2001. (Written in 1967)
*''Poemas de la tierra y de la sangre''. León: Diputación Provincial, 1969.
*''Preludios a una noche total''. Madrid: Rialp, col. Adonais, 1969.
*''Truenos y flautas en un templo''. San Sebastián: C.A.G. de Guipúzcoa, 1972.
*''Sepulcro en Tarquinia''. León: Diputación Provincial, col. Provincia, 1975. '''*2nd edition''': ''Sepulcro en Tarquinia''. Barcelona, Lumen, Col. El Bardo, 1976. '''*3rd edition''': ''Sepulcro en Tarquinia'' (poema w/ 6 drawings by Montserrat Ramoneda). Barcelona: Galería Amagatotis, 1982. '''*4th edition''': ''Sepulcro en Tarquinia'', poem w/ prologue by Juan Manuel Rozas. Segovia: Pavesas, 1994. '''*5th edition''': ''Sepulcro en Tarquinia'' (poem with illustrations by Ramón Pérez Carrió). Pedreguer (Alicante): Collection “Font de La Cometa”, 1999. '''*6th edition''': ''Sepulcro en Tarquinia'' (commemoration of the first edition (1975-2005) w/ CD of the poet's voice).  Madrid: Visor Libros, 2005. '''*7th edition''': ''Sepulcro en Tarquinia'' (poem, illuminated by the artist Javier Alcaíns). Mérida: Editora Regional de Extremadura, 2009.
*''Astrolabio''. Madrid: Visor Libros, 1979.
*''En lo oscuro''. Rota (Cádiz): Cuadernos de Cera, 1981.
*''Noche más allá de la noche''. Madrid: Visor Libros, 1983. '''*2nd edition''': ''Noche más allá de la noche''. Valladolid: Fundación Jorge Guillén, 2004.
*''La viña salvaje''. Córdoba: Antorcha de Paja, 1985.
*''Diapasón infinito'' (w/ illustrations by Perejaume). Barcelona: Tallers Chardon y Yamamoto, 1986.
*''Dieciocho poemas''. Ibiza: Caixa Balears, 1987.
*''Material de lectura''. México: Universidad Nacional Autónoma de México, 1987.
*''Jardín de Orfeo''. Madrid: Visor Libros, 1988.
*''Libro de las noches abiertas'' (w/ 16 illustrations by Mario Arlati). Milan: Peter Pfeiffer, 1989.
*''Blanco / Negro'' (con 5 ilustraciones de Mario Arlati, ed. bilingüe). Milan: Peter Pfeiffer, 1990.
*''Los silencios de fuego''. Barcelona: Tusquets, col. Marginales, 1992.
*''La hora interior''. Barcelona: Taller Joan Roma, 1992.
*''Pájaros en el muro / Birds in the wall'' (bilingual, w/ 3 illustrations by Barry Flanagan). Barcelona: Taller Joan Roma, 1995.
*''Libro de la mansedumbre''. Barcelona, Tusquets, col. Nuevos Textos Sagrados, 1997.
*''Córdoba adolescente''. Córdoba: CajaSur, col. Los Cuadernos de Sandua, 1997.
*''Amor que enciende más amor''. Barcelona: Plaza y Janés, 1999.
*''Nueve poemas''. Salamanca: Celya, col. Aedo de Poesía, 2000.
*''Tiempo y abismo''. Barcelona: Tusquets, col. Nuevos Textos Sagrados, 2002.
*''L'amour, el amor'' (w/ poems by Michel Bohbot; illustrations by Irriguible). Paris: Editions du Labyrinthe, 2002.
*''Obscur hautbois de brume'' (bilingual anthology by Françoise, et al. Contains ''Noche más allá de la noche''). Bruxelles: Le Cri, 2003.
*''Seis poemas'' (commentary by Luis Miguel Alonso). Burgos: Instituto de la Lengua de Castilla y León, 2003.
*''Treinta y ocho poemas'' (tribute to Antonio Manso). Madrid: Real Casa de la Moneda, 2003.
*''En Ávila unas pocas palabras''. Valladolid: El Gato Gris, 2004.
*''Respirar adentro'' (w/ photos by Gianfranco Negri-Clementi). Milan: Scheiwiller, 2006.
*''Trilogía de la mansedumbre'' (edition not sold). Valladolid: Junta de Castilla y León, 2006.
*''Donde la luz llora luz''. Valladolid: El Gato Gris, 2007.
*''Riberas del Órbigo'' (w/ photos by Manuel Raigada). La Bañeza (León): Ayuntamiento de La Bañeza, 2007.
*''Desiertos de la luz''. Barcelona: Tusquets, 2008.
*''Catorce retratos de mujer''. Salamanca: El Zurguén, 2011. '''*2nd edition''': ''Catorce retratos de mujer''. Cuenca: Segundo Santos Ediciones, 2011.  '''*3rd edition''': ''Catorce retratos de mujer'' (w/ 14 illustrations by Cis Lenaerts). Ibiza: Edicions H. Jenniger, 2011.
*''Canciones para una música silente''. Madrid: Siruela, 2014.

== Poetic Anthologies ==

*''Poesía, 1967-1980''. Madrid: Visor Libros, 1982.
*''Poesía, 1967-1981''. Madrid: Visor Libros, 1984.
*''El río de sombra: Poesía 1967-1990''. Madrid: Visor Libros, 1994.
*''El río de sombra: Treinta años de poesía, 1967-1997''. Madrid: Visor Libros, 1999.
*''La hora interior: Antología poética 1967-2001''. Salamanca[?]: Junta de Castilla y León, 2002.
*''El río de sombra: Treinta y cinco años de poesía, 1967-2002''. Madrid: Visor Libros, 2004.
*''En la luz respirada'' (critical edition of ''Sepulcro en Tarquinia'', ''Noche más allá de la noche'', and ''Libro de la mansedumbre''). Edited by José E. Martínez). Madrid: Cátedra, 2004.
*''La luz es nuestra sangre''. León: Edilesa, 2006.
*''Antología''. Tenerife: Caja Canarias, 2007.
*''Nueva ofrenda''. Cáceres: Colección Abezetario, 2009.
*''Obra poética completa''. Madrid: Ediciones Siruela, 2011.
*''Obra poética completa''. México DF: Fondo de Cultura Económica/Conaculta, 2011.

== Narrative, Essay, and Other Prose ==

*''Viaje a los monasterios de España''. Barcelona: Planeta (“Biblioteca Cultural RTV” Collection), 1976. (New edition: León: Edilesa, 2003)
*''Un año en el sur: para una educación estética''. Madrid: Trieste, 1985.
*''Larga carta a Francesca''. Barcelona: Seix Barral, 1986.
*''Días en Petavonium''. Barcelona: Tusquets Editores, 1994.
*''El crujido de la luz''. León: Edilesa, 1999.
*''Huellas''. Valladolid: Castilla Ediciones, 2003.
*''Cerca de la Montaña Kumgang''. Salamanca: Amarú Edciones, 2007.
*''Leyendo en las piedras''. Madrid: Siruela, 2007.
*''El sentido primero de la palabra poética''. Madrid: Siruela, 2008.
*"El hombre que odiaba los árboles." In ''Narraciones de la Escuela''. Ed. Isabel Cantón. Barcelona: Editorial Davinci, 2009.

== Critical Bibliography on Antonio Colinas’s Works: Books, Articles, Press ==

*Agustín Fernández, Susana. ''Inventario de Antonio Colinas''. Burgos: Fundación Instituto Castellano y Leonés de la Lengua, 2007.
*Alonso Gutiérrez, Luis Miguel. ''El corazón desmemoriado: Claves poéticas de Antonio Colinas''. Salamanca: Diputación Provincial de León, 1990.
*Alonso Gutiérrez, Luis Miguel. ''Antonio Colinas: un clásico del siglo XXI''. León: Universidad de León, 2000.
*Amusco, Alejandro.  "Inteligencia es belleza."  Rev. of ''Sepulcro en Tarquinia'' by Antonio Colinas.  ''El Ciervo'' 25, No. 276 (Jan. 1976): 29-30.
*Calleja Medel, Gilda Virginia. ''Antonio Colinas, traductor''. León: Universidad de León, 2001.
*Carnicero, Luis.  ''Sobre el contemplar: Antonio Colinas: "Poesía vivida y vida ensoñada"''. León: Letras de Venatia, 2007.
*Fellie, Maria C.  ''Antonio Colinas: The Re-Writing of ‘Sepulcro en Tarquinia’ in Larga carta a Francesca''.  Thesis. [[University of North Carolina at Chapel Hill]], 2009.
*de la Fuente, Manuel. [http://www.abc.es/20110224/cultura-libros/abcp-antonio-colinas-poesia-como-20110224.html/ "Antonio Colinas, poesía como un bálsamo para tiempos de crisis."] ''ABC'' 24 February 2011.
*Llamazares, Julio.  "La poesía de Antonio Colinas."  ''El viaje hacia el centro''. Madrid: Calambur, 1997. 103-109.
*Lucas, Antonio. [http://www.elmundo.es/cultura/2014/06/04/538e178522601def4f8b457e.html/ "A pesar de todo, mi visión del mundo siempre es esperanzada."] Interview of Antonio Colinas. ''El mundo'' 4 June 2014.
*Martín-Estudillo, Luis.  "Europa en el imaginario poético de la España contemporánea (1966-2006): del utopismo ansioso al desencanto crítico.”  ''Bulletin of Hispanic Studies'' 87.7 (2010): 801-819.
*Martínez Cantón, Clara Isabel. ''Métrica y poética de Antonio Colinas''. Seville: Padilla Libros, 2011.
*Martínez Fernández, José Enrique. "Introducción" to ''En la luz respirada'' by Antonio Colinas. Madrid: Cátedra, 2004. 13-131.
*Moliner, Luis. ''Respirar: La palabra poética de Antonio Colinas''. Madrid: Devenir, 2007.
*Nana Tadoun, Guy Merlin.  [http://gredos.usal.es/jspui/handle/10366/22458/ ''Antonio Colinas o la escritura como aventura circular: Poesía y transtextualidad desde su trilogía final (1992-2002)''.] Dissertation. U de Salamanca, 2008.
*Olivio Jiménez, José. "Prólogo: La poesía de Antonio Colinas." ''Antonio Colinas: Poesía, 1967-1980''. Madrid: Visor, 1982. 7-33.
*Pritchett, Kay.  "Antonio Colinas’s Larga carta a Francesca: A Lacanian Approach to Its Formal Construction." ''Hispania'' 74.2 (1991): 262-268.
*Pritchett, Kay, ed.  "Antonio Colinas."  ''Four Post-Modern Poets of Spain: A Critical Introduction with Translations of the Poems''.  Fayetteville, AK: U of Arkansas P, 1991. 187-223.
*Puerto, José Luis. "Antonio Colinas: La poesía como itinerario de purificación." ''El viaje hacia el centro''. Madrid: Calambur, 1997.  41-70.
*Riaño, Peio H.  [http://www.publico.es/culturas/362263/tiritas-para-el-paraiso/ "Tiritas para el paraíso."] ''Público'' (online newspaper) February 20, 2011. Madrid.
*Rodríguez Marcos, Javier. [http://cultura.elpais.com/cultura/2014/05/11/actualidad/1399844588_579153.html "Antonio Colinas: 'Hemos cometido el error de reducir la poesía a lo intelectual'."] ''El País'' 12 May 2014.
*Rozas, Juan Manuel. "Mi visión del poema 'Sepulcro en Tarquinia'." ''Ínsula'' 508 (April 1989): 1-2.
*Santiago Bolaños, María Fernanda. "Antonio Colinas: Fuego que emerge de las ruinas del templo." ''El viaje hacia el centro''. Madrid: Calambur, 1997. 117-128.

==References==
{{reflist}}

==External links==
*[http://www.antoniocolinas.com/ Official Website of Antonio Colinas]
*[https://www.facebook.com/AmigosdeAntonioColinas Friends of Antonio Colinas, Facebook fan page]
{{Authority control}}

{{DEFAULTSORT:Colinas, Antonio}}
[[Category:1946 births]]
[[Category:Living people]]
[[Category:Spanish essayists]]
[[Category:20th-century Spanish poets]]
[[Category:People from La Bañeza]]
[[Category:Spanish translators]]
[[Category:Spanish journalists]]
[[Category:20th-century translators]]
[[Category:Spanish male poets]]
[[Category:Male essayists]]
[[Category:20th-century essayists]]