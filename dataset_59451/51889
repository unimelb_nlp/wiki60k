{{Infobox journal
| cover = [[File:Journal of Homosexuality.jpg]]
| discipline = [[Sexology]], [[queer studies]]
| abbreviation = J. Homosex.
| editor = John Elia
| publisher = [[Routledge]]
| country =
| history = 1976-present
| frequency = 12/year
| impact = 0.862
| impact-year = 2015
| website = http://www.tandfonline.com/action/aboutThisJournal?show=readership&journalCode=wjhm20
| link1 = http://www.tandfonline.com/toc/wjhm20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/wjhm20
| link2-name = Online archive
| ISSN = 0091-8369
| eISSN = 1540-3602
| CODEN = JOHOD7
| LCCN = 74-78295
| OCLC = 01790856
}}
The '''''Journal of Homosexuality''''' is a [[peer review|peer-reviewed]] [[academic journal]] covering research into [[human sexual activity|sexual practices]] and [[gender role]]s in their cultural, historical, interpersonal, and modern social contexts.

== History ==
The founding [[editor-in-chief]] was [[Charles Silverstein]]. After the first volume, the journal was edited by John De Cecco who stayed on for about 50 volumes. The current editor-in-chief is John Elia<!-- [[John Elia|John Elia]] redirects to another person, add wikilink when article on this person has been created --> ([[San Francisco State University]]). The journal was originally published by the [[Haworth Press]], until it was acquired by [[Taylor & Francis]], who now publish it under their [[Routledge]] [[imprint (trade name)|imprint]].

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]], [[MEDLINE]], [[Current Contents]], [[PsycINFO]], [[Sociological Abstracts]], Social Work Abstracts, [[Abstracts in Anthropology]], [[Criminal Justice Abstracts]], Studies on Women & Gender Abstracts, [[AgeLine]], and Education Research Abstracts. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.862.<ref>https://jcr.incites.thomsonreuters.com/JCRJournalProfileAction.action?pg=JRNLPROF&journalImpactFactor=0.862&year=2015&journalTitle=JOURNAL%20OF%20HOMOSEXUALITY&edition=SSCI&journal=J%20HOMOSEXUAL#{{cite book|title=Journal Citation Reports|date=2016|publisher=Thomson Reuters|accessdate=16 June 2016}}</ref>

== See also ==
{{Portal|LGBT}}
* [[List of academic journals in sexology]]

== References ==
{{Reflist}}

== Further reading ==
* {{cite journal |author=Joyce S, Schrader AM |title=Twenty years of the Journal of Homosexuality: a bibliometric examination of the first 24 volumes, 1974-1993 |journal=Journal of Homosexuality |volume=37 |issue=1 |pages=3–24 |year=1999 |pmid=10203067 |doi=10.1300/J082v37n01_02 |url=http://www.informaworld.com/smpp/content~db=all~content=a903895533~frm=titlelink}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/aboutThisJournal?show=readership&journalCode=wjhm20}}

{{Early U.S. gay rights movement}}

[[Category:Sexology journals]]
[[Category:Sexual orientation and science]]
[[Category:Sexual orientation and medicine]]
[[Category:LGBT-related journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Publications established in 1976]]
[[Category:English-language journals]]