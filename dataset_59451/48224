{{Infobox monastery
| name           = Convent of the Assumption on the Hill
| image          = Vologda - Gorny monastery.jpg
| alt            = 
| caption        = Convent at the end of the 19th century
| full           = 
| other_names    = Gorne-Uspensky Convent
| order          = Orthodox
| established    = 1590
| disestablished = 1918
| mother         = 
| dedication     = 
| diocese        = Vologda and Velikoustyuzhsky
| churches       = Assumption church
| founder        = Abbess Domnikia
| abbot          = 
| prior          = 
| people         = 
| location       = [[Vologda]], [[Russia]]
| map_type       = 
| coord          = {{coord|59|13|54|N|39|52|14|E|region:RU_type:landmark|display=inline,title}}
| oscoor         = 
| remains        = Assumption church, stone walls, St Sergius chapel, Alexius barbican church, abbey house, orphanage
| public_access  = 
| other_info     = 
}}

The '''Gorne-Uspensky Convent''' ({{lang-ru|''Горне-Успенский монастырь''}}) or simply '''Gorny Convent''' (in English: ''Convent of the Assumption on the Hill'') is a monastery in [[Vologda]], [[Russia]]. It was active between 1590 and 1924. It is situated in the historic part of the town, named Upper Posad, in the area bounded by the Zavrazhskaya, Burmaginy and Mokhov streets. Several of its buildings are preserved or have been restored, while others are partially or completely in ruins. It comprises complex of monuments that have been protected by the federal government.<ref>{{cite web|url=http://www.kulturnoe-nasledie.ru/monuments.php?id=3500015000|title=Горне-Успенский Монастырь|publisher=Памятники истории и культуры народов российской федерации : объекты культурного наследия|language=Russian|year=2008–2011|accessdate=November 24, 2012}}</ref>

==History==
The convent received its name from its raised location and from the city's [[Saint Sophia Cathedral, Vologda|Cathedral of the Assumption]].<ref name=cultinfo>{{cite web|title=Горний Успенский женский монастырь (осн. 1590) |url=http://www.cultinfo.ru/arts/architecture/vologda/000026/report.htm |publisher=Культура в Вологодской Области |language=Russian |deadurl=yes |archiveurl=https://web.archive.org/web/20140316135449/http://cultinfo.ru/arts/architecture/vologda/000026/report.htm |archivedate=2014-03-16 |df= }}</ref><ref name=lukom>{{cite book|title=Вологда в её старине|author=G.K. Lukomsky|language=Russian|location=St. Petersburg|year=1914}}</ref>

The convent was established in 1590 by the Abbess Domnikia during the reign of Czar [[Feodor I of Russia|Feodor I]] and under the aegis of the Archbishop of Vologda, Iona Dumin. Domnikia was the first Mother Superior of the convent. The date of the convent's founding is based on two surviving letters, one of them a petition from 1613 from an abbess of the convent to the Archbishop Sylvester.<ref name=cultinfo/>

The first stone building in the convent was a single-storeyed hospice, constructed in the second half of the 17th century. Between 1692 and 1699, the Assumption church was constructed with a [[bell-tower|belfry]], as well as the winter chapel of [[Sergius of Radonezh|St Sergius of Radonezh]]. From 1709-1714, the barbican church of Alexius was built. In the 1790s, a stone wall with four towers at the corners then was built to surround the convent.<ref name=cultinfo/>

In 1792, a fire destroyed all the wooden structures within the convent. Many documents were destroyed too, which is why there is insufficient information on the history of the convent before the 19th century.<ref name=lukom/>

In 1824, [[Alexander I of Russia|Czar Alexander I]] visited the convent. With his financial support, a two-storeyed structure was constructed.

In 1860, the [[Holy Synod of the Russian Orthodox Church]] decreed that the Nikolaevsky-Ozersky church (previously attached to the eponymous monastery) be assigned to the Assumption convent.<ref name=cultinfo/>

Between 1870-1890, an orphanage and a women's religious school were constructed. To the end of the 19th century, the convent remained a place of exile for women convicted of prostitution or adultery.<ref>{{cite book|author=Сазонов А. И.|title=Моя Вологда: Прогулки по старому городу|location=Вологда|publisher=Древности Севера|year=2006|language=Russian}}</ref>

In 1880, the bell tower was rebuilt.<ref name="cultinfo" />

In 1888, the bishop Theodosius converted a shelter in the convent to a diocesan school for girls, which in 1903 moved to new outside premises.<ref name="ve3" /><ref name="ocherki-104" />

In 1918, the convent was closed, but several nuns remained till 1923-1924, when it was commandeered by the [[Red Army]]. The Assumption church remained in use till 1924. After their eviction, some of the nuns moved into homes next to the convent. In the [[Soviet period]], the monastery housed a transit prison and a cantonment.<ref name=malygin/><ref>{{cite book|publisher=Древности севера|location=Vologda|year=2004|title=Вологда в минувшем тысячелетии. Очерки истории города|language=Russian|url=http://www.booksite.ru/ancient/history/min/uvs/hee/index.htm|page=161}}</ref>

1995 saw the restoration of the Assumption church, and services began the following year. The abbot's residence and the convent's sanctuary now house shops and warehouses.<ref name="eparch">{{cite web|url=http://vologda-eparhia.ru/temple-of-Assumption-of-the-theotokos.html|title= Храм в честь Успения Божией Матери|publisher= Вологодская епархия|accessdate=November 23, 2012}}</ref>

==Church of the Assumption==
[[File:Vologda - Gorny monastery - Dormition church.jpg|thumb|left|Assumption cathedral, view from the side of the belltower and the chapel of St Sergiius (2009)]]
[[File:Vologda - Gorny monastery - Dormition church (other view).jpg|thumb|left|Apse of the Assumption cathedral and St Sergius chapel (2009)]]
The '''Church of the Assumption''' which gave its name to the convent was constructed and consecrated significantly earlier than the convent. The first church was wooden, one of the oldest buildings in Vologda. In 1303, during the [[Assumption of Mary|feast of the Assumption]], Theoctist, bishop of Novgorod, consecrated a Church of Our Lady in Vologda.<ref>{{cite journal|url=http://www.booksite.ru/ancient/historic/locality_29.htm|title=Горнее место|author= Александр Мухин|journal=Вологодские новости|date=March 28 – April 3, 2007|number=12|page=23|accessdate=November 23, 2012|language=Russian}}</ref> Evidently, in 1499, this ancient church burned to the ground, with its icons and manufactures, its books and indeed the entire monastery.<ref>{{cite journal|journal=[[Complete Collection of Russian Chronicles]]|title=Vologda & Perm Chronicle|volume=26|page=292}}</ref> The reference to ''monastery'' here likely means some chambers that existed at the church - although, these were not officially monastic. In the place of the burned building, another wooden church was constructed. This, in turn, was replaced by a stone edifice, which then became the monastery's church.

The stone church of the Assumption with a bell tower was unheated, so a warm winter chapel of [[Sergius of Radonezh|St Sergius of Radonezh]] was built between 1692-99 in place of two older wooden structures.<ref name=lukom/><ref name=hp81-83/> In 1691, the foundations were laid, and the next year saw the start of the construction of the buildings, which finished in 1694. On May 23, 1695, Archbishop Gabriel of Vologda and Belozero consecrated the church.<ref name="n214" />

The crosses atop the church [[cupola]]s were gilded. It is known that the church was called ''Golden cross''. The neighbouring church of [[Saint Nicholas|Nicholas the Miracle-Worker]] on the Hill was also called the Golden cross, but it is evident from archive documents of the first half of the 18th century that the Nicholas church was referred to as '' '''by''' the Golden cross'' - in other words, it was located not far from the Assumption church. In 1761, a fire badly damaged the body and roof of the Assumption church, and it lost its gilt. As a result, the name ''Golden cross'' was transferred to the Nicholas church.<ref name=cultinfo-sobor/>

From the outside, the Assumption church resembles an ordinary shrine rather than a monastic cathedral. Its architecture is based on 16th century traditional styles. The main part of the building has a cubical form, crowned with one illuminated and four decorative drums decorated with arcature and bearing large onion cupolas. The decor of the facades is simple: plain cornices and mouldings and window-jambs on rollers.<ref name="hp81-83" />

In 1880, the chapel, refectory and bell-tower were rebuilt in a pseudo-Russian style.<ref name="hp81-83">{{cite book|title=Художественные памятники XII—XIX веков|language=Russian|author1=Г. Бочаров|author2=В. Выголов|publisher=Искусство|location=Moscow|year=1979|pages=81–83|url=http://www.booksite.ru/fulltext/vyg/olov/4.htm}}</ref>

In mid-July 1924, the cathedral was taken over by the signal corps of the Ensky division. On July 26, the first moving picture presentation for the Red Army was held here.<ref name=malygin>{{cite news|newspaper=Красный Север|location=Vologda|author=N. Malygin|date=August 1, 1924|format=PDF|url=http://www.booksite.ru/krassever/1924/1924_176.pdf|page=3|title=Использовали б. церковь под кино-театр|language=Russian}}</ref>

The Assumption church along with the chapel of St Sergius of Radonezh are protected architectural buildings.<ref>{{cite web|url=http://www.kulturnoe-nasledie.ru/monuments.php?id=3500015001|accessdate=November 23, 2012|title=Успенская церковь с приделом Сергия Радонежского |publisher=Памятники истории и культуры народов российской федерации : объекты культурного наследия|language=Russian|year=2008–2011}}</ref> Currently, it is restored and in active use.<ref name=cultinfo-sobor/> It constitutes a single parish along with the church of Saints [[Constantine the Great|Constantine]] and [[Helena (Empress)|Helena]].<ref name=eparch/>

===Icons of the Assumption Church===
At the time of its closing, the Assumption church had a carved five-tier [[iconostasis]] in gilt.

There are two prominent icons in this church - the ''Assumption of the Holy Mother'', and ''Resurrection-Descent into Hell'', both from the 16th century and of local origin.<ref>{{cite encyclopedia|encyclopedia=Иконы Вологды XIV—XVI веков|publisher=Северный паломник|year=2007|isbn=978-5-94431-232-7|pages=421–431|title=Успение Богоматери|editor1=Екатерина Гладышева |editor2=Левон Нерсесян |editor3=Александр Преображенский |author=Е. А. Виноградова}}</ref><ref>{{cite encyclopedia|encyclopedia=Иконы Вологды XIV—XVI веков|publisher=Северный паломник|year=2007|isbn=978-5-94431-232-7|pages=245–251|title=Воскресение — Сошествие во ад|editor1=Екатерина Гладышева |editor2=Левон Нерсесян |editor3=Александр Преображенский |author=Е. А. Преображенский}}</ref> After the closure of the church, the icons were transferred to the Vologda State Historical-Architectural and Art Museum.

The icon of the Assumption was transferred to the Assumption cathedral from the prior wooden Assumption church, which had been constructed in place of the church that had burned down in 1499.

The collection of the Vologda State museum also contains early-18th century frames depicting the life of the Virgin in the style of Ivan Grigoryev-Markov's studio. This was renewed thrice and twice decorated with casings (first with an indigo-based dye and then with silver emboss), and restored in 1977 by N.I. Fedyshin.

It is also known that the Assumption cathedral contained old icons of the Saviour and of St Sergius of Radonezh.<ref name=lukom/>
<gallery class="center"  widths="144px" heights="184px">
File:Assumption, Gorny Convent.JPG|<center>'''Assumption'''. First quarter of the 16th century. Vologda State Historical-Architectural and Art Museum</center>
File:Assumption, Gorny Convent, fragment.jpg|<center>'''Assumption, fragment: Jesus'''. First quarter of the 16th century. Vologda State Historical-Architectural and Art Museum</center>
File:Ascension-Descent into Hell, Gorny Convent.jpg|<center>'''Resurrection - Descent into Hell'''. Second quarter of the 16th century. Vologda State Historical-Architectural and Art Museum</center>
File:Ascension-Descent into Hell, Gorny Convent, fragment.jpg|<center>'''Resurrection - Descent into Hell, fragment: angels'''. Second quarter of the 16th century. Vologda State Historical-Architectural and Art Museum</center>
</gallery>

===The Chapel Of St. Sergius Of Radonezh===
[[File:Vologda - Gorny monastery - Dormition church (1914).jpg|thumb|Assumption cathedral with the St Sergius chapel (1914). Behind the branches is the ruined chapel dome.]]
The warm chapel of St. Sergius of Radonezh is located on the northern side of the Assumption cathedral. Its construction began in 1692 and finished between 1697-98. A journeyman builder named Vasily Karpov (nicknamed Vargan) with his son Dmitri and workers from the [[Spaso-Prilutsky Monastery]] of Kornovich village completed the chapel.<ref name="hp81-83" /><ref name="cultinfo-serg">{{cite web|url=http://www.cultinfo.ru/arts/architecture/vologda/000073/report.htm |title=Церковь Сергия Радонежского (тёплая) (1692—1698) |accessdate=November 24, 2012 |publisher=Культура в Вологодской Области |language=Russian |deadurl=yes |archiveurl=https://web.archive.org/web/20140316135641/http://cultinfo.ru/arts/architecture/vologda/000073/report.htm |archivedate=March 16, 2014 |df= }}</ref>

The chapel is single-storeyed, single-domed and has three altars. It has a refectory hall to the right of which is a shrine to [[Alexander Nevsky]] and the [[iconographer]] St Joseph, which was consecrated in 1712. On the left side of the refectory is a chapel of [[St Nicholas|St Nicholas of Myra]] (who is mentioned in monastic records of 1634), consecrated in 1714.<ref name="cultinfo" /><ref name="cultinfo-serg" />

===Bell Tower===
The convent's monastery was built at the end of the 17th century at the same time as the [[cathedral]]. It is tent-like in shape and attached to the cathedral.<ref name=cultinfo/>

In 1880, there was damage to the bell tower, and it was rebuilt. It is located on the western side at the entrance to the cathedral. It is a two-tiered structure that tapers to a tent-shaped top. The tower is about 36 metres tall. The belfry has ten bells, the heaviest weighing about 4 tons. On the second tier of the tower, a sacristy was constructed.<ref name=cultinfo-sobor/><ref name=suvorov>{{cite book|title=Описание Вологодского Горнего Успенского женского монастыря|author=Н. И. Суворов|location= Vologda province|year=1885|language=Russian}}</ref>

==The Alexius Barbican Church==
[[File:Vologda - Church of Alexius.jpg|thumb|left|Barbican church of Alexius (2008)]]
[[File:Vologda - Gorny monastery - Church of Alexius (1914).jpg|thumb|Church of Alexius (1914)]]
The Alexius barbican church, or the church of [[Alexius of Rome]] was built between 1709-1714, and consecrated in 1720. For its completion, the abbess and nuns had to go to Moscow and other towns to beg donations from the believers.

The barbican church was built of stone: unheated, single-storeyed, single-domed. Beneath it were the holy gates with two bays, one large and one small. At first, it was intended to sanctify two seats within the church: one in the name of St Nicholas, and the other for Alexius. The ground floor had no external decorations and was subsumed in the wall surrounding the convent. The upper level was occupied by the church. Its four windows lent it a residential appearance. The facade of the upper level was richly decorated with half-columns and architraves with [[Baroque|Russian baroque]] gables, typically in the style of the early 18th century. The church's dome was made in the classical style.

Within the church was an ancient image of the [[Image of Edessa|Mandylion]], featuring many saints. A two-tiered iconostasis, constructed at the same time as the church, is a protected historical treasure.<ref>{{cite web|url=http://www.kulturnoe-nasledie.ru/monuments.php?id=3500015002|title=Алексеевская надвратная церковь|publisher=Памятники истории и культуры народов российской федерации : объекты культурного наследия|language=Russian|year=2008–2011|accessdate=November 24, 2012}}</ref>

These days the church is in a dilapidated state.<ref name="hp81-83" /><ref name="cultinfo-alex">{{cite web|url=http://www.cultinfo.ru/arts/architecture/vologda/000003/report.htm |title=Алексия человека Божьего надвратная церковь (1709—1714) |accessdate=November 24, 2012 |publisher=Культура в Вологодской Области |language=Russian |deadurl=yes |archiveurl=https://web.archive.org/web/20110612014205/http://www.cultinfo.ru/arts/architecture/vologda/000003/report.htm |archivedate=June 12, 2011 |df= }}</ref>

==The Abbey House==
[[File:Vologda - Gorny monastery - Hegumen's house.jpg|thumb|left|Modern view of the abbey house]]
The two-storeyed Abbey House was built between 1826-1828 with the financial support of Czar Alexander I, after his visit to the convent in October 1824. The Mother Superior's residence is a protected monument (currently uncategorized).<ref>{{cite web|url=http://www.kulturnoe-nasledie.ru/monuments.php?id=3500000898|title=Дом игуменьи|publisher=Памятники истории и культуры народов российской федерации : объекты культурного наследия|language=Russian|year=2008–2011|accessdate=November 24, 2012}}</ref>

==Orphanage and School==
[[File:Vologda - Gorny monastery - Shelter (1899).jpg|thumb|Women's school and abbey house. On the right is the stone wall. (1899)]]
[[File:Vologda - Gorny monastery - Shelter.jpg|thumb|left|Modern appearance of the Vologda Parochial's Women's School]]
The idea for the school came from the Reverend Christopher on November 7, 1862. The orphanage came into being in 1869, when the abbess Sevastiana took in 10 girls, mostly orphans of the clergy, with the blessings of the Metropolitan Palladius.<ref name="sergiya1899">{{cite book|author=А. К. Лебедев|title=Успенский женский монастырь в г. Вологде и приписная Николаевская Озерская пустынь|publisher= издательство игуменьи Сергии|location= Вологда|year=1899|language=Russian}}</ref>

At first, the orphanage was part of the abbey house, but soon thereafter it was decided to construct a separate two-storeyed building for it. Its foundations were laid on St Sergius of Radonezh's festival day, September 25, 1870. Construction began the following year and finished in 1873.<ref name=cultinfo/><ref name=sergiya1899/><ref name=n358/>

In the early 1880s, as the number of orphans increased, it was decided to merge the abbey house and the orphanage. A new three-storeyed wing was constructed to accommodate the population that was growing at forty-five children every year. The bishop Theodosius funded the extension with ten thousand rubles; later, he provided another 9,500 rubles for its improvement.<ref name=sergiya1899/>

The full course of education at the orphanage was six years long: three classes, each over two years. The orphans were taught reading, writing, the holy law, the explanation of worship and service, arithmetic, sacred and secular history, Russian language, singing, crafts and geography. Between 1873 and 1888, one hundred and six girls graduated in seven cohorts.<ref name=sergiya1899/>

In 1888, bishop Theodosius converted the orphanage to a diocesan school for girls. For its extension, Theodosius contributed twenty thousand rubles, and invested another ten thousand, the proceeds of which paid for the maintenance of the school.<ref name="ve3">{{cite web|url=http://vologda-eparhia.ru/about/history/xviii-xx-veka|title=История Вологодской епархии (XVIII—XX века)|publisher=Вологодская епархия РПЦ|accessdate=November 24, 2012|language=Russian}}</ref> Graduates of the school received certificates that allowed them to become governesses or teachers, and half of them taught in parochial schools. Between 1902-1903, the school moved to new premises on the Zlatoustinsky embankment, and in its place a new orphanage was set up.<ref name="ocherki-104">{{cite book|publisher=Древности севера|location=Vologda|year=2004|title=Вологда в минувшем тысячелетии. Очерки истории города|language=Russian|url=http://www.booksite.ru/ancient/history/min/uvs/hee/index.htm|page=104}}</ref><ref name="nepin">{{cite book|url=http://www.booksite.ru/fulltext/nep/ein/vol/ogda/index.htm|accessdate=November 24, 2012|title=Вологда прежде и теперь|author= С. Непеин|location=Вологда|year= 1906|language=Russian}}</ref><ref name="obiteli">{{cite book|title=Православные русские обители : Полное иллюстрированное описание православных русских монастырей в Российской Империи и на Афоне|author=П. П. Сойкин|language=Russian|location= Санкт-Петербург|year=1910|isbn=5-88335-001-1|pages=99–100|url=http://ru.wikisource.org/wiki/%D0%9F%D1%80%D0%B0%D0%B2%D0%BE%D1%81%D0%BB%D0%B0%D0%B2%D0%BD%D1%8B%D0%B5_%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B5_%D0%BE%D0%B1%D0%B8%D1%82%D0%B5%D0%BB%D0%B8/%D0%93%D0%BE%D1%80%D0%BD%D0%B5-%D0%A3%D1%81%D0%BF%D0%B5%D0%BD%D1%81%D0%BA%D0%B8%D0%B9_%D0%92%D0%BE%D0%BB%D0%BE%D0%B3%D0%BE%D0%B4%D1%81%D0%BA%D0%B8%D0%B9_%D0%BC%D0%BE%D0%BD%D0%B0%D1%81%D1%82%D1%8B%D1%80%D1%8C/%D0%94%D0%9E}}</ref>

The orphanage is a protected architectural monument (currently uncategorized).<ref>{{cite web|url=http://www.kulturnoe-nasledie.ru/monuments.php?id=3500000899|title=Дом воспитанниц приюта|publisher=Памятники истории и культуры народов российской федерации : объекты культурного наследия|language=Russian|year=2008–2011|accessdate=November 24, 2012}}</ref>

==Hospice==
The Hospice is located northeast of the orphanage. It was constructed with part of the funds for the Bishop's house, under the aegis of Bishop Simon, who governed between 1664-1684. It was designed to provide shelter and treatment for twelve elderly and sickly women. The hospice was the first stone construction within the convent. In 1870, a second wooden floor was added to it, along with a wooden roof. From 1871, meals and the reading of the [[Psalms]] were held within it.<ref name="cultinfo" /><ref name="suvorov" />

==Convent Walls==
[[File:Vologda - Gorny monastery - Gates.jpg|thumb|Holy Gate. Photo from the early 20th century. Destroyed in the Soviet period]]

To the end of the 1790s, the convent was surrounded by a wooden wall. In the 1890s, a new stone wall was built, complete with turrets at its four corners, and in circumference, considerably larger than the old wooden walls. It was about 3 metres tall, 780 metres long. There were two gates in the walls. The eastern, Sacred gates comprised two wings, decorated with round windows. The southern gate was a more modest affair for pedestrians.<ref name="cultinfo" /><ref name="suvorov" /><ref name="sergiya1899"/><ref name="vol_star">{{cite book|author=И. К. Степановский|title=Вологодская старина: историко-археологический сборник|url=https://books.google.com/books?id=_xELAAAAIAAJ|accessdate=24 November 2012|year=1890|publisher=Типография Вологодского Губернского Правления|location=Вологда}}</ref>

==Other Buildings==
The convent consists of several other buildings:
* A two-storeyed wooden house with a wooden roof. This housed the nuns and was located adjacent to the hospice. It was built in 1869.
* A single-storeyed wooden house with a wooden roof. This housed the nuns who were ill, and was located to the south of the abbey house. It was built in 1875 by N. N. Rataeva.
* Fifteen wooden cells, in which dwelt [[Degrees of Eastern Orthodox monasticism|rassophores]] and [[novitiate]]s.
* Two barns.
* Two cellars.
* A bath-house and laundry.
* A wooden mews with stables.

Besides these, the convent owned several other buildings outside its walls.
* A wooden cottage on a stone foundation with adjoining lands, purchased by the convent in 1875.
* A wooden one-storeyed house, donated to the convent by S. A. Zasetskaya, widow of a [[Table of Ranks|titular counsellor]].<ref name=suvorov/>

==Monastic Properties==
The convent owned twelve thatched cottages in the Vologda and Gryazovets counties:
* Krestovka;
* Novokupka;
* Bolshaya Chetvert;
* Malaya Chetvert;
* Berezovka;
* Zaberezhitsa;
* Popadeyka;
* Lykusha;
* Cherepaniha;
* Chishenye;
* Besovskoye;
* Galino Chishenye.

The convent also owned 600 square [[Obsolete Russian units of measurement|sazhens]] of land in Gryazovets, donated in 1877 by the widow of Captain E. P. Ivanov, and land at the Nikolaevsky-Ozersky church.<ref name=suvorov/>

==Nikolaevsky-Ozersky Church==
In 1860, the [[Holy Synod]] decreed that in order to extend the resources of the convent, the Nikolaevsky-Ozersky church (which originally belonged to the eponymous monastery) be assigned to it. 73 [[dessiatin]]s and 2041 square sazhens, as well as 4450 rubles donated by a former priest Alexander Sokolov, came to the convent along with the church.<ref name=cultinfo/><ref name=suvorov/>

==Abbesses of the Convent==
The list includes time periods for which there are sources indicating the reign of one or another Mother Superior, with the gaps possibly filled by the same abbesses. There are several sources for this list.
<ref name=cultinfo/><ref name="lukom"/>
<ref name=n214>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#241|title=№ 214. Из "Вологодской летописи" — о постройке новой каменной церкви Успения Богородицы в Вологодском Успенском Горнем девичьем монастыре. 1691—1695 гг.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=cultinfo-sobor>{{cite web|title=Собор Успения Богородицы (1692—1700) |url=http://www.cultinfo.ru/arts/architecture/vologda/000074/report.htm |publisher=Культура в Вологодской Области |language=Russian |deadurl=yes |archiveurl=https://web.archive.org/web/20071208093517/http://www.cultinfo.ru/arts/architecture/vologda/000074/report.htm |archivedate=2007-12-08 |df= }}</ref>
<ref name=n358>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#404|title=№ 358. Из правил учебно-воспитательного приюта при Горнем Успенском женском монастыре в г. Вологде. 31 января 1874 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name="suvorov-igumen">{{cite book|title=Описание Вологодского Горнего Успенского женского монастыря|author= Н. И. Суворов|location= Vologda province|year= 1885|language=Russian|pages=17–21}}</ref>
<ref name=n83>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#97|title=№ 83. Из наказной памяти архиепископа Гавриила игуменье Вологодского Успенского Горнего девичьего монастыря Екатерине о пострижении в монастырь ссыльных и условиях их содержания. 3 января 1702 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=n136>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#157|title=№ 136. Закладная кабала вологжанина Ильи Бобошина игуменье Вологодского Успенского Горнего девичьего монастыря Наталье на двор и огород. 6 июля 1664 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name="n210">{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#237|title=№ 210. Челобитная стариц Вологодского Успенского Горнего девичьего монастыря архиепископу Сильвестру о возвращении в монастырь первой игуменьи и строительницы Домникеи, 4 мая 1613 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=n211>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#238|title=№ 211. Жалованная грамота царя Михаила Федоровича Вологодскому Успенскому Горнему девичьему монастырю на выдачу из казны денежной и хлебной руги, воска, ладана и церковного вина. 29 января 1624 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=n213>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#240|title=№ 213. Челобитная игуменьи Вологодского Успенского Горнего девичьего монастыря Екатерины с сестрами архиепископу Гавриилу с просьбой не назначать в монастырь дьякона. 1690 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=n215>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#242|title=№ 215. Из указной памяти Приказа Церковных дел о разрешении игуменье Вологодского Успенского Горнего девичьего монастыря Афанасии собирать пожертвования на церковное строение. Не ранее 1714 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=n387>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#435|title=№ 387. Донесение игуменьи Вологодского Горнего Успенского женского монастыря Раисы епископу Вологодскому и Устюжскому Евлампию о занятиях монахинь ремёслами. 10 марта 1848 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
<ref name=n401>{{cite encyclopedia|url=http://www.booksite.ru/fulltext/sta/raya/vol/ogda/9.htm#449|title=№ 401. Из ведомости о Вологодском Горнем Успенском женском монастыре за 1904 г. 25 января 1905 г.|encyclopedia=Старая Вологда: XII - начало XX века: сборник документов и материалов|editor=Ф. Я. Коновалов|publisher=Государственный архив Вологодской области|language=Russian}}</ref>
* From 1590: Domnikia.<ref name="cultinfo" /><ref name="suvorov-igumen"/>
* To 1613: two abbesses: Eulampia and Govdela.<ref name="suvorov-igumen" /><ref name="n210"/>
* 1613: Anisia.<ref name="suvorov-igumen" /><ref name="n210" />
* 1613 - 1623: Domnikia again.<ref name="suvorov-igumen" /><ref name="n210" />
* 1623 - 1624: Maryemyana.<ref name="suvorov-igumen" /><ref name="n211"/>
* 1639 - 1644: Anna Levasheva.<ref name="suvorov-igumen" />
* 1649 - 1658: Vera<ref name="cultinfo-sobor"/>
* 1660 - 1673: Natalia<ref name="suvorov-igumen" /><ref name="n136"/>
* 1677 - 1685: Catherine<ref name="suvorov-igumen" />
* 1686 - 1688: Anisia Zueva<ref name="suvorov-igumen" />
* In 1689: Agripina Berdyaeva - on transfer from the Pskov Elias Monastery<ref name="suvorov-igumen" />
* 1690-1709: Catherine Luninskih,<ref name="suvorov-igumen" /><ref name="n83"/><ref name="n213"/><ref name="n401" /> possibly in place of Eugenia in 1714<ref name="n214"/>
* 1710-1713: Eugenia<ref name="suvorov-igumen" />
* In 1714: Athanasia<ref name="suvorov-igumen" /><ref name="n215"/>
* 1721 -1722: Dosifea<ref name="suvorov-igumen" />
* 1738 -1741: Arcadia Skvortsova<ref name="suvorov-igumen" />
* 1742 -1746: Eudokia<ref name="suvorov-igumen" />
* 1746 -1770: Arcadia<ref name="suvorov-igumen" />
* 1770 - 1784: Araksana<ref name="suvorov-igumen" />
* 1784 - 1786: Iroda<ref name="suvorov-igumen" />
* 1787 - 1795: Anastasia<ref name="suvorov-igumen" />
* 1795 - 1805: Dorothea<ref name="suvorov-igumen" />
* 1805 - 1814: Theoctista<ref name="suvorov-igumen" />
* 1814 - 1818: Eustolia<ref name="suvorov-igumen" />
* 1818 - 1822: Mitropolia<ref name="suvorov-igumen" />
* July 5, 1822 - 1839: Seraphima<ref name="suvorov-igumen" /><ref name=n401/>
* January 21, 1840 - 1844: Methodia<ref name="suvorov-igumen" />
* August 13, 1844 - 1847: Euphrasia<ref name="suvorov-igumen" />
* September 25, 1847 - December 17, 1848: Raisa<ref name="suvorov-igumen" /><ref name=n387/>
* February 2, 1848 - February 2, 1850: Solomia<ref name="suvorov-igumen" />
* May 27, 1850 - November 22, 1856: Raisa again<ref name="suvorov-igumen" />
* November 22, 1856 - June 7, 1857: Amphilogia<ref name="suvorov-igumen" />
* June 7, 1857 - November 1, 1866: Smaragda<ref name="suvorov-igumen" />
* October 31, 1866 - January 21, 1871: Sebastiana<ref name=n358/>
* January 21, 1871 - June 4, 1871: Vitalia<ref name="suvorov-igumen" />
* June 4, 1871 - December 27, 1893: Arsenia (Varvara Alexandrovna Yaryshkina)<ref name="sergiya1899" /><ref name="n358"/><ref name="suvorov-igumen" /><ref name="n401" />
* February 20, 1894 - 1904. Sergia.<ref name="sergiya1899" /><ref name="n401"/>

==References==
{{reflist|3}}
<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Russian Orthodox monasteries]]
[[Category:Monasteries in Russia]]
[[Category:Christian monasteries established in the 16th century]]
[[Category:Christian monasteries in Russia]]