{{Infobox airport
| name         = Haslemoen Airport
| nativename   = 
| image        =
| image-width  =
| caption      =
| IATA         =
| ICAO         =
| type         = Private
| owner        = Våler Municipality
| operator     = Solungen Mikroflyklubb
| city-served  = [[Våler, Hedmark|Våler]], [[Norway]]
| location     = [[Haslemoen]], Våler
| metric-elev  = y
| elevation-m  = 175
| elevation-f  = 575
| coordinates  = {{coord|60.6543|N|011.9105|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_label_position =
| pushpin_label          = Haslemoen
| pushpin_map_alt        =
| pushpin_mapsize        = 300
| pushpin_image          =
| pushpin_map_caption    = Location in Norway
| website      = 
| metric-rwy   = y
| r1-number    = <s>17/35</s>
| r1-length-f  = <s>5,910</s>
| r1-length-m  = <s>1,800</s>
| r1-surface   = <s>Concrete</s>
| r2-number    = 16/34
| r2-length-f  =
| r2-length-m  = 500
| r2-surface   = Grass
}}

'''Haslemoen Airport''' ({{lang-no|Haslemoen flyplass}}) is an [[airport]] situated at [[Haslemoen]] in [[Våler, Hedmark|Våler]], [[Norway]]. Since 2003 the airport only features a {{convert|500|m|sp=us|adj=on}} grass runway used by [[ultralight aviation|ultralight aircraft]]. This is located next to a closed {{convert|1800|m|sp=us|adj=on}} formerly military runway.

The airport was planned by the [[Norwegian Army Air Service]] as a main air station during the late 1930s, but the plans were stopped by the break-out ofh the [[Second World War]]. Haslemoen was built as a labor camp in 1943 and then taken over by [[Luftwaffe]], who built the airport. Opening in August 1944, its main purpose was to station a squadron of [[Focke-Wulf Fw 200 Condor]]s. Haslemoen was rebuilt and opened as the base of the [[Artillery Battalion (Norway)|Artillery Battalion]] in 1955. Operation of the airport resumed in 1960 with the delivery of [[Cessna O-1 Bird Dog]] observation aircraft. They remained in service with the artillery until 1992. The grass ultralight runway was established in 1990 and was sold to Våler Municipality in 2007.

==History==
The interest to establish an airfield and military base at Haslemoen was first articulated by the Norwegian Army Air Service in the late 1930s. Their two main air stations in Eastern Norway, [[Kjeller Airport|Kjeller]] and [[Gardermoen Air Station|Gardermoen]], were both too small. They therefore proposed that the relatively flat, forested areas at Haslemoen would be a suitable site for a main air station. Grants to expropriate {{convert|5.7|km2|sp=us}} was granted by [[Parliament of Norway|Parliament]] in February 1940. Clearing of {{convert|2.0|km2|sp=us}} was completed by April. A planned meeting om 9 April concerning further construction was, however, interrupted by the [[Operation Weserübung|German invasion]].<ref name=g121>Gamst: 121</ref>

Haslemoen was built in 1943 as a camp for conscripted labor services, serving in the Våler area.<ref name=g121 /> Haslemoen served in this capacity until the end of the year, when it was taken over by Luftwaffe. They were concerned about an attack on Norway and wanted to establish a reserve airport for Gardermoen, located far from the coast. Construction began in late 1943. Originally the airport was proposed to also have a [[water airport]] on the lake [[Gjesåssjøen]], using water during summer and the ice and runway during winter.<ref name=g122>Gamst: 122</ref>

The airfield was taken into use in late 1944, although minor works continued until the end of the war in May 1945. Both a runway, measuring {{convert|1800|by|60|m|sp=us}}, and a taxiway were built, along with auxiliary buildings, barracks, hangars, storehouses and workshops.<ref name=g122 /> The concrete for the runway was of poor quality and caused problems with [[frost heaving]].<ref name=g123>Gamst: 123</ref>

The airport saw little during the remainder of the Second World War, after its opening in August 1944.<ref name="Hafsten: 316">Hafsten: 316</ref> From 21 November it served as a base for a squadron of the remaining Focke-Wulf Fw 200 Condors in Norway, reaching 28 aircraft by 28 December. Thereafter their numbers dwindled as they were transferred to the Continent. They squadron was disbanded on 7 March 1945, by which time only two Condors remained in Norway.<ref>Hafsten: 307</ref> Haslemoen was also home to a [[target tug]] squadron.<ref name="Hafsten: 316"/>

The aerodrome was taken over by the Royal Norwegian Air Force after [[end of World War II in Europe|the war ended]] on 8 May 1945. Initially Haslemoen was used as a transit camp for German soldiers before being repatriated.<ref name=g123 /> RNoAF allocated Haslemoen to a light bomber squadron, which was to be operated on a repetition- and mobilization basis.<ref>Arheim: 48</ref> Soon the air force decided to re-prioritize its fleet procurement,<ref>Arheim: 49</ref> and by 1948 it was clear that Haslemoen would not be used as an air station.<ref>Arheim: 50</ref>

Haslemoen Base was, however, rebuilt for the Artillery Battalion and opened on 24 April 1955.<ref name=g123 /> The Air Force took delivery of the Cessna O-1 Bird Dog in 1960, and those allocated to Southern Norway were stationed at Haslemoen Airport. These were used for aerial observations to support the artillery. They remained in service until 1992.<ref name=a175>Arheim: 175</ref>

A government commission considered Haslemoen in 1988 as a potential main air station for Eastern Norway. Although there was ample space available and the area relatively flat, the airport was cut early in the process. The main concern was that its proximity to Sweden meant that operations would cause problems for the self-imposed no-fly zone for allied aircraft. The runway needed to be shifted {{convert|150|m|sp=us}} east, meaning that little of the existing infrastructure could be used.<ref>Ministry of Defence: 32</ref>

Solungen Mikroflyklubb was established in 1990. In cooperation with the military it built a parallel, {{convert|500|m|sp=us|adj=on}} grass runway to the east of the concrete runway.<ref>{{cite news |title=Stevne på flystripa |last=Mellem |first=Kenneth |work=[[Glåmdalen (newspaper)|Glåmdalen]] |date=3 September 2013 |pages=6–7 |language=Norwegian}}</ref> A second proposal for Haslemoen Airport came in 1992, during the retirement of the Bird Dogs. The Air Force proposed replacing the observation services with helicopters. While the Army preferred basing them with the artillery at Haslemoen, the Air Force preferred basing them at either [[Rygge Air Station]] or [[Bardufoss Air Station]]. In the end no replacement aircraft were bought for the Bird Dogs.<ref name=a175 />

The artillery was moved to [[Rena Base]] in 2003 and Haslemoen subsequently closed. The {{convert|18|km2|sp=us|adj=on|0}} property was bought by Våler Municipality for 46 million [[Norwegian krone]] in 2007. The military abandonment saw the airport facilities being used for motor sports. The pinnacle was hosting [[2007 Rally Norway]].<ref>{{cite news |title=Full fart på Haslemoen |last=Hope |first=Vidar |work=[[Forsvarets Forum]] |date=29 May 2007 |url=http://www.fofo.no/Full+fart+p%C3%A5+Haslemoen.b7C_wZvM2N.ips |language=Norwegian |archiveurl=https://web.archive.org/web/20061115095910/http://fofo.no/Full+fart+p%C3%A5+Haslemoen.b7C_wZvM2N.ips |archivedate=15 November 2006 |accessdate=25 January 2015 |deadurl=no}}</ref> The municipality changed the focus away from motor sports and instead initiated a program to cultivate {{convert|6|km2|sp=us}} of the former base, including the airport area.<ref>{{cite news |title=Krever at alle krigsminner blir fredet |url=http://www.nrk.no/ho/krigsminner-midt-i-matfatet-1.11798292 |last=Myhre |first=Torunn |last2=Stensrud |first2=Vivian |date=26 June 2014 |language=Norwegian |publisher=[[Norwegian Broadcasting Corporation]]}}</ref>

==Facilities==
The airport consisted of a concrete runway which measured {{convert|1800|by|60|m|sp=us}}. It also had a series of taxiways and aircraft shelters for a squadron of aircraft. This consisted both of taxiways to the shelters and a parallel taxiway located west of the runway. The [[control tower]] was situated between the runway and parallel taxiway. The workshop and communications center were located close to the garrison, west of the aerodrome.<ref name=g122 />

The current runway consists of a grass cover measuring {{convert|500|by|15|m|sp=us}}. It is solely used by ultralight aircraft and is operated by Solungen Mikroflyklubb.<ref>{{cite web |url=http://generator.firmanett.no/%28or3q2xamejvziqz4vdaucf55%29/generator.aspx?PID=176985&M=0 |title=Haslemoen |publisher=Norske Flyplasser |accessdate=25 January 2015 |language=Norwegian}}</ref>

==References==
{{reflist|30em}}

==Bibliography==
* {{cite book |last=Arheim |first=Tom |last2=Hafsten |first2=Bjørn |last3=Olsen |first3=Bjørn |last4=Thuve |first4=Sverre |title=Fra Spitfire til F-16: Luftforsvaret 50 år 1944–1994 |url=http://www.nb.no/nbsok/nb/2bb06c47357b69c0081d12c7bb647284 |location=Oslo |publisher=Sem & Stenersen |year=1994 |language=Norwegian |isbn=82-7046-068-0}}
* {{cite book |last=Gamst |first=Thorbein |title=Befalsskolen for Feltartilleriet 1931–1996 |publisher=Artilleriregimentet og Artilleriets offisersforening |location=Oslo |year=1998 |language=Norwegian |isbn=8299465206  |url=http://www.nb.no/nbsok/nb/9411f3b67d33b0000c168e62b64e208c}}
* {{cite book |last=Hafsten |first=Bjørn |last2=Larsstuvold |first2=Ulf |last3=Olsen |first3=Bjørn |last4=Stenersen |first4=Sten |title=Flyalarm: Luftkrigen over Norge 1939–1945 |year=1991 |publisher=Sem & Stenersen |location=Oslo |language=Norwegian |isbn=82-7046-058-3 |url=http://www.nb.no/nbsok/nb/824921f23d8278cf9650ee046c3209f8}}
* {{cite book |author=[[Ministry of Defence (Norway)|Ministry of Defence]] |title=Innstilling fra Jagerflyplassutvalget |location=Oslo |year=1988 |language=Norwegian |url=http://www.nb.no/nbsok/nb/5ee79330283df175f2e922db8bdf7992}}

{{Airports in Norway}}
{{Portal bar|Aviation|World War II|Norway}}

[[Category:Airports in Hedmark]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Royal Norwegian Air Force airfields]]
[[Category:Våler, Hedmark]]
[[Category:1944 establishments in Norway]]
[[Category:Airports established in 1944]]
[[Category:Military installations in Hedmark]]