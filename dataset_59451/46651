{{Infobox NRHP
| name                 = Boulder on the Park
| nrhp_type            = 
| image                = 
| alt                  = 
| caption              = 
| locmapin             = 
| map_alt              = 
| map_caption          = 
| coordinates          = {{coord|36.1342|N|95.9893|W|display=inline,title}}
| location             = 
| built                = 1923
| architect            = Atkinson & Olston (1923)<br>Malcolm McCune (1947)

| architecture         = Commercial Style Art Deco/Streamline Moderne
| added                = September 2003
| designated_nrhp_type = 
| visitation_num       = 
| visitation_year      = 
| refnum               = 03000872
| mpsub                = 
| governing_body       = Private
}}

'''Boulder-on-the Park''' is the name of a three-story building at 1850 South Boulder Avenue in Tulsa, Oklahoma.{{efn|The name of this building often appears in the print media in hyphenated form, Boulder-on-the Park.}} Originally built in 1923, it represents [[Commercial Style]] [[Art Deco]] architecture, It was designed by the Atkinson & Olston architectural firm and built by C. A. Sanderson & Son.<ref name="NRHPApp">{{NRHP url|id=03000872|title=Application for NRHP listing}}, Accessed August 2, 2015.</ref> The building overlooks Veterans Park, south of Downtown Tulsa.{{efn|The park was named Boulder Park when it was built in 1928.<ref>[https://www.cityoftulsa.org/culture--recreation/tulsa-parks/administration/tulsa-parks-history.aspx City of Tulsa. "Tulsa Parks History."] Accessed August 2, 2015</ref> It was renamed Veterans Park on November 10, 1990.<ref>[http://www.tulsaworld.com/archives/boulder-park-renamed-veterans-park/article_3ff3cbe2-adfb-5d58-8e45-6b1ded1bee61.html Tracy Souter, "Boulder Park Renamed Veterans Park," ''Tulsa World''. November11, 1990.] Accessed August 5, 2015.</ref>}}

The building has served several different functions. Upon completion, it housed the [[Holland Hall (Tulsa, Oklahoma)|Holland Hall School]], and bore the name of the school until 1932, when the school moved to new and larger premises, the Boulder building housed corporate offices, particularly the Aero Exploration Company, an aerial mapping company, which occupied it from 1938 to 1946. The building was extensively remodeled in 1947, then occupied by radio station KTUL. The station coined the name "Boulder on the Park" for its new premises.<ref name="TheseWalls">[https://www.questia.com/newspaper/1P2-38466052/these-walls-boulder-on-the-park Davis, KirLee, "These Walls:Boulder on the Park."] Accessed August 4, 2015.</ref> It remained in the building until 1956, when it moved to new facilities on [[Lookout Mountain, Oklahoma|Lookout Mountain]] and converted to a television station, KTUL-TV.<ref name="NRHPApp"/>  Holway Engineering Corporation bought the building in 1957 and remained there until 1974. The building was used as office space by several other companies until 2000, when Pan Western Energy Company sold it to Larry McIntosh, owner of The McIntosh Group, an architectural firm.<ref name="Jamison">[http://www.tulsapeople.com/Tulsa-People/November-2013/A-gem-on-Boulder/ Jamison, Alana. "A gem on Boulder," ''Tulsa People''.] November 2013, Accessed August 3, 2015.</ref>

==Addition to NRHP==
Boulder on the Park was added to the National Register of Historic Places on September 2003, under Criteria A. Its NRIS number is 03000872.<ref name="TPC-BOTP">[http://tulsapreservationcommission.org/art_deco/boulder-on-the-park/ "Boulder on the Park," Tulsa Preservation Commission.] Accessed August 2, 2015.</ref>

Although the building has been extensively remodeled since its original construction, its nomination was accepted because of its historical significance as a radio broadcasting station. The Period of Significance is stated as 1947, when KTUL moved into the building, to 1953 (50 years prior to listing on the NRHP).<ref name="NRHPApp"/>

==Original construction==
This building faces east, toward Veterans Park. It is three stories high, with a flat roof, brick walls and concrete foundation. Since the property has a significant uphill grade from front to rear, the first floor was excavated into the hillside and is mostly below grade. None of the first floor is visible from the rear.  The only entry was a single metal paneled door with a single light. The windows are wood with brick rowlock sills.<ref name="NRHPApp"/>

According to one news report, the school building had an interior area of {{convert|8200|sqft|m2}} which contained at least nine classrooms, a shop, gymnasium and a laboratory.<ref name="TheseWalls"/>

==1947 modifications==
Before radio station KTUL moved its studio from the National Bamk of Tulsa building (now known as the [[320 South Boston Building]]), its management decided to modernize the appearance. A rear entry was added to the west wall. The brick walls were painted, which emphasized the difference between the more decorative facing brick used on the front (east) and the first quarter of the north and south sides, and the common brick that covered the remainder of the building. Some additional architectural details were added to the facade, which gave the building a more [[Streamline Moderne]] appearance.

== Notes ==
{{notelist}}

== References ==
{{Reflist}}

==See also==
*[[Holland Hall School]]
*[[KTUL]]

== External links ==
* [http://www.example.com www.example.com]

{{NRHP in Tulsa County}}

[[Category:Buildings and structures in Tulsa, Oklahoma]]
[[Category:Office buildings completed in 1923]]
[[Category:Office buildings on the National Register of Historic Places]]
[[Category:Commercial buildings on the National Register of Historic Places in Oklahoma]]
[[Category:National Register of Historic Places in Tulsa, Oklahoma]]
[[Category:Art Deco architecture in Oklahoma]]
[[Category:Commercial Style architecture in the United States]]