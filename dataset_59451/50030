{{Infobox album
| Name       = Game of Thrones: Season 4
| Type       = soundtrack
| Artist     = [[Ramin Djawadi]]
| Cover      = Game_of_Thrones_Soundtrack_-_Season_4.png
| Alt        = Game of Thrones (soundtrack) album cover
| Released   = {{Start date|2014|06|10}} 
| Recorded   = 
| Genre      = Soundtrack
| Length     = 60:38
| Label      = [[WaterTower Music]]
| Producer   = [[Ramin Djawadi]]
| Chronology  = ''[[Music of Game of Thrones|Game of Thrones]]'' music
| Last album = ''[[Game of Thrones: Season 3 (soundtrack)|Game of Thrones: Season 3]]''<br />(2013)
| This album = '''''Game of Thrones: Season 4'''''<br />(2014)
| Next album = ''[[Game of Thrones: Season 5 (soundtrack)|Game of Thrones: Season 5]]''<br />(2015)
| Misc        = {{Extra chronology
| Artist     = [[Ramin Djawadi]] soundtrack
| Type       = Soundtrack
| Last album = ''[[Person of Interest: Season 2 (soundtrack)|Person of Interest: Season 2]]''<br />(2014)
| This album = '''''Game of Thrones: Season 4'''''<br />(2014)
| Next album = ''[[Game of Thrones: Season 5 (soundtrack)|Game of Thrones: Season 5]]''<br />(2015)
}}}}

The soundtrack album of the [[Game of Thrones (season 4)|fourth season]] of HBO series ''[[Game of Thrones]]'', titled '''''Game of Thrones: Season 4''''' was released digitally on June 10, 2014, and on CD on July 1, 2014. Season 4 of ''[[Game of Thrones]]'' saw the Icelandic band [[Sigur Rós]] perform their rendition of "[[Game of Thrones: Season 2 (soundtrack)#"The Rains of Castamere"|The Rains of Castamere]]" in a cameo appearance at King Joffrey's wedding in the second episode, "[[The Lion and the Rose]]".

==Reception==
The soundtrack received mostly positive reviews from critics. The soundtrack was awarded a score of 4/5 by Heather Phares of [[AllMusic]].<ref>{{cite web|title=Game of Thrones: Season 4 by Ramin Djawadi|url=http://www.allmusic.com/album/game-of-thrones-season-4-original-tv-soundtrack-mw0002679744|accessdate=July 20, 2014}}</ref>

==Track listing==
{{Track listing
| extra_column = Key scenes/Notes

| title1   = [[Game of Thrones Theme|Main Title]]
| length1  = 1:43
| extra1   = Used in the opening sequence.

| title2   = [[The Rains of Castamere (song)|The Rains of Castamere]]
| length2  = 2:42
| extra2   = (''Performed by [[Sigur Rós]]'') "The Lion and the Rose": Played at Joffrey's wedding and during the end credits.

| title3   = Breaker of Chains
| length3  = 4:05
| extra3   = "The Children": Daenerys places her dragons in their new home. "The House of Black and White": Drogon lands at the top of Daenerys' pyramid and she watches him fly away after she tries to caress him. "Mother's Mercy": Daenerys stands with Drogon in the mountains north of Mereen.

| title4   = Watchers on the Wall
| length4  = 2:11
| extra4   = "The Watchers on the Wall": Jon goes beyond the Wall to find Mance Rayder. The cue is an adventurous variation of the Night's Watch theme. The track also contains hints of Jon and Ygritte's theme, "You Know Nothing".

| title5   = I'm Sorry for Today
| length5  = 2:09
| extra5   = "The Mountain and the Viper": Heard when Grey Worm apologises to Missandei for watching on her during the river bathing. "Kill the Boy": When Missandei is sitting next to a convalescent Grey Worm and Daenerys mourns Barristan Selmy in the Audience Chamber.

| title6   = Thenns
| length6  = 1:43
| extra6   = "Breaker of Chains": Variations of the theme (with an extra melodic line in the low strings) are heard when the Thenns attack a peaceful village, which Olly is from. "The Mountain and the Viper": the same variations are heard when Mole's Town is attacked. "The Watchers on the Wall": heard fully during the attack and battle at Castle Black. "Home": the wildlings, led by Tormund Giantsbane and Dolorous Edd, attack Castle Black.

| title7   = Meereen
| length7  = 2:53
| extra7   = "Walk of Punishment": the introduction of the piece can be heard when Daenerys offers to hand one of her dragons so as to buy all of the Unsullied. "Two Swords": the introduction again is heard during Daario's and Grey Worm's wager. "Breaker of Chains": Daenerys and her troops arrive at Meereen, before the duel of Daario and the champion of Meereen outside the city walls. "Oathkeeper": the last seconds are heard when Daenerys stands victorious on the Pyramid of Meeren after taking the city.

| title8   = First of His Name
| length8  = 3:52
| extra8   = "The Lion and the Rose": the first part is heard when Joffrey and Margaery Tyrell are married. The second dissonant part is heard when Joffrey suffocates to death and Cersei accuses Tyrion of murdering her son. "High Sparrow": the first part is heard when Tommen marries Margaery. Contains the House Baratheon theme.

| title9   = The Biggest Fire the North Has Ever Seen
| length9  = 1:56
| extra9   = "The Watchers on the Wall": Jon and the rangers watch from atop the wall the huge fire beneath them, just before the commence of the battle of Castle Black. Contains the melody of "Thenns".

| title10  = Three Eyed Raven
| length10 = 3:58
| extra10  = "The Children" Bran, Hodor and Meera find the Weirwood tree.

| title11  = Two Swords
| length11 = 1:49
| extra11  = "Two Swords": played in cold open scene (prior to the main title sequence) showing Tywin reforging the Stark family greatsword "Ice" into two smaller swords and burning Ned's wolfskin scabbard-covering in the forge fire. Contains House Stark theme (Goodbye Brother) and House Lannister theme (The Rains of Castamere).

| title12  = Oathkeeper
| length12 = 4:31
| extra12  = "Dark Wings, Dark Words": A variation is heard when Brienne duels Jaime on the bridge. "And Now His Watch is Ended": parts play when the Hound stands accused in front of the Brotherhood without Banners in the cave. "The Children", the Hound and Brienne duel. "Mother's Mercy" A variation plays when Brienne sentences Stannis to death and prepares to execute him. Contains a melody associated with Catelyn Stark and later with Brienne of Tarth (The Old Gods and The New).

| title13  = You Are No Son of Mine
| length13 = 4:29
| extra13  = "The Children": Tyrion confronts Tywin with the crossbow while making his escape. Afterwards, Tyrion and Varys make it to the departing ship and Arya rides to the port to board the ship to Braavos. Contains the melody of the Lannister theme (The Rains of Castamere) and the Main Theme, the latter in a Cello version and a choral version.

| title14  = The North Remembers
| length14 = 2:33
| extra14  = "The Kingsroad": Catelyn Stark says goodbye to a still-dormant Bran before leaving for King's Landing. "Garden of Bones": Petyr returns Ned's remains to Catelyn. "Dark Wings, Dark Words": Catelyn tells Talisa how she once wished for Jon Snow's death when he was a child and how she later regretted and built a prayer wheel for him to pray for his recovery. "Two Swords": Sansa laments Catelyn and Robb's murder.
The cue is a sorrowful rendition of the House Stark theme.

| title15  = Let's Kill Some Crows
| length15 = 3:36
| extra15  = "The Watchers on the Wall": During the battle at Castle Black. It is also heard during the end credits. Contains the melody of "Thenns" and the Main Theme.

| title16  = Craster's Keep
| length16 = 2:06
| extra16  = "Walk of Punishment": the beginning of the piece is also heard when the remains of the party of Night Watch beyond the Wall reach Craster's Keep. "First of His Name": Jon burns down Craster's keep.

| title17  = The Real North
| length17 = 2:03
| extra17  = "The Children": Jon goes North of the Wall to make a funeral pyre for Ygritte. Contains the melody from Jon and Ygritte's theme (You Know Nothing).

| title18  = Forgive Me
| length18 = 2:31
| extra18  = "The Mountain and the Viper": Daenerys dismisses Ser Jorah and sends him into exile. "Hardhome": With the suggestion of Tyrion, Daenerys dismisses Ser Jorah yet again. Contains part of Daenerys Targaryen's theme.

| title19  = He Is Lost
| length19 = 3:37
| extra19  = "The Children": Heard when Bran, Meera and Jojen get attacked on the frozen lake and Jojen is killed. Contains hints of the White Walkers' theme (White Walkers).

| title20  = I Only See What Matters
| length20 = 1:25
| extra20  = "The Children": Cersei reconciles with Jaime and both make love.

| title21  = Take Charge of Your Life
| length21 = 2:05
| extra21  = "The Mountain and the Viper": Sansa is interrogated by the Lords of the Vale and clears Petyr from all charges. The first part of the cue is also heard when Petyr tells Robin to discover the world outside the Eyrie. "The Gift":  Petyr Baelish and Olenna Tyrell secretly plot in the first's brothel. Contains elements of the Stark theme (Goodbye Brother) and elements of Petyr Baelish's theme (Chaos is a Ladder).

| title22  = The Children
| length22 = 2:41
| extra22  = "The Children": heard when Arya leaves Westeros and sails to Braavos. The track is predominantly a modified choral version of the Main Theme, though it also contains elements of the House Stark theme (Goodbye Brother), and Jaqen H'ghar's theme (Valar Morghulis). A shorter version is used on the main menu of Season 4's DVD/Blu-ray release.
}}

==Credits and personnel==
Personnel adapted from the album liner notes.<ref>{{cite web|title=Game of Thrones: Season 4 by Ramin Djawadi|url=http://www.allmusic.com/album/game-of-thrones-season-4-original-tv-soundtrack-mw0002679744/credits|accessdate=July 20, 2014}}</ref>
{{col-begin}}
{{col-2}}
* David Benioff – liner notes
* Ramin Djawadi – composer, primary artist, producer
* Sigur Rós – primary artist 
{{col-2}}
* George R.R. Martin – lyricist
* D.B. Weiss – liner notes 
{{col-end}}

==Charts==
{| class="wikitable sortable plainrowheaders" style="text-align:center"
|-
! scope="col"| Chart (2014)
! scope="col"| Peak<br /> position
|-
{{albumchart|Wallonia|150|artist=Soundtrack / Ramin Djawadi|album=Game of Thrones - Season 4|rowheader=true|accessdate=July 7, 2016}}
|-
|}

==Awards and nominations==
{| class="wikitable sortable plainrowheaders"
|-
! Year
! Award
! Category
! Nominee(s)
! Result
! class="unsortable" | {{Abbreviation|Ref.|Refeference}}
|-
| rowspan="2"| 2014
|scope="row"| [[66th Primetime Creative Arts Emmy Awards]]
|scope="row"| [[Primetime Emmy Award for Outstanding Music Composition for a Series|Outstanding Music Composition For A Series (Original Dramatic Score)]]
|scope="row"| Episode: "[[The Mountain and the Viper]]"
| {{nom}}
| <ref>{{cite web|url=http://www.telegraph.co.uk/culture/tvandradio/emmys/10959030/Emmy-Awards-2014-Nominations.html|title=Emmy Awards 2014: the nominations in full|date=July 10, 2014|work=Daily Telegraph|accessdate=July 10, 2014}}</ref>
|-
|scope="row"| [[Hollywood Music in Media Awards]]
|scope="row"| Best Original Score - TV Show/Digital Streaming Series
|scope="row"|
| {{nom}}
| <ref>{{cite web|url=http://filmmusicreporter.com/2014/10/08/hollywood-music-in-media-awards-nominations-announced/|title=Hollywood Music in Media Awards Nominations Announced|publisher=Filmmusicreporter.com|date=October 8, 2014|accessdate=February 28, 2017}}</ref>
|-
|}

==References==
{{reflist|30em}}

{{ASOIAF}}
{{Ramin Djawadi}}

[[Category:Ramin Djawadi soundtracks]]
[[Category:2014 soundtracks]]
[[Category:Music of Game of Thrones|Soundtrack]]
[[Category:Classical music soundtracks]]
[[Category:Instrumental soundtracks]]
[[Category:Television soundtracks]]
[[Category:WaterTower Music soundtracks]]