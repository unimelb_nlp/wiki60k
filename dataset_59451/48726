{{infobox book 
| name          = Inland
| title_orig    =
| translator    =
| image         = 
| image_size    = 
| caption = 
| author        = [[Gerald Murnane]]
| illustrator   =
| cover_artist  =
| country       = Australia
| language      = English
| series        =
| genre         = 
| publisher     = [[William Heinemann Australia]]
| release_date  = 1988
| english_release_date =
| external_url  = 
| dewey= 
| ISBN= 978-1-922146-28-1
| congress= 
| oclc= 
| preceded_by   =
| followed_by   =
}}

'''''Inland''''' is a novel by [[Gerald Murnane]], first published in 1988. It has been described.<ref>{{Cite web|title = Reading Gerald Murnane {{!}} Dalkey Archive Press|url = http://www.dalkeyarchive.com/reading-gerald-murnane/|website = www.dalkeyarchive.com|access-date = 2016-01-22}}</ref><ref name=":0">{{Cite web|title = The Quest for the Girl from Bendigo Street|url = http://www.nybooks.com/articles/2012/12/20/quest-girl-bendigo-street/|website = The New York Review of Books|access-date = 2016-01-22}}</ref><ref name=":10">{{Cite web|title = Gerald Murnane's new novel keeps faith with longtime obsessions|url = http://www.smh.com.au/entertainment/books/gerald-murnanes-new-novel-keeps-faith-with-longtime-obsessions-20140609-zs1l9.html|website = The Sydney Morning Herald|access-date = 2016-01-23}}</ref> as one of Murnane's greatest and most ambitious works, although some reviewers<ref name=":8">{{Cite web|title = Inland by Gerald Murnane|url = http://quarterlyconversation.com/inland-by-gerald-murnane|website = Quarterly Conversation|access-date = 2016-01-23|language = en-us}}</ref><ref name=":5" /> have criticised its use of repetition, lack of clear structure and reliance on writing as a subject matter. Reviewing the book in 2012, [[J. M. Coetzee]] called it "the most ambitious, sustained, and powerful piece of writing Murnane has to date brought off".<ref name=":0" />

Set in the plains of [[Hungary]], the [[United States]] and [[Australia]], ''Inland'' explores themes of memory,<ref name=":1">Murphy, p. 2</ref> landscape,<ref name=":1" /> longing,<ref name=":5" /> love<ref name=":0" /> and writing.<ref name=":5">{{Cite web|title = Inland - Gerald Murnane|url = http://www.complete-review.com/reviews/murnane/inland.htm|website = www.complete-review.com|access-date = 2016-01-22|first = M. A.|last = Orthofer}}</ref>

== Inspiration ==
[[File:Hortobagy-ziehbrunnen.jpg|thumb|Typical draw well in the [[Puszta]] in [[Hortobágy National Park]]]]
In 1977, Murnane read ''A Puszták népe'' (''People of the [[Puszta]]'') by the [[Hungarians|Hungarian]] poet and novelist [[Gyula Illyés]].<ref name=":3">{{Cite web|title = The Angel’s Son: Why I Learned Hungarian Late in Life - Hungarian Review|url = http://www.hungarianreview.com/article/the_angel_s_son_why_i_learned_hungarian_late_in_life|website = www.hungarianreview.com|access-date = 2016-01-22}}</ref> The book had such a profound effect on him that it not only made him learn the [[Hungarian language]], but also compelled him to write a book – ''Inland'' – in order to "relieve [his] feelings", as he later put it.<ref name=":3" /> In a 2014 interview,<ref name=":4">{{Cite web|title = Gerald Murnane Interview {{!}} Ivor Indyk {{!}}|url = http://www.sydneyreviewofbooks.com/fiction-alchemy-interview-gerald-murnane/|website = Sydney Review of Books|access-date = 2016-01-22|language = en-US}}</ref> Murnane described one passage in particular as having changed his life, which passage describes cowherds pulling the body of a young woman out of a well:<blockquote>Of all the images that I have in mind, that one has probably has yielded the most and has perhaps even still the most to yield. It caused me to learn the Hungarian language, for one thing, and to be able to quote the whole of that passage in Hungarian.  [''Speaks Hungarian''] – that’s the cowherds pulled her out when they watered the cattle at dawn section. And I wrote the book ''Inland'' and the well just keeps occurring – I don’t go looking for it, it comes looking for me. And it occurs in numerous places, as you’ve said, in other books and things I have written.<ref name=":4" /></blockquote>A quote of this passage (from the English translation of ''A Puszták népe'') appears towards the end of ''Inland'' itself.<ref name=":7">Murnane, p. 236</ref>

== Plot summary ==
''Inland'' has been described as a complex work of fiction,<ref name=":0" /><ref name=":9">Murphy, p. 1</ref> lacking plot and characters in the traditional sense.

The early parts of the book are set in [[Szolnok County]], Hungary, where the narrator is writing in "heavy-hearted Magyar"<ref>Murnane, p. 5</ref> to his editor and translator, Anne Kristaly Gunnarsen. Gunnarsen lives with her husband in [[Ideal, South Dakota]], where they are both working at the Calvin O. Dahlberg Institute of Prairie Studies.

The second half of the book is mainly set in Australia and concerns the narrator's trying to find the address of a girl (referred to only as "the girl from Bendigo Street") he once knew as a child.

== Interpretations ==
''Inland'' is set partly on the [[Great Alfold]] in Hungary, partly in the grasslands of [[Tripp County, South Dakota|Tripp County]], [[South Dakota]] and partly in [[Melbourne County]], Australia. The geographical shifts in the book's narrative has been interpreted by some<ref name=":9" /><ref>Fawkner, p. 25</ref>  as indicating the presence of multiple, separate narrators, although Murnane himself denies this, stating that he "consider[s] the book to have only one narrator and not the several that some readers have seemed to find".<ref name=":2" />

Some commentators<ref>Murphy, p. 3</ref><ref>Fawkner, p. 16-18</ref> have taken the recurring plains and grasslands to be metaphors of the author's metaphysics. The book's narrator repeatedly declares what Murnane has called "a little musical phrase":<ref name=":4" /> that "no thing in the world is one thing"<ref>Murnane, p. 73</ref> and that "each place is more than one place".<ref>Murnane, p. 80</ref> In a [[Martin Heidegger|Heideggerian]] reading, Murphy suggests that the narrator, in describing his experience of wind moving over a landscape, is able to "[transpose] his understanding of his immersion in the physical world to the [[World disclosure|ontological world]]".<ref>Murphy, p. 9</ref>

The book contains numerous references to other literary works. At one point in the book, the narrator discovers an epigraph in a novel by [[Patrick White]]: "One day in this room I read in the preliminary pages of an unlikely book these words: ''There is another world but it is in this one. [[Paul Éluard|Paul Eluard]]''".<ref>Murnane, p. 148</ref> The narrator also describes at length his memories of having read [[Emily Brontë]]'s ''[[Wuthering Heights]]'' and [[Thomas Hardy]]'s ''[[Tess of the d'Urbervilles|Tess of the D'Urbervilles]]'',<ref>Murnane, p. 173-177</ref> and quotes from a number of works, including a book by [[William Henry Hudson|W. H. Hudson]],<ref>Murnane, p. 205</ref> a biography of [[Marcel Proust]],<ref>Murnane, p. 190</ref> the [[New Testament]]<ref>Murnane, p. 209 & 229</ref> and, as mentioned above, Gyula Illyés's ''People of the Puszta''.<ref name=":7" />

In a foreword to the 2013 edition, Murnane, worried that he might have put into the text "more of [himself] than was seemly", wrote that, as he was reading the [[page proofs]] for the reissue, he was "surprised and relieved to learn how much of the text must have sprung from not from its author's memory but from those other sources often called collectively the imagination."<ref name=":2">Murnane, p. 1-2 (Foreword)</ref>

== Reception ==
Internationally, ''Inland'' did not initially capture a large readership. It appeared in the [[United Kingdom]], where it had only a single review and scanty sales.<ref name=":6">{{Cite journal|url = http://www.nla.gov.au/ojs/index.php/jasal/article/viewFile/857/1746|title = The Global Reception of Post-national Literary Fiction: The Case of Gerald Murnane|last = Genoni|first = Paul|date = 2009|journal = JASAL, Special Issue: Australian Literature in a Global World|doi = |pmid = |access-date = 2016-01-23|page = 7}}</ref> In 1995, a [[Sweden|Swedish]] translation of the book (''Inlandet'') was released;<ref name=":6" /> this edition did not garner much attention,<ref>{{Cite web|title = Murnane skapar  gåtfulla landskap|url = http://www.svd.se/murnane-skapar-gatfulla-landskap|website = SvD.se|access-date = 2016-01-23|first = Eva|last = Johansson}}</ref> although reviews of Murnane's work, including ''Inland'', have generally been kinder in Scandinavia than in the US and UK.<ref name=":6" />

Helen Harris, reviewing the book for the [[The Times Literary Supplement|Times Literary Supplement]], wrote that "[by] constantly game-playing and undermining the edifice of his own fiction, Murnane is left with an end product too artificial to have much evocative force".<ref>Harris, Helen (1989). ''Times Literary Supplement''.</ref> Another reviewer considered the book's use of repetition and its "disappointing, stagnant rendering of memory, time, and fantasy" to make it tedious to read.<ref name=":8" />

However, in 2012, after its having been re-released in the US, ''Inland'' was the subject of a lengthy review by Coetzee, published in the [[The New York Review of Books|New York Review of Books]], that described it as Murnane's most ambitious and powerful work to date.<ref name=":0" /> Coetzee wrote: "The emotional conviction behind the later parts of ''Inland'' is so intense, the somber lyricism so moving, the intelligence behind the chiseled sentences so undeniable, that we suspend all disbelief".<ref name=":0" /> [[Peter Craven (literary critic)|Peter Craven]], writing in the [[The Sydney Morning Herald|Sydney Morning Herald]], called it "a work that dazzles the mind with its grandeur and touches the heart with a great wave of feeling".<ref name=":10" />

The book has also been the subject of several PhD theses and other scholarly work.<ref>{{Cite book|title = The Double Aspect : Gerald Murnane's Visual Poetics|last = Posti|first = Piia|publisher = Stockholm: Engelska institutionen|year = 2005|isbn = |location = Stockholm|pages = }}</ref><ref>{{Cite book|title = Iconicity in the writing process : Virginia Woolf's To the Lighthouse and Gerald Murnane's Inland|last = Sundin|first = Lena|publisher = Göteborg University, Dept. of English|year = 2004|isbn = |location = Göteborg|pages = }}</ref><ref>Fawkner</ref>

== References ==
<references />

== Cited texts ==
* {{Cite book|title = Grasses that Have No Fields: From Gerald Murnane's Inland to a Phenomenology of Isogonic Constitution|url = https://books.google.com/books?id=420iAQAAIAAJ|publisher = Stockholm University|date = 2006-01-01|isbn = 9789185445226|first = Harald William|last = Fawkner}}
* {{cite book | last = Murnane | first = Gerald | title = Inland | year = 2013 | publisher = Giramondo Publishing Company | location =Artarmon, New South Wales | isbn = 978-1-922146-28-1 }}
* {{Cite journal|url = http://www.nla.gov.au/ojs/index.php/jasal/article/viewFile/3192/4079|title = Being-in-landscape: a Heideggerian reading of landscape in Gerald Murnane's Inland.|last = Murphy|first = Julian|date = 2014|journal = Journal of the Association for the Study of Australian Literature: JASAL|doi = |pmid = |access-date = 2016-01-23}}

[[Category:20th-century Australian novels]]