{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
{|{{Infobox Aircraft Begin
 | name=Lancaster PA278 disappearance
 | image=Lanc2adj2.JPG
 | caption=A Lancaster bomber in flight
}}{{Infobox Aircraft Career
|sole example of type?= N <!-- only Y or N -->
|aircraft name        = City of Lincoln
|image                = ONLY AVAILABLE IF STANDING ALONE<!--in the ''Image:filename'' format with no image tags-->
|caption              = ONLY AVAILABLE IF STANDING ALONE<!--Image caption; if it isn't descriptive, please skip-->
|other names          = <!--Other names (nicknames, nose art names) this aircraft is known by-->
|type                 = [[Avro Lancaster]]
|manufacturer         = 
|construction number  = 
|construction date    =  <!-- either roll-out date or span of time for lengthy projects, whichever seems more appropriate -->
|civil registration   = 
|military serial      = PA278
|radio code           = <!-- military radio codes where this is a commonly-used way of identifying this aircraft (eg. US, British, and German military aircraft of WWII -->
|first flight         = <!-- date of first flight -->
|owners               = [[Royal Air Force]]
|in service           = 1945
|flights              = <!-- number of flights made by this aircraft, usually only relevant for an aircraft no longer flying -->
|total hours          = <!-- total number of hours flown by this aircraft, usually only relevant for an aircraft no longer flying -->
|total distance       = <!-- total distance flown by this aircraft, usually only relevant for an aircraft no longer flying -->
|fate                 = Lost on 4 October 1945.
}}
|}
The '''Lancaster PA278 disappearance''' involved [[Avro Lancaster Mk.I]] PA278, "F for Freddie", operated by [[No. 103 Squadron RAF]] (103 Sqn) of [[RAF Bomber Command|Bomber Command]] just after the end of the [[World War II|Second World War]]. 

It disappeared over the [[Mediterranean]], probably near [[Corsica]], on 4 October 1945 with its crew of 6 airmen and 19 female service personnel - the worst loss of female British and Commonwealth service personnel from the Second World War II to date.<ref>[http://www.aircrewremembered.com/raf1945/3/sassoonrichard.html  Aircrew Remembered Website - Female casualties]</ref><ref>Cummings, (2004).  PA278</ref>

==The Lancaster==
PA278 was one of a batch of 265 Lancasters built by [[Vickers-Armstrongs| Vickers-Armstrongs Ltd]] at its [[Hawarden Airport|Broughton factory]] near [[Chester]] between June 1944 and September 1945. On completion it was assigned to 103 Sqn of Bomber Command and flew operationally on a number of bombing attacks in March and April 1945.<ref>Halley (1985), p.114</ref><ref>National Archives, London. AIR 27/817/8 - 103 Squadron</ref>

==Operation Dodge==
From 3 August 1945 103 Sqn was one of the heavy bombers squadrons participating in ''[[Operation Dodge]]'' in which veteran soldiers of the [[Eighth Army (United Kingdom)|Eighth Army]] were flown home from Italy and the Central Mediterranean. An embarkation centre was established at [[Bari]] airfield, on the [[Adriatic]] coast of southern [[Italy]]. An airfield at [[Naples]], [[Italy]] was also used for geographic convenience.<ref>National Archives, London. AIR 27/817/11  - 103 Squadron</ref><ref>Mason (1989), p.405</ref>

It was a six-hour flight back to England and normally a maximum of 22 soldiers would be transported home as the internal confines of a Lancaster were not really suitable for passengers. Often the return trip to Italy saw aircraft transporting essential members of services out to Italy.<ref>Mason (1989), p.207-209</ref>

103 Sqn usually despatched between six and ten aircraft for ''Operation Dodge'' several days each week, the aircraft and crews would stop overnight in [[Italy]] and return with their passengers the following day or the day after that.  Most of the trips were uneventful although squadron records do show that [[Warrant Officer]] Francis flying Lancaster "B for Baker" and [[Flying Officer]] Leigh flying "G for George" both had to land at [[Marseille]] in [[France]] due to engine failure, on 19 August 1945 and 5 September 1945 respectively.

==Disappearance==
[[File:Lancaster bomber over Cowes in May 2013.jpg|thumb|right|A Lancaster in flight.]]
On 3 October 1945 the weather was poor and low cloud base precluded flying for much of the day.  Eight Lancasters of 103 Sqn were available for "Operation Dodge" and took off for [[Italy]] starting at 23:30 hours GMT.  Two aircraft returned with engine trouble, one crew borrowed an aircraft from [[No. 100 Squadron RAF]] and took off again at 01:30 hours GMT.  Two aircraft, including PA278 "F for Freddie", landed at [[RAF Glatton]] near [[Huntingdon]] to embark passengers who were commencing service in [[Italy]] or were returning from leave.<ref>[http://aviation-safety.net/wikibase/wiki.php?id=158426  AviationSafetyNetwork - loss of PA278]</ref><ref>[http://www.reffell.org.uk/WW2/heathercosens.php   Obituary - Heather Cosens]</ref>

PA278 was flown by Geoffrey Taylor's experienced crew who had flown together on some of the last bombing attacks of [[World War II]] during April 1945, although obviously their bomb aimer was not required and on this occasion the rear gun turret was manned by the squadron gunnery leader [[Flight Lieutenant]] John Wymark,<ref>[http://www.cwgc.org/find-war-dead/casualty/1531505/WHYMARK,%20JOHN%20PERCY  CWGC details - F/Lt JP Whymark DSO DFC]</ref> who had been decorated with the [[Distinguished Service Order]]<ref>{{LondonGazette|issue=37324|supp=yes|startpage=5234|date=23 October 1945}}</ref> and [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] for his bravery in three tours of duty.<ref>{{LondonGazette|issue=36745|supp=yes|startpage=4693|date=10 October 1944}}</ref>

PA278 took off from [[RAF Glatton]] at 00:30 hours GMT on 4 October 1945 flown by [[Flight lieutenant]] Geoffrey Taylor,<ref>[http://www.cwgc.org/find-war-dead/casualty/1808508/TAYLOR,%20GEOFFREY  CWGC details - F/Lt G Taylor RAFVR]</ref> (pilot) aided by [[Sergeant]] Richard Steel,<ref>[http://www.aircrewremembered.com/raf1945/3/sassoonrichard.html   AircrewRemembered - Richard Steel]</ref><ref>[http://www.cwgc.org/find-war-dead/casualty/1807895/STEEL,%20RICHARD%20GEORGE  CWGC details - Sgt RA Steel RAFVR]</ref> (flight engineer) leading the second transport aircraft into the air. It carried its crew of six and was scheduled fly to [[Naples]] with 17 female soldiers of the [[Auxiliary Territorial Service]] under Staff Serjeant Jessie Ellen Semark,<ref>[http://www.cwgc.org/find-war-dead/casualty/2148023/SEMARK,%20JESSIE%20ELLEN  CWGC details - S/Sjt JE Semark ATS]</ref> Senior Matron Gertrude Irene Sadler of the South African Military Nursing Service<ref>[http://www.southafricawargraves.org/search/view-paginated.php?page=22&ipp=10&locality=1&country=39&unit=1  South African War Graves - Miss GI Sadler]</ref> and Nursing Sister Jane Simpson Annand Curran,<ref>{{LondonGazette|issue=35698|supp=yes|startpage=3959|date=11 September 1944}}</ref> of [[Queen Alexandra's Imperial Military Nursing Service]].<ref>[http://aviation-safety.net/wikibase/wiki.php?id=158426  AviationSafetyNetwork - loss of PA278]</ref>

The passengers sat in canvas folding seats down the length of the fuselage beneath where [[Sergeant]] William Kennedy,<ref>[http://www.cwgc.org/find-war-dead/casualty/1802609/KENNEDY,%20WILLIAM%20JOHN  CWGC details - Sgt WJ Kennedy RAFVR]</ref> sat in his sling seat in the "Mid-Upper" gun turret, but no heating or parachutes were available for them and there was no oxygen supply for passengers so the aircraft flew at {{convert|2000|ft|m|abbr=on}}.<ref>[http://www.reffell.org.uk/WW2/heathercosens.php  Obituary - Heather Cosens]</ref> 

The prescribed route was directly across [[France]] and over the [[Mediterranean]] coast west of [[Marseille]].  [[Flight Sergeant]] Jack Reardon,<ref>[http://www.cwgc.org/find-war-dead/casualty/2940309/ROBBINS,%20NORMAN%20REGINALD   CWGC details - F/Sgt NR Robbins RAFVR]</ref> (navigator) gave the correct course out over the sea but sometime afterwards, approximately 30 miles north-north-west of [[Cap Corse]], [[Corsica]], [[Flight Sergeant]] Norman Robbins,<ref>[http://www.cwgc.org/find-war-dead/casualty/2939966/REARDON,%20JACK%20ANTHONY  CWGC details - F/Sgt JA Reardon RAFVR]</ref> (wireless operator) radioed the other aircraft reporting some engine problems and advising that "F for Freddie" would be turning back to land at [[Marseille]]. The partner aircraft continued onwards for [[Naples]] but noted a flash of flame at 04:40 hours GMT.<ref>[http://aviation-safety.net/wikibase/wiki.php?id=158426  AviationSafetyNetwork - loss of PA278]</ref>

Nothing further was heard of PA278 or her crew and passengers and no wreckage was sighted during the air-sea rescue operations. On the outward flight every aircraft reported poor weather in the vicinity of [[Corsica]].<ref>[http://aviation-safety.net/wikibase/wiki.php?id=158426  AviationSafetyNetwork - loss of PA278]</ref>

==Aircrew and passengers==
{| class="wikitable sortable"
|-
! Name !! Rank !! Nation !! Age!! Status !! Number !! Unit || Commemorated  
|-
| Geoffrey Taylor || [[Flight lieutenant]] ||{{Flagicon|UK}} UK || 24 || Pilot || 189687 || [[No. 103 Squadron RAF]]||[[Runnymede Memorial]]
|-
| Jack Anthony Reardon|| [[Flight Sergeant]] ||{{Flagicon|UK}} UK ||   || Navigator || 1626816 ||[[No. 103 Squadron RAF]]||[[Runnymede Memorial]]
|-
| Norman Reginald Robbins  || [[Flight Sergeant]]||{{Flagicon|UK}} UK|| 20 || Wireless Operator || 1894110|| [[No. 103 Squadron RAF]] || [[Runnymede Memorial]]
|-
| Richard George Steel || [[Sergeant]] ||{{Flagicon|UK}} UK || 20 || Flight Engineer || 1818104 || [[No. 103 Squadron RAF]] || [[Runnymede Memorial]]
|-
| William John Kennedy || [[Sergeant]] ||{{Flagicon|UK}} UK || || Mid-Upper Gunner || 2208930 || [[No. 103 Squadron RAF]] || [[Runnymede Memorial]]
|-
| John Percy Whymark DSO DFC || [[Flight lieutenant]] ||{{Flagicon|UK}} UK || 25 || Rear Gunner || 53481 || [[No. 103 Squadron RAF]]|| [[Runnymede Memorial]]
|-
| Gertrude Irene Sadler || Senior Matron||{{Flagicon|South Africa|1928}} [[Republic of South Africa|RSA]] || 41|| Passenger || 254580 || S A Military Nursing Service || [[Cassino Memorial]]
|-
| Jane Simpson A Curran || Nursing Sister ||{{Flagicon|UK}} UK ||  || Passenger || 236425 || [[QAIMNS]] || [[Brookwood Memorial]]
|-
| Jessie Ellen Semark<ref>[[Mentioned in Despatches]]</ref> || Staff Serjeant ||{{Flagicon|UK}} UK|| 29 || Passenger || W/7326 || [[Auxiliary Territorial Service]] ||[[Brookwood Memorial]]
|-
| Heather Cosens || [[Corporal]] ||{{Flagicon|UK}} UK || 25 || Passenger || W/184715 || [[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Jill Goring || [[Corporal]]||{{Flagicon|UK}} UK || 23 || Passenger || W/237256 || [[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Williamina Allan || [[Lance Corporal]] ||{{flagicon|UK}} UK || 37 || Passenger || W/23244 || [[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Sheila MacLeod || [[Lance Corporal]] ||{{Flagicon|UK}} UK ||  || Passenger || W/170036 ||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| May Eleanor Mann || [[Lance Corporal]] ||{{Flagicon|UK}} UK || 23 || Passenger || W/236937 ||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Enid Dacia Rice || [[Lance Corporal]] ||{{Flagicon|UK}} UK  || 24 || Passenger || W/144264 ||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Stefahia Courtman || Private ||{{flagicon|Mandatory Palestine}} [[Mandatory Palestine|BMP]] ||  || Passenger || W/Pal/203386 ||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Phyllis Kathleen Bacon || Private ||{{Flagicon|UK}} UK || 34 || Passenger|| W/77415 ||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Barbara Diana Cullen<ref>[[Mentioned in Despatches]]</ref> || Private||{{Flagicon|UK}} UK || 30 || Passenger|| W/252761||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Agnes Edwards || Private ||{{Flagicon|UK}} UK || 28 || Passenger || W/258692||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
|Rhoda Alice Fraser || Private||{{Flagicon|UK}} UK || 24 || Passenger || W/155271||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
|Bessie Goodman  || Private ||{{Flagicon|UK}} UK || 25 || Passenger || W/143732||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Joan Larkin || Private ||{{Flagicon|UK}} UK || 25 || Passenger || W/154454||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Alice Lillyman || Private ||{{Flagicon|UK}} UK || 22 || Passenger || W/74459||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Betty Evelyn Precious || Private||{{Flagicon|UK}} UK || 24 || Passenger || W/147946||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
| Marion Taylor || Private ||{{Flagicon|UK}} UK || 22 || Passenger || W/99752||[[Auxiliary Territorial Service]] || [[Brookwood Memorial]]
|-
|}

==News blackout==
Due to the large number of female service personnel lost in the disappearance of PA278 "F for Freddie" a widely repeated story is that the news was not released to the press until March 1946,<ref>[https://derbyshirewarmemorials.wikispaces.com/Jill+Goring   Derbyshire War Memorials - Jill Goring]</ref> although this is incorrect as on 17 November 1945 it was being reported widely in local newspapers in the UK.<ref>''Nottingham Evening News'', 17 November 1945, p.4</ref>

==See also==
* [[List of fatal accidents and incidents involving Royal Air Force aircraft from 1945]]
* [[Operation Exodus (WWII operation)]]

==References==
{{reflist|2}}

;Bibliography
{{refbegin}}
* Halley, James J. "Lancaster File". London: Air Britain., 1985. ISBN 978-0851301075.
* Cummings, Colin. "The Price of Peace".  Nimbus., 2004. ISBN 978-0952661955.
* Mason, Francis K. "The Avro Lancaster". London: Air Britain., 1989. ISBN 978-0946627301.
* Bowyer, Michael J.F. and John D.R. Rawlings. ''Squadron Codes, 1937-56''. Cambridge, UK: Patrick Stephens Ltd., 1979. ISBN 0-85059-364-6.
* Charlwood, Don. ''No Moon Tonight''. London: Goodall Publications Ltd., 1984. ISBN 0-907579-06-X.
* Finn, Sid. ''Black Swan: A History of 103 Squadron RAF''. Newton Publishers, 1989. ISBN 1-872308-00-7.
* Flintham, Vic and Andrew Thomas. ''Combat Codes: A full explanation and listing of British, Commonwealth and Allied air force unit codes since 1938''. Shrewsbury, Shropshire, UK: Airlife Publishing Ltd., 2003. ISBN 1-84037-281-8.
* Halley, James J. ''The Squadrons of the Royal Air Force & Commonwealth 1918-1988''. Tonbridge, Kent, UK: Air Britain (Historians) Ltd., 1988. ISBN 0-85130-164-9.
* Jefford, Wing Commander C.G., [[Order of the British Empire|MBE]], BA, RAF(Retd.). ''RAF Squadrons, a Comprehensive record of the Movement and Equipment of all RAF Squadrons and their Antecedents since 1912''. Shrewsbury, Shropshire, UK: Airlife Publishing, 1988 (second edition 2001). ISBN 1-85310-053-6.
* Moyes, Philip J.R. ''Bomber Squadrons of the RAF and their Aircraft''. London: Macdonald and Jane's (Publishers) Ltd., 2nd edition 1976. ISBN 0-354-01027-1.
* Public Record Office AIR 27 103 Squadron files.
* Rawlings, John D.R. ''Coastal, Support and Special Squadrons of the RAF and their Aircraft''. London: Jane's Publishing Company Ltd., 1982. ISBN 0-7106-0187-5.
{{refend}}

==External links==
{{Commons category|No. 103 Squadron RAF}}
* [http://www.raf.mod.uk/bombercommand/h103.html Squadron history on MOD site] 
* [http://www.rafweb.org/Sqn101-105.htm Squadron histories for nos. 101-105 sqn on RafWeb's Air of Authority - A History of RAF Organisation]



[[Category:History of the Royal Air Force]]
[[Category:Avro Lancaster|PA278]]
[[Category:Individual aircraft]]
[[Category:Aviation accidents and incidents in 1945]]
[[Category:Missing aircraft]]
[[Category:Accidents and incidents involving Royal Air Force aircraft|*]]
[[Category:Royal Air Force]]