<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person 
|name= Francis Ryan Smith
|image= 
|caption= 
|birth_date= 23 July 1896
|death_date= Unknown
|birth_place= [[Brisbane]], [[Queensland]], [[Australia]]
|death_place= 
|placeofburial= 
|placeofburial_label= 
|nickname= 
|allegiance= {{flag|Australia}}
|branch= [[First Australian Imperial Force|Australian Imperial Force]]<br/>[[Australian Flying Corps]]<br/>[[Royal Australian Air Force]]
|serviceyears= 1915 &ndash; 1919<br/>1941 &ndash; 1944
|rank= [[Squadron Leader]]
|unit= 
|commands= 
|battles= [[First World War]]
* [[Western Front (World War I)|Western Front]]
* [[Battle of Fromelles]]
[[Second World War]]
|awards= [[Military Cross]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
|relations=
|laterwork=
}}

'''Francis Ryan Smith''', [[Military Cross|MC]], [[Distinguished Flying Cross (United Kingdom)|DFC]] was an Australian [[flying ace]] of the [[First World War]], credited with 16 aerial victories.<ref name="theaerodrome.com">{{cite web|url=http://www.theaerodrome.com/aces/australi/smith3.php|title=Francis Ryan Smith|work=The Aerodrome|accessdate=12 January 2010}}</ref>

Francis Ryan Smith worked as a clerk before joining the Australian Imperial Force on 20 July 1915. He served with distinction in the [[31st Battalion, Royal Queensland Regiment]] and winning the Military Cross in 1916, for bravery under fire. He transferred to the [[Australian Flying Corps]] for training, then joined No. 2 Squadron AFC as a pilot on 28 February 1918.<ref name="southsearepublic.org">{{cite web|url=http://www.southsearepublic.org/2004_2002/people/aces/smithfrancis.html|archiveurl=https://web.archive.org/web/20120302003805/http://www.southsearepublic.org/2004_2002/people/aces/smithfrancis.html|title=Captain Francis R. Smith, Australian Flying Corps|work=Australian Flying Corps: A Complete History of the Australian Flying Corps|accessdate=12 January 2010|archivedate=2 March 2012}}</ref>

Piloting a [[RAF SE.5a]], he began his victory string by destroying a German [[Pfalz D.III]] fighter on 9 May 1918, and continued until he ended it with a quadruple victory on 14 October 1918, when he destroyed three [[Fokker D.VII]]s and sent another one down out of control. His final tally amounted to eight enemy fighters sent down out of control, seven others destroyed, and an enemy observation plane shot down out of control.<ref name="theaerodrome.com"/>

Along the way, Smith became a Flight Leader by mid September 1918; he also became his squadron's leading ace. Additionally, he became the squadron's final casualty, being shot down on 10 November 1918. Although downed behind enemy lines, he evaded capture by donning civilian clothing and covering 40 miles back to his squadron mess. He found his squadron-mates celebrating the Armistice ending the war.<ref name="southsearepublic.org"/>

He returned to Australia on 6 May 1919.<ref name="southsearepublic.org"/> By 18 June, he had been discharged.<ref>{{cite web|url=http://www.diggerhistory.info/pages-conflicts-periods/ww1/afc/2-sqn-afc.htm|title=No. 2 Squadron, Australian Flying Corps|work=Digger History|accessdate=12 January 2010}}</ref>

Francis Ryan Smith is known to have survived until at least 1951, when he is recorded as leasing out an auto service station in Willandra, Australia.<ref>{{cite web|url=http://www.ryde.nsw.gov.au/WEB/SITE/RESOURCES/DOCUMENTS/PDF/ConservationMngPlan/willandra_conserv_manag_plan_sec_1_2.pdf |title=Willandra Conservation Management Plan Draft |publisher=Ryde City Council |date=March 2007 |accessdate=12 January 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20070902050937/http://www.ryde.nsw.gov.au/WEB/SITE/RESOURCES/DOCUMENTS/PDF/ConservationMngPlan/willandra_conserv_manag_plan_sec_1_2.pdf |archivedate=September 2, 2007 }}</ref>

==Honors and awards==
'''Distinguished Flying Cross (DFC)'''

Lieut. (A./Capt.) Francis Ryan Smith, M.C. (Australian F.C.). (FRANCE)
   
This officer combines high individual enterprise and determination with exceptionally able leadership. These qualities were conspicuous on 14 October, when, leading a patrol of five machines, he saw a formation of twelve Fokker biplanes above him. Relying on the co-operation of another higher formation of Bristol machines, he, deliberately manoeuvred his formation into a disadvantageous position in order that our higher patrol might be able to attack the enemy while the latter's attention was concentrated upon destroying his, Lt. Smith's, formation. The stratagem was entirely successful, with the result that two enemy machines were destroyed and two others were believed to crash. The Fokkers were then reinforced by eight more machines, and in the ensuing combat Lt. Smith shot down one in flames, his patrol destroying two others. We suffered no casualties.<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200242.html|title=Honours|date=20 February 1919|work=Flight International|page=242|accessdate=12 January 2010}}</ref>

==Notes==
{{reflist}}

==References==
*{{cite book|first=Norman|last=Franks|title=SE 5/5a Aces of World War I|publisher=Osprey Publishing|location=Oxford|year=2007|isbn=9781846031809}}

{{DEFAULTSORT:Smith, Francis}}
[[Category:1896 births]]
[[Category:Australian Flying Corps officers]]
[[Category:Australian military personnel of World War I]]
[[Category:Royal Australian Air Force personnel of World War II]]
[[Category:Australian World War I flying aces]]
[[Category:People from Brisbane]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Recipients of the Military Cross]]
[[Category:Royal Australian Air Force officers]]
[[Category:Year of death unknown]]