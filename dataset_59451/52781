{{Distinguish|Royal Medical Society}}
{{Infobox Organization
| name         = Royal Society of Medicine
| image        = RSM logo.jpg
| caption      = The logo for the Royal Society of Medicine
| image_border =
| size         = 200 px
| motto        = ''Non est vivere sed valere vita est''<br>("Life is not being alive but being well" — [[Martial]])
| headquarters = [[London]]
| location     = United Kingdom
| membership   = 22,500
| language     = English
| leader_title = President
| leader_name  = Mr [[Babulal Sethia]]
| website      = {{URL|www.rsm.ac.uk}}
}}

'''The Royal Society of Medicine ''' ('''RSM''') is one of the major providers of accredited postgraduate medical education in the [[United Kingdom]]. Each year, the RSM organises over 400 academic and public events.<ref>{{cite web|url=https://www.rsm.ac.uk/events.aspx |title=RSM Events |publisher=Rsm.ac.uk |date= |accessdate=2013-12-03}}</ref> spanning 60 areas of special interest providing a multi-disciplinary forum for discussion and debate. Videos of many key lectures are also available online, increasing access to the Society’s education programme. The RSM is home to one of the largest medical libraries in Europe,<ref>{{cite web|url=https://www.rsm.ac.uk/library.aspx |title=RSM library |publisher=Rsm.ac.uk |date= |accessdate=2013-12-03}}</ref> with an extensive collection of books, journals, electronic journals and online medical databases. As well as providing medical education, the Society aims to promote an exchange of information and ideas on the science, practice and organisation of medicine, both within the health professions and with responsible and informed public opinion. The Society is not a policy-making body and does not issue guidelines or standards of care.

== History ==
The Society was established in 1805 as [[Medical and Chirurgical Society of London]],<ref>{{cite journal |url=http://pmj.bmj.com/content/81/951/45.full.pdf+html |title=The Royal Society of Medicine |first=P |last=Hunting |journal=[[Postgraduate Medical Journal]] |date=2005 |volume=81 |pages=45–48 |doi=10.1136/pgmj.2003.018424}}</ref> meeting in two rooms in barristers’ chambers at Gray’s Inn and then moving to Lincoln’s Inn Fields where it stayed for 25 years. In 1834 the Society moved to Berners Street and was granted a Royal Charter by King William IV.

In 1889 under the leadership of Sir John MacAlister,<ref>{{cite book|title=Centenary, 1805–1905, Royal Medical and Chirurgical Society of London|year=1906|page=186|url=https://books.google.com/books?id=VeQHAAAAIAAJ&pg=PA186}}</ref> a Building Committee chaired by [[Timothy Holmes]] supervised the move of the quarters of the Society from Berners Street to 20 Hanover Square. In 1905 an eleven-member committee headed by [[Sir Richard Powell, 1st Baronet|Sir Richard Douglas Powell]] organised the celebration of the Society's centenary.<ref>{{cite book|title=Centenary, 1805–1905, Royal Medical and Chirurgical Society of London|year=1906|pages=313–314|url=https://books.google.com/books?id=VeQHAAAAIAAJ&pg=PA313}}</ref> Two years later the Royal Medical and Chirurgical Society of London came together with seventeen specialist medical societies and, with a supplementary Royal Charter granted by Edward VII, became the Royal Society of Medicine.

In 1910 the Society acquired the site on the corner of Wimpole Street and Henrietta Place, which was opened by King George V and Queen Mary in May 1912.

==Governance==
The Council<ref>[https://www.rsm.ac.uk/about-us/governance/council.aspx "The RSM council"] Royal Society of Medicine Council</ref> is the governing body of the Society and Council members are the Society’s Trustees. A permanent team of Directors<ref>[https://www.rsm.ac.uk/about-us/governance/directors.aspx "RSM Directors"] Royal Society of Medicine Directors</ref> and their staff support the work of Council.
There are two Standing Committees. The Audit Committee reports directly to Council and is responsible for audit and risk management. The remit of the Academic Board is to provide academic initiatives and to consider changes and improvements to the organisation of meeting programmes for the Sections and the Society.
The Dean is responsible for Continuing Professional Development, Society Conferences, and the accreditation of Section meetings and e-learning programmes.
The RSM Council meets throughout the year. Only RSM members and fellows can access the Minutes of RSM Council meetings.

==Presidents==
{{main|List of Presidents of the Royal Society of Medicine}}

Recent Presidents of the society have been:

{{columns-list|3|
* 2014–present [[Babulal Sethia]]<ref>[https://www.rsm.ac.uk/about-us/media-information/2014-media-releases/royal-society-of-medicine-appoints-new-president.aspx "Royal Society of Medicine appoints new President"] Royal Society of Medicine Media Releases</ref>
* 2012–2014 Sir [[Michael Rawlins]]
* 2010–2012 [[Parveen Kumar]]
* 2008–2010 [[Robin C. N. Williamson]]
* 2006–2008 [[Ilora Finlay, Baroness Finlay of Llandaff]]
* 2004–2006 Sir [[John Lilleyman]]
* 2002–2004 Sir [[Barry Jackson (surgeon)|Barry Jackson]]
* 2000–2002 [[Deirdre Hine]]
}}

Previous Presidents of note of the former [[Royal Medical and Chirurgical Society]] of London were:
*[[Frederick William Pavy]] (1900)
*[[Sir James Paget]] (1875)
*[[Joseph Hodgson]] (1851)
*[[Thomas Addison]] (1849)
*[[Richard Bright (physician)|Richard Bright]] (1837)
*[[William Saunders (physician)|William Saunders]] (1805 (1st))

==Membership==

Fellowship of the RSM<ref>[https://www.rsm.ac.uk/about-us/joining-the-rsm.aspx "RSM Fellowship"] Royal Society of Medicine Fellowship</ref> is open to those who hold a UK recognised medical, dental or veterinary qualification, or a higher scientific qualification in a healthcare related field. Associate membership is open to those who do not qualify for Fellowship but who work within the healthcare sector or have an interest in healthcare issues. The Society also welcomes student members of medicine, dentistry and veterinary science as members plus other healthcare students. 
In addition there are up to one hundred Honorary Fellows, drawn from internationally distinguished members of the medical profession and branches of science and allied humanities, who are awarded this honour by Council. 
Members enjoy an extensive range of benefits including access to online resources, including videos of latest lectures, attendance at educational meetings and use of the Society’s Central London club facilities which includes member only accommodation.

Famous Honorary Fellows (of the Royal Medical and Chirurgical Society of London) have been:

*[[Charles Darwin]]
*[[Louis Pasteur]]
*[[Edward Jenner]]
*[[Sigmund Freud]]

==Journal of the Royal Society of Medicine==
The Journal of the Royal Society of Medicine<ref>[http://jrs.sagepub.com "JRSM"] JRSM</ref> is a scientific and educational publication featuring articles ranging from evidence-based reviews and original research papers to editorials and personal views. 

Edited by Dr [[Kamran Abbasi]], JRSM has been published continuously since 1809. JRSM has full editorial independence of the RSM and features well-argued debate and dissent on important clinical topics. Although UK-based, it publishes articles of interest and relevance to clinicians internationally.
JRSM Open<ref>[http://shr.sagepub.com/ "JRSM Open"] JRSM Open</ref> is a companion publication to the [[Journal of the Royal Society of Medicine]]. Previously known as JRSM Short Reports, JRSM Open is a successful peer-reviewed, open-access journal, meaning there are no subscription barriers for readers.
The aim of JRSM Open is to influence clinical practice and policy making across the whole range of medicine. JRSM Open has an international and multispecialty readership that includes primary care and public health professionals. It accepts articles of interest to any reader involved with improving patient care and publishes case reports, research papers and clinical reviews. A fee, to cover article processing and open access, is payable once an article is accepted for publication. The editor of JRSM Open is Dr Kamran Abbasi.

==Awards and Prizes==

The society's Gold Medal is awarded for outstanding contribution to medicine. Past recipients have included [[Wilfred Trotter]] (1938), Sir [[Alexander Fleming]] (1947), [[Lord Florey]] (1947), [[Sir Martin John Evans]] (2009) and [[Lord Walton of Detchant]] (2014).

The [[Edward Jenner Medal]] was originally established in 1896 by the Epidemiological Society of London (1850–1907) to commemorate the centenary of Edward Jenner’s discovery of a means of smallpox vaccination. It is awarded periodically by the RSM to individuals who have undertaken distinguished work in epidemiological research.

The society hosts the annual [[Ellison-Cliffe Lecture]] concerning the advancement of medicine, along with the associated award of a medal. Past presenters/recipients include Sir [[Walter Bodmer]], Lord [[George Porter]], Sir [[Colin Blakemore]] and [[Kevin Warwick]]. 

The RSM also has a number of awards and prizes for students and trainees.<ref>[https://www.rsm.ac.uk/prizes-awards.aspx "RSM prizes and awards"] RSM prizes and awards</ref> There are over 70 prizes available for trainees, worth a total of around £60,000. The annual Ellison-Cliffe Travelling Fellowship of £15,000 is open to Fellows of the Royal Society of Medicine working in the UK or Ireland who are of specialist registrar or lecturer grade or equivalent or who are consultants within 3 years of their first consultant appointment. The prize covers expenses for travel abroad in pursuit of further study, research or clinical training relevant to the applicant's current interests. The annual RSM Young Trainee of the Year Award brings together all of the trainee prize winners from the previous academic year. Representing the cream of the RSM’s trainee doctors, five finalists are invited to attend an awards evening to present their research to a panel of distinguished panel of academics and clinicians.

==Public information==
While the main function of the Royal Society of Medicine is to provide ongoing education to healthcare professionals and students, the Society offers a variety of services to the public. A series of public events, including lectures and topical debates, allows the RSM to provide a forum for informed debate amongst the public. There is also a range of meetings for school students, including an annual meeting for those considering a career in medicine, together with Christmas and Easter Lectures. 

==Library==
The RSM Library is open to members of the public, who can make use of its reference facilities for free. The Library represents one of the largest postgraduate biomedical collections in Europe and contains around 600,000 volumes. This includes [[William Harvey]]'s ''Exercitatio anatomica de motu cordis et sanguinisin animalbus''. The Library also features exhibitions. Due to its historical Library holdings, the Royal Society of Medicine is a member of [[The London Museums of Health & Medicine]] group.<ref>{{cite web|title=Medical Museums|url=http://medicalmuseums.org/|website=medicalmuseums.org|accessdate=26 August 2016}}</ref>

==Event facilities==

[[File:RoyalSocMedicine2011.jpg|thumb|The Royal Society of Medicine headquarters, 1 [[Wimpole Street]], [[London]], [[England]].]]

The Royal Society of Medicine has event facilities available for hire at 1 Wimpole Street - home of the RSM - and sister property [[Chandos House]]. Both are located just five minutes walk from Oxford Circus and Bond Street underground stations.

==References==
{{reflist|30em}}

==External links==
* {{Official website}}

{{Coord|51.5161|-0.1471|type:landmark_region:GB-WSM|display=title}}

{{DEFAULTSORT:Royal Society Of Medicine}}
[[Category:1805 establishments in the United Kingdom]]
[[Category:Buildings and structures in the City of Westminster]]
[[Category:Health in the City of Westminster]]
[[Category:Libraries in the City of Westminster]]
[[Category:Medical associations based in the United Kingdom]]
[[Category:Organisations based in the City of Westminster]]
[[Category:Organisations based in London with royal patronage]]
[[Category:Organizations established in 1805]]
[[Category:Medical museums in London]]