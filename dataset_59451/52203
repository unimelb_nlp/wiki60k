{{Infobox journal
| title  = Maal og Minne
| cover  = 
| editor = [[Lars S. Vikør]], [[Jon Gunnar Jørgensen]]
| language = Danish, English, German, Norwegian, Swedish
| discipline = [[Linguistics]]
| abbreviation = Maal Minne
| publisher = Novus forlag
| country = Norway
| history = 1909-present
| frequency = Biannual
| impact = 
| impact-year =
| website = http://web.novus.no/Novus_tidsskrifter/Maal_og_minne.html
| link2 =
| link2-name =
| ISSN = 0024-855X
| OCLC = 186357362
| LCCN = ca13000842
| CODEN = MAMIFJ
}}
'''''Maal og Minne''''' ("Language and Memory") is a Norwegian [[academic journal]] of [[linguistics]] established in 1909 by [[Magnus Olsen]]. It covers research on [[North Germanic languages|Scandinavian languages]],  focusing mainly on language history and [[philology]]. It is a "level 2" journal in the [[Norwegian Scientific Index]].<ref>[http://dbh.nsd.uib.no/kanaler/kanalDetalj.do?produktid=341002 Norwegian Social Sciences Data Services]</ref> The current [[editors-in-chief]] are [[Lars S. Vikør]] and [[Jon Gunnar Jørgensen]].

== Editors ==
The following persons are or have been editors of the journal:
* 1909&ndash;1950 [[Magnus Olsen]]
* 1951&ndash;1967 [[Trygve Knudsen]] and [[Ludvig Holm-Olsen]]
* 1968&ndash;1984 [[Ludvig Holm-Olsen]] and [[Einar Lundeby]]
* 1985&ndash;1993 [[Einar Lundeby]] and [[Bjarne Fidjestøl]]
* 1994 [[Einar Lundeby]]
* 1995 [[Einar Lundeby]] and [[Odd Einar Haugen]]
* 1996&ndash;2005 [[Kjell Ivar Vannebo]] and [[Odd Einar Haugen]]
* 2006&ndash;present [[Lars S. Vikør]] and [[Jon Gunnar Jørgensen]]

== References ==
{{reflist}}

== External links ==
* {{Official|http://web.novus.no/Novus_tidsskrifter/Maal_og_minne.html}}

[[Category:Linguistics journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1909]]
[[Category:Biannual journals]]