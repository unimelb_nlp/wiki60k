[[File:Anderson, goldstein and mcquirk 1983.jpg|thumb|right|Goldstein (m) with Delegate [[Curt Anderson]] (l) and Senator Harry McQuirk (r), Annapolis, 1983]]
'''Louis Lazerus Goldstein''' (March 14, 1913&nbsp;– July 3, 1998) served as [[Comptroller of Maryland|Comptroller]], or chief financial officer, of [[Maryland]] for ten terms from 1959 to 1998. A popular politician and lifelong Democrat, he was first elected to the [[Maryland House of Delegates]] in 1938 and served three terms in the [[Maryland Senate]] before winning election as Comptroller. He ran unsuccessfully for Governor in 1964.

==Early life==
Goldstein was born in [[Prince Frederick, Maryland]] in [[Calvert County, Maryland|Calvert County]]. His father Goodman Goldstein was a Jewish immigrant from [[Prussia]] who had settled in the rural area after he was assigned as a salesman to Calvert County by his first employer, a Baltimore retailer. Louis worked in his father's store in Prince Frederick until he left to attend [[Washington College]] in [[Chestertown, Maryland]] and later the [[University of Maryland]], where he received his law degree in 1938. He was elected that year to the [[Maryland House of Delegates]] as a Democrat from Calvert County. Goldstein enlisted in the [[United States Marine Corps]] at the age of 29 following the Pearl Harbor attack and served in the Pacific. Commissioned as an officer from enlisted rank he reached 1st lieutenant. Following the surrender of Japan he was a member of General [[Douglas MacArthur]]'s staff that investigated Japanese war crimes in the Philippines.<ref name="weil1">{{cite news|last1=Weil|first1=Martin|title=Maryland Comptroller Dies at 85|url=http://www.washingtonpost.com/wp-srv/politics/campaigns/junkie/links/goldstein.htm|access-date=March 5, 2017|newspaper=Washington Post|date=July 4, 1998}}</ref><ref name="janofsky1">{{cite news|last1=Janofsky|first1=Machael|title=Louis Goldstein, 85, Maryland Comptroller|url=http://www.nytimes.com/1998/07/07/us/louis-goldstein-85-maryland-comptroller.html|access-date=March 5, 2017|newspaper=New York Times|date=July 7, 1998}}</ref>

==Political career==
Returning to politics in 1946 Goldstein was elected  to the [[Maryland Senate]] for the first of three terms. In the Senate he was majority leader from 1951 to 1955 and President of the Senate from 1955 to 1958. In 1959 he was elected to the first of ten terms as [[Comptroller of Maryland]]. The politically powerful position entails membership on the Public Works Board with the governor and state state treasurer, granting final authority over most state contracts and purchases. Goldstein ran for Governor in 1964, losing in the Democratic primary eventual governor [[Joseph D. Tydings]].<ref name="weil1"/><ref name="janofsky1"/>

==Family==
Goldstein's father had significant landholdings in Calvert County, to which Louis added, eventually owning about {{convert|2000|acre|ha}}. Some of this land was sold in 1967 to [[Baltimore Gas and Electric]] for the [[Calvert Cliffs Nuclear Power Plant]] at above-market prices, prompting criticism.<ref name="weil1"/> Goldstein married lawyer Hazel Horton in 1948, with whom he practiced law. They had two daughters and a son. Hazel died in 1996.<ref name="janofsky1"/> Goldstein died at their Calvert County home, Oakland Park, on July 3, 1998 of an apparent heart attack.<ref name="weil1"/>

==Political legacy==
As a legislator in the [[Maryland General Assembly|General Assembly of Maryland]], he was known for his 11th-hour strong arming to get votes behind closed doors. He also owned land in every county in the State of Maryland in an effort to show his commitment to the entire state. He practiced law with his wife Hazel (1917–1996). The statue of Louis L. Goldstein,<ref name=JHCwebLLG>{{Cite web |url=http://www.jayhallcarpenter.com/publictext/goldstein.htm |title=Louis L. Goldstein |year=2004 |publisher=Jay Hall Carpenter |access-date=July 2, 2011 |dead-url=yes |archive-url=http://web.archive.org/web/20110723191600/http://www.jayhallcarpenter.com/publictext/goldstein.htm |archive-date=July 23, 2011 }}</ref> outside the Louis L. Goldstein Treasury Building in the state capital of [[Annapolis, Maryland|Annapolis]], was created by Jay Hall Carpenter<ref name=JHCwebArt>{{cite web |author=Donna M. Cedar-Southworth |title=Jay Hall Carpenter, A Glimpse at Grandeur |publisher= 
ChesapeakeHome |url=http://www.jayhallcarpenter.com/articles.htm  |access-date=April 5, 2008 }}</ref> and unveiled on April 3, 2002.<ref name=marylandgov>[http://www.dgs.maryland.gov/press/2002/040302.htm Governor Parris N. Glendening Leads Unveiling of Louis L. Goldstein Statue], Maryland Department of General Services internet website, April 2, 2002</ref>

Goldstein Hall at his alma mater Washington College is named for him. The "Goldstein Award" at the College's annual commencement awards the graduate with the greatest potential for success in public service. All of [[Maryland Route 2]]/[[Maryland Route 4|4]] in [[Calvert County, Maryland|Calvert County]] is named after Goldstein.

The Calvert County Democratic Party's annual dinner banquet is also named after Louis L. Goldstein. Goldstein deputy, Robert L. "Bobby" Swann was appointed Comptroller after Goldstein's death by then-governor [[Parris Glendening]]. Former four-term [[Mayor of Baltimore]] and two-term Governor [[William Donald Schaefer]] later ran for the office of Comptroller in November 1998. Goldstein had already announced he was running for another term before his death and would have almost certainly been re-elected even at age 85. Schaefer, tired of being out of public office, and still popular with a wide support among the electorate, won easily following Goldstein's death.<ref>{{Cite web |url=http://www.msa.md.gov/msa/speccol/sc3500/sc3520/001400/001489/html/msa01489.html |title=William Donald Schaefer |publisher=Maryland State Archives |date=April 19, 2011 |access-date=July 2, 2011 }}</ref>
Ironically, Schaefer and Goldstein sat on the Maryland Board of Public Works together when Goldstein was comptroller and Schaefer was governor. The two were not particularly close personally or professionally, although Goldstein was almost always gracious but tough at BPW meetings. Longtime Maryland Senate President Thomas V. (Mike) Miller, Jr., considers Goldstein one of the greatest politicians he has ever known. Goldstein rarely forgot a name or at least a face.

His Annapolis office was taken apart piece-by-piece after his death at the guidance of his longtime friend and deputy comptroller, Swann, and was replicated at the Jefferson Patterson Park located in [[St. Leonard, Maryland]].<ref>{{Cite web |url=http://www.jefpat.org/exhibits.html |title=Jefferson Patterson Park & Museum |work=jefpat.org |publisher=Maryland Department of Planning |access-date=July 2, 2011}}</ref>

==Quotes==
"God bless y'all real good."<ref name=marylandgov />

==References==
{{reflist}}

==External links==
{{Portal|Biography}}
*[http://www.mdarchives.state.md.us/msa/mdmanual/08conoff/former/html/msa01579.html Maryland Archives biography]
*{{Find a Grave|7508823}}

{{S-start}}
{{s-off}}
{{succession box | before = [[George Della]] | title = [[President of the Maryland State Senate]] | years = 1955–1958 | after = [[George Della]]}}
{{succession box | before = [[J. Millard Tawes]] | title = [[List of Comptrollers of Maryland|Comptroller of Maryland]] | years = 1959–1998 | after = [[Robert L. Swann (comptroller of Maryland)|Robert L. Swann]]}}
{{S-end}}

{{Authority control}}

{{DEFAULTSORT:Goldstein, Louis L.}}
[[Category:1913 births]]
[[Category:1998 deaths]]
[[Category:Comptrollers of Maryland]]
[[Category:People from Prince Frederick, Maryland]]
[[Category:Presidents of the Maryland State Senate]]
[[Category:United States Marines]]
[[Category:Washington College alumni]]
[[Category:Maryland Democrats]]
[[Category:American chief financial officers]]
[[Category:20th-century American politicians]]