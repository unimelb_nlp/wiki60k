{{Infobox journal
| title = Stanford Environmental Law Journal
| cover =
| formernames = Stanford Environmental Law Annual
| abbreviation = Stanford Environ. Law J.
| discipline = [[Environmental law]]
| editors = Mary Rock, Lauren Tarpey, Michelle Wu
| publisher = [[Stanford University]]
| history = 1978-present
| frequency = Triennially 
| website = http://journals.law.stanford.edu/stanford-environmental-law-journal-elj
| impact = 
| link1-name =
| link2 =
| ISSN = 0892-7138
| eISSN = 
| OCLC = 15244703
| LCCN = 88648722
| JSTOR = 
}}
The '''''Stanford Environmental Law Journal''''' (''[[Bluebook]]'' abbreviation: ''Stan. Envtl. L. J.'') is a student-run [[law review]] published at [[Stanford Law School]] that covers [[natural resources law]], [[environmental policy]], [[law and economics]], [[International law|international environmental law]], and other related disciplines.<ref>{{smallcaps|Stanford Environmental Law Journal}}, [http://journals.law.stanford.edu/stanford-environmental-law-journal-elj/ Homepage].</ref>

== Overview ==
The journal was established in 1978 as the ''Stanford Environmental Law Annual'' to "provide a forum for student papers in developing areas of environmental law."<ref>''Forward'', 1 {{smallcaps|Stan. Envtl. L. Ann.}} i (1978).</ref> After a three-year hiatus between 1983 and 1986, the journal resumed publication as the ''Stanford Environmental Law Journal''.<ref>''President's Page'', 6 {{smallcaps|Stan. Envtl. L. J.}} 1 (1987).</ref> In the 2016 [[Washington and Lee University]] Law Journal Rankings, the journal was the second-highest rated environmental, natural resources, and land use law journal by [[impact factor]].<ref>[http://lawlib.wlu.edu/LJ/index.aspx "Law Journals: Submission and Ranking, 2008-2015,"] Washington & Lee University (Accessed: September 25, 2016).</ref> Articles in the journal have been cited by many [[state supreme courts]]<ref>See, e.g., ''Peace v. Northwestern Nat'l Ins. Co.'', 228 Wis. 2d 106 (1999); ''Pyramid Lake Paiute Tribe of Indians v. Washoe County'', 112 Nev. 743 (1996); ''Surface Waters of the Yakima River Drainage Basin v. Yakima Reservation Irrigation Dist.'', 121 Wn. 2d 257 (1993).</ref> and [[United States Courts of Appeals]].<ref>See, e.g., ''NRDC v. EPA'', 529 F.3d 1077 (D.C. Cir. 2008); ''Second Generation Props., L.P. v. Town of Pelham'', 313 F.3d 620 (1st Cir. 2002); ''Mont. Wilderness Ass'n v. McAllister'', 666 F.3d 549 (9th Cir. 2011).</ref> Articles also appear in treatises written by ''[[American Law Reports]]''<ref>See, e.g., Ann K. Wooster, ''Designation of "Critical Habitat" Under Endangered Species Act'', 176 {{smallcaps|A.L.R. Fed.}} 405.</ref> and [[Westlaw]].<ref>See, e.g., Tracy Bateman Farrell, ''Conservation Easements'', 28 {{smallcaps|CA Jur Easements and Licenses in Real Property}} § 16.</ref>

== Abstracting and indexing ==
The journal is abstracted or indexed in [[EBSCO Information Services|EBSCO databases]], [[HeinOnline]], [[LexisNexis]], [[Westlaw]],<ref name = Finder>{{smallcaps|Washington and Lee University Law Library}}, [http://lawlib.wlu.edu/resolver.aspx?title=Stanford%20Environmental%20Law%20Journal&issn=0892-7138/ Journal Finder: Stanford Environmental Law Journal].</ref> and the [[University of Washington]]'s Current Index to Legal Periodicals.<ref>{{smallcaps|University of Washington Gallagher Law Library}}, [https://lib.law.washington.edu/cilp/period.html Periodicals Indexed in CLIP].</ref> Tables of contents are also available through Infotrieve and [[Ingenta]],<ref name=Finder/> and the journal posts some past issues on its website.<ref>{{smallcaps|Stanford Environmental Law Journal}}, [http://journals.law.stanford.edu/stanford-environmental-law-journal-elj/past-issues// Past Issues].</ref>

== See also ==
* [[List of law journals]]
* [[List of environmental law journals]]

== References ==
{{cbignore}}
{{reflist|30em}}

== External links ==
* {{Official website|http://journals.law.stanford.edu/stanford-environmental-law-journal-elj}}

{{DEFAULTSORT:Stanford Environmental Law Journal}}
[[Category:American law journals]]
[[Category:Publications established in 1978]]
[[Category:English-language journals]]
[[Category:Environmental law journals]]
[[Category:Law journals edited by students]]