{{Infobox journal
| title = Human Biology 
| cover = 
| editor = Brian M. Kemp and Ripan S. Malhi
| discipline =  [[Biological anthropology]] [[Human Evolution]] [[Anthropology]] [[Human Genetics]]
| abbreviation = Hum. Biol.
| publisher = [[Wayne State University Press]]
| country =
| frequency = Quarterly
| history = 1929-present
| openaccess = 
| license = 
| impact = 0.857
| impact-year = 2014
| website = http://digitalcommons.wayne.edu/humbiol
| link1 = http://www.bioone.org/loi/hbio
| link1-name = Online access at [[BioOne]]
| link2 = http://muse.jhu.edu/journals/human_biology/
| link2-name = Journal page at [[Project Muse]]
| JSTOR = 
| OCLC = 1752384
| LCCN = 31029123
| CODEN = HUBIAA
| ISSN = 0018-7143
| eISSN = 1534-6617
}}
'''''Human Biology''''' is a [[peer review]]ed [[scientific journal]], currently published by [[Wayne State University Press]]. The journal was established in 1929 by [[Raymond Pearl]] and is the official publication of the [[American Association of Anthropological Genetics]].<ref name=":0" /> The focus of the journal is [[human genetics]], covering topics from human population genetics, evolutionary and genetic demography and quantitative genetics. It also covers ancient DNA studies, evolutionary [[biological anthropology]], and research exploring biological diversity expressed in terms of adaptation. The journal also publishes interdisciplinary research linking biological and cultural diversity from evidence such sources as [[archaeology]], [[ethnography]] and [[cultural anthropology]] studies, and more. As of July 1, 2016, the journal is on Volume 87, Issue 3.<ref name=":0" /> The journal's current editors are Brian M. Kemp (Washington State University) and Ripan S. Malhi (University of Illinois Urbana-Champaign).<ref name=":1">{{Cite web|url=http://digitalcommons.wayne.edu/humbiol/editorialboard.html|title=Editorial Board {{!}} Human Biology: International Journal of Population Genetics and Anthropology {{!}} WSU Press {{!}} Wayne State University|website=digitalcommons.wayne.edu|access-date=2016-07-12}}</ref>

== Publication Details ==

=== Impact Factor and Other Measurements of Importance ===
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.857.<ref name=":0">{{Cite web|url=http://www.bioone.org/loi/hbio|title=Human Biology|last=|first=|date=|website=BioOne|publisher=BioOne|access-date=1 May 2016}}</ref> For the year of 2014, the [[Eigenfactor]], the rating of total importance of an academic journal, was 0.1.<ref name=":0" /> The journal has an Article Influence score of 0.5 for the year 2014.<ref name=":0" /> Article Influence is a ranking based on the Eigenfactor score, and is considered comparable to the Impact Factor score.

=== Online Publishing Details ===
Beginning with the February 2001 issue, ''Human Biology'' is available online through [[Project MUSE]]. Since 2006 Human Biology is also available through [[BioOne]].<ref name=":1" />

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://digitalcommons.wayne.edu/humbiol}}

[[Category:Medical genetics journals]]
[[Category:Anthropology journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1929]]
[[Category:English-language journals]]
[[Category:Academic journals published by university presses]]


{{genetics-journal-stub}}
{{anthropology-journal-stub}}