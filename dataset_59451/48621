{{Infobox medical person
| name              = Mark Humayun
| caption           = Mark S. Humayun, M.D., Ph.D
| image             =
| birth_date        = 
| birth_place       = 
| profession        = [[Ophthalmologist]], [[Engineer]], [[Scientist]] and [[Inventor]]
| work_institutions = [[USC Eye Institute]]
| known_for         = Co-inventing the Argus Series retina implants
}}

'''Mark S Humayun''',is an  [[ophthalmologist]], [[engineer]], [[scientist]] and [[inventor]] – the only ophthalmologist ever to be elected a member of both U.S. National Academies of Medicine and Engineering.<ref>{{cite web|last1=Lewit|first1=Meghan|title=Humayun Elected to Institute of Medicine|url=http://keck.usc.edu/Education/Academic_Department_and_Divisions/Department_of_Ophthalmology/News/Detail/archive__departments__ophthalmology__humayun_elected_to_institute_of_medicine|website=Keck School of Medicine of USC|accessdate=5 February 2016|date=23 October 2009}}</ref><ref>{{cite web|title=Keck School Faculty Honored by National Academy of Engineering|url=http://keck.usc.edu/Education/Academic_Department_and_Divisions/Department_of_Ophthalmology/News/Detail/2011__spring__keck_school_faculty_honored_by_national_academy_of_engineering|website=Keck School of Medicine of USC|accessdate=5 February 2016|date=16 February 2011}}</ref> He is a university professor with joint appointments at the Keck School of Medicine of USC and the USC Viterbi School of Engineering.<ref>{{cite web|title=Biomed Miracle Workers|url=http://tfm.usc.edu/autumn-2012/biomed-miracle-workers|website=USC Trojan Family|accessdate=5 February 2016}}</ref>

Humayun was named a recipient of the [[National Medal of Technology and Innovation]] in 2015 and received the award from U.S. President Barack Obama in 2016.<ref>http://nationalmedals.org/laureates/mark-s-humayan</ref> The award recognizes "those who have made lasting contributions to America’s competitiveness and quality of life and helped strengthen the Nation’s technological workforce.<ref>{{cite web|title=President Obama to Honor Nation’s Leading Scientists and Innovators|url=https://www.whitehouse.gov/the-press-office/2015/12/22/president-obama-honor-nations-leading-scientists-and-innovators|website=The White House (whitehouse.gov)|accessdate=5 February 2016|date=22 December 2015}}</ref>"
Humayun co-invented the Argus Series retina implants, which are manufactured by Second Sight, and are intended to restore sight to the blind.<ref>{{cite web|title=Humayun named first director of USC Eye Institute|url=http://www.keckmedicine.org/humayun-named-first-director-of-usc-eye-institute/|website=Keck Medicine of USC|accessdate=5 February 2016|date=14 November 2013}}</ref> The Argus Series implants were named by Time Magazine among the top 10 inventions of 2013.<ref>{{cite web|title=The 25 Best Inventions of the Year 2013|url=http://techland.time.com/2013/11/14/the-25-best-inventions-of-the-year-2013/slide/the-argus-ii/|website=TIME Magazine|accessdate=5 February 2016|date=13 November 2013}}</ref>

He has more than 100 patents and patent applications,<ref>{{cite web|title=Patents by Inventor Mark Humayun|url=http://patents.justia.com/inventor/mark-humayun|website=JUSTIA Patents|accessdate=5 February 2016}}</ref> and was nominated by R&D Magazine as Innovator of the Year in 2005.<ref>{{cite web|title=USC researcher Mark Humayun wins 2005 R&D Innovator of the Year Award|url=http://bmes-erc.usc.edu/brl/in-the-news/news-oct20_2005.htm|website=USC  Bioelectronics Research Lab|accessdate=5 February 2016|date=20 October 2005}}</ref><ref>{{cite web|last1=Derra|first1=Skip|title=Bringing Sight to the Blind|url=http://www.rdmag.com/articles/2005/08/bringing-sight-blind|website=R&D Magazine|accessdate=5 February 2016|date=12 August 2005}}</ref>
He was recently nominated to the National Academy of Inventors , the highest professional distinction accorded to academic inventors. 

Humayun was named director of the USC Institute of Biomedical Therapeutics (IBT) in 2012,<ref>{{cite web|title=Biomed Miracle Workers|url=http://tfm.usc.edu/autumn-2012/biomed-miracle-workers|website=USC Trojan Family|accessdate=5 February 2016}}</ref> director of the National Science Foundation BioMimetic MicroElectronic Systems Engineering Research Center,<ref>{{cite web|title=An Engineering Research Center for Biomimetic Microelectronic Systems|url=http://www.nsf.gov/awardsearch/showAward?AWD_ID=0310723|website=National Science Foundation|accessdate=5 February 2016|date=24 September 2003}}</ref> and director of the Department of Energy Artificial Retina Project.<ref>{{cite web|title=Overview of the Artificial Retina Project|url=http://artificialretina.energy.gov/about.shtml|website=Artificial Retina Project|accessdate=5 February 2016}}</ref> He was also inaugural director of the USC Eye Institute and interim chair of the USC Department of Ophthalmology.<ref>{{cite web|title=Humayun named first director of USC Eye Institute|url=http://www.keckmedicine.org/humayun-named-first-director-of-usc-eye-institute/|website=Keck Medicine of USC|accessdate=5 February 2016|date=14 November 2013}}</ref>

==Education & Career==

Dr Humayun received his B.S. from Georgetown University in 1984, his M.D. from Duke University in 1989, and his Ph.D. from the University of North Carolina, Chapel Hill in 1994.<ref>{{cite web|title=Mark S. Humayun, MD, PhD|url=http://www.keckmedicine.org/doctor/mark-s-humayun/|website=Keck Medicine of USC|accessdate=5 February 2016}}</ref> He completed his ophthalmology residency at Duke Eye Center and fellowships in both vitreoretinal and retinovascular surgery at the Wilmer Ophthalmological Institute at Johns Hopkins Hospital. He stayed on as faculty at Johns Hopkins where he rose to the rank of associate professor before moving to [[University of Southern California|USC]] in 2001.

===Tenure at USC===

Humayun is a clinician-researcher at the University of Southern California (USC) Eye Institute,<ref>{{cite web|title=About USC Eye Institute|url=http://eye.usc.edu/|website=USC Eye Institute|accessdate=5 February 2016}}</ref> and a professor of ophthalmology and biomedical engineering for the [[Keck School of Medicine of USC]] and USC Viterbi School of Engineering. He holds the Cornelius J. Pings Chair in Biomedical Sciences. He was named Professor of Ophthalmology, Biomedical Engineering, Cell and Neurobiology in 2001. He served as Interim Chair, Department of Ophthalmology, in 2013. He was named Inaugural Director of the USC Eye Institute in 2013. He was named Director of the USC Sensory Science Institute in 2013.

===California Institute of Technology===

Humayun has served as a Visiting Associate in Medical Engineering at the California Institute of Technology since 2014.<ref>{{cite web|title=Update on Argus II Retinal Prosthesis Study|url=http://www.mede.caltech.edu/seminar_series/humayun|website=Caltech Division of Engineering & Applied Science|accessdate=5 February 2016|date=13 March 2014}}</ref>

===Boards of Directors===

He serves on the Board of Directors for the American Society of Retina Specialists,<ref>{{cite web|title=ASRS Board of Directors|url=https://www.asrs.org/about/leadership|website=American Society of Retina Specialists (ASRS)|accessdate=5 February 2016}}</ref> and the Board of Directors of Replenish, Inc.<ref>{{cite web|title=Leadership Team|url=http://www.replenishinc.com/leadership-team|website=Replenish, Inc.|accessdate=5 February 2016}}</ref>

==Research==

Humayun’s research projects<ref>{{cite web|title=Mark S. Humayun|url=http://www.keckmedicine.org/doctor/mark-s-humayun/|website=Keck Medicine of USC|accessdate=6 February 2016}}</ref> focus on the treatment of debilitating eye diseases through advanced engineering.

===Argus II retinal prosthesis===

Humayun co-invented the Argus II retinal prosthesis,<ref>{{cite web|title=Argus II Retinal Prosthesis System - H110002|url=http://www.fda.gov/medicaldevices/productsandmedicalprocedures/deviceapprovalsandclearances/recently-approveddevices/ucm343162.htm|website=U.S. Food and Drug Administration|accessdate=6 February 2016|date=13 February 2013}}</ref> a retinal implant designed to help patients with genetic retinitis pigmentosa.<ref>{{cite web|title=Seeing is Believing: USC Eye Institute's retinal prosthesis gives blind woman the gift of sight|url=https://www.youtube.com/watch?v=wFUwJZ2aL6I|website=YouTube|accessdate=6 February 2016|date=29 August 2014}}</ref><ref>{{cite web|last1=Gross|first1=Rachel|title=A Bionic Eye That Restores Sight|url=https://www.theatlantic.com/health/archive/2014/08/a-bionic-eye-that-restores-sight/378628/|website=The Atlantic|accessdate=6 February 2016|date=31 August 2014}}</ref> More than 30 clinical trial participants in Argus II trial launched in 2007 at sites in the U.S. and Europe. It was approved by the FDA in February 2013.<ref>{{cite web|title=Argus II Retinal Prosthesis System - H110002|url=http://www.fda.gov/MedicalDevices/ProductsandMedicalProcedures/DeviceApprovalsandClearances/Recently-ApprovedDevices/ucm343162.htm|website=U.S. Food and Drug Administration|accessdate=6 February 2016|date=13 February 2013}}</ref> The first USC Eye Institute patient received the implant post-FDA approval in June 2014,<ref>{{cite web|title=USC Eye Institute ophthalmologists implant first FDA-approved Argus II retinal prosthesis in western United States|url=http://www.keckmedicine.org/usc-eye-institute-ophthalmologists-implant-first-fda-approved-argus-ii-retinal-prosthesis-in-western-united-states/|website=Keck Medicine of USC|accessdate=6 February 2016|date=27 August 2014}}</ref> and saw light one week following activation of device.

===Stem cell research===

Humayun is one of two principal investigators working with USC Eye Institute researchers to study how to replace damaged retinal pigment epithelial (RPE) cells with stem cells to restore sight,<ref>{{cite web|last1=Lytal|first1=Cristy|title=Mark Humayun and David Hinton look to stem cells to bring sight to the blind|url=https://stemcell.usc.edu/2014/07/03/mark-humayun-and-david-hinton-look-to-stem-cells-to-bring-sight-to-the-blind/|website=USC Stem Cell|accessdate=6 February 2016|date=3 July 2014}}</ref> a potential cure for age-related macular degeneration, the leading cause of permanent impairment of reading and fine or close-up vision among people aged 65 years and older.<ref>{{cite web|title=Common Eye Disorders|url=http://www.cdc.gov/visionhealth/basic_information/eye_disorders.htm|website=Centers for Disease Control and Prevention|accessdate=6 February 2016}}</ref> This research is funded by a grant from California Institute for Regenerative Medicine.

==Personal life==

Humayun saw his own grandmother lose her vision while in medical school, which motivated him to switch his medical specialty to ophthalmology and specifically innovative research.<ref>{{cite web|last1=Gross|first1=Rachel|title=A Bionic Eye That Restores Sight|url=https://www.theatlantic.com/health/archive/2014/08/a-bionic-eye-that-restores-sight/378628/|website=The Atlantic|accessdate=6 February 2016|date=31 August 2014}}</ref>

Humayun's paternal grandfather was [[Lt. Col.]] Dr Ilahi Bakhsh, the personal physician of [[Muhammad Ali Jinnah]], the founder of [[Pakistan]] (one of his ancestors took the same role for the ruler of [[Punjab]], [[Ranjit Singh|Maharaja Ranjit Singh]]), while his maternal grandfather was [[Muhammad Aslam Khan Khattak|Mohammad Aslam Khan Khattak]], also involved in the [[Pakistan Movement]], and who served as [[Governor of Khyber Pakhtunkhwa|Governor]] of the [[Khyber Pakhtunkhwa|North-West Frontier Province]] (1973-1974) under [[Zulfikar Ali Bhutto]].<ref>{{cite web|last1=Niaz|first1=Anjum|title=The man who invented the ‘bionic eye’|url=http://www.dawn.com/news/1242055|website=Dawn News|accessdate=16 December 2016|date=28 February 2016}}</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Humayun, Mark}}
[[Category:Living people]]
[[Category:American inventors]]
[[Category:Ophthalmologists]]
[[Category:Year of birth missing (living people)]]
[[Category:American physicians of Pakistani descent]]
[[Category:American people of Pashtun descent]]