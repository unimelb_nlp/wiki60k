{{Italic title}}
{{Infobox journal
| title = Structural and Multidisciplinary Optimization
| cover = [[File:Structural and Multidisciplinary Optimization.jpg]]
| editor = Raphael Haftka
| discipline = [[Engineering]]
| abbreviation = 
| publisher = [[Springer Science+Business Media]]
| country =
| history = 1989-present
| frequency = Bimonthly
| openaccess = 
| impact = 2.208
| impact-year = 2015
| website = http://www.springer.com/journal/00158/ 
| link1 = http://www.springerlink.com/content/1615-1488
| link1-name = Online access
| JSTOR = 
| OCLC = 44437432
| LCCN = 91643999
| CODEN = SOPTEQ
| ISSN = 1615-147X
| eISSN = 1615-1488
}}
'''''Structural and Multidisciplinary Optimization''''' is a [[peer review|peer-reviewed]] [[scientific journal]] published by [[Springer Science+Business Media]]. It is the official journal of the [[International Society of Structural and Multidisciplinary Optimization]]. It covers all aspects of designing optimal structures in stressed systems as well as multidisciplinary optimization techniques when one of the disciplines deals with structures (stressed solids) or fluids. The journal's scope ranges from the mathematical foundations of the field to [[algorithm]] and software development with benchmark studies to practical applications and case studies in structural, aero-space, mechanical, civil, chemical, and naval engineering. Closely related fields such as [[computer-aided design]] and manufacturing, reliability analysis, [[artificial intelligence]], system identification and modeling, inverse processes, computer simulation, and active control of structures are covered when the topic is relevant to optimization. The current [[editor-in-chief]] since 2015 is Raphael T. Haftka ([[University of Florida]])<ref name=WoS>{{cite book |year=2012 |chapter=Structural and Multidisciplinary Optimization |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref> since the passage of the founding editor George I. N. Rozvany ([[Budapest University of Technology and Economics]]).<ref name="special_issue">{{cite journal|last1=Zhou|first1=Ming|last2=Allaire|first2=Gregoire|last3=Cheng|first3=Gengdong|last4=Du|first4=Jianbin|last5=Gilbert|first5=Matthew|last6=Guo|first6=Xu|last7=Guest|first7=James|last8=Haftka|first8=Raphael|last9=Kim|first9=Alicia|last10=Lewinski|first10=Thomas|last11=Maute|first11=Kurt|last12=Norato|first12=Julian|last13=Olhoff|first13=Niels|last14=Paulino|first14=Glaucio H|last15=Sokol|first15=Thomasz|last16=Wang|first16=Michael|last17=Yang|first17=Ren-Jye|last18=Youn|first18=Byeng Dong|title=Special issue dedicated to Founding Editor George Rozvany|journal=Structural and Multidisciplinary Optimization|date=11 October 2016|volume=54|issue=2016|page=1107-1111|doi=10.1007/s00158-016-1591-2|url=https://paulino.ce.gatech.edu/journal_papers/2016/SMO_16_RozvanyEditorial.pdf|accessdate=6 April 2017}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in: [[Academic OneFile]], [[Academic Search]], [[Compendex]], [[Current Contents]]/Engineering, ''[[Mathematical Reviews]]'', [[Science Citation Index Expanded]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 1.488.<ref name="WoS" />

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.springer.com/00158}}

{{DEFAULTSORT:Structural And Multidisciplinary Optimization}}
[[Category:English-language journals]]
[[Category:Engineering journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1989]]