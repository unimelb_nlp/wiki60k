<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= A2N
 |image= 90siki-kansen.jpg
 |caption= A2N1
}}{{Infobox Aircraft Type
 |type= [[Aircraft carrier|Carrier-borne]] [[Fighter aircraft|fighter]]
 |manufacturer= [[Nakajima Aircraft Company]]
 |designer= Takao Yoshida
 |first flight= 1929
 |introduced= 1932
 |retired=
 |status=
 |primary user= [[Imperial Japanese Navy]]
 |more users=
 |produced= 1932-1936 (A2N)
 |number built= approx 100 (A2N) + 66 (A3N)
 |unit cost=
 |variants with their own articles=
}}
|}
The '''Nakajima A2N''' or '''Navy Type 90 Carrier-based fighter''' was a [[Japan]]ese [[Aircraft carrier|carrier]]-borne [[Fighter aircraft|fighter]] of the 1930s.  It was single-engine, [[biplane]] of mixed construction, with a fixed, [[Conventional landing gear|tailwheel undercarriage]].

==Design and development==
The A2N was originally developed as a private venture by [[Nakajima Aircraft Company|Nakajima]] for the [[Imperial Japanese Navy]]. It was based loosely on the [[Boeing F2B|Boeing Model 69]] and [[Boeing P-12|Boeing Model 100]], examples of both having been imported in 1928 and 1929 respectively. Takao Yoshida led the design team. Two [[prototype]]s, designated 'Navy Type 90 Carrier-based fighter' in anticipation of Navy acceptance were ready by December 1929.<ref>{{Harvnb|Mikesh and Abe|1990|p=225.}}</ref> Powered by [[Bristol Jupiter]] VI engines, these were rejected, not being regarded as offering a significant improvement over the [[Nakajima A1N]].<ref name="MikeshAbe1990p226">{{Harvnb|Mikesh and Abe|1990|p=226.}}</ref>

Jingo Kurihara carried out a partial redesign and another prototype, the A2N1, powered by a 432&nbsp;kW (580&nbsp;hp) [[Nakajima Kotobuki]] 2, was completed in May 1931. The type was adopted by the Navy in April 1932.<ref name="MikeshAbe1990p226"/> In 1932, [[Minoru Genda]] formed a flight demonstration team known as "Genda's Flying Circus" to promote naval aviation and flew this type.

A two-seat [[Trainer (aircraft)|trainer]] was later developed from the Navy Type 90 Carrier-based fighter as the '''A3N1''' and 66 of these were built between 1936 and 1939.<ref name="MikeshAbe1990p226"/>

==Operational history==
The Navy Type 90 Carrier-based fighter flew from the {{Ship|Japanese aircraft carrier|Hōshō||2}}, {{Ship|Japanese aircraft carrier|Kaga||2}} and {{Ship|Japanese aircraft carrier|Ryūjō||2}}.

==Variants==
[[File:Nakajima A2N3-1.jpg|thumb|250px|A2N3-1]]
;A2N1:('''Navy Type 90-I Carrier-based fighter''') - Guns located in both sides of the nose, but few produced.
;A2N2:('''Navy Type 90-II Carrier-based fighter''') - Guns transferred to the upper surface of the nose, the fuel tanks mounted on the fuselage sides.
;A2N3:('''Navy Type 90-III Carrier-based fighter''') - principal production variant. 5° of [[dihedral (aircraft)|dihedral]] on upper mainplane.
;A3N3-1:two-seat trainer

==Specifications (A2N1)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?= prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. To add a new line, end the old one with a right parenthesis ")" and start a new, fully formatted line beginning with * 
-->
|ref= ''Japanese Aircraft, 1910-1941'';<ref name="MikeshAbe1990p226"/> ''The Complete Book of Fighters''<ref name="complete fighters p423">{{Harvnb|Green and Swanborough|1994|p=423.}}</ref> 
|crew= 1
|capacity=
|length main= 6.18 m
|length alt= 20 ft 3⅜ in
|span main= 9.37 m
|span alt= 30 ft 9 in
|height main= 3.20 m
|height alt= 9 ft 11 in
|area main= 19.74 m²
|area alt= 212.5 ft²
|airfoil=
|empty weight main= 1,045 kg
|empty weight alt= 2,304 lb
|loaded weight main= 1,550 kg
|loaded weight alt= 3,417 lb
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)= [[Nakajima Kotobuki]] 2 
|type of prop= nine cylinder air-cooled [[radial engine]]
|number of props= 1
|power main= 433 kW
|power alt= 580 hp
|power original=
|max speed main= 293 km/h
|max speed alt= 158 knots, 182 mph
|max speed more=at 3,000 m (9,845 ft)
|cruise speed main= 166 km/h
|cruise speed alt= 90 knots, 103.6 mph 
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 501 km
|range alt= 270 [[nautical mile|nmi]], 311 mi
|ceiling main= 9,000 m
|ceiling alt= 29,500 ft
|climb rate main= 
|climb rate alt= 
|loading main= 78.5 kg/m²
|loading alt= 16.1 lb/ft²
|thrust/weight=
|power/mass main= 
|power/mass alt= 
|more performance=* '''Endurance:''' 3 hours
* '''Climb to 3,000 m (9,845 ft):''' 5.75 min
|guns= 2× 7.7 mm (0.303 in) [[Type 97 machine gun]]s
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
* [[Boeing F2B]]
* [[Boeing F4B]]
|similar aircraft=<!-- similar or comparable aircraft -->
* [[Boeing F2B]]
* [[Boeing P-12]]
* [[Hawker Nimrod]]
|lists=<!-- related lists -->
* [[List of aircraft of the Japanese Navy]]
* [[List of fighter aircraft]]
|see also=<!-- other relevant information -->
}}

==References==
{{commons category|Nakajima A2N}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* {{cite book |last= Green|first= William |author2=Gordon Swanborough |title= The Complete Book of Fighters |edition= |year= 1994|publisher= Smithmark|location=New York |isbn= 0-8317-3939-8}}
* {{cite book |last= Mikesh|first= Robert C.|author2=Shorzoe Abe |title= Japanese Aircraft, 1910-1941|year= 1990|publisher= Putnam Aeronautical Books|location=London |isbn= 0-85177-840-2}}
{{refend}}
<!-- ==External links== -->

{{Nakajima aircraft}}
{{Japanese Navy Carrier Fighters}}

[[Category:Japanese fighter aircraft 1920–1929|A02N, Nakajima]]
[[Category:Propeller aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:Nakajima aircraft|A02N]]