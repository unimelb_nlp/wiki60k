{{Infobox journal
| title         = Blackbird
| cover         = [[Image:Bbird.png|180px]]
| editor        = 
| discipline    = [[literary journal]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = [[Virginia Commonwealth University]] and New Virginia Review
| country       = [[United States]]
| frequency     = Biannual
| history       = 2002-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.blackbird.vcu.edu
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 1540-3068
| eISSN         = 
| boxwidth      = 
}}
'''''Blackbird''''' is an [[Electronic journal|online journal]] of literature and the arts based in the [[United States]] that posts two issues a year, May 1 and November 1.  During the six-month run of an issue, additional content appears as "featured" content.  Previous issues are archived online in their entirety.<ref name=Georgetown>{{cite web|url=http://library.georgetown.edu/newjour/b/msg02983.html |title=Blackbird: an Online Journal of Literature and the Arts |publisher=Library.georgetown.edu |date=2004-12-29 |accessdate=2012-01-03}}</ref>

== Background ==
''Blackbird'' publishes fiction, poetry, plays, interviews, reviews, and art by both new and established writers and artists.  The journal frequently includes streaming audio and video content, including readings, interviews, and art lectures.<ref name=Georgetown/>  Each fall issue forefronts work by, and about, the late [[Larry Levis]].<ref>{{cite web|url=http://www.blackbird.vcu.edu/v7n2/features/levis_remembered/intro.htm |title=Larry Levis Remembered Reading Loop Introduction, Blackbird |publisher=Blackbird.vcu.edu |date= |accessdate=2012-01-03}}</ref><ref>{{cite web|url=http://www.poets.org/poet.php/prmPID/385 |title=Larry Levis- Poets.org - Poetry, Poems, Bios & More |publisher=Poets.org |date=1946-09-30 |accessdate=2012-01-03}}</ref>  The journal's reading period for poetry and fiction closes between April 15 and September 15.  Unsolicited reviews, plays, and art work are not considered.<ref>{{cite web|url=http://www.blackbird.vcu.edu/v7n2/submissions.htm |title=Blackbird Submissions Guidelines, v7n2 |publisher=Blackbird.vcu.edu |date= |accessdate=2012-01-03}}</ref>

== Publisher ==
''Blackbird'' is published jointly by the Creative Writing Program of the Department of English at [[Virginia Commonwealth University]] in partnership with New Virginia Review, Inc., a [[nonprofit]] literary arts organization based in [[Richmond, Virginia]].<ref name=Georgetown/> Its founding editors included Gregory Donovan, Mary Flinn, William Tester, M.A. Keller and Jeff Lodge. The journal is staffed by employees of the two sponsoring organizations as well as by interns and volunteers.<ref>{{cite web|url=http://www.has.vcu.edu/eng/resources/blackbird.htm |title=Blackbird, VCU Department of English |publisher=Has.vcu.edu |date=2010-10-11 |accessdate=2012-01-03}}</ref>

== Recognition ==
''Blackbird'' was one of the first literary journals to be included in the [[LOCKSS]] international archive.<ref>{{cite web|url=http://www.lockss.org/lockss/Content_Releases_Archive |title=Archived copy |accessdate=2009-04-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20090124042913/http://lockss.org:80/lockss/Content_Releases_Archive |archivedate=2009-01-24 |df= }}</ref>

''Blackbird'' received AP Wire coverage for its publication of a previously unpublished sonnet of [[Sylvia Plath]] in their Fall 2006 issue.  Entitled ''Ennui,'' the poem was composed during Plath's early years at [[Smith College]].<ref>{{cite news|author=Associated Press |url=https://www.theguardian.com/books/2006/oct/31/news |title=Unpublished Plath sonnet goes online tomorrow &#124; Books &#124; guardian.co.uk |publisher=Guardian |date= 2006-10-31|accessdate=2012-01-03 |location=London}}</ref><ref>{{cite news|url=http://www.washingtonpost.com/wp-dyn/content/article/2006/10/31/AR2006103100411.html |title=Student Finds Unpublished Plath Poem |publisher=Washingtonpost.com |date=2006-10-31 |accessdate=2012-01-03}}</ref>

Ron Antonucci, in the ''[[Library Journal]]'', describes ''Blackbird'' it as "one of the most successful, well-assembled online literary magazines available... it is graphically attractive and has attracted writers of stature such as . . . Reginald Shepherd and [[Gerald Stern]]."<ref>{{cite web|url=http://www.libraryjournal.com/article/CA405420.html |title=Collection Development "Poetry": A Rhyme a Day |publisher=Libraryjournal.com |date= |accessdate=2012-01-03}}</ref>

''Blackbird'' was named as the "best online publication" of 2007 by ''storySouth''. The same year, ''Blackbird'' was awarded the [[Million Writers Award]] by ''storySouth'' by having seven stories selected as "notable stories of the year."<ref>{{cite web|url=http://www.storysouth.com/millionwriters/2007notablestories.html |title=storySouth / notable short stories of 2006 |publisher=Storysouth.com |date= |accessdate=2012-01-03}}</ref>

== References ==
{{Reflist|2}}

== External links ==
* [http://www.blackbird.vcu.edu/ ''Blackbird'']

{{Virginia Commonwealth University}}

{{DEFAULTSORT:Blackbird (online journal)}}
[[Category:American online magazines]]
[[Category:Art websites]]
[[Category:Literature websites]]
[[Category:American literary magazines]]
[[Category:Magazines established in 2002]]
[[Category:Biannual magazines]]
[[Category:Magazines published in Virginia]]