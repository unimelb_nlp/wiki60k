
{{Infobox Motorcycle
|name             = Honda Interceptor VF750F
|image            = [[File:1983 Honda Interceptor VF750F.jpg|frameless|upright=1.25]]
|caption          = 1983 Honda Interceptor VF750F
|aka              = 
|manufacturer     = [[Honda]]
|parent_company   = 
|production       = 1983–1985
|assembly         =
|predecessor      = [[Honda Magna|Honda Magna VF750C]] and [[Honda Sabre V4|Sabre VF750S]]
|successor        = 
|class            = 
|engine           = {{convert|748|cc|abbr=on}} [[liquid-cooled engine|liquid-cooled]] [[Four-stroke engine|four-stroke]], 90° [[V4 engine|V]] [[Multi-cylinder engine#Four-cylinder engines|4]], [[OHC|DOHC]], 4 valves per cylinder
|bore_stroke      = {{convert|70|x|48.6|mm|abbr=on}}
|compression      = 10.5:1
|top_speed        = {{convert|138|mph|abbr=on}}<ref name="Smith first sport bike" /> 
|power            = {{convert|86|hp|abbr=on}} @ 10000 rpm<ref name="Smith first sport bike">{{cite journal|last=Smith|first=Robert|title=The First Sport Bike? The 1983 Honda VF750F Interceptor|journal=Motorcycle Classics|date=May–June  2012|volume=7|issue=5|pages=16–21|url=http://www.motorcycleclassics.com/classic-japanese-motorcycles/honda-vf750f-interceptor-zm0z12mjzbea.aspx|accessdate=21 December 2012}}</ref> 
|torque           = 46.3 ft.lb. @ 7500 rpm<ref name="Smith first sport bike" /> 
|ignition         = 
|transmission     = 5-speed [[Manual transmission|manual]], [[Roller chain|chain]] final drive
|frame            = 
|suspension       = 
|brakes           = [[Disc brakes|Disc]]
|tires            = 
|rake_trail       = 
|wheelbase        = 
|length           = 
|width            = 
|height           = 
|seat_height      = 
|dry_weight       = {{convert|249|kg|abbr=on}}<ref name="Smith first sport bike" /> 
|wet_weight       = 
|fuel_capacity    = {{convert|23|l|abbr=on}}
|oil_capacity     = 
|fuel_consumption = 37.3 mpg<ref name="Smith first sport bike" /> 
|turning_radius   = 
|related          = 
|sp               =  
}}

The '''Honda VF750F''' was a street bike designed by [[Honda]] from 1983-1985. It had an {{convert|86|hp|abbr=on}}, [[Liquid-cooled engine|liquid-cooled]], [[V4 engine]] which sported dual [[OHC|overhead cam]]s (DOHC). The V4's were started a year before with the 1982 [[Honda Magna]] VF750C and Sabre VF750S<ref name="frank honda">{{cite book|last=Frank|first=Aaron|title=Honda Motorcycles|year=2003|publisher=Motorbooks International|isbn=0-7603-1077-7|pages=145|url=https://books.google.com/books?id=CSxTaoGagKoC&pg=RA1-PA745&dq=Honda+Interceptor+VF750&hl=en&sa=X&ei=hHXYUILlBebC2gXg14GABw&ved=0CDEQ6AEwAA#v=onepage&q=Honda%20Interceptor%20VF750&f=false}}</ref>  but were adapted for the VF750F in 1983 by reducing the six speed transmission to a five speed because of the change from shaft drive to chain. This reduced the available space in the transmission thus changing to a five speed.<ref name="Smith first sport bike" />

Because of new AMA super bike class regulations it required that four-cylinder bikes be downsized from 1000cc to 750cc, and the bikes had to be production based. This regulation created the first Japanese "Repli-Racer" the 1983 Honda Interceptor VF750F designed for Honda's American Motorcycle Association (AMA) VF750F super bike. Honda didn't cut corners when making the Interceptor, and made it as close to the super bike as possible without losing its street legality.

==New Technology==
The Honda Interceptor introduced technology to the street that was only seen on the race track. The bike debuted to the press in late 1982 and was available for sale to the public in 1983. The 1983 Honda Interceptor was given high praise by industry sources: "The handling is a treat, the power more than adequate and the appearance, the Interceptor's primary thrust, spells out its job: To boldly go where only race bikes have gone before.".<ref>''Cycle World'', December 1982, as quoted from ''The First Sport Bike? The 1983 Honda VF750F Interceptor''</ref> “On tight, twisty mountain roads the Honda does everything you ask of it; flick it from side to side, up hills or down, with the brakes on or off, and it responds willingly, instantly and precisely.<ref>''Cycle'', May 1983, as quoted by ''The First Sport Bike? The 1983 Honda VF750F Interceptor''</ref> The Interceptor's front bank was moved from the Magna's 23.5 degrees to 30 degrees to tuck the [[Comstar wheel|Comstar]] 16-inch front wheel closer to the engine which improved the handling now the bike had a 58.9 inch wheelbase. The front and rear wheels were controlled by fully adjustable Showa suspension. The front suspension was equipped with 39mm forks with the TRAC (Torque Reactive Anti-dive Control) anti-dive system to stabilize the ride on the track and on the road. The rear suspension used a sand cast swing-arm. The {{convert|748|cc|abbr=on}} engine had an {{convert|8|hp|abbr=on}} boost from the Magna's V4 because of a newly designed air-box which forced cold air onto the cylinder heads. Along with the air-box, the engine was liquid-cooled with two radiators. The Interceptor was equipped with a [[Slipper clutch|"Slipper Clutch"]] which made the clutch slip on hard braking to stop the rear tire from bouncing. This was the first time a street bike had ever had a slipper clutch. The engine was attached to the bike via a steel perimeter frame with a removable section so that the engine can be removed if needed.<ref name="Smith first sport bike" />

== Style ==
The Interceptor sported racing bike fairings that shaped the future for modern sport bikes. The tank was 5.8 gallons<ref name="Smith first sport bike" /> and was shaped to keep the rider's legs tucked in to eliminate air drag. The new front fairing replaced the cafe racer headlight with an  aerodynamic fiberglass fairing to push the air over the rider's helmet. The lower cowl produced down force to push the 551 pound<ref name="Smith first sport bike" /> bike to the road. The front wheel fairing was designed to eliminate debris from striking the bike and causing damage. The 32.3 inch  seat height provided comfort for a long trip without sacrificing the track bike stance.

== Accessories ==
In 1983 Honda offered a variety of accessories for the Interceptor. In 1983, a rear seat cowl with matching paint scheme was available to make the bike a closer replica of the single seated race bikes. The seat cowl had a storage compartment for small items and was lockable as well. Another optional extra were the engine crash bars which went from the bottom of the top radiator and followed the frame down the foot pegs. This provided protection to the engine in the case of an accident. Honda also offered a luggage rack and a bike cover.

== Engine Issues ==
The Interceptor had one endemic engine issue in the V4, camshaft wear. In 1984, camshaft issues started to arise and owners started to complain. At first Honda did not admit that there was a problem but after conducting further research, they decided that the camshaft wear was most likely caused by oil flow issues. Valve clearance issues were also suggested as a possible contributory factor. Honda issued a recall on the bikes and added kink-free oil lines and less restrictive banjo bolts. They also made alterations by drilling holes in the cam lobes and capping off the hollow cam's ends. This did not fix the problem, so Honda went back to the drawing board. The first thought was that the issue was heat related, but after further investigation the real problem was discovered - there was too much clearance in the camshaft bearings. Honda fixed this by changing all the camshafts with new components. This fixed the camshaft issues but the reputation of the Honda V4 was already damaged.<ref>http://vfreek.tripod.com/id11.html</ref>

==References==
{{reflist}}

[[Category:Honda motorcycles]]
[[Category:Motorcycles introduced in 1983]]