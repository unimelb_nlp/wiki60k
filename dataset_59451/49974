{{Infobox person
| name         = Cherry Jones
| image        = Cherry Jones 2009.jpg
| alt          = 
| caption      = Jones at ''[[24 (TV series)|24]]''{{'}}s season 7 finale screening in 2009
| birth_name   = 
| birth_date   = {{birth date and age|mf=yes|1956|11|21}}
| birth_place  = [[Paris, Tennessee]], [[United States|U.S.]]
| death_date   = 
| alma_mater   = [[Carnegie Mellon University]]
| occupation   = Actress
| years_active = 1980–present
| spouse       = {{marriage|Sophie Huber|2015}}
| partner      = [[Sarah Paulson]] (2004–2009)
| website      = 
}}

'''Cherry Jones''' (born November 21, 1956) is an American actress. A five-time [[Tony Award]] nominee for her work on [[Broadway (theatre)|Broadway]], she won the [[Tony Award for Best Actress in a Play]] for the 1995 revival of ''[[The Heiress (1947 play)|The Heiress]]'' and for the 2005 original production of ''[[Doubt (play)|Doubt]]''. She won the 2009 [[Primetime Emmy Award for Outstanding Supporting Actress in a Drama Series]] for her role as [[Allison Taylor]] on the [[Fox Broadcasting Company|FOX]] television series ''[[24 (TV series)|24]]''. She has also won three [[Drama Desk Awards]].

Jones made her Broadway debut in the 1987 original Broadway production of ''[[Stepping Out (play)|Stepping Out]]''. Other stage credits include ''[[Pride's Crossing]]'' (1997–98) and ''[[The Glass Menagerie]]'' (2013–14). Her film appearances include ''[[The Horse Whisperer (film)|The Horse Whisperer]]'' (1998), ''[[Erin Brockovich (film)|Erin Brockovich]]'' (2000), ''[[The Village (2004 film)|The Village]]'' (2004), ''[[Amelia (film)|Amelia]]'' (2009) and ''[[The Beaver (film)|The Beaver]]'' (2011). In 2012, she played Dr. Judith Evans on the [[NBC]] drama ''[[Awake (TV series)|Awake]]''.

==Early life and education==
Jones was born in [[Paris, Tennessee]], to her mother who was teaching [[high school]] and her father who owned a flower shop.<ref>{{cite web|url=http://www.filmreference.com/film/17/Cherry-Jones.html|title=Cherry Jones Biography (1956–)|publisher=|accessdate=5 May 2015}}</ref> She is a 1978 graduate of the [[Carnegie Mellon School of Drama]]. While at CMU, she was one of the earliest actors to work at [[City Theatre (Pittsburgh)|City Theatre]], a prominent fixture of [[Theatre in Pittsburgh|Pittsburgh theatre]].<ref>Conner, Lynne (2007). Pittsburgh In Stages: Two Hundred Years of Theater. University of Pittsburgh Press. pg. 247. ISBN 978-0-8229-4330-3. Retrieved 2011-07-15.</ref>

==Career==
Jones may be best known for her role as [[U.S. President]] [[Allison Taylor]] on the [[Fox Broadcasting Company|Fox]] series ''[[24 (TV series)|24]]'', for which she won an Emmy. However, most of her career has been in the theatre.  Her Broadway performances include [[Lincoln Center]]'s 1995 production of ''[[The Heiress]]'', and also a 2005 production of [[John Patrick Shanley]]'s play ''[[Doubt (play)|Doubt]]'' at the [[Walter Kerr Theatre]]. For both roles she earned a [[Tony Award]] for [[Tony Award for Best Performance by a Leading Actress in a Play|Best Leading Actress in a Play]].

Other Broadway credits include [[Nora Ephron]]'s play ''[[Imaginary Friends (play)|Imaginary Friends]]'' (with [[Swoosie Kurtz]]); ''[[Angels in America]]: Millennium Approaches'' and ''Perestroika'', the 2000 revival of ''[[A Moon for the Misbegotten]]'', and [[Timberlake Wertenbaker]]'s ''[[Our Country's Good]]'', for which she earned her first Tony nomination.<ref>Internet Broadway Database {{IBDB name|id=47147}}</ref> She is considered to be one of the foremost theater actresses in the United States.<ref>{{cite news|last=Brantley|first=Ben|title='The Glass Menagerie,' at Loeb Drama Center, Cambridge, MA|url=http://theater.nytimes.com/2013/02/16/theater/reviews/the-glass-menagerie-at-loeb-drama-center-cambridge-mass.html?adxnnl=1&pagewanted=all&adxnnlx=1361136928-OzA85bu2exVyID8L2BSijw|accessdate=17 February 2013|newspaper=New York Times|date=14 February 2013}}</ref>

She has narrated the audiobook adaptations of [[Laura Ingalls Wilder]]'s ''Little House '' series including, ''[[Little House in the Big Woods]]'', ''[[Little House on the Prairie (novel)|Little House on the Prairie]]'', ''[[Farmer Boy]]'', ''[[On the Banks of Plum Creek]]'', ''[[By the Shores of Silver Lake]]'', ''[[The Long Winter (novel)|The Long Winter]]'' and ''[[Little Town on the Prairie]]''. In recent years, Jones has ventured into feature films. Her screen credits include ''[[Cradle Will Rock]]'', ''[[The Perfect Storm (film)|The Perfect Storm]]'', ''[[Signs (film)|Signs]]'', ''[[Ocean's Twelve]]'' and ''[[The Village (2004 film)|The Village]]''.<ref>{{IMDb name|name=Cherry Jones|id=0427728}}</ref>

Jones played [[Allison Taylor (24 character)|President Taylor]] on the Fox series ''[[24 (TV series)|24]]'', a role for which she won an [[Emmy Award|Emmy]] for [[Primetime Emmy Award for Outstanding Supporting Actress&nbsp;– Drama Series|Outstanding Supporting Actress in a Drama Series]].<ref>{{cite news|author=Joyce Eng|title=Kristin Chenoweth, Jon Cryer Win First Emmys|url=http://www.tvguide.com/News/Kristin-Chenoweth-Jon-1009931.aspx|work=TVGuide.com|date=20 September 2009|accessdate=2009-09-20}}</ref> She played the role in the [[24 (season 7)|seventh season]] as well as eighth season, which began airing in January 2010 and concluded in May 2010.<ref>
{{cite news|url=http://www.reuters.com/article/entertainmentNews/idUSN2027850820070721?feedType=RSS&rpc=22&sp=true|title=Jones moves into 24 Oval Office|publisher=Reuters|accessdate=2008-07-26 | date=2007-07-21}}</ref>

In 2012, Jones starred in the NBC drama series ''[[Awake (TV series)|Awake]]'' as psychiatrist Dr. Judith Evans.

Also in 2012, she portrayed Amanda Wingfield in the Loeb Drama Center's revival of [[Tennessee Williams]]' ''[[The Glass Menagerie]]'' alongside [[Zachary Quinto]], [[Brian J. Smith]] and [[Celia Keenan-Bolger]].<ref>Hetrick, Adam. [http://www.playbill.com/news/article/171309-Zachary-Quinto-Celia-Keenan-Bolger-and-Brian-J-Smith-Join-Cherry-Jones-for-ARTs-Glass-Menagerie "Zachary Quinto, Celia Keenan-Bolger and Brian J. Smith Join Cherry Jones for A.R.T.'s Glass Menagerie"] {{webarchive |url=https://web.archive.org/web/20121019234350/http://www.playbill.com/news/article/171309-Zachary-Quinto-Celia-Keenan-Bolger-and-Brian-J-Smith-Join-Cherry-Jones-for-ARTs-Glass-Menagerie |date=October 19, 2012 }} playbill.com, October 18, 2012</ref>

In 2014, Cherry Jones was inducted into the [[American Theater Hall of Fame]].<ref>{{cite web|url=http://www.theatermania.com/new-york-city-theater/news/01-2014/cherry-jones-ellen-burstyn-cameron-mackintosh-and-_67312.html|title=Cherry Jones, Ellen Burstyn, Cameron Mackintosh and More Inducted Into Broadway's Theater Hall of Fame|accessdate=April 10, 2013}}</ref>

In 2015 and 2016 Jones had a recurring role on the [[Primetime Emmy Award]]-winning [[Amazon.com|Amazon]] comedy-drama series ''[[Transparent (TV series)|Transparent]]'' in its second and third seasons. She was nominated for the [[Critics' Choice Television Award for Best Guest Performer in a Comedy Series]] for her work in the 2015 season.

In 2016, she appeared in "[[Nosedive]]", an episode of the [[anthology series]] ''[[Black Mirror]]''.<ref>{{cite news|title=‘Black Mirror’ Season 3 Trailer: "No One Is This Happy’|url=http://deadline.com/2016/10/black-mirror-season-3-trailer-netflix-bryce-dallas-howard-alice-eve-james-norton-1201832611/|accessdate=7 October 2016|publisher=Deadline|date=7 October 2016}}</ref>

==Personal life==
Jones is [[Coming out|openly]] [[lesbian]].<ref name=newyork>{{cite web|last1=Witchel|first1=Alex|title=Cherry Jones, at the Peak of Her Powers|url=https://www.nytimes.com/2013/09/22/magazine/cherry-jones-at-the-peak-of-her-powers.html|website=New York Times}}</ref> In 1995, when Jones accepted her first Tony Award, she thanked her then-partner, architect Mary O'Connor,<ref>{{cite web|last1=Crews|first1=Chip|title=A Benefit of 'Doubt'|url=http://www.washingtonpost.com/wp-dyn/articles/A18958-2005Apr1.html|website=Washington Post}}</ref> with whom she had an 18-year relationship.<ref name=newyork/><ref>{{cite web|title=Cherry Jones: Prop 8 Supporters ‘Will Be Ashamed of Themselves’|url=http://www.queerty.com/cherry-jones-prop-8-supporters-will-be-ashamed-of-themselves-20090211}}</ref>

She started dating actress [[Sarah Paulson]] in 2004. When she accepted her Best Actress Tony in 2005 for her work in ''[[Doubt (play)|Doubt]]'', she thanked "Laura Wingfield," the ''[[Glass Menagerie]]'' character being played in the Broadway revival by Paulson.<ref name="auto">AfterEllen.com [http://www.afterellen.com/People/2005/6/sarahpaulson.html Sarah Paulson] {{webarchive |url=https://web.archive.org/web/20050609025237/http://www.afterellen.com:80/People/2005/6/sarahpaulson.html |date=June 9, 2005 }}</ref> In 2007, Paulson and Jones declared their love for each other in an interview with [[Velvetpark]] at Women's Event 10 for the LGBT Center of New York.<ref>{{cite web|url=http://www.velvetparkmedia.com|title=Velvetpark – Art Thought Culture|publisher=|accessdate=5 May 2015}}</ref> Paulson and Jones ended their relationship amicably in 2009.<ref name="aol">{{cite web|url=http://www.popeater.com/2009/10/09/cherry-jones-sarah-paulson-split/?icid=main%7cmain%7cdl2%7clink4%7chttp%3A%2F%2Fwww.popeater.com%2F2009%2F10%2F09%2Fcherry-jones-sarah-paulson-split%2F|title=Jones, Paulson Have 'Happiest Break Up'|author=AOL|work=Popeater|accessdate=5 May 2015}}</ref>

In mid-2015, Jones married her girlfriend, filmmaker Sophie Huber.<ref>{{cite web|last1=Bendix|first1=Trish|title=Cherry Jones on getting married and playing a lesbian feminist in Season 2 of "Transparent"|url=http://www.afterellen.com/tv/461535-cherry-jones-getting-married-playing-lesbian-feminist-season-2-transparent?utm_source=sc-twitter&utm_medium=referral&utm_campaign=post|website=Afterellen}}</ref>

==Filmography==

===Film===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! Notes
|-
| 1987
| ''[[Light of Day]]''
| Cindy Montgomery
| 
|-
| 1987
| ''[[The Big Town (1987 film)|The Big Town]]''
| Ginger McDonald
| 
|-
| 1992
| ''[[Housesitter]]''
| Patty
| 
|-
| 1995
| ''Polio Water''
| Virginia
| Short film
|-
| 1997
| ''[[Julian Po]]''
| Lucy
| 
|-
| 1998
| ''[[The Horse Whisperer (film)|The Horse Whisperer]]''
| Liz Hammond
| 
|-
| 1999
| ''[[Cradle Will Rock]]''
| Hallie Flanagan
| 
|-
| 1999
| ''[[The Lady in Question (1999 film)|The Lady in Question]]''
| Mimi Barnes
| 
|-
| 2000
| ''[[Erin Brockovich (film)|Erin Brockovich]]''
| Pamela Duncan
| 
|-
| 2000
| ''[[The Perfect Storm (film)|The Perfect Storm]]''
| Edie Bailey
| 
|-
| 2002
| ''[[Divine Secrets of the Ya-Ya Sisterhood (film)|Divine Secrets of the Ya-Ya Sisterhood]]''
| Buggy Abbott
| 
|-
| 2002
| ''[[Signs (film)|Signs]]''
| Officer Paski
| 
|-
| 2004
| ''[[The Village (2004 film)|The Village]]''
| Mrs. Clack
| 
|-
| 2004
| ''[[Ocean's Twelve]]''
| Molly Star/Mrs. Caldwell
| 
|-
| 2005
| ''[[Swimmers (film)|Swimmers]]''
| Julia Tyler
| 
|-
| 2009
| ''[[Amelia (film)|Amelia]]''
| [[Eleanor Roosevelt]]
| 
|-
| 2009
| ''[[Mother and Child (2009 film)|Mother and Child]]''
| Sister Joanne
| 
|-
| 2011
| ''[[The Beaver (film)|The Beaver]]''
| Vice President
| 
|-
| 2011
| ''[[New Year's Eve (film)|New Year's Eve]]''
| Mrs. Rose Ahern
| 
|-
| 2013
| ''[[Days and Nights]]''
| Mary
|
|-
| 2015
| ''[[Knight of Cups (film)|Knight of Cups]]''
| Ruth
| 
|-
| 2015
| ''[[I Saw the Light (film)|I Saw the Light]]''<ref name=RollingStone2>{{cite web|author1=Stephen L. Betts|title=Bradley Whitford, Cherry Jones Cast in Upcoming Hank Williams Movie|url=http://www.rollingstone.com/movies/news/bradley-whitford-hank-williams-movie-i-saw-the-light-20141107|work=Rolling Stone|accessdate=16 December 2014|date=7 November 2014}}</ref>
|Lillie Williams
| 
|-
| 2016
| ''[[Whiskey Tango Foxtrot (film)|Whiskey Tango Foxtrot]]''
| Geri Taub
| 
|-
|2017
| ''[[The Party (2017 film)|The Party]]''
|
|
|}

===Television===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! Notes
|-
| 1986
| ''Alex: The Life of a Child''
| Tina Crawford
| Television movie
|-
| 1987
| ''[[Spenser: For Hire]]''
| Tracy Kincaid
| Episode: "Sleepless Dream"
|-
| 1992
| ''[[Loving (TV series)|Loving]]''
| Frankie
| Unknown episodes
|-
| 1993
| ''[[Tribeca]]''
| Tough Woman
| Episode: "The Loft"
|-
| 1999
| ''[[Murder in a Small Town]]''
| Mimi 
| Television movie
|-
| 2000
| ''[[Cora Unashamed]]''
| Lizbeth Studevant
| Television movie
|-
| 2001
| ''[[What Makes a Family]]''
| Sandy Cataldi
| Television movie
|-
| 2001
| ''[[Frasier]]''
| Janet
| Episode: "Junior Agent"
|-
| 2002
| ''[[The American Experience]]''
| Narrator
| Episode: "[[Miss America (2002)|Miss America]]"
|-
| 2004
| ''[[The West Wing]]''
| Barbara Layton
| Episode: "Eppur Si Muove"
|-
| 2004–05
| ''[[Clubhouse (TV series)|Clubhouse]]''
| Sister Marie
| 3 episodes
|-
| 2008
| ''[[24: Redemption]]''
| President-Elect Allison Taylor
| Television movie
|-
| 2008–10
| ''[[24 (TV series)|24]]''
| President Allison Taylor
| 43 episodes<br>[[Primetime Emmy Award for Outstanding Supporting Actress in a Drama Series]]<br>Nominated—[[Satellite Award for Best Supporting Actress – Series, Miniseries or Television Film]]
|-
| 2012
| ''[[Awake (TV series)|Awake]]''
| Dr. Judith Evans
| 11 episodes
|-
| 2015–16
| ''[[Transparent (TV series)|Transparent]]''
| Leslie
| 11 episodes<br>Nominated—[[Critics' Choice Television Award for Best Guest Performer in a Comedy Series]]<br>Nominated—[[Screen Actors Guild Award for Outstanding Performance by an Ensemble in a Comedy Series]]
|-
| 2016
| ''[[Mercy Street (TV series)|Mercy Street]]''
| [[Dorothea Dix]]
| 2 episodes
|-
| 2016
| ''[[11.22.63]]''
| [[Marguerite Oswald]]
| 5 episodes
|-
| 2016
| ''[[Black Mirror]]''
| Susan
| Episode: "[[Nosedive]]"
|-
| 2017
| ''[[American Crime (TV series)|American Crime]]''
| Laurie Ann Hesby
| Upcoming series
|}

===Theatre===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! Notes
|-
| 1983
| ''[[The Philanthropist (play)|The Philanthropist]]''
| Liz
|
|-
| 1984
| ''The Ballad of Soapy Smith''
| Kitty Chase
| 
|-
| 1985–96
| ''[[The Importance of Being Earnest]]''
| Cecily Cardew
| 
|-
| 1987
| ''[[Claptrap]]''
| Sarah Littlefield
| 
|-
| 1987
| ''[[Stepping Out (play)|Stepping Out]]''
| Lynne 
| 
|-
|1987
|Tartuffe
|Dorine
|Portland Stage Company (Maine)
|-
| 1988
| ''[[Macbeth]]''
| Lady Macduff
| 
|-
| 1991
| ''[[Our Country's Good]]''
| Reverend Johnson/Liz Morden
| Nominated—[[Tony Award for Best Actress in a Play]]
|-
| 1991
| ''Light Shining in Buckinghamshire''
| {{n/a}} 
| 
|-
| 1992
| ''[[The Baltimore Waltz]]''
| Anna
| 
|-
| 1992
| ''Good Night Desdemona (Good Morning Juliet)''
| Constance Ledbelly
| 
|-
| 1993–94
| ''[[Angels in America: Millennium Approaches]]''
| Various replacements
| 
|-
| 1993–94
| ''[[Angels in America: Perestroika]]''
| Various replacements
| 
|-
| 1993
| ''And Baby Makes Seven''
| Anna
| 
|-
| 1993
| ''[[Desdemona (play)|Desdemona]]''
| Bianca
| 
|-
| 1995
| ''[[The Heiress (1947 play)|The Heiress]]''
| Catherine Sloper 
| [[Drama Desk Award for Outstanding Actress in a Play]]<br>[[Tony Award for Best Actress in a Play]]
|-
| 1996
| ''[[The Night of the Iguana]]''
| Hannah Jelkes
| 
|-
| 1997–98
| ''[[Pride's Crossing]]''
| Mabel Tidings/Bigelow
| [[Drama Desk Award for Outstanding Actress in a Play]]<br>[[Outer Critics Circle Awards|Outer Critics Circle Award for Best Actress in a Play]]
|-
| 1999
| ''Tongue of a Bird''
| Maxine
| 
|-
| 2000
| ''[[A Moon for the Misbegotten]]''
| Josie Hogan
| Nominated—[[Tony Award for Best Actress in a Play]]
|-
| 2001
| ''[[Major Barbara]]''
| Barbara Undershaft
| 
|-
| 2002–03
| ''[[Imaginary Friends (play)|Imaginary Friends]]''
| Mary McCarthy
| 
|-
| 2003
| ''Flesh and Blood''
| Mary Stassos
| 
|-
| 2005–06
| ''[[Doubt (play)|Doubt]]''
| Sister Aloysius
| [[Drama Desk Award for Outstanding Actress in a Play]]<br>[[Tony Award for Best Actress in a Play]]
|-
| 2006
| ''[[Faith Healer]]''
| Grace
| Nominated—[[Drama Desk Award for Outstanding Actress in a Play]]
|-
| 2010
| ''[[Mrs. Warren's Profession]]''
| Mrs. Kitty Warren
| 
|-
| 2013–14
| ''[[The Glass Menagerie]]''
| Amanda Wingfield
| [[Outer Critics Circle Awards|Outer Critics Circle Award for Best Actress in a Play]]<br>Nominated—[[Tony Award for Best Actress in a Play]]
|-
| 2014
| ''When We Were Young and Unafraid''
| Agnes
| 
|-
| 2017
| ''[[The Glass Menagerie]]''
| Amanda Wingfield
| Pending—[[Laurence Olivier Award for Best Actress|Olivier Award for Best Actress]]
|}

==Awards and nominations==
{| class="wikitable sortable"
|-
! Year
! Association
! Category
! Nominated work
! Result
|-
| 1991
| [[Tony Awards]]
| [[Tony Award for Best Actress in a Play|Best Actress in a Play]]
| ''[[Our Country's Good]]''
| {{nom}}
|-
| 1995
| [[Drama Desk Awards]]
| [[Drama Desk Award for Outstanding Actress in a Play|Outstanding Actress in a Play]]
| ''[[The Heiress (1947 play)|The Heiress]]''
| {{won}}
|-
| 1995
| [[Tony Awards]]
| [[Tony Award for Best Actress in a Play|Best Actress in a Play]]
| ''[[The Heiress (1947 play)|The Heiress]]''
| {{won}}
|-
| 1998
| [[Drama Desk Awards]]
| [[Drama Desk Award for Outstanding Actress in a Play|Outstanding Actress in a Play]]
| ''[[Pride's Crossing]]''
| {{won}}
|-
| 2000
| [[Tony Awards]]
| [[Tony Award for Best Actress in a Play|Best Actress in a Play]]
| ''[[A Moon for the Misbegotten]]''
| {{nom}}
|-
| 2004
| [[GLAAD Media Awards]]
| [[GLAAD Media Awards|Vito Russo Award]]
| Herself
| {{won}}
|-
| 2005
| [[Drama Desk Awards]]
| [[Drama Desk Award for Outstanding Actress in a Play|Outstanding Actress in a Play]]
| ''[[Doubt (play)|Doubt]]''
| {{won}}
|-
| 2005
| [[Tony Awards]]
| [[Tony Award for Best Actress in a Play|Best Actress in a Play]]
| ''[[Doubt (play)|Doubt]]''
| {{won}}
|-
| 2006
| [[Drama Desk Awards]]
| [[Drama Desk Award for Outstanding Actress in a Play|Outstanding Actress in a Play]]
| ''[[Faith Healer]]''
| {{nom}}
|-
| 2009
| [[Primetime Emmy Awards]]
| [[Primetime Emmy Award for Outstanding Supporting Actress in a Drama Series|Outstanding Supporting Actress in a Drama Series]]
| ''[[24 (TV series)|24]]''
| {{won}}
|-
| 2009
| [[Satellite Awards]]
| [[Satellite Award for Best Supporting Actress – Series, Miniseries or Television Film|Best Supporting Actress – Series, Miniseries or Television Film]]
| ''[[24 (TV series)|24]]''
| {{nom}}
|-
| 2014
| [[Outer Critics Circle Awards]]
| Outstanding Actress in a Play
| ''[[The Glass Menagerie]]''
| {{won}}
|-
| 2014
| [[Tony Awards]]
| [[Tony Award for Best Actress in a Play|Best Actress in a Play]]
| ''[[The Glass Menagerie]]''
| {{nom}}
|-
| 2016
| [[Critics' Choice Television Awards]]
| [[Critics' Choice Television Award for Best Guest Performer in a Comedy Series|Best Guest Performer in a Comedy Series]]
| ''[[Transparent (TV series)|Transparent]]''
| {{nom}}
|-
| 2016
| [[Screen Actors Guild Awards]]
| [[Screen Actors Guild Award for Outstanding Performance by an Ensemble in a Comedy Series|Outstanding Performance by an Ensemble in a Comedy Series]]
| ''[[Transparent (TV series)|Transparent]]''
| {{nom}}
|-
| 2017
| [[Laurence Olivier Award|Olivier Awards]]
| [[Laurence Olivier Award for Best Actress|Olivier Award for Best Actress]]
| ''[[The Glass Menagerie]]''
| {{pending}}
|}

==References==
{{reflist|2}}

==External links==
{{Commons category|Cherry Jones}}
* {{IBDB name}}
* {{IMDb name|0427728}}
* [http://www.lortel.org/LLA_archive/index.cfm?search_by=people&first=Cherry&last=Jones&middle= Cherry Jones] at the [[Internet Off-Broadway Database]]
* [http://www.amazon.com/gp/product/0472069330 Cast Out: Queer Lives in Theater] (U. Michigan Press, edited by Robin Bernstein) republishes the interview in which Cherry Jones first publicly discussed her sexuality.
* [https://soundcloud.com/american-theatre-wing/episode-56 Cherry Jones] – ''Downstage Center'' interview at [[American Theatre Wing|American Theatre Wing.org]]
* [http://www.tonyawards.com/en_US/interactive/video/index.html#j TonyAwards.com Interview with Cherry Jones]

{{Navboxes
|title = Awards for Cherry Jones
|list =
{{DramaDesk PlayOutstandingActress 1975-2000}}
{{DramaDesk PlayOutstandingActress 2001-2025}}
{{Distinguished Performance Award}}
{{EmmyAward DramaSupportingActress 2001–2025}}
{{TonyAward PlayLeadActress 1976-2000}}
{{TonyAward PlayLeadActress 2001-2025}}
}}

{{Authority control}}

{{DEFAULTSORT:Jones, Cherry}}
[[Category:1956 births]]
[[Category:Living people]]
[[Category:American film actresses]]
[[Category:American stage actresses]]
[[Category:Audio book narrators]]
[[Category:Carnegie Mellon University College of Fine Arts alumni]]
[[Category:Drama Desk Award winners]]
[[Category:Outstanding Performance by a Supporting Actress in a Drama Series Primetime Emmy Award winners]]
[[Category:Lesbian actresses]]
[[Category:LGBT entertainers from the United States]]
[[Category:People from Henry County, Tennessee]]
[[Category:Tony Award winners]]
[[Category:LGBT rights activists from the United States]]
[[Category:20th-century American actresses]]
[[Category:21st-century American actresses]]
[[Category:Actresses from Tennessee]]
[[Category:American Theater Hall of Fame inductees]]