{{italic title}}
{{Infobox Journal
| title        = Willamette Law Review
| cover        = [[File:Willamette Law Journal 1959 issue 1 vol 1.JPG|150px]]
| editor       = Melissa Vollono
| discipline   = [[Law review]]
| language     = [[English language|English]]
| abbreviation = Willamette L. Rev.
| publisher    = [[Willamette University College of Law]]
| country      = United States
| frequency    = Triannually
| history      = 1959 to present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = https://willamette.edu/law/resources/journals/review/
| ISSN         = 0191-9822
}}
The '''''Willamette Law Review''''' is a [[law review]] academic journal published by [[Willamette University College of Law]] in [[Salem, Oregon]], [[United States]]. Founded in 1959 as a predecessor to an earlier publication, the triannual publication is housed in the [[Oregon Civic Justice Center]]. Cited as ''Willamette L. Rev.'', the journal is edited by students of the [[law school]] with oversight by the college's faculty. As of 2016, Willamette Law Review has published a total of 52 volumes.

==History==
[[Willamette University]]’s law school established a publication called the ''Legal Handbooks'' in 1949.<ref name="chron2">{{cite book|last=Gregg|first=Robert D.|title=Chronicles of Willamette: Those Eventful Years of the President Smith Era|publisher=Willamette University|location=Salem, Oregon|date=1970|volume=Volume II|pages=87, 207}}</ref> In 1959, the school founded their law review journal, replacing the ''Legal Handbooks''.<ref name=history>[http://www.willamette.edu/wucl/journals/review/ Willamette Law Review.] Willamette University College of Law. Retrieved on September 26, 2008.</ref> The school's faculty had decided to start the journal and selected the first editorial staff.<ref>{{cite journal|date=Fall 2009|title=50 Years Later|journal=Willamette Lawyer|publisher=Willamette University College of Law|volume=IX|issue=2|pages=18–19|url=http://www.willamette.edu/wucl/pdf/lawyer/fall2009.pdf}}</ref> Ronald B. Lansing served as the first [[editor in chief]] of what started as a twice-yearly publication.<ref>{{cite journal|date=1959|title=Editorial Board|journal=Willamette Law Review|volume=1|pages=1}}</ref> The first issue focused on employer liability.<ref name=becka/>

At the beginning of its existence, the [[Oregon State Bar]] helped pay for the publication, with copies sent to all members of the Oregon Bar.<ref name="chron2"/> The journal was first located in the law school building at what is now [[Gatke Hall]], and moved in 1967 to the new Truman Wesley Collins Legal Center when the law school relocated to its new home.<ref name=becka>Celebrating 125 Years of Outstanding Legal Education and Bar Leadership. Editor Anne Marie Becka, Willamette University College of Law, 2008. p. 29.</ref> In the early years of the journal, student authors were required to meet certain academic standards.<ref name="capjournal">{{cite news|title=Fledgling attorneys feud over publishing right|last=Bennett|first=Chuck|date=October 4, 1974|work=Capital Journal|pages=Sec. 1, p. 7}}</ref> First year students and those in the lower two-thirds of their second and third year class could not submit articles for publication.<ref name="capjournal"/>

Originally titled as the '''''Willamette Law Journal''''' for its first 14 volumes, the name was changed to the ''Willamette Law Review'' in 1978.<ref>[http://www.co.harris.tx.us/law/WY.html Periodical Holdings: WY.] Harris County Law Library. Retrieved on September 26, 2008.</ref> By Spring 1981, the yearly subscription cost for the journal had risen to USD $12.50.<ref name=econ>[http://www.jstor.org/stable/2724717 “New Journals”,]
''Journal of Economic Literature'', Vol. 20, No. 1. (Mar., 1982), pp. 215-218; p. 218.</ref> That issue included articles on the use of televisions in courtrooms and [[piercing the corporate veil]] among other topics.<ref name=econ/> In October 2006, the journal sponsored a symposium on former [[Oregon Supreme Court]] justice and distinguished scholar in residence at Willamette, [[Hans A. Linde]].<ref>”Bar News: Up to date news of the Oregon State Bar: Willamette Conference to honor Linde.” ''The [[Oregon State Bar Bulletin]]'', October 2006. 67 Or. St. B. Bull. 49.</ref>

In September 2008, ''Willamette Law Review'' moved into the new Oregon Civic Justice Center, located in the former Salem Carnegie Library, along with several other law school programs.<ref name=then>Lynn, Capi. “Then & Now”, ''[[Statesman Journal]]'', September 11, 2008, Life, p. 1.</ref> The building was rededicated in a ceremony with Supreme Court justice [[Ruth Bader Ginsburg]] as the guest of honor.<ref name=then/>

==Editions==
[[Image:Oregon Civic Justice Center construction summer 2008.JPG|thumb|[[Oregon Civic Justice Center]] where the ''Willamette Law Review'' is housed]]
Early volumes of the publication focused on a single legal issue.<ref name="chron2"/> Although the periodical is focused on legal issues in general, every other year one edition is focused on purely [[Oregon]] legal items.<ref name=history/> One of these topics was the [[Oregon Revised Statutes|Oregon Uniform Trust Code]] when it became law in 2006.<ref>“Department: Briefs: Law review considers new trust code.” ''The [[Oregon State Bar Bulletin]]'', February/March, 2006. 66 Or. St. B. Bull. 7.</ref> According to a draft list published by the ''Albany Law Review'', the ''Willamette Law Review'' ranks in their tier five (journals in that tier rank 196 to 260 out of 540 total law reviews) of assessment of law journals based on the journal’s article selection process.<ref>Nance, Jason P. and Dylan J. Steinberg. [http://www.law.indiana.edu/people/henderson/share/AppendixA.pdf “The law review article selection process: results from a national study”.] ''Albany Law Review'', March 22, 2008, Pg. 565(57) Vol. 71 No. 2 {{ISSN|0002-4678}}.</ref>

The ''Willamette Law Review'' also sponsors symposiums on various legal topics each year,<ref>Guerrero-Huston, Thelma. “Symposium focuses on health and law”, ''[[Statesman Journal]]'', March 5, 2008.</ref> and then publishes the resulting articles.<ref name=ajil>Shelton, Dinah. [http://www.jstor.org/stable/2200834 Book Reviews and Notes: Willamette Law Review.] ''The American Journal of International Law'', Vol. 76, No. 3. (Jul., 1982), pp. 697-698.</ref> Topics have included international law (2008),<ref name=ajil/> and sports law (2006) in recent years.<ref>Franke, Gloria. “The Right of Publicity vs. the First Amendment; Will One Test Ever Capture the Starring Role?” 79 ''Southern California Law Review'' 945 (2006), ''Entertainment Law Reporter'', November, 2006, Vol. 28, No. 6.</ref> The journal has been cited in a variety of publications including ''[[The Washington Quarterly]]'',<ref>Moore, John Norton. “Considering an International Subseabed Waste Repository: Rational Choice and Community Interest”. ''The Washington Quarterly'', Summer 1986, Vol. 9, No. 3; Pg. 115.</ref> ''[[The Oregonian]]'',<ref>Green, Ashbel S. and Laura Oppenheimer. “Land-use lawsuit invokes ant-favoritism clause”. ''[[The Oregonian]]'', January 15, 2005, p. A1.</ref> and ''[[The Birmingham News]]'' to name several.<ref>White, David. “Democrats seek nonpartisan elections for state's judges”, ''[[The Birmingham News]]'', February 6, 2006, p. 1B, Vol. 118 No. 303.</ref>

==References==
{{Reflist}}

==External links==
*[http://papers.ssrn.com/sol3/Jeljour_results.cfm?form_name=Pip_jrl&journal_id=236841&Network=no&SortOrder=ab_title&stype=asc&lim=false Social Science Research Network]
*[http://lawlib.wlu.edu/LJ/index.aspx?mainid=155 Law Journals: Submissions and Ranking] - Washington & Lee Law School

{{Willamette University}}

[[Category:Willamette University College of Law]]
[[Category:Publications established in 1959]]
[[Category:Magazines published in Oregon]]
[[Category:University and college media in Oregon]]
[[Category:American law journals]]
[[Category:Media in Salem, Oregon]]
[[Category:1959 establishments in Oregon]]