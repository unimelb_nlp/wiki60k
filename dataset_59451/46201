{{advert|date=December 2013}}

{{Infobox Software
| name                   = AnimatLab
| developer              = David W. Cofer, Gennady Cymbalyuk, James Reid, Ying Zhu, William J. Heitler, and Donald H. Edwards
| programming language   = [[C++]],  [[VB.NET]]
| latest release version = 2.0.7
| latest release date    = {{release date and age|2013|6|8}}
| operating_system       = [[Windows]]
| status                 = Active
| genre                  = Neuromechanics
| website                = http://www.animatlab.com
}}

'''AnimatLab''' is an [[Open-source software|open-source]]{{citation needed|date=August 2013}} neuromechanical simulation tool that allows authors to easily build and test [[Biomechanical engineering|biomechanical models]] and the [[neural networks]] that control them to produce behaviors. Users can construct neural models of varied level of detail, 3D mechanical models of triangle meshes, and use muscles, motors, receptive fields, stretch sensors, and other transducers to interface the two systems. Experiments can be run in which various stimuli are applied and data is recorded, making it a useful tool for computational neuroscience. The software can also be used to model biomimetic robotic systems.

== Motivation ==
Neuromechanical simulation enables investigators to explore the dynamical relationships between the brain, the body, and the world in ways that are difficult or impossible through experiment alone. This is done by producing biologically realistic models of the neural networks that control behavior, while also simulating the physics that controls the environment in which an animal is situated. Interactions with the simulated world can then be fed back to the virtual nervous system using models of sensory systems. This provides feedback similar to what the real animal would encounter, and makes it possible to close the sensory-motor feedback loop to study the dynamic relationship between nervous function and behavior. This relationship is crucial to understanding how nervous systems work.<ref>{{cite journal | last1 = Chiel | first1 = H. J. | last2 = Beer | first2 = R. D. | year = 1997 | title = The brain has a body: adaptive behavior emerges from interactions of nervous system, body and environment | url = | journal = Trends in Neurosciences | volume = 20 | issue = 12| pages = 553–7 | pmid = 9416664 | doi=10.1016/s0166-2236(97)01149-1}}</ref>

== History ==
The application was initially developed at [[Georgia State University]] under [[National Science Foundation|NSF]] grant #0641326.<ref>[http://www.nsf.gov/awardsearch/showAward?AWD_ID=0641326&HistoricalAwards=false  National Science Foundation Awards]</ref> Version 1 of AnimatatLab was released in 2010. Work has continued on the application and a new, improved second version was recently released in June 2013.

== Functionality ==
AnimatLab allows users to develop models of varied levels of detail due to the types of models available. Neurons may be simple firing rate models, [[integrate-and-fire model]]s, or [[Hodgkin–Huxley model]]s. Plugins for other neuron models can be written and used. [[Hill's muscle model|Hill-type muscles]], motors, or [[servos]] can be used to actuate joints. Adapters between neurons and actuators are used to generate forces. Adapters between mechanical components (joints, body segments, muscles, etc.) provide feedback to the control system. Stimuli, such as [[voltage clamp]]s, [[current clamp]]s, and velocity clamps (for joints) can be added to design experiments. Data can be recorded from virtually every component of the system, and viewed in graphs or exported as a [[comma separated values]] file, making analysis easy. In addition, the user interface is entirely graphical, making it easy for beginners to use.

=== Neural modeling ===
A variety of [[biological neuron models]] are available for use. The [[Hodgkin–Huxley model]], both single- and multi-compartment [[integrate-and-fire model]]s, and various abstracted firing-rate models are available.<ref name="ncbi">{{cite journal | last1 = Cofer | first1 = D. W. | last2 = Cymbalyuk | first2 = G. | last3 = Reid | first3 = J. | last4 = Zhu | first4 = Y. | last5 = Heitler | first5 = W. | last6 = Edwards | first6 = D.H. | year = 2010 | title = AnimatLab: A 3-D graphics environment for neuromechanical simulations | url = | journal = J Neuroscience Methods | volume = 187 | issue = 2| pages = 280–288 | pmid = 20074588 | doi=10.1016/j.jneumeth.2010.01.005}}</ref> This is a valuable feature because the purpose of one's model and its complexity decide which features of neural behavior are important to simulate.<ref>{{cite journal | last1 = Izhikevich | first1 = E. M. | year = 2004 | title = Which model to use for cortical spiking neurons? | url = http://www.izhikevich.org/publications/whichmod.pdf | format = PDF | journal = IEEE transactions on neural networks / a publication of the IEEE Neural Networks Council | volume = 15 | issue = 5| pages = 1063–70 | doi = 10.1109/TNN.2004.832719 }}</ref>

Network construction is graphical, with neurons dragged and dropped into a network and synapses drawn between them. When a synapse is drawn, the user specifies what type to use. Both spiking and nonspiking [[chemical synapses]], as well as [[electrical synapse]]s, are available. Both short-term (through [[Neural facilitation|facilitation]]) and long term ([[Hebbian]]) learning mechanisms are available, greatly increasing the capability of the nervous systems constructed.

=== Rigid body modeling ===
Body segments are modeled as [[rigid bodies]] drawn as [[triangle mesh]]es with uniform mass density.<ref name="ncbi" /> Meshes can be selected from a set of primitives (cube, ellipsoid, cone, etc.) or imported from third party software such as [[Maya (software)|Maya]] or [[Blender (software)|Blender]]. Physics are simulated with the [[Vortex (physics engine)|Vortex]] engine. Users can specify separate collision and graphical meshes for a rigid body, greatly reducing simulation time. In addition, material properties and the interaction between materials can be specified, allowing different restitution, coefficient of friction, etc. within the simulation.

=== Muscle modeling ===
A [[Hill's muscle model|Hill-type muscle model]] modified according to (<ref>Shadmehr R., Wise S. [http://mitpress.mit.edu/books/computational-neurobiology-reaching-and-pointing Computational neurobiology of reaching and pointing: a foundation for motor learning.] Cambridge, Massachusetts: MIT Press; 2005.</ref>) can be used for actuation. Muscles are controlled by placing a voltage-tension adapter between a motor neuron and a muscle. Muscles also have stiffness and damping properties, as well as [[Muscle contraction|length-tension]] relationships that govern their behavior. Muscles can are placed to act on muscle attachment bodies in the mechanical simulation, which then apply the muscle tension force to the other bodies in the simulation.

=== Sensory modeling ===
Adapters may be placed to convert rigid body measurements to neural activity, much like how voltage-tension adapters are used to activate muscles. These may be joint angles or velocities, rigid body forces or accelerations, or behavioral states (e.g. hunger).

In addition to these scalar inputs, contact fields may be specified on rigid bodies, which then provide pressure feedback to the system. This functionality has been used for skin-like sensing <ref name="ncbi" /> and to detect leg loading in walking structures.<ref name="Szczecinski">Szczecinski, N. S. Massively distributed neuromorphic control for legged robots modeled after insect stepping. Master's Thesis. Case Western Reserve University, 2013.</ref>

=== Stimulus types ===
Stimuli can be applied to mechanical and neural objects in simulation for experimentation. These include [[Electrophysiology|current and voltage clamps]], as well as velocity clamps for joints between rigid bodies.

=== Graph types ===
Data can be output in the form of line graphs and two-dimensional surfaces. Line graphs are useful for most data types, including neural and synaptic output, as well as body and muscle dynamics. Surface plots are useful for outputting activation on contact fields. Both of these can be output as [[.csv|comma separated values]] files, allowing the user to use other software such as [[MATLAB|Matlab]] or [[Microsoft Excel|Excel]] for quantitative analysis.

== Research performed with AnimatLab ==
Many academic projects have used AnimatLab to build neuromechanical models and explore behavior. These include:
* Shaking of a wet cat paw<ref>Klishko A., Cofer D. W., Edwards D. H., Prilutsky B. Extremely high paw acceleration during paw shake in the cat: a mechanism revealed by computer simulations. AbstrAm Phys Soc Meeting A38.00007; 2008a.</ref><ref>Klishko A., Prilutsky B., Cofer D. W., Cymbalyuk G., Edwards D. H. Interaction of CPG, spinal reflexes and hindlimb properties in cat paw shake: a computer simulation study. Neuroscience Meeting Planner Online, Program No. 375.12. Society for Neuroscience; 2008b.</ref>
* Locust jump and flight control <ref>Cofer, D. W. (2009). Neuromechanical Analysis of the Locust Jump (Ph.D. dissertation). Available from digital archive database. (Article No. 1056)</ref><ref>{{cite journal | last1 = Cofer | first1 = D. W. | last2 = Cymbalyuk | first2 = G. | last3 = Heitler | first3 = W. J. | last4 = Edwards | first4 = D.H. | year = | title = Neuromechanical simulation of the locust jump | url = | journal = J Exp Biol | volume = 2010 | issue = 213| pages = 1060–1068 }}</ref><ref>{{cite journal | last1 = Cofer | first1 = D. W. | last2 = Cymbalyuk | first2 = G. | last3 = Heitler | first3 = W. J. | last4 = Edwards | first4 = D. H. | year = 2010 | title = Control of tumbling during the locust jump | url = | journal = J Exp Biol | volume = 213 | issue = 19| pages = 3378–87 | doi=10.1242/jeb.046367}}</ref>
* Crayfish walking<ref>Rinehart M. D., Belanger J. H. Biologically realistic limb coordination during multi-legged walking in the absence of central connections between legs. In: Society for Neuroscience Annual Meeting; 2009.</ref>
* Cockroach walking and turning<ref name="Szczecinski" />

== External links ==
* {{Official website|http://animatlab.com}}

== References ==
{{reflist}}

[[Category:Robotics simulation software]]
[[Category:Scientific simulation software]]
[[Category:Science software]]