__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Twister
 | image=KnightTwister.jpg
 | caption=1951 D-1 Knight Twister
}}{{Infobox Aircraft Type
 | type=Sport aircraft
 | national origin=United States
 | manufacturer=[[Homebuilt aircraft|Homebuilt]]
 | designer=Vernon W. Payne, Sr.
 | first flight=1932
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=ca 75
 | unit cost=approximately $2530 to build in 1971<ref>{{cite journal|magazine=Air Trails|date=Winter 1971|title=The true cost of building your own plane|author=Leo J. Kohn|page=63}}</ref>
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Payne Knight Twister''' is a single-seat, single-engine aerobatic sport aircraft first flown by Vernon Payne Sr. in the United States in 1932 and marketed in plans form for [[homebuilt aircraft|homebuilding]].<ref name="JAE 716">Taylor 1989, p.716</ref><ref name="JAWA77 551">''Jane's All the World's Aircraft 1977–78'', p.551</ref>

==Design and development==
It is a conventional biplane design with slightly staggered wings of unequal span.<ref name="JAWA77 551"/> The wings are of fully cantilever design and do not require the bracing wires commonly used on biplanes or even interplane struts; however, most builders brace the wings with I-struts and at least one pair of wires.<ref name="Davisson 1999">Davisson 1999, p.44</ref> The cockpit is open, and the undercarriage is of fixed, tailwheel type with divided main units.<ref name="JAWA77 551"/> The wings and horizontal stabilizer are of wooden construction, skinned in plywood, while the fuselage and vertical stabilizer are of welded steel tube covered in fabric.<ref name="JAWA77 552">''Jane's All the World's Aircraft 1977–78'', p.552</ref>

Payne designed the Knight Twister in 1928 while teaching aircraft design and repair at a school attached to the  [[Aviation Service and Transport Company]] in [[Chicago]].<ref name="Historical">"Knight Twister Historical Information"</ref> Construction of a prototype by Payne and his students commenced the following year but ended shortly thereafter when the school was forced to close as a consequence of the [[Great Depression]].<ref name="Historical"/> Payne began building a second prototype in 1931, which first flew in fall the following year powered by a [[Salmson 9 (air cooled engine)|Salmson 9A]]d radial engine.<ref name="Historical"/> This aircraft was damaged in a forced landing due to fuel exhaustion during a demonstration flight for the press, and parts of the airframe were reused to build the second Knight Twister in 1935.<ref name="Historical"/> This machine, powered by a converted [[Ford Model A (1927-1931)|Ford Model A]] automobile engine, was built for an Argentine buyer<ref name="New">"The New Knight Twister" 1937, p.35</ref> who eventually declined to take delivery.<ref name="Historical"/> After it had passed through several hands, Payne himself bought the aircraft back after World War II and his son, Richard, was killed in it during a test flight on which the engine failed shortly after take-off.<ref name="Historical"/>

==Operational history==
The Knight Twister built a reputation as a racing aircraft.<ref name="Historical"/> In 1964, Clyde Parsons flying the "Parsons Twister", won the Sport Biplane Championship race at [[Reno Air Races|Reno]] with a speed of 144.7&nbsp;mph.<ref name="Historical"/><ref>{{cite book|title=Air Racing Today: Heavy Iron at Reno|author=Philip Handleman|page=110}}</ref> In the 1970s, [[Don Fairbanks]] competed with a Knight Twister preserved initially at the [[Motorsports Hall of Fame of America]] museum in [[Novi, Michigan]],<ref name="Historical"/> and later in the lobby of Sporty's Pilot Shop at [[Clermont County Airport]], [[Batavia, Ohio]]{{Citation needed|date=July 2013}}. Fairbanks set the world record in the sport biplane class of 178&nbsp;mph (284&nbsp;km/h) with this aircraft.<ref name="Historical"/>

The Knight Twister has a reputation as a "handful" to fly,<ref name="Davisson 1999"/><ref name="Davisson 2000">Davisson 2000</ref> but this has been vigorously denied by both its designer<ref name="Payne">Payne 1985</ref> and by Fairbanks.<ref name="Fairbanks">Fairbanks</ref> Both men have attributed this reputation to the controls being lighter and more responsive than those of the light aircraft that most pilots are more familiar with.

In the 1990s, the rights to the design were acquired by [[Steen Aero]], who continue to offer plans for sale in 2009.<ref name="Historical"/>

==Variants==
[[File:Knight Twister 125 (N7D).jpg|right|thumb|KT-125]]
''Data from:'' "Knight Twister Historical Information" (except as noted)
* '''KTS-1''' - first prototype with [[Salmson 9 (air cooled engine)|Salmson 9A]]d engine (1 built)<ref>[https://books.google.com/books?id=YSgDAAAAMBAJ&pg=PA32&dq=Popular+Science+1933+plane+%22Popular+Science%22&hl=en&ei=iF1WTaWnOY66tgeC0YXHDA&sa=X&oi=book_result&ct=result&resnum=4&ved=0CDYQ6AEwAzge#v=onepage&q=Popular%20Science%201933%20plane%20%22Popular%20Science%22&f=true "Fifteen Foot Plane Flies 120 Miles An Hour"] ''Popular Science'', July 1934, rare photo of only one built</ref>
* '''KTD-2''' - second prototype with converted [[Ford Model A (1927-1931)|Ford Model A]] engine designated ''Douglas Bear''(1 built), later redesignated '''Knight Twister Junior 75-85'''<ref>[https://books.google.com/books?id=rNoDAAAAMBAJ&pg=PA383&dq=Popular+Science+1935+plane+%22Popular+Mechanics%22&hl=en&ei=r81ETsS3KI3isQLY3dX5BQ&sa=X&oi=book_result&ct=result&resnum=7&ved=0CEUQ6AEwBjgU#v=onepage&q&f=true "Tiny Pane Has Wingspread of Only Fifteen Feet" ''Popular Science'', September 1936]</ref>
* '''KT-50''' - version with {{convert|50|hp|kW|0|abbr=on}} [[Continental Motors Company|Continental]] or [[Franklin Engine Company|Franklin]] engine and 18-ft wingspan
* '''KT-75 Knight Twister Junior''' - version with {{convert|75|hp|kW|0|abbr=on}} Continental or [[Lycoming Engines|Lycoming]] engine and 17&nbsp;ft 6 in-wingspan<ref name="JAWA77 552" /><ref name="JAE 717">Taylor 1989, p.717</ref><ref name="Knight Twister Junior">"Knight Twister Junior" 1949, p. 34</ref>
* '''KT-80''' - version with {{convert|80|hp|kW|0|abbr=on}} Franklin engine
* '''KT-85''' - standard version with Continental engine of {{convert|85|to|90|hp|kW|0|abbr=on}} and 15-ft wingspan<ref name="JAE 716"/>
* '''KT-90''' - version with {{convert|90|hp|kW|0|abbr=on}} engine and 15-ft wingspan
** '''KTT-90''' - version with {{convert|90|hp|kW|0|abbr=on}} Lycoming engine and 18-ft wingspan
* '''KT-95''' - version with {{convert|95|hp|kW|0|abbr=on}} Lambert engine
* '''KT-125''' - version with {{convert|185|hp|kW|0|abbr=on}} engine <!--sic, as per source. Typo? --> 
* '''KT-140''' - version with {{convert|140|hp|kW|0|abbr=on}} engine.<ref>{{cite journal|magazine=Flying Magazine|date=November 1960|title=EAA Fly-In|page=37}}</ref>
** '''SKT-125 Sunday Knight Twister''' - version with {{convert|125|hp|kW|0|abbr=on}} Lycoming engine and 19&nbsp;ft 6 in-wingspan<ref name="JAWA77 552"/><ref name="JAE 717"/>
* '''KT Imperial''' - version with engine of {{convert|135|to|150|hp|kW|0|abbr=on}} and wing area increased (span: 17&nbsp;ft 6 in) to comply with Sport Biplane class rules<ref>{{cite journal|magazine=Air Trails|date=Winter 1971|page=65}}</ref>
* '''KT Holiday''' - version with {{convert|125|hp|kW|0|abbr=on}} engine and wingspan of 19&nbsp;ft 6 in
* '''KT Acro''' - version with wingspan of 15&nbsp;ft 6 in
* '''KT Coed''' - version with passenger seat in tandem with pilot's; wingspan of 22&nbsp;ft 6 in.
* '''Double Twist''' - A two place model with an untapered 21&nbsp;ft M6 airfoil.<ref>{{cite journal|magazine=Air Trails|date=Winter 1971|page=61}}</ref>
* '''Pretty Prairie Special II model 1''' - Straight leg conventionally braced based on a Knight Twister, stretched 16 inches<ref>{{cite journal|magazine=Air Progress Homebuilt Aircraft|date=June 1968|pages=28}}</ref>
* '''Pretty Prairie Special III''' - A Menasco powered variant displayed in the [[EAA Airventure Museum]] in [[Oshkosh, Wisconsin]] until 2006,<ref>{{cite web|title=UNRUH PRETTY PRAIRIE SPECIAL III – N1473V|url=http://www.airventuremuseum.org/collection/aircraft/Unruh%20Pretty%20Prairie%20Special%20III.asp|accessdate=16 April 2011}}</ref> and now at the [[Kansas Aviation Museum]].<ref>{{cite web|title=Kansas Aviation Museum|url=http://kansasaviationmuseum.org/pprairie.php|accessdate=17 April 2012}}</ref>

==Specifications (KT-85)==
{{aerospecs
|ref=<!-- reference -->''Jane's All the World's Aircraft 1977–78'', p.552
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->eng
|crew=One pilot
|capacity=
|length m=4.27
|length ft=14
|length in=0
|span m=4.57
|span ft=15
|span in=0
|height m=1.60
|height ft=5
|height in=3
|wing area sqm=5.6
|wing area sqft=60
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=243
|empty weight lb=535
|gross weight kg=435
|gross weight lb=960
|eng1 number=1
|eng1 type=[[Continental C90]]
|eng1 kw=<!-- prop engines -->67
|eng1 hp=<!-- prop engines -->90
|max speed kmh=257
|max speed mph=160
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=625
|range miles=390
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=4.6
|climb rate ftmin=900
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Mong MS1 Sport]]
*[[Smith Miniplane]]
<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|30em}}

==References==
{{commons category|Payne Knight Twister}}
* {{cite journal |last=Davisson |first=Budd |title=Hale Wallace's Baby Biplane Bullet |journal=Sport Aviation |date=October 1999 |pages=44}}
* {{cite journal |last=Davisson |first=Budd |title=Biplanes You can Build |journal=EAA Sport Aviation |date=October 2000 }}
*{{cite web |last=Fairbanks |first=Don |title=undated letter |url=http://knighttwister.com/ktimages/fairbanks.letter.t.jpg |accessdate=2009-01-04 }}
* {{cite book |title=Jane's All the World's Aircraft 1977–78 |publisher=Jane's Publishing |location=London}}
* {{cite web |title=Knight Twister Historical Information |work=Steen Aero Lab website |publisher=Steen Aero Lab |location=Palm Bay, Florida |url=http://www.steenaero.com/KnightTwister/history.cfm |accessdate=2009-01-04}}
* {{cite web |last=Payne |first=Vernon |title=Vernon Payne's History of the Knight Twister |work=Steen Aero Lab website |publisher=Steen Aero Lab |location=Palm Bay, Florida |url=http://www.steenaero.com/articles_detail.cfm?ArticleID=7 |accessdate=2009-01-04}}
* {{cite journal |title=Knight Twister Junior |journal=Air Trails |date=July 1949 |pages=34}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |isbn= 0-7106-0710-5}}
* {{cite journal |title=The New Knight Twister |journal=Popular Aviation |date=October 1937 |pages=35}}
<!-- ==External links== -->
{{Aerobatics}}

[[Category:United States sport aircraft 1930–1939]]
[[Category:Homebuilt aircraft]]
[[Category:Aerobatic aircraft]]