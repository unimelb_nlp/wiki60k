{{Onesource|date=October 2014}}
{{Use mdy dates|date=September 2011}}
{{for|the Australian footballer|Eddie Regan}}
{{Infobox Politician
|name= Edward Regan
|image=
|caption=
|imagesize=
|office2 = 8th President of [[Baruch College]] 
|term_start2 = 2000
|term_end2 = 2004
|predecessor2 = Sidney I. Lirtzman
|successor2 = [[Kathleen Waldron]]
|office1 = 1st [[Erie County, New York|Chairman of the Erie County Fiscal Stability Authority]]
|predecessor1 = ''New position''
|successor1 = Anthony Baynes, Sr.
|term_start1 = 2005
|term_end1 = 2006
|office3=51st [[New York State Comptroller|Comptroller of New York]]
|term_start3= January 1, 1979
|term_end3= May 7, 1993
|governor3= [[Hugh Carey]]<br>[[Mario Cuomo]]
|preceded3= [[Arthur Levitt, Sr.]]
|succeeded3= [[Carl McCall]]
|office4 = 3rd [[Erie County, New York|Erie County]] [[County Executive|Executive]]
|term_start4 = 1972
|term_end4 = 1978
|predecessor4 = [[B. John Tutuska]]  
|successor4 = [[Ed Rutkowski]]
|birth_name= Edward Van Buren Regan
|birth_date= {{birth date|1930|5|14}}
|birth_place= [[Plainfield, New Jersey]], U.S.
|death_date= {{death date and age|2014|10|18|1930|5|14}}
|death_place= [[Greenwich, Connecticut]], U.S.
|spouse=
|children=
|party= [[Republican Party (United States)|Republican]]
|alma_mater=
|profession= Legislator, politician
|religion= [[Roman Catholic]]}}

'''Edward Van Buren "Ned" Regan''' (May 14, 1930 – October 18, 2014) was an American politician and public figure from [[New York State]]. He was a member of the [[Republican Party (United States)|Republican Party]].

Regan's political career began on the [[Buffalo Common Council]]. He rose to prominence as the third [[Erie County, New York|Erie County Executive]] during the 1970s. Regan then became [[New York State Comptroller]], and served in that role for nearly 15 years. He appeared on the Republican ticket in five statewide elections, more than any politician in the history of New York. From 2000 to 2004, Regan was president of [[Baruch College]] of the [[CUNY|City University of New York]], where he also served as a professor.

==Life==
Born in [[Plainfield, New Jersey]] to William and Caroline (née Van Buren) Regan, Edward Van Buren Regan was raised in [[Utica, New York]]. He attended [[Nichols School]], a prep school in [[Buffalo, New York]], graduating in 1947.<ref name=NYTObit>McFadden, Robert D. [https://www.nytimes.com/2014/10/19/nyregion/edward-v-regan-longtime-new-york-state-comptroller-dies-at-84-.html?_r=0 "Edward V. Regan, Longtime New York State Comptroller, Dies at 84"], ''[[The New York Times]]'', October 18, 2014. Accessed October 19, 2014. "Edward Van Buren Regan was born in Plainfield, NJ, on May 14, 1930, the oldest of five children of William and Caroline Van Buren Regan. He attended primary school in Utica, N.Y., and graduated from the Nichols School in Buffalo in 1947 and from Hobart College in Geneva, N.Y., in 1952."</ref> 

He graduated in the Hobart Class of 1952 at [[Hobart and William Smith Colleges]], where he was a member of [[Kappa Alpha Society|The Kappa Alpha Society]], and ''[[cum laude]]'' from [[University at Buffalo Law School]] in 1964. In 1970, he was defeated by the incumbent Comptroller [[Arthur Levitt, Sr.]], but was elected [[New York State Comptroller]] in 1978, and re-elected in 1982, 1986 and 1990. He remained in office until May 7, 1993 when he resigned. He was succeeded by [[Carl McCall]] who was elected by the [[New York State Legislature]] to fill the unexpired term.<ref name=NYTObit/>

Prior to becoming Comptroller, Regan served as County Executive of [[Erie County, New York|Erie County]]. He also served as a councilman in Buffalo. Regan was investigated by law enforcement officials after the disclosure of a memo written by members of his staff, one of which pointedly said, "Those who give will get." He denied any impropriety. Regan occasionally talked of running for governor, but never did so. Regan was Chairman of the [[Municipal Assistance Corporation]] for New York City in the 1990s. The corporation was set up in the 1970s to assist with the financial recovery of New York City following the city's fiscal crisis and near bankruptcy.<ref name=NYTObit/>

In the early 1990s, Regan served as a member of the US [[Competitiveness Policy Council]] and ably led its efforts on Corporation Governance. After leaving the comptroller's office, Regan served as a board member of numerous business and nonprofit organizations. He was President of [[Baruch College]] in New York from 2000-04.<ref name=NYTObit/>

After retiring from the Baruch presidency, Regan became a professor at the Graduate Center of the [[City University of New York]]. He served as a trustee of the Financial Accounting Foundation and was a consultant to the Chairman of the Financial Accounting Standards Board (FASB) on matters of the convergence of GAAP with international accounting standards. For several months in 2005 and 2006, he served as the first chairman of the Erie County Fiscal Stability Authority, which was set up by the state in order to oversee the county's finances and make recommendations to the county government on financial affairs. The authority, considered a "soft" control board, was created in response to the Erie County fiscal crisis of 2005.<ref name=NYTObit/>

In January 2007, he served on the search committee for a new State Comptroller, following the resignation of Comptroller [[Alan Hevesi]]. The other search committee members were former State Comptroller [[Carl McCall]] and former New York City Comptroller [[Harrison J. Goldin]]. The committee recommended New York City Finance Commissioner [[Martha Stark]], Nassau County Comptroller [[Howard Weizman]] and businessman [[William Mulrow]] to the State Legislature for consideration, but the Legislature elected [[Thomas DiNapoli]] instead.<ref name=NYTObit/>

Regan was a faculty member at the City University of New York (CUNY), holding the title of "Distinguished Professor" at Baruch College and the Graduate Center, and also teaching at the Macaulay Honors College on the civic and economic issues affecting New York City. He was a consultant to the chairman of the Financial Accounting Standards Board (FASB) working on a project with the International Accounting Standards Board (IASB) to create a global set of high-quality financial reporting standards. He was active in many civic organizations such as the [[Council on Foreign Relations]], the Committee for Economic Development and the New York Economic Club.<ref name=NYTObit/>

==Death==
On October 18, 2014, Regan died at a hospital in [[Greenwich, Connecticut]] at the age of 84. At the time of his death he had [[Alzheimer's disease]] and lived in a retirement home in [[Rye, New York]].<ref name=NYTObit/>

==Political campaigns==
===1970 NYS Republican ticket===
*Governor: [[Nelson Rockefeller]]
*Lieutenant Governor: [[Malcolm Wilson (New York)|Malcolm Wilson]]
*Comptroller: Edward Regan
*Attorney General: [[Louis Lefkowitz]]
*U.S. Senate: [[Charles Goodell]]

===1978 NYS Republican ticket===
*Governor: [[Perry B. Duryea, Jr.]]
*Lieutenant Governor: [[Bruce Caputo]]
*Comptroller: Edward Regan
*Attorney General: [[Michael Roth (US politician)|Michael Roth]]

===1982 NYS Republican ticket===
*Governor: [[Lewis Lehrman]]
*Lieutenant Governor: [[James L. Emery]]
*Comptroller: Edward Regan
*Attorney General: [[Frances Sclafani]]
*U.S. Senate: [[Florence Sullivan]]

===1986 NYS Republican ticket===
*Governor: [[Andrew O'Rourke]]
*Lieutenant Governor: [[E. Michael Kavanagh]]
*Comptroller: Edward Regan
*Attorney General: [[Peter T. King]]
*U.S. Senate: [[Alfonse D'Amato]]

===1990 NYS Republican ticket===
*Governor: [[Pierre Rinfret]]
*Lieutenant Governor: [[George Yancey]]
*Comptroller: Edward Regan
*Attorney General: [[Bernard C. Smith]]

==References==
{{Reflist}}

==Sources==
*[https://query.nytimes.com/gst/fullpage.html?res=950DE6DA1F3AF93AA35750C0A96F948260&sec=&spon=&pagewanted=1] The campaign finance controversy, in NYT on March 9, 1989
*[http://media.www.theticker.org/media/storage/paper909/news/2004/02/02/News/Baruch.President.Ned.Regan.To.Step.Down.In.Fall.2005-1780061.shtml] His resignation from Baruch, in The Ticker on February 2, 2004
*[https://query.nytimes.com/gst/fullpage.html?res=9F0CE1D6173DF93AA25751C0A965958260] His resignation announced, in NYT on February 19, 1993

{{s-start}}
{{s-off}}
{{succession box | title = [[Erie County, New York|Erie County Executive]] | before = [[B. John Tutuska]] | after = [[Ed Rutkowski]] | years = 1972–1978}}
{{succession box | title = [[New York State Comptroller]] | before = [[Arthur Levitt, Sr.]] | after = [[Carl McCall]] | years = 1979–1993}}
{{succession box | title = [[Erie County, New York|Chairman of the Erie County Fiscal Stability Authority]] | before = New Position | after = [[Anthony Baynes, Sr.]] | years = 2005–2006}}
{{s-aca}}
{{succession box | title = [[Baruch College|President of Baruch College]] | before = [[Sidney I. Lirtzman]] | after = [[Kathleen Waldron]] | years = 2000–2004}}
{{s-end}}


{{DEFAULTSORT:Regan, Edward V}}
[[Category:1930 births]]
[[Category:2014 deaths]]
[[Category:American people of Irish descent]]
[[Category:American Roman Catholics]] <!-- per funeral/burial info -->
[[Category:New York State Comptrollers]]
[[Category:People from Utica, New York]] 
[[Category:Presidents of campuses of City University of New York]]
[[Category:New York Republicans]]
[[Category:Erie County Executives]]
[[Category:Buffalo Common Council members]]
[[Category:Hobart and William Smith Colleges alumni]]
[[Category:Disease-related deaths in Connecticut]]