{{Infobox journal
| title = Clinical Nursing Research
| cover = [[File:Clinical Nursing Research.jpg]]
| editor = Pamela Z. Cacchione
| discipline = [[Nursing]]
| former_names = 
| abbreviation = Clin. Nurs. Res.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1992-present
| openaccess = 
| license = 
| impact = 1.278
| impact-year = 2014
| website = http://www.uk.sagepub.com/journals/Journal200890?siteId=sage-uk&prodTypes=any&q=Clinical+Nursing+Research&fs=1
| link1 = http://cnr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://cnr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1054-7738
| eISSN = 
| OCLC = 645295681
| LCCN = 
}}
'''''Clinical Nursing Research''''' is a quarterly [[Peer review|peer-reviewed]] [[nursing journal]] covering the field of [[Nursing|clinical nursing]]. The [[editor-in-chief]] is Pamela Z. Cacchione ([[University of Pennsylvania]]). It was established in 1992 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Academic Premier]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[Science Citation Index]]
* [[Social Sciences Citation Index]]
* [[Scopus]]
According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 1.278, ranking it 30 out of 110 journals in the category "Nursing" (Science)<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Nursing |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> and 26 out of 108 journals in the category "Nursing" (Social Sciences).<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Nursing |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://cnr.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:General nursing journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1992]]