[[File:PresenceSIHM.jpg|right|200px|thumb|International Society for the History of Medicine presence]]
The '''International Society for the History of Medicine''' is a non profit [[international society]] devoted to the academic study of the [[history of medicine]], including the organization of international congresses.

The society is present in 50 countries,<ref name="GlobeFoundations" /> holds delegations in 38 countries, and has about 800 members. It also includes national societies in Argentina, Belgium, Chile, Finland, France, Greece, Mexico, Morocco, Romania, Turkey, and the United Kingdom. Membership is open to both [[physician]]s and [[historian]]s.

== International congresses ==
The society holds a biennial International Congress, and, beginning in 2001, an international meeting in the years the main conference is not held.[http://www.biusante.parisdescartes.fr/ishm/eng/acc_hist.htm See list] Communications to the international congresses are [[peer review]]ed.<ref name="GlobeFoundations">Brief of the Globe Foundations,vol 13: " 1990, to the last decade of the Century", pages 10&11</ref><ref>2009 Edmond Gravenor and the Council of World Organizations, Review of the International Societies and Organizations of the XX century, page 345-346 [[Northampton]] Press, UK</ref>

== ''Vesalius'' ==
The ISHM publishes twice a year ''Vesalius'', subtitled ''Acta Internationalia Historiae Medicinae'', an [[academic journal]] publishing some abstracts from its International Congresses and International Meetings for the History of Medicine, and some other scientific communications.

== Presidents ==
{{columns-list|colwidth=30em|
* 1921-1930: [[Jean-Joseph Tricot-Royer]]
* 1930-1936: [[Davide Giordano]]
* 1936-1946: [[Victor Gomoiu]]
* 1946-1953: [[Maxime Laignel Lavastine]]
* 1953-1964: [[Ernest Wickersheimer]]
* 1964-1968: [[Adalberto Pazzini]]
* 1968-1971: [[Maurice Bariety]]
* 1971-1976: [[Noël Poynter]]
* 1976-1980: [[De la Broquerie Fortie]]
* 1980-1984: [[Jean-Charles Sournia]]
* 1984-1992: [[Hans Schadewaldt]]
* 1992-1996: [[John Cule]]
* 1996-2000: [[Ynez Viole O'Neill]]
* 2000-2004: [[Jean-Pierre Tricot]]
* 2004-2008: [[Athanasios Diamandopoulos]]
* 2008–present: [[Giorgio Zanchin]]
}}

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.biusante.parisdescartes.fr/ishm/eng/}}
* [http://www.vesalius.org.uk ''Vesalius'']

[[Category:History of medicine]]
[[Category:History organizations]]