{{Orphan|date=July 2015}}
[[File:Jinx and Darlene.jpg|thumb|Jinx and Darlene in 1959]]
'''Jinx''' (1953–19??) was a performing [[chimpanzee]], and member of the ice skating duo "Darlene and Jinx", with his owner and trainer Darlene Sellek.<ref>New Orleans Times-Picayune, "Chimp Heads Three-Act Program at Beach Today", 9 Apr 1955</ref>

==Early life==
Jinx – a member of the [[Common chimpanzee|Pan Troglodyte]] species – was born in late 1953 in the [[Belgian Congo]], Africa, near Kinshasa (then called Leopoldville).  Jinx was purchased for $600 (f.o.b. Africa) from a Miami, Florida, pet shop when he was 2 month old. “We had no idea of going into show business.  We just wanted a pet”, explained Darlene Sellek in an interview during one of the act’s [[Ice Capades]] appearances in 1957.<ref>Chicago American, "World’s 1st Chimp on Ice Is Real Cool", 13 Apr 1957</ref>

==Performing career==
Jinx and Darlene first appeared as an act at “The Homecoming” in Dekalb, Illinois, in August, 1954, when Jinx was 10 months old.<ref>The Hinckley Review, "A Surprise Act", 12 Aug 1954</ref> Jinx showed off his ability to walk a tightrope upside down and blindfolded. 4 months later, the act – billed as “Darlene and Jinx” – performed at the [[The Chase Park Plaza Hotel|Chase Hotel]] in St. Louis, where the pair began a significant relationship with the major American circus impresario and director [[Al Dobritch]].

In late January, Darlene started to train Jinx to ice skate, with the help of [[Baptist Schreiber]], who is better known for his training of elephants. A few other show-business chimpanzees in the world were known to roller skate, but Jinx was the first to skate on ice. He first appeared on ice skates in the “Hollywood Ice Review” at [[Chicago Stadium]] in February, 1955, with the World and Olympic Ice Skating Champion [[Gundi Busch]].

The relationship with Al Dobritch provided a number of bookings at various [[Shrine Circus|Shriner Circuses]] around the USA over the next few years, the first of which was the Zuhrah Shriner Circus in Minneapolis, Minnesota, at the end of February, 1955. The spring of 1955 found Darlene and Jinx in New Orleans at [[Pontchartrain Beach]], working alongside Runyon and Edwards, and “Daddy Longlegs”, the man with the longest legs in the world.<ref>New Orleans Times-Picayune, "Chimp Heads Three-Act Program at Beach Today", 9 Apr 1955</ref>

By June, 1955, Al Dobritch had negotiated a television contract for the act; Darlene and Jinx had signed a 3-year contract with “[[Super Circus]]” television show, produced in Chicago for station WBKB, “Chicago’s Family Station”.<ref>Billboard, "‘Super Circus’ Adds Chimp to Regulars", 2 Jul 1955</ref>  While with Super Circus, Jinx worked with the children's television pioneer Claude Kirchner and the ever popular [[Mary Hartline]].  Darlene and Jinx were forced to leave the show after only 5 months when the production moved from Chicago to New York in November, 1955.

Jinx visited New York for the first time in late 1955 when Darlene appeared on the “[[Whats My Line|What’s My Line]]?” television game show (Game 2, Season 7, Episode 282), hosted by [[John Daly (radio and television personality)|John Daly]].  During that visit, Jinx was inducted into the American Airlines “Sky Cradle Club” for the flight from New York to Chicago in November, 1955.<ref>Houston Chronicle, "The Show Goes On", 11 Nov 1955</ref>

In November, 1955, Jinx began regular appearances with [[Ned Locke]] on the “Captain Hartz and His Pets” television show produced in Chicago for television station WMAQ, sponsored by the [[Hartz Mountain Industries|Hartz Mountain]] pet care company.  (Jinx first appeared with Ned Locke as his WMAQ radio disc jockey assistant – the world’s first chimpanzee radio personality.)

From the end on 1955 through mid-1956, Darlene and Jinx appeared at a number of Shriners Circuses across the USA, including 
* the Rizpah Shrine Circus in Madisonville, Kentucky,  
* the Al Chymia Shrine Circus in Memphis, Tennessee 
* the Arabia Temple Shrine Circus in Houston, Texas,  
* the Shrine Circus in Topeka, Kansas, and  
* the Hamid Morton Shrine Circus at Hunt Armory in Bloomsbury, Pennsylvania.<ref>Pittsburg Sun-Telegraph, "Chimp Writes Own Story", 17 Apr 1956</ref>

In July, 1956, Darlene and Jinx appeared at [[Belmont Park, Montreal|Belmont Park]] in Montreal, Quebec.  This was followed in August at the [[Canadian National Exhibition]] (CNE) where they appeared with the television cowboy star [[Gene Autry]], and then in September at the 4th annual Sudbury Rotary Exhibition in Sudbury, Ontario.<ref>Sudbury Daily Star, (untitled), 14 Sep 1956</ref>

New Year, 1957, found Darlene and Jinx at Jack Valentine’s Supper Club Restaurant in Ft. Lauderdale, Florida, for a 3-month engagement with Jack Valentine’s Ice Show.  Here he performed with the great figure skater Jack Paul, who is quoted saying “Jinx outdoes me in figure work”.<ref>Miami Herald, "Night Life", George Bourke, 12 Jan 1957</ref>

In March 1957, the duo appeared on the [[Jackie Gleason Show]] “Cavalcade of Circuses”.  In April, Darlene and Jinx made their first appearance in the Ice Capades at Chicago Stadium.<ref>Chicago American, "World’s 1st Chimp on Ice Is Real Cool", 13 Apr 1957</ref>   In July, they appeared in Las Vegas at [[The Sands]] Cirque Room and the [[El Cortez (Las Vegas)|Hotel El Cortez]] “Rhythm On Ice Revue”.

Back in Chicago in January, 1958, Darlene and Jinx filled in for “The Three Adaros” at the [http://www.nmfrc.org/subs/history/may1953b.cfm American Electroplaters Society] 46th Educational Session and Banquet and performed an act called “Hi Jinx” in the Merriel Abbott Ice Revue at the [[Hilton Chicago|Conrad Hilton Hotel]].

Darlene and Jinx appeared on America’s then most popular television variety program [[The Ed Sullivan Show]] “John Harris’s Ice Capades Of 1958”, broadcast on CBS television from [[Madison Square Garden]] in New York City on Sunday, 7 September 1958.

==Retirement==
As Jinx aged, he began to become less controllable.  At a Christmas Show in New York in 1960, he exhibited greater aggression toward his young audience. [https://news.google.com/newspapers?nid=1961&dat=19601218&id=W-ciAAAAIBAJ&sjid=v4wFAAAAIBAJ&pg=1067,2691924&hl=en]

Jinx’s 1966 appearance in “Jamboree On Ice” at [[Indian Rocks Beach|Indian Rocks]] Palm Garden Restaurant, St. Petersburg, Florida, marked the end of his performing career. [https://news.google.com/newspapers?nid=888&dat=19660112&id=iexRAAAAIBAJ&sjid=KXQDAAAAIBAJ&pg=7031,792055&hl=en]  He retired in 1967 to the [[St. Louis Zoo]].

== Co-stars ==
* [[Gene Autry]]
* [[Professor Backwards]]
* [[Gundi Busch]]
* [[Leo Carrillo]] 
* [[Jill Corey]]
* [[John Daly (radio and television personality)|John Daly]]
* [http://www.circopedia.org/Lola_Dobritch Al Dobritch] 
* [http://www.circopedia.org/Lola_Dobritch Lola Dobritch]
* [[Sands Hotel and Casino|Jack Entratter]]
* [[Georgia Gibbs]]
* [[Great Wallendas]]
* [[Mary Hartline]]
* [[Peter Lind Hayes]]
* [[Mary Healy (entertainer)|Mary Healy]]
* [[Miss America award winners|Joan Hyldoft]]
* [[Dennis James]]
* [[Emmett Kelly]]
* [[Super Circus|Claude Kirchner]]
* [[Ned Locke]] 
* [http://www.circopedia.org/William_Woodcock,_Jr. Miller & Woodcock Elephants]
* [[Garry Moore]] 
* [[Jane Morgan]]
* [[Hugh O'Brian|Hugh O’Brian]] 
* [[Joe Pasternak]]
* [[Johnnie Ray]]
* [[Duncan Renaldo]] 
* [[Bob Reynolds (American football, born 1914)|Bob Reynolds]]
* [[Sharon Kay Ritchie]]
* [[Adrian Swan]]
* [[Sophie Tucker]]

== Performances ==
{| class="wikitable"
|-
! Date !! Location !! Venue !! Event
|-
| 12 Aug 1954 || Dekalb, IL || || The Homecoming
|-
|26 Dec 1954 || St Louis || Chase Hotel || 
|-
|01 Feb 1955 || Chicago || Chicago Stadium || Hollywood Ice Revue
|-
|28 Feb 1955 || Minneapolis || Minneapolis Auditorium || Zuhrah Shrine Circus
|-
|01 Apr 1955 || New Orleans || Pontchartrain Beach || 
|-
|02 Jul 1955 || Chicago ||  || Super Circus TV Show
|-
|31 Jul 1955 || Chicago ||  || Super Circus TV Show
|-
|01 Oct 1955 || Madisonville, KY || Municipal Baseball Park || Rizpah Temple Shrine Circus
|-
|30 Oct 1955 ||  ||  || Garry Moore Show "Zoo Parade"
|-
|11 Nov 1955 || Houston || Sam Houston Coliseum || Arabia Temple Shrine Circus
|-
|21 Nov 1955 || Chicago || Chicago Heart Assn || Hilton Hotel
|-
|13 Dec 1955 || Madisonville, KY || Municipal Park || Rizpah Shrine Circus
|-
|12 Feb 1956 || Memphis ||  || Al Chymia Shrine Circus
|-
|01 Mar 1956 || Kansas City || Municipal Auditorium || Police Circus
|-
|02 Mar 1956 || Tokeka, KA ||  || Shrine Circus
|-
|16 Mar 1956 || Wichita, KA ||  || Shrine Circus
|-
|17 Apr 1956 || Bloomsbury, PA || Hunt Armory || Hamid Morton Shrine Circus
|-
|07 Jul 1956 || Montreal || Belmont Park || 
|-
|18 Jul 1956 || Minneapolis || Golden Strand || Aqua Follies
|-
|18 Aug 1956 || Toronto || CNE || 
|-
|12 Sep 1956 || Toronto || CNE || 
|-
|14 Sep 1956 || Sudbury, Ontario ||  || 4th annual  Sudbury Rotary Exhibition
|-
|12 Jan 1957 || Ft. Lauderdale || Jack Valentine's Supper Club Restaurant || Valentine's Ice Show
|-
|08 Mar 1957 || Ft. Lauderdale || Jack Valentine's Supper Club Restaurant || Valentine's Ice Show
|-
|30 Mar 1957 || Buffalo, NY || Buffalo Shrine Circus || Cavalcade Of Circuses
|-
|13 Apr 1957 || Chicago || Chicago Stadium || Ice Capades
|-
|13 Jul 1957 || Las Vegas || The Sands Cirque Room || 
|-
|20 Jul 1957 || Las Vegas || Hotel El Cortez || Rhythm On Ice Revue
|-
|02 Dec 1957 ||  || Municipal Auditorium || 
|-
|04 Jan 1958 || Chicago || Conrad Hilton Hotel || Merriel Abbott Ice Revue
|-
|25 Jan 1958 || Chicago || Conrad Hilton Hotel || American Electroplaters Society 46th Educational Session and Banquet
|-
|07 Oct 1958 || New York || Madison Square Garden || Ed Sullivan Show
|-
|11 May 1959 || Philadelphia || Willow Grove Park || 
|-
|24 Aug 1959 || Toronto || CNE || 
|-
|25 Jul 1960 || Hagarstown MD || Hagarstown Fair || Ice Skating Extravaganza
|-
|18 Dec 1960 || New York || Manhattan Savings || Christmas Show
|-
|12 Jan 1966 || St. Petersburg || Indian Rocks || Jamboree On Ice
|}

== References ==

{{Reflist}}
*The information contained in this entry was obtained from a scrap book of newspaper clippings collected by Darlene Nay (formerly Darlene Sellek) during Jinx's career as a performing artist.

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Chimpanzees]]
[[Category:Duos]]