{{Infobox musical artist <!-- See Wikipedia:WikiProject Musicians -->
|name = Budd Albright
|image =|thumb|Budd Albright
|caption = 
|image_size =
|background = solo_singer
|birth_name = Forrest Edwards Albright
|birth_date = {{birth date and age|mf=yes|1936|8|18}}
|birth_place =[[Elkhart, Indiana]], United States
|genre = [[Rock n Roll]]
|occupation = Singer, [[actor]], [[stuntman]], action coordinator, writer, sportsman
|instrument = 
|years_active = 1961–1974
|label = [[RCA Victor]]
|website = {{URL|http://www.buddalbright.com/}}
|associated_acts =
}}

'''Budd Albright''' (born August 18, 1936) is an American actor, singer, stunt man, and sportsman.

==Early years==
Budd Albright was born '''Forrest Edwards Albright''' in [[Elkhart, Indiana]] of Dutch-Irish and Italian parents. His father, Forrest (Buzz) Albright, was an athlete who played professional baseball for the [[St. Louis Cardinals]] organization and was inducted into the Elkhart County Sports Hall of Fame for his prowess in baseball, basketball and track.<ref name="Elkhart">{{cite web|url=http://www.elkhartcountyhof.com/n-ABCDE-Folder-Forrest--Albright.html |title=Forrest Albright - Elkhart County, Indiana, Sports Hall of Fame |publisher=Elkhartcountyhof.com |date= |accessdate=2012-10-07}}</ref> He went on to play basketball for the Pittsburgh Panthers. His mother (Margaret) Jeanne Rutter was a would be actress who met Budd's father after a local theater performance.<ref name="Budd">{{cite web|url=http://www.buddalbright.com/ |title=Official Website for Budd Albright, Actor, Sportsman |publisher=Buddalbright.com |date=1936-08-18 |accessdate=2012-10-07}}</ref>

When Budd was born in 1936 the young family moved to [[Los Angeles]]. Budd had two younger brothers Brian and Tom (both deceased). Times were tough and they lived in a small apartment on Melrose Avenue that overlooked the back lot of [[Paramount Studios]].<ref>{{cite web|url=http://www.ancestry.com/1940-census/usa/California/Forrest-Albright-Junior_2kq60m |title=Forrest Albright Junior in the 1940 Census |publisher=Ancestry.com |date= |accessdate=2012-10-07}}</ref> Budd's parents worked hard and Budd spent a lot of time with his grandmother Viola, who had been [[General Douglas MacArthur|General Douglas MacArthur's]] personal secretary and had graduated from the [[Chicago Art Institute]].  She exposed Budd to films, museums and upscale restaurants of the day. The War took the family back to [[Cleveland, Ohio]] where his father worked for [[Republic Aviation]]<ref>{{cite web|url=http://www.centennialofflight.net/essay/Aerospace/Republic/Aero43.htm |title=Republic Aviation |publisher=centennialofflight.net |date= |accessdate=2012-10-07}}</ref> building the [[P-47 Thunderbolt]] fighter plane.<ref name="Budd" />

At the end of the war the family moved back to Southern California and settled in [[Long Beach, California|Long Beach]] where Budd attended David Starr Jordan High School<ref name="pipl">{{cite web|url=https://pipl.com/directory/name/Albright/Budd/ |title=Budd Albright - Pipl Directory |publisher=Pipl.com |date= |accessdate=2012-10-07}}</ref> and hung out with the late [[Bob Denver]]. They both were kicked out of acting class for horsing around. In 1953 his family returned to [[Cleveland]] and Budd attended Willoughby Union High School<ref name="pipl" /> for two years. One night, after watching [[The Wild One]] with [[Marlon Brando]] in a Cleveland movie theater, Budd decided to get out of the cold and return to Southern California to give movies a try.<ref name="Budd" />

In 1955 Budd returned to LA, parked cars in [[Beverly Hills]], pumped gas in [[Pacific Palisades, Los Angeles|Pacific Palisades]] and joined [[Richard Boone|Richard Boone's]] acting class on a tip from actor [[Billy Gray (actor)|Billy Gray]]. His roommate at the time was another struggling actor [[Doug McClure]].<ref name="Budd" />

==Career==

===Music===
In 1958, Budd landed a recording contract with [[RCA Victor]].<ref>{{cite web|url=http://www.rocky-52.net/chanteurs/albright_budd.htm |title=Budd Albright |publisher=Rocky-52.net |date= |accessdate=2012-10-07}}</ref> He recorded the Rockabilly songs: "Adrienne"<ref>{{cite web|url=https://www.youtube.com/watch?v=9wcbhOCuRYcHe |title=Budd Albright - Adrienne |publisher=YouTube |date= |accessdate=2012-10-07}}</ref> and "Got No Sunshine in My Soul".<ref>{{cite web|url=https://www.youtube.com/watch?v=43sh1wQRs5c |title=Budd Albright - Got No Sunshine In My Soul ~ Rockabilly |publisher=YouTube |date= |accessdate=2012-10-07}}</ref><ref>{{cite web|url=http://rcs-discography.com/rcs/artist.php?key=albr1000 |title=Albright, Budd (RCS Artist Discography) |publisher=Rcs-discography.com |date= |accessdate=2012-10-07}}</ref>

Budd, along with actor and recording artist [[Steve Rowland (record producer)|Steve Rowland]] and sax player Chuck Rio formed the Hollywood band "The Exciters."  They played all the hot spots around the [[Sunset Strip]] and LA club circuit.<ref name="Budd" /><ref>{{cite web|url=http://www.steverowland-action.com/biog/index.html |title=Steve Rowland - Biography |publisher=Steverowland-action.com |date= |accessdate=2012-10-07}}</ref>  On at least one occasion, their band created near havoc while playing at the Encore Club when the police and fire department were called out to arrest couples twisting in the street.

===Acting===
Budd Albright began his acting career in 1961, with a small part in the Warner's film ''[[Lad, A Dog (film)|Lad: A Dog]].''<ref>{{cite web|title=Lad: A Dog at IMDB |url=http://www.imdb.com/title/tt0056162/|publisher=IMDb.com|accessdate=2012-10-07}}</ref> In 1962, Budd appeared in five episodes of the television series "The Lively Ones," a musical variety show hosted by [[Vic Damone]]. For the next few years, Budd played bit parts in various television series, including a bad guy in [[McCloud (TV series)|McCloud]], a gang leader in [[The Outcasts (TV series)|The Outcasts]], and an officer in The Reluctant Astronaut.  He was even the Belair Cigarette Man on TV commercials and magazine ads for a few years.<ref name="Budd" />

In 1964 Budd was contacted by friend [[James Drury]], who is best known for his title role in [[The Virginian (TV series)|The Virginian]].  Drury told Budd that [[Clu Gulager]] was producing [[Bye Bye Birdie (musical)|Bye Bye Birdie]] for summer stock in [[North Carolina]] and Budd got the part of Conrad Birdie, one of the leads.<ref name="Budd" />

In 1966, he appeared in two episodes during the first season of [[Star Trek: The Original Series|Star Trek]] as an actor and was killed off in both shows: as Security Guard Rayburn in ''[[What Are Little Girls Made Of?]]'', which was directed by [[James Goldstone]] and written by [[Robert Bloch]] and [[Gene Roddenberry]]<ref name=Aveleyman>{{cite web|url=http://www.aveleyman.com/ActorCredit.aspx?ActorID=26707 |title=Budd Albright |publisher=Aveleyman |date=1966-10-20 |accessdate=2012-10-07}}</ref> and as Barnhart the navigator in ''[[The Man Trap]],'' which was directed by [[Marc Daniels]] and written by [[George Clayton Johnson]] and [[Gene Roddenberry]].<ref name="Aveleyman" />

===Stunt man===
The early 1960s produced a flourish of war films that were perfect for young, up and coming stuntmen. Budd spent a year living with the late [[Peter Breck]], who starred in [[The Big Valley]] and his wife Diane in the [[San Fernando Valley]] and would get together on weekends with friends [[Robert Fuller (actor)|Robert Fuller]], [[James Stacy]], Chuck Courtney, actor/stuntmen Jerry Summers and Ronnie Rondell riding dirt bikes and partying at Bob Fuller's house. It was Jerry Summers that convinced Budd to try his hand at stunt work.<ref name="Budd" /> Budd stunt doubled [[Robert Vaughn]], [[Robert Wagner]], [[Warren Beatty]] and Chris George. He worked in ''[[What Did You Do in the War, Daddy?]], [[Beau Geste (1966 film)|Beau Geste]], [[First to Fight (film)|First to Fight]], [[Tobruk (1967 film)|Tobruk]], [[Ice Station Zebra]]'' and ''[[There Was a Crooked Man (film)|There Was a Crooked Man]]'' and was part of the original '''[[Rat Patrol]]''' Stunt Team.<ref>{{cite web|title=IMDB Budd Albright |url=http://www.imdb.com/name/nm0017003/bio|publisher=IMDb.bom|accessdate=2012-10-07}}</ref>

By the late sixties and early seventies [[Universal Studios]] was bursting at the seams with action TV shows and films. Sometimes Budd worked two or three shows at once with parts that included action sequences. Budd logged 31 high falls during that time.  He often did double duty as actor/stuntman or actor/stunt coordinator. He worked as both an actor and the [[Stunt coordinator|Action Coordinator]] on the movies ''Drive Hard Drive Fast'' (1973) and ''[[The Lonely Profession]]'' (1969) for writer/director [[Douglas Heyes]]. From 1968 to 1971, Budd worked as a stunt man in all 76 episodes (and as an actor in five episodes) of the groundbreaking TV series, ''[[The Name of the Game (TV series)|The Name of the Game]].''<ref name="game">{{cite web|title=The Name of the Game|publisher=IMDb.com|url=http://www.imdb.com/title/tt0062591/fullcredits#cast|accessdate=2012-10-07}}</ref> It was a pioneering [[wheel series]] of 90 minute episodes rotating around three main characters played by [[Tony Franciosa]], [[Gene Barry]], and [[Robert Stack]].  Other actors who appeared on some episodes during the series included [[Peter Falk]], [[Robert Culp]], [[Robert Wagner]], [[Darren McGavin]], [[Susan Saint James]], [[Mark Miller (actor)|Mark Miller]], [[Ben Murphy]], [[William Shatner]], [[Vera Miles]], [[Jack Klugman]] and [[Cliff Potts]].<ref name="game" />

Budd has worked with Hall of Fame stuntmen [[Hal Needham]], Ronnie Rondell, Glenn Wilder, Roger Creed and [[Bill Hickman]] who is remembered most for the landmark car chase alongside [[Steve McQueen]] in the 1968 film [[Bullitt]].<ref name="Budd" />

==Personal life==
Budd was briefly married to actress Sharon Lee.  They were married on September 21, 1958, at Santa Catalina Island's Community Church, separated on June 21, 1959, and legally divorced on February 1, 1961. This was Budd's first marriage, but Sharon's fourth. She married her fifth husband shortly after the couple divorced.<ref name="pqarchiver1958">{{cite web|url=http://pqasb.pqarchiver.com/latimes/access/444944012.html?dids=444944012:444944012&FMT=CITE&FMTS=CITE:AI&type=historic&date=Sep+11%2C+1958&author=&pub=Los+Angeles+Times&desc=Sharon+Lee%2C+Budd+Albright+Get+License&pqatl=google |title=Los Angeles Times: Archives - Sharon Lee, Budd Albright Get License |publisher=Pqasb.pqarchiver.com |date=1958-09-11 |accessdate=2012-10-07}}</ref><ref name="pqarchiver1958"/>

Budd has always been active.  He raced Go Karts with [[Paul Newman]], [[Keenan Wynn]] and [[Steve Rowland (record producer)|Steve Rowland]]. He has also raced sports cars and was an [[American Power Boat Association]] (APBA) Grand National Boat Racing Champion. He has enjoyed water skiing, despite narrowly escaping serious injury during a dangerous mishap at Hanson Dam in 1961. In his spare time, he races bicycles and acts as the team captain for the Rinaldi/To Be Healthy cycling team.<ref name="Budd" />

Budd left the acting/stunt business in 1974 and has since worked as a photo journalist with producer Gary Berwin.<ref>{{cite web|title=Gary Berwin|publisher=IMDb.com|url=http://www.imdb.com/name/nm3356693/|accessdate=2012-10-07}}</ref> and has written 32 magazine articles.  In 1994, he formed Strike Team Media, a TV-promotional advertising firm and is currently working on getting his screenplays ''Closest of Enemies'', ''Sea Foam Green'' and a TV series idea ''HLS Coconut Grove'' into production.<ref name="Budd" />

Budd Albright resides in [[Palm Desert, California]] and enjoys traveling often.<ref name="Budd" />

== Filmography ==

===As a stunt man===
* 1974 ''[[Ironside (TV series)|Ironside]]'' (TV series) (stunts - 1 episode) 
::''[[List of Ironside episodes#Season 8: 1974–75|The Over-the-Hill Blues]]'' (1974) (stunts) 
* 1973 ''Drive Hard, Drive Fast'' (TV movie) (stunt coordinator) 
* 1973 ''[[McCloud (TV series)|McCloud]]'' (TV series) (stunts - 1 episode) 
::''[[List of McCloud episodes#Season 3: 1972–73|The Million Dollar Round Up]]'' (1973) (stunts) 
* 1968-1971 ''[[The Name of the Game (TV series)|The Name of the Game]]'' (TV series) (stunt performer - All 76 episodes) 
* 1970 ''[[There Was a Crooked Man...]]'' (stunts - uncredited) 
* 1970 ''[[It Takes a Thief (1968 TV series)|It Takes a Thief]]'' (TV series) (stunts - 1 episode) 
::''[[List of It Takes a Thief (1968 TV series) episodes#Season 3 (1969–1970)|The Suzie Simone Caper]]'' (1970) (stunts) 
* 1969 ''[[The Lonely Profession]]'' (TV movie) (stunt coordinator) 
* 1968 ''[[Ice Station Zebra]]'' (stunts - uncredited) 
* 1966-1968 ''[[The Rat Patrol]]'' (TV series) (stunts - All 58 episodes) 
* 1967 ''[[Tobruk (1967 film)|Tobruk]]'' (stunts - uncredited) 
* 1967 ''[[First to Fight (film)|First to Fight]]'' (stunt performer) 
* 1966 ''[[Beau Geste (1966 film)|Beau Geste]]'' (stunts - uncredited) 
* 1966 ''[[What Did You Do in the War, Daddy?]]'' (stunts - uncredited)

===As an actor===
*1973 ''Drive Hard, Drive Fast'' (TV movie) 
::CIA Man & Chase Driver 
*1972 ''[[McCloud (TV series)|McCloud]]'' (TV series) 
::''[[List of McCloud episodes#Season 3: 1972–73|The Barefoot Stewardess Caper]]'' (1972) - Bad Guy 
*1968-1971 ''[[The Name of the Game (TV series)|The Name of the Game]]'' (TV series) - Man
::''The Showdown'' (1971) 
::''The Broken Puzzle'' (1971) 
::''Beware of the Watchdog'' (1971) 
::''Appointment in Palermo'' (1971) 
::''The Savage Eye'' (1971) 
*1969 ''[[The Lonely Profession]]'' (TV movie) - Interviewed Waiter
*1968 ''[[The Outcasts (TV series)|The Outcasts]]'' (TV series) - Gang Leader Tony 
::''A Ride to Vengeance'' (1968) 
*1967 ''[[The Reluctant Astronaut]]'' - Officer (uncredited)
*1966 ''[[Star Trek: The Original Series|Star Trek]]'' (TV series) 
::''[[What Are Little Girls Made Of?]]'' (1966) - Rayburn 
::''[[The Man Trap]]'' (1966) - Barnhart (uncredited) 
*1962 ''The Lively Ones'' (TV series) 
::Vic Damone's Friend 
*1962 ''[[Lad, A Dog (film)|Lad: A Dog]]'' (movie) - Family friend

==References==
{{Reflist|2}}

==External links==
* [http://www.buddalbright.com/ Official website]
* {{IMDb name|0017003|Budd Albright}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Albright, Budd}}
[[Category:American stunt performers]]
[[Category:American male film actors]]
[[Category:1936 births]]
[[Category:Living people]]
[[Category:People from Elkhart, Indiana]]
[[Category:Male actors from Los Angeles]]
[[Category:Male actors from Cleveland]]
[[Category:Male actors from Long Beach, California]]
[[Category:People from Palm Desert, California]]
[[Category:Musicians from Long Beach, California]]