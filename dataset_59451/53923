{{Multiple issues|
{{Refimprove|date=July 2014}}
{{More footnotes|date=March 2015}}
}}

[[Image:UltrasoundProbe2006a.jpg|thumb|right|A linear array ultrasonic transducer for use in [[medical ultrasonography]]]]
'''Ultrasonic [[transducer]]s''' are divided into three broad categories: transmitters, receivers and transceivers.  Transmitters convert [[signal (electrical engineering)|electrical signals]] into ultrasound, receivers convert ultrasound into electrical signals, and transceivers can both transmit and receive ultrasound.  

In a similar way to [[radar]] and [[sonar]], ultrasonic transducers are used in systems which evaluate targets by interpreting the reflected signals. For example, by measuring the time between sending a signal and receiving an echo the distance of an object  can be calculated. Passive ultrasonic sensors are basically microphones that detect ultrasonic noise that is present under certain conditions.

Ultrasonic probes and ultrasonic baths apply ultrasonic energy to agitate particles in a wide range of materials; See '''[[Sonication]]'''.

==Applications and performance==
Ultrasound can be used for measuring wind speed and direction ([[anemometer]]), tank or channel fluid level, and speed through air or water. For measuring speed or direction, a device uses multiple detectors and calculates the speed from the relative distances to particulates in the air or water. To measure tank or channel level, the sensor measures the distance to the surface of the fluid. Further applications include: [[humidifier]]s, [[sonar]], [[medical ultrasonography]], [[burglar alarms]], [[non-destructive testing]] and [[wireless charging]].

Systems typically use a transducer which generates sound waves in the ultrasonic range, above 18&nbsp;kHz, by turning electrical energy into sound, then upon receiving the echo turn the sound waves into electrical energy which can be measured and displayed.

The technology is limited by the shapes of surfaces and the density or consistency of the material. Foam, in particular, can distort surface level readings.<ref name="Ultrasonic Flow Meters and Foam">{{cite web|url=http://www.openchannelflow.com/blog/article/the-heady-problem-of-foam-in-flumes-and-weirs |title=Foam (and how to counter it) in Flumes and Weirs |publisher=Openchannelflow.com |date=2013-03-18 |accessdate=2015-03-17}}</ref><ref name="Ultrasonic Testing Surface Preparation">{{cite web|url=http://www.testexndt.co.uk/services/ultrasonic-testing/|title=Ultrasonic Testing |publisher=testexndt.co.uk |date=2016-08-04 |accessdate=2016-08-04}}</ref>

This technology, as well, can detect approaching objects and track their positions.

==Transducers==
[[Image:Soundfield_Water_4MHz_TransducerRadius5mm.png|thumb|right|Sound field of a non focusing 4&nbsp;MHz ultrasonic transducer with a near field length of N&nbsp;=&nbsp;67&nbsp;mm in water. The plot shows the sound pressure at a logarithmic db-scale.]]
[[Image: Soundfield_Water_4MHz_TransducerRadius5mm_Focus30mm.png|thumb|right|Sound pressure field of the same ultrasonic transducer (4&nbsp;MHz, N&nbsp;=&nbsp;67&nbsp;mm) with the transducer surface having a spherical curvature with the curvature radius R&nbsp;=&nbsp;30&nbsp;mm]]

Ultrasonic transducers convert AC into [[ultrasound]], as well as the reverse.  Ultrasonics, typically refers to [[Piezoelectric sensor|piezoelectric transducers]] or [[Capacitive micromachined ultrasonic transducers|capacitive transducers]]. Piezoelectric crystals change size and shape when a [[voltage]] is applied; AC voltage makes them oscillate at the same frequency and produce ultrasonic sound. Capacitive transducers use electrostatic fields between a conductive diaphragm and a backing plate.

The beam pattern of a transducer can be determined by the active transducer area and shape, the ultrasound wavelength, and the sound velocity of the propagation medium. The diagrams show the sound fields of an unfocused and a focusing ultrasonic transducer in water, plainly at differing energy levels.

Since piezoelectric materials generate a voltage when force is applied to them, they can also work as  ultrasonic detectors. Some systems use separate transmitters and receivers, while others combine both functions into a single piezoelectric transceiver.

Ultrasound transmitters can also use non-piezoelectric principles. such as magnetostriction. Materials with this property change size slightly when exposed to a magnetic field, and make practical transducers.

A capacitor ("condenser") microphone has a thin diaphragm that responds to ultrasound waves. Changes in the electric field between the diaphragm and a closely spaced backing plate convert sound signals to electric currents, which can be amplified.

The diaphragm (or membrane) principle is also used in the relatively new micro-machined ultrasonic transducers (MUTs). These devices are fabricated using silicon micro-machining technology ([[MEMS]] technology), which is particularly useful for the fabrication of transducer arrays. The vibration of the diaphragm may be measured or induced electronically using the capacitance between the diaphragm and a closely spaced backing plate ([[CMUT]]), or by adding a thin layer of piezo-electric material on diaphragm ([[PMUT]]). Alternatively, recent research showed that the vibration of the diaphragm may be measured by a tiny [[optical ring resonator]] integrated inside the diaphragm (OMUS).<ref>
{{cite thesis |type=Ph.D. |last=Westerveld |first=Wouter J |date=2014 |title= Silicon photonic micro-ring resonators to sense strain and ultrasound |publisher=Delft University of Technology |isbn=9789462590793 |doi=10.4233/uuid:22ccedfa-545a-4a34-bd03-64a40ede90ac }}</ref><ref>{{cite journal|authors=S.M. Leinders, W.J. Westerveld, J. Pozo, P.L.M.J. van Neer, B. Snyder, P. O’Brien, H.P. Urbach, N. de Jong, and M.D. Verweij|year=2015|title=A sensitive optical micro-machined ultrasound sensor (OMUS) based on a silicon photonic ring resonator on an acoustical membrane|journal=Scientific Reports|volume=5| pages= 14328|doi= 10.1038/srep14328|bibcode = 2015NatSR...514328L }}
</ref>

==Use in medicine==
{{main|Medical ultrasonography}}
Medical ultrasonic transducers (probes) come in a variety of different shapes and sizes for use in making cross-sectional images of various parts of the body. The transducer may be passed over the surface and in contact with the body, or inserted into a [[body opening]] such as the [[rectum]] or [[vagina]]. Clinicians who perform ultrasound-guided procedures often use a [[probe positioning system]] to hold the ultrasonic transducer.

Air detection sensors are used in various roles.{{elucidate|date=July 2014}} Non-invasive air detection is for the most critical situations where the safety of a patient is mandatory. Many of the variables, which can affect performance of amplitude or continuous-wave-based sensing systems, are eliminated or greatly reduced, thus yielding accurate and repeatable detection.

One key principle in this technology is that the transmit signal consists of short bursts of ultrasonic energy. After each burst, the electronics looks for a return signal within a small window of time corresponding to the time it takes for the energy to pass through the vessel. Only signals received during this period will qualify for additional signal processing. This principle is similar to radar range gating.

==Use in industry==
Ultrasonic sensors can detect movement of targets and  measure the distance to them in many [[automatic factory|automated factories]] and process plants. Sensors can have an on or off digital output for detecting the movement of objects, or an analog output proportional to distance. They can  sense the edge of material as part of a [[Web-guiding systems|web guiding]] system.

Ultrasonic sensors are widely used in cars as [[parking sensor]]s to aid the driver in reversing into parking spaces. They are being tested for a number of other automotive uses including ultrasonic people detection and assisting in autonomous [[UAV]] navigation.{{Citation needed|date = November 2015}}

Because ultrasonic sensors use sound rather than light for detection, they work in applications where [[photoelectric sensor]]s may not. Ultrasonics are a great solution for clear object detection, clear label detection<ref>{{cite web|url=http://www.labelsensors.com/choose/labelSensorTypes.html |title=Label Sensor Types and Technologies, Clear Label Sensor Choice |publisher=Labelsensors.com |date= |accessdate=2015-03-17}}</ref> and for liquid level measurement, applications that photoelectrics struggle with because of target translucence. As well, target color and/or reflectivity do not affect ultrasonic sensors, which can operate reliably in high-glare environments.<ref>[http://www.bannerengineering.com/training/faq.php?faqID=34&div=1 ]{{dead link|date=March 2015}}</ref>

Passive ultrasonic sensors may be used to detect high-pressure gas or liquid leaks, or other hazardous conditions that generate ultrasonic sound. In these devices, audio from the transducer (microphone) is converted down to human hearing range.

High-power ultrasonic emitters are used in commercially available [[ultrasonic cleaning]] devices. An ultrasonic transducer is affixed to a stainless steel pan which is filled with a solvent (frequently water or [[isopropanol]]). An electrical square wave feeds the transducer, creating sound in the solvent strong enough to cause [[cavitation]].

Ultrasonic technology has been used for multiple cleaning purposes.  One of which that is gaining a decent amount of traction in the past decade is ultrasonic gun cleaning.

[[Ultrasonic testing]] is also widely used in metallurgy and engineering to evaluate corrosion, welds, and material defects using different types of scans.

==References==
{{Reflist}}
<ref>{{cite web|title=GunCleaners|url=http://www.guncleaners.com|website=GunCleaners.com}}</ref>

{{Sensors}}
[[Category:Ultrasound]]
[[Category:Sensors]]