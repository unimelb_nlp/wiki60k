{{Infobox food
| name = Acquacotta
| image = File:Acquacotta soup (cropped).jpg
| caption = Acquacotta soup|alt=Acquacotta soup
| alternate_name = 
| country = Italy
| region = [[Maremma]]
| national_cuisine = [[Italian cuisine]]
| creator =           <!-- or | creators = -->
| year = [[Ancient history]]
| mintime = 
| maxtime = 
| type = [[Soup]]
| course = 
| served = Hot
| main_ingredient = Water, [[Staling|stale]] bread, onion, tomato and olive oil
| minor_ingredient = Vegetables and [[Leftovers|leftover]] foods
| variations = ''Aquacotta con funghi'', ''Aquacotta con peperoni''
| serving_size = 100&nbsp;g
| calories = 
| protein = 
| fat = 
| carbohydrate = 
| glycemic_index = 
| similar_dish = 
| other = 
}}
[[File:Acquacotta soup at a restaurant (cropped).jpg|thumb|Acquacotta at a restaurant in [[Milan]], Italy|alt=Acquacotta at a restaurant in Milan, Italy]]
[[File:Acquacotta, bean and minestrone soup.jpg|thumb|Acquacotta, bean and minestrone soup|alt=Acquacotta, bean and minestrone soup]]

'''Acquacotta''' ({{IPA-it|ˌakkwaˈkɔtta|pron}}; [[Italian language|Italian]] for "cooked water") is a [[broth]]-based hot [[soup]] in [[Italian cuisine]] that was originally a [[Peasant foods|peasant food]]. Its preparation and consumption dates back to [[ancient history]], and it originated in the coastal area known as the [[Maremma]] in southern [[Tuscany]] and northern [[Lazio]]. The dish was invented in part as a means to make hardened, [[Staling|stale]] bread edible. In contemporary times, ingredients can vary, and additional ingredients are sometimes used. Variations of the dish include ''Aquacotta con funghi'' and ''Aquacotta con peperoni''.

==History==
Acquacotta is a simple traditional dish originating in the coastal region of Italy known as [[Maremma]],<ref name="Roddy 2016"/> which spans the southern half of Tuscany's coast and runs into northern [[Lazio]].<ref name="Johns"/><ref name="Hazan"/> The word "acquacotta" means "cooked water" in the [[Italian language]].<ref name="Johns"/><ref name="Scicolone"/> It was originally a [[Peasant foods|peasant food]], and has been described as an ancient dish, the recipe of which was derived in part by people who lived in the Tuscan forest working as colliers ([[Charcoal#Production methods|charcoal burners]]), who were typically very poor and are "traditionally among the poorest of people".<ref name="Scicolone"/><ref name="Romer"/><ref name="Nasello 2015"/> It was also prepared and consumed by farmers and shepherds in the Maremma area.{{efn|"Acquacotta means "cooked water," but once you've tasted the sweet rich vegetable flavors of this country soup, you may well wonder why it got its name. For centuries it has been the everyday meal of shepherds and charcoal burners of the&nbsp;..."<ref name="Field 1990"/>}}<ref name="Pasanella"/> Historically, the soup was sometimes served as an [[antipasto]] dish,<ref name="Hazan"/> the first course in an Italian meal. It remains a popular dish in Maremma and throughout Italy.<ref name="Roddy 2016"/>

Acquacotta was invented in part as a means to make [[Staling|stale]], hardened bread edible.<ref name="Romer"/> People that worked away from home for significant periods of time, such as woodcutters and shepherds, would bring bread and other foods with them (such as [[pancetta]] and [[Dried and salted cod|salt cod]]) to hold them over.<ref name="Romer"/> Acquacotta was prepared and used to marinate the stale bread, thus softening it.<ref name="Roddy 2016"/><ref name="Romer"/>

A legend about acquacotta exists in relation to the concept of [[stone soup]], which is generally based upon a premise of a poor traveler who arrived at a village having only a stone, but convinced the villagers to add ingredients to his stone soup, creating acquacotta;<ref name="Nasello 2015"/> variations of the legend exist.<ref name="Nasello 2015"/>

===Ingredients===
Historically, acquacotta's primary ingredients were water, stale bread, onion, tomato and olive oil,<ref name="Hazan"/> along with various vegetables and [[Leftovers|leftover]] foods that may have been available.<ref name="Scicolone"/><ref name="Johns"/> In the earlier 1800s, some preparations used ''[[Verjuice|agresto]]'', a juice derived from half-ripened grapes, in place of tomatoes,<ref name="Romer"/> which were not a common food in Italy prior to "the latter decades of the nineteenth century".<ref name="Romer"/>

==In contemporary times==
Contemporary preparations of acquacotta may use stale, fresh, or toasted bread,<ref name="Johns"/><ref name="Croce"/> and can include additional ingredients such as [[Broth|vegetable broth]], eggs, cheeses such as [[Parmigiano-Reggiano]] and [[Pecorino Toscano]], celery, garlic, basil, beans such as [[cannellini bean]]s, cabbage, kale, lemon juice, salt, pepper, potatoes and others.<ref name="Scicolone"/><ref name="Johns"/><ref name="Roddy 2016"/><ref name="Hazan"/><ref name="Croce"/> Some versions may use edible mushrooms such as porcini,<ref name="Romer"/> wild herbs, and leaf vegetables/greens such as arugula, endive, mint, chard, [[chicory]], dandelion greens, watercress, [[Valerian (herb)|valerian]] and others.<ref name="Croce"/> As the greens boil down, they contribute to the broth's flavor.<ref name="Croce"/> The dish may be topped with a poached egg.<ref name="Roddy 2016"/><ref name="Nasello 2015"/> Contemporary versions may be prepared in advance from a few hours to a day, stored in a cold place or refrigerated, and then reheated prior to serving.<ref name="Hazan"/> It can also be preserved by freezing.<ref name="Vegetarian Times 2013"/>

==Variations==
''Acquacotta con funghi'' is an aquacotta soup variation that uses [[Boletus edulis|porcini]] mushrooms as a primary ingredient.<ref name="Romer"/> Additional ingredients include bread, stock or water, tomato ''conserva'', Parmesan cheese, eggs, ''[[Calamintha nepeta|mentuccia]]'', wild mint, garlic, olive oil, salt and pepper.<ref name="Romer"/> This variation's flavor and aroma has been described as based upon the porcini mushrooms that are used;<ref name="Romer"/> parsley may also be used.<ref name="Romer"/>

''Acquacotta con peperoni'' is an aquacotta soup variation that includes celery, red pepper and garlic.<ref name="Romer"/>

==See also==
{{div col|colwidth=30em}}
* [[Bread soup]]
* [[Food history]]
* [[Garbure]]
* [[List of bread dishes]]
* [[List of Italian soups]]
* [[List of soups]]
* [[Lablabi]]
* [[Migas]]
* [[Ribollita]]
* [[Wodzionka]]
{{div col end}}
{{portalbar|Food|History|Italy}}

==Notes==
{{notelist}}

==References==
{{reflist|30em|refs=
<ref name="Scicolone">{{cite book | url=https://books.google.com/books?id=Wz3iAgAAQBAJ&pg=PA67 | title=The Italian Vegetable Cookbook | publisher=Houghton Mifflin Harcourt | author=Scicolone, Michelle | year=2014 | page=67 | isbn=0-547-90916-0}}</ref>
<ref name="Hazan">{{cite book | url=https://books.google.com/books?id=1FVLv7WS4C4C&pg=PT190 | title=Essentials of Classic Italian Cooking | publisher=Knopf Doubleday Publishing Group | year=2011  | author=Hazan, Marcella|isbn=0-307-95830-2}}</ref>
<ref name="Johns">{{cite book| url=https://books.google.com/books?id=GpZh_63EcLAC&pg=PA64 | title=Cucina Povera: Tuscan Peasant Cooking | publisher=Andrews McMeel Publishing | year=2011  | author=Johns, Pamela Sheldon (contributor) | page=64|isbn=1-4494-0851-6}}</ref>
<ref name="Croce">{{cite book | url=https://books.google.com/books?id=SrZ1cZdG--QC&pg=PA65 | title=Roma: Authentic Recipes from In and Around the Eternal City | publisher=Chronicle Books | author=Croce, Julia della | year=2004 | page=65 | isbn=0-8118-2352-0}}</ref>
<ref name="Romer">{{cite book | url=https://books.google.com/books?id=H_wJw4Zh2XEC&pg=PA103 | title=The Tuscan Year: Life and Food in an Italian Valley | publisher=Macmillan | author=Romer, Elizabeth | year=1989 | pages=103–106 | isbn=0-86547-387-0}}</ref>
<ref name="Pasanella">{{cite book | url=https://books.google.com/books?id=lJm3nHvbw6YC&pg=PA103 | title=Uncorked: One Man's Journey Through the Crazy World of Wine | publisher=Clarkson Potter | author=Pasanella, Marco | year=2012 | pages=103–104 | isbn=0-307-71984-7}}</ref>
<ref name="Roddy 2016">{{cite web | last=Roddy | first=Rachel | title=Rachel Roddy's Tuscan vegetable broth recipe| website=[[The Guardian]] | date=March 29, 2016 | url=https://www.theguardian.com/lifeandstyle/2016/mar/29/acquacotta-tuscan-vegetable-soup-recipe-rachel-roddy | accessdate=April 29, 2016}}</ref>
<ref name="Nasello 2015">{{cite web | last=Nasello | first=Sarah | title=Try Acquacotta soup as a new recipe for the new year | website=[[The Jamestown Sun]] | date=January 10, 2015 | url=http://www.jamestownsun.com/life/food/3652652-try-acquacotta-soup-new-recipe-new-year | accessdate=April 29, 2016}}</ref>
<ref name="Vegetarian Times 2013">{{cite web | title=Acquacotta (Vegetable Soup) | website=[[Vegetarian Times]] | date=April 3, 2013 | url=http://www.vegetariantimes.com/recipe/acquacotta-vegetable-soup/ | accessdate=April 29, 2016}}</ref>
<ref name="Field 1990">{{cite book | last=Field | first=C. | title=Celebrating Italy | publisher=W. Morrow and Company | year=1990 | isbn=978-0-688-07093-9 | url=https://books.google.com/books?id=ga3YAAAAMAAJ&q=%22Acquacotta%22 | page=166}} {{paywall}}</ref>
}}

==External links==
{{commons}}
* [http://www.bbcgoodfood.com/recipes/acquacotta Acquacotta]. [[BBC]].
* [http://www.jamesbeard.org/recipes/acquacotta Acquacotta]. [[James Beard Foundation]].

{{Cuisine of Italy|state=collapsed}}
{{Good article}}

[[Category:Ancient dishes]]
[[Category:Bread soups]]
[[Category:Italian soups]]