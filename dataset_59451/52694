{{for|the topic/subject|Public understanding of science}}
{{Infobox journal
| title = Public Understanding of Science
| cover = [[File:Public Understanding of Science.jpg]]
| editor = [[Massimiano Bucchi]] (University of Trento, Italy)
| discipline = [[Public awareness of science]]
| former_names =
| abbreviation = Public Underst. Sci.
| publisher = [[SAGE Publications]]
| country =
| frequency = Bi-monthly 
| history = 1992–present
| openaccess =
| license =
| impact = 1.724
| impact-year = 2012
| website = http://pus.sagepub.com
| link1 = http://pus.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pus.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0963-6625
| eISSN = 1361-6609
| OCLC = 36297216
| LCCN = 92658626
| CODEN = PUNSEM
}}
'''''Public Understanding of Science''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] that was established in 1992 and is published by [[SAGE Publications]]. It covers topics in the [[Public awareness of science|popular perception of science]], the role of science in society, [[philosophy of science]], [[science education]], and science in [[public policy]]. The [[editor-in-chief]] is [[Massimiano Bucchi]] ([[University of Trento]]).
 
== Abstracting and indexing ==
''Public Understanding of Science'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 1.724, ranking it 12 out of 72 journals in the category "Communication"<ref name="WoS">{{cite book |year=2013 |chapter=Journals Ranked by Impact: Communication |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2014-01-28 |work=Web of Science |postscript=.}}</ref> and 3 out of 41 journals in the category "History & Philosophy of Science".<ref name="WoS1">{{cite book |year=2013 |chapter=Journals Ranked by Impact: History & Philosophy of Science |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2014-01-28 |work=Web of Science |postscript=.}}</ref>

==Criticism==
''Public Understanding of Science'' has been criticised for its lack of commitment to [[open access]], given it publishes research about public understanding and access to scientific knowledge. Journal editors have published reasons for their position in the journal.<ref>{{cite journal |doi=10.1177/0963662512461882 |title=Open access to Public Understanding of Science |year=2012 |journal=Public Understanding of Science |volume=21 |issue=7 |pages=780}}</ref> However debate continues even within the journal's editorial team.<ref>{{cite news |url=https://www.theguardian.com/science/political-science/2013/jun/18/science-policy |title=Open access inaction |author=Jack Stilgoe |format= |work=[[The Guardian]] |accessdate=2013-07-05 |location=London |date=2013-06-18}}</ref>

== References ==
{{Reflist|30em}}

==External links==
* {{Official website|http://pus.sagepub.com/}}

[[Category:Publications established in 1992]]
[[Category:Education journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Bimonthly journals]]
[[Category:Philosophy journals]]
[[Category:English-language journals]]