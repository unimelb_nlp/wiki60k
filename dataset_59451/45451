{{Infobox military person
| name          = Thomas George Lanphier, Sr.
| image         = File:Thomas Lanphier.jpg
| alt           =
| caption       = Col. Thomas George Lanphier, Sr.
| birth_name    = <!-- only use if different from name -->
| birth_date    = {{Birth date|1890|04|16}}
| birth_place   = [[Lohrville, Iowa|Lohrville]], [[Calhoun County, Iowa|Calhoun Co.]], [[Iowa]]
| death_date    = {{Death date and age|1972|10|09|1890|04|16}}
| death_place   = [[San Diego]], [[California]]
| placeofburial = [[Arlington National Cemetery]], [[Arlington, Virginia]]
| allegiance    = [[United States]]
| branch        = [[United States Army Air Corps]]
| serviceyears  = 1914–c.1929<br />c.1941–1943
| rank          = Colonel
| servicenumber = O3727<!--O not Zero-->
| unit          =
| commands      = [[1st Pursuit Group]]<br />[[Selfridge Field]], [[Michigan]]
| battles       = [[World War I]]<br />[[World War II]]
| battles_label =
| awards        =
| memorials     =
| spouse        = Janet Cobb, (m. 1915-div. 1936)<br />Elsa ( -1972 his death)
| relations     = [[Thomas George Lanphier, Jr.]] (1915–1987)<br />Charles Cobb Lanphier (1918–1944)<br />James Francis Lanphier (1920–1969)
| laterwork     = [[United States Department of Veterans Affairs|Veterans Administration]] to 1954.
| module        =
}}

'''Thomas George Lanphier, Sr.''' (April 16, 1890{{spaced ndash}} October 9, 1972) was a retired [[Colonel]] in the [[United States Army Air Corps]], and was [[Commanding Officer]] of [[Selfridge Field]] in [[Michigan]] from late 1924 to early 1926 and was an American Aviation pioneer.<ref name="Longden, 2009" /><ref name="NYT Obit" /> He is buried in [[Arlington National Cemetery]], [[Arlington, Virginia]].

== Early Years ==
Thomas George Lanphier was born April 16, 1890 in [[Lohrville, Iowa|Lohrville]], [[Calhoun County, Iowa|Calhoun]], [[Iowa]] to John Joseph "Jack" Lanphier and Catherine Ann "Kate" Carey. His father, John Joseph "Jack" Lanphier, was born on September 27, 1854 in [[Lucan Biddulph|Biddulph]], [[Middlesex County, Ontario]], [[Canada]]. His grandparents on both sides were from [[Ireland]]. His parents were married February 15, 1882 in Biddulph. They moved to Lohrville, Iowa, where they had six children: Bernard Anthony; Cyril Crawford; Cecilia Margaret; Thomas George Sr.; Basil 'Charles'; and Catherine Loretto.<ref name="Longden, 2009">{{cite news |url= http://www.desmoinesregister.com/article/99999999/FAMOUSIOWANS/90126027/Lanphier-Thomas-Sr-Thomas-Jr- |title= War flights by two Lanphiers made history |newspaper= Des Moines Register |location= Des Moines, Iowa |first= Tom |last= Longden |date= 26 January 2009 }}</ref><ref name="CA1852 p28">{{cite web |url= http://automatedgenealogy.com/census52/View.jsp?id=8994 |title= Census 1852 / Canada West / Huron (county) / 136 Biddulph township / p. 29d, 30a, (59) |website= automatedgenealogy.com |accessdate= 12 April 2015 }}</ref><ref name="CA1852 p30">{{cite web |url= http://automatedgenealogy.com/census52/View.jsp?id=8996 |title= Census 1852 / Canada West / Huron (county) / 136 Biddulph township / p. 30d, 31a, (61) |website= automatedgenealogy.com |accessdate= 12 April 2015}}</ref><ref>{{cite web |url= http://automatedgenealogy.com/census/View.jsp?id=71033 |title= 1901 Canada Census, MIDDLESEX (North/Nord) (#88), Biddulph B-4, Page 4 |website= automatedgenealogy.com |accessdate= 12 April 2015}}</ref>

When Lanphier was twelve years old his family moved to [[Omaha, Nebraska]]. He attended [[Creighton Preparatory School]] and [[Creighton University]] followed by the [[United States Military Academy]], [[West Point, New York]]. While at West Point, he met his future wife Janet Cobb, who was attending [[Vassar College]]. They married February 1, 1915 in New York. Lanphier graduated from West Point in 1914 and was a classmate and friend of [[Dwight D. Eisenhower]].<ref name="Longden, 2009" />

== World War I and interwar period ==
After West Point, Lanphier was stationed in the [[Panama Canal Zone]]. His unit was transferred to France in March 1918 after the US entered World War I. He served in combat in a machine gun unit and was later transferred to the air corps.<ref name="NYT Obit" /> He received pilot training at [[Issoudun Aerodrome]], [[France]]. He returned to the United States June 1, 1919.<ref name="Longden, 2009" />

After World War I, Lanphier was stationed at [[Mitchel Field]], New York in February 1921.<ref>{{cite journal |url= https://archive.org/stream/officersofarmy10adju#page/n192/mode/1up/ |journal= Army List and Directory |year= 1921 |title= Officers of the Army – February 1921 |page= 185}}</ref> He was stationed at [[Henry Post Army Airfield|Post Field]], [[Fort Sill]] later that year until September 1924.<ref>{{cite journal |url= https://archive.org/stream/officersofarmy11adju#page/n477/mode/1up |journal= Army List and Directory |year= 1923 |title= Officers of the Army – March 1923 |page= 194}}</ref> He was transferred to [[Selfridge Field]] in [[Michigan]] by November 1924.<ref>{{cite journal |url= https://archive.org/stream/officersofarmy13adju#page/253/mode/1up |journal= Army List and Directory |year= 1924 |title= Officers of the Army – November 1924 |pages= 215, 253}}</ref>

Later, Lanphier became the commandant of the [[1st Pursuit Group]] at [[Selfridge Field]] in [[Michigan]].<ref name="Longden, 2009" /><ref name="NYT Obit" /> Lanphier was [[Commanding Officer]] of [[Selfridge Field]] in [[Michigan]]<ref name="Longden, 2009" /> from late 1924 to early 1926.

[[File:Wilkins arctic expedition 1926.jpg|thumb|left|Major Lanphier was an unofficial observer during the [[Hubert Wilkins|Wilkins]] Detroit Arctic Expedition, 1926.]]
Major Lanphier was an unofficial observer during a 1926 arctic flying expedition led by [[Hubert Wilkins]].<ref>{{cite news |url= https://news.google.com/newspapers?nid=950&dat=19260310&id=NupPAAAAIBAJ&sjid=41QDAAAAIBAJ&pg=4529,4362106&hl=en |title= Lanphier says Point Barrow will be first critical test for Wilkins, polar flier |agency= Associated Press |newspaper= The Evening Independent |location= [[St. Petersburg, Florida]] |date= March 10, 1926}}</ref><ref>{{cite news |title=Newspaperman |url=http://www.time.com/time/magazine/article/0,9171,721792-2,00.html |journal=[[Time (magazine)]] |date=March 22, 1926 |accessdate=2008-05-06 }}, 1926 Detroit Arctic expedition with [[Hubert Wilkins]].</ref><ref>{{cite journal |journal= [[Time (magazine)|Time]] |date= April 19, 1926 |url= http://content.time.com/time/magazine/article/0,9171,729162,00.html |title= Science: Polar Pilgrims: Apr. 19, 1926 }}, 1926 Detroit Arctic expedition with [[Hubert Wilkins]].</ref>

Lanphier testified in support of General [[Billy Mitchell]] during Mitchell's 1925 [[court-martial]].<ref name="Longden, 2009" />

Lanphier was a friend and business partner of Colonel [[Charles Lindbergh]] and was one of Lindbergh's flying instructors. On July 1, 1927, Lanphier flew the [[Spirit of St. Louis]] on a single flight in the vicinity of Selfridge Field.<ref>{{cite web |url= http://www.charleslindbergh.com/history/log.asp |title= The Log of the Spirit of St. Louis—Charles A. Lindbergh, Pilot |website= charleslindbergh.com |accessdate= 12 April 2015}}</ref> Lanphier was the head of the Transcontinental Air Transport Company (Sept 1928)<ref name="Longden, 2009" /> and in 1931 was the president of Bird Aircraft Corporation, the manufacturer of the [[Brunner-Winkle Bird]].<ref>{{cite news |url= http://www.timesnewsweekly.com/news/2014-10-16/Feature_Stories/Taking_A_Trip_Back_To_Glendales_Aviation_Past.html |title= Taking A Trip Back To Glendale’s Aviation Past |newspaper= Times News Weekly |location= Ridgewood, New York |date= October 16, 2014 |accessdate= 12 April 2015}}</ref> He also gave a statement in the aftermath of the [[Lindbergh kidnapping]]<ref name="Longden, 2009" /> and piloted an airplane during the search for Lindbergh's son.<ref>{{cite news |url= http://www.charleslindbergh.com/ny/19.asp |title= Sleepless Father Persists In Search |date= March 3, 1932 |newspaper= The New York Times via charleslindbergh.com |accessdate= 12 April 2015}}</ref>

In 1933, after retiring from the military, he bought Manhattan's Phoenix Cereal Beverage Company and applied for a license to manufacture 3.2 beer under the brewery's old name of Flanagan-Nay Brewery Corp.<ref>{{cite news |title=Names make news |url=http://www.time.com/time/magazine/article/0,9171,882171,00.html  |work=[[Time (magazine)]] |date= June 26, 1933 |accessdate=2008-05-06 }}</ref> The brewery had been operated by mobsters [[Owney Madden]] and [[Bill Dwyer (mobster)|Bill Dwyer]], since 1925 during [[Prohibition]]. Madden also ordered an airplane and took flight instruction from Lanphier.<ref>{{cite book |url= https://books.google.com/books?id=aEdXAwAAQBAJ&pg=PA93#v=onepage&q&f=false |title= Bootleggers and Beer Barons of the Prohibition Era |first= J. Anne |last= Funderburg |page= 93 |publisher= McFarland |year= 2014 |ISBN= 978-1-4766-1619-3 }}</ref>

In 1936, Lanphier headed [[The Association for Legalizing American Lotteries]], an illegal [[Lotteries in the United States|lottery]].<ref>{{cite journal |url= http://content.time.com/time/magazine/article/0,9171,848481,00.html |journal= [[Time (magazine)|Time]] |title= THE CABINET: Stakes & Sweeps |date= April 20, 1936 }}</ref>

Lanphier was divorced from Janet Cobb-Lanphier on March 19, 1936 in [[Wayne County, Michigan]].<ref>Wayne County, Michigan Divorce Record 60,179. Wayne Certificates 59,534 – 63,829. Ancestry.com. Michigan, Divorce Records, 1897–1952 [database on-line]. Provo, UT, USA: Ancestry.com Operations, Inc., 2014.</ref> Lanphier married Mary E. Werner in [[Summit County, Ohio]], 10 March 1938.<ref>Thomas George Lanphier married Mary E. (Hayes-Werner) 10 March 1938. Vol 75, 1938. Ancestry.com. Summit County, Ohio, Marriage Records, 1840-1980 [database on-line]. Provo, UT, USA: Ancestry.com Operations, Inc., 2010.</ref>

== World War II and later ==
In [[World War II]], Lanphier Sr. returned to the Army as a [[Lieutenant colonel]] and was made the air intelligence officer for Army Chief of Staff General [[George Marshall|George C. Marshall]].<ref name="Longden, 2009" />

He voluntarily entered inactive duty in 1943 and was appointed to the Veterans Administration by General [[Omar Bradley]]. He was a deputy administrator for the Dallas District Office in 1947.<ref>{{cite news |title= VA Official Is Chief Convention Speaker |date= June 5, 1947 |newspaper= The Amarillo Globe-Times |location= Amarillo, Texas |page= 23 }}</ref> He was the Manager of the Dallas District Office in 1950.<ref>{{cite journal |title= Veterans Administration – Texas |journal= US Army Register |year= 1950 |page= 701 |url= https://archive.org/stream/officialregister50unit#page/701/mode/1up/search/lanphier }}</ref> He retired from the VA in 1954.<ref name="NYT Obit">{{cite news |newspaper= [[The New York Times]] |date= October 10, 1972 |agency= Associated Press  |url= https://query.nytimes.com/gst/abstract.html?res=9B01E3DE153DEF34BC4952DFB6678389669EDE |title= Thomas Lanphier, Aviation Pioneer; Colonel Who Helped Chart First Passenger Route Dies}}{{subscription}}</ref>

Lanphier died October 9, 1972 in [[San Diego]], [[California]].<ref name="Longden, 2009" /> He was interred October 17, 1972 in section 11, grave 826-B at [[Arlington National Cemetery]].<ref name="ANC Explorer">{{cite web |url= http://public.mapper.army.mil/ANC/ANCWeb/PublicWMV/ancWeb.html |title= Lanphier, Thomas G. Sr |website= Arlington National Cemetery Explorer |accessdate= April 12, 2015 }}</ref> He was survived by his wife Elsa, his son Thomas Jr., 5 grand children and 2 great-grandchildren.<ref name="NYT Obit" />

== Sons ==
His oldest son [[Thomas George Lanphier, Jr.]] was born while Lanphier, Sr. was in Panama. Lanphier, Jr. was a [[colonel]] and [[flying ace|ace]] [[fighter pilot]] during [[World War II]]. He was involved in [[Operation Vengeance]], the mission to shoot down the plane carrying [[Admiral Yamamoto]], the commander in chief of the [[Imperial Japanese Navy]], April 18, 1943.<ref name="Longden, 2009" /><ref>{{cite news |first= Robert D.|last= McFadden|authorlink= |coauthors= |title=Thomas G. Lanphier Jr., 71, Dies. U.S. Ace Shot Down Yamamoto. |url=https://www.nytimes.com/1987/11/28/obituaries/thomas-g-lanphier-jr-71-dies-us-ace-shot-down-yamamoto.html?pagewanted=all&src=pm |publisher=[[New York Times]] |date=November 28, 1987 |accessdate=2007-07-21}}</ref> He was also buried at Arlington National Cemetery.<ref name="ANC Explorer" />

Another son, Charles Cobb Lanphier, was also a pilot. He was a Captain in the [[United States Marine Corps]] attached to [[VMF-214]] when his [[Vought F4U Corsair|F4U Corsair]] crashed during a mission at [[Bougainville Island]] on August 28, 1943. He was captured and on May 15, 1944 he died of neglect while in captivity at a prison camp in [[Rabaul]], [[Papua New Guinea]].<ref name="Longden, 2009" /> His remains were recovered and 1st Lt. Charles C. Lanphier, USMC was interred at Arlington National Cemetery, April 5, 1949.<ref name="ANC Explorer" /><ref>{{cite book |url= https://books.google.com/books?id=vVhIAgAAQBAJ&pg=PA125#v=onepage&q&f=false |title= Target: Rabaul, The Allied Siege of Japan's Most Infamous Stronghold |first= Bruce |last= Gamble |pages= 125, 364 |publisher= Zenith Press |year= 2013 |ISBN= 978-0-7603-4407-1}}</ref><ref>{{cite web |url= http://www.pacificwrecks.com/aircraft/f4u/02577.html |title= F4U-1 Corsair Bureau Number 02577 |website= pacificwrecks.com |accessdate= April 12, 2015 }}</ref><ref name="Dallas News, 1949">{{cite news |url= http://www.findagrave.com/cgi-bin/fg.cgi?page=pv&GRid=49244314&PIpi=36724076 |title= Marine Pilot's reburial held |newspaper= Dallas News via Find a Grave |date= April 6, 1949 }}</ref>

Lanphier had a third son who was an actor, [[James Lanphier|James Francis Lanphier]]<ref name="Dallas News, 1949" /> born in New York in 1920 and died in California, 1969.<ref>Death record for James F. Lanphier. Date of death, 11 Feb 1969. Mother's maiden name, Cobb. Ancestry.com. California, Death Index, 1940–1997 [database on-line]. Provo, UT, USA: Ancestry.com Operations Inc, 2000.</ref> He was perhaps best known for his role in [[The Pink Panther (1963 film)]].<ref>{{cite web |url= http://www.imdb.com/name/nm0487036/?ref_=tt_cl_t3 |title= James Lanphier |website= imdb.com |accessdate= April 12, 2015 }}</ref>

== References ==
{{Reflist|30em}}

== Further reading ==
*{{cite journal |journal= [[Time (magazine)|Time]] |date= June 26, 1933 |url= http://www.time.com/time/magazine/article/0,9171,882171,00.html |title= People: Thomas George Lanphier}}{{subscription}}
*{{cite news |newspaper= [[The New York Times]] |date= February 6, 1937 |page= 4 |url= https://query.nytimes.com/gst/abstract.html?res=9A01E7DA1638EF3ABC4E53DFB466838C629EDE |title= Lanphier was not in Spain; Major Did Not Fly for Loyalist Forces as Reported}}{{subscription}}, a correction of a Jan. 16, 1937 article.
*{{cite journal |url= https://archive.org/stream/officialarmyregi1924unit#page/338/mode/1up |journal= Official Army Register |date= January 1, 1924 |title= Army Register, 1924 |page= 338}}, listing of dates of rank as of 1924.
 	
{{Authority control}}

{{DEFAULTSORT:Lanphier, Thomas George}}
[[Category:American aviators]]
[[Category:United States Army officers]]
[[Category:1890 births]]
[[Category:1972 deaths]]
[[Category:People from Calhoun County, Iowa]]
[[Category:United States Military Academy alumni]]
[[Category:Creighton University alumni]]
[[Category:Burials at Arlington National Cemetery]]