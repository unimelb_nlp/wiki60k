<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Pander E
 |image= File:Pander EC.tif{{!}}border
 |caption= The RAC's second Pander EC at the Dutch International Competition in July 1928
}}{{Infobox Aircraft Type
 |type= Two-seat sport and [[Training aircraft|training]] [[biplane]]
 |national origin= [[Netherlands]]
 |manufacturer= Nederlandse Fabriek van Vliegtuigen H.&nbsp;Pander & Zonen
 |designer= Theo Slot<ref name=DNV/>
 |first flight= 18 February 1926<ref name=DNV/>
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users= <!--Limited to three in total; separate using <br /> -->
 |produced= <!--years in production-->
 |number built=17<ref name=DNV/>
 |program cost= <!--Total program cost-->
 |unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 |developed from= 
 |variants with their own articles=
}}
|}

The '''Pander E''' was the first indigenous Dutch [[training aircraft]], used by clubs and also privately owned. A two-seat, single-engine [[biplane]], 17 were built in the [[Netherlands]] from 1926 with engines of increasing power.

==Design and development==
[[File:Pander Ea.png|thumb|right|First prototype with Anzani engine]]

Pander followed their first aircraft, the small, single-seat [[monoplane]] [[Pander D]] with a two-seat, low-powered biplane for club and training use. The Pander E was a true [[sesquiplane]], with a lower wingspan exactly half that of the upper span; in plan, though not in detailed construction the lower wing was almost a half scale copy of the upper wing, the two maximum [[chord (aircraft)|chords]] being in the ratio of 0.52 and the area ratio 0.23.  The wings were entirely of wooden construction, apart from part of their covering.  The upper plane was built around two box spars with [[spruce]] [[Spar (aviation)#Wooden construction|flanges]] and [[plywood]] webs.  From the [[leading edge]] to behind the forward spar the covering was three-ply, with [[aircraft fabric covering|fabric]] aft.  There were [[ailerons]] only on this upper wing.  The narrower lower wings were of similar construction but used only a single box spar, though this was extended into a larger D-box by ply skinning forward around the leading edge.  The wings were arranged so that the rear spar of the upper one was vertically above the single lower wing spar and the aft member of the V-form [[interplane strut]]s was correspondingly vertical, with the forward member reaching forward to the forward upper spar. These Vs leaned outwards, their base on the lower wing strengthened with a further single strut to the upper [[fuselage]] [[longeron]]. The upper wing, built in two pieces, was held high above the [[fuselage]] by four [[cabane strut]]s to the front spar and two more to the aft; the wings were pin-jointed to them.<ref name=Flight/>

The fuselage of the early Pander Es was of mixed construction, the forward structure from welded [[steel]] tubes with [[aluminium]] sheet skinning and the rear wood-framed and [[plywood]]-covered.<ref name=Flight/>  From the seventh aircraft onward, the fuselage became an all-metal structure.<ref name=DNV/> The fin and tailplane were wood framed and ply covered but the [[rudder]] and [[elevator (aircraft)|elevators]] had steel tube [[leading edge]]s and sheet steel [[rib (aircraft)|ribs]]. The open, [[tandem]] [[cockpit]]s were fitted with dual control.  The first Pander Es had a 45&nbsp;hp (33&nbsp;kW) [[Anzani 6-cylinder]] [[radial engine]], cowled but with the [[cylinder heads]] exposed for cooling; later models used several different engines including members of the Walter NZ radial family engine as well as the [[de Havilland Gipsy]] [[straight engine|inline]]. The fixed, [[conventional undercarriage]]  was a steel tube structure with a single axle supported by V-form stuts on each side, one leg to the upper fuselage longeron and one to the lower, braced laterally by short struts to the lower centre fuselage.<ref name=Flight/>

The first Pander E flew on 18 February 1926.<ref name=DNV/>

==Operational history==
One of the first public appearances of the Pander E first prototype was at the Coupe Zenith, held on 4 July 1926, where it was placed fourth. It was destroyed in a fatal crash less than a month later. Nonetheless, the [[Rotterdam]] Aero Club (RAC) placed an initial order for two, received in 1927, and another the following year; these were the first Dutch aircraft used for training. For financial reasons they set up the National Aviation School (NLS) and in 1929 transferred their three aircraft, all ECs with the 60&nbsp;hp (33&nbsp;kW) five-cylinder engine, to the NLS.  Between then and 1932 the NLS received four more new aircraft from Pander, all 85&nbsp;hp (63&nbsp;kW), seven-cylinder powered EFs. In 1930 they acquired a previously privately owned EC and in 1932 a privately owned Gipsy-powered EG. The Dutch East Indies Flying Club bought two EGs, and there were five other individually owned examples, one EF and three EGs.<ref name=DNV/>

==Variants==
[[File:Pander Eb.png|thumb|right|First prototype with Anzani engine]]
Numbers from Golden Years<ref name=GY/>
;E: First prototype. 45&nbsp;hp (34&nbsp;kW) [[Anzani 6-cylinder]] [[radial engine]].<ref name=Flight/>
;EC: or '''EC60'''; 60&nbsp;hp (40&nbsp;kW) [[Walter NZ 60]] five-cylinder radial.<ref name=DNV/><ref name=AE/><ref name=n1 group=note/> five built.
;EF: or '''EF85'''; 85&nbsp;hp (64&nbsp;kW) [[Walter NZ 85]] (NZ VII) 7-cylinder radial.<ref name=Flight3/> six built.
;EG: or '''EG100'''; 100&nbsp;hp (75 KW) take off power [[de Havilland Gipsy I]] four-cylinder upright air-cooled inline.<ref name=Flight2/> four built.
;EH: or '''EH120'''; 120&nbsp;hp (90 KW) take off power [[de Havilland Gipsy III]] four-cylinder upright air-cooled inline. one built.
;EK: or '''EK80''';<ref name=Lailes1/> one converted from EC.
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Anzani engine)==
[[File:Pander E 3-view.png|thumb|right|First prototype, Anzani engine]]
{{Aircraft specs
|ref=Flight 1 April 1926<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=two
|capacity=
|length m=6.20
|length note=
|upper span m=10.0
|upper span note=
|lower span m=5.0
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=17.61
|wing area note=total (upper wing 14.31 m<sup>2</sup> (154.0 sq ft)); lower 3.0 m<sup>2</sup> (35.5 sq ft))
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=702
|empty weight note=
|gross weight kg=
|gross weight lb=1180
|gross weight note=pilot, passenger, 4 h fuel
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=68.2 L (15 Imp gal; 18 US gal)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Anzani 6-cylinder]]
|eng1 type=2-row air-cooled [[radial engine]]
|eng1 hp=45
|eng1 note=continuous at 1,500 rpm and maximum 37 kW (50 hp) at 1,580 rpm
|power original=

|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed mph=78
|max speed note=
|cruise speed mph=72
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=11500
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=915 m (3,000 ft) in 7.75 min
|wing loading lb/sqft=6.2
|wing loading note=
|power/mass=62 W/kg (0.038 hp/lb) at normal power
|more performance=
*'''Landing speed:''' 61 km/h (38 mph)

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Pander E}}

{{reflist|refs=

<ref name=DNV>{{cite book |title= De Nederlandse vliegtuigen |last=Wesselink|first=Theo|last2= Postma|first2=Thijs|year=1982|publisher=Romem  |location=Haarlem |isbn=90 228 3792 0|pages=62–5}}</ref>

<ref name=Flight>{{cite journal |date=1 April 1926|title=The Pander Light Biplane|journal=[[Flight International|Flight]]|volume=XVIII|issue=14|pages=192–4|url=http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200224.html}}</ref>

<ref name=Flight2>{{cite journal |date=17 October 1930|title=Private Flying and Club News|journal=[[Flight International|Flight]]|volume=XXII|issue=42|page=1183|url=http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%201197.html}}</ref>

<ref name=Flight3>{{cite journal |date=9 September 1932|title=The Week-end Aérien|journal=[[Flight International|Flight]]|volume=XXIV|issue=37|page=841|url=http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200905.html}}</ref>

<ref name=Lailes1>{{cite journal |last=Frachet |first=André |date=19 March 1931|title=L'avion léger Pander E.H.120|journal=Les Ailes|issue=509|pages=3|url=http://gallica.bnf.fr/ark:/12148/bpt6k6554698t/f9 }}</ref>

<ref name=AE>{{cite journal|date=June 1929|journal=Aircraft Engineering|volume=1|issue=4|page=135|title=The 5-cylinder Walter NZ 60 engine}}</ref>

<ref name=GY>{{cite web |url=http://www.goldenyears.ukf.net/|title=Golden Years - PH |author= |work= |publisher= |accessdate=23 February 2013}}</ref>

}}

===Notes===
{{reflist|group=note|refs=
<ref name=n1 group=note>Wesselink refers to the 60 hp engine as the Walter IV.</ref>
}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

{{Pander aircraft}}

[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:Sesquiplanes]]
[[Category:Dutch aircraft 1920–1929]]