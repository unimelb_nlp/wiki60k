{{Use dmy dates|date=July 2011}}
{{Use Australian English|date=July 2011}}
{{Infobox person
| name            = Richard Cawthorne
| image           = Richard Cawthorne - 2012 Best Guest or Supporting Actor in a Television Drama.jpg
| caption         = ''Richard Cawthorne - [[AACTA Awards]]'' - [[Sydney Opera House]], 2012.
| birthname       = Richard Cawthorne
| birth_date      = {{Birth date and age|df=yes|1976|12|03}}
| occupation      = Actor
| yearsactive     = 2000{{ndash}}present
}}
'''Richard Seymour Cawthorne''' (born 3 December 1976) is an Australian actor of theatre, film and television.

==Career==

Cawthorne has appeared in many film and television productions. His credits include [[Catching Milat]], [[Fat Tony & Co]], [[Bikie Wars: Brothers in Arms]], [[The Straits]], [[East West 101]], [[Rush (2008 TV series)]], [[Noise (2007 Australian film)]], [[Neighbours]], [[Underbelly: Squizzy]], [[Wolf Creek (TV series)]] and [[Killing Time (TV series)|Killing Time]],<ref>{{cite news | work=The Sydney Morning Herald| url=http://www.smh.com.au/entertainment/tv-and-radio/killing-time-wednesday-november-9-20111104-1mzq3.html | title=Killing Time | date= 9 November 2011  | author=Bridget McManus| accessdate=4 October 2013}}</ref>  for which he won the 2012 Australian Academy of Cinema and Television Arts ''([[AACTA]])''<ref name="aacta">[http://aacta.org/media/174979/inaugural%20samsung%20aacta%20awards%20winners%20and%20noms%20ceremony%20only.pdf] 2012 AACTA Awards Nominees and Winners</ref>
'''''Award for Best Supporting Actor in a Television Drama''.
''' 
In 2015 Richard was appointed Ambassador to the [[National Theatre, Melbourne]].

==Personal life==
Cawthorne was born in Hong Kong in 1976, the youngest of two, his mother Zelda<ref name="Pakula kills time watching a crook">[[The Age]] 9 November 2011 [http://www.theage.com.au/national/melbourne-life/pakula-kills-time-watching-a-crook-20111108-1n5ds.html Pakula kills time watching a crook] by Suzanne Carbone</ref> was a journalist for the [[South China Morning Post]], and his father Russell<ref>[[IMDb]] [http://www.imdb.com/name/nm1010852/?ref_=sr_1 Biography]</ref>l, a Marketing Executive for Hong Kong film company [[Orange Sky Golden Harvest|Golden Harvest Studios]]. 

Cawthorne is also first cousin to Australian Labour Minister, and Victorian Attorney General [[Martin Pakula]].<ref name="Pakula kills time watching a crook"/>

==Filmography==
{| class="wikitable sortable"
|-
! Year !! Title !! Role !! Notes
|-
| 2016 || ''Found'' || Carson || Short Film
|-
| 2016 || ''[[The Death and Life of Otto Bloom]]'' || Duane Renaud || 
|-
| 2016 || ''[[Wolf Creek (TV series)]]'' || Kane || 
|-
| 2016 || ''[[Jack Irish]]'' || Fraser Boyd || 
|-
| 2015 || ''[[Catching Milat]]'' || Detective Paul Gordon ||
|-
| 2014 || ''[[Fat Tony & Co]]'' || Jarrod Ragg ||
|-
| 2013 || ''[[Underbelly: Squizzy]]'' || Long Harry' Slater ||
|-
| 2012 || ''[[Bikie Wars: Brothers in Arms]]'' || Foggy ||
|-
| 2012 || ''[[10 Terrorists]]'' || Judge MI6 || 
|-
| 2012 || ''Australia on Trial - Massacre at Myall Creek''  || Russell ||
|-
| 2012 || ''Australia on Trial - The Eureka 13''  || James Harris ||
|-
| 2011 || ''[[Killing Time (TV series)|Killing Time]]'' || [[Dennis Allen (criminal)|Dennis Allen]] || 
|-
| 2011 || ''[[East West 101]]'' || Sterling || 
|-
| 2011 || ''[[Rush (2008 TV series)|Rush]]'' || Joe Hadden || (3 episodes, 2011)
|-
| 2010 || ''[[The Pacific (TV miniseries)|The Pacific]]''  || Perle || 
|-
| 2009 || ''[[City Homicide]]'' || Harry Bolingbroke || (1 episode, 2009)
|-  
| 2008 || ''[[East of Everything]]'' || Driver || (1 episode, 2008)
|-
| 2008 || ''[[Canal Road (TV series)|Canal Road]]'' || Greg Manor || (1 episode, 2008)
|-
| 2008 || ''[[Rush (2008 TV series)|Rush]]'' || Novak || (1 episode, 2008)
|-
| 2007 || ''[[Noise (2007 Australian film)|Noise]]'' || Kermond ||
|-
| 2006 || ''[[Nightmares and Dreamscapes: From the Stories of Stephen King]]'' || Hot Rod Driver || Tele Movie
|-
| 2005 || ''[[Neighbours]]'' || [[List of Neighbours characters (2005)#Reuben Hausman|Reuben Hausman]] || (11 Episodes, 2005)
|-
| 2005 || ''The Glenmore Job'' || Officer Danoz || Tele Movie 
|-
| 2003 || ''[[Stingers]]'' || Mark 'Weasel' Burridge || (1 episode, 2003)
|-
| 2003 || ''[[Razor Eaters]]'' || Zach || 
|-
| 2001 || ''[[Blue Heelers]]'' || Bradley Eckhardt || (2 Episodes, 2001)
|-
| 2000 || ''[[Halifax f.p.]]'' || Gary Groom || Tele Movie
|-
| 2000 || ''[[Stingers]]'' || Mick Prentice || (1 episode, 2000)
|-
| 2000 || ''[[Eugénie Sandler P.I.]]'' || Sammy || 
|}

==Awards and nominations==
{| class="wikitable" style="font-size:90%;"
|-
! Year !! Group !! Award !! Film/Show !! Result
|-
| 2012
| [[AACTA Awards]]
| Best Guest or Supporting Actor in a Television Drama
| ''[[Killing Time (TV series)|Killing Time]]''
| {{won}}<ref name="aacta"/>
|-
| 2005
| [[Shriekfest|Shriekfest Film Festival]]
| Best Actor
| ''Razor Eaters''
| {{won}}.<ref>[[Shriekfest]] 2005 [http://www.shriekfest.com/winners.php?id=2005 Winners - 2005] {{webarchive |url=https://web.archive.org/web/20111017085404/http://www.shriekfest.com/winners.php?id=2005 |date=17 October 2011 }}</ref>
|}

== References ==
{{reflist|2}}

==External links==
* {{IMDb name|id=1010852|name=Richard Cawthorne}}

{{AACTA Award GuestSupportingActor}}

{{Authority control}}

{{DEFAULTSORT:Cawthorne, Richard}}
[[Category:1976 births]]
[[Category:Australian male film actors]]
[[Category:Australian male television actors]]
[[Category:Australian Jews]]
[[Category:Jewish Australian male actors]]
[[Category:Living people]]