{{Use dmy dates|date=December 2013}}
{{good article}}
{{Infobox earthquake
|title              = 1979 Imperial Valley earthquake
|image              = 
|image alt          = 
|imagecaption       =
|map                = 
|map alt            = 
|image name         =
|map2 = {{Location map+ | USA California
|places =
{{Location map~|California|lat=34.05|long=-118.25|label=Los Angeles|position=left|mark=Green pog.svg}}
{{Location map~|California|lat=32.79|long=-115.56|label=El Centro|position=left|mark=Green pog.svg}}
{{Location map~|California|lat=32.617|long=-115.317|mark=Bullseye1.png|marksize=40}}
 | relief = yes
 | width = 250
 | float = right
 | caption = }}
|mapsize            = 
|caption            =
|date               = {{Start date|1979|10|15}}
|origintime         = 23:16 UTC
|duration           = 5–13 seconds <ref>{{harvnb|Youd|Wieczorek|1982|p=223}}</ref>
|magnitude          = 6.4 [[Moment magnitude scale|M<sub>w</sub>]] <ref name=Wesnousky/>
|depth              = {{convert|8|km|abbr=on}} <ref name=Archuleta2/>
|location           = {{Coord|32|37|N|115|19|W|region:US-CA_type:event|display=inline, title}}
|type               = [[Fault (geology)#Strike-slip faults|Strike-slip]]
|countries affected = [[Baja California]] <br> (Mexico) <br> [[Southern California]] <br> (United States) 
|damage             = $30 million USD <ref name=Stover_p166/>
|intensity          = [[Mercalli intensity scale|IX (''Violent'')]] <ref name=Reagor/>
|PGA                = 1.74''[[Peak ground acceleration|g]]'' <ref name=Rial/>
|landslide          = 
|foreshocks         =
|aftershocks        = {{nowrap|5.8 [[Richter magnitude scale|M<sub>L</sub>]] October 16 at 6:59 <ref>{{harvnb|Heaton|Anderson|German|1983|pp=1161, 1166}}</ref>}}
|casualties         = 91 injured, no deaths <ref name=Stover_p166/> 
}}

The '''1979 Imperial Valley earthquake''' occurred at 16:16 [[Pacific Time Zone|Pacific Daylight Time]] (23:16 [[Coordinated universal time|UTC]]) on October 15 just south of the [[Mexico–United States border]]. It affected [[Imperial Valley (California)|Imperial Valley]] in [[Southern California]] and [[Mexicali Municipality, Baja California|Mexicali Valley]] in northern [[Baja California]]. The earthquake had a relatively shallow [[hypocenter]] and caused property damage in the United States estimated at $30 million USD. The irrigation systems in the Imperial Valley were badly affected, but no deaths occurred. It was the largest earthquake to occur in the [[contiguous United States]] since the [[1971 San Fernando earthquake]] eight years earlier.

The earthquake was 6.4 on the [[moment magnitude scale]], with a maximum perceived intensity of IX (''Violent'') on the [[Mercalli intensity scale]]. However, most of the intensity measurements were consistent with an overall maximum intensity of VII (''Very strong''), and only the damage to a single structure, the Imperial County Services building in [[El Centro, California|El Centro]], was judged to be of intensity IX. Several comprehensive studies on the total structural failure of this building were conducted with a focus on how the building responded to the earthquake's vibration. It was one of the first heavily instrumented office buildings to be severely damaged by seismic forces.

The Imperial Valley is surrounded by a number of interconnected fault systems and is vulnerable to both moderate and strong earthquakes as well as [[earthquake swarm]]s. The area was equipped with an array of strong motion seismographs for analyzing the fault mechanisms of nearby earthquakes and seismic characteristics of the sediments in the valley. The earthquake was significant in the scientific community for studies of both [[fault mechanics]] and repeat events. Four of the region's known strike-slip faults and one additional newly discovered normal fault all broke the surface during the earthquake.

==Tectonic setting==

The [[Salton Trough]] is part of the complex plate boundary between the [[Pacific Plate]] and the [[North American Plate]] where it undergoes a transition from the [[transform fault|continental transform]] of the [[San Andreas Fault]] system to the series of short [[Mid-ocean ridge|spreading center]]s of the [[East Pacific Rise]] linked by oceanic transforms in the [[Gulf of California]]. The two main right–lateral [[Fault (geology)#Strike-slip faults|strike-slip fault]] strands that extend across the southern part of the trough are the [[Elsinore Fault Zone]]/[[Laguna Salada Fault]] to the western side of the trough and the [[Imperial Fault Zone|Imperial Fault]] to the east.<ref name=Mueller>{{cite journal|last=Mueller|first=K.J.|author2=Rockwell T.K.|year=1995|title=Late quaternary activity of the Laguna Salada fault in northern Baja California, Mexico|journal=Geological Society of America Bulletin|publisher=[[Geological Society of America]]|volume=107|issue=1|pages=8–18|url=http://gsabulletin.gsapubs.org/content/107/1/8.full.pdf|doi=10.1130/0016-7606(1995)107<0008:LQAOTL>2.3.CO;2|bibcode = 1995GSAB..107....8M }}</ref> The Imperial Fault is linked to the San Andreas Fault through the [[Brawley Seismic Zone]], which is a spreading center beneath the southern end of the [[Salton Sea]].

With the [[San Jacinto Fault Zone]] to the northwest, the Elsinore fault to the south-southwest, and the Imperial fault centered directly under the Imperial Valley, the area frequently encounters seismic activity, including moderate and damaging earthquakes. Other events in 1852, [[1892 Laguna Salada earthquake|1892]], 1915, 1940, 1942, and 1987 have impacted the region.<ref>{{cite news|title=Historic Imperial County earthquakes|url=http://articles.ivpressonline.com/2011-04-02/magnitude_29376053|first=Elizabeth|last=Varin|date=April 2, 2011|newspaper=[[Imperial Valley Press]]}}</ref> More small to moderate events of less than 6.0 ([[Local magnitude scale|local magnitude]]) have occurred in this area than any other section of the San Andreas fault system.<ref>{{harvnb|Pauschke|Oliveira|Shah|Zsutty|1981|p=1}}</ref>

== Earthquake ==
[[File:USGS Shakemap - 1979 Imperial Valley earthquake.jpg|thumb|USGS shakemap showing the intensity of the 6.4 M<sub>w</sub> mainshock]]
{{see also|List of earthquakes in California}}

The earthquake was caused by rupture along parts of the [[Imperial Fault Zone|Imperial Fault]], the [[Brawley seismic zone|Brawley fault zone]] and the [[Rico Fault]], a previously unknown normal fault near [[Holtville, California|Holtville]], though slip was also observed on the Superstition Hills Fault and the San Andreas Fault. The maximum observed right lateral displacement on the Imperial fault—measured within the first day of the event to the northwest of the epicenter—was {{convert|55|-|60|cm|abbr=on}}, but measurements taken five months following the earthquake closer to the southeast end of the rupture showed there was an additional {{convert|29|cm|abbr=on}} of postseismic slip (for a total slip of {{convert|78|cm|abbr=on}}. Several strands of the Brawley fault zone, to the east of the Imperial fault, ruptured intermittently along a length of {{convert|11.1|km|abbr=on}}, and just one kilometer of the Rico fault slipped with a maximum vertical displacement of {{convert|20|cm|in|abbr=on}} (no horizontal slip was observed on that fault).<ref name=Sharp>{{cite book |last1=Sharp |first1=R.V. |last2=Lienkaemper |first2=J.J. |last3=Bonilla |first3=M.G. |last4=Burke |first4=D.B. |author5=Fox B.F. |author6=Herd D.G. |author7=Miller D.M. |author8=Morton D.M. |author9=Ponti D.J. |author10=Rymer M.J. |author11=Tinsley J.C. |author12=Yount J.C. |author13=Kahle J.E. |author14=Hart E.W. |author15=Sieh K. |last-author-amp=yes |title=The Imperial Valley, California, Earthquake of October 15, 1979|url=https://books.google.co.uk/books?hl=en&lr=&id=hSksAQAAIAAJ&oi=fnd&pg=PA119&dq=1979+Imperial+valley+earthquake&ots=foQuaeOU1I&sig=n9xyah_PHPzG_GZGBBd3-Iy2kTM#v=onepage&q=1979%20Imperial%20valley%20earthquake&f=false |series=Geological Survey professional paper 1254 |year=1982 |publisher=[[United States Government Printing Office]] |chapter=Surface faulting in the central Imperial Valley |pages=119, 120 |deadurl=no}}</ref>

The pattern of displacement along the Imperial Fault was very similar to that observed for the northern part of the rupture during the [[1940 El Centro earthquake]], although on this occasion the rupture did not extend across the border into Mexico. This had been explained as the behavior of individual slip patches along the Imperial Fault with two patches rupturing in 1940 and only the northern one in 1979.<ref name=Sieh>{{cite journal|last=Sieh|first=K.|authorlink=Kerry Sieh|year=1996|title=The repetition of large-earthquake ruptures|journal=Proceedings of the National Academy of Sciences|publisher=[[National Academy of Sciences]]|volume=93|pages=3764–3771|url=http://www.pnas.org/content/93/9/3764.full.pdf|issue=9|doi=10.1073/pnas.93.9.3764|bibcode = 1996PNAS...93.3764S }}</ref> The faulting that gave rise to the earthquake has been modeled by comparing [[synthetic seismogram]]s with near-source strong motion recordings. This analysis showed that the rupture speed had at times exceeded the [[S-wave|shear wave]] velocity,<ref name=Archuleta>{{cite journal|last=Archuleta|first=R.J.|year=1984|title=A Faulting Model for the 1979 Imperial Valley Earthquake|journal=Journal of Geophysical Research|publisher=[[American Geophysical Union]]|volume=89|issue=B6|pages=4559–4585|url=http://www.agu.org/pubs/crossref/1984/JB089iB06p04559.shtml|doi=10.1029/JB089iB06p04559|bibcode=1984JGR....89.4559A}}</ref> making this the first earthquake for which [[Supershear earthquake|supershear]] rupture was inferred.<ref name=Bouchon_2010>{{cite journal|last=Bouchon|first=M.|author2=Karabulut H.|author3=Bouin M.-P.|author4=Schmittbul J.|author5=Vallée M.|author6=Archuleta R.|author7=Das S.|author8=Renard F.|author9=Marsan D.|last-author-amp=yes|year=2010|title=Faulting characteristics of supershear earthquakes|journal=Tectonophysics|publisher=[[Elsevier]]|volume=493|issue=3-4|pages=244–253|url=http://eost.u-strasbg.fr/schmittb/Articles/Bo-Ka-Bo-Sc-Va-Ar-Da-Re-Ma2010.pdf|doi=10.1016/j.tecto.2010.06.011|bibcode = 2010Tectp.493..244B }}</ref>

The [[United States Geological Survey]] operates a series of strong motion stations in the Imperial Valley and while the majority of stations in the array recorded ground accelerations that were not unexpected, station number six registered an unusually high vertical component reading of 1.74g which, at the time, was the highest yet recorded as the result of an earthquake. One explanation of the anomaly attributed the amplification to path effects and a separate theory put forth described supershear effects that generated a focused pulse directly at the station. A later proposal stated that both multipath and focusing effects due to a "lens like effect" produced by a sedimentary wedge at the junction of the Imperial and Brawley faults (under the station) may have been the cause of the high reading.<ref name=Rial>{{cite journal|title=An explanation for USGS Station 6 record, 1979 Imperial Valley earthquake: a caustic induced by a sedimentary wedge|url=http://www.vpereyra.com/files/GEOJRS86.pdf|first=J.A.|last=Rial|first2=V.|last2=Pereyra|first3=G.L.|last3=Wojcik|year=1986|journal=Geophysical Journal|publisher=[[Royal Astronomical Society]]|volume=84|number=2|pages=257–258, 270, 277|doi=10.1111/j.1365-246X.1986.tb04356.x|bibcode = 1986GeoJ...84..257R }}</ref>

== Damage ==
[[File:Fallen porch Brawley 1979.tif|thumb|left|Collapsed porch at Brawley]]

The earthquake caused damage to the Californian cities of [[El Centro, California|El Centro]] and [[Brawley, California|Brawley]], and in the Mexican city of [[Mexicali]] Mexico. There were injuries from the quake on both sides of the border. The state Office of Emergency Preparedness declared 61 injuries on the American side and police claimed that 30 were injured in Mexico. The Red Cross stated that cuts from broken glass, bruises from falling objects, and a few broken bones were reported. California's [[Interstate 8]] developed cracks in it, but vehicles were still able to traverse the highway. The [[California&nbsp;Highway&nbsp;Patrol]] warned drivers that use of the road would be at their own risk.<ref name=latOct17>{{citation|title=Aid for Quake Victims Sought; Emergency proclaimed by Brown|first=Richard E.|last=Meyer|date=October 17, 1979|newspaper=[[Los Angeles Times]]}}</ref>

Damage to the roadways was heavier farther north on [[California State Route 86]] where settling of the road by as much as four to six inches occurred, and a bridge separation closed the highway west of Brawley. Governor [[Jerry Brown]] ended a presidential campaign trip through New England early in order to return to the Imperial Valley and declare a state of emergency there. Two fires occurred in El Centro with the loss of a trailer being reported, though fire was avoided near the [[Imperial County Airport]] when a 60,000 barrel gasoline tank farm was seriously damaged and was losing {{convert|50|USgal}} a minute. Firefighters drained the tanks and replaced the fuel with water to avoid the gasoline vapor from causing a hazard.<ref name=latOct16>{{citation|title=6.4 Quake Jolts Southland; 91 Hurt, Buildings Heavily Damaged in Border Area|first=Richard|last=West|date=October 16, 1979|newspaper=[[Los Angeles Times]]}}</ref>

The earthquake shaking also led to extensive damage to the irrigation systems of the Imperial Valley, leading to breaches in some canals, particularly the [[All-American Canal]] that brings water to the valley from the [[Colorado River]]. A {{convert|13|km|abbr=on}} section of the unlined canal between the Ash and East Highline canals experienced settling. The Imperial Irrigation District estimated damage to be $982,000 for the three canals. Water flow was immediately reduced to prevent further damage and to allow assessments to be made, and within four days the repairs had been completed and full capacity restored. A hydraulic gate and a concrete facility that were damaged during the May 1940 earthquake needed repair again. The 1940 event caused significant destruction to canals on both sides of the international border, with {{convert|108|km|abbr=on}} of damage along eight canals on the US side alone.<ref>{{harvnb|Youd|Wieczorek|1982|pp=235, 237}}</ref>

== Imperial County Services building ==
[[File:Imperial County Services building damage 1979.jpg|thumb|The Imperial County Services building in El Centro following the earthquake with the east end dropping down 30 cm]]
{{see also|Earthquake resistant structures|Seismic analysis}}

The Imperial County Services building, a six-story reinforced concrete building located {{convert|29|km|abbr=on}} northwest of the epicenter in El Centro, was built in 1971 when there were few other tall buildings in the area. The decision to equip the building with nine strong motion sensors in May 1976 was based on its size, structural attributes, and location in a seismically active area.<ref name=ICSBp7>{{harvnb|Pauschke|Oliveira|Shah|Zsutty|1981|p=7}}</ref> Unusually detailed structural analysis was possible as a result of the building having been outfitted with the instrumentation. The initial configuration was tested shortly after its installation when a relatively small (4.9 local magnitude) earthquake occurred {{convert|32|km|abbr=on}} northwest of the building on November 4, 1976. The accelerations recorded on the equipment during the event proved to be of very low amplitude and, as a result, the instrumentation was upgraded to include a 13 channel configuration in the building along with a Kinemetrics triaxial (3 channel) [[accelerograph]] located {{convert|340|ft|abbr=on}} east of the building at ground level. The full 16 channel system was managed by the California Division of Mines and Geology Office of Ground Motion Studies and provided almost 60 seconds worth of high resolution data during the 1979 event.<ref name=ICSBp10>{{harvnb|Pauschke|Oliveira|Shah|Zsutty|1981|p=10}}</ref>

[[File:Imperial County Services building 1979.tif|thumb|left|Damage to the Imperial County Services building's columns]]

In an interview with the ''[[Los Angeles Times]]'' following the earthquake, Fritz Matthiesen, a scientist with the [[United States Geological Survey]], said that the instruments captured "about the third or fourth most significant recording of building damage we've made in 40 years" and that they "have only three other cases in which damage has occurred in an instrumented building".<ref name=latOct17/>

Several types of irregular construction styles were incorporated into the building that contributed to its mass and strength not being uniform throughout the structure. These differences in strength allowed damage to be concentrated in one or more areas rather than being distributed equally and reduced the building's ability to sustain the tremors. Two of the irregularities of the building were the end shear walls that stopped below the second floor and the first floor carrying its load via square support columns. The result of the design was that the first floor was less stiff than the upper floors, and during the earthquake the building sustained uneven damage distribution, a condition that may have led to the complete collapse of the building in a larger earthquake. Because of its failure at the foundation and first floor level, the building was considered a [[total loss]] and was ultimately [[Demolition|demolished]].<ref name=nibs>{{cite web|url=http://www.fema.gov/library/viewRecord.do?id=4711|title=Earthquake-Resistant Design Concepts—An Introduction to the NEHRP Recommended Seismic Provisions for New Buildings and Other Structures |author=NIBS |author-link=National Institute of Building Sciences |publisher=National Institute of Building Sciences Building Seismic Safety Council|pages=38–39 |date=December 2010}}</ref>{{clear}}

== Aftershocks ==
{{Location map | USA California Southern|label=|lat=32.98|long=-115.55|mark=Bullseye1.png|marksize=30|position=top|width=250|float=right|caption=Location of the 5.8 M<sub>L</sub> aftershock near Brawley}}

An early study of the event encompassed more than 2,000 aftershocks (and included four of magnitude 5.0 or greater) that were recorded within 20 days of the mainshock, with the area south of the border near the epicenter remaining relatively quiet. Most of the aftershock activity was within {{convert|15|km|sp=us}} of Brawley (especially the first eight hours after the mainshock), although they occurred from the Salton Sea in the north to the [[Cerro Prieto Geothermal Power Station]] to the south, a distance of {{convert|110|km|sp=us}}. The first strong aftershock (5.0) occurred at 23:19 GMT just 2.5 minutes after the mainshock and the strongest aftershock (5.8) occurred at 6:58 GMT on October 16 west of Brawley.<ref name=Johnson>{{citation|title=The Imperial Valley, California, Earthquake of October 15, 1979|series=Geological Survey professional paper 1254|chapter=Aftershocks and preearthquake seismicity|year=1982|publisher=[[United States Government Printing Office]]|first1=C.E|last=Johnson|first2=L.K.|last2=Hutton|authorlink2=Kate Hutton|url=http://pubs.er.usgs.gov/publication/pp1254|pages=59–63}}</ref>

While the focal mechanism of the mainshock was right-lateral fault slip on the northwest trending Imperial fault, a marked change in the distribution of aftershocks occurred with the onset of the Brawley aftershock, which exhibited left-lateral slip. A distinct zone of aftershocks formed a belt from west of Brawley to near Wiest Lake, where sinistral motion on a northeast trending conjugate fault responded to an increase in tension at the northwest end of the Imperial fault. Another line of aftershocks along the projection of the southern San Andreas fault extended south into the valley up to {{convert|50|km|abbr=on}}. Activity in that area of the valley had been aseismic through 1978, and a few events occurred just prior to the event, and a significant increase in the amount of activity followed the mainshock.<ref name=Johnson/>

== Ground disturbances ==
[[File:Sand boil Imperial 1979.tif|thumb|left|[[Sand volcano|Sand boil]] near El Centro, such evidence of [[Soil liquefaction|liquefaction]] was found particularly at the southern end of the fault rupture]]
{{see also|Slope stability}}

During two outings in late 1979 and early 1980 several researchers (including [[Thomas H. Heaton]] and [[John G. Anderson]]) examined the region near the New River and discovered ground disturbances that were related to the Brawley aftershock. Along the banks of the river the seismologists discovered sand boils, a newly formed pond, and an extension crack that was found to run {{convert|10|km|abbr=on}} near the south bank in an irregular and disconnected fashion from Brawley to Wiest lake. It was later discovered that the Brawley earthquake had an aftershock zone that matched the area of the disturbances. Accelerograms recorded from the nearby Del Rio Country Club also showed "clear and impressive evidence" of near field ground motions, which may have indicated nearby primary faulting.<ref name=Heaton>{{citation|title=Ground Failure Along the New River Caused by the October 1979 Imperial Valley Earthquake Sequence|url=http://resolver.caltech.edu/CaltechAUTHORS:20121211-141241559|first=T. H.|last=Heaton|first2=J. G.|last2=Anderson|first3=P. T.|last3=German|authorlink=Thomas H. Heaton|journal=Bulletin of the Seismological Society of America|publisher=[[Seismological Society of America]]|volume=73|number=4|pages=1161–1163|year=1983}}</ref>

Numerous sites running along the New River were examined including the twin reinforced concrete bridges in Brawley. [[Slump (geology)|Slumping]] of the foundations there resulted in severe damage, and occurred as a result of the October 15 main shock, though the Brawley earthquake's epicenter was nearby. At the Imperial County Dump, several instances of ground failure were observed in sedimentary deposits near the top of and parallel to the river bank, and other cracks were found in that area that were determined to be the result of differential settling. Farther north at the entrance to the Del Rio Country Club, {{convert|30|cm|abbr=on}} scarplets were located west of [[California State Route 111|Route 111]], but undisturbed [[Pleistocene]] sedimentary layers likely indicated that the scarps were the result of local slumping in the roadcut and not the result of surface faulting. A large pond had apparently formed near the [[KROP]] radio station's antenna site where profound liquefaction and subsidence occurred in the river valley. Two weeks following the earthquake sand boils at the same location were still discharging water.<ref name=Heaton/>{{clear}}

==See also==
*[[2010 Baja California earthquake]]

== References ==
{{Reflist|30em|refs=
<ref name=Archuleta2>{{cite journal|title=Hypocenter for the 1979 Imperial Valley earthquake|url=http://onlinelibrary.wiley.com/doi/10.1029/GL009i006p00625/pdf|first=R.J.|last=Archuleta|year=1982|journal=Geophysial Research Letters|publisher=[[American Geophysical Union]]|volume=9|number=6|page=625|doi=10.1029/GL009i006p00625|bibcode=1982GeoRL...9..625A}}</ref>
<ref name=Wesnousky>{{cite journal|title=Earthquakes, quaternary faults, and seismic hazard in California|url=http://onlinelibrary.wiley.com/doi/10.1029/JB091iB12p12587/abstract|first=S. G.|last=Wesnousky|year=1986|journal=Journal of Geophysical Research|publisher=[[American Geophysical Union]]|volume=91|number=B12|page=12,589|doi=10.1029/JB091iB12p12587|bibcode=1986JGR....9112587W}}</ref>
<ref name=Reagor>{{citation|title=The Imperial Valley, California, Earthquake of October 15, 1979|series=Geological Survey professional paper 1254|chapter=Preliminary evaluation of the distribution of seismic intensities|year=1982|publisher=[[United States Government Printing Office]]|first1=B.G.|last=Reagor|first2=C.W.|last2=Stover|first3=S.T.|last3=Algermissen|first4=K.V.|last4=Steinbrugge|first5=P.|last5=Hubiak|first6=M.G.|last6=Hopper|first7=L.M.|last7=Marnhard|url=http://pubs.er.usgs.gov/publication/pp1254|page=255}}</ref>
<ref name=Stover_p166>{{cite book|last1=Stover|first1=C.W.|last2=Coffman|first2=J.L.|title=Seismicity of the United States, 1568–1989 (Revised)|url=https://books.google.co.uk/books?id=bY0KAQAAIAAJ&pg=PA166&dq=seismicity+of+the+united+states+stover+coffman+1979+imperial&hl=en&sa=X&ei=l9oaT6y5HsrH8gO9t_CnCw&ved=0CD0Q6AEwAg#v=onepage&q&f=false|series=U.S. Geological Survey professional paper 1527|year=1993|publisher=[[United States Government Printing Office]]|page=166 |deadurl=no}}</ref>
}}

'''Sources'''
{{refbegin}}
* {{citation|title=A Preliminary Investigation of the Dynamic Response of the Imperial County Services Buildings During the October 15, 1979 Imperial Valley Earthquake|last=Pauschke|first=J. M.|last2=Oliveira|first2=C. S.|last3=Shah|first3=H. C.|last4=Zsutty|first4=T. C.|url=https://blume.stanford.edu/sites/default/files/TR49_Pauschke.pdf|year=1981|publisher=Stanford University Department of Civil and Environmental Engineering |deadurl=no}}
* {{citation|title=The Imperial Valley, California, Earthquake of October 15, 1979|series=Geological Survey professional paper 1254|chapter=Liquefaction and secondary ground failure|year=1982|publisher=[[United States Government Printing Office]]|first1=T.L.|last=Youd|authorlink1=T. Leslie Youd|first2=G.F.|last2=Wieczorek|url=http://pubs.er.usgs.gov/publication/pp1254 |deadurl=no}}
{{refend}}

== External links ==
* [http://earthquake.usgs.gov/earthquakes/eventpage/ci3352060#executive M 6.4 - 10km E of Mexicali, B.C., MX] – [[United States Geological Survey]]
* [http://www.data.scec.org/significant/imperial1979.html Imperial Valley Earthquake] – Southern California Earthquake Data Center
* [http://www.ngdc.noaa.gov/nndc/struts/results?eq_0=4847&t=101650&s=13&d=22,26,13,12&nd=display Significant Earthquake] – [[National Geophysical Data Center]]

{{Earthquakes in 1979}}
{{Earthquakes in California}}
{{Earthquakes in Mexico}}
{{Earthquakes in the United States}}

[[Category:Earthquakes in California|Imperial]]
[[Category:Earthquakes in Mexico|Imperial]]
[[Category:1979 earthquakes|Imperial]]
[[Category:Imperial Valley|Earthquake]]
[[Category:1979 in California|Imperial]]
[[Category:1979 in Mexico|Imperial]]
[[Category:1979 natural disasters in the United States|Imperial]]
[[Category:History of Baja California|Earthquake]]
[[Category:History of Imperial County, California|Earthquake]]
[[Category:Geology of Imperial County, California]]
[[Category:Natural history of Baja California]]
[[Category:El Centro metropolitan area|Earthquake]]
[[Category:Brawley, California|Earthquake]]
[[Category:Calexico, California|Earthquake]]
[[Category:Mexicali|Earthquake]]
[[Category:October 1979 events]]