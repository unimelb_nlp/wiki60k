{{Infobox Officeholder
|name           = Raul Khajimba<br>{{small|Рауль Ҳаџьымба}}<br>{{small|რაულ ჰაჯიმბა}}
|image          = Raul Khajimba 2015.jpg
|office         = 4th [[President of Abkhazia]]
|primeminister  =
|vicepresident  = [[Vitali Gabnia]]
|term_start     = 25 September 2014
|term_end       = 
|predecessor    = [[Alexander Ankvab]]
|successor      = 
|order1         = Member of Parliament<br>for Constituency no. 30 ([[Tkvarcheli]])
|term_start1    = 3 April 2012
|term_end1      = 7 October 2014
|predecessor1   = [[Daur Arshba]]
|successor1     = [[Taifun Ardzinba]]
|order2         = Chairman of the [[Forum for the National Unity of Abkhazia]]
|office2        = 
|term_start2    = 12 May 2010
|term_end2      = 31 March 2015
|president2     = 
|predecessor2   = [[Daur Arshba]]<br>[[Astamur Tania]]
|successor2     = [[Daur Arshba]]
|office3        = [[Vice President of Abkhazia]]
|president3     = [[Sergei Bagapsh]]
|term_start3    = 12 February 2005 
|term_end3      = 28 May 2009
|predecessor3   = [[Valery Arshba]]
|successor3     = [[Alexander Ankvab]]
|office4        = [[Prime Minister of Abkhazia]]
|president4     = [[Vladislav Ardzinba]]
|term_start4    = 22 April 2003
|term_end4      = 6 October 2004
|predecessor4   = [[Gennady Gagulia]]
|successor4     = [[Nodar Khashba]]
|office5        = [[Minister for Defence of Abkhazia|Minister for Defence]]
|primeminister5 = [[Gennady Gagulia]]
|term_start5    = 19 December 2002
|term_end5      = 22 April 2003
|predecessor5   = [[Vladimir Mikanba]]
|successor5     = [[Viacheslav Eshba]]
|office6        = [[First Vice Premier of Abkhazia|First Vice Premier]]
|primeminister6 = [[Anri Jergenia]]<br>[[Gennady Gagulia]]
|term_start6    = 18 June 2001
|term_end6      = 22 April 2003
|predecessor6   = [[Beslan Kubrava]]
|successor6     = [[Astamur Tarba]]
|office7        = [[State Security Service of Abkhazia|Head of the State Security Service]]
|president7     = [[Vladislav Ardzinba]]
|term_start7    = December 1999
|term_end7      = 1 November 2001
|predecessor7   = [[Astamur Tarba]]
|successor7     = [[Zurab Agumava]]
|birth_date     = {{birth date and age|1958|3|21|df=y}}
|birth_place    = [[Tkvarcheli]], [[Georgian SSR]], [[Soviet Union]]
|death_date     = 
|death_place    = 
|party          = [[Forum for the National Unity of Abkhazia]]
|alma_mater     = [[Abkhazian State University]]
}}
'''Raul Khajimba''' ({{lang-ab|Рауль Ҳаџьымба}}, {{lang-ka|რაულ ჰაჯიმბა}}; born 21 March 1958) is the current [[President of Abkhazia]], having been elected in 2014 after the [[Abkhazian Revolution|May Revolution]]. He was also Chairman of the [[Forum for the National Unity of Abkhazia]] from 2010 - 2015. Khajimba previously held the offices of [[Vice President of Abkhazia|Vice President]] (2005–2009), [[Prime Minister of Abkhazia|Prime Minister]] (2003–2004) and [[Minister of Defence of Abkhazia|Defence Minister]] (2002–2003). He unsuccessfully ran for President in [[Abkhazian presidential election, 2004|2004]], [[Abkhazian presidential election, 2009|2009]] and [[Abkhazian presidential election, 2011|2011]].

==Early life and career==
Raul Khajimba was born on 21 March 1958 in [[Tkvarcheli]], where he went to school and worked as a mechanic at the power station. From 1976 until 1978, he served in the [[Soviet Air Defence Forces]]. From 1979 until 1984, he graduated from the Law Faculty of the [[Abkhazian State University]]. From 1985 until 1986 Khajimba studied at the KGB school in [[Minsk]], and he subsequently served as a KGB agent in Tkvarcheli until 1992.

During the [[War in Abkhazia (1992–1993)|1992–1993 war with Georgia]], Khajimba was the head of the military intelligence and counterintelligence operation on the eastern front. For his work, he was awarded the [[Order of Leon]].

From 1996 until 1998, Khajimba headed the anti-smuggling division of the State Customs Committee. In 1998, he became its Deputy Chairman.<ref name=uzel62031>{{cite news|title=Хаджимба Рауль Джумкович|url=http://www.kavkaz-uzel.ru/articles/62031/|accessdate=1 October 2014|agency=[[Caucasian Knot]]|date=25 September 2014}}</ref>


==In government (1999–2004)==

===Security Service Chairman, First Vice Premier and Minister of Defence (1999–2003)===
After a bomb attack on 13 December 1999 in [[Sukhumi]] targeting government officials, President Ardzinba dismissed Astamur Tarba as Security Service Chairman and appointed Khajimba in his stead.<ref name=rferl1341924>{{cite news|last=Fuller|first=Liz|title=Caucasus Report: January 7, 2000|url=http://www.rferl.org/content/article/1341924.html|accessdate=15 January 2012|newspaper=[[RFE/RL]]|date=7 January 2000}}</ref> On 18 June 2001, he additionally became First Vice Premier.<ref name=apress010618>{{cite news|title=|url=http://coffeenews.narod.ru/ArchiveOfNews/APSNYPRESS/AP18-06-01.htm|accessdate=11 May 2011|newspaper=[[Apsnypress]]|date=18 June 2001|language=Russian}}</ref> On 1 November he was succeeded as Head of the State Security Service by Interior Minister Zurab Agumava.<ref name=uzel12046>{{cite news|script-title=ru:ООН: поделите мандариновую страну|url=http://www.kavkaz-uzel.ru/articles/12046/|accessdate=12 May 2011|newspaper=[[Caucasian Knot]]|language=Russian|date=1 November 2001}}</ref><ref name=uzel12051>{{cite news|script-title=ru:Абхазский парламент не поддержал вотум недоверия правительству|url=http://www.kavkaz-uzel.ru/articles/12051/|accessdate=12 May 2011|newspaper=[[Caucasian Knot]]|language=Russian|date=2 November 2001}}</ref> On 16 May 2002, Khajimba was appointed [[Minister of Defence of Abkhazia|Defence Minister]], replacing [[Vladimir Mikanba]], while remaining First Vice Premier.<ref name=uzel20184>{{cite news|script-title=ru:Назначен новый глава министерства обороны Абхазии|url=http://www.kavkaz-uzel.ru/articles/20184/|accessdate=29 September 2011|newspaper=[[Caucasian Knot]]|language=Russian|date=16 May 2002}}</ref>

===Prime Minister (2003–2004)===
In the evening of 7 April 2003, Prime Minister [[Gennadi Gagulia]] filed for resignation. Early in the morning of that day, nine prisoners had escaped, four of which had been sentenced to death due to their involvement in the [[2001 Kodori crisis]].<ref name="uzel36186">{{cite news|url=http://www.kavkaz-uzel.ru/articles/36186/|script-title=ru:Правительство Абхазии уходит в отставку|last=Gordienko|first=Anatoly|date=9 April 2003|publisher=[[Nezavisimaya Gazeta]] / Caucasian Knot|language=Russian|accessdate=21 December 2009}}</ref> President Ardzinba initially refused to accept Gagulia's resignation, but was forced to agree on 8 April.<ref name="civil4016">{{cite news|url=http://www.civil.ge/eng/article.php?id=4016|title=Breakaway Abkhaz Government in Crisis|date=9 April 2003|publisher=[[Civil Georgia]]|accessdate=21 December 2009}}</ref> Vice President [[Valery Arshba]] denied on 8 April that the government's resignation was due to the prison escape, and stated that instead it was caused by the opposition's plans to hold protest rallies on 10 April.<ref name="civil4010">{{cite news|url=http://www.civil.ge/eng/article.php?id=4010|title=Abkhaz de facto Premier Remains on Post|date=8 April 2003|publisher=[[Civil Georgia]]|accessdate=21 December 2009}}</ref> On 22 April 2003, Raul Khajimba was appointed the new Prime Minister.<ref name=apress030422>{{cite news|script-title=ru:Выпуск № 075|url=http://www.apsnypress.narod.ru/2003/04_apr-03/AP04-22-03.htm|accessdate=18 April 2011|newspaper=[[Apsnypress]]|date=22 April 2003|language=Russian}}</ref><ref name=skakov>{{cite journal
  | last = 
  | first = 
  | authorlink = 
  |author=Alexander Skakov
  | title = Abkhazia at a Crossroads: On the Domestic Political Situation in the Republic of Abkhazia.
  | journal = Iran and the Caucasus
  | volume = 9
  | issue = 1
  | pages = 159–186
  | publisher = 
  | location = 
  | date = 
  | url = http://www.ingentaconnect.com/content/brill/ic/2005/00000009/00000001/art00011
  | doi = 10.1163/1573384054068088
  | id = 
  | accessdate = 2008-07-10}}</ref> He remained Prime Minister until October 2004.

As then-President [[Vladislav Ardzinba]] was seriously ill and did not appear in public during his term, Khajimba acted as a de facto head of state in his absence. In this role, he met a number of political leaders, including [[Igor Ivanov]], foreign minister of [[Russia]]. He has been a sharp opponent of reunification with Georgia, and vehemently condemned Georgian President [[Mikhail Saakashvili]]'s proposal for a two-state federation in May 2004.

==2004 and 2005 Presidential elections==
{{Main|Abkhazian presidential election, 2004|Abkhazian presidential election, 2005}}

Khadjimba was tipped as the favourite to win the [[Abkhazian presidential election, 2004|October 2004 presidential elections]], and was strongly endorsed by both outgoing president Ardzinba and [[Russian Federation|Russian]] President [[Vladimir Putin]]. Both men campaigned on his behalf and dedicated significant resources to assisting the Khadjimba campaign. However, opposition candidate [[Sergei Bagapsh]] polled more votes on election day, in what was widely attributed as a backlash against the strong Russian influence in his campaign.

After the election, both Bagapsh and Khadjimba claimed victory, with Khadjimba alleging that electoral fraud in the pro-Bagapsh [[Gali District, Abkhazia|Gali]] region had been responsible for Bagapsh's win. Ardzinba soon dismissed Khadjimba as Prime Minister, replacing him with a compromise candidate, [[Nodar Khashba]], and two months of drawn-out disputes followed, involving public protests, court action and parliamentary proceedings.

In December 2004, Khadjimba and Bagapsh came to an agreement which would see the pair run as part of a national unity ticket in repeat elections, with Khadjimba running as Bagapsh's Vice-President. As part of this deal, the position of vice-president was given expanded powers covering defence and foreign affairs. The joint ticket easily won the [[Abkhazian presidential election, 2005|January 2005 election]], winning more than 90% of the vote.

==Vice President (2005–2009)==
However, in the aftermath of the election win, many analysts have suggested that Khadjimba's executive authority would  be somewhat limited under the new arrangement, with Bagapsh and his Prime Minister, [[Alexander Ankvab]], likely to maintain ultimate control over the areas of policy nominally assigned to the vice-president.

The controversy resurfaced again in June 2008, when Khadjimba attended a congress of the [[Aruaa]] veteran organisation, of which he is a member. The congress issued a statement criticizing the Bagapsh administration’s “multi-vector foreign policy”, referring to the talks with Georgian and Western diplomats, and called for greater ties with Russia. The pro-Bagapsh politicians from the [[Amtsakhara]] veteran organisation described Khadjimba’s criticism of the government, in which he was a vice-president, “immoral”. Later that month, Khadjimba reiterated his stance towards Bagapsh’s foreign policy, stating that Abkhazia’s only protector could be Russia and using force would be inevitable for gaining control of the upper [[Kodori Valley]] in northeastern Abkhazia, the only part of Abkhazia under Georgian control at the time.<ref>[http://www.civilgeorgia.ge/eng/article.php?id=18594&search=Khajimba Abkhaz VP: Force May be Needed to Regain Kodori]. Civil Georgia. 21 June 2008.</ref><ref>{{ru icon}} [http://www.regnum.ru/news/1017559.html "Нам следует заручиться военно-политическим союзом с Россией": Обзор СМИ Абхазии] (“We need to secure a military-political alliance with Russia”: Review of the Abkhaz press). Regnum. 20 June 2008.</ref><ref>{{ru icon}} [http://www.regnum.ru/news/fd-abroad/1014316.html Депутат парламента Абхазии считает действия вице-президента Рауля Хаджимба аморальными] (Member of the Parliament of Abkhazia considers Vice-President Raul Khadjimba’s actions immoral). Regnum. 12 June 2008.</ref><ref>{{ru icon}} [http://www.regnum.ru/news/1013500.html Оппозиция во власти и власть в оппозиции: Абхазия встает на выборные рельсы] (Opposition within authorities and authorities in opposition: Abkhazia  on the election track). Regnum. 11 June 2008.</ref> In August 2008, the [[Military of Abkhazia|Abkhazian military]] [[Battle of the Kodori Valley|did take the upper Kodory Valley by force]] during the [[2008 South Ossetia war|August 2008 war over South Ossetia]].<ref>{{cite news |title=Kodori Under Abkhaz Control |url=http://www.civil.ge/eng/article.php?id=19073&search=kodori |work=Civil Georgia |date=August 12, 2008 |accessdate=2008-08-16}}</ref>

On 18 May 2009 the [[Forum of the National Unity of Abkhazia]] and Aruaa issued a press statement and on 20 May they gave a press conference with several other opposition parties, on both occasions voicing severe criticism over the achievements of the government and recent foreign policy decisions.<ref name="rferl0524">{{cite news|url=http://www.rferl.org/content/Abkhaz_Leadership_Opposition_Exchange_Accusations/1738893.html|title=Abkhaz Leadership, Opposition Exchange Accusations |last=Fuller|first=Liz|date=2009-05-24|work=Caucasus Report|publisher=[[Radio Free Europe/Radio Liberty]]|accessdate=2009-05-29}}</ref> On 28 May Khajimba resigned, saying that he agreed with the criticism,<ref name="apress0528">{{cite news|url=http://www.apsnypress.info/news2009/May/28.htm|script-title=ru:ВИЦЕ-ПРЕЗИДЕНТ РАУЛЬ ХАДЖИМБА УШЕЛ В ОТСТАВКУ |date=2009-05-28|publisher=[[Apsnypress]]|language=Russian|accessdate=2009-05-29}}</ref> but attributing his failure to tackle corruption and improve security to lack of room for maneuvre and no support from the president Bagapsh whom he also accused of violating the 2004 powersharing agreement and criticized him for signing a border protection agreement with Russia in 2009.<ref>[http://www.mnweekly.ru/news/20090528/55378291.html Split emerges in Abkhaz government]. ''[[Moscow News]]'' №20. May 28, 2009</ref>

==Opposition leader (2009–2014)==

===2009 Presidential election===
{{Main|Abkhazian presidential election, 2009}}

The Russian newspaper [[Kommersant]] reported that during the summer of 2009 Khajimba had entered negotiations with [[Beslan Butba]] over forming an alliance for in the presidential election, but the pair fell out following the visit of Russian Prime Minister [[Vladimir Putin]] to Abkhazia. Putin had met with Khajimba, but not with Butba, and Butba considered this an unfriendly act on the part of Khajimba. During the nomination period for candidates, Khajimba then tried to form a team with Ardzinba. The alliance would have combined Ardzinba's backing by part of the business community and his financial resources with Khajimba's electoral popularity. The pair said they would run together during two meetings with voters, and the idea was that they would receive the joint nomination by the [[Forum of the National Unity of Abkhazia]]. According to the Kommersant, in the end the pair could not agree on what positions they would get. Khajimba wanted the Presidency, and offered Ardzinba to become Prime Minister, but this was not acceptable to the latter. The congress of the Forum of the National Unity of Abkhazia planned on 29 October was called off, and Ardzinba was instead nominated by an initiative group that day.<ref name="kommersant1267836">{{cite news|url=http://www.kommersant.ru/doc.aspx?DocsID=1267836|script-title=ru:Сергей Багапш столкнется с поддержанными конкурентами|last=Allenova|first=Olga|date=2009-11-02|publisher=[[Kommersant]]|language=Russian|accessdate=2009-11-15}}</ref>

Khajimba had already been officially nominated for the Presidency by an interest group on 19 October,<ref name="uzel160860">{{cite news|url=http://abkhasia.kavkaz-uzel.ru/articles/160860/|script-title=ru:Рауль Хаджимба примет участие в выборах президента Абхазии|last=Kuchuberia|first=Anzhela|date=2009-10-19|publisher=Caucasian Knot|language=Russian|accessdate=2009-10-19}}</ref> and received the additional support on 20 October of the Forum of the National Unity of Abkhazia, [[Aruaa]] and [[Akhatsa]].<ref name="uzel160921">{{cite news|url=http://abkhasia.kavkaz-uzel.ru/articles/160921/|script-title=ru:Оппозиционеры поддержали выдвижение Хаджимбы кандидатом в президенты Абхазии|last=Kuchuberia|first=Anzhela|date=2009-10-20|publisher=Caucasian Knot|language=Russian|accessdate=2009-10-21}}</ref> Khajimba picked [[Vasilii Avidzba]] as his Vice Presidential candidate.

Nevertheless, on 18 November Khajimba and Ardzinba announced that they would continue to coordinate their campaigns, and that they had appointed chairman of [[Aruaa]] [[Vadim Smyr]] to lead this coordination.<ref name="apress091118">{{cite news|url=http://www.apsnypress.info/news2009/November/18.htm|script-title=ru:Выпуск №555-556-557-558|date=2009-11-18|publisher=[[Apsnypress]]|language=Russian|accessdate=19 November 2009}}</ref> On 20 November, Khajimba stated that he and Ardzinba had different visions on coming to power, but that he didn't consider Ardzinba his opponent, and that in the case of a second round, he, Ardzinba and Butba would support each other.<ref name="uzel162224">{{cite news|url=http://abkhasia.kavkaz-uzel.ru/articles/162224/|script-title=ru:Рауль Хаджимба пожаловался на власти Абхазии в Центризбирком|last=Kuchuberia|first=Anzhela|date=2009-11-20|publisher=Caucasian Knot|language=Russian|accessdate=21 November 2009}}</ref>

A second round proved to be unnecessary, since incumbent President Sergei Bagapsh won a 61,16% first round victory. Khajimba came in second place with 15,584 votes, 15.32% of the total number cast.<ref name="uzel163085">{{cite web|last=Kuchuberia|first=Anzhela|script-title=ru:ЦИК Абхазии: на выборах президента победил Сергей Багапш (Видео)|url=http://abkhasia.kavkaz-uzel.ru/articles/163085/|work=Caucasian Knot|accessdate=20 May 2010|language=Russian|date=14 December 2009}}</ref>

===Chairman of the Forum of the National Unity of Abkhazia===
On 12 May 2010, Raul Khajimba was elected Chairman of the FNUA, after the party congress had reduced the number of Chairmen from 2 to 1.<ref name="uzel168754">{{cite web|last=Kuchuberia|first=Anzhela|script-title=ru:Рауль Хаджимба возглавил оппозиционный "Форум народного единства Абхазии"|url=http://abkhasia.kavkaz-uzel.ru/articles/168754/|work=Caucasian Knot|accessdate=19 May 2010|language=Russian|date=13 May 2010}}</ref>

===2011 Presidential election===
{{Main|Abkhazian presidential election, 2011}}

Khajimba again ran for President in 2011, after the death of President [[Sergei Bagapsh]]. His running mate was [[Svetlana Jergenia]], widow of first President [[Vladislav Ardzinba]]. The pair was first nominated by an initiative group on 28 June and then by the [[Forum for the National Unity of Abkhazia]] on 16 July.<ref name=apress3547>{{cite news|title=ЦИК Абхазии зарегистрировал инициативную группу по выдвижению кандидатом в президенты Рауля Хаджимба|url=http://apsnypress.info/news/3547.html|accessdate=2 July 2011|newspaper=[[Apsnypress]]|date=28 June 2011}}</ref><ref name=apress3691>{{cite news|title=Рауль Хаджимба и Светлана Джергения дали согласие баллотироваться на пост президента и вице-президента Абхазии|url=http://apsnypress.info/news/3691.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=16 July 2011}}</ref> They received the additional support of [[Akhatsa]] (5 July), [[Aruaa]] (7 July), former Prime Minister and 2004 Presidential candidate [[Anri Jergenia]] (27 July) and the [[Union of the Cossacks of Abkhazia]] (5 August).<ref name=aiaaira2178>{{cite news|title="Ахьаца" поддерживает тандем Рауля Хаджимба и Светланы Джергения|url=http://aiaaira.com/index.php?option=com_content&view=article&id=2178:qq-------&catid=69:politics&Itemid=112|accessdate=6 July 2011|newspaper=Aiaaira|date=5 July 2011}}</ref><ref name=aiaaira2183>{{cite news|title=Заявление Высшего Совета "АРУАА"|url=http://aiaaira.com/index.php?option=com_content&view=article&id=2183:---lr&catid=73:society&Itemid=116|accessdate=11 July 2011|newspaper=Aiaaira|date=7 July 2011}}</ref><ref name=nuzhnaya29>{{cite news|title=Анри Джергения о выборах, санаторном вопросе и о прочем...|url=http://aiaaira.com/component/content/article/65-headline/2261-2011-07-30-14-12-35|accessdate=5 August 2011|newspaper=Nuzhnaya/Aiaaira|date=27/30 July 2011}}</ref><ref name=apress3865>{{cite news|title=Союз Казаков Абхазии поддерживает кандидатуру Рауля Хаджимба|url=http://apsnypress.info/news/3865.html|accessdate=5 August 2011|newspaper=[[Apsnypress]]|date=5 August 2011}}</ref>

The pair scored a 19.82% third place, losing to Acting President [[Alexander Ankvab]].<ref>{{cite news|url=http://www.mfaabkhazia.net/en/node/1054|title=Final results of the presidential elections in Abkhazia|date=27 August 2011|publisher=Abkhazian Ministry of Foreign Affairs|accessdate=4 September 2011}}</ref>

===May 2014 revolution===
{{main|Abkhazian Revolution|Abkhazian presidential election, 2014}}
Khajimba led opposition protests that forced Alexander Ankvab to resign as President on 1 June 2014. Khajimba won the subsequent Presidential election with a slim 50.60% first-round victory.<ref name=apress12925>{{cite news|title=Центризбирком подвел окончательные итоги по выборам президента РА|url=http://apsnypress.info/news/12925.html|accessdate=14 September 2014|agency=[[Apsnypress]]|date=26 August 2014}}</ref> 

==President (since 2014)==
{{more|Government of President Khajimba}}
Khajimba was inaugurated as president on 25 September 2014. Two months into his presidency, he signed a controversial treaty with [[Russia]]n president [[Vladimir Putin]] deepening ties between Abkhazia and Russia. Provisions of the agreement included placing the Abkhazian military under the direct control of Russia's armed forces and committing Abkhazia toward bringing its trade laws into alignment with the [[Eurasian Economic Union]]. The treaty was widely condemned in the [[Western world|West]] and by the [[Georgia (country)|Republic of Georgia]], with [[United States|U.S.]] newspaper ''[[The New York Times]]'' suggesting the Abkhazian government had no choice but to agree to Putin's terms. However, Khajimba hailed closer ties with Russia as promoting "the full scope of guarantees for the safety of our state and extensive opportunities for the social and economic development".<ref>{{cite news|url=https://www.nytimes.com/2014/11/25/world/europe/pact-tightens-russian-ties-with-abkhazia.html|agency=The New York Times|title=Pact Tightens Russian Ties With Abkhazia|date=24 November 2014|accessdate=9 January 2015}}</ref>

== References ==
{{Reflist|
refs=
<!--ref name=TimesAm2014-09-25>
{{cite news 
| url         = http://www.times.am/?p=94304&l=en
| title       = NKR Parliament speaker met President of the Republic of Abkhazia 
| publisher   = [[Times of Armenia]]
| author      = 
| date        = 2014-09-25
| page        = 
| location    = 
| archiveurl  = https://web.archive.org/web/20140929152722/http://www.times.am/?p=94304&l=en
| archivedate = 2014-09-29
| deadurl     = No 
| quote       = On September 25, NKR delegation headed by the Chairman of the National Assembly Ashot Ghoulyan participated in the ceremony of inauguration of the newly elected President of the Republic of Abkhazia Raul Khadjimba. NKR NA Press Service informs about this. 
}}
</ref-->
}}

{{s-start}}
{{s-off}}
{{s-bef|before=[[Astamur Tarba]]}}
{{s-ttl|title=[[State Security Service of Abkhazia|Head of the State Security Service]]|years=1999–2001}}
{{s-aft|after=[[Zurab Agumava]]}}
|-
{{s-bef|before=[[Beslan Kubrava]]}}
{{s-ttl|title=[[First Vice Premier of Abkhazia|First Vice Premier]]|years=2001–2003}}
{{s-aft|after=[[Astamur Tarba]]}}
|-
{{s-bef|before=[[Vladimir Mikanba]]}}
{{s-ttl|title=[[Minister for Defence of Abkhazia|Minister for Defence]]|years=2002–2003}}
{{s-aft|after=[[Viacheslav Eshba]]}}
|-
{{s-bef|before=[[Gennady Gagulia]]}}
{{s-ttl|title=[[Prime Minister of Abkhazia]]|years=2003–2004}}
{{s-aft|after=[[Nodar Khashba]]}}
|-
{{s-bef|before=[[Valery Arshba]]}}
{{s-ttl|title=[[Vice President of Abkhazia]]|years=2005–2009}}
{{s-aft|after=[[Alexander Ankvab]]}}
|-
{{s-bef|before=[[Daur Arshba]]}}
{{s-ttl|title=Member of Parliament<br>for Constituency no. 30 ([[Tkvarcheli]])|years=2012–2014}}
{{s-aft|after=[[Taifun Ardzinba]]}}
|-
{{s-bef|before=[[Alexander Ankvab]]}}
{{s-ttl|title=[[President of Abkhazia]]|years=2014–present}}
{{s-inc}}
|-
{{s-ppo}}
{{s-bef|before=[[Daur Arshba]]}}
{{s-ttl|rows=2|title=Chairman of the<br>[[Forum for the National Unity of Abkhazia]]|years=2010–2015}}
{{s-aft|rows=2|after=[[Daur Arshba]]}}
|-
{{s-bef|before=[[Astamur Tania]]}}
{{s-end}}

{{Heads of State of Abkhazia}}
{{Prime Ministers of Abkhazia}}
{{Vice Presidents of Abkhazia}}

{{DEFAULTSORT:Khajimba, Raul}}
[[Category:1958 births]]
[[Category:5th convocation of the People's Assembly of Abkhazia]]
[[Category:Forum for the National Unity of Abkhazia politicians]]
[[Category:First Vice Premiers of Abkhazia]]
[[Category:Living people]]
[[Category:Ministers for Defence of Abkhazia]]
[[Category:People from Tkvarcheli District]]
[[Category:Presidents of Abkhazia]]
[[Category:Prime Ministers of Abkhazia]]
[[Category:Vice Presidents of Abkhazia]]
[[Category:Chairmen of the State Security Service of Abkhazia]]
[[Category:Candidates for President of Abkhazia, 2004]]
[[Category:Candidates for Vice President of Abkhazia, 2005]]
[[Category:Candidates for President of Abkhazia, 2009]]
[[Category:Candidates for President of Abkhazia, 2011]]
[[Category:Candidates for President of Abkhazia, 2014]]