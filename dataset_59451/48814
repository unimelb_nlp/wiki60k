{{orphan|date=May 2011}}
'''Kirk Iverson''' is an [[United States|American]] [[inventor]], writer, producer, media executive, investor and financier who invented the Veterans Media Center Free Enterprise Zone (VMC2) and VMC2 Banking Consortium architecture, the world’s first integrated, media-specific [[Free trade zone|free enterprise zone]] and media-specific free enterprise zone financing model.<ref>{{cite web|url=http://www.wweek.com/portland/article-17504-lights__camera__coliseum_.html |title=Veterans Media Center Free Enterprise Zone (VMC2) and VMC2 Banking Consortium Coverage in "Willamette Week" |publisher=Wweek.com |date= |accessdate=2011-05-28}}</ref>

==Career==
Iverson spent a [[decade]] working from [[intern]] at [[Baltimore Pictures]], [[Barry Levinson]]'s production company at [[Warner Bros.|Warner Bros. Studios]] and [[1492 Pictures]], [[Chris Columbus (filmmaker)|Chris Columbus]]' production company at [[20th Century Fox]] to global head of creative development and production at [[Wieden+Kennedy|Wieden+Kennedy advertising agency]], focusing on its entertainment projects: ''Ginga, Battlegrounds, Sportsblender, The Sims Show, Nothing but the Truth, Kobe’s Disciples'' and ''Coraline'' feature film marketing. He has worked for unit production manager [[Sharon Mann (productioin manager)|Sharon Mann]] on [[Steven Spielberg]]’s ''[[Minority Report (film)|Minority Report]]'', producer [[Colin Wilson (producer)|Colin Wilson]] on ''[[Terminator 3: Rise of the Machines]]'' and [[Dan Wieden]], co-founder and CEO of Wieden+Kennedy.  He assisted [[Phil Knight]] (co-founder and chairman of [[Nike, Inc.|Nike Inc.]] and chairman and owner of [[Laika (company)|LAIKA]]) in designing the marketing strategy and building the marketing team for LAIKA’s ''[[Coraline (film)|Coraline]]'' distributed by [[Focus Features]], securing Wieden+Kennedy’s first feature film marketing campaign with a major studio distributor.<ref>{{cite web|url=http://blog.wkstudio.com/?p=105 |title=She’s heeeeeere! « W+K Studio Blog |publisher=Blog.wkstudio.com |date=2009-02-06 |accessdate=2011-05-28}}</ref><ref>{{cite web|url=http://www.imdb.com/name/nm0412306/|title=Kirk Iverson|work=IMDb}}</ref><ref>{{cite web|url=http://www.huffingtonpost.com/polly-labarre/did-you-make-your-custome_b_73338.html|title=Did You Make Your Customers Smarter Today?|work=The Huffington Post}}</ref><ref>{{cite web|url=http://www.wktokyo.jp/blog/?p=1494 |title=Iverson and Wieden+Kennedy Partners Rollout Wieden+Kennedy Radio on 2009 Global Tour |publisher=Wktokyo.jp |date=2010-07-28 |accessdate=2011-05-28}}</ref>

Iverson brought ''[[CliffsNotes]] Films'' and ''[[Search for the Ultimate Athlete]]''<ref>{{cite web|url=http://www.searchfortheultimateathlete.com/|title=Home|work=searchfortheultimateathlete.com}}</ref> into Wieden+Kennedy as co-productions, and after leaving Wieden+Kennedy, was hired by the series’ copyright holders to executive produce, write and creative direct both series. A third party production company and studio then optioned the series, respectively.<ref>{{cite web|url=http://www.hollywoodreporter.com/news/aol-mark-burnett-team-produce-94604 |title=AOL, Mark Burnett Team Up to Produce CliffsNotes Video Shorts |publisher=The Hollywood Reporter |date=2011-01-31 |accessdate=2011-05-28}}</ref>

Iverson has worked with [[Nike, Inc.|Nike]], [[Electronic Arts]], Warner Brothers, 20th Century Fox, Endgame Entertainment, [[Procter & Gamble]], [[Nokia]], [[Walt Disney Pictures]], [[Target Corporation|Target]], [[Google]], [[YouTube]], LAIKA, [[The Coca-Cola Company|Coca-Cola]], [[MTV]], Wasserman Media Group, Echo Lake Entertainment, Gigapix Studios, [[PBS]], DEVO and [[IMAX]].

==Globalcraft==
In an effort to decrease budgetary waste and investor risk without hurting creative quality, Iverson founded [[Globalcraft]]<ref name="Globalcraft">{{cite web|url=http://www.globalcraftstudios.com |title=Globalcraft |publisher=Globalcraftstudios.com |date= |accessdate=2011-05-28}}</ref> in 2009, run by leading creatives and strategists from [[Fortune 500]] entertainment and marketing backgrounds. Globalcraft uses proprietary quantitative analysis to assess the financial viability of media investments in addition to providing development, finance, production, marketing and distribution services.<ref name="Globalcraft"/>

==Holdings==
Iverson is the majority owner of Globalcraft.

In 2011, Iverson invented The Cloud Studio Model, a privately held joint venture for top producers, brands, financiers and distributors to achieve greater cost savings, creative protection and investor protection compared to status quo studio models. Globalcraft is the majority owner of the Cloud Studio Model.

In 2011, Iverson (Globalcraft) partnered with Tim Lawrence ([[Digital Works Productions]]) to lobby the [[Government of Portland, Oregon|City of Portland]] and the [[Government of Oregon|State of Oregon]] to convert the [[Memorial Coliseum (Portland)|Veterans Memorial Coliseum]] in [[Portland, Oregon|Portland]], [[Oregon]] into an integrated production (recording and interactive) [[soundstage]], exhibition venue, free enterprise zone and free enterprise zone banking consortium. Cornilles and Lawrence designed and led the soundstage portion of the initiative while Iverson designed and led the financing architecture for the soundstage, free enterprise zone and banking consortium. They proposed that the project be named Veterans Media Center (VMC2).<ref>{{cite web|url=http://www.wweek.com/portland/article-17504-lights__camera__coliseum_.html |title=Lights! Camera! Coliseum! |publisher=Wweek.com |date= |accessdate=2011-05-28}}</ref><ref>{{cite web|url=http://www.neworegonjobs.com/ |title=Veterans Media Center 2 |publisher=New Oregon Jobs |date= |accessdate=2011-05-28}}</ref> VMC2 would be the largest integrated production space under one roof in the world.

==Training==
Iverson studied story under the instruction of [[DreamWorks|DreamWorks SKG]] story analyst Mitchell Levin.<ref>{{cite web|url=http://www.imdb.com/name/nm0505679/|title=Mitchell Levin|work=IMDb}}</ref>

Iverson studied the [[Meisner technique|Meisner Technique]] for two years under the instruction of Laurel Smith,<ref>{{cite web|url=http://www.imdb.com/name/nm0809047/|title=Laurel Smith|work=IMDb}}</ref> a direct disciple of [[Sanford Meisner]].

Iverson worked as a [[Clapper loader|2nd Assistant Camera]] person and camera (film) loader on feature films.

Iverson was mentored in strategic finance and investment banking by Benjamin Dickey (Madison Advisors)<ref>{{cite web|url=http://www.madisonadv.com/index.html|title=Madison Advisors Homepage|work=Madison Advisors Homepage}}</ref> and [[Andrew Kline]] ([[Park Lane (investment bank)|Park Lane Investment Bank]]).

==Personal life==
Iverson was born and raised in [[Portland, Oregon]]. After raising the money to serve as a volunteer public health worker on [[rabies]] [[vaccine|vaccination]], dental hygiene, reforestation and public sanitation projects in [[Ecuador]] and [[Honduras]] with [[Amigos de las Américas|Amigos de Las Americas]] during [[high school]], he graduated [[Phi Beta Kappa Society|Phi Beta Kappa]] from [[Beloit College]] in 1999. He speaks fluent Spanish.

==References==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

==External links==
# [http://www.imdb.com/name/nm0412306/ Iverson's Filmography on Internet Movie Database (IMDB)]
# [http://www.globalcraftstudios.com Globalcraft Media+Studios]

{{DEFAULTSORT:Iverson, Kirk Vernstrom}}
[[Category:American inventors]]
[[Category:Living people]]
[[Category:Writers from Portland, Oregon]]
[[Category:Wieden+Kennedy people]]
[[Category:Phi Beta Kappa members]]
[[Category:Beloit College alumni]]