{{BLP sources|date=June 2015}}
{{Orphan|date=March 2012}}

{{Infobox artist
| bgcolour      = #6495ED
| name          = Nicole Cohen
| image         =
| imagesize     =
| caption       =
| birth_name     =
| birth_date     = {{birth-date|1970}}
| birth_place      =
| death_date     =
| death_place    =
| nationality   = [[United States|American]]
| field         = [[Video art]], [[Installation art]]
| training      =
| movement      =
| works         =
| patrons       =
| influenced by = [[Dennis Oppenheim]], [[R. Buckminster Fuller]], [[Tony Oursler]], [[Laurie Anderson]], [[Laurie Simmons]], [[Dan Graham]]
| influenced    =
| awards        =
}}

'''Nicole Cohen''' is an American installation artist, who works with video and new media in order to explore issues of how interior design and architecture reveals aspects of portraiture and identity. Cohen's video works often include
interiors from vintage magazines, period rooms, that reveal an intervention with technology of surveillance use or video projection overlay. She uses photography and creates a video animation on top of the past image.

She uses video to transform and alter interior designed and architectural spaces. She explores ideas of perception and surveillance through her projects. Her influences stem from film/ cinematic theory and the physical experience of immersion.

Her work is positioned at the crossroads of contemporary reality, personal fantasy, and culturally constructed space. Although, trained in painting and drawing, Cohen most frequently uses video as her medium, playing upon intrinsic capacities to manipulate time, distort scale and environment, and overlay imagery. Consistently interested in engaging her audience and challenging the notions of lifestyle, domesticity, celebrity, and social behavior, Cohen also uses the surveillance camera to involve viewers in their own voyeurism. – Getty Museum, “Please Be Seated”, Solo Commissioned Video Installation, 2007–09, from brochure publication, written by Peggy Fogelman.

Born: 1970, United States of America

Period: Contemporary Art

Education: Hampshire College, University of Southern California

== Life and career  ==
Born in Falmouth, Massachusetts in 1970, Nicole Cohen received a Bachelor of Arts degree from Hampshire College in Amherst, Massachusetts and a Master of Fine Arts degree from the University of Southern California. She has exhibited at the Williams College Museum of Art (Williamstown, MA), the Fabric Workshop and Museum (Philadelphia, PA), the Los Angeles County Museum of Art, and the New York Public Library. She has also shown internationally in Berlin, Germany; Bergen, Norway; Paris, France; Harajaku, Osaka, Kobe, and Tokyo, Japan; and Shanghai,China.

Cohen’s work is positioned at the intersection of contemporary reality, personal fantasy, and culturally constructed space. She consistently explores her interest in engaging the audience and challenging notions of lifestyle, domesticity, celebrity, and social behavior. Although trained in painting and drawing, Cohen most frequently uses video as her medium, playing upon its intrinsic capacities to manipulate time, distort scale and environment, and overlay imagery.

Cohen grew up in Washington, D.C. and Woods Hole, Cape Cod. She lived in Berlin, Germany for four years and currently lives in NYC.<ref>[http://www.aptglobal.org/view/article.asp?ID=2225] Nicole Cohen Biography.</ref>

=== Collective ===
Nicole Cohen is the Founder/ Director of the [http://berlincollective.de/ Berlin Collective].  Berlin Collective was founded in 2009, and is an artist platform for international exchange to support the arts and American culture. Cohen focused on creating an artist/ curator run collective based in NYC and mostly Berlin with the aim to provide opportunities, knowledge and intellectual growth to art professionals by strengthening international discourse. .<ref>[http://berlincollective.de/about-2] Berlin Collective Mission. {{webarchive |url=https://web.archive.org/web/20120312124950/http://berlincollective.de/about-2 |date=March 12, 2012 }}</ref>

=== Museum exhibitions ===
* Brooklyn Museum, Brooklyn, New York (2012)<ref>[http://broadwayworld.com/article/Brooklyn-Museums-Annual-Gala-Set-for-418-20120228] Brooklyn Museum Artists Ball.</ref>
* Katzen Arts Center, American University Museum, Washington, D.C. (2011)<ref>[http://www.american.edu/cas/museum/gallery/nicole-cohen-2011.cfm] American University Museum of Art: Nicole Cohen.</ref>
* The J. Paul Getty Museum, Los Angeles, CA, (2007–09)<ref>[http://www.getty.edu/art/gettyguide/videoDetails?segid=4113] J. Paul Getty Museum.</ref>
* Williams College Museum of Art, Williamstown, MA, (2003)<ref>[http://thewilliamsrecord.com/2004/01/13/students-participate-in-museum-associates-program/] The Williams Record.</ref>
* Los Angeles County Museum of Art, Los Angeles, CA, (2001),<ref>[http://www.lacma.org/seeing/brown.html] Los Angeles County Museum of Art.</ref> Autostadt, Wolfsburg, Germany, Schloss Britz, Berlin, Germany

===Galleries===
[http://morganlehmangallery.com/artists/nicole-cohen/ Morgan Lehman Gallery, NYC] <br/>
[http://shoshanawayne.com/artist.php?id=130583&name=Nicole_Cohen Shoshana Wayne Gallery, LA] <br/>
La B.A.N.K.Galerie, Paris

=== Selected video works ===
* Grand Maison, video, 2009 [http://www.visualartsource.com/index.php?page=editorial&pcID=26&aID=146 still from video]
* Please Be Seated, Video Installation, 2007-2009 [http://www.getty.edu/art/gettyguide/videoDetails?segid=4113 video]
* How to Make Your Windows Beautiful, Video Installation, 2005 [http://www.frequency.com/video/how-to-make-your-windows-beautiful-400/24890156 video]
* Sunday Morning, video, 2003 [http://nicolecohen.org/wp-content/uploads/2011/03/Still.jpg still from video]
* 40-Love, Video Installation, 2003 [https://www.youtube.com/watch?v=TolbW6GFEkM video]
* Van Fantasies, video, 2001 [https://www.youtube.com/watch?v=93FrWjaCGdY video]

== References ==
{{Reflist}}

== External links ==
* [http://www.nicolecohen.org/ Nicole Cohen – Official website]
* [http://morganlehmangallery.com/artists/nicole-cohen/ Morgan Lehman Gallery: Nicole Cohen]
* [http://shoshanawayne.com/artist.php?id=130583&name=Nicole_Cohen Shoshana Wayne Gallery: Nicole Cohen]
* [http://www.berlincollective.de/ Berlin Collective]
* [[Artbound initiative|ArtBound Initiative Internship Program]]

{{DEFAULTSORT:Cohen, Nicole}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:1970 births]]
[[Category:Living people]]