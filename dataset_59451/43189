{{Refimprove|date=December 2009}}

<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
  |name = Martin 156
  |image = Martin 156.jpg
  |caption=Martin M-156 "Russian Clipper"
}}{{Infobox aircraft type
  |type = [[Flying boat]] [[airliner]]
  |manufacturer = [[Glenn L. Martin Company]]
  |designer =
  |first flight =
  |introduced = 1940
  |introduction= 
  |retired = 1944
  |status =
  |primary user = [[Aeroflot]]
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970–1999, if still in active use but no longer built-->
  |number built = 1
  |unit cost =
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles =
}}
|}

The '''[[Glenn L. Martin Company|Martin]] 156''', referred to in the press variously as the "Russian Clipper", "Moscow Clipper", or "Soviet Clipper" was a very large [[flying boat]] aircraft intended for trans-Pacific service. The single example of the M-156 was designed and built in response to a request from [[Pan American World Airways]] to provide a longer-range replacement for the [[Martin M-130]].

==Design and development==

Martin delivered three Martin Ocean Transport Model-130s (M-130) to Pan Am in 1935. The aircraft became aviation icons of their day and were flown by Pan Am as the ''[[China Clipper]]'', ''[[Pan Am Flight 1104|Philippine Clipper]]'' and ''[[Hawaii Clipper]]''.<ref>Barry Taylor, ''Pan American's Ocean Clippers'', TAB Aero, 1991. pp. 112, 116, 120–121. ISBN 0-8306-8302-X.</ref> Unfortunately for Martin, Pan Am rejected the M-156 in preference to the now classic [[Boeing 314]]. The Martin's model numbers reflected the aircraft's wing span.

Pan Am was seeking to expand its trans-Pacific air service between [[San Francisco]] and [[Hong Kong]] in [[1937 in aviation|1937]]. This route had been pioneered by the Martin M-130 and Pan Am was in need of a larger aircraft.  The San Francisco / Hawaii flight was 2,400 miles and took 18 – 20 hours. Pan Am would have configured the M-156 as a 26-berth sleeper.  The trans-Pacific hops between Hawaii / Midway island / Wake Island / Guam / Manila / Hong Kong were less than half the California / Hawaii leg. With a lower fuel load requirement, the M-156 could carry additional passengers. The M-156 would have been converted to 33 - 56 day trip configuration. Pan Am and Matson Liners advertised an "Air-Sea Cruise" where Matson Liners carried passages from San Francisco to Honolulu.  Passengers would then transfer to Pan Am Clippers for westward flights to China and the Orient.<ref>An American Saga – Juan Trippe and His Pan Am Empire, Robert Daily: © 1990 Random House</ref>

After Pan Am selected the Boeing 314,<ref>Barry Taylor, ''Pan American's Ocean Clippers'', TAB Aero, 1991. pp. 160. ISBN 0-8306-8302-X. "On July 31, 1936, [Pan Am and Boeing] signed a contract for six aircraft, designated the B-314."</ref> Martin negotiated a deal with the Soviet Union for this aircraft and the M-156 was never put into regular trans-Pacific service. The M-156 was sold to the Soviets and operated by [[Aeroflot]] on the Soviet Union's far-east routes under the designation PS-30.

Like the M-130, the M-156/PS-30 was a 4-engined, high-wing design with the wing mounted atop a pylon extension of the fuselage (hull). While the M-156/PS-30 retained the same length as its predecessor, its wingspan was increased by more than 27&nbsp;ft (8.2&nbsp;m) with the addition of [[flap (aircraft)|flap]]s for increased control. The M-156/PS-30 also differed from the M-130 in having a horizontal stabilizer mounted atop a pylon at the rear of the hull, with twin vertical stabilizers and twin rudders located atop the horizontal stabilizer.<ref>Pan Am – an Airline and its Aircraft, R.E.G. Davies Illustrated by Mike Machat, © 1987 Paladar Press</ref>

Along with the increase in wing size, fuel capacity was expanded from the M-130's 3,165&nbsp;gal (11,981&nbsp;l) to a total of 4,260&nbsp;gal (16,126&nbsp;l) in the M-156/PS-30. Power for each of the four engines increased from 850&nbsp;hp (634&nbsp;kW) to 1,000&nbsp;hp (746&nbsp;kW) utilizing the more powerful [[Wright Cyclone series|Wright Cyclone]] G2 radials.<ref>Pan Am – an Airline and its Aircraft, R.E.G. Davies Illustrated by Mike Machat, © 1987 Paladar Press</ref>

The Soviet government purchased the M-156 from Martin in 1937. The sale included a set of production plans, engineering specifications and manufacturing licenses as the Soviets intended to mass-produce this aircraft. The German invasion of the Soviet Union in 1941 nixed these plans.

The single M-156/PS-30 was put into regular service in 1940 by Aeroflot and was utilized in the Soviet Far East along the Pacific coast. In this role, Aeroflot configured the aircraft to carry up to 70 passengers. It was flown by Aeroflot until 1944, at which time it was scrapped.<ref>Aeroflot – an Airline and its Aircraft, R.E.G. Davies Illustrated by Mike Machat, © 1992 Paladar Press</ref>

==Specifications (Martin 156C)==
[[File:Aeroflot Martin 156 Russian Clipper.jpg|thumb|right|The M-156 in Aeroflot service]]
{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref={{Citation needed|date=August 2011}}
|crew=
|capacity=53 passengers (San Francisco - Hawaii)<br> 26 passengers (San Francisco - Hong Kong)
|payload main=
|payload alt=
|length main=  92&nbsp;ft 2 in
|length alt=28.11 m
|span main=157&nbsp;ft
|span alt=47.87 m
|height main=27&nbsp;ft 2 in
|height alt=8.29 m
|area main=
|area alt=
|airfoil=
|empty weight main= 31,292&nbsp;lb
|empty weight alt=14,194&nbsp;kg
|loaded weight main= 60,708&nbsp;lb
|loaded weight alt=27,561&nbsp;kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)=[[Wright Cyclone series|Wright Cyclone]] G2
|type of prop=radial piston engine
|number of props=4
|power main= 1,000&nbsp;hp
|power alt=745&nbsp;kW
|power original=
|power more=
|propeller or rotor?=<!-- options: propeller/rotor -->
|propellers=
|number of propellers per engine=
|propeller diameter main=
|propeller diameter alt=
|max speed main= 182&nbsp;mph
|max speed alt= 293&nbsp;km/h
|cruise speed main=
|cruise speed alt=
|cruise speed more
|stall speed main=
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|range main= 3,000 miles
|range alt=4,830&nbsp;km
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main=
|ceiling alt=
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|avionics=

}}

==See also==
*[[Boeing 314 Clipper]]
*[[Dornier Do X]]
*[[Kawanishi H6K]]
*[[Latécoère 521]]
*[[Latécoère 631]]
*[[Short Empire]]
*[[Short S.26]]
*[[Sikorsky S-40]]
*[[Sikorsky S-42]]

==References==
{{Reflist}}

==External links==
{{commons category-inline|Martin 156}}

{{Martin aircraft}}

[[Category:United States airliners 1930–1939]]
[[Category:Flying boats]]
[[Category:Martin aircraft|156]]
[[Category:Aeroflot]]
[[Category:Four-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Individual aircraft]]