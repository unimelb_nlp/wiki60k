{{about|the cricket club|the T20 team|Kongonis}}
{{Infobox cricket team
|county = Kenya Kongonis
|image = [[File:Kongonis Cricket Club Logo.jpg]]
|oneday =
|secondteam =
|chairman= {{flagicon|Australia}} [[Dr. Phil Toye]]
|captain = {{flagicon|United Kingdom}} [[Chris Feist]]
|odcaptain =
|overseas =
|colors = 
|founded = 1927
|ground = [[Nairobi Club Ground]], [[Nairobi]]
|capacity = 
|fcdebutvs =
|fcdebutyr = 
|fcdebutvenue = 
|champs = 
|nlwins =
|fptwins = 
|t20wins = 
|website = [http://www.kenyakongonis.com/pages/default.asp Welcome to the Kenya Kongonis Cricket Club]
}}
The '''Kenya Kongonis Cricket Club''' also abbreviated as '''Kenya Kongonis''', is a Kenyan domestic cricket club based in the [[Nairobi Club Ground]], [[Nairobi]]. Founded in 1927, it is one of the oldest and most prestigious cricket clubs in Kenya. It takes part in the Nairobi-based NPCA ([[Nairobi Provincial Cricket Association]]) Leagues and in the national cricket tournaments such as the East African tournaments, run by [[Cricket Kenya]].<ref>
[http://www.kenyakongonis.com/pages/default.asp Official Website] Kenya Kongonis. Retrieved 3 January 2012</ref>

Kongonis is T20 franchise [[cricket team]] in the [[East Africa Premier League]] and [[East Africa Cup (cricket)|East Africa Cup]], tournaments started by [[Cricket Kenya]]. The team is based in [[Nairobi]], [[Kenya]] and is operated by the Kenya Kongonis Cricket Club which is a member of the [[Nairobi Province Cricket Association]].

==East African Tournaments Record==
[[File:Kongonis in EACC.jpg|thumb|right|Kongonis Logo in the East African Tournaments.]]
The [[Twenty20]] competition of [[East Africa Premier League]] and the 50-over competition of the East Africa Cup was initiated by [[Cricket Kenya]] to raise the profile of the game and develop a grassroots structure that would consistently provide young talent to perform for the national team in international events.<ref>
[http://www.espncricinfo.com/kenya/content/story/527519.html Kenya prepare for new superleagues] ESPNCricinfo. Retrieved 3 January 2012</ref> In the inaugural tournament, the 20-over competition were mostly dominated by the Ugandan teams invited to compete in the tournaments, but they dominated the 50-over competition, finishing top of the table, winning 6 games out of 10 matches,<ref>
[http://www.espncricinfo.com/ci/engine/series/527518.html?view=pointstable East African Cup, 2011/12 / Points table] ESPNCricinfo. Retrieved 3 January 2012</ref> and is due to play the final against the against Nile Knights, one of two Ugandan teams along with Rwenzori Warriors to compete in the tournaments.<ref>
[http://www.espncricinfo.com/ci/engine/match/527588.html 2011-12 East African Cup Final Scorecard] ESPNCricinfo. Retrieved 3 January 2012</ref>

==History==
The franchise was first formed when the East African competitions were formed in the wake of the 2011 World Cup disaster. Kongonis were one among the four Kenyan teams among those tournaments.
Although they did not make much of an impression in the inaugural East African Premier League, dominated by the two Ugandan sides, they did dominate in the opening rounds. It was a different story in the 50-over counterpart, the East African Cup, though. They totally dominated the tournament, and except the occasional Ugandan good performance, nearly every franchise appeared minnows in front of them. They topped the group with 28 points from 10 games (6 wins and only two losses),<ref>
[http://www.espncricinfo.com/ci/engine/series/527518.html?view=pointstable East African Cup, 2011/12 / Points table] Cricinfo. Retrieved 2 February 2012</ref> and then won comprehensively in the twice-postponed final against Ugandan side [[Nile Knights]] (the other being [[Rwenzori Warriors]]) by 84 runs. After they posted a massive 280/5 ([[Rakep Patel]] 92, [[Duncan Allan]] 65), Pakistani-born Kenyan cricketer [[Abdul Rehman (Kenyan cricketer)|Abdul Rehman]] took 4-32 as their opponents could only manage 196/9.<ref>
[http://www.espncricinfo.com/kenya/content/story/551567.html Kongonis victory gives Kenya boost] Cricinfo. Retrieved 2 February 2012</ref>

==2012 EAPL Squad==
* {{flagicon|KEN}} [[Shem Obado]] ''' ([[Captain (cricket)|c]])
* {{flagicon|KEN}} [[Alex Obanda]]
* {{flagicon|KEN}} [[Collins Obuya]]
* {{flagicon|KEN}} [[Emmanuel Bundi]]
* {{flagicon|KEN}} [[Elijah Otieno]]
* {{flagicon|KEN}} [[Ken Owino]]
* {{flagicon|KEN}} [[Amit Shukla]]
* {{flagicon|KEN}} [[William Rudd]]
* {{flagicon|KEN}} [[Eugene Maneno]]
* {{flagicon|KEN}} [[Pramveer Singh]]
* {{flagicon|KEN}} [[Henry Rudd]]
* {{flagicon|KEN}} [[Abdul Rehman (Kenyan cricketer)|Abdul Rehman]]
* {{flagicon|KEN}} [[Martin Mworia]]
* {{flagicon|KEN}} [[Nick Oluoch]]
The squad list is updated as of the 2012-13 East African Premier League.<ref>
 http://www.facebook.com/kenyacricket?ref=stream</ref>

==Notable Players==
Here is a list of Kongonis cricketers who have gone on to achieve something extraordinary or some other famous achievement.<ref>
[http://www.kenyakongonis.com/pages/default.asp?Id=87 Hall of Fame] Kenya Kongonis. Retrieved 3 January 2012</ref>
* [[Maurice Walter]] and [[Philip Walter]] - A match-winning pair of cricketers who excelled in all departments of the game.
* [[Arthur H. Kneller]] - After playing 8 first class games for Hampshire, Arthur Kneller emigrated to Kenya through work.  His subsequent batting feats make him a worthy inhabitant of the Hall of Fame. His record batting average for Gymkhana (Nairobi Club) of 97.86 (which included 5 centuries) has never been surpassed. He scored over 1000 runs in 1935/36. His first 3 innings were unbeaten centuries. 1941/2:He scored 1187 runs at an average of 69.82. 1942/3:He scored 1153 runs at an average 82.35. Also an outstanding [[wicketkeeper]].
* [[Bill Rand-Overy]] - 1936 English tour; 414 runs at 46, 27 wickets at 10.51, 1935 to 1940:6 matches for Europeans v Asians topped batting averages with 357 runs at 39.66 and took  18 wickets at 17.00. 1938/9:Scored 1,013 runs at 53.31
* [[E.J. Boase]] - Cricket Coach at Prince of Wales School. 1945/6:Scored 1,200 runs at 46.0 and 160 wickets at 8.8
* [[D.W. Dawson]] - An iconic figure and brother of Ossie Dawson the SA cricketer. 1947/8:Scored 155n.o. KKCC v. Uganda Kobs. For the season scored 1174 runs and took 109 wickets at 10.77. 1948/9:Took 97 wickets at 10.2 and scored 884 runs at 36.8
* [[D.G. Hunter]] - 1947/8:Scored 2009 runs at 77.0 and took 103 wickets at 9.0
* [[R.W. Smith]] - He was a fine left arm bowler with plenty of pace and lift with an enviable easy action. On 2 UK tours, in 1947 and 1951, he took a remarkable 85 wickets at an impressive average of 6.16.

==Statistics==

===UK Averages===
The Kongonis Cricket Club has toured the UK for many years. The statistics have been sourced from the official website, where the stats are given.
* Most Runs Scored - M. Brawn (5334)<ref>
[http://www.kenyakongonis.com/documents/KKCC%20Total%20runs%2065%20-%2010.pdf Overall Runs Scored - PDF] Kenya Kongonis. Retrieved 3 January 2012</ref>
* Most Wickets Taken - D. Breed (214)<ref>
[http://www.kenyakongonis.com/documents/KKCC%20Total%20wickets%2065%20-%2010.pdf Overall Wicket Taken - PDF] Kenya Kongonis. Retrieved 3 January 2012</ref>
* Record wicket partnerships - 240 (P Dunt 138, P Prodger 123*)<ref>
[http://www.kenyakongonis.com/documents/KKCC%20Record%20Wicket%20Partnerships%2065%20-%2010.pdf Record Wicket Partnerships - PDF] Kenya Kongonis. Retrieved 3 January 2012</ref>
* Batting Averages (all Kongonis) - J. Abeed (25.13)<ref>
[http://www.kenyakongonis.com/documents/KKCC%20Batting%20av%2065-10.pdf Batting Averages (all Kongonis) - PDF] Kenya Kongonis. Retrieved 3 January 2011</ref> Top allrounders-N Trestrail 4135 runs, 176 wkts.
* Bowling Averages (all Kongonis) - C. Acheson - 22.54<ref>
[http://www.kenyakongonis.com/documents/KKCC%20Bowling%20av%2065-10.pdf Bowling Averages (all Kongonis) - PDF] Kenya Kongonis. Retrieved 3 January 2012</ref>
Statistics correct as of 1965-2010.

==Upcoming Fixtures==
*In January 2012, they will playoff the East African Cup final in a bid to be crowned as the 50-over champions.<ref>
[http://www.espncricinfo.com/ci/engine/match/527588.html 2011-12 East African Cup Final Scorecard] ESPNCricinfo. Retrieved 3 January 2012</ref>
*In March and April 2012, Kongonis will host UK. The return leg will be played in August, at the UK, as the cricket club will compete against some cricket clubs there.<ref>
[http://www.kenyakongonis.com/pages/default.asp?Id=74 UK Tour Dates 2011] Kenya Kongonis. Retrieved 3 January 2012</ref>

==Home Ground==
The Kongonis franchise's home ground is the [[Nairobi Club Ground]] at [[Nairobi]]. It is one of the oldest and one of the best cricket grounds in the country. It was one of the 2003 Cricket World Cup venues, which Kenya co-hosted alongside [[South Africa]] and [[Zimbabwe]].<ref>
[http://www.espncricinfo.com/kenya/content/ground/58657.html Nairobi Club Ground Profile] Cricinfo. Retrieved 2 February 2012</ref>

==External links==
* [http://www.kenyakongonis.com/pages/default.asp Official Website]
* [http://www.espncricinfo.com/ci/content/team/45273.html Kongonis] on [[ESPNCricinfo]]
* [http://cricketarchive.com/Archive/Teams/5/5814/5814.html Kongonis] on [[CricketArchive]]
* [http://cricketarchive.com/Archive/Seasons/Tours_1964.html Tours in 1964] on [[CricketArchive]]

== References ==
{{reflist}}

{{Cricket in Kenya}}
[[Category:Kenyan club cricket teams]]