{{more citations|date=January 2017}}
{{Infobox website
| name = ''Flow''
| logo = [[File:Flow_Journal_Logo.png|alt=|200px]]
| logocaption =
| screenshot =
| collapsible =
| collapsetext =
| caption =
| url = {{URL|flowjournal.org}}
| slogan = A Critical Forum on Television and Media Culture
| commercial = No
| type = Online journal
| registration = 
| language =
| num_users =
| content_license =
| owner =  Radio-Television-Film Department, The University of Texas at Austin
| author = 
| editor = 
| launch_date = {{start date and age|2004}}
| alexa = 
| revenue =
| current_status = Active
| footnotes =
}}

'''''Flow''''' is an online journal of [[television studies|television]] and [[Media studies|media]] studies, published by the Department of Radio-TV-Film at the [[University of Texas at Austin]].<ref>{{cite web|title=Flow|url=https://www.flowjournal.org|website=Flowjournal.org}}</ref>

It was conceived by graduate students Christopher Lucas and Avi Santo and launched in October 2004. 

''Flow'' is intended to foster conversations amongst media scholars and non-academic communities. ''Flow'''s mission is to provide a space where researchers, teachers, students, and the public can read about and discuss the changing landscape of contemporary media at the speed that media move. 

''Flow'' is organized around short, topical columns written by respected media scholars on a bi-weekly schedule. These columns invite response from the critical community by asking provocative questions that are significant to the study and experience of media.

==Journal staff==
The journal is edited and organized by RTF graduate students at the University of Texas at Austin. Issues are managed by coordinating editors, and columns are assembled, solicited, and edited by a team of graduate student editors.

The Former Coordinating Editors include:

Chris Lucas (Vols. 1-2)<br />
Avi Santo (Vols. 1-2)<br />
Marnie Binfield (Vols. 3-5)<br />
Bryan Sebok (Vols. 3-4)<br />
Matthew Thomas Payne (Vols. 5-6)<br />
Alexis Carreiro (Vols. 6-7)<br />
Peter Alilunas (Vol. 7)<br />
Annie Petersen (Vols. 8 - ?)<br />
Jacqueline Vickery (Vols. 8 - 11)<br />
Alexander Cho (Vols. 11-12)
William Moner (Vols. 12-14)
Colin Tait (Vols. 13-14)
Paul Gansky (Vols. 14-17)
Alfred Martin (Vols. 14-17)
Keara Goin (Vols. 18-21)
Adolfo Mora (Vols. 18-21)
Jacqueline Pinkowitz (Vol. 22-)
Tim Piper (Vol. 22-)

==Flow Conference==
Flow is also affiliated with a biennial television and media culture conference hosted in [[Austin, Texas]]. The first Flow Conference, which brought together Flow columnists, scholars, industry representatives, and students interested in the topics covered by the journal, was held October 26-29, 2006 at the University of Texas at Austin.

==References==
{{reflist}}

==External links==
*[http://www.flowtv.org FlowTV]

[[Category:Television studies journals]]
[[Category:Media studies journals]]