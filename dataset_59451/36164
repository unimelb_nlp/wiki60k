{{good article}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox book
| name             = The Almost Nearly Perfect People: The Truth About the Nordic Miracle
| image            = File:The Almost Nearly Perfect People.jpg
| caption          = First edition cover
| author           = [[Michael Booth]]
| title_orig       = 
| translator       = 
| illustrator      = 
| cover_artist     = 
| country          = United Kingdom
| language         = English
| series           = 
| subject          = [[Nordic countries]]
| genre            = Non-fiction
| publisher        = [[Jonathan Cape]]
| pub_date         =
| english_pub_date = 
| media_type       = 
| pages            = 416
| isbn             = 9780224089623
| oclc             = 
| dewey            = 
| congress         =
| preceded_by      = 
| followed_by      = 
}}
'''''The Almost Nearly Perfect People: The Truth About the Nordic Miracle''''' is a 2014 nonfiction book by the British journalist [[Michael Booth]]. In the book, Booth focuses on the five [[Nordic countries]]—[[Denmark]], [[Iceland]], [[Norway]], [[Finland]] and [[Sweden]]—dedicating a section of the book to each one. He began writing the book after migrating from England to Denmark, based on his perceptions of the Nordic region before and after moving. He wanted to present an alternative perspective to the extremely positive depiction of the region in British media. The book received mixed reviews: some critics found it to be overly critical with poor humour, others praised its tone and informativeness.

==Background and release==
Michael Booth began writing ''The Almost Nearly Perfect People'' when he moved from England to Denmark about 15 years before its publication in 2014.<ref name=bbcp>{{cite web|url=http://www.bbc.co.uk/programmes/p01s51n4|title=Michael Booth introduces The Almost Nearly Perfect People|date=14 February 2014|accessdate=9 August 2014|publisher=[[BBC Radio 4]]|type=audio}}</ref> Before moving, he had perceived Scandinavians to be a "bearded, woolly jumper-wearing, recycling bunch of people", but afterwards was surprised by how different each of the Nordic countries seemed to be.<ref name=bbcb>{{cite web|url=http://www.bbc.co.uk/programmes/b03tqx9s|title=Simon Reeve|date=8 February 2014|publisher=[[BBC Radio 4]]|accessdate=15 December 2014|type=audio|at=17:12–31:12}}</ref> He wanted to write a book to explore these differences and to explain what he saw as a "fascinating dysfunctional family dynamic" between the five Nordic neighbors.<ref name=bbcb/> He was further inspired by the "Nordic wave" phenomenon that gained popularity in the 2000s and 2010s when the western world became fascinated with the Nordic countries and their ways of life.<ref name=bbcp/> In particular, he wanted to investigate Denmark's consistently high scores on various happiness indexes, since these figures conflicted with his own observations that "they didn't seem that happy", and also challenge the perception that the Nordic nations as a group are "little jolly green countries in the north".<ref name=bbcp/>

Booth undertook four years of research while writing the book, including travelling to each of the countries and interviewing prominent political and cultural figures of each nationality.<ref name=bbcp/> In writing about each country, he tried to examine both their successes and their weaknesses to "rebalance the utopian view" of Scandinavia held by many British people and to present a different perspective of the region than the extremely positive depiction in a lot of British media.<ref name=bbcb/> The tone of the book was inspired by [[Simon Winder]]'s ''Germania'', which combines humour with an historical and travel-based narrative.<ref name=bbcp/>

The book was published in English on 14 February 2014 by [[Jonathan Cape]].<ref name=bbcp/> Its first publication, however, was in September 2013 as a Danish translation. Although the manuscript was in English, it was translated and published first in Denmark. On 23 October 2014, the book was translated into Finnish.<ref>{{Cite web|title = Pohjolan onnelat – karu totuus Suomesta, Ruotsista, Norjasta, Tanskasta|url = https://www.goodreads.com/book/show/23429995-pohjolan-onnelat-karu-totuus-suomesta-ruotsista-norjasta-tanskasta|website = Goodreads|accessdate = 2016-01-06}}</ref> The Polish translation came on 7 October 2015.<ref>{{Cite web|title = Skandynawski raj. O ludziach prawie idealnych|url = https://www.goodreads.com/book/show/26808278-skandynawski-raj-o-ludziach-prawie-idealnych|website = Goodreads|accessdate = 2016-01-06}}</ref> In addition, a Norwegian translation of the book is planned.<ref name=gbt>{{cite web|url=http://gbtimes.com/life/almost-nearly-perfect-nordic-stereotypes-and-beyond|date=19 March 2014|first=Sara|last=Steensig|work=GB Times|title= Almost Nearly Perfect: Nordic stereotypes and beyond |accessdate=9 August 2014}}</ref>

==Content==
''The Almost Nearly Perfect People'' is divided into five sections for Denmark, Iceland, Norway, Finland and Sweden.<ref name=literary>{{cite web|url=http://www.literaryreview.co.uk/porter_02_14.php|date=February 2014|title= Great Danes?|work=[[Literary Review]]|first=Bernard|last=Porter|accessdate=9 August 2014}}</ref> Beginning with Denmark, Booth explains the Danish concept of ''[[Culture of Denmark#Hygge|hygge]]'' ("cosy times"), which he sees as conformism.<ref name=guardian2>{{cite web|url=https://www.theguardian.com/books/2014/feb/10/perfect-people-nordic-miracle-michael-booth-review|title= The Almost Nearly Perfect People: The Truth About the Nordic Miracle by Michael Booth – review |first=Mariella|last=Frostrup|date=10 February 2014|accessdate=9 August 2014|work=[[The Guardian]]}}</ref><ref name=independent>{{cite web|url=http://www.independent.co.uk/arts-entertainment/books/reviews/book-review-the-almost-nearly-perfect-people-by-michael-booth-9144478.html|title=Book review: The Almost Nearly Perfect People by Michael Booth|first=Susie|last=Mesure|date=23 February 2014|work=[[The Independent]]|accessdate=9 August 2014}}</ref> He criticises the Danish population's [[environmental footprint]] and notes that their taxation rate and levels of personal debt are among the highest in the world.<ref name=telegraph>{{cite web|url=http://www.telegraph.co.uk/culture/books/10622754/The-Almost-Nearly-Perfect-People-the-Truth-about-the-Nordic-Miracle-by-Michael-Booth-review.html|title=The Almost Nearly Perfect People: the Truth about the Nordic Miracle, by Michael Booth, review|date=9 February 2014|first=Alwyn|last=Turner|work=[[The Daily Telegraph]]|accessdate=9 August 2014}}</ref> Moving to Iceland, Booth details the banking practices that led to the collapse of the country's largest banks in the [[2008–11 Icelandic financial crisis|2008 financial crisis]], as well as the popular belief among Icelanders in the existence of ''[[Huldufólk]]'' (elves).<ref name=independent/><ref name=telegraph/>

In Norway, he highlights the rise of [[far-right politics]], the widespread opposition to immigration, and the multiple high-profile Norwegian [[neo-Nazis]].<ref name="guardian1">{{cite web|url=https://www.theguardian.com/books/2014/jan/22/nearly-perfect-people-nordic-miracle-review|date=22 January 2014|title= The Almost Nearly Perfect People: The Truth About the Nordic Miracle by Michael Booth – review |first=Ian|last=Thomson|work=[[The Guardian]]|accessdate=9 August 2014}}</ref> He notes that despite having a "nature-loving" reputation, Norway has a large [[ecological footprint]], and that the sale of fossil fuels accounts for much of the country's wealth.<ref name="independent" /><ref name="telegraph" /> He recounts Finland's history of heavy alcohol consumption and its high rates of murder, suicide, and [[antipsychotic]] drug use.<ref name="guardian2" /><ref name="telegraph" /><ref name="guardian1" /> He explains the Finnish concept of ''[[sisu]]'' and what he sees as the resulting obsession with machismo.<ref name="guardian2" />

Sweden receives Booth's strongest criticism,<ref name="gbt" /><ref name="literary" /> where consumerist influences are blamed for the apparent downfall of the [[Nordic model]] of [[social democracy]] and recurrent failures of the Swedish justice system.<ref name="guardian1" /> He argues that Sweden, with its strict rules of social etiquette, has a strong culture of conformity.<ref name="guardian2" />

'''Denmark''' 
* Chapter 1 - Happiness
* Chapter 2 - Bacon
* Chapter 3 - Gini
* Chapter 4 - Boffers
* Chapter 5 - Chicken 
* Chapter 6 - Vikings
* Chapter 7 - 72 per cent
* Chapter 8 - Hot-tub sanwiches
* Chapter 9 - The bumblebee
* Chapter 10 - Denim dungarees
* Chapter 11 - The Law of Jante
* Chapter 12 - Hygge
* Chapter 13 - Legoland and Other Spiritual Sites
* Chapter 14 - The happiness delusion

'''Iceland''' 
* Chapter 1 - Hakarl
* Chapter 2 - Bankers
* Chapter 3 - Denmark
* Chapter 4 - Elves
* Chapter 5 - Steam

'''Norway'''
* Chapter 1 - Dirndls
* Chapter 2 - Egoiste
* Chapter 3 - The new Quislings
* Chapter 4 - Friluftsliv
* Chapter 5 - Bananas
* Chapter 6 - Dutch disease
* Chapter 7 - Butter

'''Finland'''
* Chapter 1 - Santa
* Chapter 2 - Silence
* Chapter 3 - Alcohol
* Chapter 4 - Sweden
* Chapter 5 - Russia 
* Chapter 6 - School
* Chapter 7 - Wives

'''Sweden'''
* Chapter 1 - Crayfish
* Chapter 2 - Donald Duck
* Chapter 3 - Stockholm syndrome
* Chapter 4 - Integration
* Chapter 5 - Catalonians
* Chapter 6 - Somali pizza
* Chapter 7 - The party 
* Chapter 8 - Guilt
* Chapter 9 - Hairnets
* Chapter 10 - Class
* Chapter 11 - Ball bearings

==Reception==
{{Quote box
 |quote  = "It was interesting to observe the different reactions to my piece. The Finns were pretty cool; the Swedes, pedantic but resigned; the Danes did get a little fighty; the Icelanders were irritated not to have been given more attention; but the Norwegians, boy, they were not happy."
 |source = Michael Booth<ref>{{cite web|url=https://www.theguardian.com/world/2014/feb/05/scandinavian-miracle-denmark-finland-iceland-norway-sweden|title= 'The grim truth behind the Scandinavian miracle' – the nations respond |date=6 February 2014|accessdate=9 August 2014|work=[[The Guardian]]|first=Michael|last=Booth}}</ref>
 |width = 25%
|title = The Countries React|bgcolor = #F9F9A9}}
''The Almost Nearly Perfect People'' received mixed reviews from critics:
* [[Mariella Frostrup]] described the book for ''[[The Guardian]]'' as a "comprehensive and occasionally downright hilarious explanation of the Nordic miracle" and praised its "companionable, lightly mocking tone".<ref name="guardian2" /> 
* Alwyn Turner gave the book 4 (out of 5) stars in a review for ''[[The Daily Telegraph]]'', writing that "if [Booth's] tone is sometimes a little too jokey, his enthusiasm is contagious" and that "the real joy of the book" lay in the collection of interesting trivia.<ref name="telegraph" />
* The ''[[Literary Review]]''{{'s}} Bernard Porter found the book to be "a thoroughly entertaining read, written brilliantly", but criticised its largely impressionistic nature and the lack of sources and references.<ref name="literary" />
* [[Ian Thomson (writer)|Ian Thomson]] of ''The Guardian'' described the book as "informative, if strenuously humorous", but felt that Booth's "schoolboy humour" was at times "pretty embarrassing".<ref name="guardian1" />
* Anna Vesterinen, writing for the [[Rationalist Association]], felt that Booth relied too much on quoted studies and surveys and ought to have included more interviews with "ordinary locals".<ref>{{cite web|url=http://rationalist.org.uk/articles/4637/book-review-the-almost-nearly-perfect-people-by-michael-booth|title=Book review: The Almost Nearly Perfect People by Michael Booth|first=Anna|last=Vesterinen|date=17 April 2014|publisher=[[Rationalist Association]]|accessdate=9 August 2014}}</ref>
* The ''[[Financial Times]]''{{'}} Richard Milne wrote that, despite Booth's tendency to reinforce some stereotypes, "Behind the jokey tone is a lot of good material", and described the book as "a welcome rejoinder to those who cling to the idea of the Nordic region as a promised land".<ref>{{cite web|url=http://www.ft.com/intl/cms/s/2/19a18e2a-88fb-11e3-9f48-00144feab7de.html#axzz39RDNhzDw|title='The Almost Nearly Perfect People', by Michael Booth|date=31 January 2014|first=Richard|last=Milne|work=[[Financial Times]]|accessdate=9 August 2014}}</ref>
* Sara Steensig opined in the ''GB Times'' that the book's section on Iceland was somewhat superficial and that Booth's analysis of Sweden was too critical, but nevertheless, "while he does make a lot of fun of the Nordic countries, I think you can feel his affection for the inhabitants too."<ref name="gbt" />

==References==
{{reflist|30em}}

{{DEFAULTSORT:Almost Nearly Perfect People, The}}
[[Category:2014 books]]
[[Category:2014 non-fiction books]]
[[Category:British travel books]]
[[Category:Jonathan Cape books]]
[[Category:Nordic countries]]