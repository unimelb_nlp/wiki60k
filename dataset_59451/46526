{{Infobox company
| name             = Beyond Meat
| native_name      = 
| native_name_lang = <!-- Use ISO 639-1 code, e.g. "fr" for French. Enter multiple names in different languages using {{tl|lang}}, instead. -->
| romanized_name   = 
| trading_name     = 
| logo_caption     = 
| image            =Packagingwhite-0021 Cropped.png 
| image_caption    = 
| type             =Private 
| traded_as        = 
| predecessor      = 
| successor        = 
| founder          = Ethan Brown

| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| fate             = 
| area_served      = 
| key_people       = 
| industry         =Food 
| genre            = <!-- Only used with media and publishing companies -->
| brands           = 
| production       = 
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| aum              = <!-- Only for financial services companies -->
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| footnotes        = 
| intl             = 
| former_name      = 
| former type      = 
| foundation       = 2009 
| location_city    =El Segundo, CA 
| location_country = 
| locations        = <!-- Number of -->
| homepage         = {{URL|www.beyondmeat.com}} 
| bodystyle        = 
}}

'''Beyond Meat''' is a Los Angeles-based producer of [[meat substitute|plant-based meat]]. Beyond Meat's Products became available nationwide at [[Whole Foods Markets]] in 2013.<ref name=Foster2013 /><ref name=Flanagan2014>{{cite web |url=http://www.businessinsider.com/fake-chicken-beef-meat-from-plants-2014-7 |title= This Fake Meat Is So Good It Fooled Whole Foods Customers For 3 Days|publisher=Business Insider |author=Flanagan, Graham |date=7 July 2014 |accessdate=7 July 2014}}</ref><ref name=Strickland2013>{{cite web |url= http://www.inc.com/julie-strickland/ethan-brown-beyond-meat-wired-conference.html |title= Fake Meat for the Masses? |publisher= Inc |author= Julie Strickland |date= 05/07/2013 |accessdate=7 July 2014}}</ref>

Beyond Meat was founded by CEO Ethan Brown in 2009.<ref name=Gunther2013 /> The company has received venture funding from [[Kleiner Perkins Caufield & Byers]], [[Twitter|Obvious Corporation]], [[Bill Gates]], [[Biz Stone]], and the [[Humane Society]].<ref name=Fehrenbacher2013 /><ref name=Etherington2013 />

==Origins==
CEO Ethan Brown founded Beyond Meat, formerly Savage River Inc., in 2009 to produce [[Veganism|plant-based]] products that replicated meat and to attempt to eliminate "some of the downsides" of the meat industry in the process.<ref name=Foster2013 /><ref name=Gunther2013>{{cite web |url=http://fortune.com/2013/10/03/the-bill-gates-backed-company-thats-reinventing-meat/ |title= Beyond Meat closes in on the perfect fake chicken, turns heads, tastebuds |publisher= Fortune |author= Marc Gunther |date= Oct 3, 2013|accessdate=7 July 2014}}</ref><ref name = Chang2014 /><ref name=Barker2012>{{cite web |url= http://www.columbiatribune.com/news/local/chicken-substitute-to-be-made-in-columbia/article_1424b83c-17a0-5319-91e2-fee26bf59ecc.html |title= Chicken substitute to be made in Columbia |publisher= Columbia Tribune |author=Jacob Barker |date= Aug 7, 2012|accessdate=7 July 2014}}</ref> The inspiration for Beyond Meat began for Brown in "chicken country," where as a young boy he spent weekends on his family farm near the [[Pennsylvania]] border in rural [[Maryland]].<ref name= Noguchi2012/>

Brown initially contacted two [[University of Missouri]] professors, Fu-hung Hsieh and Harold Huff, who had already been refining their meatless protein for years.<ref name=Kile2014>{{cite web |url= http://america.aljazeera.com/watch/shows/techknow/blog/2014/1/15/need-to-know-tasteslikechickenmadeinalab.html |title= Need to Know: Tastes like chicken, made in a lab |publisher= Aljazeera America |author= Meredith Kile |date= Jan 15, 2014 |accessdate=7 July 2014}}</ref> The duo spent nearly a decade in the University lab working different temperatures and pressures to get their mixture of soybean and [[pea protein]]s "as close to the look and feel of real chicken as possible".<ref name=Kile2014/> It took them another five years to come up with a recipe for Beyond Meat's first product, "Chicken-Free Strips," which the company released to limited locations in 2012.<ref name= Hurley2014>{{cite web |url= http://blogs.riverfronttimes.com/gutcheck/2014/06/beyond_meat_it_tastes_like_chicken_sort_of.php |title= "Beyond Meat," the Mizzou-Creared Faux Chicken That Fooled the New York Times |publisher= Riverfront Times |author= Patrick J. Hurley |date= Jun 27, 2014|accessdate=7 July 2014}}</ref><ref name=Manjoo2012>{{cite web |url= http://www.slate.com/articles/technology/technology/2012/07/beyond_meat_fake_chicken_that_tastes_so_real_it_will_freak_you_out_.html |title= Fake Meat So Good it Will Freak You Out |publisher= Slate |author= Farhad Manjoo |date= Jul 2012 |accessdate=7 July 2014}}</ref>

Brown has stated that his long-term goal is to offer a product that can satisfy the world's growing demand for meat, especially in markets like [[India]] and [[China]].<ref name=Flanagan2014/><ref name = Manjoo2012/>

==History==

Ethan Brown founded Beyond Meat in 2009 as a potential solution to problems he saw with the meat industry.<ref name=Kanani2014>{{cite web |url= http://www.forbes.com/sites/rahimkanani/2014/03/06/the-future-of-meat-is-meatless-just-as-tasty-and-about-to-change-the-world/ |title= The Future of Meat is Meatless, Just as Tasty, And About to Change the World |publisher= Forbes |author= Rahim Kanani |date= 3 June 2014 |accessdate=7 July 2014}}</ref>

Beyond Meat first released its Chicken-Free Strips exclusively to Whole Foods Markets in Northern [[California]] in 2012.<ref name=Manjoo2012/> Soon after, the company began building a larger production facility with plans of adding meat-free ground beef and pork products to their repertoire.<ref name= Noguchi2012>{{cite web |url= http://www.npr.org/blogs/thesalt/2012/05/17/152519988/a-farmer-bets-better-fake-chicken-meat-will-be-as-good-as-the-real-thing |title= Betting Better Fake Chicken Meat Will Be As Good As The Real Thing  |publisher= NPR |author= Yuki Noguchi |date= May 17, 2012 |accessdate=7 July 2014}}</ref>

''[[Fast Company (magazine)|Fast Company]]'' listed Beyond Meat as one of the most innovative companies in 2014, and [[PETA]] named Beyond Meat as its company of the year for 2013.<ref name=Chang2014>{{cite web |url= http://www.cbsnews.com/news/meet-the-man-behind-beyond-meat/ |title= Meet the man behind "Beyond Meat" plant-based protein substitute|publisher= cbsnews |author= Lulu Chang|date= Feb 7, 2014 |accessdate=7 July 2014}}</ref><ref name=Black2014>{{cite web |url= http://www.fastcompany.com/most-innovative-companies/2014/beyond-meat |title= 43. Beyond Meat |publisher= Fast Company|author= Jane Black |date=  Feb 2, 2014|accessdate=7 July 2014}}</ref>

===Beyond Meat goes nationwide===

The company began selling its chicken-free products in Whole Foods Stores nationally in April 2013.<ref name=Gunther2013/><ref name=Fehrenbacher2013>{{cite web |url= http://gigaom.com/2013/05/07/beyond-meat-ceo-one-day-eating-meat-will-have-no-connection-to-animals/ |title= Beyond Meat CEO: One day eating meat will have no connection to animals |publisher= Gigaom|author= Katie Fehrenbacher |date= May 7, 2013|accessdate=7 July 2014}}</ref><ref name = Manjoo2012/><ref name=Brown2013>{{cite web |url= https://www.wired.com/2013/09/fakemeat/ |title= Tastes Like Chicken |publisher= Wired |author= Alton Brown |date= Sep 2013 |accessdate=7 July 2014}}</ref>

[[Atlanta]]-based [[Tropical Smoothie Café]] started carrying Beyond Meat products in May 2013 as a vegan-friendly substitute for items with chicken on the menu.<ref name=Gunther2013/><ref name= Nguyen2013>{{cite web |url= http://vegnews.com/articles/page.do?pageId=5961&catId=8 |title= Tropical Smoothie Café: Beyond Meat Here to Stay  |publisher= Veg News Daily|author= Melissa Nguyen |date= Jul 12, 2013|accessdate=7 July 2014}}</ref><ref name= Seward2013>{{cite web |url= http://www.ajc.com/news/business/tropical-smoothie-cafe-adds-beyond-meats-chicken/nXyD4/ |title= Tropical Smoothie Café adds Beyond Meat’s ‘chicken’|publisher= Atlanta Journal Constitution|author= Christopher Seward |date= May 21, 2013|accessdate=7 July 2014}}</ref>

===Obvious Corporation, Bill Gates, and other investors ===
[[Twitter|Obvious Corporation]], which was founded by Twitter co-founders [[Evan Williams (Internet entrepreneur)|Evan Williams]], [[Biz Stone]], and Jason Goldman, began backing Beyond Meat in June, 2013.<ref name= Etherington2013>{{cite web |url= http://techcrunch.com/2013/05/07/were-80-of-the-way-to-fake-meat-thats-indistinguishable-from-the-real-thing/ |title= We’re 80% of the way to fake meat that’s indistinguishable from the real thing|publisher= TechCrunch |author= Darrell Etherington |date= May 7, 2013|accessdate=7 July 2014}}</ref><ref name = Manjoo2012/><ref name=Schwartz2012>{{cite web |url= http://www.fastcoexist.com/1680007/biz-stone-explains-why-twitters-co-founders-are-betting-big-on-a-vegan-meat-startup |title= Biz Stone Explains Why Twitter’s Co-Founders Are Betting Big on a Vegan Meat Startup |publisher= |author= |date= Jun 13, 2012|accessdate=7 July 2014}}</ref><ref name=Schwartz2012/>

[[Bill Gates]] also became an investor in Beyond Meat in 2013 after he sampled the product and said he "couldn't tell the difference between Beyond Meat and real chicken".<ref name=Flanagan2014/><ref name=Gunther2013/>

The same year, [[Kleiner Perkins Caufield & Byers|Kleiner Perkins]], the [[Silicon Valley]] [[venture capital]] firm, made Beyond Meat its first investment in a food startup.<ref name=Gunther2013/><ref name=Schwartz2012/> The company also has backing from the [[Humane society|Humane Society]].<ref name=Black2014/>  In 2016 [[Tyson Foods]] purchased a 5% stake in the company.<ref>{{cite news|last1=Strom|first1=Stephanie|title=Tyson Foods, a Meat Leader, Invests in Protein Alternatives|url=https://www.nytimes.com/2016/10/11/business/tyson-foods-a-meat-leader-invests-in-protein-alternatives.html?_r=0|accessdate=14 October 2016|work=[[The New York Times]]|date=10 October 2016}}</ref>

==Founder Ethan Brown==
Beyond Meat founder Ethan grew up spending weekends visiting his father's dairy farm in western Maryland, an experience that made him concerned about the welfare of the animals he cared for. When he eventually became a vegan, his frustration over the limited availability of meat-free options inspired him to learn more about the industry, and eventually start his own company.<ref name= Noguchi2012/> After acquiring a master's degree in public policy and an MBA from Columbia University, he spent his young career working at Ballard Power Systems (BLDP), a fuel-cell company. While studying clean energy, Brown became acquainted with the contribution of livestock to resource depletion and climate change, and as a result shifted his focus from energy to food.<ref name=Gunther2013/><ref name=Kanani2014/><ref name= Bessette2014>{{cite web |url= http://fortune.com/2014/01/31/10-questions-ethan-brown-ceo-beyond-meat/ |title= 10 Questions: Ethan Brown, CEO, Beyond Meat|publisher= Fortune|author= Chanelle Bessette|date= 2014-01-31|accessdate=7 July 2014}}</ref>

A 2013 ''[[Fortune (magazine)|Fortune]]'' article noted Brown's support of animal welfare groups such as [[Farm Sanctuary]].<ref name=Gunther2013/>

==Products==
Beyond Meat develops and manufactures a variety of plant protein-based food products. The [[vegetarian]] meat substitutes are made from mixtures of [[soy protein]], [[pea protein|pea protein isolates]], yeast, and other ingredients.<ref name=Brown2013 /><ref name=Strom2014>{{cite web |url= https://www.nytimes.com/2014/04/03/business/meat-alternatives-on-the-plate-and-in-the-portfolio.html?_r=1 |title= Fake Meats, Finally, Taste Like Chicken |publisher= New York Times |author= Stephanie Strom|date= April 2, 2014|accessdate=7 July 2014}}</ref> As of 2014, the company's product offerings consisted of Beyond Chicken and Beyond Beef.<ref name=Flanagan2014 /><ref name=Barker2012 /><ref name=Brown2013 /><ref name=Nassauer2014 /> A vegan and soy-free burger patty, called the ''The Beast,'' was released in 2015. Beyond Meat products are available for purchase in packaged form as well as in retail-prepared dishes.<ref name=Flanagan2014 /><ref name=Barker2012 /><ref name=Brown2013 /><ref name=Schwartz2012 />
 
===Beyond Chicken===

Beyond Meat's Chicken-Free products, marketed as Beyond Chicken, are made from a mixture of soy and pea proteins, fiber, and other ingredients and are marketed as a healthy alternative to chicken meat.<ref name=Gunther2013 /><ref name=Schwartz2012 /> Ingredients are mixed and fed into a [[food extrusion]] machine which cooks the mixture while forcing it through a specially designed mechanism that uses steam, pressure, and cold water to form the product's chicken-like texture.<ref name=Brown2013 /> After being processed in the extrusion machine, the product is cut to size, seasoned, and grilled before being packaged.<ref name=Kile2014 /> Each batch of chicken takes approximately 90 minutes to produce.<ref name=Kile2014 />

The analog chicken is available as lightly seasoned strips, grilled strips, and southwest style strips.<ref name=Brown2013 />

===Beyond Beef===
The company's two flavors of Beyond Beef imitation [[ground beef]] product, Beefy and Feisty, are made from pea proteins, [[canola oil]], and various seasonings.<ref name=Schwartz2012 /><ref name=beyondmeatbeefcrumbles>{{cite web|url=http://beyondmeat.com/beef-free-crumbles-beefy/|title=Beef-free crumbles|publisher=Beyond Meat|accessdate=7 July 2014}}</ref> The soy and gluten-free pea protein mixture initially resembles a paste before being heated and processed by an extrusion machine.<ref name=Manjoo2012 />

The beefy crumbles possess the same protein content per 55 gram serving as traditional ground beef.<ref name=Gunther2013 /><ref name=Strom2014 />

===The Beast===
[[File:Beyond Meat Beast Burger 1.jpg|thumb|A Beast Burger]]
Beyond Meat announced in 2014 that it had begun development and testing of a new product called The Beast. The vegetable protein-based burger patties were taste tested by [[The New York Mets]] during a pre-game event.<ref name=Nassauer2014>{{cite web |url= https://www.wsj.com/articles/meatless-burgers-make-their-mlb-pitch-1403736799 |title= Meatless Burgers Make Their MLB Pitch|publisher= The Wall Street Journal |author= Sarah Nassauer |date= 2014-06-25 |accessdate=7 July 2014}}</ref><ref name=Just2014>{{cite web|url=http://www.ecorazzi.com/2014/07/18/beyond-meat-unleashes-new-beast-vegan-burger-on-ny-mets/|title=Beyond Meat Unleashes New ‘Beast’ Vegan Burger on NY Mets|publisher=Ecorazzi|date=7 July 2014|accessdate=7 July 2014}}</ref><ref name=abc2014>{{cite web|url=http://abcnews.go.com/GMA/video/beast-burger-packs-flavor-nutrients-patty-24313619|title=Beast Burger Packs Flavor, Nutrients|publisher=ABC News|date=7 July 2014|accessdate=7 July 2014}}</ref><ref name=Dean2014>{{cite web|url=http://vegnews.com/articles/page.do?pageId=6485&catId=8|title=Beyond Meat Pitching Beast Burger to Big Leagues|publisher=VegNews|date=7 July 2014|accessdate=7 July 2014}}</ref>

The Beast Burger was officially released in February 2015 and is available at Whole Foods markets.<ref name="Haber2015">{{cite web | url = http://nymag.com/next/2015/04/when-will-fake-meat-taste-like-real-meat.html| title = How Long Before Silicon Valley Can Produce Fake Meat That Tastes Like Real Meat?| date = April 9, 2015| author = Haber, Matt| publisher = New York Magazine| accessdate = April 16, 2015}}</ref>
Burger ingredients include powdered pea protein, water, sunflower oil, and an OIL BLEND or (CANOLA OIL, FLAXSEED OIL, PALMOIL*, SUNFLOWER OIL, DHA ALGAL OIL). A twin-screw extruder mixes, cooks, and pressurizes the ingredients. After leaving the extruder, the product is shaped into patties and then packaged for retail food sales.<ref name="Jacobsen2014">{{cite web | url = http://www.outsideonline.com/1928211/top-secret-food-will-change-way-you-eat?| title =The Top-Secret Food That Will Change the Way You Eat | date = December 26, 2014| author = Jacobsen, Rowan| publisher =Outside Magazine | accessdate = April 16, 2015}}</ref><ref name="Kumm2015">{{cite web | url =http://www.technologyreview.com/review/536296/the-problem-with-fake-meat/ | title = The Problem with Fake Meat| date =March 31, 2015 | author = Kummer, Corby| publisher = MIT Technology Review| accessdate = April 16, 2015}}</ref>

The burgers are vegan, soy-free, and contain 23 grams of protein in addition to antioxidants, iron, calcium, Vitamins B6, B12 & D, Potassium, DHA Omega-3s and ALA Omega-3s all of which may assist in muscle recovery.<ref name="Angle2015">{{cite web | url = http://www.shape.com/healthy-eating/meal-ideas/beyond-meat-s-high-protein-veggie-burger-best-thing-ever-happen-vegans| title =Beyond Meat’s High-Protein Veggie Burger Is the Best Thing to Ever Happen to Vegans | date =February 13, 2015 | author = Angle, Sara and Rachel Schultz| publisher = Shape Magazine| accessdate = April 16, 2015}}</ref><ref name="beyond2015">{{cite web | url = http://beyondmeat.com/products/view/the-beast-burger| title =The Beast Burger | date = April 2015| author = | publisher = Beyond Meat| accessdate = April 16, 2015}}</ref><ref name="beyond2015" /><ref name="Joiner2015">{{cite web | url =http://www.thedailybeast.com/articles/2015/01/15/the-veggie-burgers-with-meaty-ambition.html | title = The Veggie Burgers With Meaty Ambition| date =January 15, 2015 | author =Joiner, James | publisher = The Daily Beast| accessdate = April 16, 2015}}</ref>

=== The Beyond Burger ===
In May 2016, Beyond Meat released the first plant-based burger to be sold alongside beef, poultry  and pork in the meat section of the grocery store.<ref>{{Cite news|url=https://www.nytimes.com/2016/05/23/business/plant-based-the-beyond-burger-aims-to-stand-sturdy-among-meat.html|title=Plant-Based, the Beyond Burger Aims to Stand Sturdy Among Meat|last=Strom|first=Stephanie|date=2016-05-22|newspaper=The New York Times|issn=0362-4331|access-date=2016-10-27}}</ref> Making its debut at the Pearl St. Whole Foods in Boulder, CO, The Beyond Burger sold out within the first hour of hitting shelves.<ref>{{Cite news|url=http://www.chicagotribune.com/business/ct-beyond-burger-20160524-story.html|title=Meatless burger that bleeds beet juice sells out in an hour at its Whole Foods debut|last=Tribune|first=Chicago|newspaper=chicagotribune.com|access-date=2016-10-27}}</ref> 

The Beyond Burger contains 20 grams of protein and has no soy, no gluten, no GMOS, zero cholesterol, and half the saturated fat of an 80/20 beef burger.<ref>{{Cite web|url=http://www.beyondmeat.com/products/view/beyond-burger|title=The Beyond Burger™ {{!}} The Beyond Burger™ {{!}} Beyond Meat - The Future of Protein™|website=www.beyondmeat.com|access-date=2016-10-27}}</ref>

To see a comparison of animal-based beef and beyond meat beef, visit: http://beyondmeat.com

==Production==
The Beyond Meat factory is located in [[Columbia, Missouri]].<ref name=Foster2013>{{cite web |url= http://www.popsci.com/article/science/can-artificial-meat-save-world |title= Can Artificial Meat Save the World |publisher= Popular Science |author= Tom Foster |date= 2013-11-18|accessdate=7 July 2014}}</ref> The facility is capable of producing approximately {{convert|7|e6lb|kg|abbr=off|spell=on|sigfig=1}} of chicken substitute in one year.<ref name=Barker2012 />

==Reception==
Beyond Meat products have received favorable reviews. [[Mark Bittman]], a food journalist with ''[[The New York Times]]'', wrote that "you won't know the difference between that <nowiki>[Beyond Meat]</nowiki> and chicken. I didn't, at least, and this is the kind of thing I do for a living."<ref name=Wellman2012>{{cite web |url=http://www.dailymail.co.uk/femail/article-2159300/New-vegan-chicken-product-sells-days-entices-meat-lovers-promise-authentic-texture-flavour.html |title=New vegan 'chicken' sells out in days with promise of authentic texture and taste... but can it impress our meat-loving tester?|publisher=The Daily Mail |author=Wellman, Victoria |date=7 July 2014 |accessdate=7 July 2014}}</ref> Bill Gates wrote on his personal blog that, "I couldn't tell the difference between Beyond Meat and real chicken".<ref name=Foster2013 /> In 2013, chef and television personality [[Alton Brown]] wrote about Beyond Meat's Beyond Chicken that, "it's more like meat than anything I've ever seen that wasn't meat".<ref name=Brown2013/>

==References==
{{reflist}}

== External links ==
* [http://beyondmeat.com/ Official Web Site]

[[Category:Companies based in California]]
[[Category:Vegetarian cuisine]]
[[Category:Meat substitutes]]