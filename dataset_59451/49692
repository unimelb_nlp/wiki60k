{{other uses}}
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = Iracema, the honey lips: a legend of Brasil
| title_orig     = Iracema <br>Iracema - A Lenda do Ceará
| translator     = Lady [[Isabel Burton]] 
| image          = Estatua de Iracema da lagoa da Messejana.jpg
| caption        = Statue in honor of Iracema in [[Fortaleza]] 
| author         = [[José de Alencar]]
| illustrator    = 
| cover_artist   = 
| country        = [[Brazil]]
| language       = [[Portuguese language|Portuguese]]
| series         = Alencar's indigenist novels
| subject        = <!-- Subject is not relevant for fiction -->
| genre          = [[Romance novel]]
| publisher      = B. L. Garnier
| release_date   = 1865
| english_release_date = 1886
| media_type     = 
| pages          = 
| isbn =  978-0-85051-500-8
| isbn_note=<br>ISBN 978-0-85051-524-4 (English)
| preceded_by    = [[O Guarany]]
| followed_by    = [[Ubirajara (novel)|Ubirajara]]
}}

'''''Iracema''''' (in [[portuguese language|portuguese]]: ''Iracema - A Lenda do Ceará'') is one of the three indigenous novels by [[José de Alencar]]. It was first published in 1865. The novel has been adapted into films twice in 1917 as [[Iracema (1917 film)|a silent film]] and in 1949 as [[Iracema (1949 film)|a sound film]].

==Plot introduction==
The story revolves around the relationship between the [[Tabajara]] [[indigenous peoples in Brazil|indigenous woman]], Iracema, and the [[Portugal|Portuguese]] [[Colonialism|colonist]], [[Martim Soares Moreno|Martim]], who was allied with the Tabajara nation's enemies, the [[Pitiguaras]].

Through the novel, Alencar tries to remake the history of the Brazilian colonial state of [[Ceará]], with Moacir, the son of Iracema and Martim, as the first true [[Brazil]]ian in Ceará. This pure Brazilian is born from the love of the natural, innocence (Iracema), culture and knowledge (Martim), and also represents the mixture ([[miscegenation]]) of the native race with the European race to produce a new (Brazilian) race.

===Explanation of the novel's title===
''Iracema'' is [[Guarani language]] for ''honey-lips'', from ''ira'' - honey, and ''tembe'' - lips.  ''Tembe'' changed to ''ceme'', as in the word ''ceme iba'', according to the author.

"Iracema" is also an [[anagram]] of "America", appointed by critics as befitting the allegorization of colonization of [[Americas|America]] by Europeans, the novel's main theme.

==Characters in Iracema==
*Andira: Araquém's brother. Old warrior and hero of his people.
*Araquém: Iracema's father. Spiritual leader of the Tabajara's nation.
*Batuireté: Poti's grandfather
*Caubi: Iracema's brother
*Iracema: Araquém's daughter. She is the beautiful Tabajara woman with ''honey-lips'' and dark hair.
*Irapuã: The warrior leader of the Tabajara nation.
*Jacaúna: Poti's brother.
*Jatobá: Poti's father. He is an important veteran warrior of the Pitiguara's nation.
*Martim: Portuguese colonist. Named in honor of [[Mars (mythology)|Mars]], the Roman god of war.
*Moacir: The child of Martim and Iracema.
*Poti: Martim's friend and the Pitiguara warrior who is brother of the Pitiguara leader.

==Iracema and the Indianist Novels==
''Iracema'', along with the novels ''[[The Guarani|O Guarani]]'' and ''[[Ubirajara (novel)|Ubirajara]]'', portrays one of the stages of the formation of the Brazilian ethnic and cultural heritage. Iracema symbolizes the initial meeting between  the white man (Europeans) and the natives. 
"Moacir" means "Son of Pain", which is related to his birth, alone with his mother who was abandoned by Martim for some time when he had to go and help the Potiguaras in a tribal war against the Tabajaras.

==Awards and nominations==
[[File:Iracema (Antonio Parreiras, 1909).jpg|thumb|"Iracema", painting by [[Antônio Parreiras]]]]

*There is a Brazilian stamp in honor of ''Iracema'''s centennial (1865/1965) and its author.
*There is a Brazilian painting by [[Antônio Parreiras]].
*''Iracema'' is cited in ''Manifesto Antropófago'' (''[[Cannibal Manifesto]]''), which is published in 1928 by [[Oswald de Andrade]]

==References==
*Alencar, José de. ''Iracema'' (1865) Rio de Janeiro: B. L. Garnier. (in Portuguese) ISBN 0-85051-500-9
*Alencar, José de. ''Iracema, the honey lips: a legend of Brasil'' (1886) translated by Lady Isabel Burton. London: Bickers & Son. ISBN 0-85051-524-6
*Alencar, José de. ''Iracema'' (2000) translated by Clifford Landers. New York: Oxford University Press. ISBN 0-19-511547-3
*Burns, E. Bradford. ''A Working Bibliography for the Study of Brazilian History'' ''The Americas'', Vol. 22, No. 1 (Jul., 1965), pp.&nbsp;54–88

==External links==
{{Wikisourcelang|pt|Iracema|Iracema}}
{{Commons category|Iracema}}
*{{pt icon}} [http://www.academia.org.br/ Academia Brasileira de Letras]
*{{pt icon}} [http://educaterra.terra.com.br/literatura/ Brazilian literature]
*{{en icon}} [http://burtoniana.org/books/1886-Iracema/index.htm Iracema, The Honey-lips - A legend of Brazil]. (translated by Richard F. and Isabel Burton.)
* {{librivox book | title=Iracema | author=José de Alencar}}
{{José de Alencar}}

[[Category:1865 novels]]
[[Category:Novels by José de Alencar]]
[[Category:Portuguese-language novels]]
[[Category:Fictional indigenous people of the Americas]]
[[Category:Novels set in Brazil]]
[[Category:Ceará]]

[[bpy:ইরাসেমা]]
[[war:Iracema]]