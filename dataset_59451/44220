{{distinguish|Kristiansand Airport, Kjevik}}
{{Infobox airport
| name         = Kristiansund Airport, Kvernberget
| nativename   = {{smaller|''Kristiansund lufthavn, Kvernberget''}}
| image        = Kristiansund Airport.png
| image-width  = 300
| nativename-r = 
| image2       =
| image-width2 =
| caption      = 
| IATA         = KSU
| ICAO         = ENKB
| type         = Public
| owner        = 
| operator     = [[Avinor]]
| city-served  = [[Kristiansund]], [[Norway]]
| location     = [[Kvernberget]], [[Nordlandet]], Kristiansund
| metric-elev  = yes
| elevation-f  = 204
| elevation-m  = 62
| website      = [https://avinor.no/en/airport/kristiansund-airport/ Official website]
| coordinates  = {{coord|63|06|43|N|007|49|34|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_mapsize        = 300
| pushpin_label          = '''KSU'''
| pushpin_label_position = top
| pushpin_map_caption    = 
| metric-rwy   = yes
| r1-number    = 07/25
| r1-length-m  = 1,840
| r1-length-f  = 6,037
| r1-surface   = Asphalt
| stat-year    = 2014
| stat1-header = Passengers
| stat1-data   = 384,822
| stat2-header = Aircraft movements
| stat2-data   = 13,722
| stat3-header = Cargo (tonnes)
| stat3-data   = 111
| footnotes    = Source:<ref name=aip /><ref name=pax />
}}

'''Kristiansund Airport, Kvernberget''' ({{lang-no|Kristiansund lufthavn, Kvernberget}}; {{Airport codes|KSU<ref>{{cite web | url = http://www.iata.org/publications/Pages/code-search.aspx | title = IATA Airport Code Search (KSU: Kristiansund / Kvernberget) | publisher = [[International Air Transport Association]] | accessdate = 4 December 2012}}</ref>|ENKB|p=n}}) is an [[international airport]] serving [[Kristiansund]], [[Norway]]. It is situated at [[Kvernberget]] on the island of [[Nordlandet]] and is the sole scheduled airport serving [[Nordmøre]]. The airport features a {{convert|2390|m|sp=us|adj=on}} runway aligned 07/25. In addition to scheduled services operated by [[Scandinavian Airlines]] and [[Widerøe]], it serves offshore helicopter traffic to the [[Norwegian Sea]] operated by [[CHC Helikopter Service]]. Kvernberget handled 384,822 passengers in 2014.

Kvernberget was the second airport to open in [[Møre og Romsdal]], with flights commencing on 1 July 1970. Until 2004 the main portion of flights was provided by [[Braathens SAFE]], which flew both coastal flights and to [[Oslo]]. The first helicopter terminal opened in 1982 and a new passenger terminal was completed in 1989. A second helicopter terminal opened in 1994. The runway was extended for the original {{convert|1830|m|sp=us}} to the current length in 2012. A new terminal is under construction, due for completion in 2017.

==History==
The first airline services to Kristiansund were part of the Bergen to Trondheim route which [[Norwegian Air Lines]] (DNL) established in 1935. These called at [[Ålesund]], [[Molde]] and Kristiansund. The latter had its [[water aerodrome]] located at the port in the town center. The services were only flown during the winter. DNL resumed its routes in 1948, and Lufttransport commenced a service via [[Ålesund Airport, Sørneset]] to [[Oslo]]. From 1949 these were both taken over by [[West Norway Airlines]], which lasted until the airline folded in 1957.<ref name=oh8>Olsen-Hagen: 8</ref>

[[File:SAS RW25.png|thumb|left|[[Scandinavian Airlines]] [[Boeing 737]]]]
The [[Luftwaffe]] nearly completed [[Aukra Airport, Gossen]] during the [[Second World War]] and in the 1950s the authorities decided to complete the facility and establish it as a central airport for [[Møre og Romsdal]].<ref>Hjelle: 13</ref> Local initiative in Ålesund convinced [[Parliament of Norway|Parliament]] in 1957 that [[Ålesund Airport, Vigra]] should be built instead.<ref>Hjelle: 14</ref> It opened in 1958.<ref>Hjelle: 18</ref> This airport was by Parliament considered as sufficient for all of the county and the seaplane routes were terminated the same season.<ref name=oh8 /> Poor roads and three ferries meant that driving time to Vigra was about five hours at the time.<ref name=oh9>Olsen-Hagen: 9</ref>

Proposals for an airport in Kristiansund were first launched in 1954, with Kvernberget as the preferred site and at an estimated investment cost of 6.5 million [[Norwegian krone]]. It had however been placed on hold due to the Gossen plans. In both Molde and Kristiansund there was displeasure with the choice of Vigra and both municipalities established airport commissions in 1959 to look into building their own airports. Kristiansund's commission was led by William Dall and the same year the municipality bought a {{convert|54|ha}} lot at Kvernberget to reserve it for a future aerodrome.<ref name=oh9 />

[[File:Kvernberget mars 2011.jpg|thumb|left|Kvernberget in 2011]]
In both Kristiansund and Molde the debate centered around whether their respective districts of Nordmøre and [[Romsdal]] should have each their respective airport, of a shared aerodrome. A national airport commission was established in 1962, which looked at the primary airport structure, including in Nordmøre and Romsdal. It considered Gossen, Hendamyrene in [[Averøy]], Freistranda in [[Frei]] and Osmarka. Several of these would be suitable as a site for a shared airport. Within the two towns there was little interest in cooperation and both wanted their own airport, situated close to the town center. The commission recommended in its report of 16 December 1964 that Kristiansund and Molde both be amongst the nine recommended airports. Kristiansund was given top priority, while Molde was lowest prioritized.<ref name=oh9 />

Kvernberget was approved by Parliament on 2 April 1968. It received specifications of the time, which were based on a {{convert|1920|m|sp=us|adj=on}} runway. Construction took 19 months and cost NOK&nbsp;31 million. The official opening took place on 31 June 1970 and operations commenced the following day.<ref name=oh9 />

[[File:KSU CHC Eurocopter Super Puma.png|thumb|left|[[CHC Helikopter Service]] [[Eurocopter Super Puma]]]]
Both Braathens SAFE and Scandinavian Airlines System (SAS) applied to operate the [[Oslo Airport, Fornebu]] route out of Kristiansund. Braathens also applied to extend its coastal route to Kvernberget. The ministry wanted Braathens SAFE to fly the route with a concession granted to SAS, but Braathens SAFE rejected this. Instead, they were granted both the routes on temporary basis. The new airport received three daily flights to Oslo, of which two went via Ålesund, and four services on the West Coast route to [[Bergen Airport, Flesland]] and [[Trondheim Airport, Værnes]].<ref>Tjomsland and Wilsberg: 183</ref> The routes were flown using [[Fokker F27 Friendship]] turboprops and [[Fokker F28 Fellowship]] turbojets. Widerøe included Kristiansund as part of its coastal network which extended to [[Ørland Airport]] and southwards to the regional airports in [[Sogn og Fjordane]].<ref name=oh9 />

Molde decided that it would not wait for state funding and initiated construction of [[Molde Airport, Årø]] with municipal funding. The new airport opened on 5 April 1972. It cut away half the catchment area of Kvernberget, significantly reducing both ridership and services from Kristiansund Airport.<ref name=oh10>Olsen-Hagen: 10</ref> Kvernberget handled 88,246 passengers in 1971, but after Molde opened so high a figure was not reached again until 1985.<ref name=oh9 />

[[File:SAS Norge KSU.png|thumb|[[Scandinavian Airlines]] [[Boeing 737 Next Generation|Boeing 737-700]] landing]]
With the opening of Haltenbanken for petroleum drilling in the early 1980s, the authorities decided that Kristiansund would be the center of operations for the area. This caused the need for offshore helicopter flights to the rigs. [[Helikopter Service]] built a base at Kvernberget and commenced flights on 1 July 1982. Braathens opened a {{convert|200|m2|sp=us|adj=on}} operations building in 1985. Next the terminal was expanded; costing NOK&nbsp;27 million, it opened on 12 April 1989.<ref name=oh10 /> Although infrequently flown from 1972, regular charter flights commenced in 1982. The first [[inclusive tour]] flight took off in 1988.<ref name=oh11>Olsen-Hagen: 11</ref>

At the turn of the decade the debate about the need for two airports to serve Nordmøre and Romsdal resumed, spurred by the [[Kristiansund Mainland Connection|Kristiansund Fixed Link]] which removed the only ferry between the two towns, but the debate soon died down again.<ref name=oh10 /> [[Busy Bee]] took over some of Braathens' route. When it went out of business in 1992, the regional coastal routes were taken over by [[Norwegian Air Shuttle]].<ref>{{cite news |url= |title=Norwegian Air Shuttle på ruinene etter Busy Bee |author= |work= |agency=[[Norwegian News Agency]] |page= |date=27 January 1993 |accessdate= |language=Norwegian}}</ref>  A new {{convert|1400|m2|sp=us|adj=on}} helicopter terminal opened the same year. Helicopter services continued to make up an increasingly important part of the patronage – 26 percent in 1997 and 47 percent in 2007.<ref name=oh11 /> Widerøe retired its Twin Otters and pulled out of Kristiansund in 1993.<ref>Hjelle: 103</ref>

[[File:Kvernberget overview.jpg|thumb|left|[[Widerøe]] [[Bombardier Dash 8]] (closest) and [[Scandinavian Airlines]] [[Boeing 737]] at the airport]]
[[SAS Commuter]] took over Norwegian Air Shuttle's services from 1 April 2003.<ref>{{cite news |title=SAS skal fly langs Vestlands-kysten |work=[[Bergens Tidende]] |date=29 March 2003 |page=40 |language=Norwegian}}</ref> Braathens merged to create [[SAS Braathens]] in May 2004, with the new airline taking over the Oslo route. It passed on the Scandinavian Airlines in 2007.<ref>{{cite news |title=Lindegaard: - Vi plukker det beste fra SAS og Braathens |last=Lillesund |first=Geir |agency=[[Norwegian News Agency]] |date=10 March 2004 |page=24 |language=Norwegian}}</ref> [[City Star Airlines]] commenced the first international route, to [[Aberdeen Airport]], on 10 October 2005 using a [[Dornier Do 328]].<ref>{{cite news |title=Uventet konkurranse for SAS Braathens |last=Helgesen |first=Jan-Petter |work=[[Stavanger Aftenblad]] |date=9 September 2005 |page=10}}</ref> The route lasted until February 2008.<ref>{{cite news |title=Full stopp for City Star |work=[[Sunnmørsposten]] |date=1 February 2008 |page=7}}</ref>

A [[duty-free shop]] was opened in 2007.<ref name=oh11 /> Avinor carried out a major overhaul of the airport starting in 2010. NOK&nbsp;240 million was spent extending the runway from {{convert|1760|to|2380|m|sp=us}}. This allowed heavier aircraft to take off and will increase the desirability to use the aerodrome for Mediterranean inclusive tour charters. This opened on 19 October 2012, shortly after a new and larger car parking lot had been taken into use. The next stage is the construction is for a larger terminal. Other minor projects are a new general aviation hangar and a new helicopter hangar. Total investments are estimated at NOK&nbsp;500 million and aimed for completion in 2017.<ref>{{cite news |url=http://www.bygg.no/article/94762 |title=Kvernberget flyplass – rullebane |publisher=Byggeindustrien |date=2 October 2012 |language=Norwegian |accessdate=15 January 2015 |archiveurl=https://web.archive.org/web/20150115222532/http://www.bygg.no/article/94762 |archivedate=15 January 2015}}</ref>

[[File:KSU markedsgrunnlag.png|thumb|The airport's [[catchment area]] (red) and areas which are in equilibrium between Kristiansund Airport and [[Molde Airport, Årø]] (blue)]]
SAS Commuter's routes to Bergen and Trondheim were taken over by Widerøe in 2010.<ref>{{cite news |url=http://www.adressa.no/forbruker/reiseliv/article1445210.ece |title=Widerøe vil overta for SAS på Vestlandskysten |agency=[[Norwegian News Agency]] |work=[[Adresseavisen]] |date=15 February 2010 |accessdate=15 February 2010 |language=Norwegian}}</ref> [[BMI Regional]] introduced a daily round trip with an [[Embraer 135]] from Kristiansund to Aberdeen on 28 August 2013, targeting the oil industry.<ref>{{cite news |title=Direktefly til Kvernberget–Aberdeen fra 28. august |work=[[Tidens Krav]] |date=29 June 2013 |page=9 |language=Norwegian}}</ref> The airline commenced a domestic route on 23 January 2014, to [[Stavanger Airport, Sola]]. This route was terminate din May the same year.<ref>{{cite news |title=Legger ned Stavanger-rute |work=[[Tidens Krav]] |date=8 May 2014 |page=7 |language=Norwegian}}</ref>

== Facilities ==
Kristiansund Airport is located on the ridge of [[Kvernberget]], a hill on the island of [[Nordlandet]] in Kristiansund, Norway. Owned and operated by the state-owned Avinor, it is the sole airport with scheduled services in Nordmøre. It features an asphalt runway with physical dimensions {{convert|2390|by|45|m|sp=us}} aligned 07/25. It has a takeoff run available (TORA) of {{convert|2080|and|2070|m|sp=us}} on runways 07 and 25, respectively, and a landing distance available (LDA) of {{convert|2000|m|sp=us}}. It is equipped with a category I [[instrument landing system]] in both directions. The airport is equipped with category 7 fire and rescue service. There is no parallel [[taxiway]].<ref name=aip>{{cite web |url=https://www.ippc.no/norway_aip/current/aip/ad/enkb/EN_AD_2_ENKB_en.pdf |title=ENKB – Kristiansand/Kvernberget |publisher=[[Avinor]] |work=[[Aeronautical Information Publication]] Norway |format=PDF |accessdate=15 January 2015}}</ref>

==Airlines and destinations==
[[File:Rutekart KSU innenriks revidert.png|thumb|Map showing scheduled services by [[Widerøe]] (green), [[Scandinavian Airlines]] (blue) and offshore (red) from Kristiansund]]
The busiest route is provided by Scandinavian Airlines, which operates four daily round trips to [[Oslo]] using [[Boeing 737]] aircraft. Flights along the West Coast to [[Bergen]], [[Florø]], [[Molde]], [[Stavanger]] and [[Trondheim]] are provided by Widerøe using [[Bombardier Dash 8]] aircraft.<ref name=oh11 /> CHC Helikopter Service operates out of Kristiansund Airport to nine offshore oil platforms in the [[Norwegian Sea]].<ref>Olsen-Hagen: 15</ref> There are also some seasonal charter services.<ref name=oh11 /> Operation of the airport ran at a deficit of NOK&nbsp;23 million in 2012.<ref>{{cite news |url=http://www.nrk.no/mr/7-av-46-flyplasser-med-overskudd-1.10970207 |title=Kun 7 av 46 flyplasser gikk med overskudd i 2012 |publisher=[[Norwegian Broadcasting Corporation]] |last=Riise |first=Ivar Lid |language=Norwegian |date=2 April 2013 |accessdate=13 January 2015}}</ref> Kvernberget served 384,822 passengers, 13,722 aircraft movements and handled 111 tonnes of cargo.<ref name=pax>{{cite web |url=http://www.mynewsdesk.com/material/document/42080/download?resource_type=resource_document |title=Månedsrapport |publisher=[[Avinor]] |format=XLS |accessdate=13 January 2015 |year=2015}}</ref>

{{Airport destination list
| [[Aegean Airlines]] | '''Seasonal charter''': [[Chania International Airport|Chania]]
| [[CHC Helikopter Service]] | '''Charter''': Heidrun, Draugen, Åsgard A, Åsgard B, West Alpha, Scarabeo 5, Deep Sea Bergen, Njord, Stril Poseidon
| [[Croatia Airlines]] | '''Seasonal charter''': [[Split Airport|Split]]
| [[Scandinavian Airlines]] | [[Oslo Airport, Gardermoen|Oslo-Gardermoen]]
| [[Widerøe]] | [[Bergen Airport, Flesland|Bergen]], [[Florø Airport|Florø]], [[Molde Airport, Årø|Molde]], [[Stavanger Airport, Sola|Stavanger]], [[Trondheim Airport, Værnes|Trondheim]]
}}

==Ground transport==
The airport is situated {{convert|7|km|sp=us}} drive from the town center, just off [[Norwegian National Road 70|National Road 70]]. [[Fram (agency)|Fram]] operates two bus services past the airport, each which run twice and hour. Travel time is about twenty-five minutes. There is paid parking for 350 vehicles. Taxis and car rental is available.<ref>{{cite web |url=https://avinor.no/en/airport/kristiansund-airport/to-and-from-the-airport/#!bus-car-and-taxi-970 |title=To and from the airport |publisher=[[Avinor]] |accessdate=15 January 2015}}</ref>

{{clear}}

==References==
{{reflist|30em}}

==Bibliography==
{{commons category|Kristiansund Airport, Kvernberget}}
* {{cite book |last=Arnesen |first=Odd |title=På grønne vinger over Norge |year=1984 |publisher=Widerøe's Flyveselskap |language=Norwegian}}
* {{cite book |last=Hjelle |first=Bjørn Owe |title=Ålesund lufthavn Vigra |location=Valderøya |year=2007 |isbn=978-82-92055-28-1 |language=Norwegian}}
* {{cite book |last=Olsen-Hagen |first=Bernt |last2=Gulstad |first2=Anne Blix |last3=Gynnild |first3=Olav |title=Kristiansund lufthavn, Kvernberget – Dokumentasjon for arkivmessig bevaring |url=http://luftfartsmuseum.no/wp-content/uploads/2013/11/Kristiansund-Lufthavn-dokumentasjon.pdf |format=PDF |publisher=[[Norwegian Aviation Museum]] |year=2010 |language=Norwegian |accessdate=15 January 2015 |archiveurl=https://web.archive.org/web/20150115121701/http://luftfartsmuseum.no/wp-content/uploads/2013/11/Kristiansund-Lufthavn-dokumentasjon.pdf |archivedate=15 January 2015 |deadurl=no}}
* {{cite book |last=Tjomsland |first=Audun |last2=Wilsberg |first2=Kjell |year=1995 |title=Braathens SAFE 50 år: Mot alle odds |location=Oslo |isbn=82-990400-1-9}}

==Further reading==
* {{cite book |last=Maridal |first=Jomar |title=Kvernberget 40 år – 1970–2010 |year=2010 |publisher=Blurb |language=Norwegian}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}

[[Category:Buildings and structures in Kristiansund]]
[[Category:Airports in Møre og Romsdal]]
[[Category:Avinor airports]]
[[Category:Heliports in Norway]]
[[Category:1970 establishments in Norway]]
[[Category:Airports established in 1970]]