{{Other uses}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book| <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name = '''Oliver Twist'''
| title_orig = Oliver Twist; or, The Parish Boy's Progress
| image = Olivertwist front.jpg
| caption       = Frontispiece and title page, first edition 1838<br>Illustration and design by [[George Cruikshank]]
| author = [[Charles Dickens]]
| illustrator = [[George Cruikshank]]
| country = England
| language = English
| series = 
| genre = [[Novel]]
| publisher = Serial: ''[[Bentley's Miscellany]]''<br> Book: [[Richard Bentley (publisher)|Richard Bentley]]
| published = Serialised 1837–39; book form 1838 
| isbn =
| oclc= 185812519
| preceded_by = [[The Pickwick Papers]] (1836–37)
| followed_by = [[Nicholas Nickleby]] (1838–39)
| wikisource = Oliver Twist
}}
'''''Oliver Twist''''', or '''''The Parish Boy's Progress''''', is the second novel by English author [[Charles Dickens]] and was first published as a [[Serial (literature)|serial]] 1837–39.  The story is of the orphan [[Oliver Twist (character)|Oliver Twist]], who starts his life in a [[workhouse]] and is then sold into apprenticeship with an [[undertaker]]. He escapes from there and travels to London, where he meets the [[Artful Dodger]], a member of a gang of juvenile pickpockets led by the elderly criminal, [[Fagin]].

''Oliver Twist'' is notable for its unromantic portrayal by Dickens of criminals and their sordid lives, as well as for exposing the cruel treatment of the many orphans in London in the mid-19th century.<ref>[[Frank Donovan (writer)|Donovan, Frank]]. ''The Children of Charles Dickens.'' London: Leslie Frewin, 1968, pp. 61–62.</ref> The alternate title, ''The Parish Boy's Progress'', alludes to [[John Bunyan|Bunyan]]'s ''[[The Pilgrim's Progress]]'', as well as the 18th-century caricature series by [[William Hogarth]], ''[[A Rake's Progress]]'' and ''[[A Harlot's Progress]]''.<ref>[[Richard J. Dunn|Dunn, Richard J.]]. ''Oliver Twist: Heart and Soul'' (Twayne's Masterwork Series No. 118). New York: Macmillan, p. 37.</ref>

In this early example of the [[social novel]], Dickens satirizes the hypocrisies of his time, including child labour, the recruitment of children as criminals, and the presence of street children. The novel may have been inspired by the story of [[Robert Blincoe]], an orphan whose account of working as a child labourer in a cotton mill was widely read in the 1830s. It is likely that Dickens's own youthful experiences contributed as well.<ref>{{cite book|last=Dickens|first=Charles|title=Oliver Twist|publisher=Kiddy Monster Publication|page=Summary}}</ref>

''Oliver Twist'' has been the subject of numerous adaptations for various media, including a highly successful musical play, ''[[Oliver!]]'', and the multiple [[Academy Award]]-winning [[Oliver! (film)|1968 motion picture]]. Disney also put its spin on the novel with the movie called ''[[Oliver & Company|Oliver & Company]]'' in 1988.<ref>{{Citation|title=Oliver and Company|url=http://movies.disney.com/oliver-and-company|language=en|accessdate=2017-02-13}}</ref>

==Publications==
The novel was originally published in monthly installments in the Magazine '' Bentley's Miscellany ''from February 1837 to April 1839. It was originally intended to form part of Dickens's serial, ''[[The Mudfog Papers]]''.<ref>Dickens, Charles. ''Oliver Twist, or, The Parish Boy's Progress'' Edited by Philip Horne. Penguin Classics, 2003, p. 486. ISBN 0-14-143974-2.</ref><ref>{{cite book |last=Ackroyd |first=Peter |title=Dickens |location=London |publisher=[[Sinclair-Stevenson]] |year=1990 |page=216 |isbn=1-85619-000-5 }}</ref><ref name=bentleys>''Bentley's Miscellany'', 1837.</ref> [[George Cruikshank]] provided one steel etching per month to illustrate each installment.<ref>Schlicke, Paul (Editor). ''Oxford Reader's Companion to Dickens''. Oxford: [[Oxford University Press]], 1999, p. 141.</ref> The novel first appeared in book form six months before the initial serialization was completed, in three volumes published by Richard Bentley, the owner of ''Bentley's Miscellany'', under the author's pseudonym, "Boz". It included 24 steel-engraved plates by Cruikshank.

The first edition was titled: ''Oliver Twist, or, The Parish Boy's Progress''.
[[File:Twist serialcover.jpg|thumb|upright|Cover, first edition of serial, entitled "The Adventures of Oliver Twist" January 1846<br />Design by [[George Cruikshank]]]]

Serial publication dates:<ref>[http://www.pbs.org/wgbh/masterpiece/olivertwist/ei_staytuned_hist.html Masterpiece Theater on PBS.org]</ref>
* I – February 1837 (chapters 1–2)
* II – March 1837 (chapters 3–4)
* III – April 1837 (chapters 5–6)
* IV – May 1837 (chapters 7–8)
* V – July 1837 (chapters 9-11)
* VI – August 1837 (chapters 12–13)
* VII – September 1837 (chapters 14–15)
* VIII – November 1837 (chapters 16–17)
* IX – December 1837 (chapters 18–19)
* X – January 1838 (chapters 20–22)
* XI – February 1838 (chapters 23–25)
* XII – March 1838 (chapters 26–27)
* XIII – April 1838 (chapters 28–30)
* XIV – May 1838 (chapters 31–32)
* XV – June 1838 (chapters 33–34)
* XVI – July 1838 (chapters 35–37)
* XVII – August 1838 (chapters 38-part of 39)
* XVIII – October 1838 (conclusion of chapter 39–41)
* XIX – November 1838 (chapters 42–43)
* XX – December 1838 (chapters 44–46)
* XXI – January 1839 (chapters 47–49)
* XXII – February 1839 (chapter 50)
* XXIII – March 1839 (chapter 51)
* XXIV – April 1839 (chapters 52–53)

==Plot summary==

===Workhouse years===
[[Image:Mr Bumble 1889 Dickens Oliver Twist character by Kyd (Joseph Clayton Clarke).jpg|thumb| right|upright|Mr. Bumble by [[Joseph Clayton Clarke|Kyd (Joseph Clayton Clarke)]]]] 
[[Oliver Twist (character)|Oliver Twist]] was born into a life of poverty and misfortune in a [[workhouse]] in an unnamed town (although when originally published in ''[[Bentley's Miscellany]]'' in 1837, the town was called [[Mudfog]] and said to be within 70 miles north of [[London]] – in reality, this is the location of the town of [[Northampton]]). Orphaned by his mother's death in childbirth and his father's unexplained absence, Oliver is meagerly provided for under the terms of the [[Poor Law Amendment Act 1834|Poor Law]] and spends the first nine years of his life living at a [[baby farming|baby farm]] in the 'care' of a woman named Mrs. Mann. Oliver is brought up with little food and few comforts. Around the time of Oliver's ninth birthday, Mr. Bumble, the parish [[beadle]], removes Oliver from the baby farm and puts him to work picking and weaving [[oakum]] at the main workhouse. Oliver, who toils with very little food, remains in the workhouse for six months. One day, the desperately hungry boys decide to draw lots; the loser must ask for another portion of [[gruel]]. The task falls to Oliver, who at the next meal tremblingly comes up forward, bowl in hand, and begs Mr. Bumble for gruel with his famous request: "Please, sir, I want some more".

A great uproar ensues. The board of well-fed gentlemen who administer the workhouse hypocritically offer £5 to any person wishing to take on the boy as an apprentice. Mr. Gamfield, a brutal [[chimney sweep]], almost claims Oliver. However, when he begs despairingly not to be sent away with "that dreadful man", a kindly old magistrate refuses to sign the indentures. Later, [[Mr. Sowerberry]], an undertaker employed by the parish, takes Oliver into his service. He treats Oliver better and, because of the boy's sorrowful countenance, uses him as a mourner at children's funerals. However, Mr. Sowerberry is in an unhappy marriage, and his wife takes an immediate dislike to Oliver – primarily because her husband seems to like him – and loses few opportunities to underfeed and mistreat him. He also suffers torment at the hands of Noah Claypole, an oafish but bullying fellow apprentice and "charity boy" who is jealous of Oliver's promotion to [[Mute (death customs)#Mutes and professional mourners|mute]], and Charlotte, the Sowerberrys' maidservant, who is in love with Noah.

In trying to bait Oliver, Noah insults Oliver's biological mother, calling her "a regular right-down bad 'un". Oliver flies into a rage, attacking and beating the much bigger boy. Mrs. Sowerberry takes Noah's side, helps him to subdue, punch, and beat Oliver, and later compels her husband and Mr. Bumble, who has been sent for in the aftermath of the fight, to beat Oliver once again. Once Oliver is sent to his room for the night, he breaks down and weeps, upset at the events which he had faced. The next day, Oliver escapes from the Sowerberrys' house and decides to run away to London instead.

===London, the Artful Dodger, and Fagin===
[[File:Dodger introduces Oliver to Fagin by Cruikshank (detail).jpg|thumb|left|[[George Cruikshank]] original engraving of the [[Artful Dodger]] (centre), here introducing [[Oliver Twist (character)|Oliver]] (right) to [[Fagin]] (left)]]
During his journey to London, Oliver encounters Jack Dawkins, a [[pickpocket]] more commonly known by the nickname the "[[Artful Dodger]]", and his sidekick, a boy of a humorous nature, named [[Charley Bates]], but Oliver's innocent nature prevents him from recognizing any hint that the boys may be dishonest. Dodger provides Oliver with a free meal and tells him of a gentleman in London who will "give him lodgings for nothing, and never ask for change". Grateful for the unexpected assistance, Oliver follows Dodger to the "old gentleman's" residence. In this way, Oliver unwittingly falls in with an infamous Jewish criminal known as [[Fagin]], the so-called gentleman of whom the Artful Dodger spoke. Ensnared, Oliver lives with Fagin and his gang of juvenile pickpockets in their lair at [[Saffron Hill]] for some time, unaware of their criminal occupations. He believes they make wallets and handkerchiefs.

Later, Oliver naively goes out to "make handkerchiefs" (because there is no income) with the Artful Dodger and Charley Bates. Oliver realizes too late that their real mission is to pick pockets. Dodger and Charley steal the handkerchief of an old gentleman named Mr. Brownlow and promptly flee. When he finds his handkerchief missing, Mr. Brownlow turns round, sees Oliver running away in fright, and pursues him. Others join the chase and Oliver is caught and taken before the magistrate. Curiously, Mr. Brownlow has second thoughts about the boy&nbsp;– he seems reluctant to believe he is a pickpocket. To the judge's evident disappointment, a bookstall holder who saw Dodger commit the crime clears Oliver, who, by now actually ill, faints in the courtroom. Mr. Brownlow takes Oliver home and, along with his housekeeper Mrs. Bedwin, cares for him.

[[Image:Bill-sikes.jpg|right|thumb|upright|[[Bill Sikes]] by [[Fred Barnard]]]]
Oliver stays with Mr. Brownlow, recovers rapidly, and blossoms from the unaccustomed kindness. His bliss, however, is interrupted when Fagin, fearing Oliver might "peach" on his criminal gang, decides that Oliver must be brought back to his hideout. When Mr. Brownlow sends Oliver out to pay for some books, one of the gang, a young girl named [[Nancy (Oliver Twist)|Nancy]], whom Oliver had previously met at Fagin's, accosts him with help from her abusive lover, a brutal robber named [[Bill Sikes]], and Oliver is quickly bundled back to Fagin's lair. The thieves take the five-pound note Mr.Brownlow had entrusted to him, and strip him of his fine new clothes. Oliver, dismayed, flees and attempts to call for police assistance, but is ruthlessly dragged back by the Artful Dodger, Charley, and Fagin. Nancy, however, is sympathetic towards Oliver and saves him from beatings by Fagin and Sikes.

In a renewed attempt to draw Oliver into a life of crime, Fagin forces him to participate in a burglary. Nancy reluctantly assists in recruiting him, all the while assuring the boy that she will help him if she can. Sikes, after threatening to kill him if he does not cooperate, sends Oliver through a small window and orders him to unlock the front door. The robbery goes wrong, however, and Oliver is shot and wounded in his left arm at the targeted house. After being abandoned by Sikes, the wounded Oliver makes it back to the house and ends up under the care of the people he was supposed to rob: Miss Rose and her guardian Mrs. Maylie.

===Mystery of a man called 'Monks'===
[[File:Fagin by Kyd 1889.jpg|thumb|170px|right|Fagin by [[Joseph Clayton Clarke|'Kyd']] (1889)]]
A mysterious man named Monks found Fagin and is plotting with him to destroy Oliver's reputation. Monks denounces Fagin's failure to turn Oliver into a criminal, and the two of them agree on a plan to make sure he does not find out about his past. Monks is apparently related to Oliver in some way, although this is not mentioned until later. Back in Oliver's hometown, Mr. Bumble has married Mrs. Corney, the wealthy matron of the workhouse where the story first began, only to find himself in an unhappy marriage, constantly arguing with his domineering wife. After one such argument, Mr. Bumble walks over to a pub, where he meets Monks, who questions him about Oliver. Bumble informs Monks that he knows someone who can give Monks more information for a price, and later Monks meets secretly with the Bumbles. After Mrs. Bumble has told Monks all she knows, the three arrange to take a locket and ring which had once belonged to Oliver's mother and toss them into a nearby river. Monks relates this to Fagin as part of the plot to destroy Oliver, unaware that Nancy has eavesdropped on their conversation and gone ahead to inform Oliver's benefactors.

Now ashamed of her role in Oliver's kidnapping and fearful for the boy's safety, Nancy goes to Rose Maylie and Mr. Brownlow to warn them. She knows that Monks and Fagin are plotting to get their hands on the boy again and holds some secret meetings on the subject with Oliver's benefactors. One night, Nancy tries to leave for one of the meetings, but Sikes refuses permission when she declines to state exactly where she is going. Fagin realizes that Nancy is up to something and resolves to find out what her secret is. Meanwhile, Noah has fallen out with the undertaker Mr. Sowerberry, stolen money from him, and fled to London. Charlotte has accompanied him&nbsp;— they are now in a relationship. Using the name "Morris Bolter", he joins Fagin's gang for protection and becomes a practicer of "the kinchin lay" (robbing of children), and Charlotte (it is implied) becomes a prostitute. During Noah's stay with Fagin, the Artful Dodger is caught with a stolen silver snuff box, convicted (in a very humorous courtroom scene), and transported to Australia. Later, Noah is sent by Fagin to "dodge" (spy on) Nancy, and discovers her secret: she has been meeting secretly with Rose and Mr. Brownlow to discuss how to save Oliver from Fagin and Monks.

Fagin angrily passes the information on to Sikes, twisting the story just enough to make it sound as if Nancy had informed on him. Believing Nancy to be a traitor, Sikes beats her to death in a fit of rage and flees to the countryside to escape from the police. There, Sikes is haunted by visions of Nancy's ghost and increasingly alarmed by news of her murder spreading across the countryside. He returns to London to find a hiding place, only to die by accidentally hanging himself while attempting to flee across a rooftop from an angry mob.

===Resolution===
[[File:Cruikshank - Fagin in the condemned Cell (Oliver Twist).png|right|thumb|upright|[[Fagin]] in his cell, by British caricaturist [[George Cruikshank]]]]
Monks is forced by Mr. Brownlow to divulge his secrets: his real name is Edward Leeford, and he is Oliver's paternal half-brother and, although he is legitimate, he was born of a loveless marriage. Oliver's mother, Agnes, became their father's true love after Monks witnessed his parents' divorce. Mr. Brownlow has a picture of Agnes and began making inquiries when he noticed a marked resemblance between her and Oliver. Monks has spent many years searching for his father's child&nbsp;– not to befriend him, but to destroy him (see [[Henry Fielding]]'s ''[[The History of Tom Jones, a Foundling|Tom Jones]]'' for similar circumstances). Brownlow asks Oliver to give half his inheritance (which proves to be meagre) to Monks because he wants to give him a second chance; and Oliver, being prone to giving second chances, is more than happy to comply. Monks later moves to America, where he squanders his money, reverts to crime, and ultimately dies in prison. Fagin is arrested and condemned to the gallows. On the eve of his hanging, in an emotional scene, Oliver, accompanied by Mr. Brownlow, goes to visit the old reprobate in [[Newgate Gaol]], where Fagin's terror at being hanged has caused him to lose himself in daydreams and develop a fever.

On a happier note, Rose Maylie turns out to be the long-lost sister of Agnes, and therefore Oliver's aunt. She marries her long-time sweetheart Harry, and Oliver lives happily with his savior, Mr. Brownlow. Noah becomes a paid, semi-professional police informer. The Bumbles lose their jobs and are reduced to great poverty, eventually ending up in the same workhouse where they originally lorded it over Oliver and the other orphan boys. Charley Bates, horrified by Sikes's murder of Nancy, becomes an honest citizen, moves to the country, and works his way up to prosperity.

==Characters==
{{col-begin}}
{{col-2}}
*[[Oliver Twist (character)|Oliver Twist]] – an orphan
*[[Fagin]] – criminal boss of a gang of young boys
*[[Nancy (Oliver Twist)|Nancy]] – a prostitute
*[[Rose Maylie]] – Oliver's maternal aunt
* Mrs. Lindsay Maylie – Harry Maylie's mother. Rose Maylie's adoptive aunt
*[[Mr. Brownlow]] – a kindly gentleman who takes Oliver in
*[[Monks (Oliver Twist)|Monks]] – a sickly criminal, a cohort of Fagin's, and long-lost half-brother of Oliver
*Mr. Bumble – a beadle in the parish workhouse where Oliver was born
*[[Bill Sikes]] – a professional [[burglar]]
*Agnes Fleming – Oliver's mother
*Mr. Leeford – Oliver's and Monks's father
*Mr. Losberne – Mrs. Maylie's family doctor
*Harry Maylie – Mrs. Maylie's son
*The [[Artful Dodger]] – Fagin's most adept ''pickpocket''
*[[Charley Bates]] – a pickpocket
*Old Sally – a nurse who attended Oliver's birth
*Mrs. Corney – matron at Oliver's workhouse
{{col-break}}
*Noah Claypole – a cowardly bully, Sowerberry's apprentice
*Charlotte – the Sowerberrys' maid
*Toby Crackit – an associate of Fagin and Sikes
*Mrs. Bedwin – Mr. Brownlow's housekeeper
*Bull's Eye – Bill Sikes' vicious dog
*Monks' mother – an heiress
*[[Mr. Sowerberry]] – an [[undertaker]]
*Mrs. Sowerberry – Mr. Sowerberry's wife
*Mr. Grimwig – a friend of Mr. Brownlow's
*Mr. Giles – Mrs. Maylie's [[butler]]
*Mr. Brittles – Mrs. Maylie's [[handyman]]
*Mrs. Mann – superintendent of Oliver's workhouse
*Mr. Gamfield – a [[chimney sweep]]
*Bet – a [[prostitute]]
*Mr. Fang – a magistrate
*Barney – a Jewish criminal cohort of Fagin
*Duff and Blathers – two incompetent policemen
*Tom Chitling – one of Fagin's gang members
{{col-end}}

==Major themes and symbols==
[[Image:Bill Sikes 1889 Dickens Oliver Twist character by Kyd (Joseph Clayton Clarke).jpg|thumb|left|upright|Bill Sikes by [[Joseph Clayton Clarke|Kyd (Joseph Clayton Clarke)]]]]
[[Image:Clarke-dodger.jpg|thumb|right|upright|The Artful Dodger by [[Joseph Clayton Clarke|Kyd (Joseph Clayton Clarke)]]]]

{{refimprove section|date=April 2016}}
In ''Oliver Twist'', Dickens mixes grim realism with merciless satire to describe the effects of industrialism on 19th-century England and to criticise the harsh new [[Poor Law Amendment Act 1834|Poor Laws]]. Oliver, an innocent child, is trapped in a world where his only options seem to be the workhouse, a life of crime symbolized by Fagin's gang, a prison, or an early grave. From this unpromising industrial/institutional setting, however, a fairy tale also emerges. In the midst of corruption and degradation, the essentially passive Oliver remains pure-hearted; he steers away from evil when those around him give in to it, and in proper fairy-tale fashion, he eventually receives his reward – leaving for a peaceful life in the country, surrounded by kind friends. On the way to this happy ending, Dickens explores the kind of life an outcast, orphan boy could expect to lead in 1830s London.<ref>[[J. Hillis Miller|Miller, J. Hillis]]. "The Dark World of ''Oliver Twist''" in ''Charles Dickens'' (Harold Bloom, editor), New York: Chelsea House Publishers, 1987, p. 35</ref>

===Poverty and social class===
Poverty is a prominent concern in ''Oliver Twist''. Throughout the novel, Dickens enlarged on this theme, describing slums so decrepit that whole rows of houses are on the point of ruin. In an early chapter, Oliver attends a pauper's funeral with Mr. Sowerberry and sees a whole family crowded together in one miserable room.

This ubiquitous misery makes Oliver's few encounters with charity and love more poignant. Oliver owes his life several times over to kindness both large and small.<ref>[[Dennis Walder|Walder, Dennis]], "''Oliver Twist'' and Charity" in ''Oliver Twist: a Norton Critical Edition'' ([[Fred Kaplan (biographer)|Fred Kaplan]], Editor). New York: [[W.W. Norton]], 1993, pp. 515–525</ref> The apparent plague of poverty that Dickens describes also conveyed to his middle-class readers how much of the London population was stricken with poverty and disease. Nonetheless, in ''Oliver Twist,'' he delivers a somewhat mixed message about social caste and social injustice. Oliver's illegitimate workhouse origins place him at the nadir of society; as an orphan without friends, he is routinely despised. His "sturdy spirit" keeps him alive despite the torment he must endure. Most of his associates, however, deserve their place among society's dregs and seem very much at home in the depths. Noah Claypole, a charity boy like Oliver, is idle, stupid, and cowardly; Sikes is a thug; Fagin lives by corrupting children, and the Artful Dodger seems born for a life of crime. Many of the middle-class people Oliver encounters—Mrs. Sowerberry, Mr. Bumble, and the savagely hypocritical "gentlemen" of the workhouse board, for example—are, if anything, worse.

On the other hand, Oliver—who has an air of refinement remarkable for a workhouse boy—proves to be of gentle birth. Although he has been abused and neglected all his life, he recoils, aghast, at the idea of victimising anyone else. This apparently hereditary gentlemanliness makes ''Oliver Twist'' something of a [[changeling]] tale, not just an indictment of social injustice. Oliver, born for better things, struggles to survive in the savage world of the underclass before finally being rescued by his family and returned to his proper place—a commodious country house.

One [[Oliver Twist (2005 film)|early 21st century film adaptation of the novel]] dispenses with the paradox of Oliver's genteel origins by eliminating his origin story completely, making him just another anonymous orphan like the rest of Fagin's gang.
[[Image:Oliver Twist - Cruikshank - The Burgulary.jpg|right|thumb|upright|[[Oliver Twist (character)|Oliver]] is wounded in a burglary, by [[George Cruikshank]].]]

===Symbolism===
Dickens makes considerable use of symbolism. The many symbols Oliver faces are primarily good versus evil, with evil continually trying to corrupt and exploit good, but good winning out in the end. The "merry old gentleman" Fagin, for example, has satanic characteristics: he is a veteran corrupter of young boys who presides over his own corner of the criminal world; he makes his first appearance standing over a fire holding a toasting-fork, and he refuses to pray on the night before his execution.<ref>Miller, ibid, p. 48</ref> The London slums, too, have a suffocating, infernal aspect; the dark deeds and dark passions are concretely characterised by dim rooms and pitch-black nights, while the governing mood of terror and brutality may be identified with uncommonly cold weather. In contrast, the countryside where the Maylies take Oliver is a bucolic heaven.

The novel is also shot through with a related [[Motif (narrative)|motif]], social class, which calls attention to the stark injustice of Oliver's world. When the half-starved child dares to ask for more, the men who punish him are fat. A remarkable number of the novel's characters are overweight.

Toward the end of the novel, the gaze of knowing eyes becomes a potent symbol. For years, Fagin avoids daylight, crowds, and open spaces, concealing himself most of the time in a dark lair. When his luck runs out at last, he squirms in the "living light" of too many eyes as he stands in the dock, awaiting sentence. Similarly, after Sikes kills Nancy, he flees into the countryside but is unable to escape the memory of her dead eyes. In addition, Charley Bates turns his back on crime when he sees the murderous cruelty of the man who has been held up to him as a model.

===Characters===
[[Image:Cruikshank - The Last Chance (Oliver Twist).png|right|thumb|upright|The Last Chance, by [[George Cruikshank]].]]
In the tradition of [[Restoration Comedy]] and [[Henry Fielding]], Dickens fits his characters with appropriate names. Oliver himself, though "badged and ticketed" as a lowly orphan and named according to an alphabetical system, is, in fact, "all of a twist."<ref>Ashley, Leonard. ''What's in a name?: Everything you wanted to know.'' Genealogical Publishing, 1989, p. 200.</ref> However, Oliver and his name may have been based on a young workhouse boy named Peter Tolliver whom Dickens knew while growing up.<ref>Richardson, Ruth. "Dickens and the Workhouse: Oliver Twist and the London Poor." Oxford University Press, USA, 2012, p. 56.</ref> Mr. Grimwig is so called because his seemingly "grim", pessimistic outlook is actually a protective cover for his kind, sentimental soul. Other character names mark their bearers as semi-monstrous [[caricatures]]. Mrs. Mann, who has charge of the infant Oliver, is not the most motherly of women; Mr. Bumble, despite his impressive sense of his own dignity, continually mangles the king's English he tries to use; and the Sowerberries are, of course, "sour berries", a reference to Mrs. Sowerberry's perpetual scowl, to Mr. Sowerberry's profession as an undertaker, and to the poor provender Oliver receives from them. Rose Maylie's name echoes her association with flowers and springtime, youth and beauty while Toby Crackit's is a reference to his chosen profession of housebreaking.

Bill Sikes's dog, Bull's-eye, has "faults of temper in common with his owner" and is an emblem of his owner's character. The dog's viciousness represents Sikes's animal-like brutality while Sikes's self-destructiveness is evident in the dog's many scars. The dog, with its willingness to harm anyone on Sikes's whim, shows the mindless brutality of the master. Sikes himself senses that the dog is a reflection of himself and that is why he tries to drown the dog. He is really trying to run away from who he is.{{Citation needed|date=March 2009}} This is also illustrated when Sikes dies and the dog does immediately also.<ref>[http://www.novelsearch.com/olivertwist NovelGuide]</ref> After Sikes murders Nancy, Bull's-eye also comes to represent Sikes's guilt. The dog leaves bloody footprints on the floor of the room where the murder is committed. Not long after, Sikes becomes desperate to get rid of the dog, convinced that the dog's presence will give him away. Yet, just as Sikes cannot shake off his guilt, he cannot shake off Bull's-eye, who arrives at the house of Sikes's demise before Sikes himself does. Bull's-eye's name also conjures up the image of Nancy's eyes, which haunt Sikes until the bitter end and eventually cause him to hang himself accidentally.

Dickens employs polarised sets of characters to explore various dual themes throughout the novel;{{Citation needed|date=November 2007}} Mr. Brownlow and Fagin, for example, personify "good vs. evil". Dickens also juxtaposes honest, law-abiding characters such as Oliver himself with those who, like the Artful Dodger, seem more comfortable on the wrong side of the law. Crime and punishment is another important pair of themes, as is sin and redemption: Dickens describes criminal acts ranging from picking pockets to murder, and the characters are punished severely in the end. Most obviously, he shows Bill Sikes hounded to death by a mob for his brutal acts and sends Fagin to cower in the condemned cell, sentenced to death by due process. Neither character achieves redemption; Sikes dies trying to run away from his guilt, and on his last night alive, the terrified Fagin refuses to see a rabbi or to pray, instead of asking Oliver to help him escape. Nancy, by contrast, redeems herself at the cost of her own life and dies in a prayerful pose.

Nancy is also one of the few characters in ''Oliver Twist'' to display much ambivalence. Although she is a full-fledged criminal, indoctrinated and trained by Fagin since childhood, she retains enough empathy to repent her role in Oliver's kidnapping, and to take steps to try to atone. As one of Fagin's victims, corrupted but not yet morally dead, she gives eloquent voice to the horrors of the old man's little criminal empire. She wants to save Oliver from a similar fate; at the same time, she recoils from the idea of turning traitor, especially to Bill Sikes, whom she loves. When he was later criticised for giving a "thieving, whoring slut of the streets" such an unaccountable reversal of character, Dickens ascribed her change of heart to "the last fair drop of water at the bottom of a dried-up, weed-choked well".<ref>Donovan, Frank, ''The Children of Charles Dickens'', p. 79.</ref>

==Allegations of antisemitism==
{{See also|Fagin#Antisemitism}}
Dickens has been accused of following [[antisemitic]] stereotypes because of his portrayal of the Jewish character Fagin in ''Oliver Twist''. [[Paul Vallely]] writes that Fagin is widely seen as one of the most grotesque Jews in English literature, and the most vivid of Dickens's 989 characters.<ref name=Vallely>{{cite web|last1=Vallely|first1=Paul|title=Dickens' greatest villain: The faces of Fagin|url=http://www.independent.co.uk/arts-entertainment/films/features/dickens-greatest-villain-the-faces-of-fagin-509906.html|website=independent.co.uk|publisher=The Independent|date=7 October 2005|archiveurl=https://web.archive.org/web/20081205101924/http://www.independent.co.uk/arts-entertainment/films/features/dickens-greatest-villain-the-faces-of-fagin-509906.html|archivedate=5 December 2008|accessdate=13 February 2015|deadurl=yes}}</ref> Nadia Valdman, who writes about the portrayal of Jews in literature, argues that Fagin's representation was drawn from the image of the Jew as inherently evil, that the imagery associated him with the devil, and with beasts.<ref>Valdman, Nadia. ''Antisemitism, A Historical Encyclopedia of Prejudice and Persecution''. ISBN 1-85109-439-3</ref>

The novel refers to Fagin 257 times in the first 38 chapters as "the Jew", while the ethnicity or religion of the other characters is rarely mentioned.<ref name=Vallely/> In 1854, the ''[[Jewish Chronicle]]'' asked why "Jews alone should be excluded from the 'sympathizing heart' of this great author and powerful friend of the oppressed." Dickens (who had extensive knowledge of London street life and child exploitation) explained that he had made Fagin Jewish because "it unfortunately was true, of the time to which the story refers, that that class of criminal almost invariably was a Jew".<ref name="Oliver Twist intro">{{Cite news|url=https://books.google.com/books?id=R63Gk3ntfFkC&lpg=PR19|title=Oliver Twist – introduction|last=Howe|first=Irving}}</ref> Dickens commented that by calling Fagin a Jew he had meant no imputation against the Jewish faith, saying in a letter, "I have no feeling towards the Jews but a friendly one. I always speak well of them, whether in public or private, and bear my testimony (as I ought to do) to their perfect good faith in such transactions as I have ever had with them".<ref name="dickens-johnson">{{Cite book|last=Johnson|first=Edgar|title=Charles Dickens His Tragedy And Triumph|publisher=Simon & Schuster Inc|date=1 January 1952|chapter=4 – Intimations of Mortality|url=http://dickens.ucsc.edu/OMF/johnson.html|accessdate=8 February 2009}}</ref> Eliza Davis, whose husband had purchased Dickens's home in 1860 when he had put it up for sale, wrote to Dickens in protest at his portrayal of Fagin, arguing that he had "encouraged a vile prejudice against the despised Hebrew", and that he had done a great wrong to the Jewish people. While Dickens first reacted defensively upon receiving Davis's letter, he then halted the printing of ''Oliver Twist'', and changed the text for the parts of the book that had not been set, which explains why after the first 38 chapters Fagin is barely called "the Jew" at all in the next 179 references to him.<ref name=Vallely/>

== Film, television and theatrical adaptions ==

* ''[[August Rush]]'' &ndash; is a film, set in the present day, which a few critics have suggested is essentially a musical adaptation of ''Oliver Twist''.<ref>{{cite news|url=http://www.chicagotribune.com/entertainment/movies/chi-071121august-story,1,4894841.story|title=''August Rush'' (''Oliver Twist'' reset in N.Y.)&nbsp;— 2 stars|author=Smith, Sid|publisher=Chicago Tribune|date=2007-11-21|accessdate=2007-12-15|quote=Turn to the master, Charles Dickens, or better yet, update and recycle him. Such must have been the thinking behind ''August Rush,'' a thinly disguised retelling of ''Oliver Twist,'' transplanted to contemporary New York and sweetened by a theme of the healing magic of music.| archiveurl= https://web.archive.org/web/20071212183056/http://www.chicagotribune.com/entertainment/movies/chi-071121august-story,1,4894841.story?| archivedate= 12 December 2007 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite web|url=http://www.startribune.com/entertainment/movies/11915801.html|title= Movie review: Romanticism trumps reason in ''Rush''|publisher=[[Star Tribune]]|author=Covert, Colin|date=2007-11-20|accessdate=2007-12-15|quote=If Charles Dickens were alive today, he might be writing projects like ''August Rush,'' the unabashedly sentimental tale of a plucky orphan lad who falls in with streetwise urchins as he seeks the family he ought to have. Come to think of it, Dickens did write that one, and called it ''Oliver Twist.''| archiveurl= https://web.archive.org/web/20071210084831/http://www.startribune.com/entertainment/movies/11915801.html| archivedate= 10 December 2007 <!--DASHBot-->| deadurl= no}}</ref>
* ''Manik'' - A Bengali movie directed by Bijalibaran Sen named 'Manik' in 1961 was based on this novel. The movie was starred by [[Pahari Sanyal]], [[Chhabi Biswas]], [[Sombhu Mitra]], Tripti Mitra etc.<ref>{{Cite web|url=http://www.gomolo.com/manik-movie/13801|title=Manik|last=1961(Bengali)|first=|date=|website=Gomolo|archive-url=|archive-date=|dead-url=|access-date=February 20, 2017}}</ref><ref>{{Cite book|title=Influence of Bengali Classic Literature in Bollywood films.|last=Souvik Chatterji Master of Law from Warwick University, Coventry,UK|first=footnote [2]|publisher=|year=2007|isbn=|location=|pages=|quote=|via=}}</ref>

==References==
{{reflist|30em}}

==External links==
{{wikisource}}
{{wikiquote}}
{{Commons category}}
{{portal|Charles Dickens|Literature|Novels}}

;Online versions
* [http://www.bl.uk/works/oliver-twist Manuscript material and articles relating to ''Oliver Twist''.] From the British Library’s Discovering Literature website. 
* [https://archive.org/stream/olivertwist01dickrich#page/n7/mode/2up ''Oliver Twist''] at [[Internet Archive]].
* {{gutenberg|no=730|name=Oliver Twist}}
* {{Librivox book | title=Oliver Twist | author=Charles Dickens}}
* [http://etext.library.adelaide.edu.au/d/dickens/charles/d54ot/ ''Oliver Twist'']—easy to read HTML version
;Critical analysis
* [http://web-static.nypl.org/exhibitions/booknotbook/index.html When Is a Book Not a Book? ''Oliver Twist'' in Context, a seminar by Robert Patten] from the [[New York Public Library]]
* [http://www.fidnet.com/~dap1955/dickens/twist.html Background information and plot summary for ''Oliver Twist'', with links to other resources]
* [http://www.bmj.com/cgi/content/full/337/dec17_2/a2722 Article in ''British Medical Journal'' on Oliver Twist's diet]

{{Oliver Twist}}
{{Charles Dickens}}

{{Authority control}}

{{DEFAULTSORT:Oliver Twist}}
[[Category:Oliver Twist| ]]
[[Category:1838 novels]]
[[Category:Art by George Cruikshank]]
[[Category:Novels first published in serial form]]
[[Category:English novels]]
[[Category:19th-century British novels]]
[[Category:Novels set in London]]
[[Category:Works originally published in Bentley's Miscellany]]
[[Category:Victorian novels]]
[[Category:British novels adapted into films]]
[[Category:Novels about orphans]]
[[Category:Novels set in the 19th century]]
[[Category:Novels adapted into comics]]
[[Category:British novels adapted into plays]]
[[Category:Novels by Charles Dickens]]