{{Infobox journal
| title = {{nowrap|Journal of the}} {{nowrap|Royal Statistical Society}}
| cover =
| discipline = [[Statistics]]
| abbreviation = J. R. Stat. Soc.
| publisher = [[Wiley-Blackwell]]
| country = United Kingdom
| history = 1838–present
| frequency =
| impact = 1.573 (Series A)<br>5.721 (Series B)<br>1.418 (Series C)
| impact-year = 2013
| website = http://www.rss.org.uk/main.asp?page=1711
| ISSN = 0964-1998
| OCLC = 18305542
| LCCN = sn99023416
}}

The '''''Journal of the Royal Statistical Society''''' is a [[peer review|peer-reviewed]] [[scientific journal]] of [[statistics]]. It comprises three series and is published by [[Blackwell Publishing]] for the [[Royal Statistical Society]].

== History ==
{{Wikisource|Journal of the Statistical Society of London}}
The Statistical Society of London was founded in 1834, but would not begin producing a journal for four years. From 1834–1837, members of the society would read the results of their studies to the other members, and some details were recorded in the proceedings. The first study reported to the society in 1834 was a simple survey of the occupations of people in [[Manchester]], [[England]]. Conducted by going door-to-door and inquiring, the study revealed that the most common profession was mill-hands, followed closely by weavers.<ref name=Precursors>{{Cite journal | title = Precursors of the Journal of the Royal Statistical Society | author = S. Rosenbaum | journal = The Statistician | volume = 50 | issue = 4 | year = 2001 | pages = 457–466 | jstor=2681228 | doi=10.1111/1467-9884.00290}}</ref>

When founded, the membership of the Statistical Society of London overlapped almost completely with the statistical section of the [[British Association for the Advancement of Science]]. In 1837 a volume of ''Transactions of the Statistical Society of London'' were written, and in May 1838 the society began its journal. The first [[editor-in-chief]] of the journal was Rawson W. Rawson.<ref name=Precursors/> In the early days of the society and the journal, there was dispute over whether or not opinions should be expressed, or merely the numbers. The symbol of the society was a wheatsheaf, representing a bundle of facts, and the motto ''Aliis exterendum'', Latin for "to be threshed out by others." Many early members chafed under this prohibition, and in 1857 the motto was dropped.<ref>{{Cite journal | title = The Growth of the Royal Statistical Society | author = S. Rosenbaum | journal = Journal of the Royal Statistical Society. Series A (General) | volume = 147 | issue = 2 | year = 1984 | pages = 375–388 | doi = 10.2307/2981692 | jstor = 2981692 | publisher = Blackwell Publishing}}</ref>

From 1838–1886, the journal was published as the '''''Journal of the Statistical Society of London''''' ({{ISSN|0959-5341}}). In 1887 it was renamed the ''Journal of the Royal Statistical Society'' ({{ISSN |0952-8385}}) when the society was granted a [[Royal Charter]].

On its centenary in 1934, the society inaugurated a ''Supplement to the Journal of the Royal Statistical Society'' to publish work on industrial and agricultural applications.<ref>J. Aldrich (2010) [http://www.jehps.net/juin2010/Aldrich.pdf Mathematics in the London/Royal Statistical Society 1834-1934], [http://www.jehps.net/juin2010.html Electronic Journ@l for History of Probability and Statistics, 6, (1)].</ref> In 1948 the society reorganised its journals and the main journal became the ''Journal of the Royal Statistical Society. Series A (General)'' ({{ISSN|0035-9238}}) and the supplement became ''Series B (Statistical Methodology)''. In 1988, Series A changed its name to ''Series A (Statistics in Society)''.

In 1952, the society founded ''Applied Statistics of the Journal of the Royal Statistical Society'' which became ''Series C (Applied Statistics)''. After merging with the [[Institute of Statisticians]] in 1993, the society published ''Series D (The Statistician)'' ({{ISSN|0039-0526}}), but this journal was closed in 2003, to be replaced by [[Significance (magazine)|Significance]].

== Discussion papers ==
Traditionally papers were presented at ordinary meetings of the society and those present, whether fellows or not, were invited to comment on the presentation. The paper and subsequent discussion would then be published in the journal. This followed a format used by other scientific societies of the time, such as the [[Royal Society]]. This practice continues although papers are selected for reading and go through peer review before being presented. It is considered a significant recognition to be invited to present a paper at an ordinary meeting of the society. This selection is currently done by the research section of the society for Series B and by an appointed editor for Series A&C. Papers are selected to be of importance and wide interest in terms of application or applicability.

Any person is invited to attend discussion meetings and contribute to the discussion although they are limited to 5 minutes speaking time. Following the formal presentation of the paper, two speakers are invited to comment by prior arrangement. Formally they are there to propose and second the 'vote of thanks' and would have respectively praised and criticised the presentation. Contributions to the discussion are not peer reviewed but are limited to 400 words in the journal.

== Current series ==
As of 2009, three series are published under this general title.

=== ''Journal of the Royal Statistical Society, Series A (Statistics in Society)'' ===
''Statistics in Society'' ({{ISSN|0964-1998}}) is published quarterly. . Its 2015 [[impact factor]] is 1.6.<ref name="Wiley_A">[http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291467-985X] (accessed 4 April 2015)</ref>

'''Past and current editors:'''

{{Div col||25em}}
* 200?-200?: A. Fielding
* 200?-200? S. Day
* 201?-201?: L. Sharples
* 201?-201?: Harvey Goldstein
{{Div col end}}

=== ''Journal of the Royal Statistical Society, Series B (Statistical Methodology)'' ===
''Statistical Methodology'' ({{ISSN|1369-7412}}) is published six times a year. Its 2015 impact factor is 5.7.<ref name="Wiley_B">[http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291467-9868 ''Journal of the Royal Statistical Society: Series B (Statistical Methodology)''] (accessed 1 April 2015)</ref>

Starting in 1934, it was originally called ''Supplement to the Journal of the Royal Statistical Society'' ({{ISSN|1466-6162}}), and in 1948 was changed to ''Journal of the Royal Statistical Society. Series B (Methodological)'' ({{ISSN|0035-9246}}), before being changed to its current name in 1998.

In a 2003 survey of statisticians, Series B was perceived to have been one of the highest quality journals in statistics.<ref>{{cite journal
 |last1=Vasilis |first1=Theoharakis |first2=Mary |last2=Skordia
 |title=How Do Statisticians Perceive Statistics Journals?
 |journal=[[The American Statistician]]
 |volume=57 |issue=2 |pages=115–123
 |year=2003
 |doi=10.1198/0003130031414
}}</ref>

'''Past and current editors:'''

{{Div col||25em}}
* 1951–1959: [[J. O. Irwin]]
* 1960–1964: [[N. T. J. Bailey]]
* 1960–1964: [[D. R. Cox]]
* 1965–1969: [[D. M. G. Wishart]]
* 1965–1969: [[D. J. Bartholomew]]
* 1970–1974: [[D. E. Barton]]
* 1975–1978: [[M. Stone]]
* 1978–1981: [[J. A. Anderson]]
* 1978–1983: [[T. M. F. Smith]]
* 1980–1983: [[P. Holgate]]
* 1982–1985: [[P. M. E. Altham]]
* 1984–1987: [[P. J. Diggle]]
* 1986–1989: [[D. M. Titterington]]
* 1988–1991: [[R. L. Smith]]
* 1990–1993: [[John T. Kent]]
* 1992–1995: [[Anthony C. Atkinson]]
* 1994–1997: [[Alastair Young]]
* 1996–1999: [[Chris Jones (statistician)|Chris Jones]]
* 1998–2001: [[David Firth (statistician)|David Firth]]
* 2000–2003: [[Anthony C. Davison]]
* 2002–2005: [[T. Henderson]]
* 2004–2007: Andy Wood
* 2006–2009: [[Christian Robert]]
* 2008–2011: [[George Casella]]
* 2010–2013: [[Gareth Roberts (statistician)|Gareth Roberts]]
* 2012–2015: [[Ingrid van Keilegom]]
* 2014–2017: [[Piotr Fryzlewicz]]
{{Div col end}}

=== ''Journal of the Royal Statistical Society, Series C (Applied Statistics)'' ===
''Applied Statistics'' ({{ISSN|0035-9254}}) is published five times a year. Its 2015 impact factor is 1.4.<ref name="Wiley">[http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291467-9876 ''Journal of the Royal Statistical Society: Series C (Applied Statistics)''] (accessed 17 February 2010)</ref>

A review of the first 227 algorithms published as source code in Applied Statistics is available.<ref>{{cite journal | last1 = Martynov | first1 = G.V. | year = 1990 | title = Probabilistic-statistical programs from "applied statistics" | url = http://www.springerlink.com/content/v768870578w85872 | journal = Journal of Mathematical Sciences | volume = 50 | issue = 3| pages = 1643–1684 | doi=10.1007/BF01096290}}</ref> The last such code was published in 1997.

'''Past and current editors:'''

{{Div col||25em}}
* 1987–1990: I. R. Dunsmore
* 1989–1992: David J. Hand
* 1991–1994: W. J. Krzanowski
* 1993–1996: D. A. Preece
* 1995–1998: S. M. Lewis
* 1997–2000: J. N. S. Matthews
* 1999–2002: A. W. Bowman
* 2001–2004: Geert Molenberghs
* 2003–2006: C. A. Glasbey
* 2005–2008: M. S. Ridout
* 2007–2010: Chris J. Skinner
* 2009–2012: Stephen Gilmour
* 2011–2014: Richard Chandler
* 2013–2016: Peter W. F. Smith
* 2015–2018: Nigel Stallard
{{Div col end}}

=== ''Journal of the Royal Statistical Society, Series D (The Statistician)'' ===
''The Statistician'' ({{ISSN|0039-0526}}) is no longer published, but was published 4 times a year up to 2003, being replaced by [[Significance (magazine)|Significance]]. The final editors were A.J. Watkins ([[University of Wales]]) and L.C. Wolstenholme ([[City University London]]).<ref>{{cite web|url=http://www3.interscience.wiley.com/journal/120094253/home |title=Journal of the Royal Statistical Society: Series D (The Statistician) - Wiley Online Library |publisher=.interscience.wiley.com |date=2003-11-19 |accessdate=2012-01-22}}</ref> ''The Statistician'' was added in parallel to Series A-C as a Royal Statistical Society publication in 1993, having previously been published by the [[Institute of Statisticians]].

== Allied publications ==
Since 2004 the Society has published ''[[Significance (magazine)|Significance]]'', which consists of articles on topics of statistical interest presented at a level suited to a general audience. From September 2010 ''Significance'' is jointly published with the [[American Statistical Association]] and distributed to members of both societies.<ref>{{cite web|url=http://magazine.amstat.org/blog/2010/05/13/significancecoverstorymay1/ |title=Significance Magazine—An ASA and RSS Partnership &#124; Amstat News |publisher=Magazine.amstat.org |date=2010-05-13 |accessdate=2012-01-22}}</ref>

== References ==
{{reflist}}

== Further reading ==
* (May 1838). "[http://www-history.mcs.st-andrews.ac.uk/Extras/Statistical_society.html Introduction]". ''Journal of the Statistical Society of London'', 1 (1): 1-5. Retrieved on 2007-10-13.

== External links ==
* {{Official website|1=http://www.rss.org.uk/RSS/Publications/Journals/RSS/Publications/Journals_sub/Journals.aspx}}

{{Statistics journals}}

{{DEFAULTSORT:Journal Of The Royal Statistical Society}}
[[Category:Royal Statistical Society]]
[[Category:Statistics journals|Royal Statistical Society]]
[[Category:Publications established in 1838]]
[[Category:English-language journals]]
[[Category:1838 establishments in the United Kingdom]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]