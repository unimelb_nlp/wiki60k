{{Use Indian English|date=September 2015}}
{{Infobox organization
| name   = Gram Vikas
| logo   = [[File:Gram Vikas Organization Logo.png|centre|250px]]
| type   = [[Non-governmental organisation]]
| founded_date      = 1979 <br/> Mohuda, [[Orissa, India]]
| founder           = [[Joe Madiath]]
| location          = Mohuda, [[Orissa, India]]
| key_people        = [[Joe Madiath]], Chairman 
 Debiprasad Mishra, Executive Director
| area_served       = [[Orissa, India|Orissa]], [[Jharkhand]], [[Madhya Pradesh]], [[Andhra Pradesh]], [[Chhattisgarh]] , [[West Bengal]] & [[Nagaland]] in [[India]]; and globally [[Gambia]] , [[Tanzania]]
| focus             = [[Water]] and [[sanitation]]
| method            = [[Empowerment]], [[gender equality]], [[sustainability]], [[self-governance]]
| homepage          = [http://www.gramvikas.org/ www.GramVikas.org]
}}

'''Gram Vikas''' is an Indian [[non-governmental organisation]] based in [[Orissa, India|Orissa]], and founded in 1979.<ref name="DufloBanerjee2011">{{cite book|last1=Duflo|first1=Esther|last2=Banerjee|first2=Abhijit|title=Poor Economics: A Radical Rethinking of the Way to Fight Global Poverty|url=https://books.google.com/books?id=Tj0TF0IHIyAC&pg=PA46|accessdate=12 January 2012|date=2012-01-12|publisher=PublicAffairs|isbn=978-1-58648-798-0|pages=46–}}</ref> It uses common concerns for [[water]] and [[sanitation]] to unite and empower [[rural]] communities, including [[adivasi]] communities.

"Gram Vikas" translates to "village development", both in [[Hindi]] and in [[Oriya language|Oriya]], the official language of Orissa.<ref>{{cite journal|last=Chowdhuri|first=Imran|author2=Filipe M. Santos|title=Scaling Social Innovations: The Case of Gram Vikas|journal=INSEAD|date=October 2010|pages=11|url=http://www.insead.edu/facultyresearch/research/doc.cfm?did=43646|accessdate=19 January 2012}}</ref>

==History==

===Origins===
In 1971, Joe Madiath led 400 volunteers from the [[University of Madras]]' Young Students' Movement for Development to [[West Bengal]] to manage relief camps for refugees of the [[Bangladesh Liberation War]]. Witnessing the devastation of the [[Pre-1980 North Indian Ocean cyclone seasons#1971 Orissa Cyclone|1971 Orissa cyclone]], and realising the comparatively little attention received by the disaster victims, Madiath and a small group of 40 volunteers shifted their attention there.<ref>{{cite web|last=Pradeep|first=K.|title=In Pursuit of Social Justice|url=http://www.thehindu.com/life-and-style/metroplus/article106375.ece|publisher=The Hindu|accessdate=17 January 2012}}</ref>

The group decided that the best way to help the cyclone victims was to provide irrigation facilities. Once these efforts were completed, the group handed over the facilities to the villagers and left. But Madiath and a small group of YSMD volunteers came to the realisation that the irrigation facilities benefited mostly the landlords, and decided to stay in Orissa to become social activists.

Joe Madiath and remaining volunteers moved to [[Ganjam]] district at the invitation of the local authorities, and established Gram Vikas in 1979.

===Adivasi social activism===
Gram Vikas initially focused on social activism in support of adivasi communities who were being exploited by [[landlord]]s and [[liquor]] merchants. Gram Vikas spearheaded a movement to organise adivasi communities to reclaim land, chase out [[moneylender]]s, and free themselves from [[bonded labor]] and [[alcoholism]].

==Activities==
Since its inception, Gram Vikas has worked on a variety of [[Human development (humanity)|development]] issues, including [[biogas]] promotion, community [[forestry]], rural habitat development, and [[education]]. Over the years, however, Gram Vikas have focused their efforts on mechanisms that ensure sustainable solutions for water and sanitation.

===Water and sanitation===
Prior to founding Gram Vikas, Joe Madiath worked on a project in [[Cuttack]] district to build toilets for villagers. However, because the burden of carrying water from the nearby pump to the toilets befell the women of the village, they rejected the approach, and the villagers eventually reverted to unsanitary practices.<ref>{{cite web|last=Dueñas|first=Christina|title=Water Champion: Joe Madiath|url=http://www.adb.org/water/champions/madiath.asp|publisher=Asian Development Bank|accessdate=17 January 2012}}</ref>

As a result, Gram Vikas decided to approach water and sanitation by first building solid partnerships with the villages, insisting on gender equality. Villagers are asked to contribute to a “corpus fund”, which is invested in an interest-earning deposit and used to pay the costs of families moving to the village.<ref>{{cite web|title=Bringing Water Supply and Sanitation Services to Tribal Villages in Orissa the Gram Vikas Way|url=http://www.adb.org/water/actions/IND/gram-vikas.asp|publisher=Asian Development Bank|accessdate=17 January 2012}}</ref>

Gram Vikas also require that all villagers, regardless of economic, social, or [[caste]] [[social status|status]], receive the same types of toilets and bathing rooms. Additionally, the entire community must be involved in the [[planning]], construction, monitoring, and [[Maintenance, repair and operations|maintenance]] of the system. Finally, Gram Vikas promotes [[gender equality]] as an inherent part of their programs.

By their own admission, the Gram Vikas approach is difficult to implement due to the requirements for 100% participation and community equity. However, Gram Vikas claims the solution leads to socially sustainable solutions, thus making it a “good but difficult model”.

===Other development programs===
Gram Vikas also carries out programs on the subjects of community [[health]], [[education]], natural resource management and food security, as well as [[renewable energy]].<ref>{{cite web|title=Gram Vikas Website|url=http://www.gramvikas.org|publisher=Gram Vikas|accessdate=17 January 2012}}</ref>

===Outreach===
As of March 2011, Gram Vikas has carried water and sanitation in 1089 villages in the states of Orissa, Jharkhand, Madhya Pradesh, and Andhra Pradesh, for a total population of 298,000 individuals.<ref>{{cite book|title=Gram Vikas 2010-2011 Annual Report|year=2011|publisher=Gram Vikas|pages=2}}</ref>

==Awards<ref>{{cite web|title=Curriciculum Vitae: Dr. Joseph Madiath|url=http://direc2010.gov.in/pdf/joe-madiath.pdf|publisher=Delhi International Renewal Energy Conference 2010|accessdate=12 January 2012}}</ref>==

===Awarded to Gram Vikas===
* Alan Shawn Feinstein World Hunger Award (1995-1996)<ref>{{cite news|last=Sweeney|first=Tracie|title=India's Gram Vikas to receive Alan Shawn Feinstein World Hunger Award|url=http://brown.edu/Administration/News_Bureau/1995-96/95-125.html|accessdate=12 January 2012|newspaper=The Brown University News Bureau|date=5 April 1996}}</ref>
* Dr. K.S. Rao Memorial National Award (1998)
* [[Global Development Network]] - Japanese Award for Most Innovative Development Project (2001)<ref>{{cite web|title=2001 Global Development Awards and Medals Competition|url=http://cloud2.gdnet.org/cms.php?id=competition_details&competition_id=8|publisher=Global Development Network|accessdate=12 January 2012}}</ref>
* [[World Habitat Award]] (2003)<ref>{{cite web|title=Winner (2003) - Rural Health and Environment Programme|url=http://www.worldhabitatawards.org/winners-and-finalists/project-details.cfm?lang=00&theProjectID=130|publisher=World Habitat Awards|accessdate=12 January 2012}}</ref>
* [[The Tech Awards|Tech Museum Awards Laureate]] - Accenture Economic Development Award (2003)<ref>{{cite web|title=The Tech Awards Laureates 2003|url=http://thetechawards.thetech.org/laureate/archive/2003|publisher=The Tech Awards|accessdate=12 January 2012}}</ref>
* [[World Water Forum#Prizes|Kyoto World Water Grand Prize]] (2006)<ref>{{cite web|title=Announcement of the winner of the Kyoto World Water Grand Prize|url=http://www.worldwatercouncil.org/fileadmin/wwc/News/WWC_News/News_2006/CP_Kyoto_Prize_22-03-06.pdf|publisher=World Water Council|accessdate=12 January 2012}}</ref>
* [[Skoll Foundation#Award winners|Skoll Award for Social Entrepreneurship]] (2006)<ref>{{cite web|title=Social Entrepreneurs: Joe Madiath|url=http://www.skollfoundation.org/entrepreneur/joe-madiath/|accessdate=12 January 2012}}</ref>
* Winner – [[Ashoka: Innovators for the Public#Changemakers|Ashoka Changemakers Competition]] (2006)
* [[UNESCO]] Water Digest Best Water NGO Award (2009-2010)

===Awarded to Executive Director Joe Madiath===
* [[Asian Development Bank]]'s Water Champion Award<ref>{{cite web|last=Dueñas|first=Ma. Christina|title=Water Champion: Joe Madiath - Championing 100% Sanitation Coverage in Rural Communities in India|url=http://www.adb.org/water/champions/madiath.asp|publisher=Asian Development Bank|accessdate=12 January 2012}}</ref>
* [[Schwab Foundation]]'s Outstanding Social Entrepreneur<ref>{{cite web|title=Profile: Joseph Madiath|url=http://www.schwabfound.org/sf/SocialEntrepreneurs/Profiles/index.htm?sname=117600&sorganization=0&sarea=0&ssector=0&stype=0|publisher=Schwab Foundation for Social Entrepreneurship|accessdate=12 January 2012}}</ref>
* [[Godfrey Phillips National Bravery Awards|Godfrey Phillips Red and White Bravery Award]] - Social Lifetime Achievement Award (2005)
* Doctor of Divinity, [[Honorary degree|''honoris causa'']] – [[Gurukul Lutheran Theological College]]
* Lok Samman Award (2009)

==References==
{{reflist|33em}}

[[Category:1979 establishments in India]]
[[Category:Organizations established in 1979]]
[[Category:Organisations based in Odisha]]
[[Category:Non-profit organisations based in India]]