[[File:Internal temperature distribution in a particle.png|thumb|An internal temperature distribution for a spherical particle versus radius and time under a time-varying [[heat flux]].]]

The '''extended discrete element method''' (XDEM) is a numerical technique that extends the dynamics of granular material or particles as described through the classical [[discrete element method]] (DEM) (Cundall<ref>{{cite journal
| first1=P. A.
| last1=Cundall
| first2=O. D. L.
| last2=Strack
| title=A discrete numerical model for granular assemblies
| journal=Geotechnique
| year=1979
| volume=29
| pages=47–65
| doi=10.1680/geot.1979.29.1.47
}}</ref> and Allen<ref>{{cite book
| first1=M. P.
| last1=Allen
| authorlink1=M. P. Allen
| first2=D. J.
| last2=Tildesley
| authorlink2=D. J. Tildesley
| title=Computer Simulation of Liquids
| publisher=Claredon Press Oxford
| year=1990}}</ref>) by additional properties such as the [[thermodynamic]] state, [[Stress (mechanical)|stress]]/[[Deformation (mechanics)|strain]] or [[electro-magnetic]] field for each particle. Contrary to a [[continuum mechanics]] concept, the XDEM aims at resolving the particulate phase with its various processes attached to the particles. While the discrete element method predicts position and orientation in space and time for each particle, the extended discrete element method additionally estimates properties such as internal [[temperature]]  and/or [[species]] distribution or mechanical impact with structures.

==History==

Molecular dynamics developed in the late 1950s by Alder et al.<ref>{{cite journal
| first1=B. J.
| last1=Alder
| first2=T. E.
| last2=Wainwright
| title=Studies in Molecular Dynamics. I. General Method
| journal=J. Chem. Phys.
| year=1959
| volume=31
| pages=459
| doi=10.1063/1.1730376
| bibcode=1959JChPh..31..459A}}</ref> and early 1960s by Rahman<ref>{{cite journal
| first1=A.
| last1=Rahman
| title=Correlations in the Motion of Atoms in Liquid Argon
| journal=Phys. Rev.
| year=1964
| volume=136
| doi=10.1103/physrev.136.a405
| pages=A405–A411
|bibcode = 1964PhRv..136..405R }}</ref> may be regarded as a first step toward the extended discrete element method, although the forces due to collisions between particles were replaced by energy potentials e.g. [[Lennard-Jones]] potentials of [[molecules]] and [[atoms]] as long range forces to determine interaction.

Similarly, the fluid dynamic interaction of particles suspended in a flow were investigated. The [[drag (physics)|drag]] forces exerted on the particles by the relative velocity by them and the flow were treated as additional forces acting on the particles. Therefore, these [[multiphase flow]] phenomena including a solid e.g.~particulate and a gaseous or fluid phase resolve the particulate phase by discrete methods, while gas or liquid flow is described by continuous methods, and therefore, is labelled the combined continuum and discrete model (CCDM) as applied by Kawaguchi et al.,<ref>{{cite journal
| first1=T.
| last1=Kawaguchi
| first2=Y.
| last2=Tsuji
| first3=T.
| last3=Tanaka
| title=Discrete particle simulation of two-dimensional fluidized bed
| journal=Powder Technol.
| year=1993
| volume=77
| doi=10.1016/0032-5910(93)85010-7
| pages=79–87
}}</ref> Hoomans,<ref>{{cite journal
| first1=B. P. B.
| last1=Hoomans
| first2=J. A. M.
| last2=Kuipers
| first3=W. J.
| last3=Briels
| first4=W. P. M.
| last4=Van Swaaij
| title=Discrete particle simulation of bubble and slug formation in a two-dimensional gas-fluidized bed: A hard-sphere approach
| journal=Chem. Eng. Sci.
| year=1996
| volume=51
| doi=10.1016/0009-2509(95)00271-5
| pages=99–118
}}</ref> Xu 1997<ref>{{cite journal
| first1=B. H.
| last1=Xu
| first2=A. B.
| last2=Yu
| title=Numerical simulation of the gas-solid flow in a fluidized bed by combining discrete particle method with computational fluid dynamics
| journal=Chemical Engineering Science
| year=1997
| volume=52
| pages=2785–2809
| doi=10.1016/s0009-2509(97)00081-x
}}</ref> and Xu 1998.<ref>{{cite journal
| first1=B. H.
| last1=Xu
| first2=A. B.
| last2=Yu
| title=Comments on the paper numerical simulation of the gas-solid flow in a fluidized bed by combining discrete particle method with computational fluid dynamics
| journal=Chemical Engineering Science
| year=1998
| volume=53
| pages=2646–2647
| doi=10.1016/s0009-2509(98)00086-4
}}</ref>  Due to a discrete description of the solid phase, [[constitutive equation|constitutive]] relations are omitted, and therefore, leads to a better understanding of the fundamentals. This was also concluded by Zhu 2007 et al.<ref>{{cite journal
| first1=H. P.
| last1=Zhu
| first2=Z. Y.
| last2=Zhou
| first3=R. Y.
| last3=Yang
| first4=A. B.
| last4=Yu
| title=Discrete particle simulation of particulate systems: Theoretical developments
| journal=Chemical Engineering Science
| year=2007
| volume=62
| pages=3378–3396
| doi=10.1016/j.ces.2006.12.089
}}</ref> and Zhu 2008 et al.<ref>{{cite journal
| first1=H. P.
| last1=Zhu
| first2=Z. Y.
| last2=Zhou
| first3=R. Y.
| last3=Yang
| first4=A. B.
| last4=Yu
| title=Discrete particle simulation of particulate systems: A review of major applications and findings
| journal=Chemical Engineering Science
| year=2008
| volume=63
| pages=5728–5770
| doi=10.1016/j.ces.2008.08.006
}}</ref> during a review on particulate flows modelled with the CCDM approach. It has seen a mayor development in last two decades and describes motion of the solid phase by the [[Discrete Element Method]] (DEM) on an individual particle scale and the remaining phases are treated by the [[Navier-Stokes]] equations. Thus, the method is recognized as an effective tool to investigate into the interaction between a particulate and fluid phase as reviewed by Yu and Xu,<ref>{{cite journal
| first1=B. H.
| last1=Xu
| first2=A. B.
| last2=Yu
| title=Particle-scale modelling of gas-solid flow in fluidisation
| journal=Journal of Chemical Technology and Biotechnology
| year=2003
| volume=78
| pages=111–121
| doi=10.1002/jctb.788
}}</ref> Feng and Yu <ref>{{cite journal
| first1=Y. Q.
| last1=Feng
| first2=A. B.
| last2=Yu
| first3=A. B.
| last3=Yu
| first4=A.
| last4=Vince
| title=Assessment of model formulations in the discrete particle simulation of gas-solid flow
| journal=Industrial & Engineering Chemistry Research
| year=2004
| volume=43
| pages=8378–8390
| doi=10.1021/ie049387v
}}</ref> and Deen et al.<ref>{{cite journal
| first1=N. G.
| last1=Deen
| first2=M. V. S.
| last2=Annaland
| first3=M. A.
| last3=Van Der Hoef
| first4=J. A. M.
| last4=Kuipers
| title=Review of discrete particle modeling of fluidized beds
| journal=Chemical Engineering Science
| year=2007
| volume=62
| pages=28–44
| doi=10.1016/j.ces.2006.08.014
}}</ref>  Based on the CCDM methodology the characteristics of spouted and fluidised beds are predicted by Gryczka et al.<ref>{{cite journal
| first1=O.
| last1=Gryczka
| first2=S.
| last2=Heinrich
| first3=N. S.
| last3=Deen
| first4=M.
| last4=van Sint Annaland
| first5=J. A. M.
| last5=Kuipers
| first6=M.
| last6=Mörl
| title=CFD modeling of a prismatic spouted bed with two adjustable gas inlets
| journal=Canadian Journal of Chemical Engineering
| year=2009
| volume=87
| pages=318–328
| doi=10.1002/cjce.20143
}}</ref>

The theoretical foundation for the XDEM was developed in 1999 by Peters,<ref>{{cite journal
| first1=B.
| last1=Peters
| title=Classification of combustion regimes in a packed bed based on the relevant time and length scales
| journal=Combustion and Flame
| year=1999
| volume=116
| pages=297–301
| doi=10.1016/s0010-2180(98)00048-0
}}</ref> who described incineration of a wooden moving bed on a forward acting grate.<ref>{{cite journal
| first1=B.
| last1=Peters
| title=Measurements and application of a discrete particle model (DPM) to simulate combustion of a packed bed of individual fuel particles
| journal=Combustion and Flame
| year=2002
| volume=131
| pages=132–146
| doi=10.1016/s0010-2180(02)00393-0
}}</ref>  The concept was later also employed by Sismsek et al.<ref>{{cite journal
| first1=E.
| last1=Simsek
| first2=B.
| last2=Brosch
| first3=S.
| last3=Wirtz
| first4=V.
| last4=Scherer
| first5=F.
| last5=Kröll
| title=Numerical simulation of grate firing systems using a coupled CFD/Discrete Element Method (DEM)
| journal=Powder Technology
| year=2009
| volume=193
| pages=266–273
| doi=10.1016/j.powtec.2009.03.011
}}</ref> to predict the furnace process of a grate firing system. Applications to the complex processes of a blast furnace have been attempted by Shungo et al.<ref>{{cite journal
| first1=Shungo
| last1=Natsui
| first2=Shigeru
| last2=Ueda
| first3=Zhengyun
| last3=Fan
| first4=Nils
| last4=Andersson
| first5=Junya
| last5=Kano
| first6=Ryo
| last6=Inoue
| first7=Tatsuro
| last7=Ariyama
| title=Characteristics of solid flow and stress distribution including asymmetric phenomena in blast furnace analyzed by discrete element method
| journal=ISIJ International
| year=2010
| volume=50
| pages=207–214
| doi=10.2355/isijinternational.50.207
}}</ref> Numerical simulation of fluid injection into a gaseous environment nowadays is adopted by a large number of CFD-codes codes such as Star-CD of [[CD-adapco]], [[Ansys]] and [[AVL (Engineering Firm)|AVL]]-Fire. Droplets of a spray are treated by a zero-dimensional approach to account for heat and mass transfer to the fluid phase.

==Methodology==
[[File:Staggered methodology for software coupling.png|thumb|Staggered methodology for discrete/continuous applications.]]

Many engineering problems exist that include continuous and discrete phases, and those problems cannot be simulated accurately by continuous or discrete approaches. XDEM provides a solution for some of those engineering applications.

Although research and development of numerical methods in each domains of discrete and continuous solvers is still progressing, software tools are available. In order to couple discrete and continuous approaches, two major approaches are available:

*'''Monolithic approach''': The equations describing multi-physics phenomena are solved simultaneously by a single solver producing a complete solution.
*'''Partitioned or staggered approach''': The equations describing multi-physics phenomena are solved sequentially by appropriately tailored and distinct solvers with passing the results of one analysis as a load to the other.

The former approach requires a solver that handles all physical problems involved, therefore it requires a larger implementation effort. However, there exist scenarios for which it is difficult to arrange the coefficients of combined [[differential equations]] in one [[Matrix (mathematics)|matrix]].

The latter, partitioned, approach couples a number of solvers representing individual domains of physics offers advantages over a monolithic concept.  It encompasses a larger degree of flexibility because it can use many solvers. Furthermore, it allows a more modular software development. However, partitioned simulations require stable and accurate coupling algorithms.

Within the staggered concept of XDEM, continuous fields are described by the solution the respective continuous (conservation) equations. Properties of individual particles such as temperature are also resolved by solving respective conservation equations that yield both a spatial and temporal internal distribution of relevant variables. Major conservation principles with their equations and variables to be solved for and that are employed to an individual particle within XDEM are listed in the following table.

{| border="2" cellspacing="0" align="right" width="400" cellpadding="3" rules="all" style="border-collapse:collapse; empty-cells:show; margin: 1em 0em 1em 1em; border: solid 1px #aaaaaa;"
|+ Conservation principles of Interfaces
|- class="hintergrundfarbe6"
! [[Conservation law (physics)|Conservation law]]
! [[Equation]]
! [[Variable (mathematics)|Variable]]
|-
| Mass (compressible medium) || Continuity || Pressure/density
|-
| [[Linear Momentum]] || [[Navier-Stokes]] || Velocity
|-
| [[Energy]] || Energy || Temperature
|-
| Species mass || Species transport || Mass fractions
|-
| Charge, current || [[Maxwell's equations|Maxwell]] ||  electric, magnetic field, electric displacement field
|}

The solution of these equations in principle defines a three-dimensional and transient field of the relevant variables such as temperature or species. However, the application of these conservation principles to a large number of particles usually restricts the resolution to at most one representative dimension and time due to CPU time consumption. Experimental evidence
at least in reaction engineering supports the assumption of one-dimensionality as pointed out by Man and Byeong,<ref>{{cite journal
| first1=Y. H.
| last1=Man
| first2=R. C.
| last2=Byeong
| title=A numerical study on the combustion of a single carbon particle entrained in a steady flow
| journal=Combustion and Flame
| year=1994
| volume=97
| pages=1–16
| doi=10.1016/0010-2180(94)90112-0
}}</ref> while the importance of a transient behaviour is stressed by Lee et al.<ref>{{cite journal
| first1=J. C.
| last1=Lee
| first2=R. A.
| last2=Yetter
| first3=F. L.
| last3=Dryer
| title=Numerical simulation of laser ignition of an isolated carbon particle in quiescent environment
| journal=Combustion and Flame
| year=1996
| volume=105
| pages=591–599
| doi=10.1016/0010-2180(96)00221-0
}}</ref>

==Applications==

[[File:Particles impacting on a conveyer belt.png|thumb|Deformation of a conveyor belt due to impacting granular material.]]

Problems that involve both a continuous and a discrete phase are important in applications as diverse as pharmaceutical industry e.g.~drug production, agriculture food and processing industry, mining, construction and agricultural machinery, metals manufacturing, energy production and systems biology. Some predominant examples are coffee, corn flakes, nuts, coal, sand, renewable fuels e.g.~biomass for energy production and fertilizer.

Initially, such studies were limited to simple flow configurations as pointed out by Hoomans,<ref>{{cite journal
| first1=B. P. B.
| last1=Hoomans
| first2=J. A. M.
| last2=Kuipers
| first3=W. J.
| last3=Briels
| first4=W. P. M.
| last4=Van Swaaij
| title=Discrete particle simulation of bubble and slug formation in a two-dimensional gas-fluidized bed: A hard-sphere approach
| journal=Chem. Eng. Sci.
| year=1996
| volume=51
| doi=10.1016/0009-2509(95)00271-5
| pages=99–118
}}</ref> however, Chu and Yu<ref>{{cite journal
| first1=K. W.
| last1=Chu
| first2=A. B.
| last2=Yu
| title=Numerical simulation of complex particle-fluid flows
| journal=Powder Technology
| year=2008
| volume=179
| pages=104–114
| doi=10.1016/j.powtec.2007.06.017
}}</ref> demonstrated that the method could be applied to a complex flow configuration consisting of a fluidized bed, conveyor belt and a cyclone. Similarly, Zhou et al.<ref>{{cite journal
| first1=H.
| last1=Zhou
| first2=G.
| last2=Mo
| first3=J.
| last3=Zhao
| first4=K.
| last4=Cen
| title=DEM-CFD simulation of the particle dispersion in a gas-solid two-phase flow for a fuel-rich/lean burner
| journal=Fuel
| year=2011
| volume=90
| pages=1584–1590
| doi=10.1016/j.fuel.2010.10.017
}}</ref> applied the CCDM approach to the complex geometry of fuel-rich/lean burner for pulverised coal combustion in a plant and Chu et al.<ref>{{cite journal
| first1=K. W.
| last1=Chu
| first2=B.
| last2=Wang
| first3=A. B.
| last3=Yu
| first4=A.
| last4=Vince
| first5=G. D.
| last5=Barnett
| first6=P. J.
| last6=Barnett
| title=CFD-DEM study of the effect of particle density distribution on the multiphase flow and performance of dense medium cyclone
| journal=Minerals Engineering
| year=2009
| volume=22
| pages=893–909
| doi=10.1016/j.mineng.2009.04.008
}}</ref> modelled the complex flow of air, water, coal and magnetite particles of different sizes in a dense medium [[cyclone]] (DMC).

The CCDM approach has also been applied to fluidised beds as reviewed by Rowe and Nienow<ref>{{cite journal
| first1=P. N.
| last1=Rowe
| first2=A. W.
| last2=Nienow
| title=Particle mixing and segregation in gas fluidized beds: A review
| journal=Powder Technology
| year=1976
| volume=15
| pages=141–147
| doi=10.1016/0032-5910(76)80042-3
}}</ref> and Feng and Yu<ref>{{cite journal
| first1=Y. Q.
| last1=Feng
| first2=A. B.
| last2=Yu
| first3=A. B.
| last3=Yu
| first4=A.
| last4=Vince
| title=Assessment of model formulations in the discrete particle simulation of gas-solid flow
| journal=Industrial & Engineering Chemistry Research
| year=2004
| volume=43
| pages=8378–8390
| doi=10.1021/ie049387v
}}</ref> and applied by Feng and Yu<ref>{{cite journal
| first1=Y. Q.
| last1=Feng
| first2=A. B.
| last2=Yu
| title=An analysis of the chaotic motion of particles of different sizes in a gas fluidized bed
| journal=Particuology
| year=2008
| volume=6
| pages=549–556
| doi=10.1016/j.partic.2008.07.011
}}</ref> to the chaotic motion of particles of different sizes in a gas fluidized bed. Kafuia et al.<ref>{{cite journal
| first1=K. D.
| last1=Kafuia
| first2=C.
| last2=Thornton
| first3=M. J.
| last3=Adams
| title=Discrete particle-continuum fluid modelling of gas-solid fluidised beds
| journal=Chemical Engineering Science
| year=2002
| volume=57
| pages=2395–2410
| doi=10.1016/s0009-2509(02)00140-9
}}</ref> describe discrete particle-continuum fluid modelling of gas-solid fluidised beds. Further applications of XDEM include thermal conversion of biomass on a backward and forward acting grate. Heat transfer in a [[packed bed]] reactor was also investigated for hot air streaming upward through the packed bed to heat the particles, which dependent on position and size experience different heat transfer rates. The [[deformation (engineering)|deformation]] of a conveyor belt due to impacting [[granular material]] that is discharged over a chute represents an application in the field of [[Stress (mechanical)|stress]]/[[Deformation (mechanics)|strain]] analysis.

{|
| [[File:Temperature distribution on a backward acting grate.png|thumb|Distribution of particles' surface temperature on a backward acting grate.]]
| [[File:Char distribution on a forward acting grate.png|thumb|Progress of pyrolysis of straw blades on a forward acting grate, on which straw is converted into charred material]]
| [[File:Particle temperature in a packed bed reactor.png|thumb|Distribution of porosity inside the packed bed and particle temperature]]
|}

==References==
{{Reflist|30em}}

{{Numerical PDE}}

[[Category:Numerical differential equations]]
[[Category:Computational physics]]