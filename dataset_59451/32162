{{Use mdy dates|date=July 2014}}
{{Infobox musical artist
|name = David Lovering
|background = non_vocal_instrumentalist
|image = David Lovering cropped.jpg
|caption = Lovering performing at Teatro La Cúpula in Santiago, Chile in October 2010
|birth_date = {{Birth date and age|1961|12|6|mf=y}}
|birth_place =[[Burlington, Massachusetts]]
|genre = [[Alternative rock]]
|occupation = Drummer
|years_active = 1986–present
|instrument = Drums, guitar, bass guitar, vocals
|alias = The Scientific Phenomenalist
|label = 4AD
|associated_acts = [[Pixies]], [[Frank Black and the Catholics]], [[Nitzer Ebb]], [[Tanya Donelly]], [[Cracker (band)|Cracker]], [[The Martinis]],<br />Eeenie Meenie, The Hermetic Order of the Golden Dawn
|website = [http://www.davidlovering.com www.davidlovering.com]
}}
'''David Lovering''' (born December 6, 1961) is an American musician and [[Magic (illusion)|magician]]. He is best known as the drummer for the [[alternative rock]] band [[Pixies]], which he joined in 1986. After the band's breakup in 1993, Lovering drummed with several other acts, including [[The Martinis]], [[Cracker (band)|Cracker]], [[Nitzer Ebb]] and [[Tanya Donelly]]. He also pursued a magic career as '''The Scientific Phenomenalist'''; performing scientific and physics-based experiments on stage. When the Pixies reunited in 2004, Lovering returned as the band's drummer.

As a drummer Lovering was inspired by bands from a variety of genres, including [[Rush (band)|Rush]] and [[Steely Dan]].

==Biography==

===Youth and college===
David Lovering was born in [[Burlington, Massachusetts]]. He learned to play drums during his teenage years and joined his high school's marching band.<ref name="pixieshist">Mico, Ted. "Hispanic in the Streets". ''[[Melody Maker]]''. September 1990.</ref> According to his friend John Murphy, Lovering was always very "drum oriented" in his musical taste.<ref name="fg18">Frank, Ganz, 2005. p. 18</ref> In his high school yearbook entry, Lovering stated his three main ambitions: to be in a rock band, to be an electrical engineer, and to tour with Rush, his favorite band.<ref name="misfits">{{cite news | url=http://arts.guardian.co.uk/features/story/0,,1552638,00.html | title=Misfits that fit | accessdate=2007-03-17 | author=Barton, Laura | publisher=''[[The Guardian]]'' | date=July 20, 2005 | location=London}}</ref> After graduating from high school, Lovering studied [[electronic engineering]] at the [[Wentworth Institute of Technology]] in Boston. He got a job at a [[Radio Shack]] store with Murphy,<ref name="fg18" /> and the pair often played practical jokes while at work. One such incident involved Lovering wiring the store toilet to a fire alarm.<ref name="lamagic">{{cite web|url=http://www.laweekly.com/news/features/rock-magicians/2578/ |title=Rock Magicians |accessdate=2007-09-15 |author=Albert, John |publisher=''[[LA Weekly]]'' |date=July 13, 2003 |deadurl=yes |archiveurl=https://web.archive.org/web/20071213212628/http://www.laweekly.com:80/news/features/rock-magicians/2578/ |archivedate=December 13, 2007 |df=mdy }}</ref> After graduating from Wentworth with a bachelor's degree in 1982, he took a job building [[laser]]s, and continued to drum in local bands such as Iz Wizard and Riff Raff.<ref name="pixieshist" /> A number of different genres of music have influenced him, including bands  [[Steely Dan]], [[Led Zeppelin]], and [[Devo]].<ref>{{cite web|url=http://www.nofimagazine.com/e-m-int2.htm |title=Interview with Eeenie Meenie Part Two |accessdate=2007-11-24 |publisher=''No-Fi Magazine'' |deadurl=yes |archiveurl=https://web.archive.org/web/20070715161629/http://nofimagazine.com/e-m-int2.htm |archivedate=July 15, 2007 }}</ref>

===Pixies===
{{Main|Pixies}}
On [[Memorial Day]] 1985, Lovering attended Murphy and [[Kim Deal]]'s wedding service. In January 1986 Deal was hired to play bass in the newly formed Pixies, an [[alternative rock]] band formed by [[Black Francis|Charles "Black Francis" Thompson]] and [[Joey Santiago]]. Murphy suggested that Lovering audition for the band – who were still without a drummer. Lovering had stopped drumming by this point and was at first unimpressed by the trio's performance of the band's songs. However, after playing along he agreed to join.<ref name="fg18" /> Lovering and the band wrote and rehearsed material throughout 1985 and 1986 and performed at small venues in [[Boston]]. The band decided to record 18 songs for [[Pixies (EP)|a demo tape]] in 1987. Lovering co-wrote one of the tape's songs, "Levitate Me" (his only major writing contribution to any Pixies song) and appeared on the cassette's front cover, jogging naked with his back turned to the camera.<ref>Frank, Ganz, 2005. p.&nbsp;55</ref> "Levitate Me" later appeared on the band's first release ''[[Come on Pilgrim]]'', which included seven other songs taken from the demo tape.

The Pixies entered the studio again in 1988 to record their second album ''[[Surfer Rosa]]''. Lovering's contribution on songs such as "Bone Machine" – which begins with a 10-second drum solo – "Break My Body" and "River Euphrates" established his steady, accurate style.<ref>{{cite news | url=https://www.nytimes.com/2004/12/13/arts/music/13pixi.html?_r=1&scp=4&sq=David+Lovering&oref=slogin | title=Once Upon a Time, There Was This Really Loud Band | accessdate=2008-01-19 | author=Sanneh, Kelefa | publisher=''[[New York Times]]'' | date=December 13, 2004}}</ref> ''[[Doolittle (album)|Doolittle]]'', the band's major label debut, followed in 1989. During the album's recording sessions, Thompson convinced Lovering to sing on "La La Love You", which had been written as a "dig at the very idea of a love song".<ref>Sisario, 2006. p.&nbsp;104</ref> The album's producer [[Gil Norton]] later said that during the sessions Lovering "went from not wanting to sing a note to 'I can't get him away from the microphone'. He was such a showman".<ref>Frank, Ganz, 2005. p.&nbsp;113</ref> In addition to drums and vocals, Lovering played bass guitar on the album's penultimate track, "Silver".

After the release of ''Doolittle'', the relationship between the band members became strained because of constant touring and the pressure of releasing three albums in two years. After the final date of the ''Doolittle'' "Fuck or Fight" tour in November 1989, the band was too exhausted to attend their end-of-tour party the following night and shortly afterwards announced a hiatus.<ref>{{cite web|url=http://4ad.com/artists/pixies |title=4AD – Pixies Profile |accessdate=2014-11-15 |publisher=4AD |deadurl=yes |archiveurl=https://web.archive.org/web/20110606063822/http://www.4ad.com/pixies/profile/?page=3 |archivedate=June 6, 2011 |df=mdy }}</ref> After the band reconvened in mid-1990, Lovering moved to Los Angeles along with the rest of the band. The Pixies released two more albums, ''[[Bossanova]]'' (1990) and ''[[Trompe le Monde]]'' (1991). Lovering sang lead vocals on the "[[Velouria]]" B-side "Make Believe"; a song about his admitted "obsession" with US singer-songwriter [[Debbie Gibson]].<ref>{{cite AV media notes | title=Complete 'B' Sides | titlelink=Complete 'B' Sides | others=[[Pixies]] | year=2001 | last=Black | first=Frank | type=CD booklet | publisher=4AD}}</ref> The Pixies toured sporadically throughout 1991 and 1992. They eventually broke up in 1992, mostly due to tensions between Thompson and Deal, although it was not publicly announced until 1993.<ref>"Pixies' Bossa Says It's Nova". ''NME''. January 1993</ref>

===The Scientific Phenomenalist and other projects===
Following the Pixies' breakup, Lovering drummed with several artists, including [[Nitzer Ebb]], but turned down an invitation to join the [[Foo Fighters]].<ref name="fg183">Frank, Ganz, 2005. p. 183</ref> Lovering then joined Santiago's band [[The Martinis]], appearing on their song "Free" on the soundtrack of ''[[Empire Records]]''. However, he soon left the band to become a touring drummer for [[Cracker (band)|Cracker]].<ref>{{cite web | url={{Allmusic|class=artist|id=p653505|pure_url=yes}} | title=The Martinis > Biography | accessdate=2007-11-21 | author=Phares, Heather | publisher=[[Allmusic]]}}</ref> Lovering moved from band to band, drumming with [[Tanya Donelly]]'s group on 1997's ''[[Lovesongs for Underdogs]]'' and with Boston band Eeenie Meenie. After facing difficulty finding new work, Lovering gave up the drums and moved into a rented house that banned drumming.<ref name="fg183" />

Towards the end of the 1990s, Lovering's friend [[Grant-Lee Phillips]] took him to a magic convention. Lovering was very impressed by some of the illusions, and later said "I had to learn how to do it".<ref name="lamagic" /> Mutual friend Carl Grasso invited them to a show at the [[The Magic Castle|Magic Castle]], a magic-oriented nightclub in Los Angeles.<ref name="lamagic" /> There Lovering met [[Possum Dixon]] frontman [[Rob Zabrecky]], and the pair soon became friends. Zabrecky convinced Lovering to apply for a performers' membership to the Magic Castle.<ref name="lamagic2">{{cite web|url=http://www.laweekly.com/news/features/rock-magicians/2578/?page=2 |title=Rock Magicians – Page 2 |accessdate=2007-12-01 |author=Albert, John |publisher=''[[LA Weekly]]'' |date=July 13, 2003 |deadurl=yes |archiveurl=https://web.archive.org/web/20080228075823/http://www.laweekly.com:80/news/features/rock-magicians/2578/?page=2 |archivedate=February 28, 2008 |df=mdy }}</ref> After gaining his membership, Lovering reinvented himself as "The Scientific Phenomenalist". His act combined his electrical engineering knowledge with his stage performance experience. His decision to pursue a career in magic was influenced by the fact that as a musician, he "couldn't top the Pixies".<ref>{{cite news | url=http://observer.guardian.co.uk/omm/story/0,,1992289,00.html | title=Spiritual (and other) rebirths | accessdate=2007-12-17 | author=Bainbridge, Luke | publisher=''[[The Observer]]'' | date=January 21, 2007 | location=London}}</ref>

As the Scientific Phenomentalist, Lovering performs science and [[physics]] experiments in a lab coat while on stage. He shuns traditional magic tricks, and prefers "things that are more mental, using mental powers".<ref name="fg184" /> He later explained: "It's all kind of upbeat, really weird physics experiments that you'll never see. [...] I'd rather have them [the audience] going 'Is it [magic] or isn't it?' rather than 'It's all science' or 'It's all magic'. So I do kinda weird things that other magicians don't do".<ref name="fg184" /> Lovering cites sleight-of-hand artist [[Ricky Jay]], mind reader [[Max Maven]] and [[Eugene Burger]] as influences on his technique.<ref name="lamagic2" /> His performances often involve intricate self-built machines.<ref name="lamagic3">{{cite web|url=http://www.laweekly.com/news/features/rock-magicians/2578/?page=3 |title=Rock Magicians – Page 3 |accessdate=2007-12-01 |author=Albert, John |publisher=''[[LA Weekly]]'' |date=July 13, 2003 |deadurl=yes |archiveurl=https://web.archive.org/web/20080228122901/http://www.laweekly.com:80/news/features/rock-magicians/2578/?page=3 |archivedate=February 28, 2008 |df=mdy }}</ref>

Lovering became part of [[The Unholy Three (magic trio)|The Unholy Three]], a trio of magicians that resides at the Magic Castle, and performs "a new wave, alternative, avant-garde kind of magic".<ref name="lamagic2" /> He toured his act across the United States as the opener for [[Frank Black]] (the new stage name of former Pixies band-mate Thompson),<ref name="fg184">Frank, Ganz, 2005. p. 184</ref> Grant-Lee Phillips, [[The Breeders]] and [[Camper Van Beethoven]]. He performed his act at the [[Shellac (band)|Shellac]]-curated [[All Tomorrow's Parties (music festival)|All Tomorrow's Parties]] music festival in 2002. He later commented that his performance at the festival was "perhaps my greatest achievement".<ref name="lamagic" /> Lovering resumed drumming, appearing at some [[Frank Black and the Catholics]] shows. He also appeared on one track of The Martinis' 2004 album ''[[The Smitten Sessions]]''.

===Pixies reunion===
[[File:David Lovering.jpg|upright|thumb|Lovering at Teatro La Cúpula in Santiago, Chile in October 2010]]
By the summer of 2003, Lovering was feeling depressed.<ref name="misfits" /> In a 2004 interview, he commented: "I remember I was on the way to the bank, and I was just bummed out—everything, financially, was really a mess for me. I was involved in this relationship that was absolutely terrible. I was bottoming out. And I'm on the way to the bank and my cellphone rings. It's Joe [Santiago]; he says, 'Guess what?'"<ref name="misfits" /> Santiago had just received a call from Thompson stating his desire to reunite the Pixies. Lovering was overjoyed at the news.<ref name="misfits" /> He added that "the saddest thing is that when I sat down to rehearse for the Pixies, I couldn't believe that I had given up something that I loved".<ref name="misfits" /> In 2004 Lovering and the band recorded their reunion single, "[[Bam Thwok]]".

Lovering appeared in the 2006 documentary ''[[loudQUIETloud]]'', which covered the Pixies' 2004 reunion tour. His father died midway during the tour, and Lovering began drinking heavily as a result. According to Thompson, Lovering "messed up a couple of songs" during a number of live shows.<ref name="nme">{{cite web | url=http://www.nme.com/news/pixies/24809 | title=Pixies to begin work on new album | accessdate=2007-11-23 | publisher=''[[NME]]'' | date=October 24, 2006}}</ref> "It was all caught on film", said Thompson, "but they re-edited this to look like it happened in the middle of our tour and it looked like this whole tour careened into this drunken stupor with David. It really wasn't like that at all".<ref name="nme" /> He toured with the Pixies throughout 2005 and 2006, while performing at the Magic Castle on Friday nights with The Unholy Three. In 2007, Lovering played a benefit concert for [[Wally Ingram]] as part of The Martinis.<ref name="nmemart">{{cite web | url=http://www.nme.com/news/garbage/26191 | title=Pixies men, Garbage, George Clinton play benefit show | accessdate=2007-10-07 | publisher=''[[NME]]'' | date=February 1, 2007}}</ref> Later that year, he formed a new band called The Hermetic Order of the Golden Dawn, with Los Angeles musicians Amit Itelman and Oscar Rey.<ref>{{cite web | url=http://www.laweekly.com/index.php?option=com_lsd&task=default&id=139797&tab=calendar&Itemid=571 | title=LA Weekly – Calendar Listings | accessdate=2007-12-03 | publisher=''[[LA Weekly]]''}}</ref>

==Discography==

===Pixies===
{{See also|Pixies discography}}
* ''[[Come on Pilgrim]]'' (1987)
* ''[[Surfer Rosa]]'' (1988)
* ''[[Doolittle (album)|Doolittle]]'' (1989)
* ''[[Bossanova (Pixies album)|Bossanova]]'' (1990)
* ''[[Trompe le Monde]]'' (1991)
* ''[[Indie Cindy]]'' (2014)
* ''[[Head Carrier]]'' (2016)

===With Tanya Donelly===
* ''[[Lovesongs for Underdogs]]'' (1997)

===With The Martinis===
* ''[[The Smitten Sessions]]'' (2004)

===With The Everybody===
* ''[[Avatar (The Everybody album)|Avatar]]'' (2009)

==References==
* Frank, Josh; Ganz, Caryn. ''[[Fool the World: The Oral History of a Band Called Pixies]]''. [[Virgin Books]] (2005). ISBN 0-312-34007-9
* Sisario, Ben. (2006). ''Doolittle''. Continuum, 33⅓ series. ISBN 0-8264-1774-4.

==Notes==
{{Reflist|30em}}

==External links==
* [http://www.davidlovering.com Official website]
* [http://www.4ad.com/pixies/ 4AD – Pixies]
<br />
{{Pixies}}

{{featured article}}

{{Authority control}}

{{DEFAULTSORT:Lovering, David}}
[[Category:1961 births]]
[[Category:Living people]]
[[Category:American magicians]]
[[Category:American rock drummers]]
[[Category:Pixies (band) members]]
[[Category:People from Burlington, Massachusetts]]
[[Category:American alternative rock musicians]]
[[Category:Wentworth Institute of Technology alumni]]
[[Category:Alternative rock drummers]]