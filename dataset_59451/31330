{{For|the photojournalist|Stanley Greene}}
{{Use British English|date=July 2012}}
{{Use dmy dates|date=April 2012}}
{{Infobox person
| name                      = Stanley Green
| image                     = File:Stanley Green, Oxford Street, 1977.jpg
| caption                   = Stanley Green in [[Oxford Street]], 1977
| birth_date                = {{Birth date|1915|02|22|df=yes}}
| birth_place               = [[Harringay]], London
| death_date                = {{Death date and age|1993|12|04|1915|02|22|df=yes}}
| death_place               = [[Northolt]], London<ref name=Carter />
| known_for                 = Dietary reform
| parents                   = {{Plainlist|
*  Richard Green
*  May Green}}
| module        = {{Infobox military person|embed=yes
| allegiance    = United Kingdom
| branch        = [[Royal Navy]]
| serviceyears  = 1938–1945
| battles       = [[World War II]]}}
}}
'''Stanley Owen Green''' (22 February 1915 – 4 December 1993), known as the '''Protein Man''', was a [[human billboard]] who became a well-known figure in central London in the latter half of the 20th&nbsp;century.<ref name=McKie/>

Green patrolled [[Oxford Street]] in the [[West End of London|West End]] for 25 years, from 1968 until 1993, with a placard recommending "[[Protein (nutrient)|protein]] wisdom", a [[low-protein diet]] that he said would dampen the [[libido]] and make people kinder. His 14-page pamphlet, ''Eight Passion Proteins with Care'', sold 87,000 copies over 20 years.<ref name=Carter/>

Green's campaign to suppress desire, as one commentator called it, was not always popular, but he became one of London's much-loved eccentrics. The ''Sunday Times'' interviewed him in 1985, and his "less passion from less protein" slogan was used by the fashion house [[Red or Dead]].<ref>[http://www.scotsman.com/news/lives-and-times-stanley-green-1-1126079 "Lives and times: Stanley Green"], ''The Scotman'', 15 July 2006.</ref>

When he died at the age of 78, the ''Daily Telegraph'', ''Guardian'' and ''Times'' published his obituary, and the [[Museum of London]] added his pamphlets and placards to their collection. In 2006 his biography was included in the ''[[Dictionary of National Biography|Oxford Dictionary of National Biography]].<ref name=McKie>David McKie, [https://www.theguardian.com/commentisfree/2008/jul/21/london "Pining for the boards"], ''The Guardian'', 21 July 2008.</ref>

==Early life==
Green was born in [[Harringay]], north London, the youngest of four sons of Richard Green, a clerk for a [[Stopper (plug)|bottle stopper]] manufacturer, and his wife, May. He attended [[Woodside High School, Wood Green|Wood Green School]] before joining the [[Royal Navy]] in 1938.<ref name=Carter>Philp Carter, "Green, Stanley Owen (1915–1993)", ''Oxford Dictionary of National Biography'', Oxford University Press, May 2006. {{doi|10.1093/ref:odnb/92286}}</ref>

Philip Carter writes in the ''Oxford Dictionary of National Biography'' that Green's time with the Navy affected him deeply. He was shocked by the obsession with sex.<ref name=Carter/> "I was astonished when things were said quite openly – what a husband would say to his wife when home on leave", he told the ''Sunday Times'' "A Life in the Day" column in 1985. "I've always been a moral sort of person."<ref name=Green1985>Stanley Green, "My own message to the streets", ''The Sunday Times Magazine'', 14 April 1985.</ref>

After leaving the Navy in September 1945, Green worked for the [[Fine Art Society]]. In March 1946, Carter writes, he failed the entrance exam for the University of London, then worked for [[Selfridges]] and the civil service, and as a storeman for Ealing Borough Council.<ref name=Carter/> He said that he had lost jobs twice because he had refused to be dishonest.<ref>David Weeks, Jamie James, ''Eccentrics: A Study of Sanity and Strangeness'', Random House, 1995, pp.&nbsp;194–195.</ref> In 1962 he held a job with the post office, then worked as a self-employed gardener until 1968 when he began his anti-protein campaign. He lived with his parents until they died, his father in 1966 and his mother the following year, after which he was given a [[Council house|council flat]] in Haydock Green, [[Northolt]], north London.<ref name=Carter/>

==His mission==

===On the streets===
[[File:Stanley Green by Sean Hickin, Oxford Street, London, 1974 (2).jpg|thumb|left|alt=photograph|Oxford Street, 1974]]

Green began his mission in June 1968, at the age of 53, initially in [[Harrow, London|Harrow]] on Saturdays, becoming a full-time human billboard six months later on Oxford Street. He cycled there from Northolt with a [[sandwich board]] attached to the bicycle, a journey of 12 miles (19 km) that could take up to two hours, until he was given a [[bus pass]] when he turned 65.<ref name=Green1985/>

He rose early, and after porridge for breakfast made bread that would rise while he was on patrol, ready for his evening meal. Otherwise his diet consisted of steamed vegetables and pulses, and a pound of apples a day. Lunch was prepared on a [[Bunsen burner]] and eaten at 2:30 in a "warm and secret place" near Oxford Street.<ref name=Green1985/>

From Monday to Saturday he walked up and down the street until 6:30 pm, reduced to four days a week from 1985. Saturday evenings were spent with the cinema crowds in [[Leicester Square]].<ref name=Carter/> He would to go to bed at 12:30 am after saying a prayer. "Quite a good prayer, unselfish too", he told the ''Sunday Times''. "It is a sort of acknowledgment of God, just in case there happens to be one."<ref name=Green1985/>

[[Peter Ackroyd]] wrote in ''London: The Biography'' that Green was for the most part ignored, becoming "a poignant symbol of the city's incuriosity and forgetfulness".<ref>Pater Ackroyd, ''London, A Biography'', Vintage, 2001, p.&nbsp;189.</ref> He was arrested for public obstruction twice, in 1980 and 1985.<ref name=Carter/> "The injustice of it upsets me", he said, "because I'm doing such a good job". He took to wearing overalls to protect himself from spit, several times finding it on his hat at the end of the day.<ref name=Green1985/>

===Writing===
{{quote box
|border=1px
|title=External media
|title_fnt=#555555
|halign=left
|quote=[https://web.archive.org/web/20141006201030/http://www.radiantfuture.eu/Audio/03_Stanley_Green.mp3 "Stanley Green"] (2013)<br/>by [[Martin Gordon]] (song)<br /><br/>
[https://www.flickr.com/photos/16400798@N04/2401520328 Green's Adana Eight-Five printing press],<br/>Gunnersbury Museum
|qalign=center
|fontsize=90%
|bgcolor=#F9F9F9
|bordercolor=#ccc
|width=290px
|align=right
|salign=right
|source=}}
Sundays were spent at home producing ''Eight Passion Proteins'' on his printing press. [[Waldemar Januszczak]] described it as worthy of [[W. Heath Robinson|Heath Robinson]], who was known for his cartoons of ancient contraptions.<ref name=Januszczak/> The racket caused trouble between Green and his neighbours.<ref>Tom Quinn, Ricky Leaver, ''Eccentric London'', New Holland Publishers, 2008, p.&nbsp;[https://books.google.com/books?id=8xEnamQrlvEC&pg=PA14 14].</ref>

Noted for its eccentric typography, ''Eight Passion Proteins'' went through 52 editions between 1973 and 1993.<ref>Matt Lake, Mark Moran, Mark Sceurman, ''Weird England'', Sterling Publishing Company, 2007, p.&nbsp;[https://books.google.com/books?id=S7JNAU3J2lkC&pg=PA115 115].</ref> Green sold 20 copies on weekdays and up to 50 on Saturdays (for 10 pence in 1980 and 12 pence 13 years later), a total of 87,000 copies by February 1993, according to Carter. He sent copies to those in the public eye, including five British prime ministers, the [[Charles, Prince of Wales|Prince of Wales]], the Archbishop of Canterbury, and [[Pope Paul VI]].<ref name=Carter/>

The booklet argued that "those who do not have to work hard with their limbs, and those who are inclined to sit about" will "store up their protein for passion", making retirement, for example, a period of increased passion and marital discord.<ref>Stanley Green, [http://www.flaneur.org.uk/html/green/green2.html ''Eight Passion Proteins''], flaneur.org.uk, p.&nbsp;2.</ref> He left several unpublished manuscripts, including a novel, ''Behind the Veil: More than Just a Tale''; a 67-page text called ''Passion and Protein''; and a 392-page version of ''Eight Passion Proteins'', which, Carter writes, was rejected by Oxford University Press in 1971.<ref name=Carter/>

==Posthumous recognition==
[[File:Stanley Green exhibit, Museum of London, June 2010 (cropped).jpg|thumb|Part of the Stanley Green exhibit at the [[Museum of London]], 2010]]
Green enjoyed his local fame. The ''Sunday Times'' interviewed him in 1985 for its "A Life in the Day" feature, and some of his slogans, including "less passion from less protein" were used on dresses and t-shirts by the London fashion house [[Red or Dead]].<ref>[http://www.redordead.com/story/ "Story"], Red or Dead.</ref>

When he died in 1993 at the age of 78, the ''Daily Telegraph'', ''Guardian'' and ''Times'' all published obituaries. His letters, diaries, pamphlets and placards were given to the Museum of London; other artefacts went to the [[Gunnersbury Park Museum]].<ref>[http://www.museumoflondon.org.uk/archive/exhibits/changing_faces/lives/lives2.htm "Londoners"], Museum of London; Carter (''Oxford Dictionary of National Biography'') 2006.</ref> His printing press was featured in [[Cornelia Parker]]'s exhibition "The Maybe" (1995) at the [[Serpentine Gallery]], along with [[Robert Maxwell]]'s shoelaces, one of [[Winston Churchill]]'s cigars and [[Tilda Swinton]] in a glass box.<ref name=Januszczak>Waldemar Januszczak, "Making an exhibition of herself", ''The Sunday Times'', 10 September 1995, cited in Susan M. Pearce, Paul Martin, ''The Collector's Voice: Critical Readings in the Practice of Collecting'', Ashgate Publishing Ltd, 2002, pp.&nbsp;[https://books.google.com/books?id=gNM1q6y9cuUC&pg=PA293 293–294].</ref> In 2006 he was given an entry in the ''Oxford Dictionary of National Biography.<ref name=McKie/>

Two decades after his death Green was still remembered by writers and bloggers, fondly for the most part, although not invariably so. Artist Alun Rowlands' documentary fiction, ''3 Communiqués'' (2007), portrayed him as trawling the streets of London, "campaigning for the suppression of desire".<ref name=Rowlands>Alun Rowlands, [http://bookworks.org.uk/node/129 ''3 Communiqués''], Book Works, 2007.</ref> Musician [[Martin Gordon]] included a track about Green on his 2013 album, ''Include Me Out''.<ref>[http://martingordon.de/discography/include-me-out/ ''Include Me Out''], martingordon.de.</ref>

<gallery>
File:Stanley Green by Sean Hickin, Oxford Street, London, 1974 (1).jpg|Near Oxford Street, 1974
File:Stanley Green (Protein Man), London, 1983.jpg|Near the corner of [[Dean Street]], [[Soho]], c.&nbsp;1983
File:Stanley Green's placard.jpg|One of Green's placards, Museum of London
File:Cover of Eight Passion Proteins.jpg|''Eight Passion Proteins with Care''
</gallery>

==See also==
*[[Sylvester Graham]]
*[[John Harvey Kellogg]]

==References==
{{reflist|30em}}

==Further reading==
*Cumming, Valerie; Merriman, Nick; Ross, Catherine. "The Protein Man", ''Museum of London'', Scala Books, 1996.
*[[William Donaldson|Donaldson, William]]. ''[[Brewer's Rogues, Villains, and Eccentrics]]'', Cassell Reference, 2004.
*Lloyd, Felix. [http://www.thisislondon.co.uk/standard/article-23596283-im-in-training-to-be-an-old-eccentric.do "I’m in training to be an old eccentric"], ''[[London Evening Standard]]'', 3 December 2008.
*Nevin, Charles. [http://www.independent.co.uk/opinion/commentators/the-third-leader-street-scenery-482189.html "The Third Leader: Street scenery"], ''The Independent'', 13 June 2006.
*Ross, C. "The scourge of sex and nuts and sitting", ''[[The Oldie]]'', March 1997.
*''The London Traveler''. [http://www.thelondontraveler.com/2008/12/the-less-protein-man-a-sight-of-old-london/ "The Less Protein Man – a sight of old London"].
*[[Lynne Truss|Truss, Lynne]]. "Stanley Green", ''The Times'',  25 January 1994.
*Weeks, David Joseph and Ward, Kate. ''Eccentrics: The Scientific Investigation'', Stirling University Press, 1988.
*Wicks, Ben. [http://pqasb.pqarchiver.com/thestar/access/474970601.html?FMT=ABS&FMTS=ABS:FT "Come on up and see my collection of passion fruit"], ''[[The Toronto Star]]'', 8 March 1986.
*Willis, David. [http://pqasb.pqarchiver.com/guardian/doc/187570673.html "A Consuming Passion"] (obituary), ''The Guardian'', 26 January 1994.

{{London history}}
{{featured article}}
{{Authority control}}

{{DEFAULTSORT:Green, Stanley}}
[[Category:1915 births]]
[[Category:1993 deaths]]
[[Category:English health activists]]
[[Category:English food writers]]
[[Category:English health and wellness writers]]
[[Category:English pamphlet writers]]
[[Category:History of the City of Westminster]]
[[Category:People from Harringay]]
[[Category:Royal Navy personnel of World War II]]