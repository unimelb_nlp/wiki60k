{{Infobox company
 | name             = Intacct Corporation
 | logo             = [[Image:Intacct_Logo_2014.jpg]]
 | type             = [[Privately held company|Private]]
 | genre            =
 | foundation       = 1999
 | founder          =
 | location_city    = [[San Jose, California]]
 | location_country = [[United States]]
 | location         =
 | locations        =
 | area_served      =
 | key_people       =
 | industry         = [[Software]]
 | products         =
 | services         =
 | revenue          =
 | operating_income =
 | net_income       =
 | owner            =
 | num_employees    =
 | parent           =
 | divisions        =
 | subsid           =
 | caption           =
 | homepage         = http://intacct.com
 | footnotes        =
 | intl             =
}}

''' Intacct''' provides [[Corporate finance|financial management]] and [[accounting software]] based on a [[cloud computing]] platform.  Intacct is based in [[San Jose, California]].

== Background ==
Intacct was founded in 1999 by [[David Chandler Thomas]]<ref>http://www.davidchandlerthomas.com</ref> to provide small and midsized businesses an online alternative to on-premises and desktop accounting [[solution]]s.

The Intacct name was derived from “internet” and “accounting.” <ref>http://www.pcmag.com/article2/0,2817,2421693,00.asp</ref> Intacct helped pioneer [[Software as a service|software-as-a-service]] or cloud computing, and its products were some of the first to use a multi-tenant approach,<ref>[http://www.zdnet.com/blog/saas/why-multi-tenancy-matters/537 ZDNet: Why multi-tenancy matters]</ref> which defines a unique approach to software architecture that significantly lowers the cost of delivering and using products.

== Products ==
Intacct offers two main products:

* '''Intacct''' - Intacct is a cloud financial management and accounting system specifically designed for use by small and mid-sized companies.
* '''Intacct Accountant Edition''' - Intacct Accountant Edition delivers web based accounting and financial applications that are designed for use by accounting firms and business process outsourcers.

The Intacct system includes applications for accounting, contract management, revenue recognition, inventory, purchasing, vendor management, financial consolidation, and financial reporting.

Writing in PC Magazine, reviewer Kathy Yagal described Intacct's web-based products as being designed for "small to midsize companies (defined as 5 to 1,000 employees) that have outgrown entry-level products like QuickBooks but want a relatively inexpensive alternative to traditional client/server software". The cost is based on how many of the Intacct applications are used and is charged on an annual or quarterly basis.<ref>[http://www.pcmag.com/article2/0,2817,16004,00.asp PC Magazine Review by Kathy Yagal]</ref>

== Awards ==
Intacct has garnered these recent awards:

* 2011 - Top 20 Cloud Software & Apps Vendors<ref>[http://www.crn.com/slide-shows/cloud/229400539/the-top-20-cloud-software-apps-vendors-of-2011.htm?pgno=13 CRN: Top 20 Cloud Software & Apps Vendors of 2011]</ref>  
* 2011 - 25 Cloud Vendors You Need to Know<ref>[http://www.crn.com/slide-shows/cloud/231000079/25-cloud-vendors-you-need-to-know.htm CRN: 25 Cloud Vendors You Need to Know]</ref>
* 2011 - OnDemand 100 Fastest Growing Private Cloud Computing Companies<ref>[http://us.intacct.com/about-us/press/intacct-again-named-annual-ondemand-100-list-top-private-cloud-computing-companies Intacct Again Named to Annual "OnDemand 100" List of Top Private Cloud Computing Companies]</ref>
* 2012 - 20 Coolest Cloud Software Vendors of 2012<ref>[http://www.crn.com/slide-shows/cloud/232602428/the-20-coolest-cloud-software-vendors.htm CRN: The 20 Coolest Cloud Software Vendors]</ref>
* 2012 - OnDemand 100 Top Private Companies<ref>[http://aonetwork.com/announcing-the-2012-ondemand-100-top-private-companies/ Announcing the 2012 OnDemand 100 Top Private Companies]</ref>
* 2012 - CPA Practice Advisor 5-Star Review<ref>[http://www.cpapracticeadvisor.com/article/10685102/2012-review-of-intacct CPA Practice Advisor: 2012 Review of Intacct]</ref>
* 2013 - OnDemand 100 Top Private Companies<ref>[http://aonetwork.com/Announcing-the-2013-OnDemand-100-Top-Private-Companies/ Announcing the 2013 OnDemand 100 Top Private Companies]</ref>
* 2013 - Red Herring Top 100 Companies of North America<ref>[http://www.redherring.com/events/rhna/2013-rhnawinners/ 2013 Top 100 North America: Winners]</ref>
* 2013 - Nucleus Technology ROI Award<ref>[https://online.wsj.com/article/PR-CO-20130603-907380.html Nucleus Research Announces Winners of Tenth Annual Technology ROI Awards]</ref>
* 2013 - Red Herring Top 100 Global Companies<ref>[http://us.intacct.com/about-us/press/intacct-selected-red-herring-top-100-global-company Intacct Selected as a Red Herring Top 100 Global Company]</ref>

Intacct is also a three-time winner of a "Best Places to Work in Silicon Valley" award from Bay Area Newsgroup in 2011, 2012, and 2013.<ref>[http://us.intacct.com/about-us/press/intacct-earns-bay-area-top-workplace-distinction-third-straight-year Intacct Earns Bay Area Top Workplace Distinction for Third Straight Year]</ref>

== AICPA Partnership ==
In 2009, Intacct formed an alliance<ref>[http://lauriemccabe.wordpress.com/2009/04/08/will-cpas-bring-the-cloud-to-earth-for-smbs/ Laurie McCabe, Hurwitz Associates blog - Will CPAs Bring the Cloud to Earth for SMBs?]</ref> with the American Institute of Certified Public Accountants ([[AICPA]]).  This was the first time in the AICPA's history that it recognized a single technology vendor as a preferred provider.<ref>[http://www.informationweek.com/news/services/saas/showArticle.jhtml?articleID=216403517 InformationWeek: Intacct To Put Accounting Software In The Cloud]</ref>  The AICPA and its subsidiary [http://www.cpa2biz.com/ CPA2Biz] aligned with Intacct, naming the company as their preferred provider of financial applications to CPA professionals and AICPA members.  The stated goal of the partnership is to help CPA firms and small and midsized businesses adopt cloud computing to improve their financial performance, take better advantage of financial advice and make better and faster business decisions.<ref name ="prquote">[http://us.intacct.com/corporate/news_events/2009/040709.php Press Release: "AICPA, CPA2Biz and Intacct Form Alliance to Benefit Millions of Small Businesses"]</ref>

== Investors ==
Intacct has raised more than $135M in [[venture capital]] backing, with the most recent funding coming in a $45 million round that combined venture funding and a debt package in February 2014. The majority of the new financing was secured through a $30 million venture funding round led by Battery Ventures, a new Intacct investor, and featured all existing active investors, including Bessemer Venture Partners, Costanoa Venture Capital, Emergence Capital, Sigma Partners, and Split Rock Partners, as well as new investor Morgan Creek Capital Management.<ref>[http://us.intacct.com/about-us/press/intacct-solidifies-its-position-growth-leader-cloud-financial-software-45-million-new Intacct Solidifies Its Position as the Growth Leader in Cloud Financial Software with $45 Million in New Financing]</ref><ref>[http://techcrunch.com/2014/02/18/intacct-raises-45m-to-bring-bean-counters-to-the-cloud/ TechCrunch: Intacct Raises $45M To Bring Bean Counters To The Cloud]</ref>  The company stated it would use this new infusion of capital to "further invest in company growth and expanded product capabilities."

Since its inception, Intacct has received backing from the following firms:
* [[Battery Ventures]]
* [[Bessemer Venture Partners]]
* [[Costanoa Venture Capital]]
* [[Emergence Capital Partners]]
* [[Hummer Winblad Venture Partners]]
* [[JK&B Capital]]
* [[Morgan Creek Capital Management]]
* [[Sigma Partners]]
* [[Split Rock Partners]]
* [[Sutter Hill Ventures]]

== References ==
{{reflist}}

== External links ==
* [http://www.intacct.com/ Company Website]
* [http://www.proformative.com/products/intacct-software-reviews Intacct reviews on Proformative.com]
* [http://sites.force.com/appexchange/listingDetail?listingId=a0N300000016bWPEAY Intacct Reviews on Salesforce.com AppExchange]

{{Cloud computing|Partners=MicroAccounting|Url=http://www.microaccounting.com/intacct/}}
{{accounting software}}

[[Category:Accounting software]]
[[Category:ASP Accounting Systems]]
[[Category:Cloud applications]]
[[Category:Cloud computing providers]]
[[Category:Companies based in San Jose, California]]
[[Category:ERP software companies]]
[[Category:Financial software companies]]
[[Category:Software companies based in California]]