{{Use dmy dates|date=April 2012}}
{{Infobox UN resolution
|number = 1388
|organ = SC
|date = 15 January
|year = 2002
|meeting = 4,449
|code = S/RES/1388
|document = http://undocs.org/S/RES/1388(2002)
|for = 15
|abstention = 0
|against = 0
|subject = The situation in Afghanistan
|result = Adopted 
|image = Ariana Afghan A310-300 F-GEMO.jpg
|caption = Ariana Afghan Airlines aircraft
}}

'''United Nations Security Council resolution 1388''', adopted unanimously on 15 January 2002, after recalling resolutions [[United Nations Security Council Resolution 1267|1267]] (1999) and [[United Nations Security Council Resolution 1333|1333]] (2000) on the situation in [[Afghanistan]], the Council, acting under [[Chapter VII of the United Nations Charter]], lifted sanctions against [[Ariana Afghan Airlines]] as the airline was no longer controlled by or on behalf of the [[Taliban]].<ref name=un>{{cite news|url=http://www.un.org/News/Press/docs/2002/SC7269.doc.htm|title=Security Council decides to lift restrictions on Ariana Afghan Airlines|date=15 January 2002|publisher=United Nations}}</ref> 

The provisions of the resolution also terminated restrictions with regard to the airline, such as the denial of all states to refuse permission to allow Ariana Afghan Airlines aircraft to land, take off or overfly their territory; the freezing of funds and financial assets; and the closure of the airline's offices in their territory.<ref>{{cite book|last=Gowlland-Debbas|first=Vera|title=National implementation of United Nations sanctions: a comparative study|year=2004|publisher=Martinus Nijhoff Publishers|isbn=978-90-04-14090-5|author2=Tehindrazanarivelo, Djacoba Liva |page=15}}</ref>

The sanctions were originally put in place to force the Taliban regime to hand over [[Osama bin Laden]] who was indicted by the United States over the [[1998 United States embassy bombings|1998 bombings]] in [[Kenya]] and [[Tanzania]].<ref>{{cite news|title=With Taliban out, Security Council lifts sanctions against Ariana Afghan Airlines|url=http://www.un.org/apps/news/story.asp?newsid=2637&cr=&cr1=|agency=United Nations News Centre|date=15 January 2002}}</ref>

==See also==
* [[War in Afghanistan (1978–present)]]
* [[International Security Assistance Force]] 
* [[List of United Nations Security Council Resolutions 1301 to 1400]] (2000–2002)
* [[United Nations Assistance Mission in Afghanistan]]
* [[War in Afghanistan (2001–present)]]

==References==
{{reflist|colwidth=30em}}

==External links==
*[http://undocs.org/S/RES/1388(2002) Text of the Resolution at undocs.org]
{{wikisource}}

{{UNSCR 2002}}

[[Category:2002 United Nations Security Council resolutions]]
[[Category:2002 in Afghanistan]]
[[Category:United Nations Security Council resolutions concerning Afghanistan]]
[[Category:January 2002 events]]