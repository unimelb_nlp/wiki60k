{{Infobox journal
| title        = Foundations of Physics
| cover        = [[File:Foundations of Physics cover.jpg]]
| editor       = [[Gerard 't Hooft]] 
| discipline   = [[Physics]]
| language     = [[English language|English]]
| abbreviation = 
| publisher    = [[Springer Science+Business Media|Springer]]
| frequency    = 12/year
| history      = 1970-present
| openaccess   =
| impact       = 1.018
| impact-year  = 2015
| website      = http://www.springer.com/10701
| link1        = http://www.springerlink.com/content/1572-9516
| link1-name   = online access
| link2        =
| link2-name   = 
| RSS          = http://www.springerlink.com/content/1434-6028?sortorder=asc&export=rss
| atom         =
| JSTOR        =
| OCLC         = 1569928
| LCCN         =
| CODEN        =
| ISSN         = 0015-9018
| eISSN        = 1572-9516 
}}

'''''Foundations of Physics''''' is a monthly journal "devoted to the conceptual bases and fundamental theories of modern physics and cosmology, emphasizing the logical, methodological, and philosophical premises of modern physical theories and procedures".<ref>http://www.springer.com/physics/journal/10701</ref> The journal publishes results and observations based on fundamental questions from all fields of physics, including: [[quantum mechanics]], [[quantum field theory]], [[special relativity]], [[general relativity]], [[string theory]], [[M-theory]], [[cosmology]], [[thermodynamics]], [[statistical physics]], and [[quantum gravity]]

''Foundations of Physics'' has been published since 1970. Its founding editors were [[Henry Margenau]] and [[Wolfgang Yourgrau]].<ref>
{{Cite journal
 |last=Margenau  |first=H.
 |last2=Yourgrau |first2=W.
 |year=1970
 |title=Editorial Preface
 |journal=Foundations of Physics
 |volume=1 |issue=1 |pages=1–3
 |doi=10.1007/BF00708649
|bibcode = 1970FoPh....1....1M }}</ref> The 1999 Nobel laureate [[Gerard 't Hooft]] was editor-in-chief from January 2007. At that stage, it absorbed the associated journal for shorter submissions '''''Foundations of Physics Letters''''', which had been edited by [[Alwyn Van der Merwe]] since its foundation in 1988. Past editorial board members (which include several Nobel laureates) include [[Louis de Broglie]], [[Robert H. Dicke]], [[Murray Gell-Mann]], [[Abdus Salam]], [[Ilya Prigogine]] and [[Nathan Rosen]]. [[Carlo Rovelli]] was announced as new editor-in-chief in February 2016.<ref>http://www.springer.com/physics/history+%26+philosophical+foundations+of+physics/journal/10701/PS2?detailsPage=editorialBoard</ref>

== Einstein–Cartan–Evans theory ==

Between 2003 and 2005, ''Foundations of Physics Letters'' published a series of papers by M.W. Evans<ref>
{{cite journal
 |last=Evans |first=E.W
 |year=2003
 |title=A Generally covariant field equation for gravitation and electromagnetism
 |journal=Foundation of Physics Letters
 |volume=16 |issue=4 |pages=369
 |doi=10.1023/A:1025365826316
}}</ref> claiming to make obsolete well-established results of quantum field theory and general relativity.  In 2008,  an editorial was written by the new Editor-in-Chief [[Gerard 't Hooft]] distancing the journal from the topic of [[Einstein–Cartan–Evans theory]].<ref>
{{cite journal
 |last='t Hooft |first=G.
 |year=2007
 |title=Editorial note
 |journal=Foundations of Physics
 |volume=38|issue=1 |pages=1
 |doi=10.1007/s10701-007-9187-8
|bibcode = 2008FoPh...38....1T }}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in the following databases:
{{columns-list|3|
*[[Academic OneFile]]
*[[Academic Search]]
*[[Astrophysics Data System]]
*[[Current Abstracts]]
*[[Current Contents]]/Physical
*[[Chemical and Earth Sciences]]
*[[Digital Mathematics Registry]]
*[[EBSCO]]
*[[Expanded Academic]]
*[[Google Scholar]]
*[[HW Wilson]]
*[[INIS Atomindex]]
*[[Inspec]]
*[[INSPIRE-HEP]]
*[[International Bibliography of Book Reviews]]
*[[International Bibliography of Periodical Literature]]
*[[ISIS Current Bibliography of the History of Science]]
*[[Journal Citation Reports]]/Science Edition
*[[Mathematical Reviews]]
*[[OmniFile]]
*[[Science Citation Index]]
*[[Science Select]]
*[[Scopus]]
*[[Simbad Astronomical Database]]
*[[Summon by Serial Solutions]]
*[[Zentralblatt MATH]]
}}
==References==
{{reflist}}

==External links==
* [http://www.springer.com/west/home/physics?SGWID=4-10100-70-35680179-0 Foundations of Physics]

{{DEFAULTSORT:Foundations Of Physics}}
[[Category:Physics journals]]
[[Category:Philosophy of physics]]