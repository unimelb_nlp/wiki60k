{{Use dmy dates|date=January 2016}}
{{Use Indian English|date=January 2016}}
The proposed [[Dalli-Rajhara]]–[[Jagdalpur]] rail line, on paper for almost three decades, once completed, would connect [[Dalli-Rajhara]] to [[Jagdalpur]], both towns being in [[Chhattisgarh]] state in [[India]]. It would also connect [[Raipur]], the capital city of [[Chhattisgarh]], to [[Jagdalpur]] by rail via [[Durg]]. [[Jagdalpur]], which is about 300&nbsp;km from [[Raipur]], is currently meaningfully connected to it only by road. There is though a roundabout rail route<ref>[http://www.thehindu.com/news/cities/Visakhapatnam/article2868517.ece Extra coach on Vizag-Korba]</ref> to reach [[Raipur]] from [[Jagdalpur]] via [[Koraput]] and [[Rayagada]] in [[Orissa, India|Orissa]]; it is much longer (622&nbsp;km compared to 300&nbsp;km by road) and takes much longer time (about 16 hours as compared to 5–6 hr by road) to be of any utility. In view of this, almost all the transport, in relation to both people and goods, between [[Raipur]] and [[Jagdalpur]], happens only by road.

There have been a series of efforts in the past three decades to have meaningful rail connectivity between [[Jagdalpur]] and [[Raipur]], but none have succeeded so far. It would be evident from a cursory glance at the railway map of India<ref>http://www.irfca.org/faq/faq-map.html</ref> that east central India, i.e., [[Jagdalpur]] and its surrounding areas, has only one railway line i.e. the [[Kothavalasa-Kirandul line]] but not any meaningful passenger connectivity through railways and certainly no usable rail connectivity with [[Raipur]].

==Project rationale==
The proposed [[Dalli-Rajhara]]–[[Jagdalpur]] Rail Line has two main objectives: to haul iron ore from the proposed [[Rowghat Mines|Rowghat]] mines needed for [[Bhilai Steel Plant]] of [[Steel Authority of India Limited]] (SAIL) located in [[Bhilai]] city of [[Chhattisgarh]] state, and to cater to passenger and goods transport needs of those residing along and in the influence zone of the rail line till [[Jagdalpur]], another city in [[Chhattisgarh]] state.

At present, [[Bhilai Steel Plant]] is getting iron ore from [[Dalli-Rajhara]] where the reserve is reported to be fast depleting. The remaining reserve is estimated to last for not more than a few years. Further, [[Bhilai Steel Plant]] is in the process of expanding its crude steel capacity to 7 Mn tonnes per annum from the current 3.925 Mn tonnes per annum.<ref>http://www.sail.co.in/BSP_list_of_packages_in_CPFR.pdf</ref>

The [[Rowghat Mines|Rowghat]] mining project, therefore, is expected  to substitute [[Dalli-Rajhara]] mines as well as support the ongoing plant expansion project in so far as supply of iron ore to the plant is concerned.

In view of this, the proposed rail line has become critical for supporting the iron ore needs of [[Bhilai Steel Plant]]. The fate of the proposed line is in essence tied to that of the [[Rowghat Mines|Rowghat]] mining project.

==History==
The development of this rail line is integral to and is linked to the development of the [[Rowghat Mines]]. Hence issues and hurdles affecting the [[Rowghat Mines]] have been directly impacting the fate of this rail line as well.

SAIL made its first application in 1983 for [[Rowghat Mines|Rowghat]] mines and after 13 years in 1996, the [[Ministry of Environment and Forests (India)|Ministry of Environment and Forests (MoEF), Government of India]] granted in principle environmental clearance.<ref>http://www.financialexpress.com/news/sc-flags-green-signal-for-forestry-clearance-to-rowghat/372170/3</ref>

A Memorandum of Understanding (MoU) among Railway, [[Madhya Pradesh]] State Government (from which [[Chhattisgarh]] state was later formed on 1 November 2000), [[National Mineral Development Corporation]] (NMDC) and [[Steel Authority of India Limited]] (SAIL) was signed on 02.04.1998.<ref name="bare_url">http://164.100.24.167:8080/members/website/quest.asp?qref=170158</ref>

In 2004 the [[Ministry of Environment and Forests (India)|Ministry of Environment and Forests (MoEF)]] asked SAIL to submit fresh application for forestry and environment clearance. SAIL, after conducting studies by IBM, Central Mines and Research Institute, Zoological Survey of India, National Environmental Engineering Research Institute and others, submitted the application in 2006. After the government of [[Chhattisgarh]] forwarded SAIL’s proposal for forestry clearance in May 2007 to the [[Ministry of Environment and Forests (India)|Ministry of Environment and Forests (MoEF)]], the ministry referred it to the empowered committee of the Supreme Court in around June 2007.<ref name="financialexpress">http://www.financialexpress.com/news/sc-flags-green-signal-for-forestry-clearance-to-rowghat/372170/0</ref>

A revised Memorandum of Understanding (MOU) was signed between the Ministry of Railways and Government of [[Chhattisgarh]], [[Steel Authority of India Limited]] (SAIL), [[National Mineral Development Corporation]] (NMDC) on 11.12.2007 to implement the construction of Dalli-Rajhara–[[Rowghat Mines|Rowghat]]–Jagdalpur (235&nbsp;km) broad gauge rail link project on cost-sharing basis.<ref name="thehindubusinessline">http://www.thehindubusinessline.com/todays-paper/tp-logistics/article1677158.ece</ref>

The Supreme Court Committee gave its final consent for forestry clearance of [[Rowghat Mines]] during October 2008 for its F block. The Supreme Court ruling was forwarded to the [[Ministry of Environment and Forests (India)|Ministry of Environment and Forests (MoEF)]] for the final clearance.<ref name="financialexpress" />

The Mineral Resources Department of [[Chhattisgarh]] government finally in October 2009 granted mining lease for F Block of [[Rowghat Mines]] to SAIL for a period of 20 years after getting the due Environmental Clearance and Forestry Clearance from the [[Ministry of Environment and Forests (India)|Ministry of Environment and Forests (MoEF)]].<ref>http://www.business-standard.com/india/news/bhilai-steel-gets-rowghat-mining-lease/374442/</ref>

==Project phases==
The proposed 235&nbsp;km long rail line is planned to be implemented in two phases. In Phase 1, the line is proposed to link [[Dalli-Rajhara]] to [[Rowghat Mines]] located in south about 95&nbsp;km. In Phase 2, the line would be extended till [[Jagdalpur]] which is located 140&nbsp;km south of [[Rowghat Mines|Rowghat]].<ref name="bare_url" />

Construction of the first phase, cost of which is to be borne entirely by M/s SAIL, was entrusted to [[Rail Vikas Nigam Limited]] (RVNL).<ref name="bare_url_a">http://164.100.24.167:8080/members/website/quest.asp?qref=139701</ref>

==Stations in Phase1 Dalli-Rajhara–Rowghat==
Starts from [[Dalli-Rajhara]] Railway Station. Following are new Stations.
* 1. Gudum(near Dondi)
* 2. Bhanupratappur
* 3. Kewati
* 4. Antagarh
* 5. Taroki
* 6. Rowghat

Contract value for package 1 is Rs 978.1 Mn. The contract period is due to expire on 09.06.2012.<ref name="rvnl" />

Package 2 of the contract has been awarded by RVNL on 18.07.2011 to M/s JMC Projects, [[Ahmedabad]] for construction of road bed, supply of ballast, major & minor bridges, residential and service buildings & general electrical work etc. for the 53&nbsp;km Keoti–Rowghat section.

Contract value for package 2 is Rs 1317.6 Mn. The contract period is due to expire after passage of 36 months from the date of award, i.e., on 17.07.2014.<ref name="rvnl" />

===Project management consultancy===
[[Rail Vikas Nigam Limited|RVNL]] has awarded contract on 28.07.2010 to a consortium of M/s Lion Engineering Consultant & M/s Consulting Engineers 
Group Ltd, [[Bhopal]] for providing project management consultancy for construction of road bed, supply of 
ballast, p-way, major & minor bridges, residential and service buildings, signal and telecom & electrical work etc., for [[Dalli-Rajhara]]–Rowghat rail link project.

Contract value is Rs 30.285 Mn. The contract period is due to expire on 13.02.2013.<ref name="rvnl" />

===Land acquisition for phase 1 of the project===

The South East Central Railway, entrusted with the job of acquisition of land and handing over it to RVNL, has already acquired and handed over the 48&nbsp;km stretch for the proposed railway project. As of January 2010, acquisition of another 12&nbsp;km, belonging to private owners was underway.<ref>http://www.thehindubusinessline.com/todays-paper/tp-logistics/article975393.ece</ref>

The [[Chhattisgarh]] Government had acquired the land for the 12&nbsp;km stretch.

As of January 2010, for the balance 35&nbsp;km, the final clearance from the environment authorities was awaited. Of 35&nbsp;km, 25&nbsp;km stretch passes through reserve forest covering 259 hectares and the balance through revenue forest over 83.5 hectares.

The director-general, Forest, Government of India, had already given clearance to the Railways for laying line through the forest areas and necessary approvals under the Tribal Welfare Act too had been obtained from the authorities concerned. The approval of the Forest Advisory Committee under the Central Empowered Committee was awaited.

Since the Ministry of Environment and Forest had already granted permission to the Bhilai Steel Plant for mining iron ore in the forest covered Rowghat area, it was only a matter of time before the permission to the Railways too was expected to become available.

===Land acquisition for phase 2 of the project===

As of September, 2011, for phase 2 of the rail link project, between [[Rowghat Mines|Rowghat]]–[[Jagdalpur]] (140&nbsp;km), land acquisition / diversion of land of approximately 854 hectare had been submitted to respective district Collectors and Divisional Forest Officer/Narayanpur. Field verification by revenue authorities is in progress.<ref name="bare_url" />

===Stations at Phase II===
Station - Distance from Jagdalpur.
* jagdalpur -00

* Kudkanar -12.3
* Bastar -22.68
* Sonarpal -31
* Bhanpuri -41.60
* Dhikonga -52.16
* Bniaganv -61.45
* Kondagaon -70.70
* Jugani -79.23
* Chandganv -100.53
* Narayanpur -117.47
* Barnda -128.91
* Rowghat -138.50



==Project cost and Its sharing==

Latest anticipated cost (as of September 2011) of the project is pegged at Rs. 11052.3 Mn.<ref name="bare_url" />

The project cost, at 2004–05 price level, was estimated at Rs 9686 Mn. The project cost is proposed to be shared by SAIL, NMDC, Indian Railways and [[Chhattisgarh]] Government.<ref name="thehindubusinessline" />

As per 2004-05 estimate, SAIL will bear the entire cost of Rs 3040 Mn for Phase 1. Phase 2 will cost Rs 6400 Mn – which will be shared by Indian Railways (Rs 3760 Mn), SAIL (Rs 1413 Mn), [[Chhattisgarh]] (Rs 763 Mn) and NMDC (Rs 707 Mn).<ref name="thehindubusinessline" />

Expenditure of Rs. 1538.6 Mn had already been incurred on the project up to March 2011 and an amount of Rs. 1689.2 Mn was provided in Budget 2011-12 for this project.<ref name="bare_url" />

As per the recent (2015) preliminary estimates, the second phase of the project is projected to cost about Rs 2,000 crore. For this project, the State Government and Central Government have formed a joint venture (JV) in which NMDC, SAIL and IRCON are also party. The Chhattisgarh Government will have 10 per cent equity, NMDC 43 per cent equity, SAIL 21 per cent and IRCON will have 26 per cent equity in the JV. <ref>http://www.dailypioneer.com/state-editions/dpr-on-jagdalpur-rowghat-rly-track-project-soon.html</ref>

==Challenges for the project==

===Opposition from Naxalites===

Project, after facing many years of procedural and clearance related hurdles, is currently facing stiff resistance and opposition from Naxalites.<ref>http://www.business-standard.com/india/news/chhattisgarh-govt-to-setcommittee-to-expidate-workrowghat-iron-ore-mines-/445846/</ref><ref>http://www.miningweekly.com/article/indias-rowghat-iron-ore-project-delayed-on-security-concerns-2011-09-09</ref>

The Rowghat iron ore mining project in Kanker and Narayanpur districts had hit blockade following threats from the Naxalites. The left extremist group have a strong influence in the Rowghat mines area and their opposition is preventing start of the work.

Project is getting delayed as contractors have failed to start work for developing mines as Naxalites had threatened them. A few contractors had in fact started the work. But they soon abandoned when rebels struck and damaged the machines and equipment. Even railways that would lay rail lines had failed to start the work.

Union Home minister Rajnath Singh in a meeting held with Railway Minister during February 2015 has assured that centre would provide all necessary assistance to start the work that would require a heavy deployment of security personnel following Naxalites open threat. The rebels had set equipment on fire when the authorities tried to start the work on Rowghat project.<ref>http://www.business-standard.com/article/economy-policy/rowghat-iron-ore-project-gets-home-ministry-s-push-115021001610_1.html</ref>

The Centre has subsequently deployed as many as four battalions of Central Armed Police Forces (CAPFs) for ensuring security in the mineral-rich Rowghat area in Kanker district of Bastar in Chhattisgarh. <ref>http://www.dailypioneer.com/state-editions/four-capf-battalions-now-guard-rowghat-mining-area.html</ref>

===Environment clearance===
The environment and forestry clearance for undertaking mining in F Block of the [[Rowghat Mines]] took [[Steel Authority of India Limited|SAIL]] nearly 26 years to obtain as detailed in [[#History|History]] section above. This has led to delay in launch of the rail line project as well.

==Current status==

The 235-km-long railway line will be constructed in two phases. Work on the first phase of 95 km long track from Dallirajhara to Rowghat is in progress, while for the construction of 140 km long second phase from Rowghat to Jagdalpur, a Memorandum of Understanding (MoU) was signed amongst Government of Chhattisgarh, NMDC, SAIL, and IRCON on May 9, 2015 at a programme organized at Dantewada in Bastar in presence of  the Prime Minister and Chhattisgarh Chief Minister. <ref>http://www.dailypioneer.com/state-editions/raipur/spv-on-horizon-for-jagdalpur-rowghat-rly-project.html</ref>

The contracts for conducting surveys, undertaking construction (in two packages) and project management consultancy have been awarded by RVNL for Phase 1.<ref name="rvnl">http://www.rvnl.org/contracts/20111215102652_Upto_Nov_2011.pdf</ref><ref>http://economictimes.indiatimes.com/industry/indl-goods/svs/steel/rowghat-mine-project-may-include-security-expenses-sail/articleshow/34077528.cms</ref> No contracts have been awarded for Phase 2 of the project as of November 2011.<ref name="bare_url_a" />

The work for final location survey and preparation of detailed project report (DPR) in connection with construction of new broad-gauge railway line phase 2 from Jagdalpur to Rowghat measuring an estimated 124 km is likely to commence soon.<ref>http://www.dailypioneer.com/state-editions/dpr-on-jagdalpur-rowghat-rly-track-project-soon.html</ref>

===Dallirajhara-Rowghat===

The 17 km line between Dalli Rajhara and Gudum of out of phase 1 stretch of 85 km till rowghat is completed. The Passenger Train from Raipur to Dalli Rajhara (Via Durg) will be getting extended to Gudum. This will open the gate for connectivity with Kanker District.

===Jagdalpur-Rowghat <ref>http://www.dailypioneer.com/state-editions/raipur/spv-on-horizon-for-jagdalpur-rowghat-rly-project.html</ref>===

An SPV "Chhattisgarh South Railway Pvt Ltd" is proposed to be formed for undertaking implementation of the railway line from Jagdalpur to Rowghat.

An agreement amongst NMDC, IRCON, SAIL and CMDC for this has been signed on 20 January 2016. This will be followed up with incorporation of the SPV/company. 

The equity of various JV partners is as follows-- NMDC (National Mineral Development Corporation)-43 per cent, IRCON- 26 per cent, SAIL (Steel Authority of India Ltd)- 21% and CMDC (Chhattisgarh Mineral Development Corporation) 10 per cent.

SAIL, IRCON, CMDC and Ministry of Railways will each have one Director  in the company whereas NMDC will have two Directors and the Chairman of the company will be a nominee of NMDC.

The 142 km rail line from Rowghat to Jagdalpur will be constructed at the cost of Rs 2000 crores. In this route, total 32 major bridges and 172 minor bridges will be constructed. The total number of stations will be 12 with 7 crossing stations. Roughly 876 hectares of land will have to be acquired for the project.

Presently, Survey work, feasibility report and detailed project report (DPR) is under preparation. Target has been set to complete survey work of Jagdalpur-Kondagaon-Narayanpur by the month of March 2016 and of Narayanpur-Raoghat Road by the month of May 2016.

===Location survey===
[[Rail Vikas Nigam Limited|RVNL]] has awarded contract on 23.09.2008 to M/s Balaji Rail Road Systems Ltd, [[Secunderabad]] for conducting final location survey, marking centre line for final alignment on ground, design, detailed engineering, preparation of drawings and seeking approval from RVNL/Railway for the 95.00&nbsp;km rail link from [[Dalli-Rajhara]] to [[Rowghat Mines|Rowghat]].

Contract value is Rs 9.497 Mn. The contract period was initially set to expire on 15.02.2009 but was later extended by RVNL up to 30.04.2011.<ref name="rvnl" />

===Construction===
Construction of Phase 1 of the project has been divided into two main packages. Package 1 is from  [[Dalli-Rajhara]] to Keoti and the Package 2 is from Keoti to Rowghat. Contracts for both the packages of Phase 1 have already been awarded by RVNL.

Package 1 of the contract has been awarded by RVNL on 20.10.2009 to M/s SMS Infrastructure Limited, [[Nagpur]] for construction of road bed, supply of ballast, major & minor bridges, residential and service buildings & general electrical work etc., for  the 42&nbsp;km [[Dalli-Rajhara]]–Keoti section.

==References==
{{reflist}}

{{Railways in Central India}}

{{DEFAULTSORT:Dalli-Rajhara-Jagdalpur rail line}}
[[Category:Rail transport in Chhattisgarh]]
[[Category:Steel Authority of India]]
[[Category:Proposed railway lines in India]]