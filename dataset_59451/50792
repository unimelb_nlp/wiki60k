[[Image:Cover-BIFAO105.jpg|thumb|200px|right|Cover of ''Le Bulletin de l’Institut Français d’Archéologie Orientale'' (BIFAO), Vol.105 (2005).]]
Le '''''Bulletin de l’Institut Français d’Archéologie Orientale''''' ("Bulletin of the French Institute of Eastern Archaeology"), or '''BIFAO''' is a scientific journal containing scholarly articles pertaining to the study of [[Egyptology]].

Published annually in [[Cairo]] since 1901 by the [[Institut Français d'Archéologie Orientale]], it is one of the oldest and most well-known Egyptological journals.

Although primarily a [[French language|French]] publication {{ISSN|0255-0962}}, articles written in other languages (primarily [[English language|English]] and [[German language|German]]) are also accepted.

The Institute's official website provides an online index of articles from the Bulletin. For issues from Vol.1 (1901) to Vol.85 (1985), each article title includes a link to a [[PDF]] file of the article itself.

In recent years, the Bulletin has displayed a different picture on the cover of each issue.

== External links ==
*[http://www.ifao.egnet.net/doc/PubEnLigne/BIFAO BIFAO article index] (online articles & article title listings)
*[http://www.ifao.egnet.net Official website of the French Institute of Eastern Archaeology]
*[https://web.archive.org/web/20061117125649/http://www.cepam.cnrs.fr/index2.php?page=sommaires/BIFAO Picture of cover of Vol.98 (1998)], with indexes of issues from 1998-2000.

{{DEFAULTSORT:Bulletin de l'Institut Francais d'Archeologie Orientale}}
[[Category:Publications established in 1901]]
[[Category:Egyptology journals]]
[[Category:French-language journals]]
[[Category:Multilingual journals]]