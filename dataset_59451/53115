{{redirect|Foundation Press|the Christian publisher|Lockman Foundation}}
{{Update |inaccurate=yes |talk=Article has been out of date since Feb 2013 due to ownership change |date=January 2017}}
{{Unreferenced|date=December 2015}}
{{Infobox publisher
| image        = [[Image:West blue.png|200px]]
| caption      = Logo used from 1999 to 2008
| parent       = Thomson Reuters
| status       = 
|name          = West
| founded      = 1872
| founder      = John B. West 
| successor    = 
| country      = [[United States]]
| headquarters = [[Eagan, Minnesota]]
| distribution = 
| keypeople    = 
| publications = [[Book]]s, [[database]]s
| topics       = Law
| genre        = 
| imprints     = 
| revenue      = 
| numemployees = 
| nasdaq       = 
| url          = 
}}
'''West''' (also known by its original name, '''West Publishing''') is a business owned by [[Thomson Reuters]] that publishes legal, business, and regulatory information in print, and on electronic services such as [[Westlaw]].  Since the late 19th century, West has been one of the most prominent publishers of legal materials in the [[United States]].  Its headquarters is in [[Eagan, Minnesota]]; it also has an office in [[Rochester, New York]], and had an office in [[Cleveland, Ohio]], until it closed in 2010.  Organizationally, West is part of the global legal division of Thomson Reuters.

==History==
West Publishing was founded by John B. West and his brother Horatio in 1872 in [[St. Paul, Minnesota]].  It is best known for establishing the [[National Reporter System]], a system of regional [[Law report|reporter]]s, each of which became known for reporting state court appellate decisions within its region.  West also reports decisions of the [[United States courts of appeals|federal Courts of Appeals]] in the ''[[Federal Reporter]]'' and of the [[United States district courts|federal district courts]] in the ''[[Federal Supplement]]'', and retroactively republished the decisions of all lower federal courts predating the NRS in ''[[Federal Cases]]''.  All these reporters are also part of the NRS, meaning that all cases published therein are annotated with [[headnote]]s by West attorney-editors, and all those headnotes are then indexed in the [[West American Digest System]] (and its electronic version, KeyCite) for easy cross-referencing. 

Technically, all of West's reporters were originally unofficial reporters published without the express authorization or endorsement of the courts.  West reporters have become the nationwide ''[[de facto]]'' standard used by all federal courts and most state courts, despite their technically unofficial nature.  Indeed, over 20 states have discontinued publication of their own official reporters, and a few states with West's cooperation began inserting certificates in the volumes of the relevant West regional reporter to certify it as their official reporter.

In 1995 West retained the services of A.G. Edwards and Goldman Sachs in a search for potential purchasers. Thomson purchased West in 1996. Thomson also consolidated into West a number of other law book companies purchased by either Thomson or West, including Bancroft-Whitney, Banks-Baldwin, Barclay, Callaghan & Company, Clark Boardman, Foundation Press, Gilbert's, Harrison, Lawyers Cooperative Publishing, and Warren, Gorham & Lamont. Today, West also publishes some treatise titles purchased from Shepard's (but not [[Shepard's Citations]]).  Through these acquisitions, Thomson has become one of the "big three" legal publishers, along with [[LexisNexis]] and [[Wolters Kluwer]].  Following the acquisition by Thomson, West was known as WIPG, West Information Publishing Group. From 1997 to 2004, West was known as "West Group".

In 2009/2010 West began offering buyouts to its U.S. editorial staff as it began to move editorial production overseas.

==West products and services==
{{Expand section|Basic company data (number of employees, revenue & profits)|date=September 2008}}
{|
|----- valign="top"
|
*AAJ Press
*''American Casebook'' series
*''[[American Jurisprudence]]''
*''[[American Law Reports]]''
*Aspatore Books
*''Black Letter'' series
*''[[Black's Law Dictionary]]''
*Calendars
*[[Case law]]
*CLE programs
*''Concepts and Insights'' series
*Contact networks
*''[[Corpus Juris Secundum]]''
*Court rules
*Dictionaries/desk references
*Digests
*Document retrieval services
*''Exam Pro'' series
|
*''[[Federal Reporter]]''
*''[[Federal Supplement]]''
*[[Findlaw]]
*Forms
*Handbooks
*''Hornbook'' series
*[[Jury instructions]]
*Keycite and Citators
*Law firm marketing services
*[[Law review]]s and journals
*Law school casebooks
*Law school publications
*Lawyering skills
*Legal assistant/paralegal
*Legal encyclopedias
*LiveNote
*''Nutshell'' series
*Practitioner treatises
|
*Public records
*[[National Reporter System|Reporters]]
*''[[Restatements of the Law]]''
*[[Rutter Group]]
*[[Statute]]s
*''Supreme Court Reporter''
*''Turning Point'' series
*''Uniform Laws Annotated''
*''[[United States Code Congressional and Administrative News]]''
*''University Casebook'' series
*''University Textbook'' series
* ''USCA'' (''[[United States Code#Annotated codes|United States Code Annotated]]'')
*[[West American Digest System]]
*[[Westlaw]]
*[[WestlawNext]]
*West Court Reporting Services
*[[West LegalEdcenter]]
*[[Words And Phrases]]
|}

==External links==
* [http://www.mnopedia.org/group/west-publishing-company West Publishing Company in MNopedia, the Minnesota Encyclopedia]

{{Dakota County, Minnesota topics}}
{{DEFAULTSORT:West (Publisher)}}
[[Category:Book publishing companies based in Minnesota]]
[[Category:Companies based in Eagan, Minnesota]]
[[Category:Bibliographic database providers]]
[[Category:Publishing companies established in 1872]]
[[Category:Legal publishers]]
[[Category:1872 establishments in Minnesota]]