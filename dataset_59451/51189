{{Infobox journal
| title = eLife
| italic title = no
| cover = [[File:ELife logo.png]]
| editor = [[Randy Schekman]]
| discipline = [[Biomedicine]], [[life sciences]]
| abbreviation = eLife
| publisher = eLife Sciences Publications
| country = 
| frequency = Continuous
| history = 2012–present
| openaccess = Yes
| license = [[CC-BY 3.0]], [[CC-BY 4.0]], and [[CC0]]
| impact = 8.303
| impact-year = 2015
| website = http://elifesciences.org/
| JSTOR = 
| OCLC = 813236730
| LCCN = 
| CODEN = ELIFA8
| ISSN = 
| eISSN = 2050-084X
}}
{{DISPLAYTITLE:''eLife''}}
'''''eLife''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] for the [[Biomedicine|biomedical]] and [[life sciences]], It was established at the end of 2012 by the [[Howard Hughes Medical Institute]], [[Max Planck Society]], and [[Wellcome Trust]], following a workshop held in 2010 at the [[Janelia Farm Research Campus]]. Together, these organizations provided the initial funding to support the business and publishing operations,<ref>{{citation |url=http://www.bbc.co.uk/news/science-environment-17668722 |date=10 April 2012 |title=Trust pushes for open access to research |author=Matt McGrath |publisher=[[BBC]]}}</ref> and in 2016 the organizations committed USD$26 million to continue publication of the journal.<ref>{{Cite journal |last=Callaway |first=Ewen |date=2016-06-02 |title=Open-access journal eLife gets £25-million boost |url=http://www.nature.com/news/open-access-journal-elife-gets-25-million-boost-1.20005 |journal=[[Nature (journal)|Nature]] |volume=534 |issue=7605 |pages=14–15 |doi=10.1038/534014a}}</ref>

The  [[editor-in-chief]] is [[Randy Schekman]] ([[University of California, Berkeley]]).<ref name=THE>{{citation |url=http://www.timeshighereducation.co.uk/story.asp?sectioncode=26&storycode=418047&c=1 |title=Open-access science journal leaves editing to the experts |date=5 November 2011 |author=Freya Boardman-Pretty |journal=[[Times Higher Education]]}}</ref> Editorial decisions are made largely by senior editors and members of the board of reviewing editors; all of whom are active scientists working in fields ranging from [[human genetics]] and [[neuroscience]] to [[biophysics]] and [[epidemiology]].<ref>{{citation |url=http://elifesciences.org/about |title= Communicating the latest advances in life science and biomedicine |publisher=eLife |accessdate=6 October 2015}}</ref> 

==Business model==
In September 2016, the journal announced that starting on 1 January 2017, it will introduce [[article processing charge]]s of USD$2,500 for papers accepted for publication.<ref>{{Cite web |url=https://elifesciences.org/elife-news/inside-elife-setting-fee-publication |title=Inside eLife: Setting a fee for publication |access-date=2016-09-29}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in [[Medline]], [[BIOSIS Previews]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-10-10}}</ref> [[Chemical Abstracts Service]],<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-10-10 }}</ref> [[Science Citation Index Expanded]],<ref name=ISI/> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-10-10}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 8.303.<ref name=WoS>{{cite book |year=2016 |chapter=eLife |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> However, the journal claims that it will not promote its impact factor.<ref>{{citation |url=http://elifesciences.org/about |title= Communicating the latest advances in life science and biomedicine |publisher=eLife |accessdate=26 October 2015}}</ref> In an interview, Howard Hughes Medical Institute then President [[Robert Tjian]] reflected on ''eLife'' and noted, "The other big thing is, we want to kill the journal impact factor. We tried to prevent people who do the impact factors from giving us one. They gave us one anyway a year earlier than they should have. Don't ask me what it is because I truly don't want to know and don't care."<ref>http://news.sciencemag.org/funding/2015/08/qa-outgoing-hhmi-chief-reflects-leading-18-billion-biomedical-charity</ref>

==eLife Podcast==
The ''eLife Podcast'' is produced by BBC Radio presenter and [[University of Cambridge]] consultant virologist [[Chris Smith (doctor)|Chris Smith]] of ''[[The Naked Scientists]]''.

==eLife digests==
Most research articles published in the journal include an "eLife digest", a non-technical summary of the research findings aimed at a lay audience. Since December 2014, the journal has been sharing a selection of the digests on the blog publishing platform [[Medium (service)|Medium]].<ref>{{citation |url=http://elifesciences.org/eLife-news/eLife-is-now-on-Medium |title=eLife is now on Medium |accessdate=2 March 2015}}</ref>

== Other partners ==

In April 2017, eLife was one of the founding partners in the [[Initiative for Open Citations]].<ref name="I4OC">{{cite web|url=https://i4oc.org/press.html|title=Press|date=6 April 2017|publisher=Initiative for Open Citations|accessdate=6 April 2017}}</ref>
==See also==
* [[List of open access journals]]

== References ==
{{reflist|30em}}

== External links ==
{{Commons category|Media from eLife|eLife}}
* {{Official website|http://elifesciences.org/}}
* [http://medium.com/@elife eLife on Medium]
{{Wellcome Trust}}

[[Category:English-language journals]]
[[Category:Biology journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2012]]
[[Category:General medical journals]]
[[Category:Max Planck Society]]
[[Category:Wellcome Trust]]
[[Category:Continuous journals]]