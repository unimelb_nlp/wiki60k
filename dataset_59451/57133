The '''''Georgetown Journal on Poverty Law and Policy''''' is a student-edited law review published at [[Georgetown University Law Center]] in the [[United States]].

The ''Georgetown Journal on Poverty Law and Policy'' is the "nation's premier law journal on poverty"<ref>[http://www.hg.org/journals.html Legal Journals - Law Related Journals<!-- Bot generated title -->]</ref> issues. As part of its mission to help bring an end to poverty in the United States, the Journal publishes articles from distinguished law professors and practitioners in poverty-related fields. In addition, the Journal features student research, works from scholars in poverty-related disciplines, and the "voices" of persons living in poverty.

The ''Georgetown Journal on Poverty Law and Policy'' is published three issues each year. Each issue is focused upon a particular aspect of poverty law and policy.  Recent "themes" have included rural poverty, urban poverty, former prisoner re-entry, and the impact of natural disasters such as [[Hurricane Katrina]].

The Journal was first published in 1993 as the ''Georgetown Journal on Fighting Poverty''. Volume 14, consisting of 58 [http://www.law.georgetown.edu/journals/poverty/masthead.html editors and members], is currently in publication.

The faculty advisor of the Journal is [[Peter Edelman]], long-time anti-poverty advocate and former HHS Assistant Secretary of Planning and Evaluation under President [[Bill Clinton]].

==External links==
*[http://www.law.georgetown.edu/journals/poverty/ Georgetown Journal on Poverty Law and Policy]

==References==
{{reflist}}

{{Georgetown University}}
[[Category:Georgetown University publications|Journal on Poverty Law and Policy]]
[[Category:American law journals]]
[[Category:Works about poverty]]


{{law-journal-stub}}