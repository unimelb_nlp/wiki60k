{{redirect|Legion of Boom|the album|Legion of Boom (album)}}
{{Infobox organization
| name                = Legion of Boom
| native_name         = 
| native_name_lang    = 
| named_after         = 
| image               = Legion of Boom.jpg
| image_size          = 
| alt                 = 
| caption             = The Legion of Boom includes Maxwell (#41), Lane (#20), Chancellor (#31), Sherman (#25), and Thomas (#29)
| logo                =
| logo_size           = 
| logo_alt            = 
| logo_caption        = 
| map                 = 
| map_size            = 
| map_alt             = 
| map_caption         = 
| map2                = 
| map2_size           = 
| map2_alt            = 
| map2_caption        = 
| abbreviation        = 
| motto               = 
| predecessor         = 
| merged              = 
| successor           = 
| formation           = 2011
| founder             = 
| founding_location   = 
| extinction          = <!-- use {{end date and age|YYYY|MM|DD}} -->
| merger              = 
| type                = 
| tax_id              = <!-- or | vat_id = (for European organizations) -->
| registration_id     = <!-- for non-profit org -->
| status              = 
| purpose             = 
| headquarters        = 
| location            = 
| coords              = <!-- {{coord|LAT|LON|display=inline, title}} -->
| region              = 
| services            = 
| products            = 
| methods             = 
| fields              = 
| membership          = 
| membership_year     = 
| language            = 
| owner               = <!-- or | owners = -->
| sec_gen             = 
| leader_title        = 
| leader_name         = 
| leader_title2       = 
| leader_name2        = 
| leader_title3       = 
| leader_name3        = 
| leader_title4       = 
| leader_name4        = 
| board_of_directors  = 
| key_people          = Richard Sherman, Byron Maxwell, Jeremy Lane, DeShawn Shead, Brandon Browner, Kam Chancellor, Earl Thomas
| main_organ          = 
| parent_organization = Seattle Seahawks
| subsidiaries        = 
| secessions          = 
| affiliations        = 
| budget              = 
| budget_year         = 
| revenue             = 
| revenue_year        = 
| disbursements       = 
| expenses            = 
| expenses_year       = 
| endowment           = 
| staff               = 
| staff_year          = 
| volunteers          = 
| volunteers_year     = 
| slogan              = 
| mission             = 
| website             = <!-- {{URL|example.com}} -->
| remarks             = 
| formerly            = 
| footnotes           = 
}}

The '''Legion of Boom''' is the nickname for the [[defensive backs]] group of the [[Seattle Seahawks]], a term widely popularized following their emergence in [[2011 Seattle Seahawks season|2011]]. The current starting players are outside [[cornerback]]s [[Richard Sherman (American football)|Richard Sherman]] and [[DeShawn Shead]], inside cornerback [[Jeremy Lane]], and [[Safety (American and Canadian football position)|safeties]] [[Kam Chancellor]] and [[Steven Terrell]], with injured safety [[Earl Thomas (defensive back)|Earl Thomas]] sidelined for the remainder of the 2016 season recovering from a broken leg. The group, ''aka ''''LOB'''','' quickly earned a reputation for consistently leading the [[National Football League|NFL]] in numerous defensive categories. Their earliest crowning achievement was their team's victory in [[Super Bowl XLVIII]]. Since the LOB's inception, the Seahawks defense has led the NFL in minimum points allowed, and the team has finished each season ranked #1 by [[Football Outsiders|DVOA]] metrics.

== Background ==
Before the [[2011 Seattle Seahawks season|2011 season]], the Seahawks drafted cornerback Richard Sherman in the 5th round of the [[2011 NFL Draft]] and signed cornerback [[Brandon Browner]] as a [[free agent]] from the [[Calgary Stampeders]] of the [[Canadian Football League]] (CFL), adding to [[2010 NFL Draft|2010]] draftees [[Safety (American and Canadian football position)#Free safety|free safety]] [[Earl Thomas (defensive back)|Earl Thomas]] and [[Safety (American and Canadian football position)#Strong safety|strong safety]] [[Kam Chancellor]]. They first met during the [[2011 NFL lockout]] at a [[basketball]] game against [[Jamal Crawford]], and played good team defense together from the start.<ref name="espn">{{cite web|url=http://espn.go.com/nfl/playoffs/2013/story/_/id/10259323/nfl-playoffs-seattle-secondary-comes-first |title=NFL Playoffs - In Seattle, the secondary comes first |publisher=ESPN |date=2014-01-10 |accessdate=2014-01-20}}</ref> After an injury to [[Marcus Trufant]], Sherman earned his first career start on October 30, 2011 against the [[Cincinnati Bengals]], marking the first time the four original members started a game together.<ref name="blog.seattlepi">{{cite web|last=Caple |first=Christian |url=http://blog.seattlepi.com/football/2011/10/31/richard-sherman-a-j-green-is-overrated-runs-bad-routes/ |title=Richard Sherman: A.J. Green is overrated, runs bad routes - Seattle Seahawks & NFL News |publisher=Blog.seattlepi.com |date=2011-10-31 |accessdate=2014-01-20}}</ref> The high level play of the group over the rest of the [[2011 Seattle Seahawks season|2011 season]] would inspire the nickname. [[Brandon Browner]] would leave the [[Seattle Seahawks]] and move to the [[New England Patriots]] in the 2014 season, where he eventaully won a Super Bowl before playing for New Orleans Saints. Browner briefly returned as a hybrid safety for 2016 preseason,<ref>{{Cite web|url=http://www.nfl.com/news/story/0ap3000000661025/native/brandon-browner-to-play-hybrid-safety-for-seahawks?akapp=mobile&akview=phone&akvideo=on|title=Brandon Browner to play hybrid safety for Seahawks|access-date=2016-08-29}}</ref> but was cut prior to the regular season.<ref>{{Cite web|url=http://www.nfl.com/news/story/0ap3000000692247/native/seahawks-cut-defensive-back-brandon-browner?akapp=mobile&akview=phone&akvideo=on|title=Seahawks cut defensive back Brandon Browner|access-date=2016-08-29}}</ref>

The origin of the term "Legion of Boom" is claimed to have been coined on [[KIRO (AM)|710 ESPN Seattle's]] Bob and Groz show on August 2, 2012, when Chancellor appeared on the show, noting the way the secondary "brings the boom". It is also a play on the [[Legion of Doom (DC Comics)|Legion of Doom]] supervillain group from [[DC Comics]]. A discussion regarding a nickname for the secondary arose, and the term "Legion of Boom" became widespread on [[Twitter]].<ref name="espn" /><ref name="kiroradio">{{cite web|url=http://www.kiroradio.com/listen/9962140/# |title=October 2, 2013 - Hour: 1 - Bob and Groz Show at |publisher=KIRO radio |date=2013-10-02 |accessdate=2014-01-20}}</ref> Shortly thereafter, [[Google]] searches for the term skyrocketed.<ref name="google">{{cite web|url=http://www.google.com/trends/explore#q=legion%20of%20boom%20seahawks |title=Google Trends |publisher=Google |date= |accessdate=2014-01-20}}</ref> The term became commonly used in the media by sources like [[National Football League|NFL.com]] and [[ESPN]] commentator [[Jon Gruden]].<ref name="espn" /><ref name="nfl">{{cite web|author=|url=http://www.nfl.com/videos/nfl-game-highlights/0ap2000000271715/Legion-of-Boom-denies-Rams-final-drive |title=Legion of Boom denies Rams' final drive |publisher=NFL |date=2013-10-29 |accessdate=2014-01-20}}</ref> [[Nike, Inc.|Nike]] currently offers "Legion of Boom" branded apparel as the group grows in popularity.<ref name="store.nike">{{cite web|url=http://store.nike.com/us/en_us/pd/nike-legion-of-boom-nfl-seahawks-t-shirt/pid-883443?cp=usns_kw_AL!1778!3!46106755425!!!g!!20762933025 |publisher=Nike Store |title=Nike "Legion Of Boom" (NFL Seahawks) Men's T-Shirt |date=2013-10-28 |accessdate=2014-01-20}}</ref> An ESPN feature found the "Legion of Boom" comparable to nicknamed great defenses such as the [[Monsters of the Midway]], [[Big Blue Wrecking Crew]], [[Steel Curtain]] and the [[Doomsday Defense]].<ref name="espn" />

The unit's aggressive nature is notable in an era where the NFL has laid particular emphasis on player safety. However, Seahawks defensive coordinator [[Kris Richard]], who was the defensive backs coach during the Super Bowl season, built the Legion of Boom into a unit that hits hard while staying within the rules. The receivers run routes with shields strapped to their chests that run from neck to mid-thigh, and the defensive backs are taught to keep their hits within that area in order to limit penalties for blows to the head. ''[[Bleacher Report]]'' describes the Legion of Boom as a monument both to Richard and head coach [[Pete Carroll]], who was a safety himself in his playing days and a defensive backs coach in his early coaching career.<ref name=BleacherReport>{{cite news|url=http://bleacherreport.com/articles/1927338-seattle-seahawks-legion-of-boom-is-pure-old-school-football-at-its-finest|title=Seattle Seahawks' Legion of Boom Is Pure, Old-School Football at Its Finest|last=Pompei|first=Dan|publisher=[[Bleacher Report]]|date=2014-01-17}}</ref> Carroll also studied taller cornerback tandems like [[Mike Haynes (cornerback)|Mike Haynes]] and [[Lester Hayes]] when assembling the core of defensive backs.

== Members==

=== Starters ===
* [[Richard Sherman (American football)|Richard Sherman]], at 6&nbsp;ft&nbsp;3&nbsp;in, 205&nbsp;lb, is the starting left cornerback, and most vocal member of the group, who attained national attention after the [[2013-14 NFL playoffs#NFC Championship Game: Seattle Seahawks 23, San Francisco 49ers 17|2014]] [[NFC Championship Game]] when he made a game saving pass deflection that was intercepted. After the game he conducted a highly emotional interview with [[Fox Sports (United States)|FOX Sports]]' [[Erin Andrews]]. This "rant" has become a pop culture reference{{examples|date=January 2015}}, as even [[President of the United States|U.S. President]] [[Barack Obama]] mimicked it at the [[White House Correspondents' Association#White House Correspondents' Dinner|White House Correspondents' Dinner]]{{citation needed|date=January 2015}}. Beyond Sherman's vocal personality, he is generally considered to be one of the best corners in football and was briefly the highest paid corner in football when he received his new contract from the Seahawks in 2014. Since coming into the league in 2011, Sherman has 30 regular-season interceptions (1st in NFL during that span), including 2 interceptions returned for touchdowns, 321 tackles, 89 passes defensed, 1 sack, and 5 forced fumbles. He has also recorded 2 interceptions in the playoffs (totaling 32 career interceptions).
* [[Kam Chancellor]], at 6&nbsp;ft&nbsp;3&nbsp;in, 232&nbsp;lb, is the tallest and heaviest safety in the NFL and is known for his hard-hitting tackles.<ref name="sports.yahoo">{{cite web|last=McIntyre |first=Brian |url=https://sports.yahoo.com/blogs/nfl-shutdown-corner/seahawks-sign-safety-kam-chancellor-four-extension-202508799--nfl.html |title=Seahawks sign safety Kam Chancellor to four-year extension &#124; Shutdown Corner |publisher=Yahoo Sports |date=2013-04-22 |accessdate=2014-01-20}}</ref> He was taken with the 133rd overall pick in the 5th round of the [[2010 NFL Draft]].<ref>http://www.nfl.com/draft/history/fulldraft?season=2010#round5</ref> During the Legion of Boom's run (2011–present), strong-safety Chancellor has recorded 529 tackles, 12 interceptions, 41 passes defensed, 2 sacks, and 8 forced fumbles. Over that same span, he also has 3 postseason interceptions, including 1 returned for a touchdown. 
* [[Earl Thomas (defensive back)|Earl Thomas]] is the shortest member of the Legion of Boom at 5'10", 202&nbsp;lb.<ref name="seahawks">{{cite web|author=Earl Thomas |url=http://www.seahawks.com/team/roster/earl-thomas/2b7944eb-8876-431d-9149-f29a6e51e5be |title=Seattle Seahawks: Earl Thomas |publisher=Seahawks |date= |accessdate=2014-01-20}}</ref> He was described by ''[[Sports Illustrated]]'' as "a great cover safety from anywhere to anywhere on the field, able to take ridiculous angles and read plays with microscopic precision".<ref name="nfl_a">{{cite web|last=Farrar |first=Doug |url=http://nfl.si.com/2013/11/01/earl-thomas-seattle-seahawks-best-safety/ |title=The All-22: Closing speed makes Seahawks' Earl Thomas the NFL's best safety |publisher=Sports Illustrated |date=2013-11-01 |accessdate=2014-01-20}}</ref> He was selected with the 14th overall pick in the 1st round of the 2010 NFL Draft.<ref name="pro-football-reference_c">{{cite web|url=http://www.pro-football-reference.com/years/2010/draft.htm |title=2010 NFL Draft Listing |publisher=Pro-Football-Reference.com |date= |accessdate=2014-01-20}}</ref> From 2011–present, free-safety Earl Thomas has 554 total tackles, 23 interceptions, including 1 interception returned for a touchdown, 56 passes defensed, 9 forced fumbles, and 1 fumble recovery returned for a touchdown. Thomas also has 2 interceptions in the playoffs (totaling 25 career interceptions). Earl Thomas suffered a season-ending injury during a game on December 4, 2016, breaking his leg in a mid-air collision with teammate Chancellor.
* [[DeShawn Shead]], at 6&nbsp;ft&nbsp;2&nbsp;in, 220&nbsp;lb, signed with the Seahawks as an undrafted rookie free-agent in 2012. Shead, a collegiate [[Decathlon]] [http://www.athletic.net/TrackAndField/Athlete.aspx?AID=676027#/L0 star], is a special-teams standout who excelled as the team's '[[Swiss Army knife|Swiss-Army Knife]]' multipurpose [[defensive back]], and became the starter at right cornerback, opposite Sherman. He is a fast-and-physical player with elite body control, who displays his prowess intuitively as a quick and reliable tackler against the run, and as a solid pass defender with excellent speed downfield. Shead demonstrates uncanny athleticism, and has a knack for stripping the ball and forcing fumbles. When utilized as an additional weak-side 'bandit/hybrid' safety, or as a 'big nickel' defensive back, Shead matches up effectively against taller, larger and more physical receivers and tight ends. During the 2015 season, Shead started games at five different positions, designated as the primary backup at free- and strong-safety positions, both outside cornerback positions, and inside at [[Nickelback (gridiron football)|nickelback]], and ultimately earned the starting job at right cornerback. From 2012–present, in the regular season, DeShawn Shead has produced 132 tackles, 21 passes defensed, 1 sack, 2 interceptions, and 3 forced fumbles.
* [[Jeremy Lane]], at 6&nbsp;ft&nbsp;0&nbsp;in, 190&nbsp;lb, was drafted by the Seahawks in 2012, number 172 overall (6th round). Lane is a premier inside nickelback, possessing elite speed, quickness, and shadowing ability in coverage. His speed is well respected; he was one of the most dangerous [[Gunner (American football)|gunners]] in special teams coverage, often double-teamed and deliberately forced out-of-bounds. Lane's skills as a defensive back afford him advantages against smaller, quicker slot receivers and deep threats. From 2012–present, in the regular season, Jeremy Lane has 104 tackles, 14 passes defensed, 2 interceptions, and 1 forced fumble. Lane also recorded an interception in Super Bowl XLIX.
* [[Steven Terrell]], at 5&nbsp;ft&nbsp;10&nbsp;in, 197&nbsp;lb, an undrafted free agent from the 2013 NFL Draft, had spent time on both the Jaguars and Texans practice squads before being signed by the Seattle Seahawks on July 26, 2014. He was subsequently released and signed to the practice squad on August 30–31, 2014, and promoted to the active roster on October 18, 2014. He became the primary backup to Earl Thomas at free-safety, while adding depth at inside nickelback. Comparable to Thomas, Steven Terrell's athletic measurables<ref>{{Cite web|url=http://www.fieldgulls.com/nfl-offseason-2014/2014/7/27/5942577/seahawks-training-camp-notes-sparq-russell-wilson-christine-michael|title=SPARQ}}</ref> are elite, which he showcases as a core player on special teams. His sideline-to-sideline coverage range and quick bursts to top speed are punctuated by superior leaping ability. Terrell assumed full-time starting duties upon Thomas' season-ending injury on December 4, 2016. Since 2014, in the regular season, Steven Terrell has posted 19 tackles, and 2 passes defensed. Terrell was also active for Super Bowl XLIX.

=== Backups ===
The other defensive backs on the Seahawks roster are also considered members of the Legion of Boom. Particularly following the suspensions of Brandon Browner in 2012 (four games)<ref><span id="eow-title" class="watch-title long-title yt-uix-expander-head" title="Richard Sherman, Brandon Browner sit down with Michael Irvin on NFL Gameday - SightsNSounds" dir="ltr">[https://www.youtube.com/watch?v=fbvE6M5oVCE Richard Sherman, Brandon Browner sit down with Michael Irvin on NFL Gameday] - SightsNSounds</span></ref> and 2013 (suspended indefinitely, but reinstated March 4, 2014), as well of that of Sherman (which he successfully appealed in December 2012), the term "Legion of Boom" has encompassed more than just the four original starters.<ref name="espn" /><ref name="seattletimes">{{cite web|url=http://seattletimes.com/html/larrystone/2021755998_stone05xml.html|title=Seahawks' 'Legion of Boom' personifies team's mission |date=|newspaper=[[The Seattle Times]] |author=Larry Stone|accessdate=2014-01-20}}</ref> At the Super Bowl parade in Seattle, Sherman called the Legion of Boom, "more than the secondary, it's the linebackers, the defensive line, the entire defense."

The 2013 backups during their [[Super Bowl XLVIII]]-winning year were: [[Byron Maxwell]] (replacing injured starter Brandon Browner), Jeremy Lane, [[Walter Thurmond]], [[Jeron Johnson]] and DeShawn Shead.

The 2014 backups during their subsequent [[Super Bowl XLIX]] year were: Jeremy Lane, [[Jeron Johnson]], [[Tharold Simon]], [[Marcus Burley]], [[Steven Terrell]] and DeShawn Shead.

After the 2014 season, Richard Sherman expressed his approval of two new Legion of Boom members:<ref>{{Cite web|url=http://www.nfl.com/news/story/0ap3000000498132/article/richard-sherman-breaks-down-newest-lob-members|title=Richard Sherman breaks down newest LOB members |publisher=NFL |accessdate=2015-06-23}}</ref> rookie [[Tye Smith]], and veteran [[Cary Williams]] (who was subsequently released by the Seahawks after starting 10 games in the 2015 season).<ref>{{Cite web|url=http://www.nfl.com/news/story/0ap3000000596660/article/seahawks-cut-cornerback-cary-williams|title='Seahawks Cut Cornerback Cary Williams' |publisher=NFL |accessdate=2015-12-07}}</ref>

Backups during the 2015 season were: DeShawn Shead, Jeremy Lane, [[Marcus Burley]], [[Steven Terrell]] and [[Kelcie McCray]]. DeShawn Shead emerged as the starter at right cornerback.

The 2016 backups are:<nowiki/> veterans [[Kelcie McCray]] and [[Steven Terrell]], newcomers [[Dewey McDonald]] and [[Neiko Thorpe]], and rookies [[DeAndre Elliott|DeAndre Elliot]] and [[Tyvis Powell]]. Terrell advanced to full-time starter at free-safety after Earl Thomas suffered a broken leg during a game.

== Accomplishments ==
Following the [[2011 NFL season|2011 season]], Thomas, Chancellor, and Browner were named to the [[2012 Pro Bowl]]. Thomas also earned [[All-Pro|AP All-Pro]] honors. Browner tied for fourth in the NFL with six interceptions.<ref name="espn_b">{{cite web|url=http://espn.go.com/nfl/statistics/_/year/2011 |title=2011 NFL Statistics - National Football League |publisher=ESPN |date= |accessdate=2014-01-20}}</ref> In every year thereafter, at least three members of the "Legion of Boom" have been named either AP All-Pro or voted to the Pro Bowl.

After the [[2012 NFL season|2012 season]], both Sherman and Thomas were named AP All-Pro. Sherman finished second in the league with eight interceptions.<ref name="espn_a">{{cite web|url=http://espn.go.com/nfl/statistics/_/year/2012 |title=2012 NFL Statistics - National Football League |publisher=ESPN |date= |accessdate=2014-01-20}}</ref> Additionally, the team defense finished first in points allowed, and second in passing touchdowns allowed.<ref name="pro-football-reference">{{cite web|url=http://www.pro-football-reference.com/teams/sea/2012.htm |title=2012 Seattle Seahawks Statistics & Players |publisher=Pro-Football-Reference.com |date= |accessdate=2014-01-20}}</ref>

The Legion of Boom had a banner year in [[2013 NFL season|2013]]. In the regular season, they allowed the fewest passing yards and passing touchdowns in the league<ref name=BleacherReport/> while anchoring the league's best passing defense as well as overall defense. Sherman finished first in the league with eight interceptions.<ref name="pro-football-reference_a">{{cite web|url=http://www.pro-football-reference.com/teams/sea/2013.htm |title=2013 Seattle Seahawks Statistics & Players |publisher=Pro-Football-Reference.com |date= |accessdate=2014-01-20}}</ref> Seattle finished the season with the most interceptions in the NFL.<ref name="pro-football-reference_b">{{cite web|url=http://www.pro-football-reference.com/years/2013/opp.htm |title=2013 NFL Opposition & Defensive Statistics |publisher=Pro-Football-Reference.com |accessdate=2014-01-20}}</ref> Sherman and Thomas were named first-team AP All-Pro, while Chancellor was named to the second team.<ref name="profootballtalk.nbcsports">{{cite web|url=http://profootballtalk.nbcsports.com/2014/01/03/manning-sherman-and-mathis-among-ap-first-team-all-pros/ |title=Manning, Sherman and Mathis among AP first team All-Pros |publisher=ProFootballTalk |accessdate=2014-01-20}}</ref> Sherman, Chancellor, and Thomas were also named to the [[2014 Pro Bowl]], but did not play due to the Seahawks playing in [[Super Bowl XLVIII]]. In their rout of the [[Denver Broncos]], they held [[Peyton Manning]] and the record-setting Broncos offense to only eight points, intercepting Manning twice, forcing two fumbles, and playing a major part in securing the first Super Bowl championship in franchise history.

On June 6, 2014, Richard Sherman won the ''[[Madden NFL 15]]'' cover vote against [[Carolina Panthers]] quarterback [[Cam Newton]].<ref name=EAsports>{{cite web|title=Richard Sherman Wins Madden NFL 15 Cover Vote|url=http://www.easports.com/madden-nfl/news/2014/madden-15-cover-vote |publisher=EA Sports|accessdate=5 September 2014|ref=EAsports-sherman}}</ref>

During the [[2014 NFL season|2014 season]], the Legion of Boom helped their team reach their [[Super Bowl XLIX|second straight Super Bowl]], which ended in a loss to the [[New England Patriots]], 28-24. Thomas, Chancellor, and Sherman were selected to the [[2015 Pro Bowl]], marking the second year in a row in which all three were named to the Pro Bowl together. Thomas and Sherman were again named first-tt 9 points and 339 yards, coming up with a last-second goal line stand to seal the victory 14-9. Sherman intercepted a pass and returned it 38 yards to set up a touchdown, and [[Walter Thurmond]] recorded a sack.

In a week 10 revenge game against the [[Atlanta Falcons]], Seattle allowed 10 points and 226 yards in a 33-10 win. Thurmond would force and recover a fumble in the win.

Week 11 brought the [[Minnesota Vikings]] to Seattle, who managed 336 yards and 20 points, but turned it over 4 times. Thurmond returned an interception 29 yards for a touchdown in the 41-20 win.

In week 13, in a [[Monday Night Football]] game against the [[New Orleans Saints]], the Seahawks faced off against the eventual #2 ranked passing attack in the league. They held the Saints to 188 yards in a 34-7 blowout.

In week 15, the Seahawks shut the [[New York Giants]] out, winning 23-0. It was one of the best games of the season for the Legion of Boom, the Giants managing just 181 yards of offense and turning it over 5 times. Byron Maxwell had two interceptions, as did Richard Sherman. Sherman also tipped a pass to Earl Thomas for an interception. Sherman also recovered a fumble.

The next week, Seattle kept the [[Arizona Cardinals]] to 168 total yards and 17 points, forcing 4 turnovers. Sherman had two more interceptions and Chancellor added another. However, the Seahawks would fall 17-10, a late 31-yard touchdown pass from [[Carson Palmer]] to [[Michael Floyd]] being the difference.

They would close the regular season strong, beating the [[St. Louis Rams]] 27-9 and allowing 158 total yards. Byron Maxwell would add an interception and the Rams would not score a touchdown until 4:18 remained in the game, with the game already decided.

In the divisional round, a rematch with the [[New Orleans Saints]], the Saints did not score until early in the 4th quarter, although a late flurry allowed them to get to 15 points and 409 offensive yards. Of note was the Seahawks holding tight end [[Jimmy Graham]] to 1 catch for 8 yards. The Seahawks would win 23-15.

For the third time that season the Seahawks played the [[San Francisco 49ers]]. The Niners scored 17 points and gained 308 total yards, but Seattle survived 23-17 after forcing turnovers on all three of the 49ers 4th quarter possessions. Kam Chancellor picked off a pass to set up a field goal, and Richard Sherman would tip a pass to [[Malcolm Smith (American football)|Malcolm Smith]] to clinch the game with less than 30 seconds left. This play and the immediate aftermath became infamous, for a brief shoving match between Sherman and 49ers Wide Receiver [[Michael Crabtree]], Sherman's choke sign at QB [[Colin Kaepernick]] that he was later fined for, and his post-game rant in an interview with [[Erin Andrews]].

The Seahawks saved their best for last with their performance in [[Super Bowl XLVIII]], holding the [[Denver Broncos]] record-breaking offense to 306 yards and 8 points, forcing 4 turnovers and not allowing a score until the final play of the 3rd quarter. Kam Chancellor intercepted one pass and Byron Maxwell forced a fumble.

The 2013 Seahawks allowed just 1 300-yard passer, [[Matt Schaub]] in week 5, allowing a second in the divisional round against [[Drew Brees]]. 11 times in the regular season they held an opposing quarterback to less than 200 yards passing, then did it once more in the NFC Championship game. Their 28 team interceptions were most in the league, and 16 passing touchdowns allowed second fewest.<ref>http://www.pro-football-reference.com/teams/sea/2013.htm</ref>

===2014 season===

The Seahawks again allowed the fewest points in the NFL, the first NFL defense to accomplish this three seasons in a row since the 1969–1971 [[Minnesota Vikings]].<ref>http://sea.247sports.com/Article/Unit-Study-Breaking-down-the-Seattle-Seahawks-defense-34540982</ref> They also allowed the fewest yards, becoming the first defense since the 1985–1986 [[Chicago Bears]] to allow the fewest points and yards two seasons in a row. They allowed the fewest passing yards and passing touchdowns, though they ranked 20th in interceptions.

In week 1, the [[Green Bay Packers]] scored 16 points and gained 255 yards in a 36–16 victory by the Seahawks. Byron Maxwell, now the full-time starter at left corner with Brandon Browner's departure, intercepted a pass in the game.

In week 3, a rematch against the [[Denver Broncos]], Seattle allowed 332 yards and 20 points, eventually winning 26–20 in overtime. Kam Chancellor forced a fumble and intercepted a pass.

In week 8, another trip to play the [[Carolina Panthers]] saw the Seattle defense allow 9 points and 266 yards, just enough for the 13–9 win. [[Marcus Burley]], a 5th corner forced into action due to injuries, was the key member of the Legion on the day with an interception and 24-yard return.

In week 9, the visiting [[Oakland Raiders]] scored 17 points on offense with 226 yards of total offense. Sherman intercepted his first pass of the season and also forced a fumble.

In week 12, with the season on the line, the Seahawks beat the [[Arizona Cardinals]] 19–3. Allowing 3 points and 204 yards, the game featured a Byron Maxwell interception and Jeremy Lane forced fumble. This game began a six-game hot-streak for the Seahawks, particularly on defense, which would give up an average of 6.5 points for the remainder of the year and allow fewer than 250 total yards in all six games.

In week 13, they allowed 3 points and 164 total yards in another 19–3 victory, this against the [[San Francisco 49ers]]. Sherman had two interceptions.

In week 14, against a [[Philadelphia Eagles]] offense that would eventually rank third in the league in points and fifth in yards, the Seahawks gave up 139 yards and 14 points. [[Tharold Simon]] picked off his first pass of the season and Earl Thomas recovered a fumble. The Seahawks won, 24–14.

In another tilt against the 49ers in week 15, San Francisco had 245 yards and 7 points in the 17–7 loss.

In a game that would likely decide the NFC West division, the Seahawks traveled to play the [[Arizona Cardinals]]. The Seahawks won 35–6, holding Arizona to 216 yards and the 6 points. Sherman grabbed the only turnover of the game, intercepting a pass and returning it 53 yards.

To close out their run, the Seahawks beat the [[St. Louis Rams]] 20–6, giving up 245 yards. The game was sealed on an Earl Thomas forced fumble at the goal line.

==== 2014 postseason ====
Seattle surrendered 362 yards in the divisional round game against the [[Carolina Panthers]], as well as 17 points. Kam Chancellor had the game-sealing interception and took it back 90 yards for a touchdown. He also twice leaped over the center on field goal tries in order to block the field goal, although he missed the ball both times and both plays were whistled dead by a penalty. Richard Sherman recorded an interception as well, and Earl Thomas forced a fumble. Seattle won, 31–17.

In a rematch against the [[Green Bay Packers]], Green Bay scored 22 points and gained 308 yards. Sherman and Maxwell each recorded an interception. The Seahawks offense and special teams turned the ball over five times in the game, but the defense forced overtime, and the 28–22 victory sent the Seahawks to a [[Super Bowl XLIX|second straight Super Bowl]], where their season ended in a loss to the [[New England Patriots]], 28-24.

The 2014 Seahawks defense did not allow a single 300-yard passer in any game, regular season or postseason, heading into the Super Bowl. Ten times they held the opposing quarterback to under 200 yards passing, doing it an 11th time against MVP [[Aaron Rodgers]] in the NFC Championship game.<ref>http://www.pro-football-reference.com/teams/sea/2014.htm</ref>

Starting nickelback [[Jeremy Lane]] suffered a gruesome injury during [[Super Bowl XLIX|Superbowl XLIX]]. After intercepting Tom Brady's pass in the end-zone and attempting to return the ball downfield, Lane was tackled awkwardly while attempting to brace himself for impact with the ground, severely fracturing his wrist and forearm, while simultaneously damaging the [[Anterior cruciate ligament injury|ACL]] in his knee. Unfortunately, backup nickelback [[Marcus Burley]] had been listed as inactive, so Lane was replaced by backup outside cornerback [[Tharold Simon]] for the remainder of the game. Simon, seemingly overmatched, was outplayed by slot receiver Julian Edelman and repeatedly targeted by Tom Brady, fostering an unlikely Patriots' 4th-quarter comeback victory.

Their defeat in [[Super Bowl XLIX]] was the first time all season that the Legion of Boom allowed a 300-yard passer. It was only the second time in the [[2014 Seattle Seahawks season|2014 season]] that they allowed more than two passing touchdowns in one game, and the only time they allowed four passing touchdowns. The Legion of Boom has given up 300 or more passing yards in only six games since their inception with Richard Sherman's first start in 2011; of those six, two are their games against the [[New England Patriots]].

===2015 season===

The Legion of Boom had a rocky start to the 2015 season, both on and off the field. Kam Chancellor did not attend training camp and missed the entire preseason, holding out in an effort to get the team to renegotiate his contract, which had three seasons remaining. The holdout continued into the regular season, with Chancellor not being a part of the team for the first two games. The Seahawks would lose both games, the defense allowing 27 points in both games. [[Dion Bailey]], replacing Chancellor at Strong Safety in Week 1, fell down in coverage and allowed the game-tying touchdown with less than a minute left in the fourth quarter.

Chancellor would bring the Legion back together in week 3 by ending his holdout without any contract concessions from the team. In that Week 3 game, the Seahawks surrendered 0 points and 146 total offensive yards.

The next week, the Seahawks defense allowed 3 points and 256 yards against the Lions. With less than two minutes remaining in the game and the Seahawks leading 13-10, Lions Wide Receiver [[Calvin Johnson]] caught a pass and was preparing to leap into the endzone for the go-ahead touchdown, but Kam Chancellor punched the ball out at the one-yard line, the forced fumble ultimately preserving the win. The play garnered more attention for what happened after the fumble, with [[KJ Wright]] illegally batting the ball out of the back of the endzone for a touchback, which should have been a penalty that would have kept the ball in the possession of the Lions.

Against the [[Cincinnati Bengals]] in week 5, the LOB recorded an interception from [[Andy Dalton]], the first interception by the Seahawks in the 2015 season.<ref>http://www.cbssports.com/nfl/eye-on-football/25335566/michael-bennett-went-after-andy-dalton-aggressively-following-earl-thomas-int</ref> In week 6 against the [[Carolina Panthers]], both Earl Thomas and Kam Chancellor recorded interceptions. Both games, however, would end in close losses for the Seahawks.

In week 7, playing the [[San Francisco 49ers]] on the road, the Seahawks defense allowed just 142 offensive yards (the 49ers lowest output in a game since 2006)<ref>http://www.heraldnet.com/article/20151022/SPORTS/151029571</ref> in a 20-3 victory. The following week, the Seahawks held the Dallas Cowboys to 220 total yards and 12 points on four field goals in a 13-12 victory.

Despite allowing a Seahawk's opponents record 456 passing yards to Pittsburgh Steelers' quarterback [[Ben Roethlisberger]], the Legion of Boom recorded 4 interceptions (their most since [[Super Bowl XLVIII]]) in a crucial week 12 game in Seattle. In addition, they held star wide receiver [[Antonio Brown]] to just 6 catches for 51 yards.<ref>http://www.pro-football-reference.com/boxscores/201511290sea.htm</ref> The turnovers, especially the two by Sherman, would help lead Seattle to a 39-30 win.<ref>http://www.nfl.com/gamecenter/2015112910/2015/REG12/steelers@seahawks#menu=gameinfo%7CcontentId%3A0ap3000000592162&tab=recap</ref> In week 17, to close out another dominant year, the Legion of Boom dismantled the high-powered Arizona Cardinals offense in a 36-6 win.<ref>{{Cite web|title = Seattle Seahawks at Arizona Cardinals - January 3rd, 2016 |publisher=Pro-Football-Reference |url = http://www.pro-football-reference.com/boxscores/201601030crd.htm |accessdate = 2016-01-06}}</ref>  They held Arizona to only 236 yards and recorded 3 interceptions. It was the only time all year that the Cardinals would score fewer than 20 points and gain fewer than 350 yards at home.

Although they weren't quite as dominant defensively as they were the last 3 years, 2015 marked the fourth straight season that the Seahawks led the NFL in points allowed, averaging 17.3 points per game.<ref>{{Cite web|title = Seattle Seahawks defense leads NFL in fewest points allowed for 4th year in row|url = http://www.seattletimes.com/sports/seahawks/seahawks-defense-leads-nfl-in-points-allowed-for-4th-year-in-row/|newspaper = The Seattle Times|date = 2016-01-03|accessdate = 2016-01-06}}</ref> The Legion of Boom also held opponents to the second-fewest passing yards and the fewest passing touchdowns of any defensive secondary in the NFL.<ref>{{Cite web|title = 2015 NFL Opposition & Defensive Statistics |publisher=Pro-Football-Reference.com|url = http://www.pro-football-reference.com/years/2015/opp.htm |accessdate = 2016-01-06}}</ref> According to [[Football Outsiders]]' proprietary DVOA metric,<ref>{{Cite web |publisher = Football Outsiders |title=Innovative Statistics, Intelligent Analysis {{!}} Methods To Our Madness|url = http://www.footballoutsiders.com/info/methods |accessdate = 2016-01-06}}</ref> Seattle's defense was ranked third against the pass and fourth overall,<ref>{{Cite web |publisher = Football Outsiders |title = Innovative Statistics, Intelligent Analysis {{!}} 2015 Defense efficiency ratings |url = http://www.footballoutsiders.com/stats/teamdef |accessdate = 2016-01-06}}</ref> their lowest rankings in those respective categories since 2011.

===2016 season===

== References ==
{{reflist|30em}}

{{Portalbar|National Football League|American football}}
{{Seattle Seahawks}}

[[Category:Seattle Seahawks]]
[[Category:Sports in Seattle]]
[[Category:Nicknamed groups of American football players]]