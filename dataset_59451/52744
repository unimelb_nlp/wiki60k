{{Infobox journal
| title = Review of Educational Research
| cover = [[File:Review_of_Educational_Research_Cover.gif]]
| editor = Frank Worrell
| discipline = [[Education studies]]
| abbreviation = Rev. Educ. Res.
| publisher = [[Sage Publications]] on behalf of the [[American Educational Research Association]]
| country =
| frequency = Quarterly
| history = 1931-present
| openaccess =
| license =
| impact = 3.897
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201854/title
| link1 = http://rer.sagepub.com/content/current
| link1-name = Online access
| link2 = http://rer.sagepub.com/content
| link2-name = Online archive
| link3= http://www.aera.net/Publications/Journals/ReviewofEducationalResearch/tabid/12611/Default.aspx
| link3-name = Journal page at association's website
| JSTOR =
| OCLC = 1588319
| LCCN = 33019994
| CODEN = REDRAB
| ISSN = 0034-6543
| eISSN =1935-1046
}}
The '''''Review of Educational Research''''' is a quarterly [[peer-reviewed]] [[review journal]] published by [[Sage Publications]] on behalf of the [[American Educational Research Association]]. It was established in 1931 and covers all aspects of [[education]] and [[educational research]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Academic Search Premier]], [[EBSCO Publishing|EBSCO databases]], [[Current Contents]]/Social & Behavioral Sciences, [[ProQuest|ProQuest Education Journals]], [[PsycINFO]], [[SafetyLit]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.897, ranking it first out of 224 journals in the category "Education & Educational Research".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Education & Educational Research |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201854/title}}


[[Category:Education journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1931]]
[[Category:English-language journals]]
[[Category:SAGE Publications academic journals]]