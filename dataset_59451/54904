{{Politics of Abkhazia}}

'''Presidential elections''' were held in [[Abkhazia]] on 3 October 1999, alongside a [[Abkhazian constitutional referendum, 1999|constitutional referendum]]. They were the first direct presidential elections in Abkhazia, and resulted in a victory for incumbent President [[Vladislav Ardzinba]], who ran unopposed.<ref name=delayed>{{cite book |title= Georgia from national awakening to Rose Revolution: delayed transition in the former Soviet Union |last= Wheatley |first= Jonathan |year= 2005 |publisher= Ashgate Publishing, Ltd |isbn= 0-7546-4503-7 |page= 121 |url= https://books.google.com/books?id=80T4YjHFxycC&source=gbs_navlinks_s}}</ref>

==Background==
The previous [[Abkhazian presidential election, 1994|presidential elections]] in 1994 had been indirect, with Ardzinba elected by the [[People's Assembly of Abkhazia|People's Assembly]].

In August 1999 the Central Election Commission (CEC) headed by [[Viacheslav Tsugba]] set-up 28 electoral districts for the purpose of the elections.<ref name=sarke990809>{{cite news|title=Daily News August 9, 1999|url=http://sarke.com/daily.asp?IssueID=1&Day=09&Month=8&Year=1999|accessdate=7 January 2012|newspaper=Sarke|date=9 August 1999}}</ref> 

==Campaign==
Incumbent President Ardzinba  was nominated by the national-patriotic movement [[Apsny (political party)|Apsny]],<ref name=sarke990811>{{cite news|title=Daily News August 11, 1999|url=http://sarke.com/daily.asp?IssueID=1&Day=11&Month=8&Year=1999|accessdate=7 January 2012|newspaper=Sarke|date=11 August 1999}}</ref> and also received the support of the [[Communist Party of Abkhazia|Communist Party]]<ref name=sarke990816>{{cite news|title=Daily News August 16, 1999|url=http://sarke.com/daily.asp?IssueID=1&Day=16&Month=8&Year=1999|accessdate=7 January 2012|newspaper=Sarke|date=16 August 1999}}</ref> and the Abkhaz diaspora in [[Turkey]].<ref name=sarke991001>{{cite news|title=Daily News October 1, 1999|url=http://sarke.com/daily.asp?IssueID=1&Day=01&Month=10&Year=1999|accessdate=7 January 2012|newspaper=Sarke|date=1 October 1999}}</ref>

On 2 September, the CEC registered the nomination of former [[Minister for Foreign Affairs of Abkhazia|Foreign Minister]] [[Leonid Lakerbaia]] by his [[People's Party of Abkhazia|People's Party]].<ref name=sarke990903>{{cite news|title=Daily News September 3, 1999|url=http://sarke.com/daily.asp?IssueID=1&Day=03&Month=9&Year=1999|accessdate=7 January 2012|newspaper=Sarke|date=3 September 1999}}</ref> However, his candidacy was not approved.<ref>{{cite news|last=Sharia|first=Vitali|title=Как в Абхазии президентов выбирали|url=http://www.ekhokavkaza.com/content/article/24318849.html|accessdate=25 September 2011|newspaper=Ekho Kavkaza|date=5 September 2011}}</ref> The CEC also denied registration to [[Yahya Kazan]], who had been Abkhazia's representative in the [[United States]], on the grounds that he had not lived in Abkhazia for the previous five years and that he did not have a working command of the [[Abkhaz language]].<ref name=ng_alternate>{{cite news|last1=Tesemnikova|first1=Ekaterina|title=Тбилиси ищет альтернативу Ардзинбе|url=http://www.ng.ru/cis/2001-07-07/1_alternate.html|accessdate=5 October 2014|publisher=Независимая газета|date=7 July 2001}}</ref>

There were also rumours that former Vice Chairman [[Zurab Achba]] of [[Aidgylara]] would run for president, which he dismissed in an interview with ''[[Nuzhnaya Gazeta]]'' as a "nightmare of an idea".<ref name=sarke990813>{{cite news|title=Daily News August 13, 1999|url=http://sarke.com/daily.asp?IssueID=1&Day=13&Month=8&Year=1999|accessdate=7 January 2012|newspaper=Sarke|date=13 August 1999}}</ref>

==Results==
{| class=wikitable style=text-align:right
!Candidate
!Running mate
!Party
!Votes
!%
|-
|align=left|[[Vladislav Ardzinba]]||align=left|[[Valery Arshba|Valeri Arshba]]||align=left|[[Apsny (political party)|Apsny]]||186,792||99.11
|-
|align=left colspan=3|Against all||1,683||0.89
|-
|align=left colspan=3|Invalid/blank votes||129||–
|-
|align=left colspan=3|'''Total'''||'''188,604'''||'''100'''
|-
|align=left colspan=3|Registered voters/turnout||214,503||87.93
|-
|align=left colspan=5|Source: [http://lenta.ru/world/1999/10/04/abhazia Lenta.ru]
|}

==References==
{{reflist}}

{{Abkhazian elections}}

[[Category:1999 elections in Europe]]
[[Category:Abkhazian presidential election, 1999| ]]


{{Abkhazia-poli-stub}}
{{Georgia-election-stub}}