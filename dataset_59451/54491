{{infobox book | 
| name         = A New Alice in the Old Wonderland
| image        = File:New-alice-lippincott-1895.jpg
| caption      = Cover of the first edition
| author       = Anna M. Richards
| illustrator  = [[Anna Richards Brewster|Anna M. Richards, Jr.]]
| cover_artist = 
| country      = United States

| series       = 
| genre        = [[Fantasy novel]]
| publisher    = [[Lippincott Williams & Wilkins|J. B. Lippincott]]
| pub_date     = October 1895
| media_type   = Print ([[hardcover]])
| pages        = 309
| oclc         = 531268
| congress     = PZ8.R363 Ne<ref>[https://lccn.loc.gov/36029331 "A new Alice in the old Wonderland"]. LC Online Catalog. Library of Congress. Retrieved 2016-08-03.</ref>
}}

'''''A New Alice in the Old Wonderland'''''  is a fantasy novel written by Anna M. Richards, illustrated by [[Anna Richards Brewster|Anna M. Richards Jr.]], and published in 1895 by J. B. Lippincott of Philadelphia. According to Carolyn Sigler, it is one of the more important "Alice imitations", or novels inspired by [[Lewis Carroll]]'s ''Alice'' books.<ref>Sigler, Carolyn, ed. (October 1997). ''Alternative Alices: Visions and Revisions of Lewis Carroll's "Alice" Books: An Anthology.'' Lexington, KY: University Press of Kentucky. ISBN 0-8131-2028-4. {{LCCN|97002205}}</ref>

''A New Alice'' features Alice Lee, an American girl with a coincidental name, visiting Wonderland and meeting all the characters she knows from ''[[Alice's Adventures in Wonderland]]'' and ''[[Through the Looking-Glass]]''. 
It is illustrated with 67 drawings by the writer's daughter, closely "after" the originals by [[John Tenniel]].

The Preface, pp. 5–6 in the first edition, is illustrated by a drawing of three children and signed "A. M. R., Sr." A rhyming poem in four stanzas, it indicates that the story originated years ago when "unreasonable children three" would accept nothing but the Wonderland of Alice. Evidently Anna M. Richards was the mother, who gave them more of what they knew and loved. Now grown up, one of them made the pictures. With apology to Tenniel and Carroll ("We're not original, nor wise, nor witty"), they plead for mercy from the Critic.<ref>Richards, Anna M. (1895). [https://archive.org/stream/anewaliceinoldw00richgoog#page/n13/mode/2up "Preface"]. First edition, e-copy at the Internet Archive. Retrieved 2016-08-03.</ref>

[[File:New-alice.png|160px|left|thumb| Cover of a recent edition (Evertype Publishing, 2009) ]]
The 2009 Evertype edition credits mother and daughter under their full names Alice Matlack Richards (1835–1900) and [[Anna Richards Brewster]]. Brewster, whose given middle name was Mary, not Matlack, became a renowned artist under her married name.

According to Carolyn Sigler, Anna Matlack was already known as a poet and playwright before she married William Trost Richards in 1856, age 20 or 21. They had eight children. In the 1890s she wrote comic poems that were published in American children's magazines. ''A New Alice'' was "an expanded version of the stories she had invented for her young children, to whom the novel is dedicated, about their favorite storybook world." A second edition was published in 1896. Sigler interpolates, "Alice Lee—Richards's protagonist as well as her daughter, who is fictionalized in the text". (Sigler, 1996, pp. 56–57).

==Bibliography==

'''Publication history'''
* Richards, Anna M. (1895) ''A New Alice in the Old Wonderland''. Lippincott.
*Richards, Anna M. (2000) ''A New Alice in the Old Wonderland''. Wildside Press. ISBN 1-58715-199-5
*Richards, Anna Matlack (2009) [http://www.evertype.com/books/new-alice.html ''A New Alice in the Old Wonderland'']. Cathair na Mart, County Mayo, Ireland: Evertype Publishing (www.evertype.com). Edited by [[Michael Everson]]. Foreword by Everson. ISBN 978-1-904808-35-0

There are multiple new editions since 2009. 

'''About ''A New Alice'''''
* Sigler, Carolyn (1996). "Brave New Alice: Anna Matlack Richards's Maternal Wonderland". ''Children's Literature'' 24, pp. 55–73. [https://muse.jhu.edu/article/246338 Reprint] at Project Muse (muse.jhu.edu).

==References==
{{reflist}}

==External links==
{{Portal |Children's literature }}
* [http://www.arbrewster.com/ Anna Richards Brewster] – official website of the traveling exhibition ''Anna Richards Brewster, American Impressionist'' (?)
* [https://archive.org/details/anewaliceinoldw00richgoog Digital copy] by Google at the [[Internet Archive]] (archive.org)
* {{librivox book | title=A New Alice in the Old Wonderland | author=Anna Matlack RICHARDS}}
* {{LCAuth|n86864850|Anna M. Richards|4|}}
* {{worldcat id|lccn-n86-864850|Anna M. Richards}} – evidently includes works by a distinct A. M. Richards and at least one later Anna M. Richards

{{alice}}

{{DEFAULTSORT:New Alice in the Old Wonderland}}
[[Category:1895 novels]]
[[Category:1890s fantasy novels]]
[[Category:19th-century American novels]]
[[Category:American fantasy novels]]
[[Category:Books based on Alice in Wonderland]]

{{1890s-fantasy-novel-stub}}