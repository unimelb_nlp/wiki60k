{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:SMSAugsburg.jpg|300px]]
|Ship caption=''Augsburg'' at anchor, 4 August 1914
}}
{{Infobox ship career
|Hide header=
|Ship country=[[German Empire]]
|Ship flag={{shipboxflag|German Empire|naval}}
|Ship name=SMS ''Augsburg''
|Ship namesake=[[Augsburg]]
|Ship ordered=
|Ship builder=
|Ship laid down=1908
|Ship launched=10 July 1909
|Ship completed=1 November 1910
|Ship commissioned=
|Ship decommissioned= 
|Ship in service=
|Ship out of service= 
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship fate=[[ship breaking|Scrapped]], 1922
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Kolberg|cruiser}}
|Ship displacement={{convert|4362|t|LT|lk=out|sp=us}}
|Ship length={{convert|130.50|m|ftin|abbr=on}}
|Ship beam={{convert|14|m|ftin|abbr=on}}
|Ship power={{convert|19000|PS|kW shp|lk=on|abbr=on|0}}
|Ship draft={{convert|5.36|–|5.45|m|ftin|abbr=on}}
|Ship propulsion=*4 shafts, 2 sets of Parsons [[Steam turbines]]
*15 [[boiler (steam generator)|boiler]]s 
|Ship speed={{convert|25|kn|lk=in}}
|Ship range=
|Ship complement=367
|Ship armament=*12 × 1 – {{convert|10.5|cm|in|1|adj=on|abbr=on}} guns
*2 × {{convert|45|cm|in|abbr=on|1}} [[torpedo tube]]s
|Ship armor=*[[Deck (ship)|Deck]]: {{convert|20|-|40|mm|in|abbr=on}}
*[[Gun shield]]s: {{convert|50|mm|in|abbr=on|0}}
*[[Conning tower]]: {{convert|100|mm|in|abbr=on|1}}
|Ship notes=
}}
|}

'''SMS ''Augsburg''''' was a {{sclass-|Kolberg|cruiser|0}} [[light cruiser]] of the German ''[[Kaiserliche Marine]]'' (Imperial Navy) during the [[First World War]]. She had three sister ships, {{SMS|Kolberg}}, {{SMS|Mainz||2}}, and {{SMS|Cöln|1909|2}}. The ship was built by the [[Kaiserliche Werft Kiel|''Kaiserliche Werft'']] in [[Kiel]]; her hull was laid down in 1908 and she was launched in July 1909. ''Augsburg'' was commissioned into the [[High Seas Fleet]] in October 1910. She was armed with a main battery of twelve [[10.5 cm SK L/45 naval gun|10.5&nbsp;cm SK L/45 guns]] and had a top speed of {{convert|25.5|kn}}.

After her commissioning, ''Augsburg'' spent her peacetime career first as a torpedo test ship and then as a gunnery training ship. After the outbreak of World War I, she was assigned to the [[Baltic Sea]], where she spent the entire war. On 2 August 1914, she participated in an operation that saw the first shots of the war with Russia fired, and she later took part in the [[Battle of the Gulf of Riga]] in August 1915 and [[Operation Albion]] in October 1917, as well as numerous smaller engagements throughout the war. She struck a [[naval mine|mine]], once, in January 1915, though the ship was again operational in a few months. After the end of the war, ''Augsburg'' was ceded to Japan as a [[war prize]], and was subsequently broken up for scrap in 1922.

==Design==
{{main|Kolberg-class cruiser}}

''Augsburg'' was ordered as a replacement for {{SMS|Sperber}} under the contract name ''Ersatz Sperber'' and was laid down in 1908 at the [[Kaiserliche Werft Kiel|''Kaiserliche Werft'']] shipyard in [[Kiel]]. She was launched on 10 July 1909, after which [[fitting-out]] work commenced. She was commissioned into the [[High Seas Fleet]] on 1 October 1910.<ref name=G1067>Gröner, pp. 106–107</ref> The ship was {{convert|130.50|m|ftin|sp=us}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|14|m|ftin|abbr=on}} and a [[draft (hull)|draft]] of {{convert|5.45|m|ftin|abbr=on}} forward. She displaced {{convert|4915|t|LT|abbr=on}} at full combat load. Her propulsion system consisted of two sets of [[Parsons Marine Steam Turbine Company|Parsons]] [[steam turbine]]s driving four {{convert|2.25|m|ftin|adj=on|sp=us}} propellers. They were designed to give {{convert|19000|PS|kW shp|lk=on|0}}. These were powered by fifteen coal-fired Marine [[water-tube boiler]]s. These gave the ship a top speed of {{convert|25.5|kn}}. ''Augsburg'' carried {{convert|940|MT|abbr=on}} of coal that gave her a range of approximately {{convert|3500|nmi|lk=in}} at {{convert|14|kn}}. ''Augsburg'' had a crew of 18 officers and 349 enlisted men.<ref name=G106>Gröner, p. 106</ref>

The ship was armed with twelve [[10.5 cm SK L/45 naval gun|10.5&nbsp;cm SK L/45 guns]] in single pedestal mounts. Two were placed side by side forward on the [[forecastle]], eight were located amidships, four on either side, and two were side by side aft.<ref name=C159>Gardiner & Gray, p. 159</ref> These were replaced in 1916–1917 with six [[15 cm SK L/45]] guns. She also carried four [[5.2 cm SK L/55 naval gun|{{convert|5.2|cm|in|abbr=on}} SK L/55]] anti-aircraft guns, though these were replaced with a pair of two {{convert|8.8|cm|in|abbr=on}} SK L/45 anti-aircraft guns in 1918. She was also equipped with a pair of {{convert|45|cm|abbr=on|1}} [[torpedo tube]]s submerged in the hull. Two deck-mounted {{convert|50|cm|abbr=on}} torpedo tube launchers were added in 1918 She could also carry 100 [[naval mine|mines]]. The [[conning tower]] had {{convert|100|mm|abbr=on}} thick sides, and the deck was covered with up to {{convert|40|mm|abbr=on}} thick armor plate.<ref name=G106/>

==Service history==
[[File:SMS Augsburg.png|thumb|Illustration of ''Augsburg'' at sea]]
After her commissioning, ''Augsburg'' was used as a torpedo test ship. In 1912, she was transferred to gunnery training.<ref name=G107/> On 20 May 1914 she visited [[Dundee]] on a courtesy visit. Captain Fischer and his crew were welcomed by the Lord Provost and "the greatest friendliness was displayed".<ref name=Dundee>''Dundee, Perth, Forfar, and Fife's People's Journal'' - Saturday 23 May 1914</ref> Following the outbreak of [[World War I]] in August 1914, she was assigned to the [[Baltic Sea]],<ref name=G107>Gröner, p. 107</ref> under the command of Rear Admiral [[Robert Mischke]]. On 2 August, ''Augsburg'' laid a minefield outside the Russian harbor of [[Liepāja|Libau]], while {{SMS|Magdeburg||2}} shelled the port. The Russians had in fact already left Libau, which was seized by the German Army. The minefield laid by ''Augsburg'' was poorly marked and hindered German operations more than Russian efforts. ''Augsburg'' and the rest of the Baltic light forces then conducted a series of bombardments of Russian positions. On 17 August, ''Augsburg'', ''Magdeburg'', three [[destroyer]]s, and the [[minelayer]] {{SMS|Deutschland|1914|2}} encountered a pair of powerful Russian [[armored cruiser]]s, {{ship|Russian cruiser|Admiral Makarov||2}} and {{ship|Russian cruiser|Gromoboi||2}}. The Russian commander, under the mistaken assumption that the German armored cruisers {{SMS|Roon||2}} and {{SMS|Prinz Heinrich||2}} were present, did not attack and both forces withdrew.<ref name=H184>Halpern, p. 184</ref>

In September, the light forces in the Baltic were reinforced with the IV Battle Squadron, composed of the older {{sclass-|Braunschweig|battleship|5}} and {{sclass-|Wittelsbach|battleship|2}}s, and the large armored cruiser {{SMS|Blücher||2}}. Starting on 3 September, the combined German force conducted a sweep into the Baltic. During the operation, ''Augsburg'' spotted the Russian cruisers {{ship|Russian cruiser|Pallada|1906|2}} and {{ship|Russian cruiser|Bayan|1907|2}}. She attempted to draw them closer to ''Blücher'', but the Russians refused to take the bait and withdrew. On 7 September, ''Augsburg'' and the [[torpedo boat]] {{SMS|V25||2}} steamed into the [[Gulf of Bothnia]] and sank a Russian steamer off [[Raumo]]. By the 9th, the German fleet had returned to port.<ref>Halpern, p. 185</ref> On the night of 24–25 January, ''Augsburg'' ran into a Russian minefield off [[Bornholm]] and struck a mine. The crew kept the ship afloat, and she was towed back to port for repairs.<ref>Halpern, pp. 186–187</ref>

''Augsburg'' was back in service by April 1915, ready for a major operation against Libau. The German Army planned to seize the port as a distraction from the main Austro-German effort at [[Gorlice–Tarnów Offensive|Gorlice–Tarnów]]. They requested naval support, and so the Navy organized a force comprising the coastal defense ship {{SMS|Beowulf||2}}, three armored cruisers, three light cruisers, including ''Augsburg'', and a large number of torpedo boats and minesweepers. In addition, the [[IV Scouting Group]], consisting of four light cruisers and twenty-one torpedo boats, was sent from the North Sea to reinforce the operation. The German Army captured Libau in May, and it was subsequently turned into an advance base for the German Navy.<ref>Halpern, pp. 191–193</ref> Later that month, the Navy assigned a mine-laying operation to ''Augsburg'' and {{SMS|Lübeck||2}}; they were to lay a minefield near the entrance to the [[Gulf of Finland]]. A submarine attack on the cruiser {{SMS|Thetis||2}}, however, prompted the German naval command to cancel the operation.<ref>Polmar & Noot, p. 40</ref>

On 1 June, ''Augsburg'', {{SMS|Roon||2}}, ''Lübeck'', and seven torpedo boats escorted the minelaying cruiser {{SMS|Albatross|1907|6}} while she laid a field off [[Bogskär]]. ''Augsburg'' served as the [[flagship]] of Commodore [[Johannes von Karpf]], the commander of the operation. After finishing laying the minefield, Karpf sent a wireless transmission informing headquarters he had accomplished the mission, and was returning to port. This message was intercepted by the Russians, allowing them to intercept the Germans. Four Russian armored cruisers, with the powerful armored cruiser {{ship|Russian cruiser|Rurik|1906|2}} steaming in support, attempted to ambush the German squadron. Karpf dispersed his force shortly before encountering the Russians; ''Augsburg'', ''Albatross'', and three torpedo boats steamed to [[Rixhöft]] while the remainder went to Libau. Shortly after 06:30 on 2 June, lookouts on ''Augsburg'' spotted the Russian force; Karpf ordered the slower ''Albatross'' to seek refuge in neutral Swedish waters, while ''Augsburg'' and the torpedo boats used their high speed to escape the Russians. In the [[Battle of Åland Islands|engagement that followed]], ''Albatross'' was badly damaged and ran aground in Swedish waters. The Russians then turned to engage the second German force, but were low on ammunition after the engagement with ''Augsburg'' and ''Albatross'' and broke off the engagement.<ref>Halpern, pp. 194–195</ref>

The Russian {{sclass-|Kasatka|submarine|2}} {{ship|Russian submarine|Okun||2}} fired two torpedoes at ''Augsburg'' on the night of 28 June, though both missed.<ref>Polmar & Noot, p. 41</ref> ''Augsburg'' was assigned to the forces that took part in the [[Battle of the Gulf of Riga]] in August 1915. A significant detachment from the [[High Seas Fleet]], including eight [[dreadnought]]s and three battlecruisers, went into the Baltic to clear the [[Gulf of Riga]] of Russian naval forces. ''Augsburg'' participated in the second attack on 16 August, led by the dreadnoughts {{SMS|Nassau||2}} and {{SMS|Posen||2}}.<ref>Halpern, p. 197</ref> On the night of 19 August, ''Augsburg'' encountered a pair of Russian [[gunboat]]s—{{ship|Russian gunboat|Sivutch||2}} and {{ship|Russian gunboat|Korietz||2}}; ''Augsburg'' and ''Posen'' sank ''Sivutch'', though ''Korietz'' managed to escape. The Russian surface forces had by this time withdrawn to [[Moon Sound]], and the threat of Russian [[submarine]]s and mines still in the Gulf prompted the Germans to retreat.<ref>Halpern, p. 198</ref> On 13 October, an unknown submarine fired a torpedo at ''Augsburg'', though it did not hit her.<ref>Polmar & Noot, p. 45</ref> In September 1916, ''Augsburg'' participated in an attempt to force the [[Irben Strait]] into the Gulf of Riga in September 1916. Heavy Russian resistance, primarily from the old battleship {{ship|Russian battleship|Slava||2}}, forced the Germans to retreat from the Gulf.<ref>Polmar & Noot, p. 47</ref>

In November 1917, ''Augsburg'' participated in another attack on the Gulf of Riga, [[Operation Albion]]. By this point, she had been assigned to the [[VI Scouting Group]] along with {{SMS|Strassburg||2}} and her sister {{SMS|Kolberg||2}}. At 06:00 on 14 October 1917, the three ships left [[Liepāja|Libau]] to escort minesweeping operations in the Gulf of Riga. They were attacked by Russian {{convert|12|in|adj=on}} coastal guns on their approach and were temporarily forced to turn away. By 08:45, however, they had anchored off the [[Miķeļbāka|Mikailovsk Bank]] and the minesweepers began to clear a path in the minefields.<ref>Staff, p. 60</ref> Two days later, ''Augsburg'' joined the [[dreadnought]]s {{SMS|König||2}} and {{SMS|Kronprinz||2}} for a sweep of the Gulf of Riga. While the battleships engaged the Russian naval forces, ''Augsburg'' was tasked with supervising the occupation of [[Kuressaare|Arensburg]].<ref>Staff, pp. 102–103</ref>

According to the [[Armistice with Germany|Armistice]] that ended the war, ''Augsburg'' and the rest of the German fleet not interned in [[Scapa Flow]] were to be returned to the main German ports and disarmed.<ref>See: [[s:Armistice between the Allied Governments and Germany]] V. Naval Conditions, Article 23</ref> In the subsequent [[Treaty of Versailles]] that formally ended the conflict, ''Augsburg'' was listed as a warship to be surrendered to the Allied powers; she was to be disarmed in accordance with the terms of the Armistice, but her guns were to remain on board.<ref>See: [[s:Treaty of Versailles 1919|Treaty of Versailles]] Section II: Naval Clauses, Article 185</ref> After the end of World War I, ''Augsburg'' was surrendered to Japan as a [[war prize]] on 3 September 1920, under the name "Y". The Japanese had no use for the ship, and so she was broken up in [[Dordrecht]] in 1922.<ref name=G107/>

==Footnotes==
{{Reflist|colwidth=20em}}

== References ==
* {{Cite book|editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1922|year=1984|location=Annapolis, MD|publisher=Naval Institute Press|isbn=0-87021-907-3}}
* {{Cite book |last=Gröner|first=Erich|title=German Warships: 1815–1945|year=1990|publisher=Naval Institute Press|isbn=0-87021-790-9|location=Annapolis, MD}}
* {{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis, MD|publisher=Naval Institute Press|isbn=1-55750-352-4}}
* {{Cite book |last1=Polmar|first1=Norman|last2=Noot|first2=Jurrien|title=Submarines of the Russian and Soviet Navies, 1718–1990|year=1991|location=Annapolis|publisher=Naval Institute Press|isbn=0-87021-570-1}}
* {{Cite book |last=Staff|first=Gary|title=Battle for the Baltic Islands|year=2008|location=Barnsley, South Yorkshire|publisher=Pen & Sword Maritime|isbn=978-1-84415-787-7}}

{{Kolberg class light cruisers}}
{{Good article}}

{{DEFAULTSORT:Augsburg}}
[[Category:Kolberg-class cruisers]]
[[Category:Ships built in Kiel]]
[[Category:1909 ships]]
[[Category:World War I cruisers of Germany]]