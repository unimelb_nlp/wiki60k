{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb 
| name     = Gulfview Heights
| state    = sa
 |city=Adelaide
| image    = Gulfview heights house 2008.jpg
| caption  = 
| lga      = City of Salisbury
| postcode = 5096
| pop      = 
| est      = 1996
| stategov = [[Electoral district of Playford|Playford]]
| fedgov   = [[Division of Makin|Makin]]
 |near-nw=
 |near-n=[[Greenwith, South Australia|Greenwith]]
 |near-ne=[[Golden Grove, South Australia|Golden Grove]]
 |near-w=[[Salisbury East, South Australia|Salisbury East]]
 |near-e=[[Wynn Vale, South Australia|Wynn Vale]]
 |near-sw=[[Para Hills West, South Australia|Para Hills West]]
 |near-s=[[Para Hills, South Australia|Para Hills]]
 |near-se=[[Modbury Heights, South Australia|Modbury Heights]]
 |dist1=5
|location1=[[Modbury, South Australia|Modbury]]
}}

'''Gulfview Heights''' is a small [[suburb]] of [[Adelaide]], [[South Australia]] and is within the [[City of Salisbury]] and [[City of Tea Tree Gully]] local government area. It is adjacent to [[Wynn Vale, South Australia|Wynn Vale]], [[Salisbury East, South Australia|Salisbury East]] and [[Para Hills, South Australia|Para Hills]].

==History==
{{Unreferencedsect|date=March 2017}}
Gulfview Heights is a relatively affluent new estate suburb carved out of [[Salisbury East, South Australia|Salisbury East]] and [[Para Hills, South Australia|Para Hills]]. The main estate south of Wynn Vale Drive was first settled in 1996 and the whole area was officially renamed in 2001.

The area was originally farmed by William Pedler, a shoemaker whose trade gave rise to the name of Cobbler Creek. The reserve was set aside in 1970 and declared in 1989.

==Area==
The size of Gulfview Heights is approximately 4&nbsp;km².  It has 4 parks covering nearly 10% of the total area.  There are 1 school located in Gulfview Heights. The population of Gulfview Heights in 2001 was 2,632 people.  By 2006 the population was 2,971 showing a population growth of 13% in the area during that time. The predominant age group in Gulfview Heights is 10 – 19 years.  

The boundary of Gulfview Heights is the Smith Road extension and the southern boundary of Cobbler Creek Recreation Park to the north, the suburb of Wynn Vale to the east, Bridge Road to the west and Para Hills Ovals & Yulinda Gully to the south. Bayview Parade is the main thoroughfare for the estate, while Kiekebusch Road services the north (both running roughly north-south).  Wynn Vale Drive and McIntyre Road run through the suburb in a roughly east-west direction.<ref>[http://www.governmentgazette.sa.gov.au/2002/January/2002_002.pdf Government Gazette (SA)], 10 January 2002, p.6.</ref>

==Facilities==
The suburb has one primary school - Gulfview Heights Primary School (formerly Salisbury South-East Primary School) - and is near several others such as [[King's Baptist Grammar School]], Keithcot Primary in Wynn Vale and Keller Road Primary in Salisbury East. Para Hills and Golden Grove High Schools are nearby, as is the Golden Grove Village Shopping Centre.

The 288 hectare [[Cobbler Creek Recreation Park]] is accessible from Smith Road, and ranges from open grasslands near Bridge Road to [[River Red Gum|red gums]] along the creek itself. There are no visitor facilities or amentities other than a number of walking trails starting from Smith Road extension and McIntyre Road.  There is also a number of parks and reserves the largest being Wynn Vale Gullies (58 Ha and includes a lake), Pepermint Gum Gully (48 Ha) and Yulinda Gully (22 Ha).  All of which extend into neighbouring suburbs.  There is also a number of smaller parks and reserves, several with play equipment in Leonard Street Reserve and Cordoba Avenue Reserve.

==Transport==
The 560 bus route links Bridge Road with the [[O-Bahn Busway|Adelaide O-Bahn]] at the [[Tea Tree Plaza Interchange]], and also to the [[Salisbury railway station, Adelaide|Salisbury bus/train interchange]].
The 500 & 502 bus routes link Bridge Road with the [[O-Bahn Busway|Adelaide O-Bahn]] at the [[Paradise Interchange]], and also to the [[Salisbury railway station, Adelaide|Salisbury bus/train interchange]]<ref>http://adelaidemetro.com.au/timetables/buses</ref>

== See also ==
* [[City of Salisbury]]
* [[List of Adelaide suburbs]]

==References==
{{Reflist}}

==External links==
*[http://www.adelaidemetro.com.au Adelaide Metro]
*[http://www.parks.sa.gov.au/publish/groups/public/@parks/@northernlofty/documents/rawfile/yurre_pdfs_cobbler_creek_rp.pdf Department of Environment and Heritage - Cobbler Creek]
*[http://ourworld.compuserve.com/homepages/rvtaylor/ Friends of Cobbler Creek]

{{Coord|-34.796|138.667|format=dms|type:city_region:AU-SA|display=title}}
{{City of Salisbury suburbs}}

[[Category:Suburbs of Adelaide]]