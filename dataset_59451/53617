{{about|investment in finance|investment in macroeconomics|Investment (macroeconomics)|other uses|Investment (disambiguation)}}
{{redirect|Invest|the term in meteorology|Invest (meteorology)}}
{{refimprove|date=February 2017}}

To '''invest''' is to allocate money (or sometimes another resource, such as [[time]]) in the expectation of some benefit in the future. 

In finance, the benefit from investment is called a [[rate of return|return]]. The return may consist of [[capital gain]] or investment income, including [[dividend|dividends]], [[interest]], rental income etc., or a combination of the two. The projected economic return is the appropriately discounted value of the future returns. The historic return comprises the actual capital gain (or loss) or income (or both) over a period of time.

Investment generally results in acquiring an [[asset]], also called an investment. If the asset is available at a price worth investing, it is normally expected either to generate income, or to appreciate in value, so that it can be sold at a higher price (or both).

Investors generally expect higher [[rate of return|returns]] from [[risk|riskier]] investments. Financial assets range from low-risk, low-[[rate of return|return]] investments, such as high-grade [[government bond]]s, to those with higher risk and higher expected commensurate reward, such as [[emerging markets]] [[stock]] investments. 

Investors, particularly novices, are often advised to adopt an [[investment strategy]] and [[Diversification (finance)|diversify]] their [[portfolio (finance)|portfolio]]. [[Diversification (finance)|Diversification]] has the [[statistics|statistical]] effect of reducing overall [[risk]].

==Related terms==
Investment differs from [[arbitrage]], in which [[profit (accounting)|profit]] is generated without investing [[Capital (economics)|capital]] or bearing [[risk]].

An investor may bear a risk of loss of some or all of their [[Capital (economics)|capital]] invested, whereas in [[saving]] (such as in a [[bank deposit]]) the risk of loss in [[nominal value]] is normally remote. (Note that if the currency of a savings account differs from the account holder's home currency, then there is the risk that the exchange rate between the two currencies will move unfavorably, so that the value in the account holder's home currency of the savings account decreases.)

[[Speculation]] involves a level of risk which is greater than most investors would generally consider justified by the expected return. An alternative characterization of speculation is its short-term, opportunistic nature.

==Famous investors==
Investors famous for their success include [[Warren Buffett]]. In March 2013 ''[[Forbes]]'' magazine, Warren Buffett ranked number 2 in their [[Forbes 400]] list.<ref>{{cite web|last=Editor|title=Forbes 400: Warren Buffett|url=http://www.forbes.com/profile/warren-buffett/|work=Forbes Magazine|accessdate=1 March 2013}}</ref> Buffett has advised in numerous articles and interviews that a good investment strategy is long term and choosing the right assets to invest in requires [[due diligence]]. 

[[Edward O. Thorp]] was a highly successful [[hedge fund]] manager in the 1970s and 1980s who spoke of a similar approach.<ref>{{cite book|last=Thorp|first=Edward|title=Kelly Capital Growth Investment Criterion|url=https://books.google.com/?id=GherKrR0X5cC&dq=edward+thorp+hedge+fund+kelly+criterion|publisher=World Scientific|isbn=9789814293495|year=2010}}</ref> 

The investment principles of both of these investors have points in common with the [[Kelly criterion]] for money management.<ref>{{cite web|title=The Kelly Formula: Growth Optimized Money Management|url=http://seekingalpha.com/article/250811-the-kelly-formula-growth-optimized-money-management-not-for-the-faint-of-heart|work=Seeking Alpha|publisher=Healthy Wealthy Wise Project}}</ref> Numerous interactive calculators which use the Kelly criterion can be found online.<ref>{{cite web|last=Jacques|first=Ryan|title=Kelly Calculator Investment Tool|url=http://rldinvestments.com/Kelly%20Calculator/js/Money%20Management.html|accessdate=7 October 2008}}</ref>

==Intermediaries and collective investments==
Investments are often made indirectly through [[intermediary]] financial institutions. These intermediaries include pension funds, [[bank|banks]], and [[insurance]] companies. They may pool money received from a number of individual end investors into funds such as [[investment trust]]s, [[unit trust]]s, [[SICAV]]s etc. to make large scale investments. Each individual investor holds an indirect or direct claim on the assets purchased, subject to charges levied by the intermediary, which may be large and varied. 

Approaches to investment sometimes referred to in marketing of collective investments include [[dollar cost averaging]] and [[market timing]].

== History ==
The [[Code of Hammurabi]] (around 1700 BC) provided a legal framework for investment, establishing a means for the pledge of collateral by codifying debtor and creditor rights in regard to pledged land. Punishments for breaking financial obligations were not as severe as those for crimes involving injury or death.<ref>{{cite web|title=The Code of Hammurabi|url=http://avalon.law.yale.edu/ancient/hamframe.asp|website=The Avalon Project; Documents in Law, History and Diplomacy}}</ref>

In the early 1900s purchasers of stocks, bonds, and other securities were described in media, academia, and commerce as [[speculation|speculators]]. By the 1950s, the term investment had come to denote the more conservative end of the securities spectrum, while speculation was applied by financial brokers and their advertising agencies to higher risk securities much in vogue at that time. Since the last half of the 20th century, the terms speculation and speculator have specifically referred to higher risk ventures.

==Value investment==
{{main|value investing}}
A value investor buys undervalued securities (and sells overvalued ones). To identify undervalued securities, a value investor uses analysis of the financial reports of the issuer to evaluate the security. Value investors employ accounting ratios, such as [[earnings per share]] and sales growth, to identify securities trading at prices below their worth.

[[Warren Buffett]] and [[Benjamin Graham]] are notable examples of value investors. Graham and [[David Dodd|Dodd's]] seminal work [[Security Analysis]] was written in the wake of the [[Wall Street Crash of 1929]].

The [[price to earnings ratio]] (P/E), or earnings multiple, is a particularly significant and recognized fundamental ratio, with a function of dividing the share price of stock, by its [[earnings per share]]. This will provide the value representing the sum investors are prepared to expend for each dollar of company earnings. This ratio is an important aspect, due to its capacity as measurement for the comparison of valuations of various companies. A stock with a lower P/E ratio will cost less per share, than one with a higher P/E, taking into account the same level of [[financial]] performance; therefore, it essentially means a low P/E is the preferred option.<ref>{{cite web|title=Price-Earnings Ratio - P/E Ratio|url=http://www.investopedia.com/terms/p/price-earningsratio.asp|website=Investopedia}}</ref>

An instance, in which the price to earnings ratio has a lesser significance, is when companies in different industries are compared.  An example; although, it is reasonable for a telecommunications stock to show a P/E in the low teens; in the case of hi-tech stock, a P/E in the 40s range, is not unusual.  When making comparisons the P/E ratio can give you a refined view of a particular stock valuation.

For investors paying for each dollar of a company's earnings, the P/E ratio is a significant indicator, but the [[price-to-book ratio]] (P/B) is also a reliable indication of how much investors are willing to spend on each dollar of company assets. In the process of the P/B ratio, the share price of a stock is divided by its net assets; any intangibles, such as goodwill, are not taken into account. It is a crucial factor of the price-to-book ratio, due to it indicating the actual payment for tangible assets and not the more difficult valuation, of intangibles. Accordingly, the P/B could be considered a comparatively, conservative metric.

==Free cash flow and capital structure==
[[Free cash flow]] measures the cash a company generates which is available to its debt and equity investors, after allowing for reinvestment in [[working capital]] and [[capital expenditure]]. High and rising free cash flow therefore tend to make a company more attractive to investors.

The [[debt-to-equity ratio]] is an indicator of [[capital structure]]. A high proportion of [[debt]], reflected in a high debt-to-equity ratio, tends to make a company's [[earnings]], free cash flow, and ultimately the returns to its investors, more risky or [[volatility (finance)|volatile]]. Investors compare a company's debt-to-equity ratio with those of other companies in the same industry, and examine trends in debt-to-equity ratios and free cash flow.

==EBITDA==
A popular valuation metric is [[Earnings before interest, taxes, depreciation and amortization|Earnings Before Interest, Tax, Depreciation and Amortization]] (EBITDA), with application for example to valuing unlisted companies and [[mergers and acquisitions]].<ref>{{cite web|title=What is EBITDA|url=http://www.investorwords.com/1632/EBITDA.html}}</ref>

For an attractive investment, for example a company competing in a high growth industry, an investor might expect a significant acquisition premium above book value or current market value, which values the company at several times the most recent EBITDA. A private equity fund for example may buy a target company for a multiple of its historical or forecasted EBITDA, perhaps as much as 6 or 8 times.

In certain cases, an EBITDA may be sacrificed by a company, in order for the pursuance of future growth; a strategy frequently used by corporate giants, such as, [[Amazon.com|Amazon]], [[Google]] and [[Microsoft]], among others. This is a business decision that can impact negatively on buyout offers, founded on EBITDA and can be the cause of many negotiations, failing. It may be recognized as a valuation breach, with many investors maintaining that sellers are too demanding, while buyers are regarded as failing to realize the long-term potential of, expenditure or [[acquisitions]].

==Capital gains taxes==
{{Globalize |date=January 2017 |discuss=Talk:Investment#Globalize }}

The amount to pay in taxes for long term investments, investments that span over a year long term, and short term investments such as those that are below a year are different. The long term investments range from Zero to twenty percent for capital gains and they are regulated by what tax bracket you are in for income taxes. For the zero to fifteen percent income tax bracket you could qualify for the zero percent long-term capital gains rate. The next bracket is the fifteen to twenty percent income tax bracket where you are set at fifteen percent capital gains tax for long term investments. The next bracket is between twenty and 39.6 percent and that leads to a twenty percent capital gains tax however with these numbers you should add 3.8 percent for the health care surtax. The short term capital gains tax is also related to your total taxable income and is taxed at the same rate as your income and ranges from ten to 39.6 percent.<ref>{{cite web|title=Guide to Short-term vs Long-term Capital Gains Taxes (Brokerage Accounts, etc.)|url=https://turbotax.intuit.com/tax-tools/tax-tips/Investments-and-Taxes/Guide-to-Short-term-vs-Long-term-Capital-Gains-Taxes--Brokerage-Accounts--etc--/INF22384.html|website=Intuit Turbo Tax|publisher=Turbo Tax|accessdate=19 November 2015}}</ref>

==Types of financial investment==
Types of financial investments include:
* [[Alternative investment]]s
* [[Traditional investments]]

==See also==<!-- PLESE RESPECT ALPHABETIAL ORDER - why not group logically -->
{{colbegin|2}}
* [[Asset]]
* [[Capital accumulation]]
* [[Capital gains tax]]
* [[Diversification (finance)]]
* [[Foreign direct investment]]
* [[Fundamental Analysis]]
* [[Fundamental Analysis Software]]
* [[Hedge fund]]
* [[List of countries by gross fixed investment as percentage of GDP]]
* [[List of economics topics]]
* [[Market sentiment]]
* [[Mortgage investment corporation]]
* [[Rate of return]]
* [[Risk]]
* [[Socially responsible investing]]
* [[Specialized investment fund]]
* [[Technical Analysis]]
* [[Time value of money]]
{{colend}}

==References==
{{reflist}}

==External links==
{{wikiquote}}
{{commons category|Investments}}
{{NSRW Poster|Investments}}
* {{dmoz|Business/Investing/Stocks_and_Bonds/Equities/Research_and_Analysis/|Investing}}
<!--======================== {{No more links}} ============================
    | PLEASE BE CAUTIOUS IN ADDING MORE LINKS TO THIS ARTICLE. Wikipedia  |
    | is not a collection of links nor should it be used for advertising. |
    |                                                                     |
    |           Excessive or inappropriate links WILL BE DELETED.         |
    | See [[Wikipedia:External links]] & [[Wikipedia:Spam]] for details.  |
    |                                                                     |
    | If there are already plentiful links, please propose additions or   |
    | replacements on this article's discussion page, or submit your link |
    | to the relevant category at the Open Directory Project ...(dmoz.org)   |
    | and link back to that category using the {{dmoz}} template.         |
    ======================= {{No more links}} =============================-->
{{economics}}

{{Authority control}}

[[Category:Investment| ]]