{{Redirect|La Marche de l'Empereur|the soundtrack|La Marche de l'Empereur (soundtrack)}}
{{Infobox film
| name           = March of the Penguins
| image          = march_of_the_penguins_poster.jpg
| alt            = 
| caption        = North American release poster
| director       = [[Luc Jacquet]]
| producer       = {{plainlist|
* Yves Darondeau
* Christophe Lioud
* Emmanuel Priou
}}
| writer         = {{plainlist|
* Luc Jacquet
* Michel Fessler
}}
| narrator       = {{plainlist|
* [[Charles Berling]]
* [[Romane Bohringer]]
* [[Jules Sitruk]]
* [[Morgan Freeman]]
}}
| music          = {{plainlist|
* [[Émilie Simon]] (France)
* [[Alex Wurman]] (US)
}}
| cinematography = {{plainlist|
* [[Laurent Chalet]]
* Jérôme Maison
}}
| editing        = Sabine Emiliani
| studio         = {{plainlist|
* [[Wild Bunch (film company)|Wild Bunch]]
* [[National Geographic Society|National Geographic Films]]
* Bonne Pioche
}}
| distributor    = [[Walt Disney Studios Motion Pictures|Buena Vista International France]]<br>[[Warner Independent Pictures]]<ref name=lat/>
| released       = {{film date|2005|01|26}}<!-- Do not add the US release date here, please; see WP:FILMRELEASE. -->
| runtime        = 80 minutes<!-- Theatrical runtime: 80:10 --><ref>{{cite web|title=''MARCH OF THE PENGUINS'' (U)|url=http://www.bbfc.co.uk/AFF211653/|work=[[British Board of Film Classification]]|date=2005-06-21|accessdate=2012-09-22}}</ref>
| country        = France
| language       = English
| budget         = $8 million<ref name=lat>{{cite news |first=Claudia  |last=Eller | author2= Dawn C. Chmielewski |title=Disney gets back to nature |url=http://articles.latimes.com/2008/apr/22/business/fi-disney22 |date=April 22, 2008 | accessdate=October 30, 2013 |newspaper=Los Angeles Times }}</ref>
| gross          = $127.4 million<ref>{{mojo title|marchofthepenguins|March of the Penguins}}</ref>
}}
'''''March of the Penguins''''' ([[French language|French]] '''''La Marche de l'empereur''''' ; {{IPA-fr|lamaʁʃ dəlɑ̃ˈpʁœʁ}}) is a 2005 French [[feature-length]] [[nature documentary]] directed and co-written by [[Luc Jacquet]], and co-produced by Bonne Pioche<ref name="March of the Penguins">{{cite web|url=http://www.tcm.com/tcmdb/title/613088/CNN-Films-March-of-the-Penguins/full-credits.html|title=March of the Penguins|work=[[Turner Classic Movies]]|publisher=[[Turner Broadcasting System]] ([[Time Warner]])|accessdate=July 4, 2016|location=[[Atlanta]]}}</ref> and the [[National Geographic Society]]. The documentary depicts the yearly journey of the [[emperor penguin]]s of [[Antarctica]]. In autumn, all the [[penguin]]s of breeding age (five years old and over) leave the ocean, their normal [[Habitat (ecology)|habitat]], to walk inland to their ancestral breeding grounds. There, the penguins participate in a [[courtship]] that, if successful, results in the hatching of a chick. For the chick to survive, both parents must make multiple arduous journeys between the ocean and the breeding grounds over the ensuing months.

It took one year for the two isolated cinematographers [[Laurent Chalet]] and Jérôme Maison to shoot the documentary, which was shot around the French scientific base of [[Dumont d'Urville Station|Dumont d'Urville]] in [[Adélie Land]].

The documentary won the [[79th Academy Awards|2006 Academy Award]] for [[Academy Award for Best Documentary Feature|Best Documentary Feature]].<ref name="NY Times">{{cite news |url=https://movies.nytimes.com/movie/328540/March-of-the-Penguins/details |title=NY Times: March of the Penguins |accessdate=2008-11-23|work=[[The New York Times]]}}</ref>

The documentary had a 2007 follow-up movie, "Arctic Tale," which only took in at the box office $1.8 million worldwide.<ref name=lat/>

==Subject matter==
The emperor penguins use a particular spot as their breeding ground because it is on ice that is solid year round and no danger of the ice becoming too soft to support the colony exists. At the end of [[Climate of Antarctica|Antarctic summer]], the breeding ground is only a few hundred meters away from the open water where the penguins can feed. However, by the end of winter, the breeding ground is over {{convert|100|km|mi}} away from the nearest open water. To reach it, all the penguins of breeding age must traverse this great distance.

The penguins practice [[serial monogamy]] within each breeding season. The female lays a single egg, and the co-operation of the parents is needed if the chick is to survive. After the female lays the egg, she transfers it to the feet of the waiting male with minimal exposure to the elements, as the intense cold could kill the developing embryo. The male tends to the egg when the female returns to the sea, now even farther away, both to feed herself and to obtain extra food for feeding her chick when she returns. She has not eaten in two months and by the time she leaves the hatching area, she will have lost a third of her body weight.

For an additional two months, the males huddle together for warmth, and incubate their eggs. They endure temperatures approaching {{convert|-62|°C|°F}}, and their only source of water is [[snow]] that falls on the breeding ground. When the chicks hatch, the males have only a small meal to feed them, and if the female does not return, they must abandon their chick and return to the sea to feed themselves. By the time they return, they have lost half their weight and have not eaten for four months. The chicks are also at risk from predatory birds such as northern giant petrels.<ref>This bird is unidentified in the documentary itself, but the Region 2 DVD identifies it.[http://coolantarctica.com/Antarctica%20fact%20file/wildlife/antarctic_skuas.htm Antarctic Skua]</ref>

The mother penguins come back and feed their young, while the male penguins go all the way back to sea (70&nbsp;miles) to feed themselves. This gives the mothers time to feed their young ones and bond with them. Unfortunately, a fierce storm arrives and some of the chicks perish.

The death of a chick is tragic, but it does allow the parents to return to the sea to feed for the rest of the breeding season. When a mother penguin loses its young in a fierce storm, it sometimes attempts to steal another mother's chick. At times, the young are abandoned by one parent, and they must rely on the return of the other parent, which can recognize the chick only from its unique call. Many parents die on the trip, killed by exhaustion or by predators (such as the [[whale shark]]), dooming their chicks back at the breeding ground.

The ingenious fight against starvation is a recurring theme throughout the documentary. In one scene, near-starving chicks are shown taking sustenance out of their father's throat sacs, 11th-hour nourishment in the form of a milky, protein-rich substance secreted from a sac in the father's throat to feed their chicks in the event that circumstances require.

The parents must then tend to the chick for an additional four months, shuttling back and forth to the sea  to provide food for their young. As spring progresses, the trip gets progressively easier as the ice melts and the distance to the sea decreases, until finally the parents can leave the chicks to fend for themselves.

==International versions==
The style of the documentary differs considerably between the original French version and some of the international versions.

The original French-language release features a [[first-person narrative]] as if the story is being told by the penguins themselves. The narration alternates between a female ([[Romane Bohringer]]) and a male ([[Charles Berling]]) narrator speaking the alternate roles of the female and male penguin, and as the chicks are born, their narration is handled by child actor [[Jules Sitruk]]. This style is mimicked in some of the international versions. For example, in the Hungarian version, actors Ákos Kőszegi, Anna Kubik, and Gábor Morvai provide the voices of the penguins, and the German version as seen in German movie theaters (and in the televised broadcast in April 2007 on channel [[ProSieben]]) uses the voices of Andrea Loewig, Thorsten Michaelis, and Adrian Kilian for the "dubbed dialog" of the penguins. This style of narration is also used in the Danish and Cantonese DVD versions.

The English-language distribution rights were acquired at the Sundance documentary festival in January, 2005, by [[Adam Leipzig]] of [[National Geographic Films]], who had forged a distribution partnership with [[Warner Independent Pictures]].<ref>{{cite web|url=http://coverageink.blogspot.com/2011/02/interview-adam-leipzig_21.html|title=STORMBLOG: The Adam Leipzig Interview|work=coverageink.blogspot.com}}</ref> In contrast to the French version, their English release has a [[third-person narrative]] by a single voice, actor [[Morgan Freeman]]. Similarly, the Austrian channel ORF 1 used for their broadcast in April 2007, the alternate version available on the German "Special Edition" DVD which uses a documentary narration [[voiceover]] spoken by the German actor [[Sky Du Mont]]. Other releases' narrators include the Dutch version, narrated by Belgian comedian [[Urbanus]]; the Indian version, narrated in [[Hindi]] and English by Indian actor [[Amitabh Bachchan]], is titled ''Penguins: A Love Story''; the Polish version, narrated by Polish actor [[Marek Kondrat]]; and the Swedish version, narrated by Swedish actor [[Gösta Ekman]]. The [[Filipino language|Tagalog]] version is narrated by actress [[Sharon Cuneta]]; it is entitled ''Penguin, Penguin, Paano Ka Ginawa?'' (English: ''Penguin, Penguin, How Were You Made?'') with the English title as the subtitle. The Tagalog title is similar to that of a Philippine novel and film, ''[[Bata, Bata, Paano Ka Ginawa?]]'' (English: ''Child, Child, How Were You Made?'')

Another difference between the various international versions involves the music. The original version uses an original experimental soundtrack by electronic music composer [[Émilie Simon]], whereas the English-language version replaces it with an instrumental [[Film score|score]] by [[Alex Wurman]].

==Releases and responses==
The first screening of the documentary was at the [[Sundance Film Festival]] in the United States on 21 January 2005. It was released in [[France]] the next week, on 26 January, where it earned a 4-star rating from [[AlloCiné]], and was beaten at the box office only by ''[[The Aviator (2004 film)|The Aviator]]'' during its opening week.

The original French version was released in [[Quebec]]. Subsequently, an English-language version was released in the rest of [[North America]] on 24 June 2005, drawing praise from most critics who found it both informative and charming. It has received a 94% "fresh" rating with the consensus "Only the hardened soul won't be moved by this heartwarming doc" on the website [[Rotten Tomatoes]], which collects film reviews. The movie-going public apparently agreed with that assessment, as the documentary distinguished itself as one of the most successful documentaries of the season on a per-theatre basis: it became the second most successful documentary released in North America, after ''[[Fahrenheit 9/11]]'', grossing over $77 million in the United States and Canada (in nominal dollars, from 1982 to the present.) It grossed over $127 million worldwide.<ref>{{Cite web|url=http://www.boxofficemojo.com/movies/?id=marchofthepenguins.htm|title=March of the Penguins|publisher=[[Box Office Mojo]]|accessdate=2008-09-08}}</ref> It's the only movie from Warner Independent to be rated G by the MPAA.

The documentary was released on [[DVD]] in France on 26 July 2005. The DVD extras address some of the criticisms the documentary had attracted, most notably by reframing the documentary as a scientific study and adding facts to what would otherwise have been a family film. This [[DVD region code|Region 2]] release featured no English audio tracks or subtitles.

An extra on the DVD issue was the controversial 1948 [[Bugs Bunny]] cartoon ''[[Frigid Hare]]'', in which Bugs visits the [[South Pole]] and meets a young penguin fleeing an [[Eskimo]] hunter. The cartoon is not frequently seen because of its stereotypical depiction of the [[Inuit]] hunter, but it was included here uncut and uncensored. This is substituted in the European release with ''[[8 Ball Bunny]]''.

In November 2006, the documentary was adapted into a video game by DSI Games for the [[Nintendo DS]] and [[Game Boy Advance]] platforms. It features ''[[Lemmings (video game)|Lemmings]]''-like gameplay.

In 2007, a [[Direct-to-video|direct-to-DVD]] [[parody]] written and directed by [[Bob Saget]] called ''[[Farce of the Penguins]]'' was released. It is narrated by [[Samuel L. Jackson]] and features other stars providing voice-overs for the penguins. Although the film uses footage from actual nature documentaries about penguins, the parody was not allowed to include footage from ''March of the Penguins'' itself.<ref>{{cite web|url=http://suicidegirls.com/interviews/Bob+Saget/|title=Bob Saget by Anderswolleck - SuicideGirls|work=suicidegirls.com}}</ref>

==Production==
The DVD version includes a 54-minute film entitled ''Of Penguins and Men'' made by the film crew Laurent Chalet and Jérôme Mason about the filming of ''March of the Penguins''.<ref>
{{cite web|title=March of the Penguins, limited edition giftset|url=http://www.dvdmoviecentral.com/ReviewsText/march_of_the_penguins.htm|author=Ed Nguyen|accessdate=2011-01-31}}
</ref>

Director and film crew spent more than 13 months at the Dumont d'Urville Station, where the Institut polaire français Paul-Émile Victor is based. Although the penguins' meeting place, one of four in Antarctica, was known to be near, the day on which it occurs is not known, so they had to be ready every day. Fortunately, the gathering that year was huge – more than 1,200 penguins, compared with the norm of a few hundred.

For cameras to operate at −40°, they had to use film and to load all the film for the day, as it was impossible to reload outside. Because of the isolation from any film studios, remembering each shot was necessary  to ensure continuity and to make sure that all the necessary sequences were finished.<ref>[http://www.lemac.com.au/NEWS/Penguins.htm AATON Super 16 marches with the penguins]. Lemac.com.au.</ref><ref>[http://celebritywonder.ugo.com/movie/2005_March_of_the_Penguins_conversation_with_the_crew_jerome_maison_and_laurent_chalet.html March of the Penguins: interview with camera operators]. Celebritywonder.com. (accessed in 2012)</ref>

The main challenge of making the documentary was the weather with temperatures between −50 and −60°C (−58 and −76°F). At dawn, the film crew would spend half an hour putting on six layers of clothes, and on some days they could not spend more than three hours outside. They worked in winds with gusts up to 125 miles per hour, "which in some ways is worse than the cold temperatures" according to director Jaquet.<ref>
{{cite web|url=http://www2.scholastic.com/browse/article.jsp?id=8054|title=The Making of March of the Penguins|author=Jeffrey Rambo|publisher=scholastic.com|accessdate=2011-01-31}}
</ref>

==Political and social interpretations==
The documentary attracted some political and social commentary in which the penguins were viewed [[anthropomorphic]]ally as having similarities with, and even lessons for, human society. [[Michael Medved]] praised the documentary for promoting [[Social conservatism|conservative]] family values by showing the value of stable parenthood.<ref>{{Cite news|url=https://www.nytimes.com/2005/09/13/science/13peng.html?ex=1284264000&en=36effea48de3fa22&ei=5088&partner=rssnyt&emc=rss|title=March of the Conservatives: Penguin Film as Political Fodder |work=[[New York Times]] |author=Jonathan Miller|date=2005-09-13}}</ref> Medved's comments provoked responses by others, including [[Andrew Sullivan]],<ref>{{cite news|last=Sullivan| first=Andrew |authorlink=Andrew Sullivan| title=Not-so-picky penguins muddy the morality war| publisher=[[The Times]]| date=18 September 2005| url=http://www.timesonline.co.uk/tol/news/article567775.ece | location=London}}</ref> who pointed out that the penguins are not in fact [[monogamous]] for more than one year, in reality practicing [[serial monogamy]]. Matt Walker of ''[[New Scientist]]'' pointed out that many emperor penguin "adoptions" of chicks are in fact [[kidnappings]], as well as behaviours observed in other penguin species, such as ill treatment of weak chicks, prostitution, and ostracism of rare [[albino]] penguins.<ref>{{cite book| last=Walker| first=Matt | title=Bird-brained: a new film portrays penguins as paragons of virtue| quote=Beware, modeling human behavior on animals is fraught with danger.| publisher=[[New Scientist]]| date=1 October 2005| page= 17}}</ref> "For instance, while it is true that emperor penguins often adopt each other's chicks, they do not always do so in a way the moralisers would approve of."<ref>[http://find.galegroup.com/itx/start.do?prodId=SPJ.SP01 (Gale Cengage Learning, subscription or library card required) retrieved on 8 September 2008]</ref> Sullivan and Walker both conclude that trying to use animal behavior as an example for human behavior is a mistake.

The director, Luc Jacquet, has condemned such comparisons between penguins and humans. Asked by the ''[[San Diego Union Tribune]]'' to comment on the documentary's use as "a metaphor for family values – the devotion to a mate, devotion to offspring, monogamy, self-denial", Jaquet responded: "I condemn this position. I find it intellectually dishonest to impose this viewpoint on something that's part of nature. It's amusing, but if you take the monogamy argument, from one season to the next, the divorce rate, if you will, is between 80 to 90 percent... the monogamy only lasts for the duration of one reproductive cycle. You have to let penguins be penguins and humans be humans."<ref>{{Cite news |url=http://www.signonsandiego.com/uniontrib/20051106/news_1a06penguins.html |title='March' director marches to the tune of 'pure pleasure' |publisher=[[San Diego Union-Tribune]] |date=2005-11-06}}</ref>

Some of the controversy over this may be media driven. [[Rich Lowry]], editor of ''[[National Review]]'', reported in the magazine's blog that the [[BBC]] "have been harassing me for days over ''March of the Penguins'' ... about what, I'm not sure. I think to see if I would say on air that penguins are God's instruments to pull America back from the hell-fire, or something like that. As politely as I could I told her, 'Lady, they're just birds.'"<ref>Lowry, Rich. [http://corner.nationalreview.com/post/?q=ODhiY2VhNjRmYWUzZDlkOGIzYjI5ZGEzYWFkZWVkYTI= ''Oh no, the BBC''] 18 September 2005. {{webarchive |url=https://web.archive.org/web/20060102035223/ |date=January 2, 2006 |title=http://corner.nationalreview.com/05_09_18_corner-archive.asp#077353 }}</ref>

Another controversy involves those who feel that the emperor penguin's behavior can be viewed as an indication of [[intelligent design]], and those who consider it to be an example of [[natural selection|evolution by natural selection]] in action. [[Steve Jones (biologist)|Steve Jones]], professor of genetics at [[University College London]], is quoted as saying, "Supporters of intelligent design think that if they see something they don't understand, it must be God; they fail to recognise that they themselves are part of evolution. It appeals to ignorance, which is why there is a lot of it in American politics at the moment."<ref>{{cite news| url=https://www.theguardian.com/uk/2005/sep/18/usa.filmnews | location=London | work=The Guardian | title=How the penguin's life story inspired the US religious right | first=David | last=Smith | date=27 September 2005}}</ref> Author [[Susan Jacoby]] claims in her 2008 book, ''The Age of American Unreason'' (page 26), that the distributors of the movie deliberately avoided using the word "evolution" in order to avoid backlash from the American religious right, and writes, "As it happens, the emperor penguin is literally a textbook example, cited in college-level biology courses, of evolution by means of natural selection and random mutation. ... The financial wisdom of avoiding any mention of evolution was borne out at the box office ..."


==Awards==
* Won – [[Academy Award for Documentary Feature]]
* Won – [[American Cinema Editors]], Best Edited Documentary: Sabine Emiliani
* Nominated – [[BAFTA Awards]], Best Cinematography: [[Laurent Chalet]], Jérôme Maison
** Best Editing: Sabine Emiliani
* Won – [[Broadcast Film Critics Association]], Best Documentary
* Won – [[César Awards]], Best Sound (Meilleur son): Laurent Quaglio, Gérard Lamps
** Nominated, Best Editing (Meilleur montage): Sabine Emiliani
** Nominated, Best First Work (Meilleur premier film): Luc Jacquet
** Nominated, Best Music Written for a Film (Meilleure musique): Émilie Simon
* Won – [[National Board of Review]], Best Documentary
* Nominated – [[Online Film Critics Society Awards]], Best Documentary
* Nominated – [[Satellite Awards]], Outstanding Documentary DVD
** Nominated, Outstanding Motion Picture, Documentary
* Won – Southeastern Film Critics Association Awards, Best Documentary
* Nominated – [[Writers Guild of America]], Documentary Screenplay Award: Jordan Roberts (narration written by), Luc Jacquet (screenplay/story), Michel Fessler (screenplay)
* Won – [[Rotten Tomatoes]]' Golden Tomato Awards – Best Reviewed Documentary of 2005, beating ''[[Murderball (documentary)|Murderball]]'' and ''[[Enron: The Smartest Guys in the Room]]''
* Won – London's Favourite French Film in 2005
* Won – [[Victoires de la musique]], Original cinema/television soundtrack of 2006

==See also==
* ''[[Ape and Super-Ape]]'', a 1972 Dutch documentary film by [[Bert Haanstra]] about the differences and similarities between humans and animals, which also has extensive footage about the life of penguins on Antarctica, almost 30 years before ''March Of The Penguins'' was made.  <ref>http://cinemagazine.nl/bij-de-beesten-af-1972-recensie/</ref>

==References==
{{reflist|30em}}

==Further reading==
* [http://film.guardian.co.uk/news/story/0,,1572659,00.html "How the penguin's life story inspired the US religious right"] – David Smith, Guardian Unlimited Film News, 18 September 2005
* [https://web.archive.org/web/20070927225712/http://www.imago.org/main/public_html/_htm_news/2007/n35.htm "The Unfortunate experience of a Director of Photography"]  – Amelie Zola, IMAGO, European Federation of Cinematographers, March 2007

==External links==
{{wikinews|2006 Oscars handed out at Kodak Theatre}}
* {{official website|http://empereur.luc-jacquet.com/index_flash_ang.htm}}
* {{IMDb title|0428803|March of the Penguins}}
* {{tcmdb title|613088|March of the Penguins}}
* {{allrovi movie|328540|March of the Penguins}}
* {{mojo title|marchofthepenguins|March of the Penguins}}
* {{Rotten Tomatoes|march_of_the_penguins|March of the Penguins}}
* {{metacritic film|march-of-the-penguins|March of the Penguins}}
* {{tvtropes|Film/MarchOfThePenguins}}

{{AcademyAwardBestDocumentaryFeature2001-2020}}
{{Broadcast Film Critics Association Award for Best Documentary Film}}

{{Authority control}}

{{DEFAULTSORT:March Of The Penguins}}
[[Category:2005 films]]
[[Category:2000s documentary films]]
[[Category:French films]]
[[Category:French documentary films]]
[[Category:Best Documentary Feature Academy Award winners]]
[[Category:Documentary films about Antarctica]]
[[Category:Documentary films about nature]]
[[Category:Films about penguins]]
[[Category:French independent films]]
[[Category:Warner Independent Pictures films]]
[[Category:Warner Bros. films]]