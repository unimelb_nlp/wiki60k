{{Infobox military conflict
|conflict=Battle of Al Mansurah
|partof=the [[Seventh Crusade]]
|image=[[File:Mansura.jpg|300px]]
|caption= Battle of Al Mansurah
|date=8–11 February 1250
|place=[[Al Mansurah]], [[Egypt]]
|result= Decisive [[Ayyubid]] victory
|combatant1=[[File:Flag of Ayyubid Dynasty.svg|25px]] [[Ayyubid dynasty|Ayyubid]]
*[[Bahris]]
|combatant2=[[Crusade]]rs
|commander1= [[Image:Flag of Ayyubid Dynasty.svg|border|23px]] [[Qutuz]]<br>[[Image:Flag of Ayyubid Dynasty.svg|border|23px]] Fakhr-ad-Din Yusuf {{KIA}}<br>[[Image:Flag of Ayyubid Dynasty.svg|border|23px]] [[Baibars]]<br>[[Image:Flag of Ayyubid Dynasty.svg|border|23px]] [[Faris ad-Din Aktai]]  
|commander2=[[File:France Ancient Arms.svg|22px]] [[Louis IX of France|Louis IX]]<br>[[File:Armoiries Guillaume de Saunhac.svg|22px]] [[Guillaume de Sonnac]]{{KIA}}<br>[[File:Blason Alphonse Poitiers.png|22px]] [[Alphonse de Poitiers]]<br>[[File:Artois Arms.svg|22px]] [[Robert of Artois|Robert d'Artois]]{{KIA}}<br>[[File:Longespee.svg|22px]] [[William II Longespée]] 
|strength1=4600 Cavalry including Mamluks, a much larger number of infantry probably 6000 or more and Egyptian reserves <ref>Marshall,Christopher, Warfare in the Latin East 1192-1291  p. 149</ref>
|strength2=Several hundred knights, Several thousand infantry <ref>Marshall,Christopher, Warfare in the Latin East 1192-1291  p. 149</ref>
|casualties1= Heavy <ref>Marshall,Christopher, Warfare in the Latin East 1192-1291  p. 167</ref>
|casualties2=300 knights, 80 templars, and a very large number of infantry.<ref>Marshall,Christopher, Warfare in the Latin East 1192-1291  p. 149</ref>
}}
{{Campaignbox Seventh Crusade}}
{{Campaignbox Crusades Battles}}
The '''Battle of Al Mansurah''' was fought from February 8 to February 11, 1250, between [[Crusades|Crusaders]] led by [[Louis IX of France|Louis IX]], King of [[Kingdom of France|France]], and [[Ayyubid dynasty|Ayyubid]] forces led by [[Emir]] Fakhr-ad-Din Yusuf, [[Faris ad-Din Aktai]] and [[Baibars|Baibars al-Bunduqdari]].

==Background==
By the mid-13th century, the Crusaders became convinced that [[History of Muslim Egypt|Egypt]], the heart of Islam's forces and arsenal,<ref>[[Arnold Toynbee|Toynbee]], p. 447.</ref> was an obstacle to their ambition to capture [[Jerusalem]], which they had lost for the second time in 1244. In 1245, during the [[First Council of Lyon]], [[Pope Innocent IV]] gave his full support to the [[Seventh Crusade]] being prepared by [[Louis IX of France|Louis IX]], King of France.

The goals of the Seventh Crusade were to destroy the Ayyubid dynasty in Egypt and [[Syria]], and to recapture Jerusalem.  The Crusaders asked the [[Mongols]] to become their allies against the Muslims,<ref>Runciman, pp. 260-263. D. Wilkinson, Paragraph: THE MONGOLS AND THE WEST.  See also [[Franco-Mongol alliance]].</ref> the Crusaders attacking the [[Muslim world|Islamic world]] from west, and the Mongols attacking from the east. [[Güyük]], the Great Khan of the Mongols, told the Pope's envoy that the Pope and the kings of Europe should submit to the Mongols.<ref>The message was handed to the pope's [[Franciscan]] emissary [[Giovanni da Pian del Carpine]]. [http://asv.vatican.va/en/doc/1246.htm The document is preserved in the Vatican secret archive.] You must say with a sincere heart: "We will be your subjects; we will give you our strength". You must in person come with your kings, all together, without exception, to render us service and pay us homage. Only then will we acknowledge your submission. And if you do not follow the order of God, and go against our orders, we will know you as our enemy." <span style="font-size:87%;">—Letter from Güyük to Pope Innocent IV, 1246.</span> Lord of Joinville, pp. 249-259.</ref>

The ships of the Seventh Crusade, led by King Louis's brothers, [[Charles of Anjou|Charles d'Anjou]] and [[Robert of Artois|Robert d'Artois]], sailed from [[Aigues-Mortes]] and [[Marseille]] to [[Cyprus]] during the autumn of 1248, and then on to Egypt. The ships entered Egyptian waters and the troops of the Seventh Crusade disembarked at [[Damietta]] in June 1249. Louis IX sent a letter to as-Salih Ayyub.<ref>"As you know I am the ruler of the Christian nation I do know you are the ruler of the Muhammadan nation. The people of [[Andalusia]] give me money and gifts while we drive them like cattle. We kill their men and we make their women widows. We take the boys and the girls as prisoners and we make houses empty. I have told you enough and I have advised you to the end, so now if you make the strongest oath to me and if you go to christian priests and monks and if you carry kindles before my eyes as a sign of obeying the cross, all these will not persuade me from reaching you and killing you at your dearest spot on earth. If the land will be mine then it is a gift to me. If the land will be yours and you defeat me then you will have the upper hand. I have told you and I have warned you about my soldiers who obey me. They can fill open fields and mountains, their number like pebbles. They will be sent to you with swords of destruction." Letter from Louis IV to as-Salih Ayyub - (Al-Maqrizi, p. 436/vol.1).</ref> Emir Fakhr ad-Din Yusuf, the commander of the Ayyubid garrison in Damietta, retreated to the camp of the Sultan in Ashmum-Tanah,<ref>Ashmum-Tanah, now town of Dakahlia - Al-Maqrizi, note p. 434/vol. 1.</ref> causing a great panic among the inhabitants of Damietta, who fled the town, leaving the bridge that connected the west bank of the [[Nile]] with Damietta intact. The Crusaders crossed over the bridge and occupied Damietta, which was deserted.<ref>Al-Maqrizi, p. 438/vol.1.</ref> The fall of Damietta caused a general emergency (called al-Nafir al-Am النفير العام) to be declared, and locals from Cairo and from all over Egypt moved to the battle zone.<ref>Al-Maqrizi, p. 446/vol. 1, p. 456/vol. 1.</ref><ref>Ibn Taghri, pp. 102-273/ vol. 6.</ref>  For many weeks, the Muslims used [[Guerrilla warfare|guerrilla tactics]] against the Crusader camps; many of the Crusaders were captured and sent to [[Cairo]].<ref>Al-Maqrizi, p. 447/vol. 1.</ref> As the Crusader army was strengthened by the arrival of [[Alphonse of Poitiers|Alphonse de Poitiers]], the third brother of King Louis IX, at Damietta, the Crusaders were encouraged by the news of the death of the Ayyubid [[Sultan]], [[as-Salih Ayyub]]. The Crusaders began their march towards Cairo. [[Shajar al-Durr]], the widow of the dead Sultan, concealed the news for some time and sent [[Faris ad-Din Aktai]] to [[Hasankeyf]] to recall [[Al-Muazzam Turanshah|Turanshah]], the son and heir, to ascend the throne and lead the Egyptian army.

==Battle==
The Crusaders approached the battle by the canal of Ashmum (known today by the name Albahr Alsaghir), which separated them from the Muslim camp. An Egyptian showed the Crusaders the way to the canal shoals.  The Crusaders, led by [[Robert I, Count of Artois|Robert of Artois]], crossed the canal with the Knights Templar and an English contingent led by [[William II Longespée|William of Salisbury]], launching a surprise assault on the Egyptian camp in Gideila, two miles (3&nbsp;km) from [[Mansoura, Egypt|Al Mansurah]],<ref>[https://maps.google.com/maps?f=q&hl=en&geocode=&q=Mansura,+Egypt&sll=37.0625,95.677068&sspn=44.47475,108.457031&ie=UTF8&t=h&ll=31.045581,31.382618&spn=0.09442,0.21183&z=13 Gideila and Al Mansurah on map.]</ref> and advancing toward the royal palace in [[Mansoura, Egypt|Al Mansurah]]. The leadership of the Egyptian forces passed to the [[Mamluk]]s Faris Ad-Din Aktai and Baibars al-Buduqdari who contained the attack and reorganized the Muslim forces. This was the first appearance of the Mamluks as supreme commanders inside Egypt.<ref>Baibars led the Egyptian army at the Battle of La Forbie east of Gaza in 1244. See also [[Battle of La Forbie]].</ref>  Shajar al-Durr, who had full control of Egypt, agreed with Baibars' plan to defend [[Mansoura, Egypt|Al Mansurah]].<ref>Qasim, p.18</ref>  Baibars ordered the  gate be opened to let the Crusaders enter the town. The crusaders rushed in, thinking the town deserted, only to find themselves trapped inside. The Crusaders were besieged from all directions by Egyptian forces and the local population, and they took heavy losses. [[Robert I, Count of Artois|Robert of Artois]], who took refuge in a house,<ref>Lord of Joinville, 110, part II.</ref><ref>Asly, p. 49.<br>Skip Knox, ''Egyptian Counter-attack, The Seventh Crusade''.</ref> and [[William II Longespée|William of Salisbury]] were both killed along with most of the [[Knights Templar]]. Only five Templar Knights escaped alive.<ref>According to Matthew Paris, only 2 Templars, 1 Hospitaller and one ‘contemptible person’ escaped. Matthew Paris, ''LOUIS IX`S CRUSADE'', p. 14/ Vol. 5.</ref> The Crusaders retreated to their camp in disorder, and surrounded it with a ditch and wall. Early on the morning of February 11, the Muslim forces launched a devastating offensive against the Frankish camp. On February 27, the new sultan Turanshah arrived in [[Mansoura, Egypt|Al Mansurah]] to lead the Egyptian army, and the death of as-Salih Ayyub was formally announced in Egypt.<ref>Turanshah did not go to Cairo, he was enthroned in al-Salihiya and went straight to Al Mansurah. - Al-Maqrizi, pp. 449-450/ vol. 1.</ref>  Ships were transported overland and dropped in the Nile behind the Crusader ships blocking the reinforcement line from Damietta. The Egyptians used [[Greek fire]], destroying and seizing many Crusader supply vessels. The besieged Crusaders soon began suffering from famine and disease. Some Crusaders deserted to the Muslim side.<ref>[[Matthew Paris]], ''LOUIS IX`S CRUSADE'', p. 108 / Vol. 5.</ref><ref>Al-Maqrizi, p. 446/vol. 1.</ref>

Despite being overwhelmed and ultimately defeated, [[Louis IX of France|King Louis IX]] tried to negotiate with the Egyptians, offering the surrender of the Egyptian port of Damietta in exchange for Jerusalem and a few towns on the Syrian coast. The Egyptians rejected the offer, and the Crusaders retreated to Damietta under cover of darkness on April 5, followed closely by the Muslim forces. At the subsequent [[Battle of Fariskur]], the last major battle of the Seventh Crusade, the Crusader forces were annihilated and King Louis IX was captured on April 6. Meanwhile, the Crusaders were circulating false information in Europe, claiming that [[Louis IX of France|King Louis IX]] defeated the Sultan of Egypt in a great battle, and Cairo had been betrayed into Louis's hands.<ref>Lord of Joinville, 170, part II.</ref><ref>False rumours from Egypt: letters from the [[bishop of Marseille]] and certain Templars spread the rumour that Cairo and Babylon have been captured and the fleeing Saracens have left Alexandria undefended. - Matthew Paris, note. p. 118 / Vol. 5. ''LOUIS IX`S CRUSADE 1250''</ref>  Later, when the news of Louis IX's capture and the French defeat reached France, the [[Shepherds' Crusade, 1251|Shepherds' Crusade]] movement occurred in France.<ref>Matthæi Parisiensis, pp. 246-53.</ref>

[[File:Louis-ix.jpg|thumb|220px|left|Louis IX was taken prisoner.]]

==Aftermath==
According to medieval Muslim historians, 15,000 to 30,000 French fell on the battlefield and thousands were taken prisoners.<ref>[[Al-Maqrizi]], pp. 455-56/ vol.1 <br>[[Abu al-Fida]], pp. 66-87/year 648H.<br>Ibn Taghri, pp.102-273/ vol.6</ref> [[Louis IX of France]] was captured in the nearby village of Moniat Abdallah 
(now Meniat el Nasr), chained and confined in the house of Ibrahim Ibn Lokman, the royal chancellor, and under the guard of a eunuch named Sobih al-Moazami.<ref>Though Louis IX, a king, was treated well, he was chained and put under the guard of a slave  which was not the custom.</ref>  The king's brothers, [[Charles I of Naples|Charles d'Anjou]] and [[Alphonse, Count of Poitiers|Alphonse de Poitiers]], were taken prisoner at the same time, and were carried to the same house with other French nobles. The sultan provided for their subsistence. A camp was set up outside the town to shelter the rest of the prisoners. [[Louis IX of France|Louis IX]] was ransomed for 400,000 dinars. After pledging not to return to Egypt, Louis surrendered Damietta and left for [[Acre, Israel|Acre]] with his brothers and 12,000 war prisoners whom the Egyptians agreed to release.<ref>Many prisoners were executed.  Al-Maqrizi, p. 455/ vol.1.- Ibn Taghri, pp. 102-273/vol. 6. - The number 12,000 included prisoners from previous battles. Al-Maqrizi, p. 460/ vol.1</ref> 

The battle of Al Mansurah was a source of inspiration for writers and poets of that time. One of the satiric poems ended with the following verses:
"''If they (the Franks) decide to return to take revenge or to commit a wicked deed, tell them :The house of Ibn Lokman is intact, the chains still there as well as the eunuch Sobih''". <span style="font-size:87%;">—from stanza by Jamal ad-Din ibn Matruh.</span> <ref>Al-Maqrizi, p. 460/ vol. 1.</ref>

The name of Al Mansurah (Arabic: "the Victorious") that dates from an earlier period<ref>The name Al Mansurah was first used by [[al-Kamil]] for his camp during the siege of Damietta ([[Fifth Crusade]]) in 1219. Skip Knox, Mansourah, The Seventh Crusade. It was named al-Madinah al-Mansurah (the victorious town). Al-Maqrizi, al-Mawaiz wa al-'i'tibar, p. 373/ vol.1</ref> was consolidated after this battle.  The city still holds the name of Al Mansurah today, as the capital of the Egyptian governorate, [[Dakahlia Governorate|Daqahlia]]. The National Day of [[Dakahlia Governorate|Daqahlia]] [[Governorates of Egypt|Governorate]] (capital Al Mansurah) on February 8, marks the anniversary of the defeat of Louis IX in 1250. The house of Ibn Lokman, which is now the only museum in Al Mansurah, is open to the public and houses articles that used to belong to the French monarch, including his personal thirteenth century toilet.

[[File:LetterGuyugToInnocence.jpg|thumb|100px|The 1246 letter of [[Güyük]] to Pope Innocent IV.]]

==Historical consequence==
{{unreferenced section|date=February 2013}}
The Seventh Crusade's defeat in Egypt in 1250 marked a turning point for all the existing regional parties. Egypt again proved to be Islam's stronghold. Western kings, except Louis IX, lost interest in launching new crusades. The Seventh Crusade was the last major crusade against Egypt and the Crusaders never recaptured Jerusalem.

Shortly after the defeat of the Seventh Crusade, the Ayyubid Sultan Turanshah was assassinated at Fariskur. The Mamluks, those who defended Al Mansurah and prevented Louis IX from advancing to Cairo, became the ruling power in Egypt, ending the Ayyubid rule in that country. The southern and eastern [[Mediterranean basin]] was divided among four main dominions. Mamluk Egypt, Ayyubid Syria, Franks of Acre with their Christian strongholds on the Syrian coast, and the [[Levant]]ine Christian [[Armenian Kingdom of Cilicia]]. The Ayyubids of Syria clashed with the Mamluks of Egypt. The Franks, the Cilician Armenians and the [[Principality of Antioch]] formed a western Christian alliance.

==See also==
*[[Berke–Hulagu war]]
*[[Battle of Fariskur]]

==Notes==
{{reflist|2}}

==References==
{{colbegin}}
*[[Abu al-Fida]], Tarikh Abu al-Fida, ''[[The Concise History of Humanity or Chronicles|The Concise History of Humanity]]''
*[[Al-Maqrizi]], ''Al Selouk Leme'refatt Dewall al-Melouk, Dar al-kotob'', 1997. In English: Bohn, Henry G., ''The Road to Knowledge of the Return of Kings, Chronicles of the Crusades'', AMS Press, 1969.
*Al-Maqrizi, al-Mawaiz wa al-'i'tibar bi dhikr al-khitat wa al-'athar, Matabat aladab, Cairo 1996, ISBN 977-241-175-X. In French: Bouriant, Urbain, Description topographique et historique de l'Egypte, Paris 1895
*Asly, B., ''al-Muzafar Qutuz'', Dar An-Nafaes Publishing, Beirut 2002, ISBN 9953-18-051-2
*Bournoutian, George A., ''A Concise History of the Armenian People: From Ancient Times to the Present'', Mazda Publishers, 2002
*David Wilkinson, ''Studying the History of Intercivilizational Dialogues'', presented to United Nation University, Tokyo/Kyoto 2001
*Dawson, Christopher, ''The Mongol Mission'', London: Sheed and Ward, 1955
*Hassan. O, ''Al-Zahir Baibars'', Dar al-Amal 1997
*[[Ibn Taghri]], al-Nujum al-Zahirah Fi Milook Misr wa al-Qahirah, al-Hay'ah al-Misreyah 1968
*Michaud, Yahia (Oxford Centre for Islamic Studies) ''Ibn Taymiyya'', Textes Spirituels I-XVI 2002
*Qasim, Abdu Qasim Dr., ''Asr Salatin Al-Mamlik'' (Era of the Mamluk Sultans), Eye for human and social studies, Cairo, 2007
*Rachewitz, I, ''Papal envoys to the Great khans'', London: Faber and Faber, 1971
*[[Steven Runciman|Runciman, Steven]] ''A history of the Crusades'' 3. Penguin Books, 1987
*Sadawi. H, ''Al-Mamalik'', Maroof Ikhwan, Alexandria.
*Setton, Kenneth (editor), ''A History of the Crusades (II) The Later Crusades 1189-1311'', 1969
*Skip Knox, Dr. E.L., ''The Crusades, Seventh Crusade, A college course on the Crusades'', 1999
*Shayal, Jamal, Prof. of Islamic history, ''Tarikh Misr al-isalamiyah'' (History of Islamic Egypt), dar al-Maref, Cairo 1266,  ISBN 977-02-5975-6
*''The chronicles of [[Matthew Paris]]'' (Matthew Paris: Chronica Majora) translated by Helen Nicholson, 1989
*Matthæi Parisiensis, monachi Sancti Albani, ''Chronica majora'' by Matthew Paris, Roger, Henry Richards, Longman & co. 1880.
*''The New Encyclopædia Britannica'', Macropædia, H. H. Berton Publisher, 1973–74
*''The Memoirs of the Lord of Joinville'', translated by Ethel Wedgwood, 1906
*[[Arnold J. Toynbee|Toynbee, Arnold J.]], ''Mankind and mother earth'', Oxford University Press, 1976
*www.historyofwar.org
{{colend}}
==External links==
*[https://maps.google.com/maps?f=q&hl=en&geocode=&q=Mansura,+Egypt&sll=37.0625,-95.677068&sspn=44.47475,108.457031&ie=UTF8&t=h&ll=31.045581,31.382618&spn=0.09442,0.21183&z=13 Map of Mansura]

{{coord|31.0456|31.3826|type:event_source:enwiki-googlemaplink|display=title}}

{{DEFAULTSORT:Battle Of Al Mansurah}}
[[Category:Conflicts in 1250]]
[[Category:Battles of the Seventh Crusade|Al Mansurah]]
[[Category:Battles involving Egypt|Al Mansurah]]
[[Category:Battles involving the Ayyubids|Al Mansurah]]
[[Category:1250 in Egypt]]
[[Category:Battles involving France|Mansurah]]
[[Category:February 1250 events]]