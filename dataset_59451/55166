{{Orphan|date=April 2016}}
{{infobox journal
| title = The AAPS Journal
| cover =
| caption =
| former_name =
| abbreviation = AAPS J.
| discipline = [[Pharmaceutical sciences]]
| editor = Ho-Leung Fung
| publisher = [[Springer Science+Business Media]] on behalf of the [[American Association of Pharmaceutical Scientists]]
| country =
| history = 1999–present
| frequency = Bimonthly
| openaccess =
| license =
| impact = 3.819
| impact-year = 2015
| ISSN =
| eISSN = 1550-7416
| CODEN = AJAOB6
| JSTOR =
| LCCN = 2004212722
| OCLC = 55603053
| website = http://www.springer.com/biomed/pharmacology+%26+toxicology/journal/12248
| link2 = http://link.springer.com/journal/volumesAndIssues/12248
| link2-name = Online archive
| link1 = http://www.aaps.org/AAPSJournal/
| link1-name = Journal page at society website
}}
'''''The AAPS Journal''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering all aspects of [[pharmaceutical sciences]]. It is published by [[Springer Science+Business Media]] on behalf of the [[American Association of Pharmaceutical Scientists]]. The [[editor-in-chief]] is Ho-Leung Fung ([[State University of New York at Buffalo]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
*[[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-04-28}}</ref>
*[[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-04-28}}</ref>
*[[CSA (database company)|CSA databases]]
*[[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2016-04-28}}</ref>
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101223209 |title=The AAPS Journal |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-04-28}}</ref>
*[[ProQuest|Proquest databases]]
*[[Science Citation Index Expanded]]<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-04-28}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.799.<ref name=WoS>{{cite book |year=2015 |chapter=The AAPS Journal |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==See also==
{{portal|Pharmacy and Pharmacology}}
* [[List of pharmaceutical sciences journals]]

==References==
{{reflist}}

==External links==
*{{Official website|http://www.springer.com/biomed/pharmacology+%26+toxicology/journal/12248}}

{{DEFAULTSORT:AAPS Journal, The}}
[[Category:Academic journals associated with learned and professional societies of the United States]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Online-only journals]]
[[Category:Pharmacology journals]]
[[Category:Publications established in 1999]]
[[Category:Springer Science+Business Media academic journals]]