{{Use dmy dates|date=March 2015}}
{{Use Australian English|date=March 2015}}

{{Infobox museum
| name                = Backwoods Gallery
| native_name         = 
| native_name_lang    = 
| image               = 
| imagesize           = 250
| caption             = 
| alt                 = 
| map_type            = Australia
| coordinates         = {{coord|37|47|52.566|S|144|59|9.88|E|region:AU-VIC|display=inline,title}}
| established         = {{Start date|df=yes|2010}}
| dissolved           = 
| location            = [[Collingwood, Victoria]], , [[Australia]]
| type                = {{Plainlist|
* [[Contemporary art gallery|Contemporary art]]
* [[Street art]]
* [[Urban art]]
* [[Graffiti art]]
}}
| accreditation       = 
| key_holdings        = 
| collections         = 
| collection          = 
| visitors            = 
| director            = Alexander Mitchell
| president           = 
| curator             = Sean Carroll
| owner               =
| publictransit       =
| car_park            = 
| network             = 
| website             = {{URL|www.backwoodsgallery.com}}
}}
'''Backwoods Gallery''' is a [[contemporary art]] gallery located in the [[Melbourne]] suburb of [[Collingwood, Victoria|Collingwood]], [[Australia]].<ref>{{cite web|title=Backwoods Gallery Art, Collingwood, Galleries, Painting & Drawing|url=http://www.au.timeout.com/melbourne/art/venues/1325/backwoods-gallery|publisher=Time Out|accessdate=19 March 2012}}</ref> Founded in August 2010 by Alexander Mitchell<ref>{{cite web|last=Noble |first=Sasha |title=Backwoods Gallery's Alexander Mitchell |url=http://everguide.com.au/arts-and-culture/exhibitions/interview/backwoods-gallery-alexander-mitchell.aspx |publisher=Everguide |accessdate=7 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121110032542/http://everguide.com.au:80/arts-and-culture/exhibitions/interview/backwoods-gallery-alexander-mitchell.aspx |archivedate=10 November 2012 |df=dmy }}</ref>  the gallery exhibits Australian and international artists with a focus on urban contemporary art, [[street art]] and illustration.

In December 2013, [[Complex magazine]] listed Backwoods Gallery in the top ten "Street Art Galleries You Should Know About".<ref>{{cite web|last=Castro|first=Vanessa|title=Street Art Galleries You Should Know About|url=http://www.complex.com/art-design/2013/12/street-art-galleries/backwoods-in-australia|publisher=complex magazine|accessdate=23 December 2013}}</ref>

==Artists==
Backwoods Gallery represents Stanislava Pinchuk (MISO), Hiroyasu Tsuri (TwoOne), Lush, [[James Reka]], Stabbs, Jun Inoue, Kami & Sasu, Shohei Otomo and Usugrow.

==Exhibitions==

===Exhibitions 2010===

{| class="wikitable" width="600"
! scope="col" width="30%" | Dates
! scope="col" width="35%" | Exhibition
! scope="col" width="35%" | Artist(s)
|-
| 25/09/2010 - 10/10/2010 || Shake That Ass || Yasumasa Yonehara
|-
| 05/11/2010 - 13/11/2010 || Too Big To Be Human || Petro
|-
| 20/11/2010 - 04/12/2010 || Omni <ref>{{cite web|title=Kami and Sasu "Omni" Mini-Documentary|url=http://www.juxtapoz.com/current/video-kami-and-sasu-omni|publisher=Juxtapoz|accessdate=22 February 2011}}</ref><ref>{{cite web|title=Kami and Sasu"Omni"|url=http://hidden-champion.net/blog/mats/2011/02/kami-and-sasu-omni.html|publisher=Hidden Champion|accessdate=22 February 2011|language=Japanese}}</ref> || Kami and Sasu
|-
| 17/17/2010 - 23/12/2010 || SALE! || Lush
|}

===Exhibitions 2011===

{| class="wikitable" width="600"
! scope="col" width="30%" | Dates
! scope="col" width="35%" | Exhibition
! scope="col" width="35%" | Artist(s)
|-
| 25/03/2011 - 11/04/2011 || Down Low, Too Slow <ref>{{cite web|last=Spoor|first=Nathan|title=James Reka - Down Low, Too Slow.|url=http://hifructose.com/2011/03/24/james-reka-down-low-too-slow/|publisher=Hi Fructose|accessdate=22 March 2011}}</ref><ref>{{cite web|last=Irvine |first=Sean |title=Interview With Melbourne Artist Reka |url=http://www.lifelounge.com.au/art-and-design/interview/interview-with-melbourne-artist-reka.aspx |publisher=Life Lounge |accessdate=25 March 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110326192246/http://www.lifelounge.com.au:80/art-and-design/interview/interview-with-melbourne-artist-reka.aspx |archivedate=26 March 2011 |df=dmy }}</ref> || James Reka
|-
| 15/04/2011 - 20/04/2011 || New Yellow 01  || Jun Inoue
|-
| 20/05/2011 - 10/06/2011 || If We’re Going, Then Lets Go || Ghostpatrol
|-
| 17/06/2011 - 26/06/2011 || l’inconnue de la rue || Rone
|-
| 15/07/2011 - 30/07/2011 || Oriental Elements<ref>{{cite web|title=Openings: Usugrow & Toshikazu – "Oriental Elements" @ Backwoods Gallery|url=http://arrestedmotion.com/2011/07/openings-usugrow-toshikazu-oriental-elements-backwoods-gallery/|publisher=Arrested Motion|accessdate=26 July 2011}}</ref>  || Usugrow, Toshikazu Nozaka
|-
| 19/08/2011 - 30/08/2011 || Inner Demons II  || Meggs
|-
| 09/09/2011 - 25/09/2011 || You Have No Chance With Coincidence || Yuske Imai
|-
| 14/10/2011 - 30/10/2011 || Lite Works || Stabbs
|-
| 18/11/2011 - 05/12/2011 || Momoka || Tanja "Misery" Jade
|-
| 09/12/2011 - 12/12/2011 || Another Shithouse "Art" Show || Lush
|}

===Exhibitions 2012===

{| class="wikitable" width="600"
! scope="col" width="30%" | Dates
! scope="col" width="35%" | Exhibition
! scope="col" width="35%" | Artist(s)
|-
| 10/02/2012 - 10/02/2012 || Oath Of Armageddon || French
|-
| 18/02/2012 - 20/02/2012 || Bode Australian Tour || [[Mark Bode]]
|-
| 02/03/2012 - 16/02/2012 || Primary Suspects  || James Reka
|-
| 24/04/2012 - 06/05/2012 || Natural Progression || Beastman
|-
| 01/05/2012 - 16/06/2012 || Cosmic Scale And The Super Future || Ghostpatrol
|-
| 20/06/2012 - 22/06/2012 || Open Studio <ref>{{cite web|last=Giuliani|first=Jasmine|title=ARTS REVIEW: REKA OPEN STUDIO - BACKWOODS GALLERY, MELBOURNE (20.06.12)|url=http://www.theaureview.com/melbourne/arts-review-reka-open-studio-backwoods-gallery-melbourne-20-06-12|accessdate=23 June 2012}}</ref>  || James Reka
|-
| 29/06/2012 - 10/07/2012 || Fall From Grace || Rone
|-
| 03/08/2012 - 12/08/2012 || Jet Babies || Stephen Ives
|-
| 17/08/2012 - 26/08/2012 || Decorating The Apocalypse || [[Fred Fowler]]
|-
| 14/09/2012 - 30/09/2012 || Seven Samurai <ref>{{cite web|last=Schonberger|first=Nick|title=Street Artist TwoOne Shows Us How To Create A Limited Print|url=http://www.complex.com/art-design/2012/09/street-artist-twoone-shows-us-how-to-create-a-limited-print|publisher=Complex Magazine|accessdate=6 September 2012}}</ref> || Hiroyasu Tsuri (TwoOne)
|-
| 07/09/2012 - 30/09/2012 || Hakuchi || Shohei Otomo
|-
| 12/10/2012 - 14/10/2012 || A Study Of Hands <ref>{{cite web|title=A Study Of Hands At Backwoods Gallery|url=http://arrestedmotion.com/2012/09/upcoming-a-study-of-hands-backwoods-gallery/|publisher=ArrestedMotion.com|accessdate=19 September 2012}}</ref>  || [[Anthony Lister]], [[Ashley Wood]], [[Dave Kinsey]], Haroshi, James Reka, James Greenaway, Ian Strange, [[Mark Bode]], Shohei Otomo, Hiroyasu Tsuri, Usugrow, Yuske Imai
|-
| 19/10/2012 - 28/10/2012 || Truth In Myth II || Meggs
|-
| 16/11/2012 - 19/11/2012 || Neds Maps and Other Outback Stories || [[Mark Bode]]
|-
| 30/11/2012 - 16/12/2012 || Carrion <ref>{{cite web|title=BACKWOODS GALLERY PRESENTS "CARRION": A NEW EXHIBITION BY ROA|url=http://www.juxtapoz.com/street-art/backwoods-gallery-presents-qcarrionq-a-new-exhibition-by-roa|publisher=Juxtapoz|accessdate=25 November 2012}}</ref><ref>{{cite web|last=Ross|first=Annabel|title=Artist makes no bones about passion|url=http://www.smh.com.au/entertainment/art-and-design/artist-makes-no-bones-about-passion-20121130-2amh8.html|accessdate=1 December 2012}}</ref>  || [[ROA (artist)|ROA]]
|}

===Exhibitions 2013===

{| class="wikitable" width="600"
! scope="col" width="30%" | Dates
! scope="col" width="35%" | Exhibition
! scope="col" width="35%" | Artist(s)
|-
| 01/01/2013 - 28/02/2013 || Artist Residency <ref>{{cite web|title=The Melbourne based street artist returns home|url=http://www.acclaimmag.com/arts/art-reka-one-backwoods-residency-janfeb-2013/|publisher=Acclaim Magazine|accessdate=21 February 2013}}</ref> || James Reka
|-
| 01/03/2013 - 03/03/2013 || The Evolution Of A Graffiti Shitcunt || Lush
|-
| 31/03/2013 - 15/03/2013 || Organized Chaos || Slicer
|-
| 19/04/2013 - 05/05/2013 || Ecstasy In the Abyss || Shida
|-
| 24/05/2013 - 09/06/2013 || Hero || Stephen Ives
|-
| 12/07/2013 - 21/07/2013 || Define Nothing <ref>{{cite web|last=Johnston|first=Chris|title=Talking art in voice of the hare-brained|url=http://www.smh.com.au/entertainment/talking-art-in-voice-of-the-harebrained-20130713-2px3x.html|publisher=Sydney Morning Herald|accessdate=14 July 2013}}</ref>  || Hiroyasu Tsuri (TwoOne)
|-
| 15/08/2013 - 25/08/2013 || Take Me To Your Leader <ref>{{cite web|last=Ross|first=Annabel|title=Portrait of a lady focus of artist's bodily fascination|url=http://www.theage.com.au/entertainment/art-and-design/portrait-of-a-lady-focus-of-artists-bodily-fascination-20130815-2rz7m.html|publisher=The Age|accessdate=16 August 2013}}</ref>  || Shohei Takasaki
|-
| 20/09/2013 - 28/09/2013  || Coalescence || DrewFunk, Otis Chamberlain
|-
| 18/10/2013 - 03/11/2013 || Bright Night Sky <ref>{{cite web|last=Johnston|first=Chris|title=Joining the dots to sublime effect|url=http://www.theage.com.au/entertainment/art-and-design/joining-the-dots-to-sublime-effect-20131015-2vkom.html|publisher=The Age|accessdate=16 October 2013}}</ref> || Stanislava Pinchuk
|-
| 15/11/2013 - 24/11/2013 || KUH || Jun Inoue
|-
| 06/12/2013 - 15/12/2013 || A Study Of Eyes || Al Stark, [[Anthony Lister]], [[Dave Kinsey]], Hiroyasu Tsuri, James Reka, Ian Strange, [[Kozyndan]], Marda, [[Miss Van]], Shohei Otomo, Stanislava Pinchuk, Stephen Ives, Usugrow, Yuske Imai
|}

===Exhibitions 2014===

{| class="wikitable" width="600"
! scope="col" width="30%" | Dates
! scope="col" width="35%" | Exhibition
! scope="col" width="35%" | Artist(s)
|-
| 17/01/2014 - 26/01/2014 || The Existential Vacuum <ref>{{cite web|title=The Existential Vacuum|url=http://www.artslantstreet.com/articles/show/38006|publisher=Artslant|accessdate=10 January 2014}}</ref> || Jonathan Guthmann
|-
| 10/02/2014 - 21/02/2014 || Backwoods Retrospective || Stanislava Pinchuk, Ghostpatrol, Hiroyasu Tsuri, [[Fred Fowler]] & James Reka
|-
| 7/03/2014 - 16/03/2014 || Gold Blood, Magic Wierdos || Ghostpatrol, Brendan Monroe, Simon Hanselmann, kozyndan, Sheryo, The Yok, Jean Jullien, James Jirat Patradoon, Mr. Gauky, Mel Stringer
|-
| 18/04/2014 - 27/04/2014 || Keep It Simple <ref>{{cite web|title=Keep It Simple|url=http://agonistica.com/keep-it-simple-by-stabs/|publisher=Agnostica|accessdate=5 April 2014}}</ref><ref>{{cite web|title=Keep It Simple|url=http://www.widewalls.ch/stabs-exhibition-backwoods-gallery/|publisher=WideWalls|accessdate=18 April 2014}}</ref><ref>{{cite web|last=Ross|first=Annabel|title=Street artist Stabs: Inspired by sitting down on the job|url=http://www.smh.com.au/entertainment/art-and-design/street-artist-stabs-inspired-by-sitting-down-on-the-job-20140419-36xqs.html|publisher=Sydney Morning Herald|accessdate=20 April 2014}}</ref>|| Stabs
|-
| 02/05/2014 - 11/05/2014 || Stick Folk <ref>{{cite web|title=Stick Folk|url=http://australianinfront.com.au/news/article/stick-folk/|publisher=Australia In Front|accessdate=11 April 2014}}</ref> ||  Tom Civil
|-
| 06/06/2014 - 15/06/2014 || Untold <ref>{{cite web|title=Recap, Untold by James Reka at Backwoods Gallery|url=http://arrestedmotion.com/2014/06/recap-james-reka-untold-backwoods-gallery/|publisher=ArrestedMotion|accessdate=19 June 2014}}</ref><ref>{{cite web|title=James Reka Tells The Untold Story Of Berlin's Abandoned Factories|url=http://www.complex.com/style/2014/06/james-reka-tells-the-untold-story-of-berlins-abandoned-factories-and-gritty-streets-in-his-latest-backwoods-gallery-exhibition|publisher=Complex Magazine|accessdate=19 June 2014}}</ref><ref>{{cite web|title=Behind The Scenes Of Untold, Exclusive Interview With James Reka|url=http://hifructose.com/2014/06/05/behind-the-scenes-of-untold-exclusive-interview-with-james-reka/|publisher=Hi Fructose|accessdate=5 June 2014}}</ref>|| James Reka
|-
| 20/06/2014 - 29/06/2014  || New Landscapes <ref>{{cite web|title= New Landscapes|url=http://verynearlyalmost.com/dev/2014/06/fred-fowler-new-landscapes/|publisher=Very Nearly Almost Magazine|accessdate=15 June 2014}}</ref> || [[Fred Fowler]]
|-
| 18/07/2014 - 27/07/2014 || Drawing Blood || Eno
|-
| 19/07/2014 - 27/07/2014 || Modern-Classic || Shun Kawakami
|-
| 01/08/2014 - 10/08/2014 || Bright Lights <ref>{{cite web|title= New Landscapes|url=http://www.acclaimmag.com/arts/events-arts/event-numskull-bright-lights-backwoods-gallery/|publisher=Acclaim Magazine|accessdate=21 July 2014}}</ref> || Numskull
|-
| 22/08/2014 - 31/08/2014 || Oblivion || Yusk Imai
|-
| 26/09/2014 - 05/10/2014 || A Study Of Hair <ref>{{cite web|title= Exhibition Opening A Study Of Hair at Backwoods Gallery|url=http://www.acclaimmag.com/arts/exhibition-opening-study-hair-backwoods-gallery-september-26/|publisher=Acclaim Magazine|accessdate=12 September 2014}}</ref><ref>{{cite web|title= A Study Of Hair|url=http://www.widewalls.ch/a-study-of-hair-exhibition-backwoods-gallery/|publisher=WideWalls|accessdate=12 September 2014}}</ref><ref>{{cite web|title= A Study Of Hair|url=http://verynearlyalmost.com/dev/tag/a-study-of-hair/|publisher=Very Nearly Almost Magazine|accessdate=23 September 2014}}</ref><ref>{{cite web|title= A Study Of Hair|url=http://arrestedmotion.com/2014/09/upcoming-a-study-of-hair-backwoods-gallery/|publisher=ArrestedMotion|accessdate=21 July 2014}}</ref>|| Merda, Inkie, [[C215 (street artist)|C215]], [[Dave Kinsey]], [[Mark Bode]], Stanislava Pinchuk (Miso), [[Faith47]], Shohei Otomo, Jonathan Guthmann, [[ROA (artist)|ROA]], Stephen Ives, Hiroyasu Tsuri (TwoOne), Usugrow, Yusk Imai, Alexander Mitchell.
|-
| 10/10/2014 - 19/10/2014 || Outsiders || Hiroyasu Tsuri (TwoOne)
|-
| 24/10/2014 - 02/11/2014 || Flat Blend <ref>{{cite web|title= Exhibition Opening A Study Of Hair at Backwoods Gallery|url=http://hypebeast.com/2014/10/blackwoods-gallery-presents-shohei-otomo/|publisher=HypeBeast|accessdate=29 October 2014}}</ref> || Shohei Otomo, Kosuke Kawamura
|-
| 04/11/2014 - 16/11/2014 || Flat Blend || Jun Inoue
|-
| 28/11/2014 - 07/12/2014 || Silent Landscapes || Stanislava Pinchuk
|-
| 06/12/2013 - 07/12/2013 || 81 Bastards || Jun Inoue, Yoshi47, Mhak, Jun Inoue, Sand, O.T
|}

==References==
{{reflist}}

==External links==
*[http://www.backwoodsgallery.com/ Official website]

{{Melbourne landmarks}}

[[Category:2010 establishments in Australia]]
[[Category:Art galleries established in 2010]]
[[Category:Art museums and galleries in Melbourne]]
[[Category:Contemporary art galleries in Australia]]