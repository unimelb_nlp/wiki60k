{{Infobox settlement
|official_name            = Beatty, Nevada
|settlement_type          = [[Unincorporated towns in Nevada|Unincorporated town]]
|nickname                 = Gateway to Death Valley<ref>{{cite news
  | last = Waite
  | first = Mark
  | title = Death Valley may choose Hafen Commercial Center for offices
  | work = Pahrump Valley Times
  | date = November 7, 2007
  | url = http://www.pahrumpvalleytimes.com/2007/Nov-07-Wed-2007/news/17725240.html
  | accessdate = February 25, 2009 |archiveurl = https://web.archive.org/web/20071108181429/http://www.pahrumpvalleytimes.com/2007/Nov-07-Wed-2007/news/17725240.html |archivedate = November 8, 2007}}</ref>
<!-- Images -->
|image_skyline            = File:Beatty_NV_-_downtown.jpg
|imagesize                = 290px
|image_caption            =
|image_flag               =
|image_seal               =
<!-- Maps -->
|image_map = Nye_County_Nevada_Incorporated_and_Unincorporated_areas_Beatty_Highlighted.svg
|mapsize                  = 250x200px
|map_caption              = Location of Beatty, Nevada
|image_map1               =
|mapsize1                 =
|map_caption1             =
<!-- Location -->
|subdivision_type         = [[List of countries|Country]]
|subdivision_name         = [[United States]]
|subdivision_type1        = [[Political divisions of the United States|State]]
|subdivision_name1        = [[Nevada]]
|subdivision_type2        = [[County (United States)|County]]
|subdivision_name2        = [[Nye County]]
|government_footnotes     =
|government_type          =
|leader_title             =
|leader_name              =
|leader_title1            =
|leader_name1             =
|established_title        =
|established_date         =
<!-- Area -->
|unit_pref  =  Imperial
|area_footnotes           =
|area_magnitude           =
|area_total_km2           = 454.9
|area_land_km2            = 454.9
|area_water_km2           = 0.0
|area_total_sq_mi         = 175.6
|area_land_sq_mi          = 175.6
|area_water_sq_mi         = 0.0

<!-- Population -->
|population_as_of         = [[United States Census, 2010|2010]]
|population_footnotes     =
|population_total         = 1,010
|population_density_km2   = 2.5
|population_density_sq_mi = 6.6

<!-- General information -->
|timezone                 = [[Pacific Time Zone|Pacific (PST)]]
|utc_offset               = -8
|timezone_DST             = PDT
|utc_offset_DST           = -7
|elevation_footnotes      =
|elevation_m              = 1008
|elevation_ft             = 3307
|coordinates              = {{coord|36|54|34|N|116|45|16|W|region:US-NV|display=inline,title}}
|postal_code_type         = [[ZIP code]]
|postal_code              = 89003
|area_code                = [[Area code 775|775]]
|blank_name               = [[Federal Information Processing Standard|FIPS code]]
|blank_info               = 32-05100
|blank1_name              = [[Geographic Names Information System|GNIS]] feature ID
|blank1_info              = 0845356
|website                  = [http://www.beattynv.info/ Beatty, Nevada]
|footnotes                =

<!-- Registry designation -->
{{designation list|embed=yes|designation1=Nevada Historical Marker|designation1_offname=Beatty (Center of the Gold Railroads--"Chicago of the West")|designation1_number=173}}
}}

'''Beatty''' (pronounced BAY-dee) is an [[Unincorporated towns in Nevada|unincorporated town]]<ref>{{cite web|title=Nye County Code: Section 19.02.010 - Formation of Town|publisher=Sterling Codifiers|url=http://www.sterlingcodifiers.com/codebook/index.php?book_id=648|accessdate=January 29, 2017}}</ref> along the [[Amargosa River]] in [[Nye County, Nevada|Nye County]] in the [[U.S. state]] of [[Nevada]]. [[U.S. Route 95]] runs through the town, which lies between [[Tonopah, Nevada|Tonopah]], about {{convert|90|mi|km}} to the north, and [[Las Vegas, Nevada|Las Vegas]], about {{convert|120|mi|km}} to the southeast. [[Nevada State Route 374|State Route 374]] connects Beatty to [[Death Valley National Park]], about {{convert|8|mi|km}} to the west.

Before the arrival of non-[[Indigenous peoples|indigenous people]] in the 19th&nbsp;century, the region was home to groups of [[Western Shoshone]]. Established in 1905, the community was named after Montillus (Montillion) Murray "Old Man" Beatty, who settled on a ranch in the Oasis Valley in 1896 and became Beatty's first postmaster. With the arrival of the [[Las Vegas and Tonopah Railroad]] in 1905, the town became a railway center for the Bullfrog Mining District, including mining towns such as nearby [[Rhyolite, Nevada|Rhyolite]].<ref name ="McCracken History 56-59">McCracken, ''History'', pp. 56–59</ref> Starting in the 1940s, [[Nellis Air Force Base]] and other federal installations contributed to the town's economy as did tourism related to Death Valley National Park and the rise of Las Vegas as an entertainment center.

Beatty is home to the Beatty Museum and Historical Society and to businesses catering to tourist travel. The [[ghost town]] of Rhyolite and the [[Goldwell Open Air Museum]]  (a sculpture park), are both about {{convert|4|mi|km|0}} to the west, and [[Yucca Mountain]] and the [[Nevada Test Site]] are about {{convert|18|mi|km}} to the east.

==History==
Before the arrival of non-indigenous explorers, prospectors, and settlers, Western Shoshone in the Beatty area hunted game and gathered wild plants in the region. It is estimated that the 19th-century population density of the Indians near Beatty was one person per {{convert|44|sqmi|km2}}. By the middle of the century, European diseases had greatly reduced the  [[Native Americans in the United States|Indian]] population, and incursions by newcomers had disrupted the native traditions. In about 1875, the Shoshone had six camps, with a total population of 29, along the Amargosa River near Beatty. Some of the survivors and their descendants continued to live in or near Beatty, while others moved to [[Indian reservation|reservations]] at [[Walker Lake, Nevada|Walker Lake]], [[Reese River]], [[Duckwater, Nevada|Duckwater]], or elsewhere.<ref name = "McCracken 8-12">McCracken, ''History'',  pp. 8–12</ref>

Beatty is named after "Old Man" Montillus (Montillion) Murray Beatty, a Civil War veteran and miner who bought a ranch along the [[Amargosa River]] just north of the future community<ref name ="McCracken 21-22">McCracken, ''History'', pp. 21–22</ref> and became its first postmaster in 1905.<ref name = "McCracken 6">McCracken, ''History'', p. 6</ref>  The community was laid out in 1904 or 1905 after Ernest Alexander "Bob" Montgomery, owner of the Montgomery Shoshone Mine near Rhyolite, decided to build the Montgomery Hotel in Beatty.<ref name = "McCracken 48-49">McCracken, ''History'', pp. 48–49</ref> Montgomery was drawn to the area, known as the Bullfrog Mining District, because of a [[gold rush]] that began in 1904 in the [[Bullfrog Hills]] west of Beatty.<ref>Lingenfelter, p. 203</ref>

[[File:Montgomery Hotel 1905.jpg|thumb|left|The Montgomery Hotel in 1905. It was owned by Bob Montgomery, namesake of the Montgomery-Shoshone Mine in nearby [[Rhyolite, Nevada|Rhyolite]].]] During Beatty's first year, wagons pulled by teams of horses or mules hauled freight between the Bullfrog district (that included the towns of Rhyolite, [[Bullfrog, Nevada|Bullfrog]], [[Gold Center, Nevada|Gold Center]], Transvaal, and Springdale) and the nearest railroad, in Las Vegas, and by the middle of 1905, about 1,500 horses were engaged in this business.<ref>McCracken, ''History'', p. 52</ref> In October 1906, the [[Las Vegas and Tonopah Railroad]] (LV&T) began regular service to Beatty; in April 1907, the Bullfrog Goldfield Railroad (BG) reached the community, and the [[Tonopah and Tidewater Railroad|Tonopah and Tidewater]] (T&T) line added a third railroad in October 1907.<ref name="McCracken History 56-59"/> The LV&T ceased operations in 1918, the BG in 1928, and the T&T in 1940.<ref name="McCracken History 56-59"/> Until the railroads abandoned their lines, Beatty served as the railhead for many mines in the area, including a [[Fluorite|fluorspar]] mine on [[Bare Mountain Range (Nevada)|Bare Mountain]], to the east.<ref>McCracken, ''History'', pp. 62–70</ref>

Beatty's first newspaper was the ''Beatty Bullfrog Miner'', which began publishing in 1905 and went out of business in 1909. The ''Rhyolite Herald'' was the region's most important paper, starting in 1905 and reaching a circulation of 10,000 by 1909. It ceased publication in 1912, and the Beatty area had no newspaper from then until 1947. The ''Beatty Bulletin'', a supplement to the ''Goldfield News'', was published from then through 1956.<ref>McCracken, ''History'', pp. 49–51</ref>

Beatty's population grew slowly in the first half of the 20th&nbsp;century, rising from 169 in 1929 to 485 in 1950.<ref>McCracken, ''History'', pp. 102–03</ref> The first reliable electric company in the community, Amargosa Power Company, began supplying electricity in about 1940. Phone service arrived during [[World War II]], and the town installed a community-wide sewer system in the 1970s.<ref name = "McCracken 92-93">McCracken, ''History'', pp. 92–93</ref> When a new mine opened west of Beatty in 1988, the population briefly surged from about 1,000 to between 1,500 and 2,000 by the end of 1990.<ref name ="McCracken 155-57"/> Since the mine's closing in 1998, the population has fallen again to near its former level.

==Geography and climate==
[[Image:Amargosa River in Beatty, Nevada.jpg|thumb|The [[Amargosa River]] flows through Beatty.]]
Beatty lies along [[U.S. Route 95]] between [[Tonopah, Nevada|Tonopah]], about {{convert|90|mi|km}} to the north, and [[Las Vegas, Nevada|Las Vegas]], about {{convert|120|mi|km}} to the southeast.  [[Nevada State Route 374|State Route 374]] connects Beatty to [[Death Valley National Park]], about {{convert|8|mi|km}} to the west. [[Yucca Mountain]] and the [[Nevada Test Site]] are about {{convert|18|mi|km}} to the east.<ref name ="Rand">{{cite map  |publisher = Rand McNally & Company  |title = The Road Atlas  |edition = 2008  |section = 64  |isbn = 0-528-93961-0 }}</ref> The most densely populated part of the [[census-designated place]] (CDP) of Beatty is at {{Coord|36|54|34|N|116|45|16|W|type:city}} (36.909337, −116.754531),<ref>{{cite web | title = U.S. Gazetteer files: 2000 and 1990 | url = http://www.census.gov/geo/www/gazetteer/gazette.html|date = May 3, 2005 | accessdate = January 31, 2008| archiveurl= http://webarchive.loc.gov/all/20080917225736/http%3A//www%2Ecensus%2Egov/geo/www/gazetteer/gazette%2Ehtml| archivedate= September 17, 2008 <!--DASHBot-->| deadurl= no}}</ref> although the CDP extends well beyond this urban center.<ref>{{cite web| title =2010 Boundary and Annexation Survey (BAS): Nye County, NV |publisher = United States Census Bureau | url = http://www2.census.gov/geo/www/bas/st32_nv/cou/c32023_nye/BAS10C23202300000_000.pdf| format = PDF | accessdate = June 5, 2010}}</ref><ref>{{cite web|title = 2010 Boundary and Annexation Survey (BAS): Nye County, NV (inset map of population center)| publisher = United States Census Bureau | url = http://www2.census.gov/geo/www/bas/st32_nv/cou/c32023_nye/BAS10C23202300000_D01.pdf | format = PDF | accessdate = June 5, 2010}}</ref> According to the [[United States Census Bureau]], the CDP has a total area of {{convert|175.6|sqmi|km2}}, all land.<ref>{{cite web|title = United States Gazetteer files: 2000 and 1990 (places) | url = http://www.census.gov/tiger/tms/gazetteer/places.txt | publisher = United States Census Bureau | accessdate = June 4, 2010}}</ref> The most populated area lies  at {{convert|3307|ft|m}} above [[sea level]]<ref name="gnis">{{cite web | work = [[Geographic Names Information System]] (GNIS) | publisher = United States Geological Survey | date =  December 12, 1980 | url ={{Gnis3|845356}} | title = Beatty | accessdate = June 5, 2010}}</ref> between Beatty Mountain and [[Bare Mountain (Nevada)|Bare Mountain]] to the east and the Bullfrog Hills to the west. The Amargosa River, an intermittent river that ends in Death Valley, flows on the surface through part of the CDP but has not been counted as water in the Census Bureau statistics.

Nevada's main climatic features are bright sunshine, low annual precipitation, heavy snowfall in the higher mountains, clean, dry air, and large daily temperature ranges. Strong surface heating occurs by day and rapid cooling by night, and usually even the hottest days have cool nights. The average percentage of possible sunshine in southern Nevada is more than 80&nbsp;percent. Sunshine and low humidity in this region account for an average evaporation, as measured in [[Pan evaporation|evaporation pans]], of more than {{convert|100|in|mm}} of water a year.<ref name=climate>{{cite web | author = Nevada state climatologists | title = Climate of Nevada  | publisher = National Climatic Data Center | url = http://www.wrcc.dri.edu/narratives/NEVADA.htm  |accessdate = February 18, 2009}}</ref>

Beatty receives only {{convert|5.71|in|mm|0}} of precipitation a year.<ref name= NOAA/> Precipitation of at least {{convert|.01|in|mm}} falls on an average of 21&nbsp;days annually.<ref name = "wettest and driest">{{cite web|title =Period of Record General Climate Summary - Precipitation |publisher = Western Regional Climate Center|url = http://www.wrcc.dri.edu/cgi-bin/cliMAIN.pl?nv0714 | accessdate = June 4, 2010}}</ref> The wettest year was 1941 with {{convert|11.89|in|mm|abbr=on}} and driest year was 1953 with {{convert|0.69|in|mm|abbr=on}}.<ref name = "wettest and driest"/> The monthly daily average temperature ranges from {{convert|41.1|°F|1}} in December to {{convert|80.2|°F|1}} in July. On average, there are 26 days of {{convert|100|F|0}}+ highs, 97 days of {{convert|90|F|0}}+, and 38 days where the high remains at or below {{convert|50|F|0}}; the average window for freezing temperatures is November 2 to April 6. Snow is uncommon and measurable (≥{{convert|0.1|in|cm|abbr=on|disp=or}}) amounts occur in less than 60% of seasons. The highest recorded temperature was {{convert|115|F|0}} on June 11, 1961,<ref name = "record temp">{{cite web|title = Beatty, Nevada: Period of Record General Climate Summary - Temperature |publisher = Western Regional Climate Center| url = http://www.wrcc.dri.edu/cgi-bin/cliMAIN.pl?nv0714 | accessdate = June 4, 2010}}</ref> and the lowest was {{convert|1|F|0}} on February 2, 1933.<ref name = "record temp"/>

{{Weather box
|location = Beatty, Nevada (1981–2010 normals)
|single line = Y
|Jan high F = 55.0
|Feb high F = 58.3
|Mar high F = 65.3
|Apr high F = 72.1
|May high F = 82.2
|Jun high F = 91.6
|Jul high F = 97.8
|Aug high F = 96.2
|Sep high F = 88.8
|Oct high F = 77.5
|Nov high F = 64.0
|Dec high F = 54.3
|year high F= 75.3
|Jan low F = 28.7
|Feb low F = 31.5
|Mar low F = 36.0
|Apr low F = 41.3
|May low F = 49.7
|Jun low F = 56.3
|Jul low F = 62.6
|Aug low F = 60.9
|Sep low F = 54.4
|Oct low F = 44.4
|Nov low F = 34.5
|Dec low F = 27.9
|year low F= 44.0
|Jan precipitation inch = 0.70
|Feb precipitation inch = 0.98
|Mar precipitation inch = 0.79
|Apr precipitation inch = 0.40
|May precipitation inch = 0.25
|Jun precipitation inch = 0.19
|Jul precipitation inch = 0.40
|Aug precipitation inch = 0.42
|Sep precipitation inch = 0.40
|Oct precipitation inch = 0.22
|Nov precipitation inch = 0.45
|Dec precipitation inch = 0.52
|year precipitation inch= 5.71
| Jan snow inch = 1.0
| Feb snow inch =  .2
| Mar snow inch =  .3
| Apr snow inch =   0
| May snow inch =   0
| Jun snow inch =   0
| Jul snow inch =   0
| Aug snow inch =   0
| Sep snow inch =   0
| Oct snow inch =   0
| Nov snow inch =  .3
| Dec snow inch =  .7
|year snow inch = 2.6
|unit precipitation days = 0.01 in
| Jan precipitation days = 3.5
| Feb precipitation days = 4.1
| Mar precipitation days = 3.7
| Apr precipitation days = 2.3
| May precipitation days = 2.2
| Jun precipitation days = 1.1
| Jul precipitation days = 2.1
| Aug precipitation days = 1.8
| Sep precipitation days = 1.9
| Oct precipitation days = 1.7
| Nov precipitation days = 1.9
| Dec precipitation days = 2.3
|year precipitation days =28.7
|unit snow days = 0.1 in
| Jan snow days = 0.5
| Feb snow days = 0.1
| Mar snow days = 0.1
| Apr snow days =   0
| May snow days =   0
| Jun snow days =   0
| Jul snow days =   0
| Aug snow days =   0
| Sep snow days =   0
| Oct snow days =   0
| Nov snow days = 0.1
| Dec snow days = 0.4
|year snow days = 1.3
|source 1 = NOAA<ref name = NOAA>
{{cite web
  |url = http://www.nws.noaa.gov/climate/xmacis.php?wfo=vef
  |title = NowData - NOAA Online Weather Data
  |publisher = [[National Oceanic and Atmospheric Administration]]
  |accessdate = 2013-07-19}}</ref>
}}

==Demographics==
As of the [[census]] of 2000, there were 1,154 people, 535 households, and 270 families residing in the CDP.<ref>{{cite web|title = Glossary: F | publisher = United States Census Bureau | url = http://factfinder.census.gov/home/en/epss/glossary_f.html | accessdate = June 5, 2010| archiveurl= https://web.archive.org/web/20100527122300/http://factfinder.census.gov/home/en/epss/glossary_f.html| archivedate= 27 May 2010 <!--DASHBot-->| deadurl= no}}The Census Bureau distinguishes between "family" and "household". It says, "A family includes a householder and one or more people living in the same household who are related to the householder by birth, marriage, or adoption... Not all households contain families since a household may comprise a group of unrelated people or one person living alone."</ref> The [[population density]] was 6.6 people per square mile (2.5/km<sup>2</sup>). There were 740 housing units at an average density of 4.2 per square mile (1.6/km<sup>2</sup>). The racial makeup of the CDP was 90.9% [[White (U.S. Census)|White]], 0.1% [[African American (U.S. Census)|African American]], 1.5% [[Native American (U.S. Census)|Native American]], 1.2% [[Asian (U.S. Census)|Asian]], 3.1% from [[Race (United States Census)|other races]], and 3.2% from two or more races. [[Hispanic (U.S. Census)|Hispanic]] or [[Latino (U.S. Census)|Latino]] of any race were 8.9% of the population.<ref name = "census factfinder">{{cite web|title = American FactFinder | publisher = United States Census Bureau| url = http://factfinder.census.gov/servlet/SAFFFacts?_event=Search&geo_id=&_geoContext=&_street=&_county=Beatty&_cityTown=Beatty&_state=04000US32&_zip=&_lang=en&_sse=on&pctxt=fph&pgsl=010&show_2003_tab=&redirect=Y |accessdate = November 21, 2009}}</ref> There were 535 households out of which 26.9% had children under the age of 18 living with them, 39.8% were [[Marriage|married couples]] living together, 6.9% had a female householder with no husband present, and 49.5% were non-families. 43.4% of all households were made up of individuals and 15.0% had someone living alone who was 65 years of age or older. The average household size was 2.16 and the average family size was 3.04.<ref name = "census factfinder"/> In the CDP the population was spread out with 26.1% under the age of 18, 5.6% from 18 to 24, 24.7% from 25 to 44, 29.5% from 45 to 64, and 14.0% who were 65 years of age or older. The median age was 40 years. For every 100 females there were 119.4 males. For every 100 females age 18 and over, there were 121.6 males.<ref name = "census factfinder"/> The median income for a household in the CDP was $41,250, and the median income for a family was $52,639. Males had a median income of $44,438 versus $25,962 for females. The [[per capita income]] for the CDP was $16,971. About 10.4% of families and 13.4% of the population were below the [[poverty line]], including 7.1% of those under age 18 and 19.6% of those age 65 or over.<ref name = "census factfinder"/> As of the 2010 census, 1,010 people lived in Beatty.<ref>{{cite web|title=American FactFinder: Beatty CDP, Nevada|url=http://factfinder2.census.gov/faces/tableservices/jsf/pages/productview.xhtml?src=bkm|publisher=U.S. Census Bureau|accessdate=August 25, 2012}}</ref>

==Government==
Under the terms of the Unincorporated Town Government Law of Nevada, Beatty is governed by the [[Nye County, Nevada|Nye County]] Commission assisted by a local board acting as a liaison between the citizens of Beatty and the commissioners.<ref>{{cite web| title = State and Local Government: Nevada Legislative Counsel Bureau Research Division Policy Brief | publisher =  The Community Environmental Legal Defense Fund  |date = January 2005 | url = http://www.celdf.org/Portals/0/PDF/Home%20Rule%20in%20Nevada.pdf | page = 244 | format = PDF | accessdate =June 4, 2010}}</ref> The Beatty Town Advisory Board consists of five elected members who meet twice a month at the Beatty Community Center.<ref>{{cite web| title = Beatty Town Advisory Board | url = http://www.beattynv.info/town.html | publisher = The Town of Beatty | year = 2009 | accessdate = July 28, 2012}}</ref> The Beatty General Improvement District manages the community's parks, swimming pool, [[Golf stroke mechanics#Putt|putting]] course, and other recreational grounds.<ref>{{cite web | title = BGID – Parks and Recreation | publisher = The Town of Beatty | url = http://www.beattynv.info/bgid.html | year = 2009 | accessdate = November 21, 2009}}</ref>

[[File:Beatty FD Antique Fire Truck.jpg|thumb|left|Beatty Volunteer Fire Department antique [[fire engine]] in a 2006 parade]] Andrew Borasky, Lorinda Wichman, Frank Carbone, Donna Cox, and Dan Schinhofen are the county commissioners in 2015.<ref>{{cite web|title = Board of County Commissioners | url = http://www.nyecounty.net/Directory.aspx?did=103 | publisher = Nye County, Nevada | accessdate = August 14, 2015}}</ref> Among the many county departments are administration, works and roads, building and code compliance, [[sheriff]], animal control, planning, property assessment, the Fifth Judicial [[Nevada District Courts|District Court]], health and human services, senior centers including the Beatty Senior Center, and lower courts including the Beatty Justice Court.<ref>{{cite web | title = Staff Directory: Categories | url = http://www.nyecounty.net/directory.aspx | publisher = Nye County, Nevada | accessdate = November 21, 2009}}</ref> The Nye County Sheriff's Office has a substation in Beatty.<ref>{{cite web | title = Nye County Sheriff – Beatty Sub Station | url = http://www.beattynv.info/sheriff.html | publisher = The Town of Beatty | year = 2009 |accessdate = November 21, 2009}}</ref> Among other things, the office handles [[Dispatcher|dispatch]] for the Beatty Volunteer Fire Department, which provides firefighting and ambulance services.<ref>{{cite web | title =Beatty Volunteer Fire Department | url = http://www.beattynv.info/fire.html | year = 2009 | publisher = The Town of Beatty | accessdate = November 21, 2009}}</ref>

James Oscarson, a [[Republican Party (United States)|Republican]], represents Beatty and the rest of District 36 in the [[Nevada Assembly]].<ref name ="Legislative Districts">{{cite web| title = Legislative Districts 2012−2013 | url = http://www.leg.state.nv.us/Division/Research/Districts/WhoRepresentsMy/DistrictsByTown.pdf | publisher = Nevada Legislature |format=PDF| accessdate = May 14, 2013}}</ref>  In the [[Nevada Senate]], Beatty, as part of District 19, is represented by [[Pete Goicoechea]], a Republican.<ref name ="Legislative Districts"/>

[[Mark Amodei]], a Republican, represents Beatty and the rest of Nevada's Second Congressional District in the [[United States House of Representatives]]. His term runs through January 2015. [[Harry Reid]], a [[Democratic Party (United States)|Democrat]], and [[Dean Heller]], a Republican, represent Nevada in the [[United States Senate]]. Reid's term runs through January 2017 and Heller's through January 2019.<ref>{{cite web|title=Nevada's Congressional Delegation|url=http://www.reid.senate.gov/nevada/NVDelegation.cfm|publisher=Harry Reid|accessdate=May 14, 2013}}</ref>

==Economy==
[[File:Beatty (Nevada), Main Street.jpg|thumb|Downtown Beatty in 2012]] Early businesses in Beatty included the Montgomery Hotel, built by a mine owner in 1905,<ref name="McCracken 48-49"/> and freight businesses first centered on horse-drawn wagons and later on railroads serving the mining towns in the Bullfrog district.<ref>McCracken, ''History'', pp. 52–59</ref>  Beatty became the economic center for a large sparsely populated region. Aside from mining, other activities sustaining the community during the 1920s and 1930s included retail sales, gas and oil distribution, construction of [[Scotty's Castle]], and the production and sale of illegal alcohol during [[Prohibition]].<ref>McCracken, ''History'', p. 74</ref>

Nevada's legalization of [[gambling]] in 1931, the establishment of Death Valley National Monument in 1933, and the rise of Las Vegas as an entertainment center, brought visitors to Beatty, which became increasingly tourist-oriented.<ref name="McCracken 101-02">McCracken, ''History'', pp. 101–02</ref>

As underground mining declined in the region, federal defense spending, starting with the [[Nellis Air Force Range]] in 1940 and the Nevada Test Site in 1950, also contributed to the local economy.<ref name="McCracken 101-02"/> However, in 1988, an [[Open-pit mining|open-pit mine]] and mill began operations about {{convert|4|mi|km}} west of Beatty along State Route 374. [[Barrick Gold]] acquired the mine in 1994 and continued to extract and process ore at what became known as the Barrick Bullfrog Mine.<ref name ="Hustrulid">{{cite book|last = Kump |first = Dan |author2=Arnold, Tim |editor=Hustrulid, William A. |editor2=Bullock, Richard L. | title = Underground Mining Methods |chapter=Underhand Cut-and-Fill at the Barrick Bullfrog Mine | publisher = Society for Mining, Metallurgy, and Exploration | location = Littleton, Colo.  |chapter-url = https://books.google.com/books?id=N9Xpi6a5304C&pg=PA345 |pages=345–50 |isbn=978-0-87335-193-5}}</ref> At its peak, the mine employed 540&nbsp;workers, many of whom lived in Beatty.<ref name ="McCracken 155-57">McCracken, ''History'', pp. 155–57</ref> The mine closed in 1998.<ref name ="Hustrulid"/>

In 2004, the federal [[United States Environmental Protection Agency|Environmental Protection Agency]] (EPA) named the closed Barrick Bullfrog mine site as one of six slated for pilot reclamation projects under the national Brownfields Mine-Scarred Land Initiative. A local group, the Beatty Economic Development Corporation (BEDC), in discussions with the EPA, suggested solar-power generation as a potential use for the site.<ref>{{cite web  | title = U.S. EPA Selects Former Nye County Gold Mine for National Brownfields Pilot  | publisher = United States Environmental Protection Agency   | date =  May 18, 2004  | url = http://yosemite.epa.gov/opa/admpress.nsf/a883dc3da7094f97852572a00065d7d8/60f256158d5cb5e0852570d8005e1643!OpenDocument  | accessdate = March 23, 2009}}</ref> Barrick Gold later transferred {{convert|81|acres|ha}} of its land to Beatty.<ref name = "Streater"/> In February 2009, the ''New York Times'' published a [[Environment and Energy Publishing|Greenwire]] article suggesting that part of the economic stimulus money from the $787&nbsp;billion [[American Recovery and Reinvestment Act]] might finance the Beatty project. "Studies show that the Beatty area has some of the best solar energy potential in the United States, as well as a high potential for wind-power generation," the Greenwire story said.<ref name = "Streater">{{cite news | author = Streater, Scott | title =Polluted Mines as Economic Engines? Obama Admin Says 'Yes' | work = New York Times  |publisher = E&E Publishing| date = February 26, 2009 | url = https://www.nytimes.com/gwire/2009/02/26/26greenwire-polluted-mines-as-economic-engines-obama-admin--9896.html?pagewanted=1&%2334&%2359;&emc=eta1 | accessdate = March 23, 2009}}</ref>

The Beatty Chamber of Commerce web site describes the community as the ''Gateway to Death Valley'', a small rural locality that has "everything the desert visitor needs" including motels and [[recreational vehicle]] (RV) sites.<ref name = "Adventure">{{cite web | title = Beatty, Nevada: Where Adventure Awaits You | publisher = Beatty Chamber of Commerce | year = 2007 | url = http://www.beattynevada.org/ | accessdate = June 4, 2010| archiveurl= https://web.archive.org/web/20100522130501/http://www.beattynevada.org/| archivedate= 22 May 2010 <!--DASHBot-->| deadurl= no}}</ref> Aside from tourism, businesses contributing to the local economy include mining, retail trade, public administration and gambling.<ref>{{cite web|title= Beatty Community Profile|publisher = Beatty Chamber of Commerce|url = http://www.beattynevada.org/community.htm|accessdate = June 4, 2010| archiveurl= https://web.archive.org/web/20100522135722/http://www.beattynevada.org/community.htm| archivedate= 22 May 2010 <!--DASHBot-->| deadurl= no}}</ref>

==World Championship Wild Burro Race==

From 1961 until 1972 the local [[Lions Clubs International|Lions Club]] held annual [[burro]] races that were quite successful. Competitors came from all over the United States, Canada, and as far away as Iran. National attention was brought to the race when Reg Potterton wrote a feature article about the 1971 race for ''Playboy'' magazine. The story appeared in the May 1972 issue, and featured stories about the town, the contestants, and the tourists who attended.<ref>{{cite journal|last1=Potterton|first1=Reg|title=The Great Race|journal=Playboy|date=May 1972|pages=111–12, 186–88}}</ref> Eventually the races were discontinued after organizers decided that the visitors they attracted were not good for Beatty's image.<ref>{{cite web|title=Beatty, Nevada, History|publisher=Beatty Museum and Historical Society|url=http://www.beattymuseum.org/beatty_nevada_history_page_2.html|year=2010|accessdate=November 26, 2013|archiveurl=https://web.archive.org/web/20100302055530/http://beattymuseum.org/beatty_nevada_history_page_2.html|archivedate=March 2, 2010|deadurl=yes}}</ref>

==Infrastructure and culture==
[[Image:Beatty public library.jpg|thumb|right|Beatty Public Library]] The community is home to the Beatty Museum and Historical Society. The [[ghost town]] of Rhyolite and the Goldwell Open Air Museum, a sculpture park, are about {{convert|4|mi|km|0}} to the west. Bailey's Hot Springs and bathhouses are about {{convert|5|mi|km|0}} north of Beatty in the Oasis Valley. In addition to highways, Beatty has a general aviation airfield, [[Beatty Airport]], about {{convert|3|mi|km|0}} south of downtown. Beatty Medical Center, which opened in 1977, provides family medicine and other services.<ref>{{cite web|title =  Beatty Medical Center Nevada | publisher = Nevada Health Centers, Inc. |year = 2008 | url = http://www.nvrhc.org/beatty.cfm| accessdate = March 14, 2009| archiveurl= https://web.archive.org/web/20090211100449/http://nvrhc.org/beatty.cfm| archivedate= 11 February 2009 <!--DASHBot-->| deadurl= no}}</ref> The Beatty Library, a member of the Cooperative Libraries Automated Network, has a searchable online catalog.<ref>{{cite web|title = Beatty Library District | publisher = Polaris Library Systems | year = 2007 |  url= http://www.clan.lib.nv.us/polaris/default.aspx?ctx=24.1033.0.0.1 |accessdate = March 14, 2009}}</ref> Beatty's combined elementary and [[middle school]]s, serving [[kindergarten]] through eighth grade, and Beatty High School, grades 9–12, are part of the Nye County School District.<ref>{{cite web|url=http://nyecounty.schoolinsites.com/?PN=Schools2|title=Nye County School District School|last=Staff writer|publisher=Nye County School District|accessdate=May 31, 2010}}</ref> The Beatty Water and Sanitation District supplies drinking water from three wells to the town residents and treats the community's wastewater.<ref>{{cite web|last = Farr West Engineering | title = Beatty Water and Sanitation District: Water Conservation Plan |publisher = The Town of Beatty |url = http://www.beattynv.info/water.pdf| date = November 2008| format = PDF | accessdate=June 5, 2010}}</ref>

{{clear}}
{{wide image|Bare Mountain Nevada panorama.jpg|1100px|Panorama of [[Bare Mountain (Nevada)|Bare Mountain]] as seen from near State Route 374 between Beatty and Rhyolite.}}

== References ==
{{Reflist|2}}

==Works cited==
* Lingenfelter, Richard E. (1986). ''Death Valley & the Amargosa: A Land of Illusion''. Berkeley and Los Angeles, California: University of California Press. ISBN 0-520-06356-2.
* McCracken, Robert D. (1992). ''A History of Beatty, Nevada''. Tonopah, Nevada: Nye County Press. ISBN 1-878138-54-5.

==External links==
{{Wikivoyage|Beatty}}
{{Commons category|Beatty, Nevada}}
* [http://www.beattynv.info/ Beatty, Nevada Town Website]
* [http://inyo.coffeecup.com/site/beatty/beattyfossils.html Fossils From Near Beatty, Nevada] Roughly 475 million-year old invertebrate fossils from the world-famous Beatty Mudmound, in the vicinity of Beatty, Nevada
* [http://beattylibrarydistrict.org/Home.html Beatty Library]
* [http://www.beattynevada.org/ Beatty Chamber of Commerce]
* [http://www.goldwellmuseum.org/ Goldwell Open Air Museum]
* [http://www.beattymuseum.org/ Beatty Museum and Historical Society]

{{Nye County, Nevada}}
{{Good article}}

[[Category:Amargosa Desert]]
[[Category:Census-designated places in Nye County, Nevada]]
[[Category:Unincorporated towns in Nevada]]
[[Category:History of Nye County, Nevada]]
[[Category:Tonopah and Tidewater Railroad]]
[[Category:Populated places established in 1905]]
[[Category:1905 establishments in Nevada]]