{{Accounting}}

'''International Accounting Standard 8 ''Accounting Policies, Changes in Accounting Estimates and Errors''''' or '''IAS 8''' is an [[International Financial Reporting Standards|international financial reporting standard]] (IFRS) adopted by the [[International Accounting Standards Board]] (IASB). It prescribes the criteria for selecting and changing accounting policies, accounting for changes in estimates and reflecting corrections of prior period errors.

The standard requires compliance with IFRSs which are relevant to the specific circumstances of the entity. In a situation where no specific guidance is provided by IFRSs, IAS 8 requires management to use its judgement to develop and apply an accounting policy that is relevant and reliable.<ref name="pwc">PwC Inform</ref> Changes in accounting policies and corrections of errors are generally accounted for retrospectively, unless this is impracticable; whereas changes in accounting estimates are generally accounted for prospectively.<ref name="pwc"/><ref name="plus">IAS Plus</ref>

IAS 8 was issued in December 1993 by the [[International Accounting Standards Committee]], the predecessor to the IASB. It was reissued in December 2003 by the IASB.<ref name="plus"/>

==Accounting policies==
Accounting policies are the specific principles, bases, conventions, rules and practices applied by an entity in preparing and presenting financial information.<ref name="p5">IAS 8 para 5</ref>

Where an IFRS specifically applies to a transaction, event or condition, the accounting policy applied to that item should be determined by reference to that standard.<ref name="pwc"/><ref>IAS 8 para 7</ref> When no standard applies specifically to a transaction, event or condition, management should use its judgement to develop a policy that results in information that is relevant to the economic decision-making needs of users and reliable, such that the [[financial statements]] faithfully represent the financial position, performance and cashflows of the entity, reflect the economic substance of transactions, events and conditions, are free from bias, prudent, and complete in all material respects.<ref>IAS 8 para 10</ref>

In making judgement, management should take into account (in the following order) the requirements in IFRSs dealing with similar and related issues, and the definitions, recognition criteria and measurement concepts for assets, liabilities, income and expenses in the ''[[Conceptual Framework for Financial Reporting|Conceptual Framework]]''.<ref>IAS 8 para 11</ref> Management may also consider recent pronouncements of other standard-setting bodies, accounting literature and accepted industry practices, to the extent that these do not conflict with IFRSs and the ''Framework''.<ref>IAS 8 para 12</ref>

Accounting policies should be applied consistently for similar transactions, events or conditions, unless an IFRS requires or permits different accounting policies to be applied to different categories of items.<ref name="pwc"/><ref>IAS 8 para 13</ref>

An entity can change an accounting policy only if it is required by an IFRS or results in the financial statements providing reliable and more relevant information.<ref name="pwc"/><ref>IAS 8 para 14</ref> If the change is due to requirement by an IFRS, an entity shall account for the change from the initial application of the IFRS in accordance with the specific transitional provisions (i.e. the standard may specify retrospective application or only prospective application), if any.<ref name="pwc"/><ref>IAS 8 para 19(a)</ref> Where there are no specific transitional provisions in the IFRS requiring the change in accounting policy, or an entity changes an accounting policy voluntarily, it should apply the change retrospectively.<ref name="pwc"/><ref>IAS 8 para 19(b)</ref>

Where a change in accounting policy is applied retrospectively, an entity should adjust the opening balance of each affected component of equity for the earliest prior period presented and the other comparative amounts for each prior period presented as if the new accounting policy had always been applied. The standard permits exemption from this requirement when it is impracticable to determine either the period-specific effects or cumulative effect of the change.<ref name="pwc"/><ref>IAS 8 paras 22–23</ref>

==Changes in accounting estimates==
A change in accounting estimate is "an adjustment of the [[book value|carrying amount]] of an asset or liability, or the amount of the periodic consumption of an asset, that results from the assessment of the present status of, and expected future benefits and obligations associated with, assets and liabilities. Changes in accounting estimates result from new information or new developments and, accordingly, are not correction of errors."<ref name="p5"/>

Changes in accounting estimates are reflected ''prospectively'' (that is, from the date of change) by including it in the income statement for the period of the change (if the change affects that period only), or the period of the change and future periods (if the change affects both).<ref name="pwc"/> However, to the extent that a change in an accounting estimate gives rise to changes in assets and liabilities, or relates to an item of equity, it is recognised by adjusting the carrying amount of the related asset, liability, or equity item in the period of the change.<ref name="plus"/>

==Errors==
[[Materiality (auditing)|Material]] prior period errors are corrected retrospectively in the first financial statements issued after their discovery.<ref name="pwc"/> Correction is made by restating the comparative amounts for the prior period(s) presented in which the error occurred. If the error occurred before the earlier comparative prior period presented, the opening balances of assets, liabilities and equity for the earliest prior period should be restated to reflect correction of the error(s).<ref>IAS 8 para 42</ref>

==Notes==
{{reflist}}

==See also==
*[[creative accounting]]
*[[Hollywood accounting]]
*[[plug (accounting)]]

==References==
# {{cite web |title=Accounting policies, accounting estimates and errors (IAS 8) |url=https://inform.pwc.com/inform2/show?action=informContent&id=0915113103126213 |work=PwC Inform |publisher=[[PwC]] |accessdate=2013-09-25}}
# {{cite web |title=IAS 8 — Accounting Policies, Changes in Accounting Estimates and Errors |url=http://www.iasplus.com/en/standards/ias/ias8 |work=IAS Plus |publisher=[[Deloitte]] |accessdate=2013-09-25}}
# {{cite book|title=International Financial Reporting Standards: required for annual periods beginning on 1 January 2012 |chapter=IAS 8 Accounting Policies, Changes in Accounting Estimates and Errors |author=[[International Accounting Standards Board]] |location=London |year=2011 |publisher=[[IFRS Foundation]] |isbn=9781907877360}}

==External links==
* [http://www.ifrs.org/Documents/IAS8.pdf IFRS Foundation Technical Summary: IAS 8 ''Accounting Policies, Changes in Accounting Estimates and Errors'']

{{International Financial Reporting Standards}}

[[Category:International Financial Reporting Standards|IAS 08]]