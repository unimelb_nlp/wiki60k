{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox horseraces
| surface       = Turf
| class         = Group II
| horse race    = Adelaide Cup
| image         =
| caption       =
| location      = [[Morphettville Racecourse]], [[South Australia]]
| inaugurated   = 1864
| race type     = [[Thoroughbred]] – [[Flat racing]]
| website       =
| distance      = 3200 metres 
| track         = Left-handed
| qualification = Three-year-old and older
| weight        = Handicap
| purse         = [[Australian dollar|A$]]400,000 (2017)
| bonuses       =
| sponsor       = [[Ubet, Wagering Company|UBET]] (2017)
}}
The '''Adelaide Cup''' is a [[South Australian Jockey Club]] [[Group races|Group 2]] [[Thoroughbred]] [[horse race]] handicap race for three-year-old and older, run over 3,200 metres at [[Morphettville Racecourse]] in [[Adelaide]], Australia on the second Monday in March. Total prize money for the race is [[A$]]400,000.

== Race history ==

The first Adelaide Cup was raced 21 April 1864 at Thebarton Racecourse, where [[Mile End, South Australia|Mile End]] is today. The race had stakes of 500 [[Sovereign (British coin)|sovereigns]] with an additional sweep of 50 sovereigns to induce owners from other [[British colony|colonies]] to compete in the race.<ref name="first_cup">{{cite news|title= SOUTH AUSTRALIAN JOCKEY CLUB'S AUTUMN MEETING |newspaper = [[South Australian Register]], [[Adelaide]]|date=22 April 1864|accessdate=7 March 2013|url=http://nla.gov.au/nla.news-article39122539|publisher=Trove Digitized Newspapers|page=2 column 8}}</ref> Victoria's P. Dowling's '''''Falcon''''' carried 10 [[Stone (unit)|stone]] 1 [[pound (mass)|pound]] and ridden by jockey J.Morrison won the race in a time of 3:50.50.<ref name="Res_1864">{{cite news|title= ''Official Race results'' Second race. Adelaide Cup. |newspaper = [[South Australian Register]], [[Adelaide]]|date=22 April 1864|accessdate=7 March 2013|url=http://nla.gov.au/nla.news-article39122539|publisher=Trove Digitized Newspapers|page=3 column 1}}</ref> A crowd of 7,000 or 8,000 was present for the event.<ref name="first_cup"/>

The race was run at as a [[Weight for Age]] over two miles from 1864&ndash;68.<ref name="history_1884">{{cite news |url=http://nla.gov.au/nla.news-article43660588 |newspaper=[[South Australian Register]], [[Adelaide]]|location=Adelaide |date=15 May 1884 |accessdate=7 March 2013 |page=7 column 3 |publisher=National Library of Australia|title=The Adelaide Cup-Day. The History of the Cup}}</ref>
In 1869, still at Thebarton Racecourse, it was run as a handicap race over two miles.<ref name="history_1884"/> There was no Cup raced in 1870 or 1871.<ref name="history_1884"/> 
The Cup resumed in 1872 and was run at "The Old Course" ([[Victoria Park Racecourse]]) over two miles with a smaller stake of 200 [[Sovereign (British coin)|sovereigns]] with an additional sweep of 15 sovereigns. In 1876 the race was held at [[Morphettville Racecourse]].<ref name="history_1884"/> In 1884 the race was run over 13 [[furlong]]s or {{frac|1|5|8}} miles (~2,600 metres), as was every race held until 1941.<ref name="adelaide_cup">{{cite web|url=http://www.progroupracing.com.au/group-races/south-australian-jockey-club/adelaide-cup|title=Winners and Past Results for the Adelaide Cup |work = Pro Group Racing Australia|year=2012}}</ref>

The race in 1885 was held at [[Flemington Racecourse|Flemington]], [[Victoria (Australia)|Victoria]] due to the Totalizator Repeal Act 1883 and was not held for three more years.<ref name="history_1933">{{cite news |url=http://nla.gov.au/nla.news-article41484208 |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=9 May 1933 |accessdate=24 June 2012 |page=8 column 7 |publisher=National Library of Australia|title=The Adelaide Cup – History Of A Great Event|author=G. K. Soward}}</ref> In 1889 running of the Cup resumed at [[Morphettville, South Australia|Morphettville]] when [[Richard Baker (Australian politician)|Sir Richard Baker]] and [[A. O. Whitington]] took over the course.<ref name="history_1933"/>

Starting on 1 March 1942, there was a ban on racing due to World War II and thus the race was not held in 1942 and 1943.<ref name="SAJC">{{cite web|url=http://www.sajc.com.au/about/history|title=SAJC History |work = [[South Australian Jockey Club]]|year=2012|accessdate=7 March 2013}}</ref>

In 1980 due to renovation of Morphettville Racecourse the race was run at [[Victoria Park Racecourse]] and in 2000 the Cup race day was abandoned due to rain and was rescheduled later and also run at Victoria Park.<ref name="SAJC"/>

The highest ever Cup attendance was in 1951 when 50,000 spectators attended.<ref name="SAJC"/>

The cup is held on the second Monday of March since March 2006. Before 2006 it was held in May. It was first run in 1864, just three years after the [[Melbourne Cup]] commenced.
The day received [[Public holidays in Australia#Other holidays|public holiday]] status in 1973 and became a major social event in South Australia.<ref name="holiday">{{cite web|url=http://www.timeanddate.com/holidays/australia/adelaide-cup|title=Holidays: Adelaide Cup in Australia |work = Time and Date|year=2013|accessdate=7 March 2013}}</ref> In the decade since the move of the Adelaide Cup to March, attendance has fallen to a third of previous levels, seeing 10,500 people trackside in 2015. This is due to use of the public holiday by other "Mad March" events (the Adelaide Festival of the Arts, the associated Fringe Festival, the WOMAD world music festival, a touring car race the weekend prior). Thoroughbred Racing SA and the South Australian Jockey Club wish to move the race and its associated public holiday out of March. However the South Australian government Minister for Racing said "We can't be at the whim of a racing club that wants to dictate when the public holiday is going to be" and suggests if the race be moved again then it be held on a Saturday.<ref name="ABC2015">{{cite web|title=Adelaide Cup: 'Mad March' depletes Morphettville Racecourse crowd, SAJC says|url=http://www.abc.net.au/news/2015-03-09/adelaide-cup-attendances-down-due-to-competing-march-events/6291028|website=ABC|publisher=Australian Broadcasting Corporation|accessdate=8 March 2017|date=10 Mar 2015}}</ref>

===Race grade===
The race was a Principal Race from 1864 until 1979 and a Group 1 race from 1979 until 2006.<ref name="adelaide_cup"/>
The race was downgraded from [[Group races|Group 1]] to Group 2 status following a decision by the [[Group races|Australian Pattern Committee]] in 2007.<ref name="adelaide_cup"/>

== The winners ==
{{refbegin|20em}}
* 2017 – Annus Mirabilis<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=13/03/2017&v=Morphettville&r=7 |title= 2017 Result &ndash; Ubet Adelaide Cup ''(held 13 March 2017)''|publisher=breednet.com|accessdate=13 March 2017}}</ref>
* 2016 – Purple Smile<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=14/03/2016&v=Morphettville&r=7 |title= 2016 Result &ndash; Ubet Adelaide Cup ''(held 14 March 2016)''|publisher=breednet.com|accessdate=14 March 2016}}</ref>
* 2015 – Tanby<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=9/03/2015&v=Morphettville&r=7&race=Tattsbet+Adelaide+Cup  |title= 2015 Result &ndash; Tattsbet Adelaide Cup ''(held 9 March 2015)''|publisher=breednet.com|accessdate=9 March 2015}}</ref>
* 2014 – Outback Joe<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=10/03/2014&amp;v=Morphettville&amp;r=7  |title= 2014 Result &ndash; Tattsbet Adelaide Cup ''(held 10 March 2014)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2013 – Norsqui<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=11/03/2013&amp;v=Morphettville&amp;r=7  |title= 2013 Result &ndash; Adelaide Casino Adelaide Cup ''(held 11 March 2013)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2012 – Rialya<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=12/03/2012&amp;v=Morphettville&amp;r=7  |title= 2012 Result &ndash; Adelaide Casino Adelaide Cup ''(held 12 March 2012)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2011 – Muir<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=14/03/2011&amp;v=Morphettville&amp;r=8  |title= 2011 Result &ndash; Adelaide Casino Adelaide Cup ''(held 14 March 2011)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2010 – Capecover<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=8/03/2010&amp;v=Morphettville&amp;r=9  |title= 2010 Result &ndash; Adelaide Casino Adelaide Cup ''(held 8 March 2010)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2009 – Zavite<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=9/03/2009&amp;v=MORPHETTVILLE&amp;r=9  |title= 2009 Result &ndash; Adelaide Cup ''(held 9 March 2009)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2008 – Lacey Underall<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=10/03/2008&amp;v=MORPHETTVILLE&amp;r=7  |title= 2008 Result &ndash; Adelaide Cup ''(held 10 March 2008)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2007 – Gallic<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=12/03/2007&amp;v=MORPHETTVILLE&amp;r=7  |title= 2007 Result &ndash; Adelaide Cup ''(held 12 March 2007)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2006 – Exalted Time<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=13/03/2006&amp;v=MORPHETTVILLE&amp;r=7  |title= 2006 Result &ndash; Adelaide Cup ''(held 13 March 2006)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2005 – Demerger<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=16/05/2005&amp;v=MORPHETTVILLE&amp;r=7  |title= 2005 Result &ndash; Adelaide Cup ''(held 16 May 2005)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2004 – Pantani<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=17/05/2004&amp;v=MORPHETTVILLE&amp;r=7  |title= 2004 Result &ndash; Adelaide Cup ''(held 17 May 2004)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2003 – Pillage 'N Plunder<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=19/05/2003&amp;v=MORPHETTVILLE&amp;r=7  |title= 2003 Result &ndash; Adelaide Cup ''(held 19 May 2003)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2002 – The A Train<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=20/05/2002&amp;v=MORPHETTVILLE&amp;r=7  |title= 2002 Result &ndash; Adelaide Cup ''(held 20 May 2002)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2001 – Apache King<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=21/05/2001&amp;v=MORPHETTVILLE&amp;r=8  |title= 2001 Result &ndash; Adelaide Cup ''(held 21 May 2001)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 2000 – Bohemiath<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=20/05/2000&amp;v=VICTORIA%20PARK&amp;r=7  |title= 2000 Result &ndash; Adelaide Cup ''(held 20 May 2000)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1999 – Sheer Kingston<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=17/05/1999&amp;v=MORPHETTVILLE&amp;r=8  |title= 1999 Result &ndash; Adelaide Cup ''(held 17 May 1999)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1998 – The Hind<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=18/05/1998&amp;v=MORPHETTVILLE&amp;r=8  |title= 1998 Result &ndash; Adelaide Cup ''(held 18 May 1998)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1997 – Cronus<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=19/05/1997&amp;v=MORPHETTVILLE&amp;r=7  |title= 1997 Result &ndash; Adelaide Cup ''(held 19 May 1997)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1996 – French Resort<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=20/05/1996&amp;v=MORPHETTVILLE&amp;r=7  |title= 1996 Result &ndash; Adelaide Cup ''(held 20 May 1996)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1995 – Scrupulous<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=15/05/1995&amp;v=MORPHETTVILLE&amp;r=7  |title= 1995 Result &ndash; Adelaide Cup ''(held 15 May 1995)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1994 – Our Pompeii<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=16/05/1994&amp;v=MORPHETTVILLE&amp;r=7  |title= 1994 Result &ndash; Adelaide Cup ''(held 16 May 1994)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1993 – Our Pompeii<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=17/05/1993&amp;v=MORPHETTVILLE&amp;r=6  |title= 1993 Result &ndash; Adelaide Cup ''(held 17 May 1993)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1992 – [[Subzero (horse)|Subzero]]<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=18/05/1992&amp;v=MORPHETTVILLE&amp;r=6  |title= 1992 Result &ndash; Adelaide Cup ''(held 18 May 1992)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1991 – Ideal Centreman<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=20/05/1991&amp;v=MORPHETTVILLE&amp;r=5  |title= 1991 Result &ndash; Adelaide Cup ''(held 20 May 1991)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1990 – Water Boatman<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=21/05/1990&amp;v=MORPHETTVILLE&amp;r=5  |title= 1990 Result &ndash; Adelaide Cup ''(held 21 May 1990)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1989 – [[Lord Reims]]<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=15/05/1989&amp;v=MORPHETTVILLE&amp;r=5  |title= 1989 Result &ndash; Adelaide Cup ''(held 15 May 1989)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1988 – [[Lord Reims]]<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=16/05/1988&amp;v=MORPHETTVILLE&amp;r=6  |title= 1988 Result &ndash; Adelaide Cup ''(held 16 May 1988)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1987 – [[Lord Reims]]<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=18/05/1987&amp;v=MORPHETTVILLE&amp;r=5  |title= 1987 Result &ndash; Adelaide Cup ''(held 18 May 1987)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1986 – Mr. Lomondy<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=19/05/1986&amp;v=MORPHETTVILLE&amp;r=5  |title= 1986 Result &ndash; Adelaide Cup ''(held 19 May 1986)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1985 – Toujours Mio<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=20/05/1985&amp;v=MORPHETTVILLE&amp;r=5  |title= 1985 Result &ndash; Adelaide Cup ''(held 20 May 1985)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1984 – Moss Kingdom<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=21/05/1984&amp;v=MORPHETTVILLE&amp;r=5  |title= 1984 Result &ndash; Adelaide Cup ''(held 21 May 1984)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1983 – Amarant<ref>{{cite web |url=http://www.breednet.com.au/stakes-race-results/stakesResults.asp?mdate=16/05/1983&amp;v=MORPHETTVILLE&amp;r=5  |title= 1983 Result &ndash; Adelaide Cup ''(held 16 May 1983)''|publisher=breednet.com|accessdate=3 March 2015}}</ref>
* 1982 – Dealer's Choice
* 1981 – [[Just A Dash]]
* 1980 – Yashmak
* 1979 – Panamint
* 1978 – [[Hyperno]]
* 1977 – Reckless
* 1976 – Grand Scale
* 1975 – Soulman
* 1974 – Phar Ace
* 1973 – Tavel
* 1972 – Wine Taster
* 1971 – Laelia
* 1970 – Tavel
* 1969 – Gnapur
* 1968 – [[Rain Lover]]
* 1967 – Fulmen
* 1966 – Prince Camillo
* 1965 – Hunting Horn
* 1964 – Jamagne
* 1963 – Woolstar
* 1962 – Cheong Sam
* 1961 – Far Away Places
* 1960 – Lourdale
* 1959 – Mac
* 1958 – Star Aim
* 1957 – Borgia
* 1956 – Pushover
* 1955 – Storm Glow
* 1954 – Spearfolio
* 1953 – Royal Padjeant
* 1952 – Aldershot
* 1951 – Peerless Fox
* 1950 – Peerless Fox
* 1949 – Colin
* 1948 – Sanctus
* 1947 – Beau Cheval
* 1946 – Little Tich
* 1945 – Blankenburg
* 1944 – Chief Watchman
* 1943 – †''race not held''
* 1942 – †''race not held''
* 1941 – Yodvara
* 1940 – Apostrophe
* 1939 – Son Of Aurous
* 1938 – Dartford
* 1937 – Donaster
* 1936 – Cape York
* 1935 – Mellion
* 1934 – Sir Roseland
* 1933 – Infirmiere
* 1932 – Romany Rye
* 1931 – Suzumi
* 1930 – Temptation
* 1929 – Parallana
* 1928 – Altimeter
* 1927 – Three Kings
* 1926 – Spearer
* 1925 – Stralia
* 1924 – Wynette
* 1923 – King Ingoda
* 1922 – Repique
* 1921 – Sir Marco
* 1920 – Wee Gun
* 1919 – Dependence
* 1918 – Elsdon
* 1917 – Greencap
* 1916 – St. Spasa
* 1915 – Naxbery
* 1914 – Hamburg Belle
* 1913 – Midnight Sun
* 1912 – Eye Glass
* 1911 – Eye Glass
* 1910 – Medaglia
* 1909 – Kooringa
* 1908 – Destinist
* 1907 – Spinaway
* 1906 – Dynamite
* 1905 – Troytown
* 1904 – Sport Royal
* 1903 – Sojourner
* 1902 – The Idler
* 1901 – Gunga Din
* 1900 – Tarquin
* 1899 – Contrast
* 1898 – Paul Pry
* 1897 – Mora
* 1896 – Warpaint
* 1895 – Elswick
* 1894 – Port Admiral
* 1893 – Vakeel
* 1892 – Jericho
* 1891 – Stanley
* 1890 – Shootover
* 1889 – The Lawyer
* 1888 – ''race not held''
* 1887 – ''race not held''
* 1886 – ''race not held''
* 1885 – ‡Lord Wilton 
* 1884 – [[Malua (horse)|Malua]]
* 1883 – Sting
* 1882 – Euclid
* 1881 – Totalisator
* 1880 – Firstwater
* 1879 – Banter
* 1878 – Glenormiston
* 1877 – Aldinga
* 1876 – Impudence
* 1875 – Lurline
* 1874 – Ace Of Trumps
* 1873 – Dolphin
* 1872 – Australian Buck
* 1871 – ''race not held''
* 1870 – ''race not held''
* 1869 – Norma
* 1868 – Cupbearer
* 1867 – Cowra
* 1866 – Cowra
* 1865 – Ebor
* 1864 – Falcon<ref name="Res_1864"/>
{{refend}}
{{refbegin}}
† Race not held due to a ban on war time racing in the state.<ref name="holiday"/><br/>
‡ Race held at [[Flemington Racecourse|Flemington]], [[Victoria (Australia)|Victoria]] due to the Totalizator Repeal Act 1883.<ref name="history_1933"/>
{{refend}}

==See also==

* [[List of Australian Group races]]
* [[Group races]]

==References==

{{reflist|2}}

[[Category:Horse races in Australia]]
[[Category:Open long distance horse races]]
[[Category:Sport in Adelaide]]
[[Category:Public holidays in Australia]]