{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Dick Sand, A Captain at Fifteen| title_orig    = Un capitaine de quinze ans
| translator    = Ellen Elizabeth Frewer, anonymous
| image         = 'Dick Sand, A Captain at Fifteen' by Henri Meyer 001.jpg
| image_size = 200px
| author        = [[Jules Verne]]
| illustrator   = Henri Meyer
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #17
| genre         = [[Adventure novel]] 
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 15 December 1878
| english_pub_date = 1878
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| preceded_by   = [[The Child of the Cavern]]
| followed_by   = [[The Begum's Fortune]]
}}

'''''Dick Sand, A Captain at Fifteen''''' ({{lang-fr|Un capitaine de quinze ans}}) is a [[Jules Verne]] novel published in [[1878 in literature|1878]].  It deals primarily with the issue of slavery, and the African [[slave trade]] by other Africans in particular.

== Themes ==
Themes explored in the novel include:
* The painful learning of adult life - the hero, Dick Sand, must assume command of a ship after the death of his captain.
* The discovery of entomology
* Condemnation of slavery
* Revenge

== Plot ==
Dick Sand is a fifteen-year-old boy that serves on the schooner ''Pilgrim'', a [[whaler]] that normally voyages across the Pacific in their efforts to find targets. However, this time the hunting season has been unsuccessful, and as they plan to return home three people request passage to [[Valparaíso|Valparaiso]]: Mrs Weldon, the wife of the hunting firm's owner, her five-year-old son Jack, his old nanny Nan and her cousin Bénédict, an entomologist. With not much of a choice, the captain accepts.

Several days into their journey north-east, the ''Pilgrim'' encounters a shipwreck, with only five African-American survivors (Tom, Actéon, Austin, Bat and Hercule) plus a dog (Dingo), all of whom are brought into the ship and offered passage to America.

It is as they get closer east that they encounter a whale, and the captain and crew decide to hunt it, in an attempt to make some profit off the season. Captain Hull reluctantly leaves Dick responsible for the ship in his absence while the rest of the crew approaches the whale on a smaller boat. However, the whale, while defending itself, destroys the boat and kills the crew, leaving Dick in charge of a ship with no experienced sailors to help him: only the shipwreck survivors are well enough to help him.

However, the ship's cook, Negoro, has sinister plans for the ship: after breaking one of the ship's compasses and leaving them without a measuring device, he places a magnet on the other compass to trick the inexperienced crew into changing their route. In spite of the longer than expected travel, the group perseveres, and finally makes land, although the ''Pilgrim'' is lost. Negoro escapes with Mrs Weldon's money.

A man called Harris meets the group and assures them they are in the Bolivian coast, encouraging them to follow him into the jungle, saying he can lead them to a nearby city. Dick begins to suspect that they are being lied to as they encounter several animals Harris insists are native, but do not seem to be like any he knows. It is as they hear a lion's roar and a horrorized Tom (who had been enslaved in his youth) finds several implements, that Dick realizes they are in Africa: Dick further learns, from eavesdropping into Harris, that they are in Angola, that Harris is Negoro's partner in crime, and that he was leading the group in to weaken them and make it easier for him to take Tom and the others into slavery.

Dick and Tom's group decides to keep the truth from Mrs Weldon and her family, knowing that it will cause undue stress in them: instead, they tell Mrs Weldon that Harris lied to them, and Dick tries to lead the group to a river, which he hopes will allow them to reach the shore. A great storm leads the group to take refuge in a large ant nest, but the nest becomes flooded and the group barely manages to escape alive. This time, though, they encounter a group of slave traders led by Harris, who takes them prisoners: only Hercules and Dingo manage to escape, killing several traders and getting into the thicket to avoid detection. Dick, along with Tom, Bat, Actéon and Austin, is taken separate from Mrs Waldon's family, while Nan dies after a forced walk becomes too much for her.

Both groups are taken to the lands of the king of Kazoonde, an old, petty king that trades with slave traders (among them Harris, Negoro and their chief, José Antonio Álvez) for European commodities. While Actéon, Austin, Bat and Tom are sold into slavery while none of the others can prevent it, Harris decides to taunt Dick by telling him Mrs Weldon, Jack and Bénédict have died as well: Dick answers by jumping on Harris and killing him with his own knife. Dick is made prisoner, to await his death. That night, however, the king (who drinks much alcohol, provided by Álvez) dies, burning alive, after he tries to drink flaming punch. The king's first wife takes over, and, in the subsequent funeral ceremony, nearly all of the king's wives, along with Dick Sand, are sacrificed.

However, Mrs. Weldon, Jack and Bénédict are alive, kept prisoner in Álvez's factory: Negoro intends to use them to blackmail Mrs Weldon's husband into paying him one hundred thousand dollars. Mrs Weldon rejects Negoro's demand to write a letter to her husband. However, her hopes that [[David Livingstone]], who is expected to pass by Kazoondé some time soon, will be able to free her and her family, die when the explorer passes away: defeated, she writes the letter, which Negoro will take to San Francisco. Meanwhile, Bénédict (who is allowed out of the factory to pursue his passion for entomology) becomes distracted with a mysterious bug (which he cannot see well because the king of Kazoondé stole his glasses): this bug leads him out of the town and into the jungle, where a large man captures him and takes him away.

It is at this point where the weather suddenly changes: torrential rains submerge the harvests, putting the town at risk of famine. The queen and her ministers have no idea of how to revert this trouble, and none of the local "mgangas" (shamans) are capable of putting a stop to the bad weather, leading the queen to hire a famous mganga living in the north of Angola. This mganga arrives a few days later and, in a ceremony, makes to sacrifice young Jack, before taking him and Mrs Weldon away... to Dick: the mganga is Hercules, who was also the one to take Bénédict away, and also rescued Dick from drowning. Now reunited, the group takes a canoe downriver, braving several dangers in an attempt to reach the coast. On the way, Dingo leads them to a hut, out of which a tree is marked with the letters SV (the same on Dingo's collar), a corpse and a box with a small letter that reveals the corpse as Samuel Vernon, a French explorer who was betrayed and murdered by his guide, Negoro (which explains why Dingo constantly growled at the cook). Negoro appears just then, and Dingo kills him, not without dying from a mortal injury caused by Negoro.

The cook, however, is being followed by a group of natives on a canoe, and Dick decides to take on them while Hercules gets the rest of the group away: in the fight on board of the canoe, which takes place close to a large waterfall, Dick manages to destroy the canoe's oar and saves himself from the waterfall by using the canoe to protect himself while the natives die. Reunited with his friends again, the group meets and joins a caravan of Portuguese traders that are going to the coast, where they take a ship that gets them to San Francisco.

Dick is adopted by the Weldons, who also take Hercules in, thankful for all that he has done. Dick eventually manages to finish his studies, becoming a captain under Mr Weldon, who also finds, thanks to his contacts, where Tom, Actéon, Austin and Bat are, freeing them and bringing them to San Francisco, reuniting the group after so long a struggle.

== Full text ==
* {{Gutenberg|no=9150|name=Dick Sands the Boy Captain}}, English translation by Ellen E. Frewer published in 1879.
* {{librivox book | title=Dick Sands the Boy Captain | author=Jules Verne}}
* {{Gutenberg|no=12051|name=Dick Sand; or, a Captain at Fifteen}}, a different English translation published in 1878.
{{Wikisourcelang|fr|Un capitaine de quinze ans}}

{{Commons category|Dick Sand, A Captain at Fifteen}}
<!-- Templates -->
{{Verne}}

{{Authority control}}

{{DEFAULTSORT:Dick Sand, A Captain At Fifteen}}
<!-- Categories -->
[[Category:1878 novels]]
[[Category:Novels by Jules Verne]]
[[Category:Novels set in colonial Africa]]
[[Category:Novels about slavery]]
[[Category:Nautical fiction]]


{{1870s-novel-stub}}