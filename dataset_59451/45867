{{Italic title|reason=[[:Category:Japanese words and phrases]]}}
[[File:Goyō Hashiguchi (1915) Yokugo no onna (cropped and compressed) 01.jpg|thumb|''Woman At Her Bath'', by [[Hashiguchi Goyō]] (1915)]]
[[File:'Iwabuchi', woodblock print by Charles W. Bartlett.jpg|thumb|''Iwabuchi'', by [[Charles W. Bartlett]] (1916)]]
[[File:Kawase Zôjôji.jpg|thumb|''Zōjōji in Shiba'', by [[Kawase Hasui]] (1925)]]
[[File:Hiroshi Yoshida01a.jpg|thumb|''Avenue of [[Sugi]] trees'', by [[Yoshida Hiroshi]] (1937)]]
{{nihongo|'''''Shin-hanga'''''|新版画|extra=lit. "new prints", "new woodcut (block) prints"}} was an [[art movement]] in early 20th-century [[Japan]], during the [[Taishō period|Taishō]] and [[Showa period|Shōwa period]]s, that revitalized traditional ''[[ukiyo-e]]'' art rooted in the [[Edo period|Edo]] and [[Meiji period]]s (17th–19th century). It maintained the traditional ''ukiyo-e'' collaborative system (''hanmoto'' system) where the artist, carver, printer, and publisher engaged in division of labor, as opposed to the ''[[sōsaku-hanga]]'' (creative prints) movement which advocated the principles of "self-drawn" (''jiga''), "self-carved" (''jikoku'') and "self-printed" (''jizuri''), according to which the artist, with the desire of expressing the self, is the sole creator of art.

The movement flourished from around 1915 to 1942, though it resumed briefly from 1946 through the 1950s. Inspired by European [[Impressionism]], the artists incorporated Western elements such as the effects of light and the expression of individual moods, but focused on strictly traditional themes of landscapes (''fukeiga''), famous places (''meishō''), beautiful women (''[[bijinga]]''), [[kabuki]] actors (''[[yakusha-e]]''), and birds-and-flowers (''[[kachō-e]]'').

== History ==
The term ''shin-hanga'' was coined in 1915 by [[Watanabe Shozaburo|Watanabe Shōzaburō]] (1885–1962), the most important publisher of ''shin-hanga'', with the aim of differentiating ''shin-hanga'' from the commercial mass art that ''ukiyo-e'' had been, though it was driven largely by exports to the United States.

''Shin-hanga'' prints were directed to a Western audience largely through Western patronage and art dealers such as [[Bob Muller|Robert O. Muller]] (1911-2003). Directed primarily to foreign markets, ''shin-hanga'' prints appealed to Western taste for nostalgic and romanticized views of Japan. ''Shin-hanga'' prints flourished and enjoyed immense popularity overseas. In the 1920s, there were articles on ''shin-hanga'' in the International Studio, the Studio, the Art News and the Art Digest magazines. In 1921, a ''Shinsaku-hanga Tenrankai'' ("New Creative Print Exhibition") was held in Tokyo. One hundred and fifty works by ten artists were exhibited. In 1930 and 1936, two major ''shin-hanga'' exhibitions were held at the [[Toledo Museum of Art]] in Ohio. They were the largest showcases of ''shin-hanga'' prints at the time.

Ironically, there was not much domestic market for ''shin-hanga'' prints in Japan. ''Ukiyo-e'' prints were considered by the Japanese as mass commercial products, as opposed to the European view of ''ukiyo-e'' as [[fine art]] during the climax of [[Japonisme]]. After decades of [[modernization]] and [[Westernization]] during the [[Meiji period|Meiji]] era, [[architecture]], art and clothing in Japan came to follow Western modes. Japanese art students were trained in the Western tradition. Western [[oil paintings]] (''[[Yōga (art)|yōga]]'') were considered high art and received official recognition from the [[Bunten]] (The Ministry of Education Fine Arts Exhibition). ''Shin-hanga'' prints, on the other hand, were considered as a variation of the outdated ''ukiyo-e''. They were dismissed by the Bunten and were subordinated under [[oil paintings]] and [[sculptures]].

''Shin-hanga'' declined as the military government tightened its control over the arts and culture during wartime. In 1939, the Army Art Association was established under the patronage of the Army Information Section to promote war art. By 1943, an official commission for war painting was set up and artists’ materials were rationed. Overseas market for Japanese prints declined drastically at the same time. ''Shin-hanga'' never regained its momentum in [[postwar Japan]]. Instead, ''[[sōsaku-hanga]]'' emerged as the genuine heir of the ''ukiyo-e'' woodblock tradition and enjoyed immense popularity and prestige in the international art scene.

==Subject matter and technique==
The nostalgic and romanticized views of Japan that ''shin-hanga'' artists offered reveal the ways artists perceive their own environment in the midst of transformation. Most ''shin-hanga'' landscape prints (which constitute seventy percent of ''shin-hanga'' prints) feature places that are obscure and tranquil. Artists such as [[Kawase Hasui]] (1883–1957) produced dreamlike qualities in their prints, yearning for rural roots and the warm wooden architecture that was disappearing in urban Tokyo.

==Shin-hanga vs. Ukiyo-e==
''Shin-hanga'' is often defined as "neo-ukiyo-e" under the shadow of the ''ukiyo-e'' tradition. While ''shin-hanga'' prints retain much of the ''ukiyo-e'' tradition in terms of subject matter, they reveal vastly different techniques and sensibilities. Inspired by Western [[Realism (visual arts)|Realism]], ''shin-hanga'' artists produce hybrids that combine modern design with traditional subjects. The use of naturalistic light, colored lines, soft colors, 3-dimensionality, deep space are artistic innovations that break with the ''ukiyo-e'' tradition.

==Shin-hanga vs. Sōsaku-hanga==
The ''shin-hanga'' movement is often defined in opposition to the ''[[sōsaku-hanga]]'' movement (creative print movement) that began in the 1910s. While ''sōsaku-hanga'' artists advocated the principles of "self-drawn" (''jiga''), "self-carved" (''jikoku'') and "self-printed" (''jizuri''), according to which the artist engages in artistic expression by involving himself in all stages of the [[printmaking]] process, ''shin hanga'' artists continued to collaborate with carvers, printers and publishers in print production. At the core of the ''shin-hanga'' and ''sōsaku-hanga'' dichotomy is the debate of what constitutes a creative print or pure art. ''Shin-hanga'' artists and publishers believed that their works were as creative as those produced by ''sōsaku-hanga'' artists. In 1921, Watanabe Shōzaburō even used the term ''shinsaku-hanga'' ("new made prints") to emphasize the creative aspects of ''shin-hanga''.

In a larger context, the dichotomy between ''shin-hanga'' and ''sōsaku-hanga'' was but one of many tensions in the Japanese art scene during decades of modernization, Westernization and [[internationalization]]. Parallel to the ''shin-hanga''/''sōsaku-hanga'' antagonism was the polarization between Japanese paintings (''[[nihonga]]'') and Western paintings (''[[Yōga (art)|yōga]]''), along with the flowering of many artistic currents such as [[futurism (art)|futurism]], [[avant-garde]], [[proletarian art]], and the ''[[mingei]]'' (folk art) movement, all of which were actively seeking a voice in the art scene in the period from 1910 to 1935 before the rise of [[militarism]] in Japan.

==Notable artists==
*[[Itō Shinsui]]
*[[Kawase Hasui]]
*[[Hashiguchi Goyō]]
*[[Charles W. Bartlett]]
*[[Kiyokata Kaburagi|Kaburagi Kiyokata]]
*{{illm|Hirano Hakuhō|ja|平野白峰}}
*[[Yoshida Hiroshi]]
*[[Ohara Koson]]
*[[Torii Kotondo]]
*[[Natori Shunsen]]
*[[Hiroaki Takahashi (also known as Shotei)]]
*Yamamura Toyonari
*Elizabeth Keith
*Fritz Capelari
*Shiro Kasamatsu
*Takeji Asano
*Koichi Okada
*Tsuchiya Koitsu
*[[Ota Masamitsu]] (also known as Ota Gako)
*[[Tsuchiya Rakusan]]

==Reference and Further Reading==
*Blair, Dorothy. ''Modern Japanese prints: printed from a photographic reproduction of two exhibition catalogues of modern Japanese prints published by the Toledo Museum of Art in 1930-1936''. Ohio: Toledo Museum of Art, 1997.
*Brown, K. and Goodall-Cristante, H. ''Shin-Hanga: New Prints in Modern Japan''. Los Angeles County Museum of Art, 1996. ISBN 0-295-97517-2 
*Hamanoka, Shinji. ''Female Image: 20th Century Prints of Japanese Beauties''. Hotei Publishing 2000. ISBN 90-74822-20-7 
*Jenkins, D. ''Images of a Changing World: Japanese Prints of the Twentieth Century''. Portland: Portland Art Museum, 1983. ISBN 0-295-96137-6 
*Menzies, Jackie. ''Modern boy, Modern Girl: Modernity in Japanese Art 1910-1935''. Sydney, Australia: Art Gallery NSW, c1998. ISBN 0-7313-8900-X  
* Merritt, Helen and Nanako Yamada.  (1995). ''Guide to Modern Japanese Woodblock Prints, 1900-1975.'' Honolulu: University of Hawaii Press.	ISBN 9780824817329; ISBN 9780824812867; [http://www.worldcat.org/title/guide-to-modern-japanese-woodblock-prints-1900-1975/oclc/247995392  OCLC 247995392]
*Merritt, Helen. ''Modern Japanese Woodblock Prints: The Early Years''. Honolulu: University of Hawaii Press 1990. ISBN 0-8248-1200-X 
*Mirviss, Joan B. ''Printed to Perfection: Twentieth-century Japanese Prints from the Robert O. Muller Collection''. Washington D.C.: Arthur M. Sackler Gallery, Smithsonian Institution and Hotei Publishing 2004. ISBN 90-74822-73-8 
* Newland, Amy Reigle. (2005). ''Hotei Encyclopedia of Japanese Woodblock Prints.''  Amsterdam: Hotei. ISBN 9789074822657; [http://www.worldcat.org/title/hotei-encyclopedia-of-japanese-woodblock-prints/oclc/61666175  OCLC 61666175] 
*Smith, Lawrence. ''Modern Japanese Prints 1912-1989''. New York, London, Paris: Cross River Press, 1994. 
*Swinton, Elizabeth de Sabato. ''Terrific Tokyo: A panorama in Prints from the 1860s to the 1930s''. Worcester: Worcester Art Museum, 1998. ISBN 0-936042-00-1
*Masuda, Koh. ''Kenkyusha's New Japanese-English Dictionary'', Kenkyusha Limited, Tokyo 1991, ISBN 4-7674-2015-6

==External links==
{{commons category|Shin Hanga}}
*[http://www.viewingjapaneseprints.net/texts/shinhangatexts/shinhanga_intro.html Shin hanga — ''Viewing Japanese Prints'', a website by John Fiorillo]
*[http://www.artelino.com/articles/shin_hanga.asp Shin hanga — ''artelino - Art Auctions'']
*[http://www.hanga.com/side.cfm Side gallery of Hanga Gallery] Information, print gallery,...
*[http://www.asia.si.edu/exhibitions/online/dreamWorlds/default.htm ''Dream Worlds: Modern Japanese Prints and Paintings from the Robert O. Muller Collection (Online Exhibition)'']
*[http://www.moma.org/exhibitions/2001/whatisaprint/flash.html “What is a Print?”] An excellent flash-demonstration of the printmaking process.
*[http://www.aoigallery.com/ www.AoiGallery.com - Shin Hanga Art Gallery]
*[http://japaneseartsgallery.com/gallery/woodblock-prints/takeji-asano/ Shin Hanga Prints by Takeji Asano]
*[http://shotei.com/articles/bobmuller/bobmuller.htm - Robert O. Muller] Information about the man behind one of the most well known collections of Shin Hanga.

{{Ukiyo-e}}

[[Category:Ukiyo-e genres]]
[[Category:Schools of Japanese art]]