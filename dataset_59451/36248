{{good article}}
{{Use mdy dates|date=March 2015}}
{{Infobox film
| name           = The American and the Queen
| image          =
| caption        =
| director       =
| producer       = [[Thanhouser Company]]
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = [[Motion Picture Distributing and Sales Company]]
| released       = {{film date|1910|11|11}}
| runtime        =
| country        = United States
| language       = Silent film<br> [[English language|English]] inter-titles
}}

'''''The American and the Queen''''' is a 1910 American [[silent film|silent]] [[short film|short]] [[drama]] produced by the [[Thanhouser Company]]. The film focuses on Maud, the fictional queen of Rumania, who is overthrown by her cousin, Rupert. Maud is thrown into prison after refusing the romantic advances of Rupert. She escapes with the aid of her [[lady-in-waiting]] and a priest. A wealthy American named Jack Walton, foils an assassination attempt on Maud and he falls in love with her. Maud is recaptured and set to be executed when the priest comes up with a plan to save her, by marrying Jack and Maud. The ceremony takes place through her cell window, and soon the United States military arrives to save the now wife of an American. Rupert is killed in the ensuing conflict. No known cast or production credits for the film is known. The film was released on November 11, 1910 and was met with neutral to negative reviews by critics. The patriotic element of the film was cited as likely being comical for European audiences and the film was also used as an example of an inappropriate example of American flag-waving. The film is presumed [[lost film|lost]].

== Plot ==
Though the film is presumed [[lost film|lost]], a synopsis survives in ''[[The Moving Picture World]]'' from October 22, 1910. It states: "Maud, the beautiful queen of Rumania, is deposed through the efforts of her wicked cousin, Rupert, who seizes the throne. He tries to make love to Maud, but when she spurns him he has her thrown into prison. Through the efforts of her faithful [[lady-in-waiting]], and the kindly priest, the young queen escapes. While on a steamer, her cousin's spy locates her and decides to give her poison, but his attempt is detected by Jack Walton, wealthy young American who has seen the queen and fallen in love with her without knowing anything as to her history. A number of noblemen urge the queen to make an effort to regain the throne, and when Jack joins his plea to theirs, she consents. But the queen is only in her country a few hours when she is arrested during the absence of Jack on a mission."<ref name=queen />

"Jack prepares to save the queen, but has hit on no definite plan. Then Father Paul proposes a scheme. The queen's prison is on the ground floor; it is possible for her to stretch her hand through the bars. And Jack marries the woman he loves, though a stone wall is between them. The priest, with the bridegroom is outside, and the careless guards do not see them. Rupert then sends for his cousin again, and for the second time he offers to marry her. But she spurns him. Then he orders her execution. The sentence is about to be carried out when Jack arrives with a squad of U.S. troops from a troop ship in the harbor. The queen is entitled to their protection, for she is now the wife of an American. And she gets it. Rupert loses his life and the ensuing combat."<ref name=queen />

== Production ==
The writer of the scenario is unknown, but it was most likely [[Lloyd Lonergan]]. He was an experienced newspaperman employed by ''[[The New York Evening World]]'' while writing scripts for the Thanhouser productions.<ref>{{cite web | url=http://www.thanhouser.org/tcocd/Biography_Files/2fyqag.htm | title=Volume 3: Biographies - Lonergan, Lloyd F. | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=January 17, 2015 | author=Q. David Bowers}}</ref> A reviewer said that the film was a poor take on [[Anthony Hope]], but did not further expand on the nature of this connection.<ref name=queen />{{refn|group=note|Bowers has "Anthony Hope" in italics, but it is unlikely that this was intended to reflect a work. Anthony Hope was English novelist and playwright whose works were well-known at the time.}} This was likely a reference to Anthony Hope's works like ''[[The Prisoner of Zenda]]'' and may have been made due to the arrival of a foreigner who resolves a royal conspiracy. Bowers would list this film as being of "patriotic" character instead of listing it simply as a drama or a comedy production.<ref name=queen /> The patriotic element of the film was heavily promoted in advertising and was likely true given the reaction of viewers to the film.<ref name=queen /> One article in ''[[The Moving Picture News]]'' referred to the film as an example of a problematic use of the [[Flag of the United States|United States flag]] appearing suddenly with the [[United States Marine Corps|marines]] at the climax of the plot, all while on foreign soil.<ref>{{cite web | url=https://archive.org/stream/movingpicturenew04unse#page/n1379/mode/1up | title=Moving Picture News Jan-Dec 1911 | publisher=Cinematograph Publishing Company | date=1911 | accessdate=6 March 2015 | pages=1379}}</ref>

The film director is unknown, but it may have been [[Barry O'Neil]] or [[Lucius J. Henderson]]. Cameramen employed by the company during this era included [[Blair Smith]], [[Carl Louis Gregory]], and [[Alfred H. Moses, Jr.]] though none are specifically credited.<ref name=cast /> The role of the cameraman was uncredited in 1910 productions.<ref name=begin>{{cite web | url=http://www.thanhouser.org/tcocd/Narrative_files/c3s1.htm | title=Volume 1: Narrative History - Chapter 3 - 1910: Film Production Begins | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=January 14, 2015 | author=Q. David Bowers}}</ref> The cast credits are unknown, but many 1910 Thanhouser productions are fragmentary.<ref name=cast>{{cite web | url=http://www.thanhouser.org/tcocd/Filmography_files/ind6i4_n5.htm | title=Volume 2: Filmography - Thanhouser Filmography - 1910 | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=February 12, 2015 | author=Q. David Bowers}}</ref> In late 1910, the Thanhouser company released a list of the important personalities in their films. The list includes [[G.W. Abbe]], [[Justus D. Barnes]], [[Frank H. Crane]], [[Irene Crane]], [[Marie Eline]], [[Violet Heming]], [[Martin J. Faust]], [[Thomas Fortune]], [[George Middleton (actor)|George Middleton]], [[Grace Moore (Thanhouser actress)|Grace Moore]], [[John W. Noble (actor)|John W. Noble]], [[Anna Rosemond]], [[Mrs. George Walters]].<ref>{{cite web | url=http://www.thanhouser.org/tcocd/Filmography_files/ind6i4_n5.htm | title=Volume 2: Filmography -Thanhouser Filmography - 1910 | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=February 24, 2015 | author=Q. David Bowers}}</ref>

==Release and reception ==
The single reel drama, approximately 1,000 feet long, was released on November 11, 1910.<ref name="queen">{{cite web | url=http://www.thanhouser.org/tcocd/Filmography_files/6141_h.htm | title=Volume 2: Filmography - The American and the Queen | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=March 5, 2015 | author=Q. David Bowers}}</ref> The film was originally planned to be released on November 4, but it was delayed and some advertisements and later listings for the film give the erroneous date.<ref name=queen /> The film received negative reviews in trade publications from even those which typically praised even the weaker productions. Though it was not well-received, the production was not criticized as sharply as ''[[Avenged (1910 film)|Avenged]]'' had been.<ref name=ven>{{cite web | url=http://www.thanhouser.org/tcocd/Filmography_files/indlyp5tg.htm | title=Volume 2: Filmography - Avenged | work=Thanhouser Films: An Encyclopedia and History | date=1995 | accessdate=February 26, 2015 | author=Q. David Bowers}}</ref> Walton of ''[[The Moving Picture News]]'' stated that the film was "[a] very feeble echo on Anthony Hope. The throne room staging is ridiculous and so is the resolution. The flag-waving business is feebly melodramatic. Princes, etc. don't sit on that sort of chair. I'm an admirer of Thanhouser, but this thing is straight, plain punk. In Europe it will be received as a [[Yankee]] joke."<ref name=queen /> The reviewer for ''[[The Moving Picture World]]'' was decidedly neutral, highlighting only the novelty of the plot and refraining from any criticism or praise of the merits of the production.<ref name=queen /> ''The New York Dramatic Mirror'' was negative, but not scathing. The reviewer stated, "The narrative is very entertaining - or could be made so, if the producer had cared to trouble himself about details. In its present crude form, its wild impossibility rather grates upon a spectator, because every scene in the film has some inconsistency. At least, it was worth doing well. Unfortunately, neither the acting nor the mounting has any particular merit."<ref name=queen />

==See also==
* [[List of American films of 1910]]

==Notes==
{{reflist|group=note}}

== References ==
{{reflist|30em}}

{{DEFAULTSORT:American and the Queen, The}}
[[Category:1910 films]]
[[Category:1910s drama films]]
[[Category:American drama films]]
[[Category:American short films]]
[[Category:American silent short films]]
[[Category:American black-and-white films]]
[[Category:Thanhouser Company films]]
[[Category:Lost films]]