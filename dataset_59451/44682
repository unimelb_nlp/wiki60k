{{Infobox journal
| title = Behavior Genetics
| cover = [[File:Behavior Genetics cover.jpg]]
| editor = [[John K. Hewitt]]
| discipline = [[Behavioural genetics|Behavior genetics]]
| abbreviation = Behav. Genet.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Bimonthly
| history = 1971–present
| openaccess = Hybrid
| impact =  3.268
| impact-year = 2015
| website = http://www.springer.com/psychology/psychology+general/journal/10519
| link1 = http://www.springerlink.com/content/0001-8244
| link1-name = Online access
| OCLC = 01519335
| LCCN = 77015467
| CODEN = BHGNAT
| ISSN = 0001-8244
| eISSN = 1573-3297
}}
{{Portal|Neuroscience}}
'''''Behavior Genetics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published monthly by [[Springer Science+Business Media]] that is devoted to "research in the inheritance of behavior". It is the official journal of the [[Behavior Genetics Association]]. The journal was established in 1971 with [[Steven G. Vandenberg]] as its founding [[editor-in-chief]]. The abstracts of the annual meetings are printed in the journal. Each year, the [[editorial board]] chooses a particularly meritorious paper in the previous year's volume of the journal for the Fulker Award, acknowledged by "$1000 and a good bottle of wine" as well as a citation made in the journal.<ref>[http://www.bga.org/pages/11/ByLaws.html BGA By Laws]</ref> This award was created in the honor of [[David Fulker]], a past president of the Behavior Genetics Association (1982) and former editor-in-chief of the journal.

== Abstracting and indexing ==
The journal is abstracted in ''[[Biological Abstracts|Biological Abstracts/BIOSIS Previews]], [[CAB Direct (database)|CAB Abstracts]], [[Current Awareness in Biological Sciences]], [[Current Contents]]/Social & Behavioral Sciences'' and ''Life Sciences, [[EMBASE]], [[PsycINFO]], [[PubMed]]/[[MEDLINE]], [[Science Citation Index]], [[Scopus]], [[Social Sciences Citation Index]]'', and ''[[The Zoological Record]]''. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 2.520, ranking it 26th out of 48 in the category "Behavioral Sciences"<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Behavioral Sciences |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> and 81st out of 158 in the category "Genetics".<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Genetics |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/psychology/psychology+general/journal/10519}}

{{DEFAULTSORT:Behavior Genetics}}
[[Category:Behavioural genetics journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1971]]
[[Category:Bimonthly journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Twin studies]]