{{refimprove|date=November 2016}}{{Infobox military person
|honorific_prefix=
|name=Helmut Bennemann
|birth_date={{Birth date|1915|3|16|df=y}}
|death_date=  {{death date and age|2007|11|17|1915|3|16|df=y}}
|birth_place=[[Wanne-Eickel]]
|death_place=[[Bad Sassendorf]]
|image=Helmut Bennemann.jpg
|caption=Helmut Bennemann
|nickname=
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=
|rank=[[Oberstleutnant]]
|commands=I./[[JG 52]], [[JG 53]]
|unit=[[JG 52]], [[JG 53]]
|battles=[[World War II]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Helmut Bennemann''' (16 March 1915 – 17 November 2007) was a ''[[Oberstleutnant]]'' of [[Nazi Germany]]'s [[Luftwaffe]] in [[World War II]]. Bennemann claimed 93 aerial victories in over 400 combat missions. The majority of his victories were claimed over the [[Eastern Front (World War II)|Eastern Front]]. His commands included ''[[Geschwaderkommodore]]'' of the [[JG 53]] fighter wing.

==World War II==
Helmut Bennemann held the position of Adjutant of I./[[Jagdgeschwader 52]] (JG 52—52nd Fighter Wing) in June 1940. He claimed his first aerial victory on 26 August, when he shot down a [[Royal Air Force]] (RAF) [[Supermarine Spitfire|Spitfire]] near [[Dover]]. On 15 September, Bennemann claimed three RAF [[Hawker Hurricane|Hurricane]] shot down to record his sixth through eighth victories. On 27 April 1941, he was appointed ''[[Staffelkapitän]]'' (Squadron Leader) of 3./JG 52. By the time I./JG 52 was transferred to the [[Eastern Front (World War II)|Eastern Front]] in September 1941, Bennemann had claimed 12 victories. On 14 June 1942, Bennemann was promoted to ''[[Gruppenkommandeur]]'' (Group Commander) of I./JG 52. By the End of 1942 his score stood at 72 victories.

With his score at 88 victories, Benemann was severely wounded by the explosion of an incendiary bomb on 10 May 1943. Convalescence kept him from combat duty for some months.

Bennemann was appointed ''[[Geschwaderkommodore]]'' of [[Jagdgeschwader 53]] (JG 53—53rd Fighter Wing) on 9 November 1943, taking over command from ''[[Oberst]]'' [[Günther Freiherr von Maltzahn]]. On 25 April 1944, he shot down a [[United States Air Force|USAAF]] [[B-24 Liberator|B-24]] over [[Bologna]] to claim his 90th victory. However, his [[Messerschmitt Bf 109#Variants|Bf 109G-6]] (''Werknummer'' 163 314—factory number) "Black < 3" was hit by defensive fire and Bennemann was again wounded, baling out successfully. In June 1944, Bennemann led the ''Geschwaderstab'' of JG 53 on a short return to the Eastern Front, to direct the supply missions for the city of [[Vilna]]. The unit departed the Soviet Union for [[Wunstorf]] near [[Hannover]] in Germany on 22 July. From August 1944, Bennemann led JG 53 on [[Defense of the Reich|''Reichsverteidigung'']] missions, initially from bases in [[France]], then from bases in Germany. He claimed his last three victories in October 1944 to bring his final score to 93. Among his 93 victories are at least 10 [[Ilyushin Il-2|Il-2 ''Sturmoviks'']] claimed over the [[Eastern Front (World War II)|Eastern Front]].

In early 1945 he participated in the "[[Fighter Pilots Revolt]]", a minor insurrection among the high-ranking Luftwaffe pilots, whereas they confronted the Reich Marshal and chief of the Luftwaffe [[Hermann Göring]] with their demands on the conduct of the air war.

==Awards==
* ''[[Ehrenpokal der Luftwaffe]]'' (5 October 1940)<ref>Obermaier 1989, p. 87.</ref>
* [[German Cross]] in Gold on 27 July 1942 as ''[[Hauptmann]]'' in the I./JG 52<ref>Patzwall & Scherzer 2001, p. 35.</ref>
* [[Iron Cross]] (1939) 2nd and 1st class
* [[Knight's Cross of the Iron Cross]] on 2 October 1942 as ''Hauptmann'' and ''[[Gruppenkommandeur]]'' of the I./JG 52<ref>Scherzer 2007, p. 214.</ref>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last=Scherzer
  |first=Veit 
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* {{Cite book
  |last=Weal
  |first=John
  |year=2004
  |title=Jagdgeschwader 52: The Experten (Aviation Elite Units)
  |location=London, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84176-786-4
}}
{{Refend}}

{{s-start}}
{{s-mil}}
{{succession box|
before=Major [[Kurt Ubben]]|
after=none|
title= Commander of [[Jagdgeschwader 53]] ''Pik As''|
years=November 9, 1943 – April 27, 1945
}}
{{s-end}}

{{Knight's Cross recipients of JG 52}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Bennemann, Helmut}}
[[Category:1915 births]]
[[Category:2007 deaths]]
[[Category:People from the Province of Westphalia]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:People from Herne, North Rhine-Westphalia]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]