{{Infobox journal
| title = Veterinary Pathology 
| cover = 
| editor = Jeff L. Caswell
| discipline = 
| former_names = 
| abbreviation = 
| publisher = [[SAGE Publications]]
| country = 
| frequency = Bi-monthly 
| history = 1964 -present
| openaccess = 
| license = 
| impact = 1.869
| impact-year = 2014
| website = http://www.uk.sagepub.com/journals/Journal201966?siteId=sage-uk&prodTypes=any&q=Veterinary+Pathology&fs=1
| link1 = http://vet.sagepub.com/content/current
| link1-name = Online access
| link2 = http://vet.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0300-9858
| eISSN =
| OCLC = 51952823
| LCCN = 77646648
}}

'''''Veterinary Pathology ''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Pathology]]. As of 2017, the journal's [[Editor-in-Chief|editor]] is Jeff L. Caswell. It has been in publication since 1964 and is currently published by [[SAGE Publications]], in association with the [[American College of Veterinary Pathologists]], the [[European College of Veterinary Pathologists]], and the [[Japanese College of Veterinary Pathologists]].

== Scope ==
''Veterinary Pathology'' publishes reports of basic and applied research involving wildlife, marine and zoo animals and poultry. The journal focuses on details of the diagnostic investigations of diseases of animals, reports of experimental studies on mechanisms of specific processes and also provides insights into animal models of human disease.

== Abstracting and indexing ==
''Veterinary Pathology'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 1.869, ranking it 18 out of 133 journals in the category ‘Veterinary Science’<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Veterinary Science |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |work=Web of Science |postscript=.}}</ref> and 41 out of 75 journals in the category ‘Pathology’ <ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Pathology|title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://vet.sagepub.com/}}
* [http://www.acvp.org/index.php/en/ Official website of ACVP]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]