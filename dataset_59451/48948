{{Orphan|date=August 2014}}
{{Infobox award
| name           = Junior Writers Awards
| subheader      =
| current_awards =
| image          =
| alt            =
| caption        =
| description    =
| sponsor        =
| date           = <!-- {{Start date|YYYY|MM|DD}} -->
| location       = {{HK}}
| country        = {{HK}}
| presenter      =
| host           =
| preshow_host   =
| acts           =
| reward         =
| year           = 2014
| year2          =
| holder         =
| website        = {{URL|http://www.jwya.org/}}
| network        =
| runtime        =
| ratings        =
| producer       =
| director       =
| most_awards    =
| most_nominations =
| image2         =
| image2size     =
| alt2           =
| caption2       =
| previous       =
| main           =
| next           =
}}

The '''Junior Writer Awards''' (JWA) is an English literacy writing competition for secondary school students based in [[Hong Kong]] and [[Macau]]. It was founded in 2014 by Norton House Global Education Initiative, a non-government organization ([[NGO]]), and organised jointly with [[Senate House Education]], Upper House Academy and ''[[South China Morning Post]]'' as a philanthropic educational initiative to raise English literacy levels, thinking skills and social awareness of secondary students in Hong Kong and Macau.<ref>[http://www.jwya.org Junior Writers Awards official website]</ref><ref>[http://yp.scmp.com/juniorwritersawards Junior Writers Awards (South China Morning Post) official website]</ref>

== Winner <ref>[http://www.jwya.org/top-writers-winners/winners-2014 Junior Writers Awards Winners 2014]</ref><ref>[https://apiwinecircle.scmp.com/YoungPost/juniorwriterawards/result.php Junior Writers Awards Top 100 Winner 2014 on South China Morning Post]</ref><ref>[http://www.vakiodaily.com/index.php?tn=viewer&ncid=1&nid=205808&dt=20140327&lang=tw 2014 Mar 27 - Va Kio Daily (Macau): 英文寫作「少年作家獎」頒獎  十三歲楊樂至膺「最佳作家」]</ref> ==

=== Junior Group ===

Age Group: 11–13 years

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=500 | Essay Topic
|-
| 2014 || What is my role in society?
|}

{| class="wikitable" style="text-align:center;"
|-
! rowspan="2" width=50 | Year !! colspan="2" width=350 | Champion !! colspan="2" width=350 | 1st Runner Up !!  colspan="2" width=350 | 2nd Runner Up
|-
! Name of Contestant !! Representing School !! Name of Contestant !! Representing School !! Name of Contestant !! Representing School
|-
| 2014 || Lok Chi Yeung <ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6421 2014 Mar 20 - South China Morning Post (Hong Kong): Legacy of a cancer hero]</ref><ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6422 2014 Mar 20 - South China Morning Post (Hong Kong): Freddie's my inspiration]</ref><ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6407&section=hong 2014 Mar 18 - South China Morning Post (Hong Kong): Heartfelt story takes top gong]</ref><ref>[http://hk.apple.nextmedia.com/news/art/20140331/18674324 2014 Mar 31 - Apple Daily (Hong Kong): 延續已故港大抗癌鬥士精神　伍庭發表弟　膺最佳少年作家]</ref><ref>[http://std.stheadline.com/yesterday/edu/0331go03.html 2014 Mar 31 - Sing Tao Daily (Hong Kong): 頌讚抗癌鬥士　表弟奪作家獎]</ref><ref>[http://news2.mingpao.com/pns/%E6%B8%AF%E5%A4%A7%E6%8A%97%E7%99%8C%E9%AC%A5%E5%A3%AB%E5%9E%82%E7%AF%84%E5%BE%8C%E8%BC%A9-%E9%A0%93%E6%82%9F%E7%94%9F%E5%91%BD%E5%AF%B6%E8%B2%B4%20%20%E8%A1%A8%E5%BC%9F%E7%B9%BC%E6%89%BF%E4%B8%8D%E6%94%BE%E6%A3%84%E7%B2%BE%E7%A5%9E/web_tc/article/20140331/s00011/1396203932209 2014 Mar 31 - Ming Pao (Hong Kong): 港大抗癌鬥士垂範後輩 頓悟生命寶貴 表弟繼承不放棄精神]</ref> || [[International Christian School (Hong Kong)]] || Liam Fung || [[Chinese International School]] || Olivia Ko || [[St. Paul's Co-educational College]]
|}

=== Intermediate Group ===

Age Group: 14–16 years

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=500 | Essay Topic
|-
| 2014 || What does Rule of Law have to do with me?
|}

{| class="wikitable" style="text-align:center;"
|-
! rowspan="2" width=50 | Year !! colspan="2" width=350 | Champion !! colspan="2" width=350 | 1st Runner Up !!  colspan="2" width=350 | 2nd Runner Up
|-
! Name of Contestant !! Representing School !! Name of Contestant !! Representing School !! Name of Contestant !! Representing School
|-
| 2014 || Serena Chan<ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6431 2014 Mar 20 - South China Morning Post (Hong Kong): How does the rule of law affect me?]</ref> || [[St. Paul's Co-educational College]] || Edward Mak || [[King George V School (Hong Kong)]] || Gregory Wong<ref>[http://web.lasalle.edu.hk/eng/home.php?t=achievement 2014 Mar 22 - La Salle College - School Achievements Junior Writers Awards & Lions Club Writers Awards (English)]</ref> || [[La Salle College]]
|}

=== Advanced Group ===

Age Group: 16+ years

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=500 | Essay Topic
|-
| 2014 || If I could go back in time to any period of history, when would it be and why?
|}

{| class="wikitable" style="text-align:center;"
|-
! rowspan="2" width=50 | Year !! colspan="2" width=350 | Champion !! colspan="2" width=350 | 1st Runner Up !!  colspan="2" width=350 | 2nd Runner Up
|-
! Name of Contestant !! Representing School !! Name of Contestant !! Representing School !! Name of Contestant !! Representing School
|-
| 2014 || Reo Shoshi <ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6432 2014 Mar 20 - South China Morning Post (Hong Kong): Going back to the Age of Exploration]</ref> || French International School || Victoria Cherrington || [[West Island School]] || Charlotte Lam<ref>[http://www.ssgc.edu.hk/readnews.asp?id=68 2013 Mar 19 - St.Stephen's Girls College: Junior Writers Award 2014 (1st Annual)]</ref> || St. Stephen's Girls' College
|}

=== Greatest Pen Winner ===

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=230 | Name of Contestant !! width=270 |Representing School
|-
| 2014 || Lok Chi Yeung<ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6421 2014 Mar 20 - South China Morning Post (Hong Kong): Legacy of a cancer hero]</ref><ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6422 2014 Mar 20 - South China Morning Post (Hong Kong): Freddie's my inspiration]</ref><ref>[http://www.yp.scmp.com/home/website/article.aspx?id=6407&section=hong 2014 Mar 18 - South China Morning Post (Hong Kong): Heartfelt story takes top gong]</ref><ref>[http://hk.apple.nextmedia.com/news/art/20140331/18674324 2014 Mar 31 - Apple Daily (Hong Kong): 延續已故港大抗癌鬥士精神　伍庭發表弟　膺最佳少年作家]</ref><ref>[http://std.stheadline.com/yesterday/edu/0331go03.html 2014 Mar 31 - Sing Tao Daily (Hong Kong): 頌讚抗癌鬥士　表弟奪作家獎]</ref><ref>[http://news2.mingpao.com/pns/%E6%B8%AF%E5%A4%A7%E6%8A%97%E7%99%8C%E9%AC%A5%E5%A3%AB%E5%9E%82%E7%AF%84%E5%BE%8C%E8%BC%A9-%E9%A0%93%E6%82%9F%E7%94%9F%E5%91%BD%E5%AF%B6%E8%B2%B4%20%20%E8%A1%A8%E5%BC%9F%E7%B9%BC%E6%89%BF%E4%B8%8D%E6%94%BE%E6%A3%84%E7%B2%BE%E7%A5%9E/web_tc/article/20140331/s00011/1396203932209 2014 Mar 31 - Ming Pao (Hong Kong): 港大抗癌鬥士垂範後輩 頓悟生命寶貴 表弟繼承不放棄精神]</ref> || [[International Christian School (Hong Kong)]]
|}

=== Greatest Potential ===

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=230 | Name of Contestant !! width=270 |Representing School
|-
| 2014 || Fong Man Hong Maxwell<ref>[http://www.macaodaily.com/html/2014-03/20/content_887678.htm 2014 Mar 20 - Macao Daily News (Macau): 培正生獲少年作家潛質]</ref><ref>[http://www.macaodaily.com/html/2014-03/20/content_887679.htm 2014 Mar 20 - Macao Daily News (Macau): 永援教師校本培訓]</ref><ref>[http://www.puiching.edu.mo/modules.php?name=docs&file=news 2013 Mar 31 - 澳門培正中學 最新消息: Ref: 1546]</ref> || [[Pui Ching Middle School (Macau)]]
|}

=== Most Inspirational ===

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=230 | Name of Contestant !! width=270 |Representing School
|-
| 2014 || Yeung Chak Sze || Wong Kam Fai Secondary & Primary School
|}

=== Most Innovative ===

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=230 | Name of Contestant !! width=270 |Representing School
|-
| 2014 || Ronaq Mathur || [[Island School]]
|}

=== Scholastic Writer ===

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=230 | Name of Contestant !! width=270 |Representing School
|-
| 2014 || Adrian Au || [[Diocesan Boys' School]]
|}

=== Most Promising Young Writer ===

{| class="wikitable" style="text-align:center;"
|-
! width=50 | Year !! width=230 | Name of Contestant !! width=270 |Representing School
|-
| 2014 || Lui Hei Ching Tiffany || [[Diocesan Girls' School]]
|}

== References ==

{{reflist}}

<!--If these are references, they need to be presented as "inline" references. See [[WP:REFB]] Otherwise they belong in External links-->
* Junior Writers Awards (South China Morning Post) official Website: http://yp.scmp.com/juniorwritersawards
* Junior Writers Awards Winners 2014: http://www.jwya.org/top-writers-winners/winners-2014
* Junior Writers Awards Top 100 Winner 2014 on South China Morning Post: https://apiwinecircle.scmp.com/YoungPost/juniorwriterawards/result.php
* 2014 Mar 31 - [[Apple Daily]] (Hong Kong): 延續已故港大抗癌鬥士精神　伍庭發表弟　膺最佳少年作家 http://hk.apple.nextmedia.com/news/art/20140331/18674324
* 2014 Mar 31 - [[Sing Tao Daily]] (Hong Kong): 頌讚抗癌鬥士　表弟奪作家獎 http://std.stheadline.com/yesterday/edu/0331go03.html
* 2014 Mar 31 - [[Ming Pao]] (Hong Kong): 港大抗癌鬥士垂範後輩 頓悟生命寶貴 表弟繼承不放棄精神 http://news2.mingpao.com/pns/%E6%B8%AF%E5%A4%A7%E6%8A%97%E7%99%8C%E9%AC%A5%E5%A3%AB%E5%9E%82%E7%AF%84%E5%BE%8C%E8%BC%A9-%E9%A0%93%E6%82%9F%E7%94%9F%E5%91%BD%E5%AF%B6%E8%B2%B4%20%20%E8%A1%A8%E5%BC%9F%E7%B9%BC%E6%89%BF%E4%B8%8D%E6%94%BE%E6%A3%84%E7%B2%BE%E7%A5%9E/web_tc/article/20140331/s00011/1396203932209
* 2014 Mar 27 - [[Va Kio Daily]] (Macau): 英文寫作「少年作家獎」頒獎  十三歲楊樂至膺「最佳作家」 http://www.vakiodaily.com/index.php?tn=viewer&ncid=1&nid=205808&dt=20140327&lang=tw
* 2014 Mar 20 - [[South China Morning Post]] (Hong Kong): Legacy of a cancer hero http://www.yp.scmp.com/home/website/article.aspx?id=6421
* 2014 Mar 20 - [[South China Morning Post]] (Hong Kong): Freddie's my inspiration http://www.yp.scmp.com/home/website/article.aspx?id=6422
* 2014 Mar 20 - [[South China Morning Post]] (Hong Kong): How does the rule of law affect me? http://www.yp.scmp.com/home/website/article.aspx?id=6431
* 2014 Mar 20 - [[South China Morning Post]] (Hong Kong): Going back to the Age of Exploration http://www.yp.scmp.com/home/website/article.aspx?id=6432
* 2014 Mar 20 - [[Macao Daily News]] (Macau): 培正生獲少年作家潛質 - http://www.macaodaily.com/html/2014-03/20/content_887678.htm
* 2014 Mar 20 - [[Macao Daily News]] (Macau): 永援教師校本培訓 - http://www.macaodaily.com/html/2014-03/20/content_887679.htm
* 2014 Mar 18 - [[South China Morning Post]] (Hong Kong): Heartfelt story takes top gong - http://www.yp.scmp.com/home/website/article.aspx?id=6407&section=hong

== External links ==

* {{facebook|juniorwritersawards}}

[[Category:English literary awards]]
[[Category:Education in Hong Kong]]
[[Category:Education in Macau]]
[[Category:Articles created via the Article Wizard]]