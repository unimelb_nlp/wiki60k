{{Infobox journal
| title = Annals of Human Genetics
| formernames = Annals of Eugenics
| cover = [[File:Cover Annals of Human Genetics 2005.jpg]]
| discipline = [[Human genetics]]
| abbreviation = Ann. Hum. Genet.
| editor = Andres Ruiz-Linares
| publisher = [[John Wiley & Sons]]
| country =
| frequency = Bimonthly
| history = 1925-present
| impact = 2.215
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1469-1809
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1469-1809/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1469-1809/issues
| link2-name = Online archive
| ISSN = 0003-4800
| eISSN = 1469-1809
| OCLC = 472337129
| LCCN = 28012242
| CODEN = ANHGAA
}}
The '''''Annals of Human Genetics''''' is a bimonthly [[Peer review|peer-reviewed]] [[scientific journal]] covering [[human genetics]]. It was established in 1925 by [[Karl Pearson]] as the '''Annals of Eugenics''', with as subtitle, Darwin's epigram "I have no Faith in anything short of actual measurement and the rule of three".<ref>{{cite journal |last=Stigler|first=Stephen |title=Darwin, Galton and the Statistical Enlightenment |journal=[[Journal of the Royal Statistical Society, Series A]] |date=July 2010 |volume=173 |issue=3 |pages=469–482 |doi=10.1111/j.1467-985X.2010.00643.x}}</ref> The journal obtained its current name in 1954 to reflect changing perceptions on [[eugenics]].<ref>{{cite journal |last=Barnett |first=Richard |title=Eugenics |journal=The Lancet |date=May 2004 |volume=363 |issue=9422 |page=1742 |doi=10.1016/S0140-6736(04)16280-6}}</ref> The [[editor-in-chief]] is [[Mark G. Thomas]] ([[University College London]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 2.215.<ref name=WoS>{{cite book |year=2013 |chapter=Annals of Human Genetics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
<references/>

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1469-1809}}

{{biology-journal-stub}}

[[Category:Medical genetics journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1925]]