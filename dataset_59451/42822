<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = G.I
  |image = Fokker_g1.gif
  |caption = Fokker G.I 
}}{{Infobox Aircraft Type
  |type = [[Heavy fighter]]
  |manufacturer = [[Fokker]]
  |designer = Erich Schatzki and Marius Beeling (after 1938)  
  |first flight = 16 March 1937
  |primary user = ''[[Royal Netherlands Air Force|Luchtvaartafdeling]]''{{#tag:ref|The Netherlands Air Force before World War II was known as the '' Luchtvaartafdeeling''. (''Luchtvaartafdeeling'' was correctly written in the 1930s style of writing with a double ee.)|group=N}}
  |more users = ''[[Luftwaffe]]''
  |number built = 63 <ref>[http://web.archive.org/web/20021015163319/www.fokkerg-1.nl/g-1-e.htm "Foreign Fokker G.1"] ''Fokker G-1 Foundation.'' Retrieved: 26 August 2010.</ref>
}}
|}
The '''Fokker G.I''' was a [[Netherlands|Dutch]] heavy twin-engined fighter aircraft comparable in size and role to the German [[Messerschmitt Bf 110]]. Although in production prior to World War II, its combat introduction came at a time the Netherlands were overrun by the Germans. The few G.Is that were mustered into service were able to score several victories. 
Some were captured intact after the Germans had occupied the Netherlands. The remainder of the production run was taken over by the ''Luftwaffe'' for use as trainers.

==Design and development==
The G.I, given the nickname ''le Faucheur'' ("The Reaper" in French), was designed as a private venture in 1936 by Fokker chief engineer Dr. Schatzki. Intended for the role of ''jachtkruiser'', "heavy" fighter or air cruiser, able to gain air superiority over the battlefield as well as being a bomber destroyer, the G.1 would fulfill a role seen as important at the time, by advocates of [[Giulio Douhet]]'s theories on air power. The Fokker G.I utilized a twin-engined, twin-boom layout that featured a central nacelle housing two or three crew members (a pilot, radio operator/navigator/rear gunner or a bombardier) as well as a formidable armament of twin 23&nbsp;mm (.91&nbsp;in) Madsen cannon and a pair of 7.9&nbsp;mm (.31&nbsp;in) machine guns (later eight machine guns) in the nose and one in a rear turret.<ref name="Green1967p507">{{Harvnb|Green|1967|p=507.}}</ref>

Besides its main mission, the G.1 could be configured for ground attack and light bombing missions (it could carry a bomb load of one 400&nbsp;kg/882&nbsp;lb bomb or combinations of two 200&nbsp;kg/441&nbsp;lb or 10 26&nbsp;kg/57&nbsp;lb bombs).<ref name="Green1967p507"/>

The design and construction of the prototype (registered as X-2) was completed in just seven months. At its introduction at the [[Paris Air Show]] in November 1936, even before its first flight, the G.I was a sensation, appearing in a purple and yellow finish (evocative of the Spanish Republican colors, thought to be Fokker's first export customer).<ref name="Green1967p507"/>

Like all Fokker aircraft of the period, the G.I was of mixed construction; the front of the central pod were built around a welded frame, covered with [[aluminium]] plating. The back of the central pod, however, as well as the wings, were completely constructed with wood.

The G.I prototype, powered by 485&nbsp;kW (650&nbsp;hp) [[Hispano-Suiza]] 14AB-02/03 engines, had its first flight at [[Eindhoven Airport|Welschap Airfield]], near [[Eindhoven]] on 16 March 1937 with Karel Mares at the controls.<ref>{{Harvnb|Van der Klaauw|1996|p=183.}}</ref> Later, [[Emil Meinecke]] took over much of the test flights.<ref name="Green1967p509">{{Harvnb|Green|1967|p=509.}}</ref> The maiden flight went well, but a subsequent test flight in September 1937 ended with a supercharger explosion that nearly caused the loss of the prototype.<ref name="Green1967p509"/> The accident prompted a replacement of the Hispano-Suiza engines with 559&nbsp;kW (750&nbsp;hp) [[Pratt & Whitney R-1535|Pratt & Whitney SB4-G Twin Wasp Junior]] engines.

==Operational history==
[[File:Fokker G.1A replica Soesterberg vr.jpg|thumb|Replica of the G.I at the Dutch Air Force Museum in [[Soesterberg]], The [[Netherlands]].]]
During testing, the company received a contract from the Spanish Republican government for 26 G.1 "export" versions with Pratt & Whitney engines. Despite receiving payment, the order was destined never to be fulfilled as the Dutch government placed an embargo on the sale of military equipment to Spain.<ref name="Green1967p509"/> Fokker however continued building the aircraft and a story was released to the press that they were intended for Finland, hence the persistent tales about a "Finnish" order.<ref>{{harvnb|Hooftman|1981|p=62.}}</ref> To make matters more complex, Finland showed great interest in the G.I, but eventually purchased Bristol Blenheim light bombers.

Besides the Dutch ''[[Royal Netherlands Air Force|Luchtvaartafdeeling]]'', several foreign air forces showed an interest in the G.I. as either a fighter or dive-bomber. In order to test its potential as a dive-bomber, the G.1 prototype was fitted with hydraulically operated dive brakes under the wings. Flight tests revealed that the G.1 was capable of diving at over 644&nbsp;km/h (400&nbsp;mph) and demonstrated aerobatic capabilities. Swedish Air Force officer [[Captain (land)|Captain]] [[Björn Bjuggren]] tested the G.1 in over 20 dives and reported favourably on its effectiveness as a dive bomber.<ref name="Green1967p510">{{Harvnb|Green|1967|p=510.}}</ref> Orders for G.1b Wasp aircraft came from Spain (26 ordered) and Sweden (18), while the Mercury variant was ordered by Denmark (12) together with a production license that never became to be used,<ref>[http://www.rathbonemuseum.com/DENMARK/DKPhotos/DKPhotos.html Morten Hein, "Danish military aviation in relation to the Second World War", website of the Rathbone Museum, rathbonemuseum.com, retrieved 2 August 2014]</ref> and Sweden (72). Although Belgium, Finland, Turkey, Hungary and Switzerland air forces showed great interest, they never placed firm orders.

The ''Luchtvaartafdeeling'' ordered 36 G.I's with 541&nbsp;kW (725&nbsp;hp) [[Bristol Mercury]] VIII engines, the standard engine used by the Dutch Air Force in the [[Fokker D.XXI]] fighter, in order to equip two squadrons.<ref name="Green1967p509"/> Only the first four examples were built as three-seaters intended for ground-attack, with the remainder being completed as two-seat fighters. During the lead-up to hostilities, a total of 26 G.I's were operational in the 3rd ''Jachtvliegtuigafdeling'' (JaVA) at Rotterdam (Waalhaven Airfield), and 4th JaVA Fighter Group at Bergen near [[Alkmaar]]. The aircraft were actively involved in border patrols and in order to ensure neutrality, on 20 March 1940, a G.1 from 4th JaVA forced down an [[Armstrong Whitworth Whitley]] from the RAF's [[No. 77 Squadron RAF|77 sqn]] when it strayed into Dutch air space.<ref name="Green1967p511"/>

===Battle of the Netherlands===
On 10 May 1940, when [[Nazi Germany]] [[Battle of the Netherlands|invaded the Netherlands]], 23 G.1 aircraft were serviceable while production of Spain's order of the G.1 Wasp variant continued with a dozen aircraft completed, awaiting armament.

The German invasion started with an early morning (03:50 hours) ''[[Luftwaffe]]'' attack on the Dutch airfields. While the 4th JaVA received a devastating blow, losing all but one of its aircraft, eight 3rd JaVA G.1 fighters were [[Scrambling (military)|scrambled]]  in time and successfully engaged several German aircraft. The surviving aircraft continued to fly, but with mounting losses, bringing their numbers down to three airworthy aircraft by the end of the first day. Despite the heavy losses of 4th JaVA, some of the planes could be kept in the air, by scavaging parts from various planes. In the "Five-day War", the available G.1 fighters were mainly deployed in ground attack missions, strafing advancing German infantry units, but also used to attack Junkers Ju 52/3m transports.<ref name="Green1967p511">{{Harvnb|Green|1967|p=511.}}</ref> Although reports are fragmentary and inaccurate as to the results, G.1 fighters were employed over Rotterdam and the Hague, contributing to the loss of 167 Ju 52s, scoring up to 14 confirmed aerial kills.<ref>{{Harvnb|Van der Klaauw|1966|p=186.}}</ref>

===Aftermath===
At the conclusion of hostilities, several G.Is were captured by the Germans, with the remainder of the Spanish order completed at the Fokker plant by mid-1941 in order for the G.1s to be assigned as fighter trainers for Bf 110 crews at [[Wiener Neustadt]].<ref>{{Harvnb|Green|1961|p=107.}}</ref> For the next two years, ''Flugzeugführerschule'' (B) 8 flew the G.1 Wasp until attrition grounded the fleet.<ref name="Green1967p511"/>

On 5 May 1941, a Fokker test pilot, Hidde Leegstra, accompanied by engineer (and member of the Fokker Board of Directors) Ir. Piet Vos, managed to fly a G.1 to England. The crew's subterfuge involved acquiring additional fuel for the supposed test flight as well as ducking into clouds to deter the trailing ''Luftwaffe'' aircraft from following.<ref name="Green1967p511"/> After landing in England, the G.1 was conscripted by [[Miles Aircraft|Phillips and Powis Aircraft]], later [[Miles Aircraft]]. The company had designed an all-wooden fighter-bomber, and was interested in the G.1 wing structure and its resistance to the rigours of a British climate. Despite being left outdoors for the remainder of the war, the G.1 survived only to be eventually scrapped after 1945.<ref>{{Harvnb|Van der Klaauw|1966|pp=188–189.}}</ref><ref>{{Harvnb|Hooftman|1981|pp=108–117.}}</ref>

There are no surviving G.Is today, although a replica has been built. This could be found at the Dutch Air Force Museum in [[Soesterberg]] until 2013. Nowadays, it is in storage at the National Military Museum (NMM), also located at Soesterberg.

==Variants==
* '''G.I''' : Prototype, powered by two Hispano-Suiza 14AB-02/03 engines; one built, c/n 5419.<ref>{{Harvnb|Hooftman|1981|p=156.}}</ref>
* '''G.I Mercury''' : Two and three-seat models, powered by [[Bristol Mercury]] VIII engines; 36 built, c/n 5521-5556.<ref>{{Harvnb|Hooftman|1981|pp=156–176.}}</ref>
* '''G.I Wasp''' : Two-seat, smaller "export model" {{#tag:ref|The G.I Wasp had a significantly smaller fuselage length and reduced wingspan/area. G.1 Wasp length: 10.38m (33.9 ft), wingspan: 16.50m (54.2 ft), wing area: 35.70 m² (384.27) ft².<ref name="Green1967p507"/>|group=N}}, powered by [[Pratt & Whitney R-1535|Pratt & Whitney SB4-G Twin Wasp Junior]]; 26 built, c/n 5557-5581.<ref>{{Harvnb|Hooftman|1981|p=176.}}</ref>

==Operators==
;{{NLD}}:
*''[[Royal Netherlands Air Force|Luchvaartafdeling]] (LVA)''
;{{flag|Nazi Germany|name=Germany}}
*''[[Luftwaffe]]'' (small numbers)<ref>{{Harvnb|Ketley and Rolfe|1996|p=11.}}</ref>{{#tag:ref|The ''Luftwaffe'' operated between 35 and 40 Fokker G.1s.<ref>{{Harvnb|Hooftman|1981|pp=120–121.}}</ref>|group=N}}

==Specifications (Fokker G.I Mercury)==
[[File:Fokker G.1.svg|right|300px]]
{{aircraft specifications
|<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?= plane
|jet or prop?= prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with ) and start a new, fully formatted line with * -->
|ref= ''Nederlandse Vliegtuig Encyclopedie No.12: Fokker G-1 (2nd edition)'';<ref>{{Harvnb|Hooftman|1981|pp=194–195.}}</ref> ''The Fokker G-1''<ref>{{Harvnb|Van der Klaauw|1966|p=191.}}</ref>  
|crew= 2-3
|length main= 10.87 m 
|length alt= 35 ft 8 in
|span main= 17.16 m 
|span alt= 56.29 ft
|height main= 3.80 m 
|height alt= 12.4 ft
|area main= 38.30 m²
|area alt= 412.26 ft²
|empty weight main= 3,325 kg 
|empty weight alt= 7,330 lb
|loaded weight main= 4,800 kg
|loaded weight alt= 10,582 lb
|max takeoff weight main= 5,000 kg 
|max takeoff weight alt= 11,023 lb
|engine (prop)= [[Bristol Mercury]] VIII
|type of prop= nine-cylinder air-cooled single-row piston [[radial engine]]
|number of props= 2
|power main= 730 hp at 2,650 rpm for takeoff, 830 hp (618 kW) at 4,100m at 2,750rpm maximum continuous power
|power alt= 545 kW
|max speed main= 475 km/h at 4,100m
|max speed alt= 295 mph
|range main= 1,510 km
|range alt= 938 mi
|ceiling main= 10,000 m 
|ceiling alt= 32,808 ft
|climb rate main= 13.5 m/s 
|climb rate alt= 44.29 ft/s
|loading main= 125.3 kg/m²
|loading alt= 25.68 lb/ft²
|power/mass main= 0.22 kW/kg
|power/mass alt= 0.14 hp/lb; 0.30 hp/kg
|more performance=
* '''Time to altitude:''' 6.0 min 20 sec to 5,000 m (16,405 ft)
|armament=
* 8× 7.9 mm (0.31 in) forward-firing [[M1919 Browning machine gun|FN-Browning machine guns]] in the nose
* 1× 7.9 mm (0.31 in) machine gun in rear turret
* 300 kg (660 lb) of bombs (G.1 Wasp could take 400 kg (880 lb))
}}

==See also==
{{aircontent
|similar aircraft=
* [[Bristol Beaufighter]]
* [[Kawasaki Ki-45]]
* [[Lockheed P-38 Lightning]]
* [[Messerschmitt Bf 110]]
* [[Potez 630]]
|lists=
* [[List of aircraft of World War II]]
* [[List of fighter aircraft]]
|see also=
* [[Twin-boom aircraft]]
}}

==References==
;Notes
{{reflist|group=N}}
;Citations
{{reflist|2}}
;Bibliography
{{refbegin}}
* Green, William, ed. "Le Faucheur... Fokker's Formidable G.1". ''Flying Review International'', Volume 22, no. 8, April 1967.
* Green, William. ''Warplanes of the Second World War, Volume Three: Fighters''. London: Macdonald & Co.(Publishers) Ltd., 1961. ISBN 0-356-01447-9.
* Hooftman, Hugo. ''Fokker G-1, Tweede druk (Nederlandse Vliegtuig Encyclopedie, deel 12)'' (in Dutch). Bennekom, the Netherlands: Cockpit-Uitgeverij, 1981.
* Ketley, Barry and Mark Rolfe. ''Luftwaffe Fledglings 1935–1945: Luftwaffe Training Units and their Aircraft''. Aldershot, UK: Hikoki Publications, 1996.
* Taylor, John W.R. "Fokker G.1." ''Combat Aircraft of the World from 1909 to the present''. New York: G.P. Putnam's Sons, 1969. ISBN 0-425-03633-2.
* Van der Klaauw, Bart. ''The Fokker G-1 (Aircraft in Profile, Volume 6, number 134)''. Windsor, Berkshire, UK: Profile Publications Ltd., 1966.
{{refend}}

==External links==
{{commons category|Fokker G.1}}
* [http://www.fokkerg-1.nl/ Fokker G-1 Foundation] (Mostly in Dutch, English site discontinued)
* [http://www.go2war2.nl/artikel/190 G.I, Fokker] (in Dutch)
* [http://www.dutch-aviation.nl/index5/Military/index5-1%20G1.html Images and Videos of the G.1 at Dutch Aviation]

{{Fokker aircraft}}
{{Swedish bomber aircraft}}
{{Swedish reconnaissance aircraft}}
{{Swedish military aircraft}}

[[Category:Fokker aircraft|G.I]]
[[Category:Dutch fighter aircraft 1930–1939]]
[[Category:Twin-boom aircraft]]
[[Category:1936 introductions]]