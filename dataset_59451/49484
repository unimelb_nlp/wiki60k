{{Infobox person
| name        = Miguel de Icaza
| image       = Miguel de Icaza.jpg
| caption     = 
| birth_date  = c. {{birth year and age|1972}}
| birth_place = [[Mexico City]], Mexico
| occupation  = [[Software developer]]
| citizenship = {{unbulleted list|Mexico|United States}}
| title       = Distinguished Engineer
| employer    = [[Microsoft]]
| spouse      = Maria Laura de Icaza
| website     = {{URL|www.tirania.org/blog}}
}}

'''Miguel de Icaza''' (born c. 1972) is a Mexican programmer, best known for starting the [[GNOME]], [[Mono (software)|Mono]], and [[Xamarin]] projects.<ref>{{cite book|title=Under the Radar|first=Robert|last=Young|author2=Wendy Goldman Rohm|isbn=978-1-57610-506-1|page=139|publisher=Coriolis|year=1999|url=https://books.google.com/books?id=OqSHZst3S0QC&q=Miguel+de+Icaza&dq=Miguel+de+Icaza&pgis=1}}</ref>

==Biography==

===Early years===
De Icaza was born in [[Mexico City]] and studied Mathematics at the [[National Autonomous University of Mexico|Universidad Nacional Autonoma de México]] (UNAM), but never received a degree.<ref>http://tirania.org/blog/archive/2009/Oct-19.html</ref> He came from a family of scientists in which his father is a physicist and his mother a biologist.<ref>{{cite web|title=Interview{{sic|hide=y}} with Miguel De Icaza|url=http://www.linuxjournal.com/article/6833|publisher=''[[Linux Journal]]''|accessdate=2008-08-19}}</ref> He started writing free software in 1992.

===Early software career===
One of the earliest pieces of software he wrote for Linux was the [[Midnight Commander]] file manager, a text-mode file manager.<ref name="mc-maintainer">{{cite web | url = http://savannah.gnu.org/cgi-bin/viewcvs/mc/mc/FAQ?rev=HEAD | title = Midnight Commander FAQ | author = Midnight Commander authors | accessdate = 2010-09-06 | quote = ''Midnight Commander was started by Miguel de Icaza and he is the maintainer of the package. Other authors have joined the project later.''}}</ref> He was also one of the early contributors to the [[Wine (software)|Wine project]].<ref>{{cite web|title=Wine History|url=http://wiki.winehq.org/WineHistory|publisher=wiki.winehq.org|accessdate=2012-07-10}}</ref>

He worked with [[David S. Miller]] on the Linux [[SPARC]] port and wrote several of the video and network drivers in the port, as well as the libc ports to the platform.<ref name="sparc-port-usenix97">{{cite web | url = http://www.usenix.org/publications/library/proceedings/ana97/summaries/miller_invite.html | title = The SPARC Port of Linux | author=David S. Miller, Rutgers CAIP, and Miguel de Icaza, Instituto de Ciencias Nucleares, Universidad Nacional Autonoma de Mexico | work = Usenix Proceedings | year = 1997 | publisher = [[USENIX]] Association | accessdate = 2010-04-18}}</ref> They both later worked on extending Linux for [[MIPS architecture|MIPS]] to run on SGI's [[SGI Indy|Indy]] computers and wrote the original X drivers for the system.<ref>{{cite web | url = http://www.kneuro.net/cgi-bin/lxr/http/source/drivers/sgi/char/graphics.c | title = graphics.c | author = Miguel de Icaza | accessdate = 2011-11-19 | quote = ''Author: Miguel de Icaza''}}</ref> With [[Ingo Molnar]] he wrote the original software implementation of RAID-1 and RAID-5 drivers of the Linux kernel.<ref>{{cite web | url = http://www.koders.com/c/fid90D0506D0981288C46C7A849BBB82C7276351E12.aspx?s=rsa | title = raid5.c | quote = ''Copyright: (C) 1996, 1997 Ingo Molnar, Miguel de Icaza, Gadi Oxman''}}</ref>

In summer of 1997, he was interviewed by [[Microsoft]] for a job in the [[Internet Explorer for UNIX|Internet Explorer Unix]] team (to work on a [[SPARC]] port), but lacked the university degree required to obtain a work [[H-1B visa]].<ref>{{cite web | url = http://nat.org/blog/2009/10/ | title = Startup Visa | first = Nat| last = Friedman | date = 2009-10-31 | accessdate = 2010-09-06 }}</ref> He said in an interview that he tried to persuade his interviewers to free the IE code even before [[Netscape]] did so with their own browser.<ref>[http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dndotnet/html/deicazainterview.asp] {{webarchive |url=https://web.archive.org/web/20020108174635/http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dndotnet/html/deicazainterview.asp |date=January 8, 2002 }}</ref>

===GNOME, Ximian, Xamarin and Mono===
De Icaza started the [[GNOME]] project with [[Federico Mena]] in August 1997 to create a completely free desktop environment and component model for [[Linux]] and other [[Unix-like]] operating systems.<ref>{{cite book|title=Practical Mono|first=Mark|last=Mamone|page=7|url=https://books.google.com/books?id=YpIAry7MNCcC&dq=Federico+Mena&cad=0|publisher=Apress|year=2005|isbn=978-1-59059-548-0}}</ref> He also created the GNOME spreadsheet program, [[Gnumeric]].

In 1999, de Icaza, along with [[Nat Friedman]], co-founded Helix Code, a GNOME-oriented free software company that employed a large number of other GNOME hackers. In 2001, Helix Code, later renamed [[Ximian]], announced the [[Mono (software)|Mono]] Project, to be led by de Icaza, with the goal to implement [[Microsoft]]'s new [[.NET Framework|.NET]] development platform on [[Linux]] and [[Unix-like]] platforms. In August 2003, Ximian was acquired by [[Novell]]. There, de Icaza was [[Vice president|Vice President]] of Developer Platform.

In May 2011, de Icaza started [[Xamarin]] to replace MonoTouch and Mono for [[Android (operating system)|Android]] after Novell was bought by [[Attachmate]] and the projects were abandoned. Shortly afterwards, Xamarin and Novell reached an agreement where Xamarin took over the development and sales of these products.<ref>Friedman, Nat. (2011-07-18) [http://blog.xamarin.com/2011/07/18/first-press-release/ Xamarin mobile products available now! | Xamarin Blog]. Blog.xamarin.com. Retrieved on 2013-09-19.</ref>

In February 2016, Xamarin announced being acquired by Microsoft.<ref>[https://blog.xamarin.com/a-xamarin-microsoft-future/]</ref> One month later in Microsoft Build conference, it was announced that the Mono Project would be [[Software relicensing|relicensed]] to MIT, Visual Studio would include Xamarin (even the free versions) without restrictions, and Xamarin SDKs would be opensourced.<ref>[https://blog.xamarin.com/xamarin-for-all/]</ref>

===Advocacy of Microsoft open technologies===
De Icaza endorsed Microsoft's [[Office Open XML|Office Open XML (OOXML)]] document standard,<ref>{{cite web|title=OOXML. (Score:4, Informative)|url=http://slashdot.org/comments.pl?sid=293507&cid=20547277|publisher=''[[Slashdot]]''|accessdate=2008-08-19}}</ref><ref>{{cite web|title=The EU Prosecutors are wrong|url=http://tirania.org/blog/archive/2007/Jan-30.html}}</ref><ref>{{cite web|title=OOXML: The Wins|url=http://tirania.org/blog/archive/2008/Apr-02.html}}</ref> disagreeing with a lot of the widespread criticism in the [[open-source]] and [[free-software community]].

He also developed [[Mono (software)|Mono]] – a free and open-source alternative to Microsoft's [[.NET Framework]] – for GNOME.<ref>{{cite web|title=Mono and Gnome: The Long Reply|url=http://www.linuxtoday.com/news_story.php3?ltsn=2002-02-06-011-20-OP-GN-MS|publisher=''[[LinuxToday]]''|accessdate=2010-01-15}}</ref> This has raised much disagreement due to the patents that Microsoft holds on the .NET Framework.

De Icaza was criticized by [[Richard Stallman]] on the ''[[Software Freedom Day]] 2009'', who labeled him as ''"Traitor to the Free Software Community"''.<ref>{{cite web|url=http://www.osnews.com/story/22225/RMS_De_Icaza_Traitor_to_Free_Software_Community/|date=2009-09-21|title=RMS: De Icaza Traitor to Free Software Community|first=Thom |last=Holwerda |accessdate=2012-11-19|publisher=osnews.com}}</ref> Icaza responded on his blog to Stallman with the remark that he believes in a ''"world of possibility"'' and that he is open for discussions on ways to improve the [[FOSS]] pool.<ref>{{cite web|url=http://tirania.org/blog/archive/2009/Sep-23.html |title=On Richard Stallman|first=Miguel|last=de Icaza|quote=''I want to say that God loves all creatures. From the formidable elephant to the tiniest ant. And that includes Richard Stallman. As for me, I think that there is a world of possibility, and if Richard wants to discuss how we can improve the pool of open source/free software in the world he has my email address. Love, Miguel.''|publisher=tirania.org}}</ref>

===Preference for Mac OS X over Linux===
In August 2012, de Icaza criticized the [[Linux desktop]] as "killed by Apple". De Icaza specifically criticized a generally developer-focused culture, lack of [[backward compatibility]] and fragmentation among the various Linux distributions.<ref>{{cite web|url=https://www.wired.com/wiredenterprise/2012/08/osx-killed-linux/| title= How Apple Killed the Linux Desktop and Why That Doesn’t Matter|first=Klint |last=Finley|publisher=[[wired.com]] |date=2012-08-27 |accessdate=2012-09-02}}</ref><ref>{{cite web|url=http://tirania.org/blog/archive/2012/Aug-29.html| first=Miguel|last=de Icaza|title=What Killed the Linux Desktop|publisher=tirania.org |date=2012-08-29 |accessdate=2012-08-30}}</ref> In March 2013, de Icaza announced on his personal blog that he regularly used Mac [[OS X]] instead of Linux for desktop computing.<ref>{{cite web|url=http://tirania.org/blog/archive/2013/Mar-05.html| first=Miguel|last=de Icaza|title=How I ended up with Mac |publisher=tirania.org |date=2013-03-05 |accessdate=2013-03-05}}</ref>

===.NET Foundation director===
In 2014 he joined [[Anders Hejlsberg]] on stage during the announcements of the [[.NET Foundation]] and the open sourcing of Microsoft's [[C Sharp (programming language)|C#]] Compiler. He serves on the [[board of directors]] of the .NET Foundation.<ref>[http://www.businesswire.com/news/home/20140403006500/en/Microsoft-Xamarin-Collaborate-Establish-.NET-Foundation Microsoft-Xamarin-Collaborate-Establish-.NET-Foundation] (2014)</ref><ref>[http://www.theregister.co.uk/2014/11/12/release_microsoft_net_from_its_windows_chains_mono_and_xamarin_guy_miguel_de_icaza_on_open_source_net/ Microsoft .NET released from its Windows chains... but what ABOUT MONO?] on [[theregister.co.uk]] ''"Xamarin is a close partner of Microsoft, and De Icaza is one of three directors of the .NET Foundation, and the only director that does not work for Microsoft. The .NET Foundation was announced by Microsoft at its Build conference earlier this year, to host and support open source .NET projects."'' by Tim Anderson (Nov 2014)</ref>

==Awards and recognition==
Miguel de Icaza has received the [[Free Software Foundation]] 1999 [[FSF Free Software Awards|Award for the Advancement of Free Software]], the [[Massachusetts Institute of Technology|MIT]] [[Technology Review]] Innovator of the Year Award 1999,<ref>{{cite web|url=http://www.technologyreview.com/TR35/?year=1999|title=1999 Young Innovators Under 35|quote=1999 Innovator of the Year: Miguel De Icaza|year=1999|accessdate=2012-09-02|publisher=technologyreview.com}}</ref> and was named one of ''[[Time (magazine)|Time]]'' magazine's 100 innovators for the new century in September 2000.

In early 2010 he received a [[Microsoft Most Valuable Professional|Microsoft MVP]] Award.<ref>{{cite web|title=Miguel de Icaza's web log|url=http://tirania.org/blog/archive/2010/Jan-11-1.html|accessdate=2010-01-14}}</ref>

In March 2010, he was named as the fifth in the "Most Powerful Voices in Open Source" by [[MindTouch]].<ref>{{cite web|title=MindTouch.com|url=http://www.mindtouch.com/blog/2010/03/17/mpv/|accessdate=2010-03-21}}</ref>

==Personal life==
De Icaza has had [[cameo appearance]]s in the 2001 [[film|motion pictures]] ''[[Antitrust (film)|Antitrust]]'' and ''[[The Code (2001 film)|The Code]].''<ref name='IMDB'>{{IMDb name|208899}}</ref>

He married [[Brazil]]ian Maria Laura Soares da Silva (now Maria Laura de Icaza) in 2003.<ref>{{cite web|first=Jurandréia|last=Santos|url=http://jurandreiasantos.blogspot.com/2005/09/entrevista-maria-laura-de-icaza.html|title=Entrevista Maria Laura De Icaza|date=2005-09-10|accessdate=2010-09-06}}</ref>

De Icaza is critical of the actions of the state of [[Israel]] towards the [[State of Palestine|Palestinians]] in the Middle East and has blogged about the subject.<ref>[http://tirania.org/blog/archive/2002/Sep-03.html Israel: terrorist state. - Miguel de Icaza]. Tirania.org (2002-09-03). Retrieved on 2013-09-19.</ref>

De Icaza was granted [[US citizenship]] in January 2015.<ref>{{cite web|title=Nat Friedman on Twitter|url=https://twitter.com/natfriedman/status/555467340075499520}}</ref>

==References==
{{Reflist|30em}}

==External links==
{{Commons category}}
* [http://tirania.org/blog/ Miguel de Icaza's blog]
*{{IMDb name|id=0208899|name=Miguel de Icaza}}
* [http://www.twitter.com/migueldeicaza Miguel de Icaza's Twitter]

===Interviews===
* [http://media.libsyn.com/media/linuxoutlaws/linuxoutlaws75.ogg Miguel de Icaza 2009 interview on [[Linux Outlaws]]]
* [http://www.dotnetrocks.com/default.aspx?showNum=255 Catching up with Miguel de Icaza] by [[.NET Rocks!]]
* [http://radiotux.de/interviews/miguel_de_icaza_gnome.ogg Interview with Miguel de Icaza by [[RadioTux]]]
* [http://twit.tv/floss5 Miguel de Icaza interview] on [[FLOSS Weekly]]
* [http://port25.technet.com/archive/2006/08/11/Let_2700_s-talk-Mono_3A00_--Sam-interviews-Miguel-de-Icaza.aspx Talking Mono with Miguel de Icaza] on [[Port25]]
* [http://tllts.org/mirror.php?fname=tllts_131-04-04-06.ogg Linux Link Tech Show interview (audio), 2006] [http://tllts.org/mirror.php?fname=tllts_131-04-04-06.mp3 MP3]
* [http://derstandard.at/?url=/?id=2818611 March 2007 interview of de Icaza] by [[Der Standard]]

{{GNOME}}

{{Authority control}}

{{DEFAULTSORT:Icaza, Miguel De}}
[[Category:1972 births]]
[[Category:Free software programmers]]
[[Category:GNOME developers]]
[[Category:Living people]]
[[Category:Mexican bloggers]]
[[Category:Mexican computer programmers]]
[[Category:National Autonomous University of Mexico alumni]]
[[Category:People from Mexico City]]
[[Category:Technology evangelists]]