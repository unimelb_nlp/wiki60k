{{Use dmy dates|date=June 2014}}
{{Use British English|date=June 2014}}
{{Infobox book 
| name          = Kit and Kitty
| image         = Kit and Kitty by R D Blackmore - 1890 book cover.jpg
| caption       = Cover of the 1890 edition
| author        = [[R. D. Blackmore]]
| country       = United Kingdom
| language      = English
| genre         = 
| publisher     = 
| pub_date      = 1890
| pages         = 
| isbn          = 
}}
'''''Kit and Kitty: a story of west Middlesex''''' is a [[three-volume novel]] by [[R. D. Blackmore]] published in 1890. It is set near [[Sunbury-on-Thames]] in [[Middlesex]].

==Plot==
The novel is set in and around "Uncle Corny's" garden near [[Sunbury-on-Thames]]. The story turns on the love of Kit, the market-gardener's nephew, for Kitty, the daughter of a good but foolish scientific man, who has succeeded in making his own and his daughter's life miserable by marrying a second wife.<ref name="guardian">[https://www.newspapers.com/newspage/34369818/ Novels], ''The Guardian'', 5 February 1890, page 21</ref> This lady and her son Donovan are the villains of the story, and by their machinations poor Kit and Kitty are separated and made miserable.<ref name="guardian"/>

The course of true love is thwarted both before and after marriage: Kitty, for example, being stolen from her bridegroom during the honeymoon.<ref name="dubrev">''The Dublin Review'', (1889), Volume 106, page 440</ref> Poetic justice is amply wreaked in the end on all ill-doers in an accumulation of horrors, including a parricide, a suicide, a leper husband returned to claim his wife, and her collapse from the shock into paralysis and imbecility.<ref name="dubrev"/>

==Publication==
''Kit and Kitty'' was first published as three volumes in 1890.<ref name="cambio">"Richard Doddridge Blackmore" entry in ''The Cambridge Bibliography of English Literature: 1800-1900'', (1999), Cambridge University Press. ISBN 0521391008</ref>

==Reception==
''Kit and Kitty'' received mixed reviews. ''[[The Guardian]]'' found the book "interesting, well written, and very pleasant to read."<ref name="guardian"/> ''[[The Dublin Review]]'' reckoned that "Mr. Blackmore's gift of story-telling enables him to silence common-sense with a wave of his magician's wand."<ref name="dubrev"/> The ''Eclectic Magazine'' even reckoned that the novel would "rank among this great novelist's best work."<ref>The Eclectic Magazine, (1890), Volume 51, pages 425-6</ref> ''[[The Spectator]]'' enjoyed the first volume with its suburban idyll, but found the rest of the story to be "melodrama ... of the cheapest kind, with impossible villains, incredible plots, and a final scene of butchery which rivals the close of the last act of [[Hamlet]]."<ref name="spect">[http://archive.spectator.co.uk/article/25th-january-1890/19/recent-novels Recent Novels], ''The Spectator'', 25 January 1890, page 19</ref> It also complained about the "aggressive" use of "the technical details of fruit-growing" as a theme.<ref name="spect"/>

==References==
{{reflist}}

{{R. D. Blackmore}}

[[Category:1890 novels]]
[[Category:Novels by Richard Doddridge Blackmore]]