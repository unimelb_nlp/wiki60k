{|{{Infobox Aircraft Begin
 | name=B-80 Chablis
 | image=Boisavia B.80 Chablis 01 F-PBGO Persan 01.06.57 edited-2.jpg
 | caption=The first Chablis ''F-PBGO'' at Persan-Beaumont airfield near Paris in June 1957
}}{{Infobox Aircraft Type
 | type=Ultra-light monoplane
 | national origin=France
 | manufacturer=[[Boisavia]]
 | designer=Lucien Tieles
 | first flight=16 July 1950
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Boisavia B-80 Chablis''' was [[France|French]] light sport aircraft of the 1950s.

==Design and development==
The Chablis was designed by Lucien Tieles and constructed by Avions [[Boisavia]] in 1950.  It was a two-seat ultra-light monoplane with a high parasol wing supported by struts. The seats were arranged in tandem fashion. It was of extremely simple all-wood design with fabric covering and was intended to be fitted with a variety of engines in the 50-80 h.p. range.<ref>{{Harvnb|Green|1965|p=36}}</ref>

Two Chablis were built by Boisavia, the first ''F-PBGO'' making its first flight on 16 July 1950.<ref>{{Harvnb|Simpson|2005|p=64}}</ref> These were powered by a 65&nbsp;hp (48&nbsp;kW) [[Continental O-170|Continental A65]] flat four-cylinder air-cooled engine. The Chablis was intended for construction by amateur builders using kits supplied by the firm. In the event, no further examples were completed and further development was not proceeded with.<ref>{{Harvnb|Green|1965|p=36}}</ref>

==Specification==
{{aerospecs
|ref=<ref>{{Harvnb|Green|1965|p=36}}</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->met

|crew=2
|capacity=
|length m=7
|length ft=22
|length in=11
|span m=9
|span ft=29
|span in=6
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=513
|gross weight lb=1131
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Continental O-170|Continental A65]] flat four-cylinder air-cooled engine
|eng1 kw=<!-- prop engines -->48
|eng1 hp=<!-- prop engines -->65
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|perfhide=Y <!-- please remove this line if performance data becomes available -->
|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==References==
{{Commons category|Boisavia Chablis}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
*{{cite book|last=Green|first=William|title=The Aircraft of the World|year=1965|publisher=Macdonald & Co (Publishers) Ltd|isbn= |ref=harv}}
*{{cite book|last=Simpson|first=Rod|title=The General Aviation Handbook|year=2005|publisher=Midland Publishing|isbn=978-1-85780-222-1 |ref=harv}}
{{refend}}

{{Boisavia aircraft}}

[[Category:Boisavia aircraft|Chablis]]
[[Category:French sport aircraft 1950–1959]]
[[Category:Single-engined tractor aircraft]]
[[Category:Parasol-wing aircraft]]