{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox military person
|name= Virgil Paul Brennan
|image= 
|caption= 
|birth_date= 6 March 1920
|death_date= {{Death date and age|df=yes|1943|06|13|1920|03|06}}
|birth_place= [[Warwick, Queensland]]
|death_place= [[Garbutt, Queensland]]
|placeofburial= Townsville War Cemetery
|nickname= "Digger"<ref name="ADB"/>
|allegiance= Australia
|branch= [[Royal Australian Air Force]]
|serviceyears= 1940–1943
|rank= [[Flight Lieutenant]]
|unit= 
|commands= 
|battles= Second World War
* [[European Theatre of World War II|European Theatre]]
* [[Mediterranean, Middle East and African theatres of World War II|Mediterranean Theatre]]
* [[Siege of Malta (World War II)|Siege of Malta]]
|awards= [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]<br/>[[Distinguished Flying Medal]]
|relations= 
|laterwork= Co-author of ''Spitfires over Malta''
}}
'''Virgil Paul Brennan''', {{postnominals|country=AUS|size=100%|sep=,|DFC|DFM}} (6 March 1920 – 13 June 1943) was an Australian aviator and [[flying ace]] of the [[Second World War]]. Enlisting in the [[Royal Australian Air Force]] in November 1940, he briefly served in the [[European Theatre of World War II|European Theatre]] before transferring to [[Malta]]. Over the next five months, Brennan was officially credited with the destruction of 10 [[Axis powers|Axis]] aircraft from a total of twenty-four operational sorties. Reposted to England, he was assigned as a flying instructor and collaborated in the writing of ''Spitfires over Malta'', a book about his experiences on the island. Returning to Australia during 1943, Brennan was killed in a flying accident at [[Garbutt, Queensland]], in June that year.

==Early life==
Brennan was born in [[Warwick, Queensland]], on 6 March 1920 to Edgar James Brennan, a solicitor, and his wife Katherine (née O'Sullivan). He was educated at the Christian Brothers' School in Warwick, before moving on to [[Downlands College]] at [[Toowoomba, Queensland|Toowoomba]] and later [[Brisbane State High School]]. After leaving school, Brennan studied part-time at the [[University of Queensland]], while simultaneously being employed as a [[law clerk]] in [[Brisbane]].<ref name="ADB">{{Australian Dictionary of Biography|last=Wilson|first=David|year=1993|id=A130284b|title=Brennan, Virgil Paul (1920–1943)|accessdate=16 March 2009}}</ref>

==Second World War==
On 8 November 1940, Brennan enlisted in the Royal Australian Air Force for service during the Second World War.<ref>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=R&veteranId=1041455|title=Brennan, Virgil Paul|accessdate=16 March 2009|work=World War II Nominal Roll|publisher=Commonwealth of Australia}}</ref> Accepted for pilot training, he received his initial flight instruction in Australia. He later embarked for Canada, where he completed his flight training before being posted to the United Kingdom in August 1941, where he was appointed to an [[Operational Training Unit]]. On graduating from this course, he was allotted to [[No. 64 Squadron RAF|No.&nbsp;64 Squadron RAF]]. During this time, he was advanced to temporary [[flight sergeant]] on 4 January 1942, prior to receiving a posting to the [[Mediterranean, Middle East and African theatres of World War II|Mediterranean Theatre]] the following month.<ref name="ADB"/>

===Malta===
On arrival in the Mediterranean, Brennan was posted to [[No. 249 Squadron RAF|No.&nbsp;249 Squadron RAF]]. On 7 March 1942, Brennan was one of fifteen pilots sent to the island of Malta. Flying [[Supermarine Spitfire]]s, the party took off from the aircraft carrier [[HMS Eagle (1918)|HMS ''Eagle'']]; they were to spend the next few months in the defence of the island. As the [[Axis powers|Axis forces]] commenced a major aerial assault on Malta later that month, the [[Allies of World War II|Allied]] fighter pilots on the island were forced to "contend with fatigue and inadequate rations while battling the enemy's superior forces". On 17 March, Brennan claimed his first aerial victory when he shot down a [[Messerschmitt 109]].<ref name="ADB"/>

On 20 April 1942, Brennan added a further two aircraft to his tally when he destroyed a Messerschmitt 109, before bringing down a [[Junkers 88]] later that day.<ref name="ADB"/> Praised as "a most determined and courageous pilot",<ref name="DFM">{{cite web|url=http://www.awm.gov.au/cms_images/awm192/00306/003060128.pdf|title=Recommendation for Virgil Paul Brennan to be awarded a Distinguished Flying Medal|accessdate=16 March 2009|format=PDF|work=Index to Recommendations: Second World War|publisher=Australian War Memorial}}</ref> Brennan was subsequently recommended for the [[Distinguished Flying Medal]].<ref name="DFM"/> The announcement and accompanying citation for the award was published in a supplement to the ''[[London Gazette]]'' on 22 May 1942, reading:<ref>{{LondonGazette|issue=35569|date=22 May 1942|startpage=2239|supp=yes|accessdate=16 March 2009}}</ref>

{{quote|''Air Ministry, 22nd May, 1942.''

ROYAL AIR FORCE.

The KING has been graciously pleased to approve the following awards in recognition of gallantry displayed in flying operations against the enemy: —

Distinguished Flying Medal.

Aus. 404692 Sergeant Virgil Paul BRENNAN, Royal Australian Air Force, No. 249 Squadron.

This airman is a most determined and courageous pilot. An exceptional shot, he always presses home his attacks with vigour. In 2 combats, he has destroyed at least 4 enemy aircraft and damaged others.}}

[[File:RAF Supermarine Spitfire Malta.JPG|thumb|alt=A black and white photograph of an aircraft with military style painting flying.|left|250px|A Supermarine Spitfire operating off Malta.]]

Brennan scored further aerial victories on 10 May.<ref>{{Harvnb|Nichols|2008|p=35}}</ref> The following day, German ''[[Luftwaffe]]'' General [[Albert Kesselring]] ordered a contingent of 20 [[Junkers Ju 87|Stukas]] and 10 [[Junkers Ju 88]]s with a small escort of fighter aircraft to bomb [[Grand Harbour]], Malta. A formation of 50 [[Royal Air Force]] aircraft—37 Spitfires and 13 [[Hawker Hurricane]]s—were dispatched to intercept the group; Brennan was piloting one of the Spitfires. Attacking one of the Stukas, Brennan later recorded that the aircraft "disintegrated, with huge chunks flying off in every direction".<ref name="McCaffery"/> During the battle, a total of 14 German aircraft were shot down, for the loss of 2 Spitfires.<ref name="McCaffery">{{Harvnb|McCaffery|1998|pp=77–78}}</ref> In an engagement the next day, Brennan was wounded in his left arm. He was [[Officer (armed forces)|commissioned]] as a [[pilot officer]] later that month.<ref name="ADB"/>

By the conclusion of his combat tour in July 1942, Brennan had flown a total of twenty-two operational sorties and was credited with destroying 10 Axis aircraft over Malta, with one probably destroyed and a further 6 damaged.<ref name="ADB"/><ref>{{Harvnb|Nichols|2008|p=86}}</ref> For his efforts in the destruction of Axis aircraft during this period, Brennan was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]].<ref>{{cite web|url=http://www.awm.gov.au/cms_images/awm192/00306/003060127.pdf|title=Recommendation for Virgil Paul Brennan to be awarded a Distinguished Flying Cross|accessdate=16 March 2009|format=PDF|work=Index to Recommendations: Second World War|publisher=Australian War Memorial}}</ref> The notification of the decoration was published in a supplement to the ''London Gazette'' on 6 October 1942.<ref>{{LondonGazette|issue=35731|date=6 October 1942|startpage=4342|supp=yes|accessdate=16 March 2009}}</ref>

===Later war service and death===
Embarking from Malta during July 1942, Brennan returned to the United Kingdom and was posted to No.&nbsp;52 Operational Training Unit as an instructor with the rank of acting [[flight lieutenant]]. During this period, Brennan and fellow No.&nbsp;249 Squadron pilot, Pilot Officer Ray Hesselyn, collaborated with journalist [[Charles Bateson|Henry Bateson]] on writing ''Spitfires over Malta'', a novel relating the experiences of Brennan and Hesselyn during their time on Malta. On 17 April 1943, Brennan was repatriated from the United Kingdom and returned to Australia.<ref name="ADB"/>

Arriving back in Australia, Brennan was posted to the newly raised [[No. 79 Squadron RAAF|No.&nbsp;79 Squadron]], based at [[Laverton, Victoria]], on 1 May 1943. Later that month, the squadron was ordered to deploy to [[Goodenough Island]], near [[New Guinea]]. During this time, Brennan related his previous combat experiences to fellow pilots, however his commanding officer, Squadron Leader [[Alan Rawlinson]], noted that Brennan appeared "strained and tired".<ref name="ADB"/><ref name="No.79">{{cite web|url=http://www.awm.gov.au/units/unit_11109second_world_war.asp|title=79 Squadron RAAF|accessdate=16 March 2009|work=Australian military units|publisher=Australian War Memorial}}</ref> An advance party of the squadron was moved up to Goodenough Island that month, while the pilots followed during June.<ref name="No.79"/>

On 13 June 1943, the pilots of No.&nbsp;79 Squadron continued on their journey north, arriving at Garbutt airfield in Queensland. At approximately 14:00, Brennan landed his Spitfire in the [[wake turbulence]] of the aircraft ahead of himself and touched down on the left side of the runway. Brennan was informed that he was cutting in on the path of the Spitfire following him, which was to land on the right side of the runway. Brennan landed his aircraft short, and at the conclusion of his landing run turned across the path of the second Spitfire. In the ensuing collision, Brennan sustained severe injuries and was rushed to hospital; he died before arrival.<ref name="ADB"/><ref>{{cite web|url=http://naa12.naa.gov.au/scripts/imagine.asp?B=1063674&I=1&SE=1|title=Brennan, Virgil Paul : Service Number – 404692; File type – Casualty|accessdate=16 March 2009|work=Records Search|publisher=National Archives of Australia}}</ref> Described as one with "an easy-going nature, an engaging sense of humour and&nbsp;... loyal to his friends",<ref name="ADB"/> Brennan was buried in Townsville War Cemetery.<ref>{{cite web|url=http://www.cwgc.org/search/casualty_details.aspx?casualty=2244130|title=Brennan, Virgil Paul|accessdate=16 March 2009|work=Casualty Details|publisher=Commonwealth War Graves Commission}}</ref>

==Notes==
{{Reflist|30em}}

==References==
* {{cite book|last=McCaffery|first=Dan|title=Hell Island|year=1998|publisher=James Lorimer & Company|location=Toronto, Canada|isbn=1-55028-625-0|ref=harv}}
* {{cite book|last=Nichols|first=Steve|title=Malta Spitfire Aces|year=2008|publisher=Osprey Publishing|location=Oxford, England|isbn=1-84603-305-5|ref=harv}}

==Further reading==
* {{cite book|last=Brennan|first=Paul|last2=Hesselyn|first2=Ray|last3=Bateson|first3=Henry|authorlink3=Charles Bateson|title=Spitfires over Malta|year=1943|publisher=Consolidated Press|location=Sydney, Australia|isbn=}}

{{Good article}}

{{DEFAULTSORT:Brennan, Virgil}}
[[Category:1920 births]]
[[Category:1943 deaths]]
[[Category:Accidental deaths in Queensland]]
[[Category:Australian aviators]]
[[Category:Australian World War II flying aces]]
[[Category:People educated at Brisbane State High School]]
[[Category:People from Warwick, Queensland]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Recipients of the Distinguished Flying Medal]]
[[Category:Royal Australian Air Force officers]]
[[Category:University of Queensland alumni]]
[[Category:Victims of aviation accidents or incidents in Australia]]