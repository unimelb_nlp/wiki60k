<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Schoettler I
 | image=SchoettlerB.tif
 | caption=
}}{{Infobox Aircraft Type
 | type=Two seat [[biplane]]
 | national origin=[[China]]
 | manufacturer=
 | designer=F.L. Schoettler
 | first flight=Summer 1923
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Schoettler I''' was one of the first aircraft constructed in [[China]], albeit with a [[Germany|German]] designer. It was a two-seat, single engine [[biplane]], first flown in mid-summer 1923.

==Design and development==
[[File:Schoettler(1).tif|right|thumb|Schoettler 1 in 1923]]
In late 1923 ''Flight'' reported on what was claimed to be the first successful aircraft constructed in [[China]]. It was designed by the German engineer Leopold Carl Ferdinand Friedrich Schoettler (born April 7, 1881 in Bruchhausen-Vilsen, Germany; died September 27, 1948) but the only major components imported from [[Europe]] were the engine, instruments, wheels and dope for the [[aircraft|fabric covering]]; everything else was locally produced from local materials by workers without aviation experience or modern machinery. Work on it began in the summer of 1922.<ref name=Flight/>

The Schoettler I was a conventional European style two seat [[tractor configuration|tractor]] [[biplane]], rather similar to the German [[Aviatik B.II]] and [[Albatros B.II]] designs, with equal span [[biplane#Bays|two bay]] wings. These were mounted with 2° of [[dihedral (aircraft)|dihedral]] and 597&nbsp;mm, almost 2&nbsp;ft, of [[Biplane#Stagger|stagger]].  The gap between the upper and lower planes was {{convert|66|in|mm|abbr=on|0|disp=flip}}, maintained by parallel pairs of [[aerofoil]] section [[strut]]s and wire bracing. The unswept wings had a constant [[chord (aircraft)|chord]] of {{convert|63|in|mm|abbr=on|0|disp=flip}} with blunt [[wing tip]]s and [[ailerons]] on both upper and lower planes.  The Schoettler's [[empennage]] was also conventional.<ref name=Flight/>

The [[fuselage]] was likewise a standard rectangular section wooden girder structure, [[aircraft fabric covering|fabric covered]] except around the engine and a wood upper decking around the open, [[tandem]] [[cockpit]]s for pilot and for the observer, who sat under the wing [[trailing edge]]. It tapered to a knife-edge at the tail. At the front the {{convert|160|hp|kW|abbr=on|0}} Mercedes water-cooled upright [[Straight engine|inline engine]] was enclosed in a rectangular cross-section metal cowling which tapered vertically, exposing the upper [[cylinder (engine)|cylinder]]s, to a two blade [[propeller (aircraft)|propeller]]. At the rear of the housing an external [[radiator (engine cooling)|radiator]], with shutters for engine temperature control, projected on each side. The Schoettler had a [[conventional undercarriage|conventional]] fixed undercarriage, with the mainwheels on a rigid axle mounted on V-struts.<ref name=Flight/>

Flight does not report a first flight date, but this was on or before 23 July 1923 when the Schoettler  was test flown by an ex-[[RAF]] pilot, W.E. Holland.  The latter reported good handling and an excellent, 360°, field of view for the observer noting the aircraft's potential for development.<ref name=Flight/> More recent articles claim the first flight by a Chinese built aircraft was that of the [[Xianyi Rosamonde]] (or [[Dashatou Rosamonde]]) on 12 July 1923, though without mention of the Schoettler;<ref name=Scramble/><ref name=baike/> the two aircraft were evidently close contemporaries.

==Specifications==
{{Aircraft specs
|ref=Flight, 1 November 1923<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|capacity=Two
|length ft=27
|length in=4.75
|length note=
|span ft=39
|span in=6
|span note=
|height ft=10
|height in=3
|height note=
|wing area sqft=401.75
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight lb=1634
|empty weight note=
|gross weight lb=2558
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Mercedes
|eng1 type=
|eng1 hp=160
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed mph=128
|max speed note=at 1,000 ft (305 m)
|cruise speed mph=98
|cruise speed note=
|stall speed mph=45
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=4.5 h
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading lb/sqft=6.3
|wing loading note=
|power/mass=0.063 hp/lb (103 W/kg)
|thrust/weight=
|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
}}

<!--==See also==-->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{commons category|Schoettler I}}
{{reflist|refs=

<ref name=Flight>{{cite magazine|date=1 November 1923 |title=The "Schoettler I" biplane|magazine=[[Flight International|Flight]]|volume=XV|issue=44|pages=675–6|url=http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200675.html}}</ref>

<ref name=Scramble>{{cite web |url=http://wiki.scramble.nl/index.php/Xianyi_Rosamonde|title=Xianyi Rosamonde |author= |work= |publisher= |accessdate=17 April 2013}}</ref>

<ref name=baike>{{cite web |url=http://www.baike.com/wiki/%E4%B9%90%E5%A3%AB%E6%96%87%E4%B8%80%E5%8F%B7|title=Dulux No.1 (in Chinese) |accessdate=17 April 2013}}</ref>
}}
<!-- ==Further reading== -->
<!-- ==External links== -->

[[Category:Biplanes]]
[[Category:Single-engine aircraft]]
[[Category:Chinese aircraft 1920–1929]]