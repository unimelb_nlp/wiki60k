{{lowercase title}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{| {{Infobox aircraft begin
 |name = Spectre
 |image = De Havilland Spectre RAFM Cosford.JPG
 |caption = de Havilland Spectre on display at the [[Royal Air Force Museum Cosford]]
}}{{Infobox aircraft engine
 |type=[[Rocket engine]]
 |manufacturer=[[de Havilland]]
 |national origin=[[United Kingdom]]
 |first run={{Avyear|1952}}
 |major applications= [[Saunders-Roe SR.53]]
 |number built = 
 |program cost =
 |unit cost = 
 |developed from = 
 |variants with their own articles = 
 |developed into = 
}}
|}

The '''de Havilland Spectre'''  was a [[rocket engine]] built by [[de Havilland Engine Company]] in the 1950s. It was one element of the intended mixed powerplant for combination rocket-jet interceptor aircraft for the [[Royal Air Force]], such as the [[Saunders-Roe SR.53]].

==Design and development==
The Spectre was a bipropellant engine burning [[kerosene]] and [[hydrogen peroxide]]. The power could be controlled from 10-100% delivering 8,000 [[lbf]] (35.7&nbsp;kN) of thrust at full power. In the [[SR.53]] it used the same fuel tanks as the turbojet engine and if run at full power was expected to consume the full load in about seven minutes.

In 1952 static testing commenced with the Spectre DSpe.l. The aircraft industry had no precedent for an engine which would gain in thrust with altitude and the required maximum thrust was estimated at between {{convert|2000|lbf|kN|abbr=on}} and {{convert|15000|lbf|kN|abbr=on}} thrust. The design was based on a variable thrust which could be throttled from {{convert|8000|lbf|kN|abbr=on}} to {{convert|2000|lbf|kN|abbr=on}}. Design philosophy was matched to the mixed power concept of an aircraft having both a turbojet and rocket engine for maximum operational flexibility.<ref>{{cite journal
  |title=More About the Super Sprite
  |journal=The Aeroplane 
  |date=29 July 1955
}}</ref>

Primary innovation was as the first to incorporate its turbo pump turbine upstream of its combustion chamber. Described then as low loss.
Technological innovation embraced the [[Barske centrifugal pump|Barske]] high-speed open-impeller [[centrifugal pump]]s,<ref>{{cite journal
  |last=Barske
  |first=U. M.
  |title=Development of Some Unconventional Centrifugal Pumps 
  |journal=Proc. IMech Engrs 
  |volume= 174
  |issue= 11 
  |year=1960
}}</ref> as formerly researched in the [[Hellmuth Walter|Walter]] organisation, [[Regenerative cooling (rocket)|regenerative cooling]] with pump stages both upstream and downstream, gauze catalyst packs, low-loss internal-flow turbine and the use of straight kerosene fuel. The aircraft tanks were to be pressurised to suppress pump [[cavitation]] problems.<ref>{{cite journal
 |author=W. N. Neat, 
 |title=Development of the Spectre (Lecture to Royal Aeronautical Society) 
 |date=14 November 1958 
 |journal=Flight I 
 |url=http://www.cue-dih.co.uk/aerospace/aeropdfs/htp_for_prop.pdf 
 |format=PDF 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20060215104117/http://www.cue-dih.co.uk:80/aerospace/aeropdfs/htp_for_prop.pdf 
 |archivedate=2006-02-15 
 |df= 
}}</ref>

It went through rig tests commencing in 1953, bench tests from mid-1954, and testing in two [[English Electric Canberra|Canberra]]s. From flight approval in Autumn 1956, flight experience again posed altitude starvation problems. Clearance was given for flight in the SR.53 prototype from May 1957.<ref>{{cite journal|title=Enterprise in Rocketry Activity at De Havilland Engine Co. |journal=The Aeroplane |date=4 March 1955 |url=http://www.cue-dih.co.uk/aerospace/aeropdfs/htp_for_prop.pdf |format=PDF |deadurl=yes |archiveurl=https://web.archive.org/web/20060215104117/http://www.cue-dih.co.uk/aerospace/aeropdfs/htp_for_prop.pdf |archivedate=February 15, 2006 }}</ref><ref>{{cite web|url=http://www.skomer.u-net.com/projects/spectre.htm |title=Spectre |publisher=Skomer |deadurl=yes |archiveurl=https://web.archive.org/web/20081202122058/http://www.skomer.u-net.com/projects/spectre.htm |archivedate=December 2, 2008 }}</ref>

In October 1957 a contract was announced for a more advanced version of the aircraft as the [[Saunders-Roe SR.177|SR.177]] to utilise a revised design Spectre DSpe.5 engine together with a reheated supersonic capability {{convert|14000|lbf|kN|abbr=on}} thrust [[de Havilland Gyron Junior]] [[turbojet]] engine, thus meeting a full mixed-power aircraft concept. In conjunction with the new engine, development had been undertaken with two major ancillaries, a peroxide starter for the gas turbine and a peroxide [[auxiliary power unit]]. Virtually on the heels of the announcement of the contract came the notorious [[1957 Defence White Paper]] declaring that all future combat would be undertaken by computer controlled missiles, and that manned interceptors were now considered obsolete.

Development flying of the SR.53 continued through 39 flights operating to Mach 1.33, and to altitudes at {{convert|55000|ft|m|abbr=on}}, as research and construction proceeded on the SR.177, until its cancellation in 1958.

After merging of interests in 1959, it was manufactured by [[Bristol Siddeley]].

The Spectre project was cancelled in October 1960, at a reported total cost of £5.75 million.<ref name="Flight, 17 August 1967, Cancelled projects list" >{{cite journal
  |journal=[[Flight International]] 
  |title=Cancelled projects: the list up-dated
  |url=http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%201672.html
  |format=PDF
  |date=17 August 1967
  |page=262
|number=3049 
|volume=92
  |ref=Flight, Flight, 17 August 1967, Cancelled projects list
}}</ref>

==Variants==
Variants included:
;Dspe.1:Initial version
;Dspe.2:Constant thrust version, simpler design
;Dspe.3:Development of Dspe.1 with variable thrust
;Dspe.4:Development of Dspe.2
;Dspe.5:Further development of Dspe.1
;DSpe.D.1 Double Spectre: A doubled version of this engine was used for early flight-testing of [[Blue Steel missile|Blue Steel]] nuclear stand off bomb, with two chambers arranged vertically.<ref>{{cite web
  |url=http://www.skomer.u-net.com/projects/sr53.htm
  |title=Double Spectre
  |publisher=Skomer
  |format=image
}}</ref>

The conventional Spectre DSpe.5 had been developed alongside a DSpe.4 [[RATO]] variant, the latter for the [[Avro Vulcan]] and [[Handley-Page Victor]] [[V bomber]]s, another programme subsequently cancelled after a single trial take-off of a Victor from the de Havilland aerodrome at [[Hatfield, Hertfordshire|Hatfield]]. These two engines were then used in combination to power the development rounds of the [[Blue Steel missile]] stand-off bomb, together with the peroxide [[auxiliary power unit|APU]], from its first flight in October 1959.{{citation needed|date=June 2015}}

==Applications==
*[[Blue Steel (missile)|Blue Steel missile]]
*[[Saunders-Roe SR.53]]

==Engines on display==
Preserved Spectre engines are on display at the following museums:
* [[Royal Air Force Museum Cosford]]
* [[De Havilland Aircraft Museum]]

==Specifications==
[[File:De Havilland Spectre DH Museum.JPG|thumb|Spectre rocket engine on display at the [[de Havilland Aircraft Museum]]]]
{{rocketspecs
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref={{citation needed|date=June 2015}}
|type=[[Liquid-propellant rocket|Liquid-propellant rocket engine]]
|length= 
|diameter= 
|weight= 
|combustion=
|fueltype=Hydrogen peroxide/Kerosene
|thrust=8,000 lbf (35.7 kN) (full power)
|fuelcon=
|specfuelcon=
|thrust/weight=
}}

==See also==
{{aircontent
<!-- first, the related articles that do not fit the specific entries: -->
|see also=

<!-- designs which were developed into or from this aircraft or engine: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

== References ==
<div style="display: none;" >

<ref name="Flight, 6 May 1955, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Britain's New Engines
   |url=http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%200572.html
  |format=PDF
  |date=6 May 1955
  |pages=572
  |ref=Flight, 6 May 1955, Spectre
}}</ref><!-- Early announcement. Even at this date, variants 1-5 are mentioned -->

<ref name="Flight, 17 June 1955, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Paris Show
  |url=http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%200840.html
  |format=PDF
  |year= 1955
  |pages=838
  |ref=Flight, 1955, Spectre
}}</ref><!-- the flight path of a high-altitude interceptor and the name Spectre announced, but no other details -->

<ref name="Flight, 5 August 1955, Spectre 2" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre 
  |url=
  |format=PDF
  |date=5 August 1955
  |pages=
  |ref=Flight, 5 August 1955, Spectre 2
}}</ref>

<ref name="Flight, 2 September 1955, Super Sprite" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Rocket Motors
  |url=http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201281.html
  |format=PDF
  |volume=68  |issue=2432
  |date=2 September 1955
  |pages=395
  |ref=Flight, 2 September 1955, Super Sprite
}}</ref><!--  also notes Spectre announced earlier in the year, and likely to be pump-fuelled -->

<ref name="Flight, 2 September 1955, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=At Farnborough Next Week
  |url=http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201223.html
  |format=PDF
  |volume=68  |issue=2432
  |date=2 September 1955
  |pages=
  |ref=Flight, 2 September 1955, Spectre
}}</ref><!-- First Farnborough appearance for Spectre and A-S Screamer -->

<ref name="Flight, 9 September 1955, Tomorrow's Engines" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Tomorrow's Engines
  |url=http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201336.html
  |format=PDF
  |date=9 September 1955
  |pages=450
  |ref=Flight, 9 September 1955, Tomorrow's Engines
}}</ref><!-- Spectre announced, planned for pairing with Gyron Junior as high-altitude interceptor -->

<ref name="Flight, 8 March 1957, de Havilland engines 1957" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=de Havilland engines 1957
  |url=http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%200295.html
  |format=PDF
  |date= 8 March 1957
  |pages=297–298
  |ref=Flight, 8 March 1957, de Havilland engines 1957
}}</ref><!-- Notes that Spectre is still so secret that thrust & fuel are unknown, but that there are 5 variants. Also that the first test was July 1954. Flight clearance achieved shortly after, flight testing now under way in Canberra VN813 -->

<ref name="Flight, 5 August 1957, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre 
  |url=
  |format=PDF
  |date=5 August 1957
  |pages=
  |ref=Flight, 5 August 1957, Spectre
}}{{failed verification|date=June 2015}}</ref>

<ref name="Flight, 23 August 1957, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre 
  |url=http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%201157.html
  |format=PDF
  |date=23 August 1957
  |pages=245
  |ref=Flight, 23 August 1957, Spectre
}}</ref><!-- DSpe.5 -->

<ref name="Flight, 30 August 1957, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre
  |chapter=British Aero Engines, 1957
  |url=http://www.flightglobal.com/pdfarchive/view/1957/1957%20-%201248.html
  |format=PDF
  |date=30 August 1957
  |pages=336
|number= 2536 
|volume= 72
  |ref=Flight, 30 August 1957, Spectre
}}</ref>

<ref name="Flight, 7 February 1958, Spectre ATO unit" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre ATO unit 
  |url=http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200170.html 
  |format=PDF
  |date=7 February 1958
  |pages=178–179
|number=2559 
|volume=73
  |ref=Flight, 7 February 1958, Spectre ATO unit
}}</ref>

<ref name="Flight, 14 November 1958, Spectre development history" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Development of the Spectre
  |url=http://www.flightglobal.com/pdfarchive/view/1958/1958-1-%20-%200761.html
  |format=PDF
  |date=14 November 1958
  |pages=765–766
  |volume=74 
  |number=2599
  |ref=Flight, 14 November 1958, Spectre development history
}}</ref>

<ref name="Flight, 20 March 1959, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre
  |chapter=Aero engines, 1959
  |url=http://www.flightglobal.com/pdfarchive/view/1959/1959%20-%200795.html
  |format=PDF
  |date=20 March 1959
  |page=392
|number=2617
|volume=75
  |ref=Flight, 20 March 1959, Spectre
}}</ref><!-- Chief versions: 
DSpe.4 Vulcan & Victor
DSpe.5 SR.53

"Most important member of the family" is the Double Spectre (illus.)

-->

<ref name="Flight, 20 July 1961, Spectre" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Spectre
  |chapter=Aero engines, 1961
  |url=http://www.flightglobal.com/pdfarchive/view/1961/1961%20-%200979.html
  |format=PDF
  |date= 20 July 1961
  |pages=
  |ref=Flight, 20 July 1961, Spectre
}}</ref><!-- 
DSpe.4 for RDS.15 ATO Victor  "limited quantities"
DS.33 for Blue Steel "delivered in quantity"
-->

</div>
{{reflist|colwidth=30em}}

==External links==
{{Commons category|De Havilland Spectre}}

{{DHaeroengines}}
{{Aviation rocket engines}}

[[Category:Aircraft rocket engines]]
[[Category:De Havilland aircraft engines|Spectre]]
[[Category:Rocket engines using kerosene propellant]]