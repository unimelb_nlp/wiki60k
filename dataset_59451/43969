{|{{Infobox Aircraft Begin
 |name=Mercury
 |image=CosmosMercury.JPG
 |caption=The Cosmos Mercury
}}{{Infobox Aircraft Engine
 |type=[[Radial engine|Radial]] [[Aircraft engine|aero engine]]
 |manufacturer=[[Cosmos Engineering]]<br>[[Straker-Squire|Brazil Straker]]
 |designer=[[Roy Fedden]]
 |first run= July {{avyear|1917}}
 |major applications=[[Bristol Scout F]]
 |number built = 
 |program cost = 
 |unit cost =  
 |developed from = 
 |developed into = 
 |variants with their own articles =
}}
|}

:''For the 1926 nine-cylinder radial engine see [[Bristol Mercury]]''

The '''Cosmos Mercury''' was a fourteen-cylinder twin-row air-cooled radial [[Aircraft engine|aeroengine]]. Designed by [[Roy Fedden]] of [[Cosmos Engineering]], it was built in the United Kingdom in 1917. It produced 347&nbsp;horsepower (259&nbsp;kW). It did not enter production; a large order was cancelled due to the [[Armistice with Germany (Compiègne)|Armistice]].

==Design and development==
Built in [[Fishponds]], [[Bristol]] by [[Straker-Squire|Brazil-Straker]] under the direction of Roy Fedden, the Mercury featured an unusual [[crankshaft]] and [[connecting rod]] arrangement that dispensed with the more normal design of a single master rod linking to individual rods for each cylinder. It was said to run well without vibration and set an unofficial time to climb record while fitted to the [[Bristol Scout F]], the aircraft achieving 10,000&nbsp;ft (3,000 m) in 5.4 minutes and 20,000&nbsp;ft (6,000 m) in 16.25 minutes.<ref name = "Lumsden p.92">Lumsden 2003, p.92.</ref>

An [[Admiralty]] order for 200 engines was placed in 1917 but was later cancelled by [[William Weir, 1st Viscount Weir|Lord Weir]] due to the end of [[World War I]], it is also stated that Lord Weir had a preference for the [[ABC Dragonfly]].<ref>Gunston 1989, p.44.</ref>

The name was re-used by Fedden for the later nine-cylinder [[Bristol Mercury]] radial engine.

==Applications==
* [[Bristol Scout F]]

==Specifications (Mercury)==
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully-formatted line with <li> -->
|ref=''Lumsden''.<ref name= "Lumsden p.92"/><ref name=JWWI>{{cite book |title=|last=Taylor |first=Michael |coauthors= |edition=|year=2001|publisher= Jane's Publishing Company|location=London|isbn=1-85170-347-0|page=279 }}</ref><ref name=JAWA1919>{{cite book|last=Grey|first=C.G.|title=Jane's All the World's Aircraft 1919|year=1969|publisher=David & Charles (Publishing) Limited|isbn=978-0-7153-4647-1|pages=1b to 145b|edition=Facsimile}}</ref>
|type=14-cylinder air-cooled two-row [[radial engine]]
|bore={{convert|4.375|in|mm|abbr=on|1}}
|stroke={{convert|5.8|in|mm|abbr=on|1}}
|displacement={{convert|1,223|cuin|l|abbr=on|1}}
|length=
|diameter={{convert|41.625|in|mm|abbr=on|1}}
|width=
|height=
|weight={{convert|587|lb|kg|abbr=on|0}}
|valvetrain=3 [[poppet valve]] per cylinder; 2 exhaust and 1 inlet
|supercharger=
|turbocharger=
|fuelsystem=
|fueltype=Petrol
|oilsystem=Pressure feed to main bearings
|coolingsystem=Air-cooled
|power='''Normal:''' {{convert|315|hp|kW|abbr=on|0}} at 1,800 rpm at sea level, '''Maximum:''' {{convert|347|hp|kW|abbr=on|0}} at 2,000 rpm at sea level
|specpower=
|compression=5.3:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
* [[Armstrong Siddeley Jaguar]]
* [[Gnome-Rhône 14M]]
* [[Pratt & Whitney R-1535 Twin Wasp Junior]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

== References ==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book|last=Grey|first=C.G.|title=Jane's All the World's Aircraft 1919|year=1969|publisher=David & Charles (Publishing) Limited|isbn=978-0-7153-4647-1 |pages=1b to 145b|edition=Facsimile}}
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
{{refend}}

==External links==
*{{cite journal |date=July 3, 1919 |title=The Cosmos Aero Engines |format=PDF |journal=[[Flight (magazine)|Flight]] |volume=XI |issue=27 |id=No. 549 |pages=869–871 |url=http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200869.html |accessdate=January 12, 2011 }} Contemporary article on Cosmos Engineering's air-cooled radial engines. Photos of the Mercury are on page 869, and a short technical description is on [http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200871.html page 871].
*[http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200675.html ''Flight'' magazine, 22 May 1919 - Flightglobal.com]
*[http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%200208.html ''Flight'' magazine, 12 February 1960 - Flightglobal.com] 
*[http://www.aviationarchive.org.uk/stories/pages.php?enum=GE122&pnum=13&maxp=15 Cosmos Mercury at Aviationarchive.org]

{{Cosmos aeroengines}}

[[Category:Radial engines]]
[[Category:Aircraft piston engines 1910–1919]]