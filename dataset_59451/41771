{{Use dmy dates|date=April 2015}}
{{Use South African English|date=April 2015}}
{{infobox military award
| name             = Sir Harry Smith's Medal for Gallantry
| image            = [[File:Sir Harry Smith's Medal for Gallantry.jpg|300px]]
| caption          = 
| awarded_by       = [[Sir Harry Smith, 1st Baronet|Major General Sir Henry George Wakelyn Smith]], [[Baronet|Bt]] [[Order of the Bath|GCB]]
| country          = [[File:Cape Colony flag.png|x18px]] [[Cape Colony|Cape of Good Hope]]
| type             = Military decoration for bravery
| eligibility      = All ranks
| for              = Gallantry in action
| campaign         = [[Xhosa Wars#Eighth war (1850–53)|8th Cape Frontier War]]
| status           = Unofficial
| description      = 
| motto            = 
| clasps           = 
| post-nominals    = 
| established      = 1851
| first_award      = 1851
| last_award       = 
| total            = 31
| posthumous       = 
| recipients       = 
| precedence_label = 
| individual       = 
| higher           = 
| same             = 
| lower            = 
| related          = 
| image2           = [[File:Ribbon - Sir Harry Smith's Medal for Gallantry.png|x29px]] 
| caption2         = Ribbon bar
}}

In the Colonies and former Boer Republics which became the [[Union of South Africa]] in 1910, several unofficial military decorations and medals were instituted and awarded during the nineteenth and early twentieth century. '''Sir Harry Smith's Medal for Gallantry''' is an unofficial military decoration for bravery, awarded for actions following the siege of [[Fort Cox, Eastern Cape|Fort Cox]] in December 1850, at the beginning of the [[Xhosa Wars#Eighth war (1850–53)|8th Cape Frontier War]]. The medal was privately instituted in 1851 by [[Sir Harry Smith, 1st Baronet|Major General Sir Henry George Wakelyn Smith]] [[Baronet|Bt]] [[Order of the Bath|GCB]], at the time the Governor and Commander-in-Chief of the [[Cape Colony|Cape of Good Hope]].<ref name="OnlineMedals">[http://www.onlinemedals.co.uk/medal-encyclopaedia/pre-ww1-medals/sir-harry-smith%E2%80%99s-medal-gallantry Online Medals - Medal Encyclopaedia - Sir Harry Smith’s Medal For Gallantry] (Access date 27 April 2015)</ref><ref name="ArmyMuseum">[http://www.nam.ac.uk/online-collection/detail.php?acc=1986-12-31-1%20 National Army Museum - Sir Harry Smith Medal for Gallantry 1851, awarded to Paul Arendt] (Access date 27 April 2015)</ref><ref name="Unofficial"/>

==The 8th Cape Frontier War==
Fort Cox was situated inland from [[King William's Town]]. During the unrest in the [[Eastern Cape]] in December 1850, which led to the outbreak of the 8th Cape Frontier War, the longest, costliest and bloodiest of the frontier wars, [[Cape Colony|Cape of Good Hope]] Governor Sir Harry Smith travelled to the Fort to meet with prominent [[Xhosa people|Xhosa]] chiefs. Smith's reputation for humiliating treatment of the various chiefs had already fostered a deep, smouldering anger amongst the Xhosa peoples, even though he still believed that they regarded him as their ''Inkhosi Inkhulu'' or Supreme Chief.<ref name="OnlineMedals"/><ref name="ArmyMuseum"/><ref name="Harington">[http://samilitaryhistory.org/vol031ah.html The South African Military History Society - Military History Journal Vol 3 No 1, June 1974 - Sir Harry Smith, By Andrew L. Harington] (Access date 27 April 2015)</ref><ref name="Saks">[http://samilitaryhistory.org/vol154ds.html The South African Military History Society - Military History Journal Vol 15 No 4, December 2011 - Harry Smith, Henry Somerset and the Siege of Fort Cox, By David Saks] (Access date 27 April 2015)</ref>

[[File:Mgolombane Sandile - Xhosa Chief.jpg|thumb|left|150px|Mgolombane Sandile]]
One of the chiefs, [[Mgolombane Sandile]], paramount chief of the Rharhabe clan, refused to attend the meeting outside the fort on 19 December and was therefore declared as deposed and a fugitive by Smith, who ordered the gathering of some 3,000 Ngqika and their chiefs to capture Sandile and his rebels to demonstrate their own loyalty to the Crown and avoid the fate of those who defied it. This was the last straw and Fort Cox then came under siege from warriors of the Xhosa tribes, led by Chief Sandile.<ref name="ArmyMuseum"/><ref name="Harington"/><ref name="Saks"/>

Fort Cox was not provisioned to withstand a long siege, had no artillery and could only be supplied with water by hazardous expeditions to and from the [[Keiskamma River]] far below. Several attempts to relieve the Fort were unsuccessful and Smith, concerned that his being trapped in Fort Cox would affect the Colony's morale and cause the defection of loyal Xhosa tribes, decided to fight his way out. On 31 December 1850, escorted by about 250 men of the [[Cape Mounted Riflemen#Cape Mounted Riflemen (1827–1870)|Cape Mounted Riflemen]], which consisted of [[Khoisan]] and [[Coloured]] men under white officers, Smith succeeded to get through the Xhosa lines and safely reached King William's Town, after evading an attempt to stop him at [[Debe Nek]]. Fort Cox was finally relieved on 31 January 1851.<ref name="OnlineMedals"/><ref name="ArmyMuseum"/><ref name="Harington"/><ref name="Saks"/>

==Institution==
Impressed by the showing of the Cape Mounted Riflemen under his command, Smith created Sir Harry Smith's Medal for Gallantry in recognition of their conduct. In later years, when asked who made the best soldiers, Smith put the men of southern France during the [[Battle of Waterloo]] in a class of their own, followed by the Cape's ''[[Khoikhoi|Hottentots]]'' who had, in his opinion, a truly remarkable natural aptitude for soldiering.<ref name="OnlineMedals"/><ref name="ArmyMuseum"/><ref name="Harington"/>

Although the British government initially disapproved of Sir Harry's institution of the medal, it subsequently paid for it and thereby gave it recognition, but not official status. Sir Harry Smith's Medal for Gallantry is regarded by some as the first South African military medal. As an unofficial British medal for valour, it predates the institution of the [[Victoria Cross]] (1856) as well as  the oldest British award for gallantry, the [[Distinguished Conduct Medal]] (1854).<ref name="Unofficial">[http://www.geocities.ws/militaf/un52.htm South African Medal Website - Unofficial Military Awards] (Access date 27 April 2015)</ref><ref name="GoogleBooks">[https://books.google.co.za/books?id=JxEoQKR4tc4C&pg=PA6&lpg=PA6&dq=Sir+Harry+Smith%27s+Medal+for+Gallantry&source=bl&ots=3V-Zucm0Dl&sig=IjLpd8SWId_wEPKF988Vzoo9jZc&hl=en&sa=X&ei=HTY-Vf_SAfGv7Ab5p4DIAQ&ved=0CEgQ6AEwCA#v=onepage&q=Sir%20Harry%20Smith's%20Medal%20for%20Gallantry&f=false The Origins of Gallantry Awards - Sir Harry Smith's Medal for Gallantry] (Access date 27 April 2015)</ref>

==Description==
The medal is a disk, 34 millimetres in diameter and struck in silver. The plain curved bar suspender is attached to the medal through a hole in the top of a claw mount, which is attached to the medal by a pin through the upper edge of the medal.<ref name="OnlineMedals"/>

;Obverse
The obverse depicts a lion beneath a crown of laurel leaves, with the year "1851" in the [[exergue]].<ref name="OnlineMedals"/><ref name="Unofficial"/>

;Reverse
The reverse is plain and is inscribed "PRESENTED BY" around the upper perimeter, "HIS EXCELLENCY", "S<small>IR</small> H.G. SMITH B<small>ART</small> C.G.B." and "TO" in three lines in the centre, with open space for private naming, and "F<small>OR</small> GALLANTRY <small>IN THE</small> FIELD" around the bottom perimeter.<ref name="OnlineMedals"/><ref name="Unofficial"/>

;Ribbon
The ribbon is 32 millimetres wide with 7 millimetres wide brownish red bands, separated by an 18 millimetres wide dark blue band. While the ribbon has been described as being that of the British [[Sutlej Medal]], the dimensions and colour of the bands appear to be different.<ref name="ArmyMuseum"/><ref name="Unofficial"/>

==Recipients==
Around 31 of the medals were awarded to officers and men of the Cape Mounted Riflemen for gallantry in action. Of these, 23 are known and 20 are known to have been privately engraved in various styles with the names of the recipients.<ref name="OnlineMedals"/><ref name="GoogleBooks"/>

The medals known or reputed to have been named are:<ref name="OnlineMedals"/>
* Paul Arendt (depicted).
* Piet Jan Cornelis.
* RSM William Richard Dakins.
* Thomas Dicks.
* Thomas Duncan.
* Sapper R. Dunning, [[Royal Engineers|RE]].
* Henry Evans.
* David Faroe.
* Hendrick Ferara.
* Fundi.
* J. Hassall.
* John Keiburg.
* Lt. Edward Lister-Green.
* John Main.
* H. McKain.
* John McVarrie.
* Francis Meades, [[Cape Mounted Riflemen|CMR]].
* J. Mouatt, [[Cape Mounted Riflemen|CMR]].
* Capt. Skead, [[Royal Navy|RN]].
* Adrian Strauss.

The unnamed medals known or reputed to have been issued are:<ref name="OnlineMedals"/>
* Sgt. Lodewyck Kleinhans.
* Sgt. Appolis Lieuw.
* Sgt. Maj. Johannes Tass.

==Status==
While privately instituted military decorations and medals do not enjoy official status as a result of not having been formally instituted or sanctioned by the [[fount of honour]] at the time, and while none of them were therefore allowed to be worn with military uniform, some have become well-known and have acquired recognition in South Africa's military medal history. Four of these decorations and medals are considered to be significant.<ref name="Unofficial"/><ref name="Legal aspects">[http://www.geocities.ws/militaf/legal.htm South African Medal Website - Legal aspects - Fount of Honour] (Accessed 1 May 2015)</ref>
* '''Sir Harry Smith's Medal for Gallantry''' of 1851.
* The [[Johannesburg Vrijwilliger Corps Medal]] of 1899.
* The [[Kimberley Star]] of 1900.
* The [[Cape Copper Company Medal for the Defence of O'okiep]] of 1902.

==See also==
* [[Harrismith]]
* [[Ladismith]]
* [[Ladysmith, KwaZulu-Natal|Ladysmith]]

==References==
{{Reflist|30em}}
{{South African military decorations and medals}}

[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa pre-1952]]
[[Category:1851 establishments in Africa]]
[[Category:1851 establishments in the British Empire]]