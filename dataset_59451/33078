{{Use mdy dates|date=June 2016}}
{{Infobox information appliance
| name         = PowerBook 100
| title        = 
| aka          = 
| logo         = 
| image        = powerbook 100 pose.jpg
| caption      = PowerBook 100
| developer    = [[Apple Inc|Apple Computer]]
| manufacturer = 
| family       = 
| type         = [[Laptop]]
| generation   = 
| releasedate  = {{Start date and age|1991|10|21}}<ref name="specs"/>
| lifespan     = 
| price        = {{USD|2300|1991}}<ref name="pricespec"/>
| discontinued = {{Start date and age|1992|09|03}}<ref name="specs"/>
| unitssold    = 
| unitsshipped = 
| media        = 
| os           = System [[System 6|6.0.8L]]<ref name="6.0.8L"/><br /> ''[[System 7|7.0.1]]''–[[System 7|7.5.5]]<ref name="specs"/>
| power        = 
| soc          = 
| cpu          = [[Motorola 68000]] 16&nbsp;[[Megahertz|MHz]]<ref name="specs"/>
| memory       = 2 to 8&nbsp;[[Megabyte|MB]]<ref name="specs"/>
| storage      = 
| memory card  =
| display      = 
| graphics     = 
| sound        = 
| input        = 
| controllers  = 
| camera       = 
| touchpad     = 
| connectivity = 
| platform     = 
| service      = 
| dimensions   = 
| weight       = 
| topgame      = 
| compatibility= 
| predecessor  = [[Macintosh Portable]]
| successor    = [[PowerBook 145]]<br>[[PowerBook Duo]] series
| related      = 
| website      = <!--{{URL|example.org}}-->
}}
The '''PowerBook&nbsp;100''' is a portable [[subnotebook]] [[personal computer]] that was manufactured by [[Apple Inc|Apple Computer]] and introduced on October 21, 1991, at the [[COMDEX]] [[computer expo]] in [[Las Vegas, Nevada]].<ref name="debut">{{Citation|title=New Macs headline in Vegas|publisher= MacWEEK|page=2|date=October 22, 1990}}</ref> Priced at [[US Dollar|US$]]2,300, the PowerBook&nbsp;100 was the low-end model of the first three simultaneously released [[PowerBook]]s. Its [[CPU]] and overall speed closely resembled those of its predecessor, the [[Macintosh Portable]]. It had a [[Motorola 68000]] 16-[[megahertz]] (MHz) processor, 2-8&nbsp;[[megabyte]]s (MB) of memory, a {{convert|9|in|cm|adj=on}} [[monochrome]] backlit [[liquid crystal display]] (LCD) with 640&nbsp;&times;&nbsp;400&nbsp;[[pixel]] resolution, and the [[System 7|System 7.0.1]] operating system. It did not have a built-in [[floppy disk drive]] and was noted for its unique compact design that placed a [[trackball]] pointing device in front of the keyboard for ease of use.

Apple's then-chief executive officer (CEO) [[John Sculley]] started the PowerBook project in 1990, allocating $1&nbsp;million for marketing. Despite the small marketing budget, the new PowerBook line was a success, generating over $1&nbsp;billion in revenue for Apple in its first year. [[Sony]] designed and manufactured the PowerBook&nbsp;100 in collaboration with the [[Apple Industrial Design Group]], Apple's internal design team. It was discontinued on September 3, 1992, and superseded by the [[PowerBook 145]] and [[PowerBook Duo]] series. Since then, it has been praised several times for its design; ''[[PC World (magazine)|PC World]]'' named the PowerBook&nbsp;100 the tenth-greatest PC of all time in 2006, and US magazine ''Mobile PC'' chose the PowerBook&nbsp;100 as the greatest gadget of all time in 2005.

==History==
From 1990, John Sculley, then CEO of Apple, oversaw [[product development]] personally to ensure that Apple released new computers to market more quickly. His new strategy was to increase market share by lowering prices and releasing more "hit" products. This strategy contributed to the commercial success of the low-end [[Macintosh Classic]] and [[Macintosh LC]], desktop computers released by Apple in 1990. Sculley wanted to replicate the success of these products with Apple's new PowerBook line.<ref name="carlton">{{Citation|last=Carlton|first=Jim|title=Apple: The Inside Story of Intrigue, Egomania, and Business Blunders|publisher=[[Random House]]|year=1997|pages= 181–190|isbn=0-8129-2851-2}}</ref>

Sculley began the project in 1990 and wanted the PowerBook to be released within one year. The project had three managers: John Medica, who managed engineering for the new laptop; Randy Battat, who was the vice president for product marketing; and Neil Selvin, who headed the marketing effort.<ref name="carlton"/> In 1991, the two leaders in the laptop computer industry were [[Toshiba]] and [[Compaq]], both of which had introduced models weighing less than {{convert|8|lb|kg|2|abbr=on}}.<ref name="carlton"/> Medica, Battat, and Selvin deliberately designed the PowerBook to weigh less than its competitors.<ref name="carlton"/>

Sculley allocated a $1&nbsp;million marketing budget to the PowerBook product line, in contrast to the $25&nbsp;million used to market the [[Macintosh Classic]].<ref name="carlton"/> Medica, Battat, and Selvin used most of the money to produce and air a television commercial that viewers would remember. Advertising agency [[TBWA Worldwide|Chiat/Day]] filmed retired [[Los Angeles Lakers]] basketball star [[Kareem Abdul-Jabbar]] sitting uncomfortably in a small airline coach seat yet comfortably typing on his PowerBook. The ad caption read: "At least his hands are comfortable."<ref name="carlton"/>

Apple unveiled the PowerBook&nbsp;100 on October 21, 1991, at the [[COMDEX|Comdex]] computer expo in Las Vegas, with two other models, the [[PowerBook 140]] and [[PowerBook 170]].<ref name="debut"/> The advertisement and the product were both successful. Apple projected US sales of more than 200,000 PowerBooks in the first year, with peak demand in the first three months of release.<ref>{{Citation|last=Gore|first=Andrew|title=Fall product line on track, but PowerBooks could be scarce
|publisher=MacWEEK|page=2|date=September 24, 1991}}</ref> By January 1992, Apple had sold more than 100,000 PowerBooks, by which time they were in short supply.<ref name="nyt1">{{Citation|last= Pollack|first=Andrew|title=Apple's Net Is Up 10.3% In Quarter|work=[[The New York Times]]|date= January 17, 1992|url=https://query.nytimes.com/gst/fullpage.html?res=9E0CE0DA1238F934A25752C0A964958260&scp=1&sq=PowerBook+100+supply&st=nyt| accessdate=May 10, 2008}}</ref> Apple soon solved the supply problems, and the proceeds from PowerBook sales reached $1&nbsp;billion in the first year after launch. Apple surpassed Toshiba and Compaq as the market leader in worldwide share of portable computer shipments.<ref>Carlton, p. 191</ref> The PowerBook&nbsp;100, 140, and 170 contributed greatly to Apple's financial success in 1992.<ref name="financial success">{{Citation|periodical=[[Macworld]]| volume = 10|issue=1|date= January 1993}}</ref> At the end of the financial year, Apple announced its highest figures yet, $7.1&nbsp;billion in revenues and an increase in global market share from 8% to 8.5%, the highest it had been in four years.<ref name="financial success"/>

However, the initial popularity of the PowerBook&nbsp;100 did not last. Sales decreased, and by December 1991 the 140 and 170 models had become more popular because customers were willing to pay more for a built-in floppy disk drive and second serial port, which the PowerBook&nbsp;100 lacked.<ref name="features">{{Citation|last=Said|first=Carolyn|title=PowerBook 100 slips off U.S. price list. (Apple's Macintosh PowerBook 100 notebook computer)|publisher=[[Macworld]]|date=August 17, 1992|url=http://www.accessmylibrary.com/coms2/summary_0286-9259598_ITM|accessdate=June 4, 2008}}</ref> By August 10, 1992, Apple quietly dropped the PowerBook&nbsp;100 from its price list but continued to sell existing stock through its own dealers and alternative discount consumer-oriented stores such as [[Price Club]]. In these stores, a 4MB RAM/40MB hard drive configuration with a floppy drive sold for less than $1,000 (more than $1,500 less than the similar 2MB/20MB configuration's original list price).<ref name=features/>

On September 17, 1992, Apple recalled 60,000 PowerBook&nbsp;100s because of a potential safety problem.<ref name="nyt2">{{Citation|last=Fisher|first=Lawrence M.|title=60,000 Notebook Computers Are Being Recalled by Apple|work=The New York Times|date=September 17, 1992|url= https://query.nytimes.com/gst/fullpage.html?res=9E0CEEDE1F3CF934A2575AC0A964958260&scp=1&sq=PowerBook+100&st=nyt|accessdate=May 10, 2008}}</ref> An [[short circuit|electrical short]], it was discovered, could melt a small hole in the casing, which occurred in three of the 60,000 notebooks manufactured between October 1991 and March 1992.<ref name="nyt2"/> On the day of the recall, Apple shares closed at $47, down $1.25, but some analysts discounted the recall's importance.<ref name="nyt2"/> In addition, the original power supplies had problems with insulation cracks that could cause a short in a fuse on the [[motherboard]]; and the computer was prone to cracks in the [[power supply|power adapter]] socket on the motherboard, which required a $400 replacement motherboard if the warranty had expired.<ref name="defective">{{Citation|last=Aker|first=Sharon| title=The Macintosh Bible 7th Edition|publisher=[[Peachpit|Peachpit Press]]|year=1998|page=835|isbn =0-201-87483-0}}</ref>

==Features==
Most of the PowerBook&nbsp;100's internal components were based on its predecessor, the [[Macintosh Portable]]. It included a [[Motorola 68000|Motorola 68HC000]] 16&nbsp;MHz processor, had 2&nbsp;MB memory, no [[floppy disk drive]], and cost approximately $2,300.<ref name="pricespec">{{Citation|last=LePage|first=Rick|title=PowerBooks: price-competitive and technologically brilliant|publisher =[[MacWEEK]]|page=54|date=October 22, 1991}}</ref> An external floppy disk drive was available for $279.<ref name="debut"/> The dimensions of the PowerBook&nbsp;100 were an improvement over the Portable. It was {{convert|8.5|in|cm|}} deep, {{convert|11|in|cm}} wide, and {{convert|1.8|in|cm|}} high,<ref name="specs">{{Citation|last=Joannidi|first=Christine|title=Macintosh PowerBook 100: Technical Specifications|publisher=[[Apple, Inc.]]|date=March 14, 2002|url=http://docs.info.apple.com/article.html?artnum=112182|accessdate=May 9, 2008}}</ref> compared to the Portable, which was {{convert|14.83|in|cm}} deep, {{convert|15.25|in|cm}} wide and {{convert|4.05|in|cm}} high.<ref name="portable-specs">{{Citation|last=Joannidi|first=Christine|title=Macintosh Portable: Technical Specifications|publisher=Apple, Inc.|date=March 14, 2002|url=http://docs.info.apple.com/article.html?artnum=112174|accessdate=May 11, 2008}}</ref> Another significant difference was the less expensive [[Super-twisted nematic display|passive matrix]] display used instead of the sharper [[Active-matrix liquid crystal display|active matrix]] used on the Portable (and the 170).<ref name="pricespec"/><ref name="devnote"/> The PowerBook&nbsp;100 included the [[System 7|System 7.0.1]] [[operating system]] as standard, with support for all versions up to [[System 7|System 7.5.5]]. Apple, however, released [[System 6|System 6.0.8L]], which allowed the PowerBook&nbsp;100 to run [[System 6]].<ref name="6.0.8L">{{Citation|title=System 6.0.8L: ReadMe File (8/95)|publisher= Apple, Inc.|date=August 17, 1995|url=http://docs.info.apple.com/article.html?artnum=10863| accessdate=May 3, 2008}}</ref> It could also be used with some earlier System&nbsp;6 versions, although Apple did not officially support this.<ref name="System6">{{Citation|title=PowerBook & Macintosh Classic II: No Support for System 6|publisher=Apple, Inc.|date=November 30, 1994|url= http://docs.info.apple.com/article.html?artnum=9140|accessdate=May 3, 2008}}</ref>

The PowerBook&nbsp;100 had one external serial port, designed for use with a printer or any compatible [[RS-422]] device. It was the first Macintosh to omit an external [[modem]] port,<ref name="AppleSpec">{{Citation|title=AppleSpec pre November 1997|publisher=Apple, Inc.|year=2008|url= http://www.info.apple.com/support/applespec.legacy/index.html|accessdate=May 17, 2008}}</ref> instead offering an optional built-in [[List of device bandwidths#Modems/Home user connections|2400 baud]] modem for communications. As a result, for the first time a user could not print directly and access [[AppleTalk]] or a faster external modem simultaneously,<ref name="serialconfig">{{Citation|title=PowerBook: Internal Modem & Serial Printer Configuration|publisher=Apple, Inc.| date=November 21, 1997|url=http://docs.info.apple.com/article.html?artnum=15650|accessdate= May 17, 2008}}</ref><ref name="oneport">{{Citation|title=PowerBook: Using MacLink Plus With Only One Serial Port (3/95)|publisher=Apple, Inc.|date=March 31, 1995|url=http://docs.info.apple.com/article.html?artnum=16226|accessdate=May 17, 2008}}</ref><ref name="misc">{{Citation|title= PowerBook: Miscellaneous Frequently Asked Questions|publisher=Apple, Inc.|date=November 22, 2002| url=http://docs.info.apple.com/article.html?artnum=19088|accessdate=May 17, 2008}}</ref> and devices such as advanced [[MIDI]] interfaces could not be used because they required the dedicated use of both ports.<ref name="MIDI">{{Citation|title=APPLE NOTES: Acronyms and MIDI|author=Martin Russ| publisher=[[Sound On Sound|Sound On Sound, Ltd.]], Cambridge|date=April 1994|url=  http://www.soundonsound.com/sos/1994_articles/apr94/applenotes.html|accessdate=May 17, 2008}}</ref> A third-party serial modem port could, however, be installed in the internal modem slot for consumers who needed traditional functions.<ref name="modem">{{Citation|title=PB Serial Adapter Provides Full Featured Modem Port for Apple PowerBook 150 and PowerBook 100|publisher=Sigma Seven Systems Ltd.|date=January 1999|url=http://ruby.he.net/~sigma/pbsa.html|accessdate=May 13, 2008}}</ref>

When the computer was not in use, contents of the memory were preserved as long as the main lead-acid battery remained charged.<ref name="devnote"/> The PowerBook&nbsp;100 Power Manager was an [[integrated circuit]], usually placed on the [[logic board]] of a PowerBook,<ref name="pmu">{{Citation|title=PowerBook 100 through PowerBook 5300: Resetting Power Management Unit (PMU)| publisher=Apple, Inc.|date=May 26, 2004|url=http://docs.info.apple.com/article.html?artnum=58416| accessdate=May 11, 2008}}</ref> and was responsible for the power management of the computer.<ref name="pmu"/> Identical to that of the Macintosh Portable,<ref name="devnote"/> it controlled the display's [[backlight]], [[Hard disk drive#Technology|hard drive spin-down]], sleep and wake, battery charging, trackball control, and [[input/output]] (I/O).<ref name="pmu"/> The 100 did add a new feature: 3.5&nbsp;V batteries backed up permanent and expansion [[random access memory]] (RAM) when the PowerBook&nbsp;100's battery was being replaced or when the 100 was otherwise temporarily removed from all power sources.<ref name="devnote">{{Citation|title=Macintosh PowerBook 100 Developer Note |publisher=Apple, Inc. (Developer Technical Publications) |year=1991 |url=http://developer.apple.com/documentation/Hardware/Developer_Notes/Macintosh_CPUs-68K_Portable/PowerBook_100.pdf |format=PDF |accessdate=May 10, 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20040721000003/http://developer.apple.com/documentation/Hardware/Developer_Notes/Macintosh_CPUs-68K_Portable/PowerBook_100.pdf |archivedate=July 21, 2004 }}</ref><ref name="backupbatteries">{{Citation| title=PowerBook 100: Backup Battery Life|publisher=Apple, Inc.|date=May 16, 1994|url= http://docs.info.apple.com/article.html?artnum=11326|accessdate=May 17, 2008}}</ref> This made it a perfect candidate for use with Apple's [[RAM disk]] to help increase battery life by accessing the hard disk less frequently, since the 100 was the only PowerBook that maintained the contents of RAM on shutdown in order to reduce startup time.<ref name="RAM disk">{{Citation|title=PowerBook 100: Creating and Using a RAM Disk(7/93)|publisher=Apple, Inc.|date=July 7, 1994|url= http://docs.info.apple.com/article.html?artnum=12858|accessdate=May 17, 2008}}</ref>

The PowerBook&nbsp;100 was the first PowerBook to incorporate [[Target Disk Mode|SCSI Disk Mode]], which allowed it to be used as an external hard disk on a desktop Macintosh. This provided a convenient method for software to be installed onto the PowerBook or transferred to the desktop, without the need for the 100's optional floppy disk drive. A specialized SCSI cable with a unique connector was required, however, to use any SCSI device on the PowerBook series. A second dedicated cable was required for SCSI Disk Mode.<ref name="devnote"/> This feature was unique to the 100 until Apple introduced new PowerBooks more than a year later.<ref name="SCSI">{{Citation|title= PowerBook: Using SCSI Devices|publisher=Apple, Inc.|date=September 17, 2007|url= http://docs.info.apple.com/article.html?artnum=13611|accessdate=May 13, 2008}}</ref>

There are two versions of the PowerBook&nbsp;100's [[QWERTY]] layout keyboard: a domestic US version with 63&nbsp;keys and an international [[International Organization for Standardization|ISO]] version with 64&nbsp;keys.<ref name="devnote"/> The [[caps lock]] key on the PowerBook&nbsp;100 did not have a locking position or a lighted indicator of its status, and to compensate, the System&nbsp;7 operating system software includes an [[Extension (Mac OS)|extension]] file that causes an icon of the international caps lock symbol (⇪) to appear in the upper right-hand corner of the [[menu bar]]<ref name="devnote"/> when Caps Lock is active.

==Design==
Both the PowerBook&nbsp;140 and 170 were designed before the 100 by the [[Apple Industrial Design Group]], from March 1990-February 1991.<ref name="appledesign"/> The 100's styling was based on those computers and represents the first improvements to the PowerBook line as Apple benefited from the lessons learned in developing the more powerful models' enclosure. The 100 was designed between September and December 1990, and retained the same design elements, which were a variation on the [[Snow White design language]] Apple had been using since 1984. Specifically, {{Convert|2|mm|in|abbr=on}} raised ridges spaced {{Convert|10|mm|in|abbr=on}} apart intended to tie it into the existing product line.<ref name="appledesign">{{Citation|last=Kunkel|first=Paul|title= Appledesign: The publisher of the Apple Industrial Design Group|publisher=Graphis Inc., New York| date=May 1997|page=30|isbn=1-888001-25-9}}</ref>

Apple approached Sony in late 1989 because it did not have enough engineers to handle the number of new products that were planned for delivery in 1991.<ref name="fortune">{{Citation|last=Schlender| first=Brenton R.|title=Apple's Japanese ally. (Sony Corp. designs Apple's PowerBook 100)|publisher=[[Fortune (magazine)|Fortune]]|page=151|date=November 4, 1991}}</ref> Using a basic [[blueprint]] from Apple, including a list of chips and other components, and the Portable's architecture, the 100 was miniaturized and manufactured by [[Sony]] in [[San Diego, California]], and [[Japan]].<ref name="businessweek">{{Citation|last=Rebello|first=Kathy|title=Apple gets a little more help from its friends. (possible alliance with Sony)|publisher=[[BusinessWeek]]|page=132|date=October 28, 1991}}</ref><ref>{{Citation|last=Ely|first=Ed|title=Apple's PowerBook: is it late, or perfectly timed?|publisher=The Business Journal Serving Greater Sacramento|page=19|date=November 25, 1991}}</ref> Sony engineers had little experience building personal computers but nonetheless completed Apple's smallest and lightest machine in under 13&nbsp;months,<ref name="fortune"/> cancelling other projects and giving the PowerBook&nbsp;100 top priority. Sony president [[Norio Ohga]] gave project manager Kihey Yamamoto permission to recruit engineers from any Sony division.<ref name="fortune"/>

[[Robert Brunner]], Apple's head of industrial design at the time, led the design team that developed the laptop, including its trackball and granite color.<ref name="brandweek">{{Citation| last=Lefton|first=Terry|title=Bob Brunner. (marketing successes) (The Marketers of the Year)| publisher=[[Brandweek]]|page=28|date=November 16, 1992}}</ref> Brunner said he designed the PowerBook "so it would be as easy to use and carry as a regular book".<ref name="brandweek"/> The dark granite grey color set it apart from other notebook computers of the time and also from Apple's other products, which traditionally were [[beige]] or platinum grey.<ref name="brandweek"/> The trackball, another new design element, was placed in the middle of the computer, allowing the PowerBook to be easily operated by both left- and right-handed users. The designers were trying to create a [[fashion statement]] with the overall design of the laptop, which they felt made it a more personal accessory, like a wallet or briefcase.<ref name="brandweek"/> Brunner said: "It says something about the identity of the person who is carrying it".<ref name="brandweek"/>

==Reception==
Crystal Waters of ''Home Office Computing'' praised the PowerBook&nbsp;100's "unique, effective design" but was disappointed because the internal modem did not receive faxes, and the 100 had no monitor port.<ref name="homeofficereview">{{Citation|last=Waters|first=Crystal|title=Pack a traveling Mac: PowerBook 100 - Hardware Review|date=February 1992|journal=Home Office Computing}}</ref> The low-capacity 20&nbsp;MB hard drive was also criticized. Once a user's core applications had been installed, little room was left for optional programs and documents.<ref name="homeofficereview"/> Waters concluded: "Having used the 100 constantly in the past few weeks, I know I wouldn't feel cheated by buying it - if only it had a 40MB&nbsp;hard-disk drive option."<ref name="homeofficereview"/>

''[[eWeek|PC Week]]'' [[benchmark (computing)|benchmark]]ed the PowerBook&nbsp;100, measuring it against its predecessor, the [[Macintosh Portable]]. The PowerBook&nbsp;100 took 5.3&nbsp;seconds to open a [[Microsoft Word]] document and 2.5&nbsp;seconds to save it.<ref name="pcweek">{{Citation| last=Bethoney|first=Herb|title=Lightweight PowerBooks live up to their name|publisher=[[eWeek|PC Week]]|page=12|date=October 21, 1991}}</ref> The Portable took 5.4 and 2.6&nbsp;seconds respectively.<ref name="pcweek"/> ''PC Week'' tested the battery life, which delivered 3&nbsp;hours 47&nbsp;minutes of use.<ref name="pcweek"/> ''[[Byte (magazine)|Byte magazine]]''{{'}}s review concluded, "The PowerBook&nbsp;100 is recommended for word processing and communications tasks; the higher-end products offer enough power for complex reports, large spreadsheets and professional graphics."<ref name="bytereview">{{Citation|last=Thompson|first=Tom|title=Apple reinvents the notebook. (Hardware Review) (Apple Macintosh PowerBook 100, 140, 170)|publisher=[[Byte (magazine)|Byte]]|page=253|date=March 1992}}</ref> ''[[MacWEEK]]'' described it as "ideal for writers and others on a tight budget."<ref name="macweekreview">{{Citation|last=Ford|first=Ric| title=Talkin' about a Mac revolution: PowerBooks represent a big change for Mac computing, opening new doors as the first truly mobile Macs|publisher=MacWEEK|page=3|date=January 6, 1992}}</ref>

The PowerBook&nbsp;100 continues to receive recognition from the press. ''[[PC World (magazine)|PC World]]'' named the PowerBook&nbsp;100 the 10th-greatest PC of all time in 2006,<ref>{{Citation| title=The 25 Greatest PCs of All Time|publisher=[[PC World (magazine)|PC World]]|date=August 11, 2006|url=http://www.pcworld.com/article/126692-7/the_25_greatest_pcs_of_all_time.html|accessdate= August 9, 2008}}</ref> and in 2005, US magazine ''Mobile PC'' chose the PowerBook&nbsp;100 as the greatest gadget of all time, ahead of the [[Sony Walkman]] and [[Atari 2600]].<ref>{{Citation|title= Apple laptop is 'greatest gadget'|publisher=[[BBC|BBC News]]|date=February 22, 2005|url= http://news.bbc.co.uk/1/hi/technology/4284501.stm|accessdate=May 11, 2008}}</ref> The PowerBook&nbsp;100 received multiple awards for its design, including the 1999 IDSA Silver Design of the Decade Award, ''Form'' magazine's 1993 Designer's Design Awards, the 1992 ISDA Gold Industrial Design Excellence Award, the 1992 Appliance Manufacturer Excellence in Design award, and the Industry Forum Design 10 Best - Hannover Fair award.<ref>{{Citation|title=Complete Award Listing (1986–2008)|publisher=Lunar Design|url=http://www.lunar.com/inside/awards2.html|accessdate= May 11, 2008}}</ref>

==Specifications==
{| class="wikitable"
|-
! Component
! Specification<ref name="specs"/>
|-
|[[Computer display|Display]]
|{{convert|9|in|cm|adj=on}} [[monochrome]] [[Super-twisted nematic display|passive matrix (FSTN)]]<ref name="devnote"/> [[Liquid crystal display|LCD]] (backlit) display, 640 × 400&nbsp;pixel resolution
|-
|[[Computer Storage|Storage]]
|20–40&nbsp;[[megabyte|MB]] [[SCSI]] [[hard disk drive]] internal; external 3.5" [[Superdrive|floppy disk drive]] (optional)
|-
|[[Central processing unit|Processor]]
|16-[[MHz]] [[Motorola 68000]]
|-
|[[Bus (computing)|Bus speed]]
|16&nbsp;[[MHz]]
|-
|[[Random access memory]]
|2&nbsp;[[Megabyte|MB]], expandable to 8&nbsp;MB using 100&nbsp;ns [[SIMM]]s and optional custom RAM-slot expansion card
|-
|[[Read-only memory]]
|256&nbsp;[[Kilobyte|KB]]
|-
|Networking
|[[AppleTalk]], optional [[modem]]
|-
|[[Battery (electricity)|Battery]]
|2½–3¾ hour 7.2V sealed [[lead acid]] rechargeable battery<ref name="pcweek"/><br />3.5-[[volt]] lithium [[Backup battery|backup batteries]]<ref name="devnote"/>
|-
|Physical [[dimensions]]
|8.5&nbsp;in D &times; 11&nbsp;in W &times; 1.8&nbsp;in H (22 &times; 28 &times; 4.6&nbsp;cm)<br /> {{convert|5.1|lb|kg|2|abbr=on}}
|-
|Port connections
|1 × [[Apple Desktop Bus|ADB]] ([[Apple keyboard|keyboard]], [[Apple mouse|mouse]])<br />1 × [[mini-DIN]]-8 [[RS-422]] [[serial port]] (printer/modem, [[AppleTalk]])<br />1 × HDI-20 (ext. floppy drive)<br />1 × [[SCSI connector#First Generation|HDI-30 connector]] [[SCSI]] (ext. hard drive, scanner)<br />1 × 3.5&nbsp;mm headphone jack socket
|-
|[[Operating system]]
|System [[System 6|6.0.8L]], ''[[System 7|7.0.1]]''–[[System 7|7.5.5]]
|-
|Expansion slots
|1 &times; serial [[modem]]
|-
|Audio
|8-bit [[Monaural|mono]] [[Sampling rate|22 kHz]]
|-
|[[Gestalt (Mac OS)|Gestalt ID]]
|24
|-
|[[Code name]]
|''Elwood'', ''Jake'', ''O'Shanter & Bess'', ''Asahi'', ''Classic'', ''Derringer'', ''Rosebud'',<ref>{{Citation|last=Linzmayer|first=Owen W|title=[[Apple Confidential]] (1st Edition)|publisher= [[No Starch Press]]|year=1999|page=30|isbn=1-886411-31-X}}</ref> and ''Sapporo''<ref name="appledesign"/>
|}

{{Timeline of portable Macintoshes}}

==See also==
*[[Macintosh Portable]]
*[[List of Macintosh models by case type]]
*[[List of products discontinued by Apple Inc.]]
*[[List of Macintosh models grouped by CPU type]]

==References==
{{reflist|30em}}

==External links==
*[http://support.apple.com/kb/HT1752 Vintage and obsolete products] from Apple.com
*[http://lowendmac.com/1991/powerbook-100/ PowerBook 100] on Low End Mac
*[http://www.everymac.com/systems/apple/powerbook/specs/mac_powerbook100.html PowerBook 100 Specs] on Everymac.com
*[http://apple-history.com/100 PowerBook 100] on Apple-History.com

{{Apple hardware before 1998}}

{{featured article}}

{{DEFAULTSORT:Powerbook 100}}
[[Category:68k Macintosh computers]]
[[Category:Macintosh laptops|100]]
[[Category:PowerBook|100]]
[[Category:1991 introductions]]