{{Infobox journal
| discipline = [[Neuroimaging]], [[functional neuroimaging]]
| cover =
| editor = Peter T. Fox, Jack L. Lancaster
| publisher = [[John Wiley & Sons]]
| country =
| abbreviation = Hum. Brain Mapp.
| frequency = Monthly
| history = 1993-present
| impact = 5.969
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0193
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0193/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0193/issues
| link2-name = Online archive
| ISSN = 1065-9471
| eISSN = 1097-0193
| OCLC = 474770986
| LCCN = 94648368
| CODEN = HBMAEE
}}
{{Portal|Neuroscience}}
'''''Human Brain Mapping''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by [[John Wiley & Sons]] covering research on human [[brain mapping]].

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.969, ranking it second out of 14 journals in the category "Neuroimaging",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Neuroimaging |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref> fifth out of 125 journals in the category "Radiology Nuclear Medicine & Medical Imaging",<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Radiology Nuclear Medicine & Medical Imaging |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |accessdate= |series=Web of Science |postscript=.}}</ref> and 27th out of 252 journals in the category "Neuroscience".<ref name=WoS3>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Neuroscience |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |accessdate= |series=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0193}}

[[Category:Neuroimaging journals]]
[[Category:Monthly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Publications established in 1993]]
[[Category:English-language journals]]