{{Infobox Officeholder
| name                = Leovigildo B. Banaag
| image               = 
| imagesize           = 
| smallimage          =
| caption             = 
|-
| order               = 
| office              = Member of the [[House of Representatives of the Philippines|Philippine House of Representatives]] from  [[Legislative districts of Agusan del Norte|Agusan del Norte's First District]]
| term_start          = June 30, 1998
| term_end            = June 30, 2007
| succeeding          = 
| predecessor         = Charito B. Plaza
| successor           = [[Jose Aquino II]]
|-
| birth_date          = {{birth date|mf=yes|1943|12|31}}
| birth_place         = [[Butuan]], [[Commonwealth of the Philippines]]
| death_date          = {{death date and age|mf=yes|2011|01|06|1943|12|31}}
| death_place         = [[Davao City]], [[Philippines]]
| nationality         = Filipino
| party               = [[Lakas-Kampi-CMD]]
| spouse              = Pearl Famador (deceased)
| relations           =
| children            = Trisha Ysobelle, Jan Gilbert
| residence           = Butuan City, Agusan del Norte
| alma_mater          = [[San Beda College]]
| occupation          = 
| profession          = [[Politician]], [[Lawyer]]
| religion            = Roman Catholic
| signature           =
| website             = 
| footnotes           =
}}

'''Leovigildo Briones Banaag''' (December 31, 1943 – January 6, 2011) was a lawyer and a Congressman in the Philippine House of Representatives for 3 consecutive terms from 1998 to 2007.

==Early life and career==
Banaag was born on December 31, 1943 in Butuan City. He graduated Cum Laude in both Bachelor of Arts in 1964 and Bachelor of Laws in 1968 at San Beda College. He was admitted to the Integrated Bar of the Philippines in March 14, 1969 and has been a law practitioner ever since. Married to Pearl Famador(†), they had two children, Trisha Ysobelle and Jan Gilbert.<ref>{{cite web|url=http://www.i-site.ph/Databases/Congress/13thHouse/personal/banaag-personal.html |title=Banaag, Leovigildo &#124; Personal Information |publisher=I-site.ph |date=1943-12-31 |accessdate=2012-01-15}}</ref><ref>{{cite web|url=http://philippinelaw.info/filipino-lawyers-directory/leovigildo-b-banaag |title=Profile of Atty. Leovigildo B. Banaag |publisher=PhilippineLaw.info |date=1969-03-14 |accessdate=2012-01-15}}</ref>

Banaag began his career in politics in 1980 when he was elected as a Provincial board member of Agusan del Norte. Since then he had held several positions, as a Councilor of Butuan City (1988–1995) and then Vice Mayor of Butuan City (1995–1998), while holding other positions as well.<ref>http://www.congress.gov.ph/download/cv/13th/banaagcv.pdf</ref>

==House of Representatives==
After just one term as Vice Mayor of Butuan City, Banaag was elected as District Representative for the First District of Agusan del Norte in 1998, serving Butuan City and Las Nieves, Agusan del Norte. He was a Chairman of the House Committee on Natural Resources, as well as a Member of several other House Committees, including Agrarian Reform, Ethics, Good Government, and Ways and Means, among others.<ref>{{cite web|url=http://www.i-site.ph/Databases/Congress/13thHouse/political/banaag-political.html |title=Banaag, Leovigildo &#124; Political Information |publisher=I-site.ph |date= |accessdate=2012-01-15}}</ref>

==Death==
Leovigildo Banaag died of [[cardiac arrest]], aged 67, at [[Davao Doctors Hospital]], Davao City on January 6, 2011.<ref>[http://www.butuan.gov.ph/home/former-congressman-banaag-posthumously-honoured.html Former Congressman Banaag posthumously honored.]</ref>

==References==
{{reflist}}

{{s-start}}
{{s-par|ph-lwr}}
{{succession box |
  before= Charito B. Plaza |
  title=[[Legislative districts of Agusan del Norte#1st District|Representative, 1st District of Agusan del Norte]] |
  years= June 30, 1998 &ndash; June 30, 2007|
  after= [[Jose Aquino II]]
}}
{{s-end}}

{{DEFAULTSORT:Banaag, Leovigildo B.}}
[[Category:People from Agusan del Norte]]
[[Category:1943 births]]
[[Category:2011 deaths]]
[[Category:Members of the House of Representatives of the Philippines from Agusan del Norte]]
[[Category:San Beda College alumni]]
[[Category:People from Butuan]]

{{Philippines-politician-stub}}