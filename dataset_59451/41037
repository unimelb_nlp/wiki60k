[[File:Torbreck Home Units.jpg|thumb|Torbreck Home Units (Constructed 1957-60; Photograph taken 2012)]]
The '''Torbreck Home Units''' were the first high-rise and mix-use residential development in [[Queensland]], Australia. These [[heritage-listed]] home units are located at 182 Dornoch Terrace, [[Highgate Hill, Queensland|Highgate Hill]], [[Brisbane]].  Designed by architects Aubrey Job and Robert Froud (Job and Froud Architects), construction began in 1957 and was completed three years later in 1960. The project acquired the name 'Torbreck' to recognise a small, gabled timber cottage that previously occupied the site (called Torbreck).<ref>{{cite web|last=Wilson|first=Andrew|title=Life Cycle: Torbreck|url=http://www.australiandesignreview.com/features/18511-life-cycle-torbreck|work=Architect|publisher=Australian Design Review|accessdate=03/10/12}}</ref>

== Description ==

Torbreck is a soft-[[Modernist architecture|modernist]] high-rise building, located between Dornoch Terrace and Chermside Street, on approximately 6,521m2 (70,191.5 sq. feet) of land. The building itself covers approximately 1,749.15m2 of land (18,827.7 sq. feet), with 150 individual [[Unit (housing)|units]], totalling 19,892.78m2 in floor area.<ref name="8i">{{cite news|last=Kai|first=Charmaine|title=Subtropical Design Multi-Unit Dwelling Case Study: Torbreck Home Units|url=http://www.8i.net.au/Documents/TorbreckCaseStudy.pdf|accessdate=07/10/12|newspaper=Centre for Subtropical Design|year=2006}}</ref>  It consists of two adjoining components: an eighteen storey, south-facing Tower Block, and a long, narrow, eight storey north-facing Garden Block. The former features panoramic views of Brisbane city, while the latter overlooks a landscaped garden and communal swimming pool.<ref name="8i" /> The immediate surrounds are well vegetated, with the plants providing shade for the outdoor living areas and car park. Each unit in Torbreck has at least one balcony, which include planter boxes and glass balustrades. Torbreck is constructed from concrete and brick, with expressive detailing in brickwork, stone and timber, to address the domestic scale.The development was originally designed to provide social and public amenity with a mix-used ground floor, having facilities such as shops and restaurants, these were never built.<ref name="Ferguson1997">{{cite book|last=Ferguson|first=Susan|title=The Residential Design Legacy of R. P. Froud (Thesis)|year=1997|publisher=The University of Queensland|location=Brisbane, Australia}}</ref>

=== Climatic design ===

Job and Froud saw it important that each apartment carefully considered privacy, outlook and proper orientation. These principles, coupled with the trend towards [[Passive solar building design|passive climatic design]] that was popularised in Queensland by [[Karl Langer]], resulted in a design that strongly incorporated passive design principles.<ref name="Ferguson1997" />  As well as informing Torbreck's planning, technologies such as the innovative aluminium vertical blade sun shading system were incorporated to manage breezes, solar heat and glare impact and maximise the use of natural light within the building. The significance and attention given to external garden spaces further demonstrates the significance of the climate and place of Brisbane as design drivers.<ref name="8i" />

== Construction ==

=== Origins ===

The design project and planning began in 1957 at the request of Rowley Pym and progressed quickly due to his enthusiasm. The capital used to fund construction was drawn from pre-sales of units, prompting an unprecedented level of media attention to the tower. The project was considerably rushed, encountering many financial difficulties and administrative errors. Torbreck Kratzman Pty Ltd., the company formed to build the apartments declared bankruptcy prior to the construction of the tower block, and the project was sold to Reid Murray Developments in 1959, which consequently went into liquidation before the project was completed.<ref name="Ferguson1997" />

=== Construction techniques ===

Both blocks demonstrate a different approach to concrete construction, the Tower block being a relatively standard example of a reinforced concrete structure of the late 1950s, while the Garden block employed the new ‘[[lift slab construction]]' system, the first of its kind in Queensland. The term lift-slab defines a style of construction in which each floor, and the roof are [[pre-fabricated]] on the ground in a stack and raised into position along vertical wall supports.<ref>{{cite news|title=Novel Building Method for Torbreck|url=http://torbreck.com.au/pdf/Sept_10_1958_2.pdf|accessdate=05/10/2012|newspaper=Telegraph (Brisbane)|date=10/09/1958}}</ref>  The external walls are cavity brick and provide an aesthetically domestic cladding for the building, as well as providing insulative properties. Internal walls dividing apartments were of a double brick construction to reduce sound transfer, while walls within apartments were of a non-load bearing nature to allow for simple future renovations.<ref name="8i" />Different room configurations were also made possible off the plan. Provisions were made for television points (TV was relatively new) and each apartment had a gas clothes drying cupboard and the building boasted a filtered water supply via rooftop tanks. A garbage chute was available on every floor in both towers and rubbish was originally incinerated via a basement incinerator on level B1 which was ignited at night time so that the smoke out of the tower block rooftop was less visible. The building used to ( and may still have) a postal box in the lobby in order to mail letters and mail is delivered to each floor by Australia Post rather than to central mailboxes in the lobby as with modern buildings. The rooftop observation deck, a common area for all residents, was originally designed to be a cocktail lounge however these plans never materialised. In order to purchase an apartment off the plan, an initial deposit was required and then scheduled progress payments fell due at certain points during the construction until the balance was due upon completion. One monthly fee per apartment covers council rates, water and association/ maintenance and administration fees. Potential buyers could also see their potential view via helicopter at one stage during construction. Apparently there was little security in the early years and Highgate Hill locals used to walk through the lobby off Dornoch Tce and exit on Chermside St via the Garden Block lift as a short cut. The original Tower Block internal lift doors contained a small rectangular window through which you could see the various floors, concrete and brickwork pass by as you travelled between floors.  

== Media coverage ==

Though initially Torbreck was only designed so far as a single perspective sketch, the project drew headlines, due to intense publicity efforts of developer Row Pimley.<ref>{{cite news|title=£750,000 Block of "Home Units"|url=http://torbreck.com.au/pdf/Oct_27_1957.pdf|accessdate=05/10/12|newspaper=Sunday Mail|date=1957-10-27}}</ref>  As the first high-rise residential project in Queensland, Torbreck received a considerable amount of public interest and media coverage throughout its inception and construction. It made several headlines from October 1957, to 1960, when construction was completed and residents began moving in. Public response and interest in the project was greater than anything earlier in Brisbane's history.<ref name="Ferguson1997" />

==Heritage listing==
Torbreck was added to the [[Queensland Heritage Register]] on 17 December 1999.<ref name=qhr>{{cite QHR|16019|Torbreck|601256|accessdate=10 January 2014}}</ref>

== See also ==
{{Portal|Queensland}}
*[[Architecture of Australia]]

== References ==
{{Reflist|33em}}

== Further reading ==

*Ferguson, Susan (1997). The Residential Design Legacy of R. P. Froud (Thesis). St. Lucia, Australia: The University of Queensland.
*De Gruchy, Graham (1988). Architecture in Brisbane. Bowen Hills, Australia : Boolarong Publications with Kookaburra Books.

== External links ==
*[http://torbreck.net.au/ Torbreck Home Units]
*[http://www.yourbrisbanepastandpresent.com/2010/03/torbreck.html Torbreck (Your Brisbane: Past and Present)]
*[http://www.8i.net.au/Documents/TorbreckCaseStudy.pdf Subtropical Design Multi-Unit Dwelling Case Study: Torbreck Home Units]
*[http://2.bp.blogspot.com/_WfG4A8PQTAs/S243mXzoxTI/AAAAAAAAAbY/2il6jzo3EKQ/s1600-h/Torbreck_drawing.jpg Persepective image of Torbreck]
*[http://archipelagoarchitects.com.au/projects/torbreck Sample contemporary project within Torbreck Home Units]
*[http://torbreck.com.au/intro.html# Torbreck: Brisbane]
*[http://www.library.uq.edu.au/fryer/ms/uqfl426.pdf Torbreck Home Units collection at the University of Queensland, UQFL426]

{{coord|-27.48701|153.01792|display=title}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box.  Just press "Save page". -->

[[Category:Apartment buildings in Brisbane]]
[[Category:Residential skyscrapers in Australia]]
[[Category:Modernist architecture in Australia]]