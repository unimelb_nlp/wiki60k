{{Infobox biography
| name            = May Dugas de Pallandt van Eerde
| image           =
| caption         = 
| birth_name      = May Dugas
| birth_date      = 23 May 1869
| birth_place     = Chicago, Illinois
| death_date      = 10 March 1937 (aged 67)
| death_place     = New York, NY
| nationality     = 
| alias           = Pauline Davidson, Pauline Townsend, Maude Jackson
| occupation     = Courtesan, Businesswoman, Blackmailer/Extortionist
| parents         = Sopnie and Eugene Dugas 
| weight          = 
| spouse          = Rudolph de Pallandt van Eerde
| child           = John Andrew van Eerde
}}
''' May Dugas de Pallandt van Eerde ''' (23 May 1869 to 10 March 1937) is reputed to have earned an estimated $2 million from blackmail schemes and various business ventures during her life.<ref name="Chicago Daily Tribune">Wendt, Lloyd. [http://pqasb.pqarchiver.com/chicagotribune/doc/177179852.html?FMT=ABS&FMTS=ABS:AI&type=historic&date=Nov+10%2C+1946&author=Wendt%2C+Lloyd&pub=Chicago+Daily+Tribune+%281923-1963%29&edition=&startpage=&desc=QUEEN+OF+THE+Blackmailers "Queen of the Blackmailers"] series, Chicago Daily Tribune, November 10, 1946, p. B8-B9; November 24, 1946; December 1, 1946; December 8, 1946; December 15, 1946; December 29, 1946; January 5, 1947; January 12, 1947; January 19, 1947; January 26, 1947.
</ref> She was sued by long-time friend Frank Gray Shaver for extortion<ref name="NY Times">"Baroness Accused of $125,000 Swindle," The New York Times, January 10, 1917.</ref><ref name="Charges of Fraud">"Baroness Answers Charges of Fraud," The New York Times, January 13, 1917.</ref><ref name="Callow">Callow, Janet. Baroness May de Pallandt van Eerde. Presentation for the Upper Peninsula Michigan History Conference, Menominee, MI, June 2010.</ref><ref name="Menominee">"Baroness is Sued for $100,000," Menominee Herald-Leader, September 16, 1916.</ref> and pursued by [[Pinkerton Government Services|Pinkerton]] detectives for numerous blackmail, defrauding, and extortion incidents.<ref name="Chicago Daily Tribune" />

The Pinkerton detective who hunted Dugas across three continents told the story of her blackmail and extortion cons to Lloyd Wendt, reporter for the [[Chicago Tribune|Chicago Daily Tribune]] in the 1940s. Wendt published that report in a series of Tribune articles running from late 1946 to early 1947.<ref name="Chicago Daily Tribune" /> Dugas herself did not leave any written record of her life.



== Early life ==

She was born May Dugas on May 23, 1869, in the Chicago area.<ref name="Callow" /> Her French-Canadian immigrant parents, Eugene and Sophie, had two other children, older son Paul and younger son Eugene. At the time of Dugas’s birth the family was living in the Chicago area but by 1880 had moved to Muskegon, Michigan.<ref name="Callow" /> Dugas’s father worked there as a saloonkeeper and died between 1883 and 1885.<ref name="Chicago Daily Tribune" /> After his death the family moved to [[Menominee, Michigan]], where Dugas graduated from high school.<ref name="Chicago Daily Tribune" /> By all reports she was a precocious student.<ref name="Chicago Daily Tribune" />

In 1887, when Dugas was 18, she left Menominee for Chicago.<ref name="Callow" /> There is no record of her early months in Chicago but within a year of her arrival she took up residence at the city’s most infamous [[Brothel|bordello]], Carrie Watson’s at 441 South Clark Street.<ref name="Chicago Daily Tribune" /><ref name="Kendall">Kendall, Todd D. "Carrie Watson—Come in, Gentlemen." The Chicago Crime Scenes Project. 	Retrieved September 9, 2009, from http://chicagocrimescenes.blogspot.com/2009/01/carrie-	watson-come-in-gentlemen.html</ref> She also enrolled in some university courses and studied psychology, French, and business law.<ref name="Chicago Daily Tribune" /> She managed to attract the attentions of a young man from a wealthy family, and he proposed marriage.<ref name="Chicago Daily Tribune" /> But the young man’s father became suspicious and hired a Pinkerton detective to investigate her. Detective Edwards (this name appears to be an alias) set up a trap, inviting her to participate in a scheme to earn "millions in fake copper mine stocks."<ref name="Chicago Daily Tribune" /> When she fell for the bait he upended the engagement. But she extracted $20,000 from the father in exchange for a promise to save his son’s reputation by not divulging the engagement.<ref name="Chicago Daily Tribune" />

Dugas next traveled to New York and then to [[Portland, Oregon]], where she took up residence at another well-known bordello at that time, Emma Wingard’s.<ref name="Chicago Daily Tribune" /> During her residence there she met the woman who became her companion and accomplice for a short time, Fanny Lisle.<ref name="Chicago Daily Tribune" /> Dugas and Lisle left Portland and, around 1889, journeyed to San Francisco.<ref name="Chicago Daily Tribune" /> Lisle found work at Lillie Winter’s bordello in [[North Beach, San Francisco|North Beach]]. Meantime, Dugas entered into a romantic liaison with a Guatemalan coffee dealer, frequenting the Palm Court, Poodle Dog, and Cliff House with him.<ref name="Chicago Daily Tribune" /> He put her up in an apartment on Powell Street, and she and Lisle formulated a plan to rob him of his money.<ref name="Chicago Daily Tribune" /> However, their plan went awry and they were arrested in 1890<ref name="District Attorney">San Francisco Municipal Reports, District Attorney’s Report, Cases reported pending July 1, 1892 against defendants fugitives from justice.</ref> and held in the San Francisco jail on charges of larceny.<ref name="Chicago Daily Tribune" /> They managed to break out of jail and flee the country.<ref name="Chicago Daily Tribune" /><ref name="Article 2">"Article 2, no title…The police here connect Baroness de Pallandt," The New York Times, January 22, 1914.</ref> In 1892, San Francisco Municipal Reports identified them as fugitives from justice.<ref name="District Attorney" />

Dugas and Lisle journeyed to [[Shanghai]], where they parted ways. Dugas met a British mining executive there and accompanied him to Hong Kong, where she blackmailed him for $25,000.<ref name="Chicago Daily Tribune" />



Dugas later traveled to Tokyo and took up residence at the Imperial Hotel. There she met a young John Kilpatrick. They embarked on an affair, and Kilpatrick delayed his return home to New York. His family became concerned about his ever-lengthening travels and dwindling funds and hired a Pinkerton detective to travel to Tokyo to determine what was delaying him.<ref name="Chicago Daily Tribune" /> Upon discovering that May Dugas was the source of Kilpatrick’s delay, Detective Edwards used what leverage he could, primarily his knowledge of Dugas’s life as a prostitute, to persuade her to release Kilpatrick from her clutches.<ref name="Chicago Daily Tribune" /> Dugas did leave Kilpatrick, who returned to New York, only to commit suicide a few months later.<ref name="Kilpatrick">"J.D. Kilpatrick’s Suicide," The New York Times, September 22, 1903: 14.</ref> One of the coroners on the case suggested that it was a matter of homicide, not suicide,<ref name="Kilpatrick's Suicide">"Kilpatrick Death Mystery," The New York Times, September 23, 1903: 14</ref> and another source asserted that heartbreak over Dugas was the reason for his suicide.<ref name="Love">"Love for Baroness Cause of Kilpatrick’s Suicide," Menominee Herald, October 3, 1903.</ref>

Dugas herself left Tokyo after breaking with Kilpatrick, first traveling to New York. There she advertised for and hired the woman who was to become her long-time companion and accomplice, Belle (Daisy) Andrews.<ref name="Chicago Daily Tribune" /> Around 1890 they left New York for London.<ref name="Chicago Daily Tribune" />

=== Life as Baroness van Eerde ===

[[File:Kasteel Eerde.jpg|framed|left|Castle Eerde c. 2011]]

It was in London that Dugas met Rudolph de Pallandt van Eerde.<ref name="Chicago Daily Tribune" /><ref name="Callow" /> He pursued her and they were married November 20, 1892, at his estate in the Netherlands. Dugas, at age 23, became a baroness.<ref name="Chicago Daily Tribune" /> She and her husband split their time between his Dutch country estate, Castle Eerde, and London. While in London the Baroness enjoyed life in the circles of high society and royalty. Her husband was master of hounds for the Princess of Wales and brother-in-law of Baroness Greenings van McEllen, lady-in-waiting to the Dowager Queen of the Netherlands.<ref name="Callow" /> Sometime around 1900 the Baroness and her assistant, Daisy, left London for the United States.

In 1899, the Baron became disenchanted with the Baroness’s long absences and began to press her for a divorce.<ref name="Chicago Daily Tribune" /> She was reluctant to grant a divorce even though they never again resided together.<ref name="Chicago Daily Tribune" /> In November 1899 the Baron and Baroness signed a deed of separation in Antwerp, which defined the financial terms of their legal separation.<ref name="Baron">"Baron and Baroness," New Zealand Herald, Vol. XXXVIII, Issue 11650, 11 May 1901, accessed online at http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&d=NZH19010511.2.82.22.</ref> The Baron agreed to pay her £6000 when the deed was signed.<ref name="Baron" /> According to one report <ref name="Chicago Daily Tribune" /> Dugas inherited a small fortune at the time of the Baron’s death. She maintained her title as Baroness until her death.<ref name="Callow" />

=== Life in the U.S. and liaison with Frank Shaver ===

The Baroness met Miss Frank Gray Shaver, an attorney living in the Chicago area, in 1901 and they became friends and traveling companions.<ref name="Chicago Daily Tribune" /><ref name="Callow" />



Around 1901, acting on a tip about a racetrack soon to be built outside [[Hot Springs, Arkansas]], the Baroness traveled there and bought the Potash Sulphur Springs Hotel.<ref name="Chicago Daily Tribune" /> She spent a good amount of time and money renovating the hotel and sold it for a tidy profit prior to the opening of the racetrack in 1903.<ref name="Chicago Daily Tribune" /> Shaver acted as her legal advisor on matters related to this real estate deal.

Around 1902 Shaver became engaged to Dugas’s younger brother, Gene, but Shaver grew increasingly uncomfortable with his habit of borrowing money from her and broke off the engagement around 1903.<ref name="Chicago Daily Tribune" /><ref name="NY Times" /><ref name="Callow" /><ref name="Menominee" /> This led to a falling out between the Baroness and Frank Shaver, who suspended their friendship until 1912.<ref name="Miss Shaver">"Miss Shaver Takes Stand," Menominee Herald-Leader, January 23, 1917.</ref>

=== Life in London with Ernest Appleby ===

The Baroness met Ernest Appleby on a train in 1903, four years after she and the Baron separated.<ref name="Chicago Daily Tribune" /><ref name="Callow" /><ref name="American">"American’s Suit Against Baroness," The New York Times, January 21, 1914.</ref>  He made an offer of marriage that she declined, although the two began to live together as husband and wife<ref name="Amazing" /> and she represented herself as Mrs. Appleby.<ref name="Chicago Daily Tribune" /> In 1906 they took up residence on the outskirts of London, at Bray Lodge in Maidenhead on the Thames.<ref name="Callow" /><ref name="Amazing">"An Amazing Romance," Otautau Standard and Wallace County Chronicle, March 24, 1914: 2.</ref> Appleby enjoyed gambling, and he and the Baroness took several took trips together to Monte Carlo, where he was so successful that one year he won $50,000 at the gambling tables there.<ref name="American" /><ref name="Amazing" /> On one of these Monte Carlo trips he purchased an expensive pearl necklace for the Baroness, which later became the subject of dispute.<ref name="American" /><ref name="Amazing" /><ref name="Baroness Sued">"Baroness Sued: Is Ill Here," The New York Times, June 11, 1913.</ref><ref name="Sues">"Sues on Gift to Baroness," The New York Times, January 14, 1914.</ref> Appleby claimed he had only loaned her the money for this, but she said he had gifted the piece to her.<ref name="Amazing" /> Appleby sued her in 1914 for $20,000.<ref name="American" /> The Baroness collapsed during the trial after being confronted about her arrest in San Francisco in 1890.<ref name="Article 2" /><ref name="Baroness Collapses">"Baroness Collapses," The New York Times, January 22, 1914.</ref> The trial was adjourned due to her "sudden illness"<ref name="Baroness Collapses" /> and settled out of court in April 1914.<ref name="Pallandt Suit">"Pallandt Suit Settled," The New York Times, April 23, 1914.</ref> The settlement terms were not disclosed.<ref name="Pallandt Suit" />

In 1912 the Baroness sued Lloyd’s Insurance Company and Gresham Insurance Company for payment on a black pearl and diamond brooch which she claimed was lost at the Shakespeare Ball in London.<ref name="Black Pearl">"Black Pearl Case Settled," The New York Times, February 2, 1912.</ref><ref name="Brooch">"Suit over $15,000 Brooch," The New York Times, February 1, 1912.</ref><ref name="Value">"Value of Black Pearls," The New York Times, February 18, 1912.</ref> Lloyd’s threatened to put her on the stand "to make a full statement of her life."<ref name="Black Pearl" /> The suit was settled out of court in February 1912, and terms were not disclosed.<ref name="Black Pearl" />

== Menominee Trial==

In 1912, when Miss Shaver’s father died, the Baroness sought her out and reestablished the friendship.<ref name="Chicago Daily Tribune" /><ref name="Callow" /> The Baroness asked Shaver to accompany her to Menominee and invest a half-interest in the family’s Menominee home, which Shaver agreed to do.<ref name="Chicago Daily Tribune" /> Shaver took up residence in the Menominee home and oversaw some improvements in the home.<ref name="Miss Shaver" /> However, she quickly ran through the inheritance from her father and asked that the Baroness repay all the money she had loaned her over the years, funds totaling some $100,000.<ref name="NY Times" /><ref name="Miss Shaver" /> But the Baroness claimed that she did not owe Miss Shaver any money and that Miss Shaver had signed a release absolving her of any indebtedness.<ref name="Charges of Fraud" /><ref name="Very">"Very Ill When She Signed, is Claim in Suit," Menominee Herald-Leader, January 26, 1917.</ref> Shaver, seeing no other recourse, sued the Baroness for extortion.<ref name="Menominee" /> The trial took place in Menominee in 1917 and was settled out of court, with the Baroness being required to repay $13,515.<ref name="Callow" />

[[File:Passport Application 1927.jpg|thumbnail|Passport Application 1927]]

==Adoption==
Little is known of the Baroness’s life after the conclusion of the 1917 Menominee trial. She adopted an infant boy in 1917 (she was 47 or 48 at the time) and named him John Andrew van Eerde.<ref name="Obituary">"John A. van Eerde, Ph.D., Obituary," The Morning Call (Lehigh University Newsletter), January 10, 2006.</ref> National Archives ship records show the Baroness and her son traveling in the summer months to Europe during his childhood and adolescent years. John van Eerde attended Harvard and became a Professor of Romance Languages at Lehigh University.<ref name="Obituary" /> He served with distinction during World War II (as in interpreter of Italian prisoner mail for Allied Intelligence), married, had one child, and died in 2006.<ref name="Obituary" />

==Death==
The Baroness died in New York City on March 10, 1937.<ref name="Callow" /> Her death certificate identified her as May de Pallandt van Eerde<ref name="Callow" /> and the cause of death was noted as cancer. The newspaper death notice listed her as beloved wife Marie of Edward Allen Morrisey, who worked for the Zanuck Movie Company.<ref name="Callow" /> She is buried in [[Gate of Heaven Cemetery (Hawthorne, New York)|Gate of Heaven Cemetery]], Hawthorne, Westchester County, New York.<ref>[http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GScid=64622&GRid=134175699 May "Marie" Dugas De Pallandt Van Eerde (1869 - 1937) - Find A Grave Memorial]</ref>

== In Popular Culture ==

The life of May Dugas de Pallandt van Eerde is the subject of the novel ''Parlor Games'' (Doubleday 2013) by Maryka Biaggio.

== References ==

{{reflist}}

*
*
*

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box.  Just press "Save page". -->




{{DEFAULTSORT:Dugas de Pallandt van Eerde, May}}
[[Category:1869 births]]
[[Category:1937 deaths]]
[[Category:People from Chicago]]