{{Taxobox|fossil_range=Early [[Miocene]] ([[MN 4 (biostratigraphic zone)|MN 4]]–[[MN 5 (biostratigraphic zone)|5]])
|regnum=[[Animal]]ia|phylum=[[Chordate|Chordata]]|classis=[[Mammal]]ia|ordo=[[Rodent]]ia|familia=[[Gliridae]]|subfamilia=[[Glirinae]]|genus='''''Seorsumuscardinus'''''|genus_authority=Hans de Bruijn|De Bruijn, 1998|subdivision_ranks=Species|subdivision=
*''Seorsumuscardinus alpinus'' <small>De Bruijn, 1998</small>
*''Seorsumuscardinus bolligeri'' <small>(Prieto and Böhme, 2007)</small>
:[[Synonym (taxonomy)|Synonym]]:
:*''Heissigia bolligeri'' <small>Prieto and Böhme, 2007</small>
|type_species=''Seorsumuscardinus alpinus''
|type_species_authority=[[Hans de Bruijn|De Bruijn]], 1998
|synonyms=
*''Heissigia'' <small>Prieto and Böhme, 2007</small>
|range_map=Seorsumuscardinus distribution.png
|range_map_alt=Seorsumuscardinus is known from a site in northeastern Greece, one in southeastern Austria, and one in northeastern Switzerland in MN 4 and from a site in southeastern Germany in MN 5.
|range_map_caption=Localities where ''Seorsumuscardinus'' has been found. [[MN 4 (biostratigraphic zone)|MN 4]] localities (''S. alpinus'') in red; the single [[MN 5 (biostratigraphic zone)|MN 5]] locality (''S. bolligeri'') in blue.}}
'''''Seorsumuscardinus''''' is a [[genus]] of [[fossil]] [[dormice]] from the early [[Miocene]] of Europe. It is known from zone [[MN 4 (biostratigraphic zone)|MN 4]] (see [[MN zonation]]) in [[Oberdorf am Hochegg|Oberdorf]], Austria; Karydia, Greece; and Tägernaustrasse-Jona, Switzerland, and from zone [[MN 5 (biostratigraphic zone)|MN 5]] in a single site at [[Affalterbach, Pfaffenhofen an der Ilm|Affalterbach]], Germany. The MN 4 records are placed in the species '''''S.&nbsp;alpinus''''' and the sole MN 5 record is classified as the species '''''S.&nbsp;bolligeri'''''. The latter was placed in a separate genus, ''Heissigia'', when it was first described in 2007, but it was reclassified as a second species of ''Seorsumuscardinus'' in 2009.

The two species of ''Seorsumuscardinus'' are known from isolated teeth, which show that they were medium-sized dormice with flat teeth. The teeth are all characterized by long transverse crests coupled with shorter ones. One of these crests, the [[anterotropid]], distinguishes the two species, as it is present in the lower molars of ''S.&nbsp;alpinus'', but not in those of ''S.&nbsp;bolligeri''. Another crest, the [[centroloph]], reaches the outer margin of the first upper molar in ''S.&nbsp;bolligeri'', but not in ''S.&nbsp;alpinus''. ''Seorsumuscardinus'' may be related to ''[[Muscardinus]]'', the genus of the living [[hazel dormouse]], which appears at about the same time, and the older ''[[Glirudinus]]''.

==Taxonomy==
In 1992, Thomas Bolliger described some teeth of ''Seorsumuscardinus'' from the Swiss locality of Tägernaustrasse ([[MN 4 (biostratigraphic zone)|MN 4]]; [[early Miocene]], see [[MN zonation]]) as an indeterminate [[dormouse]] (family Gliridae) perhaps related to ''Eomuscardinus''.<ref name=B129>Bolliger, 1992, p.&nbsp;129</ref> Six years later, [[Hans de Bruijn]] named the new genus and species ''Seorsumuscardinus alpinus'' on the basis of material from [[Oberdorf am Hochegg|Oberdorf]] in Austria (also MN 4) and included fossils from Tägernaustrasse and from [[Karydia, Rhodope|Karydia]] in Greece (MN 4) in ''Seorsumuscardinus''.<ref>De Bruijn, 1998, pp.&nbsp;111–113; Prieto, 2009, pp.&nbsp;377, 379; Doukas, 2003, table&nbsp;2</ref> In 2007, Jerome Prieto and Madeleine Böhme named ''Heissigia bolligeri'' as a new genus and species from [[Affalterbach, Pfaffenhofen an der Ilm|Affalterbach]] in [[Bavaria]] ([[MN 5 (biostratigraphic zone)|MN 5]], younger than MN 4), and referred the Tägernaustrasse material to it, but failed to compare their new genus to ''Seorsumuscardinus''.<ref>Prieto and Böhme, 2007, pp.&nbsp;303, 305; Prieto, 2009, p.&nbsp;377</ref> Two years later, Prieto published a note to compare the two and concluded that they were referable to the same genus, but different species. Thus, the genus ''Seorsumuscardinus'' now includes the species ''Seorsumuscardinus alpinus'' from MN 4 and ''S.&nbsp;bolligeri'' from MN 5. Prieto provisionally placed the Tägernaustrasse material with ''S.&nbsp;alpinus''.<ref name=P378>Prieto, 2009, p.&nbsp;378</ref> He also mentioned ''[[Pentaglis|Pentaglis földváry]]'', a name given to a single upper molar from the [[middle Miocene]] of Hungary, which is now lost. Although the specimen shows some similarities with ''Seorsumuscardinus'', published illustrations are too poor to confirm the identity of ''Pentaglis'', and Prieto considered the latter name to be an unidentifiable ''[[nomen dubium]]''.<ref name=P379>Prieto, 2009, p.&nbsp;379</ref>

Because of its [[Synapomorphy|derived]] and specialized [[morphology (anatomy)|morphology]], the relationships of ''Seorsumuscardinus'' are obscure. However, it shows some similarities with ''[[Muscardinus]]'', a genus which includes the living [[hazel dormouse]], and may share a common ancestor with it, such as the earlier fossil genus ''[[Glirudinus]]''.<ref>Prieto and Böhme, 2007, p.&nbsp;306; Prieto, 2009, p.&nbsp;378</ref> All three are part of the dormouse family, which includes many extinct forms dating back to the [[early Eocene]] (around 50 million years ago), as well as a smaller array of living species.<ref>McKenna and Bell, 1997, pp.&nbsp;174–178</ref> The [[name of a biological genus|generic name]] ''Seorsumuscardinus'' combines the Latin ''seorsum'', which means "different", with ''Muscardinus'' and the [[specific name (zoology)|specific name]] ''alpinus'' refers to the occurrence of ''S.&nbsp;alpinus'' close to the Alps. ''Heissigia'' honored paleontologist Kurt Heissig for his work in Bavaria on the occasion of his 65th birthday<ref name=PB302>Prieto and Böhme, 2007, p.&nbsp;302</ref> and ''bolligeri'' honors Thomas Bolliger for his early description of material of this dormouse.<ref name=PB303>Prieto and Böhme, 2007, p.&nbsp;303</ref>
{{clear}}

==Description==
{| class="wikitable" align="right" style="margin-top: 0em; margin-left: 0.5em"
|+ Measurements<ref name=DB112-PB303/>
|-
! Tooth !! Measurement !! Affalterbach !! Oberdorf
|-
| rowspan="2" | P4 || Length || 1.03 || 0.93–0.97
|-
| Width || 1.07 || 1.06–11.2
|-
| rowspan="2" | M1 || Length || 1.26 || 1.20–1.29
|-
| Width || 1.40 || 1.31–1.43
|-
| rowspan="2" | M2 || Length || 1.14–1.22 || 1.21–1.24
|-
| Width || 1.37–1.50 || 1.33–1.45
|-
| rowspan="2" | M3 || Length || 1.05 || 1.03
|-
| Width || 1.25 || 1.19
|-
| rowspan="2" | p4 || Length || – || 0.80
|-
| Width || – || 0.65
|-
| rowspan="2" | m1 || Length || 1.35 || 1.25–1.27
|-
| Width || 1.28 || 1.26–1.31
|-
| rowspan="2" | m2 || Length || 1.28 || –
|-
| Width || 1.40 || –
|-
| rowspan="2" | m3 || Length || – || 1.15–1.28
|-
| Width || – || 1.06–1.27
|-
| colspan="4" | <small>All measurements are in millimeters.<br />P4: fourth upper premolar; M1: first upper molar; etc.<br />p4: fourth lower premolar; m1: first lower molar; etc.</small>
|}
Only the [[cheek teeth]] of ''Seorsumuscardinus'' are known; these include the fourth [[premolar]] and three [[molar (tooth)|molars]] in the upper ([[maxilla]]) and lower jaws ([[mandible]]).<ref>De Bruijn, 1998, p.&nbsp;110; Prieto and Böhme, 2007, p.&nbsp;303</ref> The teeth are medium-sized for a dormouse and have a flat [[Occlusion (dentistry)|occlusal]] (chewing) surface.<ref>Bolliger, 1992, p.&nbsp;129; De Bruijn, 1998, p.&nbsp;111; Prieto and Böhme, 2007, p.&nbsp;303</ref> ''S.&nbsp;bolligeri'' is slightly larger than ''S.&nbsp;alpinus''.<ref name=P378/>

===Upper dentition===
The fourth upper premolar (P4) has four main, transversely placed crests;<ref name=DB112-PB303>De Bruijn, 1998, p.&nbsp;112; Prieto and Böhme, 2007, p.&nbsp;303</ref> the description of ''S.&nbsp;bolligeri'' mentions an additional, centrally placed small crest.<ref name=PB303/> De Bruijn interpreted the four main crests as the [[anteroloph]], [[protoloph]], [[metaloph]], and [[posteroloph]] from front to back and wrote that these crests are not connected on the sides of the tooth.<ref name=DB112/> Prieto and Böhme note that the posteroloph is convex on the back margin of the tooth.<ref name=PB303/> In ''Muscardinus'', the number of ridges on P4 ranges from five in ''[[Muscardinus sansaniensis]]'' to two in ''[[Muscardinus pliocaenicus|M.&nbsp;pliocaenicus]]'' and the living hazel dormouse, but the protoloph and metaloph are always connected on the lingual (inner) side of the tooth.<ref name=DB112/> P4 is two-rooted in ''S.&nbsp;alpinus''<ref name=DB112>De Bruijn, 1998, p.&nbsp;112</ref> and three-rooted in ''S.&nbsp;bolligeri''.<ref name=PB303/>

The first upper molar (M1) was described as square by De Bruijn<ref name=DB111>De Bruijn, 1998, p.&nbsp;111</ref> and as rounded by Prieto and Böhme.<ref name=PB303/> There are five main transverse crests,<ref name=DB112-PB303/> which are mostly isolated, but some may be connected on the borders of the teeth.<ref>De Bruijn, 1998, pp.&nbsp;111–113; Prieto and Böhme, 2007, pp.&nbsp;303–304</ref> The middle crest, the [[centroloph]], reaches to the labial (outer) margin in the single known M1 of ''S.&nbsp;bolligeri'', but does not in any of the five M1 of ''S.&nbsp;alpinus''.<ref name=P377>Prieto, 2009, p.&nbsp;377</ref> The front crest, the anteroloph, is less distinct in ''S.&nbsp;bolligeri'' than most ''S.&nbsp;alpinus'', but one M1 of ''S.&nbsp;alpinus'' is similar to that of ''S.&nbsp;bolligeri''.<ref name=P378/> M1 has three roots in ''S.&nbsp;alpinus'',<ref name=DB112/> but the number of roots in ''S.&nbsp;bolligeri'' is not known.<ref name=PB304>Prieto and Böhme, 2007, p.&nbsp;304</ref>

Prieto and Böhme describe M2 as less rounded than M1<ref name=PB304/> and De Bruijn notes that the crests are more parallel.<ref name=DB113>De Bruijn, 1998, p.&nbsp;113</ref> In addition to the five main crests, small crests are present in front of and behind the centroloph that do not cover the full width of the tooth.<ref name=DB113-PB304>De Bruijn, 1998, p.&nbsp;113; Prieto and Böhme, 2007, p.&nbsp;304</ref> In one ''S.&nbsp;bolligeri'' M2, there is a small crest on the lingual side in front of the centroloph, but such a crest does not occur in any ''S.&nbsp;alpinus''.<ref name=P377/> Another M2 of ''S.&nbsp;bolligeri'' has this crest on the labial side.<ref name=PB304/> On the other hand, all five M2 of ''S.&nbsp;alpinus'' have a minor crest on the labial side behind the centroloph. In two M2 of ''S.&nbsp;alpinus'', the centroloph and the metaloph are connected by a longitudinal crest, which is never present in ''S.&nbsp;bolligeri''.<ref name=P377/> There are three roots.<ref name=DB112-PB304>De Bruijn, 1998, p.&nbsp;112; Prieto and Böhme, 2007, p.&nbsp;304</ref>

M3 is known from a single specimen each from Oberdorf, Affalterbach, and Tägernaustrasse.<ref>De Bruijn, 1998, p.&nbsp;112; Prieto and Böhme, 2007, p.&nbsp;303; Bolliger, 1992, p.&nbsp;129</ref> In addition to the main crests, there are two or three additional smaller crests.<ref name=DB113-PB304/> The roots are unknown.<ref name=PB304/>

===Lower dentition===
The fourth lower premolar (p4) is known from a poorly preserved specimen from Oberdorf and a less worn specimen from Tägernaustrasse. There are four ridges, of which the front and back pair are connected at the lingual side and in the Oberdorf specimen also at the labial side. This tooth is similar to that of ''Muscardinus hispanicus'', but the front pair is better developed.<ref name=DB113/> There is one root.<ref name=DB112/>

The first lower molar (m1) bears four main crests and a smaller one between the two crests at the back.<ref name=DB113-PB303/> An additional crest (the [[anterotropid]]) is present between the two front crests, the [[anterolophid]] and the [[metalophid]], in ''S.&nbsp;alpinus'', but not in ''S.&nbsp;bolligeri''.<ref name=P377/> The occlusal pattern of m2 resembles that of m1.<ref name=DB113-PB303>De Bruijn, 1998, p.&nbsp;113; Prieto and Böhme, 2007, p.&nbsp;303</ref> ''S.&nbsp;bolligeri'' also lacks an anterotropid on m2,<ref name=P377/> but the tooth is not known from Oberdorf.<ref name=DB113/> In a worn m2 from Tägernaustrasse, there is a thickened portion in the labial part of the anterolophid, which Prieto interpreted as a remnant of the anterotropid; this led him to identify the Tägernaustrasse population as ''S.'' [[cf.]] ''alpinus''.<ref name=P378/> Only Oberdorf has yielded the m3 of ''Seorsumuscardinus''. It resembles the m1 and has a short anterotropid, but has more oblique crests.<ref name=DB113/> In ''S.&nbsp;alpinus'', the lower molars have two and occasionally three roots.<ref name=DB112/> The roots of the m1 of ''S.&nbsp;bolligeri'' are not preserved and the m2 has two roots.<ref name=PB303/>

==Range==
In MN 4, ''Seorsumuscardinus'' has been recorded from Oberdorf, Austria (sites 3 and 4, which yielded 6 and 17 ''Seorsumuscardinus alpinus'' teeth, respectively); Karydia, Greece (''S.&nbsp;alpinus''); and Tägernaustrasse, Switzerland (5 teeth; ''S.'' cf. ''alpinus''). Affalterbach, Germany, where 10 teeth of ''S.&nbsp;bolligeri'' were found, is the only known MN 5 locality.<ref>Prieto, 2009; Bolliger, 1992, p.&nbsp;128; De Bruijn, 1998, p.&nbsp;112; Prieto and Böhme, 2007, p.&nbsp;303</ref> In all these localities, it is part of a diverse dormouse fauna.<ref>Bolliger, 1992; De Bruijn, 1998; Doukas, 2003; Prieto and Böhme, 2007</ref> Because the distributions of the two known species are temporally distinct, Prieto suggested that the genus may be useful for [[biostratigraphy]] (the use of fossils to determine the age of deposits).<ref name=P378/> ''Seorsumuscardinus'' occurred at the same time as the oldest known ''Muscardinus''.<ref>Prieto and Böhme, 2007, pp.&nbsp;305–306</ref>

==References==
{{reflist|colwidth=30em}}

==Literature cited==
*Bolliger, T. 1992. Kleinsäuger aus der Miozänmolasse der Ostschweiz. Documenta Naturae 75:1–296.
*Bruijn, H. de. 1998. Vertebrates from the Early Miocene lignite deposits of the opencast mine Oberdorf (Western Styrian Basin, Austria): 6. Rodentia I (Mammalia). Annalen des Naturhistorischen Museums in Wien 99A:99–137.
*Doukas, C.S. 2003. [http://revistas.ucm.es/geo/11321660/articulos/COPA0303220127A.PDF The MN4 faunas of Aliveri and Karydia (Greece)]. Coloquios de Paleontología, Volúmen Extraordinario 1:127–132.
*McKenna, M.C. and Bell, S.K. 1997. Classification of Mammals: Above the species level. New York: Columbia University Press, 631&nbsp;pp. ISBN 978-0-231-11013-6
*Prieto, J. 2009. [http://dx.doi.org/10.1127/0077-7749/2009/0252-0377 Comparison of the dormice (Gliridae, Mammalia) ''Seorsumuscardinus alpinus'' De Bruijn, 1998 and ''Heissigia bolligeri'' Prieto & Böhme, 2007] (subscription required). Neues Jahrbuch für Geologie und Paläontologie, Abhandlungen 252(3):377–379.
*Prieto, J. and Böhme, M. 2007. [http://dx.doi.org/10.1127/0077-7749/2007/0245-0301 ''Heissigia bolligeri'' gen. et sp. nov.: a new enigmatic dormouse (Gliridae, Rodentia) from the Miocene of the Northern Alpine Foreland Basin] (subscription required). Neues Jahrbuch für Geologie und Paläontologie, Abhandlungen 245(3):301–307.
{{featured article}}

[[Category:Dormice]]
[[Category:Fossil taxa described in 1998]]
[[Category:Miocene rodents]]
[[Category:Prehistoric mammals of Europe]]