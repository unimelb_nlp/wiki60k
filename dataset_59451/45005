{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox company
| name = Charles Moore and Co.
| logo = 
| type = [[Private company]]
| industry = Retail [[department store]]
| fate = 
| predecessor = <!-- or: | predecessors = -->
| successor = <!-- or: | successors = -->
| founded = <!-- if known: {{Start date and age|YYYY|MM|DD}} in [[city]], [[state]], [[country]] -->
| founder = Charles Moore<!-- or: | founders = -->
| defunct = ''[[circa]]'' 1980<!-- {{End date|YYYY|MM|DD}} -->
| hq_location_city = [[Adelaide]], [[South Australia]]
| hq_location_country = Australia
| area_served = <!-- or: | areas_served = -->
| key_people = 
| products = 
| owner = <!-- or: | owners = -->
| num_employees = 
| num_employees_year = <!-- Year of num_employees data (if known) -->
| parent = 
| website = <!-- {{URL|example.com}} -->
}}
{{Infobox person
| name          = Charles Moore
| image         = 
| alt           = 
| caption       = 
| birth_name    = 
| birth_date    = ''[[circa|ca.]]'' 1858 <!-- {{birth date and age|YYYY|MM|DD}} for living people. For people who have died, use {{Birth date|YYYY|MM|DD}}. -->
| birth_place   = near [[Derby]], [[Ireland]]
| death_date    = {{Death date and age|1916|09|30|1858|df=y}}
| death_place   =
| resting_place = [[Brighton Cemetery]]
| nationality   = [[Irish Australian]]
| other_names   = 
| occupation    = Merchant; philanthropist
| years_active  = 1881-1916
| known_for     = Charles Moore and Co.
| notable_works =
| spouse        = Jane Cocks Carty
| children      = 4 sons; 3 daughters
 }}
'''Charles Moore and Co.''' was a company based in [[Adelaide, South Australia]] which owned a number of [[department store]]s in three [[Australia]]n [[States and territories of Australia|states]]. It was founded by [[Irish Australian|Irish]]-born businessman, Charles Moore (''[[circa|ca.]]'' 1858 &ndash; 30 September 1916). Its best-known assets were the department store known to two generations of Adelaideans as "Moore's on the Square", Charles Moore's on [[Hay Street, Perth|Hay Street]], {{WAcity|Perth}}, [[Western Australia]] and Read's in [[Prahran, Victoria|Prahran]], [[Victoria (Australia)|Victoria]].

==History==

===Charles Moore===
Charles Moore was born near [[Derry]] in the north of Ireland. He emigrated to Adelaide around 1881 and for a time worked for [[John Martin & Co.]] in [[Rundle Street, Adelaide|Rundle Street]], then for the wholesalers Matthew Goode & Co. He struck out on his own account by opening a store in 64&ndash;74 Gouger Street, later the site of Peoplestores Ltd., on 9 April 1884. In 1905 this store moved to larger premises on the Gouger Street market site.

He took over Peter Smith and Co.'s "Sandringham House" (previously "Gault's") at 16&ndash;18 Rundle Street and reopened it as the "Coliseum" in 1898, managed by F. C. Catt, who later had his own Rundle Street store.<ref>{{cite news |url=http://nla.gov.au/nla.news-article43276007 |title=F. C. Catt Stores Closing |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=28 February 1925 |accessdate=13 January 2013 |page=13 |publisher=National Library of Australia}}</ref> This shop closed in December 1909 after water leaking from the fifth floor percolated through the building, resulting in extensive damage to stock and fittings.<ref>{{cite news |url=http://nla.gov.au/nla.news-article57870724 |title=Water Fiend |newspaper=[[The Register (Adelaide)|TheRegister]] |location=Adelaide |date=13 December 1909 |accessdate=13 January 2013 |page=7 |publisher=National Library of Australia}}</ref> This occurred just four years after the store had been enlarged by the addition of another two storeys. It became Donaldson and Andrews (later Donaldson's), then in 1933 Glasson's, which was taken over by Myers in 1938.

He opened a branch in the boom town of [[Kadina, South Australia|Kadina]] (on the corner of Graves and Hallett Streets) in 1887.<ref name=fifty>{{cite news |url=http://nla.gov.au/nla.news-article74088580 |title=Fifty Years of Growth |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=9 April 1934 |accessdate=13 January 2013 |page=17 |publisher=National Library of Australia}}</ref> By 1891 he also had stores in [[Eudunda, South Australia|Eudunda]] and [[Balaklava, South Australia|Balaklava]] in country South Australia. By 1893 a store in [[Manoora, South Australia|Manoora]] was being advertised, but was dropped by 1895. From 1900 only the Kadina shop was advertised, and that closed by 1981.

[[File:Samuel Way Building.jpg|thumb|"Moore's on the Square" (now Sir Samuel Way Building)]]

Moore was a leading figure in the Central Traders' Association, which represented businesses around [[Grote Street, Adelaide|Grote]] and [[Gouger Street, Adelaide|Gouger]] Streets, bravely making major investments away from the major retail precinct of [[Rundle Street, Adelaide|Rundle]], [[Hindley Street, Adelaide|Hindley]] and [[Grenfell Street, Adelaide|Grenfell]] Streets.

In 1914 he opened a new palatial store on the west side of [[Victoria Square, Adelaide|Victoria Square]]<ref>{{cite news |url=http://nla.gov.au/nla.news-article6468847 |title=Personal |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=2 October 1916 |accessdate=12 January 2013 |page=6 |publisher=National Library of Australia}}</ref> between Grote and Gouger Streets, designed by architects Garlick & Jackman. No expense was spared in providing a maximum of display area behind large plate glass windows, generously lit by a huge leadlight cupola and extensive artificial lighting.<ref>{{cite news |url=http://nla.gov.au/nla.news-article6380037 |title=An Enterprising Firm |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=2 May 1914 |accessdate=12 January 2013 |page=24 |publisher=National Library of Australia}}</ref> A feature was a grand marble staircase leading to the first floor.<ref>In the US this would be reckoned the second floor.</ref> The store was officially opened by the Mayor of Adelaide, [[Isaac Isaacs (mayor)|Isaac Isaacs]], on 29 August 1916.<ref name=fifty/>

On 2 March 1948 Moore's was gutted by fire; all that remained was some ground floor structures, the external shell, and the staircase. The shop was rebuilt under the architects Garlick, Jackman and Gooden and business returned until a gradual decline in the 1970s. In 1979 the store was sold to the South Australian Government and was later transformed into a major law courts building containing some 26 courtrooms, library and administration. It was named the "Sir Samuel Way Building" by the Governor of South Australia [[Donald Dunstan (governor)|Sir Donald Dunstan]] <!--NOT Don Dunstan, SA Premier from 1970 to 1979 --> in 1983 commemorating the South Australian jurist [[Samuel Way]].

He opened a store "Moore and Gobbett" in Hay Street, Perth, Western Australia in 1895, and bought the "Coliseum" furniture shop, also on Hay Street, but sold it in 1902.<ref>{{cite news |url=http://nla.gov.au/nla.news-article24742076 |title=Business Announcements |newspaper=[[The West Australian]] |location=Perth |date=4 April 1902 |accessdate=13 January 2013 |page=5 |publisher=National Library of Australia}}</ref> By 1899 the shop was advertised as Charles Moore and Co. Moore is particularly remembered in Perth for his advocacy for a [[Princess Margaret Hospital for Children|children's hospital]] (founding in 1897 the Children's Hospital Movement)<ref>{{cite news |url=http://nla.gov.au/nla.news-article76523717 |title=Original Committee |newspaper=[[Daily News (Perth, Western Australia)|The Daily News]] |location=Perth |date=26 June 1908 |accessdate=13 January 2013 |page=3 |publisher=National Library of Australia}}</ref> and its generous support.<ref>{{cite news |url=http://nla.gov.au/nla.news-article81720899 |title=Mr. Chas. Moore Dies |newspaper=[[Daily News (Perth, Western Australia)|The Daily News]] |location=Perth |date=30 September 1916 |accessdate=13 January 2013 |page=12 Edition: Third Edition |publisher=National Library of Australia}}</ref>

He was principal partner of the "Charles M. Read" store in [[Chapel Street]], Prahran, and owned the property.<ref>{{cite news |url=http://nla.gov.au/nla.news-article88437522 |title=Obituary. |newspaper=[[Prahran Chronicle]] |location=Vic. |date=7 October 1916 |accessdate=13 January 2013 |page=5 |publisher=National Library of Australia}}</ref> and was full owner when his partner Jacob Read died in 1910. In 1956 Read's was Australia's largest suburban store.<ref>{{cite news |url=http://nla.gov.au/nla.news-article72530920 |title=Suburbia's Top Store |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=19 January 1956 |accessdate=13 January 2013 |page=11 |publisher=National Library of Australia}}</ref>

Around 1900 Charles and his family moved to [[Melbourne, Victoria|Melbourne]], living at "Woorigoleen", Clendon Road, [[Toorak, Victoria|Toorak]] to the suburb of Toorak in Melbourne, where he died after a short illness, and was buried at [[Brighton, Victoria|Brighton]] cemetery. Substantial sums were left to his employees in his will.<ref>{{cite news |url=http://nla.gov.au/nla.news-article77664047 |title=A Generous Employer. |newspaper=[[The Border Watch]]|location=Mount Gambier, SA |date=11 April 1917 |accessdate=13 January 2013 |page=3 |publisher=National Library of Australia}}</ref> They owned a nearby property "Warrawee" on the corner of Grange and Struan Roads, where Mrs. Moore lived for a time, and subdivided for sale in 1918. She purchased "Merriwa", [[Alfred Rutter Clarke|A. Rutter Clarke]]'s home, noted for its garden of indigenous plants,<ref>{{cite news |url=http://nla.gov.au/nla.news-article90864250 |title=Cultivating Indigenous Flowers |newspaper=[[The Leader (Melbourne)|The Leader]] |location=Melbourne |date=17 November 1917 |accessdate=14 January 2013 |page=13 Edition: Town and Weekly |publisher=National Library of Australia}}</ref><ref>{{cite news |url=http://nla.gov.au/nla.news-article90188918 |title=Social Circle |newspaper=[[The Leader (Melbourne)|The Leader]] |location=Melbourne |date=18 December 1915 |accessdate=14 January 2013 |page=41 Edition: Weekly |publisher=National Library of Australia}}</ref> on Orrong Road, Toorak in September 1917. From around 1930 until her death she lived at "Tara", also on Orrong Road, the last ten years as an invalid.<ref>{{cite news |url=http://nla.gov.au/nla.news-article11824610 |title=Obituary |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=22 May 1944 |accessdate=14 January 2013 |page=4 |publisher=National Library of Australia}}</ref>

==Family==
Charles Moore married Jane Cocks Carty (1871 &ndash; 19 May 1944). They had four sons and three daughters. Jane Carty was born in [[Dublin]], the daughter of the founder of the [[Band of Hope]] in Ireland.<ref>{{cite news |url=http://nla.gov.au/nla.news-article11824610 |title=Obituary |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=22 May 1944 |accessdate=13 January 2013 |page=4 |publisher=National Library of Australia}}</ref> Both were interred at [[Brighton Cemetery]].
*Charles Carty Moore (''[[circa|ca.]]'' May 1895 &ndash; ) married then lived at Montalto Avenue, Toorak. He was a noted horseman and huntsman. Their son Fred Moore (born ca.1926) was Managing Director in 1956. Another son, Charles Owen Moore, married Mary Noel Murphy on 14 December 1942.
*Nora married Lawrence "Larry" Heath ( &ndash; ) of [[Malvern, Victoria]] on 24 April 1918.<!-- perhaps Frederick Lawrence Heath of Malvern Vic born ca.June 1891--> His brother was the tennis player [[Rod Heath]]. Their home in 1942 was "Brenchley", 213 Orrong Road, Toorak.<!--Larry accompanied Miss Kathleen Moore to Adelaide in 1927! -->
*Kenneth George Carty "Ken" Moore (15 March 1899 &ndash; ) married Gwen Johnston on 26 August 1931. Gwen was a noted horsewoman, aviator, and daughter of Major-General George Johnston.
*Kathleen Mary "Kitty" ( &ndash; 15 December 1949) married [later General] (Walter) Noel Tinsley [[Distinguished Service Order|DSO]] (24 December 1898 &ndash; 1974) on 15 November 1933. They lived at Stradbroke Avenue, Toorak then Glenferrie Road, [[Malvern, Victoria|Malvern]].
*Denis Washington Coburn Moore (23 April 1904 &ndash; July 1963) was a member of the Stock Exchange and respected economist.<ref>{{cite news |url=http://nla.gov.au/nla.news-article63246322 |title=Financial Position Abroad |newspaper=[[Gippsland Times]] |location=Vic. |date=18 May 1931 |accessdate=13 January 2013 |page=1 |publisher=National Library of Australia}}</ref> He married Dorothy Rogers (a noted angler<ref>{{cite news |url=http://nla.gov.au/nla.news-article91799864 |title=Woman's Record Catch |newspaper=[[The Advocate (Australia)|The Advocate]] |location=Burnie, Tas. |date=24 March 1936 |accessdate=13 January 2013 |page=7 Edition: Daily |publisher=National Library of Australia}}</ref> and horsewoman) on 8 October 1935
*Desmond Carty Moore ( &ndash; ) <!--perhaps married Phyllis Reid?--> was a lawyer and pastoralist of Marlow Farm, [[Nar Nar Goon, Victoria]]. He was noted for his court actions against the Tax Department following a change to the method of taxing woolgrowers.<ref>{{cite news |url=http://nla.gov.au/nla.news-article23022643 |title=Wool tax stands until test. |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=12 December 1950 |accessdate=14 January 2013 |page=7 |publisher=National Library of Australia}}</ref> He lived at "Monomeeth", 217 Orrong Road, Toorak.
*Patricia Margaret "Pat" ( &ndash; 31 October 1947) married Robert Wylie "Rob" Burns Cuming (5 March 1911 &ndash; ) of Minchin Street, [[Torrensville, South Australia|Torrensville]] on 31 October 1931

===Second generation===
Charles C. Moore succeeded his father as Chief Executive Officer of Chas. Moore and Co. and was succeeded by a grandson Fred Moore some time before 1956.

Campbell Smith, a nephew of Charles Moore, was General Manager until 1935.<ref>{{cite news |url=http://nla.gov.au/nla.news-article92367186 |title=News of the Week At Home. |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |location=Adelaide |date=25 April 1935 |accessdate=13 January 2013 |page=40 |publisher=National Library of Australia}}</ref>

Charles Edward Stuart Smith, another nephew, managed the Charles M. Read store in Prahran from 1902 then managed the Adelaide business then from 1907 to 1930 managed the Perth business. He then worked for another company in Sydney but returned to the Prahran store on the death of his uncle and remained there until 1932.<ref>{{cite news |url=http://nla.gov.au/nla.news-article32589746 |title=Personal. |newspaper=[[The West Australian]] |location=Perth |date=1 December 1932 |accessdate=13 January 2013 |page=16 |publisher=National Library of Australia}}</ref>

== References ==
{{Reflist|30em}}

==External links==
* [http://www.heritage.gov.au/cgi-bin/ahpi/record.pl?RNE17719 Sir Samuel Way Building]

{{DEFAULTSORT:Moore, Charles and Co.}}
<!--[[Category:Australian businesspeople]]-->
[[Category:Defunct department stores of Australia]]
[[Category:History of Adelaide]]
[[Category:Companies based in Adelaide]]