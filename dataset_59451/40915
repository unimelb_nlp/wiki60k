{{Infobox person
|name          = Charles W. Sandford
|image         = File:Gen. C.W. Sandford.jpg
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = {{Birth date|1796|5|5}}
|birth_place   = [[Newark, New Jersey]], [[United States]]
|death_date    = {{death date and age|1878|7|25|1796|5|5}}
|death_place   = [[Avon, New York|Avon Springs, New York]]
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = Charles W. Sanford
|known_for     = New York militia officer who led the First Division in several major riots and civil disturbances between the 1830s to the 1860s. 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Militia officer, lawyer and businessman
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = Commanding officer of the New York First Division
|predecessor   = [[Jacob Morton]]
|successor     = [[Alexander Shaler]]
|party         = 
|boards        = 
|religion      = [[Episcopal Church (United States)|Episcopalian]]
|spouse        = 
|partner       = 
|children      = 2 sons, 4 daughters
|parents       = William B. Sandford
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}
Major General '''Charles W. Sandford''' (May 5, 1796 &ndash; July 25, 1878) was an American militia and artillery officer, lawyer and businessman. He was a senior officer in the [[New York State Militia]] for over thirty years and commanded the First Division in every major civil disturbance in New York City up until the [[American Civil War]], most notably, the [[New York Draft Riots]] in 1863.

==Biography==
Charles W. Sandford was born in [[Newark, New Jersey]] to William B. Sandford, a farmer and veteran of the [[American Revolutionary War]], on May 5, 1796. He pursued a career in law, studying under [[Ogden Hoffman]], and enlisted as a private in the New York State Artillery. Assigned to the [[3rd New York Militia Infantry Regiment|Third Regiment]], he remained with the unit as it became the [[8th New York Militia Infantry Regiment|Eighth Regiment]] popularly known as the "Washington Grays". Rising up the ranks as a [[non-commissioned officer]], he commanded Company F and was subsequently promoted to lieutenant colonel and to full colonel. In 1834, he was elected brigadier general of the Sixth Brigade Artillery.<ref name="Article">"An Old Militia Leader.; Death Of Major-Gen. Sandford. His Varied Experience In The Citizen-Soldiery--A Veteran Of The National Guard--The Story Of His Military Career". ''The New York Times''. 26 Jul 1878</ref>

On May 10, 1839, Sandford was commissioned major general of the First Division and held command for nearly three decades. Although having a fine service record, Sandford kept an informal atmosphere and sometimes lax discipline within his command. Units were also far below regimental quotas. Brigadier generals were late reporting for duty and he himself was very late organizing division formations on occasion. This often resulted in delays such as [[military parades]] being three or four hours overdue. These officers were generally not held accountable for their negligence and [[court-martial|courts-martial]] were rarely held.<ref name="Article"/>

Sandford was fond of military pomp and often organized celebrations and public events involving the militia. Among these included a parade honoring visiting General [[José Antonio Páez]], the former president of [[Venezuela]], in July 1850. His eldest son, 30-year-old Charles Sandford, accompanied Páez back to South America where he died of fever shortly afterwards. The following year, he also had the militia receive [[Louis Kossuth]] upon his arrival in the city in October 1851, his formal reception at [[Castle Garden]] in December and a third parade at his departure. On several occasions, he and his men escorted American presidents when visiting the city and paraded at the funerals of [[Henry Clay]] and [[Daniel Webster]].<ref name="Article"/>

Sandford had a commendable military record leading the militia in the [[Flour Riot of 1837]], the [[Astor Place Riot]] in 1849, the [[Dead Rabbits Riot|Dead Rabbits]] and [[New York City Police Riot|Municipal Police Riots of 1857]] and the [[New York Draft Riot]] in 1863. General [[Winfield Scott]] once said that "Sandford was one of the finest volunteer service generals that he ever knew."<ref name="Article"/> He commanded the Seventh Regiment and militia forces on behalf of Sheriff Westervelt and eventually confronted Mayor [[Fernando Wood]], his forces surrounding [[City Hall Park]], and took him into custody.<ref>Hershkowitz, Leo. ''Tweed's New York: Another Look''. Garden City, New York: Anchor Press, 1977. (p. 59–60) ISBN 0-385-07656-8</ref> That same year, he was asked by NYPD Police Commissioner [[Simeon Draper]] for his assistance during the Dead Rabbits Riot. He sent the Eight and Seventy-First Regiments, both at half strength but supported by two 75-man police detachments, which marched down [[White Street (Manhattan)|White]] and [[Worth Street (Manhattan)|Worth Streets]] and confronted the gang members driving them back to the [[Five Points, Manhattan|Five Points]]. This action ended the rioting, but police and soldiers continued to patrol the district that night and all the next day.<ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 100, 105, 138) ISBN 1-56025-275-8</ref>

His command seriously weakened due to manpower shortages during the [[American Civil War]], Sandford served on active duty with the [[Union Army]] from April 19 to July 25, 1861. In May 1861, he was ordered by Brigadier General [[Joseph K. Mansfield]] to oversee the capture of [[Alexandria, Virginia]] as the vast majority the Union troops were from New York. He also served under Major General [[Robert Patterson]] for three months and took part in the [[Battle of Hoke's Run]].<ref>Rafuse, Ethan S. ''A Single Grand Victory: The First Campaign and Battle of Manassas''. Wilmington, Delaware: Scholarly Resources, 2002. (p. 41, 95-96) ISBN 0-8420-2876-5</ref>

Returning to New York, he was present during the [[New York Draft Riots]] in 1863 and managed to organize a small force of scattered militia regiments, military troops and home guards from his headquarters at the State Arsenal at [[Seventh Avenue (Manhattan)|Seventh Avenue]] and [[35th Street (Manhattan)|Thirty-Fifth Street]].<ref name="Article"/> He was one of the senior officers who directed police and military during the riots. When receiving reports of the battle between police and rioters at the Union Steam Works, with hundreds of rioters now armed with muskets, swords and pistols, he sent Colonel [[H.J. O'Brien]] and 150 men to help police. Lieutenant Eagleson, in command of two 6-pound cannons and 25 artillerymen, accompanied O'Brien to the battle.<ref name="Asbury"/>

After the war, Sandford was relieved of his command by Governor [[Reuben Fenton]] who appointed [[Alexander Shaler]] to succeed him and officially took command on January 23, 1867. Sandford, who had been involved in the theater as early as 1847, ran the [[Lafayette Circus (Theatre)|Lafayette Theatre]] on [[Sullivan Street (Manhattan)|Sullivan Street]]. His success encouraged him to open a second theater in The [[Bowery]], ''The Mount Pitt Theatre and Circus'', but both buildings burned down within the same week ending his career in the theater. He also built a number of buildings on [[Canal Street (Manhattan)|Canal Street]] although he lost these to fire as well. Sandford would often experience success and disaster in his business dealings, acquiring and then losing small fortunes two or three times, however he was able to provide his family with a comfortable competency his later years. He and his wife often entertained at their West Twenty-Second Street residence whose social functions were often attended by prominent citizens of the city. For over fifty years, he was a leading member in the old [[St. Paul's Episcopal Church (Manhattan, New York)|St. Paul's Episcopal Church]] in [[Broadway (Manhattan)|Broadway]].<ref name="Article"/>

He was also an accomplished lawyer and the one-time partner of John Bristed, son of author [[Charles Astor Bristed]]. Sandford also served as counsel for the [[New York and Harlem Railroad|Harlem Railroad Company]] for twenty years and later represented the company against inventor [[Ross Winans]]. At the time of his death, he was vice president of the [[New York City Bar Association]]. In late-July 1878, Sandford left the city for his annual summer vacation to [[Avon (town), New York|Avon Springs]] in [[Livingston County, New York]]. A day after his arrival however, he died suddenly on the morning of July 25, 1878. A telegram was sent announcing his passing, occurring shortly after his 82nd birthday, but the circumstances of his death were unknown to his family. His body was brought back to the city by one of his daughters and buried shortly afterwards.<ref name="Article"/>

==References==
{{Reflist}}

==Further reading==
{{Portal|Biography|United States Army|American Civil War}}
*Bernstein, Iver. ''The New York City Draft Riots: Their Significance for American Society and Politics in the Age of the Civil War''. New York: Oxford University Press, 1991.
*Burlingame, Michael and John R. T. Ettlinger, ed. ''Inside Lincoln's White House: The Complete Civil War Diary of John Hay''. Carbondale: University of Illinois Press, 1997. 
*Clark, Emmons. ''History of the Seventh Regiment of New York, 1806-1889''. Vol. I. New York: The Seventh Regiment, 1890.  
*Cook, Adrian. ''The Armies of the Streets: The New York City Draft Riots of 1863''. Lexington: University Press of Kentucky, 1974.
*Leech, Margaret. ''Reveille in Washington, 1860-1865: 1860-1865''. New York and London: Harper and Brothers, 1941. 
*McCague, James. ''The Second Rebellion: The Story of the New York City Draft Riots of 1863''. New York: Dial Press, 1968.

==External links==
*[http://www.civilwarhome.com/cwsandfordor.htm Report of Maj. Gen. Charles W. Sandford, commanding First Divisions, New York State National Guard, of operations June 16-July 16, including the Draft Riots in New York City]

{{DEFAULTSORT:Sandford, Charles W.}}
[[Category:1796 births]]
[[Category:1878 deaths]]
[[Category:American lawyers]]
[[Category:Union Army colonels]]
[[Category:People of New York in the American Civil War]]
[[Category:People from Manhattan]]