{{Infobox journal
| title = Arthroscopy: The Journal of Arthroscopic and Related Surgery
| cover = [[File:Arthroscopy Journal March 09 Cover.jpg]]
| editor = James H. Lubowitz
| discipline = [[Orthopedic surgery]]
| publisher = [[Elsevier]]
| frequency = Monthly
| history = 1985–present
| openaccess =
| license =
| impact = 3.206
| impact-year = 2014
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/623124/description#description
| link1 = http://www.arthroscopyjournal.org
| link1-name = Online access
| link2 = http://www.arthroscopyjournal.org/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 11198618
| LCCN =
| CODEN = ARTHE3
| ISSN = 0749-8063
| eISSN = 1526-3231
}}
'''''Arthroscopy: The Journal of Arthroscopic and Related Surgery''''' is a [[peer review|peer-reviewed]] [[healthcare journal|medical journal]] that was established in 1985 and covers research on the clinical practice of [[arthroscopy|arthroscopic]] and [[invasiveness of surgical procedures|minimally invasive surgery]], a subspecialty of [[orthopedic surgery]]. It is the official journal of the [[Arthroscopy Association of North America]], the [[International Society of Arthroscopy, Knee surgery and Orthopaedic Sports Medicine]], and the [[International Society for Hip Arthroscopy]]. The initiative in establishing the journal was taken by Robert Metcalf and the first [[editor-in-chief]] was S. Ward Casscells, who was succeeded in 1992 by Gary G. Poehling. In 2014 James H. Lubowitz succeeded the retiring Dr. Poehling. <ref>Casscells SW. A new journal. ''Arthroscopy'' 1985;1:1</ref><ref name=pmid3281689>{{cite journal |author=Joyce JJ |title=History of the Arthroscopy Association of North America, its origin and growth: Part II |journal=Arthroscopy |volume=4 |issue=1 |pages=1–4 |year=1988 |pmid=3281689 |doi= 10.1016/s0749-8063(88)80002-1|url= }}<!--|accessdate=2010-11-20--></ref> '''''Arthroscopy Techniques''''' is an [[open access]] online companion journal publishing peer-reviewed techniques videos.

== Abstracting and indexing ==
The journal is abstracted and indexed by [[MEDLINE]], [[Web of Science]], and [[Scopus]].

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.arthroscopyjournal.org}}
* [http://www.arthroscopytechniques.org ''Arthroscopy Techniques'' - companion page to Journal website]

[[Category:Orthopedic surgical procedures]]
[[Category:Surgery journals]]
[[Category:Monthly journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1985]]
[[Category:Academic journals associated with learned and professional societies]]