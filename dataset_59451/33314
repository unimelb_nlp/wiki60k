{{Use mdy dates|date=April 2016}}
{{Infobox bridge
| bridge_name      = Rosendale Trestle
| native_name      = 
| native_name_lang = 
| image            = Rosendale trestle early spring.jpg
| image_size       = 300px
| alt              = The trestle, photographed in April 2011
| caption          = The trestle, photographed in April 2011
| official_name    = 
| other_name       = 
| carries          = [[Wallkill Valley Rail Trail]], formerly [[Wallkill Valley Railroad]]
| crosses =
  {{plainlist | style = margin-left: 0.5em; text-indent: -0.5em; |
* [[Rondout Creek]]
* [[New York State Route 213]]
* Formerly the [[Delaware and Hudson Canal]]
  }}
| locale           = [[Rosendale, New York]], United States
| maint            = 
| id               = 
| designer         = 
| design           = [[Post truss]]<ref name="documents" />
| material         = Steel
| spans            = 9
| pierswater       = 
| mainspan         = 
| length           = {{convert|940|ft|m}}<ref name="ny299-plan" />
| width            = 6 ft (1,829 mm) ([[broad gauge]])<ref name="documents" />
| height           = 
| load             = 
| clearance        = 
| below            = {{convert|150|ft|m}} above the water{{sfn|Mabee|1995|pp=18–20}}
| traffic          = 
| builder =
  {{plainlist | style = margin-left: 0.5em; text-indent: -0.5em; |
* A. L. Dolby &amp; Company
* Waston Manufacturing Company
* [[King Bridge Company]]
  }}
| fabricator       = [[Carnegie Steel Company]]
| begin            = 1870
| complete         = 1872
| inaugurated      = April 6, 1872
| preceded         = 
| followed         = 
| heritage         = 
| collapsed        = 
| closed           = 
| toll             = 
| map_cue          = 
| map_image        = 
| map_alt          = 
| map_text         = 
| map_width        = 
| coordinates      = {{coord|41|50|36|N|74|05|18|W|type:landmark_region:US-NY|display=inline,title}}
| lat              = 
| long             = 
| extra            = [[File:WVRR 1899.jpg|300px|center|border]]<p>The original Wallkill Valley rail line, stretching from Montgomery to Kingston</p>
}}
The '''Rosendale Trestle''' is a {{convert|940|ft|m|adj=on|abbr=off|sp=us}} [[continuous truss bridge]] and former [[trestle bridge|railroad trestle]] in [[Rosendale Village, New York|Rosendale Village]], a [[Hamlet (New York)|hamlet]] in the [[Rosendale, New York|town of Rosendale]] in [[Ulster County, New York]]. Originally constructed by the [[Wallkill Valley Railroad]] to continue its rail line from [[New Paltz (village), New York|New Paltz]] to [[Kingston, New York|Kingston]], the bridge rises {{convert|150|ft|m|abbr=on}} above [[Rondout Creek]], spanning both [[New York State Route 213|Route 213]] and the former [[Delaware and Hudson Canal]]. Construction on the trestle began in late 1870, and continued until early 1872. When it opened to rail traffic on April 6, 1872, the Rosendale trestle was the highest [[Span (architecture)|span]] bridge in the United States.

The trestle was rebuilt in 1895 by the [[King Bridge Company]] to address public concerns regarding its stability, and it has been repeatedly reinforced throughout its existence. Concern over the sturdiness of the trestle has persisted since its opening, and was a major reason [[Conrail]] closed the Wallkill Valley rail line in 1977. After the rail line's closure, Conrail sold the bridge in 1986 for one dollar to a private businessman who tried unsuccessfully to operate the trestle as a [[bungee jumping]] platform in the 1990s. A similar attempt was made the following decade. The trestle was seized by the county in 2009 for [[Tax deed sale|tax nonpayment]], and renovated as a [[Footbridge|pedestrian walkway]] for the [[Wallkill Valley Rail Trail]]. The  deck and railings now continue all the way across the trestle, and access is from a parking lot about {{convert|1/4|mi|m|abbr=off|sp=us}} north on Binnewater Road.  It opened to the public with a celebration on June 29, 2013.

== History ==

=== Construction ===
[[File:Rosendale trestle cropped.jpg|thumb|left|The railroad bridge as it was originally built, prior to its 1895 reconstruction]]
In 1870, the [[Wallkill Valley Railroad]] operated trains between [[Montgomery, New York|Montgomery]] and [[New Paltz, New York]],{{sfn|Mabee|1995|p=38}} and began building a {{convert|413|ft|m|adj=on}}{{sfn|Chazin|2001|pp=289–290}} bridge south of Rosendale, at Springtown Road, to cross the [[Wallkill River]].{{sfn|Mabee|1995|p=13}} The Springtown bridge was completed by 1871, and the rail line was opened north to the [[Rosendale, New York|town of Rosendale]].{{sfn|Mabee|1995|p=38}} Rosendale issued $92,800 in [[municipal bond|bonds]] on May 13, 1869 to finance its portion of the railroad.{{sfn|Gilchrist|1976|p=87}}

Though the trestle was difficult to build,{{sfn|Best|1972|p=54}} and viewed as weak by modern standards,{{sfn|Penna|Sexton|2002|p=188}} it was remarkable for its time,{{sfn|Gilchrist|1976|p=88}} and can be considered the "most awesome part" of the Wallkill Valley rail line.{{sfn|Mabee|1995|pp=18–20}} Construction on the bridge's [[abutment]]s began in August 1870 by A. L. Dolby & Company, but work on the [[superstructure]] by the Waston Manufacturing Company did not begin until the following year due to problems with [[quicksand]] during the excavation.<ref name="bridge-opening" /><ref name="rosendale-bridge" /> Sections of the superstructure were built in [[Paterson, New Jersey]].{{sfn|Best|1972|p=31}} The bridge originally had seven [[wrought-iron]] spans and two shorter wooden spans;{{sfn|Mabee|1995|pp=18–20}} the longer spans were each{{sfn|Sylvester|1880|p=239}} {{convert|105|ft|m}} in length.{{sfn|Best|1972|p=31}} The bridge cost $250,000 to build,<ref>{{cite journal |title=The Commercial and Financial Chronicle |url=https://books.google.com/books?id=WHDlAAAAMAAJ |volume=14 |publisher=Willian B. Dana &amp; Company |date=January–June 1872 |accessdate=December 9, 2010 |location=New York, NY |page=156}}</ref> and followed a [[Post truss]] design.<ref name="documents" /> Roughly {{convert|1000|ST|MT}} of iron and {{convert|420000|board feet|lk=in}} of timber went into its construction. At the time of its completion in January 1872, it had the highest span of any bridge in the United States.<ref name="bridge-opening" /><ref name="rosendale-bridge" /> Due to its height, it could "scarcely be crossed for the first time without something like a feeling of terror".{{sfn|Sylvester|1880|p=230}} The bridge allowed the rail line to continue north to [[Kingston, New York|Kingston]].{{sfn|Mabee|1995|pp=18–20}}

=== Active rail service ===

[[File:Rosendale trestle with train and two women.jpg|thumb|left|A depiction of two women walking under the trestle, in the area known as "Dead Man's Stretch"]]
The bridge was opened on April 6, 1872 in a ceremony attended by 5,000 people,<ref name="bridge-opening" /> including prominent men from across [[Ulster County, New York|Ulster]], [[Orange County, New York|Orange]], and [[Dutchess County, New York|Dutchess]] counties. One of the spectators was James S. McEntee, [[Esquire|Esq]]., an engineer who had worked on the Delaware and Hudson Canal in 1825. He was the only person to have seen both "the passage of the first loaded boat through the canal and the first train over the bridge which spans it".<ref>{{cite news |title=Testing the Great Bridge |newspaper=Weekly Freeman |location=Rondout, NY |date=April 12, 1872 |oclc=09824069}}</ref> A [[4-4-0|4-4-0 locomotive]] with five [[boxcar]]s and two [[Passenger car (rail)|passenger cars]] made the inaugural run. Many spectators doubted the strength of the bridge, and believed that the trestle would collapse under the weight of the train. The bridge appeared unaffected by the strain, and an increasing number of people rode over the bridge during the second and third runs.{{sfn|Mabee|1995|pp=18–20}}

A. L. Dolby & Company was contracted to complete the rail line between the bridge and Kingston. The track reached the [[Kingston, New York railroad stations|Kingston Union Station]] in November 1872.{{sfn|Gilchrist|1976|p=87}} By this time, trains were running regularly to and from Kingston.{{efn|Sources say that trains were running to and from Kingston by October 1872,{{sfn|Mabee|1995|pp=18–20}} but that the rail line was not completed to Kingston until that November.{{sfn|Best|1972|p=31}}}}

By 1885, the bridge supports were reinforced and the track was converted from [[broad gauge]] to [[standard gauge]].<ref name="documents" /> In 1888, the Wallkill Valley Railroad received a permit from the town of Rosendale to "construct and maintain abutments to support [the] trestle" as long as such work did not interfere with traffic along the underlying highway (present-day [[New York State Route 213|NY 213]]). That same year, the [[Delaware and Hudson Canal]] allowed the railroad to temporarily use some of its property by the [[Rondout Creek]] to place [[Bent (structural)|bents]] for bridge repairs.<ref>{{cite web |url=http://www.dec.ny.gov/docs/legal_protection_pdf/15_10.pdf |title=Letter to John Rahl |publisher=[[New York State Department of Environmental Conservation]] |location=Albany, NY |first=Marc |last=Gerstman |date=August 14, 1992 |accessdate=December 9, 2010 |format=PDF}}</ref> The waterway beneath the trestle could be quite treacherous; so many people drowned that the area became known as "Dead Man's Stretch".{{sfn|Gilchrist|1976|p=83}} There have been reports of ghostly "apparitions" in the area, particularly of a white dog.<ref>{{cite news |title=Pages Out of the Past |newspaper=Rosendale News |location=Rosendale, NY |date=January 9, 1942 |first=Joseph |last=Fleming}}</ref>

[[File:Rosendale trestle postcard.jpg|thumb|The trestle, spanning the former [[Delaware and Hudson Canal]], as well as [[Rondout Creek]]]]
The bridge was rebuilt by the [[King Bridge Company]] between 1895 and 1896, remaining in use most of the time;<ref name="king-bridges" /> the trestle is the only railroad bridge featured in the King Bridge Company catalogs of the 1880s and 1890s that remains standing.<ref>{{cite web |url=http://www.kingbridgeco.com/railroad_special.htm |archiveurl=https://web.archive.org/web/20100908085025/http://www.kingbridgeco.com/railroad_special.htm |archivedate=September 8, 2010 |title=Working on the Railroad: The Role of the King Bridge Company |first=Allan King |last=Sloan |date=February 2005 |accessdate=December 13, 2010 |publisher=King Bridge Company}}</ref> The renovation converted the bridge's structure from iron and wood to steel to allay public concerns about its strength;{{sfn|Mabee|1995|pp=18–20}} the height of the bridge evoked collapses such as the [[Tay Bridge disaster]].{{sfn|Sylvester|1880|p=230}} The steel was provided by the [[Carnegie Steel Company]].<ref name="king-bridges" /> The renovation raised the bridge's [[Pier (architecture)|piers]] by {{convert|8|ft|m}}<ref>{{cite news |date=September 13, 1895 |title=New Paltz Times |location=New Paltz, NY}}</ref> and made the bridge straighter; the original design had a curve on the southern terminus.<ref>{{cite news |url=https://query.nytimes.com/mem/archive-free/pdf?res=F60A16FA355D15738DDDAB0994DB405B8285F0D3 |title=Notes of Various Interests |date=March 12, 1892 |accessdate=December 10, 2010 |newspaper=[[The New York Times]] |format=PDF}}</ref> One of the northern spans was completed by February 1896,<ref>{{cite news |date=February 14, 1896 |title=New Paltz Times |location=New Paltz, NY}}</ref> and the entire reconstruction was finished by June.<ref>{{cite news |date=June 5, 1896 |title=New Paltz Times |location=New Paltz, NY}}</ref> The layout of the spans was unchanged from the original 1872 design.{{sfn|Mabee|1995|pp=18–20}} Following its reconstruction, the bridge was unaffected by the shock of a large cave-in at a nearby [[Rosendale cement]] quarry on December 26, 1899,<ref>{{cite news |url=https://query.nytimes.com/mem/archive-free/pdf?res=F60612FE3D5811738DDDA10A94DA415B8985F0D3 |title=Another Cave-in at Rosendale |date=December 28, 1899 |accessdate=November 3, 2010 |newspaper=[[The New York Times]] }}</ref> though it was shaken by a nearby [[boiler explosion]] that occurred days before the collapse.{{sfn|Gilchrist|1976|pp=48, 129}}

From the time of its reconstruction to its eventual closure, passengers continued to have concerns over the trestle. The "speed, weight, and positioning of [[rolling stock]] on the bridge" was monitored,{{sfn|Mabee|1995|pp=18–20}} and it was repeatedly reinforced to "carry the ever heavier loads of modern railroading".<ref name="king-bridges" /> In the 1940s, steam engines carrying heavy loads over the bridge caused the [[Footbridge#Catwalk|catwalk]] on the west side of the bridge to shake.{{sfn|Mabee|1995|p=118}} By 1975, the rail line had deteriorated to the point where federal regulations allowed only {{convert|8|mph|km/h|adj=on}} traffic over the trestle,{{sfn|Mabee|1995|pp=134–135}} though engineers were instructed to only go as fast as {{convert|5|mph|km/h}}.{{sfn|Mabee|1995|p=124}} The sturdiness of the bridge, specifically the stability of its piers, was a deciding factor when [[Conrail]] (then-owner of the Wallkill Valley rail line) closed the Wallkill Valley Railroad in 1977.{{sfn|Mabee|1995|pp=134–135}}

=== Modern use ===
[[File:Rainy walk over the Rosendale Trestle.jpg|thumb|left|Hikers on the re-opened trestle]]
Conrail had begun taking bids on the trestle as early as 1983.<ref name="railroad-developer" /> An initial offer was made to the town of Rosendale, which refused, unwilling to accept the liability.<ref name="dollar-deal" /> Conrail sold the bridge, along with {{convert|11.5|mi|km}} of the Wallkill Valley rail corridor, in 1986 to a private businessman, John Rahl, for one dollar.{{sfn|Mabee|1995|p=144}} Rahl [[Title (property)|took title]] of the trestle and corridor on July 11, 1986.<ref name="railroad-developer" /> Included in Rahl's purchase was a [[train station]] in Rosendale's hamlet of Binnewater;<ref name="binnewater-collapse" /> the station was a part of the [[Binnewater Historic District]].<ref>{{cite web|url=http://www.oprhp.state.ny.us/hpimaging/hp_view.asp?GroupView=9703 |title=Binnewater Historic District |work=National Register of Historic Places Registration |date=September 1982 |accessdate=November 9, 2010 |first=Neil G. |last=Larson |publisher=[[New York State Office of Parks, Recreation and Historic Preservation|NYS OPRHP]] |format=Java |page=5}}</ref> A Rosendale [[homeowner association]] had tried to purchase the properties before Rahl, also for one dollar, but Conrail declined their offer.<ref name="dollar-deal" />

Rahl, born around 1948<ref name="dollar-deal" /> in [[Washingtonville, New York|Washingtonville]], was a [[construction worker]]<ref name="railroad-developer" /> and [[auto mechanic]].<ref name="dollar-deal" /> He lived near the trestle, in a "converted warehouse, whose support beams had once formed the scaffolding for the trestle crews".<ref name="railroad-developer" /> Rahl's reason for buying the rail line was originally to open a "dining car restaurant" along the corridor,<ref name="dollar-deal" /> and to establish a [[Heritage railway|tourist railroad]] from Kingston to the trestle.{{sfn|Penna|Sexton|2002|p=188}} He claimed the purchase granted him the right to "restore rail service on the whole Wallkill line",{{sfn|Mabee|1995|p=141}} and joint ownership of Conrail.<ref>{{cite web |url=http://www.wvrr.biz/19980415_STB_VERNONWILLIAMS.pdf |archiveurl=https://web.archive.org/web/20110706150046/http://www.wvrr.biz/19980415_STB_VERNONWILLIAMS.pdf |archivedate=July 6, 2011 |title=Re: Finance Docket No. 33388 Joint Ownership of Conrail |first=John |last=Rahl |date=April 15, 1998 |accessdate=November 23, 2010 |format=PDF}}</ref> Plans to restore service subsequently "didn't pan out".{{sfn|Penna|Sexton|2002|p=188}} Within one year of the purchase, Rahl sold {{convert|11|acre|ha}} of the property to a housing developer.<ref name="railroad-developer" /> On May 16, 1989, a storm caused such severe damage to the Binnewater station that part of it broke off and fell into the road;<ref name="binnewater-collapse" /> the building was subsequently demolished by the town of Rosendale's highway department.{{sfn|Mabee|1995|p=140}}

[[File:Rosendale NY.jpg|thumb|View from the trestle of [[Joppenbergh Mountain]] ''(left)'', [[New York State Route 213|NY&nbsp;213]] ''(center)'', and [[Rondout Creek]] ''(right)'']]
Between 1989 and 1991, Rahl installed planking and guard rails on the southern half of the bridge,<ref name="hase-2004" /> which was then opened to the public.{{sfn|Mabee|1995|p=144}} He allowed [[bungee jumping]] off the bridge{{sfn|Mabee|1995|p=141}}<ref name="rahl-seizure-1" /> until a January 1992 court order held that it violated [[zoning]] laws.<ref name="hase-hearing" /><ref name="hase-2003" /> One person tried to bungee jump off the bridge without a restraining cord.{{sfn|Genero|2005|p=9}} The bridge was slightly damaged by a fire in mid-1999, but it was repaired by September of that year.<ref>{{cite news |newspaper=[[Poughkeepsie Journal]] |location=Poughkeepsie, NY |date=September 25, 1999 |title=Bridge repaired just in time |first=Bond |last=Brugard}}</ref>

Douglas Hase, an [[entrepreneur]] who had run bungee jumping and [[hot air ballooning]] companies,<ref>{{cite news |url=https://www.nytimes.com/2005/10/30/fashion/weddings/30vows.html |title=Carolyn Kaplan and Douglas Hase |date=October 30, 2005 |accessdate=November 22, 2010 |first=Katie |last=Zezima |newspaper=[[The New York Times]] }}</ref><ref>{{cite news |url=http://www.bizjournals.com/boston/stories/2007/01/01/story3.html |title=Balloon operator gets hot over trade secret battle |date=December 31, 2006 |accessdate=November 22, 2010 |newspaper=[[Boston Business Journal]] |first=Brian |last=Kladko}}</ref> tried unsuccessfully in 2003 and 2004 to get a [[Variance (land use)|variance]] for another bungee jumping venture.<ref name="hase-2004" /><ref name="hase-2003" /><ref>{{cite news |url=http://www.dailyfreeman.com/articles/2004/04/16/top%20stories/11331914.txt |title=Leap of faith: Bungee jumping may come to Rosendale |date=April 16, 2004 |newspaper=[[Daily Freeman]] |location=Kingston, NY |first=Pat |last=Rowe}}</ref> During a public hearing about Hase's proposal, a county legislator began screaming to simulate the sound neighbours would hear repetitively when people jumped off the bridge.<ref name="hase-hearing" />

After Rahl failed to pay $13,716 in property taxes over a period of three years,<ref name="rahl-seizure-1" /> Ulster County foreclosed on the entire {{convert|63.34|acre|ha|adj=on}} property on April 15, 2009. The Wallkill Valley Land Trust and [[Open Space Institute|Open Space Conservancy]] offered to purchase the property, and the county authorized the sale in July.<ref>{{cite web |url=http://www.co.ulster.ny.us/resolutions/215-09.pdf |archiveurl=http://www.webcitation.org/5u9bi6M9P |archivedate=November 11, 2010 |format=PDF |title=Resolution No. 215 Authorizing The County Commissioner Of Finance To Accept Bids On Parcels Of County Owned Property To Be Used For Public Use And Benefit And Authorizing The Chairman To Convey Property To Open Space Conservancy, Inc., And Wallkill Valley Land Trust, Inc. |date=July 22, 2009 |accessdate=November 4, 2010 |publisher=Ulster County}}</ref> The sale was completed in late August 2009.<ref name="wvlt-purchase" /> The Land Trust agreed to pay all outstanding taxes before receiving full ownership and adding it to the [[Wallkill Valley Rail Trail]].<ref>{{cite web |url=http://www.wallkillvalleylt.org/index.php?option=com_content&task=view&id=102&Itemid=88 |title=New Addition to the Wallkill Valley Rail Trail |publisher=Wallkill Valley Land Trust |location=New Paltz, NY |date=August 2009 |accessdate=November 1, 2010|archiveurl=https://web.archive.org/web/20100706222345/http://www.wallkillvalleylt.org/index.php?option=com_content&task=view&id=102&Itemid=88|archivedate=July 6, 2010}}</ref> Ownership of the trestle was transferred to the Wallkill Valley Rail Trail Association.<ref name="wvlt-purchase" />

[[File:Close up of engineer on Rosendale trestle.jpg|thumb|left|An engineer from Bergmann Associates surveying the trestle in late 2010]]
Following an engineering survey by Bergmann Associates<ref>{{cite press release |url=http://www.osiny.org/site/News2?id=7799 |title=Hanging by a Thread: Engineers Start Inspection on the Railroad Bridge |publisher=[[Open Space Institute]] |date=November 15, 2010 |accessdate=November 18, 2010}}</ref>&nbsp;– the same firm that inspected the [[Poughkeepsie Bridge]] prior to its conversion to a walkway<ref>{{cite journal |url=http://bridgeweb.com/news/fullstory.php/aid/1607/Walk_back_in_time.html |title=Walk back in time |journal=Bridge Design & Engineering |publisher=Hemming Information Services |date=December 2, 2008 |issue=53 |first=Eric |last=DeLony |archiveurl=http://www.mgmclaren.com/node/365 |archivedate=December 15, 2008}}</ref>&nbsp;– the bridge was closed to the public in June 2010 for repairs.<ref>{{cite news |url=http://www.recordonline.com/apps/pbcs.dll/article?AID=/20100612/NEWS/6120320 |title=Rosendale trestle secton (sic) of Wallkill Rail Trail to be shut down for repairs |first=Adam |last=Bosch |date=June 12, 2010 |newspaper=[[Times Herald-Record]]|location=Middletown, NY |accessdate=November 1, 2010}}</ref> Renovations were originally expected to cost $750,000 and begin in early 2011, with a 12-month completion time. The [[New York State Office of Parks, Recreation and Historic Preservation]] granted $150,000 toward the renovation.<ref name="restoration-announcement" /> By November 2010, an additional $300,000 had been raised from private donors, and other sources.<ref name="work-set" /> By the end of 2010, over {{convert|10+1/2|ST}} of [[tire]]s were removed from the Rosendale section of the rail trail.<ref name="fundraising" />

The surface of the walkway was rebuilt with a [[wood-plastic composite]] built by a volunteer force.<ref>{{cite news |last=Platt |first=Frances Marion |date=June 4, 2013 |title=Renovated Rosendale trestle reopens, reconnecting long-sundered Wallkill Valley Rail Trail |url=http://www.newpaltzx.com/2013/07/04/renovated-rosendale-trestle-reopens-reconnecting-long-sundered-wallkill-valley-rail-trail/ |newspaper=New Paltz Times |accessdate=April 6, 2016}}</ref> On February 17, 2011,<ref>{{cite web |url=http://www.capitaldistricteweek.org/FLDPDF/2011/EWeek_Schedule_Jan17.pdf |title=The Capital District's Celebration of National Engineers Week 2011 |page=1 |publisher=Capital District Engineers Week |format=PDF |date=January 17, 2011 |accessdate=February 6, 2011}}</ref> a Bergmann Associates employee used the trestle as a [[case study]] in a seminar on [[adaptive reuse]] of defunct railroad bridges.<ref>{{cite web |url=http://www.capitaldistricteweek.org/FLDPDF/2011/SeminarDescriptions_Final.pdf |title=2011 E-Week Seminar Descriptions |page=10 |publisher=Capital District Engineers Week |format=PDF |date=January 19, 2011 |accessdate=February 6, 2011}}</ref> By late March 2011, the estimated cost of renovating the trestle had risen to $1.1&nbsp;million,<ref>{{cite news |url=http://www.midhudsonnews.com/News/2011/March/24/RosRRBr_fundr-24Mar11.html |title=Fundraising effort begins to renovate historic Rosendale Railroad Bridge |date=March 24, 2011 |accessdate=March 24, 2011 |newspaper=Mid-Hudson News Network |publisher=Statewide News Network, Inc.}}</ref> and the expected time to completion increased to two years.<ref name="fundraising" />

[[File:Joppenburg.jpg|thumb|The northern half of the trestle in 2008, without decking or guard rails]]A campaign to raise $500,000 for the renovation began on March 27, 2011;<ref name="fundraising" /> by June 30, about $50,000 had been raised.<ref name="matching" /> Two 2009 lawsuits brought by John E. Rahl against the [[New York Telephone|New York Telephone Company]] over alleged fees due to him for a [[Fiber-optic communication|fiber optic line]] crossing the trestle were dismissed by two lower courts (in Vermont and New York). On November 18, 2011, the [[United States Court of Appeals for the Second Circuit|US Court of Appeals for the Second Circuit]] dismissed Rahl's appeal.<ref>John E. Rahl v. New York Telephone Company, Case No. 11-2266</ref> {{As of|July 2011}}, a lawsuit brought by John Rahl over the ownership of the trestle remains pending before the Second Circuit Court. Rahl claimed that he retained ownership of the property because only the state, and not the county, had the right to seize the trestle, which was "forever railroad under 19th&nbsp;century eminent domain legal doctrines&nbsp;– long forgotten by modern jurisprudence".<ref name="matching" /> The trestle has been the site of numerous picnics, barbecues, and at least one wedding.{{sfn|Genero|2005|p=9}}

In late June 2012, contractors began welding new railings to the trestle and conducting other preparatory work for opening the walkway.<ref>{{cite web |url=http://www.townofrosendale.com/news/railings-on-the-trestle/ |title=Railings on the Trestle |publisher=Town of Rosendale |date=June 28, 2012 |accessdate=September 9, 2012}}</ref> An event at nearby Willow Kiln Park was held on June 29, 2013 to celebrate the grand opening of the trestle to the public.<ref>{{cite web |url=https://www.facebook.com/events/627770347242503/?ref=22 |title=Rosendale Trestle Grand Opening: Connecting Communities |accessdate=June 25, 2013}}</ref> The trestle was fully re-opened to the public for the first time since the rail line closed,<ref>{{cite news |url=http://www.dailyfreeman.com/articles/2010/06/12/news/doc4c13887c6d460545772608.txt |title=Groups plan rehab of Rosendale rail trestle for hikers |date=June 12, 2010 |newspaper=[[Daily Freeman]] |location=Kingston, NY}}</ref> and a {{convert|24|mi|km|adj=mid|-long}} segment of the Wallkill Valley Rail Trail from [[Gardiner, New York|Gardiner]] to Kingston was opened.<ref name="trestle-closed" />

== See also ==

{{portal|Hudson Valley|Trains}}

* [[List of crossings of Rondout Creek]]
{{clear}}

== Notes ==

{{notes}}

== References ==

{{reflist
| colwidth = 30em
| refs =

<ref name="documents">
{{cite book | url = https://books.google.com/books?id=s6MlAQAAIAAJ | title = Documents of the Senate of the State of New York | volume = 2 | issue = 8 | pages = 319–320 | location = Albany, NY | publisher = Weed, Parsons and Company | year = 1885
}}
</ref>

<ref name="ny299-plan">
{{cite web | url = http://www.co.ulster.ny.us/planning/uctc/documents/nmtp/final_plan.pdf | archiveurl = https://web.archive.org/web/20101008065004/http://www.co.ulster.ny.us/planning/uctc/documents/nmtp/final_plan.pdf | archivedate = October 8, 2010 | title = Ulster County Non-Motorized Transportation Plan | date = December 2008 | accessdate = November 7, 2010 | publisher = Ulster County Transportation Council | location = Kingston, NY | format = PDF | page = 33
}}
</ref>

<ref name="bridge-opening">
{{cite news | url = https://query.nytimes.com/mem/archive-free/pdf?res=F00916F93C5D1A7493CAA9178FD85F468784F9 | title = Opening of the Walkill Valley Railroad Bridge | date = April 8, 1872 | accessdate = November 3, 2010 | newspaper = [[The New York Times]] | format = PDF
}}
</ref>

<ref name="rosendale-bridge">
{{cite news | url = https://query.nytimes.com/mem/archive-free/pdf?res=F20814FB3C5D1A7493CBA81788D85F468784F9 | title = A Bit of Extraordinary Railroad History–The Great Rosendale Bridge of the Wallkill Valley Railroad–Other Railroad Items | date = March 19, 1872 | accessdate = November 3, 2010 | newspaper = [[The New York Times]] | format = PDF
}}
</ref>

<ref name="king-bridges">
{{cite web | url = http://www.centuryhouse.org/newsletr/summ2000/kingbrdg.html | archiveurl = https://web.archive.org/web/20111002102328/http://www.centuryhouse.org/newsletr/summ2000/kingbrdg.html | archivedate = October 2, 2011 | title = King Bridges in New York State | first = Allan King | last = Sloan | publisher = Century House Historical Society | accessdate = November 4, 2010
}}
</ref>

<ref name="railroad-developer">
{{cite news | title = Rosendale railroad owner whistles development tune | newspaper = [[Daily Freeman]] | location = Kingston, NY | date = March 22, 1987
}}
</ref>

<ref name="dollar-deal">
{{cite news | title = A dollar a deal: Buyer spends $1 for railbed title | newspaper = [[Times Herald-Record]] | location = Middletown, NY | date = July 16, 1986 | first = Wayne A. | last = Hall
}}
</ref>

<ref name="binnewater-collapse">
{{cite news | title = Old depot collapses in storm | first = Jacqueline | last = Sergeant | date = May 18, 1989 | newspaper = [[Daily Freeman]] | location = Kingston, NY
}}
</ref>

<ref name="hase-2004">
{{cite web | url = http://www.townofrosendale.com/zba2004min.cfm | archiveurl = https://web.archive.org/web/20110717081417/http://www.townofrosendale.com/zba2004min.cfm | archivedate = July 17, 2011 | title = Minutes of the April 20, 2004 Meeting | work = Zoning Board of Appeals Minutes | accessdate = November 4, 2010 | date = April 20, 2004 | publisher = Town of Rosendale
}}
</ref>

<ref name="rahl-seizure-1">
{{cite news | url = http://www.recordonline.com/apps/pbcs.dll/article?AID=/20090601/COMM/906010315/-1/NEWS | title = Wallkill Rail Trail could double in size | first = Adam | last = Bosch | date = June 1, 2009 | newspaper = [[Times Herald-Record]] | location = Middletown, NY | accessdate = November 1, 2010
}}
</ref>

<ref name="hase-hearing">
{{cite news | url = http://www.dailyfreeman.com/articles/2004/04/21/regional%20news/11356577.txt | title = Foes jump all over Rosendale bungee plan | date = April 21, 2004 | accessdate = November 22, 2010 | first = Pat | last = Rowe | newspaper = [[Daily Freeman]] | location = Kingston, NY}}
</ref>

<ref name="hase-2003">
{{cite web | url = http://www.townofrosendale.com/zba2003min.cfm | archiveurl = https://web.archive.org/web/20100822081552/http://www.townofrosendale.com/zba2003min.cfm | archivedate = August 22, 2010 | title = Minutes of the May 20, 2003 Meeting | work = Zoning Board of Appeals Minutes | accessdate = November 4, 2010 | date = December 16, 2003 | publisher = Town of Rosendale
}}
</ref>

<ref name="wvlt-purchase">
{{cite news | url = http://www.ellenvillejournal.com/2009/10/15/news/0910156.html | title = Wallkill Valley Land Trust: Rail trails, trestle bridges and the Pine Hole Bog | newspaper = Shawangunk Journal | first = Chris | last = Rowley | publisher = Electric Valley Media | location = Ellenville, NY | date = October 15, 2009 | accessdate = February 11, 2011
}}
</ref>

<ref name="restoration-announcement">
{{cite news | url = http://www.midhudsonnews.com/News/2010/November/15/RndCrBr_rest0re-15Nov10.html | title = Walkway Over the Rondout to be restored | date = November 15, 2010 | accessdate = November 15, 2010 | newspaper = Mid-Hudson News Network | publisher = Statewide News Network, Inc.
}}
</ref>

<ref name="work-set">
{{cite news | title = Work set to preserve historic Rosendale train trestle | first = Maria | last = Farr | date = November 19, 2010 | newspaper = Blue Stone Press | location = Stone Ridge, NY
}}
</ref>

<ref name="fundraising">
{{cite news | url = http://www.ulsterpublishing.com/view/full_story/12572975/article--Bridging-the-gaps-Wallkill-Valley-Rail-Trail-Association-starts-raising-money-to-refurbish-trestle- | title = Bridging the gaps: Wallkill Valley Rail Trail Association starts raising money to refurbish trestle | date = March 31, 2011 | accessdate = July 9, 2011 | newspaper = New Paltz Times | publisher = [[Ulster Publishing]] | location = Kingston, NY | first = Carrie Jones | last = Ross
}}
</ref>

<ref name="trestle-closed">
{{cite news | url = http://www.dailyfreeman.com/articles/2010/06/13/news/doc4c1468b386069361628178.txt | title = Rosendale trestle closed for improvements | newspaper = [[Daily Freeman]] | location = Kingston, NY | date = June 13, 2010
}}
</ref>

<ref name="matching">
{{cite news | url = http://ellenvillejournal.com/2011/07/07/news/1107074.html | title = Rosendale Trestle Wins Matching Funds | date = July 7, 2011 | accessdate = July 9, 2011 | newspaper = Shawangunk Journal | publisher = Electric Valley Media LLC | location = Ellenville, NY | first = Rob | last = Walters
}}
</ref>

}}

== Bibliography ==

* {{cite book | last = Best | first = Gerald M. | title = The Ulster And Delaware: Railroad Through The Catskills | publisher = [[Golden West Books]] | location = San Marino, CA | year = 1972 | isbn = 978-0-87095-041-4 | edition = 5th | ref = harv }}
* {{cite book | title = New York Walk Book: A Companion to the New Jersey Walk Book | first = Daniel D. | last = Chazin | year = 2001 | publisher = [[New York–New Jersey Trail Conference]] | location = Mahwah, NJ | edition = 7th | isbn = 978-1-880775-30-1 | ref = harv }}
* {{cite book | title = Thank Rosendale: New York&nbsp;– The Empire State | first = Peter P. | last = Genero | isbn = 978-0-9759419-1-1 | year = 2005 | publisher = Genero Inc. | location = Fort Pierce, FL | ref = harv }}
* {{cite book | last = Gilchrist | first = Ann | title = Footsteps Across Cement: A History of the Township of Rosendale, New York | publisher = Lith Art | location = Woodstock, NY | year = 1976 | oclc = 2597851 | ref = harv }}
* {{cite book | last = Mabee | first = Carleton | title = Listen to the Whistle: An Anecdotal History of the Wallkill Valley Railroad | publisher = Purple Mountain Press | location = Fleishmanns, NY | year = 1995 | isbn = 978-0-935796-69-8 | ref = harv }}
* {{cite book | last1 = Penna | first1 = Craig Della | last2 = Sexton | first2 = Tom | title = The Official Rails-to-Trails Conservancy Guidebook | year = 2002 | publisher = Globe Pequot Press | location = Guilford, CT | isbn = 978-0-7627-0450-7 | ref = harv }}
* {{cite book | last = Sylvester | first = Nathaniel Bartlett | title = History of Ulster County, New York, with Illustrations and Biographical Sketches of its Prominent Men and Pioneers: Part Second: History of the Towns of Ulster County | publisher = Everts &amp; Peck | location = Philadelphia, PA | year = 1880 | oclc = 2385957 | ref = harv }}

== External links ==
{{commons category|Rosendale Trestle}}
* [http://www.wallkillvalleylt.org/ Wallkill Valley Land Trust]&nbsp;– current owner of the trestle
* [http://www.gorailtrail.com/ Wallkill Valley Rail Trail Association]&nbsp;– maintains the adjoining rail trail

{{Rail trails in New York}}
{{Featured article}}

[[Category:Bridges completed in 1872]]
[[Category:Bridges in Ulster County, New York]]
[[Category:Continuous truss bridges]]
[[Category:King Bridge Company]]
[[Category:Parks in Ulster County, New York]]
[[Category:Rail trail bridges in the United States]]
[[Category:Rail trails in New York]]
[[Category:Railroad bridges in New York]]
[[Category:New York Central Railroad bridges]]
[[Category:Rosendale, New York]]
[[Category:Viaducts in the United States]]
[[Category:Wallkill Valley Railroad]]
[[Category:Pedestrian bridges in New York]]
[[Category:Bridges over Rondout Creek]]