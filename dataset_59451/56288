{{Infobox journal
| title = Clinical Pharmacology & Therapeutics
| cover =
| editor = Scott Waldman
| discipline = [[Pharmacology]]
| former_names =
| abbreviation = Clin. Pharmacol. Ther.
| publisher = [[Wiley-Blackwell]]
| country =
| frequency = Monthly
| history = 1960-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 7.903
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1532-6535
| link1 =
| link1-name =
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1532-6535/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 1554939
| LCCN = 61066293
| CODEN = CLPTAT
| ISSN = 0009-9236
| eISSN = 1532-6535
}}
'''''Clinical Pharmacology & Therapeutics''''' is a monthly [[peer-reviewed]] [[medical journal]] which covers research on the nature, action, efficacy, and evaluation of [[therapeutics]]. The [[editor-in-chief]] is Scott Waldman ([[Thomas Jefferson University]]). The journal was established in 1960 and is published by [[Wiley-Blackwell]]. It is the official journal of the [[American Society for Clinical Pharmacology & Therapeutics]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]
* [[Current Contents]]/Clinical Medicine
* Current Contents/Life Sciences
* [[BIOSIS Previews]]
* [[EBSCO Information Services|EBSCO databases]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index]]
* [[Scopus]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 7.903, ranking it 9th out of 254 journals in the category "Pharmacology & Pharmacy".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Pharmacology & Pharmacy |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1532-6535}}

{{DEFAULTSORT:Clinical Pharmacology And Therapeutics}}
[[Category:Pharmacology journals]]
[[Category:Monthly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1960]]
[[Category:English-language journals]]


{{pharma-journal-stub}}