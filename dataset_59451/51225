{{Infobox journal
| title = Environs: Environmental Law and Policy Journal
| cover = 
| abbreviation = Environs
| discipline = [[Environmental law]]
| editors = Sean Drake, Laura Flynn
| publisher = [[University of California, Davis]]
| history = 1977-present
| frequency = Biannually
| website = http://environs.law.ucdavis.edu/
| impact =
| link1-name =
| link2 =
| ISSN = 0193-6387
| eISSN = 
| OCLC = 05225494
| LCCN = 79643414
| JSTOR = 
}}
'''''Environs: Environmental Law and Policy Journal''''' (''[[Bluebook]]'' abbreviation: ''Environs: Envtl. L. & Pol'y J.''), also known by its abbreviated title, '''''Environs''''', is a student-run [[law review]] published at the [[University of California, Davis School of Law]]. The journal primarily covers [[environmental law|environmental law and policy]] and related subjects with a regional focus in [[California]].<ref>{{smallcaps|Environs: Environmental Law and Policy Journal}}, [http://environs.law.ucdavis.edu//journal homepage].</ref>

== Overview ==
The journal was established in 1977 as ''Environs'', a "clearinghouse for the dissemination of all environmental information relevant to the [[Solano County|Solano]]-[[Yolo County|Yolo]]-[[Sacramento County|Sacramento]] tri-county area."<ref>Sam Imperati, ''Introduction'', [http://environs.law.ucdavis.edu/volumes/01/1/environs.pdf 1 {{smallcaps|Environs}} 1] (1977).</ref> Early editions of the journal were published in a "layperson-oriented style" and did not utilize "[[legalese]]."<ref name = Patton>Cynthia Patton, ''Letter from the Editor: Environs and the Future'', [http://environs.law.ucdavis.edu/volumes/13/1/editor_note/patton.pdf 13 {{smallcaps|Environs: Envtl. L. & Pol'y J.}} 3] (1989-1990).</ref> In 1989, the journal obtained its current name to reflect its broader focus and the inclusion of "non-legal policy articles" in future publications.<ref name = Patton/> In 2016, [[Washington and Lee University]]'s Law Journal Rankings placed ''Environs'' among the top thirty environmental, natural resources, and land use law journals with the highest [[impact factor]].<ref name=wl>[http://lawlib.wlu.edu/LJ/index.aspx "Law Journals: Submission and Ranking, 2008-2015"], Washington & Lee University (Accessed: September 25, 2016).</ref>

== Abstracting and indexing ==
The journal is abstracted or indexed in [[HeinOnline]], [[LexisNexis]], [[Westlaw]],<ref name = Finder>Washington and Lee University Law Library, [http://lawlib.wlu.edu/resolver.aspx?title=Environs:%20Environmental%20Law%20and%20Policy%20Journal&issn=0193-6387/ Journal Finder: Environs: Environmental Law and Policy Journal].</ref> and the [[University of Washington]]'s Current Index to Legal Periodicals.<ref>University of Washington Gallagher Law Library, [https://lib.law.washington.edu/cilp/period.html Periodicals Indexed in CLIP].</ref> Tables of contents are also available through Infotrieve and [[Ingenta]].<ref name=Finder/>

== See also ==
* [[List of law journals]]
* [[List of environmental law journals]]

== References ==
{{cbignore}}
{{reflist|30em}}

== External links ==
* {{Official website|http://environs.law.ucdavis.edu/}}

{{DEFAULTSORT:Environs: Environmental Law And Policy Journal}}
[[Category:American law journals]]
[[Category:Publications established in 1977]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Environmental law journals]]
[[Category:Law journals edited by students]]