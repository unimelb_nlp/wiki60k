{{Infobox journal
| title = Canadian Journal of Gastroenterology & Hepatology
| formernames = Canadian Journal of Gastroenterology
| cover = 
| editors = John K. Marshall, Eric M. Yoshida
| discipline = [[Gastroenterology]], [[Hepatology]]
| language = English, French
| abbreviation = Can. J. Gastroenterol. Hepatol.
| publisher = [[Pulsus Group]]
| country =
| frequency = Monthly
| history = 1987-present
| openaccess = 
| impact = 1.966
| impact-year = 2014
| website = http://www.pulsus.com/journals/journalHome.jsp?sCurrPg=journal&jnlKy=2&fold=Home
| link1 = http://www.pulsus.com/journals/toc.jsp?sCurrPg=journal&jnlKy=2&fold=Current%20Issue
| link1-name = Online access
| link2 = http://www.pulsus.com/journals/past_issues.jsp?sCurrPg=journal&jnlKy=2&fold=Past%20Issues
| link2-name = Online archive
| JSTOR = 
| OCLC = 878931900
| LCCN = 
| CODEN = 
| ISSN = 2291-2789
| eISSN = 2291-2797
}}
'''''Canadian Journal of Gastroenterology & Hepatology''''' is a monthly [[peer-reviewed]] [[medical journal]] covering all aspects of [[gastroenterology]] and [[liver disease]]. It is the official journal of the [[Canadian Association of Gastroenterology]] and the [[Canadian Association for the Study of the Liver]] and is published by the [[Pulsus Group]], which is on [[Jeffrey Beall]]'s list of "Potential, possible, or probable" [[Predatory open access publishing|predatory open-access publishers]].<ref>{{cite web |url=http://scholarlyoa.com/publishers/ |title=LIST OF PUBLISHERS |work=Scholarly Open Access |last1=Beall |first1=Jeffrey |authorlink=Jeffrey Beall |accessdate=2016-09-21}}</ref> It was established in 1987 as the ''Canadian Journal of Gastroenterology'' and obtained its current name in 2014.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101623613 |title=Canadian Journal of Gastroenterology & Hepatology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-07-30}}</ref>
* [[Current Contents]]/Clinical Medicine<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-30}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI/>
* [[Embase|Embase/Excerpta Medica]] <ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2015-07-30}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-30}}</ref>
* [[ProQuest|ProQuest databases]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.966.<ref name=WoS>{{cite book |year=2015 |chapter=Canadian Journal of Gastroenterology & Hepatology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.pulsus.com/journals/journalHome.jsp?sCurrPg=journal&jnlKy=2&fold=Home}}

{{authority control}}
{{DEFAULTSORT:Canadian Journal of Gastroenterology and Hepatology}}
[[Category:Gastroenterology and hepatology journals]]
[[Category:Monthly journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1987]]
[[Category:Pulsus Group academic journals]]


{{med-journal-stub}}