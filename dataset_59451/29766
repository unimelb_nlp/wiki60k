{{featured article}}

{|{{Infobox ship begin}}
{{Infobox ship image
| Ship image=[[File:Battleship Andrea Doria.png|300px]]
| Ship caption=''Andrea Doria'' during World War I
}}
{{Infobox ship class overview
| Operators=*{{navy|Kingdom of Italy}}
*{{navy|Italy}}
| Class before={{sclass-|Conte di Cavour|battleship|4}}
| Class after=*{{sclass-|Francesco Caracciolo|battleship|4}} (planned)
*{{sclass-|Littorio|battleship|4}} (actual)
| Built range=1912–16
| In service range=1915–53
| Total ships completed=2
| Total ships scrapped=2
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(as built)
|Ship type=[[Dreadnought battleship]]
|Ship displacement={{convert|24729|LT|t|abbr=on}} ([[deep load]])
|Ship length={{convert|176|m|ftin|abbr=on}} ([[Length overall|o/a]])
|Ship beam={{convert|28|m|ftin|abbr=on}}
|Ship draft={{convert|9.4|m|ftin|abbr=on}}
|Ship power=*{{convert|30000|shp|abbr=on}}
*20 × [[Yarrow boiler]]s
|Ship propulsion=*4 × shafts
*4 × [[Parsons Marine Steam Turbine Company|Parsons]] [[steam turbine]] sets
|Ship speed={{convert|21|kn|lk=in}}
|Ship range={{convert|4800|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
|Ship complement=*31 officers
*969 enlisted men
|Ship armament=*3 × triple, 2 × twin [[305 mm /46 Model 1909|{{convert|305|mm|in|0|abbr=on}} guns]]
*16 × single {{convert|152|mm|in|0|abbr=on}} guns
*19 × single {{convert|76|mm|in|0|abbr=on}} guns
*3 × {{convert|450|mm|in|1|abbr=on}} [[torpedo]] tubes
|Ship armor=*[[Belt armor|Belt]]: {{convert|250|mm|abbr=on}}
*[[Gun turret]]s: {{convert|280|mm|1|abbr=on}}
*[[Casemate]]s: {{convert|130|mm|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|98|mm|abbr=on}}
*[[Conning tower]]: {{convert|280|mm|1|abbr=on}}
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(after reconstruction)
|Ship displacement={{convert|28882|-|29391|LT|t}} (deep load)
|Ship length={{convert|186.9|m|ftin|abbr=on}}
|Ship beam={{convert|28.03|m|ftin|abbr=on}}
|Ship draft={{convert|10.3|m|ftin|abbr=on}}
|Ship power=*{{convert|75000|shp|abbr=on}}
*8 × Yarrow boilers
|Ship propulsion=*2 × shafts
*2 × geared steam turbines
|Ship speed={{convert|26|kn}}
|Ship range={{convert|4000|nmi|abbr=on}} at {{convert|18|kn}}
|Ship complement=1,520
|Ship armament=* 2 × triple, 2 × twin [[320 mm Model 1934 naval gun|320 mm Model 1934]] guns
* 4 × triple [[135 mm /45 Italian naval gun|135 mm /45]] guns
* 10 × single [[Cannone da 90/53#Naval version|90 mm /50]] [[AA gun]]s
* 6 × twin, 3 × single [[Cannone-Mitragliera da 37/54 (Breda)|37 mm Breda]] AA guns
* 8 × twin, [[Breda Model 35|20 mm Breda]] AA guns 
|Ship armor=
|Ship notes=
}}
|}

The '''''Andrea Doria'' class''' (usually called '''''Caio Duilio'' class''' in Italian sources) was a pair of [[dreadnought battleship]]s built for the [[Royal Italian Navy]] (''Regia Marina'') during the early 1910s. The two ships—[[Italian battleship Andrea Doria|''Andrea Doria'']] and [[Italian battleship Caio Duilio|''Caio Duilio'']]—were completed during [[World War I]]. The class was an incremental improvement over the preceding {{sclass-|Conte di Cavour|battleship|4}}. Like the earlier ships, ''Andrea Doria'' and ''Caio Duilio'' were armed with a main battery of thirteen {{convert|305|mm|adj=on|sp=us}} guns.

The two ships were based in southern Italy during World War I to help ensure that the [[Austro-Hungarian Navy]] surface fleet would be contained in the [[Adriatic Sea|Adriatic]]. Neither vessel saw any combat during the conflict. After the war, they cruised the [[Mediterranean Sea|Mediterranean]] and were involved in several international incidents, including at [[Corfu Incident|Corfu in 1923]]. In 1933, both ships were placed in [[Reserve fleet|reserve]]. In 1937 the ships began a lengthy reconstruction. The modifications included removing their center main battery turret and boring out the rest of the guns to {{convert|320|mm|abbr=on|1}}, strengthening their armor protection, installing new [[boiler (steam generator)|boiler]]s and [[steam turbine]]s, and lengthening their hulls. The reconstruction work lasted until 1940, by which time Italy was already engaged in [[World War II]].

The two ships were moored in [[Taranto]] on the night of 11/12 November 1940 when the British launched a carrier strike on the Italian fleet. In the resulting [[Battle of Taranto]], ''Caio Duilio'' was hit by a torpedo and forced to beach to avoid sinking. ''Andrea Doria'' was undamaged in the raid; repairs for ''Caio Duilio'' lasted until May 1941. Both ships escorted convoys to North Africa in late 1941, including Operation M42, where ''Andrea Doria'' saw action at the inconclusive [[First Battle of Sirte]] on 17 December. Fuel shortages curtailed further activity in 1942 and 1943, and both ships were interned at [[Malta]] following Italy's surrender in September 1943. Italy was permitted to retain both battleships after the war, and they alternated as fleet [[flagship]] until the early 1950s, when they were removed from active service. Both ships were [[ship breaking|scrapped]] after 1956.

==Design and description==
[[File:Andrea Doria class battleship diagrams Brasseys 1923.jpg|thumb|left|<center>Right elevation and deck plan as depicted in Brassey's Naval Annual 1923</center>]]
The ''Andrea Doria''-class ships were designed by [[naval architect]] [[Vice Admiral]] (''Generale del Genio navale'') [[Giuseppe Valsecchi]] and were ordered in response to French plans to build the {{sclass-|Bretagne|battleship|2}}s. The design of the preceding {{sclass-|Conte di Cavour|battleship}}s was generally satisfactory and was adopted with some minor changes. These mostly concerned the reduction of the [[superstructure]] by shortening the [[forecastle]] deck, the consequent lowering of the [[amidships]] [[gun turret]] and the upgrading of the [[Battleship secondary armament|secondary armament]] to sixteen {{convert|152|mm|in|0|sp=us|adj=on}} guns in lieu of the eighteen {{convert|120|mm|in|1|sp=us|adj=on}} guns of the older ships.<ref name=g8>Giorgerini, p. 278</ref>

===General characteristics===
The ships of the ''Andrea Doria'' class were {{convert|168.9|m|ftin|sp=us}} [[length at the waterline|long at the waterline]], and {{convert|176|m|ftin|sp=us}} [[length overall|overall]]. They had a [[beam (nautical)|beam]] of {{convert|28|m|ftin|sp=us}}, and a [[draft (hull)|draft]] of {{convert|9.4|m|ftin|sp=us}}. They displaced {{convert|22956|LT|t}} at normal load, and {{convert|24729|LT|t}} at [[deep load]].<ref name=gg0>Gardiner & Gray, p. 260</ref> They were provided with a complete [[double bottom]] and their hulls were subdivided by 23 longitudinal and transverse [[bulkhead (partition)|bulkhead]]s. The ships had two [[rudder]]s, both on the centerline. They had a crew of 31 officers and 969 enlisted men.<ref name=g02>Giorgerini, pp. 270, 272</ref>

===Propulsion===
The ships were fitted with three [[Parsons Marine Steam Turbine Company|Parsons]] [[steam turbine]] sets, arranged in three engine rooms. The center engine room housed one set of turbines that drove the two inner [[propeller shaft]]s. It was flanked by compartments on either side, each housing one turbine set powering the outer shafts. Steam for the turbines was provided by 20 [[Yarrow boiler]]s, 8 of which burned oil and 12 of which burned coal sprayed with oil. Designed to reach a maximum speed of {{convert|22|kn|lk=in}} from {{convert|32000|shp|lk=in}}, neither of the ships reached this goal on their [[sea trial]]s, only achieving speeds of {{convert|21|to|21.3|kn}}. The ships could store a maximum of {{convert|1488|LT|t}} of coal and {{convert|886|LT|t}} of [[fuel oil]] that gave them a range of {{convert|4800|nmi|lk=in}} at {{convert|10|kn}}.<ref>Giorgerini, pp. 272–73, 278</ref>

===Armament===
As built, the ships' main armament comprised thirteen 46-[[Caliber#Caliber as measurement of length|caliber]] [[305 mm /46 Model 1909|305-millimeter guns]],<ref name=p9>Preston, p. 179</ref> designed by [[Armstrong Whitworth]] and [[Vickers]],<ref>Friedman, p. 234</ref> in five gun turrets. The turrets were all on the centerline, with a twin-gun turret [[superfire|superfiring]] over a triple-gun turret in fore and aft pairs, and a third triple turret amidships, designated 'A', 'B', 'Q', 'X', and 'Y' from front to rear. The turrets had an elevation capability of −5 to +20 degrees and the ships could carry 88 [[Cartridge (firearms)|rounds]] for each gun. Sources disagree regarding these guns' performance, but naval historian Giorgio Giorgerini says that they fired {{convert|452|kg|lb|adj=on}} [[Armor-piercing shot and shell|armor-piercing (AP)]] projectiles at the rate of one round per minute and that they had a [[muzzle velocity]] of {{convert|840|m/s|ft/s|abbr=on}}, which gave a maximum range of {{convert|24000|m|yd|sp=us}}.<ref name=g86>Giorgerini, pp. 268, 276, 278</ref>{{refn|Friedman provides a variety of sources that show armor-piercing shell weights ranging from {{convert|919.16|to|997.2|lb|kg|disp=flip}} and muzzle velocities around {{convert|861|m/s|ft/s|abbr=on}}.<ref>Friedman, pp. 233–34</ref>|group=Note}}

The secondary armament on the two ships consisted of sixteen 45-caliber {{convert|152|mm|in|adj=on|sp=us|0}} guns, also designed by Armstrong Whitworth,<ref>Friedman, p. 240</ref> mounted in [[casemate]]s on the sides of the hull underneath the main guns. Their positions tended to be wet in heavy seas, especially the rear guns. These guns could depress to −5 degrees and had a maximum elevation of +20 degrees; they had a rate of fire of six shots per minute. They could fire a {{convert|22.1|kg|lb|adj=on}} high-explosive projectile with a muzzle velocity of {{convert|830|m/s|ft/s|sp=us}} to a maximum distance of {{convert|17000|yd|m|disp=flip|sp=us}}. The ships carried 3,440 rounds for them. For defense against [[torpedo boat]]s, the ships carried nineteen 50-caliber {{convert|76|mm|abbr=on}} guns; they could be mounted in 39 different positions, including on the turret roofs and upper decks. These guns had the same range of elevation as the secondary guns, and their rate of fire was higher at 10 rounds per minute. They fired a {{convert|6|kg|lb|adj=on}} AP projectile with a muzzle velocity of {{convert|815|m/s|ft/s|sp=us}} to a maximum distance of {{convert|10000|yd|m|disp=flip|sp=us}}. The ships were also fitted with three submerged {{convert|45|cm|in|adj=on|sp=us|1}} [[torpedo tube]]s, one on each [[broadside]] and the third in the stern.<ref>Giorgerini, pp. 268, 277–78</ref>

===Armor===
The ''Andrea Doria''-class ships had a complete [[waterline]] armor belt with a maximum thickness of {{convert|250|mm|in|1|sp=us}} that reduced to {{convert|130|mm|in|sp=us|1}} towards the stern and {{convert|80|mm|in|sp=us|1}} towards the bow.<ref name=w2>Whitley, p. 162</ref> Above the main belt was a [[strake]] of armor {{convert|220|mm|in|sp=us|1}} thick that extended up to the lower edge of the main deck. Above this strake was a thinner one, 130 millimeters thick, that protected the casemates. The ships had two armored [[deck (ship)|deck]]s: the main deck was {{convert|24|mm|abbr=on}} thick in two layers on the flat that increased to {{convert|40|mm|sp=us|1}} on the slopes that connected it to the main belt. The second deck was {{convert|29|mm|sp=us|1}} thick, also in two layers. Fore and aft transverse bulkheads connected the belt to the decks.<ref name=g1>Giorgerini, p. 271</ref>

The frontal protection of the [[gun turrets]] was {{convert|280|mm|in|1|sp=us}} in thickness with {{convert|240|mm|in|adj=on|sp=us|1}} thick sides, and an {{convert|85|mm|in|adj=on|sp=us}} roof and rear. Their [[barbette]]s had {{convert|230|mm|in|1|sp=us|adj=on}} armor above the deck that reduced to {{convert|180|mm|in|sp=us|1}} between the forecastle and upper decks and 130 millimeters below the upper deck. The forward [[conning tower]] had walls {{convert|320|mm|in|1|sp=us}} thick; those of the aft conning tower were {{convert|160|mm|in|1|sp=us}} thick.<ref name=g1/>

==Modifications and reconstruction==
[[File:Italian battleship Andrea Doria.jpg|thumb|''Andrea Doria'' in 1943]]
During World War I, a pair of 50-caliber 76-millimeter guns on high-angle mounts were fitted as [[anti-aircraft gun|anti-aircraft (AA) guns]], one gun at the bow and the other on top of 'X' turret. In 1925 the number of 50-caliber 76-millimeter guns was reduced to 13, all mounted on the turret tops, and six new [[Cannon 76/40 Model 1916|40-caliber 76-millimeter guns]] were installed abreast the aft [[funnel (ship)|funnel]]. Two [[license-built]] [[QF 2 pounder naval gun|2-pounder]] AA guns were also fitted. In 1926 the [[rangefinder]]s were upgraded and a fixed [[aircraft catapult]] was mounted on the port side of the forecastle for a [[Macchi M.18]] [[seaplane]].<ref>Whitley, p. 164</ref>

By the early 1930s, the ''Regia Marina'' had begun design work on the new {{sclass-|Littorio|battleship|1}}s, but it recognized that they would not be complete for some time. As a stop-gap measure in response to the new French {{sclass-|Dunkerque|battleship|1}}s, the navy decided to modernize its old battleships; work on the two surviving ''Conte di Cavour''s began in 1933 and the two ''Andrea Doria''s followed in 1937.<ref>Garzke & Dulin, p. 379</ref> The work lasted until July 1940 for ''Duilio'' and October 1940 for ''Andrea Doria''. The existing bow was dismantled and a new, longer, bow section was built, which increased their overall length by {{convert|10.91|m|ftin|sp=us}} to {{convert|186.9|m|ftin|sp=us}} (on the ''Cavour''-class the new bow had been grafted over the existing one, instead). Their beam increased to {{convert|28.03|m|ftin|sp=us}}<ref>Whitley, pp. 162, 164</ref> and their draft at deep load increased to {{convert|10.3|m|ftin|sp=us}}.<ref name=b2>Brescia, p. 62</ref> The changes made during their reconstruction increased their displacement to {{convert|28882|LT|t}} for ''Andrea Doria'' and {{convert|29391|LT|t}} for ''Duilio'' at deep load.<ref name=w2/> The ships' crews increased to 70 officers and 1,450 enlisted men.<ref name=b2/>

Two of the propeller shafts were removed and the existing turbines were replaced by two sets of Belluzzo geared steam turbines rated at {{convert|75000|shp|abbr=on}}. The boilers were replaced by eight [[superheated]] Yarrow boilers. On their sea trials the ships reached a speed of {{convert|26.9|-|27|kn}}, although their maximum speed was about {{convert|26|kn}} in service. The ships now carried {{convert|2530|LT|t}} of fuel oil, which provided them with a range of {{convert|4000|nmi}} at a speed of {{convert|18|kn}}.<ref name=b2/>

The center turret and the torpedo tubes were removed and all of the existing secondary armament and AA guns were replaced by a dozen {{convert|135|mm|in|1|sp=us|adj=on}} guns in four triple-gun turrets and ten {{convert|90|mm|in|adj=on|sp=us}} AA guns in single turrets. In addition the ships were fitted with fifteen 54-caliber [[Società Italiana Ernesto Breda|Breda]] [[Cannone-Mitragliera da 37/54 (Breda)|{{convert|37|mm|sp=us|adj=on|1}}]] light AA guns in six twin-gun and three single mounts and sixteen {{convert|20|mm|sp=us|adj=on|1}} [[Breda Model 35]] AA guns, also in twin mounts. The 305-millimeter guns were bored out to 320 millimeters (12.6&nbsp;in) and their turrets were modified to use electric power. They had a fixed loading angle of +12 degrees, but there is uncertainty on their new maximum elevation, with some sources citing a maximum value of +27 degrees,<ref>Whitley, pp. 158, 164–65</ref> while others claim one of +30 degrees.<ref>Campbell, p. 324</ref> The 320-millimeter AP shells weighed {{convert|525|kg|sp=us}} and had a maximum range of {{convert|28600|m|yd|sp=us}} with a muzzle velocity of {{convert|830|m/s|ft/s|abbr=on}}.<ref>Campbell, p. 322</ref> In early 1942 the rearmost 20-millimeter mounts were replaced by twin 37-millimeter gun mounts and the 20-millimeter guns were moved to the roof of Turret 'B', while the RPC motors from the stabilized mounts of the 90&nbsp;mm guns were removed<ref name=W165>Whitley, p. 165</ref><ref>Campbell, p. 343</ref> The forward superstructure was rebuilt with a new forward conning tower, protected with {{convert|260|mm|in|sp=us|adj=on|1}} thick armor. Atop the conning tower there was a [[Fire-control system#Naval fire control|fire-control director]] fitted with three large [[rangefinders]].<ref name="b2"/>

The deck armor was increased during reconstruction to a total of {{convert|135|mm|in|sp=us}}. The armor protecting the secondary turrets was {{convert|120|mm|in|sp=us}} thick.<ref name=b2/> The existing underwater protection was replaced by the [[Pugliese torpedo defense system|Pugliese system]] that consisted of a large cylinder surrounded by fuel oil or water that was intended to absorb the blast of a torpedo [[warhead]].<ref>Whitley, p. 158</ref>

These modernizations have been criticized by some naval historians, given that not only these ships would eventually prove to be inferior to the British battleships they were meant to face (namely the [[Queen Elizabeth Class battleship|''Queen Elizabeth''-class]]), since by the time the decision to proceed was taken a war between Italy and the United Kingdom seemed more likely, but also because the cost of the reconstruction would be not much less than the cost of building a brand new ''Littorio''-class battleship; moreover, the reconstruction work caused bottlenecks in the providing of steel plates, that caused substantial delays in the construction of the modern battleships, which otherwise might have been completed at an earlier date.<ref>{{cite web|last1=De Toro|first1=Augusto|title=DALLE "LITTORIO" ALLE "IMPERO" - Navi da battaglia, studi e programmi navali in Italia nella seconda metà degli anni Trenta|url=http://www.marina.difesa.it/conosciamoci/editoria/bollettino/Documents/2012/DE_TORO_2.pdf|website=Marina Militare|accessdate=6 May 2015}}</ref>

==Ships==
{|class="wikitable plainrowheaders" border="1"
|-
! scope="col" | Ship
! scope="col" | Namesake
! scope="col" | Builder<ref name=p9/>
! scope="col" | Laid down<ref name=p9>Preston, p. 179</ref>
! scope="col" | Launched<ref name=p9/>
! scope="col" | Completed<ref name=gg0/>
! scope="col" | Fate
|-
! scope="row" | {{ship|Italian battleship|Andrea Doria||2}}
| [[Admiral]] [[Andrea Doria]]<ref name="Silverstone 294">Silverstone, p. 294</ref>
| [[La Spezia Arsenale]], [[La Spezia]]
| 24 March 1912
| 30 March 1913
| 13 March 1916
| Scrapped, 1961<ref name="Silverstone 294"/>
|-
! scope="row" | {{ship|Italian battleship|Caio Duilio||2}}
| [[Gaius Duilius]]<ref>Silverstone, p. 297</ref>
| [[Regio Cantiere di Castellammare di Stabia]], [[Castellammare di Stabia]]
| 24 February 1912
| 24 April 1913
| 10 May 1915
| Scrapped, 1957<ref>Silverstone, p. 296</ref>
|}

==Service history==
[[File:Italian battleship Andrea Doria gunnery training.png|thumb|''Andrea Doria'' on gunnery drills during World War I]]
Both battleships were completed after Italy entered World War I on the side of the [[Triple Entente]], though neither saw action, since Italy's principal naval opponent, the [[Austro-Hungarian Navy]], largely remained in port for the duration of the war.<ref name=W165/> Admiral [[Paolo Thaon di Revel]], the Italian naval chief of staff, believed that Austro-Hungarian [[submarine]]s and minelayers could operate effectively in the narrow waters of the Adriatic.<ref name="Halpern 150">Halpern, p. 150</ref> The threat from these underwater weapons to his [[capital ship]]s was too serious for him to use the fleet in an active way.<ref name="Halpern 150"/> Instead, Revel decided to implement a blockade at the relatively safer southern end of the Adriatic with the battle fleet, while smaller vessels, such as the [[MAS (boat)|MAS torpedo boats]], conducted raids on Austro-Hungarian ships and installations. Meanwhile, Revel's battleships would be preserved to confront the Austro-Hungarian battle fleet in the event that it sought a decisive engagement.<ref>Halpern, pp. 141–42</ref>

''Andrea Doria'' and ''Caio Duilio'' both cruised in the eastern Mediterranean after the war, and both were involved in postwar disputes over control of various cities. ''Caio Duilio'' was sent to provide a show of force during a dispute over control of [[İzmir]] in April 1919 and ''Andrea Doria'' assisted in the suppression of [[Gabriele D'Annunzio]]'s [[Italian Regency of Carnaro|seizure of Fiume]] in November 1920. ''Caio Duilio'' cruised the [[Black Sea]] after the İzmir affair until she was replaced in 1920 by the battleship [[Italian battleship Giulio Cesare|''Giulio Cesare'']]. ''Andrea Doria'' and ''Caio Duilio'' were present during the [[Corfu incident]] in 1923 as part of the naval demonstration protesting the murder of General [[Enrico Tellini]] and four other Italians. In January 1925, ''Andrea Doria'' visited [[Lisbon]], Portugal, to represent Italy during the celebration marking the 400th anniversary of the death of explorer [[Vasco da Gama]]. The two ships performed the normal routine of peacetime cruises and goodwill visits throughout the 1920s and early 1930s; both were placed in reserve in 1933.<ref>Whitley, pp. 165–67</ref>

Both ''Andrea Doria'' and ''Caio Duilio'' went into drydock in the late 1930s for extensive modernizations; this work lasted until October and April 1940, respectively. By that time, Italy had entered World War II on the side of the [[Axis powers]]. The two ships joined the 5th Division based at [[Taranto]]. ''Caio Duilio'' participated in a patrol intended to catch the British battleship {{HMS|Valiant|1914|6}} and a convoy bound for [[Malta]], but neither target was found. She and ''Andrea Doria'' were present during the British [[Battle of Taranto|attack on Taranto]] on the night of 11/12 November 1940. A force of twenty-one [[Fairey Swordfish]] [[torpedo-bomber]]s, launched from {{HMS|Illustrious|R87|6}}, attacked the ships moored in the harbor. ''Andrea Doria'' was undamaged in the raid, but ''Caio Duilio'' was hit by a [[torpedo]] on her starboard side. She was grounded to prevent her from sinking in the harbor and temporary repairs were effected to allow her to travel to [[Genoa]] for permanent repairs, which began in January 1941.<ref name=W1668>Whitley, pp. 166–68</ref><ref>Rohwer, p. 47</ref> In February, [[Operation Grog|she was attacked]] by the British [[Force H]]; several warships attempted to shell ''Caio Duilio'' while she was in dock, but they scored no hits.<ref>Ireland, p. 64</ref> Repair work lasted until May 1941, when she rejoined the fleet at Taranto.<ref>Whitley, p. 166</ref>

[[File:Duilio 1948.jpg|thumb|left|''Caio Duilio'' in 1948]]

In the meantime, ''Andrea Doria'' participated in several operations intended to catch British convoys in the Mediterranean, including the [[Operation Excess]] convoys in January 1941. By the end of the year, both battleships were tasked with escorting convoys from Italy to North Africa to support the [[North African Campaign|Italian and German forces fighting there]]. These convoys included Operation M41 on 13 December and Operation M42 on 17–19 December. During the latter, ''Andrea Doria'' and ''Giulio Cesare'' engaged British cruisers and destroyers in the [[First Battle of Sirte]] on the first day of the operation. Neither the Italians nor the British pressed their attacks and the battle ended inconclusively. ''Caio Duilio'' was assigned to distant support for the operation, and was too far away to actively participate in the battle. Convoy escort work continued into early 1942, but thereafter the fleet began to suffer from a severe shortage of fuel, which kept the ships in port for the next two years.<ref name=W1668/> ''Caio Duilio'' sailed away from Taranto on 14 February with a pair of [[light cruiser]]s and seven destroyers in order to intercept the British convoy MW 9, bounded from [[Alexandria]] to Malta, but the force could not locate the British ships, and so returned to port. After learning of ''Caio Duilio'' departure, however, British escorts scuttled the transport ''Rowallan Castle'', previously disabled by German aircraft.<ref>Woodman, pp. 285–286</ref>

Both ships were interned at Malta following [[Armistice of Cassibile|Italy's surrender]] on 3 September 1943. They remained there until 1944, when the Allies allowed them to return to Italian ports; ''Andrea Doria'' went to [[Syracuse, Sicily]], and ''Caio Duilio'' returned to Taranto before joining her sister at Syracuse. Italy was allowed to retain the two ships after the end of the war, and they alternated in the role of fleet flagship until 1953, when they were both removed from service. ''Andrea Doria'' carried on as a gunnery training ship, but ''Caio Duilio'' was simply placed in reserve. Both battleships were stricken from the [[naval register]] in September 1956 and were subsequently broken up for scrap.<ref>Whitley, pp. 167–68</ref><ref>Gardiner & Chesneau, p. 284</ref>

==See also==
{{Portal|Battleships}}
*[[List of ships of the Second World War]]
*[[List of ship classes of the Second World War]]

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{Reflist|colwidth=20em}}

==References==
* {{cite book|last=Brescia|first=Maurizio|title=Mussolini's Navy: A Reference Guide to the Regia Marina 1930–45|year=2012|publisher=[[Naval Institute Press]]|location=Annapolis, Maryland|isbn=978-1-59114-544-8}}
* {{cite book|last=Campbell|first=John|title=Naval Weapons of World War II|year=1985|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-459-4}}
* {{cite book|last=Friedman|first=Norman|authorlink=Norman Friedman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|year=2011|isbn=978-1-84832-100-7}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5}}
* {{cite book|editor1-last=Gardiner|editor1-first=Robert|editor2-last=Chesneau|editor2-first=Roger | title=Conway's All the World's Fighting Ships, 1922–1946 |publisher=Naval Institute Press|year=1980|isbn=0-87021-913-8|location=Annapolis, Maryland}}
* {{cite book |last1=Garzke|first1=William H.|last2=Dulin|first2=Robert O.|title=Battleships: Axis and Neutral Battleships in World War II|year=1985|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=978-0-87021-101-0|oclc=}}
* {{cite book|last=Giorgerini|first=Giorgio|chapter=The Cavour & Duilio Class Battleships|pages=267–79|editor=Roberts, John|title=Warship IV|year=1980|publisher=Conway Maritime Press|location=London|isbn=0-85177-205-6}}
*{{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=1-55750-352-4}}
* {{cite book |last=Ireland|first=Bernard|authorlink=Bernard Ireland|title=War in the Mediterranean 1940–1943
|year=2004|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=1-84415-047-X}}
* {{cite book|last=Preston|first=Antony|authorlink=Antony Preston|title=Battleships of World War I: An Illustrated Encyclopedia of the Battleships of All Nations 1914–1918|publisher=Galahad Books|location=New York|year=1972|isbn=0-88365-300-1}}
* {{cite book|last=Rohwer|first=Jürgen|authorlink=Jürgen Rohwer|title=Chronology of the War at Sea 1939–1945: The Naval History of World War Two|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2005|edition=Third Revised|isbn=1-59114-119-2}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
* {{cite book |last=Whitley|first=M. J.|authorlink=Michael J. Whitley|title=Battleships of World War II|year=1998|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=1-55750-184-X}}
*{{cite book|last=Woodman|first=Richard|authorlink=|title=Malta Convoys 1940-1943| publisher=[[John Murray (publisher)|John Murray]]|year=2000|location=London|pages=|url=|doi=|isbn= 0-7195-6408-5}}

==Further reading==
* {{cite book|last1=Cernuschi|first1=Ernesto|last2=O'Hara|first2=Vincent P.|authorlink2=Vincent P. O'Hara|chapter=Taranto: The Raid and the Aftermath|pages=77–95|editor=Jordan, John|publisher=Conway|location=London|year=2010|title=Warship 2010|isbn=978-1-84486-110-1}}
*{{cite book|last=Fraccaroli|first=Aldo |title=Italian Warships of World War I|location=London|publisher=Ian Allan|year=1970|isbn=978-0-7110-0105-3}}
* {{cite book |last=Stille|first=Mark|title=Italian Battleships of World War II|year=2011|location=Oxford, UK|publisher=Osprey Publishing|isbn=978-1-84908-831-2}}

==External links==
*{{Commons category-inline|Andrea Doria class battleship|''Andrea Doria''-class battleship}}

{{Andrea Doria class battleship}}
{{WWIItalianShips}}
{{WWIIItalianShips}}

{{DEFAULTSORT:Andrea Doria-class battleship}}
[[Category:Battleship classes]]
[[Category:Andrea Doria-class battleships]]
[[Category:World War I battleships of Italy]]
[[Category:World War II battleships of Italy]]
[[Category:1910s ships]]