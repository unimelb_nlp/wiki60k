{{Infobox journal
| title = Journal of Chemical Sciences
| formernames = Proceedings of the Indian Academy of Sciences – Part A
| cover =
| abbreviation = J. Chem. Sci.
| editor = N. Periasamy
| discipline = [[Chemistry]]
| publisher = [[Springer Science+Business Media]] on behalf of the [[Indian Academy of Sciences]]
| country = India
| frequency = Bimonthly
| history = 1978-present
| impact = 1.298
| impact-year = 2012
| website = http://www.springer.com/chemistry/journal/12039
| link2 = http://link.springer.com/journal/volumesAndIssues/12039
| link2-name = Online archive
| OCLC = 54675731
| LCCN = 2004325296
| CODEN = JCSBB5
| ISSN = 0974-3626
| eISSN = 0973-7103
}}
The '''''Journal of Chemical Sciences''''' is a bimonthly [[Peer review|peer-viewed]] [[scientific journal]] that publishes original [[research]] articles, review articles and rapid communications in all areas of [[chemistry]]. It is published by [[Springer Science+Business Media]] on behalf of the [[Indian Academy of Sciences]]. The [[editor-in-chief]] is N. Periasamy ([[Tata Institute of Fundamental Research]]).

== History ==
The ''Journal of Chemical Sciences'' was originally part of the ''Proceedings of the Indian Academy of Sciences – Part A''. This journal was established in 1934, but in 1978 it was split into three different journals: ''Journal of Chemical Sciences, [[Journal of Earth System Science]]'', and ''[[Proceedings - Mathematical Sciences]]'', all of them continuing as "volume 87".

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[Scopus]]
* [[Inspec]]
* [[EMBASE]]
* [[Chemical Abstracts Service]]
* [[EBSCO Publishing|EBSCO databases]]
* [[CAB International]]
* [[Abstracts in Anthropology]]
* [[Academic OneFile]]
* [[Academic Search]]
* [[CAB Abstracts]]
* [[ChemWeb]]
* [[CSA Environmental Sciences & Pollution Management Database]]
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Compendex|EI-Compendex]]
* [[Elsevier BIOBASE]]
* [[GEOBASE (database)|GEOBASE]]
* [[International Nuclear Information System|INIS Atomindex]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.298.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Chemical Sciences |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
<references/>

== External links ==
* {{Official website|http://www.springer.com/chemistry/journal/12039}}

[[Category:Springer Science+Business Media academic journals]]
[[Category:Chemistry journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1978]]
[[Category:English-language journals]]