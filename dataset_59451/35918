{{good article}}
{{Infobox scientist
| name                    = Harold M. Agnew
| birth_name              = Harold Melvin Agnew
| image                   = Agnew Harold 1955 LAT1383.jpg
| image_size              = 300 px
| caption                 = Harold M. Agnew in 1955
| birth_date              = {{Birth date|1921|03|28}}
| birth_place             = [[Denver, Colorado]]
| death_date              = {{Death date and age|2013|09|29|1921|03|28}}
| death_place             = [[Solana Beach, California]]
| residence               =
| citizenship             = [[United States]]
| nationality             =
| ethnicity               =
| field                   = [[Physics]]
| work_institution        = [[Los Alamos National Laboratory]]
| alma_mater              = [[University of Denver]], A.B.<br>[[University of Chicago]] M.S.<br/>[[University of Chicago]] Ph.D.
| doctoral_advisor        = [[Enrico Fermi]]
| thesis_title            = The Beta-spectra of Cesium-137, Yttrium-91, Chlorine-147, Ruthenium-106, Samarium-151, Phosphorus-32, and Thulium-170
| thesis_url              = http://search.proquest.com/docview/301844865
| thesis_year             = 1949
| doctoral_students       =
| known_for               = Succeeded [[Norris Bradbury]] as director at [[Los Alamos National Laboratory|Los Alamos]]
| author_abbreviation_bot =
| author_abbreviation_zoo =
| prizes                  = [[Ernest Orlando Lawrence Award|E. O. Lawrence Award]] (1966)<br>[[Enrico Fermi Award]] (1978)
| religion                =
| footnotes               =
}}
'''Harold Melvin Agnew''' (March 28, 1921 – September 29, 2013) was an American physicist, best known for having flown as a scientific observer on the [[Atomic bombings of Hiroshima and Nagasaki|Hiroshima bombing mission]] and, later, as the third director of the [[Los Alamos National Laboratory]].

Agnew joined the [[Metallurgical Laboratory]] at the [[University of Chicago]] in 1942, and helped build the [[Chicago Pile-1]], the world's first [[nuclear reactor]]. In 1943, he joined the [[Los Alamos Laboratory]], where he worked with the [[Cockcroft–Walton generator]]. After the war ended, he returned to the University of Chicago, where he completed his graduate work under [[Enrico Fermi]].

Agnew returned to Los Alamos in 1949, and worked on the [[Castle Bravo]] [[nuclear test]] at [[Bikini Atoll]] in 1954. He became head of the Weapon Nuclear Engineering Division in 1964. He also served as a [[Democratic Party (United States)|Democratic]] [[New Mexico Senate|New Mexico State Senator]] from 1955 to 1961, and was the Scientific Adviser to the [[NATO]] [[Supreme Allied Commander Europe]] (SACEUR) from 1961 to 1964. He was director of the Los Alamos National Laboratory from 1970 to 1979, when he resigned to become President and Chief Executive Officer of [[General Atomics]]. He died at his home in [[Solana Beach, California]], on September 29, 2013.

==Early life and education ==
Harold Melvin Agnew was born in [[Denver, Colorado]] on March 28, 1921, the only child of a pair of [[stonemasonry|stonecutters]]. He attended [[South High School (Denver)|South Denver High School]] and entered the [[University of Denver]], where he majored in [[chemistry]]. He was a strong athlete who pitched for the university [[softball]] that won a championship. He left the University of Denver in January 1942, but had enough credits to graduate [[Phi Beta Kappa]] with his [[Bachelor of Arts]] degree in June, and he received a scholarship to [[Yale University]].{{sfn|Palevsky|2005|pp=2–3}}<ref name="New York Times">{{cite news |title=Harold M. Agnew, Physicist Present at Birth of the Nuclear Age, Dies at 92 |newspaper=[[New York Times]] |date=October 1, 2013 |last=Broad |first=William J. |url=https://www.nytimes.com/2013/10/02/us/harold-m-agnew-physicist-present-at-birth-of-the-nuclear-age-dies-at-92.html|accessdate=October 6, 2013}}</ref>

After the Japanese [[bombing of Pearl Harbor]] brought the United States into the [[Pacific War]], Agnew and his girlfriend Beverly, a fellow graduate of South Denver High School and the University of Denver, attempted to join the [[United States Army Air Corps]] together. They were persuaded not to sign the enlistment papers. Instead, Joyce C. Stearns, the head of the physics department at the University of Denver, persuaded Agnew to come with him to the [[University of Chicago]], where Stearns became the deputy head of the [[Metallurgical Laboratory]]. Although Agnew had enough credits to graduate, Beverly did not and had to remain behind. They were married in Denver on May 2, 1942. They then went to Chicago, where Beverly became a secretary to Richard L. Doan, then head of the Metallurgical Laboratory. Agnew and Beverly had two children, a daughter Nancy, and a son, John.{{sfn|Palevsky|2005|pp=2–3}}<ref name="Beverly">{{cite journal |journal=Los Alamos Historical Society Newsletter |volume=30 |issue=4 |date=December 2011 |page=3 |title=In Memoriam: Agnew and Ramsey|url=http://www.losalamoshistory.org/pdfs/december11newsletter.pdf|accessdate=December 30, 2015}}</ref>

[[File:Agnew NagasakiPuCoreDetail.jpg|thumb|right|upright|Harold Agnew on Tinian in 1945, carrying the [[plutonium]] [[Pit (nuclear weapon)|core]] of the Nagasaki [[Fat Man]] bomb]]
At the Metallurgical Laboratory, Agnew worked with [[Enrico Fermi]], [[Walter Zinn]] and [[Herbert L. Anderson]].{{sfn|Palevsky|2005|pp=2–3}}  There, he was involved in the construction of [[Chicago Pile-1]]. Initially, Agnew worked with the instrumentation. The [[Geiger counter]]s were calibrated using a [[radon]]-[[beryllium]] source, and Agnew received too high a dose of radiation. He was then put to work stacking the [[graphite]] bricks that were the reactor's [[neutron moderator]]. He witnessed the first controlled [[nuclear chain reaction]] when the reactor went critical on December 2, 1942.{{sfn|Palevsky|2005|p=5}}<ref name="New York Times"/><ref name="NF">{{cite web |title=Harold Agnew |url=http://nuclearfiles.org/menu/library/biographies/bio_agnew-harold.htm |accessdate=December 21, 2011 |publisher=Nuclear Age Peace Foundation}}</ref>

Agnew and Beverly moved to the [[Los Alamos Laboratory]] in March 1943.<ref name="New York Times"/> Agnew, Beverly and [[Bernard Waldman]] first went to the [[University of Illinois]], where the men disassembled the [[Cockcroft–Walton generator]] and [[particle accelerator]] while Beverly catalogued all the parts. The parts were shipped to New Mexico, where Agnew and Beverly met up with them, and rode the trucks hauling them to the Los Alamos Laboratory. There, Beverly worked a secretary, initially with [[Robert Oppenheimer]] and his secretary Priscilla Green. She then became secretary to [[Robert Bacher]], the head of Physics (P) Division, and later the Gadget (G) Division, for the rest of the war. Agnew's job was to reassemble the accelerator, which was then used for experiments by [[John Henry Manley|John Manley]]'s group.<ref name="Beverly"/><ref name="Voices">{{cite web |url=http://www.manhattanprojectvoices.org/oral-histories/harold-agnews-interview |title=Harold Agnew's Interview |publisher=Manhattan Project Voices |accessdate=October 6, 2013}}</ref>

{|
|+'''Los Alamos ID badges'''
|align=center|[[File:Harold Agnew ID badge.png|240px|Harold M. Agnew]]<br/>Harold M. Agnew
|align=center|[[File:Beverly J. Agnew Los Alamos ID.png|240px|Beverly J. Agnew]]<br/>Beverly J. Agnew
|}

When experimental work wound down, Agnew was transferred to [[Project Alberta]], working as part of [[Luis W. Alvarez]]'s group, whose role was to monitor the yield of nuclear explosions. With Alvarez and [[Lawrence H. Johnston]], Agnew had devised a method for measuring the yield of the nuclear blast by dropping pressure gauges on parachutes and [[telemetry|telemetering]] the readings back to the plane.<ref name="AE"/> In June 1945, he was issued with an Army uniform and dog tags at Wendover Army Air Field, Utah, and was flown to [[Tinian]] in the Western Pacific in a [[C-54]] of the [[509th Composite Group]]. Agnew's first task was to install his yield measurement instrumentation in the [[Boeing B-29 Superfortress]] aircraft ''[[The Great Artiste]]''.{{sfn|Krauss|Krauss|2005|p=343}}

During the [[atomic bombing of Hiroshima]], on August 6, 1945, Agnew, along with Alvarez and Johnston, flew as a scientific observer in the ''The Great Artiste'', piloted by [[Charles Sweeney]], which tailed the ''[[Enola Gay]]'' as the instrumentation aircraft.  Agnew later recalled, "After we dropped our gauges I remember we made a sharp turn to the right so that we would not get caught in the blast – but we still got badly shaken up by it."  He brought along a movie camera and took the only existing movies of the Hiroshima event as seen from the air.<ref name="AE">{{cite web |publisher=PBS |title=American Experience . Race for the Superbomb . Harold Agnew on: The Hiroshima Mission |url=http://www.pbs.org/wgbh/amex/bomb/filmmore/reference/interview/agnewhiroshima.html |accessdate=December 21, 2011}}</ref><ref name="BBC">{{cite news |publisher=BBC |url=http://news.bbc.co.uk/2/hi/americas/4718579.stm#graphicss |title=The men who bombed Hiroshima |accessdate=December 21, 2011}}</ref><ref name="ACAP">{{cite web |publisher=American Institute of Physics |url=http://www.aip.org/history/acap/biographies/bio.jsp?agnewh |title=Harold Agnew |accessdate=December 21, 2011}}</ref>

After the war ended, Agnew entered the University of Chicago, where he completed his graduate work under Fermi. Agnew and Beverly stayed with Fermi and his family, due to the post-war housing shortage.<ref name="Voices"/> He received his [[Master of Science]] (MS) degree in 1948 and his [[Doctor of Philosophy]] (PhD) degree in 1949,{{sfn|Palevsky|2005|p=10}} writing his thesis on "The beta-spectra of Cs137, Y91, Pm147, Ru106, Sm151, P32, Tm170".<ref>{{cite web |url=https://libcat.uchicago.edu/ipac20/ipac.jsp?session=13810V85D21C2.32763&profile=ucpublic&uri=full=3100001~!4272351~!1&ri=1&aspect=subtab13&menu=search&source=~!horizon |publisher=[[University of Chicago]] |title=The beta spectra of Cs137, Y91, Chlorine147, Ru106, Sm151, P32, Tm170 |accessdate=October 6, 2013 }}</ref> Fellow postgraduate students at Chicago at the time included [[Tsung-Dao Lee]], [[Chen Ning Yang]], [[Owen Chamberlain]] and [[Jack Steinberger]].{{sfn|Palevsky|2005|p=10}}

==Los Alamos years==
[[File:Agnew Harold 30 years of service 1974 PUB74116008.jpg|thumb|right|upright|Harold Agnew receives his 30 years of service award in 1974]]
With his doctorate in hand, Agnew returned to Los Alamos as a [[National Research Foundation]] Fellow, and worked on weapons development in the Physics Division.<ref name="ACAP"/> In 1950, he was assigned to the [[thermonuclear weapon]]s project, and was project engineer for the [[Castle Bravo]] [[nuclear test]] at [[Bikini Atoll]] in 1954.<ref>{{cite web |url=http://nuclearweaponarchive.org/Usa/Tests/Castle.html |title=Operation Castle |publisher=Nuclear Weapon Archive  |accessdate=October 6, 2013 }}</ref>{{sfn|Palevsky|2005|p=16}} He became head of the Weapon Nuclear Engineering Division in 1964.<ref name="ACAP"/>

Agnew served as a [[Democratic Party (United States)|Democratic]] [[New Mexico Senate|New Mexico State Senator]] from 1955 to 1961.<ref name="New Mexican">{{cite news |newspaper=[[The Santa Fe New Mexican]] |url=http://www.santafenewmexican.com/news/local_news/article_b9612ad1-27f8-5d07-9db3-6c84da4ff678.html |first=Tom |last=Sharpe |title=Former Los Alamos lab director Harold Agnew dies at 92 |date=September 30, 2013 |accessdate=October 6, 2013 }}</ref> He was the first state senator to be elected from [[Los Alamos County]]. Senators served unpaid, receiving only a per diem allowance of five dollars.<ref name="Nevada">{{cite web |url=http://digital.library.unlv.edu/api/1/objects/nts/1115/bitstream |series=Nevada Test Site Oral History Project |publisher=University of Nevada, Las Vegas |title=Interview with Harold M. Agnew |date=October 10, 2005 |location=Solana Beach, California |accessdate=October 29, 2013}}</ref> Since the New Mexico legislature convened for only 30 days in even numbered years and 60 days in odd numbered years,<ref>{{cite web |url=http://www.nmlegis.gov/lcs/lcsdocs/nmleghandbook01-05.pdf |accessdate=October 20, 2013 |title=New Mexico State Legislature |publisher=New Mexico Legislature }}</ref> he was able to continue working at Los Alamos, taking leave without pay to attend.<ref name="Nevada"/> He attempted to reform New Mexico's liquor laws, which specified a minimum mark-up. He was unsuccessful in 1957, but the law was reformed in 1963.{{sfn|Roybal|2008|pp=186–189}}

From 1961 to 1964, he was Scientific Adviser to the [[NATO]] [[Supreme Allied Commander Europe]] (SACEUR). He also held a number of part-time advisory position with the military over the years. He was a member of the [[United States Air Force Scientific Advisory Board]] from 1957 to 1968, and was chairman of the Science Advisory Group of the [[United States Army]]'s Combat Development Command from 1966 to 1970. He was a member of the [[Defense Science Board]] from 1966 to 1970, the Army's Scientific Advisory Panel from 1966 to 1974, and the Army Science Board from 1978 to 1984.<ref name="ACAP"/>

Agnew became director of the Los Alamos National Laboratory in 1970, when it had 7,000 employees.<ref name="The Washington Post" /> He took over at a time of great change. His predecessor, [[Norris Bradbury]], had rebuilt the laboratory from scratch after the war, and many of the people he had brought in were approaching retirement.<ref name="Agnew Years"/> Under his directorship, Los Alamos developed an underground test containment program, completed its [[Los Alamos Neutron Science Center|Meson Physics Facility]], acquired the first [[Cray-1|Cray supercomputer]], and trained the first class of [[International Atomic Energy Agency]] inspectors.<ref>{{cite web |publisher=[[Los Alamos National Laboratory]] |url=http://www.lanl.gov/history/people/agnew.shtml |title=The Agnew Years (1970–1979) |accessdate=December 21, 2011}}</ref> Agnew managed to get the Los Alamos Laboratory responsibility for the development of the [[W76]], used by the [[Trident I]] and [[Trident II]] [[Submarine Launched Ballistic Missile]]s, and the [[W78]] used by the [[LGM-30 Minuteman|Minuteman III]] [[intercontinental ballistic missile]]s. He was proud of the work with [[insensitive high explosive]] that made nuclear weapons safer to handle. Support from the [[United States Atomic Energy Commission|Atomic Energy Commission]] for reactor development dried up, but during the  [[1970s energy crisis]], the laboratory explored other types of alternative fuels.<ref name="Agnew Years">{{cite journal |journal=Los Alamos Science |volume=4 |issue=7 |date=Winter–Spring 1983 |pages=73–79 |title=The Times They Were a Changin’ |format=PDF |url=http://gendocs.ru/docs/9/8057/conv_1/file1.pdf |accessdate=October 6, 2013}}</ref>

==Later life==
In 1979, Agnew resigned from Los Alamos and became President and Chief Executive Officer of [[General Atomics]], a position he held until 1985.<ref name="ACAP" /> In his letter of resignation to [[David S. Saxon]], the President of the University of California, Agnew wrote that his decision was influenced by "dissatisfaction with University administration policies and a lack of advocacy for the total LASL [Los Alamos Scientific Laboratory] program" and "frustration with what I consider to be a continuing inequitable distribution of defense program funding by the [[United States Department of Energy|Department of Energy]] between the LASL and LLL [Lawrence Livermore Laboratory]."<ref name="Physics Today">{{cite journal |journal=[[Physics Today]] |volume=32 |issue=5 |title=Agnew quits Los Alamos, goes to General Atomic |date=May 1979 |page=116 |doi=10.1063/1.2995534 }}</ref>

[[File:Harold Agnew in 2006.jpg|thumb|left|upright|Harold Agnew at The Los Alamos Laboratory in 2006]]
Agnew chaired the General Advisory Committee of the [[Arms Control and Disarmament Agency]] from 1974 to 1978, and served as a White House science councillor from 1982 to 1989. He was a member of [[NASA]]'s Aerospace Safety Advisory Panel from 1968 to 1974, and from 1978 to 1987. He became an adjunct professor at the [[University of California, San Diego]] in 1988. He was the recipient of the [[Ernest Lawrence|E.O. Lawrence]] Award in 1966, and of the Department of Energy's [[Enrico Fermi Award]] in 1978. Along with [[Hans Bethe]], Agnew was the first to receive the Los Alamos National Laboratory Medal. He was a member of the [[National Academy of Sciences]] and the [[National Academy of Engineering]].<ref name="NF" /><ref name="ACAP" /><ref>{{cite web |url=http://science.energy.gov/fermi/award-laureates/ |accessdate=December 21, 2011 |publisher=[[United States Department of Energy|Department of Energy]] |title=Award Laureates}}</ref><ref>{{cite web |url=http://www.nae.edu/MembersSection/Directory20412/29790.aspx |accessdate=April 10, 2013 |publisher=[[National Academy of Engineering]] |title=Members}}</ref>

A proponent of [[tactical nuclear weapon]]s, Agnew pointed out in 1970 that the [[Thanh Hoa Bridge]] in Vietnam required hundreds of sorties to destroy with conventional weapons when a nuclear weapon could have done the job with just one.<ref name="Vintage Agnew">{{cite journal |journal=Los Alamos Science |format=PDF |volume=4 |issue=7 |date=Winter–Spring 1983 |title=Vintage Agnew |url=http://gendocs.ru/docs/9/8057/conv_1/file1.pdf |pages=69–72 |accessdate=October 6, 2013}}</ref> In a 1977 article for the Bulletin of the Atomic Scientists, Agnew argued that the fusion reactions of [[neutron bomb]]s could provide "tactical" advantages over conventional fission weapons, especially in countering the "massive armor component possessed by the [[Eastern bloc]]." Citing conclusions reached by the Rand Corporation, Agnew argued that without affecting the armor of a tank, the neutrons produced by a fusion blast would penetrate the vehicle and "in a matter of a few tens of minutes to hours kill or make the crew completely ineffective." Because the neutron bomb reduced collateral damage, it could be used in a much more selective fashion than a fission weapon, thereby providing a clear "advantage for the military defender as well as for the nearby non-combatant."<ref>{{cite web |publisher=Institute for Policy Studies |url=http://rightweb.irc-online.org/profile/Agnew_Harold |title=Profile of Harold Agnew |accessdate=December 21, 2011}}</ref>

Agnew maintained that no new U.S. nuclear weapon design could be certified without [[nuclear testing]], and that stockpile reliability stewardship without such testing may be problematic.<ref>{{cite journal |first=Hugh E. |last=DeWitt |first2=Gerald E. |last2=Marsh |url=https://books.google.com/?id=prgDAAAAMBAJ&pg=PA41&lpg=PA41&dq=Agnew+nuclear+testing#v=onepage&q=Agnew%20nuclear%20testing&f=false |title=Stockpile reliability and nuclear testing |journal=[[Bulletin of the Atomic Scientists]] |pages=40–41 |volume= 1 |issue= 8 |date=April 1984 |accessdate=December 21, 2011}}</ref> In a 1999 letter to the ''[[Wall Street Journal]]'', he commented on the significance of allegations of Chinese nuclear espionage. "As long as any nation has a demonstrated nuclear capability and a means of delivering its bombs and warheads, it doesn't really matter whether the warheads are a little smaller or painted a color other than red, white, and blue," he wrote. "I suspect information published in the open by the [[Natural Resources Defense Council|National [sic.] Resources Defense Council]] has been as useful to other nations as any computer codes they may have received by illegal means."<ref>{{cite news |newspaper=[[Wall Street Journal]] |title=Letter to the Editor |page=A27 |date=May 17, 1999}}</ref>

Beverly died on October 11, 2011.<ref name="Beverly"/> Agnew was diagnosed of [[chronic lymphocytic leukemia]], and died at his home in [[Solana Beach, California]], on September 29, 2013, while watching football on television.<ref name="New Mexican"/><ref name="The Washington Post">{{cite news |newspaper=[[The Washington Post]] |title=Harold Agnew, head of atomic laboratory, dies at 92 |first=Elaine |last=Woo|date=October 3, 2013 |url=http://www.washingtonpost.com/national/harold-agnew-head-of-atomic-laboratory-dies-at-92/2013/10/02/b4bb81cc-2ba4-11e3-97a3-ff2758228523_story.html |accessdate=October 6, 2013}}</ref> He was survived by his daughter Nancy and son John.<ref name="New York Times"/> He had arranged to be cremated and his ashes interred with Beverly's at the Guaje Pines Cemetery in Los Alamos.<ref>{{cite web |url=http://www.nucleardiner.com/index.php/archive/item/harold-agnew |publisher=Nuclear Diner |title=Harold Agnew (1921–2013)|accessdate=October 6, 2013}}</ref>

In a 2005 [[BBC]] interview, Agnew stated, "About three-quarters of the U.S. nuclear arsenal was designed under my tutelage at Los Alamos. That is my legacy."<ref name="BBC" />
{{Clear}}

==Notes==
{{reflist|30em}}

==References==
* {{cite book |editor-last=Krauss |editor-first=Robert |editor2-first=Amelia |editor2-last=Krauss |year=2005 |title=The 509th Remembered: A History of the 509th Composite Group as Told by the Veterans Themselves, 509th Anniversary Reunion, Wichita, Kansas October 7–10, 2004 |location=Buchanan, Michigan |publisher=509th Press |oclc=59148135 |isbn=978-0-923568-66-5 |ref=harv }}
* {{cite book |title=Interview with Harold M. Agnew |first=Mary |last=Palevsky |publisher=University of Nevada, Las Vegas Libraries |date=10 October 2005 |url=http://digital.library.unlv.edu/objects/nts/1115 |accessdate=27 December 2011 |ref=harv}}
* {{cite book |last=Roybal |first=David |title=Taking on Giants : Fabián Chávez, Jr. and New Mexico Politics |location=Albuquerque |publisher=University of New Mexico Press |year=2008 |isbn=978-0-826344-36-6 |ref=harv}}

==External links==
*[http://manhattanprojectvoices.org/oral-histories/harold-agnews-interview-1994 1994 Audio Interview with Harold Agnew by Richard Rhodes] Voices of the Manhattan Project
*[http://manhattanprojectvoices.org/oral-histories/harold-agnews-interview-1992 1992 Video Interview with Harold Agnew by Theresa Strottman] Voices of the Manhattan Project
*[http://www.oac.cdlib.org/findaid/ark:/13030/kt7d5nd5tn/ Register of the Harold Melvin Agnew motion picture film] at the Hoover Institution Archives, Stanford University

{{s-start}}
{{s-gov}}
{{s-bef | before = [[Norris Bradbury]]}}
{{s-ttl | title = Director of the [[Los Alamos Scientific Laboratory]] | years = 1970–1979}}
{{s-aft | after = [[Donald Kerr]]}}
{{s-end}}
{{Manhattan Project}}
{{Subject bar
| portal1=World War II
| portal2=Nuclear technology
| portal3=Physics
| portal4=History of science
| portal5=United States
| portal6=Biography
| portal7=Cold war
| portal8=New Mexico
| commons=y
}}
{{Authority control}}

{{DEFAULTSORT:Agnew, Harold M.}}
[[Category:1921 births]]
[[Category:2013 deaths]]
[[Category:American physicists]]
[[Category:Enrico Fermi Award recipients]]
[[Category:Los Alamos National Laboratory personnel]]
[[Category:Manhattan Project people]]
[[Category:People from Denver]]
[[Category:People associated with the atomic bombings of Hiroshima and Nagasaki]]
[[Category:Members of the United States National Academy of Sciences]]
[[Category:Members of the United States National Academy of Engineering]]
[[Category:University of Denver alumni]]
[[Category:University of Chicago alumni]]
[[Category:United States Army Science Board people]]