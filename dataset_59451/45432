{{BLP sources|date=May 2013}}
{{Use dmy dates|date=April 2012}}
{{Infobox officeholder
| honorific-prefix =
| name             = Asghar Khan
| native_name      = {{Nastaliq|اصغر خان }}
| native_name_lang =
| honorific-suffix =
| image            = Air_Marshal_Asghar_Khan.jpg
 | image_size      =
| alt              =
| order            = [[Chief of Air Staff (Pakistan)|Commander-in-Chief of the Pakistan Air Force]]
| office           = <!--Can be repeated up to eight times by changing the number-->
| term_start       = 23 July 1957
| term_end         = 22 July 1965
| president        = [[Iskander Mirza]]<br />[[Ayub Khan (President of Pakistan)|Ayub Khan]]
| predecessor      = [[Air Vice Marshal|AVM]] [[Arthur McDonald]]
| successor        = [[Air Marshal]] [[Nur Khan]]
| order1           = President of [[Pakistan International Airlines]]
| term_start1      = 20 August 1965
| term_end1        = 31 November 1968
| predecessor1     = [[Arthur McDonald]]
| successor1       = [[Air Marshal]] [[Nur Khan]]
| order3           = Chairman of the [[Tehrik-e-Istiqlal]]
| term_start3      = 29 June 1970
| term_end3        = 12 December 2011<br />Merged with [[Pakistan Tehreek-e-Insaf]]
| predecessor3     = Office created
| successor3       = [[Imran Khan]]
| birth_date       = {{Birth date and age|df=yes|1921|1|17}}
| birth_place      = [[Jammu]], [[Kashmir]], [[British Indian Empire]]
| death_date       =
| death_place      =
| birthname        = Mohammad Asghar Khan
| citizenship      = {{flagicon|UK}}[[British Subject]] (1921-1947)<br />{{PAK}} (1947-)
| nationality      =
| party            = [[Pakistan Tehreek-e-Insaf]]
| otherparty       = [[Tehrik-e-Istiqlal]]
| spouse           =
| partner          = <!--For those with a domestic partner and not married-->
| relations        =
| children         = Nasreen Asghar Khan <br /> Shereen Asghar Khan <br /> [[Omar Asghar Khan]] <br /> [[Ali Asghar Khan (Pakistani politician)|Ali Asghar Khan]]
| residence        = [[Abbottabad]]
| alma_mater       = [[Royal Air Force College Cranwell|Royal Air Force College]]<br />[[Indian Military Academy]]
| occupation       = administrator<br />politician
| profession       = [[fighter pilot]]
| religion         = [[Islam]]
| website          =
| footnotes        =
<!--Military service-->
| nickname         = ''Night Flyer''
| allegiance       = {{UK}}<br />{{PAK}}
| branch           = {{air force|UK}}<br />{{air force|PAK}}
| serviceyears     = 1940-1965
| rank             = [[File:US-O9 insignia.svg|30px]] [[Air Marshal]] ({{small|[[Lieutenant-General]]}}
| unit             = [[No. 9 Squadron Griffins|No. 9 Squadron ''Griffins'']]
| commands         = [[Pakistan Air Force Academy]]<br />[[List of Pakistan Air Force Bases|No. 1 ''Stryker'' Group]], [[Peshawar Airbase|Peshawar AFB]]<br />[[Directorate-General]] for the [[Air Force Strategic Command (Pakistan)|Air Operations]] (DGAO)<br />[[Precision Engineering Complex]]<br />[[Assistant Chief of Air Staff]]
| battles          = [[World War II]]<br />[[Burma Campaign 1944-1945|British war in Burma]]<br />[[Indo-Pakistani War of 1947]]
| awards           =
}}

[[Air Marshal]] '''Asghar Khan''' ({{lang-ur|{{Nastaliq|ائیر مارشل اصغر خان}}}}; b. January 17, 1921), is [[Pakistan|Pakistani]] politician, [[History of the Pakistan Air Force|aviation historian]], [[peace activist]], and retired [[3 star rank|three star]] rank [[Air force officer|air force]] general who served as the first native [[Chief of Air Staff (Pakistan)|Commander in Chief]] of the [[Pakistan Air Force]] (PAF) under President [[Iskander Mirza]] (1956–59) and under President [[Ayub Khan (general)|Ayub Khan]] until resigning in 1965 prior to the [[Operation Gibraltar|start]] of the [[Indo-Pakistani Air War of 1965|air operations]] of the PAF during the [[Indo-Pakistani War of 1965]].

Initially commissioned in the [[Indian Army]], Asghar Khan was drafted into [[Royal Air Force]] in 1940, seeing actions in [[Burma Campaign 1944-1945|Burma Campaign]] and later acceded to United Kingdom where he graduated from [[RAF Staff College, Bracknell|RAF Staff College]] at [[Bracknell (UK Parliament constituency)|Bracknell]], completing his collegiate courses from [[Joint Service Defence College]], and completed his post-graduate studies from [[Royal College of Defence Studies|Imperial Defence College]]. Upon return to [[British Indian Empire]], Asghar Khan resumed his active duty with the Royal Air Force and [[Pakistani citizenship|opted]] for [[Pakistan]] following the [[Pakistan Movement|independence]] in 1947, and settled in [[West Pakistan|West-Pakistan]]. Asghar Khan became first commandant of [[Pakistan Air Force Academy]] in 1947 and was also the first to head the [[Directorate-General]] for [[PAF|Air Operations]] (DGAO) in 1950. Finally in 1957, Asghar Khan became the youngest to-date and the first native Air Force Commander-in-Chief of PAF. His tenure as air commander saw the extensive modernization of the PAF, in terms of both technical and military equipment, after resigning he was controversially not consulted by the president prior to launch of [[Operation Gibraltar|Operation ''Gibraltar'']]. After retiring from air force, Asghar Khan became president of civilian national flag carrier, the [[Pakistan International Airlines]] (PIA) which he led until 1968.

In 1970, Asghar Khan founded the [[Secularism in Pakistan|secular]] party, the [[Tehrik-e-Istiqlal]], but performed poorly in [[Pakistani general election, 1970|1970 parliamentary elections]]. However, in 1977, the party significant gained momentum and participated with full force in [[Pakistani general election, 1977|1977 parliamentary elections]], although the party failed to grasp any support in the public as compared to [[democratic socialism|democratic socialist]], the [[Pakistan Peoples Party]] (PPP). He was designated a [[prisoner of conscience]] by [[Amnesty International]], and conferred with the [[Gold Medal]] by the [[Human Rights Commission of Pakistan|Human Rights Commission]], and  [[Jinnah|Jinnah Award]] Award by the Jinnah Society for the cause of democracy. After years of founding the Independence Movement, Asghar Khan merged his party with [[Pakistan Tehreek-e-Insaf]], led by cricketer-turned politician [[Imran Khan]], in January 2012.<ref>http://dunyanews.tv/index.php?key=Q2F0SUQ9MiNOaWQ9NTQ2NDk=</ref>

==Biography==

===Early life and world war II===
Mohammad Asghar Khan was born in [[Jammu]], [[Kashmir and Jammu (princely state)|British-held Kashmir]] in [[British Indian Empire]] on  17 January 1921.<ref name="Pakistan Herald, 14 March 2012">{{cite news|last=Staff report|title=Air Marshal  Muhammad Asghar Khan|url=http://www.pakistanherald.com/newprofile.aspx?hofid=648|accessdate=14 March 2012|newspaper=Pakistan Herald, 14 March 2012}}</ref>  His father, [[Brigadier (United Kingdom)|Brigadier]] [[Thakur (Indian title)|Thakur]] Rahmatullah Khan, was a senior officer and [[Colonel Commandant|colonel-commandant]] of the posted [[Brigade combat team]] of the  [[British Army]] headquartered in Kashmir.<ref name="Pakistan Herald, 14 March 2012"/> He and all his brothers, except one, then joined the armed forces of Pakistan.<ref name="Pakistan Herald, 14 March 2012"/>

After attending a military school, his family directed him at the [[Prince of Wales's Royal Indian Military College]] 1933, and joined the [[Indian Military Academy]] in 1939.<ref name="Pakistan Herald, 14 March 2012"/> Initially, Asghar Khan was an [[Indian Army]] officer, a [[Second Lieutenant]] in the Indian Army, starting his active duty from the [[Royal Deccan Horse]] in December 1940.<ref name="Pakistan Herald, 14 March 2012"/> However this was short-lived, the [[British Ministry of Defence|Ministry of Defence]] drafted Asghar Khan in the newly established [[Royal Indian Air Force]] in 1940, joining the [[No. 9 Squadron IAF|No. 9 Squadron]] of the [[Indian Air Force]].<ref name="Pakistan Herald, 14 March 2012"/> In 1944, Asghar Khan assumed the command his unit and commanded the aerial missions of No. 9 Squadron in Burma.<ref name="Pakistan Herald, 14 March 2012"/> He took active participation in [[Burma Campaign 1944–1945]], directing and commanding aerial operations against the [[Imperial Japan]] on behest of the Great Britain.<ref name="Pakistan Herald, 14 March 2012"/>

After [[World War II]], the [[British government]] called Asghar Khan to United Kingdom where he joined the  [[RAF Staff College, Bracknell|RAF Staff College]] at [[Bracknell (UK Parliament constituency)|Bracknell]], where he completed a staff course from there.<ref name="Pakistan Herald, 14 March 2012"/> Later, Asghar Khan joined the [[Joint Service Defence College]] where he gained [[Bachelor of Science|B.Sc.]] in military ethics after submitting his thesis on actions involving the [[Joint Services]].<ref name="Pakistan Herald, 14 March 2012"/> He conducted his post-graduate research and studies from [[Royal College of Defence Studies|Imperial Defence College]] where Asghar Khan was awarded [[Master of Science|M.Sc.]] in [[Military administration]] by the college faculty.<ref name="Pakistan Herald, 14 March 2012"/>

Upon his return, Asghar Khan was most-senior officer in the Indian Air Force, although his career with Indian Air Force is not completely known, but it is well understood that Asghar Khan commanded the No. 9 Squadron of the Indian Air Force, in 1945. Asghar Khan was also the first  Indian Air Force officer to fly a jet fighter aircraft—a [[Gloster Meteor]]— whilst doing a fighter leader's course in UK in 1946.<ref name="Pakistan Herald, 14 March 2012"/>

==Moving to Pakistan==

===Career with Pakistan Air Force===

On 7 June 1947, Asghar Khan joined the sub-committee led by [[Royal Air Force|RAF]] [[Air Vice Marshal]] [[Allan Perry-Keene]] to distribute the defence assets of undivided India between the proposed [[State of Pakistan]].{{Citation needed|date=August 2012}}.  After the [[Pakistan Movement|independence]] on 14 August 1947, Asghar Khan  moved to newly established country [[Pakistan]] and, Prime minister [[Liaquat Ali Khan]] promoted Asghar Khan to the rank of [[Wing commander (rank)|Wing-Commander]] and  appointed him the first commandant of the [[Pakistan Air Force Academy]] at [[Risalpur]]. He was among the most senior officers of the Pakistan Air Force so in 1949, Asghar Khan became the first [[Officer commanding]] of the [[List of Pakistan Air Force Bases|No. 1 ''Stryker'' Group]] based in [[Peshawar Airbase|Peshawar Air Force Base]]. In 1948, Asghar Khan greeted [[Quaid-e-Azam|founder of Pakistan]] and [[Governor-General of Pakistan|Governor-General]] [[Muhammad Ali Jinnah]] when Jinnah visited the Pakistan Air Force Academy.<ref name="Flicker photo, 1948">{{cite web|last=Kazi (MMBS)|title=The Founder visiting PAF Base Risalpur with Wing Commander Asghar Khan, 1948|url=https://www.flickr.com/photos/pimu/6180452659/|publisher=Flicker photo, 1948|accessdate=14 March 2012}}</ref>

In 1950, Asghar Khan assumed the directorship of the [[Directorate-General]] of the [[Aerial warfare|Air Operations]] (DGAO). In 1955, Asghar Khan was appointed as the [[Assistant Chief of the Air Staff]] in the [[PAF|Air Headquarters]], directing the air administration and personnel department at the Air Headquarters. As Assistant Chief of the Air Staff, Asghar Khan established the major units and infrastructure including the Fighter Leaders School (now Combat Commander's School), the [[PAF Staff College|Air Staff College]] and the [[College of Aeronautical Engineering]] at the Pakistan Air Force Academy. As assistant chief of the air staff, Asghar Khan  also instituted the Inspectorate directorate for the air force and initiated the tradition of regular air staff presentations. Two of his brothers, Squadron Leader Khalid Khan and Pilot Officer Asif Khan were killed during service with the Pakistan Air Force.

===Commander-in-Chief===
{{Main|Chief of Air Staff (Pakistan)}}
After the retirement of [[Air vice-marshal]] (AVM) [[Arthur McDonald]], Prime minister [[Huseyn Shaheed Suhrawardy|Huseyn Suhravardy]] approved the appointment of Asghar Khan as the commander-in-chief of Pakistan Air Force.<ref name="PAF Falcons, Chiefs of Air Staff">{{cite web|last=Press release|title=Air Marshal M Asghar Khan, HPk, HQA|url=http://www.paffalcons.com/cas/asghar-khan.php|work=PAF Falcons|publisher=PAF Falcons, Chiefs of Air Staff|accessdate=14 March 2012}}</ref> On 23 July 1957, Prime minister Suhrawardy upgraded AVM Asghar Khan to [[Three star rank|three-star]] rank air marshal, making him the first native [[Chief of Air Staff (Pakistan)|Air Force Commander-in-chief]], yet at the age of 36, also the youngest to-date.<ref name="PAF Falcons, Chiefs of Air Staff"/>

After assuming the command of air force, Asghar Khan commanded the air force for next eight years where he took initiatives to modernize and expand the [[List of Pakistan Air Force Bases|air force facilities]], installations and equipment, as well as the fighter jets [[U.S. aid to Pakistan|acquired]] from the United States.<ref name="PAF Falcons, Chiefs of Air Staff"/> Asghar Khan also launched the fighter training programmes and combat course at the PAF to train fighter pilots in modern air warfare.<ref name="PAF Falcons, Chiefs of Air Staff"/> His style of leading the air force often comes with criticisms by his junior officers, first alleging that Air Marshal Asghar Khan was inclined to be autocratic in his decision makings.<ref name="PAF Falcons, Chiefs of Air Staff"/> His juniors noted that the Air Marshal would go out of his way to elicit a whole range of opinions before taking a decision, but once that decision was made by him, he would not tolerate any ifs and buts about its implementations.<ref name="PAF Falcons, Chiefs of Air Staff"/> As for approving the appointments and selections process, Asghar Khan made no secret of his willingness to superseding the senior officers if that became unavoidable in ensuring that the best qualified officers needed to fill the key appointments, particularly in the combat units.<ref name="PAF Falcons, Chiefs of Air Staff"/> During his long tenure, Air Marshal Asghar Khan gave commission to established air force bases, in [[PAF Base Samungli|Samungli]], [[PAF Base Mushaf|Sargodha]], and the [[PAF Base Peshawar|Peshawar]].<ref name="PAF Falcons, Chiefs of Air Staff"/>

The time he was appointed as the commander-in-chief, Asghar Khan negotiated with the United States to provide the military equipment, fighter jets, on an indefinite basis.<ref name="PAF Falcons, Chiefs of Air Staff"/>  The combat units and fighter squadrons were quickly raised majority of which were equipped with the [[state-of-the-art]] [[North American F-86 Sabre|F-86 Sabres]],  and others with [[Lockheed F-104 Starfighter|F-104 Starfighter]], [[Martin B-57 Canberra|B-57 Canberra]], [[Lockheed C-130 Hercules|C-130 Hercules]], [[Lockheed T-33 Shooting Star|T-33]] and the [[Cessna T-37 Tweet|T-37 Tweet]] aircraft.<ref name="PAF Falcons, Chiefs of Air Staff"/>

===Presiding Pakistan International Airlines===
{{Main|Pakistan International Airlines}}
After leaving air force, Asghar Khan was employed at the [[Ministry of Defence (Pakistan)|Ministry of Defence (MoD)]] and was appointed as the president of the national flag carriers, the [[Pakistan International Airlines]] (PIA).<ref>{{cite web|last=PIA History|title=PIA's Finest Men and Women|url=http://www.historyofpia.com/legends36.htm|publisher=PIA History|accessdate=14 March 2012}}</ref><ref>{{cite web|last=PIA History|title=The Legengs|url=http://www.historyofpia.com/legends48.htm|publisher=PIA History}}</ref><ref name="The News International Monday, 23 August 2010">{{cite news|last=Khan|first=M. Asghar|title=My political struggle|url=http://www.thenews.com.pk/Todays-News-9-694-My-political-struggle|accessdate=14 March 2012|newspaper=The News International|date=23 August 2010}}</ref>  There, Asghar Khan learned to fly the [[commercial airline]] and obtained a [[Commercial pilot licence]] after passing the exam from  [[Federal Aviation Administration]] of the United States.<ref name="The News International Monday, 23 August 2010"/> Asghar Khan introduced new uniforms for the air hostesses and stewards which earned words of admiration at domestic and international airports.<ref name="The PIA Historical Department">{{cite web|last=PIA|title=Photo Gallery of PIA's Finest Men and Women|url=http://www.historyofpia.com/legends29.htm|publisher=The PIA Historical Department|accessdate=14 March 2012}}</ref>  During his tenure, PIA achieved lowest aircraft accident rate and highest net profit of Pakistan, and was a formidable competitor in the world airline business.<ref name="The News International, Sunday, 23 October 2011">{{cite news|last=Masood Hasan|title=The promise|url=http://www.thenews.com.pk/Todays-News-9-73928-The-promise|accessdate=14 March 2012|newspaper=The News International, Sunday, 23 October 2011|date=23 October 2011}}</ref>  His tenure as president is often reminded as "gold age of PIA".<ref name="The News International, Sunday, 23 October 2011"/> Despite urging of the government to extend his tenure, Asghar Khan took retirement and left the MoD in order to start his political career in 1968.<ref name="The News International Monday, 23 August 2010"/>

==Founding Independence Movement==

After leaving the MoD, Asghar Khan gave vehement criticism and blamed President Ayub Khan and Foreign Minister [[Zulfiqar Ali Bhutto]] for the causes of the [[1965 war]] with India, and later turn his criticism pointing straight towards General [[Yahya Khan]] for the [[Indo-Pakistani War of 1971|1971 war]] failure, which resulted in the [[Bangladesh Liberation War|breakup of]] Pakistan when [[Sheikh Mujibur Rehman]]'s [[Awami League]], which had won the election, had not been allowed to form a government.<ref name="The News International Monday, 23 August 2010"/> In protest, Asghar Khan relinquished awards of 'Hilal-i-Pakistan' and 'Hilal-i-Quaid-i-Azam' as a protest against repressive policies of Field Marshal Ayub Khan in January 1969. In 1970, Asghar Khan founded the [[Tehrik-e-Istiqlal]], initially a [[Centre (politics)|centrist]] secular party.<ref name="Boulder: West View Press">{{cite book|last=Saeed Shafqat, PhD|title=Civil-military relations in Pakistan|year=1997|publisher=Boulder: West View Press|location=Peshawar, Pakistan|isbn=978-0813388090|pages=283 pages|url=https://books.google.com/books?id=3jWOAAAAMAAJ&q=asghar+khan+bhutto&dq=asghar+khan+bhutto&hl=en&ei=PiBhT87OGMae2wXj4oybCA&sa=X&oi=book_result&ct=book-thumbnail&resnum=2&ved=0CDsQ6wEwAQ}}</ref>  Asghar Khan criticized Bhutto on numerous occasions, holding him responsible for tyranny during the 1970 elections.<ref name="Boulder: West View Press"/> However, Asghar Khan and his party failed to score any big hits during the [[Pakistani general election, 1970|1970 parliamentary elections]], initially failing to secure any seats in the [[Pakistan Parliament|parliament]].<ref name="Boulder: West View Press"/>

During the  [[Bangladesh Liberation War]], Asghar Khan did support the [[East Pakistan|East-Pakistan]] morally, alleging the [[West Pakistan|West-Pakistan]] under Bhutto, of depriving East from their political and economical rights. He also demanded power to be handed over to the people of East Pakistan.<ref name="Boulder: West View Press"/> In 1972, after Bhutto was made president, Asghar Khan accused Bhutto for the break-up, later noting that: "We are living virtually under one party state.... The outstanding feature is suppression.<ref name="Boulder: West View Press"/>

===Activism in national politics===
{{Main|Tehrik-e-Istiqlal}}
During Bhutto's rule from 1971 to 1977, Air Marshal Asghar Khan played a major role in opposition to Zulfiqar Ali Bhutto.<ref name="The Express Tribune">{{cite news|last=Zia Khan|title=Reinforcement: Asghar Khan is latest PTI recruit|url=http://tribune.com.pk/story/305443/islamabad-khyber-pakhtunkhwa-pti-members-clash/|accessdate=14 March 2012|newspaper=The Express Tribune|date=13 December 2011}}</ref>  During the 1977 elections, Asghar Khan allied his party, the Tehreek-i-Istiqlal with the [[Pakistan National Alliance]] (PNA) against the People's Party. It was during this period he and his party faced frequent attacks by [[Pakistan Peoples Party]] supporters and from the brutal paramilitary [[Federal Security Force]]. He was imprisoned in [[Kot Lakhpat jail|Kot Lakhpat]] and Sahiwal prisons from March to June 1977.

He contested two seats, one from [[Karachi]] and the other from [[Abbotabad]], despite alleged rigging by the PPP, Asghar Khan was elected by a huge margin from both seats. The PNA rejected the election results as rigged and launched a nationwide agitation against the results. Asghar Khan resigned from both National Assembly seats as a mark of protest against massive rigging in the elections.

===Supporting the Martial law===
While imprisoned, Asghar Khan wrote a much criticized letter to the leadership of [[Pakistan Armed Forces|Defence Forces]], asking them to renounce their support for the "Illegal regime of Bhutto", and asked the military leadership to "differentiate between a "lawful and an unlawful" command... and save Pakistan.".<ref name="St. Martin's Press">{{cite book|last=Talbot|first=Ian|title=Pakistan A Modern History|year=1998|publisher=St. Martin's Press|location=United States.|isbn=0-312-21606-8|pages=181–200|url=https://books.google.com/books?id=ZBs0HdpKuaQC&pg=PA415&dq=asghar+khan+pakistan&hl=en&ei=Xx5hT6vxKePO2wW064mSCA&sa=X&oi=book_result&ct=book-thumbnail&resnum=2&ved=0CDgQ6wEwAQ#v=onepage&q=asghar%20khan%20pakistan&f=true}}</ref>  This letter is considered by the historians as instrumental in encouraging the advent of the far-right [[Zia regime]].<ref name="St. Martin's Press"/> However in television show, Asghar Khan strongly defended his letter as according to him "nowhere in the letter had he asked for the military to take over", and he had written it in response to a news story that he had read in which a Major had shot a civilian showing him the "V sign".<ref name="St. Martin's Press"/> After the overthrow of Zulfiqar Ali Bhutto's government by the military in the summer of 1977, Asghar Khan was offered a cabinet post by General [[Zia-ul-Haq]], Asghar Khan refused to join the cabinet and also withdrew from the PNA after a growing split between the various parties.<ref name="St. Martin's Press"/>

=== Political activism ===

After successfully calling for Bhutto's "judicial murder", Asghar Khan decided to take on the far-right regime of General Zia-ul-Haq who announced to hold the general elections in 1979. The [[Tehrik-e-Istiqlal]] became the most favorite party and benefited with large number of high-profile civilian political figures, including [[Mian Muhammad Nawaz Sharif|Navaz Sharif]], [[Khurshid Mahmud Kasuri|Khurshid Kasur]], [[Aitzaz Ahsan]], [[Sheikh Rashid Ahmad|Rashid Ahmad]], [[Javed Hashmi]], [[Nawab Akbar Khan Bugti|Akbar Bugti]], [[Mushahid Hussain Syed|Mushahid Hussain]], [[Raja Nadir Pervez|Nadir Pervez]], [[Gohar Ayub Khan]],  [[Zafar Ali Shah]], [[Ahmed Raza Kasuri]], [[Sher Afgan Niazi]], [[Manzoor Wattoo]], [[Syeda Abida Hussain]], [[Syed Fakhar Imam]] and many others. All of these members left Asghar Khan under [[Navaz Sharif]] who founded the largest conservative party, the [[Pakistan Muslim League (N)]].

However, at the last moment, General Zia-ul-Haq indefinitely postponed the elections, ordering the arrests of Asghar Khan who remained under house arrest for more than five years. In 1983, Asghar Khan decided to join the left-wing alliance, the [[Movement for Restoration of Democracy]] (MRD) led by [[Benazir Bhutto]]<ref name="Hyman1989p52">{{cite book |last1=Hyman |first1=Anthony |last2=Ghayur |first2=Muhammed |last3=Kaushik |first3=Naresh |date=1989 |title=Pakistan, Zia and After-- |url=https://books.google.com/books?id=cjPgESaC-7sC&pg=PA52 |location=New Delhi |publisher=Abhinav Publications |page=52 |isbn=81-7017-253-5 |quote=The Tehrik-i-Istiqlal of retired air marshal Asghar Khan had also joined the MRD by [1984] ... The so-called 'three Khans' – Nazrullah Khan of the Pakistan Democratic Party, Walid Khan of National Awami Party and Asghar Khan of the Tehrik – opposed [participation in the 1985 elections] and carried the rest with them.}}</ref> but he was detained by the government.  He was kept under house arrest at his Abbotabad residence from 16 October 1979 to 2 October 1984 and was named a [[prisoner of conscience]] by [[Amnesty International]].<ref>{{cite web |url=https://www.amnesty.org/en/library/asset/ASA33/024/1981/en/d95b691b-0920-4420-b814-7c45a23d6365/asa330241981en.pdf |title=Pakistan |author= |year=1981 |work= |publisher=[[Amnesty International]] |accessdate=22 January 2012}}</ref>

===Public disapproval and declining===

In 1986, Asghar Khan left the MRD, as a result of which many of the Tehrik's members resigned in protest.{{citation needed|date=July 2016}} Asghar Khan boycotted the [[No-party|non-partisan]] [[Pakistani general election, 1985|elections held in 1985]].<ref name="Hyman1989p52" /> However, Asghar Khan and his party took full part in [[Pakistani general election, 1988|1988 parliamentary elections]].<ref name="Hyman1989p134">{{cite book |last1=Hyman |first1=Anthony |last2=Ghayur |first2=Muhammed |last3=Kaushik |first3=Naresh |date=1989 |title=Pakistan, Zia and After-- |location=New Delhi |publisher=Abhinav Publications |page=134 |isbn=81-7017-253-5 |quote=Once the [1988] National Assembly elections were over ... Air Marshal Asghar Khan, leader of the Tehrik-i-Istiklal party, has been swept aside, in both the constituencies where he contested the elections from.}}</ref> But this time, he was accused by [[Pakistan Peoples Party]] for having called for Bhutto's death sentence and the martial law, which Asghar Khan himself failed to justify. His party members disintegrated and allied with conservative [[Nawaz Sharif]], a major set back for his career.{{citation needed|date=July 2016}} Asghar Khan's public rating plummeted and faced a complete annihilation and defeat in 1988 elections.<ref name="Hyman1989p134"/> He conceded his defeat but again contested in [[Pakistani general election, 1990|1990 parliamentary elections]] from [[Lahore]], Asghar Khan once again faced defeat. Briefly retiring from active politics in the late 1990s his party faced another one of its many splits. Since 1990, Asghar Khan has not held a significant position in politics.<ref name="The Express Tribune"/>

===Collapse and merging with Pakistan Movement for Justice===
As he grew older, he handed over his small party to  his equally capable son [[Omar Asghar Khan]], who had for a while joined the [[Military occupation|military government]] of General [[Pervaiz Musharraf]], and became minister of [[Ministry of Environment (Pakistan)|Ministry of Environment (MoE)]]. After his son's resignation from the cabinet, [[Omar Asghar Khan]] took over Tehrik-e-Istiqlal and subsequently merged it with assorted other Non-governmental organization and formed a new party called [[National Democratic Party (Pakistan)|National Democratic Party]], an event which caused another split in the party. Both Independence Movement and National Democratic Party suffered major shock  and setback when Omar Asghar Khan was murdered in Karachi on 25 June 2001 prior to the elections. An inquiry into his death was ordered by the [[Sindh High Court]] and in spite of repeated requests, it was never started.

In a historic press conference on 12 December 2011, Asghar Khan announced his full support to Pakistan Tehreek-e-Insaf (PTI) and Imran Khan.<ref name="Dawn Newspapers, 2011">{{cite news|last=Press Release|title=Asghar Khan backs Imran’s PTI|url=http://www.dawn.com/2011/12/12/asghar-khan-backs-imran%E2%80%99s-pti.html|accessdate=14 March 2012|newspaper=Dawn Newspapers, 2011|date=12 December 2011}}</ref>  He praised Imran Khan for his struggle and endorsed him as the only hope left for the survival of Pakistan.<ref name="Dawn Newspapers, 2011"/> This endorsement came at a crucial time for Imran Khan, when many tainted politicians were joining his party.<ref name="Dawn Newspapers, 2011"/>  After announcing his party's support for PTI, Asghar Khan resigned as President of Tehreek-e-Istiqlal and left the future of his party in the hands of his workers. Contrary to many media reports, Asghar Khan never joined PTI.<ref name="Pakistan Tribune">{{cite news|last=Press Release|title=Air Marshal (retd) Asghar Khan to join PTI|url=http://paktribune.com/news/Air-Marshal-retd-Asghar-Khan-to-join-PTI-245805.html|accessdate=14 March 2012|newspaper=Pakistan Tribune|date=12 December 2011}}</ref>

===Peace activism===

Besides political activism, Asghar Khan has been engaged in peace activism. On various occasion, Asghar Khan called for [[Indo-Pakistani relations|normalization of Indo-Pakistan relations]].<ref name="The News International, October 2011"/> Asghar Khan also renounced the nuclear tests operations conducted by Pakistan, targeting Prime minister [[Nawaz Sharif]] move for making that move.<ref name="The News International, October 2011"/> In 2011 Asghar Khan maintained that:

{{quote|text=In the last over 60 years, India has never attacked Pakistan, as it cannot afford it. Indians know well, if Pakistan is destroyed, they will be the next target... It was made our problem that one day India would invade us. But we did so four times and the first attack was on Kashmir, where Maharaja was not prepared to accede to India for he wanted to join Pakistan and waited for this for 21 days. Indian forces came to East-Pakistan when people were being slaughtered there. Moreover, again at Kargil, Indian never mounted an assault...|sign=Asghar Khan, 2011|source=<ref name="The News International, October 2011">{{cite news|last=Alvi|first=Mumtaz|title=Asghar Khan claims Pakistan attacked India four times since 1947|url=http://www.thenews.com.pk/Todays-News-6-73654-Asghar-Khan-claims-Pakistan-attacked-India-four-times-since-1947|accessdate=15 March 2012|newspaper=The News International, October 2011|date=21 October 2011}}</ref> }}

Asghar Khan blamed Zulfikar Ali Bhutto for [[Balochistan conflict]] and the [[Bangladesh Liberation War|East-Pakistan war]], terming it "inflexible attitude" of Bhutto.<ref name="The News International, October 2011"/> Commenting of his political collapse, Asghar Khan accused the [[Pakistani society|civil society]] for his failure, and marked that: " the majority in Pakistan voted for the (corrupt) politicians, as they also wanted their job done by "hook or by crook".<ref name="The News International, October 2011"/>

Asghar Khan also criticized Late Mr Bhutto on numerous occasions, holding him responsible for tyranny during the 1970 elections. During the Bangladesh Liberation War, Asghar Khan did support the East-Pakistan morally, alleging the West Pakistan under Bhutto, of depriving East Pakistan from their political and economical rights. He also demanded power to be handed over to the people of East Pakistan. In 1972, after Mr Bhutto was made president, Asghar Khan accused him for the break-up of the country.<ref> {{cite news |title= '''The Living History of Pakistan Vol-I''' by Inam R Sehri [2015] pps 1651-76 |url=http://inamsehri.com |publisher= GHP Surrey UK}} </ref> 

AM (rtd) Asghar Khan had once filed a petition with the Supreme Court of Pakistan in 1996. Till 1999 it was occasionally heard just to push away the time. During the same year Justice Saeeduzzaman Siddiqui, the then Chief Justice of Pakistan concluded the hearing but could not find courage to announce the final decision. The case file was shelved in the cold room.

The said Asghar Khan Case [challenging unlawful distribution of Rs:140 million amongst ‘some chosen’ politicians by the Pakistan Army’s ISI in 1990’s parliamentary elections] caught momentum when the apex court was moved once again in December 2011 [may be just by coincidence] on the basis of an article ‘Similar Looks wanted please’, appeared on internet media through www.Pkhope.com on 8th October 2011. 

It is an educated guess that the PPP's high command had picked up that article; asked some of its veteran lawyers to make out a fresh reminder for the Supreme Court, not to malign the army or its ISI but to drag certain politicians into slippery mud of mockery and contempt who were the recipients of huge amounts of money from government exchequer through the then president GIK’s planning. 

Later the same essay was made part of a book published in the UK which was picked up by the Supreme Court of Pakistan as a piece of evidence, attached it with case file and decided the case in February 2012 on the basis of its article titled ''Is Hamam Main Sab Nangay…''.<ref> {{cite news |title= '''Judges & Generals in Pakistan Vol-I''' by Inam R Sehri [2012] pps 168-73 |url=http://inamsehri.com |publisher= GHP Surrey UK}} </ref>
  
Asghar Khan has also authored 13 books, including  ''We've Learnt Nothing from History'', ''Pakistan at the Crossroads'' and ''Generals in Politics''.
Asghar Khan was married to Amina Shamsie in 1946 and they had five children, Nasreen, Shereen, Saira, [[Omar Asghar Khan|Omar]] (deceased) and [[Ali Asghar Khan (Pakistani politician)|Ali Asghar Khan]].

== Selected books ==
===English===
* {{cite book |last=Khan |first=Ashghar |date=1969 |title=Pakistan at the Cross Roads |location=Karachi |publisher=Ferozsons |oclc=116825}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=1979 |title=The First Round, Indo-Pakistan War 1965 |location=Sahibabad |publisher=Vikas |isbn=0-7069-0978-X}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=1983 |title=Generals in Politics |location=New Delhi |publisher=Vikas |isbn=0-7069-2215-8}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=1985 |title=The Lighter side of the Power Game |location=Lahore |publisher=Jang Publishers |oclc=15107608}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=2005 |title=We've Learnt Nothing from History |location=Karachi |publisher=Oxford University Press |isbn=0-19-597883-8}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=2008 |title=My Political Struggle |location=Karachi |publisher=Oxford University Press |isbn=978-0-19-547620-0}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=2009 |title=Milestones in a Political Journey |location=Islamabad |publisher=Dost Publications |isbn=978-9694963556}}
===Urdu===
* {{cite book |last=Khan |first=Ashghar |date=1985 |title=Sada-i-Hosh |language=ur |location=Lahore |publisher=Jang Publishers |oclc=14214332}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=1998 |title=Chehray nahi Nizam ko Badlo |language=ur |location=Islamabad |publisher=Dost Publications |isbn=978-9694960401}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=1999 |title=Islam – Jamhooriat aur Pakistan |language=ur |location=Islamabad |publisher=Dost Publications |isbn=978-9694960852}}
* {{cite book |last=Khan |first=Ashghar |author-mask=2 |date=1999 |title=Ye Batain Hakim Logon Ki |language=ur |location=Islamabad |publisher=Dost Publications |isbn=978-9694960876}}

== References ==
{{reflist}}

== External links ==
* [http://www.paffalcons.com/cas/asghar-khan.php Bio of Air Marshal Asghar Khan]
* [http://www.webcitation.org/query?url=http://www.geocities.com/siafdu/khan.html&date=2009-10-26+02:34:08 Biography of Asghar Khan]

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Arthur McDonald]]}}
{{s-ttl|title=[[Chief of Air Staff (Pakistan)|Commander-in-Chief, Pakistan Air Force]]|years=1957–1965}}
{{s-aft|after=[[Nur Khan]]}}
{{s-end}}

{{Military of Pakistan}}

{{DEFAULTSORT:Khan, Asghar}}
[[Category:1921 births]]
[[Category:Living people]]
[[Category:Amnesty International prisoners of conscience held by Pakistan]]
[[Category:Chiefs of Air Staff, Pakistan]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Pakistani anti-war activists]]
[[Category:Pakistani aviators]]
[[Category:Pakistan Tehreek-e-Insaf politicians]]
[[Category:Pakistani military historians]]
[[Category:People from Abbottabad]]
[[Category:Rashtriya Indian Military College alumni]]
[[Category:Pakistan International Airlines people]]
[[Category:Pakistani airline chief executives]]
[[Category:Pakistani prisoners and detainees]]