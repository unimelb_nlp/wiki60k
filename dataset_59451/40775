{{Orphan|date=May 2016}}

{{Infobox software
| name                   = OpenDataPlane
| logo                   = [[File:OpenDataPlane logo.jpg|250px|center]]
| author                 = Open Source Project bootstrapping under Linaro’s Networking Group
| latest_release_version = [https://git.linaro.org/lng/odp.git/tag/refs/tags/v1.11.0.0_monarch 1.11.0.0]
| stable_release date    = {{Start date|2015|09|03|df=y}}
| status                 = Active
| programming_language   = C
| license                = [[BSD-3]]
| website                = {{URL|opendataplane.org}}
}}

The '''OpenDataPlane project (ODP)''' is an [[open-source]], cross-platform set of [[application programming interface]]s (APIs) for the networking [[Forwarding plane|data plane]].<ref>{{cite web|author=ARM Connected Community |url=http://community.arm.com/people/bobmonkman/blog/2013/10/29/linaro-networking-group-launches-new-open-source-initiative-around-data-plane-programming-apis|title= Linaro Networking Group launches new open-source initiative around data plane programming APIs|publisher=ARM.com |date=2013-10-29 |accessdate=2013-10-29}}</ref> OpenDataPlane implementations for several processors, including ARM, Intel x86, MIPS and Power have existed since September 2014.<ref>{{cite web|author=Cambridge network |url=http://www.cambridgenetwork.co.uk/news/linaro-wider-portability-networking-apps-opendataplane|title= Linaro enables wider portability of high speed networking apps with OpenDataPlane v1.0|publisher=Cambridgenetwork.com |date=2015-04-03 |accessdate=2015-04-03}}</ref> Most recently Virtual Open Systems has demonstrated VOSYSwitch connecting chained VNFs, with the virtualized infrastructure implementing an OpenFastPath web server behind an OpenDataPlane enabled packet filtering firewall.<ref>{{cite web|author=Virtual Open Systems|url=http://www.virtualopensystems.com/en/solutions/demos/vosyswitch-odp-armv8/|title= VOSYSwitch leverages ODP to enable accelerated VNFs chaining on ARMv8 NFV servers|publisher=VirtualOpensystems.com |date=2016-03-10 |accessdate=2016-03-10}}</ref>

==History==
On October 29, 2013 Linaro announced that it was collaborating with members of the Linaro Networking interest Group to develop and host an open standard application programming interface for data plane applications.<ref>{{cite web|author=Linaro Limited |url=http://www.linaro.org/news/linaro-launches-opendataplane-odp-project-deliver-open-source-cross-platform-interoperability-networking-platforms/|title= Linaro launches OpenDataPlane™ (ODP) project to deliver open-source, cross-platform interoperability for networking platforms |publisher=Linaro.org |date=2013-10-29 |accessdate=2013-10-29}}</ref> Initially defined by members of the Linaro Networking Group, this project is open to contributions from all individuals and companies who share an interest in promoting a standard set of APIs to be used across the full range of network processor architectures available.

==Technology Overview==
[[File:ODP diagram.jpg|thumb|left|The OpenDataPlane project is an open-source, cross-platform set of application programming interfaces (APIs) for the networking data plane.]] ODP consists of an API specification and a set of reference implementations that realize these APIs on different platforms.<ref>https://www.ietf.org/proceedings/90/slides/slides-90-forces-6.pdf</ref> Implementations range from pure software to those that deeply exploit the various hardware acceleration and offload features found on modern networking [[System-on-Chip]] (SoC) processors.

ODP’s goal is to allow implementers of the API great flexibility to exploit and optimize the implementation. This is intended to enable easy platform portability such that an application written to the API can pick up performance gains without needing significant platform knowledge when ported.

ODP is currently being used to develop reference platform implementations of [[Open Platform for NFV]] (OPNFV) <ref>{{cite web|author=Enea|url=http://www.businesswire.com/news/home/20150505006472/en/Enea-AB-ARM-Enea-Demonstrate-Reference-Platform#.VUop8nU4l4s |title= Enea AB: ARM and Enea Demonstrate Reference Platform of Open Platform for Network Function Virtualization|publisher=Businesswire.com |date=2015-05-05 |accessdate=2015-05-05}}</ref> and is being promoted <ref>{{cite web|author=ARM Connected Community|url=http://community.arm.com/docs/DOC-10220|title= The Emergence of the OpenDataPlane Standard|publisher=ARM.com |date=2015-05-18 |accessdate=2015-05-18}}</ref><ref>{{cite web|author=Marvell|url=http://www.marvell.com/company/news/pressDetail.do?releaseID=7816|title= Marvell Expands its 32-bit and 64-bit ARMADA SoC Family of Embedded Processors with Robust Ecosystem of Software Solutions and Partners for a Variety of Applications|publisher=Marvell.com |date=2016-02-23 |accessdate=2016-02-23}}</ref>  by companies as part of their data plane support initiatives.

Products were announced by companies such as Kalray with many acronyms.<ref>{{cite web|author=Kalrayinc|url=http://www.kalrayinc.com/kalray-to-launch-high-speed-io-processors/|title= Kalray To Launch High Speed I/O Processors|publisher=kalrayinc.com |date=2016-02-09 |accessdate=2016-02-09}}</ref> The OpenDataPlane run to completion execution models and framework are also being used by FastPath applications to leverage OpenFathPath functionality. DPDK is supported in the OpenFastPath release through the ODP-DPDK layer. The intent of OpenFastPath is to enable accelerated routing/forwarding for IPv4 and IPv6, tunneling and termination for a variety of protocols.<ref>{{cite web|author=OpenFastPath|url=https://www.youtube.com/watch?v=c6qM98_xtjQ|title= OpenFastPath-An Open Source Accelerated IP Fast Path|publisher=openfastpath.org |date=2016-03-15 |accessdate=2016-03-15}}</ref>

==Implementations==
There is a [[Linux]] based reference software implementation of the ODP API, intended to be a functional model to establish the API behavior. In conjunction with a validation suite, this gives a base for accelerated implementations to extend.
Current ODP implementations exist for several processors, with varying degrees of hardware offload:

===Current ODP Implementations===
{| class="wikitable" style="font-size: 85%; text-align: center"
|-
! Name
! Owner/Maintainer
! Target Platform
! Architecture
|-
|odp-linux
|Open contribution, maintained by LNG
|Pure software implementation, runs on any Linux kernel.  Not a performance target but has Netmap support
|Any
|-
|odp-dpdk
|Open contribution, developed by LNG
|Intel x86 using DPDK as a software acceleration layer
|Intel x86
|-
|odp-keystone2
|Texas Instruments
|TI Keystone II SoCs
|ARM Cortex-A-15
|-
|linux-qoriq
|NXP
|NXP QorIQ SoCs<ref>{{cite web|author=Freescale (currently NXP)|url=http://media.nxp.com/phoenix.zhtml?c=254228&p=irol-newsArticle&ID=2118929|title=Freescale Supports OpenDataPlane for Software-Defined Networking Based on QorIQ Processing Platforms|publisher=NXP.com |date=2015-03-03 |accessdate=2015-08-12}}</ref>
|Power & ARMv8
|-
|OCTEON
|Cavium Networks
|Cavium Octeon™ SoCs
|MIPS64
|-
|THUNDER<ref>{{cite web|author=Cavium|url=http://www.cavium.com/newsevents-Cavium-Breaks-100Gbps-IPsec-Throughput-Barrier-using-OpenDataPlane_MWC2015.htm|title=Cavium Breaks 100Gbps IPsec Throughput Barrier using OpenDataPlane™ at Mobile World Congress 2015|publisher=Cavium.com |date=2015-03-02 |accessdate=2015-03-02}}</ref>
|Cavium Networks
|Cavium ThunderX™ SoC
|ARMv8
|-
|Kalray<ref>{{cite web|author=Kalray|url=https://github.com/kalray/odp-mppa/|title=OpenDataPlane port for the MPPA platform}}</ref>
|Kalray
|MPPA platform
|MPPA
|-
|-
|odp-hisilicon<ref>{{cite web|author=HiSilicon|url=https://github.com/open-estuary/packages/tree/master/odp|title=OpenDataPlane port for the Hisilicon platform}}</ref>
|Hisilicon
|Hisilicon platform
|ARMv8
|-
|}

==Releases==
The following lists the different OpenDatePlane releases:

{| class="wikitable sortable"
|-
! Release Name !! Release Date
|-
| OpenDataPlane v1.0.0 || February 27, 2015
|-
| OpenDataPlane v1.0.1 || March 17, 2015
|-
| OpenDataPlane v1.0.2 || March 27, 2015
|-
| OpenDataPlane v1.0.3 || April 17, 2015
|-
| OpenDataPlane v1.0.4 || April 30, 2015
|-
| OpenDataPlane v1.1 || May 13, 2015
|-
| OpenDataPlane v1.2 || July 22, 2015
|-
| OpenDataPlane v1.3 || August 31, 2015
|-
| OpenDataPlane v1.4 || September 30, 2015
|-
| OpenDataPlane v1.4.1 || November 13, 2015
|-
| OpenDataPlane v1.5 || December 1, 2015
|-
| OpenDataPlane v1.6 || December 31, 2015
|-
| OpenDataPlane v1.7 || February 8, 2016
|-
| OpenDataPlane v1.8 || March 4, 2016
|-
| OpenDataPlane v1.9 || April 15, 2016
|-
| OpenDataPlane v1.10 || April 29, 2016
|-
| OpenDataPlane v1.10.1 || June 14, 2016
|-
| OpenDataPlane v1.11 || August 18, 2016
|}

==Ecosystem==
The following organizations currently sponsor the development of ODP.
*[[ARM Holdings|ARM]]
*[[Broadcom]]<ref>{{cite web|author=Broadcom|url=http://www.broadcom.com/press/release.php?id=s827048|title=Broadcom Announces Open Network Function Virtualization Platform|publisher=broadcom.com |date=2014-02-20 |accessdate=2014-02-20}}</ref>
*[[Cavium]]<ref>{{cite web|author=Cavium|url=http://www.cavium.com/newsevents-Linaro-Announces-First-LTS-Monarch-Release-of-OpenDataPlane.html|title=Linaro Announces First LTS Monarch Release of OpenDataPlane|publisher=cavium.com |date=2016-08-19 |accessdate=2016-08-19}}</ref>
*[[Cisco]]
*[[ENEA AB]] <ref>{{cite web|author=Enea|url=http://www.enea.com/Corporate/Press/Press-releases/Press-release/?item=853938|title=Enea Demonstrates Open Event Machine Implementation on Broadcom XLP Architecture at MWC|publisher=enea.com |date=2015-02-13 |accessdate=2015-02-13}}</ref>
*[[Ericsson]]
*[[HiSilicon]]
*[[Linaro]]
*[[MontaVista]]
*[[Nokia]]
*[[NXP]]
*[[Texas Instruments]]
*Wind - formerly [[Wind River Systems]]
*[[ZTE]]

==References==
{{reflist}}

[[Category:Application programming interfaces| ]]
[[Category:Free software]]