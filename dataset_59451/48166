{{Use mdy dates|date=November 2016}}
<!-- Do not remove this line! -->
{{Infobox person
| name        = Richard Gingras
| image = Richard Gingras 11-20-2011.jpg
| caption = Richard Gingras, 2011
| birth_name  = Richard Louis Gingras
| birth_date  = {{Birth date and age|1952|1|17|mf=y}}
| birth_place = [[Providence, Rhode Island]], [[United States]]
| nationality = American
| alma_mater  = [[Boston College]]
| employer    = [[Google]]
| spouse      = Mitzi Trumbo
| children    = 2
| boards      = [[First Amendment Coalition]]<br>[[International Center for Journalists]]<br>[[World Computer Exchange]]}}
'''Richard Gingras''' is an American Internet executive and entrepreneur, who has focussed on emerging digital media since 1979,<ref name=RG-B-01/> including efforts at Google, Apple Computer, Salon Media Group and the Public Broadcasting Service. Gingras has been an outspoken proponent for journalistic innovation on the Internet.<ref name=RG-B-05/><ref name=RG-B-06/><ref name=RG-B-07/>

== Career ==

Gingras is currently vice president of news at Google.

Gingras was a key instigator in creating the [[Accelerated Mobile Pages]] (AMP) project which is an open-source effort to improve the speed of the World Wide Web and improve advertising user experience.<ref name=RG-B-02/><ref name=RG-B-08/> In late 2014, Gingras co-founded the Trust Project<ref name=RG-B-10/><ref name=RG-B-11/> with Sally Lehrman of the Markulla Center for Ethics at Santa Clara University. The Trust Project is a global effort of the journalism community to explore how the architecture of journalism can be enhanced to improve the perceived credibility of high-quality journalism.

Until July 2011, Gingras was CEO of [[Salon (website)|Salon Media Group]] which operates the news site Salon.com and the pioneering virtual community The Well. Gingras has had a long association with Salon having assembled its initial seed financing in 1995. During 2007 and 2008, Gingras served as a strategic advisor to the executive team at Google focusing on strategies relating to the evolution of news and television.

In 2002, Gingras co-founded Goodmail Systems and served as its CEO and chairman. Goodmail Systems developed certified email services offered through large email providers including Yahoo and America Online. Gingras also served as interim president of MyPublisher from 2000-2001 and guided the design of a custom hardcover photo book service introduced by Apple Computer as part of iPhoto.

From early 1996 to mid 2000, Gingras led online service efforts at Excite@Home as Senior Vice President and General Manager of the company's consumer-focused product division, Excite Studios.

In January 1996, Gingras joined the early consumer broadband network @Home Network, as vice president of programming and editor-in-chief where he was responsible for the launch of @Home's broadband-enabled online portal. @Home was founded by the venture capital firm Kleiner Perkins in partnership with major US cable companies to offer high-speed Internet access.

At Apple Computer in the early 1990s Gingras led the development of the online service [[eWorld]]. A pre-Web online service, eWorld was considered innovative for its time, but it was expensive and failed to attract a high number of subscribers. The service was only available on the Macintosh, though a PC version had been planned. Following the merger of @Home with Excite, Gingras became Senior Vice President and General Manager of the company's consumer-focused product division, Excite Studios.

Gingras's work in interactive digital media began In 1979, when he produced one of the first interactive online news magazine which was delivered to several hundred test households using interactive television technology known as [[Teletext systems|broadcast teletext]]. Gingras led the effort for the PBS service (KCET in Los Angeles) which also included service components for use in schools.

From 1987 to 1992, Gingras was the founder and president of MediaWorks, an Apple-funded startup that developed early news-agenting and executive support software for Fortune 500 corporations.

From 1983 to 1986, Gingras assembled and managed a network of television stations in the top fifty US markets to provide sideband data distribution for a news and advertising service, Silent Radio, that was presented on electronic displays in retail locations.

== Boards and honors ==

Gingras serves on the boards of the First Amendment Coalition, the International Center for Journalists and the World Computer Exchange

In the fall of 2012, Gingras was recognized by Louisiana State University with the Manship Prize<ref name=RG-B-03/> for contributions to the evolution of digital media. In May 2013, Gingras gave the commencement speech at West Virginia University for the Reed School of Journalism.<ref name=RG-B-04/> In 2013, Gingras was a subject of the digital media oral history project<ref name=RG-B-01/> produced by the Joan Shorenstein Center on the Press, Politics and Public Policy at Harvard University.

== References ==

{{Reflist|30em|refs=<ref name=RG-B-01>{{cite web|url=http://www.niemanlab.org/riptide/person/richard-gingras/l|first=John|last=Huey|author2=Nisenholtz, Martin |work=[[Shorenstein Center on Media, Politics and Public Policy]] at Harvard University|title=Riptide: An oral history of the epic collision between journalism and digital technology, 1980 to the present|date=April 1, 2014|accessdate=April 21, 2014}}</ref>
<ref name=RG-B-02>{{cite news|url=http://buzzmachine.com/2015/10/07/faster-distributed-web/|first=Jeff|last=Jarvis|work=Buzz Machine|title=To a Fast and Distributed Web: AMP HTML|date=October 15, 2015|accessdate=October 7, 2015}}</ref>
<ref name=RG-B-03>{{cite web|url=http://www.lsu.edu/ur/ocur/lsunews/MediaCenter/News/2012/10/item53901.htmll|first=|last=Louisiana State University Media Center|work=[[Louisiana State University]], Manship School of Mass Communications|title=Google's Gingras Honored with 2012 Manship Prize|date=October 23, 2012|accessdate=April 13, 2014}}</ref>
<ref name=RG-B-04>{{cite web|url=https://www.youtube.com/watch?v=UvctjG_pbnc&feature=youtu.be&t=15m54s|first=|last=West Virginia University|work=[[West Virginia University]]|title=Perley Isaac Reed School of Journalism, 144th Commencement, West Virginia University|date=May 18, 2013|accessdate=April 20, 2014}}</ref>
<ref name=RG-B-05>{{cite news|url=http://www.poynter.org/latest-news/top-stories/185089/googles-gingras-the-future-of-journalism-can-and-will-be-better-than-its-past|work=[[Poynter Institute]]|title=Google's Gingras: 'The future of journalism can and will be better than its past.'|date=August 15, 2012|accessdate=May 3, 2014}}</ref>
<ref name=RG-B-06>{{cite news|url=http://www.rainews.it/dl/rainews/articoli/Richard-Gingras-evoluzione-ecosistema-news-bisogna-ripensare-tutto-503d0279-bf4a-47af-ad1b-cce55e450a62.html|first=Celia|last=Guimareas|work=[[Rai News24]]|title=Richard Gingras and the evolution of the ecosystem of the news: 'We need to rethink everything'|date=May 6, 2014|accessdate=May 8, 2014}}</ref>
<ref name=RG-B-07>{{cite news|url=http://gigaom.com/2012/05/12/googles-head-of-news-newspapers-are-the-new-yahoo|first=Matthew|last=Ingram|work=[[GigaOm]]|title=Google's head of news: Newspapers are the new Yahoo|date=May 12, 2012|accessdate=May 2, 2014}}</ref>
<ref name=RG-B-08>{{cite news|url=http://www.ampproject.org|work=AMP Project|title=Instant Everywhere|date=October 7, 2015|accessdate=October 7, 2015}}</ref>
<ref name=RG-B-10>{{cite news|url=https://medium.com/@GingrasLehrman/online-chaos-demands-radical-action-by-journalism-to-earn-trust-ea94b06cbccb#.dsr5g7xhx|first=Sally|last=Lehrman|work=Markkula Center for Ethics|title=Online Chaos Demands Radical Action by Journalism to Earn Trust|date=October 16, 2014|accessdate=October 16, 2014}}</ref>
<ref name=RG-B-11>{{cite news|url=https://www.youtube.com/watch?v=ZDWxP-kr26Q|work=Newsgeist Unconference 2014|title=Trust|date=November 14, 2014|accessdate=November 14, 2014}}</ref>
}}

{{DEFAULTSORT:Gingras, Richard}}
[[Category:1952 births]]
[[Category:Living people]]
[[Category:American technology chief executives]]
[[Category:American computer businesspeople]]
[[Category:Google employees]]
[[Category:Businesspeople in information technology]]
[[Category:Internet pioneers]]
[[Category:Boston College alumni]]
[[Category:Businesspeople from Rhode Island]]
[[Category:20th-century American businesspeople]]
[[Category:21st-century American businesspeople]]
[[Category:American technology company founders]]