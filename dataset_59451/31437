{{featured article}}
{{Infobox coin
| Country             = United States
| Denomination        = Hawaii Sesquicentennial half dollar
| Value               = 50 cents (0.50
| Unit                = [[United States dollar|US dollars]])
| Mass                = 12.5
| Diameter            = 30.61
| Diameter_inh       = 1.20
| Thickness           = 2.15
| Thickness_inch      = 0.08
| Edge                = [[Reeding|Reeded]]
| Silver_troy_oz      = 0.36169
| Composition =
  {{plainlist |
* 90.0% silver
* 10.0% copper
 }}
| Years of Minting    = {{Start date|1928}}
| Mintage = 10,008 including 8 pieces for the [[United States Assay Commission|Assay Commission]]
| Mint marks            = None, all pieces struck at Philadelphia Mint without mint mark
| Obverse             = {{Css Image Crop|Image = 1928 50C Hawaiian.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 3|Location = center|alt=A silver coin showing a man in formal 18th century dress, facing left }}
| Obverse Design      = Captain [[James Cook]]
| Obverse Designer    = [[Chester Beach]], based on sketches by [[Juliette May Fraser]]
| Obverse Design Date   = 1928
| Reverse               = {{Css Image Crop|Image = 1928 50C Hawaiian.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center|alt=A silver coin, depicting a man in native dress holding a spear (used like a walking stick)}}
| Reverse Design        = A Hawaiian chieftain with extended arm; [[Waikiki Beach]] and [[Diamond Head, Hawaii|Diamond Head]] in background
| Reverse Designer      =[[Chester Beach]], based on sketches by [[Juliette May Fraser]]
| Reverse Design Date   = 1928
}}

The '''Hawaii Sesquicentennial half dollar''' was struck in 1928 by the [[United States Mint|United States Bureau of the Mint]] in honor of the 150th anniversary of Captain [[James Cook]]'s landing in Hawaii, the first European to reach there. The coin depicts Captain Cook on the [[obverse and reverse|obverse]] and a Hawaiian chieftain on the reverse. Only 10,000 were struck for the public, making it rare and valuable.

In 1927, the legislature of the [[Territory of Hawaii]] passed a resolution calling on the U.S. government to produce a commemorative coin for the 150th anniversary of Cook's arrival in Hawaii. Treasury Secretary [[Andrew Mellon]] thought the occasion important enough that, unusually for him, he did not oppose such an issue.  The bill for the Hawaii half dollar passed through Congress without opposition or amendment, and became the Act of March 7, 1928 with the signature of President [[Calvin Coolidge]].

Sculptor [[Chester Beach]] made the plaster models for the coins from sketches by [[Juliette May Fraser]]. Beach had some trouble gaining approval for his designs, as there were issues raised by the Mint and by [[Victor Stewart Kaleoaloha Houston]], Hawaii Territory's [[Non-voting members of the United States House of Representatives|delegate to Congress]]. These concerns were eventually addressed, and the coin went into production.  Although the issue price, at $2, was the highest for a commemorative half dollar to that point, the coins sold out quickly and have risen in value to over a thousand dollars.

== Inception ==
The Hawaii Sesquicentennial half dollar was proposed because of the observances there for the 150th anniversary of Captain [[James Cook]] becoming the first European to reach the [[Hawaiian Islands]], or, as it was termed then, its discovery. Planners decided on a date for the celebrations as August 1928, as midway between the sesquicentennial of Cook's landing in January 1778 and of his death in the islands in February 1779. A resolution was passed by the legislature of the [[Territory of Hawaii]]{{efn|Hawaii became a state in 1959.}} to give the celebrations official status, and to ask the federal government to have the armed forces participate.  The resolution requested that Washington invite the [[United Kingdom]] (Cook's allegiance) and other nations. It also asked the federal government to issue a [[half dollar (United States coin)|half dollar]] and postage stamps in honor of the anniversary.{{sfn|House hearings|pp=2–5}}  At the time, commemoratives were not sold by the government—Congress, in authorizing legislation, designated an organization which had the exclusive right to purchase the coins at face value and vend them to the public at a premium.{{sfn|Slabaugh|pp=3–5}} In the case of the Hawaii half dollar, the Cook Sesquicentennial Commission of Hawaii was the designated group.{{sfn|House hearings|p=1}} 

Bruce Cartwright, Jr., was in charge of choosing a coin design for the Cook commission.   Mrs. Ethelwyn Castle, a civic-minded person, arranged for him to meet [[Juliette May Fraser]], a local artist.  Cartwright had prepared cartoon-style drawings, with the portrait of Cook based on a [[Wedgwood]] plaque that had been owned by [[Emma of Hawaii|Queen Emma]], showing the explorer facing right. Within two days, Fraser had produced sketches.{{sfn|Medcalf & Russell|p=50}} On November 2, Charles Moore, chairman of the [[Commission of Fine Arts]]{{efn|The commission, pursuant to a 1921 [[executive order]] by President [[Warren G. Harding]], renders advisory opinions on proposed designs for coins. See {{harvnb|Taxay|pp=v–vi}}.}} wrote to Assistant [[Director of the Mint]] [[Mary M. O'Reilly]] that Juliette Fraser's sketches were excellent and would translate well into a coin.{{sfn|Taxay|p=124}}
 
The Commission of Fine Arts met, and, at the suggestion of sculptor-member [[Lorado Taft]], decided to ask [[Buffalo nickel]] designer [[James Earle Fraser (sculptor)|James Earle Fraser]] (no relation) who would be most suitable to turn the sketches into plaster models, from which the Mint could make coinage dies and [[Glossary of numismatics#H|hubs]]. James Fraser suggested [[Peace dollar]] designer [[Anthony de Francisci]], but sculptor [[Chester Beach]] was engaged instead.{{sfn|Flynn|pp=276–277}}

== Legislation ==
Numismatic historian [[Don Taxay]] thought it likely that members of the House Committee on Coinage, Weights and Measures had agreed to support a Hawaii half dollar prior to a bill being submitted, as preparations had already begun.{{sfn|Taxay|p=123}} Legislation for such was introduced into the House of Representatives by the territory's [[Non-voting members of the United States House of Representatives|delegate to Congress]], [[Victor Stewart Kaleoaloha Houston]], on December 5, 1927.<ref name ="profile">{{cite web|url=http://congressional.proquest.com/congressional/result/pqpresultpage?accountid=14541&groupid=96011&pgId=d16c845c-1b86-401a-8374-616fe408d3d4&rsId=15457975EFC|title=70 Bill Profile H.R. 81 (1927–1929)|publisher=ProQuest Congressional|accessdate=May 26, 2016}} {{subscription}}</ref> It was referred to the coinage committee, of which New Jersey Congressman [[Randolph Perkins]] was the chair, and which held hearings on the bill on January 23, 1928. Delegate Houston appeared in support of his bill, and to the surprise of committee members, had gotten a statement from [[United States Secretary of the Treasury|Treasury Secretary]] [[Andrew Mellon]], stating that Mellon did not oppose the bill.  Usually, when a commemorative coin was proposed, Mellon argued that a medal should be issued instead.  This had been the case for the [[Norse-American medal]] three years previously; its sponsor, committee member [[Ole J. Kvale]] of Minnesota, had scuttled plans for a coin because of Treasury Department opposition. Congressman Kvale was "very much interested in learning what powers of persuasion have been exercised by the gentleman from Hawaii to bring out such a favorable report".{{sfn|House hearings|pp=1–3}} Kvale, a Norse-American, asked, "why this discrimination against two and a half million people in the United States has come about in favor of about 35,000 whites in that Territory?"{{sfn|House hearings|p=4}}

Houston stated he had not lobbied the Treasury for the coin, and Perkins, before promising to find out more information, speculated that perhaps it was because the coins were to be issued far from the continental United States.  Houston told the committee that the coin was "something that may be kept by those who attend the celebration as a memorial of it and will be available to foreigners who come there, as well as our own people who celebrate the occasion".{{sfn|House hearings|pp=3–4}}  Kvale stated he would vote for the bill. Mississippi's [[Bill G. Lowrey]] noted that as he had said before, he would not vote for any coin bill; Perkins agreed that Lowrey had made his position clear.{{sfn|House hearings|pp=4–5}} Perkins issued a report on February 1, 1928, recounting the history behind the proposed coin and indicating his committee's support.<ref>{{cite web|author=House Committee on Coinage, Weights and Measures|url=http://congressional.proquest.com/congressional/docview/t47.d48.8835_h.rp.535?accountid=14541|title=To authorize coinage of silver 50-cent pieces in commemoration of the one hundred and fiftieth anniversary of discovery of the Hawaiian Islands, etc.|date=February 1, 1928}} {{subscription}}</ref>

The bill was passed without objection by the House of Representatives on February 20, 1928.<ref>{{USCongRec|1928|3278–3279|date=April 20, 1928}}</ref> The bill was received by the Senate the following day and was referred to the [[United States Senate Committee on Banking, Housing, and Urban Affairs|Committee on Banking and Currency]]. On February 27, South Dakota's [[Peter Norbeck]] reported the bill back to the Senate without amendment, and included in the report a letter from Secretary Mellon to Perkins dated February 13, in which Mellon expanded on his reasons for not opposing the Hawaii coin legislation. Mellon stated that only a token number of pieces would be issued, and that the celebration, sponsored by the territorial government, was of national significance. <ref>{{cite web|author=Senate Committee on Banking and Currency|url=http://congressional.proquest.com/congressional/docview/t47.d48.8829_s.rp.432?accountid=14541|title=To authorize coinage of silver 50-cent pieces in commemoration of the one hundred and fiftieth anniversary of discovery of the Hawaiian Islands, etc.|date=February 27, 1928}} {{subscription}}</ref>
The bill was passed by the Senate on March 2, 1928 without recorded opposition<ref>{{USCongRec|1928|3949|date=March 2, 1928}}</ref> and was enacted on March 7, 1928 with the signature of President [[Calvin Coolidge]]. It provided for the issuance of up to 10,000 half dollars in honor of the sesquicentennial, with the commission's profits from the coin to be used toward establishing a Captain James Cook collection in the territorial archives.<ref name = "profile" />

== Preparation ==

Once Beach accepted the commission on March 12, 1928, Juliette Fraser's sketches were forwarded to him. On April 7, he sent completed models to the Mint and photographs to the Fine Arts Commission. Both the Mint and Houston responded with criticism, the former that the [[relief]] of the coin was high and difficult to [[reducing lathe|reduce]] to coin-sized hubs.{{sfn|Taxay|pp=123–127}} On April 19, Mint Chief Engraver [[John R. Sinnock]] wrote in a memorandum that the coin would be very hard to produce because the area of greatest relief on each side was in the same part of the design.{{sfn|Flynn|p=98}} Beach agreed to lower any high points that might cause the Mint difficulty.{{sfn|Taxay|p=130}}

Delegate Houston had a long list of quibbles about the coin's design.  For example, Beach had placed an anklet on the chief's leg; Houston felt such an item would not have been worn. Beach defended some of his choices, such as the anklet (which was removed when Houston insisted), and promised to comply with the remainder. This did not satisfy Houston, who was also unhappy about the shape of the palm tree on the coin, and Beach modified the design again.  Beach forwarded final models, indicating that he would only consider making changes if the Mint requested it.  He wrote to Moore, "I think the proper thing for Mr. Houston to do would be to take the sculptor and family to Hawaii and let us live in the cocoanut {{sic}} trees for a while and absorb the atmosphere of that paradise."{{sfn|Taxay|pp=128–131}}

The coin was endorsed by the Commission of Fine Arts; on May 9, O'Reilly wrote to Beach that the design had received Secretary Mellon's approval.{{sfn|Taxay|p=131}}

== Design ==

[[File:Kamehameha I full 5110.png|thumb|upright|Statue of [[Kamehameha I]], Honolulu|alt=An impressive statue of a dark-skinned man, richly dressed in native garb.  One arm is extended, as if in greeting, and he holds a spear, butt end grounded, in the other.]]
The obverse depicts Captain James Cook.  He faces towards a compass needle and the words <small>CAPT. JAMES COOK DISCOVERER OF HAWAII;</small>{{sfn|Swiatek|p=211}} thus, his gaze is westward.{{sfn|Flynn|p=278}} <small>[[In God We Trust|IN GOD WE TRUST]]</small> is to the right, the name of the country above, and the denomination below.  The words <small>HALF DOLLAR</small> are flanked by eight triangles, four on each side.  These represent the eight largest volcanic islands of Hawaii: [[Oahu]], [[Maui]], [[Kauai]], the "Big Island" of [[Hawai'i (island)|Hawaii]], [[Niihau]], [[Lanai]], [[Kahoolawe]], and [[Molokai]].{{sfn|Slabaugh|p=84}} The designer's initials, <small>CB</small>, are found to the right of the base of the bust.{{sfn|Swiatek|p=212}}

The reverse is based on a [[Kamehameha Statues|statue of]] King [[Kamehameha I of Hawaii]], designed by [[Thomas R. Gould]], and intended "to symbolize the past and future glory of the Kingdom [of Hawaii]".{{sfn|Medcalf & Russell|p=50}} [[Kamehameha Statue (Honolulu cast)|The sculpture]] that stands in downtown [[Honolulu]] (1883) is a replacement for one that sank while being transported from Germany; [[Kamehameha Statue (original cast)|the original]] (1880) was later salvaged and stands at [[Kohala, Hawaii|Kohala]] on the island of Hawaii.{{sfn|Medcalf & Russell|p=50}} The coin features a Hawaiian chieftain in ceremonial dress surmounting the top of a hill, with his arm extended in greeting. He represents Hawaii rising from obscurity. The palm tree that rises above him is intended to signify romance. A Hawaiian village at [[Waikiki Beach]] with [[Diamond Head, Hawaii|Diamond Head]] nearby are seen behind him, representing history and antiquity. The design is almost free of lettering, with only "<small>[[E Pluribus Unum|E PLURIBUS UNUM]]</small>" ("Out of many, one") and <small>1778 1928</small> to be seen.{{sfn|Slabaugh|p=84}} Ferns are visible under that Latin motto: Houston wanted the plants removed, but Beach insisted on retaining them to balance the design.{{sfn|Taxay|p=129}} Juliette Fraser had made a number of sketches, all with the same basic design elements, but with the chieftain in various poses and with Diamond Head in different positions.{{sfn|Medcalf & Russell|p=50}}

Art historian [[Cornelius Vermeule]], in his volume on U.S. coins and medals, wrote that the obverse "is too crowded, despite the large, flat, clothed bust" and that the various elements of the reverse design "are all too much for one small coin".{{sfn|Vermeule|pp=174–175}} He deemed "the coin honoring Hawaii in 1928&nbsp;... no more a credit to Chester Beach than was the [[Lexington-Concord half dollar|Lexington Concord coin]] [of 1925]".{{sfn|Vermeule|p=174}}

== Production, distribution, and collecting ==
The [[Philadelphia Mint]] coined 10,008 Hawaii Sesquicentennial half dollars in June 1928, with the eight pieces above the authorized mintage reserved for inspection and testing at the 1929 meeting of the annual [[Assay Commission]]. Fifty of the ten thousand were specially finished as sandblast [[proof coin|proof pieces]],{{sfn|Bowers|p=237}} to be presented to various individuals and institutions, such as members of the Cook commission, President Coolidge, and the British [[Admiralty]].  Of the remainder, half were to be sold on the Hawaiian Islands, half reserved for orders from elsewhere.  The [[Bank of Hawaii]] took charge of distribution on behalf of the Cook commission.{{sfn|Swiatek & Breen|p=96}} The price was $2 per coin, the highest for a half dollar commemorative to that point.{{sfn|Bowers|p=238}}

[[File:Hawaii stamp.JPG|thumb|right|upright|Special issue by the [[United States Post Office Department]] for the anniversary of Cook's arrival|alt=A red two-cent stamp showing George Washington, facing left.  The design is overprinted with the words HAWAII 1778–1928]]
Sales began October 8, 1928; supplies were quickly exhausted.  Numismatists Anthony Swiatek and [[Walter Breen]], in their book on commemoratives, write that while "there was never any scandal about these coins", there were unconfirmed rumors of hoards of coins, totaling as many as 1,500, taken from the allocation for Hawaiians and kept off the market.{{sfn|Swiatek & Breen|pp=96–97}} One such grouping, of 137 pieces, comprised coins from an allotment for the Bank of Hawaii for sale to its employees. When the coin the bank put on display was stolen, the bank president took the others off sale, and they remained in the vaults until 1986, when they were sold at auction.{{sfn|Bowers|p=237}} Many Hawaii Sesquicentennial half dollars were purchased by non-collectors and display the effects of poor handling.{{sfn|Medcalf & Russell|p=48}}

The Captain Cook Memorial Collection, purchased in part with funds raised from the coins, is now in the [[Bishop Museum]] in Honolulu.{{sfn|Flynn|p=97}} The Hawaii Sesquicentennial coin is the scarcest commemorative half dollar by design; according to [[R.S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'' published in 2015, it lists for between $1,850 and $11,000 depending on condition.  The sandblast proofs are listed for up to $50,000 but none has recently been sold at auction—an exceptional specimen of the regular type went under the hammer for $25,850 in 2013.{{sfn|Yeoman|pp=1138–1139}} At least three different counterfeits are known.{{sfn|Medcalf & Russell|p=48}}

==See also==
* [[Early United States commemorative coins]]
* [[Half dollar (United States coin)]]

== Notes ==
{{Notes}}

== References ==
{{Reflist|20em}}

== Sources ==
* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 1992
  | title = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter08-067.aspx
  | publisher = Bowers and Merena Galleries, Inc.
  | location = Wolfeboro, NH
  | isbn = 978-0-943161-35-8
  | ref = {{sfnRef|Bowers}}
  }}
* {{cite book
  | last = Flynn
  | first = Kevin
  | year = 2008
  | title = The Authoritative Reference on Commemorative Coins 1892–1954
  | publisher = Kyle Vick
  | location = Roswell, GA
  | oclc = 711779330
  | ref = {{sfnRef|Flynn}}
  }}
*{{cite book
 | last=Medcalf
 | first = Donald
 | last2 = Russell
 |first2 = Ronald
 | title=Hawaiian Money Standard Catalog
 | edition = second
 | year = 1991
 | origyear = 1978
 | publisher = Ronald Russell<!-- This is a reliable source although partially self-published as Medcalf was a major coin dealer in Honolulu and an expert, the book appears thoroughly researched, and it is presently sold by the government-run Iolani Palace gift shop -->
 | location = Mill Creek, WA
 | isbn = 978-0-9623263-0-1
 | ref = {{sfnRef|Medcalf & Russell}}
}}
* {{cite book
  | last = Slabaugh
  | first = Arlie R.
  | edition = second
  | year = 1975
  | title = United States Commemorative Coinage
  | publisher = Whitman Publishing
  | location = Racine, WI
  | isbn = 978-0-307-09377-6
  | ref = {{sfnRef|Slabaugh}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | year = 2012
  | title = Encyclopedia of the Commemorative Coins of the United States
  | publisher = KWS Publishers
  | location = Chicago
  | isbn = 978-0-9817736-7-4
  | ref = {{sfnRef|Swiatek}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | last2 = Breen
  | first2 = Walter
  | authorlink2 = Walter Breen
  | year = 1981
  | title = The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-04765-4
  | ref = {{sfnRef|Swiatek & Breen}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1967
  | title = An Illustrated History of U.S. Commemorative Coinage
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-01536-3
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
|author=United States House of Representatives Committee on Coinage, Weights and Measures
|title=Coinage of 50-Cent Pieces in Commemoration of Sesquicentennial of Discovery of Hawaii
|date=January 23, 1928
|url=http://congressional.proquest.com/congressional/docview/t29.d30.hrg-1928-cwe-0002?accountid=14541
|publisher=[[United States Government Printing Office]]
|ref={{sfnRef|House hearings}}
}} {{subscription}}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, MA
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2015
  | title = [[A Guide Book of United States Coins]]
  | edition = 1st Deluxe
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-4307-6
  | ref = {{sfnRef|Yeoman}}
  }}

{{Coinage (United States)}}
{{Portal bar|Arts|Business and economics|Hawaii|Numismatics|United States|Visual arts}}

[[Category:1928 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:Fifty-cent coins]]
[[Category:History of Hawaii]]
[[Category:Monuments and memorials to James Cook]]
[[Category:United States silver coins]]