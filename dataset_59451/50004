{{For|the 2012 action role-playing game|Game of Thrones (2012 video game)}}
{{Infobox video game
| title = A Game of Thrones: Genesis
| image = [[File:A Game of Thrones - Genesis box.jpg|256px]]
| developer = [[Cyanide (company)|Cyanide]]
| publisher = [[Focus Home Interactive]]
| platforms = [[Microsoft Windows]]
| released = {{vgrelease|NA|September 28, 2011|EU|September 29, 2011|AUS|October 13, 2011}}
| genre = [[Strategy video game|Strategy]]
| modes = [[Single-player video game|Single-player]]
}}

'''''A Game of Thrones: Genesis''''' is a [[strategy video game]] developed by [[Cyanide (company)|Cyanide]] and published by [[Focus Home Interactive]] released exclusively for [[Microsoft Windows]]<ref>{{cite web |url=http://www.gamasutra.com/view/news/29482/Cyanide_Creating_A_Video_Game_Of_Thrones_RTS_RPG_Based_On_Fantasy_Series.php |title=Cyanide Creating A Video Game Of Thrones RTS, RPG Based On Fantasy Series |publisher=[[Gamasutra]] |date=2010-07-16 |accessdate=2011-02-15 |first=Chris |last=Remo}}</ref> on September 28, 2011 in North America, September 29, 2011 in Europe<ref>{{cite web |url=http://www.gamasutra.com/view/pressreleases/68512/The_Next_Big_Thing_Introduces_Liz_Allaire_with_New_Video_andScreens.php |title=The Next Big Thing Introduces Liz Allaire with New Video and Screens! |date=2011-02-09 |accessdate=2011-02-15 |publisher=Gamasutra}}</ref> and October 13, 2011 in Australia.

The game is an adaptation of the ''[[A Song of Ice and Fire]]'' book series by [[George R. R. Martin]] and is the first such video game adaptation. The game takes place over 1,000 years of the fictional history of Westeros, beginning with the arrival of the Rhoynar led by the warrior-queen Nymeria.<ref name="PCMag-July">{{cite news|last=Poeter|first=Damon|title=Game of Thrones the Video Game Hits This Summer|url=http://www.pcmag.com/article2/0,2817,2388482,00.asp|publisher=PC Magazine|accessdate=17 July 2011|date=2011-07-13}}</ref>

==Gameplay==
Gameplay focuses on capturing nodes—castles, towns and goldmines—with characters. Emphasis is placed on the [[rock-paper-scissors]] mechanics of "underhanded" characters rather than the brute force combat strength of traditional realtime strategy games.

The goal of the game is to win the [[Iron Throne (A Song of Ice and Fire)|Iron Throne]] and doing so can be done by amassing enough 'prestige' within the game.<ref name="PCMag-July" />

Each house has special units and abilities. House Stark has direwolves and House Baratheon has better archers for example.<ref name="PCMag-July" />

The game has two modes of play: Versus and Campaign. The game features four main facets: diplomacy, military, economic, and underhand.<ref name=PCGamer-May>{{cite web|last=McCormick|first=Rich|title=A Game of Thrones: Genesis preview|url=http://www.pcgamer.com/2011/05/22/a-game-of-thrones-genesis-preview/|publisher=PC Gamer|accessdate=17 July 2011}}</ref>

==Reception==
''A Game of Thrones: Genesis'' received mixed reviews. It received a score of 52.94% on [[GameRankings]]<ref>{{Cite web |url=http://www.gamerankings.com/pc/998868-a-game-of-thrones-genesis/index.html |title=A Game of Thrones: Genesis (PC) |publisher=[[GameRankings]] |accessdate=2012-10-24}}</ref> and 53/100 on [[Metacritic]].<ref>{{cite web|url=http://www.metacritic.com/game/pc/a-game-of-thrones-genesis|title=A Game of Thrones: Genesis Critic Reviews for PC|work=[[Metacritic]]|accessdate=2012-10-24}}</ref>

==References==
{{reflist}}

== External links ==
* [http://www.agot-genesis.com/ Official website]
* [http://www.focus-home.com/index.php?rub=games&gid=47 Publisher game page]
* [https://web.archive.org/web/20110302130549/http://www.cyanide-studio.com:80/games/agot/? Developer game page]
{{portal bar|video games|2010s}}
{{ASOIAF}}

{{DEFAULTSORT:Game Of Thrones}}
[[Category:2011 video games]]
[[Category:Articles created via the Article Wizard]]
[[Category:Fantasy video games]]
[[Category:Strategy video games]]
[[Category:Video games based on A Song of Ice and Fire]]
[[Category:Video games developed in France]]
[[Category:Windows games]]

{{Strategy-videogame-stub}}