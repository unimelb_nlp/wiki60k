{{Infobox journal
| title = Journal of Biomolecular Screening
| cover = [[File:Front cover image of Journal of Biomolecular Screening.tif]]
| editor = Robert M. Campbell
| discipline = [[Chemistry]]
| former_names = 
| abbreviation = J. Biomol. Screen.
| publisher = [[SAGE Publications]] on behalf of the [[Society for Laboratory Automation and Screening]]
| country =
| frequency = 10/year
| history = 1996-present
| openaccess = 
| license = 
| impact = 2.423 
| impact-year = 2014
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201648
| link1 = http://jbx.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jbx.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 34159284
| LCCN = 96656657
| CODEN = JBISF3
| ISSN = 1087-0571
| eISSN = 1552-454X
}}
'''''Journal of Biomolecular Screening''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by [[SAGE Publications]] on behalf of the [[Society for Laboratory Automation and Screening]]. It covers scientific and technical applications and advances in areas such as lab automation and [[robotics]], virtual screening, and high throughput screening.

== Abstracting and indexing ==
The ''Journal of Biomolecular Screening'' is abstracted and indexed in:
* [[Elsevier BIOBASE]]
* [[Biomolecular Interaction Network Database]]
* [[Biotechnology Citation Index]]
* [[Chemical Abstracts]]
* [[Current Contents]]/Life Sciences
* [[EMBASE]]
* [[EMBiology]]
* [[Index Medicus]]
* [[MEDLINE]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
According to the ''[[Journal Citation Reports]]'', the journal's 2014 [[impact factor]] is 2.423, ranking it 26 out of 74 journals in the category "Chemistry, Analytical",<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: chemistry, Analytical |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |series=[[Web of Science]] |postscript=.}}</ref> 66 out of 162 journals in the category "Biotechnology & Applied Microbiology",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Biotechnology & Applied Microbiology |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition= Sciences |accessdate= |series=Web of Science |postscript=.}}</ref> and 49 out of 78 journals in the category "Biochemical Research Methods".<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Biochemical Research Methods |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition= Sciences |accessdate= |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://jbx.sagepub.com/}}
* [http://www.slas.org/ Society for Laboratory Automation and Screening]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Biochemistry journals]]
[[Category:Publications established in 1996]]