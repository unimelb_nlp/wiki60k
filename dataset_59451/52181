'''Libertas Academica''' is an [[Open access (publishing)|open access]] [[academic journal]] [[Academic publishing|publisher]] specializing in the [[biological sciences]] and [[clinical medicine]]. It was acquired by [[SAGE Publications]] in September 2016.<ref>https://us.sagepub.com/en-us/nam/libertas-academica-journals</ref>

== Background ==
Libertas Academica, referred to as "LA", is a publisher of open access ("OA") scientific, technical and medical journals. It is privately funded and was founded specifically to publish OA journals. It was established in late 2004 with the launch of two journals, ''Evolutionary Bioinformatics'' and ''Cancer Informatics''. Additional journals have been published since. It was included on [[Beall's List|a list of "predatory" open access publishers]] in 2010<ref>{{cite journal |url= http://www.ingentaconnect.com/content/charleston/chadv/2010/00000011/00000004/art00005 |title="Predatory" Open-Access Scholarly Publishers |last=Beall |first= Jeffrey |journal=The Charleston Advisor |volume=11 |issue=4 |date=April 2010 |pages=10–17}}</ref> but later removed.{{when|date=February 2016}} {{Citation needed span|In 2005, a single ''Cancer Informatics'' Editorial Board member, whose paper was under review at the time, proposed an open 'buddy review' system in which authors would submit a paper pre-reviewed by peers selected by the authors.  A significant proportion of the editorial board of one of LA's journals, ''Cancer Informatics'', led by Founding Editor-in-Chief James Lyons-Weiler, PhD of the University of Pittsburgh, threatened to resign if the publisher changed its peer-review systems to the "buddy review" system, which they perceived as corrupt.  The publisher kept using its original industry standard review system, in place since the founding of the first two journals.  The publisher's peer review system was, and is based on the standard, 'industry-standard' blind review process.|date=January 2015}}  In 2013, a [[Who's Afraid of Peer Review?|sham study]] reporting that a compound isolated from lichen can kill cancer cells was submitted to one of the journals published by LA for peer review.  After LA used its objective peer review system, the sham study was correctly rejected for publication.<ref>http://www.npr.org/blogs/health/2013/10/03/228859954/some-online-journals-will-publish-fake-science-for-a-fee</ref>

== Journal indexing and archiving ==
As they become suitable indexing on [[DOAJ]], [[Pubmed]], and [[MEDLINE]] is sought for all journals, as is archiving in [[PubMed Central]]. Articles also appear on indexes and repositories, including [[OAIster]] and [[Pubget]]. The publisher offers an [[Open Archives Initiative Protocol for Metadata Harvesting]].

== Green and Gold OA ==
[[SHERPA/RoMEO]] has identified LA as a Green OA publisher.<ref name=RoMEO>{{cite web |url=http://www.sherpa.ac.uk/romeo/search.php?id=163&fIDnum=&#124; |title=SHERPA/RoMEO Search - Libertas Academica |format= |work= |accessdate=2009-12-14}}</ref> This means that authors are permitted to archive their work prior to and after publication. LA is also a gold OA publisher because all articles are freely available online immediately upon publication.

== Copyright ==
All articles, including meta-data and supplementary files, are published under the [[Creative Commons]] Attribution license (often referred to as the "CC-BY" license). This means that:
<blockquote>Licensees may copy, distribute, display and perform the work and make derivative works based on it only if they give the author or licensor the credits in the manner specified by these.</blockquote>

All journals fully indexed by [[DOAJ]] have been awarded the [[SPARC Europe]] Seal owing to the use of this copyright policy.<ref name=DOA>{{cite web |url=http://www.doaj.org/doaj?func=loadTempl&templ=080423 |title=Directory of open access journals: SPARC Europe and the Directory of Open Access Journals Announce the Launch of the SPARC Europe Seal for Open Access Journals. |format= |work= |accessdate=2009-12-14 |date=2008-04-23}}</ref>

== Subject home pages ==
Pages containing the most recent papers published in the entire set of journals are available for some subjects, including   [[bioinformatics]], [[biology]], [[biomarkers]], [[cancer]], [[chemistry]], [[pharmacology|drugs & therapeutics]], [[Genetics|genes]] & therapeutics, and [[medicine]].

==References==
{{Reflist}}

==External links==
* {{official website|http://www.la-press.com/}}
* [http://libertasacademica.blogspot.com/ Latest News Blog]

[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 2004]]
[[Category:Open access publishers]]