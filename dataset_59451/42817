<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Fokker D.XXIII
 | image=Fokker D.XXIII.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Single-seat [[Fighter aircraft|fighter]]
 | national origin=[[Netherlands]]
 | manufacturer=[[Fokker]]
 | designer=[[Marius Beeling]]
 | first flight=30 May 1939
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
 | developed into=
}}
|}
The '''Fokker D.XXIII''' was a [[Netherlands|Dutch]] single-seat [[Fighter aircraft|fighter]] designed and built by [[Fokker]]. Only one aircraft was flown before the country was invaded by the [[Germany|Germans]] in May 1940.<ref name="orbis" />

==Development==
The Fokker D.XXIII was designed as a twin-engined single-seat aircraft. To overcome the problems of asymmetric flight it had a tractor engine at the front and a pusher engine at the rear.<ref name="orbis" /> The D.XXIII was a cantilever [[monoplane]] with the twin tail units on booms.<ref name="orbis" /> The pilot had an enclosed cockpit in between the tractor and pusher engines and it had a retractable [[Tricycle gear|tricycle landing gear]].<ref name="orbis" />

The prototype first flew on 30 May 1939 powered by two [[Walter Sagitta|Walter Sagitta I-SR]] air cooled vee piston engines.<ref name="orbis" /> The trial flights identified problems with the cooling of the rear engine and general engine performance. It was proposed to use Rolls-Royce or Daimler-Benz engines in the production aircraft.<ref name="orbis" /> Concerns were also raised about the pilot clearing the rear propeller if he had to bail out and an [[Ejection seat|ejector seat]] was studied.<ref name="orbis" /> As a provisional solution, rails were put on both sides of the forward fuselage for the Fokker test pilot, Gerben Sonderman, to use to bail out in an emergency. The aircraft was flown 11 times for a total flight time of less than four hours. The rear fuselage paneling was modified significantly before the last few flights in an attempt to address chronic rear engine cooling problems. On the 11th flight in April, the undercarriage was damaged, and the programme was abandoned in May 1940 when the German forces invaded the Netherlands.<ref name="orbis" />

==Specifications==
[[File:Fokker DXXIII.svg|thumb|300px|Fokker DXXIII]]
{{Aircraft specs
|ref=<ref name="orbis" /><ref>{{cite book|last=Vredeling|first=Willem|title=Fokker D.23|publisher=Uitgeverij Geromy B.V.|location=AJ Maarssen, Netherlands|year=2012|isbn=978-9081893640}}</ref>
|prime units?=met

|crew=1
|capacity=
|length m=10.2
|length ft=
|length in=
|length note=
|span m=11.5
|span ft=
|span in=
|span note=
|height m=3.8
|height ft=
|height in=
|height note=
|wing area sqm=18.5
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=2180
|empty weight lb=
|empty weight note=equipped
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=2950
|max takeoff weight lb=
|max takeoff weight note=
|more general=

|eng1 number=2
|eng1 name=[[Walter Sagitta|Walter Sagitta I-SR]]
|eng1 type=air-cooled 12-cylinder Vee piston engine
|eng1 kw=
|eng1 hp=530
|eng1 note=
|power original=

|more power=

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

|max speed kmh=525
|max speed mph=
|max speed kts=
|max speed note=estimated
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=840
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=9000
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass met=
|power/mass imp=
|power/mass note=
|more performance=

|guns= two 7.9mm (0.31in) machine guns and two 13.2mm (0.52in) machine-guns (not fitted to prototype)
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{aircontent
|see also=
|related=
|similar aircraft=
* [[Dornier Do 335]]
* [[Lockheed P-38 Lightning]]
* [[Saab 21]]
|lists=
}}

==References==

===Citations===
{{reflist|refs=
<ref name="orbis">Orbis 1985, p. 1876</ref>
}}

===Bibliography===
*{{cite book|title=The [[Illustrated Encyclopedia of Aircraft]] (Part Work 1982-1985)|year=|publisher=Orbis Publishing|location=}}
*{{cite book|last=Vreedeling|first=Willem|title=Fokker D.23|year=|publisher=Geromy BV|location=|issn=}}

==External links==
*{{commonscat-inline|Fokker D.XXIII}}

{{Fokker aircraft}}

[[Category:Fokker aircraft|D 23]]
[[Category:Dutch fighter aircraft 1930–1939]]
[[Category:Twin-engined push-pull aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Twin-boom aircraft]]