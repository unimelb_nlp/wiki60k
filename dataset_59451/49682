{{EngvarB|date=April 2015}}
{{Use dmy dates|date=April 2015}}
{{Refimprove|date=September 2012}}
[[Image:Fishermanwitch.jpeg|thumb|right|200px|Illustration of the witch from ''The Fisherman and his Soul'']]
{{italic title}}
'''''A House of Pomegranates''''' is a collection of [[fairy tale]]s, written by [[Oscar Wilde]], that was published in 1891 as a second collection for ''[[The Happy Prince and Other Tales]]'' (1888).  Wilde once said that this collection was "intended neither for the British child nor the British public."

The stories included in this collection are as follows:
*The Young King
*The Birthday of the [[Infante|Infanta]]
*The Fisherman and his Soul
*The Star-Child

==Contents==

===The Young King===
Dedicated to [[Margaret Brooke|Margaret, Lady Brooke]] (the [[Raja|Ranee]] of [[Sarawak]])

''The Young King'' tells the story of the illegitimate shepherd son of the recently dead king's daughter of an unnamed country. Being his only heir, he is brought to the palace to await his accession. There, he is in awe of the splendor of his new home and anxiously awaits his new crown, scepter, and robe which are soon to be delivered to him for his coronation in the morning.

During the night, he has three [[nightmare]]s, one for each element of his raiment, showing him where they came from and how they were obtained. The first dream shows a group of starving peasants working at looms to weave his robe where they receive little payment or food despite being worked so hard. The second dream shows a group of slaves on a ship where one slave has his ears and nostrils filled with wax and sent underwater to find pearls for the scepter and dies after finding the best pearl. The third dream is the most elaborate and deals with the source of his new crown's rubies. In it, men excavate a dry riverbed in a tropical jungle, while overlooking them, the god Death tries to bargain with the goddess [[Avarice]] for a single grain of her corn. Each time Avarice refuses, Death calls [[:wikt:ague|Ague]], Fever and [[:wikt:plague|Plague]] to kill one third of her servants.

On the coronation day, the Young King refuses the costume brought to him, and makes a crown from a loop of briers, a scepter from a stick, and wears his shepherds tunic in place of a robe. The nobles rebuke him for bringing shame to their class, the peasants for trying to deprive them of work, and the bishops for foolishly trying to take the world's suffering upon himself. The story ends with his approaching the altar of the cathedral alone, and his stick-scepter blossoming with white lilies, his brier-crown with red roses, and his robe coloured by the light streaming through the stained-glass window; the bishop says that a much higher being (God) has officially crowned the young king.

===The Birthday of the Infanta===

Dedicated to Mrs. William H Grenfell of [[Taplow Court]] ([[William Grenfell, 1st Baron Desborough|Lady Desborough]])

''The Birthday of the Infanta'' is about a hunchbacked dwarf, found in the woods by courtiers of the [[Spanish monarchy|King of Spain]]. The hunchback's father sells him to the palace for the amusement of the king's daughter, the Infanta, on her twelfth birthday.

Her birthday is the only time she is allowed to mingle with other children, and she much enjoys the many festivities arranged to mark it, especially the Dwarf's performance. He dances, as he did in the woods, thoroughly unaware of his audience's laughing at him. She insists on his performing a second time for her after dinner.

The Dwarf mistakenly believes that the Infanta must love him, and tries to find her, passing through a garden where the flowers, sundial, and fish ridicule him, but birds and lizards do not. He finds his way inside the palace, and searches through rooms hoping to find the Infanta, but finding them all devoid of life.

Eventually, he stumbles upon a grotesque monster that mimicks his every move in one of the rooms. When the realisation comes that it was his own reflection, he knows then that the Infanta did not love him, but was laughing out of mockery, and he falls to the floor, kicking and screaming. The Infanta and the other children chance upon him and, imagining it to be another act, laugh and applaud while his flailing grows more and more weak before he stops moving altogether. When the Infanta demands more entertainment, a servant tries to rouse him, only to discover that he has died of a broken heart. Telling this to the Infanta, she speaks the last line of the story "For the future, let those who come to play with me have no hearts."

====Adaptations====
* ''Az infánsnő születésnapja'' (1913/1916), ballet by [[Miklós Radnai]].
* ''[[Der Zwerg]]'' (1921), opera by [[Alexander von Zemlinsky]].
* ''Poisoned Present for Princess'' (2011) (children's musical by Cynthia Medley and Marian Wood Chaplin).

===The Fisherman and his Soul===
Dedicated to [[Alice Heine|H.S.H. Alice]], the [[Princess of Monaco]]

In ''The Fisherman and his Soul'', a young Fisherman finds a [[Mermaid]] and wants nothing more than to marry her, but he cannot, for one cannot live underwater if one has a soul. He goes to his priest, but the priest tells him his soul is his most precious possession, and the soulless mermen are lost. He tries to sell it to merchants, who tell him it is not worth anything. He goes to a witch, who tells him his soul is his shadow, and says how it can be cut away with a viper-skin knife after he dances with her.

After cutting his shadow and soul free from his body, his Soul tells him that the world is cruel and asks to take with him his heart to allay his fears. The Fisherman, however, refuses to give his Soul his heart, because his love needs it, and he sends the Soul away and joins his Mermaid under the sea.

Each year that passed, the Soul comes to the Fisherman to tell him what he has done in his absence. Each year, he travels in a different direction and meets different people from distant cultures, and each time, he comes into the possession of a magical object, but the Fisherman values love greater than everything the Soul tried to tempt him with. He first talks of the Mirror of Wisdom, which is worshipped as a 'God' in the East, then the Ring of Riches from an Emperor who was willing to give his whole treasury to the Soul rather than this after the Soul survived all his attacks.

The third year, the Soul tells the Fisherman about a nearby city where a woman dances barefooted. Deciding that, since it is so near and he could easily come back to his legless Mermaid, he agreed to go with the Soul to see her dance. Rising up from the water, he and his Soul are reunited. Passing through cities on the way, the Soul tells the Fisherman to do things: in the first, he tells him to steal a silver cup; in the second, to beat a child; in the third to kill and rob the man in whose house they were guests. The Fisherman confronted his Soul, who reminds him that he had not given him a heart. The Fisherman tries to cut away his Soul again, but discovers that, once reunited, they could never again be parted.

Returning to the shore, the Fisherman built a shelter near the water and calls the Mermaid daily, but she never came. After years pass, the lifeless body of the Mermaid washes ashore, and the Fisherman held it while the violent waves enveloped him.

The Priest, finding the drowned Fisherman cradling the dead Mermaid, pronounces them accursed and has them buried in an unmarked grave in the corner of a field, and refuses to bless the water as was his intent to do. Three years later, the Priest goes to the flower-covered altar, prepared to give a sermon on God's vengeful wrath, but, for reasons he cannot explain, he cannot do so and instead spoke of God's love. Asking the deacons where the flowers came from, they tell him they came from the corner of the field. The next day, the Priest blesses the water, but the flowers never grew again and the mermen move to a different bay.

===The Star-Child===
Dedicated to Miss Margot Tennant ([[Margot Asquith, Countess of Oxford and Asquith|Mrs. Asquith]])

''The Star-Child'' is the story of an infant boy found abandoned in the woods by a poor woodcutter, who pities him and takes him in. He grows up to be exceedingly beautiful, but vain, cruel, and arrogant, believing himself to be the divine child of the stars. He lords himself over the other children, who follow him devotedly, and takes pleasure in torturing the forest animals and town beggars alike.

One day, a beggar, haggard and with bleeding feet, comes to town in search of her lost son, who the Star-Child is revealed to be. However, he rejects her and sends her away, and in doing so, is transformed into a loathsome cross between a toad and a snake as a punishment. His followers abandon him, and he sets off to seek forgiveness from his mother. He also repents his cruelty and asks forgiveness from the animals he tortured.

At length, he comes to a city, where he is captured and sold into [[slavery]]. His master treats him cruelly. On his first task, he sends him to find a piece of white gold hidden in the forest. The Star-Child searches all day, but cannot find it. On returning to the city, he sees a rabbit caught in a trap and stops to free him. In gratitude, the rabbit shows him where the gold is and the Star-Child gets it. However, returning with the gold, a beggar calls to him that he will surely starve unless he can give him money for food. The Star-Child gives him the gold, and his master beats him and gives him neither food nor water that night.

For the second task, he is told to go find a piece of yellow gold hidden in the forest. Again, the rabbit shows him where it is, and again, the beggar meets him at the gate, and again, the Star-Child gives him the gold. His master beats him and chains him up.

For the final task, his master tells him that unless he finds the hidden piece of red gold, he will kill him. The rabbit shows him where the gold is hidden, and he returns to the city with it. Along the way, he again meets the beggar and gives him the gold, deciding it means more to him than it does to himself.

Upon entering the city, everyone awaits him to crown him the new king, and he discovers the city's present rulers to be his mother, the beggar woman, and his father, the beggar he had given the gold to. At that point also, he is transformed to his former beautiful self. At the story's end, we are told of his kind, loving, and charitable reign, but that it only lasted for three years, and the king that followed him was cruel and evil.

====Adaptations====
Two films based on "The Star-Child" were produced in the Soviet Union:
*[[:ru:Звёздный мальчик (фильм)|Звёздный мальчик]] (1958)
*[[:ru:Сказка о Звёздном мальчике|Сказка о Звёздном мальчике]] (1983) by [[Leonid Nechayev]]<ref>[http://www.imdb.com/title/tt0213249 Skazka o zvezdnom malchike] at the [[Internet Movie Database]]</ref>

==See also==
{{Portal|Oscar Wilde}}
* [[Music based on the works of Oscar Wilde]]

==Notes==
<!--This article uses the Cite.php citation mechanism. If you would like more information on how to add references to this article, please see http://meta.wikimedia.org/wiki/Cite/Cite.php --> {{Reflist}}

==External links==
{{wikisource|House of Pomegranates}}
*[https://archive.org/search.php?query=title%3AA%20House%20of%20Pomegranates%20AND%20mediatype%3Atexts ''A House of Pomegranates''] at [[Internet Archive]] (scanned books colour illustrated but not with the original version's Ricketts-Shannon illustrations)
*[http://www.gutenberg.org/etext/873 ''A House of Pomegranates''] at [[Project Gutenberg]] (plain text and HTML)
*[http://wilde.artpassions.net ''A House of Pomegranates''] with illustrations by [[Jessie M. King]] (HTML)
*[http://librivox.org/a-house-of-pomegranates/ ''A House of Pomegranates''] Librivox audio recording
*[http://www.lazybeescripts.co.uk/Scripts/Results.aspx?iSc=1192 "Poisoned Present for Princess"] (adaptation of The Birthday of the Infanta) on the Lazy Bee Scripts web site

{{Oscar Wilde|state=collapsed}}

{{DEFAULTSORT:House Of Pomegranates}}
[[Category:1891 books]]
[[Category:Fantasy short story collections]]
[[Category:Collections of fairy tales]]
[[Category:Works by Oscar Wilde]]