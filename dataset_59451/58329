{{Infobox journal
| title         = Journal of Money, Credit and Banking
| cover         = [[File:Journal of Money, Credit and Banking.jpg]]
| editor        = Robert DeYoung, Paul Evans, Pok-sang Lam, [[Kenneth D. West]]
| discipline    = monetary and financial economics
| peer-reviewed = 
| formernames   = 
| abbreviation  = 
| publisher     = [[Wiley-Blackwell]]
| country       = [[United States]]
| frequency     = 8 per year
| history       = 1969-present
| openaccess    = 
| license       = 
| impact        = 1.036
| impact-year   = 2014
| website       = https://jmcb.osu.edu/
| link1         = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1538-4616
| link1-name    = Online access
| link2         = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291538-4616/issues
| link2-name    = Online archive
| RSS           = 
| atom          = 
| JSTOR         = 00222879
| OCLC          = 1783384
| LCCN          = 70012309
| CODEN         = JMCBBT
| ISSN          = 0022-2879
| eISSN         = 1538-4616
| boxwidth      = 
}}
The '''''Journal of Money, Credit and Banking''''' is a [[Peer review|peer-reviewed]] [[economics]] [[Academic journal|journal]] covering [[Monetary economics|monetary]] and [[Finance|financial]] issues in [[macroeconomics]]. It is published by [[Wiley-Blackwell]] on behalf of the [[Ohio State University]] Department of Economics.

==Replicability==
In 2004, the ''[[American Economic Review]]'' instituted a mandatory archive for the submission of data and code used in economic journal submissions to ensure the replicability and legitimacy of research. An analysis of the ''Journal of Money, Credit and Banking'''s archive from 1996-2003 found that only 14 of 186 empirical articles could be replicated. Some economists have published suggestions regarding procedures to ensure the replicability of journal articles. As a result, the journal's editors amended their procedures beginning with the December 2006 issue. The amendments did not render the desired outcomes. In the December 2006 issue, only 2 of 9 empirical articles had data and code in the archive, neither of which could reproduce the published results.<ref>[http://econjwatch.org/articles/got-replicability-the-journal-of-money-credit-and-banking-archive McCullough, B.D. "Got Replicability? The ''Journal of Money, Credit and Banking'' Archive",] [http://econjwatch.org/issues/volume-4-issue-3-september-2007 ''Econ Journal Watch'', ''4''(3), September 2007.]</ref>

==See also==
*[[List of scholarly journals in economics]]

==References==
{{reflist}}

==External links==
*{{Official website|1=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1538-4616}}
*[http://www.jstor.org/journal/jmonecredbank The ''Journal of Money, Credit and Banking''] at [[JSTOR]]

{{Ohio State University}}

[[Category:Economics journals]]
[[Category:Finance journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1969]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Academic journals associated with universities and colleges of the United States]]
[[Category:Ohio State University]]