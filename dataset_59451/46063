{{Orphan|date=January 2012}}

{{Infobox person
| name        = Hilal Al-Ahmadi
| image       = 
| caption     = 
| birth_name  = Hilal Al-Ahmadi
| birth_date  = 1954? <!-- {{Birth date and age|YYYY|MM|DD}} -->
| birth_place = 
| death_date  = February 17, 2011
| death_place = Mosul, Iraq
| education   = 
| occupation  =  Newspaper Journalist
| alias       = 
| title       = 
| family      = 
| spouse      = Married
| domestic_partner = 
| children    = 4 children
| relatives   = 
| ethnicity   = 
| nationality = Iraqi
| religion    = 
| years_active= <!-- YYYY – present -->
| credits     = 
| agent       = 
| website     = 
}}

'''Hilal Al-Ahmadi''' (1954?  &ndash; 17 February 2011), who was an Iraqi veteran journalist with over 30 years of experience and well known for his reports about corruption, was assassinated in front of his home east of [[Mosul]], [[Iraq]].<ref name="CNN1">CNN. "Report: Gunmen kill Iraqi journalist." February 18, 2011. Retrieved 20 November 2011 from Lexis-Nexis database.</ref><ref name="CNN2">CNN. "At least two dead in Iraq mosque attack." February 20, 2011. Retrieved 20 November 2011 from Lexis-Nexis database.</ref><ref name="CPJ">Committee to Protect Journalists. 2011. “Journalist killed in Iraq by unidentified gunmen” Retrieved September 19, 2011 [http://www.cpj.org/2011/02/journalist-killed-in-iraq-by-unidentified-gunmen.php].</ref><ref name="RSF">Reporters Without Borders. "Journalist Gunned Down Outside His Home in Mosul." February 17, 2011. Retrieved September 19, 2011 [http://en.rsf.org/iraq-journalist-gunned-down-outside-his-17-02-2011,39576.html].</ref><ref name="IFJ">International Federation of Journalists. 2011. “IFJ Condemns Murder of Iraqi Journalist" Retrieved September 18, 2011 [http://mena.ifj.org/en/articles/ifj-condemns-murder-of-iraq-journalist]</ref>

His death preceded by days the intimidation of the Iraqi press that occurred during the [[Arab Spring]]- inspired Iraqi protests.<ref name="Dreazen">Dreazen, Yochi J. "Post-Hussein Iraq Stops the Press." The National Journal. Retrieved 20 November from Lexis-Nexis database.</ref><ref name="blackestdays">Reporters Without Borders. "Action call after 'black day' for media freedom." 1 March 2011. Retrieved 9 November 2011 [http://en.rsf.org/iraq-action-call-after-black-day-for-01-03-2011,39646.html RSF]</ref>

The response by major IGO and NGOs to Al-Ahmadi's killing was to denounce the "culture of impunity" in Iraq which leads to further violence against journalists.

==Career==
According to CNN, Al-Ahmadi had over 30 years of experience as a journalist.<ref name="CNN1" /><ref name="CNN2" /> He wrote freelance for the ''Mosul Echo'' and the ''Iraqiyoun'' in Mosul, Iraq.<ref name="RSF" /> He also wrote for ''al Hadba''', ''al Isalah al Jadid'', and ''al Sawt al Akhar''<ref name="IFJ" /> He worked as a spokesperson for the communication services of the [[Ninawa Governorate]] but was let go shortly before his assassination.<ref name="RSF" />

Hilal Al-Ahmadi often addressed financial and administrative corruption or the government's inability to supply services. In fact, Al-Ahmadi was well known for these kinds of reports.<ref>States News Service. "Killing of Iraqi Journalist Must Not Go Unpunished, Says UNESCO Chief." April 4, 2011. Retrieved November 20, 2011 from Lexis-Nexis database.</ref>

==Background==
Al-Ahmadi was shot dead shortly before the Journalistic Freedoms Observatory's office was ransacked on 24 February by Iraqi soldiers and before the "Day of Rage" protests in Iraq.<ref name="Dreazen" /> The nationwide protests in Iraq on 25 February were inspired by the Arab Spring uprisings in [[Tunisia]] and [[Egypt]]. Protests also took place in major Iraqi cities Mosul, where Al-Ahmadi was attacked. The protests centered on corruption and lack of services that Al-Ahmadi had written about.<ref>al-Jazeera. "Tensions flare in Iraq rallies." Feb 25,2011. Retrieved 20 November 2011 [http://www.aljazeera.com/news/middleeast/2011/02/2011224192028229471.html al-Jazeera]</ref> Journalistic Freedoms Observatory estimated that 160 journalists had been detained after Al-Ahmadi's death and around the protests.<ref name="Dreazen" /> For example, [[Hadi al-Mahdi]] was among the journalists who were either arrested or detained during those days.<ref name="blackestdays" /> [[Reporters Without Borders]] called this time “one of the blackest days for media freedom” in Iraq.<ref name="blackestdays" />

==Death==
Hilal Al-Ahmadi, 57, was leaving his home for work 17 February 2011, when a car drove up to him. The car carried two men with automatic weapons who then shot the journalist. Al-Ahmadi was shot multiple times and was found dead in his suburb of [[Mithaq]], located in Eastern Mosul, Iraq. Al-Ahmadi's body was taken to Mosul's department of forensic medicine. As with all the other cases of killed journalists in Iraq since 2003, the perpetrators have not been brought to justice.<ref name="RSF" /><ref name="IFJ" />

==International Reaction to Al-Ahmadi's Death==
The overall reaction to Hilal Al-Ahmadi's death by organizations who defend the rights of journalists worldwide was frustration over Iraq's high impunity for such attacks.

The [[Committee to Protect Journalists]] said in a CNN report and in reaction to Al-Ahmadi's death, "Not a single journalist murder since 2003 has been seriously investigated by authorities, and not a single perpetrator has been brought to justice."<ref name="CNN1" />

According to the Committee to Protect Journalists, Iraq was ranked #1 on the 2010 Impunity Index. The [[impunity]] index focuses on journalists who are killed on a recurring basis and the crimes often go unpunished. Iraq has been at the #1 spot for the last 3 years.<ref>Committee to Protect Journalists. "Getting Away With Murder." April 20, 2010. Retrieved 20 November 2011 [http://www.cpj.org/reports/2010/04/cpj-2010-impunity-index-getting-away-with-murder.php CPJ]</ref>

Jean-François Julliard, who is secretary-general of Reporters Without Borders, said in an Agence France Press report about Al-Hamadi's death: "The murders of journalists in Iraq are not letting up and generalized impunity is fuelling the cycle of violence. Very few of 232 murders of in Iraq (since 2003) have been thoroughly and conclusively investigated. Impunity must not prevail in Iraq."<ref name="AFP">Agence France Press. "Iraqi journalist murdered outside home: police." February 17, 2011. Retrieved 20 November 2011 in Lexis-Nexis database.</ref><ref>BNO News. "UNESCO condemns murder of Iraqi journalist who exposed corruption." Mar. 10, 2011. Retrieved 20 November 2011 from Lexis-Nexis database.</ref>

Mohamed Abdel Dayem, CPJ's Middle East and North Africa program coordinator, said: "We call on the Iraqi authorities to vigorously investigate the killing of Hilal al-Ahmadi and bring those behind it to justice. It is time for the government to take the long-delayed initial steps toward ending a years-long record of impunity for journalist murders in Iraq."<ref>Committee to Protect Journalists. "Journalist killed in Iraq by unidentified gunmen." February 17, 2011. Retrieved 20 November 2011. [http://www.cpj.org/2011/02/journalist-killed-in-iraq-by-unidentified-gunmen.php CPJ]</ref>

Aidan White, who is the general secretary of the [[International Federation of Journalists]], said, "The lack of credible action on violence against media sends a dangerous sign to media predators," added White. "It encourages further attacks and the Iraqi authorities need to end this culture of impunity."<ref name="IFJ" />

[[Irina Bokova]], who is the director-general of [[UNESCO]], said: "I condemn the murder of Iraqi freelance journalist Hilal al-Ahmadi. Attacking journalists cannot be tolerated. By denying citizens access to information, these crimes tear at the very fabric of emerging democracies. They must be investigated and punished."<ref>United Nations Educational, Scientific, and Cultural Organization. Publication Year. “Director-General condemns killing of Iraqi journalist Hilal al-Ahmadi” UNESCO Press. March 10, 2011. Retrieved 20 November 2011 [http://portal.unesco.org/ci/en/ev.php-URL_ID=31285&URL_DO=DO_TOPIC&URL_SECTION=201.html UNESCO].</ref>

==Personal==
Hilal al-Ahmadi was married and had four children.<ref name="RSF" />

== References ==

{{Reflist}}

{{DEFAULTSORT:Ahmadi, Hilal}}
[[Category:Articles created via the Article Wizard]]
[[Category:1950s births]]
[[Category:2011 deaths]]
[[Category:Assassinated Iraqi journalists]]