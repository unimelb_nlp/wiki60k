<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=3V-1 Eolo 
 | image=Bruni Eolo.png
 | caption=
}}{{Infobox Aircraft Type
 | type=High performance [[glider (sailplane)|glider]]
 | national origin=[[Italy]]
 | manufacturer=[[SIAI-Marchetti]]
 | designer=Giovanni Bruni
 | first flight=August 1955
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Bruni 3V-1 Eolo''' was a single seat [[Italy|Italian]] competition [[glider (sailplane)|glider]], first flown in 1955.  It took part World Gliding Championships of 1956 but retired early after sustaining damage.

==Design and development==

The 3V-1 Eolo, designed by Giovanni Bruni and built by a collaboration between [[SIAI-Marchetti]] and the Alessandro Passeleva Soaring Sports Club of [[Vergiate]], so it is sometimes known as the '''SIAI-Marchetti Eolo 3V-1'''. It was a high performance sailplane intended for competitions and record breaking. It was an all wooden [[cantilever]] [[mid-wing]] aircraft, with a high [[aspect ratio (wing)|aspect ratio]], single [[spar (aviation)|spar]] wing which was strongly straight tapered in plan and mounted with 3° of [[dihedral (aircraft)|dihedral]].<ref name=JAWA60/><ref name=IVS/> The wing was covered with unusually thick [[plywood]] to maintain the [[laminar flow|laminar]] profile<ref name=S&G/> and its tips carried "salmons", small streamlined bodies intended to minimise [[induced drag]]. Narrow control surfaces filled the whole [[trailing edge]], each occupying about a third of the span; the outermost were conventional [[aileron]]s, followed by a second set of ailerons which drooped when the [[flap (aircraft)|flap]]s on the inboard third of the wing were lowered.<ref name=JAWA60/><ref name=IVS/> The flaps and ailerons were all [[Flap (aeronautics)#Types|slotted]]. The Eolo had a pair of mid-[[chord (aircraft)|chord]] [[air brake (aircraft)|airbrakes]] mounted just behind the wing spar, each with sixteen blades deployed above and below the wing surfaces.<ref name=JAWA60/> In the initial version there were inboard [[leading edge]] tanks that could hold {{convert|32|kg|lb|abbr=on|0}} of [[gliding#Maximising average speed|water ballast]].<ref name=JAWA60/>

The Eolo's [[fuselage]] was a wood framed, ply skinned semi-[[monocoque]] of elliptical cross-section.  Behind a pointed nose the pilot sat under a long, one piece [[aircraft canopy|canopy]] which extended rearwards almost to the wing leading edge where it was smoothly blended into the fuselage. It had a [[V-tail]] with surfaces at 110°; these were straight tapered and ended in salmons, like the wings.<ref name=JAWA60/>  The [[elevator (aircraft)|elevator]]s were equipped with [[trim tab]]s.<ref name=OSTIV1/>  The glider had a [[Landing gear#Gliders|monowheel undercarriage]], equipped with a brake, which retracted behind two doors.  There was also a sprung tailskid and a small protective nose skid.<ref name=JAWA60/>

The Eolo first flew in August 1955.<ref name=JAWA60/>  A year later it competed in the World Gliding Championships held at [[Saint-Yan]] but had to retire after an accident on the third day.<ref name=Flight/>

==Specifications==

{{Aircraft specs
|ref=Wilkinson (1958)<ref name=OSTIV1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|capacity=One
|length m=8.55
|length note=
|span m=20.00
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=16.00
|aspect ratio=25
|airfoil=[[NACA airfoil|NACA 65<sub>3</sub>618]] root, NACA 65<sub>1</sub>612 tip
|empty weight kg=345
|empty weight note=with instruments
|gross weight kg=
|gross weight note=
|max takeoff weight kg=450
|max takeoff weight note=
|more general=

<!--
        Performance
-->
|perfhide=

|max speed kmh=230
|max speed note=placard
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=40
|stall speed note=with full flaps (50°)
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=32.8:1 at {{convert|96.5|km/h|mph|abbr=on|1}}
|sink rate ms=0.79
|sink rate note=at {{convert|92|km/h|mph|abbr=on|0}}
|lift to drag=
|wing loading kg/m2=28.1
|wing loading note=
|more performance=
}}
<!-- 
==See also== 
-->{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}<!--
==Notes==
-->

==References==
{{reflist|refs=

<ref name=IVS>{{cite book |title=Italian Vintage Sailplanes|last=Pedrielli |first=Vincenzo |last2=Camastra|first2=Francesco |year=2011|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn= 9783980883894|pages=224–5}}</ref>

<ref name=JAWA60>{{cite book |title= Jane's All the World's Aircraft 1960-61|last= Taylor |first= John W R |coauthors= |edition= |year=1960|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|page=191 }}</ref>

<ref name=OSTIV1>{{cite book |title= The World's Sailplanes - Die Segelflugzeuge der Welt  - Les Planeurs de le Monde|volume=I|first= K.G.|last=Wilkinson|first2=Peter|last2= Brooks|first3=B.S.|last3=Shenstone|year=1958|publisher=Organisation Scientifique et Technique International de Vol à Voile (OSTIV) & Schweizer Aero-Revue|language=English, German, French|isbn=|pages=146–9}}</ref>

<ref name=Flight>{{cite magazine |last= |first= |authorlink= |coauthors= |date=20 July 1956  |title= World Gliding|magazine= [[Flight International|Flight]]|volume=70 |issue=2478 |pages=122–3 |id= |url= http://www.flightglobal.com/pdfarchive/view/1956/1956%20-%200977.html |accessdate= |quote= }}</ref>

<ref name=S&G>{{cite journal |last= |first= |authorlink= |coauthors= |date=August 1956  |title=Eolo 3 V1|journal= Sailplane and Gliding|volume=VII |issue=4 |page=194|url= }}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:Italian sailplanes 1950–1959]]