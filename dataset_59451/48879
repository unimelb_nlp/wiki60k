{{Infobox non-profit
| name              = Jewish Partisan Educational Foundation
| image             = 
| type              = Non-profit organization
| founded_date      = 2000
| founder           = Mitch Braff
| board_of_directors   = Board President: Elliott Felson
| location          = San Francisco, CA
| area_served       = Worldwide
| focus             = Educational (Holocaust Resistance)
| homepage          = {{URL|jewishpartisans.org }}
}}
The '''Jewish Partisan Educational Foundation (JPEF)''' is a nonprofit organization based in [[San Francisco, California]], that produces short films and other educational materials on the history and life lessons of the [[Jewish partisans]].<ref name=":0">{{Cite news|url=https://billionbooksbaby.org/view.php?res=http://www.jewishpartisans.org/pdfs/NtnlJewishNews_Article.pdf&keyword=Over+1%2C000+NJ+Public+Schools+Receive+New+Holocaust+Curriculum|title=Over 1,000 NJ Public Schools Receive New Holocaust Curriculum|last=|first=|date=August 22, 2005|work=|publisher=National Jewish News|access-date=|via=}}</ref><ref>{{Cite web|url=http://www.redorbit.com/news/entertainment/2094392/larry_king_liev_schreiber_and_ed_zwick_join_jewish_partisan/#8vVAukQ3I7jsUA3A.99|title=Larry King, Liev Schreiber and Ed Zwick Join Jewish Partisan Educational Foundation to Honor America’s Last Surviving Partisans with Launch of New PSA Campaign|last=|first=|date=2011|website=Red Orbit|publisher=|access-date=}}</ref> During [[World War II]], 20,000 to 30,000 Jewish men and women fought back against the Germans and their collaborators as [[Partisan (military)|partisans]] (armed resistance fighters behind enemy lines).<ref>{{Cite web|url=https://www.ushmm.org/outreach/en/article.php?ModuleId=10007743|title=Jewish Partisans|last=|first=|date=|website=United States Holocaust Memorial Museum|publisher=|access-date=}}</ref><ref>{{Cite web|url=https://www.ushmm.org/wlc/en/article.php?ModuleId=10007222|title=Partisan Groups in the Parczew Forests|last=|first=|date=|website=United States Holocaust Memorial Museum|publisher=|access-date=}}</ref>

JPEF provides free educational resources for public, Jewish and other parochial and private schools, including short documentary films, lesson plans, study guides, and online teacher-training. The materials are primarily based on [[oral history]] interviews, conducted between 2001 and 2015, with approximately 50 Jewish partisans.<ref>{{Cite web|url=http://www.jewishpartisans.org/partisans|title=JPEF Partisans [index]|last=|first=|date=|website=|publisher=Jewish Partisan Educational Foundation website|access-date=}}</ref>

== History ==
Filmmaker Mitch Braff founded JPEF in 2000 after meeting former Jewish partisan Murray Gordon - the first time Braff had heard about organized, armed Jewish resistance during the Holocaust. Discovering that this piece of history was largely unknown in the U.S., even among American Jews, Braff founded JPEF to interview many more partisans, archive their testimonies, and create films, curricula and a website dedicated to their history.<ref>{{Cite web|url=http://jewishpartisans.org/pdfs/Jewish_Standard_screen.pdf|title=Notes From the Polish Underground: Fort Lee Woman Recalls Her Partisan Past|last=Palmer|first=Joanne|date=2002|website=|publisher=New Jersey Jewish Standard|access-date=}}</ref><ref>{{Cite web|url=http://www.sfgate.com/bayarea/article/Hidden-heroes-Filmmaker-puts-spotlight-on-2868387.php|title=Hidden heroes / Filmmaker puts spotlight on Jewish partisans who fought Nazis|website=SFGate|access-date=2016-06-03}}</ref>  Founding Board President, Paul Orbuch, the son of Jewish partisan [[Sonia Orbuch]], helped Braff launch the organization.<ref>{{Cite web|url=http://forallevents.info/reviews/two-local-women-who-resisted-the-nazis-cant-forget/|title=Two Local Women Who Resisted the Nazis Can't Forget|last=Weingarten|first=Woody|date=2013|website=For All Events|publisher=|access-date=}}</ref>

In 2003 JPEF produced its first film, "Introduction to the Jewish Partisans", narrated by actor [[Ed Asner]] (cousin of Jewish partisan Abe Asner).<ref name=":2" /><ref>{{Cite web|url=http://www.holocaustcenter.org/page.aspx?pid=466|title=<nowiki>Asner, Abe [interviewer summary]</nowiki>|last=Weisburg|first=Sherri|date=1982|website=Holocaust Memorial Center, Zekelman Family Campus|publisher=|access-date=}}</ref>

In 2008 JPEF developed "Pictures of Resistance", a traveling exhibition, of photographs taken by the only known Jewish partisan photographer, Faye Schulman.<ref>{{Cite web|url=http://www.jweekly.com/article/full/37486/rare-photos-show-hidden-life-of-partisans-who-fought-nazis|title=Rare Photos Show Hidden Life of Partisans Who Fought Nazis|last=Palevski|first=Stacey|date=2009|website=|publisher="j." the Jewish News Weekly of Northern California|access-date=}}</ref>  That same year, JPEF consulted for director [[Edward Zwick]] on the production of the motion picture [[Defiance (2008 film)|Defiance]], which portrays the story of the [[Bielski partisans]], starring [[Daniel Craig]] and [[Liev Schreiber]] star as Jewish partisan commanders [[Tuvia Bielski|Tuvia]] and [[Zus Bielski|Zus]] Bielski.<ref>{{Cite web|url=http://www.jweekly.com/article/full/36511/s-f-nonprofit-plays-role-in-movie/|title=S.F. nonprofit plays role in movie  {{!}}  j. the Jewish news weekly of Northern California|last=palevsky|first=stacey|website=www.jweekly.com|access-date=2016-06-03}}</ref><ref name=":1">{{Cite web|url=http://forward.com/culture/14992/hollywood-drama-about-jewish-resistance-hits-the-c-03206/|title=Hollywood Drama About Jewish Resistance Hits the Classroom|website=The Forward|access-date=2016-06-03|ref=forward-defiance-article-1-21-2009}}</ref>

According to JPEF, as of 2013 their educational materials, films and teacher trainings had reached over 8,000 educators and more than one million students worldwide.<ref>{{Cite news|url=|title=JPEF 2013 Annual Report|last=|first=|date=2014|work=|publisher=Jewish Partisan Educational Foundation|access-date=|via=}}</ref>

== Program Offerings ==
'''Website'''
 
Online resources include maps, archival photographs, and online profiles with video testimonies for over 50 Jewish partisans including [[Tuvia Bielski]], [[Frank Blaichman]], [[Vitka Kempner]], [[Abba Kovner]], [[Faye Schulman]], and [[Shalom Yoran]].<ref>{{Cite web|url=http://www.socialstudiescentral.com/2014/02/07/jewish-partisan-educational-foundation-and-very-handy-instructional-resources/|title=Jewish Partisan Educational Foundation and very handy instructional resources|date=2014-02-07|website=Social Studies Central|access-date=2016-06-03}}</ref>

'''Curricula'''

Lesson plans and study guides use the experiences of the Jewish partisans to teach history, leadership, ethics, women's studies, and Jewish values. The curricula, for 6th-12th grade and college, are edited by Holocaust scholar Dr. [[Michael Berenbaum]]. A number of organizations incorporate JPEF’s materials into their programs, including the [[United States Holocaust Memorial Museum]], [[Facing History and Ourselves]], the [[Association of Holocaust Organizations]], and the [[New Jersey Commission on Holocaust Education]].<ref name=":1" /><ref>{{Cite web|url=https://www.facinghistory.org/resource-library/resistance-during-holocaust|title=Resistance During the Holocaust: An Exploration of the Jewish Partisans|website=Facing History and Ourselves|access-date=2016-06-03}}</ref><ref>{{Cite web|url=http://www.ahoinfo.org/membersdirectory.html|title=AHO, Members Directory|website=www.ahoinfo.org|access-date=2016-06-03}}</ref> JPEF is also a contributor/publishing partner to the [[Encyclopædia Britannica]]'s Holocaust history project.<ref>{{Cite web|url=http://corporate.britannica.com/the-holocaust-project/|title=The Holocaust Project {{!}} Encyclopædia Britannica, Inc. Corporate Site|website=corporate.britannica.com|access-date=2016-06-03}}</ref><ref>{{Cite web|url=http://www.britannica.com/topic/Jewish-partisan|title=Jewish partisan {{!}} World War II|website=Encyclopedia Britannica|access-date=2016-06-03}}</ref>

'''Teacher Training'''

Online professional development courses on Jewish armed resistance during the Holocaust are offered on JPEF’s website, with continuing education units awarded through [[Touro College]].<ref>{{Cite book|url=https://books.google.com/books?id=qazDCwAAQBAJ&pg=PT156&dq=%22Jewish+Partisan+Educational+Foundation%22&hl=en&sa=X&ved=0ahUKEwiosq_J9_XMAhVX-2MKHbc9BsYQ6AEIRjAE#v=onepage&q=%22Jewish%20Partisan%20Educational%20Foundation%22&f=false|title=Essentials of Holocaust Education: Fundamental Issues and Approaches|last=Totten|first=Samuel|last2=Feinberg|first2=Stephen|date=2016-03-17|publisher=Routledge|isbn=9781317648079|language=en}}</ref> JPEF also conducts in-service workshops<ref>{{Cite book|url=https://books.google.com/books?id=hYf5AwAAQBAJ|title=Jewish Resistance Against the Nazis|last=Henry|first=Patrick|date=2014-04-20|publisher=CUA Press|isbn=9780813225890|language=en}}</ref> in the U.S. and internationally.

'''Photography Exhibit'''

Pictures of Resistance: The Wartime Photography of Jewish Partisan Faye Schulman, is a traveling exhibit of historical photos by the only known Jewish partisan photographer. The exhibit, curated by [[Jill Vexler]], has been displayed in more than 30 cities in the United States, Canada, Israel, Poland, South Africa, and Switzerland.<ref>{{Cite web|url=http://holocaustcentre.com/Programs-Exhibits/Temporary-Exhibitions|title=Temporary Exhibitions {{!}} Sarah and Chaim Neuberger - Holocaust Education Centre - UJA Federation of Greater Toronto|website=holocaustcentre.com|access-date=2016-05-25}}</ref><ref>{{Cite web|url=http://www.en.galiciajewishmuseum.org/pictures-of-resistance-tour.html|title=PICTURES OF RESISTANCE TOUR - Galicja|website=www.en.galiciajewishmuseum.org|access-date=2016-05-25}}</ref><ref>{{Cite web|url=http://www.ctholocaust.co.za/pages/annual_review-11.pdf|title=The South African Holocaust & Genocide Foundation Annual Review 2011|last=|first=|date=|website=|publisher=|access-date=}}</ref>

== Films ==
JPEF has produced 12 short documentaries directed by Braff, and narrated by [[Ed Asner]], [[Larry King]], [[Liev Schreiber]], and [[Tovah Feldshuh]].<ref name=":2">{{Cite web|url=http://www.jewishpartisans.org/films|title=Jewish Partisan Educational Foundation - Films|website=www.jewishpartisans.org|access-date=2016-06-03}}</ref> In chronological order:

*Introduction to the Jewish Partisans  
*Women in the Partisans Living & Surviving in the Partisans: Food
*Living & Surviving in the Partisans: Winter and Night
*Living & Surviving in the Partisans: Medicine 
*Living & Surviving in the Partisans: Shelter
*Antisemitism in the Partisans
*The Partisans Through the Eyes of the Soviet Newsreel
*A Partisan Returns: The Legacy of Two Sisters
*Introduction to the Bielski Brothers
*The Reunion 
*Survival in the Forest: Isidore Karten & the Partisans

== References ==

{{Reflist}}

== External links ==
*[http://www.jewishpartisans.org/ Jewish Partisan Educational Foundation]  
*[http://www.paramount.com/movies/defiance/ Defiance motion picture] 
*[http://www.gfh.org.il/Eng/ Ghetto Fighter's House]

[[Category:Jewish resistance during the Holocaust]]
[[Category:Articles created via the Article Wizard]]