{{Infobox political post
|post            = Comptroller
|body            = Illinois
|insignia        = Seal of Illinois.svg
|insigniasize    = 150px
|insigniacaption = [[Seal of Illinois]]
|image           = Susana Mendoza 2015.jpg
|incumbent       = [[Susana Mendoza]]
|incumbentsince  = December 5, 2016
|termlength      = Four years, no term limits
|formation       = {{start date|1970|12|15}}
|inaugural = 	[[George W. Lindberg]]
|salary          = $135,669 (2016)<ref>{{cite web | url=http://knowledgecenter.csg.org/kc/system/files/4.11%202016.pdf | title=SELECTED STATE ADMINISTRATIVE OFFICIALS: ANNUAL SALARIES | publisher=The Council of State Governments | date=April 11, 2016 | accessdate=March 24, 2017 }}</ref>
|website         = {{URL|https://illinoiscomptroller.gov/}}
}}The '''[[Comptroller]] of Illinois''' is an elected official of the U.S. state of [[Illinois]].  They are responsible for maintaining the State's fiscal accounts, and for ordering payments into and out of them.  The office was created by the [[Constitution of Illinois|Illinois Constitution of 1970]], replacing the office of Auditor of Public Accounts. 

The Comptroller of Illinois was [[Judy Baar Topinka]], a member of the [[Republican Party (United States)|Republican Party]] first elected in 2010 and subsequently re-elected in 2014 to a second four-year term. However, Topinka died unexpectedly in December 2014.<ref>{{cite news|title=Illinois Comptroller Judy Baar Topinka Dead at 70 |url=http://www.nbcchicago.com/news/politics/illinois-comptroller-judy-baar-topinka-death-285327641.html|accessdate=10 December 2014|publisher=NBC 5 Chicago, [[WMAQ-TV|WMAQ]]|date=10 December 2014}}</ref> On December 19, Governor [[Pat Quinn (politician)|Pat Quinn]] appointed [[Jerry Stermer]] to succeed Topinka, to serve until January 12, 2015, when he was replaced by [[Leslie Munger]], who was appointed by Quinn's successor as governor, [[Bruce Rauner]].<ref>{{cite news|title=Quinn names longtime aide Stermer to succeed Topinka as comptroller |url=http://www.chicagotribune.com/news/local/politics/chi-quinn-to-name-topinka-successor-20141219-story.html|accessdate=19 December 2014|publisher=Chicago Tribune}}</ref><ref name=WGN9>(January 5, 2015) - [http://wgntv.com/2015/01/05/rauner-to-appoint-leslie-munger-as-next-illinois-comptroller/ "Rauner to Appoint Leslie Munger as Next Illinois Comptroller"]. ''WGNtv.com''. Retrieved January 12, 2015.</ref><ref>{{cite news | url = http://www.sj-r.com/article/20150112/News/150119835 | title = Newcomers, veterans sworn in as statewide officers | agency = [[Associated Press]] | work = [[The State Journal-Register]] | location = Springfield, Illinois | date = January 12, 2015 <!-- 8:08PM --> | accessdate = 2015-02-01 }}</ref> Munger was then defeated by [[Susana Mendoza]] in 2016. 

== Duties ==

The Comptroller is charged, by the terms of Section 17 of Article V of the Constitution of Illinois, with the duties of: (a) maintaining the central fiscal accounts of the state, and (b) ordering payments into and out of the funds held by the [[Treasurer of Illinois]].  In accordance with this duty, the Comptroller signs [[paycheck]]s or grants approval to [[electronic payment]]s made by the state to its employees and creditors.<ref name="Constitution">Section 3, [http://ilga.gov/commission/lrb/con5.htm Article V], "Constitution of Illinois", accessed April 12, 2008.</ref>
The Comptroller is also charged, by Illinois statute, with certain additional duties.  In particular, the Comptroller regulates [[cemetery|cemeteries]] under the Cemetery Care Act, and is charged with the fiduciary protection of cemetery care funds used for the care and maintenance of Illinois gravesites.<ref name="Cemetery Care Act">[http://ilga.gov/legislation/ilcs/ilcs3.asp?ActID=2136&ChapAct=760%26nbsp%3BILCS%26nbsp%3B100%2F&ChapterID=61&ChapterName=TRUSTS+AND+FIDUCIARIES&ActName=Cemetery+Care+Act%2E 760 ILCS 100/1 et seq.], "Illinois Compiled Statutes", accessed April 12, 2008.</ref>

The Illinois Constitution provides that the comptroller must, at the time of his or her election, be a United States citizen, at least 25 years old, and a resident of the state for at least 3 years preceding the election.<ref name="Constitution"/>

The Comptroller's office operates a [[web page]] describing the office's powers and duties.<ref name="Comptroller">"Illinois State Comptroller Daniel W. Hynes", accessed April 12, 2008.[http://www.ioc.state.il.us/ Illinois State Comptroller web page]</ref>

==Controversy==
Some legislators have perceived a redundancy overlap between the offices of Comptroller and Treasurer, and have therefore proposed constitutional amendments to merge the two offices and earn administrative savings.  For example, HJRCA 12, considered by the [[Illinois General Assembly]] in the 2008-2009 session, would merge the office of Comptroller into the office of Treasurer.<ref name="HJRCA 12">{{cite web |title=House Joint Resolution - Constitutional Amendment 12 |accessdate=March 8, 2009 |url=http://www.ilga.gov/legislation/fulltext.asp?DocName=&SessionId=76&GA=96&DocTypeId=HJRCA&DocNum=12&GAID=10&LegID=41325&SpecSess=&Session=}}</ref>

In 2011, Comptroller Topinka and the Treasurer, [[Dan Rutherford]], introduced legislation to allow voters to decide whether the offices should be merged.<ref name="Move to Allow Vote">{{cite news|last=McQueary|first=Kristen|title=Move to Allow Vote to Merge Treasurer and Comptroller Jobs Stalls in House|url=https://www.nytimes.com/2012/01/01/us/illinois-voters-may-vote-whether-to-merge-treasurer-and-comptroller-jobs.html|accessdate=11 January 2012|newspaper=The New York Times|date=December 31, 2011}}</ref>  The legislation was opposed by Michael Madigan, [[Speaker (politics)|Speaker]] of the [[Illinois House of Representatives]], and did not become law.<ref name="Block of merger">{{cite news|last=Wetterich|first=Chris|title=Madigan blocking merger of treasurer, comptroller's offices|url=http://www.sj-r.com/top-stories/x795262278/Madigan-blocking-proposed-merger-of-treasurer-comptrollers-offices|accessdate=11 January 2012|newspaper=The State Journal-Register|date=8 June 2011}}</ref>

==List of office holders==
The following is an historic list of office holders for the Comptroller of Illinois and its preceding office, the Auditor of Public Accounts.<ref>[http://www.ioc.state.il.us/office/history.cfm History of the Office of Comptroller of Illinois]</ref>

===Auditors of Public Accounts, Northwest Territory===
*[[Rice Bullock]] (1799–1800)

===Auditors of Public Accounts, Indiana Territory===
*vacant (1801–1809)

===Auditors of Public Accounts, Illinois Territory===
*vacant (1809–1812)
*[[H. H. Maxwell]] (1812–1816)
*[[Daniel Pope Cook]] (1816)
*[[Robert Blackwell (Illinois politician)|Robert Blackwell]] (1817)
*[[Elijah C. Berry]] (1817–1818)

===Auditors of Public Accounts, State of Illinois===
{| class="wikitable" border="1"
|-
!#
! Name
! Political Party
! Term
|-
|1
|Elijah C. Berry
|
|1818–1831
|-
|2
|James T. B. Stapp
|
|1831–1835
|-
|3
|[[Levi Davis]]
|
|1835–1841
|-{{Party shading/Democratic}}
|3
|[[James Shields (politician, born 1810)|James Shields]]
|Democratic
|1841–1843
|-{{Party shading/Democratic}}
|4
|[[William L. D. Ewing]]
|Democratic
|1843–1846
|-
|5
|[[Thomas Hayes Campbell]]
|
|1846–1857
|-{{Party shading/Republican}}
|6
|[[Jesse K. Dubois]]
|Republican
|1857–1864
|-
|7
|Orlin H. Miner
|
|1864–1869
|-
|8
|[[Charles E. Lippincott]]
|
|1869–1877
|-
|9
|[[Thomas B. Needles]]
|
|1877–1881
|-
|10
|Charles P. Swigart
|
|1881–1889
|-
|11
|[[Charles W. Pavey]]
|
|1889–1893
|-
|12
|[[David Gore]]
|
|1893–1897
|-
|13
|[[James S. McCullough]]
|
|1897–1913
|-{{Party shading/Democratic}}
|14
|[[James J. Brady (Illinois)|James J. Brady]]
|Democratic
|1913–1917
|-{{Party shading/Republican}}
|15
|[[Andrew Russell (Illinois politician)|Andrew Russell]]
|Republican
|1917–1925
|-{{Party shading/Republican}}
|16
|[[Oscar Nelson]]
|Republican
|1925–1933
|-{{Party shading/Democratic}}
|17
|[[Edward J. Barrett (Illinois politician)|Edward J. Barrett]]
|Democratic
|1933–1941
|-
|18
|[[Arthur C. Lueder]]
|
|1941–1949
|-
|19
|Benjamin O. Cooper
|
|1949–1953
|-{{Party shading/Republican}}
|20
|[[Orville E. Hodge]]
|Republican
|1953–1956
|-
|21
|[[Lloyd Morey]]
|
|1956–1957
|-{{Party shading/Republican}}
|22
|[[Elbert S. Smith]]
|Republican
|1957–1961
|-{{Party shading/Democratic}}
|23
|[[Michael J. Howlett]]
|Democratic
|1961–1973
|}

===Comptrollers, State of Illinois===
{| class="wikitable" border="1"
|-
!#
!Name
!Political Party
!Term
|- {{Party shading/Republican}}
|1
|[[George W. Lindberg]]
|[[Republican Party (United States)|Republican]]
|1973–1977
|- {{Party shading/Democratic}}
|2
|[[Michael J. Bakalis]]
|[[Democratic Party (United States)|Democratic]]
|1977–1979
|- {{Party shading/Democratic}}
|3
|[[Roland W. Burris]]
|[[Democratic Party (United States)|Democratic]]
|1979–1991
|- {{Party shading/Democratic}}
|4
|[[Dawn Clark Netsch]]
|[[Democratic Party (United States)|Democratic]]
|1991–1995
|- {{Party shading/Republican}}
|5
|[[Loleta A. Didrickson]]
|[[Republican Party (United States)|Republican]]
|1995–1999
|- {{Party shading/Democratic}}
|6
|[[Daniel Hynes|Dan Hynes]]
|[[Democratic Party (United States)|Democratic]]
|1999–2011
|- {{Party shading/Republican}}
|7
|[[Judy Baar Topinka]]
|[[Republican Party (United States)|Republican]]
|2011–2014
|- {{Party shading/Democratic}}
|8
|[[Jerry Stermer]]
|[[Democratic Party (United States)|Democratic]]
|2014–2015
|- {{Party shading/Republican}}
|9
|[[Leslie Munger]]
|[[Republican Party (United States)|Republican]]
|2015–2016
|- {{Party shading/Democratic}}
|10
|[[Susana Mendoza]]
|[[Democratic Party (United States)|Democratic]]
|2016–present
|}

==References==
{{reflist|2}}

{{Illinois statewide elected officials}}

[[Category:State constitutional officers of Illinois|Comptroller]]
[[Category:Comptrollers of Illinois| ]]