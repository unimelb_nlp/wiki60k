{{Infobox military person
|name=Vladimir Kokkinaki
|birth_date= 12 June 1904
|death_date= {{Death date and age|df=yes|1985|1|6|1904|6|12}}
|image=V.K. Kokkinaki.jpg
|caption=Major General of Aviation, V.K. Kokkinaki
|birth_place= [[Novorossiysk]], [[Russian Empire]]
|death_place= [[Moscow]], [[Soviet Union]]
|allegiance= {{flag|Soviet Union}}
|branch= [[Red Air Force]]
|serviceyears= 1925 - 1966
|rank= [[File:RA-SA AF F6MajGen 1955.png|10px]] [[Military ranks of the Soviet Union|Major General of Aviation]]
|unit=
|commands=
|battles=
|awards= {{Hero of the Soviet Union}} {{Hero of the Soviet Union}}
|relations= 
|laterwork=
}}
'''Vladimir Konstantinovich Kokkinaki''' ({{lang-ru|Владимир Константинович Коккинаки}}, {{OldStyleDate|25 June|1904|12 June}} - 6 January 1985) was a [[test pilot]] in the [[Soviet Union]], notable for setting twenty-two [[world record]]s and serving as president of the ''[[Fédération Aéronautique Internationale]]''.<ref name=bse>[http://bse.sci-lib.com/article062616.html Great Soviet Encyclopedia entry]</ref>

==Life and career==

Kokkinaki was born in [[Novorossiysk]] on 12 June 1904 (O.S.) to a family of [[Greek people|Greek]] descent. His younger brother, Konstantin Kokkinaki (1910–1990), was also a distinguished test pilot. In 1921 he finished elementary school, and worked on grape plantations and in the Novorossiysk port.<ref name=warheroes>[http://www.warheroes.ru/hero/hero.asp?Hero_id=591 Biography at ''warheroes.ru'']</ref>

He entered the [[Red Army]] in 1925, and served in the infantry until July 1927. He then entered the Leningrad Military-Theoretical School of the Red Air Force, from where he graduated in 1928. He subsequently entered the [[Borisoglebsk]] pilot school, from where he graduated in 1930. In April 1931, after a period of service in the 11th Fighter Squadron in the Moscow military district, he was transferred back to the Leningrad Military-Theoretical School as an instructor because of his pilot skills.<ref name=warheroes/>

In the period 1932-1935 he served as test pilot for the Air Force, testing a series of aircraft. The first aircraft he tested was the attack aircraft Kocherigin-Gurevich TSh-3.<ref>[http://www.ctrl-c.liu.se/misc/RAM/tsh-3.html TSh-3, TsKB-4 (S.A. Kocherigin and M.I.Gurevich)]</ref> Subsequently, he entered the [[Ilyushin]] Design Bureau ([[OKB]]) as its chief test pilot, where he remained until 1964. Throughout that period, he was the first to test fly all the aircraft of the OKB, including the prototypes of the [[Ilyushin Il-4]] medium bomber, the [[Ilyushin Il-2|Ilyushin Il-2 ''Shturmovik'']] attack aircraft, the [[Ilyushin Il-28]] jet bomber, and the [[Ilyushin Il-14]] transport aircraft.<ref>[http://www.ilyushin.org/eng/history/dates.html Dates of Maiden Flights of Aircraft designed by the Ilyushin Design Bureau]</ref> He became a member of the [[Communist Party of the Soviet Union]] in 1938.<ref name=bse/> Promoted to Major General of Aviation in 1943, during the Second World War Kokkinaki served as head of the Main Inspectorate of the [[People's Commissariat]] for the Aircraft Industry and as head of its flight-test service between the years 1943-1947. He retired from the Air Force in January 1966, but continued working for OKB Ilyushin in a supervisory capacity with overall responsibility for flight testing, with his last project being the [[Ilyushin Il-62]].<ref name=warheroes/>

He became the vice chairman of [[Fédération Aéronautique Internationale]] (FAI) in 1961 and assumed the position of chairman in 1966, a position he held until 1967,<ref>[http://www.fai.org/about/history/presidents FAI Presidents]</ref> after which he was appointed to the honorary chairmanship.<ref name=peoples>[http://www.peoples.ru/military/aviation/kokkinaki/ Biography at ''peoples.ru'']</ref> In the 1960s, he also served as chairman of the USSR Aviation Sport Federation. Kokkinakis lived in Moscow, where he died on 7 January 1985. He is buried in the [[Novodevichy cemetery]] in Moscow, along with his wife, Valentina.<ref name=warheroes/>

==Records==
On 21 November 1935, Kokkinaki managed to obtain the unofficial [[Aircraft records|world record for ceiling]] in a [[Polikarpov I-15]] fighter, reaching the altitude of 14,575 meters. On 20 April 1936, he performed the Nesterov Loop for the first time with a twin-engine aircraft, a  TsKB-26 (the prototype of the [[Ilyushin Il-4]]), in the presence of [[Joseph Stalin]].<ref>[http://www.ctrl-c.liu.se/misc/ram/tskb-26.html TsKB-26 (S.V. Ilyushin)]</ref> In 1936-1937, Kokkinaki set seven successive altitude with varying payload records with the TsKB-26.<ref name=airwar>[http://www.airwar.ru/history/aces/legend/pilot/kokkinaki.html Biography at ''airwar.ru'']</ref><ref name=records>[http://www.ilyushin.org/eng/history/records.html World Records set up by the Aircraft Developed by the Ilyushin Experimental Design Bureau]</ref> His first official record, on 17 July 1936, was also the first official Soviet aviation record.<ref name=McCannon>McCannon (1998), p. 79</ref>

On 28 June 1937 Kokkinaki flew the circular Moscow - [[Sevastopol]] - [[Yekaterinburg|Sverdlovsk]] - Moscow route, a distance of 5018&nbsp;km, with a DB-3, setting a record in both speed at 5000&nbsp;km range (with a median speed of 325&nbsp;km/h) and range on a circular route.<ref name=records/> This flight was followed three months later by the 4000&nbsp;km Moscow - [[Baku]] - Moscow route.<ref>[http://www.ilyushin.org/eng/history/flights.html Remarkable Flights accomplished by the aircraft developed by the Ilyushin Experimental Design Bureau]</ref> On 27–28 June 1938, on board a modified TsKB-30 named "[[Moscow|Moskva]]", with A.M. Bryandinskiy as his [[navigator]], Kokkinaki flew from Moscow to [[Spassk-Dalny]] in the Soviet Far East, covering a distance of 7,580&nbsp;km in 24 h 36 min, mostly at an altitude of 7000 meters, with an average speed of 307&nbsp;km/h.<ref name=TsKB-30>[http://www.ctrl-c.liu.se/misc/RAM/tskb-30.html TsKB-30 (S.V. Ilyushin)]</ref> For this feat, he was awarded the title "[[Hero of the Soviet Union]]" on 17 July.<ref name=warheroes/> On 28 April 1939, with Mikhail Kh. Gordienko as co-pilot, he tried to surpass this feat by performing a non-stop east-west transatlantic flight from Moscow to [[New York City]], to coincide with the opening of the [[1939 New York World's Fair|"Land of Tomorrow" World Fair]].<ref name=McCannon>McCannon (1998), p. 79</ref> However, due to encountering bad weather, the airplane was forced to come down on [[Miscou Island]] in New Brunswick, Canada ([http://www.time.com/time/magazine/article/0,9171,761240,00.html TIME magazine account]). Coming not long after the death of [[Valery Chkalov]], and with the approaching war, this well-publicized debacle spelled the end of the Soviet [[Arctic aviation]] exploits of the 1930s. Despite failing to reach his original destination, he still covered a distance of 8,000&nbsp;km in 22 h 56 min, at an average speed of 348&nbsp;km/h,<ref name=TsKB-30/> and since 1959, the route he used ([[Moscow]]- [[Novgorod]] - [[Helsinki]] - [[Trondheim]] - [[Iceland]] - [[Cape Farewell, Greenland|Cape Farewell]] - Miscou Island) is used for the regular flights between New York and Moscow. In 1965, he was honored by the [[International Air Transport Association]] with the diamond "[[Compass rose|wind rose]]" necklace for his finding the "shortest flight route between Europe and America".<ref name=bse/><ref name=warheroes/>

From 1958 to 1960, he set another series of thirteen altitude with load and speed records flying the [[Ilyushin Il-18]].<ref name=warheroes/> His last world record was in 1960, when he flew an Il-18 at a distance of 5,018 kilometers with a payload of 10,000&nbsp;kg and a median speed of 693&nbsp;km/h.<ref name=records/>

==Awards and honours==
[[File:Rus Stamp GSS-Kokkinaki.jpg|thumb|200px|2004 Russian stamp honouring Kokkinanki]]
[[File:Aeroflot Ilyushin Il-96-300 RA-96011 Kokkinaki.jpg|thumb|200px|[[Aeroflot]] [[Ilyushin Il-96]] RA-96011 named after Kokkinaki]]
Kokkinaki was awarded:
*The title "[[Hero of the Soviet Union]]" (in 1938 and 1957) [[Gold Star]]s Nos. 77 and 179);
*The title "[[Honoured Test Pilot of the USSR]]";
*Six [[Order of Lenin|Orders of Lenin]] (in 1936, 1938, 1939, 1945, 1951 and 1984);
*The [[Order of the October Revolution]] (in 1974);
*Three [[Order of the Red Banner|Orders of the Red Banner]] (1944, 1945 and 1957);
*Two [[Order of the Patriotic War|Orders of the Patriotic War]] 1st class (in 1944 and 1947);
*Four [[Order of the Red Star|Orders of the Red Star]] (1939, 1941, 1944 and 1969);
*The [[Medal "For Courage" (Russia)|Medal "For Courage"]] in 1939;
*Multiple foreign medals, including the FAI Gold Air Medal in 1964.<ref>[http://www.fai.org/awards/award.asp?id=6 The FAI Gold Air Medal]</ref> He was also awarded the titles "Distinguished Test Pilot of the Soviet Union" and "Distinguished Master of Sports of the Soviet Union" in 1959 and the [[Lenin Prize]] in 1960.<ref name=warheroes/><ref name=airwar/>

A street in Moscow has been named after him (fittingly, next to the ''Academician Ilyushin'' Street), and a bronze bust was erected in his home city of Novorossiysk on the occasion of the second award of the HSU title and Gold Star. A tanker, built in [[Kherson]] in 1985, is also named in his honor.

A heavy-lift airplane [[Volga-Dnepr]] [[Ilyushin Il-76TD|Ilyushin Il-76TD-90VD]] (RA-76950, cn 2043420697) was named in his honour. An [[Aeroflot Russian Airlines]] [[Ilyushin Il-96]] (RA-96011, cn 74393201008) has also been named in his honour.

==References==
{{reflist|2}}

==Sources==
* {{ru icon}} [http://www.warheroes.ru/hero/hero.asp?Hero_id=591 Biography at ''warheroes.ru'']
* {{ru icon}} [http://www.peoples.ru/military/aviation/kokkinaki/ Biography at ''peoples.ru'']
* {{ru icon}} [http://www.airwar.ru/history/aces/legend/pilot/kokkinaki.html Biography at ''airwar.ru'']
* {{ru icon}} [http://bse.sci-lib.com/article062616.html Entry] in the ''[[Great Soviet Encyclopedia]]'', 3rd Edition
* {{ru icon}} [http://militera.lib.ru/bio/brontman_lk_kokkinaki/index.html 1939 biographical essay, by Lazar' K. Brontman]
* {{cite book |title=Red Arctic: Polar Exploration and the Myth of the North in the Soviet Union |last=McCannon |first=John |year=1998 |publisher=Oxford University Press (US) |isbn=0-19-511436-1 }}
* {{cite news|last1=Shabad|first1=Theodore|title=V. KOKKINAKI, 80; A SOVIET AVIATOR|url=https://www.nytimes.com/1985/01/10/world/v-kokkinaki-80-a-soviet-aviator.html|work=The New York Times|date=January 10, 1985}}

{{Authority control}}

{{DEFAULTSORT:Kokkinaki, Vladimir}}
[[Category:1904 births]]
[[Category:1985 deaths]]
[[Category:People from Novorossiysk]]
[[Category:People from Black Sea Governorate]]
[[Category:Russian people of Greek descent]]
[[Category:Communist Party of the Soviet Union members]]
[[Category:Members of the Supreme Soviet of the Soviet Union]]
[[Category:Soviet Air Force generals]]
[[Category:Soviet major generals]]
[[Category:Russian aviators]]
[[Category:Aviation pioneers]]
[[Category:Test pilots]]
[[Category:Soviet people of Greek descent]]
[[Category:Soviet World War II pilots]]
[[Category:Heroes of the Soviet Union]]
[[Category:Lenin Prize winners]]
[[Category:Burials at Novodevichy Cemetery]]
[[Category:Recipients of the Order of Lenin]]
[[Category:Recipients of the Order of the October Revolution]]
[[Category:Recipients of the Order of the Red Banner]]
[[Category:Recipients of the Order of the Patriotic War, 1st class]]
[[Category:Recipients of the Order of the Patriotic War, 2nd class]]
[[Category:Recipients of the Order of the Red Star]]
[[Category:Recipients of the Medal "For Courage" (Russia)]]