{{ infobox bibliographic database
| image       = 
| caption     = 
| producer    = Elsevier
| country     = The Netherlands
| history     = 2005-present
| languages   = English
| providers   = [[Ovid Technologies]] (exclusively)
| cost        = Subscription
| disciplines = molecular biology, biotechnology, genetics, biochemistry, microbiology,cell biology, developmental biology, agriculture, food science, plant sciences, zoology, environmental science, ecology, toxicology, laboratory science (fundamental research), and science in the field (applied sciences) 
| depth       =Index, abstract & full-text
| formats     =full text, books, science journals, electronic-only journals, trade publications, bibliographic data, organism taxonomy vocabulary, and life science thesaurus 
| temporal    =coverage is from 1980-present
| geospatial  =international - global
| number      = 4 million +
| updates     = weekly, and 250,000 per year
| p_title     = 
| p_dates     = 
| ISSN        = 
| web         = 
| titles      =http://www.elsevier.com/wps/find/authored_newsitem.cws_home/companynews05_00349  
}}

'''EMBiology''' is a bibliographic database established in June 2005, and produced by [[Elsevier]]. ''EMBiology'' focuses on indexing the literature in the [[life sciences]] in general. Coverage includes [[science]] in the laboratory ([[fundamental research]]) and science in the field ([[applied research]]). It is designed to be smaller than [[EMBASE]], with abstracting and indexing for  1,800 journals not covered by the larger database. However, there is some overlap. Hence, ''EMBiology'' is specifically designed for academic institutions that range from small to mid-size and all [[biotechnology]]  and [[pharmaceutical]] companies.<ref name=ovid-describe/><ref name=allbus/>

Global in scope, and with back file coverage to 1980, this database contains over four million bibliographic records, with an additional 250,000 records added annually. ''EMBiology''has cover to cover indexing of 2,800 active titles; these are peer reviewed journals, trade publications, and journals that are only in electronic format. A life science thesaurus known as ''EMTREE'' (see section below), and an organism taxonomy vocabulary of 500,000 terms are also part of this database.<ref name=ovid-describe/>

The organism vocabulary originates from the taxonomies described by the [[National Center for Biotechnology Information]] (NCBI) and the [[Integrated Taxonomic Information System]] (ITIS). Other Internet resource searching that is available are [[CAS Registry Number]]s, [[Enzyme Commission number|Enzyme Commission Numbers]], Cross Archive Searching (ARC), [[ChemFinder]], Molecular Sequence Information, Resource Discovery Network (RDN), and [[Scrius]].<ref name=journals/>

Subject coverage encompasses [[molecular biology]], [[biotechnology]], genetics, [[biochemistry]], [[microbiology]], [[cell biology]], [[developmental biology]], agriculture, [[food science]], plant sciences, [[zoology]], environmental science, ecology, [[toxicology]], laboratory science (fundamental research), and science in the field (applied sciences).<ref name=ovid-describe>
Description of ''EMBiology''
{{Cite web
  | title =Ovid and Elsevier announce exclusive partnership... 
  | publisher =Ovid Technologies, Inc. 
  | year =2005 
  | url =http://www.ovid.com/site/about/press_release20050606.jsp?top=42&mid=52 
  | format =Online 
  | accessdate =2011-01-30}} Press release</ref><ref name=allbus>
News release. 
{{Cite web
  | title =Ovid, Elsevier Launch EMBiology
  | publisher =All business.com
  | date =July 1, 2005 
  | url =http://www.allbusiness.com/technology/computer-software-management/888680-1.html 
  | format =Online
  | accessdate =2011-01-30}}</ref><ref name=journals>
{{Cite web
  | title =Open access journals listed in EMBiology
  | publisher =Ovid Technologies
  | date =January 9, 2006 
  | url =http://www.linksolver.com/site/pdf/LinksPackage-EMBiology.pdf 
  | format =Online
  | accessdate =2011-01-30}}</ref><ref name=information-today>
Weekly News Digest. 
{{Cite web 
  | title =Ovid Expands Electronic Content
  | publisher =Information Today
  | date =October 10, 2005 
  | url =http://newsbreaks.infotoday.com/Digest/Ovid-Expands-Electronic-Content-16108.asp 
  | format =Online
  | accessdate =2011-01-30}}</ref>

==EMTREE==
'''''EMTREE''''' is a life science thesaurus (database) published by [[Elsevier]] that is designed to support both ''[[EMBASE]]'' and ''EMBiology'' (see above).  This database contains descriptions of all biomedical terminology, indexing drug and disease with 56,000 search terms, and 230,000 synonyms.<ref>
{{Cite web 
  | title =EMTREE the Life Science Thesaurus 
  | publisher =Elsevier
  | date =
  | url = http://www.elsevier.com/wps/product/cws_home/707574  
  | format =Online
  | accessdate =2011-01-30}}</ref>

==References==
{{Reflist}}

==External links==
*[http://www.ovid.com/site/products/ovidguide/embodb.htm#top Field Guide] for EMBiology. Ovid Technologies. September 20, 2005. (accessed: 2011-01-30).

[[Category:Bibliographic databases and indexes]]
[[Category:Publications established in 2005]]
[[Category:Elsevier]]