{{featured article}}
{{Use dmy dates|date=February 2014}}
{{Use Australian English|date=February 2014}}

{{Infobox military person
| honorific_prefix  = 
| name              = Frank Jenner
| honorific_suffix  = 
| native_name       = 
| native_name_lang  = 
| image         = The sailor - Frank Jenner.jpg
| image_size    = 
| alt           = Official Royal Australian Navy portrait photograph of Frank Jenner
| caption       = Frank Jenner while he was a sailor on [[HMAS Canberra (D33)|HMAS ''Canberra'']]
| birth_date    = {{birth date|df=yes|1903|11|02}}
| death_date    = {{Death date and age|df=yes|1977|05|08|1903|11|02}}
| birth_place   = [[Southampton]], [[Hampshire]], England
| death_place   = [[Kogarah, New South Wales|Kogarah]], [[New South Wales]], Australia
| placeofburial = Woronora Lawn Cemetery
| placeofburial_label = 
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| nickname      = Bones
| birth_name    = 
| allegiance    = United Kingdom<br>United States<br>Australia
| branch        = [[Royal Navy]]<br>[[United States Navy]]<br>[[Royal Australian Navy]]
| serviceyears  = 
| rank          = 
| servicenumber = <!--Do not use data from primary sources such as service records.-->
| unit          = 
| commands      = 
| battles       = 
| battles_label = 
| awards        =
| spouse        = Jessie Jenner 
| relations     = Ann Carruthers (daughter)
| laterwork     = 
| signature     = 
| website       = <!-- {{URL|example.com}} -->
}}
'''Frank Arthur''' "'''Bones'''" '''Jenner''' (surname often misspelled '''Genor'''; 2 November 1903&nbsp;– 8 May 1977) was an Australian [[Evangelism|evangelist]]. His signature [[Approaches to evangelism|approach to evangelism]] was to ask people on [[George Street, Sydney|George Street]], [[Sydney]], "If you died within 24 hours, where would you be in eternity? Heaven or hell?" Born and raised in England, he contracted [[African trypanosomiasis]] at the age of twelve and suffered from [[narcolepsy]] for the rest of his life. After some time, he joined the [[Royal Navy]], but [[Desertion|deserted]] in New York and joined the [[United States Navy]]. When he was 24, he deserted again while in Australia. He subsequently worked for the [[Royal Australian Navy]] until he bought his way out in 1937.

That year, Jenner encountered a group of men from the [[Glanton]] [[Exclusive Brethren]] who were engaging in [[open-air preaching]], and he [[Conversion to Christianity|converted to Christianity]]. For 28 years, from his initial conversion until his [[Debility (medical)|debility]] from [[Parkinson's disease]], Jenner engaged in personal evangelism, probably speaking with more than 100,000 people in total. One person who became a Christian after encountering Jenner's question was [[Noel Stanton]], who went on to found the [[Jesus Army]] in 1969.

In 1952, the Reverend Francis Dixon of Lansdowne Baptist Church in [[Bournemouth]], England, began hearing several [[Testimony#Religion|testimonies]] from people who became Christians after Jenner accosted them on George Street, Sydney. The following year, Dixon met with Jenner in Australia and told him about the people he had met who had become Christians as a result of Jenner's evangelism, and Jenner, then fifty years old, cried because he had not previously known that even one of the people he had talked to had remained a Christian beyond their initial profession of faith.

Jenner died from [[colorectal cancer]] in 1977. While he was alive, very few people knew of him, but after he died, stories of his evangelistic activities circulated widely, and elements of some of these stories contradicted others. In 2000, Raymond Wilson published ''Jenner of George Street: Sydney's Soul-Winning Sailor'' in an attempt to tell the story of Jenner's life accurately. Nonetheless, conflicting accounts of Jenner's life have continued to propagate, including accounts from [[Ray Comfort]] and [[Ché Ahn]] in which Jenner is referred to as "Mr. Genor".

==Early life==

Frank Arthur Jenner was born on 2 November 1903 in [[Southampton]], [[Hampshire]], England.<ref name=Wilson60/> His father was a hotel pub owner and former [[sea captain]]. Jenner had four brothers. According to his posthumous biographer Raymond Wilson, Jenner was [[Anti-authoritarianism|anti-authoritarian]] as a boy and, at the age of twelve, during [[World War I]], he was sent to work aboard a [[training ship]] for misbehaving boys.<ref>Wilson (2000), p. 59.</ref> When he was fourteen, the ship sailed from Southampton to [[Cape Town]], South Africa. On the way, while the ship was docked at a port in West Africa, a [[tsetse fly]] bit Jenner and infected him with ''[[Trypanosoma]]''; he therefore contracted [[African trypanosomiasis]], which is also called "sleeping sickness". He subsequently entered a 15-day coma, but eventually recovered. From this point on, he suffered from [[excessive daytime sleepiness]] and was eventually diagnosed with [[narcolepsy]],<ref name=Wilson60/> which prevented him from ever being able to drive a car.<ref name=Wilson67/> When the war ended, he returned to England.<ref name=Wilson60>Wilson (2000), p. 60.</ref>

==Navy career==
[[File:HMAS Canberra sailing into Sydney Harbour in 1930.jpg|thumb|right|alt=HMAS Canberra sailing into Sydney Harbour in 1930|Jenner was one of the [[Royal Australian Navy]] sailors who retrieved [[HMAS Canberra (D33)|HMAS ''Canberra'']] from England.]]

After some time, Jenner joined the [[Royal Navy]], but [[Desertion|deserted]] in [[New York City]], United States. He soon joined the [[United States Navy]].<ref name=Wilson60/> Jenner's daughter stated in an interview after his death that he learned how to [[Gambling|gamble]] during this time and he soon developed the [[impulse control disorder]] of [[problem gambling]].<ref>Wilkinson (2013), 13:04.</ref> He became particularly attached to the game [[craps]], which was popular in the United States at the time. He started to keep a [[rabbit's foot]] in the left upper pocket of his shirt, and would rub it with his left hand while he rolled the dice with his right. His shipmates therefore began calling him "Bones", a nickname that he retained for the rest of his navy career.<ref name=Wilson60/>

When he was 24,<ref name=Wilson61>Wilson (2000), p. 61.</ref> his work with the United States Navy involved going to [[Australia]] and he deserted again, this time in [[Melbourne]].<ref name=Wilson60/> There, he met Charlie Peters, who invited him to his home to have a meal with his family including Jessie, Peters' 23-year-old daughter. Jessie and Jenner married a year later, on 6 July 1929,<ref name=Wilson61/> at [[HMAS Cerberus (naval base)|HMAS ''Cerberus'']].<ref>Wilkinson (2013), 13:47.</ref> They continued to live in Melbourne after their wedding and Jenner joined the [[Royal Australian Navy]]. He soon became one of the sailors assigned to travel to England to retrieve [[HMAS Canberra (D33)|HMAS ''Canberra'']].<ref name=Wilson61/> He was serving on [[HMAS Australia (D84)|HMAS ''Australia'']]<ref name=Wilson63/> in 1937 when he was legally [[Military discharge|discharged]] from the navy, buying his way out but not receiving a pension.<ref name=Wilson64>Wilson (2000), p. 64.</ref>

In 1939, with the onset of [[World War II]], Jenner was recalled to active duty. Because of his narcolepsy, he was given shore duties in Sydney. In this capacity, he participated in [[undercover operation]]s and delivered sealed orders.<ref name=Wilson65/> After the war, he left the navy and became a janitor for [[IBM]], a technology and consulting corporation.<ref name=Wilson67/>

==Conversion to Christianity==
[[File:Melbourne Collins Street Architecture.jpg|thumb|left|alt=Collins Street, Melbourne, in 2007|Jenner [[Conversion to Christianity|converted to Christianity]] because of [[open-air preaching]] on [[Collins Street, Melbourne]].]]

In 1937,<ref name=Wilson73/> Jenner encountered a group of men from the [[Glanton]] [[Open Brethren]]<ref name=Wilson62>Wilson (2000), p. 62.</ref> standing in front of the [[National Australia Bank]] on [[Collins Street, Melbourne|Collins Street]].<ref name=Wilson61/> One of the men was engaging in [[open-air preaching]]. Jenner interrupted the man to say that he would listen to the man's [[The gospel|good news]] provided that he was allowed to share some good news first.{{efn|In the [[New Testament]], the word "gospel" refers to the story of salvation through [[Jesus]]. In the [[Greek New Testament|original Greek version of the New Testament]], the word translated as "gospel" is ''euangelion'', which literally translates as "good news". Consequently, Christians sometimes refer to the gospel as the good news.<ref>Edwards (2002), p. 24.</ref>}} The man agreed, so Jenner taught the group of Brethren how to play craps there on the pavement. One of the Brethren invited Jenner into his home for tea and told him about the gospel. Jenner [[Conversion to Christianity|converted to Christianity]] and, when he went home, told Jessie that she was a sinner bound for hell and therefore in need of [[salvation]]. According to Wilson's biography of Jenner, Jessie thought that Jenner had become [[Mania|manic]] or insane. They had a young daughter named Ann by this point and Jenner was gambling so much that he was not providing for his family. For both of these reasons, Jessie left Jenner and moved to [[Corowa]] to work on a farm, taking Ann with her. She said that she would return only when Jenner regained his sanity. On several occasions, he aggressively told Jessie's brothers that they needed to become Christians, which angered them.<ref name=Wilson62/> On one of these occasions, their conversation became physical and they began punching each other. The brothers [[Social rejection|rejected]] Jenner and were never reconciled to him. He wrote to his family back in England informing them of his conversion and asking them to become Christians too, but he received no reply.<ref name=Wilson63>Wilson (2000), p. 63.</ref>

[[File:SS ORONSAY underway near Circular Quay (13860178344).jpg|thumb|alt=SS Oronsay near Sydney, Australia|Jenner's wife and daughter moved to [[India]] to avoid the stress of poverty arising from Jenner's frequent unemployment, but they eventually returned to him on [[SS Oronsay (1924)|SS ''Oronsay'']].]]

Later in 1937, Jessie became seriously infected with [[boil]]s and,<ref name=Wilson64/> while under the care of a Glanton Brethren family,<ref name=Wilson63/> became a Christian. Before the end of the year, Jenner and Jessie began living together again.<ref name=Wilson64/> Although Jenner gave up gambling,<ref name=Wilson65/> he was often [[Unemployment|unemployed]] because he would evangelise at his workplace and then be fired.<ref name=Wilson64/> In 1939, Jessie developed a [[peptic ulcer]].<ref name=Wilson65/> At the time, it was believed that such ulcers were caused by [[Stress (biology)|stress]],<ref name=PaulWilliams1136>Paul & Williams (2009), p. 1136.</ref>{{efn|Research has since demonstrated that peptic ulcers are not caused by stress but rather by infection by the bacterium ''[[Helicobacter pylori]]'', which may be [[Foodborne illness|foodborne]] or [[Waterborne diseases|waterborne]].<ref name=PaulWilliams1136/>}} and Jessie's ulcer was therefore attributed to the stress induced by the family's lack of money. Consequently, she and Ann moved to [[India]] to live with Jenner's aunt Emily McKenzie, who ran the [[Kotagiri]] Keswick Missionary Home. Ann subsequently attended [[Hebron School]] in [[Ooty]], [[Tamil Nadu]], until she was ten years old. Once Jessie had recovered from her illness, they returned to Sydney on [[SS Oronsay (1924)|SS ''Oronsay'']].<ref name=Wilson65>Wilson (2000), p. 65.</ref>

Jenner would normally wake up to pray at 5&nbsp;am each day.<ref>Wilson (2000), p. 76.</ref> In the 1940s, Jenner left the Glanton Brethren and joined the [[Open Brethren]].<ref name=Wilson66/> For the rest of his life, Jenner attended Open Brethren churches: one on [[Goulburn Street]] in Sydney and the other in [[Bexley, New South Wales]].<ref name=Wilson10/> At these churches, people did not understand what narcolepsy was and thought that Jenner was consistently falling asleep during services because he lacked respect for God. The church on Goulburn Street also disapproved of his partnership with other Christian organisations and churches;<ref>Wilson (2000), p. 79.</ref> Jenner actively partnered with [[The Navigators (organization)|The Navigators]], [[Everyman's Welfare Service|Campaigners for Christ]], [[Baptists]], [[Anglicanism|Anglicans]], and [[Methodism|Methodists]].<ref name=Wilson66/>

==Evangelism==
[[File:Gearge st sydney1345.JPG|thumb|right|alt=George Street, Sydney, in 2009|Jenner likely asked more than 100,000 people, mostly on [[George Street, Sydney]], whether they were headed for heaven or hell.]]

Out of gratitude to God for giving him salvation, Jenner committed to consistently engaging in personal evangelism, and aimed to talk with ten different people every day thenceforward. For 28 years, from his initial conversion until his [[Debility (medical)|debility]] from [[Parkinson's disease]], Jenner engaged in this form of evangelism. He probably spoke with more than 100,000 people in total,<ref name=Wilson73/> hundreds of whom made initial professions of commitment to Christianity.<ref>Wilkinson (2013), 22:05.</ref> He kept [[Tract (literature)#Religious tracts|religious tracts]] in his shirt pocket where he had previously kept his rabbit's foot, and he often gave these tracts to people he met.<ref name=Wilson60/> He also kept a card in his pocket with [[Philippians 4]]:13 on it in order to give himself courage in evangelising. This verse reads, "I can do all things through Christ who strengthens me."{{bibleref2c|Philippians|4:13|NIV|Phil.4:13}} While engaging in these activities, Jenner would normally wear a white shirt, black shoes, and [[trousers]], and sometimes a navy [[greatcoat]].<ref name=Wilson73/> Usually evangelising on [[George Street, Sydney]],<ref>Wilson (2000), p. 27.</ref> Jenner asked many people the same question: "If you died within 24 hours, where would you be in eternity? Heaven or hell?"<ref name=Wilson73>Wilson (2000), p. 73.</ref> If they were willing to engage in conversation with him, he would invite them either to his home or to a local church.<ref name=Wilson66/> The question became known as "the Frank Jenner question".<ref>Wilkinson (2013), 2:03.</ref> Jenner was most active in evangelism during World War II.<ref>Wilkinson (2013), 9:21.</ref> On Saturday nights during the war, Jenner would invite groups of sailors to his home for a service consisting of some hymns and a short sermon.<ref name=Wilson67/>

One of the people to whom Jenner posed his question was [[Noel Stanton]],<ref name=Wilson44/> a man from [[Bedfordshire]], England, who was serving in Sydney<ref name=Army>{{Cite web|title=Noel Stanton (1926–2009)|publisher=[[Jesus Army]]|location=[[Nether Heyford]], England|url=http://www.jesus.org.uk/jesus-army-more-depth/noel-stanton|accessdate=19 September 2013}}</ref> with the Royal Navy at the time.<ref>Cooper (1997), p. 24.</ref> Stanton became preoccupied with the memory of this meeting for several months afterwards and, the next year,<ref name=Wilson44>Wilson (2000), p. 44.</ref> became a committed Christian. Stanton went on to found the [[Jesus Army]] in [[Northampton]], England, in 1969.<ref name=Army/> In 1945, Jenner approached Norrie Jeffs, who had just returned from participating in [[Operation Meridian]] at [[Palembang]] on [[Sumatra]], and, having asked Jeffs his question, Jeffs responded that he was already a Christian. Jenner then invited Jeffs over to his house, where Jeffs met several other visitors, including the woman who would later become his wife.<ref>Wilkinson (2013), 17:31.</ref> In 1952,<ref>Wilkinson (2013), 1:37.</ref> another person Jenner accosted with his question on George Street was Ian Boyden, a man from [[Roseville, New South Wales|Roseville]] who was serving in the [[Royal Australian Air Force]]. After having a brief conversation with Jenner, Boyden accepted Jenner's invitation to attend a church service at Renwick Gospel Hall, where he responded to the sermon by committing to living as a Christian thenceforward, which he did for at least fifty years.<ref>Wilson (2000), p. 48.</ref> Many other people who had a brief encounter with Jenner on the street in Sydney also became Christians,<ref name=Wilson9/> but Jenner did not realise that any of the people he accosted had remained a Christian beyond their initial profession of faith<ref name=Wilson54/> until 1953, when Francis Dixon told him the stories of several such people.<ref name=Wilson67/>

When Dave Rosten, another Sydney evangelist, attempted to imitate Jenner's method of evangelism, he was punched in the midriff by the first person he spoke to, so he decided that Jenner's [[Approaches to evangelism|approach to evangelism]] was not for others to emulate.<ref name=Wilson74/> In 1947, Jenner asked his question to a man named Angus Carruthers, who responded that he was a Christian and going to heaven. Jenner invited Carruthers back to his home, where Carruthers met Jenner's daughter, Ann. Carruthers and Ann married three years later.<ref>Wilson (2000), p. 78.</ref>

==Discovery by Francis Dixon==

[[File:Frank Jenner the evangelist.jpg|thumb|upright|left|alt=Frank Jenner during his years as an evangelist|The story of Frank Jenner ''(pictured)'' became popular because of Francis Dixon, who investigated after meeting many people who had [[Conversion to Christianity|converted to Christianity]] after encountering a man on [[George Street, Sydney|George Street]].]]

The Reverend<ref name=Wilson10/> Francis Willmore Dixon<ref name=Wilson51/> was the head pastor of Lansdowne Baptist Church<ref name=Wilson43/> in [[Bournemouth]], England,<ref name=Wilson51>Wilson (2000), p. 51.</ref> and his youth pastor, Peter Culver, had become a Christian as a result of meeting Jenner<ref name=Wilson43>Wilson (2000), p. 43.</ref> on George Street on 2 September 1945.<ref>Wilson (2000), p. 42.</ref> In 1952, at an [[All Nations Christian College|All Nations Bible College]] event, Dixon and Culver heard Noel Stanton's Christian [[Testimony#Religion|testimony]], which included the episode in which Stanton had met Jenner. Dixon then realised that Culver and Stanton must have become Christians as a result of the same man.<ref name=Wilson44/> The following year, Dixon heard two different British sailors who did not know each other tell their testimonies at Lansdowne Baptist Church, and both had told very similar stories to Culver and Stanton; both had been walking down George Street and had been asked Jenner's question.<ref name=Wilson51/>

Dixon then travelled to Australia with his wife<ref name=Wilson51/> to engage in [[Itinerant minister|itinerant preaching]] there.<ref name=Wilson52>Wilson (2000), p. 52.</ref> Dixon hoped to find Jenner there,<ref name=Wilson51/> although Dixon did not yet know the name of the man he was looking for.<ref name=Wilson54>Wilson (2000), p. 54.</ref> In [[Adelaide]], Dixon told the stories of Culver and Stanton while preaching. Murray Wilkes<ref name=Wilson53>Wilson (2000), p. 53.</ref> then approached Dixon and said that he had also become a Christian after having been asked Jenner's question on George Street.<ref name=Wilson52/> At a Methodist church in [[Perth]], Dixon told Culver's, Stanton's, and Wilkes' stories again, and met yet another person who had become a Christian after an encounter with Jenner.<ref name=Wilson53/> Finally arriving in Sydney, Dixon asked Alec Gilchrist<ref name=Wilson54/> of Campaigners for Christ<ref name=Wilson66>Wilson (2000), p. 66.</ref> if he knew a man who asked strangers on George Street whether or not they were headed for heaven or hell. Gilchrist was familiar with Jenner<ref name=Wilson66/> and informed Dixon about how to contact Jenner. Dixon visited Jenner at his house and told him about all the people he had met who had become Christians because of Jenner's evangelism. Jenner, now fifty years old, had never before heard of even one person living their lives as Christians as a result of his evangelism, and he cried upon hearing that there were several.<ref name=Wilson54/>

After returning from Australia, Dixon went on to discover more people who had become Christians because of Jenner in Bournemouth,<ref name=Wilson54/> [[Cumbria]], India,<ref>Wilson (2000), p. 55.</ref> and Jamaica.<ref>Wilson (2000), p. 56.</ref> By 1979, Dixon had discovered ten people who had become Christians as a result of Jenner's evangelism. It is because of Dixon that the story of Jenner's evangelism began to be told.<ref>Wilkinson (2013), 2:27.</ref> Dixon's wife Nancy wrote an account of Jenner's evangelism, which she called "The Jenner Story".<ref name=Wilson10/>

==Later life==
[[File:Bexley Gospel Hall.JPG|thumb|right|alt=Bexley Gospel Hall in 2007|Jenner attended Bexley Gospel Hall from 1953 until his death in 1977.]]

In later years, Jenner developed Parkinson's disease and therefore retired from IBM. With money that Jessie had inherited, the couple moved to Bexley in 1953, where they began attending Bexley Gospel Hall.<ref name=Wilson67>Wilson (2000), p. 67.</ref> Towards the end of his life, Jenner developed [[dementia]] and his narcolepsy worsened. For six months, he was confined to a bed and was treated with [[amphetamine]]. He was then diagnosed with [[colorectal cancer]] and spent a subsequent ten days at Calvary Hospital, [[Kogarah, New South Wales|Kogarah]], [[New South Wales]], where he died at 11:45&nbsp;pm on 8 May 1977 at the age of 73.<ref name=Wilson68>Wilson (2000), p. 68.</ref> Because he had befriended so many police officers towards the end of his life, his body was given a [[police escort]] to the burial,<ref>Wilkinson (2013), 38:09.</ref> which took place at Woronora Lawn Cemetery. His wife died two years later.<ref>Wilson (2000), p. 69.</ref>

==Legacy==
While Jenner was alive, very few people knew of him, and the effects of his evangelism were largely unrecognised.<ref name=Wilson84>Wilson (2000), p. 84.</ref> After his death, however, stories about his evangelism circulated widely. Stories of his evangelistic activities generated a largely [[oral tradition]], and elements of some stories contradicted others.<ref name=Wilson9>Wilson (2000), p. 9.</ref> Many storytellers said that Jenner was small in stature and that he had white hair; this description is contradicted by interviews with family members.<ref name=Wilson74>Wilson (2000), p. 74.</ref>

[[File:5.5.07RayComfortByLuigiNovi.jpg|thumb|left|alt=Ray Comfort at Calvary Baptist Church in Manhattan in 2007|[[Ray Comfort]] ''(pictured)'' wrote an account of Jenner's evangelism in which Jenner is called "Mr. Genor", and [[Ché Ahn]] repeated this account in the 2006 book ''Spirit-led Evangelism''.]]

In 2000, Raymond Wilson published a book called ''Jenner of George Street: Sydney's Soul-Winning Sailor'' in an attempt to tell the story of Jenner's life accurately. Jenner's family had been finding it painful to have alternate accounts of Jenner's life circulating around the world, so they gave Wilson all the information he desired. Wilson wrote that Jenner was "eccentric... the very antithesis of the 'wise', 'mighty', and 'noble'," but that his life was therefore a good demonstration of [[2 Corinthians 12]]:9,<ref name=Wilson10/> which states that God's "power is made perfect in weakness."{{bibleref2c|2Corinthians|12:9|NIV|2Cor.12:9}} Wilson wrote that Jenner's question of "heaven or hell" was very similar to that of [[Arthur Stace]], another Australian street evangelist who wrote the word "Eternity" on the sidewalks so people would consider where they would be in eternity.<ref>Wilson (2000), p. 57.</ref> Wilson called Jenner a battler and did not recommend that his readers emulate Jenner's evangelistic activities "unless Divinely fitted in a similar way."<ref name=Wilson84/> Wilson wrote that he "travelled and corresponded widely to ascertain the facts of the story" and that he personally verified the accuracy of the information by retrieving first-hand accounts from all of the major figures in Jenner's life.<ref name=Wilson9/> The people Wilson interviewed included Nancy Dixon; Ann and Angus Carruthers, Jenner's daughter and son-in-law; Murray Wilkes; Ian Boyden; Tas McCarthy; Peter Culver; Noel Stanton; and Mary Stares.<ref name=Wilson10>Wilson (2000), p. 10.</ref>

Nonetheless, conflicting accounts of Jenner's life continued to propagate at least as late as 2006. In some accounts of Jenner's evangelism, Jenner is referred to as "Mr. Genor".<ref>Ahn (2006), p. 229.</ref> One such account was recorded by [[Ray Comfort]] on the Living Waters website and then repeated in the 2006 book ''Spirit-led Evangelism: Reaching the Lost through Love and Power'' by [[Ché Ahn]].<ref>Ahn (2006), p. 226.</ref> Ahn is one of the storytellers who refers to Jenner as a "little white-haired man",<ref>Ahn (2006), p. 228.</ref> and Ahn concludes his story by writing that Jenner died two weeks after encountering Dixon, who is not named.<ref>Ahn (2006), p. 230.</ref> These details contradict the information provided by Wilson, who writes in his biography that Jenner died more than twenty years after Dixon told him about the people who had become Christians as a result of his evangelism.<ref name=Wilson68/>

In 2013, Gary Wilkinson produced and directed ''The Frank Jenner Question'', a [[documentary film]] featuring interviews with Jenner's daughter and people who had become Christians because of Jenner's evangelism.<ref>Wilkinson (2013).</ref> Claire Goodwin encouraged people to emulate Jenner by including an account of his evangelism in her 2013 book ''Compelled to Tell: A Fascinating Journey from a New York Dead-End Street to a Lifetime of Ministry and Soul-Winning''.<ref>Goodwin (2013), p. 256.</ref>

==Notes==
{{notelist}}

==References==
{{Reflist|20em}}

==Bibliography==
* {{Cite book|title=Spirit-led Evangelism: Reaching the Lost through Love and Power|author=[[Ché Ahn|Ahn, Ché]]|year=2006|publisher=[[Baker Publishing Group#Chosen|Chosen]]|location=[[Grand Rapids, Michigan]]|isbn=1441207074}}
* {{Cite book|title=Fire in Our Hearts: The Story of the Jesus Fellowship/Jesus Army|author=Cooper, Simon|author2=Mike Farrant|publisher=Multiply Publications|location=[[Northampton]], England|year=1997|isbn=1900878054}}
* {{Cite book|title=The Gospel According to Mark|author=Edwards, James R.|year=2002|publisher=[[William B. Eerdmans Publishing Company]]|location=[[Grand Rapids, Michigan]]|isbn=0851117783}}
* {{Cite book|title=Compelled to Tell: A Fascinating Journey from a New York Dead-End Street to a Lifetime of Ministry and Soul-Winning|author=Goodwin, Claire H.|publisher=[[Thomas Nelson (publisher)|Thomas Nelson]]|location=[[Bloomington, Indiana]]|year=2013|isbn=1490805389}}
*{{Cite book|title=Brunner & Suddarth's Textbook of Canadian Medical-Surgical Nursing|author=Paul, Pauline|author2=Beverly Williams|year=2009|publisher=[[Lippincott Williams & Wilkins]]|location=[[Philadelphia]], Pennsylvania|isbn=0781799899}}
* {{Cite AV media|title=The Frank Jenner Question|people=Wilkinson, Gary (Director)|year=2013|medium=Motion picture|location=Australia|publisher=Gary Wilkinson Productions}}
* {{Cite book|title=Jenner of George Street: Sydney’s Soul-Winning Sailor|author=Wilson, Raymond|year=2000|publisher=Southwood Press|location=[[Hurstville, New South Wales]]|isbn=0646408305}}

{{DEFAULTSORT:Jenner, Frank}}
[[Category:Evangelists]]
[[Category:English emigrants to Australia]]
[[Category:People from Southampton]]
[[Category:1903 births]]
[[Category:1977 deaths]]
[[Category:English evangelicals]]
[[Category:Converts to evangelical Christianity]]
[[Category:Converts to Protestantism from atheism or agnosticism]]
[[Category:Royal Australian Navy sailors]]
[[Category:United States Navy sailors]]
[[Category:People with Parkinson's disease]]
[[Category:Australian evangelicals]]
[[Category:Australian gamblers]]
[[Category:English gamblers]]
[[Category:British people of World War I]]
[[Category:People with narcolepsy]]
[[Category:Royal Navy sailors]]
[[Category:Deaths from colorectal cancer]]
[[Category:British Plymouth Brethren]]
[[Category:Janitors]]
[[Category:Royal Australian Navy personnel of World War II]]
[[Category:IBM employees]]
[[Category:Australian Plymouth Brethren]]