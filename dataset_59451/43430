<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=P.V.9
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Floatplane fighter
 | national origin=[[United Kingdom]]
 | manufacturer=[[Port Victoria Marine Experimental Aircraft Depot|RNAS Marine Experimental Aircraft Depot]]
 | designer=
 | first flight=December 1917
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Port Victoria P.V.9''' was a [[United Kingdom|British]] single-seat [[biplane]] [[floatplane]] fighter of the [[First World War]].  Although claimed to be the best aircraft of its type yet to be tested, only a single prototype was built.

==Design and development==

In mid-1917, the [[Port Victoria Marine Experimental Aircraft Depot|RNAS Marine Experimental Aircraft Depot]] at Port Victoria on the [[Isle of Grain]] was instructed to build a new single-seat floatplane fighter as a possible replacement for the [[Royal Naval Air Service]] (RNAS)'s [[Sopwith Baby]]s. The new aircraft was to combine the good manoeuvrability and pilot view of Port Victoria's earlier [[Port Victoria P.V.2|P.V.2]] floatplane with superior speed.<ref name="Collyer p53">Collyer 1991, p. 53.</ref><ref name="Mason fighter p122">Mason 1992, p. 122.</ref>

Like the P.V.2, the new design, the Port Victoria P.V.9 was a single-engined sesquiplane (i.e. a [[biplane]] with its lower wing much smaller than its upper wing) braced with faired steel tubes. The fuselage, wider than that of the P.V.2, was mounted between the upper and lower wings, almost filling the inter-wing gap, giving an excellent view for the pilot. Armament was a [[Vickers machine gun]] [[synchronization gear|synchronised]] to fire through the propeller disc, with a [[Lewis gun]] mounted above the upper wing firing over the propeller. Power was provided by a [[Bentley BR1]] [[rotary engine]]. While the designers had hoped to use the same high-lift [[aerofoil]] section as used in the P.V.2, this was rejected by the [[Admiralty]], who demanded the use of the more conventional RAF 15 aerofoil, which resulted in a larger aircraft with a reduced climb rate and ceiling.<ref name="Mason fighter p122"/><ref name="Collyer p53-4">Collyer 1991, pp. 53–54.</ref>

==Operational history==

The P.V.9 made its maiden flight in December 1917, but trials were delayed by engine troubles and by a collision of the aircraft with a barge, which resulted in a propeller not matched properly to the aircraft being fitted, further reducing performance. Despite this, when the P.V.9 was officially tested in May 1918, the P.V.9 was said to be the best seaplane fighter tested up to that time.<ref name="Collyer p53-4"/><ref name="Bruce British p341">Bruce 1957, p. 341.</ref> No production followed, however, as the availability of [[Sopwith Pup]] and [[Sopwith Camel|Camel]] landplanes which could operate from platforms aboard ships, removed the requirement for a floatplane fighter.<ref name="Mason fighter p122"/>

==Specifications==
{{Aircraft specs
|ref=British Aeroplanes 1914–18<ref name="Bruce British p341"/>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length ft=25
|length in=2
|upper span m=
|upper span ft=30
|upper span in=11
|lower span ft=20
|lower span in=1
|lower span note=
|height m=
|height ft=9
|height in=0
|height note=
|wing area sqm=
|wing area sqft=227
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=RAF 15
|empty weight kg=
|empty weight lb=1404
|empty weight note=
|gross weight kg=
|gross weight lb=1965
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=34.5 Imp Gallons
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Bentley BR1]]
|eng1 type=9-cylinder air-cooled [[rotary engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=150<!-- prop engines -->
|eng1 note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=110.5
|max speed kts=
|max speed note=at 2,000 ft (610 m)
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=

|range km=
|range miles=
|range nmi=
|range note=
|endurance=2.5 hours<!-- if range unknown -->
|ceiling m=
|ceiling ft=11500
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=3 min 10 s to 2,000 ft (610 m), 27 min 20 s to 10,000 ft (3,050 m0

|more performance=
<!--
        Armament
-->
|guns= 1× [[.303 British|.303 in]] [[Vickers machine gun]] and 1× [[Lewis gun]] above upper wing 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=*[[Port Victoria P.V.2]]
|similar aircraft=*[[Westland N.1B]]
*[[Supermarine Baby]]
<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*Bruce, J.M. ''British Aeroplanes 1914–18''. London:Putnam, 1957.
*Collyer, David. "Babies Kittens and Griffons". ''[[Air Enthusiast]]'', Number 43, 1991. Stamford, UK:Key Publishing. {{ISSN|0143-5450}}. pp.&nbsp;50–55.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland:Naval Institute Press, 1992. ISBN 1-55750-082-7.
{{refend}}
<!-- ==Further reading== -->

==External links==
*[http://www.airwar.ru/enc/fww1/pv9.html Port Victoria PV.9] (in Russian)

<!-- Navboxes go here -->

{{Port Victoria Aircraft}}

[[Category:Floatplanes]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Port Victoria aircraft|PV9]]
[[Category:Sesquiplanes]]
[[Category:Single-engine aircraft]]
[[Category:Rotary-engined aircraft]]