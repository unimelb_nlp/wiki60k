{{Infobox person
| name        = Moye W. Stephens
| image       = 
| image_size  = 225px
| caption     = Moye Stephens in cockpit of The Flying Carpet. [[Richard Halliburton]] standing.
| birth_date  = 1906
| birth_place = [[Los Angeles]]
| nationality = [[United States|American]]
| occupation  = [[Aviator]], businessman
| death_date  = 1995
}}
'''Moye Wicks Stephens''' (1907–1995) was an [[United States|American]] aviator and businessman. He was a [[aviation history|pioneer in aviation]], circumnavigating the globe with adventure writer [[Richard Halliburton]] in 1931, and co-founding [[Northrop Corporation|Northrop]] Aircraft, Inc.

==Family==
His father was also named Moye Wicks Stephens; his mother was Mary Hendrick Stephens. His grandparents, Judge and Mrs. Albert Moye Stephens, had a ranch in the mountains near [[Pomona, California]]. He married Contessa Gadina de Turiani, an aviator from [[Trieste, Italy]], who had divorced millionaire aviator Ross Hadley. In later years Stephens lived in [[Ensenada, Baja California|Ensenada]], [[Baja California]], [[Mexico]]; he died in [[Calistoga, California]] in 1995.

==Friends and acquaintances==
Stephens knew [[barnstorming|barnstormers]] and [[World War I]] [[flying ace]]s, pioneers of [[aviation history|early aviation]]. These were people such as [[Frank Howard Clark|Frank Clarke]], [[Pancho Barnes]], Sandy Sandblom, Leo Nomis, Bud Creeth, Eddie Bellande, Ross Hadley,  and Stephens's lifelong best friend, Dick Rinaldi. He knew movie stars such as [[Richard Arlen]], [[Ramon Novarro]], [[Sue Carol]], [[Reginald Denny (actor)|Reginald Denny]], [[Wallace Beery]], and [[Dolores del Río]]. He also knew movie executives and directors such as [[Cecil B. DeMille]], [[Victor Fleming]], [[Howard Hawks]],and [[Howard Hughes]].

==Early life==

===First encounter with planes===
Not quite four, he saw his first planes in 1910 at the Los Angeles International Air Meet <ref>{{cite web|url=http://www.csudh.edu/1910airmeet/ |title=1910 International Air Meet - Home Page |publisher=Csudh.edu |date=2012-04-16 |accessdate=2012-05-12}}</ref><ref>{{cite web|url=http://www.aerofiles.com/dominguez.html |title=Dominguez Air Meet |publisher=Aerofiles.com |date=1910-01-10 |accessdate=2012-05-12}}</ref><ref>[http://www.centennialofflight.gov/essay/Explorers_Record_Setters_and_Daredevils/Early_US_shows/EX4.htm The First U.S. Airshows-the American Air Meets of 1910<!-- Bot generated title -->]</ref> at Dominguez Field, in today's Dominguez Hills, California, on the southern outskirts of Los Angeles, and the boy became enamored of aeroplanes. His most vivid image was of a French [[Blériot Aéronautique|Blériot]] <ref>[http://www.centennialofflight.gov/essay/Explorers_Record_Setters_and_Daredevils/Bleriort_1909/EX1.htm Louis Bleriot's Record-setting Flight Across the English Channel<!-- Bot generated title -->]</ref> flown by [[Louis Paulhan]] into the Southern California sky; in 1910 it was the only monoplane to have successfully flown, including the first flight across the English Channel.

===Flying lessons===
After classes at Hollywood High, Stephens rushed to Rogers Airport <ref name="airfields-freeman.com">[http://www.airfields-freeman.com/CA/Airfields_CA_LA_W.htm] [[Abandoned & Little-Known Airfields]]: California - West Los Angeles Area<!-- Bot generated title --></ref> at the corner of Wilshire Boulevard and Fairfax Avenue to look at the biplanes and to talk with the pilots. (Today, not a trace of the airport remains.) As months passed, the men came to like the lad, and taught him what they knew, even taking him on short hops. Eventually, Jim Webster, airport manager, agreed—with parental permission—to give him flying lessons in exchange for work around the hangars. In his lessons he was a quick learner, and after logging more cockpit time he finally soloed. This, of course, was an era before the [[Federal Aviation Administration]], with its requisite ground courses and instruction hours. When a pilot first took him aloft, he saw a Los Angeles unrecognizable today. He looked down on a Wilshire Boulevard surrounded by countryside, bordered by "a row of stunted palms." He sometimes flew out of Mines Field,<ref>[http://www.laalmanac.com/history/hi01f.htm Los Angeles County - 1910 to 1929<!-- Bot generated title -->]</ref> one day to become [[Los Angeles International Airport]].

In 1923, he flew for the first time in an [[OX-5]]-powered [[Standard J-1]] from Rogers Airport  <ref name="airfields-freeman.com"/> at the corner of Wilshire Boulevard and Fairfax Avenue. That same year he persuaded the manager to allow him flying lessons in exchange for work.  As a sixteen-year-old at Hollywood High in Los Angeles, he was taught to fly by pilots such as [[Leo Nomis]].<ref name="Leo Nomis">[http://www.imdb.com/name/nm0634567/ Leo Nomis<!-- Bot generated title -->]</ref>

===First plane purchased===
His first plane was a [[Thomas-Morse S-4|Thomas-Morse S-4C Scout]],<ref name="Thomas-Morse Scout - USA">[http://www.aviation-history.com/thomas/scout.html Thomas-Morse Scout - USA<!-- Bot generated title -->]</ref><ref name="Aircraft: Thomas-Morse S.4C Scout">[http://aeroweb.brooklyn.cuny.edu/specs/tmorse/s_4c.htm Aircraft: Thomas-Morse S.4C Scout<!-- Bot generated title -->]</ref> A single-seat biplane with fabric-covered wooden airframe it had been an advanced trainer for World War I pilots. Its engine was quite common at the time, while being most unusual today - a rotary Le [[Rhône]] <ref>[http://www.aviation-history.com/engines/rotary.htm Le Rhone Rotary Engine - France<!-- Bot generated title -->]</ref> Rotary,<ref>[http://www.keveney.com/gnome.html Animated Engines, Gnome<!-- Bot generated title -->]</ref> in which the engine crankcase with cylinders rotate and drive the propeller, while the crankshaft is stationary, being attached to the firewall. Moye paid movie stunt pilot Leo Nomis <ref name="Leo Nomis"/> $450 for the Scout after his father relented and allowed the boy to buy the plane. (In 2007 a "Tommie" was on sale for $125,000.) Stephens' plane weighed 961 pounds empty and had no throttle. It did have two power-control levers, or manettes, in its place, which served in lieu of carburetor, one controlling air intake, the other, fuel. Engine speed was controlled by a so-called blipper, a switch. To manage speed as he taxied for take off, the pilot cut ignition with the blipper switch, then let it start again. With this plane Moye really learned how to fly.

In 1926, he bought his first airplane for $450, a war surplus Thomas-Morse S-4C Scout<ref name="Thomas-Morse Scout - USA"/><ref name="Aircraft: Thomas-Morse S.4C Scout"/> from [[Leo Nomis]],<ref name="Leo Nomis"/> a leading motion picture stunt pilot. In 2007, a "Tommie" was listed for sale at $125,000.

Link to a picture of a Thomas-Morse S-4 Scout [http://www.aviation-history.com/thomas/scout-1.jpg]

===Education===
He graduated Hollywood High School at age 17. His parents kept him from college for a year. He attended [[Stanford University]], Palo Alto, California, graduating in 1928 with an AB in law, then entering Stanford graduate school for a JD so he could one day practice law in his father's firm, mainly to satisfy his father.

In 1924 he began study at [[Stanford University]], Palo Alto, California, and majored in law.  His father, an attorney, had plans for his son to join his law firm. That summer Stephens returned to his flying lessons at Rogers Airport. In the fall he resumed his studies at Stanford.

===Early work===
During his summer vacations, he soon started flying in movies as a paid stunt pilot.  This included work in [[Cecil B. DeMille]]'s <ref name="Cecil B. DeMille's Biography">[http://www.cecilbdemille.com/bio.html Cecil B. DeMille's Biography<!-- Bot generated title -->]</ref> film, [[Corporal Kate]] <ref>[http://www.imdb.com/title/tt0016743/ Corporal Kate (1926)<!-- Bot generated title -->]</ref> and in [[Howard Hughes]]' <ref name="Howard Hughes">[http://www.famoustexans.com/howardhughes.htm Howard Hughes<!-- Bot generated title -->]</ref>' [[Hell's Angels (film)|Hell's Angels]],<ref name="Hell's Angels 1930">[http://www.imdb.com/title/tt0020960/ Hell's Angels (1930)<!-- Bot generated title -->]</ref> famous today for its harrowing World War I aerial combat scenes.

In summer 1928 he taught the donor of the Los Angeles [[La Brea Tar Pits]], [[Allan Hancock|Captain G. Allan Hancock]],<ref>[http://familytreemaker.genealogy.com/users/s/c/o/Joan-M-Scott/PHOTO/0026photo.html Family Tree Maker's Genealogy Site: Photo<!-- Bot generated title -->]</ref> to fly in an [[OX-5]]-powered [[Travel Air]] 2000. Hancock backed the [[Kingsford-Smith]] Southern Cross Pacific flight.<ref>[http://www.aviation-history.com/airmen/smith.htm Kingsford Smith - Trans-Pacific Flight of the Southern Cross<!-- Bot generated title -->]</ref>

Jack [[Northrop Corporation|Northrop]], Jerry [[Vultee]], and [[Cliff Garrett]], each eventually founding, respectively, [[Northrop Corporation|Northrop]] Aviation, [[Vultee Aircraft]], and [[Garrett AiResearch]], were given their initial flying instruction by Moye Stephens in the same OX-5 Travel Air.

[[Moye Stephens]] was a stunt pilot for Hollywood movies produced by [[Howard Hughes]] <ref name="Howard Hughes"/> and [[Cecil B. DeMille]].<ref name="Cecil B. DeMille's Biography"/> He gave Hughes flying lessons, and knew [[Victor Fleming]],<ref>[http://www.hollywood.com/celebrity/Victor_Fleming/197651 Victor Fleming at Hollywood.com<!-- Bot generated title -->]</ref> [[Gone with the Wind (film)|Gone With The Wind]] director, as well as various movie stars.

In 1926 with J.M. Hiatt, a Stanford classmate, he wrote stories, "The Assault Upon Miracle Castle", and "Ghosts of the Air",<ref>[http://zzmaster.best.vwh.net/HF/weird_tales_ha.html Weird Tales: Fiction Index, Ha<!-- Bot generated title -->]</ref> both published in ''[[Weird Tales]]'', a magazine still in existence.  He also co-authored science-fiction for [[Weird Tales]], gave flying lessons to [[Howard Hughes]] and flew as a stunt pilot in Hughes' silent movie extravaganza, [[Hell's Angels (film)|Hell's Angels]].<ref name="Hell's Angels 1930"/>

===Maddux===

In 1928 he left Stanford just when beginning law school as he was hired as captain for [[Maddux Airlines]] to fly Ford Tri-Motors, although he had no multi-engine experience. He had no co-pilot at Maddux. Instead the right hand seat was occupied by a mechanic with the title "mate." For aircraft checkout, he rode shotgun on a trip to San Diego, and a trip to Alameda, California. He handled the controls while in the air. He was checked-out for take off and landing by doing a quick circuit of [[Grand Central Airport (United States)|Grand Central Air Terminal]] <ref>[http://www.airfields-freeman.com/CA/Airfields_CA_LA_C.htm] [[Abandoned & Little-Known Airfields]]: California, Central Los Angeles area<!-- Bot generated title --></ref> in [[Glendale, California]], with the pilot observing from the right-hand seat. Then the pilot got out and Stephens did three quick solo circuits. The pilot had not stayed to observe.

In June 1929 he resigned from Maddux to join [[Transcontinental Air Transport]] (TAT) which eventually became [[Trans World Airlines]] (TWA). TAT was the first United States transcontinental passenger airline.
At TAT he flew [[Ford Tri-Motor]]s.

In 1929 he was elected to a term as president of the [[Professional Pilots Association]] [http://www.propilots.org/]. In the same year he became one of the founders of the Los Angeles Hangar of the [[Quiet Birdmen]].{{citation needed|date=October 2012}}

==The Flying Carpet expedition==

A few years after [[Charles Lindbergh]]'s [[New York City]] to [[Paris]] flight, Stephens flew an open cockpit biplane around the world. He was approached by travel-adventurist [[Richard Halliburton]], who proposed up to two years away, visiting well-known and less known places.  Stephens chose a [[Stearman C3|Stearman C-3B]] <ref name="travel.webshots.com">[http://travel.webshots.com/photo/2775081500046209414JLpjBi Stearman C-3B - The Museum of Flight pictures from washington photos on webshots<!-- Bot generated title -->]</ref> with Halliburton, taking eighteen months, and reaching places such as [[Timbuktu]] in Africa, [[Mount Everest]] in the Himalayas, The [[Taj Mahal]] in India, [[Petra]] in Jordan, [[Singapore]] in Southeast Asia, and [[Sarawak]] in Borneo.

The two men simply shook hands. Moye had no pay, but unlimited expenses.[http://www.opencockpit.net/moye.html]  When Crown Prince [[Ghazi of Iraq]] was a boy, Stephens and Halliburton flew the lad over a school yard so class mates could see the Prince in an open-cockpit airplane. They performed acrobatics for the Maharajah of Nepal. They were feted by [[Sylvia Brett]], wife of the  [[White Rajah]]<ref name="The Rajah's Return - TIME">{{cite news| url=http://www.time.com/time/magazine/article/0,9171,830349-2,00.html | work=Time | title=Sarawak: The Rajah's Return | date=1963-05-17}}</ref> of Sarawak.

In a 23 January 1932 letter to his parents, Halliburton wrote, "Moye continues to be the world's best pilot. Once we are in the air, no matter where, everything goes like clock-work." <ref>[http://www.antiqbook.com/boox/grn/013983.shtml Richard Halliburton: His Story Of His Life'S Adventures - Halliburton, Richard<!-- Bot generated title -->]</ref>

For Halliburton, he piloted a Stearman C-3B biplane,<ref name="travel.webshots.com"/> The [[Flying Carpet]], and Halliburton wrote a book of the same name, published in 1932, which became a best seller. They flew to [[French Foreign Legion]] outposts, and across the [[Sahara]] to [[Timbuktu]]. Their flight took them to Asia, including India, Persia, Malaysia, and other countries. In Iran, they met young German aviator [[Elly Beinhorn]], famous in her day, who had also flown to Timbuktu in a [[Klemm]], although her plane had been forced down because of mechanical failure.

In 1931, [[Richard Halliburton]], a famous travel-adventure writer of the time, asked Stephens to pilot and mechanic for him in an around the world flight. The purpose was to gather material for Halliburton's next book, ''[[The Flying Carpet]]'', which became a best seller. The trip in a biplane called The Flying Carpet, a Stearman C-3B, took eighteen months, covered 33,660 miles, visited 34 countries, and included France, the Sahara, Persia, Singapore, and the Philippines. In that global flight, Stephens performed aerobatics for the first air meet in Oran, Algeria, aerobatics for the first air meet in Fez, Morocco, rescued [[Elly Beinhorn]] a famous German aviator, and flew aerobatics for the Maharajah of Nepal.

Among the highlights of his trip was the first aerial photograph of [[Mount Everest]]. He and Halliburton were the first Americans to fly to the Philippines. He flew Crown Prince [[Ghazi of Iraq]]. In Persia, Princess [[Mahin Banu]] <ref>[http://www.4dw.net/royalark/Persia/qajar29.htm qajar29<!-- Bot generated title -->]</ref> climbed into the front cockpit for a ride with him. In Borneo, he took [[Sylvia Brett]],<ref name="The Rajah's Return - TIME"/> known as Ranee Sylvia of Sarawak, for a ride, the first woman to fly in that country. At the Rajang River there, he took the chief of the [[Dayak people|Dyak]] head-hunters for a flight.

==Northrop Corporation==
In the ''Northrop News'', March 27, 1981, Stephens is described as "one of the founders of [[Northrop Corporation]]." Stephens said, "Tom Ellsworth was talking to my brother-in-law Wesley LeFevre. He said, 'It's a shame for that fellow Northrop to be working for other people. He should head his own company.'" In 1939, Stephens used his lawyerly skills to scout the countryside for a site to erect [[Northrop Corporation|Northrop]] Aviation's first buildings. He was one of the men who brought [[John Knudsen Northrop|John K. "Jack" Northrop]]<ref>[http://www.britannica.com/eb/article-9056268/John-Knudsen-Northrop John Knudsen Northrop - Britannica Online Encyclopedia<!-- Bot generated title -->]</ref> together with Gage H. Irving <ref>[http://www.dmairfield.com/airplanes/NC11Y/index.html The NORTHROP ALPHA 2 NC11Y Page of the Davis-Monthan Airfield Register Website<!-- Bot generated title -->]</ref> and La Motte Cohu.<ref>[http://www.aliciapatterson.org/APF1103/Biddle/Biddle.html How America Eagerly Built Her Arsenal<!-- Bot generated title -->]</ref> Stephens was appointed to the board as secretary and was chief of the flight division. As chief test pilot, he flew the first [[flying wing]],<ref>[http://www.americanheritage.com/articles/magazine/it/1994/3/1994_3_54.shtml AmericanHeritage.com / THE DREAM of THE FLYING WING<!-- Bot generated title -->]</ref> the [[Northrop N-1M]], prototype of today's [[B-2 Spirit|B-2]] bomber.

In 1939 he was instrumental in the promotion and organization of Northrop Aircraft, Inc., in consideration of which he was awarded stock interest in the company and was made Assistant Corporate Secretary. Following the departure of the original secretary, he was moved into that position by the board of electors.

The [[Northrop N-1M]] [http://www.americanheritage.com/articles/magazine/it/1994/3/1994_3_54.shtml], was the first aircraft Northrop produced. The "M" stood for "mock-up", as the craft was intended to explore the controllability and stability of the all-wing aircraft concept. Stephens did most of the experimental test flying in the two-year test program. The ship was vastly overweight, severely underpowered, and was plagued with constant engine problems. Still, it fulfilled its design purpose. The test program produced the general configuration for the subsequent Northrop flying wings.

Stephens also tested other experimental Northrop aircraft: the N-3PB float plane patrol bomber, the [[A-31 Vengeance]] dive bomber, and the [[P-61 Black Widow]] night fighter.

In the 1920s he had given Jack Northrop flying lessons. (He also taught Jerry [[Vultee]], of Vultee Aircraft, maker of jets such as the [[F-102]], [[F-106]] and [[B-58 Hustler|B-58]].)

==Further career and recognition==
In 1932, shortly after the return of the ''Flying Carpet'', he was invited to become an inaugural member of the Los Angeles County Sheriff's Aero Squadron.  In 1937 he became a founder and board member of the Aviation Country Club of California, Inc. That same year he took leave of absence from Pacific Aircraft Sales, and enjoyed his honeymoon on a lengthy business trip: [[Lockheed Corporation|Lockheed]] sent him to [[New Zealand]] and [[Australia]] to promote sales and to check out the pilots of [[Union Airways]] and [[Ansett Airways]] in their new [[Lockheed Model 10 Electra]] airplanes. In 1942, he was presented an award of Distinguished Service by the [[Los Angeles Chamber of Commerce]] in recognition of this work.

At the close of [[World War II]], Stephens and Ted Coleman, Northrop sales manager, left Northrop to start an [[air cargo]] business in the interior of [[Brazil]]. They obtained a franchise from the Brazilian government, but the climate severely affected his young son's health so Stephens returned to the United States.

In 1948, [[John K. Northrop]] awarded him a lifetime membership in the Flying Wing Club as "one of a small group of distinguished pilots who have participated in the historical development and pioneered in the public acceptance of this revolutionary and highly efficient type of aircraft".

In 1982, he was made a member of the [[OX-5]] Aviation Pioneers. In 1989, he was elected to the OX-5 Aviation Pioneers [[Hall of Fame]]; he was chosen to join [[Charles Lindbergh]], [[Howard Hughes]], [[Jimmy Doolittle]] and some 50 other "carefully selected...eminent individuals in the aerospace field"<ref>[http://www.historynet.com/exploration/adventurers/3025941.html?page=3&c=y TheHistoryNet | Adventurers & Trail Blazers | Moye Stephens: Aviation Pioneer and Adventurer<!-- Bot generated title -->]</ref> "Moye Stephens: Aviation Pioneer and Adventurer" by Ronald Gilliam, originally published in the July 1999 issue of ''Aviation History''. In 1983, he was elected an Honorary Fellow of The [[Society of Experimental Test Pilots]] in the [[San Diego Aerospace Museum]]. In 1990, his interview as a pioneer aviator appeared on a [[Public Broadcasting Service|PBS]] television series, ''[[The American Experience]]'',<ref>[http://www.pbs.org/wgbh/amex/lindbergh/filmmore/transcript/transcript1.html The American Experience | Lindbergh | Enhanced Transcript<!-- Bot generated title -->]</ref> on the life of [[Charles Lindbergh]], produced by [[Ken Burns]].

==References==
{{reflist|2}}

==Further reading==
*Alt, John H. ''Don't Die in Bed: The Brief, Intense Life of Richard Halliburton''.  Atlanta: Quincunx Press, 2013. Chapters on Moye Stephens from his private papers.
*Gilliam, Ronald, "Moye Stephens Piloted More Than 100 Types of Aircraft and Flew Around the World in the Flying Carpet."  Aviation History, vol. 9, issue 6 (July 1999).
*Max, Gerry, ''Horizon Chasers - The Lives and Adventures of Richard Halliburton and Paul Mooney''.  Jefferson, NC: McFarland Publishers, 2007.
*Root, Jonathan, ''Halliburton - The Magnificent Myth''.  New York: Coward-McCann, 1965.
*Schultz, Barbara H., ''Flying Carpets, Flying Wings - The Biography of Moye Stephens'' (PlaneMercantile, c2011)
*Taylor, William R., ''A Shooting Star Meets the Well of Death, Why and How Richard Halliburton Conquered the World.'' Moonshine Cove Publishing, 2013

==External links==
* [http://www.historynet.com/air_sea/aviation_history/3025941.html History Net] (About Moye Stephens)

{{Authority control}}

{{DEFAULTSORT:Stephens, Moye W.}}
[[Category:1907 births]]
[[Category:1995 deaths]]
[[Category:Aviation pioneers]]