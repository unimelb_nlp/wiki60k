{{Use dmy dates|date=November 2012}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:SS Minnesotan.jpg|300px|SS ''Minnesotan'']]
|Ship caption= SS ''Minnesotan''
}}
{{Infobox ship career
|Ship name=SS ''Minnesotan''
|Ship owner=[[American-Hawaiian Steamship Company]]
|Ship registry=Boston
|Ship route=
|Ship ordered=September 1911<ref name=Cochran-358>Cochran and Ginger, p. 358.</ref>
|Ship awarded=
|Ship builder=*[[Maryland Steel]]
*[[Sparrows Point, Maryland]]
|Ship original cost=$668,000<ref name=Cochran-365>Cochran and Ginger, p. 365.</ref>
|Ship yard number=124<ref name=Colton>{{cite web | last = Colton | first = Tim | url = http://www.shipbuildinghistory.com/history/shipyards/1major/inactive/bethsparrowspoint.htm | title = Bethlehem Steel Company, Sparrows Point MD | work = Shipbuildinghistory.com | publisher = The Colton Company | accessdate = 21 August 2008}}
</ref>
|Ship laid down=
|Ship launched=8 June 1912<ref name=CSM-hawaiian>{{cite news | title = Hawaiian ship launched | work = [[The Christian Science Monitor]] | date = 27 January 1913 | page = 13 }}</ref>
|Ship sponsor=Miss Lubelle Shepard<ref name=CSM-hawaiian />
|Ship completed=September 1912<ref name=Colton />
|Ship maiden voyage= 
|Ship identification=U.S. official number: 210534<ref name=Miramar>{{csr|register=MSI|id=2210534|shipname=Minnesotan |accessdate=12 January 2009}}</ref>
|Ship fate=expropriated by U.S. Army, 1 June 1917
|Ship notes=
}}
{{Infobox ship career
|Ship country=United States 
|Ship flag={{USN flag|1918}}
|Ship name=USAT ''Minnesotan''
|Ship acquired=11 September 1917<ref name=DANFS />
|Ship fate=transferred to U.S. Navy, 23 August 1918<ref name=DANFS />
}}
{{Infobox ship career
|Ship country=United States 
|Ship flag={{USN flag|1919}}
|Ship name=USS ''Minnesotan'' (ID-4545)
|Ship acquired=23 August 1918
|Ship commissioned=23 August 1918
|Ship decommissioned=21 August 1919
|Ship struck=
|Ship fate=returned to USSB
}}
{{Infobox ship career
|Ship name=
* 1919–1949: SS ''Minnesotan''
* 1949–1952: SS ''Maria Luisa R.''
|Ship owner=
* 1919–1949: American-Hawaiian Steamship Co.
* 1949–1952: Societa per Azioni<ref name=DANFS />
|Ship registry=
* 1919–1949: {{flagicon|United States}} United States
* 1949–1952: [[File:Civil Ensign of Italy.svg|22x20px|border|Civil Ensign of Italy]] Italy
|Ship fate=Scrapped at [[Bari]], 1952
}}
{{Infobox ship characteristics
|Ship type=[[cargo ship]]
|Ship tonnage={{GRT|6,617}}<ref name=Cochran-365 />
{{DWT|10,175|long}}<ref name=Cochran-365 />
|Ship length=*{{convert|407|ft|7|in|m|abbr=on}} ([[length between perpendiculars|LPP]])<ref name=Miramar />
*{{convert|429|ft|9|in|m|abbr=on}} ([[length overall|overall]])<ref name=DANFS />
|Ship beam={{convert|53|ft|6|in|m|abbr=on}}<ref name=DANFS>{{cite DANFS | author = [[Naval Historical Center]] | title = Minnesotan | url = http://www.history.navy.mil/danfs/m11/minnesotan.htm | short = first }}</ref>
|Ship draft={{convert|28|ft|1|in|m|abbr=on}}<ref name=DANFS />
|Ship depth=
|Ship hold depth={{convert|39|ft|6|in|m|abbr=on}}<ref>{{cite news | title = Minnesotan is launched |work=The Washington Post | date = 9 June 1912 | page = 18 }}</ref>
|Ship propulsion=*oil-fired boilers<ref name=Cochran-357>Cochran and Ginger, p. 357.</ref>
*1 × [[quadruple expansion steam engine|quadruple-expansion]] [[steam engine]]<ref name=Miramar />
*1 × [[screw propeller]]<ref name=Cochran-357 />
|Ship speed={{convert|14.85|knots|km/h}}<ref name=DANFS />
|Ship capacity=Cargo: {{convert|490838|cuft}}<ref name=Cochran-365 />
|Ship crew=18 officers, 40 crewmen
|Ship notes=Sister ships: {{SS|Dakotan||2}}, {{SS|Montanan||2}}, {{SS|Pennsylvanian||2}}, {{SS|Panaman||2}}, {{SS|Washingtonian||2}}, {{SS|Iowan||2}}, {{SS|Ohioan|1914|2}}<ref name=Colton />
}}
{{Infobox ship characteristics
|Header caption=(as USS ''Minnesotan'')
|Ship displacement=
|Ship troops=
|Ship capacity=
|Ship complement=88<ref name=DANFS />
|Ship armament=*1 × {{convert|4|in|mm|adj=on}} gun
*1 × {{convert|3|in|mm|adj=on}} gun<ref name=DANFS />
|Ship notes=
}}
|}
'''SS ''Minnesotan''''' was a [[cargo ship]] built in 1912 for the [[American-Hawaiian Steamship Company]]. During [[World War I]] she was known as '''USAT ''Minnesotan''''' in service for the [[United States Army]] and '''USS ''Minnesotan'' (ID-4545)''' in service for the [[United States Navy]]. She ended her career as the '''SS ''Maria Luisa R.''''' under Italian ownership. She was built by the [[Maryland Steel Company]] as one of eight sister ships for the American-Hawaiian Steamship Company, and was employed in inter-coastal service via the [[Isthmus of Tehuantepec]] and the [[Panama Canal]] after it opened.

In World War I, USAT ''Minnesotan'' carried cargo and animals to France [[ship chartering|under charter]] to the U.S. Army from September 1917. When transferred to the U.S. Navy in August 1918, USS ''Minnesotan'' continued in the same duties, but after the [[Armistice with Germany|Armistice]] she was converted to a [[troop transport]] and returned over 8,000 American troops from France. Returned to American-Hawaiian in 1919, ''Minnesotan'' resumed inter-coastal cargo service, and, at least twice, carried racing yachts from the [[East Coast of the United States|U.S. East Coast]] to California.

During [[World War II]], ''Minnesotan'' was requisitioned by the [[War Shipping Administration]] and initially sailed between New York and [[Caribbean]] ports. In the latter half of 1943, ''Minnesotan'' sailed between Indian Ocean ports. The following year the cargo ship sailed between New York and ports in the United Kingdom, before returning to the Caribbean. In July 1949, American-Hawaiian sold ''Minnesotan'' to Italian owners who renamed her ''Maria Luisa R.''; she was scrapped in 1952 at [[Bari]].

== Design and construction ==
In September 1911, the [[American-Hawaiian Steamship Company]] placed an order with the [[Maryland Steel Company]] of [[Sparrows Point, Maryland]], for four new [[cargo ships]]—''Minnesotan'', {{SS|Dakotan||2}}, {{SS|Montanan||2}}, and {{SS|Pennsylvanian||2}}.<ref group=Note>Maryland Steel had built three ships—{{SS|Kentuckian||2}}, ''Georgian'', and ''Honolulan''—for American-Hawaiian in 1909 in what proved to be a satisfactory arrangement for both companies. See: Cochran and Ginger, p. 358.</ref> The contract cost of the ships was set at the construction cost plus an 8% profit for Maryland Steel, but with a maximum cost of $640,000 per ship. The construction was financed by Maryland Steel with a credit plan that called for a 5% down payment in cash with nine monthly installments for the balance. Provisions of the deal allowed that some of the nine installments could be converted into longer-term notes or mortgages. The final cost of ''Minnesotan'', including financing costs, was $65.65 per [[deadweight tonnage|deadweight ton]], which totaled just under $668,000.<ref name=Cochran-358 />

''Minnesotan'' (Maryland Steel yard no. 124)<ref name=Colton /> was the first ship built under the original contract.<ref group=Note>Further contracts on similar terms were signed in November 1911 and May 1912 to build four additional ships: {{SS|Panaman||2}}, {{SS|Washingtonian||2}}, {{SS|Iowan||2}}, {{SS|Ohioan|1914|2}}. See: Cochran and Ginger, p. 358, and Colton.</ref> She was [[ship naming and launching|launched]] on 8 June 1912,<ref name=CSM-hawaiian /> and delivered to American-Hawaiian in September.<ref name=Colton /> ''Minnesotan'' was {{GRT|6,617|disp=long}},<ref name=Cochran-365 /> and was {{convert|428|ft|9|in|m}} in length and {{convert|53|ft|7|in|m}} [[beam (nautical)|abeam]].<ref name=DANFS /> She had a [[deadweight tonnage]] of {{DWT|10,175|long}}, and her cargo holds had a storage capacity of {{convert|490838|cuft}}.<ref name=Cochran-365 /> ''Minnesotan'' had a  speed of {{convert|15|knots|km/h}}, and was powered by a single [[quadruple expansion steam engine|quadruple-expansion]] [[steam engine]] with oil-fired [[boiler]]s, that drove a single [[screw propeller]].<!-- speed --><ref name=DANFS /><ref name="Cochran-357"/><!--oil-fired, single screw -->

== Early career ==
When ''Minnesotan'' began sailing for American-Hawaiian, the company shipped cargo from [[East Coast of the United States|East Coast]] ports via the Tehuantepec Route to [[West Coast of the United States|West Coast]] ports and Hawaii, and vice versa. Shipments on the Tehuantepec Route would arrive at Mexican ports—[[Salina Cruz|Salina Cruz, Oaxaca]], for eastbound cargo, and [[Coatzacoalcos]], Veracruz, for westbound cargo—and would traverse the [[Isthmus of Tehuantepec]] on the [[Tehuantepec National Railway]].<ref>Hovey, p. 78.</ref> Eastbound shipments were primarily sugar and pineapple from Hawaii, while westbound cargoes were more general in nature.<ref name=Cochran-355-56>Cochran and Ginger, pp. 355–56.</ref> ''Minnesotan'' sailed in this service on the east side of North America.<ref>{{cite news | title = American-Hawaiian Steamship Co. | type = display ad |work=Los Angeles Times | date = 13 April 1914 | page = I-4 }}</ref><ref name=WSJ-american>{{cite news | title = American-Hawaiian new steamships |work=The Wall Street Journal | date = 6 May 1912 | page = 6 }}</ref>

After the [[United States occupation of Veracruz]] on 21 April 1914 (which found six American-Hawaiian ships in Mexican ports), the [[Victoriano Huerta|Huerta]]-led Mexican government closed the Tehuantepec National Railway to American shipping. This loss of access, coupled with the fact that the [[Panama Canal]] was not yet open, caused American-Hawaii to return in late April to its historic route of sailing around South America via the [[Straits of Magellan]].<ref name=Cochran-360>Cochran and Ginger, p. 360.</ref> With the opening of the Panama Canal on 15 August, American-Hawaiian ships switched to taking that route.<ref name=Cochran-360 />

In October 1915, landslides closed the Panama Canal and all American-Hawaiian ships, including ''Minnesotan'', returned to the Straits of Magellan route again.<ref name=Cochran-361>Cochran and Ginger, p. 361.</ref> ''Minnesotan''{{'}}s exact movements from this time through early 1917 are unclear. She may have been in the half of the American-Hawaiian fleet that was [[ship chartering|chartered]] for transatlantic service. She may also have been in the group of American-Hawaiian ships chartered for service to South America, delivering coal, gasoline, and steel in exchange for coffee, [[nitrate]]s, cocoa, rubber, and [[manganese|manganese ore]].<ref name=Cochran-362>Cochran and Ginger, p. 362.</ref>

== World War I ==
On 11 September 1917, some five months after the United States declared war on [[German Empire|Germany]], the [[United States Army]] [[ship chartering|chartered]] ''Minnesotan'' for transporting animals to Europe in support of the [[American Expeditionary Force]].<ref name=DANFS /> Although there is no information about the specific conversion of ''Minnesotan'', for other ships this typically meant that passenger accommodations had to be ripped out and replaced with ramps and stalls for the horses and mules carried.<ref>Crowell and Wilson, pp. 313–14.</ref>

On 23 August 1918, ''Minnesotan'' was transferred to the [[United States Navy]] at [[Norfolk, Virginia]]. She was [[ship commissioning|commissioned]] into the {{NOTS|first=only}} the same day, with [[Lieutenant Commander (United States)|Lieutenant Commander]] E. L. Smith, {{USNRF|first=short}}, in command. ''Minnesotan'' was refitted and rearmed and made a brief roundtrip to New York. After taking on a general cargo, ''Minnesotan'' sailed 4 September to join a convoy from New York. After passing [[Gibraltar]] on 21 September, the cargo ship sailed on to [[Marseille]] and unloaded. Departing there on 21 October, she sailed for [[Newport News, Virginia|Newport News]] via Gibraltar, arriving back in the United States on 7 November.<ref name=DANFS />

[[File:USS Minnesotan 1919.jpg|thumb|left|USS ''Minnesotan'' with returning troops in 1919 at [[Charleston, South Carolina]]]]
''Minnesotan'' next took on a load of 798 horses and sailed on 30 November for [[Bordeaux]], where she arrived on 13 December. Stopping at Saint-Nazaire the following day, ''Minnesotan'' departed for Norfolk on 21 December. After making port at Norfolk on 3 January 1919, the cargo ship sailed for New York, where she was inspected and found to be suitable for use as a troop transport. She was transferred to the [[Cruiser and Transport Force]] on 7 January and fitted with bunks and living facilities over the next three months.<ref name=DANFS />

Sailing from New York on 30 March,<ref name=DANFS /> ''Minnesotan'' began the first of her four voyages returning American servicemen from France.<ref name=Gleaves>Gleaves, pp. 258–59.</ref> On 16 April at Saint-Nazaire, ''Minnesotan'' began her first homeward journey with troops, embarking several companies of the [[111th Infantry Regiment (United States)|111th Infantry Regiment]] of the [[28th Infantry Division (United States)|U.S. 28th Infantry Division]]. George W. Cooper, historian of the 2nd Battalion of the 111th Infantry, reported that even though the fighting had been over for some five months, the fear of striking floating mines necessitated that the men wear life jackets for the first three days at sea.<ref>Cooper, pp. 175–76.</ref> ''Minnesotan'' landed her 1,765 troops in New York on 28 April.<ref>{{cite news | title = Eight transports bring 16,729 men | work = [[Chicago Tribune|Chicago Daily Tribune]] | date = 29 April 1919 | page = 5 }}</ref>

On her next journey, ''Minnesotan'' loaded some 2,000 men of the 304th Ammunition Train and the [[24th Infantry Division (United States)|U.S. 24th Infantry Division]],<ref name=AC-2600>{{cite news | title = 2,600 men at Charleston | work = [[The Atlanta Constitution]] | date = 30 May 1919 | page = 2 }}</ref> for what turned out to be a rough passage with widespread [[seasickness]].<ref name=Loomis-74>Loomis, p. 74.</ref> The men on board were greatly relieved when land was spotted,<ref name=Loomis-74 /> and the ship docked at [[Charleston, South Carolina]], on 29 May.<ref name=AC-2600 />

[[File:USS Minnesotan stitched.jpg|thumb|right|USS ''Minnesotan'' (''right'') is seen in drydock at the [[Philadelphia Navy Yard]] in August 1919, where she was prepared for decommissioning.]]
Details of ''Minnesotan''{{'}}s third journey are not available, but her final journey began by sailing from [[Brest, France|Brest]] on 23 July with elements of the [[4th Infantry Division (United States)|U.S. 4th Infantry Division]] and ended upon arrival at [[Philadelphia]] on 3 August.<ref>Smith and Eckman, "Into the Rhineland".<!-- online source has no page numbers --></ref> In total, she carried 8,038 troops in four voyages from France.<ref name=Gleaves /> By 15 August, ''Minnesotan'' had entered dry dock at the [[Philadelphia Navy Yard]] to prepare for decommissioning,<ref>{{cite web | title = Picture Data: Photo #: NH 42572 | url = http://www.history.navy.mil/photos/images/h42000/h42572c.htm | work = Online Library of Selected Images  | publisher = Navy Department, Naval Historical Center | date = 28 June 2004 | accessdate = 22 August 2008 }}</ref> which took place six days later. She was then returned to American-Hawaiian.<ref name=DANFS /> [[Leslie White]], later a noted American anthropologist, was a crewman aboard USS ''Minnesotan''.<ref>Peace, p. 2.</ref>

== Interwar years ==
''Minnesotan'' resumed cargo service with American-Hawaiian after her return from World War I service. Though the company had abandoned its original Hawaiian sugar routes by this time,<ref>Cochran and Ginger, p. 363</ref> ''Minnesotan'' continued inter-coastal service through the Panama Canal. Hints at cargos she carried during this time can be gleaned from contemporary news reports from the ''[[Los Angeles Times]]''. In March 1928, for example, the newspaper reported that ''Minnesotan'' sailed from Los Angeles with a $2,500,000 cargo that included raw silk and {{convert|1000|LT|t}} of copper bullion. The 1,000 bales of silk, picked up in [[Seattle]], were worth $1,000,000 on their own, while the load of copper was reportedly the largest water shipment of Arizona copper to that time. Canned goods, grape juice, and locally grown cotton completed the load.<ref>{{cite news | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times  | date = 17 March 1928 | page = 13 }}</ref> The ''Los Angeles Times'' also reported that ''Minnesotan'' delivered a then-record {{convert|3000|LT|t|adj=on}} cargo from the East Coast to Los Angeles in October 1930.<ref>{{cite news | last =Drake | first = Waldo | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times  | date = 14 October 1930 | page = 19 }}</ref> ''Minnesotan'' also carried some less-traditional cargo. In February 1928, she delivered one R-class and four six-meter (twenty-foot) sloops to Los Angeles. The five racing yachts, all from East Coast yacht clubs, arrived to sail in the national championships of six-meter and R-class sloops held 10–18 March.<ref>{{cite news | last = Lawrence | first = Edward | title = Eastern craft due next week | work = Los Angeles Times | date = 12 February 1928 | page = A3 }}</ref><ref group=Note>The same five yachts were shipped back east on {{SS|Virginian||2}} in late March. See: {{cite news | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times  | date = 17 March 1928 | page = 13 }}</ref> ''Minnesotan'' delivered two other six-meter sloops for new owners in November 1938.<ref>{{cite news | last =Drake | first = Waldo | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times  | date = 11 November 1938 | page = 19 }}</ref>

''Minnesotan'' did have one mishap during the interwar period. On 3 May 1936, ''[[The New York Times]]'' reported that the day before, a receding tide had stranded ''Minnesotan'' about a half-mile (800&nbsp;m) off of [[Monomoy Point|Monomoy Point, Massachusetts]].<ref>{{cite news | title = Freighter runs aground |work=The New York Times | date = 3 May 1936 | page = 37 }}</ref> Any damage the freighter sustained must have been minor; the cargo ship sailed from New York for San Francisco two weeks later.<ref>{{cite news | title = Shipping and mails | work = The New York Times | date = 17 May 1936  | page = S12 }}</ref>

=== Labor difficulties ===
''Minnesotan'' played a part in several labor difficulties in the interwar years. In March 1935, the crew of ''Minnesotan'' called a [[wildcat strike action|wildcat strike]] that delayed the ship's sailing from Los Angeles by a day, but ended the strike after they were ordered back to work by their union.<ref>{{cite news | last =Drake | first = Waldo | title = Shipping news | work = Los Angeles Times  | date = 8 March 1935 | page = 9 }}</ref> In October 1935, the deckhands and [[fireman (steam engine)|firemen]] of ''Minnesotan'' and fellow Hawaiian-American ships ''Nevadan'' and ''Golden Tide'' walked out—this time with the sanction of their union, the [[Sailors' Union of the Pacific]] (SUP)—after American-Hawaiian had suspended a member of the [[International Seamen's Union]].<ref>{{cite news | title = Crews return to last three tied-up ships | work = Los Angeles Times | date = 27 October 1935 | page = 1 }}</ref> In that same month, ''Minnesotan''{{'}}s deck engineer, Otto Blaczinsky, was murdered while the ship was in [[Port of Los Angeles|Los Angeles Harbor]]. The Industrial Association of San Francisco, an organization of anti-union businessmen and employers,<ref>{{cite web | title = Riots Precede San Francisco General Strike "Bloody Thursday" – 1934 | url = http://www.sfmuseum.net/hist/thursday.html | work = Virtual Museum of the City of San Francisco | publisher = Museum of the City of San Francisco | accessdate = 22 August 2008}}</ref> believed that Blaczinsky was killed because he opposed union policies, and offered a $1,000 reward for information leading to the arrest and conviction of Blaczinsky's killer.<ref name=LAT-ship_crew>{{cite news | title = Ship crew fingerprinted in hunt for knife-killer | work = Los Angeles Times | date = 24 March 1936 | page = 9 }}</ref><ref group=Note>At the time of a similar murder of the Chief Engineer of freighter ''Point Lobos'' in March 1936, Blaczinsky's case was still open.</ref> Threats of another Pacific coast strike in late 1936 caused west coast shippers to squeeze as much cargo as possible into ''Minnesotan'' and other ships; when ''Minnesotan'' arrived at Boston in October, ''[[The Christian Science Monitor]]'' reported that the ship had arrived "literally laden to her [[Plimsoll line]]".<ref>{{cite news | title = Heavy cargoes from west coast | work = The Christian Science Monitor | date = 8 October 1936 | page = 9 }}</ref>

In September 1941, ''Minnesotan'' played a peripheral part in a larger protest by union sailors over war bonuses for sailing in the [[West Indies]].<ref name=WP-strike>{{cite news | title = Strike-bound merchant ships must move, Roosevelt warns | work = The Washington Post | date = 24 September 1941 | page =16 }}</ref><ref name=NYT-acts_in>{{cite news | title = Acts in emergency | work = The New York Times | date = 19 September 1941 | pages = 1, 15 }}</ref> The SUP struck on ''Minnesotan'' and fellow American-Hawaiian ship ''Oklahoman'' on 18 September in sympathy with the [[Seafarers International Union of North America|Seafarers International Organization]], which had called a strike on eleven ships a week before.<ref name=NYT-acts_in /> Both of the American-Hawaiian ships were idled while docked in New York.<ref>{{cite news | title = U.S. signs crews for struck ships; one due to sail | work = The New York Times | date = 20 September 1941| page = 1 }}</ref> President [[Franklin D. Roosevelt]] called on the unions to end the strike three separate times during his press conference on 24 September.<ref name=WP-strike /> Roosevelt's admonition was heeded and both unions ended their strike after the [[National Mediation Board]] agreed to address the wartime bonus dispute.<ref>{{cite news | agency = [[Associated Press]] | title = Sailors free 25 vessels by ending strike | work = Chicago Daily Tribune | date = 25 September 1941 | page = 32 }}</ref>

== World War II ==
[[File:Atlantic convoy, 1942.jpg|thumb|right|SS ''Minnesotan'' sailed in several transatlantic convoys, like this typical one, seen in 1942.]]
By January 1941, ''Minnesotan'', though still operated by American-Hawaiian, was engaged in defense work for the U.S. government, sailing to ports in South Africa.<ref name=NYT-acts_in /><ref>{{cite news | title = Sailors tell of fast and heavily armed British mystery ship | work = Chicago Daily Tribune | date = 21 January 1941 | page = 2 }}</ref> After the United States entered [[World War II]], ''Minnesotan'' was requisitioned by the [[War Shipping Administration]] and frequently sailed in [[convoy]]s. Though complete records of her sailings are unavailable, partial records indicate some of the ports ''Minnesotan'' visited during the conflict and some of the cargo she carried. From July 1942 to April 1943, ''Minnesotan'' sailed between New York and [[Caribbean]] ports, calling at [[Trinidad]], [[Key West, Florida|Key West]], [[Hampton Roads]], [[Guantánamo Bay]], and [[Cristóbal, Colón|Cristóbal]].<ref name=CPDB>{{cite web | title = Port Arrivals/Departures: Minnesotan | url = http://convoyweb.org.uk/ports/index.html?search.php?vessel=MINNESOTAN~armain | work = Arnold Hague's Ports Database | publisher = Convoy Web | accessdate = 21 August 2008}}</ref>

In June 1943, ''Minnesotan'' called at [[Mumbai|Bombay]]. She sailed in the Indian Ocean between [[Kolkata|Calcutta]], [[Colombo]], and [[Bandar Abbas]] through August.<ref name=CPDB /> On her last recorded sailing in the Indian Ocean, ''Minnesotan'' carried steel rails between Colombo and Calcutta.<ref>{{cite AHCD | convoytype = JC | convoynumber = 17 | accessdate = 21 August 2008}}</ref> ''Minnesotan'' was back in New York by early December, and sailed to Florida and back by the end of the month.<ref name=CPDB />

On 29 December, ''Minnesotan'', loaded with a general cargo that included machinery and explosives,<ref name=HX-276>{{cite AHCD | convoytype = HX | convoynumber = 276 | accessdate = 21 August 2008}}</ref> sailed as part of convoy HX 273 from New York for [[Liverpool]]. ''Minnesotan'' developed an undisclosed problem and returned to [[St. John's, Newfoundland and Labrador|St. John's, Newfoundland]],<ref>{{cite AHCD | convoytype = HX | convoynumber = 273 | accessdate = 21 August 2008}}</ref> where she arrived on 13 January 1944.<ref name=CPDB /> Thirteen days later, she sailed from St. John's to join convoy HX 276 for Liverpool,<ref name=HX-276 /> where she arrived with the convoy on 7 February. After calling at [[Methil, Fife|Methil]] and [[Loch Ewe]], ''Minnesotan'' returned to New York in mid March.<ref name=CPDB />

''Minnesotan'' sailed on another roundtrip to Liverpool in May, but was back in New York by early June. Her last recorded World War II sailings were from New York to Key West, Guantánamo Bay, and Cristóbal, where she arrived in late July 1944.<ref name=CPDB /> Sources do not reveal where or in what capacity ''Minnesotan'' spent the remainder of the war.

== Later career ==
After the war's end, American-Hawaiian continued operating ''Minnesotan'' for several more years, but in mid-July 1949, the company announced the sale of ''Minnesotan'' to Italian owners in a move approved by the [[United States Maritime Commission]] several days later.<ref name=NYT-shipping_news>{{cite news | title = Shipping News and Notes: Two ships sold abroad | work = The New York Times | date = 15 July 1949 | page = 39 }}</ref><ref name=NYT-2_ships>{{cite news | last = Horne | first = George | title = 2 Ship transfers protested by CIO | work = The New York Times | date = 25 July 1949 | page = 29 }}</ref><ref group=Note>American-Hawaiian's 1919 ship ''Hawaiian'' was also sold at the same time to [[Panama]]nian interests.</ref> The sale of ''Minnesotan'' was protested by the [[Congress of Industrial Organizations]] which urged the [[United States Congress]] to intervene and to help retain [[United States Merchant Marine|American Merchant Marine]] jobs.<ref name=NYT-2_ships /> Nevertheless, ''Maria Luisa R.'', the new name of the former ''Minnesotan'', remained in Italian hands until she was scrapped in 1952 at [[Bari]].<ref name=Miramar />

== Notes ==
{{Reflist|group=Note}}

== References ==
{{Reflist|30em}}

== Bibliography ==
{{Refbegin}}
* {{cite journal | last = Cochran | first = Thomas C. | authorlink = Thomas C. Cochran (historian) |author2=[[Ray Ginger]]  | title = The American-Hawaiian Steamship Company, 1899–1919 | journal = The Business History Review | volume = 28 | issue = 4 |date=December 1954 | pages = 343–365 | location = Boston | publisher = The President and Fellows of Harvard College | oclc = 216113867 | doi = 10.2307/3111801 | jstor = 3111801 }}
* {{cite book | last = Cooper | first = George W. | authorlink = <!-- not: George W. Cooper --> | title = Our Second Battalion: The Accurate and Authentic History of the Second Battalion 111th Infantry | location = [[Pittsburgh]] | publisher = Second Battalion Book Co. | year = 1920 | oclc = 4139237 }}
* {{cite book | last = Crowell  | first = Benedict  | authorlink = Benedict Crowell |author2=Robert Forrest Wilson | title = The Road to France: The Transportation of Troops and Military Supplies, 1917–1918 | series = How America Went to War: An Account From Official Sources of the Nation's War Activities, 1917–1920 | location = [[New Haven, Connecticut|New Haven]] | publisher = [[Yale University Press]] | year = 1921 | oclc = 18696066 | isbn = 1-143-73956-6 }}
* {{Gleaves}}
* {{cite journal | last = Hovey | first = Edmund Otis | title = The Isthmus of Tehuantepec and the Tehuantepec National Railway | journal = Bulletin of the American Geographical Society | volume = 39 | issue = 2 | year = 1907 | pages = 78–91 | location = New York | publisher = [[American Geographical Society]] | oclc = 2097765 | doi = 10.2307/198380 | jstor = 198380 }}
* {{cite book | last = Loomis | first = Ernest L. | title = History of the 304th Ammunition Train | location = Boston | publisher = R.G. Badger | year = 1920 | oclc = 6871043 }}
* {{cite book | last = Peace | first = William J. | chapter = Leslie A. White and the Socio-Politics of War | title = Histories of Anthropology Annual: Volume 3 | editor = [[Regna Darnell]] and [[Frederic W. Gleach]] | location = [[Lincoln, Nebraska|Lincoln]] | publisher = [[University of Nebraska Press]] | year = 2007 | isbn = 978-0-8032-6664-3 | oclc = 181068410 }}
* {{cite book | last = Smith | first = Harry L. |author2=James Russell Eckman | title = Memoirs of an ambulance company officer | url = http://net.lib.byu.edu/~rdh7/wwi/memoir/Ambco/officerTC.html#TC | chapter = Into the Rhineland | chapterurl = http://net.lib.byu.edu/~rdh7/wwi/memoir/Ambco/officer6.html | location = [[Rochester, Minnesota]] | publisher = Doomsday Press | year = 1940 | oclc = 4878505  | accessdate = 22 August 2008}}
* {{cite DANFS | author = [[Naval Historical Center]] | title = Minnesotan | url = http://www.history.navy.mil/danfs/m11/minnesotan.htm | accessdate = 21 August 2008}}
{{Refend}}

== External links ==
* {{navsource|12/174545|Minnesotan}}

{{featured article}}

{{DEFAULTSORT:Minnesotan}}
[[Category:Ships built in Sparrows Point, Maryland]]
[[Category:Cargo ships]]
[[Category:World War I merchant ships of the United States]]
[[Category:World War I auxiliary ships of the United States]]
[[Category:Transport ships of the United States Army]]
[[Category:United States Navy Minnesota-related ships]]
[[Category:World War II merchant ships of the United States]]
[[Category:World War II auxiliary ships of the United States]]
[[Category:Merchant ships of Italy]]
[[Category:1912 ships]]
[[Category:Cargo ships of the United States Navy]]
[[Category:Unique transports of the United States Navy]]