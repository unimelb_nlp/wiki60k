{{Infobox accounting body
|name_in_local_language= 
|abbreviation          = AAT 
|image                 = Association of Accounting Technicians logo.jpg
|image_border          = 
|alt                   = 
|image_caption         = Logo of the AAT 
|predecessor           = 
|formation             = {{Start date|1980}}
|objective             = <!-- focus as e.g. education and licensing of professional accountants etc -->
|headquarters          = [[London]], UK
|coords                = {{Coord|51.520520|-0.097163|region:GB_type:landmark|format=dms|display=it}}
|region_served         = 90 countries    
|membership_num        = 133,600
|members_designations  = MAAT, FMAAT
|language              = <!-- official languages -->
|highest_officer_title = Chief Executive
|highest_officer_name  = Mark Farrar
|third_officer_title   = <!-- position of the third ranking officer like  Secretary, CEO etc.. -->
|third_officer_name    = <!-- name of the above person -->
|fourth_officer_title  = <!-- position of the fourth ranking officer like  Treasurer, CFO etc.. -->
|fourth_officer_name   = <!-- name of the above person -->
|administering_organ   = <!-- Council, General Body, Board of Directors, etc -->
|additional_data_name1 = <!-- Optional data1 name -->
|additional_data1      = <!-- Optional data1 -->
|additional_data_name2 = <!-- Optional data2 name -->
|additional_data2      = <!-- Optional data2 -->
|additional_data_name3 = <!-- Optional data3 name -->
|additional_data3      = <!-- Optional data2 -->
|num_staff             =
|website               = {{url|www.aat.org.uk/}}
|remarks               =
|former name           = <!-- former name if any -->
}}

The '''Association of Accounting Technicians''', or '''AAT''', is a UK qualification and professional body for vocational accountants, with over 133,000 members worldwide. It was created by the merger of two founding institutes in 1980: the Institute of Accounting Staff (then a subsidiary of [[Association of Chartered Certified Accountants|ACCA]]) and the Association of Technicians in Finance and Accounting (then a subsidiary of [[Chartered Institute of Public Finance and Accountancy|CIPFA]]).<ref>{{cite web|title=Timeline - Accounting History|url=http://www.icaew.com/en/library/subject-gateways/accounting-history/resources/timeline/1968-2003| publisher=ICAEW}}</ref><ref>{{cite web|title=Index of UK and Irish Accountancy and Professional Bodies |url=http://www.icaew.com/en/library/subject-gateways/accounting-history/resources/index-of-uk-and-irish-accountancy-and-professional-bodies |publisher=ICAEW |deadurl=yes |archiveurl=https://web.archive.org/web/20141006185850/http://www.icaew.com/en/library/subject-gateways/accounting-history/resources/index-of-uk-and-irish-accountancy-and-professional-bodies |archivedate=2014-10-06 |df= }}</ref> It is a technician level qualification offering higher apprenticeships which entitles those who have completed the exams and obtained relevant supervised work experience to become an accounting technician.  The AAT is based in London, but has branches with membership all over the UK and the rest of the world including Hong Kong and South Africa.<ref>http://www.cimaglobal.com/Starting-CIMA/Starting-CIMA/Entry-Routes/students-or-professional-members/AAT/</ref>

== Professional recognition ==
The body is sponsored by four UK chartered accountancy bodies; CIPFA, ICAEW, CIMA and ICAS

== AAT versus CAT ==
The only UK accountancy body which does not sponsor the AAT is the [[Association of Chartered Certified Accountants]] (ACCA). This organisation used to be a sponsor of the AAT but broke away in order to form a rival qualification called the [[Certified Accounting Technician]] (CAT) award. The ACCA implemented this policy as it wanted a technician level qualification that offered accountancy apprenticeships based on the same business model as itself.

== AAT in Scotland ==
The Institute of Chartered Accountants in Scotland is also keen to implement AAT courses into its own business model.{{citation needed|date=February 2015}} It presently has no plans to develop an ICAS technician qualification in direct competition with the AAT's UK model as proposed in the late 1990s.{{citation needed|date=February 2015}} It has remained a sponsoring body since the inception of the AAT.{{citation needed|date=February 2015}}

== AAT in South Africa ==
The South African Institute of Chartered Accountants decided to sponsor the AAT program offering higher apprenticeship accounting courses and it has expanded since 2011. AAT(SA) has no signatory powers for any of its South African members, this is because there is not sufficient audit content within the qualification offered. SAICA and AAT are working towards gaining authority in a number of other areas, such as [[Commissioner of Oaths]] and review status. This sort of authority does take time to approve and the new South African Company Legislation is not yet finalised.<ref>http://www.aatsa.org.za/</ref>

== Examinations ==
Since 2010 every level of the AAT qualification has a number of modules, each of which culminate in a computer based assessment.

== Qualifications ==
The AAT Intermediate Level 3 qualification or Level 6 in Scotland, is approved for the University entrance system with a value of 160 UCAS tariff points.<ref>[https://www.aat.org.uk/sites/default/files/assets/AAT_Level_3_Diploma_in_Accounting_Specification.pdf AAT Qualification Specification - published 10/2012 (website accessed 01/01/2015)]</ref>  This is also included in the KS5 performance tables. The final AAT qualification, the AAT Advanced Level is equivalent to QCF Level 4 and SCQF Level 8.

Accounting Technicians, when certified as being experienced and competently qualified, perform some similar tasks to [[British qualified accountants|qualified accountants in the UK]], but can not sign off companies audit reports. Certain AAT Members In Practice (MIP) are recognised by the DTI to sign off the Independent Review of charity accounts. An Independent Review is similar in status to the work which an AAT member could undertake under the old Reporting Accountant standard. The obsolescence of Reporting Accountant has instigated calls from senior members of the Association, for their body to recognise these more senior members' qualifications and work experience with some new distinguishing status.

In 2014 AAT expanded its suite of qualifications and launched new courses in accounting, bookkeeping, computerised accounting, tax and business skills.

== Areas of Vocational Expertise and Academic Competence ==
Accounting Technicians perform similar work based tasks to those members of its sponsoring bodies the main exclusion being that it is not possible for an AAT Member to sign off an auditor's report. However, they can, if additionally they hold a practising certificate with one of the sponsoring bodies regulated and authorised to produce audit reports. The AAT is now one of the accountancy bodies in the UK allowed to let its suitably experienced and qualified members perform independent examinations of charities accounts, subject to the audit exemption thresholds for incorporated charities.

===Employer Accreditation===
The AAT employer accreditation scheme recognises employers with a positive and planned approach to the professional development of their AAT members, whether through studying for an AAT qualification or, once qualified, supporting them in their continuing professional development.

AAT Accredited Employer Scheme provides the employer with a range of benefits including:

* Full and fellow members will automatically meet their mandatory CPD requirements by following their employer's training and development processes 
* Support in managing student member's 
* Advice on trainee recruitment and funding 
* AAT Accredited Employer logo and certificate.

==See also==
*[[British Accountant]]

==References==
<references/>

==External links==
*[http://www.aat.org.uk/ AAT website]
*[http://aatcomment.org.uk/ AAT's blog, AAT Comment]
*[http://aat-ethics.org.uk/ AAT Ethics site]
*[http://forums.aat.org.uk/ AAT discussion forums]

{{DEFAULTSORT:Association Of Accounting Technicians}}
[[Category:Professional accounting bodies]]
[[Category:Professional associations based in the United Kingdom]]
[[Category:Accounting in the United Kingdom]]