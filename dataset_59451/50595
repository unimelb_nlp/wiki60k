{{Infobox journal
| title = Arthritis Research & Therapy
| formernames = Arthritis Research
| cover = [[File:ArthritisResearchLogo.gif]]
| discipline = [[Rheumatology]]
| abbreviation = Arthritis Res. Ther.
| editor = Christopher Buckley, Harris Perlman
| publisher = [[BioMed Central]]
| country =
| history = 1999–present
| frequency = Bimonthly
| impact = 3.753
| impact-year = 2014
| website = http://arthritis-research.biomedcentral.com/
| link2 = http://arthritis-research.biomedcentral.com/articles
| link2-name = Online archive
| CODEN = 
| OCLC = 51648457
| ISSN = 1478-6354
| eISSN = 1478-6362
}}
'''''Arthritis Research & Therapy''''', formerly '''''Arthritis Research''''', is a [[peer-reviewed]] [[open access]] [[medical journal]] covering the field of cellular and molecular mechanisms of [[arthritis]], musculoskeletal conditions, and systemic [[autoimmune disease|autoimmune]] [[Rheumatism|rheumatic diseases]]. It is published by [[BioMed Central]] and the [[editors-in-chief]] are Christopher Buckley ([[University of Birmingham]]) and Harris Perlman ([[Northwestern University]]).<ref name="ART1970">{{cite web |title=Editorial Board |work=Arthritis Research & Therapy |url=http://www.arthritis-research.com/about/edboard |accessdate=2015-11-15}}</ref><ref name="NCBI2015">{{cite web |title=Arthritis Research & Therapy |work=NLM Catalog |publisher=NCBI |date=14 Nov 2015 |url=https://www.ncbi.nlm.nih.gov/nlmcatalog/101154438 |accessdate=2015-11-15}}</ref> The journal was established in 1999 as ''Arthritis Research'', obtaining its current title in.<ref name="NCBI2015AR">{{cite web |title=Arthritis research |work=NLM Catalog |publisher=NCBI |date=14 Nov 2015 |url=https://www.ncbi.nlm.nih.gov/nlmcatalog/100913255 |accessdate=2015-11-15}}</ref> The journal's print version ceased in 2010 with volume 12, number 6, and the journal converted to an online only format.<ref name="NCBI2015" /> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.753.<ref name=WoS>{{cite book |year=2015 |chapter=Arthritis Research & Therapy |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://arthritis-research.biomedcentral.com/}}

{{DEFAULTSORT:Arthritis Research and Therapy}}
[[Category:Rheumatology journals]]
[[Category:BioMed Central academic journals]]
[[Category:Publications established in 1999]]
[[Category:English-language journals]]