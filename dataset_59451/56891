{{Infobox Journal
| title        = European Physical Journal D: Atomic, Molecular, Optical and Plasma Physics
| cover        = [[File:Epjd webcover.jpg]]
| editor       = K. Becker, C. Fabre,F.A. Gianturco
| discipline   = [[Physics|Atomic Physics]]
| language     = [[English language|English]]
| abbreviation = EPJ D
| publisher    = [[Springer Science+Business Media]], [[EDP Sciences]], [[Società Italiana di Fisica]] 
| frequency    = Monthly
| history      = 1998–present
| openaccess   = 
| impact       = 1.228
| impact-year = 2014
| website      = http://www.springer.com/10053
| link1        = 
| link1-name   = 
| link2        = 
| link2-name   = 
| RSS          = 
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 1434-6060
| eISSN        = 1434-6079
}}
The '''''European Physical Journal D: Atomic, Molecular, Optical and Plasma Physics''''' is an [[academic journal]] recognized by the [[European Physical Society]], presenting new and original research results.

==Scope==
The main areas covered are:
*Atomic Physics
*Molecular Physics and Chemical Physics
*Atomic and Molecular Collisions
*Clusters and Nanostructures
*Plasma Physics
*Laser Cooling and Quantum Gas
*Nonlinear Dynamics
*Optical Physics
*Quantum Optics and Quantum Information
*Ultraintense and Ultrashort Laser Fields
 
The range of topics covered in these areas is extensive, from Molecular Interaction and Reactivity to Spectroscopy and Thermodynamics of Clusters, from Atomic Optics to Bose-Einstein Condensation to Femtochemistry.

==History==
The EPJ D arose from various predecessors: ''Il Nuovo Cimento (Section D)'', ''Journal de Physique'', and ''Zeitschrift für Physik D''. Prior to 1998, this journal was named ''Zeitschrift für Physik D: Atoms, Molecules and Clusters''.

Until 2003, Ingolf Hertel was the [[Editor-in-Chief]] of EPJ D. From May 2003 on EPJ D had two Editors-in-Chief: Tito Arecchi and Jean-Michel Raimond. In January 2004, Arecchi stepped down and Franco A. Gianturco took over his position.

In 2009, the newly appointed (third) Editor-in-Chief, Kurt Becker, took on the responsibility for promoting the Plasma Physics coverage of the journal.

==See also==
* ''[[European Physical Journal]]''

[[Category:Physics journals]]
[[Category:EDP Sciences academic journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1998]]