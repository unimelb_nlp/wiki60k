{{featured article}}

{{Use dmy dates|date=January 2015}}
{|{{Infobox ship begin}} <!-- warships except submarines -->
|+Yugoslav torpedo boat ''T1''
{{Infobox ship image
|Ship image=[[File:Yugoslav torpedo boat T3.jpg|300px|alt=a black and white photograph of a small ship underway]]
|Ship caption= ''T1''{{'}}s sister ship, ''T3''
}}
{{Infobox ship career
|Hide header=
|Ship country=[[Austria-Hungary]]
|Ship flag={{shipboxflag|Austria-Hungary|naval}}
|Ship name=''76 T''
|Ship namesake=
|Ship ordered=
|Ship builder=[[Stabilimento Tecnico Triestino]]
|Ship laid down=24 June 1913
|Ship launched=15 December 1913
|Ship acquired=
|Ship commissioned=20 July 1914
|Ship decommissioned=
|Ship in service=
|Ship out of service=1918
|Ship honours=
|Ship fate=Assigned to the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]]
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Kingdom of Yugoslavia]]
|Ship flag={{shipboxflag|Kingdom of Yugoslavia|naval}}
|Ship name=''T1''
|Ship namesake=
|Ship acquired=March 1921
|Ship commissioned=
|Ship decommissioned=
|Ship in service=
|Ship out of service=April 1941
|Ship honours=
|Ship fate=Captured by Italy
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Kingdom of Italy|Italy]]
|Ship flag={{shipboxflag|Kingdom of Italy|naval}}
|Ship name=''T1''
|Ship namesake=
|Ship acquired=April 1941
|Ship in service=
|Ship out of service=September 1943
|Ship honours=
|Ship fate=
|Ship status=
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Kingdom of Yugoslavia]]
|Ship flag={{shipboxflag|Kingdom of Yugoslavia|naval}}
|Ship name=''T1''
|Ship acquired=December 1943
|Ship out of service=
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship fate=Transferred to [[Yugoslav Navy]] post-war
|Ship status=
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Socialist Federal Republic of Yugoslavia|Federal People's Republic of Yugoslavia]]
|Ship flag={{shipboxflag|Yugoslavia|naval}}
|Ship name=''Golešnica''
|Ship acquired=post-[[World War II]]
|Ship out of service=1959
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship fate=
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=[[250t-class torpedo boat|250t-class]], T-group sea-going [[torpedo boat]]
|Ship displacement=*{{convert|262|t|LT|0|lk=out|abbr=on}}
*{{convert|320|t|LT|0|abbr=on}} (full load)
|Ship length={{convert|58.2|m|ftin|abbr=on}}
|Ship beam={{convert|5.7|m|ftin|abbr=on}}
|Ship draught={{convert|1.5|m|ftin|abbr=on}}
|Ship power=*{{convert|5000|-|6000|shp|lk=in|abbr=on}}
*2 × [[Yarrow boiler|Yarrow]] [[water-tube boiler]]s
|Ship propulsion=*2 × shafts
*2 × [[Parsons Marine Steam Turbine Company|Parsons]] [[steam turbine]]s
|Ship speed={{convert|28|kn}}
|Ship range={{convert|980|nmi|abbr=on}} at {{convert|16|kn}}
|Ship complement=39 officers and enlisted
|Ship armament=*2 × [[Škoda Works|Škoda]] {{convert|66|mm|in|abbr=on}} L/30 guns
*4 × {{convert|450|mm|in|abbr=on|sigfig=3}} [[torpedo tubes]]
*10–12 [[naval mine]]s
|Ship armour=
|Ship notes=
}}
|}

The '''Yugoslav torpedo boat ''T1''''' was a seagoing [[torpedo boat]] that was operated by the [[Royal Yugoslav Navy]] between 1921 and 1941. Originally built as '''''76 T''''', a [[250t-class torpedo boat]] built for the [[Austro-Hungarian Navy]] in 1914, she was armed with two {{convert|66|mm|in|abbr=on}} guns and four {{convert|450|mm|in|abbr=on|sigfig=3}} [[torpedo tube]]s, and could carry 10–12 [[naval mine]]s. She saw active service during [[World War I]], performing [[convoy]], escort and [[minesweeping]] tasks, [[Anti-submarine warfare|anti-submarine operations]] and [[shore bombardment]] missions. She was part of the escort force for the Austro-Hungarian [[dreadnought]] [[SMS Szent István|SMS ''Szent István'']] during the action that resulted in the sinking of that ship by [[Kingdom of Italy|Italian]] torpedo boats in June 1918. Following [[Austria-Hungary]]'s defeat later that year, ''76 T'' was allocated to the Navy of the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]], which became the Royal Yugoslav Navy, and was renamed ''T1''.

During the [[Nazi Germany|German]]-led [[Axis powers|Axis]] [[invasion of Yugoslavia]] in April 1941, the vessel was captured by the Italians and served with the [[Regia Marina|Royal Italian Navy]] under her Yugoslav designation. Following the Italian [[Armistice of Cassibile|capitulation]] in September 1943, she was returned to the Royal Yugoslav Navy-in-exile. She was commissioned by the [[Yugoslav Navy]] after [[World War II]], and after a refit which included replacement of her armament, she served as '''''Golešnica''''' until 1959.

==Background==
In 1910, the [[Austria-Hungary]] Naval Technical Committee initiated the design and development of a {{convert|275|t|LT|adj=on}} coastal [[torpedo boat]], specifying that it should be capable of sustaining {{convert|30|kn|km/h}} for 10 hours. This specification was based an expectation that the [[Strait of Otranto]], where the [[Adriatic Sea]] meets the [[Ionian Sea]], would be [[blockade]]d by hostile forces during a future conflict. In such circumstances, there would be a need for a torpedo boat that could sail from the [[Austro-Hungarian Navy]] ({{lang-de|kaiserliche und königliche Kriegsmarine}}) base at the [[Bay of Kotor]] (Bocche di Cattaro) to the Strait during darkness, locate and attack blockading ships and return to port before morning. [[Steam turbine]] power was selected for propulsion, as [[Marine propulsion#Reciprocating diesel engines|diesels]] with the necessary power were not available, and the Austro-Hungarian Navy did not have the practical experience to run [[Turbo-electric transmission|turbo-electric]] boats. [[Stabilimento Tecnico Triestino]] (STT) of [[Trieste]] was selected for the contract to build eight vessels, ahead of one other tenderer.{{sfn|Gardiner|1985|p=339}} The T-group designation signified the builder, STT.{{sfn|Greger|1976|p=58}}

==Description and construction==
The [[250t-class torpedo boat|250t-class]] T-group boats had a [[waterline length]] of {{convert|58.2|m|ftin|abbr=on}}, a [[Beam (nautical)|beam]] of {{convert|5.7|m|ftin|abbr=on}}, and a normal [[Draft (hull)|draught]] of {{convert|1.5|m|ftin|abbr=on}}. While their designed displacement was {{convert|262|t|LT|lk=in|0}}, they displaced about {{convert|320|t|LT}} fully loaded. The crew consisted of 39 officers and enlisted men. The boats were powered by two [[Parsons Marine Steam Turbine Company|Parsons]] steam turbines driving two propellers, using steam generated by two [[Yarrow boiler|Yarrow]] [[water-tube boiler]]s, one of which burned [[fuel oil]] and the other coal. The turbines were rated at {{convert|5000|shp|lk=in|abbr=on}} with a maximum output of {{convert|6000|shp|lk=no|abbr=on}} and designed to drive the boats to a top speed of {{convert|28|kn}}. They carried {{convert|18|t|LT|sigfig=3}} of coal and {{convert|24|t|LT|sigfig=3}} of fuel oil,{{sfn|Jane's Information Group|1989|p=313}} which gave them a range of {{convert|980|nmi|lk=in}} at {{convert|16|kn|lk=in}}.{{sfn|Greger|1976|p=58}} The T-group had one [[Funnel (ship)|funnel]] rather than the two funnels of the later groups of the class. Despite the specifications of the contract being very close to the requirements for the coastal torpedo boat, the STT boats were classified as seagoing. The 250t-class, T-group were the first small Austro-Hungarian Navy boats to use turbines, and this contributed to ongoing problems with them.{{sfn|Gardiner|1985|p=339}}

The boats were originally to be armed with three [[Škoda Works|Škoda]] {{convert|66|mm|in|abbr=on}} L/30{{refn|L/30 denotes the length of the gun. In this case, the L/30 gun is 30 [[Caliber (artillery)|calibre]], meaning that the gun was 30 times as long as the diameter of its bore.|group = lower-alpha}} guns, and three {{convert|450|mm|in|abbr=on|sigfig=3}} torpedo tubes,{{sfn|Gardiner|1985|p=339}} but this was changed to two guns and four torpedo tubes before the first boat was completed, in order to standardise the armament with the following F-group. They could also carry 10–12 [[naval mine]]s.{{sfn|Greger|1976|p=58}}

The third of its class to be completed, ''76&nbsp;T'' was [[Keel laying|laid down]] on 24 June 1913, [[Ceremonial ship launching|launched]] on 15 December 1913 and completed on 20 July 1914.{{sfn|Greger|1976|p=58}} In 1914, one {{convert|8|mm|in|abbr=on}} [[machine gun]] was added.{{sfn|Gardiner|1985|p=339}}

==Career==
===World War I===
At the outbreak of [[World War I]], ''76&nbsp;T'' was part of the 1st Torpedo Group of the 3rd Torpedo Craft Division of the Austro-Hungarian 1st Torpedo Craft Flotilla.{{sfn|Greger|1976|pp=11–12}} During the war, ''76&nbsp;T'' was used for [[convoy]], escort and [[minesweeping]] tasks, [[Anti-submarine warfare|anti-submarine operations]],{{sfn|Gardiner|1985|p=339}} and [[shore bombardment]] missions.{{sfn|Cernuschi|O'Hara|2015|p=171}} She also conducted patrols and supported [[seaplane]] raids against the Italian coast. Due to inadequate funding, ''76&nbsp;T'' and the rest of the 250t class were essentially coastal vessels, despite the original intention that they would be used for "high seas" operations.{{sfn|O'Hara|Worth|Dickson|2013|pp=26–27}} On 24 May 1915, ''76&nbsp;T'' and seven other 250t-class boats were involved in the shelling of various Italian shore-based targets known as the [[Bombardment of Ancona]], with ''76&nbsp;T'' involved in the operation against [[Ancona]] itself.{{sfn|Cernuschi|O'Hara|2015|p=168}} In late November 1915, the Austro-Hungarian fleet deployed a force from its main fleet base at [[Pula|Pola]] to Cattaro in the southern Adriatic; this force included six of the eight T-group torpedo boats, so it is possible that one of these was ''76&nbsp;T''. This force was tasked to maintain a permanent patrol of the Albanian coastline and interdict any troop transports crossing from Italy.{{sfn|Halpern|2012|p=229}}

On 3 May 1916, ''76&nbsp;T'' and five other 250t-class boats were accompanying four destroyers when they were involved in a surface action off [[Port of Ravenna|Porto Corsini]], near [[Ravenna]], against an Italian force led by the [[flotilla leader]]s [[Italian cruiser Cesare Rossarol|''Cesare Rossarol'']] and [[Italian cruiser Guglielmo Pepe|''Guglielmo Pepe'']]. On this occasion the Austro-Hungarian force retreated behind a [[minefield]] with no damage to the torpedo boats, and only [[Fragmentation (weaponry)|splinter]] damage to the destroyer {{sclass-|Huszár|destroyer|2}} {{SMS|Csikós||2}}. In 1917, one of her 66&nbsp;mm guns was placed on an [[anti-aircraft warfare|anti-aircraft]] mount.{{sfn|Greger|1976|p=58}} By 1918, the [[Allies of World War I|Allies]] had strengthened their ongoing blockade on the Strait of Otranto, as foreseen by the Austro-Hungarian Navy. As a result, it was becoming more difficult for the [[Imperial German Navy|German]] and Austro-Hungarian [[U-boat]]s to get through the strait and into the [[Mediterranean Sea]]. In response to these blockades, the new commander of the Austro-Hungarian Navy, ''[[Konteradmiral]]'' [[Miklós Horthy]], decided to launch an attack on the Allied defenders with [[battleship]]s, [[scout cruiser]]s, and [[destroyer]]s.{{sfn|Sokol|1968|pp=133–134}}

During the night of 8 June, Horthy left the naval base of [[Pula|Pola]] in the upper Adriatic with {{ship|SMS|Viribus Unitis||2}} and {{ship|SMS|Prinz Eugen|1912|2}}. At about 23:00 on 9 June 1918, after some difficulties getting the harbour defence barrage opened, {{ship|SMS|Szent István||2}} and {{ship|SMS|Tegetthoff||2}},{{sfn|Sokol|1968|p=134}} escorted by one destroyer and six torpedo boats, including ''76 T'', also departed Pola and set course for [[Slano]], north of [[Dubrovnik|Ragusa]], to rendezvous with Horthy in preparation for a coordinated attack on the [[Otranto Barrage]]. About 03:15 on 10 June,{{refn|group=lower-alpha|Sources differ on what the exact time was when the attack took place. Sieche states that the time was 3:15 am when the ''Szent István'' was hit,{{sfn|Sieche|1991|pp=127, 131}} while Sokol claims that the time was 3:30 am.{{sfn|Sokol|1968|p=134}}}} while returning from an uneventful patrol off the [[Dalmatia]]n coast, two [[Regia Marina|Italian Navy]] ({{lang-it|Regia Marina}}) [[MAS (boat)|MAS boats]], ''MAS 15'' and ''MAS 21'', spotted the smoke from the Austrian ships. Both boats successfully penetrated the escort screen and split to engage the dreadnoughts individually. ''MAS 21'' attacked ''Tegetthoff'', but her torpedoes missed.{{sfn|Sokol|1968|p=135}} Under the command of [[Luigi Rizzo]], ''MAS 15'' fired two torpedoes at 03:25, both of which hit ''Szent István''. Both boats evaded pursuit although Rizzo had to discourage ''76 T'' by dropping [[depth charge]]s in his wake. The torpedo hits on ''Szent István'' were abreast her boiler rooms, which flooded, knocking out power to the pumps. ''Szent István'' [[Capsizing|capsized]] less than three hours after being torpedoed.{{sfn|Sieche|1991|pp=127, 131}}

===Interwar period===
''76 T'' survived the war intact.{{sfn|Gardiner|1985|p=339}} In 1920, under the terms of the previous year's [[Treaty of Saint-Germain-en-Laye (1919)|Treaty of Saint-Germain-en-Laye]], she was allocated to the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] (KSCS, later Yugoslavia). Along with three other 250t-class T-group boats, ''77 T,'' ''78 T'' and ''79 T'', and four 250t-class F-group boats, she served with the [[Royal Yugoslav Navy]] ({{Lang-sh-Latn|Kraljevska Jugoslovenska Ratna Mornarica}}, KJRM; Кpaљeвcкa Југословенска Pатна Морнарица). Taken over in March 1921,{{sfn|Vego|1982|p=345}} in KJRM service, ''76 T'' was renamed ''T1''.{{sfn|Greger|1976|p=58}} When the navy was formed, she and the other seven 250t-class boats were the only modern sea-going vessels in the KJRM.{{sfn|Chesneau|1980|p=355}} In 1925, exercises were conducted off the [[Dalmatia]]n coast, involving the majority of the navy.{{sfn|Jarman|1997a|p=733}} In May and June 1929, six of the eight 250t-class torpedo boats accompanied the light cruiser ''[[SMS Niobe|Dalmacija]]'', the [[submarine tender]] ''Hvar'' and the submarines {{ship|Yugoslav submarine|Hrabri||2}} and {{ship|Yugoslav submarine|Nebojša||2}}, on a cruise to [[Crown Colony of Malta|Malta]], the Greek island of [[Corfu]] in the Ionian Sea, and [[Bizerte]] in the [[French protectorate of Tunisia]]. It is not clear if ''T1'' was one of the torpedo boats involved. The ships and crews made a very good impression while visiting Malta.{{sfn|Jarman|1997b|p=183}} In 1932, the British naval [[attaché]] reported that Yugoslav ships engaged in few exercises, manoeuvres or gunnery training due to reduced budgets.{{sfn|Jarman|1997b|p=451}}

===World War II and post-war service===
In April 1941, Yugoslavia entered [[World War II]] when it was [[Invasion of Yugoslavia|invaded]] by the [[Nazi Germany|German]]-led [[Axis powers]]. At the time of the invasion, ''T1'' was assigned to the Southern Sector of the KJRM's Coastal Defence Command based at the Bay of Kotor, along with her [[sister ship]] [[Yugoslav torpedo boat T3|''T3'']] and a number of [[minesweeper]]s and other craft.{{sfn|Niehorster|2013}} ''T1'' was captured by the Italian Navy shortly after the commencement of hostilities and was operated by them under her Yugoslav designation, conducting coastal and second-line escort duties in the Adriatic. Her guns were also replaced by two {{convert|76|mm|in|abbr=on}} L/40 anti-aircraft guns.{{sfn|Brescia|2012|p=151}} After the Italians [[Armistice of Cassibile|capitulated]] in September 1943, she was returned by them to the KJRM-in-exile in December 1943.{{refn|group=lower-alpha|One source states that she was captured by the Germans and transferred to the [[Navy of the Independent State of Croatia|navy]] of the puppet state, the [[Independent State of Croatia]],{{sfn|Gardiner|1985|p=339}} but several other sources state that she was returned to the KJRM in December 1943.{{sfn|Brescia|2012|p=151}}{{sfn|Chesneau|1980|p=357}}{{sfn|Whitley|1988|p=186}}}} She was commissioned by the [[Yugoslav Navy]] ({{lang-sh-Latn|Jugoslavenska Ratna Mornarica|links=no}}, Југословенска Pатна Mорнарица) after the war, serving as ''Golešnica''. Her post-war fit-out included replacing her guns with two {{convert|40|mm|in|abbr=on}} guns on single mounts and four {{convert|20|mm|in|abbr=on}} guns, and removing her torpedo tubes. She continued in Yugoslav service until October 1959, when she was stricken.{{sfn|Chesneau|1980|p=357}}

==See also==
*[[List of ships of the Royal Yugoslav Navy]]
*[[List of ships of the Yugoslav Navy]]

==Notes==
{{reflist|group=lower-alpha}}

==Footnotes==
{{reflist|20em}}

==References==
{{refbegin}}
*{{cite book
  | last = Brescia
  | first = Maurizio
  | year = 2012
  | title = Mussolini's Navy
  | publisher = Seaforth Publishing
  | location = Barnsley, South Yorkshire
  | isbn = 978-1-59114-544-8
  | ref = harv
  }}
* {{cite book
  | last1 = Cernuschi
  | first1 = Enrico
  | last2 = O'Hara
  | first2 = Vincent P.
  | editor-last = Jordan
  | editor-first =John
  | title = Warship 2015
  | publisher = Bloomsbury
  | location = London, England
  | year = 2015
  | pages = 161–173
  | chapter = The Naval War in the Adriatic Part I: 1914–1916
  | isbn = 978-1-84486-295-5
  | lastauthoramp = y
  | ref = harv
  }}
* {{cite book
  | editor-last = Chesneau
  | editor-first = Roger
  | year = 1980
  | title = Conway's All the World's Fighting Ships, 1922–1946
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-146-5
  | ref = harv
  }}
* {{cite book
  | editor-last = Gardiner
  | editor-first = Robert
  | year = 1985
  | title = Conway's All the World's Fighting Ships, 1906–1921
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-245-5
  | ref = harv
  }}
* {{cite book
  | last = Greger
  | first = René
  | year = 1976
  | title = Austro-Hungarian Warships of World War I
  | publisher = Allan
  | location = London, England
  | isbn = 978-0-7110-0623-2
  | ref = harv
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 2012
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-0-87021-266-6
  | ref = harv
  }}
* {{cite book
  | last = Jane's Information Group
  | year = 1989
  | origyear = 1946/47
  | title = Jane's Fighting Ships of World War II
  | publisher = Studio Editions
  | location = London, England
  | isbn = 978-1-85170-194-0
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997a
  | title = Yugoslavia Political Diaries 1918–1965
  | volume = 1
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997b
  | title = Yugoslavia political diaries 1918–1965
  | volume = 2
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite web
  | url =  http://niehorster.org/040_yugoslavia/41-04-06/navy_coast.html
  | title = Balkan Operations Order of Battle Royal Yugoslavian Navy Coastal Defense Command 6th April 1941
  | accessdate = 27 January 2015
  | publisher  = Dr. Leo Niehorster
  | year   = 2013
  | last = Niehorster
  | first = Dr. Leo
  | ref = harv
  }}
* {{cite book
  | last1 = O'Hara
  | first1 = Vincent
  | last2 = Worth
  | first2 = Richard
  | last3 = Dickson
  | first3 = W.
  | title = To Crown the Waves: The Great Navies of the First World War
  | url = https://books.google.com/books?id=2RlFAAAAQBAJ
  | year = 2013
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-1-61251-269-3
  | ref = harv
  }}
* {{cite journal
  | last = Sieche
  | first = Erwin F.
  | year = 1991
  | title = S.M.S. Szent István: Hungaria's Only and Ill-Fated Dreadnought
  | journal = Warship International
  | publisher = International Warship Research Organization
  | location = Toledo, Ohio
  | volume = XXVII
  | issue = 2
  | pages = 112–146
  | issn = 0043-0374
  | ref = harv
  }}
* {{cite book
  | last = Sokol
  | first = Anthony Eugene
  | title = The Imperial and Royal Austro-Hungarian Navy
  | url = https://books.google.com/books?id=hoUgAAAAMAAJ
  | year = 1968
  | publisher = U.S. Naval Institute
  | location = Annapolis, Maryland
  | oclc = 1912
  | ref = harv
  }}
* {{cite journal
  | last = Vego
  | first = Milan
  | year = 1982
  | title = The Yugoslav Navy 1918–1941
  | journal = Warship International
  | volume = 
  | issue = 4
  | pages = 342–361
  | issn = 0043-0374
  | publisher = International Naval Research Organisation
  | location = Toledo, Ohio
  | ref = harv
  }}
* {{cite book
  | last = Whitley
  | first = M. J.
  | title = Destroyers of World War Two: An International Encyclopedia
  | url = https://books.google.com/books?id=u4XfAAAAMAAJ
  | year = 1988
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-0-87021-326-7
  | ref = harv
  }}
{{refend}}
{{Yugoslav 250t-class torpedo boats}}
{{DEFAULTSORT:T1}}
[[Category:Torpedo boats of the Austro-Hungarian Navy]]
[[Category:Torpedo boats of the Royal Yugoslav Navy]]
[[Category:World War I torpedo boats of Austria-Hungary]]
[[Category:World War II naval ships of Yugoslavia]]
[[Category:Naval ships of Yugoslavia captured by Italy during World War II]]
[[Category:1913 ships]]
[[Category:Ships built in Trieste]]