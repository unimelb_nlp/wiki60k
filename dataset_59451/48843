'''James Goolnik''', BDS [[King's College London]] MSc [[Eastman Dental Hospital]], is the clinical director and founder of Bow Lane Dental Group in London.

==Academic and professional career==
Goolnik studied at [[King's College London]], qualifying in 1992. He worked for a year as a house office at King & St Georges before working in various NHS and mixed practices. James then studied for an MSc in Conservative Dentistry at the [[Eastman Dental Hospital]] under Derrick Setchel, qualifying in 1999. Between 1999, worked as an associate until 2001 when he established Bow Lane Dental Group. Goolnik launched the campaign "Heart Your Smile" on 21 October 2011, a campaign to increase attendance at the Dentist and boost the public perception of the profession.<ref>[http://dentinaltubules.com/node/2016 Heart Your Smile]</ref> He became the UK's most influential dentist in 2011 <ref>[http://www.dentistry.co.uk/news/dentistry-top-50-–-results Top 50]</ref> and 2012 as voted for by the dental community.

Goolnik is a dentist<ref>[http://www.thetimes.co.uk/tto/health/article1789518.ece Have teeth, will travel | The Times<!-- Bot generated title -->]</ref> and clinical director of the Bow Lane Dental Group in London. He opened the office in 2001 and developed it into a multi-specialist practice employing over 30 people. In 2004, Goolnik was voted onto the board of directors <ref>[https://www.bacd.com/dental-professionals/director-07.html The British Academy of Cosmetic Dentistry]</ref><ref>{{cite news| url=http://www.dailymail.co.uk/femail/article-1020666/The-market-instant-smile-makeovers-booming-We-safe-effective-really-are.html | location=London | work=Daily Mail | title=The market for instant smile makeovers is booming. We find out how safe and effective they really are – Mail Online}}</ref> of the British Academy of Cosmetic Dentistry (BACD) and became the president in 2008.

He was the past dental advisor for [[Arm & Hammer (brand)|Arm & Hammer]] in the UK.<ref>[http://www.nhsonline.net/lifestyle/article.asp?CategoryId=32&ArticleId=6133 NHS Online]</ref> His career was written as a cover story in the ''Business of Dentistry'' magazine in summer 2012.<ref>[http://www.bodmagazine.co.uk/domino/html/BoD6digi/index.html#/19/zoomed The Business of Dentistry magazine - Issue 6<!-- Bot generated title -->]</ref> He is on the board of trustees of Heart Your Smile, a [[registered charity]]. This "charity" has now been de-registered. 

==Books==
Goolnik is the author of ''Brush'', a book published in 2010 about opening, running and growing a successful dental practice. ''Brush'' reached number 1 on the Amazon bestsellers in dentistry genre on 18 March 2011. Profits from the book go to Dentaid to provide dental surgeries for the rural poor in Bantheay Meanchey Province, Cambodia.<ref name=dentaid>[http://www.dentaid.org/news/96 Brush profits to Dentaid]</ref>

==Charity==
With the help of Facing the World, James set up the charitable arm of the BACD.<ref>[https://www.bacd.com/dental-professionals/smiling.html Facing the World]</ref> Facing the World is a children’s charity that helps give children with facial deformities from developing countries the miracle of a new face and a life free from the stigma of disfigurement. So far over £17,000 has been raised for Facing the World. He worked with Dentaid to build two complete dental surgeries for the rural poor in Blantyre and Lilongwe in Malawi in 2014.<ref name=dentaid/>

His "Heart Your Smile" campaign to get more people to visit their dentist along with other trustees Seema Sharma, Simon Gambold and Roger Matthews, was alleged by many commentators to actually be a self promotion exercise and was widely derided.<ref>{{cite web|url=http://apps.charitycommission.gov.uk/Accounts/Ends06/0001147806_AC_20140228_E_C.pdf|website=Charity Commission Accounts|accessdate=21 September 2015}}</ref> Heart your smile was de-registered as a charity in November 2015.

== References ==
{{Reflist}}

== External links ==
* [http://www.jamesgoolnik.com Official website]
* [http://www.bowlanedental.com http://www.bowlanedental.com]
* [http://www.bacd.com http://www.bacd.com]
* [http://www.heartyoursmile.co.uk http://www.heartyoursmile.co.uk]
* [http://www.amazon.com/Brush-James-Goolnik/dp/0956833209/ref=tmm_pap_title_0 Amazon book page]

{{DEFAULTSORT:Goolnik, James}}
[[Category:Articles created via the Article Wizard]]
[[Category:British dentists]]
[[Category:Living people]]
[[Category:Alumni of King's College London]]