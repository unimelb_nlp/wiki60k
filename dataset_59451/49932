{{Infobox book
| name           = Watch and Ward
| image          = Image:Watch and Ward.JPG
| caption        = First edition
| author         = [[Henry James]]
| country        = United States
| language       = English
| genre          = 
| publisher      = Houghton, Osgood and Company, [[Boston]] 
| release_date   = 29 May 1878 
| media_type     = Print ([[Serial (literature)|serial]])
| pages          = 219
}}
'''''Watch and Ward''''' is a short [[novel]] by [[Henry James]], first published as a serial in ''[[The Atlantic Monthly]]'' in 1871 and later as a book in 1878. This was James' [[debut novel|first attempt at a novel]], though he virtually disowned the book later in life. James was still in his apprentice stage as a writer, and ''Watch and Ward'' shows predictable immaturity. It's an odd, sometimes [[melodrama]]tic tale of how [[protagonist]] Roger Lawrence adopts an orphaned twelve-year-old girl, Nora Lambert, and raises her as his eventual bride-to-be. But complications ensue, sometimes in a bizarre manner. James later called ''[[Roderick Hudson]]'' (1875) his first novel instead of ''Watch and Ward''.

== Plot summary ==

Wealthy and leisured Roger Lawrence adopts twelve-year-old Nora Lambert after her father kills himself in the hotel room next to Lawrence's. Roger had refused financial assistance to the man, and he feels remorse. Nora is not a pretty child but she soon starts to develop, as does Roger's idea of eventually marrying her.

Unfortunately for Roger, once Nora matures into a beautiful young woman, she is attracted to two other men: worthless George Fenton and the somewhat [[hypocrisy|hypocritical]] minister, Hubert Lawrence (Roger's cousin). After various adventures Nora winds up in the clutches of Fenton in [[New York City]], but Roger comes to her rescue. Roger and Nora marry in a conventional happy ending.

== Key themes ==
The melodramatic doings in ''Watch and Ward'' probably caused James some embarrassment in later years, and it's easy to see why he  disowned the book and spoke of ''[[Roderick Hudson]]'' as his first novel. Still, many critics have pointed out that melodrama always held a certain fascination for James. ''Watch and Ward'' is only a particularly gauche example.

James' technique is primitive at such an early stage of his career. Nora's development into the beautiful swan from the ugly duckling is told rather than shown, and Fenton is a stock [[villain]] of the most routine kind. Still, hints of the master-to-be are apparent from the well-described scenes of New York low life and the charm that Nora eventually displays.

A [[humor]]ous side note is some of the erotic language that James slips into the novel. At one point Roger "caught himself wondering whether, at the worst, a little precursory love-making would do any harm. The ground might be gently tickled to receive his own sowing; the petals of the young girl's nature, playfully forced apart, would leave the golden heart of the flower but the more accessible to his own vertical rays." [[William James]] and [[William Dean Howells]] were uncomfortable with such [[imagery (literature)|imagery]], though Henry might have enjoyed their uneasiness.

== Critical evaluation ==
[[Critic]]s have almost unanimously agreed with James' disowning of ''Watch and Ward'' as his first novel in favor of the infinitely more substantial and impressive ''[[Roderick Hudson]]''. While Nora gets kudos as a pleasant enough ingénue, the other characters are forgettable and the [[Plot (narrative)|plot]] is too often silly.

James did revise ''Watch and Ward'' for book publication in 1878, so he wasn't completely ashamed of it at that point in his career. But he dropped the novel from his 1883 collective edition and soon seemed to want to forget about it completely.

== References ==
* ''The Novels of Henry James'' by Edward Wagenknecht (New York: Frederick Ungar Publishing Co., 1983) ISBN 0-8044-2959-6
* ''The Novels of Henry James'' by [[Oscar Cargill]] (New York: Macmillan Co., 1961)

==External links==
*{{Wikisource-inline}}
*{{Commonscat-inline}}
* [http://cdl.library.cornell.edu/cgi-bin/moa/pageviewer?coll=moa&root=/moa/atla/atla0028/&tif=00238.TIF&view=50&frames=1 Original magazine publication of ''Watch and Ward'' (1871)]
* [http://www2.newpaltz.edu/~hathawar/WatchandWardVariorum.pdf Variorum edition of 1871 magazine and 1878 book versions of ''Watch and Ward'']
* [http://www.loa.org/volume.jsp?RequestID=56&section=notes Note on the various texts of ''Watch and Ward''] at the [[Library of America]] web site

{{Henry James}}

{{DEFAULTSORT:Watch And Ward}}
[[Category:1878 novels]]
[[Category:Novels by Henry James]]
[[Category:Novels first published in serial form]]
[[Category:Debut novels]]
[[Category:19th-century American novels]]
[[Category:Works originally published in The Atlantic (magazine)]]
[[Category:Novels about orphans]]
[[Category:19th-century British novels]]