{{good article}}
{{Infobox airport
| name                = Alta Airport
| nativename          = {{smaller|''{{lang|no|Alta lufthavn}}''}}
| image               = Alta Airport.jpg
| image-width         = 300
| image2              = 
| IATA                = ALF
| ICAO                = ENAT
| type                = Public
| operator            = [[Avinor]] 
| hub                 = '''Base:''' [[Widerøe]]
| city-served         = [[Alta, Norway|Alta]], [[Norway]]
| location            = [[Elvebakken]], Alta, [[Finnmark]]
| metric-elev         = yes
| elevation-f         = 10
| elevation-m         = 3
| coordinates         = {{coord|69|58|34|N|023|22|18|E|region:NO|display=inline,title}}
| website             = {{URL|https://avinor.no/en/airport/alta-airport/}}
| pushpin_map         = Norway
| pushpin_label       = '''ALF'''
| pushpin_mapsize     = 300
| pushpin_relief      = yes
| pushpin_map_caption = 
| metric-rwy          = yes
| r1-number           = 11/29
| r1-length-f         = 7,392
| r1-length-m         = 2,253
| r1-surface          = Asphalt
| stat-year           = 2014
| stat1-header        = Passengers
| stat1-data          = 368,393
| stat2-header        = Air movements
| stat2-data          = 11,707
| stat3-header        = Cargo (tonnes)
| stat3-data          = 330
| footnotes           = Source:<ref name="info">{{cite web  |title=Airport information for ENAT  |publisher=Avinor  |url=https://www.ippc.no/norway_aip/current/aip/ad/enat/EN_AD_2_ENAT_en.pdf |accessdate=20 January 2014 }}</ref><ref name=pax>{{cite web |url=http://www.mynewsdesk.com/material/document/42080/download?resource_type=resource_document |title=Månedsrapport |publisher=[[Avinor]] |format=XLS |accessdate=13 January 2015 |year=2015}}</ref>
}}

'''Alta Airport''' ({{lang-no|Alta lufthavn}}; {{Airport codes|ALF|ENAT|p=n}}) is an international [[airport]] serving [[Alta, Norway|Alta]], a town and [[Municipalities of Norway|municipality]] in [[Finnmark]] county, [[Norway]]. The airport is located at [[Elvebakken]], {{convert|4|km|sp=us}} northeast of [[Bossekop]] in Alta.<ref name=transport /> It has a single, {{convert|2253|m|0|sp=us|adj=on}} runway numbered 11/29, which lies on the southern shore of the [[Altafjord]]. Alta Airport is owned and operated by the state-owned [[Avinor]], and served 368,393 passengers in 2014, making it the busiest airport in [[Finnmark]].

The airport is served by [[Norwegian Air Shuttle]] and [[Scandinavian Airlines]] (SAS) with [[Boeing 737]] aircraft on flights to [[Tromsø]] and [[Oslo]]. [[Widerøe]] operates services to several regional airports in Finnmark, feeding to the larger airlines'routes. There are a limited number of international charter flights. The first airport in the area was a military airfield built by the ''[[Luftwaffe]]'' in 1943, but was damaged beyond repair during [[World War II]]. The civilian airport opened on 4 May 1963. At first services were provided by SAS to other primary airports in the north and to Oslo, but in the 1970s, several regional airports opened in Finnmark, with Widerøe flying connecting flights to them. Alta Airport's regional importance increased in 1990, when it became a hub for the newly created [[SAS Commuter]]. Norwegian started flying to Alta in 2003 and a new terminal building was opened in 2009.

==History==

===Construction===
The first airport in Alta was built by the [[Wehrmacht]] during the [[German occupation of Norway]]. Alta was a central part of the German military activity during World War II, resulting in an airstrip being constructed at Elvebakken in 1943.<ref>Eikeset (1998): 230</ref> The airport was damaged beyond repair under [[Operation Nordlicht (1944–1945)|Operation Nordlicht]]—the German withdrawal from Finnmark in 1944. During 1945 the [[Royal Norwegian Air Force]] operated a seaplane route along the coast of Northern Norway, which included a stop at Alta. However, the route was not resumed the following summer.<ref>Eikeset (1998): 374</ref> The municipality started lobbying the air force and the civilian aviation authorities to establish an airline route to Alta. Specifically, they argued that both the water aerodrome at Bukta and an airfield at Altagård/Elvebakken be included in the national plans for future airports. The area at Altagårdsletta was freed up by the military in 1946 and zoned for use as an airport.<ref name=n375>Eikeset (1998): 375</ref>

[[File:Alta Airport 2005.jpg|thumb|left|Aerial view of the airport from 2005, before the new terminal was built. Bukta in the foreground, the mouth of the [[Altaelva]] in the background.]]
However, no progress was made with the airfield for over a decade. Instead, the water aerodrome at Bukta was upgraded. Initially the passengers were transported to the aircraft with a boat, but after a few years a [[floating dock (jetty)|floating dock]] was built. Services were only provided during five summer months, from May through September. During the rest of the year, Alta had no airline service;<ref name=n375 /> travel time to Oslo was then six days, largely by ship.<ref name=heitmann>{{cite web |url=http://www.nfo.no/nyinfo/Main/Tidligere_artikler/2003/19.06.03_luftig_eventyr.htm |title=Luftig eventyr |last=Heitmann |first=Frithjof |date=19 June 2003 |publisher=Norsk Flytekniker Organisasjon |accessdate=30 September 2012}}</ref> The route was taken over by [[Widerøe]] in 1954, who bought a [[de Havilland Canada DHC-3 Otter]] to serve a route from Tromsø via Alta to [[Hammerfest]], [[Kirkenes]] and [[Vadsø]]. In Tromsø, the routes connected with a [[Norwegian Air Lines]]' seaplane service to Oslo.<ref>Arnesen, 1984: 61–67</ref> Bukta had 2,000 annual passengers in 1957,<ref name=n377>Eikeset (1998): 377</ref> and the summer route was flown daily beginning in 1960.<ref name=n375 />

Local politicians started discussing the airfield plans again in the mid-1950s, and an airport for Alta was included in the national airport plan launched in 1956. The municipality started preparatory work, which included [[eminent domain|expropriation]] of the necessary area. At that time the plans called for two runways, {{convert|1000|and|800|m|sp=us}} long, respectively. By 1958, the municipality had also built water and sewer pipes to the airport site and had spent 60,000 [[Norwegian krone]] on the preparatory work and purchase of land.<ref name=n375 /> Central authorities were at the same time looking for the most suited site for an airport for western Finnmark, and were considering [[Lakselv Airport, Banak]] in [[Porsanger]],  a by-then-closed military air base.<ref name=n376>Eikeset (1998): 376</ref>

[[File:Alta-lufthavn.jpg|thumb|left|The old terminal building and tower]]
The location in Elvebakken was somewhat controversial in Alta, especially as the airport would be located so close to the town center, potentially causing [[noise pollution]]. An alternative at Sokkelma was considered, but this was discarded because the surrounding mountains prevented good landing conditions. The decision to build Alta Airport was made in 1961, followed by additional expropriation of land. Construction was carried out simultaneously with that of Banak and [[Kirkenes Airport, Høybuktmoen]], which combined would give Finnmark three primary airports.<ref name=n376 />

Construction started in February 1962 and cost NOK&nbsp;3.2 million.<ref name=n376 /> The runway was completed by the fall of that year, but other parts of the construction were delayed. By late April 1963, the airport was lacking fire fighting equipment and ground crew had still not received sufficient training. However, the issues were resolved, allowing the airport to open on schedule on 4 May 1963. Lakselv and Kirkenes Airport also opened the same day,<ref name=n377>Eikeset (1998): 377</ref> while [[Tromsø Airport]] opened the following year.<ref>Malmø (1997): 65</ref> Several important facilities were missing from Alta Airport when it opened; the control tower and the passenger terminal were not completed until 1964, so at first a shed was used as terminal.<ref name=n377 />

[[File:Alta-lufthavn-wideroe-dash8-300.jpg|thumb|Widerøe [[Bombardier Dash 8|Dash-8 311]] at noontime during [[polar night]]]]

===Operational history===
Services were at first operated by [[Scandinavian Airlines System]] (SAS). At the beginning the airline used 56-passenger [[Convair CV-440 Metropolitan]] aircraft, which flew flights south to Oslo and westward to Lakselv and Kirkenes—and also to Tromsø starting in 1964. The general route scheme of flying multi-legged flights from Oslo to Finnmark remained until 1990. On 7 April 1969, SAS introduced the 85-passenger [[McDonnell Douglas DC-9|Douglas DC-9-21]] jetliner on the Alta service; the last Metropolitan flew on 1 April 1970.<ref name=heitmann /> Five regional airports opened on 1 August 1974, [[Sørkjosen Airport|Sørkjosen]], [[Hammerfest Airport|Hammerfest]], [[Mehamn Airport|Mehamn]], [[Berlevåg Airport|Berlevåg]] and [[Vadsø Airport|Vadsø]], all which were connected to Alta and other primary airports by Widerøe using [[de Havilland Canada DHC-6 Twin Otter]]s. [[Honningsvåg Airport]] opened on 1 July 1977.<ref>Arnesen, 1984: 124–130</ref> SAS' traffic increased throughout the 1970s, resulting in SAS gradually increasing the frequency of its services, and later also using larger DC-9s. The [[McDonnell Douglas MD-80]] was first flown on the Alta route on 11 July 1986.<ref name=heitmann />

[[SAS Commuter]] was established in 1988 and started operations in Northern Norway in May 1990, making Alta its central [[airline hub|hub]] for Finnmark. This involved a change to the operations so that all DC-9 services from Alta to Oslo were flown non-stop. Conversely, all services to airports in Northern Norway were flown using the smaller [[Fokker 50]]. Thus SAS was able to reduce costs by no longer operating local routes with the DC-9/MD-80 and instead increased the number of flights. The change made Alta the only airport in Finnmark with direct services to Oslo.<ref>{{cite news |title=SAS får ny flybase i nord |last=Guhnfeldt |first=Cato |authorlink=Cato Guhnfeldt |date=13 September 1988 |page=21 |work=[[Aftenposten]] |language=Norwegian}}</ref> From 1989 to 1990, the number of annual aircraft movements at Alta hiked from 7,711 to 10,035.<ref name=histstat>{{cite web|url=http://www.avinor.no/tridionimages/Historikk%20tidsserier%20pr%20lufthavn%20pr%20%C3%A5r_tcm181-72654.xls |title=Tidsserier pr lufthavn pr år |publisher=[[Avinor]] |language=Norwegian |accessdate=1 October 2012 |archivedate=1 October 2012 |archiveurl=http://www.webcitation.org/6B5KUo6WX?url=http%3A%2F%2Fwww.avinor.no%2Ftridionimages%2FHistorikk%2520tidsserier%2520pr%2520lufthavn%2520pr%2520%25C3%25A5r_tcm181-72654.xls |deadurl=no |df= }}</ref>

[[File:Braathens B737-405 at Alta.jpg|thumb|Braathens took over the Oslo route in 2002, here depicted with a [[Boeing 737 Classic|Boeing 737-400]] that visited in 2000]]
Activity at Alta Airport peaked in the following years. SAS served Alta with up to ten daily services; at peak hours six aircraft were at the airport simultaneously, including two from Widerøe.<ref name=heitmann /> From 1992, SAS reintroduced direct services from Kirkenes to Oslo, and aircraft in eastern Finnmark again started feeding into Kirkenes.<ref>{{cite news |title=SAS |last=Jensen |first=Tone |date=1 October 1991 |page=24 |work=[[Nordlys]] |language=Norwegian}}</ref> The hub paradigm was thus gradually abandoned by SAS.<ref name=heitmann /> Flights at Alta gradually decreased, hitting a low of 4,935 movements (landings and take-offs) in 2000.<ref name=histstat /> The [[North Cape Tunnel]] opened in 1999, connecting [[Honningsvåg]] and [[Nordkapp]] to the mainland. This reduced travel time to Alta and people from Nordkapp started using Alta more for long-haul flights at the expense of Honningsvåg Airport.<ref name=honningsvag>{{cite web|url=http://www.avinor.no/lufthavn/honningsvag/omoss/75_Lufthavnens+historie |title=Lufthavnens historie |publisher=[[Avinor]] |accessdate=4 October 2012 |archivedate=October 7, 2012 |archiveurl=http://www.webcitation.org/6BFG8rM11?url=http%3A%2F%2Fwww.avinor.no%2Flufthavn%2Fhonningsvag%2Fomoss%2F75_Lufthavnens%2Bhistorie |deadurl=yes |df= }}</ref>

SAS bought [[Braathens]] in 2001, resulting in the latter taking over the service to Oslo starting on 1 April 2002.<ref>{{cite news |title=Lander på delt løsning |work=[[Dagens Næringsliv]] |last=Larsen |first=Trygve |date=1 February 2002 |page=4 |language=Norwegian}}</ref> All SAS Commuter services in Northern Norway were taken over by Widerøe in October 2002.<ref>{{cite news |title=SAS skal spare penger på rutenedleggelser  |last=Arnt |first=Folgerø |agency=[[Norwegian News Agency]] |page=23 |date=17 April 2002 |language=Norwegian}}</ref> [[Norwegian Air Shuttle]] started its services from Oslo to Alta on 19 August 2003, initially with three services daily.<ref>{{cite web|url=http://www.norwegian.no/om-norwegian/presse/pressemeldinger/nyhetsarkiv-2003/mange-nye-ruter-fra-19-august/ |title=Mange nye ruter fra 19. august |publisher=Norwegian Air Shuttle |language=Norwegian |year=2003 |accessdate=4 October 2012 |archivedate=4 October 2012 |archiveurl=http://www.webcitation.org/6BA3vFrME?url=http%3A%2F%2Fwww.norwegian.no%2Fom-norwegian%2Fpresse%2Fpressemeldinger%2Fnyhetsarkiv-2003%2Fmange-nye-ruter-fra-19-august%2F |deadurl=no |df= }}</ref> SAS and Braathens merged in 2004 to form [[SAS Braathens]];<ref>{{cite news |title=Lindegaard: – Vi plukker det beste fra SAS og Braathens |last=Lillesund |first=Geir |agency=[[Norwegian News Agency]] |date=10 March 2004 |page=24 |language=Norwegian}}</ref> the airline changed its name back to Scandinavian Airlines in 2007.<ref>{{cite news |title=SAS Braathens endrer navn til SAS Norge |agency=[[Norwegian News Agency]] |date=27 April 2007 |language=Norwegian}}</ref>

Plans for expanding the {{convert|1670|m2|sp=us|adj=on}} terminal facilities were first articulated by the Civil Airport Administration in the 1990s. The upgrade was given priority in a 2000 plan, but was shortly after placed on hold in favor of upgrades of Kirkenes Airport and [[Svalbard Airport, Longyear]]. By 2005 the [[Civil Aviation Authority of Norway|Civil Aviation Authority]] announced that the aircraft parking areas at Alta were too close to the runway and that the airport would lose its certification in 2008 unless it was upgraded.<ref name=ikkegodkjent>{{cite news|url=http://www.finnmarkdagblad.no/nyheter/article1871049.ece |title=Flyterminalen ikke godkjent etter 2008 |date=15 December 2005 |work=[[Finnmark Dagblad]] |language=Norwegian |accessdate=4 October 2012 |archivedate=4 October 2012 |archiveurl=http://www.webcitation.org/6BA40iBgb?url=http%3A%2F%2Fwww.finnmarkdagblad.no%2Fnyheter%2Farticle1871049.ece |deadurl=no |df= }}</ref> Another concern was the lack of capacity, as the airport could only handle 150 passengers simultaneously at two gates.<ref name=farny>{{cite news|url=http://www.finnmarkdagblad.no/nyheter/article2574483.ece |title=Alta får ny flyterminal |date=8 February 2007 |work=[[Finnmark Dagblad]] |language=Norwegian |accessdate=4 October 2012 |archivedate=4 October 2012 |archiveurl=http://www.webcitation.org/6BA43WBsM?url=http%3A%2F%2Fwww.finnmarkdagblad.no%2Fnyheter%2Farticle2574483.ece |deadurl=no |df= }}</ref>

Avinor decided in February 2007 to build a new passenger terminal and tower for NOK&nbsp;300 million. The new terminal was {{convert|5000|m2|sp=us}} and was located between the old terminal and the port facilities. It included improved facilities for security control and allowed international services.<ref name=farny /> The new terminal was put into use on 23 October 2009 and was officially opened on 25 September.<ref>{{cite news|url=http://www.finnmarkdagblad.no/nyheter/article4606934.ece |title=Åpnet flyplassen |date=25 September 2009 |work=[[Finnmark Dagblad]] |language=Norwegian |accessdate=4 October 2012 |archivedate=4 October 2012 |archiveurl=http://www.webcitation.org/6BA46Lloy?url=http%3A%2F%2Fwww.finnmarkdagblad.no%2Fnyheter%2Farticle4606934.ece |deadurl=no |df= }}</ref>

==Facilities==
[[File:Alta-lufthavn-new-terminal.jpg|thumb|left|Airside waiting hall]]
The airport is located at Elvebakken and Altagård, on the southern shore of the [[Altafjord]] and at the mouth of the river of [[Altaelva]],<ref name=n375 /> which is about {{convert|4|km|sp=us}} east of [[Bossekop]] in the town of Alta.<ref name=transport>{{cite web |url=http://www.avinor.no/en/airport/alta/tofromairport |title=Alta Airport: To/From Airport |publisher=[[Avinor]] |accessdate=30 September 2012}}</ref> Operated by the state-owned Avinor, it has a single {{convert|2253|m|sp=us|adj=on}} runway aligned 11–29 (roughly east–west), without a [[taxiway]]. Both directions are equipped with [[instrument landing system]] category I.<ref name=info /> The terminal building is {{convert|5000|m2|sp=us}} and can handle international flights.<ref name=farny /> In 2013, the airport had 368,393 passengers, 11,707 aircraft movements (landings and take-offs) and 330 tonnes of cargo, making it the busiest airport in Finnmark.<ref name=pax />

The airbus bus is operated by [[Boreal Transport]] and takes ten minutes to the town center. Taxis and car rental is also available at the airport.<ref name=transport /> There are 520 long-term parking spaces at the airport.<ref>{{cite web |url=http://www.avinor.no/en/airport/alta/tofromairport/Parking |title=Parking |publisher=[[Avinor]] |accessdate=30 September 2012}}</ref> From Alta to Hammerfest there are both coach and fast ferry services;<ref name=toi9>Lian (2008): 9</ref> travel time to Hammerfest is two hours.<ref>Lian (2008): 2</ref>

==Airlines and destinations==
[[File:SAS-boeing-737-600-alta-airport-alf.jpg|thumb|[[Boeing 737 Next Generation|Boeing 737-600]] of [[Scandinavian Airlines]]]]
[[File:Alta-lufthavn-new-terminal-conveyor-belt.jpg|thumb|Arrival area with baggage conveyor]]
Alta Airport is served by three scheduled airlines and two charter airlines, providing services to eleven destinations, including two abroad.<ref name=destinations>{{cite web |url=http://www.avinor.no/en/airport/alta/timetables |title=Flight Timetables |publisher=[[Avinor]] |accessdate=30 September 2012}}</ref> Both [[Scandinavian Airlines]] and [[Norwegian Air Shuttle]] serve the airport using [[Boeing 737]] aircraft,<ref>Lian (2008): 25</ref> each providing two daily services to [[Oslo]] and one daily service to [[Tromsø]].<ref name=destinations /> [[Widerøe]] operates regional services in Finnmark on a [[public service obligation]] contract with the [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]]. However, its seven daily services to Tromsø are not subsidized. Widerøe operates [[de Havilland Canada Dash 8]] aircraft on their services, using the 50-passenger Dash&nbsp;8-300 aircraft to Tromsø and the 39-passenger Dash&nbsp;8-100 aircraft on services to regional airport.<ref>Draagen (2011): 11</ref>

Alta Airport's catchment area includes [[Kautokeino]] ({{convert|135|km|sp=us|disp=or}} by road), which has no airport. The catchment area partially includes [[Hammerfest]] ({{convert|137|km|sp=us|disp=or}} by road) and [[Kvalsund]] ({{convert|106|km|sp=us|disp=or}}). Both of these areas have [[Hammerfest Airport]] as their local airport, but the regional airport does not provide direct flights to Oslo and the prices to Tromsø are higher.<ref name=toi9 /> In 2007, an estimated 81,000 passenger-flights at Alta Airport were generated by people living in Hammerfest and Kvalsund.<ref name=toi9 /> Also journeys to and from other parts of western Finnmark to Alta Airport are often made via Alta, for example from the area around [[Honningsvåg Airport]].<ref name=honningsvag /> The pass at Sennalandet is sometimes closed during snow storms.<ref>{{cite news|url=http://www.finnmarken.no/lokale_nyheter/article5933069.ece |title=Melder mye vind |date=7 December 2012 |work=[[Finnmark Dagblad]] |language=Norwegian |accessdate=4 November 2012 |archivedate=4 November 2012 |archiveurl=http://www.webcitation.org/6BvtqSKt9?url=http%3A%2F%2Fwww.finnmarken.no%2Flokale_nyheter%2Farticle5933069.ece |deadurl=no |df= }}</ref>

{{Airport-dest-list
| [[Aegean Airlines]]|  '''Charter:''' [[Chania Airport|Chania]]
| [[BH Air]]| '''Charter:''' [[Burgas Airport|Burgas]]
| [[Corendon Airlines]]| '''Charter:''' [[Bodø Airport|Bodø]], [[Milas–Bodrum Airport|Milas–Bodrum]]
| [[Norwegian Air Shuttle]] | [[Oslo–Gardermoen]] <br>'''Seasonal:''' [[Tromsø Airport|Tromsø]]
| [[Scandinavian Airlines]] | [[Oslo–Gardermoen]], [[Tromsø Airport|Tromsø]]
| [[Widerøe]] | [[Hammerfest Airport|Hammerfest]], [[Kirkenes Airport, Høybuktmoen|Kirkenes]], [[Lakselv Airport, Banak|Lakselv]], [[Mehamn Airport|Mehamn]], [[Tromsø Airport|Tromsø]], [[Vadsø Airport|Vadsø]]
}}

==Future==
A proposal by a consultant company, ordered by the Ministry of Transport and Communications in 2011, suggested that starting in 2013 the subsidized routes in Finnmark should follow a coastal route, leaving those to [[Kirkenes]] and the county capital of [[Vadsø]] as the only subsidized routes remaining at Alta.<ref>Draagen (2011): 28</ref> There are proposals to build a new airport for Hammerfest at Grøtnes, either with a {{convert|1999|or|1199|m|sp=us}} runway. If the former is selected, Hammerfest could serve direct flights to Oslo. This would severely reduce Alta's catchment area as a primary airport and could result in a reduction in Oslo flights.<ref>Lian (2008): II</ref> The Norwegian government has decided not to build the Hammerfest-Grøtnes airport, and instead improve the Alta-Hammerfest road which started 2015.<ref>[http://www.vegvesen.no/Riksveg/rv94skaidihammerfest Rv. 94 Skaidi–Hammerfest] (Norwegian)</ref> Also the roads towards Kautokeino and Tromsø are being improved during the period 2010–2019.

==References==
{{reflist|30em}}

===Bibliography===
{{commons category|Alta Airport}}
* {{cite book |last=Arnesen |first=Odd |title=På grønne vinger over Norge |year=1984 |publisher=Widerøe's Flyveselskap |language=Norwegian}}
* {{cite web
| url         = http://www.regjeringen.no/Upload/SD/Vedlegg/rapporter_og_planer/2012/rapp_ruter_2013.pdf 
| title       = Alternative ruteopplegg for Finnmark og Nord Troms 
| last        = Draagen 
| first       = Lars 
| last2       = Wilsberg 
| first2      = Kjell 
| publisher   = Gravity Consult 
| year        = 2011 
| language    = Norwegian 
| accessdate  = 1 October 2012 
| archivedate = 1 October 2012 
| archiveurl  = http://www.webcitation.org/6B5OfdMM6?url=http%3A%2F%2Fwww.regjeringen.no%2FUpload%2FSD%2FVedlegg%2Frapporter_og_planer%2F2012%2Frapp_ruter_2013.pdf 
| deadurl     = no 
| format      = PDF 
| df          = 
}}
* {{cite book |last=Eikeset |first=Kjell Roger |title=Altas historie bind 3: Dramatisk tiår 1920–1964 |publisher=Alta kommune |year=1998 |isbn=82-992148-3-1 |language=Norwegian}}
* {{cite book |url=https://www.toi.no/getfile.php/Publikasjoner/T%D8I%20rapporter/2008/973-2008/973-hele%20rapporten%20internett.pdf |title=Ny Hammerfest lufthavn – marked, samfunnsøkonomi og ringvirkninger |last=Lian |first=Jon Ingen |last2=Rønnevik |first2=Joachim |last3=Thune-Larsen |first3=Harald |publisher=[[Institute of Transport Economics]] |year=2008 |series=TØI Rapport |volume=973 |language=Norwegian |accessdate=1 October 2012 |archivedate=1 October 2012 |archiveurl=http://www.webcitation.org/6B5KQPOTR?url=https://www.toi.no/getfile.php/Publikasjoner/T%EF%BF%BDI%20rapporter/2008/973-2008/973-hele%20rapporten%20internett.pdf |deadurl=no |isbn=978-82-480-0900-9 |format=PDF}}
* {{cite book |last=Malmø |first=Morten |title=Norge på vingene! |year=1997 |publisher=Adante Forlag |isbn=82-91056-13-7 |language=Norwegian}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}

[[Category:Avinor airports]]
[[Category:Airports in Finnmark]]
[[Category:Alta, Norway]]
[[Category:1963 establishments in Norway]]
[[Category:Airports established in 1963]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Military installations in Finnmark]]