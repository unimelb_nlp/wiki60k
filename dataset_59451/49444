{{Infobox artist
| name          = Liz Magor
| image         = 
| image_size    = 
| alt           = 
| caption       = 
| birth_name    = 
| birth_date    = {{Birth year and age|1948}}
| birth_place   = [[Winnipeg]], [[Manitoba]], Canada
| death_date    = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place   = 
| resting_place = 
| resting_place_coordinates = <!-- {{Coord|LAT|LON|type:landmark|display=inline,title}} -->
| nationality   = Canadian
| spouse        = 
| field         = Sculpture, photography
| training      = 
| alma_mater    = [[University of British Columbia]]
| movement      = 
| works         = 
| patrons       = 
| awards        = 
| website       = <!-- {{URL|Example.com}} -->
}}
'''Liz Magor''' (born 1948 in [[Winnipeg]], [[Manitoba]]) is an award winning senior Canadian visual artist. She is well known for her sculptures that address themes of history, shelter and survival through  objects that reference still life, domesticity and wildlife. She often re-purposes domestic objects such as blankets and is known for using mold making techniques. She had a career as a respected educator at the [[OCAD University|Ontario College of Art and Design]] before moving to Vancouver to continue her teaching at the [[Emily Carr University of Art and Design]] where she continued to be major influence on a younger generation of artists.  She received the [[Audain Prize for Lifetime Achievement in the Visual Arts]] in 2009.<ref>{{cite web|url=http://www.vancouversun.com/Magor+wins+Audain+Prize/1478042/story.html|title=Liz Magor wins Audain Prize|website=www.vancouversun.com|language=en-ca|last1=Griffin|first1=,Kevin|accessdate=21 March 2017}}</ref> She received the [[Gershon Iskowitz#Winners of the Gershon Iskowitz Prize|Gershon Iskowitz Prize]] at the AGO in 2014.<ref>{{Cite news|url=http://canadianart.ca/news/liz-magor-wins-50k-gershon-iskowitz-prize/|title=Liz Magor Wins $50K Gershon Iskowitz Prize - Canadian Art|work=Canadian Art|access-date=2017-03-04|language=en-US}}</ref> 

==Biography==
Magor was born in [[Winnipeg]], [[Manitoba]] in 1948. She is currently based in [[Vancouver]]. Magor studied at the [[University of British Columbia]] from 1966-1968, and [[Parsons The New School for Design|Parson’s School of Design]] in New York from 1968-1970. Subsequently, she completed her diploma at the [[Vancouver School of Art]] in 1971.<ref>“Discover must-known artists from across Canada.” Canadian Art. Accessed June 7, 2013. [http://www.canadianart.ca/artist/liz-magor/]</ref> Magor won the sixth annual [[Michael Audain#Audain_Prize|Audain Prize]] in 2009 for lifetime achievement in visual art;<ref>”Vancouver artist Liz Magor wins B.C.’s Audain prize”. CBC News. April 8, 2009. [http://www.cbc.ca/news/arts/artdesign/story/2009/04/08/audain-prize.html]</ref> she was also awarded the [[Governor General's Awards in Visual and Media Arts|Governor General's Award in Visual and Media Arts]] in 2001.<ref name=GG2001>{{cite web|title=2001 Winners|url=http://ggavma.canadacouncil.ca/archive/2001/winners|website=Governor General's Awards in Visual and Media  Arts|accessdate=25 March 2015}}</ref> In 2014 she was the recipient of the [[Gershon Iskowitz]] Prize.<ref>{{cite web|title=Vancouver’s Liz Magor wins the 2014 Gershon Iskowitz Prize|url=http://www.ago.net/vancouvers-liz-magor-wins-the-2014-gershon-iskowitz-prize|website=AGO|accessdate=29 October 2015}}</ref> She taught at [[Emily Carr University of Art and Design]] in Vancouver from 2000 to 2013.

Magor's internationally exhibited and produced work usually takes the form of [[sculpture]] and [[photography]]. Magor’s work has been presented at [[Museum of Contemporary Art San Diego|The Museum of Contemporary Art]], [[San Diego]]; the [[Museum of Modern Art, Antwerp]], [[Belgium]];  the [[National Gallery of Canada]], [[Ottawa]]; Marburger Kunstverein, [[Marburg]], [[Germany]]; the [[Vancouver Art Gallery]], Vancouver; [[The Power Plant]], [[Toronto]]; and the XLI Biennale di [[Venezia]], [[Italy]].<ref>Bradley, Jessica.“Liz Magor”.The Canadian Encyclopedia. Accessed June 7, 2013.[http://www.thecanadianencyclopedia.com/articles/liz-magor]</ref>

==Art Practice==
[[File:Vancouver Folly coal harbour.jpg|thumb|right|''LightShed'' in 2006]]
Liz Magor works in [[sculpture]], installation, [[public art]] and [[photography]]. Her sculptural work investigates the [[ontology]] of ordinary or familiar objects, which she remakes and presents in new contexts.<ref>Emily Carr University of Art + Design. “Liz Magor – Faculty Bio”. Accessed June 7, 2013. [http://www.ecuad.ca/people/profile/14472]</ref>  For example, Magor has created facsimiles of food items and their containers, as well as other objects such as driftwood, logs, tree stumps,and clothing.<ref>[http://catrionajeffries.com/wp-content/uploads/press/magor_brown_2009.pdf Brown, Nicholas. “Liz Magor”. Hunter & Cook 04, 2009]</ref>  A studio- and object-oriented artist, Magor’s work emphasizes process and materiality, and highlights the difference the real and the simulated.

In previous work, Magor used [[Molding (process)|mould-making]] and casting techniques to make replicas of coats, trays and cutlery (which she calls “serviceable objects”) as receptacles for other materials (such as candies or cigarettes).<ref>Woodley, E.C. “Real Dead Ringers: The Art of Liz Magor”. Border Crossings, Issue 117, March 2011. [http://bordercrossingsmag.com/article/real-dead-ringers-the-art-of-liz-magor]</ref> These works reference the accumulation of discarded goods and [[vice]]s that appeal to our common impulses. They also raise questions about the social and emotional life of objects. Magor’s more recent work involves the repurposing of used clothing and old wool blankets (other types of “serviceable objects”).<ref>Milroy, Sarah. “Liz Magor’s artistic salvage operation”. The Globe and Mail. November 30, 2012 [http://m.theglobeandmail.com/arts/art-and-architecture/liz-magors-artistic-salvage-operation/article5838328/?service=mobile]</ref>

In her article entitled ''Magor's Timeless Transitions'', Robin Laurence writes, “Art, Liz Magor says, is the place where our perceptions are opened and examined for prolonged periods of time. Much longer, she suggests, than in our day-to-day encounters with the visual world, where we tend to interpret given signs in fixed ways, and where our first impressions are usually consolidated by our second [impressions]. Magor's art refutes such consolidation: irresolution prevails and closure eludes us. Her sculptures consistently play reality against unreality, meaning against alternative meaning, initial appearance against later revelation.”<ref>Robin Laurence. “[http://www.straight.com/arts/magors-timeless-transitions Magor’s Timeless Transitions]”. Georgia Straight, December 16, 2004</ref>

== Bibliography ==
Books by Liz Magor
* {{Cite book|url=http://www.worldcat.org/title/mouth-and-other-storage-facilities-liz-magor/oclc/318166711&referer=brief_results|title=The mouth and other storage facilities: Liz Magor.|last=Magor|first=Liz|last2=Henry Art Gallery|last3=Simon Fraser Gallery|date=2008-01-01|publisher=Henry Art Gallery Association ; Simon Fraser University Gallery|isbn=0935558470|location=Seattle, WA; [Burnaby, BC|language=English}}

==References==
{{reflist}}

==External links==
* Art in America article by E. C. Woodley (http://www.artinamericamagazine.com/reviews/liz-magor/)
* Information on the 2011 exhibition at Susan Hobbs Gallery (http://susanhobbs.com/exhibits/88-liz-magor)

==Further reading==
*[http://data.logograph.com/SusanHobbs/docs/Document/307/magor_artforum_2007_summer.pdf Adler, Dan. "Liz Magor: Susan Hobbs Gallery". Artforum, Summer 2007]
*Arnold, Grant; and Monk, Philip; et al. Liz Magor. Toronto/Vancouver: The Power Plant/Vancouver Art Gallery, 2002
*Campbell, Deborah. The Outlaw. Canadian Art, Summer 2009, 42-47
*Dault, Gary Michael. Look closer to grasp Molly’s Reach. The Globe and Mail, 12 March 2005
*Feinstein, Roni. Report from Toronto: Opening Doors. Art in America, no.11(November 1994), 38-47
*Gopnik, Blake. ’Flaws’ point to artist’s crucial theme: artificiality. The Globe and Mail, 2 September 2000
*Heth, Shannon. Wilderness Escape—Liz Magor and Emily Carr. Montecristo, Autumn 2010, 54-5
*Krajewski, Sara and Jeffries, Bill. Liz Magor: The Mouth and other storage facilities. Seattle/Vancouver: The Henry Gallery/Simon Fraser University Gallery, 2008
*Lafo, Rachel Rosenfield. The Potency of Ordinary Objects: A Conversation with Liz Magor. Sculpture Magazine, November 2012, 36-41
*Laurence, Robin. Material Intelligence: The Art of Liz Magor, Border Crossings, vol.22, no.86(2003), 36-41
* {{Cite book| publisher = Mousse Publishing and Triangle France| isbn = 978-88-6749-170-4| last1 = Magor| first1 = Liz| last2 = Kopp| first2 = Céline| last3 = Robertson| first3 = Lisa| last4 = Verwoert| first4 = Jan| title = Liz Magor: the blue one comes in black| date = 2015}}
*Marshall, Lisa. Liz Magor. Canadian Art, Spring 2013
*Monk, Philip. Liz Magor, Equinox Gallery. C Magazine, September–November 1999
*Nicholas, Vanessa. Liz Magor: Blanket Statements. Canadian Art.ca, posted 5 May 2011
*Tousley, Nancy. Liz Magor. Canadian Art, 17.1(Spring 2000), 70-74
*Tousley, Nancy; Hogg, Lucy; and Shier, Reid. Liz Magor. Vancouver/Toronto: Contemporary Art Gallery/Art Gallery of York University, 2000.



{{Authority control}}

{{DEFAULTSORT:Magor, Liz}}
[[Category:1948 births]]
[[Category:Artists from Winnipeg]]
[[Category:Canadian installation artists]]
[[Category:Canadian photographers]]
[[Category:Canadian sculptors]]
[[Category:Canadian women artists]]
[[Category:Living people]]
[[Category:Canadian contemporary artists]]
[[Category:Governor General's Award in Visual and Media Arts winners]]