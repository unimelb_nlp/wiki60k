{{Infobox journal
| title = Acta Societatis Botanicorum Poloniae
| cover = File:Acta Societatis Botanicorum Poloniae Vol. 85 No. 4 cover.jpg
| frequency = Quarterly
| publisher = [[Polish Botanical Society]]
| country = Poland
| editor = Beata Zagórska-Marek
| discipline = [[Botany]]
| history = 1923–present
| openaccess = Yes
| license = [[Creative Commons license|CC-BY-4.0]]
| impact = 1.213
| impact-year = 2015
| website = http://pbsociety.org.pl/journals/index.php/asbp/index
| link1 = https://pbsociety.org.pl/journals/index.php/asbp/issue/current
| link1-name = Online access
| link2 = http://pbsociety.org.pl/journals/index.php/asbp/issue/archive
| link2-name = Online archive
| ISSN = 0001-6977
| eISSN = 2083-9480
| OCLC = 667893187
}}
'''''Acta Societatis Botanicorum Poloniae''''' is a quarterly [[peer-reviewed]] [[scientific journal]] published by the [[Polish Botanical Society]]. It covers all areas of [[botany]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.213.<ref name=WoS>{{cite book |year=2016 |chapter=Acta Societatis Botanicorum Poloniae |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref>

The subsequent [[editors-in-chief]] have been: Dezydery Szymkiewicz (1922–1930), Kazimierz Bassalik (1930–1937), Kazimierz Piech (1938–1939), Dezydery Szymkiewicz and Kazimierz Bassalik (1945–1947), Kazimierz Bassalik (1948–1949), Kazimierz Bassalik and Wacław Gajewski (1949–1960), Wacław Gajewski and Henryk Teleżyński (1960–1977), Bohdan Rodkiewicz (1980–1990), Stefan Zajączkowski (1991–1992), Jerzy Fabiszewski (1993–2010), and Beata Zagórska-Marek (since 2011).

Since 2016, the journal is available exclusively as an online edition.

==References==
{{Reflist}}

==External links==
{{Commons category|Images from Acta Societatis Botanicorum Poloniae}}
*{{Official website|http://pbsociety.org.pl/journals/index.php/asbp/index}}
*[http://pbsociety.org.pl Polish Botanical Society]

[[Category:Botany journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1923]]