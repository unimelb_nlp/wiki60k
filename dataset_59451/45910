The '''86th Aeromedical Evacuation Squadron''' (86 AES) is a unit of the [[United States Air Force]]. It is part of the [[86th Operations Group]], [[86th Airlift Wing]] at [[Ramstein Air Base]], Germany. It is a component of [[Third Air Force]] and [[United States Air Forces Europe]] of the United States Air Force.

The 86 AES provides operational aeromedical evacuation for U.S. troops in the [[United States European Command]] and [[United States Africa Command]] theaters using primarily [[Boeing C-17 Globemaster III]], [[Boeing KC-135 Stratotanker]], [[Learjet 35|Gates Learjet C-21A]] and [[Lockheed Martin C-130J Super Hercules]] aircraft. The unit is manned by Flight Nurses, Medical Service Corps officers and Aeromedical Evacuation Technicians; as well as medical administration and logistics technicians.

==History==
'''Unit Designations'''
* 7416th Medical Air Evacuation Group, 1 July 1954
* 2nd Aeromedical Evacuation Group, 8 February 1957
:Control of the 2d AEG was transferred to the 375 Aeromedical Airlift Wing at Scott AFB on 1 April 1975. It was subsequently renamed to a like numbered squadron.<ref>{{cite web|last=Office of History|first=375 Airlift Wing|title=375 Air Mobility Wing Pamphlet of History |url=http://www.scott.af.mil/shared/media/document/AFD-091103-039.pdf|accessdate=27 February 2014}}</ref><ref name="Nightingale">{{cite web|author=Drummer, Janene|author2=Wilcoxson, Katherine|last-author-amp=yes|title=A Chronological History of the C-9A Nightingale|url=http://www.amc.af.mil/shared/media/document/AFD-131018-054.pdf|work=Office of History|publisher=Air Mobility Command}}</ref>
* 2d Aeromedical Evacuation Squadron, 1 July 1975
* 86th Aeromedical Evacuation Squadron, 16 August 1994

'''Unit Locations'''
* [[Ramstein Air Base|Ramstein AB]], Germany, 1 July 1993 <ref name="Silvano">{{cite web|last=Wueschner|first=Silvano, 86 AW Historian|title=July: A Month of Great Significance|url=http://www.kaiserslauternamerican.com/july-a-month-of-great-significance/|work=Kaiserslautern American|publisher=86th Airlift Wing Public Affairs}}</ref> 
* [[Rhein-Main Air Base|Rhein-Main AB]], Germany, 15 September 1958 <ref name="Silvano" />
* [[Évreux-Fauville Air Base|Évreux-Fauville AB]], France, 8 April 1957
* Ramstein AB, Germany, 1 July 1954

==Commanders==
{| class="wikitable"
|-
! No. !! Image !! Name !! Began !! Ended
|-
| 1 || ||[[Colonel Kelly M. Christy]] || May 2014 || Present
|-
|}

==Superintendents==
{| class="wikitable"
|-
! No. !! Image !! Name !! Began !! Ended
|-
| 1 || ||[[SMSgt David Denton]] || March 2014 || Present
|-
|}

==List of Major Contingency Operations==
<big>'''86 Aeromedical Evacuation Squadron'''</big>
* [[Operation Enduring Freedom]]
* [[Operation Iraqi Freedom]]
* [[Operation Unified Protector]]<ref>{{cite web|last=Holt|first=Katherine|title=Ramstein Supports AFRICOM: Transports Wounded Libyans|url=http://www.kaiserslauternamerican.com/ramstein-supports-africom-transports-wounded-libyans/|work=Kaiserslautern American|publisher=86 AW Public Affairs|accessdate=26 February 2014}}</ref>
* [[Operation Unified Response]]<ref>{{cite web|last=Snead|first=Pablo|title=86th AES supports operations in Haiti|url=http://www.ramstein.af.mil/news/story.asp?id=123190560|work=2/16/2010|accessdate=26 February 2014}}</ref>
* [[Operation Joint Endeavor]]<ref name="Nightingale" />
* [[Operation Allied Force]]<ref name=Nightingale />
:The 86 AES provided AE coverage for deployed US and NATO forces. This included the airlift of former prisoners of war Specialist Steven Gonzales and Staff Sergeants Christopher Stone and Andrew Ramirez, to Ramstein Air Base, Germany, from Zagreb, Croatia. They had been captured by Serbian forces while patrolling in the Republic of Macedonia, during Operation Allied Force.
* [[Operation Deliberate Force]]
* [[USS Cole bombing|Bombing of USS ''Cole'']]
:On October 12, 2000, crew from the 86 Aeromedical Evacuation Squadron and CCATT team members from Landstuhl Regional Medical launched on C-9 Nightingales from the 75th Airlift Squadron to Djibouti and Yemen. In total 28 Sailors were airlifted back to definitive care in Germany by 14 October 2000.<ref>{{cite web|title=86 Aeromedical Evacuation Squadron|url=http://www.globalsecurity.org/military/agency/usaf/86aes.htm|work=GlobalSecurity.org|accessdate=26 February 2014}}</ref>
* [[Khobar Towers bombing|Bombing of the Khobar Towers]]
<big>'''2d Aeromedical Evacuation Squadron'''</big>
* [[Operation Desert Shield/Storm]]
:As part of the 1610th Air Division
* [[Operation EARNEST WILL]]
:Crews from the 2d Aeromedical Evacuation Squadron and hospital staff from Wiesbaden Military Hospital responded via [[C-141B]] and [[C-9A]] to the [[USS Samuel B. Roberts (FFG-58)]] when it struck a mine while operating in the Persian Gulf. Four of the ship's crew were airlifted to Germany.<ref>{{cite web|last=Peniston|first=Bradley|title=86th AES supports operations in Haiti|url=http://www.navybook.com/no-higher-honor/timeline/ffg-58-the-wounded/df-st-88-08985/|accessdate=3 May 2014}}</ref>
* [[1983 Beirut barracks bombing|1983 Marine Barracks Bombing in Beirut]]<ref>{{cite web|url=http://research.archives.gov/description/6375914 |title=Members of the 2nd Aeromedical Evacuation Squadron transfer a wounded Marine to a bus after his arrival from Beirut, Lebanon, where a terrorist bomb attack destroyed the Marine barracks and headquarters building, 10/23/1983 |work=Research.archive.gov}}</ref>
<big>'''55 Aeromedical Airlift Squadron''' </big>
*  [[Iran hostage crisis]]<ref name="Nightingale" />
* [[Operation Eagle Claw]]
:Crews from the 55 AAS were dispatched to care for survivors of the failed operation.<ref>{{cite web|author=435 Air Base Wing and 86 Airlift Wing History Offices|title=A Moment in Air Force History|url=http://www.kaiserslauternamerican.com/a-moment-in-air-force-history-31/|work=Kaiserslautern American}}</ref>

==Major Unit Awards==
* Mackay Trophy <ref>{{cite web|title=Mackay Trophy: 2000-20009 Winners|url=http://naa.aero/html/awards/index.cfm?cmsid=185|publisher=National Aeronautic Association|accessdate=26 February 2014}}</ref>
* Air Mobility Rodeo 2009 - Best Aeromedical Evacuation Team <ref>{{cite web|author=Air Mobility Command|title=RODEO 2009: Winners announced for competitions|url=http://www.amc.af.mil/news/story.asp?id=123160428|work=Air Mobility Command|accessdate=27 February 2014}}</ref>

==Partnership Building==
Since it is uniquely situated among active duty USAF AE units, the 86 AES participates regularly in partnership building visits with allied nations.
* Poland <ref>{{cite web|title=Ramstein Airmen Build Capability with Polish Air Force|url=http://www.ramstein.af.mil/news/story.asp?id=123353081|work=Kaiserslautern American|publisher=86 Airlift Wing Public Affairs|accessdate=26 February 2014}}</ref>
* Germany <ref>{{cite web|title=German surgeon general visits Air Force in Germany|url=http://www.ramstein.af.mil/news/story.asp?id=123306488|work=Kaiserslautern American|publisher=86th Airlift Wing Public Affairs}}</ref>
* Norway <ref>{{cite web|last=Rhynes|first=Trevor|title=Norwegians visit critical care unit|url=http://www.kaiserslauternamerican.com/norwegians-visit-critical-care-unit/|work=Kaiserslautern American|publisher=86th Air Wing Public Affairs}}</ref>

==Historical Unit Patches==
<gallery>
File:86 AES.jpg|Current Patch for 86 AES
File:86 AES Hist.jpg|Historical Instructor Patch For 86 AES
File:2d AES Sq Patch.jpeg|Patch for the 2d AES
File:55AAS.jpg|Patch for the 55 AAS
</gallery>

==References==
{{Air Force Historical Research Agency}}
* Much of this text in an early version of this article was taken from pages on the Ramstein Air Base website, which as a work of the U.S. Government is presumed to be a public domain resource. That information was supplemented by:
{{reflist}}

==External links==
{{Portal|United States Air Force|Military of the United States}}
* [http://www.ramstein.af.mil/ Ramstein AFB Home Page]

{{USAF Air Forces in Europe}}
{{USAF Bases in Germany}}
{{US Air Force navbox}}

[[Category:United States Air Force]]
[[Category:Aeromedical evacuation squadrons of the United States Air Force]]