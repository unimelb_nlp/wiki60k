{{Infobox television
| show_name=FishCenter Live
| image=FishCenter Live logo.jpg
| image_alt=The words "fish" and "center" in capital letters surrounding a picture of a blue fish enclosed in a circle, all of which is on a blue background
| caption=
| genre=[[Comedy]]<br />[[Talk show]]
| creator=
| developer=
| writer=
| director=
| presenter={{Unbulleted list|Dave Bonawits|Andrew Choe|[[Matt Harrigan]]|Max Simonet}}
| country=United States
| language=English
| num_episodes=600+ ({{As of|2016|1|lc=y}})
| executive_producer=
| producer=
| runtime=11 minutes <small>(television)</small>
| company=[[Williams Street]]
| channel=AdultSwim.com <small>(online)</small><br />[[Adult Swim]] <small>(television)</small>
| first_aired={{Start date|2014|09|22}}
| last_aired=present
| website=http://fishcenter.com
}}

'''''FishCenter Live''''' (also shortened to '''''FishCenter''''') is an American [[talk show]] hosted by Dave Bonawits, Andrew Choe, [[Matt Harrigan]], and Max Simonet.  It premiered on the official website of [[Adult Swim]] in September 2014; it started airing on the network proper in February 2015.

==Summary and production==
''FishCenter Live'' is presented as a call-in [[talk show]] narrating over footage of [[Tropical fish|tropical]] fish swimming around in a [[fish tank]].<ref name="Lindvall 2015; Jurgensen 2015a" /> The fish are ranked according to their points, which are awarded when the fish complete a number of challenges.<ref name="Lindvall 2015" /> These challenges include floating over coins that are superimposed on the video feed. The show is hosted by Dave Bonawits, Andrew Choe, [[Matt Harrigan]],{{Efn|Harrigan, a creative director at the network, was first hired at [[Turner Broadcasting System]] in 1994. ''[[The Wall Street Journal]]'' observed this relationship as a rule of management to not "get hung up on hierarchy and protocol" at the studio.<ref name="Jurgensen 2015b" />}} and Max Simonet, employees of [[Adult Swim]], from the network's digital department room. An original incarnation of the show involved a straight video feed of the fish tank, without narration. Commentary and a phone number for call-in segments were later added. Initially, callers were mostly other Adult Swim employees; when the competition portion was added, the hosts saw an increase of outside callers.<ref name="Jurgensen 2015a" />

===Animal cast===
{| class="wikitable" style="font-size:90%;"
! Name
! Species
! Tank
|-
| Greenbird
| [[Gomphosus varius|Bird wrasse]]
| Thrillerdome
|-
| Th'Lump
| [[White-spotted puffer]]
| Bubble Team
|-
| Hamburger 
| [[Zebra moray]]
|Thrillerdome
|-
| Mimosa
| [[Queen coris]]
| Thrillerdome
|-
| Jeremy Legg
| [[Cuban hogfish]]
| Bubble Team
|-
| Slider
| [[Sailfin tang]]
| Bubble Team
|-
| Kip Clawford (formerly Hot Steve (formerly Lupin the Third))
| [[Foxface rabbitfish]]
| Bubble Team
|-
| Styletoy
| [[Flame Angelfish]]
| Thrillerdome
|-
| TopXanderCupper
| [[Pinktail Triggerfish]]
| Thrillerdome
|-
| Ale
| [[Squirrelfish]]
| Thrillerdome
|-
| Mom
| [[Golden Puffer]]
| Thrillerdome
|}

===Former cast===
{| class="wikitable collapsible autocollapse" style="font-size:90%;"
! Name
! Species
! Reason for departure
|-
| Dottie
| [[Clown triggerfish]]
| Death (February 19, 2016)
|-
| Long Donovan
| [[Yellow-brown wrasse]]
| Death (August 26, 2016)
|-
| Mammoth
| [[Harlequin tuskfish]]
| Death (September 6, 2016)
|-
| Ronside
| [[Majestic angelfish]]
| Death (September 21, 2016)
|-
| Sir Squirt
| [[Lagoon triggerfish]]
| Death (October 3, 2016)
|-
|  "Yo Hal Look At That Tang" Tang
| [[Sohal surgeonfish]]
| Removed from tank due to size and aggression
|-
| Ol' Blue
| [[Holacanthus passer|King angelfish]]
| Removed from tank due to size and aggression/Sold at fish store
|-
|}


===2016 Season Winners===
{| class="wikitable" style="font-size:90%;"
| '''Winter''': Hamburger

| '''Spring''': Hamburger
| '''Summer''': Greenbird
| '''Atum''': Mimosa
|'''Galaxy:''' Greenbird
|}

===2017 Season Winners===
{| class="wikitable" style="font-size:90%;"
| '''Winter''': Th'Lump
| '''Spring''': TBD
| '''Summer''': TBD
| '''Atum''': TBD
|'''Galaxy:''' TBD
|}

==Broadcast and reception==
{{Quote box
| quote=What is our version of a sports show? What is our version of a relationship Q&A show? You can test these things out, just as we did with ''FishCenter'', in an office with a couple microphones.
| source=—[[Mike Lazzo]], [[Adult Swim]] executive<ref name="Jurgensen 2015a" />
| align=right
| width=25em
}}
''FishCenter Live'' was originally released in September 2014 on Adult Swim's official website.<ref name="Jurgensen 2015a; Jurgensen 2015b" /> The show came about when staff decided to film the tropical fish swimming around their aquarium as an idea for developing content for the website's online streaming channels. New episodes are presented on weekdays. The show was added to the network proper in February 2015, broadcast at 4&nbsp;a.m. These airings are condensed versions of the live version, consisting of 11-minute highlights from each day.<ref name="Jurgensen 2015a" />{{Efn|These broadcast versions have also been uploaded to the network's [[YouTube]] channel.<ref name="Lindvall 2015" />}}

In the first week of its televised broadcast, the show garnered 2.6 million viewers in total.<ref name="Jurgensen 2015a" /> In a press release, the network ranked the program first place across all targeted demographics in its time slot during the second week of March 2015.<ref name="Kondolojy 2015" /> The network observed some of these viewers as confused [[Twitter]] users, wondering if the show was a prank. After these airings, live viewership rose from 120 to 5,000. The success led to the creation of a separate live stream dedicated to the network's [[Toonami]] block.<ref name="Jurgensen 2015a" />

Critical reception has been positive. Eric Lindvall of ''[[The A.V. Club]]'' called ''FishCenter'' "the latest weird thing" to come from the network "in a streak of really weird things", describing it as a "wonderful, web-based world of piscine sports".<ref name="Lindvall 2015" />


==Explanatory notes==
{{Notelist}}

==References==
{{Reflist|colwidth=30em|refs=
<ref name="Jurgensen 2015a">{{Cite web
| last1=Jurgensen
| first1=John
| date=March 12, 2015
| url=https://www.wsj.com/articles/adult-swim-how-to-run-a-creative-hothouse-1426199501
| title=Adult Swim: How to Run a Creative Hothouse
| work=The Wall Street Journal
| publisher=Dow Jones & Company
| archiveurl=https://web.archive.org/web/20150313041704/http://www.wsj.com/articles/adult-swim-how-to-run-a-creative-hothouse-1426199501
| archivedate=March 13, 2015
| accessdate=March 14, 2015
}}</ref>
<ref name="Jurgensen 2015a; Jurgensen 2015b">{{Cite web
| last1=Jurgensen
| first1=John
| date=March 12, 2015
| url=https://www.wsj.com/articles/adult-swim-how-to-run-a-creative-hothouse-1426199501
| title=Adult Swim: How to Run a Creative Hothouse
| work=The Wall Street Journal
| publisher=Dow Jones & Company
| archiveurl=https://web.archive.org/web/20150313041704/http://www.wsj.com/articles/adult-swim-how-to-run-a-creative-hothouse-1426199501
| archivedate=March 13, 2015
| accessdate=March 14, 2015
}}
* For "September", see: {{Cite web
| last1=Jurgensen
| first1=John
| date=March 12, 2015
| url=https://www.wsj.com/articles/shop-rules-at-adult-swim-1426195416
| title=Shop Rules at Adult Swim
| work=The Wall Street Journal
| publisher=Dow Jones & Company
| archiveurl=https://web.archive.org/web/20150313035214/http://www.wsj.com/articles/shop-rules-at-adult-swim-1426195416
| archivedate=March 13, 2015
| accessdate=March 14, 2015
}}</ref>
<ref name="Jurgensen 2015b">{{Cite web
| last1=Jurgensen
| first1=John
| date=March 12, 2015
| url=https://www.wsj.com/articles/shop-rules-at-adult-swim-1426195416
| title=Shop Rules at Adult Swim
| work=The Wall Street Journal
| publisher=Dow Jones & Company
| archiveurl=https://web.archive.org/web/20150313035214/http://www.wsj.com/articles/shop-rules-at-adult-swim-1426195416
| archivedate=March 13, 2015
| accessdate=March 14, 2015
}}</ref>
<ref name="Lindvall 2015">{{Cite web
| last1=Lindvall
| first1=Eric
| date=February 11, 2015
| url=http://www.avclub.com/article/discover-wonderful-web-based-world-piscine-sports--215065
| title=Discover the Wonderful, Web-Based World of Piscine Sports with Adult Swim's ''FishCenter Live''
| work=The A.V. Club
| publisher=Onion Inc.
| archiveurl=https://web.archive.org/web/20150211200704/http://www.avclub.com/article/discover-wonderful-web-based-world-piscine-sports--215065
| archivedate=February 11, 2015
| accessdate=March 14, 2015
}}</ref>
<ref name="Lindvall 2015; Jurgensen 2015a">{{Cite web
| last1=Lindvall
| first1=Eric
| date=February 11, 2015
| url=http://www.avclub.com/article/discover-wonderful-web-based-world-piscine-sports--215065
| title=Discover the Wonderful, Web-Based World of Piscine Sports with Adult Swim's ''FishCenter Live''
| work=The A.V. Club
| publisher=Onion Inc.
| archiveurl=https://web.archive.org/web/20150211200704/http://www.avclub.com/article/discover-wonderful-web-based-world-piscine-sports--215065
| archivedate=February 11, 2015
| accessdate=March 14, 2015
}}
* For "tropical", see: {{Cite web
| last1=Jurgensen
| first1=John
| date=March 12, 2015
| url=https://www.wsj.com/articles/adult-swim-how-to-run-a-creative-hothouse-1426199501
| title=Adult Swim: How to Run a Creative Hothouse
| work=The Wall Street Journal
| publisher=Dow Jones & Company
| archiveurl=https://web.archive.org/web/20150313041704/http://www.wsj.com/articles/adult-swim-how-to-run-a-creative-hothouse-1426199501
| archivedate=March 13, 2015
| accessdate=March 14, 2015
}}</ref>
<ref name="Kondolojy 2015">{{Cite web
| last1=Kondolojy
| first1=Amanda
| date=March 10, 2015
| url=http://tvbythenumbers.zap2it.com/2015/03/10/monthly-ratings-notes-for-adult-swim-cartoon-network-tbs-tnt-nba-tv-rizzoli-isles-cougar-town-nba-basketball-more/373533/
| title=Monthly Ratings Notes for Adult Swim, Cartoon Network, TBS, TNT & NBA TV: ''Rizzoli & Isles'', ''Cougar Town'', NBA Basketball & More
| work=TV by the Numbers
| publisher=Tribune Digital Ventures
| archiveurl=https://web.archive.org/web/20150314051012/http://tvbythenumbers.zap2it.com/2015/03/10/monthly-ratings-notes-for-adult-swim-cartoon-network-tbs-tnt-nba-tv-rizzoli-isles-cougar-town-nba-basketball-more/373533/
| archivedate=March 14, 2015
| accessdate=March 14, 2015
}}</ref>
}}

==Further reading==
* {{Cite web
| last=Kopcow
| first=Chris
| date=May 29, 2015
| url=http://splitsider.com/2015/05/adult-swims-fishcenter-a-beginners-guide/
| title=Adult Swim's ''FishCenter'': A Beginner's Guide
| work=Splitsider
| publisher=The Awl
| archiveurl=https://web.archive.org/web/20150831031731/http://splitsider.com/2015/05/adult-swims-fishcenter-a-beginners-guide/
| archivedate=August 31, 2015
}}
* {{Cite web
| date=May 29, 2015
| url=https://www.theguardian.com/culture/2015/jun/06/populist-the-guide
| title=Populist: Items of interest this week 
| work=The Guardian
| publisher=Guardian News and Media Limited
| archiveurl=https://web.archive.org/web/20150606110329/http://www.theguardian.com/culture/2015/jun/06/populist-the-guide
| archivedate=June 6, 2015
}}
* {{Cite web
| last=Franks
| first=Nico
| date=March 25, 2015
| url=http://www.c21media.net/adult-swim-floats-new-app/
| title=Adult Swim floats new app 
| work=C21 Media
| publisher=C21 Media Limited
| archiveurl=https://web.archive.org/web/20150606110329/http://www.theguardian.com/culture/2015/jun/06/populist-the-guide
| archivedate=June 6, 2015
}}

==External links==
* {{Official website|fishcenter.com}}

{{Adult Swim original programming}}

[[Category:2014 American television series debuts]]
[[Category:2010s American television series]]
[[Category:Adult Swim original programs]]
[[Category:American television talk shows]]
[[Category:Television series about animals]]
[[Category:Fish in popular culture]]
[[Category:Television series by Williams Street]]