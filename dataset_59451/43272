<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=MC-1
 | image=Mitsubishi MC-1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Biplane]] [[airliner]]
 | national origin=Japan
 | manufacturer=[[Mitsubishi Aircraft Company]]
 | designer=
 | first flight=1928
 | introduced=
 | retired=1938
 | status=
 | primary user=
 | number built=1
 | developed from= [[Mitsubishi B1M]]
 | variants with their own articles=
}}
|}
The '''Mitsubishi MC-1''' was a 1920s [[Japan]]ese single-engined [[biplane]] [[airliner]] designed and built by the  [[Mitsubishi Aircraft Company]].<ref name="orbis" />

==Design and development==
In 1927, the Japanese Department of Communications launched a competition to design and build an indigenous passenger transport aircraft. Mitsubishi's design to meet this requirement was based on its [[Mitsubishi B1M]] torpedo bomber, using the wings of the earlier aircraft combined with a new fuselage.<ref name="Abe p193"/> The MC-1 was large three-bay biplane powered by a {{convert|385|hp|kW|0|abbr=on}} [[Armstrong Siddeley Jaguar]] [[radial engine]] and it had an open cockpit behind the wings for the pilot and room for four passengers in an enclosed cabin in the forward fuselage.<ref name="orbis" /> The MC-1 had a fixed [[conventional landing gear]] but could also be fitted with twin floats.<ref name="orbis" /><ref name="Abe p193"/>

The MC-1 was completed in April 1928, and was evaluated against the other two competitors, the [[Aichi AB-1]] and [[Nakajima N-36]], both of which were also biplanes.  No production followed of any of the aircraft, as they were considered obsolete compared with foreign types, and the state-owned airline [[Japan Air Transport]] (''Nihon Koko Yuso KK'') ordered [[Fokker Universal]] [[monoplane]]s instead.<ref name="Abe p193"/>

==Operational history==
Although no production of the MC-1 followed, the prototype was used to operate an experimental air service between [[Tokyo]] and [[Osaka]] sponsored by the ''[[Asahi Shimbun]]'' newspaper between June 1928 and April 1929, and then by Japan Air Transport for services in [[Korea]] until May 1930.  It was then used as a seaplane flying sightseeing flights around the north coast of [[Honshu]] until 1938.<ref name="Abe p193"/>

==Specifications (Landplane)==
{{Aircraft specs
|ref=Japanese Aircraft 1910–1941<ref name="Abe p193">Mikesh and Abe 1990, p. 193.</ref>
|prime units?=met 
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=4 passengers
|length m=10.475
|length ft=
|length in=
|length note=
|span m=14.75
|height m=3.80
|wing area sqm=59
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=1550
|empty weight lb=
|empty weight note=
|gross weight kg=2600
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Armstrong Siddeley Jaguar]]
|eng1 type=14-cylinder air-cooled [[radial engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=385<!-- prop engines -->
|eng1 note=
<!--
        Performance
-->

|max speed kmh=
|max speed mph=
|max speed kts=103
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|range km=
|range miles=
|range nmi=
|range note=
|ferry range note=
|endurance=6 hr
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=3,000 m (9850 ft) in 20 min 36 s
|more performance=
|avionics=
}}

==See also==
{{aircontent|
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Mitsubishi B1M]]<!-- related developments -->
|similar aircraft=*[[Aichi AB-1]]
|lists=
<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{commons category|Mitsubishi military aircraft}}

===Notes===
{{reflist|refs=
<ref name="orbis">Orbis 1985, p. 2534</ref>
}}

===Bibliography===
{{refbegin}}
*{{cite book|last1=Mikesh|first1=Robert C|last2=Abe|first2=Shorzoe|title=Japanese Aircraft 1910–1941|year=1990|publisher=Putnam|location=London|isbn=0-85177-840-2}}
*{{cite book |last= |first= |authorlink= |coauthors= |title= The [[Illustrated Encyclopedia of Aircraft]] (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=|pages=}}
{{refend}}

{{Mitsubishi aircraft}}

[[Category:Mitsubishi aircraft|MC-01]]
[[Category:Japanese airliners 1920–1929]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]