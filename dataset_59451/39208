{{Taxobox
| image = Calvatia craniiformis.JPG
| image_width = 234px
| regnum = [[Fungus|Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Agaricales]]
| familia = [[Agaricaceae]]
| genus = ''[[Calvatia]]''
| species = '''''C. craniiformis'''''
| binomial = ''Calvatia craniiformis''
| binomial_authority = ([[Schwein.]]) [[Elias Magnus Fries|Fr.]] ex [[De Toni]] (1888)
| synonyms_ref = <ref name="urlMycoBank: Calvatia craniiformis"/>
| synonyms =
''Bovista craniiformis'' <small>Schwein. (1832)</small><br>
''Calvatia craniiformis'' <small>(Schwein.) Fr. (1849)</small><br>
''Calvatia craniformis'' <small>([[orthographic variant]])</small>
}}

'''''Calvatia craniiformis''''',{{efn|'''''Calvatia craniformis''''' is an [[orthographic variant]] spelling.}} commonly known as the '''brain puffball''' or the '''skull-shaped puffball''', is a species of [[puffball]] fungus in the family [[Agaricaceae]]. It is found in Asia, Australia, and North America, where it grows on the ground in open woods. Its name, derived from the same [[Latin]] root as ''[[cranium]]'', alludes to its resemblance to an animal's [[brain]]. The skull-shaped [[basidiocarp|fruit body]] is {{convert|8|–|20|cm|in|0|abbr=on}} broad by {{convert|6|–|20|cm|in|0|abbr=on}} tall and white to tan. Initially smooth, the skin ([[peridium]]) develops wrinkles and folds as it matures, cracking and flaking with age. The peridium eventually sloughs away, exposing a powdery yellow-brown to greenish-yellow [[spore]] mass (the [[gleba]]). The puffball is [[edible mushroom|edible]] when the gleba is still white and firm, before it matures to become yellow-brown and powdery. Mature specimens have been used in the traditional or folk medicines of China, Japan, and the [[Ojibwe]] as a [[hemostatic]] or wound dressing agent. Several [[bioactivity|bioactive]] compounds have been isolated and identified from the brain puffball.

==Taxonomy==

The species was first described as ''Bovista craniiformis'' by [[Lewis David de Schweinitz]] in 1832.<ref name="Schweinitz 1832"/> [[Elias Fries]] transferred it to the then newly circumscribed genus ''[[Calvatia]]'' in 1849,<ref name="Fries 1849"/> setting ''Calvatia craniiformis'' as the [[type species|type]] and only species.{{efn|Index Fungorum indicates that although this name is [[valid name (botany)|invalid]] according to Article 33.1 of the [[International Code of Nomenclature for algae, fungi, and plants]], it is a [[conserved name]].<ref name="urlFungorum: Calvatia"/>}} Scott Bates and colleagues suggest that the name is [[synonym (taxonomy)|synonymous]] with ''Lycoperdon delicatum'' published by [[Miles Joseph Berkeley]] and [[Moses Ashley Curtis]] in 1873 (not the ''L.&nbsp;delicatum'' published by Berkeley in 1854), as is ''Lycoperdon missouriense'' published by [[William Trelease]] in 1891.<ref name="Bates et al. p.170">Bates ''et al''. (2009), p. 170.</ref> The [[form (botany)|form]] ''C.&nbsp;craniiformis'' f. ''gardneri'', published by [[Yosio Kobayasi]] in 1932 (originally ''Lycoperdon gardneri'' Berk. 1875),<ref name="Kobayasi 1932"/> since been elevated to the distinct species ''[[Calvatia gardneri]]''.<ref name="urlFungorum: Calvatia craniiformis var. gardneri"/>

In their 1962 monograph on North American ''Calvatia'', mycologists [[Sanford Myron Zeller]] and [[Alexander H. Smith]] set ''C.&nbsp;craniiformis'' as the type species of the stirps (a grouping of related species) ''Craniiformis'', containing species with a large sterile base and a persistent cottony gleba. Other species they included in this stirps were ''[[Calvatia umbrina|C.&nbsp;umbrina]]'', ''[[Calvatia diguetti|C.&nbsp;diguetti]]'', ''[[Calvatia lycoperdoides|C.&nbsp;lycoperdoides]]'', ''[[Calvatia rubroflava|C.&nbsp;rubroflava]]'', ''[[Calvatia ochrogleba|C.&nbsp;ochrogleba]]'', ''[[Calvatia excipuliformis|C.&nbsp;excipuliformis]]'' (since transferred by some authorities to ''[[Handkea]]''), and ''[[Calvatia elata|C.&nbsp;elata]]''.<ref name="Zeller 1964"/>

''Calvatia craniiformis'' is [[common name|commonly]] known as the "brain-shaped puffball"<ref name="Coker 1974"/> or the "skull-shaped puffball".<ref name="Lincoff 1981"/> The [[botanical name|specific epithet]] ''craniiformis'' derives from the [[Ancient Greek]] words ''cranion'', meaning "brain", and ''forma'', "a form".<ref name="Hard 1908"/>

==Description==
[[File:Calvatia craniiformis 308991.jpg|thumb|left|Cluster of fruit bodies found in New Zealand]]
The [[basidiocarp|fruit bodies]] of ''Calvatia craniiformis'' grow to dimensions of {{convert|6|–|20|cm|in|abbr=on}} tall by {{convert|8|–|20|cm|in|abbr=on}} wide, and have a form ranging from pear-shaped,<ref name="Miller 2006"/> to flattened-spherical, to obovate (roughly egg-shaped), to tunicate (like an inverted cone). At the bottom is a thick, often crumpled base attached to a cord-like [[rhizomorph]],<ref name="Coker 1974"/> which is often encrusted with surrounding soil.<ref name="Bates et al. p.170"/> The rhizomorphs are well developed, and when cut into longitudinal section, reveal three distinct tissues: an outer cortex, a subcortical layer, and a central core.<ref name="Swartz 1935"/> The thin and fragile [[peridium|exoperidium]] (the outer layer of "skin") is whitish-gray to gray, and initially smooth before becoming areolate (divided by cracks into discrete areas). The base extends up one-third to one-half way into the puffball (where it becomes the columnella) tapering to a point.<ref name="Coker 1974"/> The [[gleba]] is initially whitish, and then yellow-green, and finally brownish-green in older specimens with mature [[basidiospore|spores]].<ref name="Miller 2006"/>

Spores are spherical, [[hyaline]] (translucent),<ref name="Miller 2006"/> and measure 2.5–3.4&nbsp;[[micrometre|μm]] in diameter.<ref name="Portman 1997"/> They are thick-walled with a short pedicel (a tubelike extension),<ref name="Miller 2006"/> and are ornamented with tiny spines (verrucae) that are roughly equidistant from each other.<ref name="Portman 1997"/> [[Capillitium|Capillitial]] threads are long, hyaline, and branched, measuring 2.4–4&nbsp;μm thick.<ref name="Coker 1974"/> They are [[septum|septate]] and occasionally have pits on their walls.<ref name="Miller 2006"/> The exoperidium comprises thick-walled, inflated [[hypha]]e mixed with sphaerocysts (spherical cells), while the endoperidium is made of tightly interwoven, thick-walled hyphae.<ref name="Bates et al. p.170"/> In the rhizomorphs, the hyphae in the central core are several times as thick as those in the surrounding subcortex.<ref name="Swartz 1935"/>
[[File:Calvatia craniiformis 429869.jpg|thumb|right|upright=1.5|Spores and capillitial threads]]
Using light microscopy, the spores of ''Calvatia craniiformis'' are generally indistinguishable from those of ''[[Calvatia rubroflava|C.&nbsp;rubroflava]]'' and ''[[Calvatia gigantea|C.&nbsp;gigantea]]''; electron microscopy reveals that each has distinctive spore ornamentation. ''C.&nbsp;craniiformis'' features small, well-separated verrucae (wartlike projections) up to 0.2&nbsp;μm tall with rounded tips. In comparison, ''C.&nbsp;gigantea'' has larger verrucae (up to 0.4&nbsp;μm tall) that are more irregularly arranged.<ref name="Levetin 1992"/>

===Development===

Fruit bodies arise from the tips or the lateral branches or rhizomorphs. New fruit bodies are made of an exterior of thick hyphae similar to those found in the rhizomorph core; in contrast, the hyphae of the interior originate from the puffball's core. Unlike some other puffball species, the peridium does not differentiate into a distinct exoperidium and endoperidium; rather, the outer layer develops the features of a pseudoparenchyma (a tightly organised tissue where the tightly packed cells resemble plant [[parenchyma]]) as the radial and tangential hyphae become interweaved. In time, the peridium dries and falls off in flakes to expose the underlying gleba.<ref name="Swartz 1935"/>

===Similar species===

The brain-like surface folds and mature olive-brown gleba are characteristic of ''Calvatia craniiformis'', but younger puffballs that have not yet developed these characteristics may be difficult to identify to species. Another edible puffball, ''[[Calvatia cyathiformis|C.&nbsp;cyathiformis]]'' (lilac puffball) grows to similar dimensions but has gleba that is purple-brown when mature. ''[[Calvatia fragilis]]'' is smaller and pink or purple mature gleba.<ref name="Miller 2006"/> ''[[Calvatia bicolor|C.&nbsp;bicolor]]'' is a smaller, rounder puffball that could be confused with younger specimens of ''C.&nbsp;craniiformis'', but the former species has more coarsely ornamented spores, and lacks a distinct subgleba.<ref>Bates ''et al''. (2009), p. 168.</ref> ''[[Handkea utriformis]]'' is roughly similar in appearance to ''C.&nbsp;craniiformis'', but unlike the latter it develops a cavernous opening to reveal an olive-brown gleba, and has distinct slits in its capillitial threads.<ref>Bates ''et al''. (2009), p. 171.</ref>

==Uses==
[[File:Calvatia craniiformis 102452.jpg|thumb|right|''Calvatia craniiformis'' is edible when the gleba is firm and white, unlike the pictured specimen.]]
''Calvatia craniiformis'' is an [[edible mushroom|edible]] species.<ref name="Boa 2004"/> Young puffballs with a firm, white gleba have a mild odor and pleasant taste.<ref name="Miller 2006"/> Early 20th-century mycologist [[Charles McIlvaine (mycologist)|Charles McIlvaine]] noted over a century ago that "the slightest change to yellow makes it bitter."<ref name="McIlvaine 1912"/> Versatile in cooking, the puffball absorbs flavors well.<ref name="urlMushroomExpert"/>

In the United States, the [[Ojibwe]] used the powdery gleba as a [[antihemorrhagic|hemostatic agent]] to staunch the flow of nosebleeds:<ref name="Densmore 1928"/> the spore powder was inhaled through the nostrils. It is now known that this practice can lead to the [[pulmonary]] disease [[lycoperdonosis]], which causes symptoms similar to [[pneumonia]].<ref name="Burk 1983"/> It is also used as a hemostatic agent in [[Traditional Chinese medicine|Chinese]] and [[Kampo|Japanese]] folk medicines.<ref name="Takaishi 1997"/>

==Habitat and distribution==

Although ''Calvatia craniiformis'' is generally considered a [[saprobic]] species, in controlled laboratory conditions, an [[ectomycorrhiza]]e between the fungus and [[American sweetgum]] (''Liquidambar styraciflua'') was reported in a 1966 publication. A Chinese study showed that ''C.&nbsp;craniifromis'' would readily form mycorrhiza with [[Populus|poplar]] seedlings on unsterilized, but not on sterilized soil.<ref name="Ma 2007"/> Later research was unable to establish any similar association between ''C.&nbsp;craniiformis'' and ''[[Pinus ponderosa]]''.<ref name="Coetzee 2009"/> Brain puffballs grow singly or in groups in fields and open woods, hardwood forests, and wet areas.<ref name="Coker 1974"/><ref name="Miller 2006"/> In Asia, it has been recorded from China,<ref name="Hsiao 2010"/> India,<ref name="Abrar 2008"/> Indonesia,<ref name="Kasuya 2006"/> Japan,<ref name="Bates et al. p.170"/> Malaysia,<ref name="urlAIMST"/> and South Korea.<ref name="Jung 1995"/> The brain puffball has been recorded from Australia.<ref name="urlRBGM"/> In North America, its range includes the eastern and southern United States,<ref name="Miller 2006"/> and Mexico.<ref name="Esqueda 2009"/> In [[Michigan]], it is one of the few macrofungi found regularly in [[Robinia pseudoacacia|black locust]] plantations.<ref name="urlMushroomExpert"/> Fruit bodies serve as a food source for several species of [[fly|flies]].<ref name="Hosaka 2012"/>

==Research==

Extracts of the puffball have strong antitumor activity in [[mouse model]]s attributable to protein-bound [[polysaccharide]]s, the compounds [[calvatan]], [[craniformin]], and a [[tautomer]] of [[rubroflavin]]. Calvatan is thought to act by [[Immunostimulant|stimulating]] the immune response, rather than by [[cytotoxicity|killing cells]].<ref name="Coetzee 2009"/> Craniformin, originally reported in 1997, is an azoformamide compound. Three [[sterol]] compounds have been identified from the fungus: ergosta-4,6,8(14), 22-tetraene-3-one, ergosta-7,22-diene-3-ol, and [[ergosterol peroxide]].<ref name="Takaishi 1997"/>

Three azo- and azoxy[[formamide]]s compounds have been isolated and identified from the puffball and tested for their ability to inhibit the growth of plants: 4-methoxybenzene-1-''ONN''-azoxyformamide, 4-methoxybenzene-1-azoformamide, and 4-hydroxybenzene-1-azoformamide. Only the first compound was significantly inhibitory, suggesting that the presence of the azoxy [[Functional group|moiety]] is required for growth inhibition.<ref name="Kamo 2006"/>

==Notes==
{{notelist}}

==References==

{{Reflist|colwidth=30em|refs=

<ref name="Abrar 2008">{{cite journal |vauthors=Abrar S, Swapna S, Krishnappa M |title=''Bovista aestivalis'' and ''Calvatia craniiformis'' – new records to India |journal=Journal of Mycology and Plant Pathology |year=2008 |volume=38 |issue=3 |pages=504–6 |issn=0971-9393}}</ref>

<ref name="Boa 2004">{{cite book |author=Boa E. |title=Wild Edible Fungi: A Global Overview of Their Use and Importance to People |series=Non-Wood Forest Products |volume=17 |publisher=Food & Agriculture Organization of the UN |year=2004 |page=132 |isbn=978-92-5-105157-3 |url=https://books.google.com/books?id=Zd2NlcNZgvcC&pg=PA132}}</ref>

<ref name="Burk 1983">{{cite journal |author=Burk WR. |title=Puffball usages among North American Indians |journal=Journal of Enthnobiology |year=1983 |volume=3 |issue=1 |pages=55–62 |url=http://biodiversitylibrary.org/page/32863473}}</ref>

<ref name="Coetzee 2009">{{cite journal |vauthors=Coetzee JC, van Wyk AE |title=The genus ''Calvatia'' ('Gasteromycetes', Lycoperdaceae): A review of its ethnomycology and biotechnological potential |journal=African Journal of Biotechnology |year=2009 |volume=8 |issue=22 |pages=6607–15}}</ref>

<ref name="Coker 1974">{{cite book |vauthors=Johnson MM, Coker WS, Couch JN |title=The Gasteromycetes of the Eastern United States and Canada |publisher=Dover Publications |location=New York |year=1974 |origyear=First published 1928 |page=67  |isbn=978-0-486-23033-7}}</ref>

<ref name="Densmore 1928">{{cite book|author=Densmore F. |title=Strength of the Earth: The Classic Guide to Ojibwe Uses of Native Plants |url=https://books.google.com/books?id=lF0OvZ5FfeQC&pg=PA356 |year=1928 |publisher=Minnesota Historical Society |isbn=978-0-87351-562-7 |page=356}}</ref>

<ref name="Esqueda 2009">{{cite journal |vauthors=Esqueda M, Sanchez A, Rivera M, Coronado ML, Lizarraga M, Valenzuela R |title=Primeros registros de hongos gasteroides en la Reserva Forestal Nacional y Refugio de Fauna Silvestre Ajos-Bavispe, Sonora, Mexico |trans_title=First records of gasteroid fungi from the Ajos-Bavispe National Forest Reserve and Wildlife Refuge, Sonora, Mexico |journal=Revista Mexicana de Micologia |year=2009 |volume=30 |pages=19–29 |language=Spanish}}</ref>

<ref name="Fries 1849">{{cite journal |author=Fries EM. |title=Summa vegetabilium Scandinaviae |year=1849 |volume=2 |pages=259–572 (see p.&nbsp;442) |publisher=Typographia Academica |location=Uppsala, Sweden |language=Latin |url=http://biodiversitylibrary.org/page/32637063}}</ref>

<ref name="Hard 1908">{{cite book |author=Hard ME. |title=The Mushroom, Edible and Otherwise, its Habitat and its Time of Growth, with Photographic Illustrations of Nearly all the Common Species: A Guide to the Study of Mushrooms, with Special Reference to the Edible and Poisonous Varieties, with a View of Opening up to the Student of Nature a Wide Field of Useful and Interesting Knowledge |year=1908 |location=Columbus |publisher=Ohio Library |page=539 |url=http://biodiversitylibrary.org/page/17027450 |doi=10.5962/bhl.title.17723}}</ref>

<ref name="Hosaka 2012">{{cite journal |vauthors=Hosaka K, Uno K |title=A preliminary survey on larval diversity in mushroom fruit bodies |journal=Bulletin of the National Museum of Nature and Science. Series B, Botany |year=2012 |volume=38 |issue=3 |pages=77–85 |issn=1881-9060}}</ref>

<ref name="Hsiao 2010">{{cite journal |vauthors=Hsiao WC, Wang YN, Chen CY |script-title=zh:溪頭鳳凰山之大型真菌調查|trans-title=The investigation of macrofungus in Fong-Huang Mountain |journal=Journal of the Experimental Forest of National Taiwan University |year=2010 |volume=24 |issue=3 |pages=209–15 |language=Chinese}}</ref>

<ref name="Jung 1995">{{cite journal |author=Jung HS. |title=Fungal flora of Ullung Island: (VI). On ascomycetous, auriculariaceous, and gasteromycetous fungi |journal=Korean Journal of Mycology |year=1995 |volume=23 |issue=1 |pages=1–9 |issn=0253-651X}}</ref>

<ref name="Kamo 2006">{{cite journal |vauthors=Kamo T, Kashiwabara M, Tanaka K, Ando S, Shibata H, Hirota M |title=Plant growth inhibitory activity of azo- and azoxyformamides from ''Calvatia craniiformis'' and ''Lycoperdon hiemale'' |journal=Natural Products Research |year=2006 |volume=20 |issue=5 |pages=507–10 |doi=10.1080/14786410600649596 |pmid=16644550}}</ref>

<ref name="Kasuya 2006">{{cite journal |author=Kasuya T. |title=New or noteworthy species of the genus ''Calvatia'' Fr. (Basidiomycota) with probable medicinal value from Indonesia |journal=International Journal of Medicinal Mushrooms |year=2006 |volume=8 |issue=3 |pages=283–8 |doi=10.1615/IntJMedMushr.v8.i3.100}}</ref>

<ref name="Kobayasi 1932">{{cite journal |author=Kobayasi Y. |title=Fungi Austro-Japoniae et Micronesiae II |journal=Botanical Magazine Tokyo |year=1937 |volume=51 |pages=797–804}}</ref>

<ref name="Levetin 1992">{{cite journal |vauthors=Levetin E, Horner WE, Lehrer SB |title=Morphology and allergenic properties of basidiospores from four ''Calvatia'' species |journal=Mycologia |year=1992 |volume=84 |issue=5 |pages=759–67 |jstor=3760386 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0084/005/0759.htm |doi=10.2307/3760386}}</ref>
<ref name="Lincoff 1981">{{cite book |author=Lincoff G. |title=North American Mushrooms |series=The Audubon Society Field Guide |year=1981 |publisher=Knopf |location=New York |page=822 |isbn=978-0-394-51992-0}}</ref>

<ref name="Ma 2007">{{cite journal |vauthors=Ma L, Wu XQ |title=Research on mycorrhizal formation of nine ectomycorrhizal fungi with poplar seedlings |journal=Journal of Nanjing Forestry University (Natural Sciences Edition) |year=2007 |volume=31 |issue=6 |pages=29–33 |issn=1000-2006}}</ref>

<ref name="McIlvaine 1912">{{cite book |author=McIlvaine C. |title=One Thousand American Fungi |year=1912 |publisher=Bobbs-Merrill |location=Indianapolis |page=587 |url=http://biodiversitylibrary.org/page/21068624 |doi=10.5962/bhl.title.26569 }}</ref>

<ref name="Miller 2006">{{cite book |vauthors=Miller HR, Miller OK |title=North American Mushrooms: A Field Guide to Edible and Inedible Fungi |publisher=Falcon Guide |location=Guilford, Connecticut |year=2006 |page=459 |isbn=0-7627-3109-5 |url=https://books.google.com/books?id=zjvXkLpqsEgC&pg=PA459}}</ref>

<ref name="Portman 1997">{{cite journal |vauthors=Portman R, Moseman R, Levetin E |title=Ultrastructure of basidiospores in North American members of the genus ''Calvatia'' |journal=Mycotaxon |year=1997 |volume=62 |pages=435–43 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0062/0435.htm}}</ref>

<ref name="Schweinitz 1832">{{cite journal |author=von Schweinitz LD. |title=Synopsis fungorum in America boreali media degentium |journal=Transactions of the American Philosophical Society |year=1832 |volume=4 |issue=2 |pages=141–316 (see p.&nbsp;256) |url=https://archive.org/stream/transactionsofam04amer#page/256/mode/2up |doi=10.2307/1004834}}</ref>

<ref name="Swartz 1935">{{cite journal |author=Swartz D. |title=The development of ''Calvatia craniiformis'' |journal=Mycologia |year=1935 |volume=27 |issue=5 |pages=439–48 |jstor=3754214 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0027/005/0439.htm |doi=10.2307/3754214}}</ref>

<ref name="Takaishi 1997">{{cite journal |vauthors=Takaishi Y, Murakami Y, Uda M, Ohashi T, Hamanura N, Kido M, Kadota S |title=Hydroxyphenylazoformamide derivatives from ''Calvatia craniformis'' |journal=Phytochemistry |year=1997 |volume=45 |issue=5 |pages=997–1101 |doi=10.1016/S0031-9422(97)00066-6}}</ref>

<ref name="urlAIMST">{{cite web |title=Medicinal mushroom in AIMST |url=http://www.myaimst.com/2012/01/medicinal-mushroom-in-aimst/ |author=Nelson J. |year=2011 |accessdate=2013-10-23}}</ref>

<ref name="urlFungorum: Calvatia">{{cite web |title=''Calvatia'' Fr., Summa veg. Scand., Section Post. (Stockholm): 442 (1849) |url=http://www.indexfungorum.org/names/NamesRecord.asp?RecordID=19051 |publisher=CAB International |accessdate=2013-10-23}}</ref>

<ref name="urlFungorum: Calvatia craniiformis var. gardneri">{{cite web |title=''Calvatia craniiformis'' var. ''gardneri'' (Berk.) Kobayasi, Bot. Mag., Tokyo 51: 803 (1937) |url=http://www.indexfungorum.org/names/NamesRecord.asp?RecordID=487079 |publisher=CAB International |accessdate=2013-10-23}}</ref>

<ref name="urlMushroomExpert">{{cite web |title=''Calvatia craniiformis'' |url=http://www.mushroomexpert.com/calvatia_craniiformis.html |publisher=MushroomExpert.com |author=Meyers R. |date=October 2003 |accessdate=2013-10-23}}</ref>

<ref name="urlMycoBank: Calvatia craniiformis">{{cite web |title=''Calvatia craniiformis'' (Schwein.) Fr. ex De Toni :106, 1888 |url=http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=410622&Fields=All |publisher=[[MycoBank]]. International Mycological Association |accessdate=2013-10-23}}</ref>

<ref name="urlRBGM">{{cite web |title=Calvatia craniiformis (Schwein.) Fr., Summa Veg. Scand. : 442 (1849) |url=http://www.rbg.vic.gov.au/dbpages/cat/index.php/fungicatalogue/name/7584 |publisher=Royal Botanic Gardens of Melbourne |accessdate=2013-10-23}}</ref>

<ref name="Zeller 1964">{{cite journal |vauthors=Zeller SM, Smith AH |title=The genus ''Calvatia'' in North America |journal=Lloydia |volume=27 |pages=148–86 (see p. 173)}}</ref>

}}

===Cited literature===

*{{cite journal |vauthors=Bates ST, Roberson RW, Desjardin DE |title=Arizona gasteroid fungi I: Lycoperdaceae (Agaricales, Basidiomycota) |journal=Fungal Diversity |year=2009 |volume=37 |pages=153–207 |url=http://www.fungaldiversity.org/fdp/sfdp/FD37-4.pdf}}

==External links==
*{{IndexFungorum|487335}}
{{Good article}}

[[Category:Agaricaceae]]
[[Category:Edible fungi]]
[[Category:Fungi described in 1832]]
[[Category:Fungi of Asia]]
[[Category:Fungi of Australia]]
[[Category:Fungi of North America]]
[[Category:Fungi used in traditional Chinese medicine]]