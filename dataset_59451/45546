{{Use dmy dates|date=April 2012}}
{{Infobox military person
| name          =Alec Stratford Cunningham Reid
| image         =
| caption       =
| birth_date          = <!-- {{Birth date and age|df=yes|YYYY|MM|DD}} -->1895
| death_date          = <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} -->26 March 1977
| placeofburial_label = 
| placeofburial = 
| birth_place  =[[Wayland, Norfolk]], United Kingdom
| death_place  =[[Valbonne]], France
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    ={{UK}}
| branch        =[[British Army]]<br>[[Royal Air Force]]
| serviceyears  =
| rank          =Captain
| unit          =[[Royal Engineers]]<br>[[No. 85 Squadron RAF]]
| commands      =
| battles       =[[World War I|First World War]]
| awards        =[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
| relations     =
| laterwork     =Member of Parliament
}}
Captain '''Alec Stratford Cunningham-Reid''' [[Distinguished Flying Cross (United Kingdom)|DFC]] (1895 – 26 March 1977<ref>{{cite web |url=http://www.leighrayment.com/commons/Scommons1.htm |title=Historical list of MPs: House of Commons constituencies beginning with "S" |work=Leigh Rayment's House of Commons pages |accessdate=21 November 2009| archiveurl= https://web.archive.org/web/20091220043003/http://www.leighrayment.com/commons/Scommons1.htm| archivedate= 20 December 2009 <!--DASHBot-->| deadurl= no}}</ref>), known in his early life as '''Alec Stratford Reid''', was a [[United Kingdom|British]] [[World War I|First World War]] [[flying ace]]<ref>{{cite web|url=http://www.theaerodrome.com/aces/by_name.php?pageNum_names=1&totalRows_names=101&KT_az=R|title=The Aces of World War 1|work=TheAerodrome.com|accessdate=28 November 2009}}</ref> credited with seven aerial victories.<ref name="the-aerodrome">{{cite web|url=http://www.theaerodrome.com/aces/england/reid1.php|title=Alec Reid|work=TheAerodrome.com|accessdate=21 November 2009}}</ref> After the war, he entered politics as a [[Conservative Party (UK)|Conservative]], serving as a [[Member of Parliament]] (MP) for periods between 1922 and 1945.

== Early life ==
Cunningham-Reid was born in [[Wayland, Norfolk]],<ref name="the-aerodrome" /> the son of the Reverend Arthur Morse Reid and his wife Agnes Celina Flower (1861–1942).<ref name="thepeerage.com">{{cite web |url=http://thepeerage.com/p2942.htm#i29412|title=Captain Alec Stratford Cunningham-Reid |last=Lundy |first=Darryl |work=ThePeerage.com |accessdate=21 November 2009}}</ref>

He joined the [[Royal Engineers]] during the [[World War I|First World War]] and was commissioned as a [[Second Lieutenant]], transferring to the [[Royal Flying Corps]].<ref name="the-aerodrome" />  In August 1918, he was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]], the citation reading:
{{cquote
| Lt. Alec Cunningham Reid (formerly R. E.).<br />When engaging a column of infantry at a very low altitude, this officer saw a hostile balloon on the ground. This he attacked and burnt. On the two following days he shot down two aeroplanes, and a few days later destroyed a third."
|<!-- quote width in pixels -->||Citation as published in the Supplement to the [[London Gazette]], 3 August 1918 (30827/9203)<ref name="gazette-30827">{{London Gazette
 | date = 3 August 1918
 | issue = 30827
 | startpage = 9203
 | supp= yes
 | accessdate=21 November 2009
}}</ref>
}}

== Political career ==
At the [[United Kingdom general election, 1922|1922 general election]], Cunningham-Reid stood as the conservative candidate in [[Warrington (UK Parliament constituency)|Warrington]], a Conservative-held [[borough constituency]] in [[Lancashire]] where the sitting member [[Harold Smith (British politician)|Sir Harold Smith]] was retiring.  He won the seat with a comfortable majority in a two-way contest with the [[Labour Party (UK)|Labour Party]] candidate.  However, at the [[United Kingdom general election, 1923|next general election, in 1923]], the addition of a [[Liberal Party (UK)|Liberal Party]] candidate saw him lose to Labour's [[Charles Dukes, 1st Baron Dukeston|Charles Dukes]].<ref name="craig1918-1949a">{{cite book
 |last=Craig
 |first=F. W. S.
 |authorlink= F. W. S. Craig
 |title=British parliamentary election results 1918–1949
 |origyear=1969
 |edition=3rd
 |year=1983
 |publisher= Parliamentary Research Services
 |location=Chichester
 |isbn= 0-900178-06-X
 |page=267
}}</ref>

The Liberals in Warrington did not field a candidate at the [[United Kingdom general election, 1924|1924 general election]], and Reid was returned to the [[House of Commons of the United Kingdom|House of Commons]] for the next five years.<ref name="craig1918-1949a" /> At the [[United Kingdom general election, 1929|1929 election]] he did not stand in Warrington, but instead sought election in [[Southampton (UK Parliament constituency)|Southampton]]. This was a two-seat constituency, where both the sitting members were Conservatives not seeking re-election. Having returned only Conservatives since 1922, this might have been regarded as safer Conservative territory than Warrington, but Labour won both seats.<ref>Craig, op. cit. page 243</ref>

Cunningham-Reid's next chance to return to the Commons came in 1932, when the Conservative member [[Rennell Rodd, 1st Baron Rennell|Sir Rennell Rodd]] [[resignation from the British House of Commons|resigned]] from [[Parliament of the United Kingdom|Parliament]]. This caused a [[by-election]] in his [[inner London]] constituency of [[St Marylebone (UK Parliament constituency)|St Marylebone]], where Cunningham-Reid was adopted as the candidate of the St Marylebone Conservative and Constitutional Union, which was the official Conservative and Unionist organisation in the area. However, a number of local Conservatives who opposed his adoption left to form the rival St Marylebone Conservative Association and nominated their own candidate, Sir B. P Blackett.  It was customary for the Conservative Party leader (then [[Stanley Baldwin]]) to send a letter of support to the party's candidate, but both Blackett and Cunningham-Reid each claimed to be the official Conservative nominee, and Baldwin did not endorse either of them.  No other candidates were nominated, so the election became a two-way contest between the rival Conservatives.<ref name="craig-st-m">Craig, op. cit., page 43</ref> In the event, Cunningham-Reid won the seat with a slender majority of 1,013 (4.6% of the votes), and held it for a further thirteen years.  At the [[United Kingdom general election, 1935|1935 general election]] he was returned as the sole Conservative candidate with a huge majority over his Labour opponent.{{cn|date=November 2014}}

In 1943 the St Marylebone Conservative and Constitutional Union was disaffiliated from the [[National Union of Conservative and Unionist Associations]] in favour of the rival St Marylebone Conservative Association, which had remained in existence since the 1932 split. At the [[United Kingdom general election, 1945|1945 general election]], Cunningham-Reid retained the support of the Conservative and Constitutional Union, but was opposed by [[Wavell Wakefield, 1st Baron Wakefield of Kendal|Wavell Wakefield]], a former captain of the [[England national rugby union team]], who received the Conservative Association's endorsement.<ref name="craig-st-m" /> Without official party support, Cunningham-Reid fared poorly, finishing third with only 11% of the votes. Wavell won the seat with a comfortable majority over the second-placed Labour candidate.<ref name="craig-st-m" />

=== In Parliament ===
On 28 July 1943, Cunningham-Reid was involved in an exchange of blows in the lobby of the House of Commons with fellow Conservative MP [[Oliver Locker-Lampson]].<ref name="evening-indep">{{cite news
|url=https://news.google.com/newspapers?id=J4sLAAAAIBAJ&sjid=GlUDAAAAIBAJ&pg=3040,2900086&hl=en
|title=Two Members of Commons apologise for fist fighting
|date=28 July 1943
|work=The Evening Independent
|pages=1, 6
|accessdate=21 November 2009
}}</ref> Cunningham-Reid's description of the incident was that after a verbal dispute,
{{cquote|He (Locker-Lamspon) ran whirling his arms around his head and struck me in the chest. I retaliated by hitting him on the head. He went down on his knees. I helped him up and by that time other members had gotten between us"|<!-- quote width in pixels -->||''The Evening Independent'', 29 July 1943<ref name="evening-indep" />
}}

The following day, both MPs made a formal apology in the [[House of Commons of the United Kingdom|House of Commons]].<ref>{{cite web
|url=http://hansard.millbanksystems.com/commons/1943/jul/29/personal-statements#S5CV0391P0_19430729_HOC_382
|title=Personal statements
|date=29 July 1943
|origyear=House of Commons Debates 29 July 1943 vol 391 columns 1805-7
|work=Hansard 1803–2005
|accessdate=21 November 2009
}}</ref> On 30 July Cunningham-Reid made a personal statement in which he explained to the House that the matter had arisen after Locker-Lampson had accused him of leaving London during [[The Blitz]], whereas he claimed to have departed on a 14-week trip before The Blitz started.<ref>{{cite web
|url=http://hansard.millbanksystems.com/commons/1943/jul/30/personal-statement#S5CV0391P0_19430730_HOC_17
|title=Personal statement
|date=30 July 1943
|origyear=House of Commons Debates 30 July 1943 vol 391 columns 1931
|work=Hansard 1803–2005
|accessdate=21 November 2009
}}</ref> The incident became front-page news in Britain,<ref>{{cite news
|url=http://pqasb.pqarchiver.com/chicagotribune/access/471756962.html?dids=471756962:471756962&FMT=ABS&FMTS=ABS:AI&type=historic&date=Jul+29%2C+1943&author=&pub=Chicago+Tribune&desc=2+M.+P.s+Fight+After+Dispute+in+Commons&pqatl=google
|title=2 M. P.s Fight After Dispute in Commons
|date=29 July 1943
|work=Chicago Daily Tribune
|page=1
|accessdate=21 November 2009
|quote=A fist fight between two members of commons vied with the war news for space on the front pages of London's newspapers today
}}</ref> and was reported in several major American newspapers&nbsp;— including the ''[[Los Angeles Times]]'', which ran the story under the headline "England Grins as Members of Commons Trade Punches".<ref>{{cite news|url=http://pqasb.pqarchiver.com/latimes/access/418512161.html?dids=418512161:418512161&FMT=ABS&FMTS=ABS:AI&type=historic&date=Jul+29%2C+1943&author=&pub=Los+Angeles+Times&desc=England+Grins+as+Members+of+Commons+Trade+Punches&pqatl=google|title=England Grins as Members of Commons Trade Punches|authors=Los Angeles Times|date=29 July 1943|page=14|accessdate=21 November 2009}}</ref>

== Personal life ==
Cunningham-Reid was married twice.  His first marriage, on 12 May 1927, was to Hon. Ruth Mary Clarisse Ashley (1906-1986), daughter of Lt.-Col. [[Wilfrid Ashley, 1st Baron Mount Temple|Wilfrid William Ashley, 1st and last Baron Mount Temple]] and Amalia Mary Maud Cassel,<ref name="thepeerage.com" /> a multimillionaire.<ref name=time-mag-1927-05-23/> The couple, described by the ''[[Cincinnati Enquirer]]'' as "England's wealthiest girl and handsomest man",<ref name="time-mag-1927-05-23">{{cite news
 | url   = http://www.time.com/time/magazine/article/0,9171,730593,00.html
 | title = Milestones: May 23, 1927
 | date  = 23 May 1927
 | work  = [[Time (magazine)|Time Magazine]]
 | accessdate =21 November 2009
}}</ref>  had two children: Michael Duncan Alec Cunningham-Reid (1928-2014)<ref>http://peeragenews.blogspot.co.uk/2014/02/michael-duncan-alec-cunningham-reid.html</ref> and Noel Robert Cunningham-Reid (born 1930).<ref name="thepeerage.com" />  On their honeymoon, she insisted that they share her wealth because "no decent woman likes to have a man live with her in charity", but when the couple divorced in 1940, he sued for half of her $400,000 annual income.<ref name="time-mag-1943-08-09">{{cite news
 | url   = http://www.time.com/time/magazine/article/0,9171,766957,00.html
 | title = Foreign News: Old Boys
 | date  = 9 August 1943
 | work  = [[Time (magazine)|Time Magazine]]
 | accessdate =21 November 2009
}}</ref>

In 1944 Cunningham-Reid married secondly Angela Williams, and they were divorced about 1949.  During the [[World War II|Second World War]] he conducted an affair with the American heiress [[Doris Duke]].<ref>{{cite news|url=http://www.independent.co.uk/life-style/who-wants-to-be-a-billionairess-1585419.html|title=Who wants to be a billionairess?|last=Usborne|first=David|date=8 June 1995|work=The Independent|accessdate=21 November 2009|location=London}}</ref>

== Death ==
Cunningham-Reid died in [[Valbonne]], France, on 26 March 1977.<ref name="the-aerodrome" />

== References ==
{{Reflist|30em}}

== External links ==
*{{Hansard-contribs | captain-alec-cunningham-reid | Alec Reid }}
*{{NPG name|name=Alec Stratford Cunningham-Reid (1895-1977)}}

{{s-start}}
{{s-par|uk}}
{{succession box
 | title  = [[Member of Parliament]] for [[Warrington (UK Parliament constituency)|Warrington]]
 | years  = [[United Kingdom general election, 1922|1922]] – [[United Kingdom general election, 1923|1923]]
 | before = [[Harold Smith (British politician)|Sir Harold Smith]]
 | after  = [[Charles Dukes, 1st Baron Dukeston|Charles Dukes]]
}}
{{succession box
 | title  = [[Member of Parliament]] for [[Warrington (UK Parliament constituency)|Warrington]]
 | years  = [[United Kingdom general election, 1924|1924]] – [[United Kingdom general election, 1929|1929]]
 | before = [[Charles Dukes, 1st Baron Dukeston|Charles Dukes]]
 | after  = [[Charles Dukes, 1st Baron Dukeston|Charles Dukes]]
}}
{{succession box
 | title  = [[Member of Parliament]] for [[St Marylebone (UK Parliament constituency)|St Marylebone]]
 | years  = [[St Marylebone by-election, 1932|1932]] – [[United Kingdom general election, 1945|1945]]
 | before = [[Rennell Rodd, 1st Baron Rennell|Sir Rennell Rodd, Bt]]
 | after  = [[Wavell Wakefield, 1st Baron Wakefield of Kendal|Wavell Wakefield]]
}}
{{s-end}}


{{DEFAULTSORT:Cunningham-Reid, Alec}}

[[Category:1895 births]]
[[Category:1977 deaths]]
[[Category:People from Breckland (district)]]
[[Category:Royal Flying Corps officers]]
[[Category:British World War I flying aces]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Conservative Party (UK) MPs]]
[[Category:Members of the Parliament of the United Kingdom for English constituencies]]
[[Category:UK MPs 1922–23]]
[[Category:UK MPs 1924–29]]
[[Category:UK MPs 1931–35]]
[[Category:UK MPs 1935–45]]
[[Category:Royal Engineers officers]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War I]]