{{POV|date=December 2015}}
{{Use Australian English|date=July 2015}}

'''Hugh Reskymer "Kym" Bonython''', [[Companion of the Order of Australia|AC]], [[Distinguished Flying Cross (United Kingdom)|DFC]], [[Air Force Cross (United Kingdom)|AFC]] (15 September 1920 – 19 March 2011) was a prominent and active member of [[Adelaide]] society in [[Australia]], with a very wide range of interests, activities and achievements in the fields of business, the arts, entertainment and public service.

==Biography==
===Early life===
Hugh Reskymer "Kym" Bonython was born on 15 September 1920 in [[Adelaide]], [[South Australia]]),<ref name=Peuker2011/> the youngest child of Sir [[John Lavington Bonython]] and his second wife [[Lady Jean Bonython]], née Constance Jean Warren.<ref name=cjw>Joyce Gibberd, [http://www.adb.online.anu.edu.au/biogs/A130248b.htm Bonython, Constance Jean (1891 - 1977)], Australian Dictionary of Biography, Volume 13, Melbourne University Press, 1993, pp. 215-16.</ref> (Sir John's first wife died in childbirth, aged 26). He was named "Hugh [[Reskymer Bonython]]" after an ancestor who had served as [[High Sheriff of Cornwall]] in 1619.<ref>"Sheriff's from Edward I to the present day", p. 108<br>Richard Polwhele, ''The Civil and Military History of Cornwall'', Volume 4, (London: Cadell and Davies, 1806)<br>"A Catalogue of most of the gentlements names, with their dwellings, in Cornwall", page 15 of: Richard Polwhele, ''The History of Cornwall in respect to its population; ... '', Volume 7, (London: Cadell and Davies, 1806).<br>Richard Polwhele, ''The History of Cornwall, civil, military, religious ... in seven volumes'', Volumes 4, 7. [https://books.google.com/books?id=qEQQAAAAYAAJ&printsec=frontcover&source=gbs_book_other_versions_r&cad=5#v=onepage&q=bonython&f=false google books]</ref><ref name=Harris2008>{{cite news|last=Harris|first=Samela|date=6 September 2008|title=Ruby Awards 2008 Premier's Lifetime Achievement: Life in the arts lane|work=[[The Advertiser (Adelaide)|The Advertiser]]|location=Adelaide, South Australia|page=16}}</ref> Both his father, John Lavington Bonython,<ref name=lavington/> and his grandfather, [[John Langdon Bonython]],<ref name=langdon>W.B. Pitcher, [http://www.adb.online.anu.edu.au/biogs/A070345b.htm Bonython, Sir John Langdon (1848-1939)], ''Australian Dictionary of Biography'', Volume 7, Melbourne University Press, 1979, pp. 339-41.</ref> had been (among other things) editors of daily Adelaide newspaper ''[[The Advertiser (Adelaide)|The Advertiser]]''. His father had also served as a councillor, alderman, Mayor and Lord Mayor of the [[City of Adelaide]].<ref name=lavington>W. B. Pitcher, [http://www.adb.online.anu.edu.au/biogs/A070346b.htm Bonython, Sir John Lavington (1875 - 1960)], Australian Dictionary of Biography, Volume 7, Melbourne University Press, 1979, pp 341-342.</ref> Kym was the youngest of six children; he had one half-brother ([[John Langdon Bonython (1905–1992)]]), two half-sisters ([[Elizabeth Bonython|Lady Betty Wilson]] and [[Ada Bray Bonython|Ada Heath]]), a brother (Charles [[Warren Bonython]]<ref>[http://www.asap.unimelb.edu.au/bsparcs/biogs/P003067b.htm Warren Bonython], Bright Sparcs</ref>) and a sister ([[Katherine Downer Bonython|Katherine Verco]]).

===War pilot===
He attended [[St Peter's College, Adelaide]], and upon completion entered into accountancy on the recommendation of his older half-brother John.<ref name=Harris2008/> The Second World War interrupted this: in 1940 he began training as a pilot for the [[Royal Australian Air Force]] (RAAF). Bonython served in the (then) Netherlands East Indies and New Guinea, experiencing several "death defying" near misses.<ref name=Harris2008/><ref name=obit>{{cite news|url=http://www.adelaidenow.com.au/ipad/obituaries-tributes-to-three-of-our-finest/story-fn6bqvxz-1226028419163|title=Obituaries: Tributes to three of our finest|date=26 March 2011|work=The Advertiser}}</ref><ref name="SundayMail20050524">{{cite news|date=24 April 2005|title=Reflections on a fortunate life|work=[[The Advertiser (Adelaide)|The Sunday Mail]]|location=Adelaide, South Australia|page=H16}}</ref> He was in hospital in Darwin (with dengue fever) during the [[Bombing of Darwin|1942 bombing]]<ref>{{cite news|last=Bonython|first=Kym|date=9 August 2005|title=The Advertiser World War II 60th Anniversary: Bonython's battle|work=[[The Advertiser (Adelaide)|The Advertiser]]|page=66}} Exert from {{cite book|last=Bonython|first=Kym|year=1979|title=Ladies' Legs & Lemonade|publisher=Rigby|isbn=978-0-7270-1191-6}}</ref> - he had just evacuated and taken cover when the ward he had been in took a direct hit.<ref name=obit/> During his time with the RAAF, Bonython filled the roles of aircraft captain in 1941, and chief flying instructor with the rank of [[squadron leader]] in 1943.<ref name="SundayMail20050524"/>

On 1 September 1944, Flight Lieutenant Bonython (Aus.280778) was awarded the [[Air Force Cross (United Kingdom)|Air Force Cross]] (AFC),<ref name=AFClg>[http://www.london-gazette.co.uk/issues/36682/supplements/4077/page.pdf Air Force Cross], Supplement to the London Gazette, 1 September 1944, No.36682, pg.4077</ref><ref name=AFC>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=1083853&search_type=simple&showInd=true It's an Honour: AFC] 1 September 1944</ref> and on 22 February 1946, Squadron Leader Bonython AFC was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (DFC)<ref name=DFClg>[http://www.london-gazette.co.uk/issues/37479/supplements/1075/page.pdf Distinguished Flying Cross], Fourth supplement to the London Gazette, 19 February 1946, No.37479, pg.1075</ref><ref name=DFC>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=1070941&search_type=simple&showInd=true It's an Honour: DFC] 22 February 1946</ref><ref>[http://trove.nla.gov.au/ndp/del/article/48692931 Awards for Airmen] ''The Advertiser (Adelaide, SA)'' 23 February 1946 pg.12 - "Sq-Ldr. Bonython joined the RAAF in 1940 and was posted to No. 87 (PR) Squadron in 1944. In April, 1945, he carried out the longest photographic mission ever undertaken by this squadron, to Central Java. As a flight commander he showed outstanding fearlessness, and by his keenness and devotion to flying proved an inspiration to all other pilots of the squadron."</ref>

===Discovering jazz===
When he returned from service he chose not to return to accounting, deciding on a very different career path. Initially he took up [[dairy farming]] on his father's [[Mount Pleasant, South Australia|Mount Pleasant]] property, where he introduced artificial breeding of cattle into Australia.{{citation needed|date=April 2013}}  In the 1950s his career changed to incorporate music, the arts, and motor racing.<ref name=Harris2008/><ref name="SundayMail20050524" />

As a child Kym Bonython developed a passion for [[jazz]], and this influenced a number of his later pursuits. At the age of 17, in 1937, he entered the media with an [[Australian Broadcasting Corporation|ABC]] radio jazz show.<ref name="SundayMail20050524" /> The show continued for 38 years, finishing in 1975. His involvement in the jazz scene also extended to making and selling music; in 1952 he became a member of a jazz band as drummer – a skill he had learned as a child – and he opened his first [[record shop|record store]] in Bowman's Arcade on [[King William Street, Adelaide|King William Street]] in 1954.<ref name=Harris2008/> His passion for music also led him to create his own concert promotion company, Aztec Services, in the 1950s, and as a promoter he brought to Adelaide some of the greats of jazz, including [[Dizzy Gillespie]], [[Count Basie]], [[Dave Brubeck]], [[Duke Ellington]] and [[Louis Armstrong]].<ref name=Edwards>{{cite news | last = Edwards | first = Verity | date = 21 March 2011 | title = War flying ace, art lover and larrikin Kym Bonython dies at 90 | newspaper = [[The Australian]] | page = 3 |url = http://www.theaustralian.com.au/news/executive-lifestyle/war-flying-ace-art-lover-and-larrikin-kym-bonython-dies-at-90/story-e6frg9zo-1226025033070}}</ref> Later, at the urging of his children, he expanded his range to [[rock and roll]], bringing the likes of [[Chuck Berry]] to Adelaide, and he was one of the key people responsible for negotiating the addition of Adelaide to [[The Beatles]] Australian tour 1964.<ref name="Creswell">{{cite book | last1 = Creswell | first1 = Toby | last2 = Trenoweth | first2 = Samantha | year = 2006 | title = 1001 Australians You Should Know | url = https://books.google.com/books?id=QqtinbjO0oEC | publisher = Pluto Press Australia | isbn = 978-1-86403-361-8 |  pages = 37–38 | chapter = Kym Bonython, The Aristocratic Dealer}}</ref>

===Passion for art===
Along with music, Kym Bonython had a passion for art, and he began his collection in 1945. In 1961 he opened his first gallery, the Bonython Art Gallery  in [[North Adelaide]], (which later became the Bonython Meadmore gallery). His first major exhibition was British Art of the 1960s, where he exhibited the first painting of Francis Bacon shown in Australia. He subsequently moved to Sydney to open the Hungry Horse Gallery in [[Paddington, New South Wales|Paddington]] in 1966.<ref name=Harris2008/><ref name="SundayMail20050524"/><ref name="Harris20071204">{{cite news|last=Harris|first=Samela|date=4 December 2007|title = Kym Bonython a legend of art and jazz in his time|work=[[The Advertiser (Adelaide)|The Advertiser]]|location=Adelaide, South Australia|page=7}}</ref><ref name="Greenway2006">{{cite journal|title=Pioneering Gallerists: Kym Bonython|url=https://www.artlink.com.au/articles/2881/pioneering-gallerists-kym-bonython/|last=Greenway|first=Paul|year=2006|journal=Artlink|volume=26|issue=4|pages=91–92|access-date=24 October 2016 }}</ref> His time with his Sydney gallery ended in 1976, and he returned to Adelaide to buy back his original gallery, operating it until 1983.<ref name="Greenway2006"/> From 1988 Bonython managed a Sydney gallery once more, managing the BMG Fine Art for a short time.<ref name="SundayMail20050524"/><ref name="Greenway2006"/>

Kym Bonython's eye for contemporary art saw his galleries promote many Australian and international artists, including [[Sidney Nolan]], [[Pro Hart]] and [[William Dobell]], and he is widely acknowledged to have discovered and fostered the work of [[Brett Whiteley]].<ref name=Edwards/><ref name="Creswell"/><ref name="Greenway2006"/> Along with the art galleries and his personal collection (much of which was destroyed when the [[Ash Wednesday fires|Ash Wednesday]] bushfires of 1983 engulfed his [[Mount Lofty]] property, "Eurilla"<ref>[http://images.slsa.sa.gov.au/mpcimg/15750/B15676.htm "Eurilla", Mount Lofty], ca. 1890 - the residence of Sir William Milne at Mount Lofty. Later the property of Sir Lavington Bonython, and later still, of Kym Bonython. [http://images.slsa.sa.gov.au/mpcimg/33750/B33591.htm 1905] [http://images.slsa.sa.gov.au/mpcimg/47750/B47676.htm 1890]</ref>), Bonython authored and published a number of art books.<ref name=Creswell/><ref name=Harris2011>{{cite news|last=Harris|first=Samela|date=21 March 2011|title=A true champion of the arts|work=[[The Advertiser (Adelaide)|The Advertiser]]|location=Adelaide, South Australia|page=12|url=http://www.adelaidenow.com.au/ipad/a-true-champion-of-the-arts/story-fn6bqphm-1226025018449|accessdate=20 December 2015}}</ref>

===Behind the wheel===
Kym Bonython gained a reputation as a daredevil partially through another of his interests: motor racing. He raced [[Midget car racing|Speedcars]] at the [[Rowley Park Speedway]] in the Adelaide suburb of [[Bowden, South Australia|Bowden]], which he also owned the lease on and successfully promoted from 1954 to 1973. Bonython had some major crashes in Speedcars but also some success, winning the [[South Australian Speedcar Championship|South Australian Championship]] in 1959-60. He competed at venues such as the [[Sydney Showground Speedway]], [[Claremont Speedway]] in [[Perth, Western Australia|Perth]] and the [[Brisbane Exhibition Ground]] (Ekka). At one stage he was also the national [[Hydroplane (boat)|hydroplane]] champion.<ref name="Creswell"/> His life in motor sports led to many accidents, the most serious being in 1956 when, racing to defend his Australian hydroplane title at [[Snowdens Beach]], his boat crashed; the injuries that resulted led to Bonython spending the next 14 months on crutches.<ref name=Harris2008/> Amongst his achievements in motor sports was his work to bring [[Formula One|Formula 1]] to [[Australian Grand Prix|Adelaide]] in 1985, in which he has been described as a "catalyst" for the event (along with other prominent locals including then [[Premier of South Australia]] [[John Bannon]] and former F1 driver and [[1983 24 Hours of Le Mans]] winner [[Vern Schuppan]]).<ref name=Peuker2011>{{cite news | last = Peuker | first = Christie | date = 20 March 2011 | title = Jets farewell officer and gentleman Kym Bonython | work = [[The Advertiser (Adelaide)|The Sunday Mail]] | location = Adelaide, South Australia | url = http://www.adelaidenow.com.au/news/south-australia/jets-farewell-our-favourite-son-kym-bonython/story-e6frea83-1226024659061 | accessdate = 22 March 2011 }}</ref> His time in motor sports earned him the title of "the man with 99 lives" and, from [[Max Harris (poet)|Max Harris]] regarding Rowley Park, the "Cecil B. De Mille of Bowden".<ref name=Harris2008/><ref name=Harris2011/> In 2007, he was one of 10 inaugural inductees into the [[Australian Speedway Hall of Fame]].<ref name=obit/>

Bonython was also well known around Adelaide for travelling to various meetings or appointments on his yellow [[MV Agusta]] motorcycle. Bonython had a number of "near misses" on the bike which he claimed were the result of other road users being distracted by his famous racing helmet which had been painted with naked ladies by his friend and artist Louis James.

===Politics===
Bonython was also active in public life. He served on the [[Adelaide City Council]], as had both his father and grandfather before him, and he was the chairman of the South Australian [[J150W|Jubilee 150]] Board.<ref name=Peuker2011/><ref name=AC/> Other boards of which he was a member included the [[Adelaide Festival of Arts]], [[Musica Viva Australia]] and the [[Australia Council for the Arts|Australia Council]].<ref name="Creswell" /> Bonython was also one of Australia's leading monarchists, chairing the No Republic committee and serving as one of South Australia's delegates to the [[Constitutional Convention (Australia)#1998 convention|1998 Constitutional Convention]]. Other causes to receive his active support included [[euthanasia]] and [[Conscription|compulsory national service]].<ref name=Edwards/><ref name=Harris2011/><ref name=Shepherd>{{cite news | last = Shepherd | first = Tory | date = 2 January 2009 | title = Adventurer Kym Bonython wants right to die | work = [[The Advertiser (Adelaide)|The Advertiser]] | location = Adelaide, South Australia | url = http://www.news.com.au/adelaidenow/story/0,22606,24863965-5006301,00.html | accessdate = 22 March 2011 }}</ref>

===Personal life===
In 1979 he wrote an autobiography: "Ladies' Legs and Lemonade", Kym Bonython, Adelaide: Rigby, 1979.

Kym was married twice and had five children: Chris and Robyn from the first marriage, Tim, Michael and Nicole from the second.<ref name=obit/><ref name=Harris2011/> On his return from Milne Bay after the war he married Jean Adore Paine<ref>[http://trove.nla.gov.au/ndp/del/article/48892014 Items of interest] ''The Advertiser (Adelaide, SA)'' 25 November 1942 pg.3 - ''The engagement is announced of Miss Jean Paine, WAAAF, daughter of Mr. and Mrs. W. W. Paine, of Balmoral, Sydney, NSW. to Flt-Lt. Hugh Reskymer (Kym) Bonython, RAAF, younger son of Sir Lavington and Lady Bonython, East terrace, Adelaide.''</ref>  - they divorced in 1953.<ref>[http://trove.nla.gov.au/ndp/del/article/48287572 Divorce Granted] ''The Advertiser (Adelaide, SA)'' 29 May 1953 pg.6</ref> In 1957, while still on cruthes from his accident at Snowden's beach, he married former Miss South Australia Julianna McClure (Julie).<ref name=obit/><ref name=tapes>"Kym Bonython interviewed by Rob Linn [sound recording]", 2004, 3 digital audio tapes (ca. 171 min.) http://catalogue.nla.gov.au/Record/3279565. Interview conducted in association with the State Library of South Australia and Mortlock Library of South Australiana. Recorded on 22 September 2004 at North Adelaide, S. Aust.</ref>

He died on 19 March 2011 at his home in [[North Adelaide]], aged 90.<ref name=Peuker2011/> He died during the running of the [[Adelaide 500|Clipsal 500]] while an [[McDonnell Douglas F/A-18 Hornet|F-18]] flew overhead, reflecting both his love of motor sports and his time as an RAAF pilot.<ref name=Peuker2011/>

He was survived by his wife, Julie, his five children, 15 grandchildren and seven great-grandchildren.<ref name=obit/><ref name=Edwards/>

Bonython was given a State Funeral, held on 29 March 2011 at the [[St Peter's Cathedral, Adelaide|St Peter's Cathedral]]. It was attended by many of the friends he made in speedway, including former long time Rowley Park track manager Alan Marks, [[Australian Sidecar Speedway Championship|Australian Sidecar]] champions Rick Munro and Len Bowes, [[Speedway City]] promoter Wendy Turner, [[Gillman Speedway]] promoter and former sidecar racer David Parker, flagman extraordinaire Glen Dix, and 15 time World Champion [[Ivan Mauger]] who got his first start in Australia in 1960 when Bonython signed the young [[Christchurch]] native to ride at Rowley Park.<ref>[http://www.speedway.net.au/archive_release.asp?NewsId=40088#.Uwq_evQW2So Kym Bonython Laid to Rest]</ref>

==Publications==
* ''Modern Australian Painting & Sculpture: A Survey of Australian Art from 1950 to 1960'', Rigby, Adelaide, 1960.
* ''Modern Australian Painting 1960-70'', Rigby, Adelaide, 1970.
* ''Modern Australian Painting 1970-75'', Rigby, Adelaide, 1976, Kym Bonython & Elwynn Lynn
* ''Modern Australian Painting 1975-80'', Rigby, Adelaide, 1980.
* ''Modern Australian Painting 1950-75'', Rigby, Adelaide, 1980.
* ''Ladies' Legs and Lemonade'', Adelaide: Rigby, 1979.

==Honours and awards==
On 1 September 1944 he was awarded the [[Air Force Cross (United Kingdom)|Air Force Cross]].<ref name="AFC"/>  On 22 February 1946 he was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]].<ref name="DFC"/> In the Queen's Birthday Honours of June 1981, he was appointed an [[Officer of the Order of Australia]] (AO), in recognition of service to the arts.<ref name=AO>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=870102&search_type=simple&showInd=true Profile], itsanhonour.gov.au, June 1981.</ref> (Both his brothers had also been given this honour the previous year.)

In the Australia Day Honours of 26 January 1987, he was appointed [[Companion of the Order of Australia]] (AC), Australia's highest civilian honour, "in recognition of service to the community, particularly as Chairman of the SA Jubilee 150 Board".<ref name=AC>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=874008&search_type=simple&showInd=true Profile], itsanhonour.gov.au, 26 January 1987.</ref>

He was also appointed a Knight of the [[Venerable Order of Saint John]] (KStJ), and awarded the honorary degree of Doctor of the University by the University of Adelaide (DUniv) in recognition of his outstanding service to the arts and the community.<ref>[http://www.adelaide.edu.au/news/news41481.html Profile], adelaide.edu.au,  27 September 2010.</ref> Other honours included a Lifetime Achievement Award by the Adelaide Critics Circle in 2007,<ref name="Harris20071204"/> and the Kym Bonython Fellowship, which provides support to up and coming visual artists and was named in his honour, and was first awarded by the [[Adelaide Festival Centre]] in 2010.<ref name=Harris2009>{{cite news|last=Harris|first=Samela|date=16 May 2009|title=Centre honours Kym Bonython In the name of art|work=[[The Advertiser (Adelaide)|The Advertiser]]|location=Adelaide, South Australia|page=38|accessdate=20 December 2015}}</ref>

{| class="wikitable"
|-
| rowspan=2 | [[File:OrderAustraliaRibbon.png|80px]]
| [[Companion of the Order of Australia]] (AC) || 1987<ref name=AC/>
|-
| [[Officer of the Order of Australia]] (AO) || 1981<ref name=AO/>
|-
|[[File:United Kingdom Distinguished Flying Cross ribbon.svg|80px]]
|[[Distinguished Flying Cross (United Kingdom)]] (DFC) || 1946<ref name=DFC/>
|-
|[[File:UK AFC ribbon.svg|80px]] 
|[[Air Force Cross (United Kingdom)]] (AFC) || 1944<ref name=AFC/>
|-
|[[File:Order of St John (UK) ribbon.png|80px]]
|[[Venerable Order of Saint John|Knight of the Venerable Order of Saint John]] (KStJ)
|- 
|[[File:39-45 Star BAR.svg|80px]] 
|[[1939-1945 Star]]
|-
|[[File:Pacific Star.gif|80px]] 
|[[Pacific Star]]
|-
|[[File:Defence Medal BAR.svg|80px]] 
|[[Defence Medal (United Kingdom)|Defence Medal]]
|-
|[[File:War Medal 39-45 BAR.svg|80px]] 
|[[War Medal 1939-45]]
|-
|[[File:Australian Service Medal 1939-45 ribbon.png|80px]] 
|[[Australia Service Medal 1939-45]]
|}

;Others
*2007 - Lifetime Achievement Award by the Adelaide Critics Circle<ref name="Harris20071204"/>
*2008 - Premier's Lifetime Achievement Award, 2008 Ruby Awards<ref name=ruby>[http://www.arts.sa.gov.au/webdata/resources/files/2008_AbaF_Ruby_Book.pdf 2008 AbaF Ruby Awards], Australian Business Arts Foundation (www.abaf.org.au) and Arts SA (www.arts.sa.gov.au), pg.18</ref>
*2007 - Inaugural member, Australian Speedway Hall of Fame<ref name=obit/><ref>[http://www.nasr.com.au/index.php/about-nasr/hall-of-fame/ About the Australian Speedway Hall of Fame], National Association of Speedway Racing, www.nasr.com.au</ref><ref>National Association of Speedway Racing (NASR) National Office (2007) [http://www.speedway.net.au/release.asp?NewsId=23649 Australian Speedway Hall Of Fame Inductees, 2007], www.speedway.net.au</ref>
*2010 - Kym Bonython Fellowship named in his honour<ref name=Harris2009/>

==References==
{{reflist}}

==External links==
* [http://www.abc.net.au/dimensions/dimensions_in_time/Transcripts/s566259.htm Kym Bonython Episode 16, "Dimensions in Time"], May 2002, ABC

'''Copyright photos'''
* [http://resources2.news.com.au/images/2009/01/01/va1237348330087/kymbig-6423003.gif Painting of SqnLdr Bonython DFC AFC] ([http://www.adelaidenow.com.au/news/south-australia/adventurer-wants-right-to-die/story-e6frea83-1111118457146 Source])
* [http://www.abc.net.au/news/photos/2007/08/23/2013468.htm Kym Bonython portrait by John Brack, 1963] 23 August 2007
* [http://resources0.news.com.au/images/2011/03/21/1226025/154548-bonython.jpg Bonython on a motor cycle]
* [http://lh3.ggpht.com/_EtzDLZkvFws/RoByq6MfqqI/AAAAAAAAArY/ejp2DkgBQ6A/50sto1.JPG Bonython driving in a demolition derby], c. 1960 ([http://picasaweb.google.com/lh/photo/rxyM35BM1Dx6CuhmJjg3og Source])
* [https://www.flickr.com/photos/23023719@N04/3161481245/ Mrs Julie Bonython], ''The Australian Women's Weekly'', 3 April 1974
* [http://www.saspeedway.info/gillman/images/gillman_classic_2006/gillman_06%20007.jpg Opening Gillman raceway], 2006
* [http://www.rayhughesgallery.com/artPopup.asp?artId=4709 Darwin 1942 - Franck Gohier], <small>([http://www.rayhughesgallery.com/artDisplayExhibition.asp?artId=4709&exhibitionId=159&pageNo=2 Source])</small> <!-- http://www.rayhughesgallery.com/_img/art/2008327113912kimbonython.jpg -->
* [http://www.adelaidenow.com.au/news/galleries/gallery-e6frecgc-1226024663093?page=2 Bonython's life in pictures], ''Adelaide Now'', March 2011

{{Use dmy dates|date=March 2011}}

{{DEFAULTSORT:Bonython, Kym}}
[[Category:1920 births]]
[[Category:2011 deaths]]
[[Category:Australian broadcasters]]
[[Category:Australian art dealers]]
[[Category:Music promoters]]
[[Category:Australian jazz musicians]]
[[Category:Jazz drummers]]
[[Category:Royal Australian Air Force officers]]
[[Category:Companions of the Order of Australia]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Australian monarchists]]
[[Category:Knights of the Order of St John]]
[[Category:People educated at St Peter's College, Adelaide]]
[[Category:Bonython family]]
[[Category:20th-century Australian musicians]]
[[Category:20th-century drummers]]