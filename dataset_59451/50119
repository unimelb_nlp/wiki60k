{{Infobox Bilateral relations|Abkhazia_Russia|Abkhazia|Russia|map=Abkhazia-Russia Locator.svg}}
[[File:Москва, Мамоновский переулок, 4.jpg|thumb|Abkhazian embassy in [[Moscow]], Russia.]]

'''Abkhazia–Russia relations''' ({{lang-ru|Российско-абхазские отношения}}) is the [[bilateral relationship]] between the [[Republic of Abkhazia]] and the [[Russian Federation]]. Russia recognised Abkhazia on 26 August 2008, following the [[2008 South Ossetia war|August 2008 South Ossetia war]]. Abkhazia and Russia established [[diplomatic relations]] on 9 September 2008.<ref name="komma&so">{{cite news|last=Solovyev|first=Vladimir|url=http://www.kommersant.com/p1023629/r_538/South_Ossetia_Abkhazia_recognition/|title=Freshly Recognized|date=10 September 2008|publisher=[[Kommersant]]|accessdate=2009-08-03}}</ref>

==Background==

===Russian recognition of Abkhazia===
{{see also|International recognition of Abkhazia and South Ossetia}}
[[Image:Dmitry Medvedev address on 26 August 2008 regarding Abkhazia & South Ossetia.ogg|thumb|right|[[Russian President]] [[Dmitry Medvedev]] announcing that he has signed decrees recognising independence of Abkhazia and South Ossetia (in Russian) [http://www.kremlin.ru/eng/speeches/2008/08/26/1543_type82912_205752.shtml Transcript in English].]]
On 21 August 2008, the same day as a similar event in [[South Ossetia]], a [[Demonstration (people)|rally]] was held in [[Sukhum]] at which an estimated 47,000{{mdash}}50,000 people appealled to [[President of Russia|Russian President]] [[Dmitry Medvedev]] and the [[Federal Assembly of Russia|Russian Federal Assembly]] for official recognition of their independence as a sovereign state.<ref name="appeal">{{cite news|title=Abkhazia, S.Ossetia send sovereignty appeals to Russia|location=[[Sukhum]]|publisher=[[RIA Novosti]]|date=21 August 2008|url=http://en.rian.ru/world/20080821/116198014.html|accessdate=2008-09-09}}</ref><ref>{{cite news|title=Abkhazia sends request to Russia for recognition|location=[[Sukhum]]|publisher=[[RIA Novosti]]|date=21 August 2008|url=http://en.rian.ru/russia/20080821/116189365.html|accessdate=2008-09-09}}</ref> On 25 August 2008 [[President of Abkhazia]] [[Sergei Bagapsh]] made a presentation to the [[Federation Council of Russia]]. In his address to the Council, Bagapsh stated "I can say for certain that Abkhazia and South Ossetia will never be part of Georgia."<ref name="fedcouncil">{{cite news|title=Russian upper house seeks independence for Georgian rebel regions|location=[[Moscow]]|publisher=[[RIA Novosti]]|date=25 August 2008|url=http://en.rian.ru/russia/20080825/116254039.html|accessdate=2008-09-09}}</ref> After hearing the appeals from both the Abkhazian and South Ossetian leadership, on 25 August 2008 the Federation Council and [[State Duma]] passed motions calling upon President Medvedev to recognise the independence of the two regions and to establish [[diplomatic relations]] with them.<ref name="fedcouncil"/><ref>{{cite news|title=Russia parliament asks president to recognise S.Ossetia, Abkhazia|location=[[Moscow]]|publisher=[[RIA Novosti]]|date=25 August 2008|url=http://en.rian.ru/russia/20080825/116264611.html|accessdate=2008-09-09}}</ref>

On 26 August 2008, President Medvedev signed [[ukaz]]es [[International recognition of Abkhazia and South Ossetia|recognising the independence of Abkhazia and South Ossetia]]<ref>{{cite news|title=Russia recognises Georgia's breakaway republics -2|location=[[Moscow]]|publisher=[[RIA Novosti]]|date=26 August 2008|url=http://en.rian.ru/russia/20080826/116291407.html|accessdate=2008-09-09}}</ref><ref>[[The New York Times]] August 26, 2008: [https://www.nytimes.com/2008/08/27/world/europe/27russia.html?hp Russia Backs Independence of Georgian Enclaves] by [[Clifford J. Levy]]</ref> In his address to the Russian nation, Medvedev noted that he was guided by the provisions of the [[UN Charter]], the [http://www.whatconvention.org/en/conv/0703.htm 1970 Declaration on the Principles of International Law Governing Friendly Relations Between States], the [[Helsinki Accords|CSCE Helsinki Final Act of 1975]] and other fundamental [[international law|international instruments]] in issuing the decree, and further stated, "(t)his is not an easy choice to make, but it represents the only possibility to save human lives."<ref name="medspeech">{{cite news|last=Medvedev |first=Dmitry |authorlink=Dmitry Medvedev |title=Statement by President of Russia Dmitry Medvedev, 26 August 2008 |location=[[Moscow]] |publisher=[[President of Russia]] |date=26 August 2008 |url=http://www.kremlin.ru/eng/speeches/2008/08/26/1543_type82912_205752.shtml |accessdate=2008-09-04 |deadurl=yes |archiveurl=https://web.archive.org/web/20080902001442/http://www.kremlin.ru/eng/speeches/2008/08/26/1543_type82912_205752.shtml |archivedate=2008-09-02 |df= }}</ref> Sergei Bagapsh responded to the Russian recognition by saying "(t)his is the century-long dream of the people of Abkhazia made reality."<ref>{{cite news|url=http://en.rian.ru/world/20080827/116307707.html|title=Rebel Georgian regions celebrate their recognition by Russia|date=27 August 2008|publisher=[[RIA Novosti]]|location=[[Sukhum]]|accessdate=2009-08-13}}</ref>

The Russian recognition was condemned by the [[European Union]], [[United States]], [[NATO]],<ref name="rogozinnato" /> [[Parliamentary Assembly of the Council of Europe]],<ref name="pacegryzlov">{{cite news|url=http://en.rian.ru/russia/20090129/119865534.html|title=Russia stands by recognition of Abkhazia, S.Ossetia&nbsp;— speaker|date=29 January 2009|publisher=[[RIA Novosti]]|location=[[Moscow]]|accessdate=2009-08-04}}</ref> amongst others, with some calling for Russia to rescind its recognition. Many high level Russian politicians including [[Russian President]] [[Dmitry Medvedev]],<ref>{{cite news|url=http://en.rian.ru/world/20090808/155760649.html|title=Medvedev says no going back on South Ossetian recognition|date=8 August 2009|publisher=[[RIA Novosti]]|location=[[Vladikavkaz]]|accessdate=2009-08-08}}</ref> [[Prime Minister of Russia|Prime Minister]] [[Vladimir Putin]], Deputy Prime Minister [[Sergei Ivanov|Sergey Ivanov]],<ref>{{cite news|url=http://en.rian.ru/russia/20090208/120038987.html|title=Russia to stick to its decision on bases in S.Ossetia, Abkhazia |date=8 February 2009|publisher=[[RIA Novosti]]|location=[[Munich]]|accessdate=2009-08-04}}</ref> [[Chairman of the State Duma]] [[Boris Gryzlov]],<ref name="pacegryzlov" /> [[Minister of Foreign Affairs (Russia)|Minister of Foreign Affairs]] [[Sergey Lavrov]], [[Permanent Representative of Russia to the United Nations]] [[Vitaly Churkin]]<ref>{{cite news|url=http://www.unmultimedia.org/radio/english/detail/66371.html|title=South Ossetia and Abkhazia Independence a Done Deal&nbsp;— Russia's UN Ambassador |last=Piasecki|first=Jerry|date=9 September 2008|publisher=[[United Nations Radio]]|accessdate=2009-08-04}}</ref> and [[Permanent Representative of the Russian Federation to NATO|Permanent Representative of Russia to NATO]] [[Dmitry Rogozin]],<ref name="rogozinnato">{{cite news|url=http://en.rian.ru/russia/20080827/116325556.html|title=Russia says recognition of S.Ossetia, Abkhazia irreversible |date=27 August 2008|publisher=[[RIA Novosti]]|location=[[Brussels]]|accessdate=2009-08-04}}</ref> rejected the criticism, and have stated that Russian recognition of Abkhazia is irreversible. In an interview to [[Vesti (TV channel)|Vesti]] in August 2009, Sergey Lavrov stated that Russian recognition of Abkhazia and South Ossetia was not planned when the 2008 war began.<ref>{{cite news|url=http://en.rian.ru/russia/20090805/155730694.html|title=Russia's recognition of Abkhazia, S.Ossetia not planned&nbsp;— Lavrov|date=5 August 2009|publisher=[[RIA Novosti]]|location=[[Moscow]]|accessdate=2009-08-05}}</ref>

As a result of the Russian recognition of Abkhazian and South Ossetian independence, [[Georgia–Russia relations|Georgia severed diplomatic relations with Russia]] on 29 August 2008,<ref>{{cite news|url=https://www.nytimes.com/2008/08/29/world/europe/29iht-georgia.3.15746317.html|title=Georgia breaks off relations with Russia|date=29 August 2008|location=[[Tbilisi]]|publisher=[[The New York Times]]|accessdate=2009-08-04}}</ref> and declared that it regards South Ossetia and Abkhazia as ''occupied territories''.<ref name="sarkozypraise">{{cite news|url=http://en.rian.ru/world/20090808/155760058.html|title=Medvedev praises Sarkozy's role in S. Ossetia ceasefire|date=8 August 2009|publisher=[[RIA Novosti]]|location=[[Moscow]] |accessdate=2009-08-09}}</ref>

==Bilateral relationship==

===Diplomatic ties===
[[File:Dmitry Medvedev with Igor Akhba.jpg|thumb|upright|[[Igor Akhba]], the first Abkhazian Ambassador to Russia, presents his [[Letters of Credence]] to [[Russian President]] [[Dmitry Medvedev]] on 16 January 2009.]]
Russia and Abkhazia established [[diplomatic relations]] at [[embassy]] level on 9 September 2008, when [[Russian Foreign Minister]] [[Sergey Lavrov]] and [[Minister for Foreign Affairs of Abkhazia|Abkhaz Minister of Foreign Affairs]] [[Sergei Shamba]] exchanged notes at the [[Russian Foreign Ministry]] in [[Moscow]].<ref name="komma&so"/>

On 25 September 2008, President Medvedev sign an [[ukaz]] appointing the first [[Ambassadors of Russia|Russian Ambassador]] to Abkhazia, [[Semyon Grigoriyev]],<ref>{{Cite Russian law
|ru_entity=Президент Российской Федерации
|ru_type=Указ
|ru_number=1527
|ru_date=25 October 2008
|ru_title=О назначении Григорьева С.В. Чрезвычайным и Полномочным Послом Российской Федерации в Республике Абхазия
|ru_url=http://document.kremlin.ru/doc.asp?ID=048516
|en_entity=[[President of Russia]]
|en_type=[[Ukaz]]
|en_number=1527
|en_date=25 October 2008
|en_title=On the appointment of S.V. Grigoriyev as the Plenipotentiary Ambassador of the Russian Federation to the Republic of Abkhazia
|en_url=http://document.kremlin.ru/doc.asp?ID=048516
}}</ref><ref>{{cite news|url=http://www.russiatoday.com/Top_News/2008-10-24/Russia_appoints_ambassadors_to_South_Ossetia__and__Abkhazia.html|title=Russia appoints ambassadors to South Ossetia & Abkhazia|date=24 October 2008|publisher=[[Russia Today]]|accessdate=2009-08-04}}</ref> who presented his [[Letters of Credence]] to Abkhaz President [[Sergei Bagapsh]] on 16 December 2008.<ref>{{cite news|url=http://en.rian.ru/world/20081216/118889930.html|title=Envoys to S. Ossetia, Abkhazia present credentials to presidents|date=16 December 2008|publisher=[[RIA Novosti]]|location=[[Moscow]]|accessdate=2009-08-13}}</ref><ref>{{cite web|url=http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=15201|script-title=ru:Президент Сергей Багапш принял Верительную грамоту оТ Чрезвычайного и Полномочного посла Российской Федерации в Республике Абхазия С.В. Григорьева|date=16 December 2008|publisher=[[President of Abkhazia]]|accessdate=2009-06-14|language=ru}}</ref> [[Igor Akhba]], the Plenipotentiary Representative of the President of the Republic of Abkhazia to Russia was appointed by Sergei Bagapsh as Abkhazia's first [[Ambassador of Abkhazia to Russia|ambassador to Russia]] on 14 November 2008.<ref name=agov14655>{{cite news
|url = http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=14655
|title = Игорь Ахба назначен Чрезвычайным и Полномочным Послом Абхазии в России
|publisher = [[President of Abkhazia]]
|date = 14 November 2008
|accessdate = 13 December 2008
}}</ref> Akhba presented his credentials to Russian President Dmitry Medvedev on 16 January 2009.<ref name="16jan09">{{cite web|url=http://www.kremlin.ru/text/docs/2009/01/211763.shtml|script-title=ru:Послы иностранных государств, вручившие верительные грамоты Президенту России|date=16 January 2009|publisher=[[Presidential Press and Information Office]]|accessdate=2009-03-03|language=ru}}</ref>

[[Russian Prime Minister]] [[Vladimir Putin]] issued a directive to set up a [[Russian embassy]] in Abkhazia in 2009.<ref>{{cite news|url=http://www.regnum.ru/english/1123709.html|title=Vladimir Putin signed a direction about establishment of Russian embassies in Abkhazia and South Ossetia|publisher=REGNUM|date=2009-02-11|accessdate=2009-04-05}}</ref> On 1 May 2009, Russia's embassy to Abkhazia was opened in [[Sukhumi]].<ref>[http://www.interfax.ru/society/news.asp?id=77530&sw=%EF%EE%F1%EE%EB%FC%F1%F2%E2&bd=2&bm=4&by=2009&ed=2&em=5&ey=2009&secid=0&mp=0&p=1 Посольство России в Абхазии начало работу на постоянной основе.] «[[Интерфакс]]» 01 мая 2009 года 23:26</ref>

Russia plans on opening up a trade mission in Abkhazia in order to ease access to the local economy for Russian businesses.<ref>{{cite news|url=http://www.itar-tass.com/eng/level2.html?NewsID=13571521&PageNum=0|title=Russia to open trading mission in Abkhazia.|publisher=ITAR-TASS|date=2009-02-11|accessdate=2009-02-12}}</ref>

In March 2009, Abkhazian President [[Sergei Bagapsh]] told the Vice President of the [[International Crisis Group]] that his republic had no plans to become a part of Russia and that his administration was "building an independent, legal, and democratic state."<ref>{{cite news|url=http://en.rian.ru/world/20090317/120610880.html|publisher=RIA Novosti|date=2009-03-17| archiveurl= https://web.archive.org/web/20090321181710/http://en.rian.ru/world/20090317/120610880.html| archivedate= 21 March 2009 |title=Abkhazia will remain independent from Russia - Bagapsh |deadurl=no |accessdate=2013-05-16}}</ref>

==List of treaties and agreements==
{| class="wikitable" style="text-align:left;background:none"
|- style="font-weight:bold"
|Signed:
|
|Ratified by Abkhazia:
|
|Ratified by Russia:
|
|Treaty:
|Notes:
|-
|30 April 2009
|<ref name=uzel153576>{{cite news|title=Сегодня Россия подпишет соглашения с Абхазией и Южной Осетией об охране границы|url=http://abkhasia.kavkaz-uzel.ru/articles/153576|accessdate=18 June 2012|newspaper=[[Caucasian Knot]]|date=30 April 2009}}</ref>
|
|
|
|
|Cooperation agreement on the protection of state borders
|
|-
|
|
|8 May 2012
|<ref name=apress6203>{{cite news|title=Два соглашения ратифицировано и одно принято в первом чтении|url=http://apsnypress.info/news/6203.html|accessdate=8 May 2012|newspaper=[[Apsnypress]]|date=8 May 2012}}</ref>
|
|
|Agreement on the procedure of the pension schemes for Internal Affairs officials
|
|-
|
|
|8 May 2012
|<ref name=apress6203/>
|
|
|Cooperation agreement on disaster prevention and management
|
|-
|12 August 2009
|<ref name=uzel157853>{{cite news|last=Kuchuberia|first=Anzhela|title=Москва и Сухум подписали соглашение об оказании Абхазии социально-экономической помощи|url=http://www.kavkaz-uzel.ru/articles/157853/|accessdate=16 June 2012|newspaper=[[Caucasian Knot]]|date=13 August 2009}}</ref>
|15 June 2012
|<ref name=apress6541>{{cite news|title=Парламент ратифицировал соглашение между правительствами Абхазии и России об оказании помощи РА в социально-экономическом развитии|url=http://apsnypress.info/news/6541.html|accessdate=16 June 2012|newspaper=[[Apsnypress]]|date=15 June 2012}}</ref>
|
|
|Agreement to assist the Republic of Abkhazia in its socio-economic development
|
|-
|17 February 2010
|<ref name=uzel165534>{{cite news|title=Абхазия и Россия подпишут соглашение об объединенной военной базе|url=http://georgia.kavkaz-uzel.ru/articles/165534/|accessdate=18 June 2012|newspaper=[[Caucasian Knot]]|date=17 February 2000}}</ref>
|
|
|6 October 2011
|<ref name=uzel193669>{{cite news|title=Президент России ратифицировал соглашения о базах в Южной Осетии и Абхазии|url=http://www.kavkaz-uzel.ru/articles/193669/|accessdate=18 June 2012|newspaper=[[Caucasian Knot]]|date=6 October 2011}}</ref>
|Agreement on a joint Russian military base in Abkhazia
|Valid for 49 years, automatically renewable for 15-year periods
|-
|26 April 2011
|<ref name=apress6541/>
|15 June 2012
|<ref name=apress6541/>
|
|
|Agreement on the establishment of informatory-cultural centres and the conditions governing their activities
|
|-
|28 May 2012
|<ref name=apress6386>{{cite news|title=Подписано соглашение между правительствами Республики Абхазия и Российской Федерации о режиме торговли товарами|url=http://apsnypress.info/news/6386.html|accessdate=30 May 2012|newspaper=[[Apsnypress]]|date=29 May 2012}}</ref>
|
|
|
|
|Agreement on the trade of goods
|Abolishes import duties on most products, immediately for Russia, from 1 January 2015 onwards for Abkhazia
|}

==See also==
*[[Foreign relations of Abkhazia]]
*[[Foreign relations of Russia]]

==References==
{{reflist|30em}}

==External links==
{{commons category|Relations of Abkhazia and Russia}}
* {{ru icon}} [http://www.mid.ru/ns-reuro.nsf/strana?OpenView&Start=1&Count=30&Expand=1#1 Documents on the Abkhazia–Russia relationship from the Russian Ministry of Foreign Affairs]

{{Foreign relations of Abkhazia}}
{{Foreign relations of Russia|Asia}}

{{DEFAULTSORT:Abkhazia-Russia relations}}
[[Category:Abkhazia–Russia relations| ]]
[[Category:Articles containing video clips]]