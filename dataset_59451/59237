{{Infobox journal
| title = Open Astronomy
| cover =
| editor = 
| discipline = [[Astronomy]]
| former_names = Baltic Astronomy
| abbreviation = Open Astron.
| publisher = [[De Gruyter Open]]
| country = 
| frequency = 
| history = 1992-present
| openaccess = Yes
| license = 
| impact = 0.346 (Baltic Astronomy)
| impact-year = 2015
| website = https://www.degruyter.com/view/j/astro
| link1 =
| link1-name =
| link2 = http://www.tfai.vu.lt/balticastronomy/contents
| link2-name = Tables of contents of ''Baltic Astronomy''
| JSTOR = 
| OCLC = 50136117
| LCCN = 95640784
| CODEN = 
| ISSN = 2543-6376
| ISSN2 = 1392-0049
| ISSN2label = ''Baltic Astronomy'':
| eISSN = 
}}
'''''Open Astronomy''''' (formerly ''Baltic Astronomy'') is a [[peer-reviewed]] fully [[open access]] [[scientific journal]], and currently published by [[De Gruyter Open]]. The journal was established in 1992 by the Institute of Theoretical Physics and Astronomy ([[Vilnius University]], [[Lithuania]]) as ''Baltic Astronomy'', obtaining its current title in 2017 when it converted to [[open access]]. The journal is devoted to publishing research, reviews and news spanning all aspects of [[astronomy]] and [[astrophysics]]. The [[editor in chief]] is Philip Judge ([[High Altitude Observatory]]).

== History ==
Open Astronomy is the further continuation of publishing in the open access model Baltic Astronomy. Baltic Astronomy was published by the Institute of Theoretical Physics and Astronomy for astronomical institutions of the [[Baltic states]]. The journal sponsored by the [[Ministry of Education and Science (Lithuania)]]. Baltic Astronomy was published quarterly (4 issues per year).  The journal was preceded by the Vilnius Astronomijos Observatorijos Biuletenis (Bulletin of the Vilnius Astronomical Observatory), published from 1960–1992 by [[Vilnius University Astronomical Observatory]], in collaboration with the Institute of Physics and Mathematics. The [[editor in chief]] of Baltic Astronomy was [[Vytautas Straižys]] since 1992.

==Abstracting and indexing==
The journal is abstracted and indexed by:
* [[Astrophysics Data System]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences<ref name=ISI/>
* [[Inspec]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=2017-01-11}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-01-11}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2017-01-11}}</ref>
* [[VINITI]]/[[Referativny Zhurnal|Referativny Zhurnal Astronomija]]


==References==
{{Reflist}}

==External links==
*{{Official website|https://www.degruyter.com/view/j/astro}}

[[Category:Astronomy journals]]
[[Category:Publications established in 1992]]
[[Category:Quarterly journals]]
[[Category:1992 establishments in Lithuania]]
[[Category:English-language journals]]
[[Category:Open access journals]]
[[Category:Walter de Gruyter academic journals]]