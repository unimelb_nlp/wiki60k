{{about|the serial killings|the 1965 film|The Alphabet Murders|the poem|John Tranter}}
{{Infobox serial killer
| name=The Alphabet Killer
| birthname= Joseph Naso
| alias=The Double Initial Killer
| victims=3
| country=United States
| states=[[New York (state)|New York]], [[California]] (possible)
| beginyear=November 16, 1971
| endyear=November 26, 1973
| apprehended=
}}
The '''alphabet murders''' (also known as the "double initial murders") occurred in the 1970s in the [[Rochester, New York]], area and possibly in [[Los Angeles]], [[California]].

==New York alphabet murders==
Three young girls were [[rape]]d and [[strangled]] in the [[Rochester, New York]] area.

The case received its name from the fact that each of the girls' first and last names started with the same letter. Furthermore, each body was found in a town that had a name starting with the same letter as the victim's name:

* Carmen Colon in [[Churchville, New York|Churchville]]
* Michelle Maenza in [[Macedon, New York|Macedon]]
* Wanda Walkowicz in [[Webster, New York|Webster]]

Investigators have theorized that a series of murders with similar circumstances in California, in the late 1970s, is connected to these three murders.

Although hundreds of people were questioned, the killer was never caught. One man, considered to be a [[person of interest]] (he committed suicide six weeks after the last of the murders) was cleared in 2007 by [[DNA profiling]].<ref>[http://www.rnews.com/Story_2004.cfm?ID=46356&rnews_story_type=18&category=10 Double Initial DNA Test Clears Man], [[R News]], February 21, 2007.{{Dead link|date=April 2012}}</ref>

In the case of Carmen Colon, her uncle was also considered a suspect until his suicide in 1991.<ref>{{cite book |title=The Mammoth Book of Killers at Large |last=Cawthorne |first=Nigel |authorlink= |year=2007 |publisher=Robinson |location=UK |isbn=978-1-84529-631-5 |pages=209 }}</ref>

Another suspect was [[Kenneth Bianchi]], who at the time was an ice cream vendor in [[Rochester, New York]], vending from sites close to the first two murder scenes. He was a Rochester native who later moved to [[Los Angeles]] and, with his cousin [[Angelo Buono, Jr.]], committed the [[Hillside Strangler]] murders between 1977 and 1978.<ref name="D&C_Bianchi">{{cite news |first=Gary |last=Craig |title=Serial killer Bianchi denies he is 'double initial' slayer |url=http://www.democratandchronicle.com/article/20090302/NEWS01/903020343 |work=[[Democrat and Chronicle]] |publisher=[[Gannett]] |location=[[Rochester, New York|Rochester]] |page=6A |date=March 2, 2009 |accessdate=March 2, 2009 |quote=Bianchi was a suspect in the double initial killings because he lived in Rochester in the early 1970s and was a security guard.}}</ref>

Bianchi was never charged with the alphabet murders, and he has repeatedly tried to have investigators officially clear him of suspicion. However, there is circumstantial evidence that his car was seen at two of the murder scenes.

===Details===
* Carmen Colon, 10, disappeared November 16, 1971. She was found two days later in [[Riga, New York]], near [[Churchville, New York|Churchville]], 12 miles from where she was last seen.<ref name="D&C_background_feature">{{cite news |first=Gary |last=Craig |title='Double initial' murders remain mystery after 35 years |url=http://www.democratandchronicle.com/article/20090301/NEWS01/903010314 |work=[[Democrat and Chronicle]] |publisher=[[Gannett]] |location=[[Rochester, New York|Rochester]] |pages=1A, 8A |date=March 1, 2009 |accessdate=March 2, 2009 |quote=Two days later, her crumpled body was found in a gully, lying against a rock, along an infrequently traveled road in the town of Riga, near the Chili border.}}</ref>
* Michelle Maenza, 11, disappeared November 26, 1973. She was found two days later in [[Macedon, New York]], 15 miles from Rochester.
* Wanda Walkowicz, 11, disappeared April 2, 1973. She was found the next day at a rest area off [[New York State Route 104|State Route 104]] in [[Webster,  New York]], 7 miles from Rochester.

== California alphabet murders ==
On April 11, 2011, 77-year-old [[Joseph Naso]], a New York native who lived in Rochester, New York, during the 1970s, was arrested in [[Reno, Nevada]], for four murders in California (in 1977, 1978, 1993, and 1994). He was a professional photographer who had traveled between New York and California extensively for decades.<ref name="Burton" /><ref name="Lee" /><ref>{{cite web | last = Dearen | first = Jason |author2=Scott Sonner | title = What's in a name? It may link Calif, NY cold cases | work = The Salem News | publisher = A.P. | date = 13 April 2011 | url = http://www.salemnews.com/nationalnews/x1240518430/Whats-in-a-name-It-may-link-Calif-NY-cold-cases | accessdate = 3 April 2012}}</ref><ref>{{cite news | last = | first = | authorlink = | author = CNN Wire Staff | title = 77-year-old man charged in four slayings dating to 1977 | work = CNN Justice | publisher = CNN | date = 12 April 2011 | url = http://www.cnn.com/2011/CRIME/04/12/nevada.killings/index.html?iref=NS1 | accessdate = 3 April 2012}}</ref><ref>{{cite web | last = Dearen | first = Jason |author2=Scott Sonner | title = Eerie similarites &#91;sic&#93;  noted in NY, Calif. cold cases | work = Crime & Courts on MSNBC.com | publisher = A.P. | date= | url = http://www.msnbc.msn.com/id/42558019/ns/us_news-crime_and_courts/ | format = | doi = | accessdate = 3 April 2012}}</ref>   

All four of the murdered women were described by authorities as prostitutes.<ref name="Burton" >{{cite news | author=Justin Berton | url=http://articles.sfgate.com/2011-07-07/bay-area/29745957_1_case-law-safe-deposit-box-conduct-legal-research | title=Joseph Naso now wants an attorney for murder trial | publisher=SFGate.com | date=July 7, 2011 | accessdate=August 20, 2011}}</ref><ref name="Lee" >{{cite news | author=Henry K. Lee | url=http://articles.sfgate.com/2011-06-17/bay-area/29668591_1_list-photos-carmen-colon | title=Slaying suspect Joseph Naso kept notes on victims | publisher=SFGate.com | date=June 17, 2011 | accessdate=August 20, 2011}}</ref>

Naso was a [[person of interest]] in the Rochester, New York, alphabet murders, but his DNA did not match samples taken from those victims.

On January 12, 2012, in his preliminary hearing in [[Marin County, California]], his alleged "rape diary" was entered into evidence.  It mentioned the death of a girl in the "Buffalo woods," a possible allusion to [[Upstate New York]].<ref>{{cite news | last = Dillon | first = Nancy | title = Joseph Naso, suspected serial killer, kept rape diary: authorities | work = NYDailyNews.com | publisher =New York Times| date = 12 January 2012 | url = http://www.nydailynews.com/news/national/joseph-naso-suspected-serial-killer-rape-diary-authorities-article-1.1005103 | accessdate = 3 April 2012}}</ref> 

On June 18, 2013, Naso was tried for the murder of the four California alphabet murder victims.<ref>{{cite news |url=http://www.nydailynews.com/news/national/accused-serial-killer-claims-monster-prosecutors-article-1.1375752 |title=Accused 'Double Initial' serial killer Joseph Naso, on trial for killing four prostitutes, claims he is not the 'monster' prosecutors say he is |work=[[Daily News (New York)]] |publisher=[[Mortimer Zuckerman]] |date=2013-06-18 |accessdate=2014-09-13 }}</ref> On August 20, 2013, Naso was convicted by a Marin County jury of the murders. On November 22, 2013, Naso was sentenced to death for the murders.<ref>{{cite news |last=Klein |first=Gary |url=http://www.mercurynews.com/breaking-news/ci_24580867/marin-judge-sentences-joseph-naso-death-row-murders |title=Marin judge sentences Joseph Naso to death row for murders of six women |work=[[San Jose Mercury News]] |date=2013-11-22 |accessdate=2014-09-13 }}</ref>

===Details===
The California murder victims, like the New York victims, had double initials. 

* Carmen Colon (not the Rochester, New York victim of the same name)
* Pamela Parsons
* Roxene Roggasch, 18, was found dead on January 11, 1977, on the side of a road near [[Fairfax, California]]
* Tracy Tofoya

== In the media ==
*In 2001, the [[Discovery Channel]] aired a program revisiting the murders.<ref>[https://tv.yahoo.com/murder-reopened-the-alphabet-killer/show/17617/castcrew;_ylt=A0WTfmOfJqZG89sAkAuSo9EF ''Murder Reopened—The Alphabet Killer''] @ [[Yahoo!]] TV</ref> 
*A 2008 film titled ''[[The Alphabet Killer]]'' was very loosely based on the murders.<ref>{{IMDb title|id=0818165|title=The Alphabet Killer}}</ref> 
*In 2010, a book titled ''Alphabet Killer: The True Story of the Double Initial Murders'' was released by author [[Cheri Farnsworth]].<ref>{{cite news | first=Craig | last=Gary | title=New book delves deeper into Rochester unsolved Double Initial murders | url=http://www.democratandchronicle.com/article/20100907/NEWS01/9070316 | work =Democrat and Chronicle | accessdate = 2010-09-07}}</ref><ref>{{cite book | first=Farnsworth | last=Cheri | title=Alphabet Killer: The True Story of the Double Initial Murders | publisher=Stackpole Books | year=2010 | isbn=978-0-8117-0632-2}}</ref> 
*On April 1, 2011, the [[AMC (TV channel)|AMC]] network aired a short documentary titled ''Countdown to the Killing: The Alphabet Murders''.<ref>[http://www.amctv.com/the-killing/videos/countdown-to-the-killing-the-alphabet-murders ''Countdown to The Killing: Alphabet Murders''] @ [[AMC (TV channel)]] TV</ref>

==See also==
*''[[The A.B.C. Murders]]'', a 1936 detective novel by [[Agatha Christie]] describing a similar series of killings.

==References==
{{reflist|30em}}

==External links==
* [http://www.troopers.ny.gov/Wanted_and_Missing/Homicide/view.cfm?ID=13364759-65d4-4411-bc90-c667b49e7286 New York State Police Homicide Victim] (Information about the case of victim Wanda Walkowicz.)

[[Category:American serial killers|Alphabet Killer, The]]
[[Category:1971 murders in the United States]]
[[Category:1973 murders in the United States]]
[[Category:Unidentified serial killers]]
[[Category:Murderers of children]]
[[Category:History of Rochester, New York]]
[[Category:Unsolved murders in the United States]]
[[Category:Crimes in the San Francisco Bay Area]]
[[Category:People from the San Francisco Bay Area]]
[[Category:Serial murders in the United States]]
[[Category:Murdered American children]]
[[Category:Child sexual abuse]]
[[Category:Pedophilia]]
[[Category:Male serial killers]]
[[Category:1970s in New York]]