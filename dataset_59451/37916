{{good article}}
{{use mdy dates|date=February 2014}}
{{Infobox scientist
| name  = Albert Francis Birch
| image = Albert Francis Birch00.jpg
| image_size = 250px
| caption  = Francis Birch
| birth_date  = {{Birth date|1903|8|22}}
| birth_place = [[Washington, D.C.]]
| death_date  = {{Death date and age|1992|1|30|1903|8|22|df=yes}}
| death_place = [[Cambridge, Massachusetts]]
| death_cause =
| resting_place =
| field = [[Geophysics]]
| alma_mater  = [[Harvard University]]
| doctoral_advisor  = [[Percy Bridgman]]
| doctoral_students = 
| work_institution = [[Harvard University]]
| known_for  = [[Birch's law]]<br/>[[Birch-Murnaghan equation of state]]
| prizes     = [[Legion of Merit]] (1945)<br/>[[Arthur L. Day Medal]] (1950)<br/>[[William Bowie Medal]] (1960)<br/>[[National Medal of Science]] (1967)<br/>[[Vetlesen Prize]] (1968)<br/>[[Penrose Medal]] (1969)<br/>[[Gold Medal of the Royal Astronomical Society]] (1973)
| signature  = Francis Birch signature.jpg|
| spouse     = Barbara Channing
}}
'''Francis Birch''' (August 22, 1903 – January 30, 1992) was an [[United States|American]] [[geophysicist]]. He is considered one of the founders of solid Earth [[geophysics]]. He is also known for his part in the [[atomic bombing of Hiroshima and Nagasaki]].

During [[World War II]], Birch participated in the [[Manhattan Project]], working on the design and development of the [[gun-type nuclear weapon]] known as [[Little Boy]]. He oversaw its manufacture, and went to [[Tinian]] to supervise its assembly and loading into ''[[Enola Gay]]'', the [[Boeing B-29 Superfortress]] tasked with dropping the bomb.

A graduate of [[Harvard University]], Birch began working on [[geophysics]] as a research assistant. He subsequently spent his entire career at Harvard working in the field, becoming an Associate Professor of Geology in 1943, a professor in 1946, and Sturgis Hooper Professor of Geology in 1949, and [[professor emeritus]] in 1974.

Birch published over 100 papers. He developed what is now known as the [[Birch-Murnaghan equation of state]] in 1947. In 1952 he demonstrated that Earth's [[Mantle (geology)|mantle]] is chiefly composed of [[silicate minerals]], with an [[inner core|inner]] and [[outer core|outer]] core of molten [[iron]]. In two 1961  papers on [[compressional wave]] velocities, he established  what is now called [[Birch's law]].

==Early life==
'''Albert Francis Birch''' was born in Washington, D.C., on August 22, 1903, the son of George Albert Birch, who was involved in banking and real estate, and Mary Hemmick Birch, a church choir singer and soloist at [[Cathedral of St. Matthew the Apostle (Washington, D.C.)|St. Matthew's Cathedral]] in Washington, D.C. He had three younger brothers: David, who became a banker; John, who became a diplomat; and Robert, who became a songwriter. He was educated at Washington, D.C., schools, and [[Duke Ellington School of the Arts|Western High School]], where he joined the [[Junior Reserve Officers' Training Corps|High School Cadets]] in 1916.{{sfn|Ahrens|1998|p=4}}<ref name="obit">{{cite news |newspaper=[[The New York Times]] |date=February 5, 1992 |first=Walter |last=Sullivan |title=Francis Birch, Is Dead at 88; Was a Professor}}</ref>

In 1920 Birch  entered [[Harvard University]] on a scholarship. While there he served in Harvard's [[Reserve Officers' Training Corps]] Field Artillery Battalion. He graduated [[magna cum laude]] in 1924, and received his [[bachelor of science]] (S.B.) degree in [[electrical engineering]].{{sfn|Ahrens|1998|p=4}}

Birch went to work in the Engineering Department of the [[New York Telephone Company]]. He applied for and received an [[AFS Intercultural Programs|American Field Service]] Fellowship in 1926, which he used to travel to [[Strasbourg]], and study at the [[University of Strasbourg]]'s Institut de Physique under the tutelage of [[Pierre Weiss]].<ref name="papers">{{cite web |url=http://nrs.harvard.edu/urn-3:HUL.ARCH:hua20003  |accessdate=February 4, 2014 |publisher=[[Harvard University]] |title=Birch, Francis, 1903-1992. Papers of Francis Birch : an inventory ( HUGFP 132 ) }}</ref> There, he wrote or co-wrote four papers, in French, on topics such as the [[paramagnetic]] properties of [[potassium cyanide]], and the [[magnetic moment]] of Cu++ ions.{{sfn|Ahrens|1998|p=5}}

On returning to the United States in 1928, Birch went back to Harvard to pursue physics. He was awarded his [[master of arts]] (A.M.) degree in 1929, and then commenced work on his 1932 [[doctor of philosophy]] (Ph.D.) degree under the supervision of [[Percy Bridgman]],<ref name="papers"/> who would receive the [[Nobel Prize for Physics]] in 1946. For his thesis, Birch measured the [[Critical point (thermodynamics)|vapor-liquid critical point]] of [[Mercury (element)|mercury]]. He determined this as 1460±20&nbsp;°C and 1640±50&nbsp;kg/cm<sup>2</sup>, results he published in 1932 in the [[Physical Review]].{{sfn|Ahrens|1998|p=5}}<ref>{{cite journal |last=Birch |first=Francis |title=The Electrical Resistance and the Critical Point of Mercury |journal=Physical Review |issn=1050-2947 |date=September 1932 |volume=41 |issue=5 |pages=641–648 |doi=10.1103/PhysRev.41.641 |ref=harv|bibcode = 1932PhRv...41..641B }}</ref>

Around this time, there was an increased interest in geophysics at Harvard University, and [[Reginald Aldworth Daly]] established a Committee for Experimental Geology and Geophysics that included Bridgman, astronomer [[Harlow Shapley]], geologists [[Louis Caryl Graton]] and D. H. McLaughlin and chemist G. P. Baxter. [[William Zisman]], another one of Bridgman's Ph.D. students, was hired as the committee's research associate, but, having little interest in the study of rocks, he resigned in 1932. The position was then offered to Birch, who had little interest or experience in geology either, but with the advent of the [[Great Depression]], jobs were hard to find, and he accepted. {{sfn|Ahrens|1998|p=7}}<ref name="Reminiscences">{{cite journal |last=Birch |first=Francis |title=Reminiscences and digressions |journal=Annual Review of Earth and Planetary Sciences |issn=0084-6597 |volume=7 |date=1979  |pages=1–10 |doi=10.1146/annurev.ea.07.050179.000245|bibcode = 1979AREPS...7....1B }}</ref>

On July 15, 1933, Birch married Barbara Channing, a [[Bryn Mawr College]] alumna, and a [[collateral descendant]] of the theologian [[William Ellery Channing]]. They had three children: Anne Campaspe, Francis (Frank) Sylvanus and Mary Narcissa. Frank later became a professor of geophysics at the [[University of New Hampshire]].{{sfn|Ahrens|1998|p=8}}{{sfn|Sedgwick|1961|p=245}}

==World War II==
[[File:Atombombe Little Boy.jpg|thumb|tight|Birch (left) works on the [[Little Boy]] bomb while [[Norman F. Ramsey]] (right) looks on]]
In 1942, during [[World War II]], Birch took a leave of absence from Harvard, in order to work at the [[Massachusetts Institute of Technology]] [[Radiation Laboratory]], which was developing [[radar]]. He worked on the [[proximity fuze]], a radar-triggered fuze that would explode a shell in the proximity of a target. The following year he accepted a commission in the United States Navy as a [[lieutenant commander]], and was posted to the [[Bureau of Ships]] in Washington, D.C.{{sfn|Ahrens|1998|pp=10–11}}

Later that year he was assigned to the [[Manhattan Project]], and moved with his family to [[Los Alamos, New Mexico]]. There he joined the [[Los Alamos Laboratory]]'s Ordnance (O) Division, which was under the command of another Naval officer, [[Captain (United States Navy)|Captain]] [[William S. Parsons]]. Initially the goal of the O Division was to design a [[gun-type nuclear weapon]] known as [[Thin Man (nuclear bomb)|Thin Man]]. This proved to be impractical due to contamination of the reactor-bred [[plutonium]] with [[plutonium-240]], and in February 1944, the Division switched its attention to the development of the [[Little Boy]], a smaller device using [[uranium-235]]. Birch used unenriched uranium to create scale models and later full-scale mock-ups of the device.{{sfn|Ahrens|1998|pp=11–13}}
   
Birch supervised the manufacture of the Little Boy, and went to Tinian to supervise its assembly and loading it onto ''[[Enola Gay]]'', the [[Boeing B-29 Superfortress]] tasked with dropping the bomb. He devised the 'double plug' system that allowed for actually arming the bomb after ''Enola Gay'' took off so that if it crashed, there would not be a nuclear explosion.{{sfn|Ahrens|1998|pp=11–13}} He was awarded the [[Legion of Merit]]. His citation read:{{quote|for exceptionally meritorious conduct in the performance of outstanding services to the Government of the United States in connection with the development of the greatest military weapon of all time, the atomic bomb. His initial assignment was the instrumentation of laboratory and field tests. He carried out this assignment in such outstanding fashion that he was placed in charge of the engineering and development of the first atomic bomb. He carried out this assignment with outstanding judgment and skill, and finally, went with the bomb to the advanced base where he insured, by his care and leadership, that the bomb was adequately prepared in every respect. Commander Birch's engineering ability, understanding of all principles involved, professional skill and devotion to duty throughout the development and delivery of the atomic bomb were outstanding and were in keeping with the highest traditions of the United States Naval Service.<ref>{{cite web |url=http://projects.militarytimes.com/citations-medals-awards/recipient.php?recipientid=307146 |title=Valor awards for Albert Francis Birch |publisher=Military Times |accessdate=February 6, 2014  }}</ref> }}

==Post-war==
Birch returned to Harvard after the war ended, having been promoted to Associate Professor of Geology in 1943 while he was away. He would remain at Harvard for the rest of his career, becoming a professor in 1946, and Sturgis Hooper Professor of Geology in 1949, and [[professor emeritus]] in 1974. Professor Birch published over 100 papers.<ref name="obit"/><ref name="papers"/>

In 1947, he adapted the isothermal Murnaghan equation of state, which had been developed for infinitesimal strain, for [[Eulerian]] finite strain, developing what is now known as the [[Birch-Murnaghan equation of state]].<ref name="PhysRev47">{{cite journal |last=Birch |first=Francis |journal=Physical Review |issn=1050-2947 | date= June 1947 |title=Finite Elastic Strain of Cubic Crystals|volume=71 |issue=11 |pages=809–824 |doi=10.1103/PhysRev.71.809|bibcode = 1947PhRv...71..809B }}</ref>

Albert Francis Birch is known for his experimental work on the properties of Earth-forming [[minerals]] at high pressure and temperature, in 1952 he published a well-known paper in the [[Journal of Geophysical Research]], where he demonstrated that the [[Mantle (geology)|mantle]] is chiefly composed of [[silicate minerals]], the upper and lower mantle are separated by a thin transition zone associated with silicate [[phase transitions]], and the [[inner core|inner]] and [[outer core|outer]] core are alloys of crystalline and molten [[iron]]. His conclusions are still accepted as correct today. The most famous portion of the paper, however, is a humorous footnote he included in the introduction:<ref name="JGR52">{{cite journal |last=Birch |first=Francis  |title=Elasticity and Constitution of the Earth's Interior |journal=[[Journal of Geophysical Research]] |issn=0148-0227 |volume=57 |issue=2 |pages=227–286 |date=June 1952 |doi=10.1029/JZ057i002p00227 |bibcode = 1952JGR....57..227B }}</ref>

<blockquote>
Unwary readers should take warning that ordinary language undergoes modification to a high-pressure form when applied to the interior of the Earth. A few examples of equivalents follow:
<center>
{| class="wikitable"
|-
! High Pressure Form
! Ordinary Meaning
|-
| Certain
| Dubious
|-
| Undoubtedly
| Perhaps
|-
| Positive proof
| Vague suggestion
|-
| Unanswerable argument
| Trivial objection
|-
| Pure iron
| Uncertain mixture of all the elements
|}
</center>
</blockquote>

In 1961, Birch published two papers on [[compressional wave]] velocities establishing a linear relation of the compressional wave velocity V<sub>p</sub> of rocks and minerals of a constant average [[atomic weight]] <math>\bar{ M}</math> with density <math>\rho</math> as:<ref name="JGR61">{{cite journal |last=Birch |first=Francis |date=July 1961 |title=The velocity of Compressional Waves in Rocks to 10 kilobars. Part 2 |journal=[[Journal of Geophysical Research]] |issn=0148-0227 |volume=66 |issue=7 |pages=2199–2224 |doi= 10.1029/JZ066i007p02199 |bibcode = 1961JGR....66.2199B }}</ref><ref name="GJRAS61">{{cite journal |last=Birch |first=Francis |journal=Geophysical Journal of the Royal Astronomical Society |issn=1365-246X |date=December 1961 |title=Composition of the Earth's Mantle |volume=4 |pages=295–311|doi=10.1111/j.1365-246X.1961.tb06821.x|bibcode = 1961GeoJI...4..295B }}</ref>

<center><math> V_p = a (\bar{ M}) + b \rho </math>.</center>

This relationship became known as [[Birch's law]]. Birch was elected to the [[National Academy of Sciences]] in 1950,<ref name="Physics Today"/> and served as the president of the [[Geological Society of America]] in 1963 and 1964.<ref name="papers"/> He received numerous honors in his career, including the Geological Society of America's [[Arthur L. Day Medal]] on 1950 and [[Penrose Medal]] in 1969, the [[American Geophysical Union]]'s [[William Bowie Medal]] in 1960, the [[National Medal of Science]] from President [[Lyndon Johnson]] in 1967, the [[Vetlesen Prize]] (shared with Sir [[Edward Bullard]]) in 1968, the [[Gold Medal of the Royal Astronomical Society]] in 1973, and the International Association for the Advancement of High Pressure Research's [[Percy Bridgman|Bridgman]] Medal in 1983.{{sfn|Ahrens|1998|p=20}} Since 1992, the American Geophysical Union's [[Tectonophysics]] section has sponsored a Francis Birch Lecture, given at its annual meeting by a noted researcher in this field.<ref>{{cite web |url=http://honors.agu.org/bowie-lectures/francis-birch-1903-1992/ |first=Kenneth |last=Olsen |title=Francis Birch (1903–1992) |publisher=[[American Geophysical Union]] |accessdate=February 6, 2014 }}</ref>

Birch died of [[prostate cancer]] at his home in [[Cambridge, Massachusetts]], on January 30, 1992. He was survived by wife Barbara, his three children and his three brothers.<ref name="obit"/><ref name="Physics Today">{{cite journal|last=Shankland |first=Thomas|last2=O'Connell |first2=Richard|title=Obituary: Francis Birch|journal=Physics Today|date=November 1992 |issn=0031-9228|volume=45|issue=11|pages=105–106|url=http://www.physicstoday.org/resource/1/phtoad/v45/i11/p105_s2?bypassSSO=1|doi=10.1063/1.2809890|bibcode = 1992PhT....45k.105S }}</ref> His papers are in the Harvard University Archives.<ref name="papers"/>

==Notes==
{{reflist|30em}}

==References==
*{{cite book |last=Ahrens |first=Thomas J. |volume=74 |pages=1–24 |title=Albert Francis Birch |url=http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/birch-francis.pdf |location=Washington, D.C. |date=1998 |accessdate=February 4, 2014 |publisher=National Academy of Sciences  |series=Biographical Memoirs |ref=harv }}
*{{cite book |last=Sedgwick |first=Hubert M. |title=A Sedgwick Genealogy: Descendants of Deacon Benjamin Sedgwick |location=New Haven, Connecticut |publisher=New Haven Colony Historical Society |date=1961 |url=http://www.sedgwick.org/na/library/books/sed1961/sed1961-245.html  |accessdate=February 4, 2014 |ref=harv}}

{{Presidents of the Geological Society of America}}
{{Winners of the National Medal of Science}}
{{Portal bar|World War II|Geology|Physics|History of science|Biography|Nuclear technology}}
{{Authority control}}

{{DEFAULTSORT:Birch, Francis}}
[[Category:1903 births]]
[[Category:1992 deaths]]
[[Category:People from Washington, D.C.]]
[[Category:Harvard University alumni]]
[[Category:Harvard University faculty]]
[[Category:American geophysicists]]
[[Category:Penrose Medal winners]]
[[Category:National Medal of Science laureates]]
[[Category:Recipients of the Gold Medal of the Royal Astronomical Society]]
[[Category:Manhattan Project people]]
[[Category:People associated with the atomic bombings of Hiroshima and Nagasaki]]
[[Category:Deaths from prostate cancer]]