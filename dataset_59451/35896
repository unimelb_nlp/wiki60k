{{good article}}
{{Use dmy dates|date=January 2014}}
{{Infobox organization
| name = Agapemonites
| native_name = Community of The Son of Man
| image = Postcard of The Agapemone, Spaxton, Somerset 1907.jpg
| image_border =
| size =
| alt =
| caption = A 1907 postcard of the Agapemone, Spaxton, Somerset
| map =
| map_size =
| map_alt =
| map_caption =
| map2 =
| abbreviation =
| motto =
| mission =
| predecessor =
| successor =
| formation = <!-- {{Start date and age|YYYY|MM|DD}} -->
| founded = 1846
| founder = Henry Prince
| extinction = 1956
| type = Religious sect
| status =
| purpose =
| professional_title = <!-- for professional associations -->
| headquarters = [[Spaxton]], [[Somerset]], England
| location =
| coords = {{Coord|51.1279|-3.0972|type:landmark|display=inline,title}}
| region_served =
| services =
| membership = 100–500
| language =
| general =
| leader_title = John Hugh Smyth-Pigott
| leader_name =
| leader_title2 =
| leader_name2 =
| leader_title3 =
| leader_name3 =
| leader_title4 =
| leader_name4 =
| board_of_directors =
| key_people =
| main_organ =
| parent_organization = <!-- or | parent_organisation = -->
| subsidiaries =
| affiliations =
| budget =
| num_staff =
| num_volunteers =
| slogan =
| website = <!-- {{URL|example.org}} -->
| remarks =
| footnotes =
| former name =
}}
The '''Agapemonites''' or '''Community of The Son of Man''' was a [[Christian]] [[Religious denomination|religious group]] or sect that existed in England from 1846 to 1956. It was named from the {{lang-el|''agapemone''}} meaning "'''abode of love'''". The Agapemone community was founded by the Reverend Henry Prince in [[Spaxton]], [[Somerset]]. The sect also built a church in [[Upper Clapton#The Abode of Love|Upper Clapton]], London, and briefly had bases in [[Stoke-by-Clare]] in [[Suffolk]], [[Brighton]] and [[Weymouth, Dorset|Weymouth]].

The ideas of the community were based on the theories of various German religious [[mysticism|mystics]] and its primary object was the spiritualisation of the matrimonial state.<ref name="EB1911">{{EB1911|inline=1|wstitle=Agapemonites|volume=1|pages=365-366}}</ref> The [[Church of England]] had dismissed Prince earlier in his career for his radical teachings. The Agapemonites predicted the imminent return of [[Jesus Christ]]. According to newspaper accounts, Prince's successor, John Hugh Smyth-Pigott, declared himself Jesus Christ [[reincarnation|reincarnate]].

The Agapemone community consisted mostly of wealthy unmarried women. Both Prince and Smyth-Pigott took many spiritual [[bride]]s. Later investigations have shown that these "brides" were not solely spiritual and that some produced [[Legitimacy (law)|illegitimate]] children. In 1860, Prince lost a lawsuit brought on behalf of [[Louisa Nottidge]] by the Nottidge family  and the group vanished from the public eye. It finally closed in 1956 when the last member died.

==Henry James Prince==
[[File:Rev Henry Prince.jpg|thumb|The Reverend Henry Prince (1811–99)]]
The Reverend Henry James Prince (1811–99) studied medicine at [[Guy's Hospital]],{{sfn|Evans|2006|p=21}} obtained his qualifications in 1832 and was appointed medical officer to the General Hospital in [[Bath, Somerset|Bath]], his native city.{{sfn|Stunt|2006|p=27}} Compelled by ill health to abandon his profession, he entered himself in 1837 as a student at [[University of Wales, Lampeter|St David's College, Lampeter]] (now the [[University of Wales Trinity Saint David#Lampeter Campus|Lampeter campus]] of the [[University of Wales Trinity Saint David]]), where he gathered about him a band of earnest religious enthusiasts known as the [[Lampeter Brethren]].{{sfn|Evans|2006|p=21}} The vice principal of the college contacted the [[Bishop of Bath and Wells]] who, in 1846, installed Prince as the curate of Charlinch in [[Somerset]], where he had sole charge during the illness and absence of the rector, the Reverend Samuel Starkey.<ref name="EB1911"/>{{sfn|Evans|2006|p=22}}

Attendances at the church were small until, during one of the services, Prince acted as if he was possessed, throwing himself around the church. Congregations grew each week as the "possession" was repeated. The congregation were then divided with separate services for men and women. Subsequently, he separated them again into sinners and the righteous, which generally included females who were wealthy. The bishop was summoned to investigate the practices.{{sfn|Evans|2006|pp=22–23}} By that time, Prince had contracted his first "spiritual marriage" and had persuaded himself that he had been absorbed into the personality of [[God]] and become a visible embodiment of the [[Holy Spirit]].<ref>{{cite web|title=The Abode of Love|url=http://www.utopia-britannica.org.uk/pages/abode%20of%20love.htm|publisher=Utopia Britannica&nbsp;— British Utopian Experiments 1325 - 1945|accessdate=23 January 2014}}</ref> During his illness Starkey read one of his curate's sermons, and was not only "cured" forthwith, but embraced his strange doctrines. Together they procured many conversions in the countryside and the neighbouring towns. In the end the rector was deprived of his living and Prince was [[Defrocking|defrocked]]. Together with a few disciples they started the Charlinch Free Church, which had a very brief existence,<ref name="EB1911"/> meeting in a supportive farmer's barn.{{sfn|Gray|2009|pp=207–215}}

Prince used money inherited on the death of his first wife, Martha, to marry Julia Starkey, the sister of the rector.{{sfn|Dixon|1868|pp=161–166}} They all moved to [[Stoke-by-Clare]] in [[Suffolk]] where Prince started again to build a congregation, which grew over the subsequent one to two years. The [[Bishop of Ely]] then expelled them.{{sfn|Evans|2006|p=23}} Prince opened Adullam Chapel, which was also known as Cave Adullam, in the [[North Laine]] area of [[Brighton]]. Meanwhile, Starkey established himself at [[Weymouth, Dorset|Weymouth]].{{sfn|Armytage|2013|pp=272–275}} Their chief success lay in the latter town, and Prince soon moved there.{{sfn|Gray|2009|pp=207–215}}{{sfn|Grumley-Grennan|2010|pp=139–140}}

==Followers==
A number of followers, estimated by Prince at 500 but by his critics at one fifth of the number, were gathered together, and it was given out by "Beloved" or "The Lamb" (the names by which the Agapemonites designated their leader) that his disciples must divest themselves of their possessions and throw them into the common stock. This was done, even by the poor, all of whom looked forward to the speedy end of the present dispensation and were content, for the short remainder of this world, to live in common and, while not repudiating earthly ties, to treat them as purely spiritual. With the money thus obtained the house at [[Spaxton]] that was to become the "Abode of Love" was enlarged and furnished luxuriously, and the three Nottidge sisters, who contributed £6,000 each, were immediately married to three of Prince's nearest disciples.<ref name="EB1911"/> Agnes, the eldest of the Nottidge sisters, objected to the spiritual marriage which entailed a celibate life and, as one writer reports, became pregnant by another member of the community;{{sfn|Evans|2006|p=26-27}} however, it is unlikely that she committed adultery because her husband never accused her, and she later gained sole custody of their child in 1850 after proving herself of good moral character before a court.<ref>{{cite news|title=Thomas v.Roberts|newspaper=The Times|date= 22 May 1850}}</ref> Agnes wrote to her younger sister [[Louisa Nottidge|Louisa]] warning her not to come to Spaxton. Despite this Louisa travelled to Somerset to join them.{{sfn|Evans|2006|p=26-27}} Her mother Emily feared the spiritual and financial influence that Prince had established over her daughters. Emily instructed her son Edmund, her nephew Edward Nottidge, and her son-in-law, Frederick Ripley, to travel down to Somerset and to rescue her unmarried daughter, Louisa after her arrival.{{sfn|Evans|2006|p=26-27}} The three men succeeded in removing Louisa against her will in November 1846, and imprisoned her in 12 Woburn Place, a villa by Regents Park.<ref>''Nottidge v. Ripley and Another'' (1849), reported in ''The Times'': June 25–27, 1849</ref>

Following Louisa's persistent claims regarding the divinity of Henry Prince, her mother enlisted medical aid and had Louisa certified insane and then placed her in Moorcroft House [[lunatic asylum|Asylum]], [[Hillingdon]]. Her treatment and forced incarceration in the asylum has remained of interest with respect to the rights of psychiatric patients;{{sfn|Scull|1992}} Dr Arthur Stillwell, the presiding physician, made notes on Louisa's condition and treatment, recorded in ''The Lancet''.{{sfn|Stillwell|1849|pp=80–81}} Louisa escaped from the asylum in January 1848, travelling across London to meet the Reverend William Cobbe from The Agapemone at a hotel in [[Cavendish Square]], but was recaptured two days later at [[London Paddington station|Paddington railway station]].{{sfn|Mitchell|2004}}{{sfn|Wise|2012|p=107}} Cobbe alerted the Commissioners in Lunacy, whose report by [[Bryan Procter]] led to her release in May 1848. Louisa then sued her brother, cousin and brother-in-law, Frederick Ripley, for abduction and false imprisonment in ''Nottidge v. Ripley and Another'' (1849); the trial was reported daily in ''The Times'' newspaper.{{sfn|Schwieso|1996|pp=159–174}} In 1860 Louisa's brother, Ralph Nottidge, sued Prince to recoup the money that Louisa had given him as a result of his undue influence over her, in the case of ''Nottidge v. Prince'' (1860).<ref>{{cite web|title=Newsletter – Spring 2010|url=http://wilkiecollinssociety.org/newsletter-spring-2010/|publisher=The Wilkie Collins Society|accessdate=26 January 2014}}</ref><ref>{{cite web|title=The Agapemone Again|url=http://nla.gov.au/nla.news-article8793521|publisher=Trove digitised newspapers|accessdate=26 January 2014}}</ref> The Nottidges won the case, with costs.{{sfn|Parry|2010|p=138}}{{sfn|Sands|2012}} After the cases were resolved Louisa Nottidge returned to Spaxton and spent the rest of her life as one of the Agapemonites.{{sfn|Evans|2006|pp=26–27}}

In 1856, a few years after the establishment of the “Abode of Love”, Prince and Zoe Patterson, one of his virginal female followers, engaged in public ceremonial sexual intercourse on a billiard table in front of a large audience.{{sfn|Evans|2006|p=28}} The scandal led to the secession of some of his most faithful friends, who were unable any longer to endure what they regarded as the amazing mixture of blasphemy and immorality offered for their acceptance.{{sfn|Evans|2004|pp=27–33}} The most prominent of those who remained received such titles as the "Anointed Ones", the "Angel of the Last Trumpet", the "Seven Witnesses" and so forth.<ref name="EB1911"/>{{sfn|Waite|1964|pp=94–97}}

==Spaxton==
[[File:Geograph 1987454 The Agapemone Chapel.jpg|thumb|The Agapemone Chapel in [[Spaxton]]]]
Extensive building work was undertaken to accommodate members and followers at Four Forks in Spaxton,<ref>{{cite web |url=http://www.quantockonline.co.uk/quantocks/villages/spaxton/spaxton1.html |title=Spaxton |accessdate=4 December 2007 |work=Quantock Online }}</ref> to which Prince and his followers moved in the summer of 1846. Behind {{convert|15|ft}} high walls were built a 20-bedroom house and attached chapel, as well as a gazebo, stables, and cottages, all set within landscaped gardens.{{sfn|Evans|2006|p=25}} The buildings were designed by William Cobbe. The [[buttress]]ed chapel, with its [[pinnacle]]s and stained glass, was completed in 1845;<ref>{{cite web|title=The Agapemone, Four Forks, Spaxton|url=http://www.somersetheritage.org.uk/record/28706|work=Somerset Historic Environment Record|publisher=Somerset County Council|accessdate=25 January 2014}}</ref> today, together with the attached house, it is a Grade II [[Listed building (United Kingdom)|listed building]].<ref>{{cite web|title=No. 1; and attached former chapel to right|url=http://list.english-heritage.org.uk/resultsingle.aspx?uid=1178130|work=National Heritage List for England|publisher=English Heritage|accessdate=23 January 2014}}</ref>

Prince died in 1899 aged 88. His followers buried Prince in the grounds of the chapel, with his coffin positioned vertically so that he would be standing on the day of his resurrection.{{sfn|Evans|2006|p=29}}

In the early 20th century several houses (some in the [[Arts and Crafts]] style) were built at Four Forks by members of the Agapemonites including [[Joseph Morris (architect)|Joseph Morris]] and his daughter Violet.{{sfn|Dunning|Elrington|Baggs |Siraut|1992}}

Since closure of the community, the chapel has been used as a studio for the production of children's television programmes, including ''[[Trumpton]]'' and ''[[Camberwick Green]]''.{{sfn|Evans|2006|p=20}} The complex of buildings became known as Barford Gables and was put on the market in 1997.<ref>{{cite news|last=Jury|first=Louise|title=Vicar's sex cult excites buyers' interest in rustic Abode of Love|url=http://www.independent.co.uk/news/vicars-sex-cult-excites-buyers-interest-in-rustic-abode-of-love-1277680.html|accessdate=25 January 2014|newspaper=The Independent|date=9 February 1997}}</ref> The chapel received planning permission for conversion into a residential house and was put on the market again in 2004.<ref>{{cite news|last=Clark|first=Ross|title=The chapel of unrest|url=http://www.telegraph.co.uk/property/propertyadvice/propertymarket/3325822/The-chapel-of-unrest.html|accessdate=25 January 2014|newspaper=The Telegraph|date=16 June 2004}}</ref>

==Upper Clapton==
Between 1892 and 1895 the Agapemonites built the Church of the Good Shepherd in [[Upper Clapton]], London. It was designed by Joseph Morris in a [[Gothic architecture|Gothic style]].<ref>{{cite web|title=The Church of the Good Shepherd|url=http://www.pastscape.org.uk/hob.aspx?hob_id=1491140|work=Pastscape|publisher=English Heritage|accessdate=25 January 2014}}</ref> Although it is fairly conventional in floor plan, the outside of the church is a riot of statuary and symbolism. The main doorways sport large carvings of angels and the four evangelists symbolised by a man, an eagle, a bull and a lion. The same four figures, cast in bronze, look out over the four quarters of the Earth from the base of the steeple. The two flanking weather vanes show a certain symbolic debt to [[William Blake]]'s Jerusalem depicting, as they do, a fiery chariot and a sheaf of arrows (presumably of desire), while the main steeple is clearly surmounted by a spear. The stained glass windows, designed by noted children's book illustrator [[Walter Crane]], and made by J. S. Sparrow, betray the unconventional nature of the sect as they illustrate the 'true station of womankind'.<ref>{{cite web|title=The former Ark of the Covenant|url=http://list.english-heritage.org.uk/resultsingle.aspx?uid=1235310|work=National Heritage List for England|publisher=English Heritage|accessdate=23 January 2014}}</ref> The church was abandoned after 1956 and now is used by the [[Georgian Orthodox Church]].

==John Hugh Smyth-Pigott==
After Prince died in 1899 he was replaced by the Reverend John Hugh Smyth-Pigott. Around 1890, Smyth-Pigott again started leading meetings of the community and recruited 50 young female followers to supplement the ageing population of Agapemonites. He took Ruth Anne Preece as his second wife and she had three children, named Glory, Power and Hallelujah.{{sfn|Evans|2006|p=29}} By 1902 his fame had spread as far as India, from where [[Mirza Ghulam Ahmad#Encounter with the Agapemonites|Mirza Ghulam Ahmad]] warned him of his false teachings and predicted his miserable end.{{sfn|Basit|2012}}

The house which may have belonged to Smyth-Pigott in [[St John's Wood]] was visited by [[John Betjeman]] in his film ''[[Metro-Land (TV film)|Metro-land]]''. It is built in the [[Gothic Revival architecture|neo-gothic]] style. It is currently the home of the television presenter [[Vanessa Feltz]] and was previously the home of [[Charles Saatchi]].<ref>{{cite web|title=Vanessa Feltz's House History|url=http://www.bbc.co.uk/london/content/articles/2007/04/25/vanessa_home_history_feature.shtml|work=Where do you think you live|publisher=BBC|accessdate=17 June 2012}}</ref>

Smyth-Pigott died in 1927 and the sect gradually declined until the last member, Ruth, died in 1956.{{sfn|Evans|2006|p=31}} Her funeral in 1956 was the only time when outsiders were admitted to the chapel.<ref>{{cite book|last=Byford|first=Enid|title=Somerset Curiosities|date=1987|publisher=Dovecote Press|isbn=0946159483|page=22}}</ref>

==Books about the sect==
''The Abode of Love'' by [[Aubrey Menen]] – "an appallingly inaccurate popular account" according to one review {{sfn|Price|1977|p=65}} – is a novelisation of the history of the Agapemonites under Prince's leadership.{{sfn|Menen|1990}}

In 2006 Smyth-Pigott's granddaughter, Kate Barlow, published an account of life as a child with her family in the sect. The book includes family photographs and details of conversations she had as a child with the then elderly sect members.{{sfn|Barlow|2011}}

==References==
{{reflist |colwidth=30em}}

==Bibliography==
*{{cite book|last=Armytage|first=W.H.G.|title=Heavens Below: Utopian Experiments in England, 1560–1960|year=2013|publisher=Routledge|isbn=9781134529438|url=https://books.google.com/books?id=OdhXAQAAQBAJ&pg=PA274&lpg=PA274&dq=Belfield+Terrace+Weymouth#v=onepage&q=Belfield%20Terrace%20Weymouth&f=false|ref=harv}}
* {{cite book|first=Kate|last=Barlow|title=The Abode of Love|publisher=Mainstream Publishing|isbn= 978-1845962135|year=2011|ref=harv}}
*{{cite web|last=Basit|first=Asif M.|title=Rev. John Hugh Smyth Pigott, His Claim, Prophecy and End |url=http://www.reviewofreligions.org/5593/rev-john-hugh-smyth-pigott-his-claim-prophecy-and-end/|publisher=The Review of Religions|year=2012|accessdate=25 January 2014|ref=harv}}
*{{cite book|last=Dixon|first=William Hepworth|authorlink=William Hepworth Dixon|title=Spiritual Wives|year=1868|publisher=Hurst & Blackett|url=https://books.google.com/books?id=FbpVTbxBd18C&q=Prince#v=snippet&q=Prince&f=false|ref=harv}}
*{{cite web |url=http://www.british-history.ac.uk/report.aspx?compid=18588 |title=Spaxton |first1=R W |last1=Dunning|first2= C R |last2=Elrington |first3=A P |last3=Baggs|first4= M C |last4=Siraut |publisher=Institute of Historical Research |year=1992 |work=A History of the County of Somerset: Volume 6: Andersfield, Cannington, and North Petherton Hundreds (Bridgwater and neighbouring parishes) |accessdate=25 January 2014|ref=harv }}
* {{cite book|last=Evans|first=Roger|title=Somerset Tales of Mystery and Murder|year=2004|publisher=Countryside Books|isbn=1853068632|ref=harv}}
* {{cite book|last=Evans|first=Roger|title=Blame it on the vicar|year=2006|publisher=Halsgrove|isbn=9781841145686|ref=harv}}
* {{cite book|last=Gray|first=Alasdair|title=Old Men in Love: John Tunnock's Posthumous Papers|year=2009|publisher=Bloomsbury|isbn=9780747593836|url=https://books.google.com/books?id=qiXrWGc0Gq4C&pg=PA209&lpg=PA209&dq=Charlinch+Free+Church#v=onepage&q=Charlinch%20Free%20Church&f=false|ref=harv}}
*{{cite book|last=Grumley-Grennan|first=Tony|title=Tales of English Eccentrics|year=2010|publisher=Lulu.com|isbn=9780953892242|url=https://books.google.com/books?id=TXs3AgAAQBAJ&pg=PA139&lpg=PA139&dq=Charlinch+Free+Church#v=onepage&q=Charlinch%20Free%20Church&f=false|ref=harv}}
* {{cite book|first=Aubrey|last=Menen|title=The Abode of Love|publisher=Penguin Books|year=1990|isbn=978-0140123463|ref=harv}}
*{{cite book|last=Mander|first=Charles|title=Reverend Prince and His Abode of Love|year=1976|publisher=E.P.|isbn=978-0715811986|ref=harv}}
*{{cite book|last=Mitchell|first=Sally|title=Frances Power Cobbe: Victorian Feminist, Journalist, Reformer|year=2004|publisher=University of Virginia Press|isbn=978-0813922713|ref=harv}}
*{{cite book|last=Parry|first=Edward Abbott|title=The drama of the law|year=2010|publisher=Gale|isbn=978-1240076499|ref=harv}}
*{{cite book| first=D. T. W. |last=Price|title=A History of Saint David's University College, Lampeter|publisher=University of Wales Press|year=1977|isbn=978-0708306062|ref=harv}}
*{{cite book|first=Sarah|last=Wise|title=Inconvenient People|publisher=The Bodley Head|isbn= 978-1847921123|year=2012|ref=harv}}
*{{cite web|last=Qayoom|first=Rehan|title="God breathed & they were scattered" - New Light on Hazrat Mirza Ghulam Ahmad & the Agapemonites |url=http://www.scribd.com/fullscreen/86632240/God-breathed-and-they-were-scattered-New-Light-on-Hadhrat-Mirza-Ghulam-Ahmad-the-Agapemonites?secret_password=1w7da1a3kw7nq7ew4d9a|publisher=|year=2012|accessdate=17 February 2014|ref=harv}}
*{{cite journal|last=Sands|first=Kalika|title=The Possibility of Sanity|journal=Oxonian Review|date=16 December 2012|issue=20.6|url=http://www.oxonianreview.org/wp/the-possibility-of-sanity/|ref=harv}}
*{{cite book|last=Scull|first=Andrew|title=Social Order/Mental Disorder: Anglo-American Psychiatry in Historical Perspective|year=1992|publisher=University of California Press|isbn=978-0520078895|ref=harv}}
* {{cite journal|last=Stunt|first=Timothy C.F.|title=The Early Development of Arthur Augustus Rees and his relations with the Brethren|journal=Brethren Archivists and Historians Network Review|year=2006|volume=4|pages=22–35|url=http://brethrenhistory.org/qwicsitePro/php/docsview.php?docid=422| ref=harv}}
* {{cite journal|last=Schwieso|first=Joshua John|title=Religious Fanaticism' and Wrongful Confinement in Victorian England: The Affair of Louisa Nottidge|volume=9|issue=2|journal=Social History of Medicin|year=1996|pages=159–174|url=http://shm.oxfordjournals.org/content/9/2/159.full.pdf?hwshib2=authn%3A1390765925%3A20140125%253A96caafe1-b512-41de-b015-16f83de827b2%3A0%3A0%3A0%3AYLl2UKNzYI6Ik6cOzqqqEA%3D%3D| ref=harv|doi=10.1093/shm/9.2.159}}
*{{cite journal|last=Stillwell|first=Arthur|title=Dr Stillwell's History of the Case of Miss Nottidge|journal=Lancet|year=1849|volume=2|issue=1351|pages=80–81|url=http://www.sciencedirect.com/science/article/pii/S0140673602713755| ref=harv|doi=10.1016/S0140-6736(02)71375-5}}
* {{cite book |title=Portrait of the Quantocks |last=Waite |first=Vincent |authorlink= |coauthors= |year=1964 |publisher=Robert Hale |isbn=978-0709111580| ref=harv }}
* {{cite journal|last=Wilson|first=Owain W.|title=Prince and the Lampeter Brethren|journal=Trivium|year=1970|volume=V|pages=10–20|ref=harv}}

==External links==
* [http://www.abc.net.au/rn/spiritofthings/stories/2009/2493474.htm ''Abode of Love''] – audio and transcript of an interview with Kate Barlow, Smyth-Piggot's granddaughter, author of a memoir of a childhood spent at the cult's headquarters&nbsp;— broadcast on ABC [[Radio National]] in February 2009
*[http://www.apologeticsindex.org/453-agapemone ApologeticsIndex on Agapemone]

[[Category:Religious organizations established in 1846]]
[[Category:Former Christian denominations]]
[[Category:1956 disestablishments in England]]
[[Category:Religion in Somerset]]
[[Category:History of Somerset]]
[[Category:1846 establishments in England]]