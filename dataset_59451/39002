{{Good article}}
{{Infobox event
| title                    = Burnside Burn
| image                    = File:Burnside Bridge (south side) with motorboats.jpg
| image_size               = 
| image_alt                =
| caption                  = [[Portland, Oregon]]'s [[Burnside Bridge]] (pictured in 2012), the site of the "Burnside Burn" event organized by the local chapter of the [[National Organization for the Reform of Marijuana Laws]]
| time                     = Midnight
| duration                 = 
| date                     = {{start date|2015|07|01}}
| venue                    = [[Burnside Bridge]]
| location                 = [[Portland, Oregon]], United States
| coordinates              = <!-- {{coord|LAT|LON|region:XXXX_type:event|display=inline,title}} -->
| type                     = Rally
| theme                    = [[Cannabis (drug)|Cannabis]]
| cause                    = 
| budget                   = 
| patron                   =  <!-- or |patrons= -->
| organisers               =  Portland NORML
| participants             = Thousands
| outcome                  = 
| url                      = 
| website                  = <!-- {{URL|example.com}} -->
| notes                    = 
}}
The "'''Burnside Burn'''" was an event held on the [[Burnside Bridge]] in [[Portland, Oregon]], starting at midnight on July 1, 2015, the day recreational marijuana became legal in the [[U.S. state]] of [[Oregon]]. It was organized by Portland NORML, the local chapter of the [[National Organization for the Reform of Marijuana Laws]], having originated from its executive director, who wanted to photograph himself in front of the [[White Stag sign]] in the moments after [[Oregon Ballot Measure 91 (2014)|Oregon Ballot Measure 91]] took effect. The crowd, larger than anticipated, numbered in the thousands and at times blocked traffic lanes on the bridge. Some attendees wanted to commemorate the moment, while others were motivated by announcements of free marijuana and seeds. No fines were issued for consumption in public. The event was covered by cannabis publications, local and national news outlets, and the [[HBO]] television series ''[[Vice (TV series)|Vice]]''.

==Description==
[[File:Portland Oregon - White Stag sign.jpg|thumb|left|The event began with the idea of taking a photograph in front of the [[White Stag sign]] (pictured in 2010).]]

The "Burnside Burn" was organized by Portland NORML, the local chapter of the [[National Organization for the Reform of Marijuana Laws]], in celebration of the [[Oregon Ballot Measure 91 (2014)|legalization of recreational marijuana use]] in Oregon and to circumnavigate a temporary limit on recreational sales.<ref name=Roth>{{cite news|last=Roth|first=Sara|title= Event Guide: Celebrating legal cannabis in Oregon|url=http://www.kgw.com/story/news/local/marijuana/2015/06/30/portland-events-celebrate-legal-cannabis-in-oregon/29520237/|accessdate=July 1, 2015|publisher=[[KGW]]|date=June 30, 2015|location=Portland, Oregon}}</ref><ref name=OPB>{{cite news|last=Perry|first=Alyssa Jeong|title=Oregonians Celebrate Legalization With Midnight Pot Party|url=http://www.opb.org/news/article/oregonians-celebrate-legalization-with-midnight-pot-party/|accessdate=July 1, 2015|publisher=[[Oregon Public Broadcasting]]|date=July 1, 2015}}</ref><ref name=Campbell>{{cite news|last1=Campbell|first1=Andy|title=Weed Isn't Just Legal In Oregon – It's Free!|url=http://www.huffingtonpost.com/2015/07/01/weed-legal-in-oregon_n_7706926.html|accessdate=July 1, 2015|work=[[The Huffington Post]]|date=July 1, 2015}}</ref> According to Russ Belville, the chapter's executive director, the event began with the idea of taking a photograph in front of the [[White Stag sign]].<ref>{{cite news|last1=Labrecque|first1=Jackie|title=Pot sharing, midnight giveaway celebration on Burnside Bridge|url=http://www.katu.com/news/local/Pot-sharing-giveaway-celebration-at-midnight-on-Burnside-Bridge-311116861.html|accessdate=July 2, 2015|publisher=[[KATU]]|date=July 1, 2015|location=Portland, Oregon}}</ref> He recalled: "It evolved from me saying that when legalization happens, I want to take a photo under the Portland, Oregon sign. Other people said, 'Can we give marijuana away?' [and] I said, 'I can't stop you!'"<ref name=Roth/><ref>{{cite web|last1=Belville|first1=Russ|title=Schedule of Events Leading to Legalization Day in Oregon|url=http://portlandnorml.org/schedule-of-events-leading-to-legalization-day-in-oregon/|publisher=Portland NORML|accessdate=July 2, 2015|date=June 26, 2015}}</ref>

On the evening of June 30, beginning as early as 8pm,<ref name=Vice>{{cite news|last=Hamilton|first=Keegan|title=Here's What Happened Last Night at Portland's Massive Weed Legalization 'Giveaway'|url=https://news.vice.com/article/heres-what-happened-last-night-at-portlands-massive-weed-legalization-giveaway|accessdate=July 1, 2015|work=[[Vice News]]|date=July 1, 2015|location=New York City, New York}}</ref> and into the morning of July 1, 2015, between a few hundred and a few thousand people gathered on the [[Burnside Bridge]]'s north sidewalk for the free event. The crowd sometimes spilled into the road and blocked multiple traffic lanes, resulting at one point in the complete blockage of west-bound traffic.<ref name=Roth/><ref name=Acker>{{cite news|last=Acker|first=Lizzy|title=Smokey Lens: Images from Last Night's NORML Legal Weed Party|url=http://www.wweek.com/portland/blog-33411-smokey_lens_images_from_last_nights_norml_legal_weed_party.html|accessdate=July 1, 2015|work=[[Willamette Week]]|date=July 1, 2015|publisher=City of Roses Newspapers}}</ref> Activists chanted "Free the weed" and "Fuck the [[Drug Enforcement Administration|DEA]]". Cannabis was shared and consumed openly.<ref name=OPB/><ref name=Vice/><ref name=Zarkhin/>

[[File:NORML videographer - Seattle Hempfest 2010 - 01.jpg|thumb|right|Russ Belville (left), executive director of Portland NORML, at [[Seattle Hempfest]] in 2010; Belville shared an ounce of marijuana at the event.]]

The crowd was larger than anticipated and spanned the entire length of the {{Convert|1400|ft|m|adj=on}} Burnside Bridge.<ref name=OPB/><ref name=HighTimes>{{cite journal|last=Fought|first=Tim|title=Thousands Gather in Portland as Oregon Eases into Legal Weed|journal=[[High Times]]|date=July 2, 2015|url=http://hightimes.com/read/thousands-gather-portland-oregon-eases-legal-weed|accessdate=July 2, 2015}}</ref> Belville had initially expected "between 50 and 5,000 people", but details of the event spread online and through word of mouth, and ultimately it was estimated that "thousands" had turned out.<ref name=OPB/> Some attendees said they were there to commemorate an historic moment, while others admitted having come for free marijuana and cannabis seeds. One man, known as "Pork Chop" (or "Porkchop"), reportedly announced over a megaphone that he had [[420 (cannabis culture)|420]] pounds of marijuana to distribute, though his claim was not supported by news outlets.<ref name=Vice/><ref name=Zarkhin/> Two women with Ideal Farms, who wished to "share the love", distributed [[joint (cannabis)|joints]] to attendees who could prove that they were of legal age.<ref name=OPB/> One man distributed drops of [[hash oil]],<ref name=HighTimes/> and Belville himself shared an ounce of marijuana (the maximum allowed under [[Oregon Ballot Measure 91 (2014)|Oregon Ballot Measure 91]]). Some participants did receive free marijuana, seeds, and/or starter plants, but many did not, due to the larger than expected crowd.<ref name=Vice/> Coupons were also distributed for later redemption.<ref name=HighTimes/><ref>{{cite web|last=Herman|first=Steve|title=Burnside Burn|url=http://kxl.com/2015/07/01/burnside-burn/|publisher=[[KXL-FM]]|accessdate=July 1, 2015|date=July 1, 2015|location=Portland, Oregon}}</ref>

Participants smoked openly and without fear. No fines were issued for consumption in public.<ref name=Vice/> Patrol vehicles drove by the scene a few times but did not stop. Prior to the event, police urged residents to avoid calling [[9-1-1]] to report public consumption, which they did not consider an emergency.<ref name=Vice/><ref name=HighTimes/>

==Commentary==
The event was covered by cannabis publications,<ref name=HighTimes/><ref>{{cite journal|title=Crowds Count Down to Marijuana Legalization in Oregon, Then Light Up|journal=[[Cannabis Culture (magazine)|Cannabis Culture]]|date=July 1, 2015|url=http://www.cannabisculture.com/content/2015/07/01/Crowds-Count-Down-Marijuana-Legalization-Oregon-Then-Light|accessdate=July 2, 2015}} Note: Original article by Shelby Sebens for [[Yahoo! News]].</ref> local and national media outlets,<ref name=Campbell/><ref>{{cite journal|last=Tuttle|first=Brad|title=Oregon Is Celebrating Marijuana Legalization With Free Weed|journal=[[Time (magazine)|Time]]|date=June 29, 2015|url=http://time.com/money/3938535/free-marijuana-oregon-portland/|accessdate=July 2, 2015|issn=0040-781X|oclc=1311479}}</ref> and the [[HBO]] documentary television series ''[[Vice (TV series)|Vice]]''.<ref name=Roth/><ref name=Vice/> ''[[The Oregonian]]'' described the event as "loud and energetic", attracting a diverse and "eclectic" crowd of activists, marijuana enthusiasts, and first-time consumers, some from as far away as Canada and San Diego.<ref name=Vice/><ref name=Zarkhin>{{cite news|last=Zarkhin|first=Fedor|title=Hundreds celebrate marijuana's new legal status in Oregon|url=http://www.oregonlive.com/marijuana/index.ssf/2015/07/recreational_pot_becomes_legal.html|accessdate=July 1, 2015|work=[[The Oregonian]]|date=July 1, 2015|publisher=[[Advance Publications]]|location=Portland, Oregon|issn=8750-1317}}</ref> According to ''[[Willamette Week]]'', attendees ranged from octogenarians to "tweens with rainbow hair" and the crowd was "generally happy". The newspaper summarized, "All and all, the mood was celebratory as befit such an historic occasion."<ref name=Acker/>

NORML's Kaliko Castille told ''[[The Huffington Post]]'' he was "happy to see a community able to come together—peacefully—over something positive. It's great to see people from all walks of life out here, handing out joints to each other and getting to know their neighbors."<ref name=Campbell/> ''The Huffington Post''{{'s}} Andy Campbell called the event a "smoke-out with a message" and opined, "Legalization is so much more than being able to smoke a joint in your home without being a criminal. It's a health care issue; it's a tax revenue issue; it saves states millions in the court system; and it ends the hidden costs of prosecution, which emerge when someone can't get a job because there's a possession charge on their record."<ref name=Campbell/> ''[[The Washington Post]]'' called the "Burnside Burn" an opportunity for marijuana enthusiasts to "celebrate their new freedom together".<ref>{{cite news|last=Greenberg|first=Will|title=Oregon celebrates with free weed as recreational marijuana becomes legal|url=http://www.washingtonpost.com/news/morning-mix/wp/2015/07/01/oregon-celebrates-with-free-weed-as-recreational-marijuana-becomes-legal/|accessdate=July 2, 2015|work=[[The Washington Post]]|date=July 1, 2015|issn=0190-8286}}</ref>

==See also==
{{Portal|Cannabis|Oregon}}
* [[Cannabis in Oregon]]
* [[Hands Across Hawthorne]], a 2011 rally held at Portland's [[Hawthorne Bridge]]
* [[Portland Hempstalk Festival]], an annual cannabis event in Portland
* [[Weed the People]], a cannabis event held in Portland two days later
{{Clear}}

==References==
{{reflist|30em}}

==External links==
{{External media
| image1 = [http://www.reuters.com/news/picture/oregon-goes-to-pot?articleId=USRTX1ILXF Oregon goes to pot] by Steve Dipaola (July 1, 2015), Reuters
| image2 = [http://www.stltoday.com/thousands-gather-in-portland-as-oregon-eases-into-legal-weed/image_70559b23-cc73-5257-8703-b177c13b82ca.html Thousands gather in Portland as Oregon eases into legal weed]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }} by Beth Nakamura (July 1, 2015), ''[[St. Louis Post-Dispatch]]''
| video1 = [http://www.kptv.com/story/29454097/marijuana-supporters-celebrate-rec-use-legalization-with-midnight-burnside-burn Marijuana supporters celebrate rec use legalization with midnight Burnside Burn] (July 1, 2015), [[KPTV]]
}}
* [http://blog.norml.org/2015/06/30/oregon-midnight-tonight-law-takes-effect-permitting-adults-to-consume-cannabis/ Oregon: Midnight Tonight — Law Takes Effect Permitting Adults to Consume Cannabis] by [[Paul Armentano]] (June 30, 2015), [[National Organization for the Reform of Marijuana Laws ]] (NORML)
* [http://www.bustle.com/articles/94244-smoking-weed-in-oregon-just-became-totally-legal-yeah-people-could-not-be-more-excited Smoking Weed In Oregon Just Became Totally Legal and Yeah, People Could Not Be More Excited About That] by Alicia Lu (July 1, 2014), ''[[Bustle (magazine)|Bustle]]''
* [http://portlandnorml.org/burnside-burn-celebration-of-marijuana-legalization-in-oregon-a-huge-success/ "Burnside Burn" Celebration of Marijuana Legalization in Oregon a Huge Success] by Russ Belville (July 1, 2015), Portland NORML
* [http://hightimes.com/read/radical-rant-thousands-celebrate-oregon-marijuana-freedom-burnsideburn Radical Rant: Thousands Celebrate Oregon Marijuana Freedom with a #BurnsideBurn] by Russ Belville (July 2, 2015), ''[[High Times]]''
* [http://hightimes.com/read/radical-rant-burnsideburn-part-two-how-prevent-weed-riot Radical Rant: #BurnsideBurn Part Two, How to Prevent a Weed Riot] by Russ Belville (July 7, 2015), ''High Times''

[[category:2015 in cannabis]]
[[Category:2015 in Portland, Oregon]]
[[Category:Cannabis events in the United States]]
[[Category:Cannabis in Oregon]]
[[Category:Events in Portland, Oregon]]
[[Category:June 2015 events]]
[[Category:July 2015 events]]