{{Infobox Organization
| name         = IEEE Microwave Theory and Techniques Society 
| image        = IEEE_MTT-S_logo.GIF
| image_border = 
| size         = 150px
| caption      = The IEEE MTT-S Logo
| formation    = 1952
| type         = 
| headquarters = [[New York City]] 
| location     = [[United States]]
| membership   =  
| language     = [[English language|English]]
| leader_title = 2015 President
| leader_name  = [[Ke Wu]]
| key_people   = 
| num_staff    = 
| budget       = 
| website      = http://www.mtt.org
}}
The '''[[IEEE]]''' '''Microwave Theory and Techniques Society''' (MTT-S) is the largest technical profession society for the promotion of the theory and applications of [[Radio frequency|RF]], [[microwave]], [[millimeter-wave]], and [[terahertz technology|terahertz technologies]].  The MTT-S has over 11,000 worldwide professional members in academia, industry and government laboratories.  It was founded in 1952<ref>[http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=989946&isnumber=21335 T. S. Saad, J. C. Wiltse, "50 Years of the IEEE Microwave Theory and Techniques Society", IEEE Trans. on Microwave Theory and Techniques, vol. 50-3, pp 612-624, March 2002], {{doi|10.1109/22.989946}}</ref> and has over 170 local chapters.  MTT-S is one of 45 technical societies and councils of the IEEE.<ref>[http://www.ieee.org/web/societies/home/index.html IEEE Societies, Councils and Technical Communities]</ref>

==Field of Interest==
The Microwave Theory and Techniques Society focuses on the theory and applications of radio-frequency (HF, VHF/UHF, microwave, millimeter-wave and terahertz), guided-wave and wireless technologies, as they relate to nanostructures, devices, integrated circuits, multi-circuit assemblies, components, packages, transmission lines, sub-systems, and systems involving the generation, amplification, processing, modulation, control, transmission, reception, detection and demodulation, and effects of electromagnetic energy transport. It also includes the interaction & interface of microwave signals with digital & optical circuitry & interconnecting transmission media. Examples include optical waves in suitably confined structures, as well as the applications of acoustic, magnetic, & plasmonic waves to microwave systems.
Radio frequency (RF) is a term that refers to signals and associated currents having characteristics such that, if the current is input to an antenna, an electromagnetic (EM) field is generated suitable for wireless broadcasting and/or communications, radar, etc. These frequencies cover a significant portion of the electromagnetic radiation spectrum, extending from about 9&nbsp;kHz, the lowest allocated wireless communications frequency (if it were audible it would be within the range of human hearing), to thousands of gigahertz (GHz). The discipline of microwave theory & techniques applies physical and mathematical principles to analyze devices, components and structures that interact with electromagnetic fields and often have dimensions representing a significant fraction of a wavelength, or when in-circuit wave propagation effects need to be considered.
The Society's focus shall include scientific, technical, and industrial activities, subject to timely modifications approved by the IEEE Technical Advisory Board. Technical Committee focus areas of interest include microwave and millimeter-wave materials, solid state devices and integrated circuits, filters, passive components and packaging, microwave acoustics and photonics, high power and low noise techniques, frequency conversion, field theory, and computer aided design and measurements. In addition, the Society is involved in terahertz technology, ultra-wide band and microwave systems, and multidisciplinary activities such as RF microelectromechanical (RFMEMS) devices, radio frequency identification devices (RFIDs), digital signal processing, biological effects and medical applications, and business issues.

==Publications==
*   [[IEEE Transactions on Microwave Theory and Techniques]]<ref>[http://ieeexplore.ieee.org/servlet/opac?punumber=22 T-MTT home page]</ref> (T-MTT), published monthly (12/year), with an Impact Factor of 2.243  {{ISSN|0018-9480}}
*   [[IEEE Microwave and Wireless Components Letters]] <ref>[http://ieeexplore.ieee.org/servlet/opac?punumber=7260 L-MWC home page]</ref> (L-MWC), published monthly (12/year) with an Impact Factor of 1.703 {{ISSN|1531-1309}}
*   [[IEEE Microwave Magazine]] <ref>[http://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=6668 M-MW home page]</ref> (M-MW), monthly (12/year) with an Impact Factor of 1.131  {{ISSN|1527-3342}}
*   [[IEEE Transactions on Terahertz Science and Technology]],<ref>[http://ieeexplore.ieee.org/servlet/opac?punumber=5503871 T-THZ home page]</ref> (T-THZ), published Bi-monthly (6/year) with an Impact Factor of 2.177 {{ISSN|2156-342X}}

==CoSponsored Publications==
*   [[Journal of Lightwave Technology]]<ref>[http://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=77 J-LT home page]</ref> (J-LT), published bimonthly (6/year)  {{ISSN|1051-8223}}

==Major Microwave Conferences sponsored by MTT-S==
*   [[IEEE MTT-S International Microwave Symposium]] (IMS)
*   [http://www.eumweek.com/ European Microwave Week] (EuMW)
*   [http://www.apmc2016.org Asia-Pacific Microwave Conference] (APMC)
*   [http://www.imoc2016.org SBMO/IEEE MTT-S International Microwave and Optoelectronics Conference] (IMOC)
*   [http://www.radiowirelessweek.org/ IEEE Radio and Wireless Week] (RWW)
*   [http://imarc-ieee.org/ IEEE International Microwave and RF Conference]  (IMaRC)
[http://www.mtt.org/conference-calendar.html Additional MTT-S sponsored conferences]

==Technical Committees==
MTT-S has 26 technical committees supporting communities of specialization within MTT-S.  These technical committees work with the publications and conferences to organizing and support activities for their communities.<ref>[http://www.mtt.org/committees/index.html IEEE MTT-S Technical Committees]</ref>

==References==
{{reflist|2}}

==External links==
* [http://www.ieee.org/ IEEE] 
* [http://www.mtt.org/ IEEE MTT Society]
* [http://www.hem-usa.org/ Historical Electronics Museum], home of the MTT-S Historical Collection
* [http://ieeexplore.ieee.org/ IEEE Xplore] — over a million online documents
* [http://www.tryengineering.org/ www.tryengineering.org] - resource for students age 8-18, and their parents, teachers, and guidance counselors. 
* [http://wikis.ua.pt/MTT-11/index.php/Main_Page MTT-11 Microwave Measurements Wiki] - A source of information for Microwave measurements .

{{Institute of Electrical and Electronics Engineers}}

[[Category:IEEE societies]]