{{ WAP assignment | course = Wikipedia:USEP/Courses/Introduction to Mass Communications (Chad Tew) | university = University of Southern Indiana | term = 2012 Q3 | project = WikiProject Journalism }} 
{{Infobox person
| name                      = José Antonio García
| native_name               = José Antonio García Apac
| native_name_lang          = Spanish
| image                     = Garcia-Apac-journalist.jpg
| image_size                = 130px
| alt                       = 
| caption                   = 
| birth_name                = 
| birth_date                = <!-- {{Birth date and age|YYYY|MM|DD}} -->
| birth_place               = 
| disappeared_date          = 20 November 2006
| disappeared_place         = Disappeared between Tepalcatepec and Buenavista Tomatlan, Michoacán, Mexico
| disappeared_status        = Missing
| death_date                = 
| death_place               = 
| death_cause               = 
| body_discovered           = 
| resting_place             = 
| resting_place_coordinates = 
| monuments                 = 
| residence                 = Morelia, Michoacan, Mexico
| nationality               = Mexican
| other_names               = "El Chino"
| ethnicity                 = <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               = 
| education                 = 
| alma_mater                = 
| occupation                = Journalist, editor and newspaper founder
| years_active              = 
| employer                  = Ecos de la Cuenca
| organization              = 
| agent                     = 
| known_for                 = Reporting on drug cartels in the Mexican state of Michoacan
| notable_works             = 
| style                     = 
| influences                = 
| influenced                = 
| home_town                 = Morelia, Michoacan, Mexico
| salary                    = 
| net_worth                 = <!-- Net worth should be supported with a citation from a reliable source -->
| height                    = <!-- {{height|m=}} -->
| weight                    = <!-- {{convert|weight in kg|kg|lb}} -->
| television                = 
| title                     = 
| term                      = 
| predecessor               = 
| successor                 = 
| party                     = 
| movement                  = 
| opponents                 = 
| boards                    = 
| religion                  = <!-- Religion should be supported with a citation from a reliable source -->
| denomination              = <!-- Denomination should be supported with a citation from a reliable source -->
| criminal_charge           = <!-- Criminality parameters should be supported with citations from reliable sources -->
| criminal_penalty          = 
| criminal_status           = 
| spouse                    = Rosa Isela Caballero
| partner                   = 
| children                  = 6 children
| parents                   = 
| relatives                 = 
| callsign                  = 
| awards                    = 
| signature                 = 
| signature_alt             = 
| signature_size            = 
| module                    = 
| module2                   = 
| module3                   = 
| module4                   = 
| module5                   = 
| module6                   = 
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 = 
| box_width                 = 
}}
'''José Antonio García Apac''', also known as "El Chino", was a Mexican journalist and editor for the ''[[Ecos de la Cuenca]]'' in [[Tepalcatepec]], [[Michoacán]], [[Mexico]], when he disappeared 20 November 2006.<ref name=bbcwife>{{cite web|url=http://www.bbc.co.uk/news/world-latin-america-18170240 |title=Wife traumatised by Mexican journalist's disappearance |publisher=BBC News |type=print & video|date=2012-05-25 |accessdate=2012-09-24}}</ref><ref name=cancaonova>{{cite news|publisher=Cancao nova noticias|title=Jornalista mexicano é dado por desaparecido|url=http://noticias.cancaonova.com/noticia.php?id=12842|date=2006-12-06|accessdate=2012-10-15}}</ref><ref name=ifex>{{cite web|title=Death threats against "El Mundo" photographer and journalist over drug trafficking coverage; more details on disappearance of Michoacán journalist|url=http://www.ifex.org/mexico/2006/12/07/death_threats_against_el_mundo/|date=2006-12-07|publisher=IFEX.org|accessdate=2012-10-15}}</ref> He is best known for the news stories he published on the violent relationship between the [[drug cartels]] in his home state and its authorities.<ref name=cancaonova />

According to the [[Committee to Protect Journalists]], García was one of two journalists to go missing in Mexico in 2006, a year in which 8 journalists were killed.<ref name=cpjlistofdisappeared>{{cite web|author=Journalists Missing |url=http://cpj.org/reports/2008/02/journalists-missing.php |title=Journalists Missing - Reports - Committee to Protect Journalists |publisher=Cpj.org |date= |accessdate=2012-12-14}}</ref> He was one of six missing journalists between 2005 and 2006.<ref name=cpjdisappeared>{{cite web|title=The Disappeared in Mexico|publisher=Committee to Protect Journalists|url=http://cpj.org/reports/2008/09/mexico-08.php|date=2008-09-30|accessdate=2012-10-15}}</ref>

== Personal ==
José Antonio García was married to Rosa Isela Caballero, with whom he had six children. Caballero is convinced her husband's disappearance was a direct result of his work at ''Ecos de la Cuenca''.<ref name=bbcwife />

García’s family lived in [[Morelia, Michoacán]], which is the capital of the state of Michoacán. He worked about 256 kilometers, or a three-hour drive, away from his home in Tepalcatepec. García was headed to Morelia to see his family on the night of his disappearance.<ref name=cancaonova /><ref name=ifex /> García's wife says he did not receive threats but the family had noticed being followed.<ref name=ifex /><ref name=michoacan>{{cite web|url=http://www.michoacan.contralinea.com.mx/archivo/2008/enero/htm/periodistas-michoacan.htm |title=Periodistas de Michoacán en la mira &#124; Periodistas de Michoacán en la mira &#124; Revista Contralínea MICHOACAN, periodismo de investigacion. Mexico &#124; Enero 2008 |publisher=Michoacan.contralinea.com.mx |date= |accessdate=2012-09-24}}</ref>

In 2012 his son Aldo García Caballero released a film, ''El Último Día'', which was about drugs in Michoacán. His film made the final selection for the Michoacán section of El Festival de Cine y Video Ingenia in 2012.<ref name=celuloidedigital>{{cite web|last=KET|title=El Ultimo Dia|url=http://issuu.com/celuloidedigital/docs/celuloide_digital_-_octubre_2012_frankenweenie|publisher=Celuloide Digital|pages=75–77|accessdate=29 October 2012}}</ref>

== Career ==
García was the founder and editor of ''Ecos de la Cuenca'' in Tepalcatepec. His weekly newspaper ran stories about the drug trafficking and the police's involvement in the drug activity six months before his disappearance.<ref name=cancaonova /> He had compiled a list of government officials that he believed were involved in organized crime. His wife Isabella says he took the list to Mexico City May 2006 to get confirmation of his discoveries from the federal organized crime unit. Six months later García was missing. When the family lawyer, Sylvia Martinez, later demanded access to his list, the government claimed it had no record of García's visit.

After her husband's disappearance, Rosa Isela Caballero took over the Ecos de la Cuenca. She says she publishes an issue as often as possible, but keeps controversial topics to a minimum. The ''Ecos de la Cuenca'''s stories center around local government now and keeps organized crime out of the print.<ref name=bbcwife /><ref name=rsfwife>{{cite web|url=http://en.rsf.org/mexico-family-complain-of-lack-of-20-11-2007,24459 |title=Family complain of lack of progress one year after editor went missing - Reporters Without Borders |publisher=En.rsf.org |date= |accessdate=2012-09-24}}</ref>

== Disappearance ==
{{Location map+|Mexico|width=300|float=left
 |AlternativeMap = Mexico States blank map.svg
 | position = left
 |caption= Mentioned locations within Mexico relative to the capital Mexico City.
 |alt = THE ADDED CITY is located in Mexico.
 |places=
  {{Location map~ | Mexico 
  |lat_deg=19 |lat_min=25 |lat_sec=57 |lat_dir=N
  |lon_deg=99 |lon_min=07 |lon_sec=59 |lon_dir=W 
  |position=right
  |background=#FFFFFF 
  |label=Mexico City
  }}
  {{Location map~ | Mexico 
  |lat_deg=19 |lat_min=10 |lat_sec=59 |lat_dir=N
  |lon_deg=102|lon_min=50 |lon_sec=60 |lon_dir=W 
  |position=left
  |background=#FFFFFF 
  |label=Tepalcatepec
  }}
{{Location map~ | Mexico 
  |lat_deg=19 |lat_min=12 |lat_sec=36.5 |lat_dir=N
  |lon_deg=102 |lon_min=35 |lon_sec=11.5 |lon_dir=W 
  |position=bottom
  |background=#FFFFFF 
  |label=Buenavista Tomatlan
  }}
{{Location map~ | Mexico 
  |lat_deg=19 |lat_min=42 |lat_sec=2 |lat_dir=N
  |lon_deg=101 |lon_min=11 |lon_sec=11 |lon_dir=W 
  |position=top
  |background=#FFFFFF 
  |label=Morelia
  }}
}}García was somewhere between Tepalcatepec and [[Buenavista Tomatlan]] and on his way to Morelia to see his family when he disappeared on 20 November 2006. On the way, he called his family around 8 p.m. to ask about groceries and was speaking on the phone with his son when his son heard voices on the other end of the line that told García to hang up. Noises sounding like García being dragged away were heard before the line went dead for his son.<ref name=ifex /> His family filed an official report after a week on 28 November.<ref name=michoacan />

By December 5, his wife Rosa Isela Caballero had lost hope for his return. She said, "They would not have waited so long to ask for a reward if it was a kidnapping. If it was a scare, they would have let him go. Or if it's something more serious, they would have killed him."<ref name=cancaonova />

==Investigation ==
Four months passed before the prosecutor's office passed the case on to the Attorney General. Another three months later, nothing was found. Journalists in the area believed his body was thrown into the Tepalcatepec Dam,<ref name=michoacan /> and the investigators told the family to hire their own diver.<ref name=afp>{{cite news|first=Guillermo|last=Barros|title=El aniversario del olvido de los periodistas desaparecidos en México |publisher=AFP |url=https://www.google.com/hostednews/afp/article/ALeqM5ieeYbC9Byv3Tixg2cz_LcMAQYQlA?docId=CNG.39e8c26fec65671ea3acc079a4840076.851|date=2010-11-10|accessdate=2012-10-15}}</ref>

Later, news about the delays reached President Felipe Calderón, who intervened and asked the federal anti-kidnapping unit to investigate. The Federal Public Prosecutor's Office re-opened the investigation in January 2008 and then again in March 2008, but eventually also stopped working on the case due to the lack of information.<ref name=rsfwife /> The investigation was halted in March 2008 because there was not sufficient information to continue.<ref name=bbcwife /><ref name=michoacan />

Five years after García’s disappearance, Aldo García Caballero gave a testimony (Dec. 11, 2011) about his father’s disappearance. He said he was angry with investigators and the government for lack of effort in the case. He went on to make a film about drugs in the state.<ref name=celuloidedigital />

== Context ==
The state of Michoacan has been a "hot spot" in drug trafficking.<ref name=bbcwife /> In the area, drug cartels, such as [[Knights Templar Cartel]] and [[La Familia Michoacana]], influence local politics. The Committee to Protect Journalists say that in the cases of the disappeared, it has noticed a link between journalists investigating ties between the cartels and government.<ref name="cpjdisappeared" /> 

== Impact ==
Impunity for crimes against the press has been one of the issues during the [[Mexican Drug War]], which officially began in 2006. In the cases of the disappeared, such as García's, there are no cases that have been solved and crimes against journalists were not made into federal crimes until 2012.<ref name=sipiapa>{{cite web|url=http://www.sipiapa.org/v4/comunicados_de_prensa.php?idioma=us&seccion=detalles&id=4102 |title=IAPA hails progress in solving murder of journalist in Mexico |publisher=Sip-Iapa |date=2008-12-09 |accessdate=2012-12-14}}</ref>

The  CPJ says that news outlets begin to practice self-censorship of crime news, which is one of the drug cartel's objectives.<ref name="cpjdisappeared" /> The ''Ecos de la Cuenca'' under Garcia's wife's direction ceased investigative stories on organized crime in Mexico.<ref name=rsfwife /> This follows a trend that was supported by 2010 research from La Fundación MEPI, a Mexican investigative journalism organization, that shows self-censorship about crimes and drug cartels.<ref>{{cite web|url=http://www.propublica.org/article/mexicos-regional-newspapers-limit-reporting-of-cartels-role-in-drug-violenc |title=Mexico's Regional Newspapers Limit Reporting of Cartels' Role in Drug Violence |publisher=ProPublica |date=2010-11-17 |accessdate=2012-12-14}}</ref>

== Reactions ==
[[Reporters Without Borders]] condemned Garcia's disappearance and the increase in violence at the time: "Not a week has gone by since the end of October without a journalist disappearing or being murdered in Mexico,” Reporters Without Borders said. “García Apac’s disappearance makes us fear the worst as he was working as a journalist in Michoacán state, where drug traffickers do not hesitate to decapitate rivals or critics."<ref name=rsf>{{cite web|url=http://en.rsf.org/mexico-newspaper-editor-missing-since-20-04-12-2006,19985.html |title=Newspaper editor missing since 20 November in Michoacán state - Reporters Without Borders |publisher=En.rsf.org |date= |accessdate=2012-09-24}}</ref>

On 9 July 2010, 150 journalists marched through Morelia to protest impunity in cases of murders and disappearances in the Michoacán state, including the García case.<ref name=eltiempo>{{cite news|title=Periodistas piden justicia|url=http://www.periodicoeltiempo.mx/index.php?option=com_content&view=article&id=6076:periodistas-piden-justicia-&catid=130:estados&Itemid=730|publisher=El tiempo|date=2010-07-09|accessdate=2012-10-15}}</ref><ref name=ips>{{cite news|title=MÉXICO: La rebelión de los periodistas|url=http://ipsnoticias.net/print.asp?idnews=96100|publisher=Inter Press Service|date=2010-08-06|accessdate=2012-10-15}}</ref>

In 2012, García's wife told the BBC News, "When they kill a loved one, at least you have the body," she says. "But when it's a disappearance, it's like a physical torture for us as family members."<ref name=bbcwife/>

==See also==
*[[Mexican Drug War]]
*[[List of journalists killed in Mexico]]
*[[María Esther Aguilar Cansimbe]]
*[[Evaristo Ortega Zárate]]
*[[Disappearance of Zane Plemmons]]
*[[Disappearance and displacement of Mario Segura]]

== References ==
{{Reflist}}


{{Mexican Drug War}}




{{DEFAULTSORT:Garcia, Jose Antonio}}
[[Category:Articles created via the Article Wizard]]
[[Category:Mexican journalists]]
[[Category:Male journalists]]
[[Category:Journalists killed in the Mexican Drug War]]
[[Category:Disappeared journalists in Mexico]]
[[Category:Writers from Michoacán]]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->