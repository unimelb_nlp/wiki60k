{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Permanent Vacation
| Type        = studio
| Artist      = [[Aerosmith]]
| Cover       = Aerosmith - Permanent Vacation.JPG
| Released    = August 31, 1987
| Recorded    = March – May 1987
| Studio      = [[Little Mountain Sound Studios]]
| Genre       = [[Hard rock]]
| Length      = {{Duration|m=51|s=38}}
| Label       = [[Geffen Records|Geffen]]
| Producer    = [[Bruce Fairbairn]]
| Last album  = ''[[Done with Mirrors]]'' <br /> (1985)
| This album  = '''''Permanent Vacation''''' <br /> (1987)
| Next album  = ''[[Pump (album)|Pump]]'' <br /> (1989)
| Misc        = {{Singles
| Name           = Permanent Vacation
| Type           = studio
| Single 1       = [[Hangman Jury]]
| Single 1 date  = August 1987<ref name="strong">{{cite book |last1=Strong |first1=Martin |title=The Great Rock Discography |edition=Sixth |year=2002 |origyear=First published in 1994 |publisher=[[Canongate Books]] |location=United Kingdom |isbn=1-84195-312-1 |page=}}</ref>
| Single 2       = [[Dude (Looks Like a Lady)]]
| Single 2 date  = October 1987<ref name="strong" />
| Single 3       = [[Angel (Aerosmith song)|Angel]]
| Single 3 date  = April 1988<ref name="strong" />
| Single 4       = [[Rag Doll (Aerosmith song)|Rag Doll]]
| Single 4 date  = June 1988<ref name="strong" />
}}
}}
{{Album reviews
| rev1 = [[Allmusic]]
|rev1Score = {{rating|4|5}} <ref>{{cite web|last=Franck |first=John |url={{Allmusic|class=album|id=r180|pure_url=yes}} |title=Allmusic Review |publisher=Allmusic.com |date= |accessdate=2011-04-21}}</ref>
| rev2 = [[Robert Christgau]]
|rev2Score = C+ <ref>{{cite web|url=http://robertchristgau.com/get_artist.php?name=aerosmith |title=Review |publisher=Robert Christgau Review |date= |accessdate=2011-06-24}}</ref>
| rev3 = ''[[Rolling Stone]]''
|rev3Score = (unfavorable) <ref name="RS">{{cite web|url=http://www.rollingstone.com/music/albumreviews/permanent-vacation-19871022 |title=Permanent Vacation |last=Frost |first=Deborah |publisher=''[[Rolling Stone]]'' |date=1987-10-22 |accessdate=2012-05-14}}</ref>
| rev4 = ''[[The Rolling Stone Album Guide]]''
|rev4Score = {{rating|2|5}} <ref>{{cite web|url=http://www.rollingstone.com/music/artists/aerosmith/albumguide |title=Review |publisher=''Rolling Stone'' |date= |accessdate=2011-04-21}}</ref>
|rev5 = ''[[Metal Forces]]''
|rev5score = (9.9/10)<ref>{{cite journal | title = Aerosmith - Permanent Vacation| journal = [[Metal Forces]] | year = 1987 | first = Dave | last = Reynolds| issue = 25| id = | url = http://www.metalforcesmagazine.com/site/album-review-aerosmith-permanent-vacation/ | accessdate = 2012-07-05}}</ref>

}}

'''''Permanent Vacation''''' is the ninth studio album by American [[rock music|rock]] band [[Aerosmith]], released on August 31 1987 by [[Geffen Records]].<ref name="AM1">{{cite web |url={{Allmusic|class=album|id=big-ones-r205993|pure_url=yes}} |title=Permanent Vacation - Aerosmith > Overview |author=John Franck & Eduardo Rivadavia |work=[[Allmusic]] |publisher=Rovi Corporation |accessdate=November 15, 2010}}</ref>

The album marks a turning point in the band's career. It is their first to employ professional songwriters, instead of featuring material solely composed by members of the band.<ref name="RS"/> This came at the suggestion of executive [[John Kalodner]]. He also pushed the band to work with producer [[Bruce Fairbairn]], who remained with them for another two albums.<ref>[http://www.melodicrock.com/interviews/johnkalodner.html MelodicRock.com Interviews: A&R guru John Kalodner under the microscope<!-- Bot generated title -->]</ref> It was also the first Aerosmith album to be promoted by heavy music video airplay on [[MTV]]. Though ''[[Done with Mirrors]]'' was intended to mark Aerosmith's comeback, ''Permanent Vacation'' is often considered their true comeback, as it was the band's first truly popular album since their reunion. "[[Rag Doll (Aerosmith song)|Rag Doll]]", "[[Dude (Looks Like a Lady)]]", and "[[Angel (Aerosmith song)|Angel]]" became major hits (all three charted in the Top 20) and helped ''Permanent Vacation'' become the band's greatest success in a decade.

The album features a cover of "[[I'm Down]]", a piano-driven Beatles song that appeared as a B-side to their single "[[Help! (song)|Help!]]" in 1965. This was Aerosmith's second commercially released Beatles cover, after "[[Come Together]]".

''Permanent Vacation'' has sold over five million copies in the U.S.<ref>{{cite web|url=http://www.riaa.com/goldandplatinumdata.php?resultpage=4&table=tblTop100&action= |title=Recording Industry Association of America |publisher=RIAA |date= |accessdate=2012-03-05}}</ref>

In the UK, it was the first Aerosmith album to attain both Silver (60,000 units sold) and Gold (100,000 units sold) certification by the [[British Phonographic Industry]], achieving these in July 1989 and March 1990 respectively.<ref name="BPI">{{cite web|url=http://www.bpi.co.uk/certifiedawards/search.aspx |title=Search for "Aerosmith" |publisher=Bpi.co.uk |date= |accessdate=2012-06-01}}</ref>

The album's title – a phrase from [[The Angels (American group)|The Angels]]' "[[My Boyfriend's Back (song)|My Boyfriend's Back]]" – was later referenced in Aerosmith's 1993 hit "[[Amazing (Aerosmith song)|Amazing]]" from the album ''[[Get a Grip]]''.

==Track listing==
{{Track listing
| writing_credits = yes
| total_length    = 51:38

| title1          = Heart's Done Time
| writer1         = [[Joe Perry (musician)|Joe Perry]], [[Desmond Child]]
| length1         = 4:42
| title2          = Magic Touch
| writer2         = [[Steven Tyler]], Perry, [[Jim Vallance]]
| length2         = 4:37
| title3          = [[Rag Doll (Aerosmith song)|Rag Doll]]
| writer3         = Tyler, Perry, Vallance, [[Holly Knight]]
| length3         = 4:25
| title4          = Simoriah
| writer4         = Tyler, Perry, Vallance
| length4         = 3:22
| title5          = [[Dude (Looks Like a Lady)]]
| writer5         = Tyler, Child, Perry
| length5         = 4:25
| title6          = St. John
| writer6         = Tyler
| length6         = 4:10
| title7          = [[Hangman Jury]]
| writer7         = Perry, Vallance, Tyler
| length7         = 5:33
| title8          = Girl Keeps Coming Apart
| writer8         = Tyler, Perry
| length8         = 4:13
| title9          = [[Angel (Aerosmith song)|Angel]]
| writer9         = Tyler, Child
| length9         = 5:08
| title10         = Permanent Vacation
| writer10        = Tyler, [[Brad Whitford]]
| length10        = 4:49
| title11         = [[I'm Down]]
| writer11        = [[Lennon–McCartney|John Lennon, Paul McCartney]]
| length11        = 2:20
| title12         = The Movie
| writer12        = Tyler, Perry, Whitford, [[Tom Hamilton (musician)|Tom Hamilton]], [[Joey Kramer]]
| length12        = 4:04
}}

==Personnel==
Adapted from the liner notes<ref name="notes">{{cite AV media notes |title=Big Ones |titlelink=Big Ones |others=[[Aerosmith]] |year=1994 |type=CD insert |publisher=[[Geffen Records]] |id=GEFD-24716 |location=[[United States|U.S.A.]] }}</ref><ref>[http://www.discogs.com/Aerosmith-Permanent-Vacation/release/370526 Aerosmith- ''Permanent Vacation'' @Discogs.com] Retrieved 12-14-2013.</ref> & Allmusic<ref name="AM2">{{cite web |url={{Allmusic|class=album|id=permanent-vacation-r180/credits|pure_url=yes}} |title=Permanent Vacation - Aerosmith > Credits |work=[[Allmusic]] |publisher=Rovi Corporation |accessdate=November 15, 2010}}</ref>
'''Aerosmith'''
*[[Steven Tyler]] – [[Lead vocalist|lead vocals]], [[piano]], [[harmonica]], [[organ (music)|organ]], [[mute (music)|plunger mute]]
*[[Joe Perry (musician)|Joe Perry]] – [[guitar]], [[Backing vocalist|backing vocals]], [[pedal steel guitar]] on "Rag Doll"
*[[Brad Whitford]] – guitar
*[[Tom Hamilton (musician)|Tom Hamilton]] – [[bass guitar]]
*[[Joey Kramer]] – [[drum kit|drums]]
{{col-begin}}
{{col-2}}
'''Additional musicians'''
*[[Strange Advance|Drew Arnott]] – [[mellotron]] on "Angel" and "The Movie"
*[[Tom Keenlyside]] – [[clarinet]], [[tenor saxophone]], [[Arrangement|horn arrangement]] on "Dude (Looks Like a Lady)" and "Rag Doll"
*Ian Putz – [[baritone saxophone]] on "Dude (Looks Like a Lady)" and "Rag Doll"
*Bob Rogers – [[trombone]] on "Dude (Looks Like a Lady)" and "Rag Doll"
*[[Henry Christian]] – [[trumpet]]
*[[Bruce Fairbairn]] – trumpet, [[cello]], background vocals
*Scott Fairbairn – cello
*[[Mike Fraser (record producer)|Mike Fraser]] – [[plunger mute]]
*Morgan Rael – [[steel drum]]s
*[[Jim Vallance]] – [[organ (music)|organ]] on "Rag Doll" and "Simoriah"
*Christine Arnott - Backing vocals on "The Movie"
{{col-2}}
'''Production'''
*[[Bruce Fairbairn]] – [[Record producer|production]]
*[[Mike Fraser (record producer)|Mike Fraser]] – [[Audio engineering|engineering]], [[Audio mixing (recorded music)|mixing]]
*[[Bob Rock]] – engineering
*Ken Lomas – assistant engineering
*George Marino – [[Audio mastering|mastering]]
*Kim Champagne – art direction
*Andy Engel – [[illustration]]s
*Neal Preston – [[photography]]
{{col-end}}

==Charts==
{{col-begin}}
{{col-2}}
;Album
{| class="wikitable sortable"
|-
!scope="col"|Chart (1987)
!scope="col"|Peak<br />position
|-
|US ''[[Billboard (magazine)|Billboard]]'' [[Billboard 200|200]]<ref>{{cite web |url={{BillboardURLbyName|artist=aerosmith|chart=all}} |title=Permanent Vacation - Aerosmith |work=[[Billboard (magazine)|Billboard.com]] |accessdate=November 15, 2010 }}</ref>
|style="text-align:center;"|11
|-
|Canada ''[[RPM (magazine)|RPM]]'' 100 Albums<ref>{{cite web|url=http://www.collectionscanada.gc.ca/rpm/028020-119.01-e.php?&file_num=nlc008388.0907&type=2&interval=20&PHPSESSID=m89iq841abagb37ld9c0fdc1f3 |title=Item Display - RPM - Library and Archives Canada |publisher=Collectionscanada.gc.ca |date= |accessdate=2012-03-05}}</ref>
|style="text-align:center;"|7
|-
|[[Oricon|Japanese Albums Chart]]<ref>{{cite web|url=http://www.oricon.co.jp/prof/artist/46082/ranking/cd_album/|title=エアロスミスのCDアルバムランキング、エアロスミスのプロフィールならオリコン芸能人事典-ORICON STYLE |publisher=Oricon.co.jp |accessdate=2013-05-02}}</ref>
| style="text-align:center;"|75
|-
|UK ([[UK Albums Chart|Top 100]])<ref>{{cite web|url=http://www.chartstats.com/albuminfo.php?id=10586 |title=Aerosmith - Permanent Vacation |publisher=Chart Stats |date=1990-04-28 |accessdate=2012-03-05|archiveurl=http://www.webcitation.org/6JNL3dZJv |archivedate= September 4, 2013 }}</ref>
|style="text-align:center;"|37
|}
{| class="wikitable sortable"
|-
!scope="col" style="width:13.2em;"|Chart (1988)
!scope="col"|Peak<br />position
|-
|Australia (Top 50)<ref>{{cite web|author=Steffen Hung |url=http://australian-charts.com/showitem.asp?interpret=Aerosmith&titel=Permanent+Vacation&cat=a |title=Aerosmith - Permanent Vacation |publisher=australian-charts.com |date=1988-10-30 |accessdate=2012-03-05}}</ref>
|style="text-align:center;"|42
|}
{{col-2}}
;Singles
{| class="wikitable"
|-
!Year
!Single
!Chart
!Position
|-
|rowspan="4"|1987
|rowspan="2"|"[[Dude (Looks Like a Lady)]]"
|[[Mainstream Rock Tracks]]
| style="text-align:center;"|4
|-
|The [[Billboard Hot 100|''Billboard'' Hot 100]]
| style="text-align:center;"|14
|-
|"[[Hangman Jury]]"
|Mainstream Rock Tracks
| style="text-align:center;"|14
|-
|"[[Rag Doll (Aerosmith song)|Rag Doll]]"
|Mainstream Rock Tracks
| style="text-align:center;"|12
|-
|rowspan="4"|1988
|rowspan="2"|"[[Angel (Aerosmith song)|Angel]]"
|Mainstream Rock Tracks
| style="text-align:center;"|1
|-
|The Billboard Hot 100
| style="text-align:center;"|3
|-
|"Dude (Looks Like a Lady)"
|[[Hot Dance Club Play|Hot Dance Music/Club Play]]
| style="text-align:center;"|41
|-
|"Rag Doll"
|The Billboard Hot 100
| style="text-align:center;"|17
|}
{{col-end}}

==Certifications==
{| class="wikitable"
|-
!Organization
!Level
!Date
|-
|rowspan="6"|[[RIAA]] - USA
|Gold<ref name="RIAA">{{cite web
| url=http://www.riaa.com/goldandplatinumdata.php?resultpage=1&table=SEARCH_RESULTS&action=&title=Permanent%20Vacation&artist=Aerosmith&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2009&sort=Artist&perPage=25
| title=Gold and Platinum Database Search
| accessdate=2009-11-24}}</ref>
|November 10, 1987
|-
|Platinum<ref name="RIAA"/>
|December 8, 1987
|-
|2× Platinum<ref name="RIAA"/>
|May 24, 1988
|-
|3× Platinum<ref name="RIAA"/>
|March 7, 1990
|-
|4× Platinum<ref name="RIAA"/>
|November 9, 1994
|-
|5× Platinum<ref name="RIAA"/>
|February 10, 1995
|-
|rowspan="6"|CIA – Canada
|Gold
|Nov 18, 1994
|-
|Platinum
|Nov 18, 1994
|-
|2× Platinum
|Nov 18, 1994
|-
|3× Platinum
|Nov 18, 1994
|-
|4× Platinum
|Nov 18, 1994
|-
|5× Platinum
|Nov 18, 1994
|}

==See also==
*''[[Permanent Vacation 3x5]]''
*[[Permanent Vacation Tour]]

==References==
{{Reflist|30em}}

==External links==
{{MusicBrainz release|id=31ad052e-65da-48ea-a259-4f5533625b3d|name=Permanent Vacation}}

{{Aerosmith}}

{{DEFAULTSORT:Permanent Vacation (Album)}}
[[Category:Aerosmith albums]]
[[Category:1987 albums]]
[[Category:Albums produced by Bruce Fairbairn]]
[[Category:Geffen Records albums]]