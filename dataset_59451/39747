{{Infobox person
| name          = Mary Barr
| image         = Photo of Mary Barr.jpg
| alt           = Mary Barr next to her transport plane, 1975
| caption       = Mary Barr next to her transport plane, 1975
| birth_name    = <!-- only use if different from name -->
| birth_date    = {{Birth date|1925|07|11}}
| birth_place   = [[United States]]
| death_date    = {{Death date and age|2010|03|01|1925|07|11}} 
| death_place   = [[United States]]
| nationality   = [[Americans|American]]
| other_names   = 
| occupation    = [[Aviator]], safety officer, [[mechanic]], [[flight instructor]]
| years_active  = 
| known_for     = 
| notable_works = 
}}
'''Mary Barr''' (July 11, 1925 – March 1, 2010) was the first [[Women in aviation|female aviator]] to join the [[US Forest Service]],<ref>{{cite book |last=Gibson |first=Karen Bush |title=Women Aviators: 26 Stories of Pioneer Flights, Daring Missions, and Record-Setting Journeys |url=https://books.google.com/books?id=Ab64RI83BKEC |date=2013 |publisher=[[Chicago Review Press]] |page=134 }}</ref> along with being an accident prevention counselor, [[mechanic]] in a variety of fields, and [[flight instructor]] throughout her lifetime.

==Career==
While living in [[Lorain, Ohio]] and working in a factory, Barr first learned how to fly aircraft in 1946 as part of a [[Flying club|Piper club]]. She had dropped out of [[Oberlin College]] the year before in order to find a job to pay for flying lessons. After completing her training, she obtained a job training others to be commercial pilots.<ref name="People">{{cite news |last=Moore |first=Gerald |date=September 15, 1975 |title=When Western Forests Start to Burn, a Low-Flying Woman Pilot Takes the Perilous Lead |url=http://www.people.com/people/archive/article/0,,20065655,00.html |newspaper=[[People (magazine)|People]] |access-date=March 11, 2016}}</ref><ref name="WAI"/> During the end years of [[World War II]], she decided to help build aircraft for the war, which led to her moving to [[New York City]] and joining a [[aircraft mechanic]] school.<ref name="WAI"/> This also involved acting alongside members of the [[Women Airforce Service Pilots]] to assist in transporting war goods and planes across the US.<ref>{{cite news |last=Ghosh |first=Sreyashi |date=November 9, 2015 |title=Remembering women pilots of WWII |url=http://www.dailyrecord.com/story/news/2015/11/09/salute-service-world-war-ii-wasps/75447454/ |newspaper=[[Daily Record (Morristown)|Daily Record]] |access-date=March 13, 2016}}</ref> The end of the war resulted in the Barrs moving to [[Susanville, California]] in 1949, and setting about running and improving the Susanville airport. After obtaining her certification in 1957, she obtained the position of [[FAA Pilot Examiner]] for [[Lassen County]].<ref name="Lassen">{{cite news |last=Morgan |first=Woody |date=February 6, 2001 |title=Aviation hall of fame induction next for Barr |url=http://las.stparchive.com/Archive/LAS/LAS02062001P17.php |newspaper=[[Lassen County Times]] |access-date=March 12, 2016}}</ref> In 1964, Barr became one of the first four women to ever be a part of the [[Reno Air Races]].<ref name="WAI"/> She placed second in the Reno National Championships in the Stock Plane Class using a [[Piper Cherokee]].<ref>{{cite web |url=http://www.airrace.com/PDFs/Concise%20History--Chapter%207.pdf |title=A Concise History of Air Racing |last1=Berliner |first1=Don |date=April 30, 2013 |website=AirRace.com |publisher=[[Society of Air Racing Historians]] |access-date=March 12, 2016}}</ref>

Beginning a career in the [[US Forest Service]] in 1974, she became the first female pilot to do so, being promoted to official staff after having worked with the Forest Service as a contract pilot for several years.<ref name="People"/><ref>{{cite web |url=http://www.ninety-nines.org/women-in-aviation-article.htm |title=Women in Aviation |last1=Gant |first1=Kelli |date=2016 |website=The Ninety-Nines, Inc. |publisher=[[Ninety-Nines]] |access-date=March 10, 2016}}</ref> Her job entailed working as a lead plane pilot for the California North Zone Air Unit. Later in the 1970s, Barr moved to [[San Francisco]] to be an [[Aviation Safety]] Officer for the Forest Service, then becoming a National Aviation Safety Officer in [[Washington D. C.]], and finally moving to [[Sacramento, California]] in 1985 to act as Regional Safety Officer until her eventual retirement.<ref name="Lassen"/><ref name="Susanville">{{cite news |last=Couso |first=Jeremy |date=April 25, 2013 |title=Historical Society Readies New Museum Exhibits for Summer |url=http://www.susanvillestuff.com/historical-society-readies-museum-for-summer-open-house-may-3rd/ |newspaper=[[SusanvilleStuff]] |access-date=March 11, 2016}}</ref> She was named as a member of the 2001 Aviation International Pioneer Hall of Fame for [[Women in Aviation, International]]<ref name="WAI">{{cite web |url=https://www.wai.org/pioneers/2001pioneers.cfm |title=2001 Pioneer Hall of Fame |author=<!--Staff writer(s); no by-line.--> |date=2014 |website=[[Women in Aviation, International]] |access-date=March 10, 2016}}</ref> and directly honored by the [[Smithsonian National Air and Space Museum]].<ref name="Susanville"/>

During her lifetime, Barr received a number of [[FAA certification]]s, including for "Commercial, Airline Transport Pilot, Flight Instructor, Instrument, and Glider".<ref name="WAI"/>

==Personal life==
Barr met her husband and boss at the time, David Barr, while teaching piloting in 1946.<ref name="People"/> They married the next year and had two children, Molly and Virginia.<ref name="Lassen"/>

==Gallery==
<gallery caption="Photos of Mary Barr" mode=packed heights=175>
Photo of Mary Barr 2.jpg|Photo of Mary Barr sitting in the cockpit of her transport plane, 1975|alt=Photo of Mary Barr sitting in the cockpit of her transport plane, 1975
File:Photo of Mary Barr 3.jpg|Photo of Mary Barr standing on the wing of her transport plane, 1975|alt= Photo of Mary Barr standing on the wing of her transport plane, 1975
</gallery>

==References==
{{reflist|30em}}

{{DEFAULTSORT:Barr, Mary}}
[[Category:1925 births]]
[[Category:2010 deaths]]
[[Category:People from Lorain, Ohio]]
[[Category:American female aviators]]
[[Category:United States Forest Service officials]]
[[Category:Aerial firefighting]]
[[Category:American women in World War II]]