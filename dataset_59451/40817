{{Infobox person
| name        = Matthew Pittinsky
| image       = 
| alt         = 
| caption     = 
| birth_name  = 
| birth_date  = 
| birth_place = 
| death_date  = 
| death_place = 
| nationality = American 
| other_names = 
| occupation  = CEO of Parchment 
| known_for   = Co-founder of [[Blackboard Inc]] along with [[Michael Chasen]], [[Stephen Gilfus]], [[Daniel Cane]]
| alma_mater  = [[American University]], [[Harvard Graduate School of Education]]
}}

'''Matthew Pittinsky''' is an [[United States|American]] [[technology]] [[Entrepreneurship|entrepreneur]], educator and [[Academia|academic]]. He is the [[Chief executive officer|CEO]] of Parchment and a co-founder of [[Blackboard Inc.|Blackboard Inc]]. Pittinsky is also affiliated with [[Arizona State University]] as an assistant research [[professor]].

== Early life ==

The youngest of four children, Matthew Pittinsky grew up in a family of educators.<ref name=Bruno01/><ref name=Winkler13/> His father worked as a [[Academic administration|university administrator]], and was the president of the local school board.<ref name=Layne99>{{cite news |title=Matthew Pittinsky |author=Anni Layne |url=http://www.fastcompany.com/61746/matthew-pittinsky |work=[[Fast Company (magazine)|Fast Company]] |date=31 October 1999 |accessdate=25 June 2014}}</ref><ref name=Sivak/> Pittinsky's mother was a teacher at a [[State school|public]] [[Primary school|elementary school]].<ref name=JewishNews12/>

As a child, Pittinsky was interested in technology and used his family's [[Atari 8-bit family|Atari 800 computer]] to program in [[BASIC]] and later learned to program on an [[Atari ST|Atari 1040st]].<ref name=Bruno01/><ref name=Pittinsky>{{cite book |last=Pittinsky |first=Matthew |editor-first=Pittinsky |editor-last=Matthew |title=The Wired Tower: Perspectives on the Impact of the Internet on Higher Education |publisher=[[Prentice Hall|Financial Times Prentice Hall]] |date=July 1, 2002 |pages=201–202 |chapter=Chapter 8: Five Great Promises of E-Learning |isbn=978-0130428295}}</ref><ref name=Kelleher11/> He was also active in [[Boy Scouts of America|Boy Scouts]], earning the rank of [[Eagle Scout (Boy Scouts of America)|Eagle Scout]].<ref>{{cite web |url=http://troop214.a-cal.com/document/25576 |title=Troop 214 Eagle Scouts |author= |date= |work=troop214.a-cal.com |publisher=Boy Scouts of America, Troop 214 |accessdate=26 June 2014}}</ref>
Pittinsky wanted to become a teacher from an early age,<ref name=Sivak>{{cite news |title=The education process is being transformed from chalkboard-intensive to utilization of e-Learning capabilities |author=Cathy Sivak |url=http://www.education.org/interviews/pittinsky.html |work=[[Education.org]] |date= |accessdate=26 June 2014}}</ref><ref name=Gibbs/> though he initially struggled as a student and had to repeat failed classes in [[summer school]].<ref name=Aronson14>{{cite news |title=Matthew Pittinsky, CEO at Parchment |author= Isaak Aronson |url=http://edexecutives.tumblr.com/post/79172725641/matthew-pittinsky-ceo-at-parchment |work=[[The Perspective]] |date=10 March 2014 |accessdate=26 June 2014}}</ref> After high school, he attended [[American University]] with the goal of becoming a [[social studies]] teacher. At American, Pittinsky served as student body president his senior year. He graduated in 1994 with a bachelor's degree in political science.<ref name=Kelleher11>{{cite news |title=Blackboard Founders’ Roots at American University |author=Anne Kelleher |url=http://www.american.edu/americantoday/campus-news/20110701-Blackboard-Pittinsky-Chasen.cfm |work=[[American Today]] |date=1 July 2011 |accessdate=17 June 2014}}</ref><ref name=FinancialExpress>{{cite news |title=Net software for entrepreneurs |author= |url=http://expressindia.indianexpress.com/fe/daily/19990110/01055375p.html |work=[[The Financial Express (India)|The Financial Express]] |date=1999 |accessdate=30 June 2014}}</ref><ref name=WashingtonPost98>{{cite news |title=Matthew Pittinsky, 25, CEO of Blackboard Inc. |author= |url=http://www.washingtonpost.com/wp-srv/washtech/features/new/ceo/pittinsky.htm |work=[[The Washington Post]] |date=1998 |accessdate=25 June 2014}}</ref>
While at American, Pittinsky met [[Michael Chasen]], who would later become his business partner. The two met after Pittinsky borrowed a printer from Chasen. They later became roommates and [[fraternity]] brothers.<ref name=Gibbs>{{cite news |title=Blackboard Rules: Advice from Two of the Smartest Kids in the Class |author=Hope Katz Gibbs |url=http://www.beinkandescent.com/tips-for-entrepreneurs/323/the-magic-of-blackboard-inc |work=[[Beinkandescent]] |accessdate=17 June 2014}}</ref><ref name=Burn06>{{cite news |title=Agent of Change: Blackboard CEO Michael Chasen erases the old way of learning |author=Timothy Burn |url=http://www.smartceo.com/sites/default/files/May06.pdf |work=[[SmartCEO]] |date=May 2006 |accessdate=17 June 2014}}</ref>
After graduating from American, Pittinsky earned a [[Master of Education|Master's of Education]] in 1995 from [[Harvard Graduate School of Education]].<ref name=Bruno01>{{cite news |title=Washtech.com: The E-learning Revolution |author=Michael Bruno |url=http://www.washingtonpost.com/wp-srv/liveonline/01/washtech/special/washtech_blackboard120501.htm |work=[[The Washington Post]] |date=5 December 2001 |accessdate=25 June 2014}}</ref><ref name=Kelleher11/><ref name=Gibbs/>

== Career ==

=== Early career ===

While at Harvard, Pittinsky developed [[software]] for online [[University and college admission|college admissions]] applications with Chasen. They called the venture Search and Apply Group.<ref name=Kelleher11/> Pittinsky and Chasen were unable to sell the idea to universities and later abandoned the project.<ref name=Gibbs/><ref name=Burn06/>
 
In 1995, Pittinsky took a job at [[KPMG|KPMG Peat Marwick]] as a higher education consultant. Chasen was hired by KPMG the following year and together they created technology solutions for universities. In 1996, while jogging along the [[Charles River]] near Harvard, Pittinsky had the idea to create online software for course instruction. He wrote a 100-page plan for the idea, which he called ''Genesis'', in one night. With Chasen's help, he developed the [[business plan]] for what would later become [[Blackboard Inc]].<ref name=Kelleher11/><ref name=Burn06/>

Pittinsky and Chasen left KPMG to start Blackboard in 1997. They used computers loaned from their boss and also stole desk chairs by using them to move the borrowed computers out of the office.<ref name=Gibbs/>

=== Blackboard Inc ===

Pittinsky founded Blackboard Inc, originally Blackboard LLC, with Chasen in 1997. At the time of the company’s foundation, it operated as a consulting group to the IMS e-Learning standards project, where Pittinsky was an early member.<ref name=Bruno01/><ref name=Wirthman12/> In 1998, Blackboard LLC merged with CourseInfo LLC, an online learning software company developed by [[Cornell University]] students [[Stephen Gilfus]] and [[Daniel Cane]], to create Blackboard Inc.<ref name=WashingtonPost98/><ref name=Burn06/><ref name=Darcy09>{{cite news |title=Years at Blackboard an education that turns employees into entrepreneurs |author=Darlene Darcy |url=http://www.bizjournals.com/washington/stories/2009/08/03/story2.html?page=all |work=[[American City Business Journals|Washington D.C. Business Journal]] |date=9 August 2009 |accessdate=28 July 2014}}</ref> Blackboard introduced its first product, Blackboard CourseInfo, that same year.<ref name=Kelleher11/>
 
Between 1997 and 2008, Pittinsky served in several roles at the company, including [[chairman]] and [[Chief executive officer|CEO]], co-CEO, and executive chairman.<ref name=Aronson14/><ref name=DallasBusiness02/> He was responsible for building the Blackboard brand along with Chasen through [[convention (meeting)|conferences]], [[Electronic mailing list|list-serves]], [[marketing]] and [[Business networking|networking]].<ref name=Layne99/><ref name=Gibbs/><ref name=Burn06/> Pittinsky also shaped [[Strategic management|corporate strategy]] and [[Product management|product strategy]], and oversaw the company’s relations with the education community.<ref name=Bruno01/>

In 2002, while at Blackboard, Pittinsky co-wrote and edited a book called the ''The Wired Tower'' on [[e-learning]] and the internet's impact on education.<ref name=TCMedia02>{{cite news |title=The Wired Tower: Perspectives on the Impact of the Internet on Higher Education |author= |url=http://www.tc.columbia.edu/news.htm?articleId=4266 |work=[[Teachers College, Columbia University|TC Media Center]] |date=1 December 2002 |accessdate=25 June 2014}}</ref>
Pittinsky helped take Blackboard [[Public company|public]] in June 2004. The first day of trading raised US$70 million for the company, becoming the second most successful technology [[Initial public offering|IPO]] of the year.<ref name=Burn06/><ref name=Wirthman12/>

=== Academia ===

Pittinsky left Blackboard in 2008 after completing his [[Doctor of Philosophy|Ph.D.]] in [[Sociology]] of Education from [[Columbia University]] through [[Teachers College, Columbia University|Teachers College]]. After earning his Ph.D., he was hired as an assistant [[professor]] of sociology at [[Arizona State University]] in January 2009.<ref name=Wirthman12>{{cite news |title=Matthew Pittinsky found his formula for success — times two |author=Lisa Wirthman |url=http://www.bizjournals.com/denver/print-edition/2012/06/29/matthew-pittinsky-found-his-formula.html?page=all |work=[[American City Business Journals|Denver Business Journal]] |date=29 June 2012 |accessdate=25 June 2014}}</ref><ref name=Rivero12/><ref name=Parry12/> His academic research specialized in [[economic sociology]], [[sociology of education]], and [[social network analysis]].<ref name=Winkler13/><ref name=Rivero12/>

While at Arizona State, Pittinsky created a research project that used [[data]] collected from student [[Identity document|ID]] cards to track student transactions and understand student social and academic activities.<ref name=Parry12>{{cite news |title=Big Data on Campus |author=Marc Parry |url=https://www.nytimes.com/2012/07/22/education/edlife/colleges-awakening-to-the-opportunities-of-data-mining.html?pagewanted=all&_r=0 |work=[[The New York Times]] |date=18 July 2012 |accessdate=25 June 2014}}</ref>
As of March 2014, Pittinsky is still affiliated with Arizona State as an assistant research professor.<ref name=Winkler13>{{cite news |title=Matthew Pittinsky of Parchment - Today's Campus Innovation Interview Series |author=Kirsten Winkler |url=http://todayscampus.uberflip.com/h/i/53089-matthew-pittinsky-of-parchment-todays-campus-innovation-interview-series |work=[[Today's Campus]] |date=25 April 2013 |accessdate=25 June 2014}}</ref>

=== Parchment ===

While at Blackboard, Pittinsky became interested in creating a company to help users manage [[Credential|academic credentials]].<ref name=Wirthman12/><ref name=Rivero12>{{cite news |title=Matthew Pittinsky Unleashes the Power of Parchment |author=Victor Rivero |url=http://edtechdigest.wordpress.com/2012/09/10/interview-matthew-pittinsky-unleashes-the-power-of-parchment/ |work=[[EdTech Digest]] |date=10 September 2012 |accessdate=25 June 2014}}</ref> He was later introduced to Docufide, a company founded in 2003 that specialized in the online transfer of academic transcripts from high schools to colleges and universities.<ref name=Wirthman12/>

In January 2011, Pittinsky invested in Docufide and was hired as the company's CEO.<ref name=Wirthman12/><ref name=SoCalTech11>{{cite news |title=Docufide Raises $4.5M, Taps Blackboard Founder As CEO |author= |url=http://www.socaltech.com/docufide_raises_4__m_taps_blackboard_founder_as_ceo/s-0033813.html |work=[[socaltech.com]] |date=February 9, 2011 |accessdate=28 July 2014}}</ref> In April 2011, the company rebranded as Parchment and Pittinsky helped launch Parchment.com, the company's consumer site, later that year.<ref name=JewishNews12/><ref name=Rivero12/>

Under Pittinsky's leadership, the platform has grown from 800,000 users in 2011 to approximately 1.6 million users in 2014. He has led the company through three [[Venture round|rounds of financing]] which, as of March 2014, have brought in a total of $45 million in investments.<ref name=Wirthman12/><ref name=Winkler14>{{cite news |title=Parchment Raises $10 Million Follow-on Investment |author=Kirsten Winkler |url=http://www.edukwest.com/hedline-parchment-raises-10-million-follow-investment/ |work=[[Edukwest]] |date=19 March 2014 |accessdate=25 June 2014}}</ref>
He has also helped expand Parchment's services to allow users to store and transfer [[Transcript (education)|transcripts]], use academic data such as test scores and [[Grading (education)|GPA]] to find matching institutions, and determine their likelihood of acceptance.<ref name=Wirthman12/><ref name=Rivero12/>

During his time at Parchment,  became an advocate for the adoption of Postsecondary Achievement Reports (PAR), which are documents that include both traditional academic records, such as grades, and information about student's development and learning experiences.<ref name=Pittinsky14>{{cite news |title=Extending the Transcript |author=Matthew Pittinsky |url=http://www.insidehighered.com/views/2014/02/10/essay-calls-broader-concept-transcripts#sthash.ixbucJPj.dpbs |work=[[Inside Higher Ed]] |date=10 February 2014 |accessdate=25 June 2014}}</ref>

== Awards and recognition ==

While at Blackboard, Pittinsky won several awards. In 2000, he was awarded a "Young Innovator" award from the [[Kilby International Awards|Kilby Awards Foundation]] and an [[Ernst & Young]] award for "Entrepreneur of the Year for Emerging Companies in Washington, D.C."<ref name=DallasBusiness02>{{cite news |title=Blackboard Inc.'s execs honored |author= |url=http://www.bizjournals.com/dallas/stories/2000/10/30/daily14.html |work=[[American City Business Journals|Dallas Business Journal]] |date=2 November 2002 |accessdate=25 June 2014}}</ref>
He was included in [[Washingtonian (magazine)|''Washingtonian'' magazine’s]] “100 People to Watch” list in 1999  and named to ''Washington Techway'' magazine's list of Top Under-30 Technology Executives in 2000.<ref name=Bruno01/><ref name=Means99>{{cite news |title=100 People to Watch |author=Howard Means |url=http://www.archive.today/2IbsX#selection-205.0-6.4  |work=[[Washingtonian (magazine)|The Washingtonian]] |date=1999 |accessdate=25 June 2014}}</ref> In 2001, he was named "Visionary of the Year" by the Northern Virginia Technology Council.<ref name=Bruno01/>

In 2012, Pittinsky received the President's Medal of Excellence from Teachers College.<ref name=EducationUpdate12>{{cite news |title=President Susan Fuhrman Presents Awards to Distinguished Alums at Teachers College |author= |url=http://www.educationupdate.com/archives/2012/MAY/HTML/col-susanfuhrman.html |work=[[Education Update]] |date=May–June 2012 |accessdate=25 June 2014}}</ref>

== Personal life ==

Pittinsky lives in Arizona with his wife and three children. He is of [[Judaism|Jewish]] descent and is a member of [[Congregation Beth Israel (Scottsdale, Arizona)|Congregation Beth Israel]].<ref name=JewishNews12>{{cite news |title=Business Profile: Matthew Pittinsky, CEO |author= |url=http://www.jewishaz.com/localnews_features/matthew-pittinsky-ceo/article_2f6694df-6a3f-5a45-b8e5-5843a2e166d7.html |work=[[Jewish News of Greater Phoenix]] |date=10 August 2012 |accessdate=25 June 2014}}</ref>
Active as an [[angel investor]], Pittinsky has invested in companies including SocialRadar and Interfolio.<ref name=Flook13>{{cite news |title=SocialRadar raises $12.75 million from NEA, Grotech, Leonsis and more |author=Bill Flook |url=http://www.bizjournals.com/washington/blog/techflash/2013/06/michael-chasens-socialradar-lands.html?page=all |work=[[American City Business Journals|Washington Business Journal]] |date=19 June 2013 |accessdate=25 June 2014}}</ref><ref name=CrunchBase>{{cite web |url=http://www.crunchbase.com/person/matthew-pittinsky |title=Matthew Pittinsky |author= |date= |work= |publisher=[[TechCrunch|CrunchBase]] |accessdate=28 July 2014}}</ref> As of March 2014, Pittinsky serves on the board of trustees of the [[Woodrow Wilson National Fellowship Foundation]].<ref name=Aronson14/> He has previously served on the American University board of trustees and on the board of In2Books, a [[literacy]] [[nonprofit organization]].<ref name=Sivak/>

== References ==

{{reflist|2}}

== External links ==
* {{Official website|http://www.parchment.com/}}

{{DEFAULTSORT:Pittinsky, Matthew}}
[[Category:American University School of Public Affairs alumni]]
[[Category:American technology chief executives]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Harvard Graduate School of Education alumni]]
[[Category:Teachers College, Columbia University alumni]]