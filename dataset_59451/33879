{{DISPLAYTITLE:Terms of Endearment (''The X-Files'')}}
{{Infobox television episode
| title = Terms of Endearment
| series = [[The X-Files]]
| season = [[The X-Files (season 6)|6]]
|length=45 minutes<ref>{{cite web|title=The X-Files, Season 6|url=https://itunes.apple.com/gb/tv-season/the-x-files-season-6/id283986520|work=[[iTunes Store]]|accessdate=28 August 2012}}</ref>
| episode = 7
| image = [[Image:Terms of endearment x files.jpg|250px|Terms of Endearment|alt=Silhouette of a demon, with fire in the background.]]
| caption = Wayne Weinsider as a demon in his wife's "dream". The scene was shot to give the effect that the fire was mere inches away from the bed.
| airdate = January 3, 1999
| production = 6ABX06<ref name=DVD/>
| writer = David Amann
| director = [[Rob Bowman (director)|Rob Bowman]]
| guests =
* [[Bruce Campbell]] as Wayne Weinsider
* [[Lisa Jane Persky]] as Laura Weinsider
* Grace Phillips as Betsy Monroe
* [[Chris Owens (actor)|Chris Owens]] as [[Jeffrey Spender|Agent Spender]]
* [[Michael Milhoan]] as Deputy Arky Stevens
* Michael Rothhaar as Dr. Couvillion
* Lenora May as Kim Britton
* Jimmy Staszkiel as Mr. Ginsberg
* Karen Stone as Nurse
* [[Matthew Butcher]] as EMT<ref name="plot">[[#Meisler|Meisler (2000)]], p. 68–78.</ref>
| prev = [[How the Ghosts Stole Christmas]]
| next = [[The Rain King]]
| episode_list = [[List of The X-Files episodes|List of ''The X-Files'' episodes]]
}}
"'''Terms of Endearment'''" is the seventh episode of the [[The X-Files (season 6)|sixth season]] of the American [[science fiction on television|science fiction]] television series ''[[The X-Files]]'', and originally aired on the [[Fox Broadcasting Company|Fox]] network on January&nbsp;3, 1999. Written by David Amann and directed by [[Rob Bowman (director)|Rob Bowman]], "Terms of Endearment" is a "[[List of Monster-of-the-Week characters in The X-Files|Monster-of-the-Week]]" story, unconnected to the series' wider mythology. It earned a [[Nielsen ratings|Nielsen rating]] of 10.5 and was watched by 18.7 million people on its initial broadcast. The performance given by guest actor [[Bruce Campbell]] attracted positive comments, but the plot was criticized.

The show centers on [[Federal Bureau of Investigation|FBI]] special agents [[Fox Mulder]] ([[David Duchovny]]) and [[Dana Scully]] ([[Gillian Anderson]]) who work on cases linked to the paranormal called [[X-File]]s. Mulder is a believer in the paranormal, while the skeptical Scully has been assigned to debunk his work. In the installment, an unborn child is apparently abducted from its mother's womb by a demon after the prospective parents discover that their child has birth defects. After Agent Spender dismisses the assignment as irrelevant to the X-Files, Mulder and Scully steal the case and investigate the creature. While looking into the report, the duo discover that Wayne Weinsider (Campbell) is a child-abducting demon.

"Terms of Endearment", an inversion of the 1968 film ''[[Rosemary's Baby (film)|Rosemary's Baby]]'', was the first episode written by ''The X-Files''{{'}} executive story editor David Amann, a staff member who later became a regular contributor to the series. Campbell, already well known as a [[cult film]] actor in several [[Sam Raimi]] horror movies, was cast as Wayne Weinsider. Many of the episode's special effects were created without elaborate computer-generated effects. Critics have complimented the episode's unique representation of its antagonist, who has been classified as a sympathetic villain.

==Plot==
In [[Roanoke, Virginia]], Wayne Weinsider ([[Bruce Campbell]]) and his pregnant wife Laura ([[Lisa Jane Persky]]) learn via an [[medical ultrasonography|ultrasound scan]] that their unborn child has bizarre physical abnormalities, such as horn-like protrusions. Weinsider appears to be especially distraught after hearing the news. That night, Laura has a terrifying dream in which a [[demon]]-like figure snatches the baby from her womb. When she wakes up, the couple discover that Laura has seemingly [[miscarriage|miscarried]]. Laura's brother, local [[deputy sheriff]] Arky Stevens, reports her story to the [[X-File]]s section at the [[Federal Bureau of Investigation|FBI]]. Agent [[Jeffrey Spender]] ([[Chris Owens (actor)|Chris Owens]]) discards the report, but Agent [[Fox Mulder]] ([[David Duchovny]]) salvages the case and travels to Virginia, along with Agent [[Dana Scully]] ([[Gillian Anderson]]) to interview the Weinsiders.

The police, who suspect an illegal abortion, find the remains of the baby in the garden furnace. Wayne quickly confesses to destroying the evidence. He asserts that his wife had aborted the child while in a trance-like state and [[gaslighting|attempts to convince her of this]]. Laura takes the blame for the abortion and goes to jail. While visiting Laura in her cell, Wayne attempts to steal her soul, but the [[Emergency medical technician|EMT]] is able to save her life, much to Wayne's surprise. Meanwhile Wayne has another wife, Betsy Monroe, who is also pregnant.

Mulder, determining that Wayne is a Czech immigrant by the name of Ivan [[Veles (god)|Veles]], reaches the conclusion that Wayne is a demon who is trying to have a normal baby, and suspects that he terminates pregnancies when the fetus exhibits demon-like traits. Betsy has a dream similar to Laura's, but recognizes the dream-demon as her husband. Nevertheless, she loses her baby and tracks down Mulder and Scully. The two seek out Weinsider and catch him digging in Betsy's backyard. After a short conversation, the sheriff arrives at the house and shoots Weinsider. He is taken to hospital and placed in a bed next to Laura. A mist passes from Weinsider's body into Laura's, at which point Weinsider dies and Laura recovers.

Mulder and Scully discover remains of normal human babies in Betsy's yard where Weinsider was digging. When seeing the evidence, Mulder deduces that Betsy is another demon who is unable to have a baby demon unless another demon impregnates her. Unlike Weinsider, she has been terminating pregnancies that resulted in non-demonic fetuses: the very type Weinsider has been so desperate to father. As a demon, she could recognize her husband as a demon in her dream and stop him extracting her baby. However, it becomes apparent that she never lost her baby; it was merely a trick to frame her demon husband. At the end of the narrative, Betsy Monroe drives away with her new demon baby. Her eyes flash with a red gleam before returning to normal.<ref name="plot"/><ref>{{cite web |url=http://www.allmovie.com/movie/the-x-files-terms-of-endearment-v210681/ |title=The X-Files: Terms of Endearment (1999) |work=[[AllMovie]] |accessdate=27 November 2009}}</ref>

==Production==

===Background===
[[File:Chris Carter (July 2008).jpg|alt=A man with white hair is looking and smiling at the camera.|''[[The X-Files]]'' creator [[Chris Carter (screenwriter)|Chris Carter]] greenlit the episode, following a pitch provided by David Amann.|upright|thumb|right]]

"Terms of Endearment" was the first episode written by ''The X-Files'' executive story editor David Amann. Amann had previously written two network movies titled ''[[The Man Who Wouldn't Die (1995 film)|The Man Who Wouldn't Die]]'' and ''Dead Air'' and had served two years on the staff of the [[CBS]] [[medical drama]] ''[[Chicago Hope]]''. The concept for "Terms of Endearment" was "about the fifth or sixth idea" Amann came up with for the show.<ref name="m79"/> Amann's original idea was to write what he described as "''Rosemary's Baby'' in reverse".<ref name="m79"/> He explained, "I had this idea [for doing the episode] not from the point of view of the hapless woman unwittingly impregnated, but from the point of view of the devil".<ref name="m79"/> Amann pitched his idea to series creator [[Chris Carter (screenwriter)|Chris Carter]], who gave him the commission to write the rest of the episode.<ref name="m79"/><ref name="m78">[[#Meisler|Meisler (2000)]], p. 78.</ref>

According to Amann, the initial draft was "heavier on pure shock value and lighter on humor and human interest".<ref name="m79"/> In this version, Laura Weinsider gave birth to a serpent rather than a demon baby. In addition, the story unraveled in a more "linear" fashion.<ref name="m79"/> This draft also called for the devil to seek a human baby, resulting in the sacrifice of his wife. The series regular writers felt that this initial version of the story had a certain "inevitability" to it.<ref name="m79"/> Carter suggested that the second woman should be a demon. Amann later admitted that this addition made the story "work well".<ref name="m79">[[#Meisler|Meisler (2000)]], p. 79.</ref> Kerry Fall from ''DVD Journal'' suggested that the plot revolved around "the wives and lives of a demon trying to have a normal child."<ref>{{cite web|last=Fall|first=Kerry|title=The X-Files: Season Six|url=http://www.dvdjournal.com/quickreviews/x/xfiles.season6.q.shtml|work=DVD Journal|accessdate=11 January 2012}}</ref>

The score for "Terms of Endearment" was composed by series regular [[Mark Snow]], who used [[Gregorian chant]]s to give the atmosphere a "creepy" feel.<ref name="m79"/> The 1995 song "[[Only Happy When It Rains]]" by [[alternative rock]] group [[Garbage (band)|Garbage]] plays several times in the episode, most notably when Betsy Monroe drives away with her demon baby.<ref name="happy">[[#Meisler|Meisler (2000)]], p. 73.</ref><ref>{{cite AV media notes|title=[[Only Happy When It Rains]]|others=[[Garbage (band)|Garbage]]|date=1995|type=UK CD/cassette Single liner notes|publisher=[[A&E Records#Mushroom Records UK|Mushroom Records UK]]|id=D1199/C1199}}</ref> The quote "Zazas, zazas, nasatanada zazas"—what Laura Weinsider was supposed to have said while "in a trance"—is what the [[occult]]ist [[Aleister Crowley]] used to open the 10th Aethyr of the [[Thelema|Thelemic]] demon [[Choronzon]].<ref>[[#Owen|Owen (2000)]], p. 186.</ref> "Terms of Endearment" is not the first occasion that the series drew influence from Crowley; a high school from the episode "[[Die Hand Die Verletzt]]" was named after him as well.<ref name="messy"/><ref name="crowley">[[#Lowry|Lowry (1995)]], p. 195.</ref>

===Casting===
[[File:David Duchovny 2011 Shankbone.JPG|alt=A man standing in front of purple background.|[[David Duchovny]] (''pictured'') entertained himself during production by playing pranks with [[Bruce Campbell]]. |upright|thumb|left]]

Rick Millikan selected Lisa Jane Persky, who was at the top of his casting "wish list", for the role of Laura.<ref name="m79"/> In the middle of filming, a cast member withdrew from the production for religious reasons. A mother withdrew her baby from the cast during the final run-through of the "cursed-birth" scene. Although a fan of the show, as a devout Catholic she was uncomfortable with her child representing a demon. Director Rob Bowman assured her that the production staff understood her dilemma; the casting staff called down and found a replacement in less than 45 minutes. It was the first time during the filming of the series that a cast-member withdrew for religious reasons.<ref name="m78"/>

[[Bruce Campbell]], known for taking leading roles in Sam Raimi horror movies such as ''[[The Evil Dead (franchise)|The Evil Dead]]'' trilogy, was cast as the episode's antagonist Wayne Weinsider.<ref name="EVIL">[[#Kenneth Muir|Kenneth Muir (2004)]], p. 112.</ref><ref>[[#Lowry|Lowry (1995)]], p. 4.</ref> Campbell had previously worked for the [[Fox Broadcasting Company|Fox]] network on his short lived series ''[[The Adventures of Brisco County, Jr.]]'', which began as the lead-in show for ''The X-Files'' during the [[The X-Files (season 1)|first season]].<ref>{{cite journal | title = X'd Out | journal = [[Cinefantastique]] |date = April 2002| first = Paula | last = Vitaris |author2=Dan Coyle  | volume = 34 | issue = 2 | page = 38| id = }}</ref><ref name="p1921">[[#Lowry|Lowry (1995)]], pp.19–21.</ref> Fox had initially assumed that ''Brisco County, Jr.'' was going to be the more successful series, while ''The X-Files'' was more unappreciatively referred to as "the other drama Fox ordered that spring".<ref>[[#Lowry|Lowry (1995)]], p. 19.</ref><ref name="Campbell258"/><ref name="book">[[#Booker|Booker (2002)]], p. 122</ref> Several of the individuals who worked on the failed ''Brisco County, Jr.'' series later found a career working on ''The X-Files'', which led Campbell to call the process a "coming home" experience.<ref name="Campbell258"/>

Campbell first met both Duchovny and Anderson during the promotional campaigns for both series in 1993.<ref name="Campbell258">[[#Campbell|Campbell (2002)]], p. 258.</ref> He commented that, although Duchovny was known for playing a serious role on the show, in real life he was quite a funny person.<ref name="Campbell257"/><ref name="funny"/> During shooting, Campbell and Duchovny entertained themselves on set by pranking the crew members, eventually stopping it when crew members became upset.<ref name="Campbell257">[[#Campbell|Campbell (2002)]], p. 257.</ref> Although Campbell had a positive opinion of working on the show, he commented that the nature of the show did not allow for much improvisation, describing the production as a "well oiled machine".<ref name=funny>{{cite web|last=Kleinman|first=Geoffrey|title=Bruce Campbell – Chat Transcript|url=http://www.dvdtalk.com/interviews/bruce_campbell.html|work=[[DVD Talk]]|accessdate=11 August 2012}}</ref><ref>[[#Campbell2|Campbell (2005)]], p. 10.</ref>

===Filming===
[[File:Los Angeles, CA from the air.jpg|alt=An aerial view of a widespread built-up area, skyscrapers in the central district, with mountains in the background.|"Terms of Endearment" was filmed in Los Angeles, as were the rest of the episodes of the sixth season.|thumb|right]]

The first five seasons of the series were mainly filmed in Vancouver, British Columbia, and production of the show's sixth season was based in Los Angeles, California.<ref>{{cite video |people=[[Chris Carter (screenwriter)|Carter, Chris]], Rabwin, Paul and [[Kim Manners|Manners, Kim]] |year=2000 |title=The Truth About Season Six |medium= DVD |publisher=[[20th Century Fox Home Entertainment]]}}</ref><ref>{{cite journal|last=Vitaris|first=Paula|title=X-Files: A Mixed Bag of Episodes and a Feature Film Pave the Way for Season Six|journal=Cinefantastique|date=October 1998|volume=30|issue=7/8|page=27}}</ref> The principal outdoor filming for "Terms of Endearment" took place in and around Pasadena, called "the most East Coast-like part of the Los Angeles metropolitan area" by Andy Meisler in his book ''The End and the Beginning''.<ref name="m79"/> The car featured in the episode was a [[Chevrolet Camaro (second generation)|Chevrolet Camaro Z28]] convertible. Meisler sardonically wrote that [[General Motors]] had "no qualms about seeing their vehicles driven onscreen by a relative of [[Satan]]".<ref name="m79"/>

Several of the special effects used in the episode were created in a "low-stress" manner that did not rely wholly on [[Computer-generated imagery]]. During the childbirth scene, gas-burners were set a distance away from a fire-proof bed. The scene was then filmed with a long lens to give the effect that the fire was mere inches away from the bed.<ref name="m79"/> Producer [[John Shiban]] said that the film crew made "a big deal out of the eyes" to make the scene frightening.<ref name="sfx">{{cite video |people=[[Paul Rabwin]] |year=1999 |title=Special Effects with Paul Rabwin: Devil/Fire Comp |location=''[[The X-Files (season 6)|The X-Files: The Complete Sixth Season]]'' |medium= DVD }}</ref> The devil [[Medical ultrasonography|sonogram]] was created by using the videotape of a real sonogram of a crew member's wife. The videotape was then edited to give it a demonic look.<ref name="m79"/>

The burnt baby skeleton was built from scratch. Originally, the crew had planned on renting a real fetal skeleton, but the $3,000 cost forced them to make their own. Office manager Donovan Brown noted that, "we got two or three of those adults skeleton models, cut a foot or so off a leg here and shortened an arm there, glued them together to a plaster model of a fetal skull we found, and put together something that worked wonderfully".<ref name="m79"/>

==Themes==
{{quote box| align  = right| width=28em|bgcolor=#c6dbf7|Rather than playing it over the top as a devil incarnate, he [Campbell]  played Wayne as a man with a dream. One that has driven him across the world to try and achieve it no matter what lengths he must go to in order to fulfill it. |source=—Writer Tom Kessenich on the unique presentation of the antagonist.<ref name="messy"/>}}

A main theme in the episode is the horror of child birth.<ref name="avclub"/> Amann describes the episode as an inversion of the 1968 horror film ''Rosemary's Baby'' which is about a woman scared of giving birth to a demonic baby.<ref name="m79"/><ref name="avclub"/> As with many other episodes of the series, "Terms of Endearment" is heavily influenced by horror films, and features gothic imagery.<ref name="PopLit">[[#Delasara|Delasara (2000)]], p. 175.</ref><ref>[[#Delasara|Delasara (2000)]], p. 177.</ref> In addition to ''Rosemary's Baby'' and other [[Roman Polanski]] films, the episode shows stylistic references to the 1972 film ''[[The Exorcist (film)|The Exorcist]]'' and the 1981 film ''[[The Evil Dead]]''.<ref name="avclub"/><ref>[[#Kessenich|Kessenich (2002)]], p. 28.</ref><ref name="horror"/> The influence of the genre extends to the casting of Campbell, an actor unfamiliar to the mainstream public but with a prominent cult following among horror fans.<ref>[[#Carlson|Carlson (2004)]], p. 378.</ref><ref name="EVIL2">[[#Kenneth Muir|Kenneth Muir (2004)]], p. 305.</ref>

The main antagonist of "Terms of Endearment", Weinsider, is a child-murdering demon.<ref name="avclub"/> However, the episode plays against genre archetypes by turning Weinsider into a sympathetic villain.<ref name="messy2"/><ref name="sixth"/> Critics have pointed out that the character's presentation was not entirely negative.<ref name="sixth"/> Some have commented that Campbell humanized the character, portraying him in a manner that adds likability to a character who could have been more sinister.<ref name="avclub"/><ref name="sixth"/> By the time Weinsider is defeated, the audience is led to partially identify with him.<ref name="avclub"/> He ultimately sacrifices himself to save the life of his wife, showcasing heroic qualities and subverting the way that villains are often portrayed in the genre.<ref name="messy2"/>

==Broadcast and reception==

===Initial ratings and reception===
"Terms of Endearment" originally aired on the Fox network on January 3, 1999,<ref name="DVD">{{cite DVD notes |title=The X-Files: The Complete Sixth Season |titlelink=The X-Files (season 6) |origyear=1998–1999 |others=[[Kim Manners]], et al. |type=booklet |publisher=Fox Broadcasting Company}}</ref><ref>[[#Soter|Soter (2001)]], p. 213.</ref> with a tagline of "Born to raise hell. Tonight, something terrifying is about to be born".<ref>{{cite sign |title= Terms of Endearment|year=1999 |type=Promotional Flyer |publisher=[[Fox Broadcasting Company]] |location=Los Angeles|url=http://i550.photobucket.com/albums/ii421/maurisap/xfiles%20forum/94borntoraisehell.jpg}}</ref>  It earned a [[Nielsen rating]] of 10.5, with a 15 share, meaning that roughly 10.5 percent of all television-equipped households, and 15 percent of households watching television, were tuned in to the episode, which was viewed by 18.70 million viewers.<ref name="ratings">[[#Meisler|Meisler (2000)]], p. 294.</ref> It later aired in the United Kingdom and Ireland on [[Sky1]] on April 18, 1999, and received 0.62 million viewers, making it the eighth most watched television show that week.<ref name="barb2">{{cite web |url=http://www.barb.co.uk/viewing/weekly-top-10/? |title=BARB's multichannel top 10 programmes |publisher=[[Broadcasters' Audience Research Board]] |accessdate=1 January 2012}} Note: Information is in the section titled "w/e April 12–18, 1999", listed under [[Sky1]]</ref>

Upon its first broadcast, the episode received mixed reviews from critics. Michael Liedtke and George Avalos, in a review of the sixth season in ''[[The Charlotte Observer]]'', called the episode "just plain bad."<ref>{{cite news|last=Liedtke|first=Michael|title=Despite a Return to Sappiness, X-Files Works|newspaper=[[The Charlotte Observer]] |date=31 May 1999|author2=George Avalos|pages=6E}}</ref> [[Sarah Stegall]] awarded the episode two stars out of five, positively comparing it to the work of Roman Polanski, but criticizing its failure to deliver truly scary material.<ref name=stegall/> Stegall mused that "Terms of Endearment" lacked the edgy writing of some of the best episodes of the series, though she did note that Campbell "turns in a good performance with mediocre material".<ref name="stegall">{{cite web|last=Stegall|first=Sarah|title=Sympathy for the Devil|url=http://www.munchkyn.com/xf-rvws/endearment.html|publisher=The Munchkyn Zone|accessdate=14 September 2012|year=1999}}</ref> She also criticized the depiction of religious material, comparing it negatively to other episodes with religious themes including "[[Revelations (The X-Files)|Revelations]]" and "[[Miracle Man (The X-Files)|Miracle Men]]".<ref name=stegall/>

Paula Vitaris from ''[[Cinefantastique]]'' gave the episode a mixed review and awarded it two stars out of four, calling it a disappointment.<ref name="cinepaula">{{cite journal|last=Vitaris|first=Paula|title=Sixth Season Episode Guide|journal=Cinefantastique|date=October 1999|volume=31|issue=8|pages=26–42}}</ref> Vitaris heavily criticized Mulder's line: "I'm not a psychologist"—reportedly an ad lib from Duchovny himself—noting that it undermines established continuity within the series including the character's background in psychology.<ref name=cinepaula/> Tom Kessenich, in his 2002 book ''Examination: An Unauthorized Look at Seasons 6–9 of the X-Files'', praises the installment, saying, "Place a devil in the plot and I'll follow you to see what you can cook up.&nbsp;... I enjoyed this tale of a devil looking to be just a normal dad immensely."<ref name="messy2">[[#Kessenich|Kessenich (2002)]], p. 27.</ref> In his view the episode showed a return to the earlier horror-based narratives the series was known for; though he praises the casting of Campbell, he writes that moments between Mulder and Scully are lacking.<ref name="messy">[[#Kessenich|Kessenich (2002)]], pp.&nbsp;29–30.</ref>

===Later reception===
[[File:Bruce Campbell 2011.jpg|alt=A man standing with a microphone, wearing a red blazer.|[[Bruce Campbell]] received positive reviews for his role as the demonic Weinsider. Several critics felt that he brought humanity to the character. |upright|thumb|right]]
In the years following "Terms of Endearment"'s original broadcast, critical reception improved. Robert Shearman and Lars Pearson, in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', gave three stars out of five, describing it as "a very brave story".<ref name=shear/> Shearman and Pearson felt that the episode suffered from its attempt to balance comedy and horror, "not having enough comedy to explore the premise properly, and not enough suspense to provide much drama". However, both Persky and Campbell were praised for their acting, despite the perceived shallowness of the latter's character.<ref name=shear>[[#Shearman|Shearman and Pearson]], pp. 173–174.</ref> David Wharton of Cinema Blend called Campbell's performance "lowkey" and subtle, commenting that he portrayed Weinsider "a bit less madcap than many of the roles that he's known for".<ref name="BLEND"/> Wharton praised the casting, describing it as "against type", which he claimed worked well because the material of the episode was more serious and dramatic than usual for the series.<ref name="BLEND">{{cite web | author =Wharton, David |url=http://www.cinemablend.com/dvdnews/FlixWorthy-Celebrates-Ash-Wednesday-With-Best-Streaming-Bruce-Campbell-30581.html |title=FlixWorthy Celebrates Ash Wednesday With The Best Of Streaming Bruce Campbell |work=Cinema Blend |publisher=Joshua Tyler |date=9 March 2011 |accessdate=28 November 2011}}</ref>

Todd VanDerWerff from ''[[The A.V. Club]]'' gave the episode a positive review and awarded it a "B". He praised Campbell's acting, calling him "the best thing about" the installment, and complimented the entry's general concept.<ref name="avclub"/> VanDerWerff stated that after several humorous stories in a row, "Terms of Endearment" was a "return to form" for the series, bringing the season back to the more straightforward monster-of-the-week format.<ref name="avclub"/> VanDerWerff did, however, note that the episode's biggest weaknesses were its limited use of Scully and its over-the-top use of Spender as a villain.<ref name="avclub">{{cite web|last=Todd|first=VanDerWerff|title='Terms of Endearment'/'Through a Glass Darkly'|url=http://www.avclub.com/articles/terms-of-endearmentthrough-a-glass-darkly,82659/|work=[[The A.V. Club]] |accessdate=22 July 2012|date=21 July 2012}}</ref> Another critic from ''The A.V. Club'', Zack Handlen, commented that "Terms of Endearment" was notably more influenced by horror than the following week's "[[The Rain King]]".<ref name="horror">{{cite web|last=Handeln|first=Zack|title='The Rain King'/'Human Essence' {{!}} The X-Files/Millennium {{!}} TV Club {{!}} TV|url=http://www.avclub.com/articles/the-rain-kinghuman-essence,82750/|publisher=The Onion|work=The A.V. Club|date=28 July 2012|accessdate=28 July 2012}}</ref> Edward Olivier of The Celebrity Cafe stated that the installment strayed away from regular ''X-Files'' formula, showcasing Campbell in a "major role".<ref name="CAFE">{{cite web | author =Olivier, Edward |url=http://thecelebritycafe.com/movies/full_review/12231.html |title=The X-Files – The Complete Sixth Season |work=The Celebrity Cafe |date=14 June 2007 |accessdate=27 November 2009}}</ref>

Retrospective reviews of "Terms of Endearment" with regards to the series as a whole were mixed. In a run-down of ''The X-Files'' guest stars who left a lasting impression, Lana Berkowitz from the ''[[Houston Chronicle]]'' included Campbell, calling him the "demon who wants to be a father."<ref>{{cite news|last=Berkowitz|first=Lana|title='The X-Files' guest stars left a lasting impression|newspaper=[[Houston Chronicle]]|date=17 May 2002|page=10}}</ref> Christine Seghers of ''[[IGN]]'' described the entry as a "creepy standout" from the sixth season, and named Campbell's guest appearance as the sixth best of the series.<ref name="sixth"/> Campbell's performance was called "moving" by Seghers, who viewed that Campbell managed to deliver a performance that "completely shed his trademark snark".<ref name="sixth">{{cite web | author =Seghers, Christine |url=http://ca.ign.com/articles/2008/07/18/top-10-x-files-guest-stars?page=3 |title=Top 10 X-Files Guest Stars |work=[[IGN]]|date=17 July 2008 |accessdate=11 August 2011}}</ref> ''Cinefantastique'' later named the dream sequence from "Terms of Endearment" as the ninth scariest moment in ''The X-Files''.<ref>{{cite journal|last=Anderson|first=Kaite|title=The Ten Scariest Moments|journal=Cinefantastique|date=April 2002|volume=34|issue=2|pages=50–51}}</ref> Andrew Payne from ''Starpulse'' cited the episode as the second most disappointing of the series, calling the premise "lame".<ref name="pulse"/> Payne stated that only "[[Chinga (The X-Files)|Chinga]]", an episode written by author [[Stephen King]], wasted its potential more.<ref name="pulse">{{cite web|last=Payne|first=Andrew|title='X-Files' 10 Best Episodes|url=http://www.starpulse.com/news/Andrew_Payne/2008/07/25/x_files_10_best_episodes|work=Starpulse |date=25 July 2008|accessdate=16 November 2011}}</ref>

==Impact==
Following his involvement with this episode, Campbell was considered as a possible contender for the role of [[John Doggett]], a character that would appear in the [[The X-Files (season 8)|eighth season]].<ref name="stiffed"/><ref name="IGN"/> Over a hundred actors auditioned for the role.<ref name="IGN"/><ref name=doggettjohn>{{cite web | first1= Michael |last1= Fleming |first2= Josef |last2= Adalian |url=http://www.variety.com/article/VR1117784003?refCatId=14 |title=Patrick marks 'X-Files' spot |work=[[Variety (magazine)|Variety]]|date=20 July 2000 |accessdate=27 November 2009}}</ref> Due to a contractual obligation, Campbell could not take any work during the filming of his series ''[[Jack of All Trades (TV series)|Jack of All Trades]]''.<ref name="IGN"/> On potentially being cast as the series regular, Campbell mused, "I had worked on an ''X-Files'' episode before, and I think they sort of remembered me from that. It was nice to be involved in that – even if you don't get it, it's nice to hang out at that party."<ref name=IGN>{{cite web | author = Ken P.|url=http://ca.ign.com/articles/2002/12/18/an-interview-with-bruce-campbell?page=3 |title=An Interview with Bruce Campbell |work=[[IGN]] |date=18 December 2002 |accessdate=27 November 2009}}</ref> The character was eventually portrayed by actor [[Robert Patrick]].<ref>{{cite video |people=[[Kim Manners|Manners, Kim]] and [[Robert Patrick|Patrick, Robert]] |date=2001 |title=Audio Commentary for "Within" |medium= DVD |publisher=20th Century Fox Home Entertainment}}</ref> Later in Campbell's novel ''[[Make Love! The Bruce Campbell Way]]'', he joked that Patrick "stiffed him out of the role".<ref name="stiffed">[[#Campbell2|Campbell (2005)]], p. 9.</ref>

"Terms of Endearment" was the first entry of the series written by Amann, who beforehand had unsuccessfully pitched several ideas for the show.<ref name="plot"/> Based on the success of this episode, Amann went on to write several more episodes for the series, such as "[[Agua Mala]]" later in the same season.<ref name="plot"/> During the [[The X-Files (season 9)|ninth season]], Amann became one of the main supervising producers and had writing involvement in several episodes, most notably "[[Release (The X-Files)|Release]]" and "[[Hellbound (The X-Files)|Hellbound]]".<ref name="plot"/><ref>{{cite video |people=[[Chris Carter (screenwriter)|Chris Carter]] |year=2002 |title=The Truth Behind Season 9: "The Truth" |medium= DVD |location = ''[[The X-Files (season 9)|The X-Files: The Complete Ninth Season]]''|publisher=[[20th Century Fox Home Entertainment]]|display-authors=etal}}</ref>

Chris Owens, who portrayed Jeffrey Spender on the show, was negatively affected by the episode.<ref>[[#Meisler|Meisler (2000)]], p. 284.</ref><ref name="Spender"/> Following the premiere of "Terms of Endearment", he received "strange reactions" from people on the street who were displeased with his character.<ref name="Spender">[[#Meisler|Meisler (2000)]], p. 150.</ref> In the series, the character staunchly disbelieves in the paranormal, and tries to remove the initial report about child-abducting demons in the episode, only for it to be salvaged by Mulder and Scully.<ref name="plot"/> People were so annoyed by the nature of the character that Owens was pestered during his everyday life; during one incident, a person angrily called him a "paper shredder".<ref name="Spender"/>

==Notes==
{{reflist|colwidth=30em}}

==References==
{{refbegin|30em}}
* {{Cite book
| year = 2002
| first = M. Keith
| last = Booker
| title = Strange TV: Innovative Television Series from The Twilight Zone to The X-Files
| publisher = [[Greenwood Publishing Group]]
| ref = Booker
| isbn = 9780313323737}}
* {{cite book
| year = 2002
| last = Campbell
| first = Bruce
| title = If Chins Could Kill: Confessions of a B Movie Actor
| publisher = L.A. Weekly Books
| isbn = 9780312291457
| ref = Campbell}}
* {{Cite book
| year = 2005
| first = Bruce
| last = Campbell
| title = [[Make Love! The Bruce Campbell Way]]
| publisher = St. Martin's Press
| isbn = 9780312312602
| ref = Campbell2}}
* {{cite book
| year = 2004
| first = Jae
| last = Carlson
| title = The Scarecrow Video Movie Guide
| publisher = Sasquatch Books
| isbn = 9781570614156
| ref = Carlson}}
* {{Cite book
| year = 2000
| first = Jan
| last = Delasara
| title = X-Files Confidential
| publisher = PopLit, PopCult and The X-Files: A Critical Exploration
| isbn = 9780786407897
| ref = Delasara}}
* {{Cite book
| year = 2008
| first1 = Matt
| last1 = Hurwitz
| first2 = Chris
| last2 = Knowles
| title = The Complete X-Files
| publisher = Insight Editions
| isbn =  9781933784809
| ref = Hurwitz}}
* {{Cite book
| year = 2004
| first = John
| last = Kenneth Muir
| title = The Unseen Force: the Films of Sam Raimi
| publisher = Applause Theatre & Cinema Books
| isbn = 9781557836076
| ref = Kenneth Muir}}
* {{cite book
| year = 2002
| last = Kessenich
| first = Tom
| title = Examination: An Unauthorized Look at Seasons 6–9 of the X-Files
| publisher = Trafford Publishing
| isbn = 9781553698128
| ref = Kessenich}}
* {{Cite book
|year = 1995
|first = Brian
|last = Lowry
|title = The Truth is Out There: The Official Guide to the X-Files
|publisher = Harper Prism 5
|isbn = 9780061053306
| ref = Lowry}}
* {{cite book
| year = 2000
| last = Meisler
| first = Andy
| title = The End and the Beginning: The Official Guide to the X-Files Season 6
|publisher = HarperCollins
| isbn = 9780061075957
| ref = Meisler}}
* {{cite book
| last = Owen
| first = Alex
| title = The Place of Enchantment: British Occultism and the Culture of the Modern
| publisher = University of Chicago Press
| isbn = 9780226642048
| ref = Owen}}
* {{cite book
| year = 2009
| first1 = Robert
| last1 = Shearman
| first2 = Lars
| last2 = Pearson
| title = Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen
| publisher = [[Mad Norwegian Press]]
| isbn = 9780975944691
| ref = Shearman}}
* {{Cite book
| year = 2001
| first = Tom
| last = Soter
| title = Investigating Couples: A Critical Analysis of the Thin Man, the Avengers and the X-Files
| publisher = McFarland & Company
| isbn = 9780786411238
| ref = Soter}}
{{refend}}

==External links==
{{wikiquote|The_X-Files|The X-Files}}
* {{IMDb episode|0751217|Terms of Endearment}}
* [http://www.tv.com/shows/the-xfiles/terms-of-endearment-614/ "Terms of Endearment"] at [[TV.com]]

{{TXF episodes|6}}

{{featured article}}

[[Category:1999 American television episodes]]
[[Category:Demons in television]]
[[Category:The X-Files (season 6) episodes]]
[[Category:Virginia in fiction]]