{{Good Article}}
{{Use dmy dates|date=February 2017}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:Brummer-class cruiser.jpg|300px]]
|Ship caption=One of the ''Brummer''-class cruisers, probably on the way to Scapa Flow
}}
{{Infobox ship career
|Hide header=
|Ship country=[[German Empire]]
|Ship flag={{shipboxflag|German Empire|naval}}
|Ship name=SMS ''Bremse''
|Ship builder=[[AG Vulcan Stettin]], [[Stettin]]
|Ship laid down=1915
|Ship launched=11 March 1916
|Ship commissioned=1 July 1916
|Ship fate=[[Scuttling of the German fleet in Scapa Flow|Scuttled in Scapa Flow]] on 21 June 1919
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Brummer|cruiser|0}} [[light cruiser]]
|Ship displacement=*Design: {{convert|4385|MT|abbr=on}}
*Full load: {{convert|5856|MT|abbr=on}}
|Ship length= {{convert|140.4|m|ftin|abbr=on}}
|Ship beam={{convert|13.2|m|ftin|abbr=on}}
|Ship draft={{convert|6|m|ftin|abbr=on}}
|Ship propulsion=2 shaft [[steam turbines]], 6 boilers, {{convert|33000|shp|abbr=on}}
|Ship speed={{convert|28|kn|abbr=on}}
|Ship range={{convert|5800|nmi|abbr=on}} at {{convert|12|kn|abbr=on}}
|Ship complement=*16 officers
*293 enlisted men
|Ship armament=*4 × [[15 cm SK L/45|15&nbsp;cm SK L/45]] guns
*2 ×  {{convert|8.8|cm|in|abbr=on}} L/45 AA guns
*2 × {{convert|50|cm|in|abbr=on}} torpedo tubes
*400 mines
|Ship armor=*Belt: {{convert|40|mm|in|abbr=on}}
*Deck: {{convert|15|mm|in|abbr=on}}
*Conning tower: {{convert|100|mm|in|abbr=on}}
}}
|}

'''SMS ''Bremse''''' was a {{sclass-|Brummer|cruiser|0}} [[minelayer|minelaying]] [[light cruiser]] of the [[German Empire|German]] [[Kaiserliche Marine]]. She was built by [[AG Vulcan Stettin]] in 1915 and launched on 11 March 1916 at [[Stettin]], [[Germany]], the second of the two-ship class after her [[Sister ship|sister]], {{SMS|Brummer}}. She served during the [[First World War]], operating for most of the time in company with her sister. The two ships took part in an ambush on a convoy in the [[North Sea]], where they sank two destroyers in a surprise attack, before hunting down and sinking nine merchantmen, after which they returned to port unscathed.

The ''Kaiserliche Marine'' considered sending the two ships to attack convoys in the Atlantic Ocean, but the difficulties associated with refueling at sea convinced the Germans to abandon the plan. ''Bremse'' was one of the ships interned at [[Scapa Flow]] under the terms of the [[Armistice with Germany|armistice]] in November 1918. On 21 June 1919, the commander of the interned fleet, Rear Admiral [[Ludwig von Reuter]], ordered the [[Scuttling of the German fleet in Scapa Flow|scuttling of the fleet]]. She was salvaged in 1929 by teams working for [[Ernest Cox]], though they had to contend with large quantities of oil and the risks of fires and explosions. Having been brought back to the surface after a decade underwater, she was then scrapped.

==Construction==
{{main|Brummer-class cruiser}}
''Bremse'' was ordered under the contract name "D" and laid down at the [[AG Vulcan]] shipyard in [[Stettin]] in 1915.<ref name=G112>Gröner, p. 112</ref> She was launched on 11 March 1916, after which [[fitting-out]] work commenced. Completed in less than four months, she was commissioned into the [[High Seas Fleet]] on 1 July 1916.<ref name=G113>Gröner, p. 113</ref> The ship was {{convert|140.4|m|sp=us}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|13.2|m|abbr=on}} and a [[draft (hull)|draft]] of {{convert|6|m|abbr=on}} forward. She displaced {{convert|5856|MT|abbr=on}} at full combat load. Her propulsion system consisted of two sets of [[steam turbine]]s powered by two coal-fired and four oil-fired Marine-type boilers. These provided a top speed of {{convert|28|kn|lk=in}} and a range of {{convert|5800|nmi|lk=in}} at {{convert|12|kn|abbr=on}}.<ref name=G112/> In service however, the ship reached {{convert|34|kn|abbr=on}}.<ref name=M747>Massey, p. 747</ref>

The ship was armed with four [[15 cm SK L/45|15&nbsp;cm SK L/45]] guns in single pedestal mounts; two were arranged side by side forward and two were placed in a [[superfire|superfiring pair]] aft.<ref name=C162>Gardiner & Gray, p. 162</ref> These guns fired a {{convert|45.3|lb|kg|adj=on}} shell at a [[muzzle velocity]] of {{convert|840|m/s|sp=us}}. The guns had a maximum elevation of 30&nbsp;degrees, which allowed them to engage targets out to {{convert|17600|m|abbr=on}}.<ref>Gardiner & Gray, p. 140</ref> They were supplied with 600 rounds of ammunition, for 150 shells per gun. ''Brummer'' also carried two {{convert|8.8|cm|abbr=on}} L/45 anti-aircraft guns mounted on the [[centerline (ship marking)|centerline]] astern of the funnels. She was also equipped with a pair of {{convert|50|cm|abbr=on}} [[torpedo tube]]s with four torpedoes in a swivel mount [[amidships]]. Designed as a minelayer, she carried 400 [[naval mine|mines]]. The ship was protected by a waterline [[armored belt]] that was {{convert|40|mm|abbr=on}} thick amidships. The [[conning tower]] had {{convert|100|mm|abbr=on}} thick sides, and the deck was covered with {{convert|15|mm|abbr=on}} thick armor plate.<ref name=G112/>

==Career==
Over the period 11–20 October 1916, ''Bremse'' and ''Brummer'' served with the High Seas Fleet in the [[North Sea]]. On 10 January 1917, the two ships laid a minefield off [[Norderney]]. They escorted [[Minesweeper (ship)|minesweepers]] on 1–13 March based in [[Emden]] and [[Wilhelmshaven]].<ref name=N188>Novik, p. 188</ref> Their first major offensive operation was an attack on a British convoy in October 1917.<ref>Herwig, p. 229</ref> Britain had agreed to ship {{convert|250000|MT|abbr=on}} tons of coal per month to Norway, and a regular stream of convoys carrying shipments of coal was crossing the North Sea by late 1917. These were usually weakly escorted by only a couple of destroyers and [[armed trawler]]s.<ref name="COS">Massie, p. 747</ref> Attempts to interdict them with [[U-boat]]s had to that point been ineffective, so Admiral [[Reinhard Scheer]], the chief of the ''[[Admiralstab]]'', decided to deploy a surface force to carry out a surprise attack to supplement the [[U-boat Campaign (World War I)|U-boat campaign]]. In addition to damaging British shipping, Scheer sought to divert escorts from the Atlantic theater, where his [[U-boat]]s were concentrated. ''Bremse'', commanded by ''[[Fregattenkapitän]]'' Westerkamp, and ''Brummer'', commanded by ''Fregattenkapitän'' Leonhardi, were selected for the first such operation. Their high speed and large radius of action, coupled with their resemblance to British light cruisers, made them suited to the task. In preparation for the raid, their crews painted the ships dark gray to further camouflage them as British vessels.<ref name=H376>Halpern, p. 376</ref>

Half an hour after dawn on the morning of 17 October, ''Brummer'' and ''Bremse'' attacked a westbound convoy about {{convert|70|nmi|lk=in}} east of [[Lerwick]]. The convoy consisted of twelve [[merchant vessel|merchantmen]] and was escorted by the destroyers {{HMS|Strongbow|1916|6}} and {{HMS|Mary Rose|1915|2}} and a pair of armed [[Naval trawler|trawler]]s which had departed from [[Bergen]].<ref name=H376/> At dawn lookouts aboard ''Stronghold'' reported two unidentified ships closing on the convoy. Mistaking them for British cruisers ''Strongbow'' flashed recognition signals, but was suddenly fired upon at a range of {{convert|2700|m|abbr=on}} by a barrage of 15&nbsp;cm shells.<ref name="COS"/> ''Mary Rose'' tried to come to her assistance but was also hit; both ships were quickly sunk. ''Brummer'' and ''Bremse'' then turned their attention to the convoy, hunting down and sinking nine of the merchantmen, before returning to port.<ref name="COS"/> One of the armed trawlers, the ''Elise'', was fired on by ''Bremse'' while attempting to pick up survivors.<ref name="CN114">Booth, p. 114</ref> None of the ships were able to send a wireless report, and despite having a squadron of sixteen light cruisers at sea to the south of the convoy, the British did not learn of the attack until 16:00, when it was too late. Admiral [[David Beatty, 1st Earl Beatty|David Beatty]] said of the action that 'luck was against us.'<ref name="COS"/> The Admiralty responded to the raid by adding more and bigger escorts.<ref name="CN114"/>

Late in the war, the ''Admiralstab'' considered sending ''Brummer'' and ''Bremse'' on a commerce raiding mission into the Atlantic. They were to operate off the [[Azores]] in concert with an [[Replenishment oiler|oiler]]. The central Atlantic was out of the normal range of the U-boats, and convoys were therefore lightly defended in the area. The ''Admiralstab'' canceled the plan, however, after it was determined that refueling at sea would be too difficult. Another problem was the tendency of the two ships to emit clouds of red sparks when steaming at speeds over {{convert|20|kn|abbr=on}}; this would hamper their ability to evade Allied ships at night.<ref>Woodward, p. 93</ref> On 2 April 1918, ''Bremse'' laid a minefield consisting of 304 mines in the North Sea. She laid another 150 mines in the same area on 11 April. ''Bremse'' and her sister ended the month with a fleet sortie with the rest of the battle fleet on 22–24 April. On 11 May, ''Bremse'' laid another minefield in the North Sea with 400 mines. Three days later, she laid another 420 mines in the North Sea.<ref>Novik, p. 189</ref> She was to have been part of the [[Naval order of 24 October 1918|final sortie]] of the High Seas Fleet in October 1918, but the operation was cancelled due to the outbreak of mutiny in the German Fleet.

===Internment and scuttling===
[[File:Entering Scapa Flow.jpg|thumb|right|Ships of the German [[High Seas Fleet]] sailing to be interned. Visible are {{SMS|Emden|1916|6}}, {{SMS|Frankfurt}}, and SMS ''Bremse''.]]

Along with the most modern units of the High Seas Fleet, ''Brummer'' and ''Bremse'' were included in the ships specified for internment at [[Scapa Flow]] by the victorious [[Allies of World War I|Allied powers]]. The ships steamed out of Germany on 21 November 1918 in single file, commanded by Rear Admiral [[Ludwig von Reuter]].<ref>Herwig, p. 254</ref> They were met at sea by a combined fleet of 370 British, American, and French warships. The fleet arrived in the [[Firth of Forth]] later that day, and between 25 and 27 November, they were escorted to [[Scapa Flow]]. Upon arrival, all wireless equipment was removed from the ships and the breech blocks of their heavy guns taken to prevent their use. Crews were reduced to minimum levels.<ref>Herwig, p. 255</ref>

The fleet remained in captivity during the negotiations that ultimately produced the [[Treaty of Versailles]]. Reuter believed that the British intended to seize the German ships on 21 June 1919, which was the deadline for Germany to have signed the peace treaty. Unaware that the deadline had been extended to the 23rd, Reuter [[Scuttling of the German fleet in Scapa Flow|ordered the ships to be sunk]] at the next opportunity. On the morning of 21 June, the British fleet left Scapa Flow to conduct training maneuvers, and at 11:20 Reuter transmitted the order to his ships.<ref>Herwig, p. 256</ref> An armed British naval party had attempted to board ''Bremse'' and close her bottom valves, but found that they were already below the rising waterline.<ref name="CN114"/> Instead they blasted off her anchor chains and she was taken in tow by a tug and the destroyer {{HMS|Venetia|1917|6}}, in an attempt to beach her before she sank.<ref name="CN114"/> They managed to run her bow onto the beach, south of [[Cava, Orkney|Cava]], but the steeply sloping approach meant that her stern settled in deeper water, and she rolled over and sank in {{convert|75|ft|abbr=on}} of water at 14:30, leaving her bow visible at low tide.<ref name=G113/><ref name="CN114"/><ref name="JJ">George, p. 87</ref>

==Salvage==
[[File:German gun from SMS Bremse - geograph.org.uk - 118942.jpg|thumb|right|[[15 cm SK L/45|15 cm naval gun]] salvaged from ''Bremse'' and displayed at Scapa Flow]]
Though the Admiralty arranged for some of the ships to be salvaged, most were left at the bottom of the sound until entrepreneur [[Ernest Cox]] bought the salvage rights and began to raise the remaining ships in the early 1920s. ''Bremse'' presented particular challenges. She had come to rest perched precariously on a rock, which sloped away dramatically, causing fears that she might slip off and sink in deeper water.<ref name="JJ"/> Cox's salvage team sealed her bulkheads and divided the hull into watertight compartments. The hull was patched up and an airlock fitted, but the team ran into difficulties with the large amount of oil which covered the wreck, more than had been found in any other of the ships salvaged previously.<ref name="JJ"/> A three-man team using [[Oxy-fuel welding and cutting|oxyacetylene torches]] ignited some oil, causing an explosion. The men escaped without serious injuries, and thereafter small explosions and fires were common over the two months it took to prepare the ship, though no one was injured.<ref name="JJ88">George, p. 88</ref>

By July 1929 the last of the superstructure had been cleared, and ''Bremse'' was turned upside down using techniques developed on salvaging some of the destroyers. Compressors were then used to pump air into the hull and bring her to the surface, while she was supported by 9-inch wires attached to two floating docks anchored on her port shoreward side.<ref name="CN115">Booth, p. 115</ref> The salvage teams had almost raised her when she suddenly toppled onto her side and then heeled over gradually during the night, settling onto the rocks inshore.<ref name="JJ88"/>

It was thought that the failure had been caused by there being too much remaining superstructure, and attempts were made to clean out the large quantity of oil that had spilled out during the attempt to raise her.  The decision was made to burn off the oil, but the fire spread and had to be brought back under control.<ref name="JJ88"/> She was again patched up and pumped with air, breaking the surface on 29 November. The ''Bremse'' was eventually considered too unsafe to tow to [[Rosyth]] for scrapping, as had been done with the other ships Cox had salvaged, and instead she was taken to [[Lyness]] on 30 November 1929 and broken up there.<ref name="JJ88"/>

==Notes==
{{Reflist|colwidth=20em}}

==References==
*{{cite book |last =Booth| first = Tony|title = Cox's Navy: Salvaging the German High Seas Fleet at Scapa Flow 1924–1931| publisher =Pen and Sword Maritime|location=Barnsley|year = 2008|isbn=978-1-84415-181-3}}
*{{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1922|year=1984|location=Annapolis|publisher=Naval Institute Press|isbn=0-87021-907-3}}
*{{cite book |last =George| first = S. C.|title = Jutland to Junkyard: The Raising of the Scuttled German High Seas Fleet from Scapa Flow – The Greatest Salvage Operation of All Time| publisher =Birlinn|location=Edinburgh|year = 1999|isbn=1-84158-001-5}}
*{{Cite book |last=Gröner|first=Erich|title=German Warships: 1815–1945|year=1990|location=Annapolis|publisher=Naval Institute Press|isbn=0-87021-790-9}}
*{{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis|publisher=Naval Institute Press|isbn=1-55750-352-4}}
*{{Cite book |last=Herwig|first=Holger|title="Luxury" Fleet: The Imperial German Navy 1888–1918 |year=1980|location=Amherst, New York|publisher=Humanity Books|isbn=1-57392-286-2|oclc=}}
*{{cite book |last =Ireland| first = Bernard|title = The Illustrated Guide to Cruisers| publisher =Hermes House|location=London|year = 2008|isbn=978-1-84681-150-0}}
*{{cite book |last=Massie|first=Robert K.|authorlink=Robert K. Massie|title=Castles of Steel|year=2003|location=New York City|publisher=Ballantine Books|isbn=0-345-40878-0}}
* {{Cite journal |last=Novik|first=Anton|date= |year=1969|month= |title=The Story of the Cruisers Brummer and Bremse|journal=Warship International |volume=3 |pages=185–189|publisher=[[International Naval Research Organization]] }}
*{{cite book |last=Woodward|first=David|title=The Collapse of Power: Mutiny in the High Seas Fleet|year=1973|publisher=Arthur Barker Ltd|isbn=0-213-16431-0|location=London}}

{{Brummer class cruisers}}
{{1919 shipwrecks}}

{{DEFAULTSORT:Bremse}}
[[Category:Brummer-class cruisers]]
[[Category:Ships built in Stettin]]
[[Category:1916 ships]]
[[Category:World War I cruisers of Germany]]
[[Category:World War I minelayers of Germany]]
[[Category:World War I warships scuttled in Scapa Flow]]
[[Category:Maritime incidents in 1919]]