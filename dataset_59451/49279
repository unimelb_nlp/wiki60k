{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{more footnotes|date=August 2014}}
Poesy Liang ({{zh|s=梁小诗|t=梁小詩|p=Liáng xiǎoshī}}, born 31 July 1975) is a Taiwanese-Malaysian interdisciplinary artist - writer, poet, composer, multidisciplinary designer, jeweller, design entrepreneur, humanitarian and motivational speaker who survived 3 spinal surgeries and battled paralysis twice caused by a rare benign tumour in her spinal cord ([[thoracic vertebrae|thoracic]] [[spinal tumor|intradural]] [[meningioma]]). She is a Founder of [[Helping Angels]] group a movement of compassion based on random kindness with main objectives is to motivate career professionals and volunteer to use their wisdom, influence, skills and time resources to help others. 

== Health history ==

Poesy first suffered from paraplegia at 17 (1992) due to acute thoracic intradural meningioma. The first surgery to remove the cancers was done in [[Kuala Lumpur General Hospital]] by neurosurgeons Mr. Chee and Dr. Selva. She walked again and enjoyed a miraculous recovery – regaining 90% of her strength, she was ballroom dancing and back on television in 1994.

The same conditions recurred in Poesy's spine when she was 28 (2003) with the mass of tumours coiled around her spinal cord removed by neurosurgeon Dr. Richard Veerapen in [[Sunway Medical Centre]]. Major segments of the tumors were successfully extracted with some inevitable damage to her spinal cord, leaving behind some significant tumours that were impossible to remove. Poesy was paralysed for the second time, but another miracle followed. Despite major loss of sensation and control, Poesy manages to walk using her visual senses with limited sensation of her lower limbs.

With the financial aid of her private circles, Poesy underwent a third medical procedure to tackle the rest of her spinal tumours in 2006. She was treated with a [[Cyberknife]] surgery in [[Stanford University]], by neurosurgeon Dr. [[John R. Adler]], Jr, the inventor of the medical technology. There has been no news of tumour movement and regrowth since.

== Helping Angels==

[[Helping Angels]] started as a Facebook group to recruit volunteers to do welfare work in August 2007. Projects born of the Helping Angels movement are supported by 4 exclusive rules – no involvement in fund raising or collection of donations, no commercial marketing activities, no political rallying activities, no religious evangelism allowed. The group started in Malaysia and rapidly recruited members globally. All activities are funded privately, to offer opportunities for volunteers to use their wisdom, influence, skills, creativity and time resources to help others. The movement of compassion based on the random acts of kindness, seeks to encourage the donation of time and efforts, placing less importance in material and money charity. Activities cover welfare of the underprivileged, education, homeless, old aged, animal rescue and rehoming, and life-coaching. To date the volunteer movement now exists in Malaysia, Bali, Thailand, Taiwan, USA, Europe, Africa and Hong Kong. During the 2011 Japan quake, the group was quick to respond with a project collaboration that distributed 160,000 pairs of socks sent by members from all over the world, to the stricken coastal villages. The story was told in the 2012 Helping Angels project Green Humanity, which showcases the need to do kind deeds towards Mother Earth.

[[Helping Angels#Thursday tutoring program|Thursday Tutoring Program]] was established since 27 September 2007 covering Precious Children's Home, Trinity Children Centre and Precious Youth Centre. Tutoring activities ceased with Precious Children's Home and Precious Youth Centre in 2012, marking the beginning of tutoring sessions with Agathian Shelter, also in PJ.
Every Thursday evenings, volunteering-members will clock in to give academic aid to the young residents of these welfare homes. All volunteering activities are led by unit leaders who donates their time to care for the project objectives and administration.

Other events includes a classical concert for young orphans in Taipei; Gratitude Letters a penpal program between HK City University broadcasting students and orphaned children from earthquake zones in China; Jazz for the Blind in San Francisco; Back2School supplies for 300+ marginalised children in rural Malaysia; art supplies and water for street children in Senegal; various trips to the cinema, theatre, concert shows for various shelter home children groups within Kuala Lumpur; and others.

Poesy also shaved her head for Bald Empathy Movement at the 64th Cannes Film Festival on May 16, 2011 and brought the project to 10 countries with collaborations with 10 international visual artists and photographers. She also composed the theme song for Helping Angels titled Treetops, produced in London by music director Godfrey Wang whom she met through Boney M.

Poesy announced retirement from Helping Angels in August 2015 but continues to do similar work in her personal capacity and with the arts.

Tuesday Art Angels (formally ChowKids Art Angels) is a community program that started in 2013 to nurture young artists amongst stateless children under the care of Yayasan Chow Kit in Kuala Lumpur, Malaysia. The artwork (usually pop-up Christmas cards) produced are sold to individuals and corporations. The young artists are remunerated by the pieces of artwork they produce following a grade system. The top grade pieces are curated for an exhibition, as a fundraiser for an education fund to support single mother children (Q3 2017). Regular grade pieces are sold to cover cost of activities, which also offers allowance to Malaysian artists participating in mentoring the young artists. High performers amongst the young artists are offered further mentorship. The artist mentors who work for the program gets first priority to opportunities to advance their art career.
The program is entering into the 5th year, Poesy has since extended the same opportunities to 2 other centres near her downtown neighbourhood in Kuala Lumpur, with the support of International Medical University. As the program continues to evolve with new elements , we look forward to see the exhibition which Poesy has been curating for the last 4 years to finally happen so she can graduate to the next step of setting up her endowment fund to support single mother children with their education.
For these young artists, the longer term goal is to develop their skills and offer further mentorship where fit, so they can be exposed to further advancements in the arts. In the event the chosen candidates for mentorship relocates to another locality, the program will reach out and look for well-suited artists who will agree to mentor the young artist where they are.

==Bald Empathy Movement==

Poesy shaved her head bald at the 64th Cannes Film Festival on 16 May 2011 to bring awareness to the following 5 objectives :

1. To make bald fashionable and cool, as an empathy movement for those suffering from loss of dignity due to premature hair-loss, and hair-loss due to chronic illnesses such as cancer.

2. To advocate wholesome lifestyles. Living well – emotionally, physically, mentally and spiritually. Encourage healthy attitude, diet, exercise, lifestyle and free of destructive or negative habits – by getting a deeper understanding of wholesome wellness vs chronic illnesses.

3. To raise sensitivity towards survivors and patients suffering from ill health. Learning awareness so we do not cause further stress to those who are battling health issues. E.g.; Don't bring germs or viruses (no matter how minor) to visit someone who is already fighting for their life. Don't take chances with other lives. Appreciate that ignorance can lead us to take many things in life such as health for granted. Don't impose secondhand smoke onto non-smokers.

4. Know that we can give our hair to cancer patients by getting the wigs made and giving it as a gesture of love and respect. Don't just give away the loose hair – make the wig before giving. Good wigs are expensive things. Here is an opportunity for you to give away something not just great, but good and from your heart. Lighten the financial load for others. This point is an awareness for generosity.

5. No hair, no shampoo, less water. Be mindful towards the environment.

A collection of international photographers and film makers are part of the crew to viral the movement on the internet to gain worldwide attention. Her parents and Matthias Gelber joined Poesy to shave their head in support and solidarity. In 2012, singer Elvira Arul and paralympian newscaster Ras Adiba also followed suit in shaving their head bald.

==Poesy's World==
A private online magazine where Poesy Liang documented her journals daily since 1999. The site has been closed to public consumption since 2005. Some of the memoirs are currently being published at the Poesy's World Facebook page.

==Her education==

Educated between 1982–1992 in [[Bukit Bintang Girls' School]]. Completed diploma studies of Architecture Engineering in Federal Institute of Technology (1992–1995), not certified. Poesy's health rapidly deteriorated and she was paraplegic by the end of her first semester in architectural school. Ceased her media careers and completed a Graduate Dip. Management and Executive MBA with Southern Cross University (1999–2001). Poesy read 1 year of law with the University of London (2002–2003). Poesy was diagnosed with the second bouts of spinal tumours in first year . In 2005, Poesy attended courses in Daylighting and Lighting Design in Lightfair International 2005 New York while she researched for medical solutions in America.

== Arts ==

Trained classically from six years old in carving [[Seal (East Asia)|Chinese stone seal]], [[Chinese calligraphy]], [[Shan shui|Chinese landscape painting]], [[Chinese painting|Chinese watercolours]], piano and ballet. One of her art teachers who taught her from childhood is Malaysian renown calligraphy master Dr. Wong Kum Peng.
During the 2011 Bald Empathy Movement tour, Poesy ran out of budget in Europe to continue her journey to the finale event in Cannes. For 2 months she funded the rest of the tour as a sidewalk artist in Paris and sold her White Cartoon Sidewalk Series to her international followers on Facebook. This prompted her to quit her interior design career afterwards to focus solely on art.
In 2015, Poesy injured her painting hand and funded her summer travels from Asia to Cannes, Basel and Venice with her Left Hand Art series, which she produced using her left hand.
Her work is multidisciplinary that covers painting in various mediums in the contemporary genre, designing objects/architecture/fashion/jewellery, creating scents, authoring literature, composing music, and animation films.

Her most significant claim to poetry :
<blockquote>
''Probability of 1 today = 1000 tomorrows. <br />
''Half a mistake a hundred sorrows. <br />
''Time consistently brims up choices. <br />
''To be made among clanging noises. <br />
''No sure win situation? <br />
''1% inspiration 99% perspiration.<br /> 
''Every angle lends a separate hue of day.<br /> 
''A colourless debt it cannot repay. <br />
''Visions improve with quiet reflection. <br />
''Complaints regrets define incomprehension. <br />
''Churn the secular knowledge one borrows. <br />
''Today's are infinite spectrum of tomorrows.<br />
'''''Reflection of Today'''''
 <small>''poem by Poesy''</small>
</blockquote>

Poesy's artist statement :
<blockquote>
'''''I want to raise compassion, kindness & empathy by media reform.'''''
</blockquote>

== Career ==

She is most known for her survival stories<ref>http://www.poesyliang.com/press/Dare_Oct04.pdf</ref><ref>http://www.poesyliang.com/press/Star_16Nov05.pdf</ref><ref>http://www.yli.cpss.com.my/text/report02.html</ref> of overcoming paralysis twice and the various philanthropic causes she later spearheaded through [[Helping Angels]]. 2008 sees an endorsement for a Japanese luxury skincare [http://video.aol.com/video-detail/poesy-liang/2572012458 SKII campaign of 'Commitment to Compassion'](Malaysia & Singapore) as 'Founder of Helping Angels'. 
* Luxury home designer
* Lifestyle designer
* Product designer
* Jewellery designer
* Principal of a jewellery brand
* Founder of [[Helping Angels]]
* Creator of Bald Empathy Movement
* Artist
* Author
* Poet
* Music composer
* Motivational speaker

== Awards ==
* Her World Woman of the Year 2011 (Malaysia)
* NTV7 Top 5 Inspirational Woman for Bella Awards 2013, nominee for Bella Hearts Award 2013 (humanitarian category)
* NewMan Creative Pioneer Awards 2013 special category Johnnie Walker Philanthropy Award 2013
* 旭茉JESSICA Most Successful Women 2014 最成功女性
* CLEO L'Oreal Paris Women of Worth 2015

== Music ==

Poesy composed her first song Treetops as a theme song for Helping Angels in 2009, with the intention to use the tune for a collection of jewellery boxes too. To spread the message of compassion, Treetops was originally written to be sung in various languages by Poesy's selection of music celebrities, with a children's choir in mind. 
However, upon meeting Godfrey Wang (G2) during his tour to Malaysia with Boney M, Poesy was encouraged to sing the song herself.
Godfrey Wang made the music arrangement as his contribution to the movement. 
Treetops was recorded in London in March 2011.
The song debuted in Cannes during Bald Empathy Movement on 16 May 2011.
The first stage Poesy was to perform this song is with Bangkok Charity Orchestra for the Butterfly Spirit concert to raise funds to rebuild Nepal on 9 June 2015.

Subsequently, G2 further collaborated with Poesy to produce a cover album of romantic jazz tunes and original compositions.
She wrote the lyrics for If You Were Here by composer guitarist Per-Olov Kindgren and sang it. The song was recorded in London with G2's musical arrangements and released on social media in September 2015.

== Private life ==

Born 31 July 1975 in Kuala Lumpur, to father Leong Khai Kwan, a Malaysian Chinese of Cantonese descent and mother Chuang Mei Yin, a Taiwanese of Hakka descent. Poesy was discovered at the age of 14 to star in a Levi's Strauss television commercial as the 501 girl for Malaysia and Singapore. Since then she started to appear in advertisements and television programs until she announced retirement in the Chinese media about at the age of 24.

Poesy's mother is an [[acupuncturist]] and [[Chinese herbalism|traditional herbalist]], and was also trained in western medicine in [[midwifery]]. It was said that it made the big difference in Poesy's rehabilitation from paralysis. During her medical career, Dr. Chuang also travelled extensively to offer welfare aid to the underprivileged patients in Peninsula Malaysia.

During her television years, Poesy faced social difficulties which resulted in self-isolation.The media world became a great torment which led to the reinvention of her life, through her various academic and creative pursuits. In an interview she mentions the use of art for healing in aspects of both physical and spiritual well-being.

Poesy's family lost their financial comfort in the early 80s. Through her growing years, she watched her parents suffer hardships and their sacrifices made a great impact on her. Despite difficulties, her father persevered her under the artistic and cultural training with the guidance of various art masters, who were his personal friends. When the opportunities arose, Poesy worked in the media industry to lighten the family's burdens.

As a teenager, Poesy attended bible school. Due to her alternative lifestyle as a teenage television personality, she faced difficulty in the Christian community. At 30, she ventured to study [[Tibetan Buddhism]] with [[Dagpo Rinpoche|Dagpo Lama Rinpoche]]. She also met with many wise masters, including [[Jetsun Pema (Tibet)|Jetsun Pema]]. Poesy remains neutral to all religion.

She spends her free time pursuing new artistic skills and travels, while she dedicates a calling towards media reform and humanitarian messages. Her story attracts many media interviews which brings light to all her humanitarian interests.

Poesy's family is based in Kuala Lumpur. They also have family in Singapore, New Zealand, Taiwan, Hong Kong, China, Europe and the USA.

==Swimming==
Poesy swims long distance to train her legs, as she is not able to embark in many other sports due to the injury of her spine.

==Sources ==

# [http://thestar.com.my/metro/story.asp?file=/2008/3/14/central/20614611&sec=central Online group finds a way to give back to the community by Tan Ju Eng]
# [http://www.poesyliang.net/Info.asp?q=19 More about Poesy]
# [http://worldrec.info/2005/11/17/poesy-liangs-topsy-turvy-life Googled results of Poesy's news]
# [http://www.kakiseni.com/articles/features/MTMyOA.html#top New Mall, Old School: The Pavilion and the Demolition of BBGS by Gabrielle Low]
# [http://www.yli.cpss.com.my/text/report02.html Report from a participant of the Young Leadership International]
# [http://www.poezjewellers.com/ POEZ Jewellers]
# [http://video.aol.com/video-detail/poesy-liang/2572012458 SK-II Nona TV3 Interview of Poesy Liang, Founder of Helping Angels]
# [http://www.helpingangels.net Helping Angels Official Website, Work in Progress]
# [http://www.facebook.com/group.php?gid=5583718222 Helping Angels Facebook Group]
# [http://www.hospitalbank.com/asiancns/ACNS_5.html The Asian Conference of Neurological Surgeons]
# [http://www.sunway.com.my/sunmed/ Sunway Medical Centre]
# [http://www.mediatecalm.ca/workshops.asp Resolving Conflict in Healthcare]
# [http://www.mdm.org.my/articles.php?newsID=27 Medical Defence Malaysia]
# [http://www.poesyliang.com/press/Dare_Oct04.pdf Poesy in Motion Dare Magazine Oct 2004 Interview]
# [http://www.poesyliang.com/press/Star_16Nov05.pdf The Star November 16, 2005 Interview]
# [http://www.poesyliang.com/press/Star_19Nov05.pdf The Star November 19, 2005 Interview]
# [http://www.poesyliang.com/press/Star_14mar08.pdf The Star March 14, 2008 Interview]
# [http://www.apsense.com/article/108198.html Share your Tip for a Great Blog]
# [http://avran.blogspot.com/2008/04/girl-who-just-wouldnt-give-up.html The Girl Who Just Wouldn't Give Up]
# [http://dktan.multiply.com/journal/item/214/Lending_a_helping_hand Lending A Helping Hand]
# [http://www.lepak.com/precious.html Info on Precious Children's Home]
# [http://www.tccpj.com.my/trinity_children_home.htm Info onTrinity Children Centre]
# [http://www.poesyliang.net Poesy's World]
# [http://www.scribd.com/doc/2657/Accuray-Inc-A-Neurosurgical-Business-Case-Study#document_metadata Accuray, Inc.: A Neurosurgical Business Case Study by John R. Adler, Jr. MD.]
# [http://www.answers.com/topic/accuray-inc Hoover's Profile: Accuray Incorporated]
# [http://www.nst.com.my/Current_News/NST/articles/12avbb2-2/Article/index_html Sept 9, 2009 Sunday Times New Straits Times article Women who have got it all]
# [http://planetfilms.com/assets/folio_farouk/ferhad_soul.html Starring Poesy 'Soul In Me' 1999 MTV]
# [http://sundaypeople.nst.com.my/Current_News/SundayPeople/article/Features/20090829173254/Article/index_html Aug 30, 2009 Sunday People New Straits Times article WOW: A Helping Angel]
# [http://liveandinspireawards.blogspot.com/2009/08/poesy-liang.html Live & Inspire Humanitarian Awards Nomination]
# [http://www.sun2surf.com/article.cfm?id=37635 Sept 30, 2009 The Sun article Nominate ordinary people with extraordinary lives]
# [http://thestar.com.my/metro/story.asp?file=/2009/8/10/central/4457176&sec=central Aug 10th, 2009 The Star article School reunion celebrated with a big bang]
# [http://thestar.com.my/lifestyle/story.asp?file=/2008/12/30/lifefocus/2848528&sec=lifefocus Dec 30th, 2009 The Star article Crafted with Care]
# [http://www.sun2surf.com/article.cfm?id=37848 Sept 9th, 2009 The Sun article Designs from the Heart]
# [https://www.youtube.com/watch?v=OTUbV41pIOw Jan 30th, 2010 Phoenix Info-Channel interview on Poesy Liang & Gratitude Letters Project in Hong Kong]

==References==
{{reflist}}

{{DEFAULTSORT:Liang, Poesy}}
[[Category:1975 births]]
[[Category:Living people]]
[[Category:Malaysian people of Taiwanese descent]]
[[Category:Women nonprofit executives]]
[[Category:Cancer survivors]]
[[Category:Malaysian artists]]
[[Category:Malaysian poets]]
[[Category:Malaysian women writers]]
[[Category:Women poets]]