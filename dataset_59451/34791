{{good article}}
{{Infobox hurricane season
|Basin=Atl
|Year=1861
|Track=1861 Atlantic hurricane season summary map.png
|First storm formed=July 6, 1861
|Last storm dissipated=November 3, 1861
|Strongest storm name=[[#Hurricane One|One]] and [[#Hurricane Three|Three]]
|Strongest storm winds=90
|Total storms=8
|Total hurricanes=6
|Fatalities=At least 22
|Damages=
|five seasons=[[1859 Atlantic hurricane season|1859]], [[1860 Atlantic hurricane season|1860]], '''1861''', [[1862 Atlantic hurricane season|1862]], [[1863 Atlantic hurricane season|1863]]
}}
The 1861 [[Atlantic hurricane season]] occurred during the first year of the [[American Civil War]]<ref>{{cite web|author=Symonds, Craig|title=American Civil War (1861–1865)|date=|publisher=''The New York Times''|accessdate=August 20, 2011|url=http://topics.nytimes.com/topics/reference/timestopics/subjects/c/civil_war_us_/index.html}}</ref> and had some minor impacts on associated events. Eight [[tropical cyclones]] are believed to have formed during the 1861 season; the first storm developed on July 6 and the final system dissipated on November 3. Six of the eight hurricanes attained Category 1 hurricane status or higher on the [[Saffir-Simpson Hurricane Scale]], of which three produced hurricane-force winds in the United States. No conclusive damage totals are available for any storms.  Twenty-two people died in a shipwreck off the [[New England]] coast, and an undetermined number of crew members went down with their ship in the July hurricane. Based on [[maximum sustained winds]], the first and third hurricanes are tied for the strongest of the year, although the typical method for determining that record—central barometric air pressure—is not a reliable indicator due to a general lack of data and observations.

Four tropical storms from 1861 had been previously identified by scholars and hurricane experts, but three more were uncovered in modern-day reanalysis. Known tracks for most of the systems are presumed to be incomplete, despite efforts to reconstruct the paths of older tropical cyclones. Three systems completely avoided land.  They all had an effect on shipping, in some cases inflicting severe damage on vessels. A storm in September, referred to as the "Equinoctial Storm", hugged the [[East Coast of the United States]] and produced rainy and windy conditions both along the coast and further inland. The last storm of the season followed a similar track, and affected a large [[Union (American Civil War)|Union]] fleet of ships sailing to [[South Carolina]] for what would become the [[Battle of Port Royal]]. Two vessels were sunk and several others had to return home for repairs. Ultimately the expedition ended in a Union success.

==Timeline==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/07/1861 till:01/12/1861
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/07/1861

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_≥157_mph_(≥252_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:06/07/1861 till:12/07/1861 color:C2 text:"Hurricane #1"
  from:13/08/1861 till:17/08/1861 color:C1 text:"Hurricane #2"
  from:25/08/1861 till:30/08/1861 color:C2 text:"Hurricane #3"
  from:17/09/1861 till:17/09/1861 color:C1 text:"Hurricane #4"
  from:27/09/1861 till:28/09/1861 color:C1 text:"Hurricane #5"
  from:06/10/1861 till:09/10/1861 color:TS text:"Tropical Storm #6"
  from:07/10/1861 till:07/10/1861 color:TS text:"Tropical Storm #7"
  from:01/11/1861 till:03/11/1861 color:C1 text:"Hurricane #8"

  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/07/1861 till:01/08/1861 text:July
  from:01/08/1861 till:01/09/1861 text:August
  from:01/09/1861 till:01/10/1861 text:September
  from:01/10/1861 till:01/11/1861 text:October
  from:01/11/1861 till:01/12/1861 text:November
TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir-Simpson Hurricane Scale]])"

</timeline>
</center>

==Methodology==
Prior to the advent of modern tropical cyclone tracking technology, notably satellite imagery, many hurricanes that did not affect land directly went unnoticed, and storms that did affect land were not recognized until their onslaught. As a result, information on older hurricane seasons was often incomplete. Modern-day efforts have been made and are still ongoing to reconstruct the tracks of known hurricanes and to identify initially undetected storms. In many cases, the only evidence that a hurricane existed was reports from ships in its path. Judging by the direction of winds experienced by ships, and their location in relation to the storm, it is possible to roughly pinpoint the storm's center of circulation for a given point in time. This is the manner in which three of the eight known storms in the 1861 season were identified by hurricane expert José Fernández Partagás's reanalysis of hurricane seasons between 1851 and 1910. Partagás also extended the known tracks of most of the other tropical cyclones previously identified by scholars. The information Partagás and his colleague uncovered was largely adopted by the [[National Oceanic and Atmospheric Administration]]'s [[Atlantic hurricane reanalysis]] in their updates to the [[HURDAT|Atlantic hurricane database]] (HURDAT), with some slight adjustments. HURDAT is the official source for such hurricane data as track and intensity, although due to a sparsity of available records at the time the storms existed, listings on some storms are incomplete.<ref name="HURDAT"/><ref name="meta"/>

Although extrapolated peak winds based on whatever reports are available exist for every storm in 1861, estimated minimum barometric air pressure listings are only present for the three storms that directly affected the United States.<ref name="us">{{cite web|author=Hurricane Research Division|year=2011|title=Continental U.S. Hurricanes:  1851 to 1930, 1983 to 2010|publisher=National Oceanic and Atmospheric Administration|accessdate=2011-06-04|url=http://www.aoml.noaa.gov/hrd/hurdat/usland1851-1930&1983-2010-Mar2011.html}}</ref> Two hurricanes during the year made landfall on the mainland United States, and as they progressed inland, information on their meteorological demise was limited. As a result, the intensity of these storms after landfall and until dissipation is based on an inland decay model developed in 1995 to predict the deterioration of inland hurricanes.<ref name="meta"/>

==Storms==

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic hurricane 1 track.png
|Formed=July 6
|Dissipated=July 12
|1-min winds=90
|Pressure=
}}
The first tropical cyclone and hurricane of the 1861 season is believed to have formed on July 6, immediately east of the [[Leeward Islands]]. A 1938 publication documented the storm's effects on [[Guadeloupe]] and [[St. Kitts]], and given a lack of prior reports on the cyclone, modern-day reassessments concluded that it was relatively weak when it affected those islands.<ref name="Partagas1">Partagás, p. 1</ref> After crossing the northern Leeward Islands, the tropical storm broadly curved toward the northwest, likely intensifying into the equivalence of a Category 1 hurricane on the Saffir-Simpson Hurricane Scale on July 9.<ref name="HURDAT">{{cite web|author=Hurricane Specialists Unit|publisher=National Hurricane Center|year=2009|accessdate=July 15, 2011|title=Easy to Read HURDAT 1851–2008|url=http://www.aoml.noaa.gov/hrd/hurdat/easyread-2009.html| archiveurl= https://web.archive.org/web/20110628231915/http://www.aoml.noaa.gov/hrd/hurdat/easyread-2009.html| archivedate= 28 June 2011 <!--DASHBot-->| deadurl= no}}</ref> The majority of the storm's track in the western Atlantic was unknown until it was reconstructed based on reports from, and the effects on, three ships in its vicinity.<ref name="Partagas1"/>

On July 10—when the storm was approaching or at its peak intensity with winds of {{convert|100|mph|km/h|abbr=on}}<ref name="HURDAT"/>—the ''Bowditch'' encountered severe hurricane conditions which destroyed both of her masts and washed her entire crew overboard. Her captain was able to climb back aboard, where he survived for over a week with no food or water until he was rescued by a schooner. The ''Echo'' and ''Creole'' both sustained significant damage, and the crew and captain of the latter ship had to be rescued after she began taking on water. The extent of the damage to the three ships served as the basis for evaluating the storm's intensity in Partagás's paper.<ref name="Partagas1"/> The hurricane ultimately passed between [[Bermuda]] and the United States before dissipating after July 12.<ref name="HURDAT"/>
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic hurricane 2 track.png
|Formed=August 13
|Dissipated=August 17
|1-min winds=80
|Pressure=978
}}
A month after the dissipation of the first hurricane, another tropical storm formed north of [[Hispaniola]] on August 13.<ref name="HURDAT"/> Ludlum (1963) described the "[[Key West, Florida|Key West]] Hurricane" between August 14 and 16,<ref name="Partagas2">Partagás, p. 2</ref> and it was determined that the system had, in fact, surpassed the threshold for hurricane status based on wind observations from two ships.<ref name="meta">{{cite web|author=Hurricane Research Division|year=2011|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|publisher=National Oceanic and Atmospheric Administration|accessdate=2011-06-04|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html| archiveurl= https://web.archive.org/web/20110604063810/http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html| archivedate= 4 June 2011 <!--DASHBot-->| deadurl= no}}</ref> The storm skirted the north coast of [[Cuba]] as it moved west-northwest and passed through the [[Florida Straits]].<ref name="HURDAT"/> On or around August 15, [[Havana]], Cuba experienced heavy rainfall.<ref name="Partagas2"/> Although the cyclone did not directly make landfall, it delivered hurricane-force winds to southern Florida.<ref name="us"/> It turned more toward the northwest as it entered the [[Gulf of Mexico]], where it began to gradually weaken. It is listed as having dissipated on August 17 in the northern Gulf.<ref name="HURDAT"/>

The hurricane damaged or wrecked numerous vessels. Six ships were wrecked or grounded in the [[Bahamas]], and the crews of at least two, the ''John Stanley'' and the ''Linea'', had to be rescued. The steamship ''Santiago de Cuba'' left port on August 4, and began to encounter squally conditions later that afternoon. Heavy seas and a strong gale inflicted some damage on the vessel. Several ships along the eastern coast of Cuba were wrecked during the storm, leading to great uncertainty and concern regarding the fate of the ''Santiago''. At least three vessels were lost or grounded along the [[Florida Keys]].<ref name="Partagas2"/>

{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic hurricane 3 track.png
|Formed=August 25
|Dissipated=August 30
|1-min winds=90
|Pressure=958
}}
The first storm to be uncovered in modern-day reanalysis existed in late August, and ties the July hurricane for the strongest system of the season in terms of [[maximum sustained winds]]. Its track is known between August 25 and August 30, during which time it progressed generally northeasterly from a point northeast of the island of Bermuda to the central northern Atlantic.<ref name="HURDAT"/><ref name="Partagas23">Partagás, pp. 2–3</ref> On August 30, the ''Harvest Queen'' recorded a barometer of {{convert|28.30|inHg|hPa|abbr=on|lk=on}} on August 30; this report was a strong indication that the storm had attained hurricane intensity, although the system was likely undergoing its transition into an [[extratropical cyclone]] at the time.<ref name="Partagas3">Partagás, p. 3</ref>
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic hurricane 4 track.png
|Formed=September 17
|Dissipated=September 17
|1-min winds=70
|Pressure=
}}
The subsequent hurricane was also previously unrecognized until contemporary research, although the majority of its track remains unknown. The only indication that a tropical cyclone existed was the ship ''David G. Wilson'', which was dismasted by a severe storm on September 17. As no other information is available on the hurricane, it is listed in the Atlantic hurricane database as a single point in the central Atlantic (at 28.5°N, 50°W).<ref name="Partagas3"/>
{{Clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic hurricane 5 track.png
|Formed=September 27
|Dissipated=September 28
|1-min winds=70
|Pressure=985
}}
The first storm to directly strike the mainland United States was first detected on September 27, off the east coast of the [[Florida]] peninsula.<ref name="HURDAT"/> The storm is estimated to have been a minimal hurricane based on observations from the ship ''Virginia Ann''.<ref name="meta"/> Several other vessels encountered the storm along its track, including the steamship ''Marion'', which experienced hours of violent winds, torrential rainfall, and frequent thunder and lightning.<ref name="Partagas3"/> The hurricane curved north, then northeast, striking [[North Carolina]] that same day before speeding northeastward as it hugged the United States East Coast. Its track is only known through 1200 [[Coordinated Universal Time|UTC]] on September 28.<ref name="HURDAT"/> Ludlum (1963) refers to the hurricane as the "Equinoctial Storm", and describes its area of impact as the "entire coast".<ref>Ludlum, p. 194</ref>

In the aftermath of the [[Battle of Carnifex Ferry]] in present-day [[West Virginia]], [[Rutherford B. Hayes]] of the [[23rd Ohio Infantry]] was camped south of the battle site, where he wrote about a "very cold rain-storm" in a September 27 letter to his wife Lucy. Conditions at the time were characterized by leaking tents and temperatures getting "colder and colder". Hayes wrote, "We were out yesterday P.M. very near to the enemy's works; were caught in the first of this storm and thoroughly soaked. I hardly expect to be dry again until the storm is over."<ref name="Hayes">{{cite book|page=102|author=Rutherford Birchard Hayes|title=Diary and Letters of Rutherford Birchard Hayes: 1861–1865|year=1922|publisher=The Ohio State Archaeological and Historical Society|url=https://books.google.com/books?id=JxwQAAAAYAAJ&pg=PA99&lpg=PA99&dq=1861+Equinoctial+Storm&source=bl&ots=X9TJUiQCCt&sig=xE3szCFy2G0yN4GIWgQIJceRLJo&hl=en&ei=roYvToW7CsLZ0QHrp83PAQ&sa=X&oi=book_result&ct=result&resnum=1&sqi=2&ved=0CBUQ6AEwAA#v=onepage&q=1861%20Equinoctial%20Storm&f=false|accessdate=July 27, 2011}}</ref> Strong winds buffeted the [[Burlington, New Jersey]], area from early evening to midnight on September 27, uprooting trees and causing some damage to property. Further north, [[Boston, Massachusetts]], experienced intense winds and light rainfall for about five hours starting at midnight, with no initial reports of significant destruction.<ref name="Partagas3"/>

{{Clear}}

===Tropical Storm Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic tropical storm 6 track.png
|Formed=October 6
|Dissipated=October 9
|1-min winds=60
|Pressure=
}}
Two tropical systems are known to have existed during the month of October. The first was originally documented by Partagás (1995), who detected it based on a faulty report of a violent gale from a ship, the ''Mariquita''. The report was said to have been from October 16, but given her arrival date in [[New York City]] four days later and her location at the time of her encounter with the storm, she probably encountered the cyclone much earlier in the month. The violent south-southwesterly gale lasted 15 hours when the vessel was probably located at 20.5°N, 47°W. The storm was initially assigned a single set of coordinates for October 6, and no attempt was made to reconstruct its track due to a lack of certain data on it. However, it was noted that a ship further north on October 9 experienced a heavy gale.<ref name="Partagas5">Partagás, p. 5</ref> Based on the likely correlation between the two ship reports, the storm's track was extended four days to late on October 9 in the Atlantic hurricane database.<ref name="meta"/>
{{Clear}}

===Tropical Storm Seven===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic tropical storm 7 track.png
|Formed=October 7
|Dissipated=October 7
|1-min winds=50
|Pressure=
}}
A 1960 publication mentioned a tropical storm near [[Cape Hatteras, North Carolina]] sometime in October 1861 without specifying a date. Newspaper reports indicate that ships mainly north of the Cape Hatteras area encountered strong northerly gales for several days starting on October 7, and winds in New York City persisted from October 7 to October 10 with a northerly component. Partagás (1995) noted, "These findings do not seem to support a tropical system but the author made the decision of retaining the storm [...] due to the lack of solid evidence against its existence."<ref name="Partagas5">Partagás, p. 5</ref> However, little is known about the system, and its inclusion in the hurricane database is limited to a single point at 35.3°N, 75.3°W.<ref name="HURDAT"/>
{{clear}}

===Hurricane Eight===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1861 Atlantic hurricane 8 track.png
|Formed=November 1
|Dissipated=November 3
|1-min winds=70
|Pressure=985
}}
The final storm of the season followed a generally north-northeasterly course from the Gulf of Mexico northward along the U.S. East Coast between November 1 and November 3, dissipating over New England. The storm crossed southern Florida,<ref name="HURDAT"/> and based on observations from [[Hatteras Inlet]], North Carolina, and observations from the ship ''Honduras'', it is believed to have attained hurricane intensity.<ref name="meta"/> The hurricane made landfall in eastern North Carolina and proceeded up the coast before crossing eastern Long Island and coming ashore in southern New England. Its demise on November 3 marked the end of the 1861 Atlantic hurricane season; the next tropical storm would not form in the Atlantic until June 1862.<ref name="HURDAT"/> Two storm systems affected the region in the week following October 28, both of which influenced a Civil War expedition which was "the largest fleet of war ships and transports ever assembled".<ref name="Ludlum101">Ludlum, p. 101</ref><ref name="DR">{{cite web|author=David Roth|title=Late Nineteenth Century Virginia Hurricanes|year=|publisher=Hydrometeorological Prediction Center|accessdate=July 27, 2011|url=http://www.wpc.ncep.noaa.gov/research/roth/valate19hur.htm}}</ref>

The first storm, which is not recognized as a tropical cyclone, disrupted the initial assembly of the fleet on October 28. However, the fleet set sail the next day on its mission to attack Confederate forces (its destination was "supposedly a military secret"<ref name="Ludlum101"/>). On November 2, the expedition encountered the second storm—the tropical hurricane—which wreaked havoc on the organization of the fleet and sunk two of its vessels. There was knowledge at the time of the series of storm systems, but few details on the condition and fate of the fleet, sparking great concern.<ref name="Ludlum101"/> Some of the other ships were forced to return home for repairs, but the majority rode out the storm successfully.<ref>Browning, pp. 29–30</ref> The expedition proceeded onward and seized [[Port Royal Sound]] at the [[Battle of Port Royal]]. As described by Ludlum (1963), the hurricane is known as the Expedition Hurricane due to its influence on the fleet.<ref name="Ludlum101"/>

However, the hurricane also had a significant impact on land. Earlier in the year, Union forces had captured the fort guarding Hatteras Inlet at the [[Battle of Hatteras Inlet Batteries]]. In the early morning hours of November 2, high seas began to overwash [[Hatteras Island]], "completely covering all dry land except the position of the fort itself".<ref name="Ludlum101"/> After four hours, water began to subside. Extremely high tides associated with the cyclone continued up the coast as far north as [[Portland, Maine]]. Storm tides at various points, including New York City, [[Newport, Rhode Island]], and Boston, reached levels unseen for at least 10 years and up to 46 years. In New York, the storm persisted for 20 hours starting early on November 2; rising waters inundated wharves along the [[East River|East]] and [[North River (Hudson River)|Hudson]] Rivers. Floodwaters flowed up to five blocks inland, and a popular bar located in a hotel became isolated by the flooding. In response, a man transported customers to and from the bar on his private boat at a cost of two cents per ride.<ref name="Ludlum102">Ludlum, p. 102</ref> Strong winds in [[Brooklyn]] brought down trees and telegraph wires.<ref name="Ludlum102">Ludlum, p. 102</ref>

Infrastructure throughout the [[Tri-State area (New York-New Jersey-Connecticut)|Tri-State area]] suffered. Parts of the [[New Jersey Railroad]] line were undermined, and the [[Shore Line Railway (Connecticut)|Shore Line Railway]] at [[Bridgeport, Connecticut|Bridgeport]] was inundated. Flooding was also prominent in the [[New Jersey Meadowlands]] and along the Newark Turnpike and Plank Road, which was left temporarily impassable. Further east, the hurricane triggered coastal flooding along the shores of [[Long Island]], while northeasterly winds blew several ships ashore along the northern coast of Long Island. The eastern side of the hurricane blasted the southeastern New England coast between November 2 and November 3, damaging over 250 vessels at [[Provincetown, Massachusetts]], and running aground 20 others. Water from the [[Massachusetts Bay]] surged into the village of [[Wareham, Massachusetts|Wareham]]. In downtown Boston, the storm began late on November 2 and lasted until late the next morning, although the highest tides did not occur until after conditions had already cleared. Twenty-two occupants of the ship ''Maritania'' drowned when the vessel sank after striking a rock during the worst of the storm. At the time, she was located {{convert|1|mi|km|abbr=on}} east of the [[Boston Light]].<ref name="Ludlum102"/>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of Atlantic hurricane seasons]]

==Notes==
{{Reflist|2}}

==References==
*Browning, Robert M. Jr., ''Success is all that was expected; the South Atlantic Blockading Squadron during the Civil War.'' Brassey's, 2002. {{ISBN|1-57488-514-6}}
*{{cite book|author=[[David M. Ludlum|Ludlum, David McWilliams]]|title=Early American hurricanes, 1492–1870|year=1963|publisher=[[American Meteorological Society]]}}
*{{cite web|author=Partagás, José Fernández|publisher=National Oceanic and Atmospheric Administration|year=1995|title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources: Year 1861|accessdate=July 15, 2011|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1858-1864/1861.pdf}}

{{TC Decades|Year=1860|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1866 Atlantic Hurricane Season}}
[[Category:Atlantic hurricane seasons]]
[[Category:1860–1869 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]