{{Orphan|date=January 2014}}
{{Infobox person
| name        = David Vanderpool
| birth_date  = February 18, 1960
| birth_place = [[Dallas]], [[Texas]]
| education   = [[Abilene Christian University]], [[Texas Tech University Health Sciences Center]], [[Baylor University Medical Center]]
| occupation  = Trauma and vascular surgeon, CEO of Live Beyond, medical missionary
}}
'''David Vanderpool''' (born February 18, 1960) is an American medical missionary and the CEO and founder of Live Beyond, which has provided medical, spiritual and logistical support to disaster ridden countries.

Vanderpool's work is unusual in that he combines his medical training with an explicit effort to convert his patients to Christianity.

==Early life and education==
Vanderpool was born in [[Dallas, Texas]], graduated from [[St. Mark's School of Texas]], and received his undergraduate degree from [[Abilene Christian University]] in 1982.<ref>[https://smtexas.myschoolapp.com/ftpimages/73/download/The_Pride_2011_Winter.pdf]</ref> He then attended the [[Texas Tech University Health Sciences Center School of Medicine|School of Medicine]] at [[Texas Tech University Health Sciences Center]]. After medical school, Vanderpool completed two surgical residencies at [[Baylor University Medical Center]] where he trained as a vascular surgeon.<ref>{{cite web|url=http://www.mmdr.org/Staff.html |title=Staff |publisher=Mmdr.org |date= |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140126081445/http://www.mmdr.org:80/staff.html |archivedate=2014-01-26 |df= }}</ref>

==Family==
Vanderpool wife, Laurie, is a speaker for Women's Retreats and [[Bible study (Christian)|Bible Studies]], and speaks frequently for Down Syndrome organizations.<ref>{{cite web|url=http://www.livebeyond.org/Staff.html |title=Staff |publisher=Livebeyond.org |date= |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140203173807/http://www.livebeyond.org:80/Staff.html |archivedate=2014-02-03 |df= }}</ref>

==Early career==
Vanderpool remained in Texas after residency and practiced as a vascular surgeon before moving to Brentwood, Tennessee in 2001 and opening his private practice Lave MD in 2003.

Dr. Vanderpool created Lave MD to act as both a medical facility and a spa. 
After the establishment of his international organization in 2005, Dr. Vanderpool used much of Lave MD's proceeds to fund the organization's efforts abroad.

==Medical missionary work==
In 2005, after [[Hurricane Katrina]] hit the southeastern part of the United States, Dr. Vanderpool delivered healthcare across the Mississippi Coast out of a trailer.<ref>{{cite web|last=Fowler |first=Joanne |url=http://www.people.com/people/archive/article/0,,20358101,00.html |title=Hope Out of the Rubble |publisher=People.com |date= |accessdate=2014-02-03}}</ref> His goal was to provide as much free healthcare as possible while the medical infrastructure could recover. Months later, Dr. Vanderpool established his organization, Medical Mobile Disaster Relief, as a 501(c)(3) non-profit organization with goals to provide disaster relief through medical clinics, clean water projects, and micro-finance projects to areas hit by disasters. 
Vanderpool and the Mobile Medical Disaster Relief team began working in Mozambique in 2006. His goal was to provide healthcare to the indigenous people of the country in addition to enhancing the economy by implementing micro-finance projects among widows living in the communities.<ref>{{cite web|url=http://www.livebeyond.org/Mozambique.html |title=Mozambique |publisher=Livebeyond.org |date= |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140110085936/http://www.livebeyond.org/Mozambique.html |archivedate=2014-01-10 |df= }}</ref> By 2008, Dr. Vanderpool teamed up with the [[Belmont University|Belmont School of Nursing]] to construct a nursing curriculum that could teach the Mozambique women to be self-sufficient in caring for themselves and their children.<ref>{{cite web|url=http://nashville.medicalnewsinc.com/physician-spotlight-david-vanderpool-md-cms-1943 |title=Physician Spotlight: David Vanderpool, MD on Nashville Medical News |publisher=Nashville.medicalnewsinc.com |date= |accessdate=2014-02-03}}</ref>

Vanderpool and his Mobile Medical Disaster Relief partnered with PACODEP (Partners in Community Development) in Ghana in order to provide medical care, educate the locals on water purification and distribute water purifiers. PACODEP works to free enslaved children who are trafficked through Ghana for purposes of fishing work. Dr. Vanderpool has also partnered with local hospitals in Ghana in order to provide free invasive surgeries to these rescued children.<ref>{{cite web|url=http://www.livebeyond.org/Ghana.html |title=Ghana |publisher=Livebeyond.org |date= |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140110090632/http://www.livebeyond.org/Ghana.html |archivedate=2014-01-10 |df= }}</ref>

Dr. Vanderpool partnered with Mission Lazarus in 2009 to build a sustainable medical clinic in Cedeno, Honduras. Given sufficient medical supplies and equipment, Vanderpool allowed Mission Lazarus to take over the clinic and provision of healthcare for the people of Cedeno.<ref>{{cite web|url=http://www.livebeyond.org/Honduras.html |title=Honduras |publisher=Livebeyond.org |date= |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140110085736/http://www.livebeyond.org/Honduras.html |archivedate=2014-01-10 |df= }}</ref>

===Live Beyond in Haiti===
Dr. Vanderpool shifted his focus in the aftermath of the [[Haiti earthquake 2010|earthquake]] that devastated the entire country of Haiti in 2010. In 2010, Vanderpool officially changed the name of his organization from Mobile Medical Disaster Relief to Live Beyond with a stated mission to be "an organization that chooses to Live Beyond...ourselves, our culture, our borders and this life so that others can Live Beyond…disease, hunger, poverty and despair."<ref>{{cite web|url=http://www.livebeyond.org/Haiti.html |title=Haiti |publisher=Livebeyond.org |date=2010-01-12 |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140110085813/http://www.livebeyond.org/Haiti.html |archivedate=2014-01-10 |df= }}</ref>

The initial aid Vanderpool and his team brought to Haiti was primarily mobile medical care to relieve the thousands devastated and injured by the earthquake. Since then however, Vanderpool has grounded his missionary work in Thomazeau, Haiti where his organization began building a base. Vanderpool continued to provide free medical care, establishing a surgical hospital and clinic. In addition, clean water projects, orphanages, and other widow and orphan advocacy projects were begun.<ref>{{cite web|url=http://www.livebeyond.org/Haiti-Base.html |title=Haiti Base |publisher=Livebeyond.org |date=2012-05-18 |accessdate=2014-02-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20140110090459/http://www.livebeyond.org/Haiti-Base.html |archivedate=2014-01-10 |df= }}</ref>

==Religion==
Dr. Vanderpool is a Christian, who combines religion and the spread of his faith with his medical work. In Haiti, he has made it one of his objectives to bring Haiti away from its traditional voodoo culture and provide "spiritual guidance" to the Haitians in the role of Christianity, with the belief that Christianity will lead to a better Haiti.<ref>{{cite web|url=http://www.acuoptimist.com/2010/11/alumni-join-fight-against-cholera-in-haiti/ |title=Alumni join fight against Haiti cholera outbreak &#124; The Optimist |publisher=Acuoptimist.com |date=2010-11-04 |accessdate=2014-02-03}}</ref> Prior to moving to Haiti, each medical outreach trip made by Vanderpool and other Live Beyond participants included prayer and Christian ministry along with healthcare to the voodoo priests, island chiefs, idol worshipers and the sick and dying in Haiti. On the Live Beyond site, Vanderpool's religious impact since being in Haiti has been characterized as leading to the baptism of dozens, the saving of tribes through "Bibles being read in their own languages" and "the Kingdom is being expanded."<ref>{{cite web|url=http://www.livebeyond.org/Our-Mission.html |title=Our-Mission |publisher=Livebeyond.org |date= |accessdate=2014-01-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20140110085458/http://www.livebeyond.org/Our-Mission.html |archivedate=2014-01-10 |df= }}</ref>   Dr. Vanderpool promotes religious missionary work in tandem with his medical relief and sustainable development efforts. A worship center is being built in Thomazeau, Haiti and monthly mission trips are promoted and scheduled for Americans to travel to Thomazeau and volunteer.

==Awards and certificates==
#Received Lipscomb University's Dean Institute for Corporate Governance and Integrity Business with Purpose Award<ref>{{cite web|url=http://www.prweb.com/releases/2012/4/prweb9362851.htm |title=Dr. David Vanderpool, CEO of LAVE MD, Receives Lipscomb University's Business with Purpose Award |publisher=Prweb.com |date= |accessdate=2014-02-03}}</ref>
#Darrell Waltrip June Hero of the Month Award<ref>{{cite web|url=http://www.prweb.com/releases/2011/7/prweb8624219.htm |title=Dr. David Vanderpool, CEO of LAVE MD, Receives Darrell Waltrip's Hero of the Month Award |publisher=Prweb.com |date= |accessdate=2014-02-03}}</ref>
#Member of the American Academy of Cosmetic Surgery

==References==
{{Reflist}}

{{DEFAULTSORT:Vanderpool, David}}
[[Category:1960 births]]
[[Category:Living people]]
[[Category:Texas Tech University Health Sciences Center alumni]]
[[Category:People from Dallas]]
[[Category:Abilene Christian University alumni]]
[[Category:American Protestant missionaries]]
[[Category:Global health]]
[[Category:Christian medical missionaries]]
[[Category:Protestant missionaries in the United States]]
[[Category:Protestant missionaries in Mozambique]]