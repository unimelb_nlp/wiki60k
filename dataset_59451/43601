{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 |name=S.45 Seaford
 |image=IWM-ATP-14556C-Seaford.jpg
 |caption=Seaford NJ205 at Rochester, July 1946
}}{{Infobox Aircraft Type
 |type=Flying boat
 |national origin=United Kingdom
 |manufacturer=[[Short Brothers]]
 |designer=
 |first flight=30 August 1944
 |introduced=
 |retired=
 |status=
 |primary user=[[Royal Air Force]]
 |more users=
 |produced=
 |number built=10
 |developed from=[[Short Sunderland]]
 |variants with their own articles=[[Short Solent]]
}}
|}

The '''Short S.45 Seaford''' was a 1940s flying boat, designed as a long range maritime patrol bomber for [[RAF Coastal Command]].
It was developed from the [[Short S.25 Sunderland]], and initially ordered as "Sunderland Mark IV".

==Background==
In 1942, the Air Ministry issued [[Air Ministry specification|Specification]] R.8/42 for a replacement of the Sunderland, as a long range patrol bomber for service in the Pacific Ocean. It required more powerful engines, better defensive armament, and other enhancements.<ref name=barnes357>Barnes 1989, pp. 357</ref><ref name=green106>Green 1968, p. 106.</ref>

==Design and development==
The Sunderland Mark IV used major structural elements of the Sunderland Mark III, with a fuselage stretch of 3&nbsp;ft ahead of the wing, an extended and redesigned planing bottom, the same wing with thicker Duralumin skinning, and [[Bristol Hercules]] engines. Further structural changes were made after initial flight tests. The planned armament consisted of two fixed forward-firing .303&nbsp;inch (7.7&nbsp;mm) Browning machine guns in the nose, a Brockhouse Engineering nose turret with twin .50 in (12.7&nbsp;mm) machine guns, twin [[Hispano 20 mm cannon|20 mm Hispano cannon]] mounted in a [[Bristol Aeroplane|Bristol]] B.17 dorsal turret, twin .50 in (12.7&nbsp;mm) guns in a [[Glenn L. Martin Company|Glenn-Martin]] tail turret, and another .50 in (12.7&nbsp;mm) machine gun in a hand-held position on each side of the fuselage. The turrets were all electrically powered. Two prototypes and thirty production aircraft were ordered as the Sunderland Mark IV.<ref name=barnes357/><ref name=green106/>

==Operational history==
[[File:Seaford1811.jpg|thumb]]
On 30 August 1944, the prototype (MZ269) first flew from the [[River Medway]] at Rochester. The increased engine power caused aerodynamic stability problems, and a new fin was designed with greater height with forward dorsal extension, plus a new tailplane with increased span and area.<ref name=barnes360>Barnes 1967, p. 360.</ref> Changes were so extensive, that the new aircraft was given the name [[Seaford, East Sussex|Seaford]].<ref name=london196>London 2003, p. 196.</ref> Thirty production aircraft were ordered, but the first of these flew in April 1945, well after the introduction of the Sunderland Mark V, and too late to see combat in Europe. The prototypes were powered by Hercules XVII engines of 1,680&nbsp;hp (1,253&nbsp;kW), but production aircraft used 1,720&nbsp;hp (1,283&nbsp;kW) Hercules XIX engines. The planned Glenn Martin tail turrets were never installed. Eight production Seafords were completed; the first (NJ200) was used for trials at [[MAEE]] Felixstowe. The second production Seaford (NJ201) was evaluated by RAF Transport Command, then in December 1945 it was loaned without armament to [[BOAC]] as G-AGWU, then returned to MAEE as NJ201 in February 1946. In April 1946, the other six production Seafords were delivered to No. 201 Squadron RAF for brief operational trials. In 1948, those six aircraft were modified as civilian airliners at Belfast, then leased to BOAC with the designation [[Short Solent|Solent 3]].<ref name=barnes3603>Barnes 1989, pp. 360-363</ref>

==Operators==
;{{UK}}
*[[British Overseas Airways Corporation]]
*[[Royal Air Force]]
**[[No. 201 Squadron RAF]]

==Specifications (S.45 Seaford)==
{{aircraft specifications|
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|ref=Green 1968, p. 107<ref name=green107>Green 1968, p. 107.</ref>
|plane or copter?=plane
|jet or prop?=prop
<!-- Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|crew=8 &mdash; 11 (two pilots, radio operator, navigator, engineer, bomb-aimer, three to five gunners)
|length main= 88 ft 6¾ in
|length alt= 27.00 m
|span main= 112 ft 9½ in
|span alt=34.38 m
|height main=37 ft 3 in
|height alt=11.35 m
|area main=1,687 sq ft
|area alt=156.7 m²
|empty weight main=45,000 lb
|empty weight alt=20,412 kg
|loaded weight main=75,000 lb
|loaded weight alt=34,020 kg
|max takeoff weight main=
|max takeoff weight alt=
|engine (prop)= [[Bristol Hercules]] XIX
|type of prop= [[radial engine]]s
|number of props=4
|power main=1,720 hp
|power alt=1,283 kW
|max speed main=242 mph
|max speed alt=210 knots, 389 km/h
|max speed more=at 500 ft (150 m)
|cruise speed main=155 mph
|cruise speed alt=138 knots, 249 km/h
|stall speed main=
|stall speed alt=
|range main= 3,100 mi<ref name=barnes368>Barnes 1967, p. 368.</ref><ref name=london2645>London 2003, pp. 264–265.</ref>
|range alt=2,696 [[nautical mile|nmi]], 4,988 km
|ceiling main=14,000 ft
|ceiling alt=4,267 m
|climb rate main=880 ft/min
|climb rate alt=4.5 m/s
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|more performance=*'''Climb to 10,000 ft (3,050 m):''' 18 min
|guns= 6 × [[.50 BMG|.50 in]] [[M2 Browning machine gun|Browning machine gun]]s (two each in nose and tail turrets and two beam guns), 2 x 20 mm [[Hispano-Suiza HS.404|Hispano cannon]] in dorsal turret and 2 × fixed [[.303 British|.303 in]] [[M1919 Browning machine gun|Browning machine gun]]
|bombs=4,960 lb (2,250 kg) of bombs and depth charges
}}

==Survivor==
Short S.45 Seaford NJ203 displayed at [[Oakland Aviation Museum]], Oakland, California.<ref>Ogden (2007)</ref><ref>[http://oaklandaviationmuseum.org/solent_flying_boat_32.html "Short Solent"]</ref>

==See also==
{{aircontent|
|related=
|similar aircraft=
|lists=
*[[List of aircraft of the RAF]]
|see also=
}}

==Notes==
{{reflist}}

==Bibliography==
* Barnes, C.H. ''Shorts Aircraft since 1900''. Putnam, 1967, 1989 ISBN 0-85177-819-4
* Green, William. ''War Planes of the Second World War: Volume Five Flying Boats''. Macdonald, 1968. ISBN 0-356-01449-5.
* London, Peter. ''British Flying Boats''. Sutton Publishing, 2003. ISBN 0-7509-2695-3.
* Ogden, Bob (2007). ''Aviation Museums and Collections of North America''. Air-Britain. ISBN 0-85130-385-4

== External links ==
{{Commons category}}
*[http://www.flightglobal.com/pdfarchive/view/1946/1946%20-%200018.html Short Seaford] ''Flight'' 3 January 1946 (3 pages of diagrams and images)

{{Short Brothers aircraft}}

[[Category:British patrol aircraft 1940–1949]]
[[Category:Flying boats]]
[[Category:Short Brothers aircraft]]
[[Category:World War II British patrol aircraft]]
[[Category:Four-engined tractor aircraft]]
[[Category:High-wing aircraft]]