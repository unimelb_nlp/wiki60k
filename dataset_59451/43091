[[File:Lawson L-4 Exterior.JPG|thumb|right|300px|The Lawson L-4 "Midnight Air Liner" circa 1921]]
{|{{Infobox Aircraft Begin
  |name = L-4
  |image =
  |caption = 
}}{{Infobox Aircraft Type
  |type = Three-engined biplane airliner
  |manufacturer = [[Lawson Airplane Company-Continental Faience and Tile Company|Lawson Airplane Company]]
  |designer = 
  |first flight = [[1924 in aviation|1924]]
  |introduced = 
  |retired = 
  |status = 
  |primary user = [[Lawson Airplane Company-Continental Faience and Tile Company|Lawson Airplane Company]]
  |more users = 
  |produced = 
  |number built =  1
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}

[[File:Lawson L-4 Interior.JPG|thumb|right|300px|Interior photograph of the Lawson L-4 showing washroom facilities and sleeping berths]]
The '''Lawson L-4''' was the last in a series of [[lawson L-2|Lawson]] biplane airliners designed and built by the [[Alfred Lawson]] under the livery of the [[Lawson Airplane Company-Continental Faience and Tile Company|Lawson Airplane Company]] of Milwaukee, Wisconsin. The largest of the series, it designed for long-distance flights. It was completed in 1920 but never flew, crashing on its initial takeoff.

==Design and development==
[[File:Lawson L-3 Midnight Airliner.jpg|left|thumb|Lawson Midnight Airliner]]After [[Alfred Lawson]] completed his 2000-mile [[Lawson L-2]] flight, the [[Lawson Airplane Company-Continental Faience and Tile Company|Lawson Airplane Company]] built the Lawson Midnight Liner for use on the night service between Chicago and New York.  The Midnight Liner was larger with three 400-hp Liberty engines – one on each wing and another in the nose. The airliner sported sleeping berths and a shower. It was his objective to produce large number of these aircraft to outfit his airline, but the 1920 recession deprived Lawson of the investment funds to meet payroll and other development expenses. The first and only Lawson Midnight Liner was completed on December 9, 1920. Bad weather, however, delayed its maiden flight. As Lawson’s financial situation worsened, he decided to fly his new airliner from a space near the factory, rather than make a costly ground transport move to Hamilton Field (now [[Mitchel Air Force Base|Gen. Mitchell Field]]). The prepared strip was only about 300 feet long. Lawson finally gave the order to attempt flight on May 8. The aircraft did not clear an elm tree and crashed on takeoff. The pilots were unhurt but the airliner was never repaired.<ref name="ASME Milwaukee - History & Heritage">{{cite web|title=LAWSON "AIR LINER"|url=http://sections.asme.org/milwaukee/history/42-lawsonairliner.html|publisher=ASME Milwaukee Chapter|accessdate=30 December 2012}}</ref><ref>{{cite news|title=Milwaukee-Made Lawson Air Liner Pioneers Transportation Delux|newspaper=The Milwaukee Journal|date=Dec 26, 1920}}</ref>

<br/>Lawson had a 100-passenger, double-deck version on the drafting board, but the Lawson Midnight Liner was the last. The company folded in 1922, and the assets were auctioned off. The Lawson Air Liners are renowned due to their size (for the time) and the ambition of their flamboyant promoter.

==Operators==
;{{USA}}
*[[Lawson Airplane Company-Continental Faience and Tile Company|Lawson Airplane Company]]

==Specifications (L-4) ==
{{aerospecs
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng
|crew=
|capacity=
|length m=16.51
|length ft=54
|length in=2
|span m=33.96
|span ft=111
|span in=5
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=5.33
|height ft=17
|height in=6
|wing area sqm=188.4
|wing area sqft=2,028
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=4427
|empty weight lb=9,759
|gross weight kg=8457
|gross weight lb=18,645
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=3
|eng1 type=[[Liberty L-12]] V-12 piston engine
|eng1 kw=<!-- prop engines -->313
|eng1 hp=<!-- prop engines -->420
|max speed kmh=177 (estimated)
|max speed mph=110 (estimated)
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
* [[Lawson L-2]] 
|lists=
|see also=
}}

==References==
{{Reflist}}
*{{cite book |last= |first= |authorlink= |coauthors= |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982–1985)|year= |publisher= Orbis Publishing|location= |issn=}}
* {{cite book|last=Alfred|first=Lawson|title=200 Mile Trip in First Airliner|year=1928|url=https://books.google.com/books?id=Bb1BAAAAYAAJ&lpg=PA146&ots=LLFq3DGaME&dq=%222000%20mile%20trip%20in%20first%22%20alfred%20lawson&pg=PA146#v=onepage&q=%222000%20mile%20trip%20in%20first%22%20alfred%20lawson&f=false}}
* {{cite book|title=Lawson – Aircraft Industry Builder|year=1930|publisher=Humanity publishing company}}

==External links==
{{commons category-inline|Lawson L-4}}

[[Category:United States airliners 1920–1929]]
[[Category:Trimotors]]
[[Category:Lawson aircraft|L-4]]
[[Category:Biplanes]]