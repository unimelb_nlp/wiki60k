{{pp-move-indef}}
{{Infobox journal
| title = Energy & Environment
| cover = 
| editor = [[Sonja Boehmer-Christiansen]]
| discipline = [[Natural environment|Environment]], [[climate change]], [[energy economics]], [[energy policy]]
| abbreviation = Energ. Environ.
| publisher = Multi-Science
| country = 
| frequency = 8/year
| history = 1989-present
| openaccess =
| license =
| impact = 0.319
| impact-year = 2012
| website = http://www.multi-science.co.uk/ee.htm
| link1 = http://multi-science.metapress.com/content/121493/
| link1-name = Online access
| JSTOR =
| OCLC = 21187549
| LCCN = 2003210598
| CODEN = EENVE2
| ISSN = 0958-305X
| eISSN = 
}}
'''''Energy & Environment''''' (''E&E'') is an [[academic journal]] "covering the direct and indirect environmental impacts of energy acquisition, transport, production and use".<ref name=homepage>[http://www.multi-science.co.uk/ee.htm "Energy & Environment".] Accessed: May 19, 2012.</ref> Its [[editor-in-chief]] since 1998 is [[Sonja Boehmer-Christiansen]].

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]],<ref>{{cite web |title=Master Journal list |publisher=[[Thomson Reuters]] |url=http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0958-305X |accessdate=2011-05-09}}</ref> [[Scopus]],<ref>{{cite web |publisher=[[Scopus]] |url=http://www.scopus.com/source/sourceInfo.url?sourceId=29360 |title=Energy and Environment}}, accessed 24 June 2015 (subscription required)</ref> [[EBSCO Publishing|EBSCO databases]],<ref>{{cite web |title=Environment Index: Database Coverage List |publisher=[[EBSCO Industries|EBSCO]] |url=http://www.ebscohost.com/titleLists/egh-coverage.pdf |accessdate=2009-11-30}}</ref> [[Current Contents]]/Social & Behavioral Sciences, and [[Compendex]].<ref name=homepage /> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.319, ranking it 90th out of 93 journals in the category "Environmental Studies".<ref name=WoS2>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Environmental Studies |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== Objective ==
The journal's mission statement states that the publication's "objective is to inform across professional and disciplinary boundaries and debate the social, economic, political and technological implications of [[environmental law|environmental controls]], as well as interrogate the science claims made to justify environmental regulations of the [[energy industry|energy industries]], including [[transport]]."<ref name=mission>{{cite web |url=http://www.multi-science.co.uk/ee-mission.htm |title=Energy & Environment: Mission Statement |publisher=Multi-Science Publishing |accessdate=2012-08-11}}</ref>

== History ==
''Energy & Environment'' was first published in 1989; David Everest ([[Department of the Environment (UK)|Department of the Environment]], United Kingdom) was its founding editor. Following his death in 1998, Boehmer-Christiansen became the journal's editor. She and several members of the journal's editorial advisory board previously had been associated with "the Energy and Environment Groups" at the [[Science and Technology Policy Research|Science and Technology Policy Unit]] ([[University of Sussex]]), with John Surrey.<ref name=mission /> [[Benny Peiser]] has served as co-editor.<ref>{{cite web |url=http://www.multi-science.co.uk/ee.htm |title=Energy & Environment |publisher=Multi-Science Publishing |archiveurl=https://web.archive.org/web/20100819084743/http://www.multi-science.co.uk/ee.htm |archivedate=August 19, 2010 |accessdate=2012-08-11}}</ref>

== Criticism ==
According to a 2011 article in ''[[The Guardian]]'', [[Gavin Schmidt]] and [[Roger A. Pielke, Jr.]] said that ''E&E'' has had low standards of peer review and little impact.<ref name=guardian-2011>{{cite news|last=Barley|first=Shanta|title=Real Climate faces libel suit|url=https://www.theguardian.com/environment/2011/feb/25/real-climate-libel-threat|accessdate=11 June 2011|newspaper=[[The Guardian]]|date=February 25, 2011}}</ref> In addition, [[Ralph Keeling]] criticized a paper in the journal which claimed that CO<sub>2</sub> levels were above 400 ppm in 1825, 1857 and 1942, writing in a letter to the editor, "Is it really the intent of E&E to provide a forum for laundering pseudo-science?"<ref name=guardian-2011/><ref>{{cite journal | title=Comment on "180 years of atmospheric CO2 gas analysis by chemical methods" by ernst-georg beck | author=Keeling, Ralph | journal=Energy & Environment |date=September 2007  | volume=18 | issue=5 | pages=635–641 | doi=10.1260/0958-305x.18.5.635}}</ref> A 2005 article in ''[[Environmental Science & Technology]]'' stated that "scientific claims made in Energy & Environment have little credibility among scientists."<ref>{{cite web | url=http://www.realclimate.org/docs/thacker/skeptics.pdf | title=Skeptics get a journal | publisher=[[American Chemical Society]] | work=[[Environ. Sci. Technol.]] | date=31 August 2005 | accessdate=26 February 2014 | author=Thacker, Paul D.}}</ref> Boehmer-Christiansen acknowledged that the journal's "impact rating has remained too low for many ambitious young researchers to use it", but blamed this on "the negative attitudes of the [[Intergovernmental Panel on Climate Change]] (IPCC)/[[Climatic Research Unit]] people."<ref>[http://www.publications.parliament.uk/pa/cm200910/cmselect/cmsctech/memo/climatedata/uc2602.htm Memorandum submitted by Dr Sonja Boehmer-Christiansen (CRU 26)]</ref>

== Climate change skepticism ==
When asked about the publication in the Spring of 2003 of a revised version of the paper at the center of the [[Soon and Baliunas controversy]], Boehmer-Christiansen said, "I'm following my political agenda -- a bit, anyway. But isn't that the right of the editor?"<ref>{{cite journal|last=Monastersky|first=Richard|title=Storm Brews Over Global Warming|journal=[[Chronicle of Higher Education]]|date=September 5, 2003|url=http://chronicle.com/article/Storm-Brews-Over-Global/27779/}}</ref> 

Part of the journal's official [[mission statement]] reads: "E&E has consistently striven to publish many ‘voices’ and to challenge conventional wisdoms. Perhaps more so than other European energy journal, the editor has made E&E a forum for more sceptical analyses of ‘climate change’ and the advocated solutions".<ref name=mission/>

== See also ==
* [[Environmental engineering science]]
* [[List of books about renewable energy]]

== References ==
{{Reflist|30em}}

==External links==
* {{official website|http://www.multi-science.co.uk/ee.htm}}

{{DEFAULTSORT:Energy and Environment}}
[[Category:Energy and fuel journals]]
[[Category:English-language journals]]
[[Category:Environmental social science journals]]
[[Category:Energy economics]]
[[Category:Publications established in 1989]]
[[Category:Environmental skepticism]]