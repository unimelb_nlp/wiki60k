{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox church
|name                   = St Peter's Cathedral
|fullname               = Cathedral Church of St Peter
|image                  = St Peters Cathedral.JPG
|imagesize              = 300
|caption                =
|coordinates            = {{coord|34|54|46|S|138|35|53|E|type:landmark_region:AU-SA|display=inline,title}}
|location               = [[Adelaide]], [[South Australia]]
|country                = Australia
|denomination           = [[Anglican Church of Australia|Anglican]]
|churchmanship          = 
|membership             =
|attendance             =
|website                = {{URL|stpeters-cathedral.org.au}}
|bull date              =
|founded date           = 29 June 1869
|founder                =
|dedication             = [[St Peter the Apostle]]
|dedicated date         =
|events                 =
|past bishop            =
|people                 =
|functional status      = Active
|heritage designation   = Historic
|designated date        = 1980
|architect              = [[William Butterfield]] & [[Edward John Woods]]
|style                  = [[Gothic Revival architecture|Gothic Revival]]
|completed date         =
|construction cost      =
|materials              =
|bells                  = 8
|bells hung             = 
|bell weight            = 41cwt (2.09 tonnes)
|diocese                = [[Anglican Diocese of Adelaide|Adelaide]]
|province               = [[Province of South Australia|South Australia]]
|archbishop             = [[Geoffrey Smith (bishop)]] (commencing 2017)
|bishop                 = Tim Harris
|dean                   = Frank Nelson
|precentor              = Jenny Wilson
}}

'''St Peter's Cathedral''' is an [[Anglican]] [[cathedral]] in the [[South Australia]]n capital of [[Adelaide]]. It is the seat of the Archbishop of [[Anglican Diocese of Adelaide|Adelaide]] and [[Metropolitan bishop|Metropolitan]] of the Province of South Australia.<ref>Reed, p.48.</ref> The cathedral, a significant Adelaide landmark, is situated on approximately {{convert|1|acre|sqm}} of land at the corner of Pennington Terrace and [[King William Street, Adelaide|King William Road]] in the suburb of [[North Adelaide]].

The south front has similar features to the [[Notre Dame de Paris|Cathedral of Notre Dame]] and the Church of St Jean-Baptiste de Belleville in [[Paris]], including an ornate [[rose window]] above the main entrance which depicts stories of South Australia and the [[Bible]].

==Foundation and construction==

[[Image:StPeter 2179.jpg|right|thumb|St Peter's Cathedral, south front]]
[[File:Wet and Grey (9945539375).jpg|thumb|The cathedral from [[King William Street, Adelaide|King William Road]], September 2013]]
The [[Anglican Diocese of Adelaide|See of Adelaide]] was constituted in June 1847. As there was no cathedral, [[Holy Trinity Church, Adelaide|Trinity Church]] on North Terrace was denoted as the ''[[pro tempore]]'' cathedral church. [[Augustus Short]], the first Bishop of Adelaide, held the first [[ordination]]s there on 29 June 1848 ([[Feast of Saints Peter and Paul|St Peter's feast day]]).<ref>Reed, p. 6.</ref> When Adelaide was surveyed by [[Colonel William Light]] over a decade before, land in [[Victoria Square, Adelaide|Victoria Square]] had been set aside for public use. Bishop Short obtained a [[land grant]] in the square from Governor [[Frederick Robe|Robe]] in March 1848; the grant was registered on 23 April 1851.<ref>Reed, p. 8.</ref> By late 1849 a subscription was bringing in funds for construction of a cathedral on the now cleared site. Around this time the legality of the land grant began to be publicly questioned. It was argued that the area was a [[Park|public reserve]] and the governor had no power to issue such grants. To resolve matters Bishop Short, supported by the [[Synod]], took the matter to the [[Supreme Court of South Australia|Supreme Court]].<ref>Reed, p. 9.</ref> The judgment in June 1855 confirmed that the grant was invalid and construction could not proceed.<ref>Reed, p. 11.</ref>

Bishop Short purchased just over an acre of land, on Pennington Terrace in North Adelaide, on 8 August 1862.<ref>{{cite news |url=http://nla.gov.au/nla.news-article31990759 |title=St Peter's Cathedral, Adelaide. |newspaper=[[The Advertiser (Adelaide)|The South Australian Advertiser (Adelaide, SA : 1858–1889)]] |location=Adelaide, SA |date=30 June 1869 |accessdate=8 December 2014 |page=2 |publisher=National Library of Australia}}</ref> He reported in 1868 that the funds gathered were sufficient and announced to the diocese's synod of his decision to begin construction of a cathedral.<ref>Reed, p.14.</ref> Bishop Short had [[William Butterfield]] design the cathedral, but the long communication gap between England and Adelaide contributed to delays and disagreement. Butterfield's plans were purchased and given to [[Edward John Woods]], of Adelaide architectural firm Wright, Woods and Hamilton, for completion. Woods changed some of the plans' materials and design, while keeping the general details as Butterfield had proposed.<ref>Reed, p. 15.</ref> Woods was noted by his colleague [[Walter Bagot (architect)|Walter Bagot]] as strongly influenced by French architect [[Eugène Viollet-le-Duc]], and had imbued a French [[Gothic Revival architecture|Gothic]] character in many elements of its design. Bishop Short laid the foundation stone, a {{convert|13|long cwt|t|1}} block from Glen Ewin [[Quarry]], on St Peter's Day 1869 in front of over a thousand people. Brown and Thompson contracted for the building work, which progressed slowly. The first service was held on St Peter's Day 1876, though the building was incomplete. Synod meetings and regular services began in May 1877.<ref>Reed, pp.16&ndash;17.</ref>

The women of the diocese had raised [[Pound sterling|£1,200]] to purchase an [[Organ (music)|organ]] which was installed in 1877. The first part of the cathedral was [[Consecration|consecrated]] on 1 January 1878. By the time Bishop Short retired in late 1881, £18,000 received from many donors had been spent. Much of the furnishing was also donated including [[stained glass]] windows, a [[Baptismal font|marble font]], the [[chancel]]'s [[Tessellation|tessellated]] pavement and an [[altar]].<ref>Reed, p.19.</ref> Work began again in 1890 during the tenure of Bishop [[George Wyndham Kennion]]. Governor [[Algernon Keith-Falconer, 9th Earl of Kintore|the Earl of Kintore]] laid a foundation stone, {{convert|1.5|LT|t|1}} of [[Monarto, South Australia|Monarto]] South [[granite]], on 27 September. The ceremony included Masonic Honors as the Governor was [[Grand Master (Masonic)|Grand Master]] of the South Australian [[South Australian Lodge of Friendship|Freemasons]]. The congregation raised funds and Woods was again contracted as architect. Over £10,000 was spent beginning the two towers and the western part of the nave, and completing the northern porch. Building work ceased in 1894 when funds were exhausted and did not resume for some years.<ref>Reed, p.20.</ref>

A £4,000 [[bequest]] came from Sir [[Thomas Elder]] in 1897. This and other smaller amounts from offerings and gifts were added to the building fund. A tender was awarded in 1899 to complete the [[nave]] and bring the towers to roof height. The [[Society for Promoting Christian Knowledge]] donated £1,000, conditional on completion of work by 1902, and in 1900 [[Robert Barr Smith]] donated £10,000 to enable completion of the towers, spires and creation of an [[apse]] at the chancel end. The [[Duke of York|Duke]] and [[Duchess of York|Duchess]] of York (later [[George V of the United Kingdom|King George V]] and [[Mary of Teck|Queen Mary]]) were present when the nave was consecrated and a [[Boer War]] memorial unveiled on 14 July 1901. A dedication ceremony for the towers and spires was held on 7 December 1902, and the last [[scaffolding]] removed two months later. The south porch and some temporary [[Vestry|vestries]] were subsequently built, in addition to a crypt under the Lady chapel. A consecration ceremony was held on 7 April 1904; this marked completion of the cathedral's external structure. Records show that the work from the 1890s to date had cost somewhat over £25,000.<ref>Reed, pp.21&ndash;22.</ref>

==Structure==

[[File:St Peter's Cathedral Adelaide.jpg|thumb|right|St Peter's Cathedral from the [[Creswell Gardens]], due south]]
[[File:St Peter's Cathedral during a special service.jpg|thumb|right|St Peter's Cathedral during a special service]]
[[File:St Peter's Cathedral Nave and Aisle.jpg|thumb|right|Nave]]
[[File:Reredos in St Peter's Cathedral.jpg|thumb|right|Reredos and high altar]]
[[File:St Peter's Cathedral Organ Console.jpg|thumb|right|Organ console]]
[[File:St Peter's Cathedral Organ.jpg|thumb|right|1929 organ, front wooden facade only partly completed]]
[[File:Magdalen Window.jpg|thumb|right|Magdalene window, installed in 2001]]

The cathedral's interior is {{convert|203|ft|m|1}} long of which the Lady chapel occupies ? and the nave and chancel the remainder. The nave is {{convert|59|ft|m|1}} wide and, at the top of the spire's crosses, the cathedral rises {{convert|168|ft|m|1}} from ground level. Hammer dressed Tea Tree Gully sandstone&mdash;from what is now [[Anstey Hill Recreation Park]]&mdash; was used in the [[sanctuary]], [[Choir (architecture)|choir]], [[transept]]s and part of the nave. Stone used for the [[Quoin (architecture)|quoins]] is lighter in colour and came from the same area as that used in the [[Adelaide Town Hall]]. The building's base and some of the interior uses stone from [[Glen Osmond]] in the [[Adelaide Hills]]. Other parts of the cathedral use stone from [[New Zealand]], [[Pyrmont, New South Wales]] and [[Murray Bridge, South Australia|Murray Bridge]].<ref>Reed, pp. 22&ndash;23.</ref>

A [[reredos]]&mdash;a decorative structure behind the altar&mdash;was installed in 1904. It was finished considerably later and dedicated on 6 March 1910 by Bishop [[Arthur Nutter Thomas]].<ref name=reed24>Reed, p.24.</ref> The reredos, containing 23 coloured and gilded panels plus carved figures, is {{convert|34|ft|m|1}} high.<ref name=reed24 /> This structure was built at [[St Sidwells]] Art Works, Tiverton, England to a design by T. H. Lyon.<ref>Reed, p.26.</ref> The reredos features Christ in the centre panel, with saints filling the surrounding niches. Four coloured panels below the figure of Christ depict events in the life of St Peter, the patron saint of the cathedral. The cathedral has significant fine-quality stained glass windows. [[James Powell and Sons]] made three that were unveiled in the Lady chapel in November 1900. The southern transept window is the largest stained glass window in the cathedral and was dedicated in August 1926. A window representing [[Saint Cecilia]], patron saint of church music, was unveiled in 1876 in the pulpit side of the chancel, though by 1969 it was ironically concealed by the organ. Windows elsewhere were funded and influenced by bequests. Amongst these [[Charles Beaumont Howard]]'s memory is commemorated by a window in the chancel, Sir [[Anthony Musgrave]]'s daughter Joyce Harriet by one in the sacristy and churchman Richard Bowen Colley by another window in the chancel.<ref>Reed, pp.27&ndash;31.</ref> The remains of the [[England|English]] [[biology|biologist]] and [[statistics|statistician]] [[Ronald Fisher]] are interred within the cathedral.<ref name="samhs">{{cite web|url=http://samhs.org.au/Virtual%20Museum/Notable-individuals/rafisher/index-rafisher.htm|title=Ronald Aylmer Fisher|publisher=samhs.org.au|accessdate=21 March 2016}}</ref>

The stalls for the canons and choir, along with the original bishop's throne (now used as the dean's stall) and pulpit canopy, were a gift to the cathedral from Sir [[John Langdon Bonython]] in memory of his wife. Made in Adelaide in 1926, carvings can be seen on the capitals at each end of the stalls and also around the upper work in the canopies above.

In the western tower is the cathedral's [[ring of bells|ring of eight bells]], hung for [[change ringing]]. Their purchase was funded by a bequest from cathedral warden Frederick Allen Wakeman. They were cast by [[John Taylor & Co]] of [[Loughborough]] England in 1946 and were dedicated by Bishop Robin on 29 June 1947. With the tenor (largest) bell weighing just over {{convert|41|long cwt|t|1}} they are the heaviest ring of eight bells in the Southern Hemisphere, and the second heaviest ring of eight in the world after [[Sherborne Abbey]] in England.<ref>Reed pp.33&ndash;34.</ref><ref>{{cite web|url=http://dove.cccbr.org.uk/detail.php?searchString=Adelaide+Cath&Submit=++Go++&DoveID=ADELA+++CA|title=Adelaide—Cath Ch of S Peter|work=[[Dove's Guide for Church Bell Ringers]]|date=9 February 2006|accessdate=16 November 2009}}</ref> The bells are rung by members of [[The Australian and New Zealand Association of Bellringers]] who also operate the Adelaide Ringing Centre of 8 dumb-bells for training which opened in 2012.<ref>{{cite web|title=Adelaide, St Peter's Cathedral|url=http://www.anzab.org.au/SA/Adelaide-StPeter%27s.html|publisher=[[The Australian and New Zealand Association of Bellringers]]|accessdate=5 April 2014}}</ref>

==Music==
St Peter's Cathedral has a long and distinguished musical reputation in Adelaide. St Peter's Cathedral Choir has been singing services in the cathedral for over 130 years. It is the only choir of children and adults of its type in Adelaide. Currently, the choir sings three choral services per week (Solemn Eucharist and Evensong every Sunday, plus Evensong on Wednesdays during school terms) in addition to regular concert performances and other special services. In 2006/7 and 2010/11, the choir toured internationally to the UK and Italy. In 2014/15, the choir undertook their third international tour to the UK and France.

===Directors of music and masters of the choristers===
Historically, the master of the choristers was also the principal organist. In 1994, the title was changed to director of music and the role was split from principal organist.
*1996–present: Leonie Hempton OAM
*1994–1996: Peter Leech
*1963–1994: John David Swale OAM
*1963–1964: Lyall Von Einem (acting)
*1955–1962: Jack Vernon Peters
*1936–1955: Horace Percy Finnis
*1891–1936: [[John Millard Dunn]]
*1876–1891: Arthur Boult

===Organists===
*2015–present: Joshua van Konkelenberg
*1999–2015: Shirley Gale
*1994–1999: John David Swale

===Organ===
The cathedral's original organ was built by the London firm of Bishop & Son. It was installed in 1877 and dedicated on 1 January 1878. Located in what is now the sacristy, it was used for over fifty years before relocation in 1930 to [[St Augustine's Church, Unley|St Augustine's Church]] in [[Unley, South Australia]]. The current organ was built by [[William Hill & Son & Norman & Beard Ltd.|William Hill & Son and Norman & Beard]] of Melbourne and London and was dedicated on 29 July 1929. It has an electro-pneumatic action, four manuals and fifty speaking stops, featuring 26 couplers. It remains largely unaltered, though two additions have been made, the addition of a Mixture V stop on the Great (1986) and the 32' Contra Trombone to the pedal division (1989).<ref>Reed p.35.</ref><ref>{{cite web | url=http://www.ohta.org.au/organs/organs/AdelaideStPetersCath.html| title=St Peter's Anglican Cathedral cnr King William Road & Pennington Terrace, North Adelaide | accessdate=14 November 2009 | publisher=Organ Historical Trust of Australia}}</ref> The lowest 12 pipes of this rank are located on a wall above the sacristy, opposite the main organ case. A carved wooden case for the organ, designed by Walter Bagot, was only ever partly installed.

==Clergy==
The Bishop of Adelaide was the formal rector of the cathedral until 1986. The cathedral was governed by the bishop with assistance from a dean (the Dean of Adelaide) and chapter. The dean and chapter operated as an independent body, meaning they were not required to report to the synod of the Diocese of Adelaide. In 1986, the administrative functions of the dean and chapter were transferred to the cathedral council. Since 1986, the cathedral has been administrated as a parish with the dean (known as the Dean of the Cathedral) as rector. The Dean of the Cathedral is not synonymous with the Dean of Adelaide (which has been vacant since 1997).<ref>St Peter's Cathedral Handbook (2008)</ref>

===Deans===
*2012–present: [[Frank Nelson (priest)|Frank Nelson]]<ref>{{cite web | url = http://www.adelaide.anglican.com.au/communication/media-releases-2/2012/cathedral-dean/|title= Media Release from the Anglican Archbishop of Adelaide Jeffrey Driver|publisher= Anglican Diocese of Adelaide|accessdate = 26 December 2012}}</ref> (previously [[Saint Paul's Cathedral, Wellington|Dean of Wellington]], New Zealand)
*2009–2011: [[Sarah Macneil]]<ref>{{cite web| url = http://www.adelaidenow.com.au/first-female-dean-of-adelaide-quits/story-e6frea6u-1226072013415| title= First female Dean of Adelaide quits|publisher= Adelaide Now|accessdate = 26 December 2012}}</ref> (now Bishop of Grafton)
*2000–2008: Steven Ogden<ref>{{cite web|url = http://www.adelaide.anglican.com.au/assets/Uploads/Media-Releases/2008/2008-12-03Dean-and-Archdeacon-to-farewell-diocese.pdf| title= Cathedral Dean and Archdeacon say Goodbye|accessdate = 26 December 2012}}</ref>
*1999–2000: Stuart Smith (acting)
*1987–1998: [[David Richardson (priest)|David Richardson]]

===Bishop's vicar===
*1982–1987: Keith Chittleborough
*1966–1982: Lionel Edward W. Renfrey (also Assistant Bishop of Adelaide 1969–1985)
*1964–1966: William Devonshire
*1963–1964: Patrick Austin Day (became Rector of [[Christ Church St Laurence]], Sydney)
*1957–1963: Arthur Weston
*1955–1957: Thomas Thornton Reed (became Bishop of Adelaide in 1957)
*1927–1955: Horace Percy Finnis
*1890–1927: William Somerville Milne
*1878–1890: Arthur Dendy

==Notes==
{{reflist|2}}

==References==
*{{cite book|title=A history of the cathedral church of St. Peter Adelaide|last=Reed|first=Thomas T|year=1969|publisher=Lutheran Press|location=Adelaide}}

==External links==
*[http://www.stpeters-cathedral.org.au/ Official website]

{{Anglican Church in Australia}}
{{Adelaide landmarks}}

{{DEFAULTSORT:Saint Peter's Cathedral, Adelaide}}
[[Category:Churches in Adelaide]]
[[Category:Anglican cathedrals in Australia]]
[[Category:Tourist attractions in Adelaide]]
[[Category:Gothic Revival architecture in Adelaide]]
[[Category:Deans of Adelaide]]
[[Category:Anglicanism]]
[[Category:19th-century Anglican churches]]
[[Category:Gothic Revival churches in Australia]]
[[Category:South Australian Heritage Register]]
[[Category:Cathedrals in South Australia]]