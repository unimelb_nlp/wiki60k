[[File:NiclotDoglio 11.gif|thumb|right|Furio Niclot Doglio]]
'''Furio Niclot Doglio''', [[Gold Medal of Military Valor|MOVM]] (24 April 1908 – 27 July 1942) was an Italian [[test pilot]] and [[World War II]] fighter pilot in the ''[[Regia Aeronautica]]''. Doglio set nine world aviation records<ref name=fai1>[http://records.fai.org/pilot.asp?from=c&id=4323 "FAI records database for Doglio."] ''FAI.'' Retrieved: 31 October 2010.</ref> in the 1930s during his time as a test pilot. During the war, he claimed seven kills (six of them Spitfires), flying [[FIAT G. 50]]s and [[Macchi C. 202]]s, establishing himself as one of Italy's [[Flying ace|aces]]. Doglio was killed in combat on 27 July 1942 during the [[Siege of Malta (World War II)|Siege of Malta]] by [[George Beurling]], who became Canada's top ace of the war. He was awarded a ''Medaglia d'oro al Valor Militare alla memoria'' (Memorial Golden medal for military valour).

==Early life==
Furio Niclot Doglio was born in [[Turin]], [[Piedmont]]. During the early to mid-1930s, he worked as a test pilot for Italian aircraft manufacturers ''[[Compagnia Nazionale Aeronautica]]'' (CNA) and [[Società Italiana Ernesto Breda|Breda]].

===Records===
Doglio set nine official aviation world records (as recognized by the [[Fédération Aéronautique Internationale]] or FAI, the aviation world record adjudicating body).<ref name=fai1/>

{| class="wikitable" style="text-align:center;"
|+ 
! Date !! Aircraft !! FAI Class !! Record Event !! Record
|-
| 28 December 1932 || [[Fiat AS.1]] ||  C.bis 1st category (seaplane) || Altitude || {{convert|7362|m|ft|abbr=on}}
|-
| 6 November 1933 || [[Compagnia Nazionale Aeronautica|CNA]] [[CNA Eta|Eta]] || C.bis 2nd category (seaplane) || Altitude || {{convert|8411|m|ft|abbr=on}}
|-
| 24 December 1933 || CNA Eta || C 2nd category (landplane) || Altitude || {{convert|10008|m|ft|abbr=on}}
|-
| 1 April 1937 || [[Breda Ba.88]] || C (landplane) || Speed, given distance of {{convert|100|km|mi|abbr=on}} || {{convert|517.84|km/h|mph|abbr=on}}
|-
| 10 April 1937 || Breda Ba.88 || C (landplane) || Speed over {{convert|1000|km|mi|abbr=on}} || {{convert|475.55|km/h|mph|abbr=on}}
|-
| 5 December 1937 || Breda Ba.88 || C (landplane) || Speed, given distance of {{convert|100|km|mi|abbr=on}} || {{convert|554.36|km/h|mph|abbr=on}}
|-
| 9 December 1937 || Breda Ba.88 || C (landplane) || Speed, closed circuit of {{convert|1000|km|mi|abbr=on}} w/ {{convert|500|kg|lb|abbr=on}} payload || {{convert|524.19|km/h|mph|abbr=on}}
|-
| 9 December 1937 || Breda Ba.88 || C (landplane) || Speed, closed circuit of {{convert|1000|km|mi|abbr=on}} w/ {{convert|1000|kg|lb|abbr=on}} payload || {{convert|524.19|km/h|mph|abbr=on}}
|-
| 9 December 1937 || Breda Ba.88 || C (landplane) || Speed, closed circuit of {{convert|1000|km|mi|abbr=on}} || {{convert|524.19|km/h|mph|abbr=on}}
|}

==World War II==
When Italy entered World War II on 10 June 1940, Doglio enlisted in the ''Regia Aeronautica.''  His first posting was the 355a Squadriglia of 21° Gruppo. On 17 June, Niclot flew his first mission: a patrol over Rome, flying the [[Fiat G.50]].

===Corpo Aereo Italiano===
In autumn 1940, Doglio was in Belgium with the ''[[Corpo Aereo Italiano]]'', the Italian air expedition against England. Niclot carried out his first mission on 27 October, by escorting a [[Fiat BR.20]], on a mission to attack Ramsgate. During the whole campaign, Niclot, like the other Italian G.50 pilots, did not encounter enemy fighters, nor fire his guns.

===North Africa===
[[File:Bundesarchiv Bild 101I-425-0338-16A, Flugzeuge Fiat G.50 und Messerschmitt Me 110.jpg|A ''Regia Aeronautica'' G.50 flying with a ''Luftwaffe'' [[Messerschmitt Bf 110]] over North Africa in 1941. Flying this fighter, Doglio scored his first air victory against a [[Hawker Hurricane]]|thumb|right]]

Doglio's first "kill" was a [[Hawker Hurricane]], in North Africa, while he was flying a Fiat G.50. On 30 June 1941, [[Captain (land)|Captain]] Furio Niclot Doglio, while escorting [[Ju 87 Stuka]]s that were bombing an English convoy off Ras Azzas, attacked three Hurricanes that were bouncing the dive-bombers and shot down one, damaging the others. For this action, Niclot received a [[Medaglia di bronzo al Valore Militare]] (Bronze Medal for Military Valour).<ref>Massimello Giovanni. ''Furio Nicolot Doglio Un pilota eccezionale''. Milano: Giorgio Apostolo editore, 1998.</ref>

===Malta===
Doglio's other air victories were all claimed at Malta in July 1942, while flying the Macchi C. 202, as ''Capitano'' of 151a ''Squadriglia''.[[File:Mc 202 folgore.sized.jpg|thumb|Macchi C.202 of ''Regia Aeronautica'']] His first Spitfire was shot down on 2 July 1942. That day, while escorting three [[Savoia-Marchetti SM.84]], leading 10 MC.202s of 151a ''Squadriglia'', Doglio fought with Spitfires from 249 and 185 Squadron. During a head-on attack, he hit the Spitfire BR377 of ''Flight Sergeant'' C.S.G. De Nancrede from ''Squadron'' 249, that had to crash-land on Ta 'Qali airfield, near [[Mdina]].<ref name="Cull with Galea 2004, p. 111">Cull with Galea 2004, p. 111.</ref>

On the 6th, Doglio encountered again the Spitfires of 249 Squadron, while escorting three Cant.Z.1007 bis, and he claimed another Supermarine fighter, confirmed by his wingman Tarantola to crash north of Valletta, but the 249 that day had no losses, even if the Squadron had two aircraft shot up, one of them was flown by Sgt. Beurling, who three weeks later would kill Doglio in combat over [[Gozo]].<ref name="Cull with Galea 2004, p. 111"/> The following day, Niclot and seven other Macchi pilots were escorting for the first time the [[Junkers Ju 88|Ju 88A-4]] of ''Kampfgeschwader'' 77. In the sky over Luqa, they clashed with seven Spitfires. Niclot and his wingman shot down the Spitfire of Flt. Sgt. D. Ferraby from Squadron 249 (AB500). Niclot's last air victory was a double "kill": two Spitfires downed on 13 July 1942. 

On 27 July 1942, Doglio was leading three others Macchi, on the coast of Gozo. Six Spitfires of 126 Sq. attacked them head-on, while eight other Spitfires of the 249 Sq. attacked from left ("10 hour direction"). Niclot was preparing to counter-attack the Spitfires or 126 Sq. when his wingman, ''Sergente'' Ennio Tarantola, tried to warn his commander, waggling his wings, as Italian radios worked badly, of the Spitfires diving on them from the left, but Niclot understood that Tarantola was warning him of the Spitfires he had already spotted. 
[[File:BeurlingSpitVc.jpg|thumb|left|300 px|Beurling's Spitfire VC in which he shot down Doglio, on 27 July 1942]]

Fl. Sgt. George "Screwball" Beurling, from 249 Sq., first scored hits on ''Sergente'' Faliero Gelli's aircraft, who later crash landed on Gozo, and soon after shot down Doglio's C.202 (MM 9042), who was waggling his wings to warn his fellow pilots of Spitfires closing "head-on". "The poor devil simply blew to pieces in the air", Beurling recalled the following year, writing the book ''Malta Spitfire'', together with journalist Leslie Roberts.<ref>Beurling and Roberts 1944</ref>
[[File:George Beurling Vancouver 1943.jpg|thumb|150px|right|Flight Sgt. George Beurling who shot down and killed Doglio.]]

When he was killed, Doglio held the rank of ''Capitano'' and was the commanding officer of 151a ''Squadriglia'', 20° ''Gruppo'', 51° ''Stormo'', and was flying a [[Macchi C.202]], aircraft number 151-1.<ref name=ef51>[http://www.eaf51.org/Photo_08_Men51st.htm EAF51.org (in Italian)]</ref> In less than a month, July 1942, Niclot had flown 21 missions of war, over Malta, was involved in 18 air combats, claimed six aircraft shot down plus four more probable and two shared with his wingman, [[Ennio Tarantola]].<ref>Giovanni Massimello, ''Furio Niclot Doglio''. Milan: Giorgio Apostolo Editore, 1998</ref>

Doglio left a wife and two sons, Stefano and Gian Francesco, doctors, who are still alive and live in Northern Italy. In ''via Ravenna 7A'', near ''piazza Bologna'', [[Rome]], the house where Niclot lived with his family on the first floor, is still standing.

==Honors and tributes==
In Rome, in the entrance of the building built by himself and his father, in via Bolzano 14, a plaque in the hall displays the citation from Doglio's Gold Medal. The citation of the Gold Medal is also on a plaque on the Bonaria cimitery of Cagliari, Sardinia, where the local section of ''Arma Aeronautica'' is dedicated to Furio Niclot Doglio. In Fiumicino, near Rome, a street in the area of Isola Sacra is named: "Niclot".

===Awards===
In 1936, Doglio became one of the first recipients of the FAI's [[Louis Blériot medal]].<ref name=fai2>[http://www.fai.org/awards/award.asp?id=3 "Louis Blériot medal winner listing."] ''FAI.'' Retrieved: 31 October 2010.</ref> Doglio was awarded the ''[[Gold Medal of Military Valor|Medaglia d'oro al Valore Militare]]'' (MOVM) (Gold Medal for Military Valor),<ref name=ef51/> a Silver medal of aeronautical valour, a [[Silver Medal of Military Valor]] "on the field", two Bronze Medals and the [[Iron Cross]], Second Class (EK II. Klasse).

==See also==
*[[List of World War II aces from Italy]]

==References==
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* Beurling, George and Leslie Roberrts. ''Malta Spitfire: The Diary of a Fighter Pilot.'' London: Stackpole Books, 2002. ISBN 1-85367-487-7.
* Cull, Brian with Frederick Galea. ''249 at Malta: Malta top-scoring Fighter Squadron 1941-1943.'' Malta: Wise Owl Publication, 2004. ISBN 978-99932-32-52-0.
* Cull, Brian with Frederick Galea. "Spitfires over Malta". London: Grub Street, 2006. ISBN 978-1-904943-30-3.
* Massimello, Giovanni. ''Furio Niclot Doglio: Un Pilota Indimenticabile (in Italian and English)''. Milan: Giorgio Apostolo, 1998.
* Nolan, Brian. ''Hero: The Buzz Beurling Story.'' London: Penguin Books, 1981. ISBN 0-14-006266-1.
{{refend}}

==External links==
*[https://translate.google.com/translate?hl=en&sl=it&u=http://www.eaf51.org/newweb/Documenti/Storia/FND.pdf&sa=X&oi=translate&resnum=4&ct=result&prev=/search%3Fq%3DFurio%2BNiclot%26hl%3Den%26rlz%3D1C1GGLS_en-USUS294US303 Google translation of account of Doglio's death]
*[http://surfcity.kund.dalnet.se/falco_bob.htm History site which includes two photos of Doglio]

{{DEFAULTSORT:Niclot Doglio, Furio }}
[[Category:1908 births]]
[[Category:1942 deaths]]
[[Category:Italian World War II flying aces]]
[[Category:People from Turin]]
[[Category:Recipients of the Gold Medal of Military Valor]]
[[Category:Recipients of the Silver Medal of Military Valor]]
[[Category:Recipients of the Iron Cross (1939), 2nd class]]
[[Category:Italian military personnel killed in World War II]]