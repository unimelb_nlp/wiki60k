{{Infobox journal
| title = Behavioral Neuroscience
| cover = [[File:Behavioral_Neuroscience_journal_cover_image.jpg]]
| editor = Rebecca D. Burwell
| discipline = [[Behavioral neuroscience]]
| abbreviation =
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Bimonthly
| history = 1983-present
| openaccess =
| license =
| impact = 2.728
| impact-year = 2014
| website = http://www.apa.org/journals/bne/
| link1 = http://psycnet.apa.org/index.cfm?fa=browsePA.volumes&jcode=bne
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 8998034
| LCCN = 83644089
| CODEN = BENEDJ
| ISSN = 0735-7044
| eISSN = 1939-0084
}}
'''''Behavioral Neuroscience''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] published by the [[American Psychological Association]]. It was established in 1983 and covers research in [[behavioral neuroscience]].<ref>{{cite web |date=October 19, 2012 | url=http://www.apa.org/pubs/journals/bne/index.aspx |title=Behavioral Neuroscience|publisher=[[American Psychological Association]] |accessdate=2012-10-19}}</ref> The [[editor-in-chief]] is Rebecca D. Burwell ([[Brown University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by [[MEDLINE]]/[[PubMed]] and the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.728, ranking it 21st out of 51 journals in the category "Behavioral Sciences".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Behavioral Sciences |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Sciences |series=Web of Science}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.apa.org/journals/bne/}}

[[Category:American Psychological Association academic journals]]
[[Category:English-language journals]]
[[Category:Neuroscience journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1983]]


{{sci-journal-stub}}