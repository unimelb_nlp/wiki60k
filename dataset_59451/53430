{{Infobox accounting body
|abbreviation          = CMA Canada
|name_in_local_language=
|image                 = Cma_can_logo.gif
|image_border          =
|image_caption         = 
|motto                 = 
|predecessor           = 
|formation             = {{Start date and years ago|1920|05|03}}
|legal_status          = Incorporated by letters patent under the Canada Corporations Act, Part II
|objective             = Encourage professionalism in accountancy in Canada
|headquarters          = [[Mississauga, Ontario]], {{flag|Canada}}
|coords                = 
|region_served         = Canada, Bermuda, Hong Kong, Caribbean
|membership_num        = 50,000 
|students_num          = 10,000
|members_designations  = CPA, CMA, or FCPA, FCMA
|language              = English, French
|highest_officer_title = Chair
|highest_officer_name  = Cassandra Dorrington
|second_officer_title  = Vice Chair
|second_officer_name   = Lorri Lowe
|third_officer_title   = President and CEO
|third_officer_name    = Joy Thomas
|fourth_officer_title  = 
|fourth_officer_name   = 
|administering_organ   = Board of Directors
|IFAC_membership_date  = 1977
|additional_data_name1 = 
|additional_data1      = 
|additional_data_name2 = 
|additional_data2      = 
|additional_data_name3 =
|additional_data3      =
|num_staff             =
|website               = {{url|http://www.cma-canada.org}}
|remarks               =
}}

'''The Society of Management Accountants of Canada''' ({{lang-fr|La Société des comptables en management du Canada}}), also known as '''Certified Management Accountants of Canada''' ({{lang-fr|Comptables en management accrédités du Canada}}) and '''CMA Canada''', awards the [[Certified Management Accountant]] designation in [[Canada]].

==Activities==

CMA Canada, through its provincial and territorial affiliates, grants the CMA professional designation in accounting and is responsible for standards-setting, accreditation and the continuing professional development of CMAs.

CMAs apply expertise in accounting, management and strategy to ensure corporate accountability and help organizations maintain a long-term competitive advantage. CMA Canada offers executive development programs, online courses, and knowledge management publications.<ref>{{Cite web|title = CMA Canada Research Foundation |url = http://www.cma-canada.org/index.cfm/ci_id/15475/la_id/1.htm |accessdate = 2011-11-06}}</ref><ref>{{Cite web|title = Professional Development |url = http://www.cma-canada.org/index.cfm?ci_id=4602&la_id=1 |accessdate = 2011-11-06}}</ref>

Management Accounting Guidelines<ref>{{Cite web|title = Management Accounting Guidelines (MAGs) |url = http://www.cma-canada.org/index.cfm?ci_id=4614&la_id=1 |accessdate = 2011-11-14}}</ref> and Management Accounting Practices<ref>{{Cite web|title = Management Accounting Practices (MAPs) |url = http://www.cma-canada.org/index.cfm?ci_id=4616&la_id=1 |accessdate = 2011-11-14}}</ref> are published by CMA Canada to specify the best practice on key topics in management accounting. They are available for download free of charge or on CD for a nominal charge to CMAs, and can be purchased by non-members.<ref>{{Cite web|title = Business Resources |url = http://www.cma-canada.org/index.cfm?ci_id=1885&la_id=1 |accessdate = 2011-11-14}}</ref>

==History of the Society==

The mission of the Society has closely tracked the evolution from [[cost accounting]] to [[management accounting]] in Canada, and its distinction from [[financial accounting]]:<ref>{{Cite web|title = ''Professional dominance: The relationship between financial accounting and management accounting, 1926-1986'' |url = http://www.allbusiness.com/accounting-reporting/managerial-accounting/404769-1.html |author = Alan J.Richardson |publisher = Accounting Historians Journal |date = 2002-12-01|accessdate = 2011-11-08}}</ref>

* 1920 - Incorporation of '''The Canadian Society of Cost Accountants''', with head office in [[Hamilton, Ontario]]
* 1926 - Introduction of ''Cost and Management'', the predecessor of [http://cmamagazine.ca ''CMA Magazine'']
* 1930 - Name changed to '''The Canadian Society of Cost Accountants and Industrial Engineers'''
* 1941 - Formation of provincial societies in Ontario and Quebec, with the power to grant the newly established professional designation of '''Registered Industrial Accountant''' ("RIA")
* 1948 - Name changed to '''Society of Industrial and Cost Accountants of Canada'''
* 1968 - Name changed to '''The Society of Industrial Accountants of Canada'''
* 1977 - Name changed to '''The Society of Management Accountants of Canada'''
* 1985 - Introduction of the professional designation of '''Certified Management Accountant''' ("CMA"), with existing RIAs being [[Grandfather clause|grandfathered in]]
* 2004 - '''CMA''' is registered as a trademark by CMA Canada
* 2006 - '''Certified Management Accountant''' is registered as a trademark by CMA Canada
* 2007 - Incorporation of the CMA Canada Research Foundation
* 2009 - Introduction of ''The National Standard for Public Accounting for Certified Management Accountants'' to govern the affairs of CMAs that have entered into public practice<ref>{{Cite web|title = ''The National Standard for Public Accounting for Certified Management Accountants'' |url = http://www.cma-canada.org/index.cfm/ci_id/2550/la_id/1/document/1/re_id/0 |date = 2009-09-12 |accessdate = 2011-11-08}}</ref>

A merger was proposed between the [[Canadian Institute of Chartered Accountants]] and CMA Canada in 2004, but it did not go beyond exploratory talks. Discussions began again in mid-2011,<ref>{{Cite web|author = Jeff Buckstein|title = ''Two out of three in on merger talks'' |date = July 2011|url = http://www.thebottomlinenews.ca/index.php?section=article&articleid=519 |publisher = ''The Bottom Line''|accessdate = 2011-11-29}}</ref> which later expanded to include [[Certified General Accountants Association of Canada|CGA Canada]].<ref>{{Cite web|title = Your source for CA-CMA-CGA Merger Information |url = http://cpacanada.ca/ |accessdate = 2011-11-06}}</ref> A merger proposal was discussed at the provincial level of all three organizations for the creation of a national accounting body that would issue the [[professional certification|professional designation]] of [[Chartered Professional Accountant]].<ref>{{Cite web|title = Case for unity to be laid out|url = http://www.thebottomlinenews.ca/index.php?section=article&articleid=564|publisher = ''The Bottom Line''|date = March 2012|accessdate = 2012-05-01}}</ref> CICA and CMA Canada joined together in 2013, to create Chartered Professional Accountants of Canada (CPA Canada)<ref>[http://www.cma-canada.org/index.cfm?ci_id=1194&la_id=1 Welcome CPA Canada]</ref>

==Competencies, accreditation process and post-qualification development==

CMAs are expected to undergo specific training and practical experience to achieve specified competencies in the area of strategic management accounting, which are intended to prepare them for senior leadership roles in their organizations.<ref name="CompMap">{{Cite web|title = ''The CMA Competency Map'' |url = http://www.cma-canada.org/index.cfm/ci_id/19412/la_id/1/document/1/re_id/0 |accessdate = 2011-11-09}}</ref> There are six functional competencies and four enabling competencies in that regard:

'''Functional competencies'''

* [[Strategic management]]
* [[Risk management]] and [[corporate governance|governance]]
* [[Performance management]]
* [[Performance measurement]]
* [[Finance|Financial management]]
* [[Financial reporting]]

'''Enabling competencies'''

* [[Problem solving]] and [[decision making]]
* [[Leadership]] and [[group dynamics]]
* [[Professionalism]] and [[ethics|ethical behaviour]]
* [[Communication]]

While candidates may come from a variety of backgrounds, the CMA career path is expected to progress in the following manner:<ref name="CompMap"/>

{| border="1" cellpadding="5" cellspacing="0"
|- 
| '''Stage''' || '''Phase''' || '''Nature'''
|-
| '''Acquisition'''
| Prior to passage of CMA Entrance Examination
| In process of completing undergraduate studies, or 5–7 years thereafter. Already in junior to [[middle management]] level in their organization.
|-
| '''Basic Proficiency'''
| Acquisition of the CMA designation and the following five years thereafter
| Already in [[middle management]] level, and progressively gaining more practical experience and responsibility.
|-
| '''Advanced Proficiency'''
| 5–10 years post-qualification
| Early- to mid-career professionals, with most having achieved more senior positions within the [[middle management]] layer. A significant number hold introductory [[senior management]] positions in their area of specialization or in the organization as a whole.
|-
| '''Mastery'''
| 10–25 years post-qualification
| Late-career professionals, operating in [[senior management]] roles. Most are either the highest-ranking financial officers in their organization or financial professionals with specialized knowledge in particular functional areas. A significant proportion of individuals are operating in the desired “C” space - i.e., [[Chief executive officer|CEO]], [[Chief financial officer|CFO]], [[Chief risk officer|CRO]] or [[Chief information officer|CIO]] - and some of these are leaders of large enterprises or significant [[strategic business unit]]s.
|}

CMA candidates (without advanced professional standing) must have obtained a university degree and have credits in a specified list of subjects, before they can write the CMA Entrance Examination.<ref>{{Cite web|title = ''CMA Entrance Examination'' |url = http://www.cma-canada.org/index.cfm/ci_id/4122/la_id/1/document/1/re_id/0 |accessdate = 2011-11-09}}</ref> Upon passing this stage, they then enter the Strategic Leadership Programme ("SLP"),<ref>{{Cite web|title = ''CMA Canada Strategic Leadership Program'' |url = http://www.cma-canada.org/index.cfm/ci_id/4123/la_id/1/document/1/re_id/0 |accessdate = 2011-11-09}}</ref> which has the following components:<ref>{{Cite web|title = ''The CMA Canada Accreditation Process'' |url = http://www.cma-canada.org/index.cfm/ci_id/4121/la_id/1/document/1/re_id/0 |accessdate = 2011-11-09}}</ref>

* ''Development Phase''
** followed by the successful passing of the '''CMA Case Examination'''
* ''Application Phase''
** leading to the successful preparation and presentation of the '''Board Report''', containing detailed analysis and recommendations on a specified case study
* Completion of 24 months of '''relevant full-time progressive practical experience confirmed by the employer''' - of which at least 12 months must be concurrent with the SLP
** Candidates are expected to be '''expanding their on-the-job responsibilities while undertaking the SLP'''

Successful passage of the above will result in the granting of the CMA designation. After attaining the designation, there are specified requirements for continuous professional learning and development that must be undertaken to remain in good standing.<ref>{{Cite web|title = ''The National Standard for Continuous Professional Learning and Development for certified Management Accountants'' |url = http://www.cma-canada.org/index.cfm/ci_id/5104/la_id/1/document/1/re_id/0 |accessdate = 2011-11-09}}</ref>

==Relationships with other professional organizations==

* Founding member of the [[International Federation of Accountants]] (IFAC).<ref>{{cite web
 |url=http://www.ifac.org/about-ifac/membership/members/certified-management-accountants-canada
 |title=Certified Management Accountants of Canada
 |publisher=IFAC
 |accessdate=2011-11-06}}</ref>

'''Strategic alliance'''

* [[Certified Management Consultant|Canadian Association of Management Consultants (CMC-Canada)]]<ref>{{Cite web|title = CMA Canada and CMC-Canada announce strategic alliance |url = http://www.cma-canada.org/index.cfm/ci_id/13583/la_id/1/document/1/re_id/0 |accessdate = 2011-11-06}}</ref>

'''Mutual recognition agreements in effect:'''<ref>{{Cite web|title = Mutual Recognition Agreements |url = http://www.cma-canada.org/index.cfm/ci_id/17431/la_id/1.htm |accessdate = 2011-11-06}}</ref>

* [[CPA Australia]]
* [[Chartered Institute of Management Accountants]]
* [[Chartered Institute of Public Finance and Accountancy]]

'''Other relationships'''<ref>{{Cite web|title = CAAA Partners, Patrons and Donors |url = http://www.cma-canada.org/index.cfm?ci_id=2545&la_id=1 |accessdate = 2011-11-06}}</ref>

{| border="1" cellpadding="5" cellspacing="0"
|- 
! scope="col" width="300" |  '''Organization'''
! scope="col" width="300"|  '''Description'''
|- 
| [[American Accounting Association]]
| Member
|- 
| [http://www.caaa.ca/ Canadian Academic Accounting Association]
| Leadership Partner<ref>{{Cite web|title = International Associates |url = http://www.caaa.ca/AboutCAAA/PartnersandSponsors/Currentsupporters/index.html |accessdate = 2011-11-09}}</ref>
|- 
| [http://www.cam-i.org/ Consortium of Advanced Management International (CAM-I)]
| Collaboration on accounting research studies
|- 
| [[American Institute of Certified Public Accountants]]
| rowspan="5"|members are exempt from taking the CMA Entrance Examination, and can directly enter the Strategic Leadership Programme<ref>{{Cite web|title = Mutual Recognition Agreements and Advanced Standing |url = http://www.cma-ontario.org/index.cfm?ci_id=7783&la_id=1 |accessdate = 2011-11-09}}</ref>
|- 
| [[Association of Chartered Certified Accountants]]
|- 
| [[Institute of Cost and Management Accountants of Pakistan]]
|- 
| [[Canadian Institute of Chartered Accountants]]
|- 
| [[Certified General Accountants Association of Canada]]
|- 
| [[Institute of Internal Auditors]]
|
|}

==Member societies==

{| border="1" cellpadding="5" cellspacing="0"
|- 
! scope="col" width="200" |  Jurisdiction
! scope="col" width="300"|  Society/Ordre
|- valign="top"
! scope="row" align="left"| {{flag|British Columbia}}
| [http://www.cmabc.com The Certified Management Accountants Society of British Columbia]
|- valign="top"
! scope="row" align="left"| {{flag|Alberta}}
| [http://www.cma-alberta.com Society of Management Accountants of Alberta]
|- valign="top"
! scope="row" align="left"| {{flag|Saskatchewan}}
| [http://www.cma-saskatchewan.com The Society of Management Accountants of Saskatchewan]
|- valign="top"
! scope="row" align="left"| {{flag|Manitoba}}
| [http://www.cma-manitoba.com Society of Management Accountants of Manitoba]
|- valign="top"
! scope="row" align="left"| {{flag|Ontario}}
| [http://www.cma-ontario.org Certified Management Accountants of Ontario]
|- valign="top"
! scope="row" align="left"| {{flag|Quebec}}
| [http://www.cma-quebec.org Ordre des comptables en management accrédités du Québec]
|- valign="top"
! scope="row" align="left"| {{flag|New Brunswick}}
| [http://www.cmanb.com Society of Management Accountants of New Brunswick]
|- valign="top"
! scope="row" align="left"| {{flag|Nova Scotia}}
| [https://web.archive.org/web/20111026143034/http://www.cma-ns.com:80/ Society of Management Accountants of Nova Scotia]
|- valign="top"
! scope="row" align="left"| {{flag|Prince Edward Island}}
| [http://www.cma-pei.com as for Nova Scotia]
|- valign="top"
! scope="row" align="left"| {{flag|Newfoundland and Labrador}}
| [https://web.archive.org/web/20111105011634/http://www.cma-nl.com:80/ Society of Certified Management Accountants of Newfoundland and Labrador]
|- valign="top"
! scope="row" align="left"| {{flag|Yukon}}
| [http://www.cmabc.com/index.cfm/ci_id/11635/la_id/1.htm as for BC]
|- valign="top"
! scope="row" align="left"| {{flag|Northwest Territories}}
| [https://web.archive.org/web/20120608042739/http://www.cma-nwt.com:80/ The Society of Management Accountants of the Northwest Territories]
|- valign="top"
! scope="row" align="left"| {{flag|Nunavut}}
| as for NWT
|- valign="top"
! scope="row" align="left"| {{flag|Bermuda}}
| [https://web.archive.org/web/20111115235144/http://www.cma-bermuda.com:80/ as for Nova Scotia]
|- valign="top"
! scope="row" align="left"| {{flag|Hong Kong}}
| [http://www.cmabc.com/index.cfm/ci_id/2379/la_id/1.htm as for BC]
|- valign="top"
! scope="row" align="left"| Caribbean
| [https://web.archive.org/web/20120102073955/http://www.cma-caribbean.com:80/ as for Nova Scotia]
|}

==Notable members==
{{Div col|cols=3}}
* [[Navdeep Bains]]
* [[Peter G. Christie]]
* [[Roy Megarry]]
* [[Frederick John Mitchell]]
* [[Harinder Takhar]]
* [[J. Grant Thiessen]]
* [[Rick Thorpe]]
{{Div col end}}

==References==
{{reflist}}
{{IFAC Members}}

{{DEFAULTSORT:Certified Management Accountants Of Canada}}
[[Category:Accounting in Canada]]
[[Category:Management accounting]]
[[Category:Canadian accounting associations]]
[[Category:Organizations established in 1920]]
[[Category:1920 establishments in Canada]]