{{Infobox Software
|logo = 
|screenshot = [[File:WebSite Admin Home.jpg|300px]] 
|caption = Screenshot of Web Site Administration Tool main page
|developer = 
|latest_release_version = 
|latest_release_date = 
|latest_preview_version = 
|latest_preview_date = 
|operating_system = 
|platform = 
|genre = 
|license = 
|website = 
}}
'''ASP.NET Web Site Administration Tool''' is a utility provided along with [[Microsoft Visual Studio]] which assists in the [[configure script (computing)|configuration]] and [[system administrator|administration]] of a [[website]] created using [[Microsoft Visual Studio#Visual Studio 2005|Microsoft Visual Studio 2005]] and later versions.<ref name="msdn_1">{{Cite web|url=http://msdn.microsoft.com/en-us/library/yy40ytx0(vs.80).aspx|title=ASP.NET Web Site Administration Tool|accessdate=2008-05-22|publisher=MSDN Visual Studio 2005 Development Center}}</ref>

==History==
The Web Site Administration tool was first introduced with [[ASP.NET 2.0]] along with ASP.NET Microsoft Management Console (MMC) Snap-in.<ref name="msdn_2">{{Cite web|url=http://msdn.microsoft.com/en-us/library/ms178687(VS.80).aspx|title=What's New in ASP.NET Configuration  |accessdate=2008-05-22|publisher=MSDN Visual Studio 2005 Development Center}}</ref>

==Interface==
ASP.NET Web Site Administration Tool can be accessed by clicking ASP.NET Configuration from the Website menu{{Clarify|post-text="In which version or edition of Visual Studio?|date=December 2012}} or Project menu in Visual Studio 2010 Professional, or by clicking on the ASP.NET Configuration icon in the Solution Explorer window.

Programmatic access to the features provided by the ASP.NET Web Site administration tool is made possible by inclusion of the ''System.Web.Security'' namespace in the ASP.NET program. The classes ''Membership'' and ''Roles'' are used to store, access and modify user information in the ASPNETDB database. The user could be authenticated using the ''Membership.ValidateUser'' or ''FormsAuthentication.Authenticate''<ref name="authentication_authenticate">{{Cite web|url=http://msdn.microsoft.com/en-us/library/system.web.security.formsauthentication.authenticate.aspx|title=FormsAuthentication.Authenticate Method |accessdate=2008-05-23|publisher=MSDN| archiveurl= https://web.archive.org/web/20080508181938/http://msdn.microsoft.com/en-us/library/system.web.security.formsauthentication.authenticate.aspx| archivedate= 8 May 2008 <!--DASHBot-->| deadurl= no}}</ref> methods. Page-based user authorization is realized by the usage of the ''AuthorizeRequest'' event of the ''HttpApplication'' class.<ref name="msdn_authorizerequest">{{Cite web|url=http://msdn.microsoft.com/en-us/library/system.web.httpapplication.authorizerequest.aspx|title=HttpApplication..::.AuthorizeRequest Event|accessdate=2008-05-23|publisher=MSDN}}</ref>

==Features==
The ASP.NET Web Site Administration tool is a multi-tabbed utility which has the following features:
* Web Site Administration Tool Security Tab 
* Web Site Administration Tool Application Tab 
* Web Site Administration Tool Provider Tab 
* Web Site Administration Tool Internals

===Security tab===
[[File:Website Admin Security.jpg|thumb]]

The security tab is used to create users and roles, group users under different roles and assign access rules either at the role-level or user-level.<ref name="security_tab">{{Cite web|url=http://msdn.microsoft.com/en-us/library/ssa0wsyf.aspx|title=Web Site Administration Tool Security Tab|accessdate=2008-05-23|publisher=MSDN}}</ref><ref name="expert_asp.net">{{cite book | title=Expert ASP.NET 2.0 Advanced Application Design: Advanced Application Design | url=https://books.google.com/books?id=RCVoZfzs6hwC&pg=PA191&lpg=PA191l&source=web&ots=dWcSWieXbi&sig=a6pm4m70IMmwJx5iqTLqenPDciQ&hl=en#PPA192,M1| last=Selly| first=Dominic|author2=Andrew Troelsen|author3=Tom Barnaby| date=2005| pages=191| publisher=Apress| isbn=159059522X}}</ref><ref name="webcast_asp.net">{{Cite web|url=http://www.asp.net/LEARN/videos/video-06.aspx|title=Lesson 9: Securing your Web Site with Membership and Login Controls|accessdate=2008-05-23| archiveurl= https://web.archive.org/web/20080519232647/http://www.asp.net/LEARN/videos/video-06.aspx| archivedate= 19 May 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="pro_asp.net">{{cite book | title=Pro ASP.NET 2.0 Website Programming | url=https://books.google.com/books?id=zzlhSgGtVe4C&pg=PA2&lpg=PA2&source=web&ots=h1BacPycnu&sig=qQb_EIKRBysBhw6Rr4Vb9jD-Ci8&hl=en#PPA2,M1| last=Armstrong| first=Damon| date=2005| pages=2–6| publisher=Apress| isbn=1590595467}}</ref> When the Web site administration tool is opened to modify the existing settings, a new database is created in the App_Data folder of the application.<ref name="security_tab" /> This database stores ASP.NET membership-related information. The name  of the database created is ASPNETDB by default.<ref name="security_tab" />

The security tab simplifies and optimizes user [[authentication]] and [[authorization]].<ref name="security_tab" /> It makes it comparatively easy to configure user permissions than code-based user-defined authentication systems which require a great amount of time, cost and manpower. However, a major drawback of this tool is that access rules could be defined only at the [[folder (computing)|folder-level]] and not at the [[page (computer memory)|page-level]].

===Application tab===
The Application tab is used to specify application settings, configure [[SMTP]] settings and enable or disable [[debugging]] and [[tracing (software)|tracing]] apart from other uses.<ref name="expert_asp.net" /> The Application tab interacts with the configuration file of the application ([[web.config]]) and not with the ASPNETDB database.<ref name="dotnet_config">{{Cite web|url=http://www.brainbell.com/tutorials/ASP/.NET_Configuration.html|title=.NET Configuration|accessdate=2008-05-23}}</ref> Application settings are created as objects and inserted as name-value pairs in the web.config file.<ref name="dotnet_config" />

===Provider tab===
The Provider tab is used to specify the database provider for the ASPNETDB database used to store ASP.NET membership and role information.<ref name="expert_asp.net" /><ref name="pro_asp.net" /> The security page does not appear unless and until the database provider is specified in the Providers tab. An SQL Data provider is generally used, but Oracle Data providers are also used in case of Oracle databases. The provider allows the user the option of  to store all data related to the ASP.NET Website Administration tool or different databases for each purpose.

==References==
{{reflist|2}}

[[Category:.NET Framework]]
[[Category:Microsoft Visual Studio]]