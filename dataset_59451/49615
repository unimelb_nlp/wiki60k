{{Other uses}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book
| name          = A Dog of Flanders
| image         = NelloPatrache.jpg
| caption =
| author        = [[Ouida|Marie Louise de la Ramée]]
| illustrator   =
| cover_artist  =
| country       = UK
| language      = English
| genre         = Drama, [[Tragedy]]
| published     = 1872 ([[Chapman and Hall]])
| media_type    = [[Printing|Print]] (Hardcover)
| pages         = 293 pp
| preceded_by   = <!--n/a-->
| followed_by   = <!--n/a-->
}}
'''''A Dog of Flanders''''' is an 1872 novel by English author [[Ouida|Marie Louise de la Ramée]] published with her pseudonym "[[Ouida]]". It is about a Flemish boy named Nello and his dog, Patrasche and is set in [[Antwerp]]. In [[Japan]] and [[Korea]] the novel has been a children's classic for decades and was adapted into several Japanese films and [[anime]].<ref name="beneluxguide.com">{{cite web|url=http://www.beneluxguide.com/belgium/nello-and-patrasche-the-antwerps-story-of-the-dog-of-flanders/|title=Nello and Patrasche – the Antwerp’s story of the Dog of Flanders|publisher=|accessdate=30 November 2016}}</ref> Since the 1980s, the Belgian board of tourism caught on to the phenomenon and built two monuments honoring the story to please East-Asian tourists. There is a small statue of Nello and Patrasche at the Kapelstraat in the Antwerp suburb of [[Hoboken (Antwerp)|Hoboken]], and a [[commemorative plaque]] in front of the [[Antwerp Cathedral]] donated by [[Toyota]].<ref name="beneluxguide.com"/>

== Summary==
[[File:Dogcart3.jpg|thumb|left|Children selling milk from a [[Dogcart (dog-drawn)|dogcart]], Belgium, ca. 1890]]
In 19th century Belgium, a boy named Nello becomes an orphan at the age of two when his mother dies in the [[Ardennes]]. His grandfather Jehann Daas, who lives in a small village near the city of Antwerp, takes him in.

One day, Nello and Jehann Daas find a dog who was almost beaten to death, and name him Patrasche. Due to the good care of Jehann Daas, the dog recovers, and from then on, Nello and Patrasche are inseparable. Since they are very poor, Nello has to help his grandfather by selling milk. Patrasche helps Nello pull their cart into town each morning.

Nello falls in love with Aloise, the daughter of Nicholas Cogez, a well-off man in the village, but Nicholas doesn't want his daughter to have a poor sweetheart. Although Nello is illiterate, he is very talented in drawing. He enters a junior drawing contest in Antwerp, hoping to win the first prize, 200 francs per year. However, the jury selects somebody else.

Afterwards, he is accused of causing a fire by Nicholas (the fire occurred on his property) and his grandfather dies. His life becomes even more desperate. Having no place to stay, Nello wishes to go to the [[Antwerp Cathedral|cathedral of Antwerp]] (to see [[Peter Paul Rubens|Rubens]]' [[The Elevation of the Cross (Rubens)|The Elevation of the Cross]] and [[The Descent of the Cross (Rubens)|The Descent of the Cross]]), but the exhibition held inside the building is only for paying customers and he's out of money. On the night of Christmas Eve, he and Patrasche go to Antwerp and, by chance, find the door to the church open. The next morning, the boy and his dog are found frozen to death in front of the [[triptych]].

==Popularity==

The novel shares a reasonable notability in the United Kingdom and the United States of America and is extremely popular in [[South Korea]] and [[Japan]], to the point where it is seen as a children's classic. It inspired film and [[anime]] adaptations, including the 1975 animated TV series ''[[Dog of Flanders (1975 TV series)|Dog of Flanders]]'' which reached an audience of 30 million viewers on its first broadcast.<ref name="argosarts.org">{{cite web|url=http://www.argosarts.org/work.jsp?workid=42b99ac823034ab7abac8b689b417e3d|title=ARGOS centre for art and media|publisher=|accessdate=30 November 2016}}</ref>

In Belgium the story is more obscure. Only in 1987 did it receive a Dutch translation; this happened after the tale was adapted into a story of the popular comic book series ''[[Suske en Wiske]]''. Since then, monuments were raised to commemorate Nello and Patrasche to please tourists. In 2007 Didier Volckaert and An van Dienderen directed a documentary about the international popularity of the story: "Patrasch, A Dog of Flanders - Made in Japan". It researches all available film adaptations of the story and interviews several British, American and Japanese people about what attracts them to this novel.<ref name="argosarts.org"/>

===Film, TV and theatrical adaptations===
[[File:PatracheTile.jpg|thumb|260px]]
The novel has been adapted for cinema and television in live-action and animation:
* [http://www.imdb.com/title/tt0154418 ''A Dog of Flanders'' (1914)], a short film directed by [[Howell Hansel]]
* [http://www.imdb.com/title/tt0014733 ''A Boy of Flanders'' (1924)], directed by [[Victor Schertzinger]] and starring [[Jackie Coogan]] as Nello
* [http://www.imdb.com/title/tt0026285 ''[[A Dog of Flanders (1935 film)|A Dog of Flanders]]'' (1935)], directed by [[Edward Sloman]]
* [http://www.imdb.com/title/tt0052745 ''[[A Dog of Flanders (1960 film)|A Dog of Flanders]]'' (1960)], directed by [[James B. Clark (director)|James B. Clark]] and starring [[David Ladd]] as Nello.
* ''[[Dog of Flanders (1975 TV series)|Dog of Flanders]]'' (1975), a [[Japanese animation]] TV series produced by [[Nippon Animation]]
* ''[[My Patrasche]]'' (1992), a Japanese animation TV series produced by [[Tokyo Movie Shinsha]]
* [http://www.imdb.com/title/tt0304072/ ''[[The Dog of Flanders]]'' (Japan, 1997)], a remake of the 1975 TV series directed by [[Yoshio Kuroda]]
* [http://www.imdb.com/title/tt0160216 ''[[A Dog of Flanders (1999 film)|A Dog of Flanders]]'' (1999)], directed by [[Kevin Brodie]]
* ''[[Barking Dogs Never Bite]]'' (2000), a South Korean satirical version directed by [[Bong Joon-ho]]
* ''[[Snow Prince]]'' (Japan, 2009), directed by [[Joji Matsuoka]].<ref name="Japan">{{cite book|title=Snow Prince|date=2009|url=http://info.movies.yahoo.co.jp/detail/tymv/id333963/ |author =Matsuoka, Joji |location=Japan}}</ref> {{ja icon}}
* ''A Dog of Flanders'', 2011, Minoto Studios{{citation needed|date=December 2015}}

For its authentic 19th century buildings, the Open Air Museum of [[Bokrijk]], Flanders was used as scenery for the 1975 and 1992 anime and the 1999 film.{{citation needed|date=December 2015}}

None of the film versions, excluding the 1997 Japanese movie and ''Snow Prince'' (2009) uses the novel's ending, preferring to substitute a more optimistic one. In ''Snow Prince'' (2009), the boy and the dog are found frozen to death under a tree. In one of the film versions,{{which|date=December 2015}} Nello and his dog go the village church, where the pastor covers them with a woolen blanket, thus saving their lives. Two days later, one of the judges comes. Because he thought Nello was the true winner, he asks him to stay with him. As years pass Patrasche dies, and Nello becomes a famous artist.

===Documentary film===
* ''Patrasche, a Dog of Flanders - Made in Japan'' (2007), a documentary film directed by Didier Volckaert and An van Dienderen.<ref>{{cite book|title=Patrasche, a Dog of Flanders - Made in Japan|url=http://www.dogofflanders.be|location=Belgium|author =Volckaert, Didier|author2 =van Dienderen, An|last-author-amp =yes |date=2007}}</ref>

===Comic book version===

The story was used as a plot device in the ''[[Suske en Wiske]]'' comic book series, namely the album ''Het Dreigende Dinges'' (''The Threatening Thing'') (1985). The album was translated into Japanese.<ref>{{cite web|url=http://suskeenwiske.ophetwww.net/albums/4kl/201.php|title=Het dreigende dinges - Suske en Wiske no. 201|publisher=|accessdate=30 November 2016}}</ref><ref name="argosarts.org"/>

===Monument===

There are three monuments built to commemorate the story. The first one was built in 1985 and can be seen in the Kapelstraat in [[Hoboken, Antwerp]].<ref>http://www.standbeelden.be/standbeeld/120</ref> Near the Antwerp Cathedral a fictional grave stone can be seen. It has text in English and Japanese and reads: "Nello, and his dog Patrasche, main characters from the story "A Dog of Flanders", symbols of true and sternful friendship, loyalty and devotion."<ref>{{cite web|url=http://ace-charity.org/en/diaries/patrasche-en-nello/|title=ACE Animal Care España - Nello and Patrasche|first=Animal Care|last=España|publisher=|accessdate=30 November 2016}}</ref>

On December 10, 2016, a new monument will be revealed on the Handschoenmarkt square in front of the Antwerp Cathedral. A sculpture in white marble will represent Nello and Patrasche sleeping, covered by a blanket of cobble stones.<ref>{{cite web|url=http://www.gva.be/cnt/dmf20160823_02435857/zo-zal-het-nieuwe-standbeeld-van-nello-en-patrasche-op-de-handschoenmarkt-er-uit-zien|title=Zo zal het nieuwe standbeeld van Nello en Patrasche op de Handschoenmarkt er uit zien|publisher=|accessdate=30 November 2016}}</ref>

==Controversy over location==

In 1985 an employee of Antwerp tourism, Jan Corteel, wanted to promote "A Dog of Flanders". He presumed the village of the story to be [[Hoboken (Antwerp)|Hoboken]], even though this is never mentioned in the story itself. [[Ouida]] is believed to have visited Antwerp for four hours, and spoke of having seen a village near a canal, not far from a windmill. This vague explanation was used to claim the story took place in Hoboken, but other people contest this.<ref>{{cite web|url=http://www.gva.be/cnt/aid636867/foute-hond-fout-district|title=Foute hond, fout district|publisher=|accessdate=30 November 2016}}</ref>

==Additional Information==
Similar stories:
* "[[The Little Match Girl]]" (1845)
* ''[[Hachi: A Dog's Tale]]'' (2009)
* ''[https://apps.facebook.com/dogbook/content/view/5315 Ciccio]'', from Italy

==References==
{{Reflist}}

==Further reading==
* {{cite news|url=http://crossroads.journalismcentre.com/2006/culture-leisure-travel-do-you-know-%e2%80%9ca-dog-of-flanders%e2%80%9d/ |date=2006|title=Do you know "A Dog of Flanders"?|work= Crossroads}}

==External links==
{{wikisource|A Leaf in the Storm; A Dog of Flanders and Other Stories/A Dog of Flanders|A Dog of Flanders}}
* [http://www.gutenberg.org/etext/7766 Project Gutenberg eBook]

{{DEFAULTSORT:Dog Of Flanders, A}}
[[Category:1872 novels]]
[[Category:Novels by Ouida]]
[[Category:Works published under a pseudonym]]
[[Category:English adventure novels]]
[[Category:Novels about orphans]]
[[Category:Children's novels about animals]]
[[Category:Novels about dogs]]
[[Category:Novels set in Belgium]]
[[Category:Antwerp in fiction]]
[[Category:Dogs in literature]]
[[Category:Chapman & Hall books]]
[[Category:British novels adapted into films]]
[[Category:Novels adapted into television programs]]
[[Category:Novels adapted into comics]]
[[Category:Japanese culture]]
[[Category:19th-century British novels]]