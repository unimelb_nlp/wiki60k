{{Orphan|date=July 2015}}

{{Infobox United States federal proposed legislation
| name = Furthering Asbestos Claim Transparency (FACT) Act of 2015
| fullname = To amend title 11 of the United States Code to require the public disclosure by trusts established under section 524(g) of such title, of quarterly reports that contain detailed information regarding the receipt and disposition of claims for injuries based on exposure to asbestos; and for other purposes.
| acronym = FACT Act
| nickname =
| introduced in the = 114th
| sponsored by = [[Blake Farenthold|Rep. Blake Farenthold (R, TX-27)]]
| number of co-sponsors = 3
| public law url =
| cite public law =
| cite statutes at large =
| acts affected = 
| acts repealed =
| title affected =
| sections created =
| sections affected = {{USC|11|524}}, {{USC|11|107}}
| agenciesaffected =
| authorizationsofappropriations =
| appropriations =
| leghisturl =
| introducedin = House
| introducedbill = {{USBill|114|hr|526}}  
| introducedby = [[Blake Farenthold|Rep. Blake Farenthold (R, TX-27)]]
| introduceddate = January 26, 2015 (later amended to {{USBill|114|hr|1927}} on January 6, 2014 <ref>https://www.congress.gov/bill/114th-congress/house-bill/1927/all-actions</ref>)
| committees = [[United States House Committee on the Judiciary]], [[United States House Judiciary Subcommittee on Regulatory Reform, Commercial and Antitrust Law]]
| passedbody1 = U.S. House of Representatives
| passeddate1 = January 8, 2016
| passedvote1 = 211 - 188
| passedbody2 = 
| passedas2 =
| passeddate2 =
| passedvote2 =
| conferencedate =
| passedbody3 =
| passeddate3 =
| passedvote3 =
| agreedbody3 =
| agreeddate3 =
| agreedvote3 =
| agreedbody4 =
| agreeddate4 =
| agreedvote4 =
| passedbody4 =
| passeddate4 =
| passedvote4 =
| signedpresident =
| signeddate =
| unsignedpresident =
| unsigneddate =
| vetoedpresident =
| vetoeddate =
| overriddenbody1 =
| overriddendate1 =
| overriddenvote1 =
| overriddenbody2 =
| overriddendate2 =
| overriddenvote2 =
| amendments =
| SCOTUS cases =
}}

The '''Furthering Asbestos Claim Transparency (FACT) Act of 2015''' (old bill number- {{USBill|114|hr|526}}, now Section 3 of {{USBill|114|hr|1927}}) is a bill introduced in the [[United States House of Representatives|U.S. House of Representatives]] by Congressman [[Blake Farenthold]] that would require [[asbestos]] trusts in the United States to file quarterly reports about the payouts they make and personal information on the victims who receive them in a publicly accessible database.<ref name="TheHill1">{{cite news|last=Wheeler|first=Lydia |title=Asbestos bill roils public health groups|url=http://thehill.com/regulation/legislation/242989-asbestos-bill-roils-public-health-groups|accessdate=26 May 2015|newspaper=The Hill |date=24 May 2015}}</ref><ref name= "GovTrack H.R.526">[https://www.govtrack.us/congress/bills/114/hr526] Govtrack.us, H.R. 526 (2015).</ref> The legislation would also allow defendant corporations in asbestos cases to demand information from the trusts for any reason.<ref name= "NY times 13">{{cite news|last= |first=|title=One-Sided Bill on Asbestos Injuries|url=https://www.nytimes.com/2013/06/20/opinion/one-sided-bill-on-asbestos-injuries.html?_r=3&|accessdate=31 May 2015|newspaper=The New York Times |date=19 June 2013}}</ref><ref name= "Inselbuch Testimony"/>

== Purpose ==
The intent of the bill, according to the ''Wall Street Journal'', is to bring "transparency" to a system that is susceptible to abuse.<ref>{{Cite news|title = The Double-Dipping Legal Scam|url = https://www.wsj.com/articles/the-double-dipping-legal-scam-1419535915|newspaper = Wall Street Journal|access-date = 2015-12-09|issn = 0099-9660}}</ref> However, according to the ''New York Times'', the bill would limit or greatly slow down the ability of trusts to award compensation to victims by instituting additional requirements for filing and reporting.<ref name="NY times 13" />

Mesothelioma and other asbestos-related diseases often appear decades after exposure to asbestos.

A ''[[The Wall Street Journal|Wall Street Journal]]'' investigative report discovered hundreds of alleged inconsistencies between claims with trusts and in-court cases. The opinion article also claimed many implausible claims that children were exposed to asbestos while working in industrial settings.<ref name=":0" />

The [[Government Accountability Office]] (GAO) studied asbestos trusts and concluded that the trusts operate without meaningful federal oversight. The GAO directly asked the asbestos trusts if their own audits uncovered fraud. In response, the trusts replied that they did not find any fraud among the more than $20 billion in claims paid out.<ref name=":0" />

==Background==
{{main article|Asbestos and the law#United States}}

=== 1930s ===
In the 1930s corporations that manufactured asbestos knew that their product caused serious adverse health effects and even alarming rates of death among workers.<ref name= "LCP"/><ref name= "Outrageous mis"/> Fearing the liability ramifications, the industry engaged in a large cover-up to hide the dangers to exposed workers.<ref name= "Outrageous mis">See, Paul Brodeur, Outrageous Misconduct; The Asbestos Industry on Trial, Pantheon Books, New York NY, 1985.</ref><ref>Connecticut Insurance Legal Journal, Vol. 12, p. 258 (2005-2006) 
Asbestos Litigation in the United States: Triumph and Failure of the Civil Justice System; Hensler, Deborah R.</ref> Although litigation by sick and injured victims began during this period, companies demanded confidentiality regarding settlement agreements. As a result, it took 45 years before the lethal nature of the product came to light.<ref name= "LCP">Law & Contemorary Problems, Volume 68 p. 133, 134 (2006) 
Public Health versus Court-Sponsored Secrecy Sequestered Science: The Consequences of Undisclosed Knowledge; Givelber, Daniel J.; Robbins, Anthony</ref>

=== 1980s ===
In 1981, federal scientists published data showing that asbestos contributed to causing more cancer cases than any other workplace exposure.<ref name= "LCP"/><ref>Kenneth Bridbord, Pierre Decoufle, Joseph F. Fraumeni, Jr., David G. Hoel, Robert N. Hoover, David P. Rail, Umberto Saffiotti, Marvin A. Schneiderman & Aurthur C. Upton, Estimates of the Fraction of Cancer in the United States Related to Occupational Factors, in BANBURY REPORT 9: QUANTIFICATION OF OCCUPATIONAL CANCER, App. (Richard Peto & Marvin Schneiderman eds.,1981).</ref>  The Center for Disease Control estimates that over 3,000 Americans still die from asbestos related diseases and cancers every year.<ref name="CDC">[http://www.cdc.gov/nchs/data/hus/2013/037.pdf] Center for Diseases Control, “Table 37: Deaths from selected occupational diseases among persons aged 15 and over: United States, selected years 1980–2010”</ref> Mesothelioma victims most often die 4–18 months after receiving diagnosis.<ref>{{cite web|url=http://www.nlm.nih.gov/medlineplus/mesothelioma.html|title=Mesothelioma|work=nih.gov|accessdate=19 October 2015}}</ref> The United States has not banned [[asbestos]] and it is still widely used in many common products.<ref>http://www2.epa.gov/asbestos/us-federal-bans-asbestos#notbanned</ref>

=== 1994 law ===
In 1994, Congress passed legislation, amending §524 of the Bankruptcy Code, to establish special trusts to compensate past and future asbestos victims while allowing companies seeking bankruptcy protection to remain operating and economically healthy.<ref name="GAO">[http://www.gao.gov/new.items/d11819.pdf] Government Accountability Office. Report to the Chairman, Committee on the Judiciary, House of Representatives. ASBESTOS INJURY COMPENSATION, The Role and Administration of Asbestos Trusts, p.8. 2011.</ref>  Unlike other companies that file for Chapter 11 bankruptcy protection, companies filing under this special section of the Bankruptcy Code need not be insolvent, but rather merely must be "named as a defendant in personal injury, wrongful death, or property-damage actions seeking recovery for damages allegedly caused by the presence of, or exposure to, asbestos or asbestos-containing products".<ref name="Ch11">[https://www.law.cornell.edu/uscode/text/11/524] U.S. Bankruptcy Code- Chapter 11, §526.</ref>  This legislation allowed the asbestos defendant to continue to operate as a profitable company while providing for yet unknown victims.<ref name="GAO"/> Before these bankruptcy amendments were passed "courts had to find inventive ways to include future claimants in the proceeding."<ref name= "ND Law Rev.">80 Notre Dame L. Rev., Vol. 80, p.1192, 1196 (2004-2005) Demanding Due Process: The Constitutionality of the 524 Channeling Injunction and Trust Mechanisms That Effectively Discharge Asbestos Claims in Chapter 11 Reorganization; Anand, Katherine M.</ref> "The goal of the trusts is to compensate present and future claimants, equitably and outside the court system, by managing the debtor company’s assets assumed by the trust as part of the bankruptcy reorganization," while relieving the company of any future asbestos related liability.<ref name="GAO"/>

Once a trust has been established, the company has, essentially, already conceded liability.<ref name= "Elihu 2013">[http://www.americanbar.org/content/dam/aba/administrative/tips/asbestos_tf/InselbuchResponsestoQuestionsfortheRecordreFACTAct.authcheckdam.pdf] Subcommittee on Regulatory Reform, Commercial and Antitrust Law Hearing on H.R. 982, the “Furthering Asbestos Claim Transparency (FACT) Act of 2013.” March 13, 2013.
Mr. Inselbuch’s Responses to Questions for the Record.</ref> Therefore, to make a claim against a trust, a victim need not prove liability as in a normal lawsuit, but rather must show evidence of the diagnosis of an asbestos related disease/cancer, evidence of the place of exposure, and medical documentation discussing the extent that asbestos played in the illness.<ref name="GAO2">[http://www.gao.gov/new.items/d11819.pdf] Government Accountability Office. Report to the Chairman, Committee on the Judiciary, House of Representatives. ASBESTOS INJURY COMPENSATION, The Role and Administration of Asbestos Trusts, p. 17, 18. 2011.</ref>

==Procedural history==

The FACT Act of 2015 was introduced in the [[U.S. House of Representatives]] by Rep. [[Blake Farenthold]] of Texas (TX-27) on January 26, 2015 and assigned to the [[House Judiciary Committee]].<ref>{{cite web|url=https://www.congress.gov/bill/114th-congress/house-bill/526|title=H.R.526 - 114th Congress (2015-2016): Furthering Asbestos Claim Transparency (FACT) Act of 2015|work=congress.gov|accessdate=19 October 2015}}</ref>  A hearing on the FACT Act was held on February 4, 2015 by the [[United States House Judiciary Subcommittee on Regulatory Reform, Commercial and Antitrust Law]].<ref name= "Judiciary Hearing">[http://judiciary.house.gov/index.cfm/hearings?ID=DCC41737-0D2C-487A-913C-5580F5686BB4] House Judiciary Subcommittee Hearing on the FACT Act of 2015, February 5, 2015.</ref> On May 14, the bill was voted out of the Judiciary Committee, 19-9, and was sent to be voted on by the full House of Representatives.<ref name="The Hill, Asbestos Bill Advances in House">{{cite news|last= Wheeler  |first=Lydia |title=Asbestos Bill Advances in House |url=http://thehill.com/regulation/242156-bill-to-protect-trusts-for-asbestos-victims-advances-in-house |accessdate=27 May 2015|newspaper=The Hill |date=14 May 2015}}</ref>

In December 2015, the FACT Act was added onto another U.S. House bill, H.R. 1927 (the Fairness in Class Action Litigation Act), and became Section 3 of H.R. 1927.  The bill was renamed the “Fairness in Class Action Litigation and Furthering Asbestos Claim Transparency Act of 2016.<ref>https://www.congress.gov/bill/114th-congress/house-bill/1927/text</ref>

On January 8, 2016, the U.S. House of Representatives passed H.R. 1927 by a vote of 211 to 188. The vote was largely along party lines, with no Democrats voting for it and sixteen Republicans voting against it.<ref>http://clerk.house.gov/evs/2016/roll033.xml</ref>

According to a Statement of Administration Policy, issued by the Office of Management and Budget on January 6, 2016, “The [Obama] Administration strongly opposes House passage of H.R. 1927 because it would impair the enforcement of important federal laws, constrain access to the courts, and needlessly threaten the privacy of asbestos victims.” It continues, “if the president were presented with H.R.1927, his senior advisers would recommend that he veto the bill.” <ref>https://www.whitehouse.gov/sites/default/files/omb/legislative/sap/114/saphr1927r20160106.pdf</ref>

Similar versions of the FACT Act legislation have been passed the House of Representatives in previous Republican-controlled sessions, including in 2013. In 2013, although the bill passed the House, it was never voted on by the Senate.<ref>{{cite web|url=https://www.govtrack.us/congress/bills/113/hr982|title=Furthering Asbestos Claim Transparency (FACT) Act of 2013 (2013; 113th Congress H.R. 982) - GovTrack.us|work=GovTrack.us|accessdate=19 October 2015}}</ref>

==Provisions ==

According to the [[Congressional Research Service]], the FACT Act requires an asbestos trust to file with the court "quarterly reports, available on the public docket, which describe each demand the trust has received from a claimant and the basis for any payment made to that claimant."<ref name="CRS description">[https://www.congress.gov/bill/114th-congress/house-bill/526] Congressional Research Service Description of H.R. 526, The FACT Act. January 26, 2015.</ref>

In addition, the legislation "requires such reports, upon written request, and subject to payment (demanded at the option of the trust) for any reasonable cost incurred by it, to provide any information related to payment from, and demands for payment from, the trust to any party to any action in law or equity concerning liability for asbestos exposure."<ref name="CRS description"/>

==Debate==

===Veterans===
The FACT Act of 2015 would have disproportionately affected veterans suffering from asbestos related illnesses and cancers. "Veterans who received compensation from an asbestos trust could have their work histories, compensation amount and partial Social Security numbers posted in bulk on a court’s public docket." <ref name="stripes.com">http://www.stripes.com/news/veterans-groups-oppose-asbestos-bill-in-senate-1.391970</ref> While veterans make up only 8% of our population, 30% of [[mesothelioma]] victims were exposed to asbestos while serving the country.<ref name="Work Safe">[http://www.worksafe.org/ca_workers2015_final2.pdf] Work Safe. Report: "Dying at Work in California, The hidden stories behind the numbers." p.15. 2015</ref><ref name= "Mil.Com">[http://www.military.com/benefits/veteran-benefits/asbestos-and-the-military-history-exposure-assistance.html] “Asbestos and the Military, History, Exposure & Assistance,” Military.com,</ref>
According to the Veterans Administration, “veterans who served in any of the following occupations may have been exposed to asbestos: mining, milling, shipyard work, insulation work, demolition of old buildings, carpentry and construction, manufacturing and installation of products such as flooring and roofing.”<ref name= "VA Asbestos Info">[http://www.publichealth.va.gov/exposures/asbestos/] "Asbestos," United States Veterans Administration.</ref><ref name="Fact Sheet: The FACT Act of 2015, Center for Justice and Democracy at New York Law School">{{cite news|last= Center for Justice & Democracy at New York Law School |first= |title=Fact Sheet: Facts About The "Fact Act" - An Anti-Victim Asbestos Bill|url=http://centerjd.org/content/fact-sheet-facts-about-“fact-act”-anti-victim-asbestos-bill|accessdate=27 May 2015|newspaper=CenterJD.org |date=9 April 2015}}</ref>

In an op-ed written for ''The Hill'', Blake Farenthold (TX-27), the bills sponsor in the House of Representatives, argued that without it, asbestos trusts will run out. He claims, as a result, they will not be able to pay the veterans, first responders, workers and other Americans who will become sick in the future from asbestos exposure that occurred in the past.<ref name=":0">{{Cite web|title = Asbestos reforms needed to protect first responders and veterans|url = http://thehill.com/blogs/congress-blog/judicial/255623-asbestos-reforms-needed-to-protect-first-responders-and-veterans|website = TheHill|accessdate = 2015-11-17|first = Blake|last = Farnethold}}</ref> In response to Mr. Farentholds op-ed, leadership from three major veterans organizations responded with an article  titled "Farenthold has his facts wrong: The FACT Act hurts veterans." Representatives from The Military Order of the Purple Heart of the U.S.A (MOPH), American Veterans (AMVETS), and the Association of the United States Navy (AUSN), explained that the FACT Act of 2015 "would be extremely detrimental to our members and all veterans who were exposed to asbestos while serving their country, in addition to their family members. Veterans disproportionately make up those who are dying and afflicted with mesothelioma and other asbestos-related illnesses and injuries."<ref name=vets />

According to [[Stars and Stripes (newspaper)]], a “newspaper that reports on matters affecting members of the United States Armed Forces,” “at least 16 national veterans groups came out strongly against it prior to the vote, warning it would allow companies to delay the claims of terminally ill veterans while exposing their sensitive personal information to identity theft.” <ref name="military.com">http://www.military.com/daily-news/2016/01/09/house-passes-asbestos-bill-over-veterans-objections.html</ref> Veterans who received compensation from an asbestos trust could have their work histories, compensation amount and partial Social Security numbers posted in bulk on a court’s public docket,

In January 2016, several veterans organizations have come out in opposition to the legislation, explaining (in an Op-Ed) "it will add significant time and delay in paying claims to our veterans and their families by putting burdensome and costly reporting requirements on trusts." <ref>http://thehill.com/blogs/congress-blog/healthcare/266076-veterans-groups-oppose-bill-harming-asbestos-victims</ref>  Groups signed onto the op-ed include, Air Force Sergeants Association (AFSA), Air Force Women Officers Associated (AFWOA), American Veterans (AMVETS), Association of the United States Navy (AUSN), Commissioned Officers Association of the US Public Health Service, Fleet Reserve Association (FRA), Jewish War Veterans of the USA (JWV), Marine Corps Reserve Association (MCRA), Military Officers Association of America (MOAA), Military Order of the Purple Heart (MOPH), National Association for Uniformed Services (NAUS), National Defense Council, Naval Enlisted Reserve Association (NERA), The Retired Enlisted Association (TREA), U.S. Coast Guard Chief Petty Officers Association, U.S. Army Warrant Officers Association, Vietnam Veterans of America (VVA).

In a letter to the Senate Judiciary Committee the veterans organizations called the FACT Act "a cynical ploy by the asbestos industry to avoid compensating its victims who are seeking justice in court.” <ref name="stripes.com"/>

===Privacy===

The FACT Act would require public disclosure of personal information of asbestos victims.<ref name= "FACT Act of 2015 Text">[https://www.congress.gov/bill/114th-congress/house-bill/526/text] Text of H.R. 526, Furthering Asbestos Claims Transparency Act of 2015.</ref> Under the legislation, the only information shielded from the disclosure is "confidential medical records" and a victims "full social security number."<ref name= "FACT Act of 2015 Text"/><ref name= "Roll Call">{{cite news|last= Vento |first= Susan |title=Asbestos Victims to Congress: Stop Fast-Tracking Legislation That Would Violate Victims' Privacy |url=http://www.rollcall.com/news/asbestos_victims_to_congress_stop_fast_tracking_legislation_that_would-241794-1.html|accessdate=29 May 2015|newspaper=Roll Call |date=13 May 2015}}</ref> According to a 2013 piece published in ''The Hill's Congress Blog'' by Susan Vento, widow of Congressman [[Bruce Vento]], and Judy van Ness,  with the Asbestos Cancer Victims’ Rights Campaign, information that may be requested and publicly disclosed includes the victims name, address, date of birth, last four digits of their social security number, information about children and spouses, employment history, salary history, information about their medical condition, and personal finances.<ref name= "Hill2">{{cite news|last= Van Ness |first= Judy |title=Fact Act Attacks Asbestos Victims |url=http://thehill.com/blogs/congress-blog/healthcare/189477-fact-act-attacks-asbestos-victims|accessdate=31 May 2015|newspaper=The Hill |date=7 November 2013}}</ref>

Patient and consumer advocates have expressed concern that making this information easily accessible could leave sick and dying asbestos victims and their families, vulnerable to identity theft and other frauds.<ref name="Hill2"/><ref name="Coalition Letter to Congress">[http://centerjd.org/content/coalition-letter-opposition-fact-act-hr-526-anti-victim-asbestos-bill] Coalition Letter to Congress in Opposition to the FACT Act of 2015, May 13, 2015.</ref>  In a coalition letter to Congress, consumer advocates raised the possibility of "predators, con artists and unscrupulous businesses" scouring the public disclosures for asbestos victims personal information.<ref name= "Hill2"/><ref name="Coalition Letter to Congress"/>

Speaking with reporters on a January 2016 conference call, Senator [[Chuck Schumer]] explained that requiring disclosure of their personal information online would deter "veterans from filing for the money they are owed but would also result in an egregious violation of their privacy and expose them to identity theft," and called the bill “offensive invasion of the privacy of those who defended this country.” <ref>http://hudsonvalleynewsnetwork.com/2016/01/20/schumer-says-house-bill-will-re-victimize-veterans-impacted-asbestos-poisoning/</ref>

====HIPAA====

[[HIPAA]] laws that protect patient privacy only apply to a distinct group of "covered entities" and do not apply to asbestos trust.<ref name= "NIH HIPAA">[http://privacyruleandresearch.nih.gov/pr_06.asp] U.S. Department of Health and Human Services - National Institutes of Health. HIPAA Privacy Rules. "Covered entities are defined in the HIPAA rules as (1) health plans, (2) health care clearinghouses, and (3) health care providers who electronically transmit any health information in connection with transactions for which HHS has adopted standards. Generally, these transactions concern billing and payment for services or insurance coverage."</ref><ref name="GPO Report 113">[http://www.gpo.gov/fdsys/pkg/CRPT-113hrpt254/html/CRPT-113hrpt254.htm] GPO Report on FACT Act Testimony. 113th Congress.</ref>  During one [[U.S. House Judiciary Committee]] hearing, Congressman [[Hank Johnson]] (D- GA) offered an amendment to the legislation that required trust to abide by HIPAA laws but the measure failed 12-18.<ref name= "Judiciary Markup">[http://judiciary.house.gov/index.cfm/markups-meetings?ID=C3C39D00-A93A-4677-80CE-47047F075289] House Judiciary Committee. Mark-up of H.R. 526, The FACT Act of 2015. May 14, 2015</ref>

===Compliance costs===

Trust representatives have explained, in letters to Congress, that the reporting and compliance requirements of the FACT Act would require large victim trusts to spend tens of thousands of additional hours every year to compile the lists and respond to all information requests allowed by this legislation.<ref name= "Trust Testimony">See Letter from Douglas A. Campbell, Campbell & Levine, LLC, to Rep. Bob Goodlatte, Chairman, et al., H. Subcomm. on Regulatory Reform, Commercial and Antitrust Law of the H. Comm. on the Judiciary, (Mar. 20, 2013).</ref> Funds used to fulfill these new requirements would come from the limited pool of money available to pay claims. 
A study by RAND found that the asbestos trusts are already underfunded. They reported the median payment from asbestos trusts to victims is 25% of the value of the claim. Some payments are as low as 1.1 percent of the claim’s value.<ref name = "Rand Study">[http://www.rand.org/content/dam/rand/pubs/monographs/2011/RAND_MG1104.pdf]  "Asbestos Bankruptcy Trusts and Tort Litigation," RAND Institute for Civil Justice. 2011.</ref>

Several asbestos trusts have expressed strong opposition to this legislation, in part because of the burdensome administrative costs that will significantly reduce recoveries for future trust claimants.<ref>See Letter from Douglas A. Campbell, Campbell & Levine, LLC, to Rep. Bob Goodlatte, Chairman, H. Comm. on the Judiciary, Rep. John Conyers, Jr., Ranking Member, H. Comm. on the Judiciary, Rep. Spencer Bachus, Chairman, H. Subcomm. on Regulatory Reform, Commercial and Antitrust Law of the H. Comm. on the Judiciary, and Rep. Steve Cohen, Ranking Member, H. Subcomm. on Regulatory Reform, Commercial and Antitrust Law of the H. Comm. on the Judiciary (Mar. 11, 2013) (“March 11, 2013 Campbell Letter”).</ref>

===Payment delays===
Cancer patient rights advocates say the FACT Act will cause long delays in payment of trust claims.<ref name="Roll Call"/><ref name= "Trust Testimony"/> Elihu Inselbuch, an expert on trust management and partner at Caplin & Drysdale,<ref>[http://www.capdale.com/einselbuch] Biography of Elihu Inselbach, Caplin & Drysdale</ref> explained that because trusts will be buried in additional paperwork, the FACT Act "would slow down or stop the process by which the trusts review and pay claims, such that many victims would die before receiving compensation, since victims of [[mesothelioma]] typically only live for 4 to 18 months after their diagnosis.” In many cases, “the delays in trust payment will force dying plaintiffs, who are in desperate need of funds, to settle for lower amounts with solvent defendants.… Delay is a weapon for asbestos defendants.”<ref name= "Elihu 2013"/>

==Support and opposition==

===Supporters===
This bill is sponsored by Republicans in the House of Representatives and Senate.<ref name= "GovTrack H.R.526"/> The FACT Act is also supported by the [[United States Chamber of Commerce]], which listed the bill as one of their top legislative priorities of the 114th legislative session.<ref name= "US Chamber Support of Fact Act">[https://www.uschamber.com/letter/letter-supporting-hr-526-furthering-asbestos-claim-transparency-act-2015] U.S. Chamber of Commerce Letter to Congress in Support of The FACT Act, May 13, 2015.</ref><ref>{{cite web|url=http://www.takejusticeback.com/Asbestos|title=Asbestos|work=takejusticeback.com|accessdate=19 October 2015}}</ref>  In their letter to Congress, the U.S. Chamber claims this legislation is needed to shine a light on asbestos victim trusts and prevent "double-dipping" by victims.<ref name= "US Chamber Support of Fact Act"/>

Supporters include:<ref>{{cite web|title=The Coalition|url=http://www.stopasbestostrustfraud.com/coalition/|website=Alliance for Asbestos Fraud Awareness|accessdate=13 November 2015}}</ref>
* [[60 Plus Association]]
* [[Air Force Association]], Department of Indiana
* [[American Insurance Association]]
* American Military Society
* [[Arizona Chamber of Commerce and Industry|Arizona Chamber of Commerce & Industry]]
* Arizona Manufacturers Council
* BCA
* Civil Justice Association of California
* Coalition for Common Sense
* Cost of Freedom, Indiana Chapter
* [[Florida Chamber of Commerce]]
* Florida Justice Reform Institute
* [[Georgia Chamber of Commerce]]
* Hamilton County Veterans
* [[Illinois Chamber of Commerce]]
* [http://www.nylawsuitreform.org/ Lawsuit Reform Alliance of New York]
* [[Louisiana Association of Business and Industry]]
* Michigan Chamber of Commerce
* [[Military Officers Association of America]], Indianapolis Chapter
* Missing in America Project of Indiana
* [[National Association of Manufacturers]]
* National Association of Mutual Insurance Companies <ref>http://www.businessinsurance.com/article/20160108/NEWS06/160109838/class-action-and-asbestos-bill-faces-president-obama-veto?tags=%7C61%7C313%7C80%7C83%7C303</ref>
* [[National Black Chamber of Commerce]]
* New Jersey Civil Justice Institute
* [[North Carolina Chamber of Commerce]]
* Pennsylvania Chamber of Business & Industry 
* [[Reserve Officers Association]], Department of Indiana
* Save Our Veterans
* South Carolina Civil Justice Coalition
* Taxpayers Protection Alliance
* Texas Civil Justice League
* Texas Coalition of Veterans Organizations
* The Cost of Freedom, Inc. of Indiana
* TLR
* U.S. Chamber Institute for Legal Reform
* U.S. Chamber of Commerce
* Veteran Resource List
* West Virginia Business & Industry Council
* West Virginia Chamber
* [[Wisconsin Manufacturers & Commerce]]

===Opponents===
Opponents of the legislation dispute the motivation of the law and the need to prevent double dipping. 'Double-dippers' are alleged to file multiple claims that may not be congruent with one another, and thus, may be considered fraudulent." In testimony to Congress, asbestos trust expert, Elihu Inselbuch explained that the idea that victims can double-dip is largely false.<ref name= "Inselbuch Testimony">[http://judiciary.house.gov/_cache/files/8d3bf861-686a-406e-b119-903ef9a61c00/inselbuch-fact-act-testimony---february-2015.pdf] Testimony of Elihu Inselbuch to the House Judiciary Subcommittee on Regulatory Reform, Commercial and Antitrust Law. February 4, 2015.</ref> “When an asbestos victim recovers from each defendant whose product contributed to their disease, that victim is in no way ‘double-dipping;’ rather they are recovering a portion of their damages from each of the corporations who harmed them. In fact, each trust is responsible for and pays for only its own share of the damages.”<ref name= "Inselbuch Testimony"/> In 2011, the Government Accountability Office investigated possible frauds in the Asbestos Victim Trusts and could not find one example of a fraudulent claim.<ref>UNITED STATES GOVERNMENT ACCOUNTABILITY OFFICE, REPORT TO THE CHAIRMAN, COMMITTEE ON THE JUDICIARY, HOUSE OF REPRESENTATIVES, ASBESTOS INJURY COMPENSATION: THE ROLE AND ADMINISTRATION OF ASBESTOS TRUSTS 23(2011).</ref>

In October of 2015, in a piece written for ''The Hill,'' titled "Farenthold has his facts wrong: The FACT Act hurts veterans," representatives from The Military Order of the Purple Heart of the U.S.A (MOPH), American Veterans (AMVETS), and the Association of the United States Navy (AUSN), explained their organizations' strong opposition to this "legislation due to its detrimental and disparate impact on men and women in uniform."  They explained the FACT Act of 2015 "would be extremely detrimental to our members and all veterans who were exposed to asbestos while serving their country, in addition to their family members. Veterans disproportionately make up those who are dying and afflicted with mesothelioma and other asbestos-related illnesses and injuries." <ref name=vets /> In addition, according to [[Stars and Stripes (newspaper)]], 16 veterans groups came out in strong opposition to the FACT Act.<ref name="military.com"/>

Various groups that oppose the legislation include:<ref name="Coalition Letter to Congress"/><ref>http://centerjd.org/system/files/HouseFloorCoalitionSignOnOpposingFACTActJanuary2016.pdf</ref><ref>http://www.citizen.org/documents/ProtectVictimPrivacy-Oppose-Sec3-HR1927.pdf</ref><ref>http://www.citizen.org/documents/Labor-Occupational-Health-Groups-Oppose-FACT-Act.pdf</ref><ref>http://www.scmagazine.com/asbestos-bill-would-expose-victims-personal-data-medical-histories/article/463484/</ref>
*[[AFL-CIO]]<ref>{{cite web|url=http://www.aflcio.org/Legislation-and-Politics/Legislative-Alerts/Letter-to-Representatives-expressing-opposition-to-H.R.-526-the-Furthering-Asbestos-Claim-Transparency-Act|title=Letter to Representatives expressing opposition to H.R. 526, the|work=AFL-CIO|accessdate=19 October 2015}}</ref>
*[[Alliance for Justice]]
*[[AFSCME]]
*[[American Association for Justice]]
*[http://www.amvets.org/ American Veterans] <ref name= vets /> 
*The Asbestos Disease Awareness Organization
*Asbestos Cancer Victims' Rights Campaign<ref>[http://cancervictimsrights.org/legislation/fact-act/] Asbestos Cancer Victims Rights Campaign. The FACT Act.</ref>
*Association of the United States Navy <ref name= vets/>
*Center for Effective Government 
*[[Center for Justice & Democracy]]<ref name="Fact Sheet: The FACT Act of 2015, Center for Justice and Democracy at New York Law School"/>
*[[Communications Workers of America]]
*ConnectiCOSH
*Connecticut Center for Patient Safety
*Constitutional Alliance 
*[[Consumer Action]]
*[[Consumer Watchdog]]
*[[Environmental Working Group]] Action Fund
*Essential Information
*Government Accountability Project
*International Association of Fire Fighters
*Knox Area Workers’ Memorial Day Committee
*Labor & Employment Committee of the National Lawyers Guild
*Maine Labor Group on Health
*Military Order of the Purple Heart of the U.S.A.<ref name= vets>[http://thehill.com/blogs/congress-blog/healthcare/257506-farenthold-has-his-facts-wrong-the-fact-act-hurts-veterans] Letter from Veterans Groups- Farenthold has his facts wrong: The FACT Act hurts veterans</ref>
*National Association of Consumer Advocates- [[NACA]]
* [[National Educational Association]]
*National Employment Lawyers Association 
*[[National Consumers League]]
*[[National Council for Occupational Safety and Health]]
*New England Regional Council of Carpenters
*New Jersey State Industrial Union Council
*New Solutions: A Journal of Environmental and Occupational Health Policy
*NHCOSH
*Occupational Health Clinical Centers, Syracuse, New York
*Occupational Safety & Health Law Project
*OpentheGovernment.org
*Patient Privacy Rights
*Privacy Rights Clearinghouse
*Privacy Times
*Protect All Children's Environment
*[[Public Citizen]]
*RICOSH
*SafeWork Washington
*TURN-The Utility Reform Network
*U.S. [[Public Interest Research Group]] (U.S. PIRG)
*[[United Steelworkers]]
*Western New York Council on Occupational Safety and Health
*Worksafe
*World Privacy Forum

==References==
{{Reflist|30em}}

==External links==
* {{YouTube|Z5X3ill4Ifc|Hearing: H.R. 526, the “Furthering Asbestos Claim Transparency (FACT) Act of 2015”}}

[[Category:Articles created via the Article Wizard]]
[[Category:Proposed legislation of the 114th United States Congress]]
[[Category:Asbestos]]
[[Category:2015 in American politics]]
[[Category:United States proposed federal legislation]]