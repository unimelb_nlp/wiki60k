{{Italic title}}
{|{{Infobox ship begin}}
{{Infobox ship career|Hide header=
|Ship country= [[Spain]]
|Ship flag= [[Image:Flag of Spain.svg|200px]]
|Ship name= ''El Salvador''
|Ship fate= Run aground in 1750 near Beaufort Inlet, [[North Carolina]] (34°41′44″N 76°41′20″W) 
|Ship out of service= August 29, 1750
}}
|}

{{Coin image box 1 double
| header = Silver 8 real coin of [[Philip V of Spain]], 1739
| image = Image:Philip V Coin.jpg
| caption_left = '''Reverse'''<br/><small>''VTRAQVE VNUM M[EXICO] 1739''<br/>"Both (are) one, Mexico [City Mint], 1739"<br/> Displays two hemispheres of a world map, crowned between the [[Pillars of Hercules]] adorned with the ''PLVS VLTR[A]'' motto.</small>
| caption_right = '''Obverse'''<br/><small>''PHILIP[PUS] V D[EI] G[RATIA] HISPAN[IARUM] ET IND[IARUM] REX''<br/>"Philip V, by the Grace of God, King of the Spains & the Indies"<br/>Displays the arms of [[Castile and León]] with [[Granada]] in base & an [[inescutcheon]] of [[Anjou]].</small>
| width = 250
| position = right
| margin = 0
}}

'''''El Salvador'''''  alias ''El Henrique'' was a Spanish treasure ship that ran aground near present-day Beaufort Inlet, [[North Carolina]] during a hurricane in August 1750. She was traveling with six other Spanish merchantmen including the ''Nuestra Señora De Soledad'' which went ashore near present-day [[Core Banks]], NC and the ''Nuestra Señora de Guadalupe'' which went ashore near present-day [[Ocracoke, North Carolina|Ocracoke]], NC.<ref>http://northcarolinashipwrecks.blogspot.com/2012/05/dangerous-shoals.html</ref>

The merchant ship ''El Salvador'' sailed from [[Cartagena, Colombia|Cartagena]], Colombia for [[Cadiz]], Spain loaded with a cargo of gold and silver where she was part of the [[Spanish treasure fleet]], a [[convoy]] system adopted by the [[Spanish Empire]] from 1566 to 1790.<ref>Marx, Robert: ''The treasure fleets of the Spanish Main.'' World Pub. Co., 1968</ref> After taking on supplies in [[Havana]], Cuba the heavily laden ''El Salvador'' headed for Cadiz with six other Spanish ships on August 7, 1750. Around noon on August 25 the ''El Salvador'' and the six other vessels in the fleet were caught in a hurricane Northeast of present-day [[Cape Canaveral]], Florida. The storm forced the ships North along the [[Gulf Stream]] where the ''El Salvador'', ''Soledad'' and ''Guadalupe'' were driven ashore along the [[Outer Banks]] of North Carolina. Reports found in the Spanish archives indicate that ''El Salvador'' was carrying 240,000 pesos in registered Spanish Treasury funds, made up of four chests of gold coins and sixteen chests of silver coins of varying denominations, plus 50,000 pesos in commercial funds.<ref>{{cite web|title=Spanish Treasure on the Outer Banks|url=http://yesterday-on-the-outer-banks.com/tag/el-salvador|accessdate=13 July 2015}}</ref> Only four members of the crew survived the wrecking. According to a letter written to North Carolina Gov. Gabriel Johnston in September 1750, the ship was badly broken up and buried in several feet of sand with only the rigging still visible above the water.<ref>{{cite news|last1=Dukes|first1=Tyler|title=Off North Carolina coast, lure of sunken treasure fades|url=http://www.wral.com/off-north-carolina-s-coast-lure-of-sunken-treasure-fades/14660654/#JBK9p5xX7XUsvypW.99|accessdate=27 March 2016|publisher=WRAL-TV|date=14 June 2015}}</ref>

Archival documents confirm that most of the [[treasure]] aboard the other Spanish ships was salvaged but that ''El Salvador’s'' cargo of gold and silver was never recovered. During the storm she is believed to have rolled over the bar, broken apart and was soon buried in the sand. Currently Intersal, Inc., a Florida-based company, holds an exclusive permit issued by the [[North Carolina Department of Natural and Cultural Resources]] (NCDNCR) which grants the company the right to search for ''El Salvador''.  The permit allows the company to retain 75% of all treasure and cargo it recovers from the ''El Salvador'' site with the remaining 25% going to the State of North Carolina.<ref>{{cite web|title=El Salvador|url=http://www.lat3440.com/index.php/el-sal|website=Intersal, Inc.}}</ref><ref>{{cite web|title=Mediated Settlement Agreement|url=http://www.scribd.com/doc/237650718/NCDCR-13DCR15732-Mediated-Settlement-Agreement-Signed-w-1998-Agreement-pdf#scribd|website=Scribd.com}}</ref>

==References==
{{reflist}}

*
*
*
*

==External links==
* [http://www.carolinacoastonline.com/news_times/news/article_66e266e6-2f9b-5fba-9ff4-38503c5f0e6e.html Two firms seek ship], Carolina Coast Online
* [https://news.google.com/newspapers?nid=1955&dat=19991229&id=GYkxAAAAIBAJ&sjid=nKMFAAAAIBAJ&pg=1096,7803604&hl=en Treasure hunter in race to uncover ship of riches], Google
* [https://www.nytimes.com/2007/08/23/nyregion/23masters.html Philip Masters, True Amateur of History, Dies at 70], New York Times
* [https://knoji.com/shipwrecks-and-treasure-the-spanish-treasure-fleet-of-1750 Shipwrecks and Treasure: the Spanish Treasure Fleet of 1750]
*[http://www.fayobserver.com/news/local/treasure-hunter-that-found-blackbeard-s-pirate-ship-sues-state/article_106b1120-30fd-5fde-a3d8-5a5e58d444f5.html Treasure hunter that found Blackbeard's pirate ship sues state for $8.2 million], Fayetteville Observer
*[http://www.newsobserver.com/news/state/north-carolina/article29437363.html Lawmakers enter legal battle over Blackbeard's ship], News & Observer
*[http://www.wral.com/photographer-suing-state-over-blackbeard-shipwreck-footage-/15150138/ Photographer suing state over Blackbeard shipwreck footage], WRAL-TV
*[http://www.greensboro.com/news/blackbeard-s-law-would-clarify-control-of-media-rights-to/article_cdb0e03d-0306-588d-8278-c87d8df12cf7.html Blackbeard's Law would clarify control of media rights to shipwrecks], News & Record
*[http://publicradioeast.org/post/controversy-over-blackbeards-queen-annes-revenge-continues Controversy Over Blackbeard's Queen Anne's Revenge Continues], Public Radio East
*[http://www.courthousenews.com/2015/08/12/battle-over-shipwreck-photos-brews-in-n-c.htm Battle Over Shipwreck Photos Brews in N.C.], Courthouse News
*[http://www.soundingsonline.com/features/in-depth/294588-plunder-disputes-plague-the-wreck-of-blackbeards-ship Plunder disputes plague the wreck of Blackbeard’s ship], Soundings
*[http://www.carolinacoastonline.com/news_times/article_eaa27c90-f38f-11e5-a3a5-2fb41cf75a7b.html Friends group calls it quits on fundraising], Carteret County News Times

[[Category:Shipwrecks in the Atlantic Ocean]]
[[Category:Shipwrecks]]
[[Category:Shipwrecks of the Carolina coast]]
[[Category:Underwater archaeology]]
[[Category:18th-century ships]]