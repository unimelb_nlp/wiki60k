{{Infobox journal
| title        = International Journal of Operations & Production Management
| cover        =
| editor       =
| discipline   = [[Operations management]]
| abbreviation =
| publisher    = [[Emerald Group Publishing]]
| country      = [[United Kingdom]]
| frequency    = Monthly
| history      =1980[http://www.emeraldinsight.com/journals.htm?issn=0144-3577]
| openaccess   =
| impact       = 1.252<br/>([[Journal Citation Reports]])
| impact-year  = 2012
| website      = http://info.emeraldinsight.com/products/journals/journals.htm?PHPSESSID=ic7ngj4vkgo33k7rdtvand1a03&id=ijopm
| link1        = http://www.emeraldinsight.com/Insight/viewContainer.do?containerType=Journal&containerId=200
| link1-name   = Online access
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 0144-3577
| eISSN        =
}}
The '''''International Journal of Operations and Production Management''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Emerald Group Publishing]]. It is the official journal of the [[European Operations Management Association]] and provides guidance on the management of systems, whether in academic institutions, industry or consultancy.

The journal publishes articles on a wide variety of topics, including [[capacity planning|capacity planning and control]], computer applications, delivery performance, [[ergonomics]], [[human resources]] in operations, [[industrial engineering]], [[inventory planning and control]], [[Just In Time (business)|just in time]], [[Lean manufacturing|lean]]/agile production, maintenance, management of organizational or [[technological change]], [[motivation]] and payment systems, [[MRPII]], performance measurement, plant location design and layout, [[Process design|process]] and [[product design]], [[production planning]] and control, [[productivity]], [[operational excellence|quality in operations]], scheduling and load planning, service operations, strategy in [[manufacturing]] operations, [[supply chain management|supply chain management/purchasing]], and work study.

The ''International Journal of Operations and Production Management'' was indexed by the [[Social Sciences Citation Index]] (ISI), with a 2010 [[impact factor]] of 1.812; in 2012 [[Journal Citation Reports]] gave it an impact of 1.252. It publishes twelve issues per year, and is currently edited by Steve Brown of the [[University of Exeter Business School]], UK. Until 2009 the journal was edited by Margaret and Andrew Taylor of the [[Bradford University School of Management]], UK.

==External links==
* {{Official website|1=http://info.emeraldinsight.com/products/journals/journals.htm?PHPSESSID=ic7ngj4vkgo33k7rdtvand1a03&id=ijopm}}

{{DEFAULTSORT:International Journal of Operations and Production Management}}
[[Category:Business and management journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Emerald Group Publishing academic journals]]