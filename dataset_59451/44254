{{Infobox airport
| name         = Palmyra Municipal Airport
| image        = Town_of_Palmyra_Wisconsin_Airport_Sign.jpg
| caption      = Airport sign
| FAA          = 88C
| type         = Public
| owner-oper   = Town of Palmyra
| city-served  = [[Palmyra (town), Wisconsin|Palmyra, Wisconsin]]
| location     = <!--if different than above-->
| elevation-f  = 851
| website      = 
| coordinates  = {{coord|42|53|01|N|088|35|51|W|region:US-WI_scale:10000|display=inline,title}}
| pushpin_map            = USA Wisconsin#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Wisconsin / United States
| pushpin_label          = '''88C'''
| pushpin_label_position = right
| r1-number    = 9/27
| r1-length-f  = 2,800
| r1-surface   = Turf
| stat-year    = 
| stat1-header = Aircraft operations (2016)
| stat1-data   = 14,000
| stat2-header = Based aircraft (2017)
| stat2-data   = 83
| footnotes    = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=88C|use=PU|own=PU|site=27475.1*A}}. Federal Aviation Administration. Effective Mar 2, 2017.</ref>
}}

'''Palmyra Municipal Airport'''<ref name="FAA" /><ref>{{cite web | url = http://www.dot.wisconsin.gov/travel/air/docs/airports/palmyra.pdf | title = Palmyra Municipal (88C) | publisher = [[Wisconsin Department of Transportation]] | accessdate = May 11, 2013}}</ref> {{airport codes|||88C}} is a public use [[airport]] in [[Jefferson County, Wisconsin]], United States.<ref name="FAA" /> Owned by and located in the [[Palmyra (town), Wisconsin|Town of Palmyra]], it is also known as '''Palmyra Airport'''<ref>{{cite web | url = http://www.palmyraflyingclub.com/History.html | title = History | publisher = The Palmyra Flying Club | accessdate = May 11, 2013}}</ref> or Town of Palmyra Airport.{{citation needed|date=May 2013}} It is included in the [[Federal Aviation Administration]] (FAA) [[National Plan of Integrated Airport Systems]] for 2017–2021, in which it is [[FAA airport categories|categorized]] as a local [[general aviation]] facility.<ref name="NPIAS Airports">{{cite web|title=List of NPIAS Airports|url=https://www.faa.gov/airports/planning_capacity/npias/reports/media/NPIAS-Report-2017-2021-Appendix-A.pdf|website=FAA.gov|publisher=Federal Aviation Administration|accessdate=23 November 2016|format=PDF|date=21 October 2016}}</ref>

== History ==
[[File:Town of Palmyra Wisconsin Airport 2.jpg|thumb|left|Airplane at airport]]
Palmyra Municipal Airport has been owned and operated by the town of Palmyra since 1948.<ref name="DU 1-23-2013">{{cite news | url = http://dailyunion.com/main.asp?SectionID=36&SubSectionID=110&ArticleID=13589 | title = Town of Palmyra likely to continue fighting annexation | work = Daily Union | first = Ryan | last = Whisner | date = January 23, 2013}}</ref><ref name="DU 3-12-2013">{{cite news | url = http://dailyunion.com/main.asp?SectionID=36&SubSectionID=110&ArticleID=13994 | title = Palmyra town board to legally challenge annexation | work = Daily Union | first = Ryan | last = Whisner | date = March 12, 2013}}</ref>

In 2013, the [[Palmyra, Wisconsin|Village of Palmyra]] proposed [[annexation]] of more than 740 acres of property, primarily [[agricultural land]], but also including the Town of Palmyra's [[town hall]] and the Palmyra Municipal Airport.<ref name="DU 1-23-2013" /><ref>{{cite news | url = http://eauclairecountrytoday.wi.newsmemory.com/pda.php?date=20130123&eid=1&sid=0&aid=14 | title = Land squabble: Village, town, agriculture officials debate annexation plan | work = The Country Today | publisher = Eau Claire Press Company | first = Jim | last = Massey | date = January 23, 2013}}</ref> The Town of Palmyra is challenging the village's annexation attempt.<ref name="DU 3-12-2013" />

== Facilities and aircraft ==
The airport covers an area of 156 [[acre]]s (63 [[hectare|ha]]) at an [[elevation]] of 851 feet (259 m) above [[mean sea level]]. It has one [[runway]] designated 9/27 with a [[grass|turf]] surface measuring 2,800 by 200 feet (853 x 61 m).<ref name="FAA" />

For the 12-month period ending May 6, 2016, the airport had 14,000 [[general aviation]] aircraft operations, an average of 38 per day. In March 2017, there were 83 aircraft based at this airport: 82 single-engine and 1 helicopter.<ref name="FAA" />

== References ==
<references />

== External links ==
{{Commons cat|Town of Palmyra Airport}}
* [http://msrmaps.com/map.aspx?t=1&s=11&lat=42.8836&lon=-88.5974&w=600&h=400&lp=---+None+--- Aerial image] from [[USGS]] ''[[The National Map]]''
* {{US-airport-minor|88C}}

<!--Navigation box--><br />
{{Aviation in Wisconsin}}

[[Category:Airports in Wisconsin]]
[[Category:Buildings and structures in Jefferson County, Wisconsin]]