<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Nieuport 15
 | image=File:WW1 bomber Nieuport 15.jpg
 | caption=Nieuport 15 prototype cica 1916
}}{{Infobox Aircraft Type
 | type=[[Bomber aircraft|Bomber]]
 | national origin=[[France]]
 | manufacturer=[[Nieuport]]
 | designer=
 | first flight=
 | introduced=November 1916 
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=At least 4
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Nieuport 14]]<ref name=hisw>{{cite web |url=http://www.historyofwar.org/articles/weapons_nieuport_15.html|title=Nieuport 15 |author=Rickard, J|date=8 January 2015|website=historyof war.org|publisher=|access-date=1 August 2015}}</ref>
 | variants with their own articles=
}}
|}

The '''Nieuport 15''' was designed as a bomber aircraft during [[World War I]]. Due to disappointing performance the type was rejected and never entered service.<ref name=hisw/>
==Design and development==
Based on the [[Nieuport 14]], the new bomber was built in the summer of 1916 and the first prototype was ready for testing in November of that year.<ref name=hisw/>

The Nieuport 15 was a twin bay un-staggered wing [[wing configuration#Number and position of main-planes|sequiplane]] with [[Interplane strut|V-struts]] and a newly designed [[tailplane]] including heart shaped [[Elevator (aeronautics)|elevators]].<ref name=hisw/> It was powered by a {{convert|220|hp|abbr=on}} [[Renault 12F]] V-12 engine.<ref name=airp>{{cite web |url=http://1000aircraftphotos.com/Contributions/SmithGaryL/8926.htm|title= Nieuport 15 French Air Force|author=|date=31 May 2009|website=1000aircraftphotos.com|publisher=|access-date=1 August 2015}}</ref>  featuring Hazet vertical tube radiators.<ref name=hisw/>

During limited [[Flight test|flight testing]] the [[Aircraft flight control system|controls]] and [[landing gear]] were found to be unsatisfactory and the French quickly abandoned the bomber type.<ref name=hisw/> Late in December 1916 it was declared obsolete. The British showed some interest and initially ordered 70 aircraft but after disappointing tests all orders were eventually cancelled.<ref name=hisw/>

==Specifications==
{{Aircraft specs
|ref=History of War<ref name=hisw/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft=31.4
|length in=
|length note=
|span m=17
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=47.85
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=1,330
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=1,900 
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Renault 12F]]
|eng1 type=V-12 water-cooled piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->220
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=155
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=3 hours
|ceiling m=4,000
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=7 min 20 sec to 3,280 ft
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|guns=
|bombs=Fourteen {{convert|120|mm|in|abbr=on|2}} calibre {{convert|22|lb|kg|disp=flip|abbr=on}} [[Anilite bomb]]s - {{convert|308|lb|kg|disp=flip|abbr=on}} total
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}

==See also==
{{aircontent
|related=
*[[Nieuport 14]]
|similar aircraft=
*[[Airco DH.4]]
*[[Armstrong Whitworth F.K.8]]
*[[Avro 519]]
*[[Breguet 14]]
|lists=
*[[List of World War I Entente aircraft]]
*[[List of military aircraft of France]]
*[[List of bomber aircraft]]|see also=
}}

==References==
{{reflist}}

==Further reading==
*{{cite book|last1=Hartmann|first1=Gérard|title=Les NIEUPORT de la guerre|location=Paris|url=http://www.hydroretro.net/etudegh/les_nieuports_de_la_guerre.pdf|accessdate=3 August 2015|language=French|format=pdf}}

{{Nieuport aircraft}}
{{Portal|Aviation| France}}

[[Category:Military aircraft of World War I]]
[[Category:French bomber aircraft 1910–1919]]
[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:Nieuport aircraft|15]]