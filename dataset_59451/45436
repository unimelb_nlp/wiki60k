{{Multiple issues|
{{COI|date=September 2011}}
{{refimprove|date=June 2016}}
}}

{{Infobox military person
|name=Siegfried Knemeyer
|birth_date={{Birth date|1909|4|5|df=y}}
|death_date={{Death date and age|1979|4|11|1909|4|5|df=y}}
|birth_place=[[Minden|Ellerbusch]], Germany
|death_place=[[Yellow Springs, Ohio|Ohio]]
|nickname = Knall Max, Knall Kne, Star Gazer
|allegiance={{flag|Nazi Germany}} (1939–1945)<br/>{{flag|West Germany}} (1945–1947)<br/>{{flag|United States}} (1947–1979)
|serviceyears=1939–45, 1947–79
|rank=''[[Oberst]]''
|branch={{Luftwaffe}}<br/>{{air force|United States}}
|awards=[[Iron Cross]] First and Second Class<br/>[[German Cross]] in Gold<br/>[[Knight's Cross of the Iron Cross]]<br/>[[Department of Defense Distinguished Civilian Service Award]]
|laterwork=Aircraft consultant}}

'''Siegfried Knemeyer''' (5 April 1909&nbsp;– 11 April 1979) was a German [[Aerospace engineering|aeronautical engineer]], aviator and the Head of Technical Development at the [[Reich Ministry of Aviation]] of [[Nazi Germany]] during [[World War II]].

==Early career==
Knemeyer attended the [[Technische Universität Berlin]], from which he graduated in 1933 with a dual major of theoretical experimental physics and [[aeronautical engineering]]. He was affiliated with the [[Akaflieg Berlin|Academic Flying Group]]. In 1935 Knemeyer was a flight instructor for the [[Reich Air Ministry]], a civilian organization at the disposal of the German military.

He enlisted in the Luftwaffe after the outbreak of World War II, on 4 September 1939.<ref name="Herwig & Rode 1998, p. 14.">Herwig & Rode 1998, p. 14.</ref>

==World War II==

After serving as Field Marshal [[Walter von Brauchitsch]]’s pilot during the brief [[Invasion of Poland]], Knemeyer was assigned to the [[Theodor Rowehl|Rowehl]] Reconnaissance Group. During his time with this group Knemeyer flew hundreds of reconnaissance flights in every theater of the German war.<ref name="Kahn 1978, p. 115.">Kahn 1978, p. 115.</ref> In autumn 1939, Knemeyer flew a reconnaissance mission to [[Narvik]], Norway to observe whether the British had occupied Narvik seaport. While on this mission Knemeyer took photographs of the British [[Home Fleet]] at [[Scapa Flow]] and outmaneuvered two [[Spitfires]] to escape with the photographs. Based on this intelligence {{GS|U-47|1938|2}} of the [[Kriegsmarine]] sank the British battleship {{HMS|Royal Oak|08|6}} in a famous incident. For this, Knemeyer was awarded his first [[Iron Cross]].

In April 1943, Knemeyer was appointed the technical officer of General [[Dietrich Peltz]], who was responsible for the air war against England. In this capacity he established a program focused on capturing and re-fitting enemy aircraft, as a means to gain a tactical advantage and assist the Luftwaffe’s internal research efforts.<ref name="Smith, Creek & Petrick 2003, p. 70.">Smith, Creek & Petrick 2003, p. 70.</ref>

In 1943, alarmed that Allied advances in aviation technology threatened to tip the balance of the war against Germany, [[Hermann Göring]] convened a conference at [[Carinhall]] among his senior leadership. Peltz brought Knemeyer with him to this conference, and Göring was enamored with Knemeyer’s innovative ideas. After the conference Göring declared "Knemeyer is my boy!" and in July 1943 reassigned him to be his personal technical advisor. Several months later Knemeyer was promoted to [[Oberst]] and made Director of Research and Development of the Luftwaffe. Göring came to call Knemeyer the "Star Gazer"<ref name="Baumbach 1949, p. 199.">Baumbach 1949, p. 199.</ref> and would greet him with the question, "Now, my Star Gazer, what do you see in your crystal ball?"<ref name="Knemeyer 1985, p. 23.">Knemeyer 1985, p. 23.</ref> In November 1943, Knemeyer was appointed Head of Technical Development for the [[Reichsluftfahrtministerium]] (RLM), under ''Oberst'' [[Edgar Petersen]]'s command.

By February 1944, Knemeyer had surprisingly never flown a German [[heavy bomber]] of any sort, until he got his turn to fly one of the [[Heinkel He 177#Further development-the Heinkel He 177B|Heinkel He 177B]] prototypes on 24 February at the [[Wiener Neustadt]] military airfield.  His favorable opinion on the [[twin tail]]-equipped He 177 V102 aircraft's "excellent handling qualities" compelled him to recommend that the [[Heinkel]] firm place the He 177B design's priority above that of the [[Heinkel He 343]] four-jet medium bomber design, which was still in its early stages.{{sfn|Griehl|Dressel|1998|pp=166–167}}

Shortly after rising to his top-level technical appointment within the RLM, Knemeyer became close with old colleague, General [[Werner Baumbach]].<ref name="Baumbach 1949, pp. 198–199.">Baumbach 1949, pp. 198–199.</ref><ref name="Smith & Kay 1972.">Smith & Kay 1972.</ref> Knemeyer was included on a Special Committee of top-ranking Luftwaffe administrators in November 1943 for the purpose of advocating broad adoption of and investment in the Me-262.<ref name="Green 1970, pp. 622–623.">Green 1970, pp. 622–623.</ref> Smith and Creek credit Knemeyer and General [[Adolf Galland]] as the men responsible for Germany’s finally putting the Me 262A-1a jet fighter into mass production.<ref name="Smith & Creek 1982, p. 89.">Smith & Creek 1982, p. 89.</ref>

In 1944, the German hierarchy placed a renewed call for creative plans to reverse the now-inevitable defeat descending on [[Nazi Germany]]. Familiar with the newest technologies, Knemeyer conceived a plan to develop a long-range bomber that would drop a radioactive "[[dirty bomb]]" on [[New York City]], in hopes of intimidating the [[United States]] out of the war. This idea was embraced and Knemeyer set up and personally supervised a competition between the three most promising technologies: [[Wernher von Braun]]’s [[Aggregate (rocket family)#A9/A10|Aggregat A-9]] rocket missile and A-10 booster rocket; [[Eugen Sänger]]’s [[Silbervogel]], and the [[Horten brothers]]' [[Horten Ho 229]] turbojet-powered [[flying wing]] fighter. While this competition accelerated the progress of leading edge aviation technology, of these specified aerospace design projects, only one prototype example of the Ho 229 (the ''Versuchs-Zwei'', or Ho 229 V2 second prototype) flew prior to the end of the war.<ref>Shepelev & Ottens 2006, p. 70.</ref>

==Operation Paperclip==
Knemeyer was arrested in the [[Allied-occupied Germany#British Zone of Occupation|British Zone of Occupation]] and was interned in [[Münster]] and then at the [[Latimer, Buckinghamshire|Latimer]] prison camp. Knemeyer was part of the [[Operation Paperclip]] and in June 1948 he was awarded a permanent contract of employment with the [[United States Air Force]], [[Air Material Command]]. His family was then able to join him in America. Knemeyer began with the [[United States War Department]] on 1 July 1947. As acknowledgement of his contributions, in 1966 he received the highest civilian award granted by the U.S. military, the U.S. Department of Defense Distinguished Civilian Service Award.

==Awards==
In Germany
* [[Iron Cross]] (1939)
** 2nd Class
** 1st Class
* [[German Cross]] in Gold on 27 July 1942 as ''[[Hauptmann]]'' in the 4./Aufklärungs-Gruppe of the [[Oberkommando der Luftwaffe|OB.d.L.]]{{sfn|Patzwall|Scherzer|2001|p=237}}
* [[Knight's Cross of the Iron Cross]] on 29 August 1943 as ''[[Major (Germany)|Major]]'' im Stabsamt des [[Ministry of Aviation (Germany)|RLM]] (with the staff of the Air Ministry) and ''[[Gruppenkommandeur]]'' of Aufklärungs-Lehr-Gruppe of the OB.d.L.{{sfn|Scherzer|2007|p=453}}

In the United States
* [[Department of Defense Distinguished Civilian Service Award]]

==References==

===Citations===
{{Reflist|colwidth=25em}}

===Bibliography===
{{Refbegin|30em}}
*[[Werner Baumbach|Baumbach, Werner]] (1960). ''The Life and Death of the Luftwaffe''. Ballantine Books. Library of Congress Catalog Card Number: 60-11283.
*Green, William (1970). ''The Warplanes of the Third Reich''. Library of Congress Catalog Card Number: 88-29673.
* {{Cite book
 |last1=Griehl
 |first1=Manfred
 |last2=Dressel
 |first2=Joachim
 |year=1998
 |title=Heinkel He 177 – 277 – 274
 |location=Shrewsbury, UK
 |publisher=Airlife Publishing
 |isbn=978-1-85310-364-3
 |ref=harv
}}
*Herwig, Dieter and Rode, Heinz (1998). ''Luftwaffe Secret Projects: Strategic Bombers 1935–1945''. ISBN 1-85780-092-3.
*Kahn, David (1978). ''Hitler’s Spies''. ISBN 0-02-560610-7
*Myrha, David and The History Channel (2005). ''Nazi Plan to Bomb New York''. DVD. ASIN: B001CU7W76.
* {{Cite book
 |last1=Patzwall
 |first1=Klaus D.
 |last2=Scherzer
 |first2=Veit
 |year=2001
 |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
 |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
 |language=German
 |location=Norderstedt, Germany
 |publisher=Verlag Klaus D. Patzwall
 |isbn=978-3-931533-45-8
 |ref=harv
}}
* {{Cite book
 |last=Scherzer
 |first=Veit
 |year=2007
 |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
 |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
 |language=German
 |location=Jena, Germany
 |publisher=Scherzers Militaer-Verlag
 |isbn=978-3-938845-17-2
 |ref=harv
}}
*Shepelev, Andrei and Ottens, Huib (2006). ''Horten Ho 229: Spirit of Thuringia''. ISBN 1-903223-66-0.
*Smith, J. Richard and Creek, Eddie J. (1982). ''Jet Planes of the Third Reich''. ISBN 0-914144-27-8.
*Smith, J. Richard, Creek, Eddie J. and Petrick, Peter (2003). ''On Special Missions: The Luftwaffe’s Research and Experimental Squadrons 1923–1945''. ISBN 1-903223-33-4.
*Smith, J.R. and Kay, Anthony L. (1972). ''German Aircraft of the Second World War''. ISBN 1-55750-010-X.
{{Refend}}

{{Use dmy dates|date=October 2010}}

{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=United States Air Force
| portal5=World War II
}}

{{DEFAULTSORT:Knemeyer, Siegfried}}
[[Category:1909 births]]
[[Category:1979 deaths]]
[[Category:People from the Province of Westphalia]]
[[Category:Technical University of Berlin alumni]]
[[Category:German scientists]]
[[Category:German people of the Spanish Civil War]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:Luftwaffe pilots]]
[[Category:German prisoners of war in World War II held by the United Kingdom]]
[[Category:Operation Paperclip]]