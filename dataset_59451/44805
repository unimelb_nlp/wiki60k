{{italic title}}
{{Infobox journal
| title         = Veterinary Record
| cover         = [[File:Veterinary Rec Imagery.gif]]
| editor        = Martin Alder
| discipline    = [[Veterinary medicine]]
| formernames   = 
| abbreviation  = Vet. Rec.
| publisher     = [[BMJ Group]]
| country       = [[United Kingdom]]
| frequency     = Weekly
| history       = 1888–present
| openaccess    = 
| license       = 
| impact        = 1.50
| impact-year   = 2009
| website       = http://veterinaryrecord.bvapublications.com/
| link1         = http://veterinaryrecord.bvapublications.com/current.dtl
| link1-name    = Online access
| link2         = http://veterinaryrecord.bvapublications.com/archive/
| link2-name    = Online archive
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 1769072
| LCCN          = sv89073252
| CODEN         = VETRAX
| ISSN          = 0042-4900
| eISSN         = 
}}
'''''Veterinary Record''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of [[veterinary medicine]]. It is mainly distributed to members of the [[British Veterinary Association]] as part of their membership. It was established in 1888.<ref name=about>{{cite web |url=http://veterinaryrecord.bvapublications.com/misc/about.dtl |title=The Veterinary Record -- About The Veterinary Record |format= |work= |accessdate=2010-02-23}}</ref>

==Publication history==
''Veterinary Record'' was launched in July 1888 by [[William Hunting]]. Hunting is said to have started the journal with loans of £50 from another London veterinary surgeon, T. A. Dollar, which he never repaid, and £20 from Dollar's son, J.A W. Dollar. Although ''[[The Veterinarian]]'', founded 1828, and ''[[The Veterinary Journal]]'', founded 1844, were well established and covered some of the same ground as Hunting's new journal, the fact that ''Veterinary Record'' was published every week and carried verbatim reports of council and local association meetings gave it an immediacy that the other publications could not match.<ref name=PDF>{{cite web |url=http://www.bva.co.uk/BVA_history_1881-1919.pdf |title=Practice and Politics: the British Veterinary Association 1881 – 1919 | author = Edward Boden |format= |work=www.bva.co.uk |accessdate=2010-02-23}}</ref>

Since July 2009, ''Veterinary Record'' has been published by the [[BMJ Group]] with [[British Veterinary Association]] as part of the Group's Affinity and society publishing programme.<ref name=new>{{cite web |url=http://group.bmj.com/group/subscriptions-and-sales/product-training-and-userguides/Vet_flier.pdf |title=New from the BMJ Group |format= |work= |accessdate=2010-02-23}}</ref>

==Indexing and citations==
''Veterinary Record'' is indexed by [[Medline]].<ref name=Library>{{cite web |url=http://www.wsulibs.wsu.edu/hsl/vetjmed.htm |title=Animal Health Library - Veterinary Journals Indexed in MEDLINE |work= |accessdate=2010-02-23}}</ref> According to the 2009 [[Journal Citation Reports]], the journal's [[impact factor]] is 1.50 ranking it 30th out of 135 journals in the category Veterinary Science.<ref name=WoS>{{Cite web  | title = Web of Science | url = http://isiwebofknowledge.com | accessdate = 2010-02-23}}</ref> The five journals that as of 2008 have cited the ''Veterinary Record'' most often (in order of descending citation frequency) are the ''Veterinary Record'', ''The Veterinary Journal'', ''[[Veterinary Parisitology]]'', ''[[Veterinary Microbiology]]'', and ''[[Preventive Veterinary Medicine]]''.<ref name=WoS/> As of 2008, the five journals that have been cited most frequently by articles published in the ''Veterinary Record'' are the ''Veterinary Record'', ''[[Journal of the American Veterinary Medical Association]]'', ''Veterinary Microbiology'', ''[[Equine Veterinary Journal]]'', and the ''[[American Journal of Veterinary Research]]''.<ref name=WoS/>

==Article categories==
The journal publishes articles in the following categories
* News
* Comment
* Letters
* Clinical Research Papers
* Career advice

==Most cited articles==

According to the [[Web of Science]], the following three articles have been cited most often:<ref name=WoS/>
#{{cite journal |vauthors=Benestad SL, Sarradin P, Thu B, Schönheit J, Tranulis MA, Bratberg B |title=Cases of scrapie with unusual features in Norway and designation of a new type, Nor98 |journal=Veterinary Record |volume=153 |issue=7 |pages=202–8 |date=August 2003 |pmid=12956297 |doi= 10.1136/vr.153.7.202|url= }}
#{{cite journal |vauthors=Gibbens JC, Sharpe CE, Wilesmith JW, Mansley LM, Michalopoulou E, Ryan JB, Hudson M |title=Descriptive epidemiology of the 2001 foot-and-mouth disease epidemic in Great Britain: the first five months |journal=Veterinary Record |volume=149 |issue=24 |pages=729–43 |date=December 2001 |pmid=11808655 |url= |doi=10.1136/vr.149.24.729 }}
#{{cite journal |vauthors=Whay HR, Main DC, Green LE, Webster AJ |title=Assessment of the welfare of dairy cattle using animal-based measurements: direct observations and investigation of farm records |journal=Veterinary Record |volume=153 |issue=7 |pages=197–202 |date=August 2003 |pmid=12956296 |doi= 10.1136/vr.153.7.197|url= }}

== ''Brunus edwardii'' joke ==
The [[April Fools' Day|April]] 1972 issue of the ''Veterinary Record'' included a paper on the diseases of ''Brunus edwardii'': a description of lost limbs and thinning hair suffered by an animal whose Latin name means "brown" and "Edward". The paper was accompanied by sketches of a teddy bear resembling [[Winnie the Pooh]].<ref name="Brunusedwardii-1">{{cite web|url=http://www.museumofhoaxes.com/hoax/af_database/display/category/medical/|title=The April fool's day database|year=1972|work=Brunus edwardii (1972)|accessdate=27 March 2010}}</ref><ref name="Brunusedwardii-2">{{cite journal|last=Blackmore|first=DK |author2=DG Owen |author3=CM Young|year=1972|title=Some observations on the diseases of Brunus edwardii (Species nova) |journal=Veterinary Record|volume=90|pages=382–385|url=http://veterinaryrecord.bvapublications.com/content/vol90/issue14/index.dtl|accessdate=27 March 2010|doi=10.1136/vr.90.14.382}}</ref>

==References==
{{Reflist|2}}

{{DEFAULTSORT:Veterinary Record}}
[[Category:Weekly journals]]
[[Category:Publications established in 1888]]
[[Category:English-language journals]]
[[Category:BMJ Group academic journals]]
[[Category:Veterinary medicine journals]]
[[Category:1888 establishments in the United Kingdom]]
[[Category:Academic journals associated with learned and professional societies]]