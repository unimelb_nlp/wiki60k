{{Infobox journal
| title = ACM Transactions on Mathematical Software
| cover = 
| editor = Mike Heroux
| discipline = [[Mathematical software]]
| abbreviation = 
| publisher = [[Association for Computing Machinery|ACM]]
| country = United States
| frequency = Quarterly
| history = 1975–present
| openaccess =
| license =
| impact = 
| impact-year = 
| website = http://toms.acm.org/
| link1 = http://portal.acm.org/toms
| link1-name = Online access
| link2 = http://portal.acm.org/toms/archive
| link2-name = Online archive
| JSTOR =
| OCLC = 
| LCCN =
| CODEN =
| ISSN = 0098-3500
| eISSN = 1557-7295
}}
'''''ACM Transactions on Mathematical Software''''' ('''''TOMS''''') is a quarterly scientific journal that aims to disseminate the latest findings of note in the field of numeric, symbolic, algebraic, and geometric computing applications. It has been published since March 1975 by the [[Association for Computing Machinery]] (ACM).

The journal is described as follows on the [[ACM Digital Library]] page:

{{quote|ACM Transactions on Mathematical Software (TOMS) documents the theoretical underpinnings of numeric, symbolic, algebraic, and geometric computing applications. It focuses on analysis and construction of algorithms and programs, and the interaction of programs and architecture. Algorithms documented in TOMS are available as the Collected Algorithms of the ACM in print, on microfiche, on disk, and online.}}

The purpose was first stated by its founding editor, Professor [[John R. Rice (professor)|John Rice]], in the [http://toms.acm.org/Volumes/V1.html inaugural issue]. The decision to found the journal came out of the 1970 Mathematical Software Symposium at [[Purdue University]], also organized by Rice, who negotiated with both [[Society for Industrial and Applied Mathematics|SIAM]] and the ACM regarding its publication.<ref>{{Cite journal | last1 = Boisvert | first1 = Ronald F. | year = 2000 | title = Mathematical software: past, present, and future | journal = Mathematics and Computers in Simulation | volume = 54 | issue = 4–5 | pages = 227–241 | publisher =  | jstor =  | doi = 10.1016/S0378-4754(00)00185-3 | url =  | format =  | accessdate =  | arxiv = cs/0004004}}</ref>

Algorithms described in the transactions are generally published in [http://calgo.acm.org/ '''Collected Algorithms'''].<ref>[http://toms.acm.org/AlgPolicy.html TOMS Algorithms Policy] {{webarchive |url=https://web.archive.org/web/20100815095347/http://toms.acm.org/AlgPolicy.html |date=August 15, 2010 }}</ref> Algorithms published in Collected Algorithms since 1975 (and some earlier ones) are available.

Authors publishing in ACM TOMS "are required to transfer the copyright to ACM upon acceptance of the algorithm for publication"[https://web.archive.org/web/20100815095347/http://toms.acm.org:80/AlgPolicy.html], and ACM subsequently distributes the software under the ''ACM Software License Agreement'', which is free of charge for noncommercial use only [http://www.acm.org/publications/policies/softwarecrnotice].  (This restriction means that ACM TOMS software is not [[Free and open-source software]] according to [[The Free Software Definition]] or [[The Open Source Definition]].)

==References==
{{reflist}}

==External links==
*[http://www.acm.org/toms Journal home page]
*[http://www.netlib.org/toms/index.html ACM Collected Algorithms]

[[Category:Association for Computing Machinery academic journals|Transactions on Mathematical Software]]
[[Category:Publications established in 1975]]