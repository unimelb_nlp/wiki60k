{{Refimprove|date=November 2009}}
{{Accounting}}
The '''revenue recognition''' principle is a cornerstone of [[accrual accounting]] together with the [[matching principle]]. They both determine the [[accounting period]], in which [[revenues]] and [[expenses]] are recognized. According to the principle, revenues are recognized when they are '''realized''' or realizable, and are earned (usually when goods are transferred or services rendered), no matter when cash is received. In [[cash accounting]] – in contrast – revenues are recognized when cash is received no matter when goods or services are sold.

Cash can be received in an earlier or later period than obligations are met (when goods or services are delivered) and related revenues are recognized that results in the following two types of accounts:
* [[Accrued revenue]]: Revenue is recognized before cash is received.
* [[Deferred revenue]]: Revenue is recognized after cash is received.
Revenue realized during an accounting period is included in the income.

== International Financial Reporting Standards criteria ==

''The Critical-Event Approach:
[[International Financial Reporting Standards|IFRS]] provides five criteria for identifying the critical event for recognizing revenue on the sale of goods<ref>http://www.ifrs.org/Current-Projects/IASB-Projects/Revenue-Recognition/Pages/Revenue-Recognition.aspx</ref>:
# Risks and rewards have been transferred from the seller to the buyer
# The seller has no control over the goods sold
# Collection of payment is reasonably assured
# The amount of revenue can be reasonably measured 
# Costs of earning the revenue can be reasonably measured

The first two criteria mentioned above are referred to as '''Performance'''. Performance occurs when the seller has done most or all of what it is supposed to do to be entitled for the payment. E.g.: A company has sold the good and the customer walks out of the store with no warranty on the product. The seller has completed its performance since the buyer now owns good and also all the risks and rewards associated with it.
The third criterion is referred to as '''Collectability'''. The seller must have a reasonable expectation of being paid. An allowance account must be created if the seller is not fully assured to receive the payment. The fourth and fifth criteria are referred to as '''Measurability'''. Due to Matching Principle, the seller must be able to match expenses to the revenues they helped in earning. Therefore, the amount of Revenues and Expenses should both be reasonably measurable

==General rule==
Received [[Advance payment|advances]] are not recognized as revenues, but as [[liability (accounting)|liabilities]] ([[deferred income]]), until the conditions (1.) and (2.) are met.
# Revenues are realized when cash or claims to cash ([[receivable]]) are received in exchange for goods or services. Revenues are realizable when assets received in such exchange are readily convertible to cash or claim to cash.
# Revenues are earned when such goods/services are transferred/rendered. Both such payment assurance and final delivery completion (with a provision for returns, warranty claims, etc.), are required for revenue recognition.

Recognition of revenue from four types of transactions:
# Revenues from selling [[inventory]] are recognized at the date of sale often interpreted as the date of delivery.
# Revenues from rendering services are recognized when services are completed and billed.
# Revenue from permission to use company's assets (e.g. interests for using money, rent for using [[fixed assets]], and royalties for using [[intangible assets]]) is recognized as time passes or as assets are used.
# Revenue from selling an asset other than inventory is recognized at the [[point of sale]], when it takes place.

===Revenue versus cash timing===
''[[Accrued revenue]]'' (or ''accrued assets'') is an asset such as proceeds from a delivery of goods or services, at which such income item is earned and the related [[revenue]] item is recognized, while cash for them is to be received in a later [[accounting period]], when its amount is deducted from ''accrued revenues''. It shares characteristics with ''deferred expense'' (or ''prepaid expense'', or ''prepayment'') with the difference that an asset to be covered later is cash paid out to a counterpart for goods or services to be received in a later period when the obligation to pay is actually incurred, the related [[expense]] item is recognized, and the same amount is deducted from ''prepayments''

''[[Deferred revenue]]'' (or ''deferred income'') is a [[liability (accounting)|liability]], such as cash received ''from a counterpart for goods or services'' which are to be delivered in a later [[accounting period]], when such income item is earned, the related [[revenue]] item is recognized, and the ''deferred revenue'' is reduced. It shares characteristics with ''accrued expense'' with the difference that a liability to be covered later is an obligation to pay for goods or services received solo ''from a counterpart'', while cash for them is to be paid out in a later period when its amount is deducted from ''accrued expenses''.

For example, a company receives an annual [[software license]] fee paid out by a customer upfront on the January 1. However the company's [[fiscal year]] ends on May 31. So, the company using accrual accounting adds only five months worth (5/12) of the fee to its [[revenues]] in [[profit and loss]] for the fiscal year the fee was received. The rest is added to ''[[deferred income]]'' (liability) on the [[balance sheet]] for that year.

===Advances===
[[Advance payment|Advances]] are not considered to be a sufficient evidence of sale, thus no revenue is recorded until the sale is completed. Advances are considered a [[deferred income]] and are recorded as [[liability (accounting)|liabilities]] until the whole price is paid and the delivery made (i.e. [[matching principle|matching]] obligations are incurred).

==Exceptions==

===Revenues not recognized at sale===
The rule says that revenue from selling [[inventory]] is recognized at the point of sale, but there are several exceptions.
* '''[[Buyback agreement]]s''': buyback agreement means that a company sells a product and agrees to buy it back after some time. If buyback price covers all costs of the inventory plus related holding costs, the inventory remains on the seller's books. In plain: there was no sale.
* '''[[Returning|Returns]]''': companies which cannot reasonably estimate the amount of future returns and/or have extremely high rates of returns should recognize revenues only when the right to return expires. Those companies that can estimate the number of future returns and have a relatively small return rate can recognize revenues at the point of sale, but must deduct estimated future returns.

===Revenues recognized before sale===

====Long-term contracts====
This exception primarily deals with long-term contracts such as constructions (buildings, stadiums, bridges, highways, etc.), development of aircraft, weapons, and space exploration hardware. Such contracts must allow the builder (seller) to bill the purchaser at various parts of the project (e.g. every 10 miles of road built).
*The '''[[percentage-of-completion method]]''' says that if the contract clearly specifies the price and payment options with transfer of ownership, the buyer is expected to pay the whole amount and the seller is expected to complete the project, then revenues, costs, and [[gross profit]] can be recognized each period based upon the progress of construction (that is, percentage of completion). For example, if during the year, 25% of the building was completed, the builder can recognize 25% of the expected total profit on the contract. This method is preferred. However, expected loss should be recognized fully and immediately due to conservatism constraint. Apart from accounting requirement, there is a need for calculating the percentage of completion for comparing budgets and actuals to control the cost of long term projects and optimize Material, Man, Machine, Money and time (OPTM4) .The method used for determining revenue of a long term contract can be complex. Usually two methods are employed to calculate the percentage of completion: (i) by calculating the percentage of accumulated cost incurred to the total budgeted cost. (ii) by determining the percentage of deliverable completed as a percentage of total deliverable. The second method is accurate but cumbersome. To achieve this, one needs the help of a software ERP package which integrates Financial, inventory, Human resources and WBS (Work breakdown structure) based planning and scheduling while booking of all cost components should be done with reference to one of the WBS elements. There are very few contracting ERP software packages which have the complete integrated module to do this.
*The '''[[completed-contract method]]''' should be used only if percentage-of-completion is not applicable or the contract involves extremely high risks. Under this method, revenues, costs, and gross profit are recognized only after the project is fully completed. Thus, if a company is working only on one project, its income statement will show $0 revenues and $0 construction-related costs until the final year. However, expected loss should be recognized fully and immediately due to conservatism constraint.

====Completion of production basis====
This method allows recognizing revenues even if no sale was made. This applies to agricultural products and minerals.There is a ready market for these products with reasonably assured prices, the units are interchangeable, and selling and distributing does not involve significant costs.

===Revenues recognized after Sale===
Sometimes, the collection of receivables involves a high level of risk. If there is a high degree of uncertainty regarding collectibility then a company must defer the recognition of revenue. There are three methods which deal with this situation:
* '''[[Installment sales method]]''' allows recognizing income after the sale is made, and proportionately to the product of gross profit percentage and cash collected calculated. The unearned income is deferred and then recognized to income when cash is collected.<ref name=Revsine2002110>{{harvnb|Revsine|2002|p=110}}</ref> For example, if a company collected 45% of total product price, it can recognize 45% of total profit on that product.
* '''[[Cost recovery method]]''' is used when there is an extremely high probability of uncollectable payments. Under this method no profit is recognized until cash collections exceed the seller's cost of the merchandise sold. For example, if a company sold a machine worth $10,000 for $15,000, it can start recording profit only when the buyer pays more than $10,000. In other words, for each dollar collected greater than $10,000 goes towards your anticipated gross profit of $5,000.
* '''[[Deposit method]]''' is used when the company receives cash before sufficient transfer of ownership occurs. Revenue is not recognized because the risks and rewards of ownership have not transferred to the buyer.<ref>{{cite web | author=Financial Accounting Standards Board | year=2008| title=Statement of Financial Accounting Standards No. 66, Paragraph 65 | url=http://fasb.org/pdf/aop_FAS66.pdf | work= | accessdate=March 23, 2009}}</ref>
* [[Generally accepted accounting principles]]
* [[Comparison of cash and accrual methods of accounting]]
* [[Vendor-specific objective evidence]]

== New Revenue Recognition Standard ==

On May 28, 2014, the FASB and IASB issued converged guidance on recognizing revenue in contracts with customers. The new guidance is heralded by the Boards as a major achievement in efforts to improve financial reporting.<ref>{{Cite web|title = Revenue Recognition|url = http://www.fasb.org/jsp/FASB/Page/BridgePage&cid=1351027207987|website = www.fasb.org|accessdate = 2015-12-13}}</ref> The update was issued as Accounting Standards Update (ASU) 2014-09. It will be part of the Accounting Standards Codification (ASC) as Topic 606: ''Revenue from Contracts with Customers'' (ASC 606)'','' and supersedes the existing revenue recognition literature in Topic 605 issued by FASB.<ref name="Overview of ASC 606 - RevenueHub">{{Cite web|title = Overview of ASC 606 - RevenueHub|url = http://www.revenuehub.org/overview-of-asc-606/|website = RevenueHub|accessdate = 2015-12-13|language = en-US}}</ref>  ASC 606 is effective for public entities for the first interim period within annual reporting periods beginning after December 15, 2017 (nonpublic companies have an additional year).<ref>{{Cite web|url=https://www.pwc.com/us/en/cfodirect/publications/in-brief/fasb-one-year-deferral-new-revenue-standard-606.html|title=In brief: FASB finalizes one-year deferral of the new revenue standard|last=PricewaterhouseCoopers|website=PwC|access-date=2016-05-18}}</ref>

The new standard aims to:
* Remove inconsistencies and weaknesses in revenue requirements
* Provide a more robust framework for addressing revenue issues
* Improve comparability of revenue recognition practices across entities, industries, jurisdictions, and capital markets
* Provide more useful information to users of financial statements through improved disclosure requirements
* Simplify the preparation of financial statements by reducing the number of requirements to which an entity must refer<ref name="Overview of ASC 606 - RevenueHub"/>
The new revenue guidance was issued by the IASB as IFRS 15. The IASB’s standard, as amended, is effective for the first interim period within annual reporting periods beginning on or after January 1, 2018, with early adoption permitted.<ref>{{Cite web|url=https://www.pwc.com/us/en/cfodirect/publications/in-brief/principal-vs-agent-new-revenue-standard-606.html|title=In brief: IASB proposes changes to revenue standard - more FASB proposals coming soon|last=PricewaterhouseCoopers|website=PwC|access-date=2016-05-18}}</ref>

==References==
<references />

==Sources==
* {{citation|last=Revsine|first=Lawrence|title=Financial reporting & analysis|url=https://books.google.com/books?id=WgNFAAAAYAAJ|year=2002|publisher=Prentice Hall|isbn=978-0-13-032351-4}}

[[Category:Revenue]]
[[Category:Accounting terminology]]