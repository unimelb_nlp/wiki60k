{{Use mdy dates|date=March 2015}}
{{Infobox road
|state=MI
|type=CDH
|route=C-66
|map=Michigan C-66 map.png
|maint={{abbr|ECRC|Emmet County Road Commission}} and {{abbr|CCRC|Cheboygan County Road Commission}}
|length_mi=28.047
|length_ref=<ref name=PRFA/>
|alternate_name=Levering Road<br/>Cheboygan Road
|established={{circa|October 5, 1970}}<ref name=HS/>
|direction_a=West
|terminus_a={{jct|state=MI|CDH|C-77|county1=Emmet}} in [[Cross Village, Michigan|Cross Village]]
|junction={{jct|state=MI|CDH|C-81|county1=Emmet}} in [[Bliss Township, Michigan|Bliss Township]]<br>
{{jct|state=MI|US|31}} in [[Levering, Michigan|Levering]]<br/>
{{jct|state=MI|I|75}} near [[Cheboygan, Michigan|Cheboygan]]
|direction_b=East
|terminus_b={{jct|state=MI|US|23|M|27}} in Cheboygan
|counties=[[Emmet County, Michigan|Emmet]], [[Cheboygan County, Michigan|Cheboygan]]
|previous_type=CDH
|previous_route=C-65
|next_type=CDH
|next_route=C-71
}}
'''C-66''' is a [[List of County-Designated Highways in Michigan|county-designated highway]] in the US state of [[Michigan]] running about {{convert|28|mi|km}} across the northern tip of the [[Lower Peninsula of Michigan|Lower Peninsula]]. The roadway starts in the [[unincorporated community]] of [[Cross Village, Michigan|Cross Village]] in [[Emmet County, Michigan|Emmet County]] at an intersection with [[M-119 (Michigan highway)|M-119]] and [[C-77 (Michigan county highway)|C-77]]. It follows Levering Road through rural areas to an interchange with [[Interstate 75 in Michigan|Interstate 75]] (I-75) west of [[Cheboygan, Michigan|Cheboygan]]. The eastern terminus is at an intersection with [[U.S. Route 23 in Michigan|US Highway&nbsp;23]] (US&nbsp;23) and [[M-27 (Michigan highway)|M-27]] in downtown Cheboygan. The first roadways along what is now C-66 were in place by the early 20th century. Segments were paved by 1936, although some reverted to a gravel surface during [[World War II]]. The full roadway was paved by the mid-1950s, and the C-66 moniker was designated on the roadway in the early 1970s.

==Route description==
The roadway starts at a junction with M-119 (Lake Shore Drive) and C-77 (State Road) in the community of Cross Village near [[Lake Michigan]] in Emmet County. C-66 travels southward for about {{convert|300|ft|m}} [[concurrency (road)|concurrently along]] C-77 to the intersection with Levering Road. From there, C-66 turns eastward on Levering Road, leaving town. The landscape outside of town is mostly woodland with scattered fields. Except for a jog near Hill Road in [[Bliss Township, Michigan|Bliss Township]], Levering Road is a straight road running due east. C-66 intersects [[C-81 (Michigan county highway)|C-81]] (Pleasantview Road) in a clearing before entering the community of [[Levering, Michigan|Levering]]. The county road passes through some residential areas and by a baseball field before meeting [[U.S. Route 31 in Michigan|US&nbsp;31]]. C-66 turns south to run concurrently along US&nbsp;31 for about a quarter mile (0.4&nbsp;km) before turning eastward again.<ref name="MDOT11">{{cite MDOT map |year= 2011 |sections= E10–E11}}</ref><ref name="google">{{google maps |url= https://maps.google.com/maps?saddr=N+State+Rd&daddr=Main+St+%26+State+St,+Cheboygan,+MI&hl=en&sll=45.641168,-85.0383&sspn=0.015062,0.014548&geocode=FdRvuAId5Wvu-g%3BFQyEuAIdRgT3-ilTC6sExpc1TTF4LwI30ByeKg&vpsrc=0&mra=ls&t=h&z=10 |title= Overview Map of C-66 |accessdate= April 5, 2008}}</ref>

Levering Road continues easterly leaving town south of Sherett Lake. East of the intersection with Ingleside Road, C-66 crosses into Cheboygan County. The roadway curves to the northeast before returning to a due eastward course north of Munro Lake along the [[Hebron Township, Michigan|Hebron]]–[[Munro Township|Munro]] township line.. The landscape changes to include more farm fields once road passes the lake. C-66 intersects I-75 at exit&nbsp;326 along the latter. On the east side of the interchange, Levering Road passes Sea Shell City, a [[tourist trap]], and turns northeastward into Hebron Township. C-66 then turns easterly again before it crosses into [[Beaugrand Township, Michigan|Beaugrand Township]], taking the additional name Cheboygan Road. The roadway crosses branches of the [[Little Black River (Cheboygan County)|Little Black River]] and passes to the south of the [[Cheboygan County Airport]]. East of the airport, C-66 enters the city of Cheboygan and follows State Street into town; this area is residential in nature. State Street crosses the [[North Central State Trail]], a [[rail trail]], before turning to the southeast. The roadway passes into the northern end of downtown near several smaller businesses.. C-66 terminates at a four-way intersection with Main Street one block west of the [[Cheboygan River]]; this intersection also marks the northern terminus of M-27.<ref name=MDOT11/><ref name=google/> As a county-designated highway, C-66 is maintained by the Emmet and Cheboygan county road commissions (ECRC, CCRC) in their respective counties.<ref name=PRFA/>

==History==
Roadways along the route of Levering Road in Cheboygan County existed as far back as 1902 in Hebron and Munro townships; the section south of the township line was not yet built at the time.<ref name=CC1902>{{cite map |location= Minneapolis |publisher= Consolidated Publishing |year= 1902 |title= Plat Book of Cheboygan County, Michigan |url= http://www.historicmapworks.com/Atlas/US/16489/Cheboygan+County+1902/ |first1= P.A. |last1= Myers |first2= J.W. |last2= Myers |last-author-amp= yes |at=  pp. 13,16. Hebron and Munro township insets |scale= 1:31,680 |oclc= 35062217 |accessdate= April 6, 2012 |via= Historic Map Works}}</ref>  By 1927, Levering Road extended across the tip of the Lower Peninsula; near the Hebron–Beaugrand township line in Cheboygan County, the county road made a jog to follow what is now Wollangur and Hill roads.<ref name=MSHD27-12>{{cite MDOT map |date= 1927-12-01 |link= yes }}</ref> In 1930, the ECRC paved Levering Road between US&nbsp;31 and the county line and the CCRC paved the roadway in Beaugrand Township.<ref name=MSHD30-01>{{cite MDOT map |date= 1930-01-01 |c-link= yes }}</ref><ref name=MSHD30-07>{{cite MDOT map |date= 1930-07-01 }}</ref> By mid 1936, it was paved from a point between Cross Village and the Pleasantview Road intersection to the county line in Emmet County as well as the Beaugrand Township section in Cheboygan County.<ref name=MSHD36-06>{{cite MDOT map |date= 1936-06-01 |c-link= yes |sections= E10–E11 }}</ref> During [[World War II]], the counties reverted most of Levering Road to a gravel surface; pavement in Emmet County started east of Pleasantview Road instead of west, and Cheboygan County had no sections paved. At the same time, the new roadway was opened to bypass Wollangur and Hill roads.<ref name=MSHD42-06>{{cite MDOT map |date= 1942-06-01 |sections= E10–E11 }}</ref><ref name=MSHD45-10>{{cite MDOT map |date= 1945-10-01 |sections= E10–E11 }}</ref>

In late 1949 or early 1950, the ECRC repaved Levering Road westward back to the point between Cross Village and Pleasantview Road that had been paved before the war.<ref name=MSHD49-07>{{cite MDOT map |date= 1949-07-01 |section= E10 }}</ref><ref name=MSHD50-04>{{cite MDOT map |date= 1950-04-15 |section= E10 }}</ref> In late 1951 or early 1952, the CCRC paved its section of Levering Road.<ref name=MSHD51-07>{{cite MDOT map |date= 1951-07-01 |sections= E10–E11 }}</ref><ref name=MSHD52-04>{{cite MDOT map |date= 1952-04-15 |sections= E10–E11}}</ref> In late 1954 or early 1955, the ECRC completed paving all of Levering Road in its jurisdiction.<ref name=MSHD54-10>{{cite MDOT map |date= 1954-10-01 |section= E10 }}</ref><ref name=MSHD55-04>{{cite MDOT map |date= 1955-04-15 |section= E10 }}</ref> The county-designated highway system was created around after October 5, 1970.<ref name=HS>{{cite news |title= County Primary Road Marking System Okayed |work= [[Holland Sentinel|Holland Evening Sentinel]] |date= October 5, 1970 |page= 6}}</ref> The C-66 designation was first shown on the 1971 state map following the routing used today.<ref name="MDSH71">{{cite MDOT map |year= 1971 |link= yes |sections= E10–E11}}</ref>
{{-}}

==Major intersections==
{{MIinttop|ref=<ref name="PRFA">{{cite MDOT PRFA |link= yes |access-date= January 9, 2012}}</ref> }}
{{MIint
|county=Emmet
|cspan=5
|location=Cross Village
|lspan=2
|mile=0.000
|type=concur
|road={{jct|state=MI|M|119|name1=Lake Shore Drive|dir1=south}}<br/>{{jct|state=MI|CDH|C-77|county1=Emmet|name1=State Road|dir1=ends}}
|notes=Common terminus of C-66 and C-77}}
{{MIint
|mile=0.058
|type=concur
|road={{jct|state=MI|CDH|C-77|county1=Emmet|name1=State Road|city1=Harbor Springs|dir1=south}}
|notes=Southern end of C-77 concurrency}}
{{MIint
|location=Bliss Township
|mile=8.128
|road={{jct|state=MI|CDH|C-81|county1=Emmet|city1=Petoskey|city2=Mackinaw City|name1= Pleasantview Road}}
|notes=}}
{{MIint
|location=Levering
|lspan=2
|mile=12.548
|type=concur
|road={{jct|state=MI|US|31|Tour|LMCT|dir1=north|city1=Mackinaw City|name1=Mackinac Highway}}
|notes=Northern end of US&nbsp;31 concurrency}}
{{MIint
|type=concur
|mile=12.798
|road={{jct|state=MI|US|31|Tour|LMCT|dir1=south|city1=Petoskey|name1=Mackinac Highway}}
|notes=Southern end of US&nbsp;31 concurrency}}
{{MIint
|county=Cheboygan
|cspan=2
|location_special=[[Hebron Township, Michigan|Hebron]]–[[Munro Township, Michigan|Munro]] township line
|mile=20.035
|mile2=20.053
|road={{jct|state=MI|I|75|name1=G. Mennen Williams Freeway|city1=Saginaw|location2=[[Mackinac Bridge]]}}
|notes=Exit&nbsp;326 on I-75}}
{{MIint
|location=Cheboygan
|mile=28.047
|road={{jct|state=MI|US|23|city1=Rogers City|city2=Mackinaw City|name1=Main Street, State Street}}<br>{{jct|state=MI|M|27|name1=Main Street|dir1=south|city1=Indian River}}
|notes=Eastern terminus of C-66; State Street continues eastward as part of US&nbsp;23; Main Street is US&nbsp;23 north and M-27 south of the intersection}}
{{jctbtm|keys=concur}}

==See also==
*{{portal-inline|Michigan Highways}}

==References==
{{reflist|30em}}

==External links==
{{Attached KML |display=inline,title}}
* [http://www.michiganhighways.org/listings/MichHwysA2-C81.html#C-66 C-66 at Michigan Highways]

{{DEFAULTSORT:C66}}
[[Category:County-designated highways in Michigan]]
[[Category:Transportation in Emmet County, Michigan]]
[[Category:Transportation in Cheboygan County, Michigan]]

{{good article}}