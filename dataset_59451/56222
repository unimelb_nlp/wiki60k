{{Infobox journal
| title         = Chinese Journal of Chemical Physics
| editor        = Xue-ming Yang (EiC)
| discipline    = [[Physics]], [[Chemistry]]
| language      = [[English language|English]]
| abbreviation  = Chin. J. Chem. Phys.
| publisher     = [[Chinese Physical Society]]
| country       = [[China]]
| frequency     = 6 issues per year
| history       = 1988–present
| openaccess    = 
| impact        = 0.496
| SJR           = 
| impact-year   = 2015
| website       = http://cjcp.ustc.edu.cn/hxwlxb_en/ch/index.aspx
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 1674-0068
| eISSN         = 2327-2244
| cdISSN        = 
}}

The '''Chinese Journal of Chemical Physics''' ('''CJCP''') is a peer reviewed journal published by the [[Chinese Physical Society]] and hosted by the [[American Institute of Physics]]. It publishes experimental, computational and theoretical research on the interdisciplinary fields of physics, chemistry, biology and materials sciences.<ref>About CJCP, http://cjcp.ustc.edu.cn/hxwlxb_en/ch/reader/view_news.aspx?id=20150114022159001, retrieved in July 26, 2016</ref> The journal is currently edited by Xue-ming Yang (杨学明) of the [[Dalian Institute of Chemical Physics]], [[Chinese Academy of Sciences]]. CJCP publishes around 120 articles per year via bimonthly issues and has an impact factor of 0.496 (2015).

==History==

CJCP was established in 1988. From 2006 to 2012 its content was hosted by [[IOP Publishing]], and from 2013 by the American Institute of Physics (AIP).<ref>AIP - About the Journal, http://scitation.aip.org/content/cps/journal/cjcp/info/about, retrieved in July 31, 2016</ref> As of 2006, the journal started publishing abstracts in Chinese as well.

== External links ==
* [http://cjcp.ustc.edu.cn/hxwlxb_en/ch/index.aspx ''Chinese Journal of Chemical Physics'' website]

==References==
{{Reflist}}

[[Category:Physics journals]]
[[Category:Chemistry journals]]
[[Category:English-language journals]]
[[Category:Chinese Physical Society academic journals]]