{{Infobox journal
| title         = Journal of Integer Sequences
| cover         = JIS logo for wiki.gif
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = J. Integer Seq.
| discipline    = [[Integer sequence]]s
| peer-reviewed = 
| language      = 
| editor        = [[Jeffrey Shallit]]
| publisher     = [[University of Waterloo]]
| country       = 
| history       = 1998&ndash;present
| frequency     = Irregular
| openaccess    = Yes
| license       = 
| impact        = 
| impact-year   = 
| ISSNlabel     =
| ISSN          = 1530-7638
| eISSN         =
| CODEN         =
| JSTOR         = 
| LCCN          = 
| OCLC          = 42458787
| website       = https://cs.uwaterloo.ca/journals/JIS/
| link1         = https://cs.uwaterloo.ca/journals/JIS/vol1.html
| link1-name    = Online access
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}
The '''''Journal of Integer Sequences''''' is a [[peer review|peer-reviewed]] [[open access|open-access]] [[academic journal]] in mathematics, specializing in research papers about [[integer sequence]]s.

It was founded in 1998 by [[Neil Sloane]].<ref name="eej"/> Sloane had previously published two books on integer sequences, and in 1996 he founded the [[On-Line Encyclopedia of Integer Sequences]] (OEIS). Needing an outlet for research papers concerning the sequences he was collecting in OEIS, he founded the journal.<ref>{{citation|url=http://www.research.att.com/articles/featured_stories/2012_03/201203_OEIS.html|title=The Achievement of The Online Encyclopedia of Integer Sequences|publisher=[[AT&T Labs Research]]|date=March 6, 2012|accessdate=2015-08-29}}.</ref><ref>{{citation|url=http://oeis.org/FAZ2.html|title=Die Leidenschaft eines Zahlenreihen-Sammlers|trans_title=The Passion of a Collector of Number Sequences|newspaper=[[Frankfurter Allgemeine Zeitung]]|date=May 9, 2001}}.</ref> Since 2002 the journal has been hosted by the [[David R. Cheriton School of Computer Science]] at the [[University of Waterloo]], with Waterloo professor [[Jeffrey Shallit]] as its editor-in-chief. There are no page charges for authors, and all papers are free to all readers. The journal publishes approximately 50–75 papers annually.<ref name="eej">{{citation
 | last = Shallit | first = Jeffrey | authorlink = Jeffrey Shallit
 | date = February 2015
 | department = Scripta Manent
 | doi = 10.1090/noti1208
 | issue = 2
 | journal = [[Notices of the American Mathematical Society]]
 | pages = 169–171
 | title = Editing an electronic journal
 | volume = 62}}.</ref>

In most years from 1999 to 2014, [[SCImago Journal Rank]] has ranked the ''Journal of Integer Sequences'' as a third-quartile journal in [[discrete mathematics]] and [[combinatorics]].<ref>{{citation|url=http://www.scimagojr.com/journalsearch.php?q=23914&tip=sid|title=SCImagoJR report: ''Journal of Integer Sequences''|accessdate=2015-08-29}}.</ref> It is indexed by ''[[Mathematical Reviews]]''<ref>{{citation|url=http://www.ams.org/mathscinet/search/journaldoc.html?jc=JINTS|title=Journal information for the Journal of Integer Sequences|publisher=[[MathSciNet]]|accessdate=2015-08-29}}</ref> and [[Zentralblatt MATH]].<ref>{{citation|url=https://zbmath.org/journals/?s=0&q=journal+of+integer+sequences|title=Zentralblatt MATH: Journal of Integer Sequences|publisher=[[Zentralblatt MATH]]|accessdate=2016-02-18}}</ref>

==References==
{{reflist}}

==External links==
*{{official website|https://cs.uwaterloo.ca/journals/JIS/}}

[[Category:Integer sequences]]
[[Category:Mathematics journals]]
[[Category:Open access journals]]
[[Category:Publications established in 1998]]
[[Category:English-language journals]]