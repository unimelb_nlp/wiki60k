{{Use dmy dates|date=June 2011}}
{{Infobox company
| name   = Mazars
| logo   = File:Mazars.svg
| type   = Société cooperative à responsabilité limitée, based in Belgium
| foundation     = [[Rouen]], [[France]] (1940)
| location       = [[Paris]], [[France]]
| key_people     = Philippe Castagnac, Group chairman and CEO
| industry       = [[Professional services]]
| services       = [[Audit]]<br />[[Chartered accountant]]<br />[[Consultant|Consulting]]<br />[[Taxation]]<br />[[Corporate finance]]<br />[[Financial advisory]]<br />[[Legal]]
| revenue        = {{profit}}€1.252 billion [[euro]] (2016)
| num_employees  = 17,000 (2016)
| homepage       = [http://www.mazars.com www.mazars.com]
}}
'''Mazars''' is a global [[audit]], [[accounting]] and [[Consultant|consulting]] group employing more than 18,000 professionals in 79 countries through member firms.<ref>[http://www.mazars.co.uk/Home/About-us/Facts-and-figures Facts and figures - Mazars - United Kingdom<!-- Bot generated title -->]</ref>  With head offices in [[France]], Mazars has a network of correspondent partners and joint ventures in a further 21 countries and is a founding member of the Praxity alliance, a network of independent firms.

Worldwide turnover for the integrated partnership for the fiscal year ending August 2014 was 1.081 billion euro.<ref>{{cite web|url=http://annualreport.mazars.com|title=Creating Shared Value: Mazars Group publishes its 2014-2015 Annual Report|publisher=|accessdate=12 July 2016}}</ref> Mazars operates as a single entity as a fully integrated partnership. Mazars publicizes its consolidated financial accounts, a move it claims is unusual for private audit and advisory firms.<ref>http://annualreport.mazars.com/ - annual report website at 03:47, 7 March 2011 (UTC)</ref>

== History ==
The original Mazars firm was formed in [[Rouen]], [[Normandy]] in [[France]] in 1940, by Robert Mazars. Mazars stayed a local firm until the 1980s when former [[CEO]] [[Patrick de Cambourg]] started to internationalize the firm growing the business from 33 employees in 1977 to the global firm of today.<ref>[http://www.mazars.com/Home/About-us/Key-facts-organisation/Our-roots History of Mazars]</ref>

=== France ===
Mazars merged with accounting firm Guérard Viala to form Mazars & Guérard in 1995.

=== United Kingdom ===
On 1 September 1998 Mazars & Guerard merged with the British accountancy firm Neville Russell and traded, in the [[UK]],  for a number of years as "Mazars Neville Russell". In 2002, ''"Mazars Neville Russell"'', as well as its counterpart firms across [[Europe]], changed the name to become simply Mazars. Mazars currently employs 1,600 people in 20 offices<ref>{{cite web|url=http://www.mazars.co.uk/Home/About-us/Mazars-in-the-United-Kingdom|title=Mazars, a different player in auditing, accounting, tax and advisory services in the United Kingdom|publisher=|accessdate=12 July 2016}}</ref><ref>[http://www.mazars.co.uk/aboutus/150.html Detail of UK offices]</ref> in the UK and turnover is around €100m.<ref>[http://www.mazars.com/mazars/europe.php Mazars expansion]</ref>

In April 2007 Mazars merged with the London office of MRI Moores Rowland which will create a firm with turnover in the UK exceeding £90million.<ref>[http://www.financialdirector.co.uk/accountancyage/news/2187531/mazars-mri-moores-rowland-seal Announcement of merger with MRI Moores Rowland]/</ref>

Mazars, prior to the MRI Moores Rowland merger, was listed as the 14th-largest accountancy firm in the UK with about £65 millions fee income,<ref>{{cite web|url=http://www.accountancyage.com/resource/top50|title=Top50 +50 firms face ICAEW audit fines - Accountancy Age|publisher=|accessdate=12 July 2016}}</ref> however following the merger and with the merger between [[Grant Thornton]] and [[RSM Robson Rhodes]] in the summer of 2007, Mazars was listed as
12th<ref>http://www.accountancyage.com/resource/top50|Summer 2007 Top 50 UK accounting firms</ref> with an estimated fee income of £90.3m.

''"Neville Russell"'' was founded in 1900 by Charles Neville Russell to work principally with the insurance sector of the economy. Neville Russell developed a reputation in London of being specialists in this area.

Mazars is a keen advocate of a more open audit market and notably of [[joint audit]].

=== Netherlands ===

As of 2000 Mazars integrated ''"Paardekooper & Hoffman"'' (of the [[Netherlands]]), which employed 800 people. In the Dutch audit market Mazars ranks as the sixth-largest firm and contributes more than 15% of the total global turnover.

Jan Paardekooper founded ''"Paardekooper & Hoffman"'' in 1927. The firm worked with large clients involved in the maritime and harbour businesses of Rotterdam.

=== Germany: Roever Broenner Susat Mazars ===

In April, 2015 Mazars and RBS RoeverBroennerSusat announced their merger which united about 1,000 professionals, including 68 partners, and €110 millions of turnover in the German market. Today (2015), Roever Broenner Susat Mazars has twelve offices in German metropolitan areas offering a range of audit, accountancy, tax, legal and advisory services.<ref>{{cite web|url=http://eng.mazars.de/Home/About-us/Two-become-one|title=Two become one|publisher=|accessdate=12 July 2016}}</ref><ref>{{cite web|url=http://www.manager-magazin.de/unternehmen/artikel/wirtschaftsprueferfusion-von-roever-broenner-suasat-und-mazars-a-1029577.html|title=Wirtschaftsprüferfusion von Röver Brönner Suasat und Mazars - manager magazin|publisher=|accessdate=12 July 2016}}</ref>

=== Russia and Kyrgyzstan ===
Mazars has been present in Russia since 1995 and is an international player in the Russian market of audit and consulting services. Today, Mazars offices<ref>[http://www.aebrus.ru/en/for-aeb-members/our-members.php?ELEMENT_ID=6287 Mazars offices]</ref> in Moscow, St. Petersburg and Bishkek (Kyrgyzstan) employ more than 200 professionals. Mazars client portfolio includes more than 600 companies, including credit institutions that are included in the Top-100, large insurance and financial organizations, as well as banks belonging to the largest international holdings. The company has significantly improved its positions in the annual ratings published by http://www.raexpert.ru/rankingtable/auditors/2014/main/ RAEX ([[Expert RA]]) [Rating of Leading Audit and Consulting Groups (April 2015)] and the [http://www.kommersant.ru/doc/2717251 magazine Kommersant. Dengi] in April 2015. Mazars in Russia is on the list of audit firms recognized by the EBRD and other credit institutions. [[:ru:Mazars#cite note-:0-14|Mazars Russia]] is also a member of various international organizations: [http://www.aebrus.ru/en/for-aeb-members/our-members.php?ELEMENT_ID=6287 Association of European Businesses in Russia], [http://www.ccifr.ru/index.php?pid=38&page=6 CCI France Russie], [http://www.rbcc.com/index.php?option=com_content&view=article&id=281%3Am&catid=78%3Amembers-listing&Itemid=341&lang=en Russo-British Chamber of Commerce], [http://www.m-auditchamber.ru/reester/vedenie/firm.php Moscow Audit Chamber], [http://www.accaglobal.com/gb/en.html ACCA], [http://ru.spiba.ru/chlenstvo/spisok-chlenov/mazars St. Petersburg International Business Association for North-West Russia].

=== China ===
In January, 2016 Mazars and Chinese firm ZhongShen ZhongHuan announced their merger, adding a further 1,800 professionals to Mazars globally, including 83 partners, from 15 offices across mainland China. Mrs. Zhang Liwen, Chief Chartered Accountant of ZhongShen ZhongHuan, was subsequently appointed to the Mazars Group Executive Board, while Mr. Shi Wenxian, Chief Partner of ZhongShen ZhongHuan, was appointed to the Mazars Group Governance Council.<ref>{{Cite web|url=https://www.accountancyage.com/aa/news/2442162/mazars-merger-with-chinese-firm-zhongshen-zhonghuan|title=Mazars merger with Chinese firm ZhongShen ZhongHuan {{!}} Accountancy Age|website=www.accountancyage.com|access-date=2016-08-05}}</ref>

=== Malaysia ===
In 1986, Hew & Co (established in 1955) and Tan Toh Hua & Partners (established in 1958) had merged to form Hew & Tan. In the year 1999, Hew & Tan had changed their name to Moores Rowland as a rebranding exercise. On 1 September 2008, the Kuala Lumpur office of Moores Rowland merged with the global integrated structure of Mazars. To implement the merger, a new firm, Mazars (AF 001954), was registered to assume all existing mandates and statutory audit appointments of the Kuala Lumpur office of Moores Rowland.

=== Pakistan ===
In April, 2010 Mazars integrated BearingPoint into their business in Pakistan, thus adding advisory services in management and technology for sectors such as microfinance, financial services and within the public sector.<ref>{{cite web|url=http://www.mazars.com/Home/News-Media/Latest-news2/Mazars-integrates-BearingPoint-Pakistan|title=Mazars integrates BearingPoint Pakistan|publisher=|accessdate=12 July 2016}}</ref>

=== Poland ===
The Mazars office opened in Warsaw in 1992 in order to meet the expectations of the growing number of foreign investors coming to Poland.

Nowadays, Mazars ranks among the top audit and advisory firms in Poland. It is present in Warsaw and Cracow, with four partners and 200 professionals working to help companies grow by offering a full range of audit, tax, accounting, HR advisory and payroll solutions. Mazars in Poland serves more than 800 small, medium and large international and Polish enterprises, including those listed at the Warsaw Stock Exchange.

=== Qatar ===
Further to a successful cooperation initiated in 2008, Ahmed Tawfik & Co. CPA, an accounting and auditing services firm in Qatar which traces its roots back to 1976, has joined Mazars international partnership in September 2011 and thus becoming [http://www.mazarsqatar.com/ Mazars Ahmed Tawfik & Co. CPA].

[http://www.mazarsqatar.com/ Mazars Ahmed Tawfik & Co. CPA] specializes in audit, accounting, tax and advisory services and can count on 30 professionals in its office.

=== Turkey ===
Mazars Denge was founded by two partners as Denge Denetim YMM A.S. in 1977. Today Mazars Turkey operates in six offices – Istanbul (3 offices), Ankara, İzmir, Denizli, Gaziantep and Bursa – with a more than 350-person workforce.

Mazars Turkey is among the top five audit and accounting firms in Turkey and serves a portfolio of more than 500 clients in various sectors from which 50% are foreign companies with 19 partners (including 13 CPA).

Mazars Turkey Won The Tax Firm Of The Year Award in 2008 and Best Transfer Pricing Firm Award in 2009.<ref>{{cite web|url=http://www.mazars.com.tr|title=Anasayfa|publisher=|accessdate=12 July 2016}}</ref>

=== United States ===
In April 2010, Mazars and Weiser, an audit and advisory firm with a strong presence in the northeastern region of the US, announced their merger. Partners from both entities voted to incorporate 74 Weiser partners into Mazars’ international integrated partnership. The deal marked a new stage in Mazars’ international development.<ref>{{Cite web|url=http://www.forbes.com/sites/francinemckenna/2011/05/17/bienvenue-french-audit-firm-mazars-arrives-in-u-s/#6129b28e38aa|title=Bienvenue! French Audit Firm Mazars Arrives In U.S.|last=McKenna|first=Francine|access-date=2016-08-05}}</ref>

===Thailand===
{{Original research|section|date=July 2016}}
In 2007, continuing its Asian expansion, Mazars merged with the Double Impact Group in Thailand. 

Since then, Mazars Thailand<ref>[http://www.mazars.co.th Mazars Thailand]</ref> has achieved consistent strong growth to become the 6th-largest international professional services firm in Thailand, with over 150 consultants and 7 partners. It is a market leader in outsourced accounting services, as well as a major provider of audit, tax, legal and advisory services to over 500 international and Thai clients.

In November 2016, a further professional milestone was achieved, as Mazars Thailand was awarded a listed company audit license by the Thai Securities & Exchange Commission. 

=== Czech Republic ===
Mazars Czech Republic was founded in 1995 and employs around 180 professionals.<ref name=Mazars>{{cite web|title=Domovská stránka|url=http://www.mazars.cz/Domovska-stranka/O-nas/Mazars-v-Ceske-Republice|website=Mazars Česká republika|accessdate=April 2015}}</ref> Mazars is, by domestic sales, the 7th biggest audit firm on the Czech market.<ref name=Ekonom>{{cite news|last1=Horáček|first1=Jakub|title=Ekonom|accessdate=April 2015|issue=14|publisher=Economia|date=April 2015}}</ref> Mazars offers wide range of services including: audit, accountancy, outsourcing, payroll, internal audit, actuarial, transaction advisory and HR agenda. In 2015, the managing partner of Mazars Czech Republic was Milan Prokopius.

=== Vietnam ===
Mazars has been established in Vietnam since 1994 and employs around 130 professionals and general staff.<ref>{{cite web|url=http://www.mazars.vn/|title=Home|publisher=|accessdate=12 July 2016}}</ref> Mazars Vietnam operates through its offices in Hanoi and Ho Chi Minh City. The managing partner of Mazars in Vietnam is Marc Deschamps,<ref>{{cite web|url=http://www.mazars.vn/Users/Our-team/Jean-Marc-Deschamps|title=Jean-Marc Deschamps|publisher=|accessdate=12 July 2016}}</ref> based in Ho Chi Minh City.

=== Australia ===
Mazars integrated Australian audit and advisory firm Duncan Dovico on 1 January 2016.<ref>{{Cite web|url=http://www.mazars.com.au/Home/News/Latest-news/Mazars-and-Duncan-Dovico-join-forces-in-Australia|title=Mazars Australia|last=|first=|date=1 January 2016|website=Mazars Australia|publisher=|access-date=}}</ref> Mazars announced the acquisition of Australian based financial modelling and training provider Corality Financial Group on 13 July 2016 <ref>{{Cite web|url=http://www.corality.com/news/corality-acquired-by-mazars|title=Corality acquisition|last=|first=|date=13 July 2016|website=Corality Financial Group|publisher=|access-date=}}</ref> to form the new team Global Infrastructure Finance.

== References ==
<references/>

== External links ==
* [http://www.mazars.com Official website of the Mazars Group, in English and French]
* [http://www.mazars.vn Website of Mazars in Vietnam, in English]

{{Consulting}}

[[Category:Companies based in Paris]]
[[Category:Companies established in 1940]]
[[Category:International management consulting firms]]
[[Category:Management consulting firms of France]]
[[Category:Management consulting firms of the United Kingdom]]
[[Category:Accounting firms]]
[[Category:Accounting in France]]
[[Category:French brands]]