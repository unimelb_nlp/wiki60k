{{featured article}}
{{speciesbox
| name = Broad-billed parrot
| status = EX
| status_system = IUCN3.1
| status_ref = <ref>{{IUCN|id=22728847 |title=''Lophopsittacus mauritianus'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| extinct = c. 1680
| image =Gelderland1601-1603_Lophopsittacus_mauritianus.jpg
| image_width = 250px
| image_alt = Sketch of two broad-billed parrots
| image_caption = Sketch in the ''Gelderland'' ship's journal, 1601
| genus = Lophopsittacus
| parent_authority = [[Alfred Newton|Newton]], 1875
| species = mauritianus
| authority = ([[Richard Owen|Owen]], 1866)
|range_map=Mauritius island location.svg
| range_map_width = 250px
| range_map_alt = Map showing former range of the broad-billed parrot
| range_map_caption = Location of [[Mauritius]] in blue
| synonyms =
* ''Psittacus mauritianus'' <small>Owen, 1866</small>}}

The '''broad-billed parrot''' or '''raven parrot''' (''Lophopsittacus mauritianus'') is a large [[extinct]] [[parrot]] in the [[family (biology)|family]] [[Psittaculidae]]. It was [[endemism in birds|endemic]] to the [[Mascarene]] island of [[Mauritius]] in the [[Indian Ocean]] east of [[Madagascar]]. It is unclear what other [[species]] it is most closely related to, but it has been classified as a member of the [[Tribe (biology)|tribe]] [[Psittaculini]], along with other Mascarene parrots. It had similarities with the [[Rodrigues parrot]] (''Necropsittacus rodricanus''), and may have been closely related.

The broad-billed parrot's head was large in proportion to its body, and there was a distinct crest of feathers on the front of the head. The bird had a very large beak, comparable in size to that of the [[hyacinth macaw]], which would have enabled it to crack hard seeds. [[Subfossil]] bones indicate that the species exhibited greater [[sexual dimorphism]] in overall size and head size than any living parrot. The exact colouration is unknown, but a contemporary description indicates that it had multiple colours, including a blue head, and perhaps a red body and beak. It is believed to have been a weak flier, but not flightless.

The broad-billed parrot was first referred to as the "Indian raven" in Dutch ships' journals from 1598 onwards. Only a few brief contemporary descriptions and three depictions are known. It was first scientifically described from a subfossil mandible in 1866, but this was not linked to the old accounts until the rediscovery of a detailed 1601 sketch that matched old descriptions. The bird became extinct in the 17th century owing to a combination of [[deforestation]], predation by introduced [[invasive species]], and probably hunting as well.

==Taxonomy==
[[Image:View of the Mauritius roadstead - engraving.jpg|alt=Dutch activities on Mauritius. A broad-billed parrot is perched on a tree|left|thumb|Woodcut from 1601, with the first published depiction of a broad-billed parrot. The legend reads: "5* Is a bird which we called the Indian Crow, more than twice as big as the parroquets, of two or three colours".<ref name="Cheke and Hume (2008). p. 172.">Cheke and Hume (2008). p. 172.</ref>]]
The earliest known descriptions of the broad-billed parrot were provided by Dutch travellers during the [[Second Dutch Expedition to Indonesia]], led by Admiral [[Jacob Cornelis van Neck]] in 1598. They appear in reports published in 1601, which also contain the first illustration of the bird, along with the first of a [[dodo]]. The Dutch sailors who visited [[Mauritius]] categorised the broad-billed parrots separately from parrots, and referred to them as "Indische ravens" (translated as either "Indian [[raven]]s" or "Indian [[crow]]s") without accompanying useful descriptions, which caused confusion when their journals were studied.<ref name="Lost Land 23–25"/>

The English naturalist [[Hugh Edwin Strickland]] assigned the "Indian ravens" to the [[hornbill]] genus ''[[Buceros]]'' in 1848, because he interpreted the projection on the forehead in the 1601 illustration as a horn.<ref name="Lost Land 23–25">Check & Hume. (2008). pp. 23–25.</ref> The Dutch and the French also referred to South American [[macaw]]s as "Indian ravens" during the 17th century, and the name was used for hornbills by Dutch, French, and English speakers in the [[East Indies]].<ref name=4to17Hume2007>Hume, J. P. (2007). pp. 4–17.</ref> [[Sir Thomas Herbert]] referred to the broad-billed parrot as "Cacatoes" ([[cockatoo]]) in 1634, with the description "birds like Parrats{{sic}}, fierce and indomitable", but naturalists did not realise that he was referring to the same bird.<ref name="Lost Land 23–25"/> Even after subfossils of a parrot matching the descriptions were found, French [[zoologist]] [[Emile Oustalet]] argued that the "Indian raven" was a hornbill whose remains awaited discovery. The Mauritian ornithologist [[France Staub]] was in favour of this idea as late as 1993. No remains of hornbills have ever been found on the island, and apart from an extinct species from [[New Caledonia]], hornbills are not found on any [[oceanic island]]s.<ref name=4to17Hume2007/>

The first known physical remain of the broad-billed parrot was a [[subfossil]] mandible collected along with the first batch of dodo bones found in the [[Mare aux Songes]] swamp.<ref name="Extinct Birds">{{cite book
| last1 = Hume
| first1 = J. P.
| first2 = M.
| last2 = Walters
|year= 2012
|title= Extinct Birds
|location= London
|pages= 180–181
|publisher= A & C Black
|isbn=1-4081-5725-X}}</ref> The English biologist [[Richard Owen]] described the mandible in 1866 and identified it as belonging to a large parrot species, to which he gave the binomial name ''[[Psittacus]] mauritianus'' and the [[common name]] "broad-billed parrot".<ref name="Lost Land 23–25"/><ref>{{Cite journal | doi = 10.1111/j.1474-919X.1866.tb06084.x | last = Owen | first = R.| author-link = Richard Owen| title = Evidence of a species, perhaps extinct, of large parrot (''Psittacus mauritianus'', Owen), contemporary with the Dodo, in the island of Mauritius | journal = Ibis | volume = 8 | issue = 2 | pages = 168–171 | year = 1866| pmid =  | url = https://archive.org/stream/ibis02brit#page/168/mode/2up}}</ref> This [[holotype]] specimen is now lost.<ref name=4to17Hume2007/> In 1868, shortly after the 1601 journal of the [[Dutch East India Company]] ship ''Gelderland'' had been rediscovered, the German ornithologist [[Hermann Schlegel]] examined an unlabelled pen-and-ink sketch in it. Realising that the drawing, which is attributed to the artist Joris Joostensz Laerle, depicted the parrot described by Owen, Schlegel made the connection with the old journal descriptions. In 1875, because its bones and crest are significantly different from those of ''Psittacus'' species, the English zoologist [[Alfred Newton]] assigned it to its own genus, which he called ''Lophopsittacus''.<ref>{{Cite journal | last1 = Newton | first1 = E. | title = XXVII.-On the psittaci of the Mascarene Islands | doi = 10.1111/j.1474-919X.1876.tb06925.x | journal = Ibis | volume = 18 | issue = 3 | pages = 281–289 | year = 1876| url = http://biodiversitylibrary.org/item/35122#page/313/mode/1up | pmc = }}</ref> ''Lophos'' is the [[Ancient Greek]] word for crest, referring here to the bird's frontal crest, and ''psittakos'' means parrot.<ref name=4to17Hume2007/><ref name=helm>{{cite dictionary|url=http://www.scribd.com/doc/88883761/Helm-Dictionary-of-Scientific-Bird-Names |title= The Helm Dictionary of Scientific Bird Names |first=J. A |last=Jobling |year=2012 |publisher= Christopher Helm |location=London |isbn=978-1-4081-2501-4 |page=230}}</ref>

In 1973, based on remains collected by Louis Etienne Thirioux in the early 20th century, the English ornithologist Daniel T. Holyoak placed a small subfossil Mauritian parrot in the same genus as the broad-billed parrot and named it ''[[Lophopsittacus bensoni]]''.<ref>{{Cite journal | last1 = Holyoak | first1 = D. T. | title = An undescribed extinct parrot from Mauritius | doi = 10.1111/j.1474-919X.1973.tb01980.x | journal = Ibis | volume = 115 | issue = 3 | pages = 417–419 | year = 1973| pmid =  | pmc = }}</ref> In 2007, on the basis of a comparison of subfossils, correlated with 17th and 18th century descriptions of small grey parrots, Hume reclassified it as a species in the genus ''[[Psittacula]]'' and called it Thirioux's grey parrot.<ref name=4to17Hume2007/> In 1967, [[James Greenway]] had speculated that reports of grey Mauritian parrots referred to the broad-billed parrot.<ref name =Greenway>{{cite book
 | last = Greenway
 | first = J. C.
 | title = Extinct and Vanishing Birds of the World
 | publisher = American Committee for International Wild Life Protection 13
 | series = 
 | volume =
 | edition = 
 | location = New York
 | year = 1967
 | page = 126
 | doi = 
 | isbn = 0-486-21869-4
 | mr = 
 | zbl =  }}</ref>

===Evolution===
[[File:Psittacus mauritianus.jpg|alt=Subfossil broad-billed parrot mandible|thumb|[[Lithograph]] of the [[subfossil]] [[holotype]] mandible, 1866]]
The taxonomic affinities of the broad-billed parrot are undetermined. Considering its large jaws and other [[osteological]] features, the ornithologists [[Edward Newton]] and [[Hans Gadow]] thought it to be closely related to the [[Rodrigues parrot]] (''Necropsittacus rodricanus'') in 1893, but were unable to determine whether they both belonged in the same genus, since a crest was only known from the latter.<ref name="Newton & Gadow">{{cite journal| doi = 10.1111/j.1469-7998.1893.tb00001.x| last1 = Newton | first1 = E.| authorlink1 = Edward Newton| last2 = Gadow | first2 = H.| year = 1893| title = IX. On additional bones of the Dodo and other extinct birds of Mauritius obtained by Mr. Theodore Sauzier| journal = The Transactions of the Zoological Society of London| volume = 13| issue = 7| pages = 281–302| pmid = | url = http://www.biodiversitylibrary.org/page/31083700#page/379/mode/1up| ref = harv
}}</ref> The British ornithologist Graham S. Cowles instead found their skulls too dissimilar for them to be close relatives in 1987.<ref name="Cowles87">{{Cite book | doi = 10.1017/CBO9780511735769.004| editor1-last = Diamond| editor1-first = A. W.| title = Studies of Mascarene Island Birds| chapter = The fossil record| pages = 90–100| year = 1987| location = Cambridge | last1 = Cowles | first1 = G. S.| isbn = 978-0-511-73576-9}}</ref>

Many endemic Mascarene birds, including the dodo, are derived from South Asian ancestors, and the English [[palaeontologist]] [[Julian Hume]] has proposed that this may be the case for all the parrots there as well. Sea levels were lower during the [[Pleistocene]], so it was possible for species to colonise some of the then less isolated islands.<ref name="Cheke and Hume 2008. p. 71">Cheke and Hume (2008). p. 71.</ref> Although most extinct parrot species of the Mascarenes are poorly known, subfossil remains show that they shared features such as enlarged heads and jaws, reduced [[Pectoralis major muscle|pectoral]] bones, and robust leg bones. Hume has suggested that they have a common origin in the [[Evolutionary radiation|radiation]] of the [[Tribe (biology)|tribe]] Psittaculini, basing this theory on [[morphology (biology)|morphological]] features and the fact that ''[[Psittacula]]'' parrots have managed to colonise many isolated islands in the Indian Ocean.<ref name=4to17Hume2007/> The Psittaculini may have invaded the area several times, as many of the species were so specialised that they may have evolved significantly on [[hotspot island]]s before the Mascarenes emerged from the sea.<ref name="Cheke and Hume 2008. p. 71"/> A 2011 genetic study instead found that the [[Mascarene parrot]] (''Mascarinus mascarinus'') of [[Réunion]] was most closely related to the [[lesser vasa parrot]] (''Coracopsis nigra'') from Madagascar and nearby islands, and therefore unrelated to the ''Psittacula'' parrots, disputing the theory of their common origin.<ref>{{Cite journal | last1 = Kundu | first1 = S. | last2 = Jones | first2 = C. G. | last3 = Prys-Jones | first3 = R. P. | last4 = Groombridge | first4 = J. J. | title = The evolution of the Indian Ocean parrots (Psittaciformes): Extinction, adaptive radiation and eustacy | doi = 10.1016/j.ympev.2011.09.025 | journal = Molecular Phylogenetics and Evolution | volume = 62 | issue = 1 | pages = 296–305 | year = 2011| pmid =  22019932| pmc = }}</ref>

==Description==
[[File:Broad-billed Parrot.jpg|thumb|alt=Drawing of two broad-billed parrots|Artistic adaptation based on a tracing of the ''Gelderland'' sketch, 1896]]
The broad-billed parrot possessed a distinct frontal [[crest (feathers)|crest]] of feathers. Ridges on the skull indicate that this crest was firmly attached, and that the bird, unlike [[cockatoo]]s, could not raise or lower it.<ref name=4to17Hume2007/> The 1601 ''Gelderland'' sketch was examined in 2003 by Hume, who compared the ink finish with the underlying pencil sketch and found that the latter showed several additional details. The pencil sketch depicts the crest as a tuft of rounded feathers attached to the front of the head at the base of the beak, and shows long primary [[covert feathers]], large [[secondary feathers]], and a slightly bifurcated tail.<ref name="Gelderland"/> Measurements of sub-fossils known by 1893 show that the mandible was {{convert|65–78|mm}} in length, {{convert|65|mm|abbr=on}} in width, the femur was {{convert|58–63|mm|abbr=on}} in length, the tibia was {{convert|88–99|mm|abbr=on}}, and the metatarsus {{convert|35|mm|abbr=on}}.<ref name="Newton & Gadow"/> Unlike other Mascarene parrots, the broad-billed parrot had a flattened skull.<ref name=4to17Hume2007/>

Subfossils show that the males were larger, measuring {{convert|55–65|cm}} to the females' {{convert|45–55|cm|abbr=on}} and that both sexes had disproportionately large heads and beaks. The [[sexual dimorphism]] in size between male and female skulls is the largest among parrots.<ref name=4to17Hume2007/> Differences in the bones of the rest of the body and limbs are less pronounced; nevertheless, it had greater sexual dimorphism in overall size than any living parrot. The size differences between the two birds in the 1601 sketch may be due to this feature.<ref name=51Hume2007>Hume, J. P. (2007). p. 51.</ref> A 1602 account by Reyer Cornelisz has traditionally been interpreted as the only contemporary mention of size differences among broad-billed parrots, listing "large and small Indian crows" among the animals of the island. A full transcript of the original text was only published in 2003, and showed that a comma had been incorrectly placed in the English translation; "large and small" instead referred to "field-hens", possibly the [[red rail]] and the smaller [[Sauzier's wood rail]].<ref>{{Cite journal  | last =  Cheke | first = Anthony S.  | title = A single comma in a manuscript alters Mauritius avian history  | journal =  Phelsuma | volume = 21  | pages = 1–3  | year = 2013 |url=http://islandbiodiversity.com/Phelsuma%2021a.pdf}}</ref>
[[Image:Lophopsittacus.jpg|left|thumb|alt=Painting of a blue broad-billed parrot|1907 restoration by [[Henrik Grönvold]] (based on the ''Gelderland'' sketch), showing the bird as entirely blue, which may be inaccurate]]
There has been some confusion over the colouration of the broad-billed parrot.<ref name="Fuller Extinct">{{cite book
 | last = Fuller
 | first = E.
 | authorlink = Errol Fuller
 | title = Extinct Birds
 | publisher = Comstock
 | series = 
 | volume = 
 | edition = revised
 | location = New York
 | year = 2001
 | pages = 230–231
 | doi = 
 | isbn = 0-8014-3954-X
 | mr = 
 | zbl = }}</ref> The report of van Neck's 1598 voyage, published in 1601, contained the first illustration of the parrot, with a caption stating that the bird had "two or three colours".<ref name="Cheke and Hume (2008). p. 172."/> The last account of the bird, and the only mention of specific colours, was by Johann Christian Hoffman in 1673–75:
{{Quotation|There are also geese, flamingos, three species of pigeon of varied colours, mottled and green perroquets, red crows with recurved beaks and with blue heads, which fly with difficulty and have received from the Dutch the name of 'Indian crow'.<ref name="Cheke and Hume (2008). p. 172."/>}}
In spite of the mention of several colours, authors such as [[Walter Rothschild]] claimed that the ''Gelderland'' journal described the bird as entirely blue-grey, and it was restored this way in Rothschild's 1907 book ''[[Extinct Birds (Rothschild book)|Extinct Birds]]''.<ref>{{Cite book
  | last = Rothschild
  | first = W.
  | authorlink = Walter Rothschild, 2nd Baron Rothschild
  | title = Extinct Birds
  | publisher = Hutchinson & Co
  | year = 1907
  | location = London
  | page = 49
  | url = https://archive.org/stream/extinctbirdsatte00roth#page/48/mode/2up
}}</ref> Later examination of the journal by Julian Hume has revealed only a description of the dodo. He suggested that the distinctively drawn facial mask may represent a separate colour.<ref name="Gelderland">{{cite journal| doi = 10.3366/anh.2003.30.1.13| last = Hume | first = J. P.| authorlink = Julian Pender Hume| year = 2003| title = The journal of the flagship ''Gelderland''&nbsp;– dodo and other birds on Mauritius 1601| journal = Archives of Natural History| volume = 30| issue = 1| pages = 13–27| pmid = | pmc = | ref = harv
}}</ref> The head was evidently blue, and in 2007, Hume suggested the beak may have been red, and the rest of the plumage greyish or blackish, which also occurs in other members of Psittaculini.<ref name=4to17Hume2007/>

In 2015, a translation of the 1660s report of Johannes Pretorius about his stay on Mauritius was published, wherein he described the bird as "very beautifully coloured". Hume accordingly reinterpreted Hoffman's account, and suggested the bird may have been brightly coloured with a red body, blue head, and red beak; the bird was illustrated as such in the paper by Ria Winters. Possible [[iridescent]] or glossy feathers that changed appearance according to angle of light may also have given the impression that it had even more colours.<ref name="Pretorius">{{Cite journal | doi = 10.1080/08912963.2015.1036750| title = Captive birds on Dutch Mauritius: Bad-tempered parrots, warty pigeons and notes on other native animals| journal = Historical Biology| volume = 28| issue = 6| pages = 1| year = 2015| last1 = Hume | first1 = J. P. | last2 = Winters | first2 = R. }}</ref> It has also been suggested that in addition to size dimorphism, the sexes may have had different colours, which would explain some of the discrepancies in old descriptions.<ref>{{Cite book| last1 = Cheke | first1 = A. S. | editor1-last = Diamond| editor1-first = A. W.| doi = 10.1017/CBO9780511735769.003 | chapter = An ecological history of the Mascarene Islands, with particular reference to extinctions and introductions of land vertebrates | title = Studies of Mascarene Island Birds | pages = 5–89 | year = 1987 | isbn = 978-0-521-11331-1| location = Cambridge | publisher = Cambridge University Press }}</ref>

==Behaviour and ecology==
[[Image:Lophopsittacus.mauritianus.jpg|alt=Sketch of a broad-billed parrot and two other birds on Mauritius|thumb|right|Sketch by [[Sir Thomas Herbert]] from 1634 showing a broad-billed parrot, a [[red rail]], and a [[dodo]]]]
Johannes Pretorius (on Mautirius from 1666 to 1669) kept various now-extinct Mauritian birds in captivity, and described the behaviour of the broad-billed parrot as follows:
{{Quotation|The Indian ravens are very beautifully coloured. They cannot fly and are not often found. This kind is a very bad tempered bird. When captive it refuses to eat. It would prefer to die rather than to live in captivity.<ref name="Pretorius"/>}}

Though the broad-billed parrot may have fed on the ground and been a weak flier, its [[tarsometatarsus]] was short and stout, implying some [[arboreal]] characteristics. The Newton brothers and many authors after them inferred that it was [[flightless]], due to the apparent short wings and large size shown in the 1601 ''Gelderland'' sketch. According to Hume, the underlying pencil sketch actually shows that the wings are not particularly short. They appear broad, as they commonly are in forest-adapted species, and the [[alula]] appears large, a feature of slow-flying birds. Its sternal [[Keel (bird)|keel]] was reduced, but not enough to prevent flight, as the adept flying ''[[Cyanoramphus]]'' parrots also have reduced keels, and even the flightless [[kakapo]], with its [[vestigial]] keel, is capable of gliding.<ref name=4to17Hume2007/> Furthermore, Hoffman's account states that it could fly, albeit with difficulty, and the first published illustration shows the bird on top of a tree, an improbable position for a flightless bird.<ref name="Gelderland"/> The broad-billed parrot may have been behaviourally near-flightless, like the now-extinct [[Norfolk Island kaka]].<ref name="Pretorius"/>
[[File:Lophopsittacus fossils.jpg|alt=Subfossil broad-billed parrot bones|left|thumb|Subfossil remains, including leg bones, a mandible, and a [[sternum]]]]
Sexual dimorphism in beak size may have affected behaviour. Such dimorphism is common in other parrots, for example in the [[palm cockatoo]] and the [[New Zealand kaka]]. In species where it occurs, the sexes prefer food of different sizes, the males use their beaks in rituals, or the sexes have specialised roles in nesting and rearing. Similarly, the large difference between male and female head size may have been reflected in the ecology of each sex, though it is impossible to determine how.<ref name=4to17Hume2007/><ref>{{Cite book |first=J. M. |last=Forshaw | authorlink=Joseph Forshaw | title=Parrots of the World; an Identification Guide|others =Illustrated by [[Frank Knight (artist)|Frank Knight]]|publisher=[[Princeton University Press]] |isbn=0-691-09251-6 |page= plate 23 |nopp = yes| year=2006 }}</ref>

In 1953, the Japanese ornithologist [[Masauji Hachisuka]] suggested the broad-billed parrot was [[nocturnal]], like the kakapo and the [[night parrot]], two extant ground-dwelling parrots. Contemporary accounts do not corroborate this, and the [[orbits]] are of similar size to those of other large [[diurnality|diurnal]] parrots.<ref name=4to17Hume2007/> The broad-billed parrot was recorded on the dry [[leeward]] side of Mauritius, which was the most accessible for people, and it was noted that birds were more abundant near the coast, which may indicate that the fauna of such areas was more diverse. It may have nested in tree cavities or rocks, like the [[Cuban amazon]]. The terms ''raven'' or ''crow'' may have been suggested by the bird's harsh call, its behavioural traits, or just its dark plumage.<ref name=4to17Hume2007/> The following description by Jacob Granaet from 1666 mentions some of the broad-billed parrot's co-inhabitants of the forests, and might indicate its demeanour:
[[File:Vadaspark Madarak 01.jpg|thumb|Statues in Hungary of the likewise extinct [[Newton's parakeet]] of [[Rodrigues]] and the broad-billed parrot]]
{{Quotation|Within the forest dwell parrots, turtle and other wild doves, mischievous and unusually large ravens [broad-billed parrots], falcons, bats and other birds whose name I do not know, never having seen before.<ref name="Cheke and Hume (2008). p. 172."/>}}
Many other endemic species of Mauritius were lost after the arrival of man, so the [[ecosystem]] of the island is severely damaged and hard to reconstruct. Before humans arrived, Mauritius was entirely covered in forests, almost all of which have since been lost to [[deforestation]].<ref>{{cite journal| doi = 10.1017/S0030605300020457| last = Cheke | first = A. S.| year = 1987| title = The legacy of the dodo—conservation in Mauritius| journal = Oryx| volume = 21| issue = 1| pages = 29–36| pmid = | pmc = | ref = harv
}}</ref> The surviving endemic [[fauna]] is still seriously threatened.<ref>{{cite journal| doi = 10.1017/S0030605300012643| last = Temple | first = S. A.| year = 1974| title = Wildlife in Mauritius today| journal = Oryx| volume = 12| issue = 5| pages = 584–590| pmid = | pmc = | ref = harv
}}</ref> The broad-billed parrot lived alongside other recently extinct Mauritian birds such as the dodo, the red rail, the [[Mascarene grey parakeet]], the [[Mauritius blue pigeon]], the [[Mauritius owl]], the [[Mascarene coot]], the [[Mauritian shelduck]], the [[Mauritian duck]], and the [[Mauritius night heron]]. Extinct Mauritian reptiles include the [[saddle-backed Mauritius giant tortoise]], the [[domed Mauritius giant tortoise]], the [[Mauritian giant skink]], and the [[Round Island burrowing boa]]. The [[small Mauritian flying fox]] and the snail ''[[Tropidophora carinata]]'' lived on Mauritius and Réunion but became extinct in both islands. Some plants, such as ''[[Casearia tinifolia]]'' and the [[palm orchid]], have also become extinct.<ref name="Cheke and Hume 2008 371 373">Cheke and Hume (2008). pp. 371–373.</ref>

===Diet===
[[File:Latania loddigesii seeds.jpg|thumb|left|alt=Brown seeds|Seeds of ''[[Latania loddigesii]]'', which may have been part of the diet of this parrot]]
Species that are morphologically similar to the broad-billed parrot, such as the [[hyacinth macaw]] and the palm cockatoo, may provide insight into its ecology. ''[[Anodorhynchus]]'' macaws, which are habitual ground dwellers, eat very hard palm nuts.<ref name=4to17Hume2007/> Carlos Yamashita has suggested that these macaws once depended on now-extinct South American [[megafauna]] to eat fruits and excrete the seeds, and that they later relied on domesticated cattle to do this. Similarly, in [[Australasia]] the palm cockatoo feeds on undigested seeds from [[cassowary]] droppings.<ref name=4to17Hume2007/> Yamashita suggested that the abundant ''[[Cylindraspis]]'' tortoises and dodos performed the same function on Mauritius, and that the broad-billed parrot, with its macaw-like beak, depended on them to obtain cleaned seeds.<ref name="Cheke and Hume (2008). p. 38."/> Many types of palms and palm-like plants on Mauritius produce hard seeds that the broad-billed parrot may have eaten, including ''[[Latania loddigesii]]'', ''[[Mimusops maxima]]'', ''[[Sideroxylon grandiflorum]]'', ''[[Diospyros egrettorium]]'', and ''[[Pandanus utilis]]''.<ref name=4to17Hume2007/>
[[File:Lophopsittacus mauritianus mandible fragments.jpg|thumb|Mandible fragments in [[Naturalis]]]] 
On the basis of [[radiograph]]s, D. T. Holyoak claimed that the [[mandible]] of the broad-billed parrot was weakly constructed and suggested that it would have fed on soft fruits rather than hard seeds.<ref>{{Cite journal
| last = Holyoak
| first = D. T.
| title = Comments on the extinct parrot ''Lophopsittacus mauritianus''
| journal = Ardea
| volume = 59
| pages = 50–51
| year = 1971 }}</ref> As evidence, he pointed out that the internal [[trabeculae]] were widely spaced, that the upper bill was broad whereas the [[Palatine bone|palatines]] were narrow, and the fact that no preserved upper [[Rostrum (anatomy)|rostrum]] had been discovered, which he attributed to its delicateness.<ref>{{Cite journal | last1 = Holyoak | first1 = D. T. | title = Comments on taxonomy and relationships in the parrot subfamilies Nestorinae, Loriinae and Platycercinae | doi = 10.1071/MU973157 | journal = Emu | volume = 73 | issue = 4 | pages = 157 | year = 1973 | pmid =  | pmc = }}</ref> G. A. Smith, however, pointed out that the four genera Holyoak used as examples of "strong jawed" parrots based on radiographs, ''Cyanorhamphus'', ''[[Melopsittacus]]'', ''[[Neophema]]'' and ''[[Psephotus]]'', actually have weak jaws in life, and that the morphologies cited by Holyoak do not indicate strength.<ref>{{Cite journal | last1 = Smith | first1 = G. A. | doi = 10.1111/j.1474-919X.1975.tb04187.x | title = Systematics of parrots | journal = Ibis | volume = 117 | pages = 17–18 | year = 1975 | pmid =  | pmc = }}</ref> Hume has since pointed out that the mandible morphology of the broad-billed parrot is comparable to that of the largest living parrot, the hyacinth macaw, which cracks open palm nuts with ease. It is therefore probable that the broad-billed parrot fed in the same manner.<ref>{{cite journal
|author=Hume, J. P.
|author2=R. P. Prys-Jones, R. P.
|year=2005
|title=New discoveries from old sources, with reference to the original bird and mammal fauna of the Mascarene Islands, Indian Ocean
|journal=[[Zoologische Mededelingen]]
|volume=79
|issue=3
|pages=85–95
|url=http://www.repository.naturalis.nl/document/42166
|format=PDF}}</ref>

==Extinction==
Though Mauritius had previously been visited by [[Arab]] vessels in the [[Middle Ages]] and Portuguese ships between 1507 and 1513, they did not settle on the island.<ref name="Fuller Dodo">{{cite book
  | last = Fuller
  | first = E.
  | authorlink = Errol Fuller
  | year = 2002
  | title = Dodo&nbsp;– From Extinction To Icon
  | publisher = [[HarperCollins]]
  | location = London
  |pages = 16–26
  | isbn = 978-0-00-714572-0
  | ref = harv
  }}</ref> The Dutch Empire acquired the island in 1598, renaming it after [[Maurice of Nassau]], and it was used from then on for the provisioning of trade vessels of the Dutch East India Company.<ref>{{Cite journal | last1 = Schaper | first1 = M. T. | last2 = Goupille | first2 = M. | doi = 10.5172/ser.11.2.93 | title = Fostering enterprise development in the Indian Ocean: The case of Mauritius | journal = Small Enterprise Research | volume = 11 | issue = 2 | pages = 93 | year = 2003 | pmid =  | pmc = | ref=harv}}</ref> To the Dutch sailors who visited Mauritius from 1598 and onwards, the fauna was mainly interesting from a culinary standpoint.<ref name="Fuller Extinct"/> Of the eight or so parrot species endemic to the Mascarenes, only the [[echo parakeet]] (''Psittacula echo'') of Mauritius has survived. The others were likely all made extinct by a combination of excessive hunting and deforestation.<ref name=4to17Hume2007/>

Because of its poor flying ability, large size and possible [[island tameness]], the broad-billed parrot was easy prey for sailors who visited Mauritius, and their nests would have been extremely vulnerable to [[predation]] by introduced [[crab-eating macaque]]s and rats. Various sources indicate the bird was aggressive, which may explain why it held out so long against introduced animals after all. The bird is believed to have become extinct by the 1680s, when the palms it may have sustained itself on were harvested on a large scale. Unlike other parrot species, which were often taken as [[Companion parrot|pets]] by sailors, there are no records of broad-billed parrots being transported from Mauritius either live or dead, perhaps because of the [[Social stigma|stigma]] associated with ravens.<ref name=4to17Hume2007/><ref name="Pretorius"/> The birds would not in any case have survived such a journey if they refused to eat anything but seeds.<ref name="Cheke and Hume (2008). p. 38.">Cheke and Hume (2008). p. 38.</ref>

==References==
{{Reflist|30em}}

==Works cited==
*{{cite book
| last1 = Cheke | first1 = A. S.
| last2 = Hume | first2 = J. P.
|year=2008
|url=https://books.google.com/books?id=OGeENV4exXcC&printsec=frontcover#v=onepage&q&f=false
|title=Lost Land of the Dodo: an Ecological History of Mauritius, Réunion & Rodrigues
|location= New Haven and London
|publisher=T. & A. D. Poyser
|isbn=978-0-7136-6544-4}}
*{{Cite journal|last=Hume|first=J. P. |year=2007 |url=http://julianhume.co.uk/wp-content/uploads/2010/07/Hume-Mascarene-Parrots.pdf |title=Reappraisal of the parrots (Aves: Psittacidae) from the Mascarene Islands, with comments on their ecology, morphology, and affinities |work=[[Zootaxa]] |volume=1513}}

==External links==
*{{Commons category-inline|Lophopsittacus mauritianus}}
*{{Wikispecies-inline|Psittacoidea}}

{{Psittaculini}}
{{Birds}}
{{portalbar|Birds|Animals|Biology|Mauritius|Extinction|Paleontology}}
{{taxonbar}}

{{DEFAULTSORT:Broad-Billed Parrot}}
[[Category:Psittacinae]]
[[Category:Extinct birds of Indian Ocean islands]]
[[Category:Birds of Mauritius]]
[[Category:Parrots of Africa]]
[[Category:Extinct animals of Africa]]
[[Category:Extinct flightless birds]]
[[Category:Bird extinctions since 1500]]
[[Category:Birds described in 1866]]