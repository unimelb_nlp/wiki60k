{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = The Steam House
| title_orig    = La Maison à vapeur
| translator    =
| image         = 'The Steam House' by Léon Benett 018.jpg
| caption =
| author        = [[Jules Verne]]
| illustrator   = [[Léon Benett]]
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #20
| genre         = [[Adventure novel]] 
| publisher     = [[Pierre-Jules Hetzel]]
| pub_date      = 1880
| english_pub_date = 1880
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| oclc          = 2653988
| preceded_by   = [[Tribulations of a Chinaman in China]]
| followed_by   = [[Eight Hundred Leagues on the Amazon]]
}}

'''''The Steam House''''' ({{lang-fr|La maison à vapeur}}) is an 1880 [[Jules Verne]] novel recounting the travels of a group of British colonists in the [[British Raj|Raj]] in a wheeled house pulled by a [[steam engine|steam-powered]] mechanical elephant. Verne uses the mechanical house as a plot device to have the reader travel in nineteenth century India. The descriptions are interspersed with historical information and social commentary.

The book takes place in the aftermath of the [[Indian Rebellion of 1857]] against British rule, with the passions and traumas aroused still very much alive among Indians and British alike. An alternate title by which the book was known - "The End of Nana Sahib" - refers to the appearance in the book of the historical figure&mdash;Rebel leader [[Nana Sahib]]&mdash;who disappeared after the crushing of the rebellion, his ultimate fate unknown.  Verne offers a fictional explanation to his disappearance.

==Alternative titles==
* ''Demon of Cawnpore'' (Part 1 of 2)
* ''Demon of the Cawnpore'' (Part 1 of 2)
* ''Steam House'' (Part I) The Demon of Cawnpore
* ''Steam House'' (Part II) Tigers and Traitors
* ''Tigers and Traitors'' (Part 2 of 2)
* ''Tigers and Traitors, Steam House'' (Part 2 of 2)
* ''The End of [[Nana Sahib]]''

==See also==
{{portal|Novels}}

* [[History of steam road vehicles]]

==External links==
{{Commons category|The Steam House}}
* [https://archive.org/details/TheEndOfNanaSahibTheSteamHouse The End Of Nana Sahib The Steam House]
* [http://www.julesverne.ca/vernebooks/jvbksteam.html Maison a vapeur - 1880]

{{Verne}}

{{Authority control}}

[[History of The League of Extraordinary Gentlemen]]

{{DEFAULTSORT:Steam House, The}}
[[Category:1880 novels]]
[[Category:Novels by Jules Verne]]
[[Category:Novels about the Indian Rebellion of 1857]]
[[Category:Steam power]]
[[Category:Fictional robots]]
[[Category:Fictional elephants]]


{{1880s-novel-stub}}