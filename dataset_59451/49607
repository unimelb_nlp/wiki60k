{{infobox book <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Deluge
| title_orig    = Potop
| translator    = Jeremiah Curtin
| image         = Henryk Sienkiewicz - Potop - The Deluge (1898 translation by Jeremiah Curtin) - Vol 1.djvu 
| caption       = Cover of the first English language edition (Boston 1898)

| author        = [[Henryk Sienkiewicz]]
| illustrator   = 
| cover_artist  = 
| country       = Poland
| language      = [[Polish language|Polish]]
| series        = [[The Trilogy]]
| genre         = [[Historical novel]]
| publisher     =
| release_date  = 1886
| english_release_date =
| media_type    =
| pages         =
| isbn          =
| preceded_by   = [[With Fire and Sword]]
| followed_by   = [[Fire in the Steppe]]
}}

'''''The Deluge''''' ({{lang-pl|Potop}}) is a [[historical novel]] by the Polish author [[Henryk Sienkiewicz]], published in 1886. It is the second volume of a three-volume series known to Poles as "[[The Trilogy]]," having been preceded by ''[[With Fire and Sword]]'' (''Ogniem i mieczem'', 1884) and followed by ''[[Fire in the Steppe]]'' (''Pan Wołodyjowski'', 1888). The novel tells a story of a fictional [[Polish-Lithuanian Commonwealth]] soldier and noble [[Andrzej Kmicic]] and shows a panorama of the Commonwealth during its historical period of [[Deluge (history)| the Deluge]], which was a part of [[Northern Wars|the Northern Wars]].

==The Plot (Vol. I)==

'''Chapters I – V'''
[[File:Kmicicowa kompania.jpg|thumb|260px|Kmita's companions on a ride]]
The novel begins with a description of the families living in and around the district of Rossyeni, the oldest and most powerful of which are the Billeviches. Aleksandra Billevich, daughter of the chief hunter of [[Upita]], has been orphaned and left in the care of the [[szlachta|noble families]]. She is destined to marry Andrei Kmita (Polish: [[Andrzej Kmicic]]), whose father was her father’s – Pan Heraclius’s – best friend. The pair meet and she is smitten by him on their first meeting, particularly as he is a war hero from [[Smolensk]]. However, she is wary of his impetuous character and also his companions, ruffians who are almost outlaws and who depend on him for their protection from the law.

At his mansion in Lyubich, various misdeeds take place and rumours soon fly around the neighbourhood. They are taken to meet Panna Aleksandra and go on a sleigh ride, interrupted by news that a quarrel has broken out between Kmita’s troops and the citizens of Upita over provisions. He deals harshly with the affair and also news reaches Olenka, via an old servant Kassyan, of the debauchery at Lyubich. On a Sunday she again meets Kmita’s companions and treats them harshly, arousing their ire and they decide to go to Upita to complain to their superior. On the way, they stop off to drink at the Dola public house they drink vodka and play with the Butryms’ women and are slaughtered by the men.

'''Chapters VI – X'''

Kmita returns to Vodokty with his troops and has to confess to how he mistreated the guilty at Upita e.g. ordering one hundred blows for the town’s mayor and councillors. The couple quarrel and he resolves to dismiss his companions who she says are a bad influence on him. At Lyubich he finds the bodies of his murdered colleagues and, in revenge, burns the village of Volmontovichi to the ground. Kmita has to seek refuge with Olenka and she forces him to flee.

The action switches to the troubles inside the Commonwealth, particularly between the Yanush Radzivill (grand [[hetman]] of Lithuania) and [[Paweł Jan Sapieha|Pavel Sapyeha]] factions. Pan Volodyovski, a general who is recovering from a wound, is living with Pakosh Gashtovt in Lauda, and the people want him to marry Olenka. Kmita returns to kidnap Olenka and Volodyovski with his force besieges him and his [[Cossacks]] at Lyubich. They fight a duel and the banneret of [[Orsha]] is wounded. Saving Olenka, Michal Volodyovski decides to propose to her but is rejected and he knows she loves Kmita, despite everything that has occurred.

War is afoot and Volodyovski is ordered by Radzivill to grant Kmita a commission to raise a force. He visits the wounded knight at Lyubich and knows he will render good service to the Commonwealth as well as blot out his past offences.

'''Chapters XI – XV'''
[[File:Kmicic z Oleńką.jpg|thumb|260px|Kmita and Olenka enjoy sleighing together]]
[[Great Poland]] is invaded by the Swedes and the nobles are led by Pan [[Krzysztof Opaliński|Kryshtof Opalinski]], the powerful [[voivode|voivoda]] of [[Poznań]]. However, they have grown soft in peacetime and defeatism is in the air. They decide to parley with Wittemberg, the Swedish commander, as reinforcements fail to come from the Polish king, [[John II Casimir]] and [[Charles X Gustav of Sweden|Karl Gustav]] is accepted as King.

In the district of Lukovo, Yan Skshetuski is living with his wife and her adopted father, Zagloba. Stanislaw Skshetuski, Yan’s cousin, announces the treachery and all three decide to make for Prince Radzivill’s palace at Kyedani via Upita to see Michal Volodyovski. They learn that Pan Gosyevski and Yudytski have been arrested. They are summoned to a private meeting with the Prince who then meets two Swedish envoys (Count Lowenhaupt and Baron Schitte). Before a feast that evening, Kmita is summoned by the Prince and made to swear on the holy cross that he will not leave him until death. Olenka and Kmita are reunited and make peace. The Prince announces his alliance with the Swedes and the Skshetuskis and Zagloba are thrown into prison for dissent. Radzivill explains his thinking to Kmita who decides to remain loyal.

'''Chapters XVI – XXII'''

The [[Hungarians]] and a part of the [[dragoon]]s of Myeleshko and Kharlamp, who attempt to resist, are massacred by Kmita’s men. Radzivill is determined to murder Zagloba but Kmita pleads for his life and so the Prince decides to send his prisoners to the Swedes at Birji. On the way, Zagloba tricks Roh Kovalski, the conducting officer, and escapes and the captive colonels are rescued by the Lauda men and they make for the voevoda of [[Vitebsk|Vityebsk]] and defeat Swedish troops at a village.

Kmita fortifies Kyedani and Volodyovski’s squadron is nearly caught by the Prince and the regiments of Myeleshko and Ganhoff but slips through. Kmita sees Olenka again as the Prince wants her and her guardian Pan Billevich, the Prince’s sword-bearer, as hostages at Kyedani. However, Volodyovski comes to their rescue and Kmita is sentenced to death. However, he is saved by Zagloba who finds a letter amongst his clothes from the Prince berating him for having saved the Colonels and Zagloba and also his commission and is freed.

'''Chapters XXIII – XXXIII'''

Kmita finds out that Radzivill ordered the Swedes to murder the Colonels. At a feast Olenka and Kmita are obliged to sit next to each other and cannot express their true feelings. A letter arrives from Prince [[Bogusław Radziwiłł|Boguslav]], the Prince’s cousin, saying his lands are being ravaged in [[Podlasie|Podlyasye]]. Kmita gets the Prince to send him on a mission to [[Charles X Gustav of Sweden|Charles X Gustav]] via Podlyasye. On the road he meets Prince Boguslav making his way to Kyedani and learns at last about the Radzivill’s treachery. He kidnaps the Prince but the latter manages to escape, wounding Kmita and killing two of his men.

Sergeant Soroka assumes command and they take refuge in a pitch-maker’s cabin deep in the forest. The blacksmith escapes and a fight takes place with some horse thieves who turn out to be Pan Kyemlich and his two sons, ex-soldiers of Kmita’s and so loyal to him. Kmita, who has lost Radzivill’s letters, decides to act as a horse dealer and heads of with the Kyemlichs for the Prussian border after writing a letter to the Colonels under the name of Babinich warning them of Radzivill’s movements and strategy. He also writes to Radzivill warning him not to harm Olenka or he will reveal his treacherous letters.

Kmita encounters Jendzian, now a lower noble, who agrees to take the letter to the Colonels. Commonwealth troops arrive at the inn, the Mandrake, and Kmita’s men fight with Yuzva Butrym’s men and overcome them. Arriving at Shchuchyn with his small retinue, he is reunited with his old master Yan Skshetuski, and tells them about Kmita’s conversion. The Colonels are wary but, after receiving a letter signed by Kmita, decide to move for Byalystok to concentrate the Commonwealth forces. Here, Zagloba is surprisingly made temporary leader and immediately starts disciplining and organising supplies for the troops, and building breastworks. Volodyovski is sent to deal with a force besieging a village. Finally, the voevoda of Vityebsk, Sapyeha, arrives with his army, accompanied by a returning Volodyovski.

'''Chapters XXXIV – XXXVII'''

Radzivill has to wait for Swedish troops before descending on Podlyasye. The Swedes have overrun Great Poland, Little Poland and have overcome [[Cracow]]. Prince Boguslav arrives at Kyedani and bolsters his cousin’s morale as well as laying siege to Olenka who he falsely informs that Kmita is joining the Polish traitor, Radzeyovski, for gold and promising to deliver John II Casimir, taking refuge in [[Silesia]], to the Swedes. He also ensures he befriends Olenka’s guardian, the sword-bearer of Rossyeni. A letter arrives from Sapyeha urging the Radzivills to break with Karl Gustav and seek forgiveness from King John Casimir but the Prince decides to march on Podlyasye.

Andrei Kmita, now pretending to be a [[Catholic]] nobleman from [[Duchy of Prussia|Electoral Prussia]], is disillusioned by the talk of the nobles who are now resigned to Swedish rule. He is forced to sell his horses to a Prussian commandant at Pryasnysh in return for a paper receipt which he can now use as his pass to get to [[Warsaw]]. News arrives of [[Siege of Kraków (1655)|the fall of Cracow]] and the defeat of [[Stefan Czarniecki|Charnyetski]] and, the closer Kmita gets to the capital, the more news he hears about the severe Swedish oppression under [[Arvid Wittenberg|Wittemberg]], the garrison commander and Radzeyovski, and the looting, particularly by Polish traitors which mostly goes unpunished. Swedish and German plunderers near [[Sochaczew|Sohachev]] besiege Pan Lushchevski, the [[starosta]], at [[Strugi, Masovian Voivodeship|Strugi]], his private estate. Kmita and his men come to his aid and beat them off. He finally leaves for [[Czestochowa|Chenstohova]] (Czestochowa), filled with hope when the starosta's daughter, also called Olenka, tells him she will be faithful to her lover, also called Andrei.

'''Chapters XXXVIII – XLI '''
[[File:JasnaGora-Suchodolski.jpg||thumb|260px|Defenders on the walls of Jasna Gora]]
The fortunes of the Swedes are increasing. The remainder of the Polish army has revolted and there are rumours that [[Aleksander Koniecpolski (1620–1659)|Konyetspolki]]'s division – a hero from [[Siege of Zbarazh|Zbaraj]] – has joined Karl Gustav. John Casimir is living in [[Glogow|Glogov]] with his small retinue but even some of these are deserting him. At an inn, Kmita overhears a conversation in German between Baron Lisola, the [[Bohemia]]n envoy of the Emperor of Germany and Count Veyhard Vjeshohovich (a mercenary fighting for the Swedes) that Chenstohova will be plundered for its treasures.

Kmita and his band make their way to the sacred monastery of [[Jasna Gora]] and he personally warns the prior, [[Augustyn Kordecki|Father Kordetski]]. After a somewhat hostile reception, especially from a suspicious Charnyetski, the fortress takes defensive measures orchestrated by Zamoyski and Charnyetski, using cannon delivered earlier from Cracow. The Swedish force led by General Miller arrives, terms of surrender are rejected and the siege commences – against the advice of the highly experienced Colonel Sadovski – on 18 November. Kmita himself takes charge of firing one cannon and successfully destroys many Swedish cannon and troops. The besieged also make a surprise sortie on November 28 and destroy a further two Swedish cannons. Charnyetski is brought round by the Lithuanian's skill and courage, particularly when Kmita defuses a Swedish cannonball by removing its charge. Prior Kordotski requests Kmita to dedicate the iron ball to the Most Holy Lady once the enemy have left the field.

==Sources==

''The Deluge'' (Vol. I), Henryk Sienkiewicz, authorised and unabridged translation from the Polish by Jeremiah Curtin, Little, Brown and Company, Boston, 1904 (copyright 1890, 1898).

==Major characters==
'''Historical figures:'''
[[File:Januš Radzivił. Януш Радзівіл (C. Merian, 1653).jpg|thumb|Prince Yanush Radzivill (1653)]]
[[File:Bacciarelli - Jan Kazimierz.jpeg|thumb|right|King [[John II Casimir]] of Poland]]
* [[Janusz Radziwiłł (1612–1655)|Janusz Radziwiłł]]
::Prince '''Janusz Radziwiłł''' of [[Trąby coat of arms|Trąby]] (also known as ''Janusz the Second'' or ''Janusz the Younger'', 1612–1655) was a notable [[Polish-Lithuanian Commonwealth|Polish–Lithuanian]] [[szlachta|noble]] and magnate. Throughout his life he occupied a number of posts in the state administration, including that of [[podkomorzy|Court Chamberlain of Lithuania]] (from 1633) and [[Hetmans of Poland and Lithuania#Field and Great Hetmans|Field Hetman of Lithuania]] (from 1654). He was also a [[voivode]] of [[Vilnius Voivodeship|Vilna Voivodeship]], as well as a [[starost]] of [[Samogitia]], [[Kamyanyets|Kamieniec]], [[Kazimierz Dolny|Kazimierz]] and [[Sejwy]].

::In his times he was one of the most powerful people in the Commonwealth, often described as a ''de facto'' ruler of the entire [[Grand Duchy of Lithuania]]. During the [[Deluge (history)|"Deluge"]], the Swedish invasion of Poland-Lithuania during the [[Second Northern War]], he sided with the Swedish king signing the [[Treaty of Kėdainiai]] and the [[Union of Kėdainiai]]. This move however antagonised him with most of other nobles, including members of his own family. His forces were eventually defeated in battle and he himself died in a besieged castle at [[Tykocin]].
* [[Bogusław Radziwiłł]]
* [[Paweł Jan Sapieha]]
* [[John II Casimir]]
* [[Stefan Czarniecki]]
* [[Jerzy Sebastian Lubomirski]]
'''Fictional characters (some based on actual historical figures):'''
* [[Andrzej Kmicic]]
* [[Michał Wołodyjowski]]
* [[Aleksandra Billewiczówna]]
* [[Jan Onufry Zagłoba]]
* [[Jan Skrzetuski]]
* Stanisław Skrzetuski
* Soroka
* Roch Kowalski

==Film adaptations==
The novel was adapted for the screen by Polish director [[Jerzy Hoffman]] in 1974, as ''[[The Deluge (film)|The Deluge]]''. The role of Andrzej Kmicic was played by Polish actor [[Daniel Olbrychski]]. Yet the first known adaptation, then as a silent film, was made by [[Pyotr Chardynin]] in 1915.

==See also==
{{portal|Novels}}
* [[Polish-Lithuanian Commonwealth]]
* [[The Deluge (film)]]
* [[The Deluge (Polish history)|The Deluge]]
* [[List of historical novels]]

== References ==
* Henryk Sienkiewicz, The Deluge, Hippocrene Books 1991, ISBN 0-87052-004-0
* Jerzy R. Krzyżanowski, The Trilogy Companion: A Reader's Guide to the Trilogy of Henryk Sienkiewicz,  Hippocrene Books, 1992, ISBN 0-87052-221-3
* Sienkiewicz Trilogy DVD edition, 2004

==External links==
* [http://pl.wikisource.org/wiki/Potop The full text of the book in Polish in pl.wikisource]

{{Henryk Sienkiewicz}}
{{The Trilogy}}

{{Authority control}}
{{DEFAULTSORT:Deluge, The}}
[[Category:1886 novels]]
[[Category:Novels by Henryk Sienkiewicz]]
[[Category:Polish historical novels]]
[[Category:Polish novels adapted into films]]
[[Category:Sienkiewicz's Trilogy]]