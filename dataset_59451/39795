: ''For the [[jockey]], see: '' [[Jorge F. Chavez]]
: For other possible meanings see [[Chávez (disambiguation)]]
{{spanish name|Chávez|Dartnell}}
{{Infobox person
| name = Jorge Chávez
| image = Geo Chavez.jpg
| caption = 
| birth_date = {{Birth-date|January 13, 1887|January 13, 1887}} 
| birth_place = [[Paris]], [[France]]
| death_date = {{death-date|September 27, 1910|September 27, 1910}} (aged 23)
| death_place = [[Domodossola]], [[Italy]]
| education =
| occupation = Aviator
| spouse = 
| parents = Manuel Chávez Moreyra<br />María Rosa Dartnell y Guisse
| nationality = Peruvian
| children =
}}
'''Jorge Chávez Dartnell''' (January 13, 1887 &ndash; September 27, 1910), also known as Géo Chávez, was a [[Peru]]vian [[aviator]]. At a young age, he achieved fame for his aeronautical feats. He died in 1910 while attempting the first air crossing of the [[Alps]].

==Early life==
Jorge Chávez Dartnell was born in [[Paris]], [[France]] to [[Peruvians|Peruvian]] parents Manuel Chávez Moreyra and María Rosa Dartnell y Guisse. He studied at the Violet School from where he graduated with an [[engineer's degree]] in 1908.<ref>Museo Aeronáutico del Perú, [http://incaland.com/museofap/JORGE.htm ''Jorge Chávez'']. Retrieved on May 30, 2007.</ref>

==Career==
Chávez attended the school of aviation established by [[Henry Farman|Henry]] and [[Maurice Farman]] where he got his [[pilot licensing and certification|pilot license]] and undertook his first flight in [[Reims]] on February 28, 1910.<ref>Alberto Tauro del Pino, ''Enciclopedia Ilustrada del Perú'', vol. IV, p. 607.</ref> Afterwards he participated in several aviation competitions throughout France and other European countries. On August 8 of the same year he took a [[Blériot Aéronautique|Blériot]] [[monoplane]] to [[Blackpool]], [[England]] where he achieved fame after attaining an altitude of {{convert|5405|ft|m|sp=us|disp=flip}}.<ref>John Warth, [http://www.smithsonianeducation.org/scitech/impacto/graphic/aviation/jorge.html "Adventurers of the Air"]. Retrieved on May 30, 2007.</ref> He improved his mark by flying at {{convert|8700|ft|m|sp=us|disp=flip}} over the city of [[Issy]], France on September 6.<ref>Jorge Basadre, ''Historia de la República del Perú'', vol. VIII, p. 383.</ref>

==Death==
After this successful series, Chávez decided to undertake the first air crossing of the [[Alps]]. This attempt was made in response to a prize of $20,000 offered by the [[flying club|aero club]] of Italy for the first aviator to make the trip alive.<ref>John Warth, [http://www.smithsonianeducation.org/scitech/impacto/graphic/aviation/jorge.html "Adventurers of the Air"]. Retrieved on May 30, 2007.</ref> After several delays due to bad weather, he took off from [[Ried-Brig, Switzerland|Ried-Brig]], [[Switzerland]] on September 23, 1910, and made his way through the [[Simplon Pass]]. Before departing he said, "Whatever happens, I shall be found on the other side of the Alps".<ref>John Warth, [http://www.smithsonianeducation.org/scitech/impacto/graphic/aviation/jorge.html "Adventurers of the Air"]. Retrieved on May 30, 2007.</ref> Fifty-one minutes later he arrived to his destination, the city of [[Domodossola]], [[Italy]], but his plane crashed upon landing. It is believed that the airplane had been damaged previously and inadequately repaired, which caused the aircraft to break under the heavy winds of the mountains.<ref>Jorge Basadre, ''Historia de la República del Perú'', vol. VIII, p. 384.</ref> Heavily injured but conscious, Chávez agonized for four days before dying of massive [[bleeding|blood loss]].<ref>Jorge Basadre, ''Historia de la República del Perú'', vol. VIII, pp. 384-385.</ref> His last words were, "Higher. Always higher." according to the testimony of his friend and fellow aviator [[Juan Bielovucic]].<ref>Jorge Basadre, ''Historia de la República del Perú'', vol. VIII, p. 384.</ref>

==Legacy==

The death of Jorge Chávez caused great commotion in the aviation world. Brig and Domodossola, the start and end points of his last flight, dedicated monuments to the lost aviator. In Peru, Chávez became an icon for aviation related institutions such as the [[Air Force of Peru|Air Force]]. His remains were initially buried in France but repatriated to Peru in 1957, where they currently rest at the Officer's School of the [[Air Force of Peru|Peruvian Air Force]] at ''Las Palmas''.<ref>Jorge Basadre, ''Historia de la República del Perú'', vol. VIII, p. 385.</ref> The [[Jorge Chávez International Airport|International Airport of Lima]], inaugurated in 1960, is named after him. A life-sized replica of Chávez famous [[Blériot XI]] monoplane is still on display at the air terminal.

He appears as a character in scenes drawing upon his real-life tragic flight over the Alps in [[John Berger]]'s novel ''[[G. (novel)|G.]]'' (1972), awarded the Booker Prize in 1972.

== Gallery ==
<center>
<gallery>
Image:ChavezLimaMonument.JPG|Monument to Jorge Chávez in Lima, Perú
Image:GeoChavezMemorialBrig.JPG|Memorial to Jorge Chávez in the market square of Brigue
Image:Domodossola.Monumento_a_Geo_Chavez.JPG|Monument to Jorge Chávez in Domodossola
Image:Geo Chavez01.jpg|Monument to Jorge Chávez in Brigue
</gallery></center>

==See also==
*[[1910 in aviation]]
*[[Jorge Chávez International Airport]]
*[[List of aviators killed in aircraft crashes]]

==Notes==
{{reflist}}

==Bibliography==
*{{es icon}} Basadre, Jorge. ''Historia de la República del Perú''. Lima: Editorial Universitaria, 1983.
*{{es icon}} Museo Aeronáutico del Perú. [http://incaland.com/museofap/JORGE.htm ''Jorge Chávez''].
*{{es icon}} Tauro del Pino, Alberto. ''Enciclopedia Ilustrada del Perú''. Lima: Peisa, 2003.
*Warth, John. [http://www.smithsonianeducation.org/scitech/impacto/graphic/aviation/jorge.html "Adventurers of the Air"]. In Whitney, Caspar (Editor). ''Collier’s Aviation Pioneers''. 1911.

==External links==
*{{en icon}} {{es icon}} {{it icon}} {{fr icon}} [http://www.JorgeChavezDartnell.com Jorge Chavez complete Biography]
*{{es icon}} [http://www.ArribaSiempreArriba.com/jch Biography]
*{{en icon}} [http://earlyaviators.com/echavez.htm Jorge Chávez at EarlyAviators.com]

{{Aviators killed in early aviation accidents}}

{{Authority control}}

{{DEFAULTSORT:Chavez, Jorge}}
[[Category:1887 births]]
[[Category:1910 deaths]]
[[Category:People from Paris]]
[[Category:Peruvian aviators]]
[[Category:Aviators killed in aviation accidents or incidents in Italy]]
[[Category:French people of Peruvian descent]]