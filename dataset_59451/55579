{{Infobox journal
| title         = Annals of Nuclear Energy
| cover         = [[File:Annals of Nuclear Energy.gif]]
| caption       = 
| former_name   = Annals of Nuclear Science and Engineering (1974)
| abbreviation  = Ann. Nucl. Energy
| discipline    = [[Nuclear engineering]]
| peer-reviewed =
| language      = 
| editor        = Lynn E. Weaver, S. Mostafa Ghiaasiaan, Imre Pázsit
| publisher     = [[Elsevier]]
| country       = 
| history       = 1975 – present
| frequency     = Monthly
| openaccess    = [[Hybrid open access journal|Hybrid]]
| license       = 
| impact        = 1.020
| impact-year   = 2013
| ISSNlabel     =
| ISSN          = 0306-4549
| eISSN         = 1873-2100
| CODEN         = ANENDJ
| JSTOR         = 
| LCCN          = 
| OCLC          = 50375208
| website       = http://www.journals.elsevier.com/annals-of-nuclear-energy/
| link1         = http://www.sciencedirect.com/science/journal/03064549
| link1-name    = Online access
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}
'''''Annals of Nuclear Energy''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] covering research on [[Nuclear power|nuclear energy]] and [[Nuclear physics|nuclear science]]. It was established in 1975 and is published by [[Elsevier]].

The current [[editors-in-chief]] are Lynn E. Weaver ([[Florida Institute of Technology]]), S. Mostafa Ghiaasiaan ([[Georgia Institute of Technology]]) and Imre Pázsit ([[Chalmers University of Technology]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-27 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[Pubmed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/7600031 |title=Annals of Nuclear Energy |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2014-12-27}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-27}}</ref>
* [[Current Contents]]/Engineering, Computing & Technology<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=[http://www.elsevier.com/online-tools/scopus/content-overview Scopus coverage lists] |accessdate=2014-12-31}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.020.<ref name=WoS>{{cite book |year=2014 |chapter=Annals of Nuclear Energy |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==Former titles history==
''Annals of Nuclear Energy'' is derived from the following former titles:

*''Journal of Nuclear Energy'' (1954-1959)<ref name=JNE>{{cite web|url=http://www.sciencedirect.com/science/journal/08913919|title=Journal of Nuclear Energy - ScienceDirect.com|accessdate=2014-12-21}}</ref>{{refn|name=JNEn|group=Note|CASSI ISSN Search: 0022-3107.<ref name=CASSI />}}
*''Journal of Nuclear Energy. Part A. Reactor Science'' (1959-1961)<ref name=JNEA>{{cite web|url=http://www.sciencedirect.com/science/journal/03683265|title=Journal of Nuclear Energy. Part A. Reactor Science - ScienceDirect.com|accessdate=2014-12-21}}</ref>{{refn|group=Note|CASSI ISSN Search: 0368-3265.<ref name=CASSI />}}
*''Journal of Nuclear Energy. Part B. Reactor Technology'' (1959)<ref name=JNEB>{{cite web|url=http://www.sciencedirect.com/science/journal/03683273|title=Journal of Nuclear Energy. Part B. Reactor Technology - ScienceDirect.com|accessdate=2014-12-21}}</ref>{{refn|group=Note|CASSI ISSN Search: 0368-3273.<ref name=CASSI />}}
*''Journal of Nuclear Energy. Parts A/B. Reactor Science and Technology'' (1961-1966)<ref name=JNEAB>{{cite web|url=http://www.sciencedirect.com/science/journal/03683230|title=Journal of Nuclear Energy. Parts A/B. Reactor Science and Technology - ScienceDirect.com|accessdate=2014-12-21}}</ref>{{refn|group=Note|CASSI ISSN Search: 0368-3230.<ref name=CASSI />}}
*''Journal of Nuclear Energy'' (1967-1973)<ref name=JNE2>{{cite web|url=http://www.sciencedirect.com/science/journal/00223107|title=Journal of Nuclear Energy - ScienceDirect.com|accessdate=2014-12-21}}</ref>{{refn|name=JNEn|group=Note}}
*''Annals of Nuclear Science and Engineering'' (1974)<ref name=ANSE>{{cite web|url=http://www.sciencedirect.com/science/journal/03022927|title=Annals of Nuclear Science and Engineering - ScienceDirect.com|accessdate=2014-12-21}}</ref>{{refn|group=Note|CASSI ISSN Search: 0302-2927.<ref name=CASSI />}}
*''Annals of Nuclear Energy'' (1975–present)

==Notes==
{{reflist|group=Note}}

==References==
{{Reflist}}

== External links ==
{{Official website|http://www.journals.elsevier.com/annals-of-nuclear-energy/}}

[[Category:Engineering journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1975]]


{{engineering-journal-stub}}