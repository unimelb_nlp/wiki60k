[[File:Afifa Karam.jpg|thumb|Portrait photograph of Afifa Karam]]

'''‘Afīfa Karam''' (1883–1924) (عفيفة كرم) was a Lebanese-American journalist, novelist, and translator. A writer for the New York City-based Lebanese daily newspaper ''[[al-Hoda]]'', Karam authored three original Arabic novels as well as a number of Arabic translations of novels from English and French. She was an advocate for women’s rights in the [[Mahjar]], or Arab diaspora, and of Arab Feminism.

== Early life ==
‘Afīfa Karam was born in the town of ‘Amshit, Mount Lebanon into the wealthy Maronite family of<ref name=Gualtieri>{{cite book|last=Gualtieri|first=Sarah|title=Between Arab and White|year=2009|publisher=University of California Press|location=Berkeley|isbn=9780520255326|pages=88|url=https://books.google.com/books?id=MPxrMLKuXvkC&dq=editions:FKT0PIGLuI8C&source=gbs_navlinks_s}}</ref> a doctor in the Ottoman army. Karam was educated in local missionary schools until the age of thirteen, when she was married to a relative, John Karam.<ref name=Zeidan>{{cite book|last=Zeidan|first=Joseph T.|title=Arab Women Novelists: The Formative Years and Beyond|year=1995|publisher=SUNY Press|location=Albany|isbn=9780791421710|pages=70|url=https://books.google.com/books?id=J6YXo4VV6yMC&dq=arab+women+novelists&source=gbs_navlinks_s}}</ref> In 1897 she and her husband moved to the United States and settled in Shreveport, Louisiana.<ref name=Zeidan />

== Journalism ==
Karam continued to study Arabic language and literature. In 1903, at the age of twenty, she began to submit her writing to the New York City-based Arabic-language newspaper ''[[Al-Hoda]]'' (''The'' ''Guidance''). Its editor-in-chief, [[Naoum Mokarzel|Na’oum Mokarzel]], provided her with Arabic literary texts to read and he personally critiqued her writing. In 1911, he put her in charge of the paper for six months while he was out of the country.<ref name=Shakir>{{cite book|last=Shakir|first=Evelyn|title=Bint Arab: Arab and Arab American Women in the United States|year=1997|publisher=Praeger|location=Westport, CT|isbn=9780275956721|pages=55|url=https://books.google.com/books?id=sD4OAQAAMAAJ&q=bint+arab&dq=bint+arab&hl=en&sa=X&ei=MX09T5zoPOrq2AXH9uW4CA&ved=0CD4Q6AEwAA}}</ref> That same year, Karam founded a monthly women’s periodical, ''al-‘Ālam al-Jadīd al-Nisā’ī'' (''The New Women’s World'') (1911) which gave way two years later to a second publication, ''al-’Imra’a al-Sūrīyya'' (''Syrian Woman''), founded by Karam in 1913.<ref name=Shakir />

== Novels ==
At the age of 23, Karam made her literary debut in the New York City-based Lebanese newspaper ''[[al-Hoda]]'' (''Guidance''). She took a six-month hiatus from her journalistic work to devote her efforts to the writing of her first novel, ''Badī‘a wa Fu’ād'' (''Badi’a and Fu’ad''), published in 1906 by ''[[Al-Hoda]] Press''. Her second and third novels, ''Fāṭima al-Badawīyya'' (''Fatima the Bedouin'') (ca. 1908) and ''Ghādat ‘Amshīt'' (''The Girl of ‘Amshit'') (ca. 1910) were also published by ''Al-Hoda'' over the next several years. Karam’s three original novels all appeared before the 1914 publication of ''Zaynab'' by the Egyptian author [[Muḥammad Ḥusayn Haykal]], which is widely considered to be the “first Arabic novel” by the accepted canon of Arabic literature.<ref name=Allen>{{cite book|last=Allen|first=Roger|title=The Arabic Novel: An Historical and Critical Introduction|year=1995|publisher=Syracuse University Press|isbn=9780815626411|pages=31|url=https://books.google.com/books?id=P1-0uvBtWwEC&dq=the+arabic+novel&source=gbs_navlinks_s}}</ref>

== Literary Innovation ==
As a first generation immigrant writer, Karam's literature questioned and negotiated between inherited Arab and American values, and promoted the social emancipation and education of Levantine-American immigrants, particularly the women among them. She criticized restrictive gender roles and practices that she deemed oppressive to women.<ref name=Shaaban>{{cite book|last=Shaaban|first=Bouthaina|title=Voices Revealed: Arab Women Novelists: 1898-2000|year=2009|publisher=Lynne Rienner Publishers|isbn=9780894108716|pages=26–32|url=https://books.google.com/books?id=YRsqAQAAIAAJ&q=arab+women+novelists&dq=arab+women+novelists&hl=en&sa=X&ei=eoE9T5WTL6Xq2QXKyqytCA&sqi=2&ved=0CFcQ6AEwBg}}</ref> Karam's stories show man as oppressor and woman as oppressed, and condemn the governmental and religious institutions that uphold such unjust practices in Lebanon.

Karam’s novels did not circulate widely in the Arab world. None of her novels was republished until the centennial republication of her first novel ''Badī‘a wa Fu’ād'' by Sa‘īd Yaqṭīn (Rabat: Manshūrāt al-Zaman, 2007). Nonetheless, Karam’s novels are some of the earliest Arabic literary texts written in that form.

== Relationship to the Arabic Literary Scene ==
Through the international world of Arabic journalism, Karam was part of the literary scene in Cairo and the Levant, which was the locus of the Arabic literary and cultural Renaissance ([[al-Nahda]]). Feminist currents spread among a segment of the Arab intelligentsia, giving rise to the establishment of a number of women’s journals just before and just after the turn of the twentieth century. In an early issue of her journal ''The New Women’s World'', Karam pays homage to the women’s magazines in Cairo, Beirut, and Damascus, calling her own journal their “child.”<ref name=Shakir /> In turn, literati in Arab countries recognized Karam as a journalist and a novelist and her articles were republished in women’s journals such as ''Fatāt al-Sharq'' (''Young Woman of the East''). Karam was mentioned twice as a biographical subject in that journal, first in 1908, and later in 1924, when its founder Labība Hāshim wrote her obituary.<ref name=Booth /> Karam was called the “adornment of women's literature in the New World" and the "pride of Eastern ladies” who "adorned the newspapers with the pearls of her words."  Her work was described as “a sword she brandished against traditions, awakening her countrywomen from the lethargy of inaction and ignorance. She walked before them, bearing the banner of literary freedom: ‘woman is the foundation of the nation’s ascent.’”<ref name=Booth>{{cite book|last=Booth|first=Marilyn|title=May Her Liked Be Multiplied: Biography and Gender Politics in Egypt|year=2001|publisher=UC Press|location=Berkeley|isbn=9780520224209|pages=84|url=https://books.google.com/books?id=mhPjAA53mgUC&dq=may+her+likes+be+multiplied&source=gbs_navlinks_s}}</ref>

== Works ==
Karam’s novels and translations include:
* ''Badī‘a wa Fu’ād'' (1906)
* ''Fāṭima al-Badawīyya'' (ca. 1908)
* ''Ghādat ‘Amshīt'' (ca. 1910)
* ''Nānsī Stāyir'' (1914) [Arabic translation of ''Nancy Stair'' by [[Elinor Macartney Lane]]]
* ''Riwāyat ’Ibnat Nā’ib al-Malik'' (1918) [Arabic translation of ''Une fille du régent'' by [[Alexandre Dumas]]]
* ''Muḥammad ‘Alī Bāsha al-Kabīr'' (1919) [Arabic translation of ''Muhammad Ali und Sein Haus'' by [[Luise Muhlbach]]]

== See also ==
*[[Arabic Literary Renaissance]] 
*[[Al-Nahda]]
*[[Mahjar]]
*[[Al-Rabitah al-Qalamiyya]]
*[[Lebanese Americans]]
*[[Arab immigration to the United States]]
*[[Al-Hoda]]

== References ==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Karam, Afifa}}
[[Category:1883 births]]
[[Category:1924 deaths]]
[[Category:American women journalists]]
[[Category:20th-century American novelists]]
[[Category:American translators]]
[[Category:American people of Lebanese descent]]
[[Category:American women novelists]]
[[Category:20th-century women writers]]