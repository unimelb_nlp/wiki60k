{{good article}}
{{Infobox University Boat Race
| name= 138th Boat Race
| winner =Oxford
| margin =  1 and 1/4 lengths
| winning_time=  17 minutes 44 seconds
| date= 4 April 1992
| umpire = Roger Stephens<br>(Cambridge)
| prevseason= [[The Boat Race 1991|1991]]
| nextseason= [[The Boat Race 1993|1993]]
| overall =69&ndash;68
| reserve_winner= Goldie
| women_winner = Cambridge
}}
The 138th [[The Boat Race|Boat Race]] took place on 4 April 1992. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford's cox Andrew Probert was the oldest competitor in Boat Race history at the age of 38 years and 86 days.  Oxford won by one-and-a-quarter lengths, the closest margin of victory for twenty years.  The race also featured the first German competitor in the history of the event in Dirk Bangert.  Umpired by former Cambridge rower Roger Stephens, Mike Rosewell writing in ''[[The Times]]'' described the race as "one of the greatest races since 1829".

In the reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] defeated Oxford's Isis, while Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 8 July 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 8 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a "hotly contested point of honour" between the two universities, followed throughout the United Kingdom, and broadcast on several international television networks.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=8 July 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 7 July 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1991|1991 race]] by four-and-a-quarter lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher =The Boat Race Company Limited | title = Boat Race – Results| accessdate = 4 July 2014}}</ref> with Cambridge leading overall with 69 victories to Oxford's 67 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 8 July 2014}}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref> The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

President of the [[Cambridge University Boat Club]], Max Justicz, said of the previous year's race: "We were burned on that day. Badly burned. It was worse than other defeats because we thought we could win ... Oxford just rowed through us; with every stroke they took they destroyed our belief in ourselves."<ref name=ever>{{Cite news | title = Can Cambridge ever win the boat race? | first = Michael | last = Woodhead | date = 4 April 1992 | work = [[The Times]] | page = 18 | issue = 64298}}</ref>  His crew mate, Nick Clarry, focused on the approach to this year's race: "This year it's heads down and get on with the job.  We know that if we don't cross the line first on the day, nobody could care less who we are."<ref name=ever/>

[[Beefeater Gin]] sponsored the event;  prior to this year's race they had announced a £1&nbsp;million deal to continue their close involvement for a further three years.<ref name=intense>{{Cite news | title = Boys in blue arrest intense Boat Race rivalry| work =[[The Guardian]] | first = Christopher | last = Dodd | page = 19 | date = 25 March 1992}}</ref>

==Crews==
Aged 38 years and 3 months, Cambridge's cox, Andrew Probert, was the oldest competitor in the history of the Boat Race.<ref>{{Cite web | url = http://www.guinnessworldrecords.com/records-10000/university-boat-race-oldest-participant/ | work = [[Guinness World Records]]| accessdate = 8 July 2014 | title = University Boat Race &ndash; Oldest Participant}}</ref>  Oxford's crew featured four returning [[Blue (university sport)|Blues]] and two former Isis crew members, while Cambridge saw three old Blues participate.<ref name=edge/>  The Oxford boat was made up from five Britons, an Australian, an American and a Yugoslav;  Cambridge was represented by seven Britons, an American and Dirk Bangert, the first German to participate in the event.<ref name=edge/> Oxford's crew were coached by Steve Royle and [[Patrick Sweeney (rower)|Patrick Sweeney]], and assisted by the former [[East Germany at the Olympics|East Germany]] Olympic coach [[Jürgen Gröbler]],<ref name=stam/> while Cambridge were guided Oxford's successful coach of 1991,<ref name=ever/> John Wilson.<ref name=edge/>  [[Waterman (occupation)|Watermen]] Bert Green and Jim Cobb provided advice to the Oxford and Cambridge coxes respectively.<ref name=edge/>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || Kingsley Poole || [[St John's College, Oxford|St John's]] || 13 st 4 lb ||  Max Justicz (P) || [[Sidney Sussex College, Cambridge|Sidney Sussex]] || 13 st 6.5 lb
|-
| 2 ||  Joseph Michels (P)|| [[University College, Oxford|University]] || 13 st 2.5 lb || Nicholas Clarry || [[Jesus College, Cambridge|Jesus]] ||  13 st 1 lb
|-
| 3 || Boris Mavra || [[Jesus College, Oxford|Jesus]] || 14 st 8 lb || James Behrens|| [[Downing College, Cambridge|Downing]] || 13 st 2.5 lb
|-
| 4 || Hamish Hume || [[Pembroke College, Oxford|Pembroke]] || 13 st 2.5 lb || Daniel Justicz || [[Downing College, Cambridge|Downing]] || 13 st 3 lb
|-
| 5 || Peter Bridge || [[Oriel College, Oxford|Oriel]] || 13 st 13.5 lb || Donald Fawcett || [[Magdalene College, Cambridge|Magdalene ]] || 15 st 4 lb
|-
| 6 || Calman Maclennan || [[Keble College, Oxford|Keble]] || 14 st 6.5 lb || David Gillard|| [[St Catharine's College, Cambridge|St Cathareine's]] || 14 st 7.5 lb
|-
| 7 || Simon Davy|| [[Worcester College, Oxford|Worcester]] || 12 st 6 lb || Stephen Fowler || [[Robinson College, Cambridge|Robinson]] ||  13 st 4 lb
|-
| [[Stroke (rowing)|Stroke]] || Ian Gardiner  || [[St Peter's College, Oxford|St Peter's]] || 13 st 1 lb || Dirk Bangert || [[Fitzwilliam College, Cambridge|Fitzwilliam]]  || 12 st 10.5 lb
|-
| [[Coxswain (rowing)|Cox]] || Elizabeth Chick || [[Christ Church, Oxford|Christ Church]] || 7 st 11.5 lb || Andrew Probert || [[Magdalene College, Cambridge|Magdalene]]  || 7 st 11 lb
|-
!colspan="7"|Source:<ref name=edge>{{Cite news | title = Oxford have the edge in a conflict of styles | first = Mike | last = Rosewell | work = [[The Times]] | date = 4 April 1992| page = 34 | issue = 64298 }}</ref><br>(P) &ndash; boat club president
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
The race commenced at 2.35&nbsp;pm.<ref name=edge/>  Oxford won the toss and elected to start from the Middlesex station.<ref name=ann/><ref name=barnes/> The boats raced side-by-side for the first three-and-a-half miles of the race, for fourteen minutes neither crew held a lead of more than half-a-length over the other,<ref name=ann/> and umpire Roger Stephens was forced to issue warnings to both coxes for steering too close to one another.<ref name=barnes/>  The Dark Blues held a slight early advantage but Cambridge pulled ahead at [[Hammersmith Bridge]].<ref name=ann/> Oxford edged ahead at [[Barnes Railway Bridge|Barnes Bridge]],<ref name=stam/> and passed the finishing post in a time of 17 minutes 44 seconds, one-and-a-quarter lengths ahead of Cambridge, the closest finish in the last 20 years.<ref name=results/><ref name=ann>{{Cite web | url = http://news.bbc.co.uk/sport1/hi/other_sports/specials/boat_race_2002/1844116.stm | title = Anniversary action | date = 15 March 2002 | publisher = BBC Sport | accessdate = 12 July 2014}}</ref>  Oxford's Yugoslav rower Boris Mavra had to be lifted from the boat at the end of the race.<ref name=stam/>  The [[Beefeater Gin]] Trophy was presented to the winning boat club president by [[Raymond G. H. Seitz|Raymond Seitz]].<ref name=edge/>

In the reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] won by three-and-a-quarter lengths over Isis, their fifth victory in six years.<ref name=results/>  Cambridge won the 47th [[Women's Boat Race]] by one-third-of-a-length in a time of 6 minutes and 20 seconds, their third victory in four years.<ref name=results/>

==Reaction==
Mike Rosewell, writing in  ''[[The Times]]'' complimented all of the race participants: "the 18 individuals involved produced one of the greatest races since 1829."<ref name=stam>{{Cite news | title = Oxford stamina tells at close| first = Mike |last = Rosewell | work = [[The Times]] | date = 6 April 1992 | page = 26 | issue = 64299}}</ref> Former Oxford coach [[Daniel Topolski]], writing for ''[[The Observer]]'', described the Oxford's win as "scintillating" and noted that the crews were "locked in combat for fully three and a half miles."<ref name=barnes>{{Cite news | title = Barnes-storming Oxford| work = [[The Observer]] | first = Daniel | last = Topolski| authorlink= Daniel Topolski | date = 5 April 1992 | page = 46}}</ref>  Christopher Dodd, writing in ''[[The Guardian]]'' suggested that "the promise was delivered; a rare race".<ref name=bridge>{{Cite news | title = A bridge too far for brave Cambridge| first = Christopher | last = Dodd| work = [[The Guardian]] | page = 23 | date = 6 April 1992}}</ref>

Oxford's number five and Great Britain international Peter Bridge noted Gröbler's impact: "We really felt the strength that we had built up over six months under Jürgen's methods."<ref name=stam/>  He continued: "[Cambridge] were slower off the start than we expected which was nice."<ref name=barnes/>  Referring to Cambridge's lead at Hammersmith Bridge, their number seven Steve Fowler said "we were feeling good but we should have closed the door there and then.  We should have killed them on the bend."<ref name=barnes/>  Cambridge cox Probert conceded to his counterpart Liz Chick: "she steered very well."<ref name=bridge/>  Oxford coach Gröbler stated: "It was wonderful.  Both crews ... were very good &mdash; and so disciplined.  I like these university students."<ref name=barnes/> Cambridge coach Wilson lamented: "we failed to capitalise at Hammersmith and they grew in confidence thereafter."<ref name=bridge/>

==References==
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1992}}
[[Category:The Boat Race]]
[[Category:1992 in English sport]]
[[Category:1992 in rowing]]