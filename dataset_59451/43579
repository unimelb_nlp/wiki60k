<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Shche-2
 |image=
 |caption= 
}}{{Infobox Aircraft Type
 |type= [[Military transport aircraft|Light transport]]
 |manufacturer= OKB-47
 |designer=[[Alexei Shcherbakov]]
 |first flight= 1942
 |introduced=
 |retired= 
 |status= 
 |primary user= [[Soviet Air Force]]
 |more users=[[Polish Air Force]]<br/>[[SFR Yugoslav Air Force|Yugoslav Air Force]]<br/>[[Aeroflot]]
 |produced= 1943–1946
 |number built= 550 (according to some sources, 567)
 |unit cost=
 |program cost = 
 |developed from= 
 |variants with their own articles=
 |developed into=
}}
|}
The '''Shcherbakov Shche-2''' ({{lang-ru|Ще-2}}), also known as the '''TS-1''' and nicknamed "Pike",<ref name="Rus"/> was a twin-engined utility aircraft manufactured in the [[Soviet Union]], designed by [[Alexei Shcherbakov]] for construction by [[OKB]]-47, to meet an urgent requirement for a light [[military transport aircraft|transport]] and liaison aircraft for operation by the [[Soviet Air Force]] during the [[Second World War]]. Proving to be successful, it remained in service for a number of years post-war in both civilian and military roles in the Soviet Union, and with the air forces of several allied nations. 550 built, in use until 1956 (USSR) and 1960 (foreign users).

==Design and development==
The [[Operation Barbarossa|German invasion of the USSR]] revealed that there was an urgent requirement for a light transport and utility aircraft for use by the Soviet Air Force at the front. To meet this requirement, Aleksei Shcherbakov, who had previously worked at the [[Konstantin Kalinin|Kalinin]] design bureau, and who had also heavily influenced the design of the [[Polikarpov I-153]] fighter before conducting work on [[pressurisation|pressure cabins]] and [[glider aircraft|gliders]],<ref name="DD">Donald 1997, p. 829.</ref> was directed to design and develop an aircraft that received the designation "TS-1".<ref name="DD"/>

A cabin monoplane of [[Cantilever#Aircraft|semi-cantilever]], high-wing configuration, the TS-1 was designed to minimise the use of [[strategic material]]s, utilising mostly wood in the construction of its remarkably [[Drag (physics)|streamlined]] airframe, and being powered by two readily available [[Shvetsov M-11]] [[radial engine]]s.<ref>[http://www.ibiblio.org/pub/academic/history/marshall/military/airforce/sov_mil.txt "Aircraft of the Soviet and Russian forces".] [[Marshall University]] History Dept. via ''ibiblio.org'' archive. Accessed 2010-05-10.</ref> Parts of the [[Lavochkin La-5]] aircraft were also used, along with undercarriage parts from the [[Ilyushin Il-2]].<ref name="Rus">[http://www.airwar.ru/enc/cww2/she2.html "Sche-2".] ''airwar.ru''. {{Ru icon}}. Accessed 2010-05-19.</ref> The aircraft was equipped with a fixed, conventional [[taildragger]] [[Landing gear|undercarriage]], and a twin-fin [[vertical stabiliser|tail]] was also utilised.<ref name="DD"/>

Test-flown in late 1942 and early 1943, the aircraft, by now having been redesignated Shche-2,<ref name="DD"/> proved to be capable of meeting the requirement, and production began in October 1943<ref name="DD"/> at ''OKB-47'', the bureaux being established at Chkalov ([[Orenburg]])<ref name="Dex">Dexter 2000.</ref> for use by [[Yakovlev]], but being transferred to Shcherbakov's control for the manufacture of his type.<ref name="Rus"/>

==Operational history==
The Shche-2 was capable of transporting up to 16 troops, with an alternative [[air ambulance]] configuration for up to 11 wounded,<ref name="DD"/> or cargo up to {{convert|1.43|m}} by {{convert|1.64|m}} in size.<ref name="Rus"/> Alternatively, the aircraft could be used as an aircrew and navigational [[trainer (aircraft)|trainer]].<ref name="DD"/> It was extensively used in the transport and communications roles on the Eastern Front, providing essential, if unglamorous, service.<ref name="DD"/>

In 1945, the improved Shche-2TM variant entered flight test, powered by uprated M-11FM engines of {{convert|108|kW|hp}} each, and fitted with a modified wing.<ref name="Rus"/> Despite the improvements in the design, the decision was made not to produce the aircraft due to a reduction in requirements for the type with the end of the war in May of that year.<ref name="Rus"/> A proposed [[diesel engine|diesel]]-engined version, which began flight tests in July 1945, met the same fate.<ref name="Rus"/>

Proving in service to be underpowered yet still easy to fly,<ref name="DD"/> and establishing a reputation for reliability and ease of maintenance,<ref name="Rus"/> the Shche-2 was widely used by Soviet forces during the war. Seeing extensive service supplying [[guerrilla warfare|guerrilla]] and [[Soviet partisans|partisan]] forces,<ref name="Rus"/><ref>{{cite book|page=310|title=Aircraft engineering, Volume 20|publisher=Bunhill Publications|year=1948|quote=The Shche-2 was also used for dropping supplies to the partisans in 1944-45, and for taking out the sick and wounded from small, moderately inaccessible ..}}</ref> the Shche-2 also proved to be useful for the delivery of [[paratroopers]].<ref name="Rus"/>

It is estimated that at least 550 Shche-2 aircraft were completed before the close of production in 1946,<ref name="DD"/> the OKB-47 factory being closed down at the conclusion of production.<ref name="Dex"/> After the end of the war, the aircraft remained in service for several years, with the air forces of [[Yugoslavia]] and [[Poland]] making use of the type,<ref name="Rus"/> in addition to the aircraft being extensively utilised in transport and air ambulance duties in civilian service within the Soviet Union.<ref name="Rus"/> In addition, the Shche-2 was operated by [[Aeroflot]] on several local airline routes within the Soviet Union for several years after the end of the war, before its replacement by the [[Antonov An-2]].<ref name="Rus"/>

==Operators==

===Civil operators===
; {{USSR}}
*[[Aeroflot]]

===Military operators===
; {{POL}}
*[[Polish Air Force]] – 5 aircraft, used 1945–1947.<ref>[http://www.worldairforces.com/Countries/poland/pol.html "Polish Air Force".] ''worldairforces.com''. {{Pl icon}} Accessed 2010-05-19.</ref>
; {{USSR}}
*[[Soviet Air Force]]
;{{YUG}}:
*[[SFR Yugoslav Air Force|Yugoslav Air Force]] – 5 aircraft,delivered in 1945, used through 1952.<ref>[http://www.aeroflight.co.uk/waf/yugo/af2/types/utility.htm "Yugoslavia Air Force - Utility"], ''aeroflight.co.uk''. Accessed 2010-05-19.</ref>

==Specifications (Shche-2)==
[[File:Threeview Shche-2.gif|right|300px]]
{{Aircraft specs
|ref=<ref name="DD"/>
|prime units?=met
|genhide=
|crew=two
|capacity=up to 16 troops or 11 stretcher cases
|length m=14.27
|length ft=
|length in=
|length note=
|span m=20.54
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=64
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=2235
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=3700
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
|eng1 number=2
|eng1 name=[[Shvetsov M-11]]d
|eng1 type=[[radial engine]]s
|eng1 kw=86
|eng1 hp=
|eng1 note=
|power original=
|more power=
|prop blade number=2
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=
|perfhide=
|max speed kmh=155
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=980
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=3000
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=236
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=53
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=0.05 kW/kg (0.03 hp/lb) 
|more performance=
|armament=*None
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
|similar aircraft=
*[[Avro Anson]]
*[[Beech 18]]
*[[de Havilland Dominie]]
*[[Yakovlev Yak-6]]
|lists=
*[[List of aircraft of World War II]]
*[[List of military aircraft of the Soviet Union and CIS]]
}}

==References==
;Notes
{{reflist|2}}
;Bibliography
{{refbegin}}
*Dexter, Keith. ''The Numbered Factories and Other Establishments of the Soviet Defence Industry, 1928 to 1967: a Guide,'' Part II. ''Research & Design Establishments: Version 1.0.'' [[University of Warwick]], Department of Economics, July 2000. [http://www2.warwick.ac.uk/fac/soc/economics/staff/academic/harrison/vpk/history/part2/okb.pdf PDF link].
*Donald, David, ed. ''The Complete Encyclopedia of World Aircraft''. London: Orbis, 1997. ISBN 0-7607-0592-5.
{{refend}}

[[Category:Shcherbakov aircraft|Shche-2]]
[[Category:Soviet military transport aircraft 1940–1949]]
[[Category:Twin-engined tractor aircraft]]
[[Category:High-wing aircraft]]