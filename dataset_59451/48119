{{Infobox military person
| honorific_prefix  = 
| name              = George Willoughby
| honorific_suffix  = 
| image         = Lieutenant Willoughby.jpg
| image_size    = 250px
| caption       = Watercolour of Lieutenant Willoughby
| birth_date    = {{Birth date|1828|11|23|df=yes}}
| death_date    = {{Death date and age|1857|05|12|1828|11|23|df=yes}}
| birth_place   = [[Bath, England]]
| death_place   = India
| placeofburial = 
| placeofburial_label = 
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| nickname      = 
| birth_name    = George Dobson Percival Willoughby
| allegiance    = Britain
| branch        = British Army
| serviceyears  = 1846-1857
| rank          = Lieutenant
| servicenumber = 
| unit          = Bengal Field Artillery 
| commands      = 
| battles       = [[Indian Rebellion of 1857]]
| battles_label = 
}}
[[File:Springing a mine.jpeg|right|thumb|300px|Springing a Mine by George Willoughby]]
'''George Dobson Percival Willoughby''' (23 Nov 1828 – 12 May 1857) was a British soldier who served as a lieutenant in the [[Bengal Artillery]] of the [[East India Company]]. He is remembered for having blown up the [[Gunpowder magazine|powder magazine]] at Delhi during the [[Indian Rebellion of 1857]] to prevent it from falling into the hands of the rebels.

==Early life==
Willoughby was born in Bath, the son of Harriet Lucas (''nee'' Boys) and George Thomas Railton Willoughby. His mother, the daughter of a naval officer, was principal of a seminary for young ladies who she instructed in her home in Camden Place. He was educated privately by the Reverend W. Williams, and later at [[Addiscombe Military Seminary]] (1845–6).

==Military service==
On completion of his studies Willoughby was commissioned into the Bengal Field Artillery in 1846. Little is known of him between then and May 1857. In 1857 he was stationed in [[Delhi]] with the rank of lieutenant, Commissary of Ordnance. This was the year that saw the start of the Indian Rebellion. The Rebellion began in the evening of May 10, 1857 at Meerut, the first station where the native troops of the Bengal Army broke out in open [[insurrection]]. They marched unhindered to Delhi arriving the following morning.

Delhi was an important city with a large European community; it was surrounded by a high wall some six miles or so in circumference. The defences were manned by Indian troops commanded by British officers, one of whom was George Willoughby, who was in charge of the [[arsenal]], known as the Great Magazine. The Joint-Magistrate of Delhi, [[Sir Thomas Metcalfe, 4th Baronet]] lived outside the city at [[Metcalfe House]] beside the [[Jumna]]. It was he who informed Willoughby who was on duty at the Magazine, that the Meerut rebels were streaming across the river. Willoughby wasted no time and took steps to defend the Magazine to prevent it falling into the rebels' hands. He ordered the outer gates closed and barricaded; guns were brought out, loaded with double charges of [[grapeshot]] and positioned within the gates.<ref>Vibart 1894, pp. 547–548.</ref>

Under Willoughby's command were two Lieutenants,  Forrest and Raynor, six [[Conductor (Army)|Conductors]] (Warrant Officers) and two sergeants. Six pounder guns were stood ready to fire on the enemy should they break into the enclosure. A train of [[gunpowder]] was then laid from the foot of a large lime tree to the Magazine in order to destroy it should the need arise. Conductor Scully was stationed by the tree with orders to ignite the powder when Conductor Buckley gave him the signal by raising his hat. Lieutenant Forrest later described the preparations as follows:

{{quote|Inside the gate leading to the park we placed two six pounders double charged with grape, one under Acting Sub-Conductor Crow and Sergeant Stewart, with the lighted matches in their hands, and with orders that, if any attempt was made to force the gate, both guns were to be fired at once, and they were to fall back on that part of the Magazine in which Lt Willoughby and I were posted. The principal gate of the Magazine was similarly defended by two guns, with the "[[Cheval de frise|chevaux de frieze]]" laid down on the inside. For the further defence of this gate and the Magazine in its vicinity there were two six pounders so placed as both to command the gate and a small bastion in its vicinity. Within sixty yards of the gate, and in front of the office, and commanding two cross-roads, were three six pounders and one twenty four pounder [[howitzer]] which could be so managed as to act on any part of the Magazine in that neighbourhood. After all these guns and howitzers had been placed in the several positions above named they were loaded with double charges of grape.<ref name="Cave-Browne1861">{{cite book|author=John Cave-Browne|title=The Punjab and Delhi in 1857: being a narrative of the measures by which the Punjab was saved and Delhi recovered during the Indian Rebellion|url=https://books.google.com/books?id=TBANAAAAYAAJ&pg=PR3|accessdate=20 October 2012|year=1861|publisher=William Blackwood and Sons}}</ref>{{rp|76}}}}

The King of Delhi, [[Bahadur Shah II]], sent several messengers to Willoughby ordering the surrender of the Magazine, but no answer was forthcoming. Scaling ladders were put to use by the enemy and they began to swarm over the walls, whereupon the Indian troops revolted and joined the rebels. Rounds of grapeshot were fired into the advancing troops but they continued to attack. The British remained steadfast until all their ammunition was exhausted and two of their comrades wounded. For about five hours the nine British soldiers defended their [[fortress]]. Willoughby had vainly hoped that help was on its way from Meerut. None came. Willoughby gave the order to ignite the train of gunpowder, Buckley signalled to Scully who obeyed immediately. Within seconds there was a tremendous [[explosion]] and the Magazine was destroyed killing hundreds of rebels. In the ensuing confusion four of the defenders, Willoughby, Forrest, Raynor and Buckley although badly shaken and not expecting to be alive, managed to make good their escape. Sub-Conductor Crow and Sergeant Edwards had fallen at their gun. Scully had been so badly wounded that he was unable to move. George Willoughby was murdered the following day, it is said, by the inhabitants of a village near the [[Hindon river]].<ref name="Cave-Browne1861"/>{{rp|78–79}}

==Recognition==
[[File:Willoughby's campaign medals.jpg|right|thumb|200px|Willoughby's campaign medals]]
The following official account of the capture of the Delhi Magazine by the rebels was published in the ''Calcutta Government Guide'':

{{quote|The Right Hon'ble the Governor-General in Council is pleased to direct the publication of the following authentic report of the occurrences at the Delhi Magazine on the 11th of May last when attacked by mutineers, and of the noble and cool soldiership of its gallant defenders, commanded by Lieutenant G. D. Willoughby, Commissary of Ordnance.

The Governor-General in Council desires to offer his cordial thanks to Lieutenants Raynor and Forrest and the other survivors among the brave men mentioned in this report, and to express the admiration with which he regards the daring and heroic conduct of Lieutenant G. D. Willoughby and the warrant and non-commissioned officers by whom he was supported on that occasion. Their names are Lieutenants Raynor and Forrest, Conductors Shaw, Buckley, and Scully, Sub-Conductor Crow, Sergeants Edwards and Stewart.

The family of the late Conductor Scully who so devotedly sacrificed himself in the explosion of the magazine, will be liberally provided for, should it be ascertained that they have survived him.<ref name="Vibart1898">{{cite book |first=Edward |last=Vibart |authorlink=Edward Vibart |title=The Sepoy Mutiny as seen by a Subaltern: from Delhi to Lucknow |url=https://archive.org/details/sepoymutinyasse00vibagoog |year=1898 |publisher=Smith, Elder & Co. |place=London }}</ref><ref>{{cite book |first=H. C. |last=Fanshawe |title=Delhi Past and Present |place=London |publisher=John Murray |year=1902 |pages=108–9 |url=https://archive.org/stream/cu31924095603076#page/n135/mode/2up }}</ref>}}

In 1888 a memorial was erected at Delhi:

{{quote|On the 11th of May 1857 nine resolute Englishmen Lieutenant Geo. Dobson Willoughby, Bengal Artillery, in command Lieutenant William Rayner, Lieutenant Geo. Forrest, Conductor Geo. William Shaw, Conductor John Buckley, Conductor John Scully, Sub Conductor William Crow, Sergeant Benjamin Edward, Sergeant Peter Stewart, defended the magazine of Delhi for more than four hours against large numbers of the rebels and mutineers until the walls being scaled and all hope of succour gone these brave men fired the magazine – five of the gallant band perished in the explosion which at the same time destroyed many of the enemy. This tablet marking the former entrance gate of the magazine is placed here by the Government of India.<ref>{{cite web |url=http://www.angelfire.com/mp/memorials/mutinyzx.htm |title=Indian Mutiny Memorials: Bengal Artillery: Memorial at Delhi |publisher=[[Angelfire]] |accessdate=21 February 2016}}</ref>}}

In 1857 the [[Victoria Cross]] was only awarded to living recipients. The survivors of this action were thus honoured, Raynor, Buckley and Forrest; Willoughby did not receive the Victoria Cross because of his death a day later at the hand of villagers. In recognition of his gallantry Susan Willoughby was awarded a [[Civil List pension]] of £150 per annum for the rest of her life.<ref>Boase, Frederick (1965). "Modern English Biography  Vol III" , p.1393. Frank Cass & Co., Ltd.</ref>

In 1907, a posthumous award of the Victoria Cross was made to six men who had fought in the [[Indian rebellion of 1857]] and the [[Second Boer War]], but Willoughby was not among them. This may have been an oversight, or due to the fact that his mother had already received a generous pension, no other form of accolade then being available. One of the recipients in 1907 was Private [[Edward Spence (VC)|Edward Spence]], who "dauntlessly placed himself in an exposed position so as to cover the party carrying away the body".<ref name="ReferenceA">{{LondonGazette |issue=22268|startpage=2106|date=27 May 1859|accessdate=15 March 2014}}</ref> The body referred to was that of Willoughby's brother, Edward, killed on 15 April 1858. Spence died two days later from "the wound which he received on the occasion".<ref name="ReferenceA"/>

In 2012, efforts began by the family to present the case for a retrospective award of the Victoria Cross to Willoughby. It has so far been denied, three main reasons having been given: that it would be rewriting history; that there is no scope for retrospective action; and that the floodgates would be opened for similar claims.<ref>{{cite book |last=Wallace |first=William |year=2016 |title=The Lure of the Key |page=136 |publisher=Unicorn Publishing Group |place=London |ISBN=9781910787250 }}</ref>

==A Poem for The Delhi Nine==

THE DELHI NINE

They stood upon the ramparts, and knew that all was lost,

When they saw the dusty cloud above the approaching host,
 
Like a cornfield in the sunshine waved the flashing line of steel,

And the arid plain rang loudly with the chargers' steel-clad heel,

With murmur like the distant surge came on the swarthy foe;

Above the blood-stained banner waved, o'er craven hearts below,

Then turned they from the ramparts, to protect the magazine;

Stern were the looks, and firm the hearts of those brave men I ween,

Their pulses bounded boldly, and so boldly flashed each eye,

As those brave men of Delhi took the post where they should die,

Then out spoke gallant WILLOUGHBY unto the gallant eight,

"Let others fly, be ours’ to die, if need be, by this gate!

"Our dear ones may deplore us, but shall proudly mourn our fall,

"Our country shall remember us, and God be with us all"

No time lost they, but inwardly they prayed for aid Divine,

And with the gate shut out the world, that gallant band of nine.

Like bounding wave the traitors raved, and boomed against the wall;

Firm as a rock before the shock the nine defied them all.

But spake their guns, all thundered-tongued, and backwards reeled the foe,

As through them swept that storm of grape, and hundreds were laid low,

Loud yelled the savage traitor mob, alike with fear and hate,

As man by man the cowards ran, or fell before that gate,

Calmly the savage cry to yield our British brethren heard;

Calmly the gallant nine fired on, but answer'd not a word,

That band of heroes calmly stood, defending well that gate,

The swarthy foe around them closed, and well they met their fate.

One silent prayer to Heaven they breathed, for earth one tender sigh,

They grasp’d each other by the hand, and bravely turned to die,

Long as old England’s name is known, or spoken England’s tongue,

The gallant stand of that brave band shall by her men be sung.

Their cheek shall blanch, their eye shall flash, as o’er the sparkling wine.

They speak of that brave action of the gallant DELHI NINE!

WILLOUGHBY, RAYNOR, FORREST, SHAW, BUCKLEY,
SCULLY, CROW, EDWARDS, and STEWART.

'''By The Hon W. Wallace'''

==References==
{{reflist}}

==Further reading==
*{{cite book |first=H. M. |last=Vibart |title=Addiscombe: its heroes and men of note |place=Westminster |publisher=Archibald Constable |year=1894 |pages=547–9 |url=https://archive.org/stream/addiscombeitsher00viba#page/546/mode/2up }}

{{DEFAULTSORT:Willoughby, George}}
[[Category:British people of the Indian Rebellion of 1857]]
[[Category:1857 deaths]]
[[Category:1828 births]]
[[Category:People from Bath, Somerset]]
[[Category:Bengal Artillery officers]]
[[Category:Alumni of Addiscombe Military Seminary]]