{{Plot|date=October 2008}}
{{Unreferenced|date=June 2007}}
{{Infobox character
|series=[[24 (TV series)|24]]
|image=Sarah Wynter as Kate Warner.jpg
|caption=Sarah Wynter as Kate Warner
|name=Kate Warner
|portrayer=[[Sarah Wynter]]
|lbl1=Days
|first=[[24 (season 2)|Day 2 – Episode 1]]
|last=[[24 (season 3)|Day 3 – Episode 1]]
|data1=[[24 (season 2)|2]], [[24 (season 3)|3]]
|lbl2=Other Appearances
|data2=[[24: The Game]]
|family = Marie Warner (sister)<br>Bob Warner (father)|significantother = [[Jack Bauer]]}}

'''Kate Warner''' is a [[fictional character]] in the television series ''[[24 (TV series)|24]]''. She was [[Jack Bauer]]'s ([[Kiefer Sutherland]]) [[Romantic love|romantic interest]] in the show's [[24 (season 2)|second season]].
Kate Warner was played by [[Australia]]n actress [[Sarah Wynter]]. She, along with [[Carlos Bernard]] ([[Tony Almeida]]) and [[Michelle Forbes]] ([[Lynne Kresge]]) provided commentary for episode four of the second season's DVD set.

==Background==
Kate grew up with her [[Bob Warner (24)|father]] and younger sister [[Marie Warner (24)|Marie]]. After her mother's death, she put a considerable amount of effort into looking after her sister, an argument that she uses when she perceives ingratitude on Marie's part. She has lived in various countries during her childhood and young adulthood, including Great Britain, Saudi Arabia and Egypt.

==Appearances==

===24: Season 2===
Kate Warner is preparing for her sister, [[Marie Warner (24)|Marie Warner]]'s wedding, only to soon realize that her sister could be marrying a terrorist. Kate investigates further to find out the truth, and hears of Reza's possible connection with Second Wave. Reza attempts to convince her that he has nothing to hide, but she continues to be suspicious, often at inappropriate times (such as when Reza tries to introduce her to his cousin), sometimes sparking conflict between her and her sister. Both her sister and her father defend Reza.

Reza is questioned by CTU, and when he eventually reveals that Bob Warner made the transactions, Kate believes that he is lying to cover up his guilt. Kate investigates the transaction with a private investigator, and learns that her father works for the CIA. Before she can leave her house, however, she is abducted and taken to [[Minor characters in 24#24: Season 2 2|Syed Ali]] himself. Ali has her and her private investigator tortured right in front of her, and when it is clear that she knows nothing about him, he kills the private investigator and leaves her with a member of Second Wave. Around this time, [[Jack Bauer]], with evidence obtained by [[Nina Myers]], raids the house with a team of commandos; they save Kate, but the terrorist commits suicide with a hidden cyanide capsule.

Now realizing that Kate is the only person available who has seen Syed Ali, Jack takes her to a mosque based on Kate's tip that Ali had said "Prayer" in Arabic. Kate wears a [[niqab]] (incorrectly referred to as a [[hijab]]) to disguise her face, and enters the mosque to determine whether Ali is inside. She sees Ali, but he sees her as well. Kate then takes a position outside the mosque with Jack to intercept Ali on his way out. At this time, Jack learns that [[Marie Warner (24)|Marie Warner]] was the one responsible for the transfer of funds, and killed Reza for uncovering her secret, but does not tell Kate. Ali does not leave the mosque, so Jack and the others raid it, and find that Ali has had a body double set himself on fire while he escapes through an underground passage ... Syed is captured, and Jack Bauer sets up a video feed in which friendly arab security forces overseas grab Syed's family, with Jack Bauer declaring they will execute Syed's family on his order. After the forces and Bauer trick Syed into believing his oldest son has been executed, Syed breaks and gives the location of the bomb at Norton Airfield, to save his youngest son. Bauer, CTU, and Kate race to the airfield, where Kate's sister and two other terrorists are about to fly the bomb over downtown LA. One terrorist attempts to take off in a small airplane, only to be shot and stopped by Jack Bauer. It is found to be a diversion, and Kate locates her disguised sister at the airfield, who nearly executes her with a suppressed pistol before being shot in the arm by Jack Bauer.

Marie is restrained and tortured for information by Bauer, using the bullet lodged in her arm, but refuses to speak. Bauer orders her to be given a small dose of painkiller, and has Kate try to reason with her. Marie remains hostile, but becomes weakened when the painkiller wears off, and seems to relent, telling Kate and Jack the bomb is headed to downtown LA, and will detonate in three hours. Catching her in a lie concerning the nuke, Bauer deduces the bomb is still at the airfield, and the final terrorist is killed while guarding it--but the NEST team declares the bomb is tamper proof and they cannot disarm it. Bauer takes a suicide mission to fly the bomb safely into the desert, and Kate is distressed to see him leave, later being both relieved and baffled at how he survived and returned.

After Syed Ali confirms that the recording of his meeting with heads of state in Cyprus was fabricated, he is shot and killed by Jonathan Wallace.  Wallace agrees to give Jack evidence of this, provided Kate arranges a flight out of the country for him and accompanies him on the plane per procedure. Wallace suggests that he will have to kill Kate in order to prevent her from telling anyone about him, but Jack assures her that no harm will come to her if she follows his plan. However, some commandos attack Jack, Kate, Wallace and Yusuf and Wallace is shot- his dying words "It's inside..." convince Jack that the chip is inside his body. This suggests that Wallace never intended to truly honor his promise- by the time the terms of the agreement were fulfilled, the chip would have been out of Jack's reach. 

Yusuf notices a tracking device on the chip and removes it. He and Kate proceed to a spot where they will wait for Jack while he draws off their pursuers. However, three racist criminals attack Yusuf and steal the chip. Kate offers to give them money for the chip, but after they get the money, they plan on killing her. Before they can do so, Jack, told of Kate's location by a dying Yusuf, saves her and retrieves the chip.

Kate remains at her house until Jack asks her to pick up [[Kim Bauer]] when she is in a state of shock after killing [[Gary Matheson]]. She tells Kim that while Jack trusts few people, he seems to trust her, indicating the beginning of their relationship. Kate sees Marie one last time before she is taken to prison, telling her father that there is no explanation for her actions that he would accept. Marie warns Kate that she will not be safe on the outside. What Marie is referring to is unclear, although Kate faces danger in [[24: The Game]].  At the end of the day, Kate reunites Jack with Kim at the Coliseum crime scene.  

By the opening of the third season, it is revealed that while she and Jack had a relationship in the three years between seasons 2 and 3, it had ended. The reasons for this are never made clear, but any number of factors (probably relating to Jack's work, especially tracking Salazar undercover for a year) may have contributed.

===24: The Game===
Kate played a somewhat significant role in ''[[24: The Game]]'', mostly as a [[damsel in distress]].  She calls Jack, telling him that the same car keeps passing her house.  Later she is trapped in a subway car thanks to the earthquake.  She is saved, but by Peter Madsen who takes her hostage to stop Jack from shooting him.  Later she contacts CTU of her location, she is on Max's yacht.  After [[Max (24 character)|Max]]  uses her as a [[human shield]], Jack shoots Max and rescues Kate (she was being held hostage to force her father, Bob Warner, into letting Max take stolen [[nuclear weapons]] out of the country).

===24: Season 3===
Appearing only in the premier, Kate calls Jack to tell him he has left his coat at her house. Jack apologizes for ending their relationship on awkward terms. Jack warns [[Chase Edmunds]] that relationships are impossible for field agents, noting his breakup with Kate, which implies that the two broke up because Jack's work schedule and nature made it difficult for him to commit to Kate.

==References==
{{reflist}}

== External links ==
* [http://www.fox.com/24/profiles/season2/kw.htm Character biography] on the official [[24 (television)|24]] website

{{24 (TV series)}}

{{DEFAULTSORT:Warner, Kate}}
[[Category:24 (TV series) characters]]
[[Category:Fictional characters introduced in 2002]]

[[ja:ケイト・ワーナー]]