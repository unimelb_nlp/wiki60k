{{other uses|Debits and credits}}
{{DISPLAYTITLE:''Debit and Credit''}}
'''''Debit and Credit''''' ({{lang-de|Soll und Haben}}, 1855) is a [[novel]] in six volumes by [[Gustav Freytag]]. It was one of the most popular and widely read German novels of the 19th century.<ref>{{cite book |title=Masterpieces of World Literature in Digest Form |last=Magill |first=Frank N. |year=1960 |publisher=Harper & Row |location=New York |pages=261–263|volume=volume III}}</ref><ref>{{cite book |editor1-first=Kim |editor1-last=Vivian|title= A Concise History of German Literature to 1900. |year=1992 |publisher=Camden House, an imprint of Boydell & Brewer. |location=Rochester, New York |isbn=1-879751-29-1 |page=267. |quote=  '''Records show one lending library in Berlin acquiring 2,316 copies of Freytag's novel for its patrons between 1865 and 1898.''' |ref= |bibcode= |laysummary= |laydate= |separator= |postscript= |lastauthoramp=}}</ref>

The novel, a ''Zeitroman''<ref>{{cite book |title=The Oxford Companion to German Literature|last=Garland |first= Henry |author2=Mary Garland |year=1997 |edition=3rd |publisher=Oxford University Press |location=Oxford |isbn=0-19-815896-3 |page=784}}</ref> or "[[social novel]]", deals with interactions among broad segments of German society during the 19th century. The classes represented are the  mercantile or bourgeois class, the nobility, and the Jews:

* The bourgeois Schröter family represents Freytag's view of the ideal bourgeois type, invested in order, honesty, and solid virtue.
* The noble Rothsattel family represents the old nobility, who try to preserve their privileges in a changing society. Their struggle to avoid their impending financial ruin portrays this dynamic.
* The Jewish Ehrenthal family are money-lenders and speculators. Veitel Itzig is a criminal employee of the family.

In 1977, the novel came close to being filmed by [[Rainer Werner Fassbinder]], but after a debate about its alleged [[Anti-Semitism|anti-semitic]] content this project was abandoned.

==Plot==
After the death of his father, young Anton Wohlfart begins an apprenticeship in the office of the merchant T. O. Schröter in [[Breslau]]. Anton quickly succeeds through honest and diligent work, achieving a proper bourgeois existence. He has a variety of experiences with the Schröter family and also with the noble family of the Rothsattels. He later becomes involved with the liquidation of the estate of the Rothsattel family, an obvious symbol of the decline of the nobility and of its clash with emergent capitalist forces.

Anton has repeated interactions with two other young men, the Jew Veitel Itzig, whom he had known already in his home town, [[Ostrava]], and a young nobleman, Herr von Fink, who is a co-worker in the Schröter firm.

==Interpretation==
Anton Wohlfart is the emerging hero. As a result of his manifold experiences, he develops a sober and virtuous outlook (''[[Weltanschauung]]'').

==See also==
* [[Bildungsroman]]
* [[Zeitroman]]

==References==
{{Reflist}}

==External links==
* {{gutenberg|no=19754|name=Debit and Credit}}
* [https://archive.org/details/debitandcredit00bunsgoog ''Debit and Credit''] Archive.org

[[Category:German bildungsromans]]
[[Category:1855 novels]]
[[Category:19th-century German novels]]