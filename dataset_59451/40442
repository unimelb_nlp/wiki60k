{{Infobox professional wrestler
|name=Jeff Gaylord
|image= Jeff_Gaylord_in_Zubaz_Pants.jpg
|caption=
|birth_name = Jeff Gaylord
|names=Jeff Gaylord<br />Akeem Hassain<br />The Hood<br />The New Spoiler <br />The Black Knight
<!-- Please don't change the height or weight. These are the measures as officially stated and they should not be changed. -->
|height={{height|ft=6|in=3}}
|weight={{convert|280|lb|kg|abbr=on}}
|birth_date={{birth date and age|mf=yes|1958|10|15}}
|death_date=
|birth_place=[[Columbus, Ohio]], United States
|death_place=
|resides=[[Denver, Colorado]]
|billed=
|trainer=
|debut=1985
|retired=1997
|website=
}}
'''Jeff Gaylord''' (born October 15, 1958) is a retired American professional wrestler who competed in North American regional and independent promotions including [[Bill Watts]]' [[Universal Wrestling Federation (Bill Watts)|Universal Wrestling Federation]], [[World Class Championship Wrestling]] and the [[United States Wrestling Association]] during the late 1980s and 1990s, most notably as a frequent tag team partner of [[Jeff Jarrett]]. During the late 1990s, he was a member of [[Sid Eudy|"Psycho" Sid Vicious]]' '''The Psychos''' and, as '''Akeem Hassain''', the USWA-faction of the [[Nation of Domination]].

He also made a one-time appearance in the [[World Wrestling Entertainment|World Wrestling Federation]] as '''The Black Knight''' at the [[Survivor Series (1993)|1993 Survivor Series]].<ref name="Keith">{{cite web |url=http://www.wrestleview.com/info/faq/michaels.shtml |title=Pro Wrestling FAQ: Shawn Michaels |authors=Scott Keith, Paul Nemur and Luke Michael |year=2002 |publisher=WrestleView.com}}</ref>

==Early life==
Born in [[Columbus, Ohio]], Gaylord played as a [[defensive lineman]] for the [[Missouri Tigers football|Missouri Tigers]] while attending the [[University of Missouri]]<ref>DeArmond, Mike. ''"Grandma, aunt, mom passed on lots of fire to MU coach"''. ''Kansas City Star''. October 1, 1999</ref> and eventually became an All-American in 1981.<ref>Goforth, Alan. ''Tales from the Missouri Tigers''. Champagnie, Illinois: Sports Publishing LLC, 2003. (pg. 189) ISBN 1-58261-619-1</ref> He later borrowed the team name for his wrestling nickname.<ref>[http://castlerocknewspress.net/stories/Monument-bank-robbery-suspect-arrested,37105 "Monument bank robbery suspect arrested", by Rhonda Moore, ''Colorado Tribune'']</ref> 

==Football career==
Gaylord played high school football at [[Shawnee Mission South High School|Shawnee Mission South]] in [[Overland Park, Kansas]] before embarking on a college career at the University of Missouri. He went on to be a [[1982 NFL draft|4th round draft pick (88th overall)]] of the NFL's [[Los Angeles Rams]] in 1982. After being cut by the Rams in camp later that summer, Gaylord spent part of the 1982 season with the [[Canadian Football League|CFL]]'s [[Toronto Argonauts]] playing just four games before being released.

In 1983, he was signed as a free agent by the [[Boston Breakers (USFL)|Boston Breakers]] of the [[United States Football League]] (USFL) and played in 14 games during the 1983 season becoming the team's starting [[nose tackle]]. When the Breakers' franchise transferred to New Orleans in the fall of 1983, Gaylord played in 13 games with the Breakers in 1984 before being suspended for the last three games of the season. Later that fall, the Breakers relocated once again to Portland, Oregon, for the 1985 season, however Gaylord was traded to the [[San Antonio Gunslingers]] where he was a starting [[defensive tackle]] for the first seven games before ending his season on April 8. The USFL folded in the summer of 1986.

==Professional wrestling career==

===Universal Wrestling Federation (1985–1989)===
Making his professional debut in 1985, he began wrestling in the Mid-South area for the Universal Wrestling Federation facing Timothy Flowers, [[Tarzan Goto]] and [[Kevin Wacholz|Kevin the Magnificent]] in February 1986.<ref>{{cite web |url=http://harleyrace.com/evergreen/feb86.htm |title=February 1986 |author=Geigel, Bob |author2=Harley Race |date=2005-05-06 |publisher=HarleyRace.com}}</ref> During the summer, he also feuded with [[The Russian Team]] facing [[Ivan Koloff|Ivan]] and [[Nikita Koloff]] and Korstia Korchenko in a 6-man tag team match with [[Ken Massey]] and [[Perry Jackson]] as well as with Perry Jackson and [[Brett Woyan|Brett Wayne Sawyer]] against Korchenko and [[The Blade Runners]] on June 8. He also teamed with Ken Massey against [[Rick Steiner]] and [[Jack Victory]] at the [[Tulsa Convention Center]] in [[Tulsa, Oklahoma]] on August 17, 1987.<ref>{{cite web |url=http://www.oklafan.com/results/complete/UWF.html |title=Card Results: Universal Wrestling Federation |publisher=The Oklahoma Wrestling Fan's Resource Center}}</ref>

Facing [[Buddy Landell]] and [[David Sheldon (wrestler)|The Angel of Death]] during the next few months, he later appeared on the UWF's Superdome Extravaganza supercard defeating [[Art Crews]] at [[Louisiana Superdome|The Superdome]] in [[New Orleans, Louisiana]] on November 27, 1986.<ref>{{cite web |url=http://www.prowrestlinghistory.com/supercards/usa/misc/midsouth/cards2.html#dome1186 |title=Superdome Extravaganza 11/86 |publisher=ProWrestlingHistory.com}}</ref> He would later lose to [[One Man Gang]] in the opening rounds of the UWF/PWI Tournament the following month. During the next year, he would face [[Eli the Eliminator]] and, with Jeff Raitz, against [[Sting (wrestler)|Sting]] and [[Rick Steiner]]. He would later lose to Rick Steiner in a singles match at the [[Sam Houston Coliseum]] on January 23, 1987.<ref>{{cite web |url=http://www.oklafan.com/results/complete/UWF.html |title=Card Results: Universal Wrestling Federation}}</ref>

During 1989, Gaylord would also appear on ''[[WWF Wrestling Challenge]]'' teaming with [[Tim Horner]] against [[The Fabulous Rougeaus]] on April 16, before returning to World Class Championship Wrestling facing veterans such as [[Jimmy Jack Funk]] and [[Kerry Von Erich]].<ref>{{cite web |url=http://www.oldschool-wrestling.com/geeklog/article.php/2006101422152640/print |title=WCCW Results 1989 |date=2006-10-14 |publisher=Oldschool-Wrestling.com |archiveurl=https://web.archive.org/web/20061029194302/http://www.oldschool-wrestling.com/geeklog/article.php/2006101422152640/print |archivedate=2006-10-29 |quote= }}</ref>

===World Class Championship Wrestling (1989–1991)===
Wrestling for [[World Class Championship Wrestling]] as the masked wrestler '''The Hood''' during the late 1980s,<ref>{{cite web |url=http://www.worldclasswrestling.info/wccwfaq.htm#masked |title=WCCW Frequently Asked Questions: Who was (insert masked wrestler's name here)? |accessdate=2007-04-03 |author=Dananay, John |authorlink= |author2=[[John Nord]] |publisher=World Class Memories |archiveurl = https://web.archive.org/web/20070505144149/http://www.worldclasswrestling.info/wccwfaq.htm#masked |archivedate=2007-05-05 |quote= }}</ref> he would become a mainstay in the area and briefly feuded with [[Jeff Jarrett]] and [[Bill Dundee]] later teaming with [[Stone Cold Steve Austin|"Stunning" Steve Austin]], Gary Young and [[Skandor Akbar]] in an 8-man Texas Tornado match losing to Jeff Jarrett, Bill Dundee, [[Eric Embry]] and [[Percy Pringle]] in early 1990.<ref>{{cite web |url=http://www.411mania.com/wrestling/video_reviews/33176 |title=Down With The Brown: Steve Austin & Mick Foley – The Early Years |author=Brown, Sydney |date=2004-08-22 |publisher=411wrestling.com}}</ref>

However, he would soon begin teaming his former rival and defeated [[Brian Lee (wrestler)|Brian Lee]] and Chuck Casey for the USWA Tag Team Championship on September 3, 1990. Feuding with Brian Lee and [[Don Harris (wrestler)|Don Harris]] over the titles, they would trade the tag team titles twice with Lee and Harris before finally losing the titles to [[Tony Anthony (wrestler)|Tony Anthony]] and [[Doug Gilbert]] on October 6. Participating in the USWA Heavyweight Championship Tournament two days later, he defeated Doug Gilbert in the opening rounds before losing to [[Dick Slater|"Dirty" Dick Slater]] in the semi-finals on October 8.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1990.html |title=Mid-South Coliseum 1990 (Jarrett) |publisher=ProWrestlingHistory.com}}</ref>

Defeating [[Matt Osborne|Matt Borne]] and King Cobra in January 1991, Gaylord briefly appeared in the [[Global Wrestling Federation]]. He participated in the GWF World Television Championship Tournament losing to [[Lester Speight|Rasta the Voodoo Man]] in June 1991.<ref>{{cite web |url=http://www.prowrestlinghistory.com/supercards/usa/misc/gwf.html#tv |title=GWF TV Title Tournament 1991 |publisher=ProWrestlingHistory.com}}</ref> During the next two months, he would team with the Blue Avenger in the GWF World Tag Team Championship Tournament being eliminated by [[Bill Irwin]] and [[John Laurinaitis|Johnny Ace]]<ref>{{cite web |url=http://www.prowrestlinghistory.com/supercards/usa/misc/gwf.html#tag |title=GWF Tag Team Title Tournament 1991 |publisher=ProWrestlingHistory.com}}</ref> and, during the GWF North American Championship Tournament, lost to [[John Tatum (wrestler)|John Tatum]] in the opening rounds.<ref>{{cite web |url=http://www.prowrestlinghistory.com/supercards/usa/misc/gwf.html#na |title=GWF North American Title Tournament 1991 |publisher=ProWrestlingHistory.com}}</ref>

During this time, he was involved in an altercation with [[Eddie Gilbert (wrestler)|Eddie Gilbert]] at a Dallas wrestling event in which Gaylord allegedly "sucker-punched" Gilbert during an argument in Gilbert's dressing room at the [[Dallas Sportatorium]]. The two began fighting, until it was broken up by Doug Gilbert who assaulted Gaylord with a [[Coke bottle]]. Although Gaylord had been speaking to Gilbert regarding the possibility of being booked in Gilbert's promotion, Gilbert claimed in a later [[shoot interview]] that Gaylord was paid $1,000 by a Northeastern promoter to assault him after Gilbert had failed to show up for an event.<ref>{{cite web |url=http://www.eddiegilbert.com/jamesbeard.html |title="Hot Stuff" Eddie Gilbert |accessdate= |author=Beard, James |publisher=EddieGilbert.com}}</ref>

===United States Wrestling Association (1991–1995)===
Returning to the Memphis area later that year, he lost to USWA Heavyweight Champion Jerry "The King" Lawler by disqualification on November 4 and the following week, teamed with The Big O fighting to a double disqualification against Jerry Lawler and Bill Dundee on November 11. After losing to Bill Dundee and [[Randy Lewis (wrestler)|The Spirit of America]] on November 18, Gaylord was absent from the promotion for several weeks before returning to face Jerry Lawler, fighting him several times during an event on December 28, 1991.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1991.html |title=Mid-South Coliseum 1991 (Jarrett) |publisher=ProWrestlingHistory.com}}</ref>

Defeating [[Brad Armstrong (wrestler)|The Candyman]] on January 6, he spent several months in the [[Puerto Rico]]-based Americas Wrestling Federation winning the AWF International Tag Team titles with Sunny Beach before returning later that year teaming with [[Curtis Hughes|Mr. Hughes]] to defeat Jerry Lawler and Jeff Jarrett on September 21 and against Tony Williams and [[Robert Gibson (wrestler)|Robert Gibson]] on October 5. Returning to singles competition, he also defeated Tony Falk and Randy Rhodes before losing to Bill Dundee on October 26, 1992.

After he defeated Tony Williams in a rematch on November 2, he wrestled two matches in one night defeating [[Bart Sawyer]] and later teamed with USWA Southern Heavyweight Champion [[Brian Christopher]] losing to The American Eagles (Bill Dundee and Danny Davis) on November 9, 1992.

Suffering several losses to The American Eagles during the next few weeks, Gaylord teamed with Brian Christopher and The Masters of Terror ([[Ken Wayne]] and Ken Raper) in an 8-man tag-team match defeating The American Eagles, Danny Davis and Bill Dundee and later the USWA Tag Team Champions [[The Moondogs (professional wrestling)|The Moondogs]] ([[Moondog Spot]] and [[Moondog Spike]]) by disqualification on November 30. Losing to The Moondogs in a tag team match with The Star Rider, he also lost to Tony DeNucci by disqualification on December 14.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1992.html |title=Mid-South Coliseum 1992 (Jarrett) |publisher=ProWrestlingHistory.com}}</ref>

In January 1993, he lost singles matches to Tony DeNucci, Danny Davis and Scott Campione. Although defeating Danny Davis in a rematch on February 1, he later teamed with [[Doink the Clown]] in a losing effort against USWA Heavyweight Champion Jerry Lawler and USWA Southern Heavyweight Champion Jeff Jarrett on February 8. He would appear on several house shows for the World Wrestling Federation wrestling Jerry Lawler at several house shows in early 1993.

Losing to [[Tom Prichard]] later that month, he later feuded with [[Don Bass (wrestler)|The Rock-N-Roll Phantom]] defeating him and [[Bryan Clark|The Nightstalker]] several times in April. Defeating [[Raven (wrestler)|Johnny Polo]] on May 10, he and Jarrett reunited to defeat [[Well Dunn]] ([[Timothy Well]] and [[Steven Dunn]]) by disqualification during the same event.

Feuding with [[C.W. Bergstrom]] during the next three months, Gaylord defeated Bergstrom in a "Strap On A Pole Match" on May 24 and again in a rematch on June 7 before losing to him on June 14. Losing to Bo Alexander and Tony Falk during the next several weeks, he was again defeated by C.W. Bergstrom on July 5, 1993.

Defeating Johnny Polo, The Hawk and Colin Scott during the summer, he later defeated Tony Falk in a rematch and, later during the event, teamed with [[Jacqueline Moore|Miss Texas]] and Ken Wayne in a mixed 6-man tag team match defeating [[PG-13 (professional wrestling)|PG-13]] and The Black Pussycat on September 6, 1993.

Defeating [[Wolfie D]] on September 13, although suffering a loss to Colin Scott on September 20, he also defeated Leon Downs and Jim Dodson before teaming with Mike Anthony to lose to The Moondogs ([[Moondog Spike]] and [[Cousin Junior|Moondog Cujo]]) on October 18. Entering the USWA Tag Team Championship Tournament several days later, he and Anthony again lost to The Moondogs on October 25.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/tournaments/uswatag.html#1093 |title=USWA Tag Title Tournament 1993 |publisher=ProWrestlingHistory.com}}</ref>

Defeating Paul Neighbors by disqualification, he later teamed with Mike Anthony and [[Harry Del Rios|Del Rios]] in a 6-man tag team match defeating Paul Neighbors, Reggie B. Fine and [[Glenn Jacobs|Doomsday]] on November 8, 1993. He and Anthony would also defeat [[Phi Delta Slam]] and PG-13 several times later that month.

On November 24, he would also make a PPV appearance at the WWF's [[Survivor Series (1993)|1993 Survivor Series]] as '''The Black Knight''' along with [[Barry Horowitz|The Red Knight]] and [[Greg Valentine|The Blue Knight]]<ref>{{cite web |url=http://www.softwolves.pp.se/wrestling/wwf/substitutions#y93 |title=World Wrestling Entertainment Substitutions |author=Gaworecki, David |date=2005-04-14 |publisher=American Wrestling Trivia}}</ref> against the [[Hart family]]<ref>[[Dave Meltzer|Meltzer, Dave]]. ''Tributes II: Remembering More of the World's Greatest Wrestlers''. Champaigne, Illinois: Sports Publishing LLC, 2004. (pg. 10) ISBN 1-58261-817-8</ref> and was later pinned by [[Owen Hart]], becoming the first man eliminated.<ref>{{cite web |url=http://www.softwolves.pp.se/wrestling/wwf/1993#sur93 |title=Survivor Series 1993 |accessdate= |author=Karlsson, Peter |authorlink= |date=2005-04-10 |format= |work= |publisher=American Wrestling Trivia}}</ref> Although originally scheduled to be captained by Jerry Lawler, [[Shawn Michaels]] was named as a last minute replacement due to Lawler's legal problems preventing him from appearing with the WWF.<ref name="Keith"/>

Returning to the USWA, he and Mike Anthony defeated PG-13 for the USWA Tag Team Championship on November 29 although they would lose the titles a week later to The War Machines on December 6, 1993.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1993.html |title=Mid-South Coliseum 1993 (Jarrett) |publisher=ProWrestlingHistory.com}}</ref>

Losing to Doug Gilbert on January 3, he managed to defeat Del Rios however he later lost to [[Skull Von Crush]] on January 24 and fought to a time limit draw with Reggie B. Fine on January 31.

After a loss to [[Koko B. Ware]] on February 7, he teamed with Spellbinder and King Cobra defeating Skull Von Crush and The Nightmares at the supercard ''Memphis Memories'' on March 7 and later defeated Skull Von Crush on March 14. Teaming with [[Robert Gibson (wrestler)|Robert Gibson]], Gaylord lost to Well Dunn on March 21 and, defeating Ken Wayne by disqualification on March 28, he teamed with [[Ricky Morton]] and Spellbinder to defeat Ken Wayne, Skull Von Crush and Leon Downs in a 6-man tag team match later that night.

Defeating The Moondogs by disqualification in a tag team match with [[Don Bass (wrestler)|Don Bass]] on April 4, he and Spellbinder later lost to [[The Bad Breed]] ([[Ian Rotten]] and [[Axl Rotten]]) on April 11 and to [[The Eliminators]] during the USWA Tag Team Championship Tournament on April 23.  Losing to [[Randy Colley|Moondog Rex]] on May 2, he and the Spellbinder began feuding after losing to The Eliminators on June 6 and faced each other in several singles matches during the month. Defeating [[Bull Pain]], Leon Downs and Tony Falk in early July, he won a battle royal on July 25 before losing to [[Sid Eudy|Sid Vicious]] later that night. He would later team with [[King Kong Bundy]] losing to Sid Vicious and Spike Huber on August 1, 1994.

Continuing his feud with Spellbinder as well as Spike Huber during the next several weeks, he eventually lost to Spellbinder on September 19. After being eliminated by [[Perry Saturn]] in the USWA Heavyweight Championship Tournament on October 3, he would also defeat Tony Williams before losing to Spellbinder on October 17, 1994.

Feuding with The Phantoms during late 1994, he defeated Phantom Sorrow on November 14 and, with King Cobra and Miss Texas, defeated [[Fantasia (wrestler)|Fantasia]] and The Phantoms in a mixed 6-man tag team match on November 21.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1994.html |title=Mid-South Coliseum 1994 (Jarrett) |publisher=ProWrestlingHistory.com}}</ref>

Remaining with the promotion during its last years, he lost to The Gambler by disqualification in his last appearance on May 15, 1995.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1995.html#051595 |title=Mid-South Coliseum 1995 (Jarrett) |publisher=ProWrestlingHistory.com}}</ref>

===Later career (1995–1997)===
After leaving the USWA, Gaylord began wrestling for the [[American Wrestling Federation]] and later teamed with [[The Warlord (wrestler)|The Warlord]] fighting to a double disqualification with [[Hercules (wrestler)|Hercules Hernandez]] and Mr. Hughes. This would later allow several other teams to advance to the second round including [[Tommy Rich|"Wildfire" Tommy Rich]] and [[Greg Valentine|Greg "The Hammer" Valentine]] eventually going on to defeat [[Koko B. Ware]] and [[Tony Atlas|"Mr. USA" Tony Atlas]] in the tournament finals.<ref>{{cite web |url=http://www.prowrestlinghistory.com/supercards/usa/misc/awf.html#awftag |title=AWF Tag Team Title Tournament 1995 |publisher=ProWrestlingHistory.com}}</ref>

==Criminal convictions==
In October 2001, Gaylord robbed a bank in [[Aurora, Colorado]] of five thousand dollars and was able to get away.{{Citation needed|date=August 2009}} Then, in February, he robbed the same bank branch; this time his license plate number was written down and a high-speed chase resulted.{{Citation needed|date=August 2009}} The officer who pursued him ended up breaking his foot during the chase. Gaylord eventually lost control of his vehicle and was taken into custody. He pleaded guilty to two counts of bank robbery and was sentenced to two consecutive terms of seventy-eight months.<ref>{{cite web |url=http://ca10.washburnlaw.edu/cases/2003/04/02-1313.htm |title=U.S. v. Jeffery S. Gaylord |accessdate=June 27, 2009 |author=Judge Carlos F. Lucero |date=1 April 2003 |publisher=U.S. Court of Appeals, 10th Circuit}}</ref> Gaylord was released into supervised probation in 2008.

In January 2009, Gaylord, was arrested in connection with the bank robbery at a [[US Bank]] in [[Monument, Colorado]] as well as a robbery and attempted robbery at a branch in [[Castle Rock, Colorado|Castle Rock]]. In 2009, he was sentenced to six years in prison, after which he will be required to serve three years of supervised probation.

==In wrestling==
*'''[[Manager (professional wrestling)|Managers]]'''
**[[Bruno Lauer|Downtown Bruno]]<ref>{{cite web|url=http://www.onlineworldofwrestling.com/profiles/h/harvey-wippleman.html|title=Bruno Lauer's profile|publisher=Online World of Wrestling|accessdate=2009-08-03}}</ref>

==Championships and accomplishments==
*'''American Wrestling Federation'''
**AWF International Tag Team Championship (1 time) – with Sunny Beach <ref>http://www.solie.org/titlehistories/ittawfp.html</ref>
*'''[[United States Wrestling Association]]'''
**[[USWA World Tag Team Championship]] ([[USWA World Tag Team Championship#Title history|2 times]]) – with [[Jeff Jarrett]]<ref>{{cite web |url=http://www.wrestling-titles.com/us/tn/uswa/uswa-t.html |title=U.S.W.A. World Tag Team Title |accessdate= |year=2003 |publisher=Puroresu Dojo}}</ref>
*'''[[Pro Wrestling Illustrated]]'''
**PWI ranked him # '''83''' of the 500 best singles wrestlers of the [[PWI 500]] in 1991.

==References==
{{Reflist|3}}

==External links==
{{Portal|Professional wrestling}}
* [http://www.onlineworldofwrestling.com/profiles/j/jeff-gaylord.html Profile at Online World of Wrestling]
* [http://www.bodyslamming.com/other/jeffgaylord.html Other Superstars – Jeff Gaylord]
* [http://www.cagematch.net/?id=2&nr=4127&worker=Jeff+Gaylord CageMatch.de – Jeff Gaylord] {{de icon}}
* [http://www.nfl.com/player/jeffgaylord/2514729/profile NFL.com profile]

{{DEFAULTSORT:Gaylord, Jeff}}
[[Category:1958 births]]
[[Category:Living people]]
[[Category:American male professional wrestlers]]
[[Category:Professional wrestlers from Ohio]]
[[Category:University of Missouri alumni]]
[[Category:Los Angeles Rams players]]
[[Category:Toronto Argonauts players]]
[[Category:San Antonio Gunslingers players]]
[[Category:Boston/New Orleans/Portland Breakers players]]