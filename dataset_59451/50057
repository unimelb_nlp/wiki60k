{{EngvarB|date=January 2017}}
{{Use dmy dates|date=January 2017}}
{{Infobox person
| name        = Bella Ramsey
| image       =
| caption     =
| birth_date  = 2003
| birth_place = 
| education   = 
| occupation  = Actress
| birthname   =
| yearsactive = 2016–present
| homepage    =
}}
'''Bella Ramsey''' (born 2003)<ref>{{cite web|last1=Morton|first1=Ashley|title=We Know No Scene-Stealer but the One on Bear Island, Whose Name Is Bella Ramsey|url=http://www.makinggameofthrones.com/production-diary/bella-ramsey-lyanna-mormont|website=Making Game of Thrones|publisher=HBO|accessdate=29 June 2016}}</ref><ref name="news">http://m.independent.ie/entertainment/movies/movie-news/teenage-actress-ramsey-has-magic-touch-as-the-worst-witch-35318008.html</ref> is a British child actress. She made her professional acting debut as the young noblewoman [[Lyanna Mormont]] in the [[HBO]] fantasy television series ''[[Game of Thrones]]''.<ref name="The Telegraph 8 June 2016">{{cite news|last1=Hawkes|first1=Rebecca|title=Game of Thrones: who is Lyanna Mormont, the 10 year-old taking the show by storm?|url=http://www.telegraph.co.uk/tv/2016/06/07/game-of-throness-coolest-pre-teen-who-is-lyanna-mormont/|accessdate=27 June 2016|work=The Telegraph|date=8 June 2016}}</ref>

==Career==
Since 2016, Ramsey has played [[Lyanna Mormont]] in the [[HBO]] fantasy drama television series ''[[Game of Thrones]]''. The part of Lyanna Mormont is Ramsey's first credited role. After her debut appearance in one scene in the [[The Broken Man|seventh episode]] of the [[Game of Thrones (season 6)|sixth season]] of ''Game of Thrones'', fans and critics noted Ramsey as a standout actress for portraying her character's no-nonsense leadership style.<ref name="The Telegraph 8 June 2016" /><ref>{{cite news|last1=Hibberd|first1=James|title='Game of Thrones': Your new favorite character is 10 years old|url=http://www.ew.com/article/2016/06/05/game-thrones-lyanna-mormont|work=Entertainment Weekly|accessdate=27 June 2016|date=6 June 2016}}</ref> This reaction was repeated after her appearance in [[The Winds of Winter (Game of Thrones)|the season finale]],<ref>{{cite news|last1=Delgado|first1=Kasie|title=Game of Thrones just discovered an incredible new child actor|url=http://www.radiotimes.com/news/2016-06-27/game-of-thrones-just-discovered-an-incredible-new-child-actor|accessdate=27 June 2016|work=Radio Times|date=27 June 2016}}</ref> with ''[[The Hollywood Reporter]]'' calling her "season 6's breakout star".<ref name="The Hollywood Reporter 27 June 2016">{{cite news|last1=Wigler|first1=Josh|title='Game of Thrones': A Salute to Season 6's Breakout Star, Lyanna Mormont|url=http://www.hollywoodreporter.com/live-feed/game-thrones-lady-bear-island-906516|accessdate=27 June 2016|work=The Hollywood Reporter|date=27 June 2016}}</ref> She is set to return for the [[Game of Thrones (season 7)|seventh season]].<ref>{{cite web|url=https://www.instagram.com/p/BK1M1JQAskO/|title=LOOK OUT FOR THIS ONE! Bella Ramsey portrays Lyanna Mormont with such a strength, authority and coolness! She also stars in a upcomming BBC/ Netflix show called The Worst Witch! She's super cool and a highly professional actor in the age of 11! Can't tell you if our characters meet or not this season – but I can tell you this: you have SO much to look forward to in season 7! Hallelujaaaaa!|work=Instagram|date=26 September 2016|accessdate=27 September 2016}}</ref>

Ramsey is also set to appear in the [[The Worst Witch (2017 TV series)|2017 TV adaptation]] of the ''[[The Worst Witch]]'' books as the title character, [[Mildred Hubble]].<ref name="The Telegraph 8 June 2016" />

==Filmography==
===Television===
{| class="wikitable plainrowheaders sortable"
!scope="col"| Year
!scope="col"| Title
!scope="col"| Role
!scope="col"| Network
!scope="col" class="unsortable" | Notes
!scope="col" class="unsortable" | {{abbr|Ref(s)|Reference(s)}}
|-
|2016–present
!scope=row|''[[Game of Thrones]]''
|{{sort|Mormont|[[Lyanna Mormont]]}}
|[[HBO]]
|3 episodes
|style="text-align:center;"|<ref name="The Telegraph 8 June 2016" />
|-
|2017-present
!scope=row|''[[The Worst Witch (2017 TV series)|The Worst Witch]]''
|{{sort|Hubble|[[Mildred Hubble]]}}
|[[CBBC]]/[[ZDF]]
|13 episodes
|style="text-align:center;"|<ref name="The Telegraph 8 June 2016" />
|}
{| class="wikitable"
|+Key
| style="background:#FFFFCC;"| {{dagger|alt=Shows that have not yet been released}}
|Denotes shows that have not yet been released
|}

==References==
{{reflist|30em}}

==External links==
* {{Official website|www.bellaramsey.com}}
*{{IMDb name|8165602|Bella Ramsey}}

{{DEFAULTSORT:Ramsey, Bella}}
[[Category:Living people]]
[[Category:2003 births]]
[[Category:British television actresses]]
[[Category:British child actresses]]
[[Category:21st-century British actresses]]

{{UK-actor-stub}}