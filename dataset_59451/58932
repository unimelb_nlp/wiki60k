{{Infobox journal
| title = Mini-Reviews in Medicinal Chemistry
| cover = 
| editors = Atta-ur-Rahman, M. Iqbal Choudhary, George Perry
| discipline = [[Medicinal chemistry]]
| formernames = 
| abbreviation = Mini. Rev. Med. Chem.
| publisher = [[Bentham Science Publishers]]
| country =
| frequency = Monthly
| history = 2001–present
| openaccess =
| license =
| impact = 2.903
| impact-year = 2014
| website = http://benthamscience.com/journal/index.php?journalID=mrmc
| link1 = http://benthamscience.com/journal/contents-abstracts.php?journalID=mrmc#top
| link1-name = Online access
| link2 = http://benthamscience.com/journal/contents-abstracts.php?journalID=mrmc#top
| link2-name = Online archive
| OCLC = 615044453
| LCCN =
| CODEN = MMCIAE
| ISSN = 1389-5575
| eISSN = 1875-5607
}}
'''''Mini-Reviews in Medicinal Chemistry''''' is a monthly [[peer-reviewed]] [[medical journal]] covering all aspects of [[medicinal chemistry]]. It is published by [[Bentham Science Publishers]] and the [[editors-in-chief]] are Atta-ur-Rahman ([[University of Cambridge]]), M. Iqbal Choudhary ([[University of Karachi]]), and George Perry ([[University of Texas at San Antonio]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Biochemistry & Biophysics Citation Index]]
* [[Biosis Previews]]
* [[Chemical Abstracts Service|Chemical Abstracts Service/CASSI]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[Embase|Embase/Excerpta Medica]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.903, ranking it 21st of 59 journals in the category "Chemistry, Medicinal".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry, Medicinal |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links==
*{{Official website|1=http://benthamscience.com/journal/index.php?journalID=mrmc}}

[[Category:Medicinal chemistry journals]]
[[Category:Bentham Science Publishers academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2001]]
[[Category:Monthly journals]]