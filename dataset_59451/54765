"'''The Duc de L'Omelette'''" is a humorous [[short story]] by American writer [[Edgar Allan Poe]]. It was first published in the Philadelphia ''Saturday Courier'' on March 3, 1832, and was subsequently revised a number of times by the author.

==Plot summary==
The Duc de L'Omelette dies while dining on an [[Ortolan bunting|ortolan]], and finds himself in [[hell]]: an apartment filled with various works of art that has a window overlooking a fiery landscape. Face to face with [[Satan]], the Duc manages to avoid damnation by cheating him at cards.<ref>{{Cite book|title = The Poe Encyclopedia|last = Frank|first = Frederick S.|publisher = Greenwood Press|year = 1997|isbn = 0313277680|location = Westport, CT|pages = 107–8|last2 = Magistrale|first2 = Tony}}</ref>

==Publication history==
Poe originally titled the story "The Duke of L'Omelette" when it was published in the March 3, 1832, issue of the Philadelphia ''Saturday Courier''.<ref>{{Cite book|title = Edgar Allan Poe: A to Z|last = Sova|first = Dawn B.|publisher = Checkmark Books|year = 2001|isbn = 0-8160-4161-X|location = New York, NY|pages = 73}}</ref> It was one of four comedic tales Poe published anonymously in that newspaper that year, along with "A Tale of Jerusalem", "A Decided Loss" (later renamed "Loss of Breath"), and "The Bargain Lost" (later renamed "[[Bon-Bon (short story)|Bon-Bon]]").<ref>{{Cite book|title = Edgar A. Poe: Mournful and Never-ending Remembrance | last = Silverman|first = Kenneth|publisher = HarperPerennial|year = 1991|isbn = 978-0-06-092331-0|location = New York, NY|pages = 88–89}}</ref> The ''Saturday Courier'' had previously published Poe's "[[Metzengerstein]]" in January 1832;<ref>{{Cite book|title = Edgar A. Poe: Mournful and Never-ending Remembrance | last = Silverman|first = Kenneth|publisher = HarperPerennial|year = 1991|isbn = 978-0-06-092331-0|location = New York, NY|pages = 88}}</ref> it was the author's first prose work to appear in print.<ref>{{Cite book|title = Edgar Allan Poe: A to Z|last = Sova|first = Dawn B.|publisher = Checkmark Books|year = 2001|isbn = 0-8160-4161-X|location = New York, NY|pages = 155}}</ref> The five stories were submitted to the ''Saturday Courier'' as entries to a writing contest.<ref>{{Cite book|title = A Dream Within a Dream: The Life of Edgar Allan Poe | last = Barnes|first = Nigel|publisher = Peter Owen Publishers |year = 2009|isbn = 978-0-7206-1322-3|location = London|pages = 77}}</ref> Though the winner of the $50 prize was [[Delia Bacon]],<ref>{{Cite book|title = Edgar Allan Poe | last = Asselineau|first = Roger|publisher = University of Minnesota Press |year = 1970 |isbn = 0-8166-0561-0|location = |pages = 9}}</ref> the editors published Poe's submissions anyway; he was likely never compensated.<ref>{{Cite book|title = Edgar Allan Poe: A Critical Biography | last = Quinn|first = Arthur Hobson|publisher = Johns Hopkins University Press |year = 1998|isbn = 0-8018-5730-9|location = Baltimore, MD|pages = 192}}</ref>

==Analysis==
The story is intended as a [[satire]] on the works of [[Nathaniel Parker Willis]].<ref>{{Cite book|title = The Annotated Tales of Edgar Allan Poe|last = Peithman|first = Stephen|publisher = Avenel Books|year = 1981|isbn = 0517615312|location = New York|pages = 329}}</ref>

The epigraph is from [[William Cowper]]'s poem "The Task", and reads: "And stepped at once into a cooler clime."

==References==
{{Reflist}}

==External links==
*{{wikisource-inline|single=true}}
* {{librivox book | title=The Works of Edgar Allan Poe, Raven Edition, Volume 4 | author=Edgar Allan POE}}

{{Edgar Allan Poe}}

{{DEFAULTSORT:Duc de L'Omelette}}
[[Category:1832 short stories]]
[[Category:Satirical works]]
[[Category:Short stories by Edgar Allan Poe]]

{{Story-stub}}