{{ infobox bibliographic database
| title       = BIOSIS Previews
| image       = 
| caption     = 
| producer    = [[Thomson Reuters]]
| country     = [[United States]]
| history     = 
| languages   = 
| providers   = [[Web of Science]]
| cost        = 
| disciplines = Science
| depth       = Abstract & citation indexing 
| formats     = 
| temporal    = 
| geospatial  = 
| number      = 
| updates     = 
| p_title     = 
| p_dates     = 
| ISSN        = 
| web         = http://thomsonreuters.com/biosis-previews/
| titles      = http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=BP
}}

'''BIOSIS Previews''' is an English-language, [[bibliographic]] database service, with abstracts and [[citation indexing]]. It is part of ''[[Thomson Reuters]] [[Web of Knowledge]]'' suite. Content that was originally integrated from the '''BIOSIS''' company before the merger in 2004 is now part of the Web of Knowledge. BIOSIS Previews indexes data from 1926 to the present.<ref name=coverage/><ref name=Dialog-biosis/>

BIOSIS Previews is part of the ''Life Sciences'' in the Web of Knowledge. Its coverage encompasses the  [[life sciences]] literature and [[biomedical sciences]] literature, with deep, global, coverage on a wide range of subject areas. This is accomplished with access to indexed journal content from ''[[Biological Abstracts]]'', and supplemental, indexed,  non-journal content from ''[[Biological Abstracts/RRM|Biological Abstracts/Reports, Reviews, Meetings]]'' or ''(BA/RRM)'' or ''(Biological Abstracts/RRM)'' and the major publications of BIOSIS.  This coverage includes literature in pre-clinical and experimental research, methods and instrumentation, animal studies, environmental and consumer issues, and other areas.<ref name=coverage>
{{Cite web| last =Biosis Previews| title =Life Sciences in the Web of Knowledge| publisher =[[Thomson Reuters]]| year =2010| url =http://wokinfo.com/products_tools/specialized/bp/ 
  | accessdate =2010-06-27}}</ref><ref name=Dialog-biosis/><ref name=factPDF/> 

''Biological Abstracts'' consists of 350,000 references for almost 5,000 primary journal and monograph titles. ''Biological Abstracts/RRM'' additionally includes more than 200,000 ''non-journal citations''.

''Biological Abstracts/RRM'' is the former '''''BioResearch Index'''''.<ref name=Dialog-biosis>{{Cite web
  | title =BIOSIS Previews 
  | work = 
  | publisher =Dialog bluesheets 
  | date =October 15, 2007 
  | url =http://library.dialog.com/bluesheets/html/bl0005.html#TC 
  | format = 
  | accessdate =2010-07-16}}{{fails verification|date=September 2016}}</ref>

==Overview==
Acceptable content for the ''Web of Knowledge'', and ''BIOSIS previews'' is determined by an evaluation and selection process based on the following criteria: impact, influence, timeliness, peer review, and geographic representation.<ref name=factPDF/><ref name=Wht-inc/>

''BIOSIS Previews'' covers 5,000 peer reviewed journals. Moreover,  non-journal coverage includes coverage of meetings, meeting abstracts, conferences, literature reviews, U.S. patents, books, software, book chapters, notes, letters, and selected reports and. Furthermore, the non-journal coverage is in relevant disciplines from [[botany]] to [[microbiology]] to [[pharmacology]]. Moreover, this database contains more than 18 million records, more than 500,000 records are added each year, and backfiles are available from 1926 to the present day. Specialized indexing has been developed.  This has increased the accuracy of retrieval.  Taxonomic data and terms, enhanced disease terms, sequenced databank numbers, and a conceptually controlled vocabulary go back to 1969.<ref name=Dialog-biosis/><ref name=factPDF>
{{Cite web| title =Biosis Life Sciences database| publisher =[[Thomson Reuters]]| year = 2010
  | url =http://wokinfo.com/media/pdf/BIOSIS_FS.pdf| accessdate =2010-06-27 }}</ref><ref name=Wht-inc>Products A - Z BIOSIS Previews. [http://thomsonreuters.com/products_services/science/science_products/a-z/biosis_previews What's Included]. Thomson Reuters.2010.</ref><ref name=Ovid-Pre/>

Some U.S. patents are also part of ''BIOSIS Previews'' archives from 1926 to 1968, from 1986 to 1989, and from 1994 to present. Archived data is the electronic formatted content of the print ''[[Biological Abstracts]]'' volumes 1-49.<ref name=Dialog-biosis/>

===Print counterparts===
The print counterparts for this bibliographic index are:<ref name=Dialog-biosis/>
 
*Biological Abstracts
*Biological Abstracts/RRM
*BioResearch Index

==Topic coverage==
The subject areas covered is broad and [[interdisciplinary]]. Content is available from all  [[life sciences]] discipline. This  includes traditional [[biology]] ([[botany]], [[ecology]], [[zoology]]), and interdisciplinary subjects ([[biochemistry]], [[biomedicine]], and [[biotechnology]]). Related literature from the Earth & Geological Sciences, i. e.  [[agriculture]], botany, ecology, [[genetics]] and [[genomics]] are also indexed. Literature is also available for topics related to the life sciences coverage such as instrumentation and methods.<ref name=Ovid-Pre>
{{Cite web
 |title=BIOSIS Previews - Source: Thomson Scientific, Inc. 
 |publisher=Ovid Technologies 
 |year=2010 
 |url=http://www.ovid.com/site/catalog/DataBase/26.jsp?top=2&mid=3&bottom=7&subsection=10 
 |format=BIOSIS is now BIOSIS Previews 
 |accessdate=2010-06-27 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20100509145243/http://www.ovid.com:80/site/catalog/DataBase/26.jsp?top=2&mid=3&bottom=7&subsection=10 
 |archivedate=2010-05-09 
 |df= 
}}</ref>

==History==
In 1926, the [[Society of American Bacteriologists]] and the [[Botanical Society of America]] acknowledging the need for greater integration of life science information agreed to merge their two publications, ''Abstracts of Bacteriology''<ref>''Abstracts of Bacteriology'' began publication in 1917 under the editorship of A. Parker Hitchens, as an outgrowth of the ''Journal of Bacteriology''. {{OCLC|1460582}}</ref> and ''Botanical Abstracts'',<ref>''Botanical Abstracts'' was published under the direction of the Board of Control of Botanical Abstracts, Inc. During its publication it had two editors: September 1918–March 1921, B.E. Livingston and April 1921–November 1926, J.R. Schramm; at which point it was merged into ''Biological Abstracts''. {{OCLC|1536892}}</ref> thus creating ''Biological Abstracts''.  A not-for-profit company was formed to administer the publication on a financially sound basis. In 1964, the company's name was changed to ''BioSciences Information Service of Biological Abstracts (BIOSIS)''.<ref name="Kennedy-309">Kennedy, H. E. (1988) "BIOSIS to the Rescue" ''BioScience'' 38(5):  pp. 309–310</ref><ref name=Steere>Steere, William Campbell; Parkins, Phyllis V. and Philson, Hazel A. (1976) ''Biological Abstracts/BIOSIS, The First Fifty Years, The Evolution of a Major Science Information Service'' New York Botanical Garden, Plenum Press, New York, page ''v'', ISBN 0-306-30915-7</ref><ref>Trumbull, Richard (1976) "BIOSIS: 50 Years of Biological Abstracts" ''BioScience'' 26(5): p. 307</ref>  In addition to its indexing and abstracting service, it published ''[[The Zoological Record]]'' from 1980 to 2004.  

In 2004 the company was purchased by ''[[Thomson Scientific]]'' <ref name="BW-2004">Staff (12 January 2004) "Thomson Acquires BIOSIS Publishing Assets" ''Business Wire''</ref> and it is now part of [[Thomson Reuters]] Science & Healthcare division.<ref name="BW-2004"/> The proceeds from that sale were applied to fund an endowment and create a new grant-making foundation. The Board of Directors of that foundation selected as the organization’s new name the: ''J.R.S. Biodiversity Foundation''. This reflected both the historic legacy of the Foundation and its future grant-making domain. The initials J.R.S. stand for the name of one of the founders of BIOSIS.<ref>{{cite web|title=About JRS |publisher=JRS Biodiversity Foundation |url=http://jrsbdf.org/v3/About.asp |archive-url=https://web.archive.org/web/20100820021406/http://www.jrsbdf.org:80/v3/About.asp |dead-url=yes |archive-date=20 August 2010 |accessdate=23 March 2013 }}</ref> In 2007, [[Wolters Kluwer]] announced the digital availability of the BIOSIS Archive and Zoological Record Archive databases, via their [[Ovid Technologies]] online services.<ref name="WKH-2007">[http://www.wolterskluwer.com/WK/Press/Latest+News/2007/Mar/pr_06Mar07a.htm "Ovid Expands Portfolio of Archive Content with BIOSIS Archive and Zoological Record Archive"] ''Wolters Kluwer Health'' 6 March 2007</ref> The BIOSIS Archive consists of the data from the print volumes of ''[[Biological Abstracts]]'' from 1926 to 1968, and the Zoological Record Archive contains the data published in print in the ''The Zoological Record'' from 1864 to 1977.<ref name="WKH-2007"/> In 2010 Biosis Citation Index was released on the Web of Knowledge Platform, combining extensive indexing and database coverage of Biosis Previews with the Web of Science's citation tracking features.<ref>{{cite web|title=Biosis Citation Index |work=Products and tools |publisher=Web of Knowledge |url=http://wokinfo.com/products_tools/specialized/bci/ |accessdate=23 March 2013}}</ref>

==References==
<references />

==Further reading==
{{Refbegin}}
* Freedman, B. (1995) "Growth and Change in the World’s Biological Literature as Reflected in BIOSIS Publications" ''Publishing Research Quarterly'' 11(3): pp.&nbsp;61–79, DOI: 10.1007/BF02680447
* Kaser, Richard T. and Kaser, Victoria Cox (2001) ''BIOSIS: Championing the cause, the first 75 years'' National Federation of Abstracting and Information Services, Philadelphia, Pennsylvania, ISBN 978-0-942308-52-5
* Steere, William Campbell; Parkins, Phyllis V. and Philson, Hazel A. (1976) ''Biological Abstracts/BIOSIS, The First Fifty Years, The Evolution of a Major Science Information Service'' New York Botanical Garden, Plenum Press, New York, ISBN 0-306-30915-7
* Trumbull, Richard (1976) "BIOSIS: 50 Years of Biological Abstracts" ''BioScience'' 26(5): p.&nbsp;307
{{Refend}}
==External links==
* [http://library.dialog.com/bluesheets/html/blf.html Databases in Alphabetic Order]. DIALOG Bluesheets. July 16, 2010

{{Thomson Reuters}}

{{DEFAULTSORT:Biosis Previews}}
[[Category:Thomson Reuters]]
[[Category:Online databases]]
[[Category:Companies established in 1926]]
[[Category:Bibliographic databases and indexes]]