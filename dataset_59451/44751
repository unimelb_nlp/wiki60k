{{Infobox website
| name           = JSTOR
| logo           = JSTOR vector logo.svg
| logo_size      = 150px
| collapsible = y
| screenshot     = Screenshot of JSTOR.png
| caption        = The JSTOR front page
| url            = {{URL|jstor.org}}
| nonprofit      = Yes
| type           = [[Digital library]]
| language       = [[English language|English]] (includes content in other languages)
| registration   = Yes
| owner          = [[ITHAKA]]<ref name="About Ithaka"/>
| author         = [[Andrew W. Mellon Foundation]]
| launch_date    = {{start date and age|1995}}
| current_status = Active
| alexa          = {{Increase}} 1,778 ({{as of|2017|2|18|alt=February 2017}})<ref name="alexa">{{cite web|url= http://www.alexa.com/siteinfo/jstor.org |title= Jstor.org Site Info | publisher= [[Alexa Internet]] |accessdate= 2016-10-03 }}</ref><!--Updated monthly by OKBot.-->
| oclc           = 46609535
| revenue        = 
}}
'''JSTOR''' ({{IPAc-en|'|dʒ|eɪ|s|t|ɔːr}} {{respell|JAY|stor}};<ref>{{cite web|url=https://www.youtube.com/user/JSTORSupport|title=JSTOR Videos|accessdate=16 December 2012|publisher=YouTube}}</ref> short for ''Journal Storage'') is a [[digital library]] founded in 1995. Originally containing [[Digitizing|digitized]] back issues of [[academic journal]]s, it now also includes books and primary sources, and current issues of journals.<ref name="JSTOR glance"/> It provides [[full-text search]]es of almost 2,000 journals.<ref name="Annual Summary">{{cite web|title=Annual Summary|url=http://about.jstor.org/sites/default/files/misc/JSTOR-Annual-Summary_2012_v6.pdf|work=JSTOR|accessdate=13 April 2013|date=19 March 2013}}</ref> More than 8,000 institutions in more than 160 countries have access to JSTOR;<ref name="Annual Summary"/> most access is by subscription, but some older [[public domain]] content is freely available to anyone.<ref>{{cite web|url=http://about.jstor.org/download/file/fid/1423|title=Register and read beta}}</ref>

==History==
[[William G. Bowen]], president of [[Princeton University]] from 1972 to 1988, founded JSTOR.<ref>Leitch, Alexander. [http://etcweb.princeton.edu/CampusWWW/Companion/bowen_william_gordon.html "Bowen, William Gordon"]. [[Princeton University Press]].</ref> JSTOR originally was conceived as a solution to one of the problems faced by libraries, especially [[Research library|research and university libraries]], due to the increasing number of academic journals in existence. Most libraries found it prohibitively expensive in terms of cost and space to maintain a comprehensive collection of journals. By digitizing many journal titles, JSTOR allowed libraries to outsource the storage of journals with the confidence that they would remain available long-term. Online access and full-text search ability improved access dramatically.

Bowen initially considered using CD-ROMs for distribution.<ref>"JSTOR, A History" by Roger C. Schonfeld, Princeton University Press, 2003</ref> However, [[Ira Fuchs]], Princeton University's vice-president for Computing and Information Technology, convinced Bowen that CD-ROM was an increasingly outdated technology and that network distribution could eliminate redundancy and increase accessibility. (For example, all Princeton's administrative and academic buildings were networked by 1989; the student dormitory network was completed in 1994; and campus networks like the one at Princeton were, in turn, linked to larger networks such as [[BITNET]] and the [[Internet]].) JSTOR was initiated in 1995 at seven different library sites, and originally encompassed ten economics and history journals. JSTOR access improved based on feedback from its initial sites, and it became a fully searchable index accessible from any ordinary [[web browser]]. Special software was put in place{{where?|date=September 2015}} to make pictures and graphs clear and readable.<ref name="Taylor"/>

With the success of this limited project, Bowen and Kevin Guthrie, then-president of JSTOR, wanted to expand the number of participating journals. They met with representatives of the [[Royal Society|Royal Society of London]] and an agreement was made{{by whom|date=September 2015}} to digitize the ''[[Philosophical Transactions of the Royal Society]]'' dating from its beginning in 1665. The work of adding these volumes to JSTOR was completed by December 2000.<ref name = "Taylor" />

The [[Andrew W. Mellon Foundation]] funded JSTOR initially. Until January 2009 JSTOR operated as an independent, self-sustaining [[nonprofit organization]] with offices in [[New York City]] and in [[Ann Arbor, Michigan|Ann Arbor]], [[Michigan]]. Then JSTOR merged with the nonprofit [[Ithaka Harbors, Inc.]]<ref>{{cite web
  |title = About
  |url = http://about.jstor.org/about
  |publisher = JSTOR
  |accessdate = 28 November 2015
}}</ref> - a nonprofit organization founded in 2003 and "dedicated to helping the [[academic community]] take full advantage of rapidly advancing information and networking technologies."<ref name = "About Ithaka" />

==Content==
JSTOR content is provided by more than 900 publishers.<ref name="Annual Summary"/> The [[bibliographic database|database]] contains more than 1,900 journal titles,<ref name="Annual Summary"/> in more than 50 disciplines. Each object is uniquely identified by an integer value, starting at <code>1</code>.

In addition to the main site, the JSTOR labs group operates an open service that allows access to the contents of the archives for the purposes of corpus analysis at its ''Data for Research'' service.<ref>[http://dfr.jstor.org/ Data for Research]. JSTOR.</ref> This site offers a search facility with graphical indication of the article coverage and loose integration into the main JSTOR site. Users may create focused sets of articles and then request a dataset containing word and [[n-gram]] frequencies and basic metadata. They are notified when the dataset is ready and may download it in either [[XML]] or [[Comma-separated values|CSV]] formats. The service does not offer full-text, although academics may request that from JSTOR, subject to a non-disclosure agreement.

JSTOR Plant Science<ref>[http://plants.jstor.org/ JSTOR Plant Science]. JSTOR.</ref> is available in addition to the main site. JSTOR Plant Science provides access to content such as plant type specimens, taxonomic structures, scientific literature, and related materials and aimed at those researching, teaching, or studying botany, biology, ecology, environmental, and conservation studies. The materials on JSTOR Plant Science are contributed through the Global Plants Initiative (GPI)<ref>[http://about.jstor.org/content/global-plants#partnerships Global Plants Initiative]. JSTOR.</ref> and are accessible only to JSTOR and GPI members. Two partner networks are contributing to this: the African Plants Initiative, which focuses on plants from Africa, and the Latin American Plants Initiative, which contributes plants from Latin America.

JSTOR launched its Books at JSTOR program in November 2012, adding 15,000 current and backlist books to its site. The books are linked with reviews and from citations in journal articles.<ref>{{cite web |url= http://about.jstor.org/news/new-chapter-begins-books-jstor-launches |title=A new chapter begins: Books at JSTOR launches |date=November 12, 2012 |publisher=JSTOR |accessdate=December 1, 2012}}</ref>

==Access==
JSTOR is licensed mainly to academic institutions, public libraries, research institutions, museums, and schools. More than 7,000 institutions in more than 150 countries have access.<ref name="JSTOR glance" /> JSTOR has been running a pilot program of allowing subscribing institutions to provide access to their alumni, in addition to current students and staff. The Alumni Access Program officially launched in January 2013.<ref>{{cite web | url= http://about.jstor.org/alumni |title= Access for alumni | publisher =JSTOR | accessdate= December 1, 2012}} {{subscription required}}</ref> Individual subscriptions also are available to certain journal titles through the journal publisher.<ref>{{cite web | url= http://about.jstor.org/content/individual-subscriptions | title= Individual subscriptions | publisher = JSTOR | accessdate = December 1, 2012}} {{subscription required}}</ref> Every year, JSTOR blocks 150 million attempts by non-subscribers to read articles.<ref>[https://www.theatlantic.com/technology/archive/2012/01/every-year-jstor-turns-away-150-million-attempts-to-read-journal-articles/251382/ Every Year, JSTOR Turns Away 150 Million Attempts to Read Journal Articles]. ''The Atlantic''. Retrieved 29 January 2013.</ref>

Inquiries have been made about the possibility of making JSTOR [[open access]]. According to Harvard Law professor [[Lawrence Lessig]], JSTOR had been asked "how much would it cost to make this available to the whole world, how much would we need to pay you? The answer was $250 million".<ref>[https://www.youtube.com/watch?v=9HAw1i4gOU4#t=44m39 Lessig on "Aaron's Laws - Law and Justice in a Digital Age"]. YouTube (2013-02-20). Retrieved on 2014-04-12.</ref>

===Aaron Swartz incident===
{{main article|United States v. Aaron Swartz}}
{{see also|Aaron Swartz#JSTOR}}
In late 2010 and early 2011, Internet activist [[Aaron Swartz]] used [[Massachusetts Institute of Technology|MIT]]'s data network to bulk-download a substantial portion of JSTOR's collection of academic journal articles.<ref name= JSTORstatement /><ref name= HuffPost/> When the bulk-download was discovered, a video camera was placed in the room to film the mysterious visitor and the relevant computer was left untouched. Once video was captured of the visitor, the download was stopped and Swartz identified. Rather than pursue a civil lawsuit against him, in June 2011 they reached a settlement wherein he surrendered the downloaded data.<ref name=JSTORstatement/><ref name=HuffPost />

The following month, federal authorities charged Swartz with several "[[data theft]]"-related crimes, including [[wire fraud]], computer fraud, unlawfully obtaining information from a [[Computer security|protected computer]], and recklessly damaging a protected computer.<ref name="Bilton"/><ref name="open-access-adv" /> Prosecutors in the case claimed that Swartz acted with the intention of making the papers available on [[Peer-to-peer file sharing|P2P file-sharing sites]].<ref name=HuffPost /><ref name="Feds: Harvard fellow hacked millions of papers"/>

Swartz surrendered to authorities, pleaded not guilty to all counts, and was released on $100,000 bail. In September 2012, U.S. attorneys increased the number of charges against Swartz from four to thirteen, with a possible penalty of 35 years in prison and $1 million in fines.<ref name = JusticeGov /><ref name =Wired/> The case still was pending when Swartz committed [[suicide]] in January 2013.<ref>[http://www.bbc.co.uk/news/world-us-canada-21001452 "Aaron Swartz, internet freedom activist, dies aged 26"], BBC News</ref> 
Prosecutors dropped the charges after his death.<ref>[http://money.cnn.com/2014/06/27/technology/aaron-swartz-father/ "Aaron Swartz's father: He'd be alive today if he was never arrested"], money.cnn.com</ref>

===Limitations===
The availability of most journals on JSTOR is controlled by a "[[Embargo (academic publishing)|moving wall]]," which is an agreed-upon delay between the current volume of the journal and the latest volume available on JSTOR. This time period is specified by agreement between JSTOR and the publisher of the journal, which usually is three to five years. Publishers may request that the period of a "moving wall" be changed or request discontinuation of coverage. Formerly, publishers also could request that the "moving wall" be changed to a "fixed wall"—a specified date after which JSTOR would not add new volumes to its database. {{As of |2010|11}}, "fixed wall" agreements were still in effect with three publishers of 29 journals made available online through sites controlled by the publishers.<ref name="Wall" />

In 2010, JSTOR started adding current issues of certain journals through its Current Scholarship Program.<ref>{{cite web |url = http://about.jstor.org/content/about-current-journals |title=About current journals |publisher=JSTOR |accessdate=December 1, 2012}}</ref>

===Increasing public access===
Beginning September 6, 2011, JSTOR made public domain content freely available to the public.<ref name="JSTOR_EJC_announcement"/><ref name = LibraryJournal /> This "Early Journal Content" program constitutes about 6% of JSTOR's total content, and includes over 500,000 documents from more than 200 journals that were published before 1923 in the United States, and before 1870 in other countries.<ref name="JSTOR_EJC_announcement"/><ref name = LibraryJournal /><ref name=JSTOR_EJC_about/> JSTOR stated that it had been working on making this material free for some time. The Swartz controversy and Greg Maxwell's protest [[torrent file|torrent]] of the same content led JSTOR to "press ahead" with the initiative.<ref name = "JSTOR_EJC_announcement" /><ref name=LibraryJournal/>

In January 2012, JSTOR started a pilot program, "Register & Read,"  offering limited no-cost access (not [[open access]]) to archived articles for individuals who register for the service. At the conclusion of the pilot, in January 2013, JSTOR expanded Register & Read from an initial 76 publishers to include about 1,200 journals from over 700 publishers.<ref>{{cite news|last1=Tilsley|first1=Alexandra|title=Journal Archive Opens Up (Some)|url=https://www.insidehighered.com/news/2013/01/09/jstor-offer-limited-free-access-content-1200-journals|accessdate=6 January 2015|publisher=[[Inside Higher Ed]]|date=January 9, 2013}}</ref> Registered readers may read up to three articles online every two weeks, but may not print or download PDFs.<ref>{{cite web | url = http://about.jstor.org/rr | title =Register & Read |publisher=JSTOR |accessdate=2015-10-21}}</ref>

This is done by placing up to 3 items on a "shelf". The "Shelf" is under "My JSTOR" below "My Profile". The 3 works can then be read online at any time. An item cannot be removed from the shelf until it has been there for 14 days. Removing an old work from the shelf creates space for a new one, but doing so means the old work can no longer be accessed until it is shelved again.

JSTOR is conducting a [[Wikipedia:JSTOR|pilot program with Wikipedia]], whereby established editors are given reading privileges through the Wikipedia Library, as with a university library.<ref>{{cite web|last1=Orlowitz|first1=Jake|last2=Earley|first2=Patrick|title=Librarypedia: The Future of Libraries and Wikipedia|url=http://www.thedigitalshift.com/2014/01/discovery/librarypedia-future-libraries-wikipedia/|website=The Digital Shift|publisher=[[Library Journal]]|accessdate=20 December 2014|date=January 25, 2014}}</ref><ref>{{cite web|last1=Price|first1=Gary|title=Wikipedia Library Program Expands With More Accounts From JSTOR, Credo, and Other Database Providers|url=http://www.infodocket.com/2014/06/22/wikipedia-library-programs-expands-with-new-accounts-from-database-providers/|website=INFOdocket|publisher=[[Library Journal]]|accessdate=20 December 2014|date=June 22, 2014}}</ref>

==Use==
In 2012, JSTOR users performed nearly 152 million searches, with more than 113 million article views and 73.5 million article downloads.<ref name="Annual Summary"/> JSTOR has been used as a resource for linguistics research to investigate trends in language use over time and also to analyze gender differences in scholarly publishing.<ref name="Lexicology"/><ref>{{cite web|last1=Wilson|first1=Robin|title=Scholarly Publishing's Gender Gap|url=http://chronicle.com/article/The-Hard-Numbers-Behind/135236/|website=[[The Chronicle of Higher Education]]|accessdate=6 January 2015|date=October 22, 2012}}</ref>

==See also==<!-- PLEASE RESPECT ALPHABETICAL ORDER -->

* [[List of academic databases and search engines]]
* [[Aluka]]
* [[ARTstor]]
* [[ArXiv]]
* [[Japanese Historical Text Initiative]]
* [[Digital preservation]]
* [[Project MUSE]]

==References==
{{reflist|35em|refs=
<ref name="About Ithaka">{{cite web|url=http://www.ithaka.org/about-ithaka |title=About |publisher=Ithaka |accessdate=2009-10-25}}</ref>

<ref name="Feds: Harvard fellow hacked millions of papers">{{cite news | title = Feds: Harvard fellow hacked millions of papers | last = Lindsay | first = Jay |url= https://news.yahoo.com/feds-harvard-fellow-hacked-millions-papers-203301454.html|newspaper= [[Associated Press]]| date= July 19, 2011 | accessdate =July 20, 2011}}</ref>

<ref name="Bilton">{{cite news|last= Bilton |first= Nick |url= http://bits.blogs.nytimes.com/2011/07/19/reddit-co-founder-charged-with-data-theft/ | title = Internet activist charged in M.I.T. data theft |work=Bits Blog, The New York Times |date= July 19, 2011 |accessdate=December 1, 2012}}</ref><ref name="JSTOR glance">{{Cite journal|jstor= 20120213 |title= At a glance | format = PDF | publisher= JSTOR |date=February 13, 2012 }}</ref>

<ref name="Lexicology">{{Cite journal
| doi = 10.2307/455826
| volume = 73
| issue = 3
| pages = 279–296
| last = Shapiro
| first = Fred R.
| title = A Study in Computer-Assisted Lexicology: Evidence on the Emergence of ''Hopefully'' as a Sentence Adverb from the JSTOR Journal Archive and Other Electronic Resources
| journal = American Speech
| year = 1998
| jstor = 455826
}}</ref>

<ref name="Taylor">{{cite journal | last =Taylor | first = John| title=JSTOR: An Electronic Archive from 1665| journal=Notes and Records of the Royal Society of London| year=2001| volume=55| issue=1| pages=179–81| doi=10.1098/rsnr.2001.0135| jstor=532157}}</ref>

<ref name="Wall">{{cite web |url=http://about.jstor.org/content-collections/moving-wall |title=Moving wall |publisher= JSTOR}}</ref>

<ref name=JSTOR_EJC_announcement>Brown, Laura (September 7, 2011). "[http://about.jstor.org/news/jstor%E2%80%93free-access-early-journal-content-and-serving-%E2%80%9Cunaffiliated%E2%80%9D-users JSTOR–free access to early journal content and serving 'unaffiliated' users]", JSTOR. Retrieved December 1, 2012.</ref>

<ref name=JSTOR_EJC_about>{{cite web |url= http://about.jstor.org/service/early-journal-content |title=Early journal content |publisher= JSTOR | accessdate =December 1, 2012}}</ref>

<ref name=LibraryJournal>{{cite web|url=http://lj.libraryjournal.com/2011/09/academic-libraries/jstor-announces-free-access-to-500k-public-domain-journal-articles/|title=JSTOR Announces Free Access to 500K Public Domain Journal Articles|last=Rapp|first=David|date=2011-09-07|work=Library Journal}}</ref>

<ref name="open-access-adv">{{cite news | url=https://www.nytimes.com/2011/07/20/us/20compute.html?_r=1 | title=Open-Access Advocate Is Arrested for Huge Download | work=New York Times | date=July 19, 2011 | accessdate=July 19, 2011 | last =Schwartz | first = John}}</ref>

<ref name=JSTORstatement>{{cite web|url=http://about.jstor.org/news/jstor-statement-misuse-incident-and-criminal-case|title=JSTOR Statement: Misuse Incident and Criminal Case|date=2011-07-19|publisher= JSTOR}}</ref>

<ref name=HuffPost>{{cite web|url=http://www.huffingtonpost.com/2013/01/12/aaron-swartz_n_2463726.html|publisher=The Huffington Post|title= Aaron Swartz, Internet Pioneer, Found Dead Amid Prosecutor 'Bullying' In Unconventional Case|date=2013-01-12}}</ref>

<ref name=Wired>{{cite web|url=https://www.wired.com/threatlevel/2012/09/aaron-swartz-felony/all/|title=Feds Charge Activist with 13 Felonies for Rogue Downloading of Academic Articles|last=Kravets|first=David|publisher=[[Wired (magazine)|Wired]]|date=2012-09-18}}</ref>

<ref name=JusticeGov>{{cite web|url= http://www.justice.gov/archive/usao/ma/news/2011/July/SwartzAaronPR.html|archiveurl= https://web.archive.org/web/20110724043722/http://www.justice.gov/usao/ma/news/2011/July/SwartzAaronPR.html|archivedate= 2011-07-24 |dead-url=no|title= Alleged Hacker Charged with Stealing over Four Million Documents from MIT Network|last=Ortiz|first=Carmen|publisher= The United States Attorney's Office" |date= 2011-07-19}}</ref>
}}

==Further reading==
{{refbegin}}
* {{cite journal
 | last1 = Gauger | first1 = Barbara J
 | title = JSTOR usage data and what it can tell us about ourselves: is there predictability based on historical use by libraries of similar size?
 | journal = OCLC Systems & Services
 | year = 2006 | volume = 22 | issue = 1 | pages = 43–55
 | doi = 10.1108/10650750610640801
 | last2 = Kacena | first2 = Carolyn}}
* {{cite book
 | title = JSTOR: A History
 | last = Schonfeld | first = Roger C
 | year = 2003
 | publisher = Princeton University Press
 | location = Princeton, NJ
 | isbn = 0-691-11531-1}}
* {{cite journal
 | last = Seeds | first = Robert S
 | title = Impact of a digital archive (JSTOR) on print collection use
 | journal = Collection Building
 |date=November 2002 | volume = 21 | issue = 3 | pages = 120–22
 | doi = 10.1108/01604950210434551}}
* {{cite journal |doi=10.1300/J111v46n02_05 |title=JSTOR |year= 2007 |last =Spinella |first = Michael P |journal= Journal of Library Administration | volume = 46 |issue=2 |pages=55–78}}
* {{cite journal |doi=10.1108/02641610810878549 |title=JSTOR and the changing digital landscape |year=2008 |last1=Spinella |first1=Michael |journal = Interlending & Document Supply |volume=36 |issue=2 |pages=79–85}}
{{refend}}

==External links==
* {{Official website|http://www.jstor.org/}}
* {{cite web |url=http://about.jstor.org/jstor-institutions |title= Libraries and institutions offering access |publisher= JSTOR |accessdate= 2015-10-21}}. Searchable database, includes many public libraries offering free access to library card holders.
* {{cite web |url=http://about.jstor.org/rr |title=Register & Read |publisher= JSTOR |accessdate= 2015-10-21}}. Free individual registration, offering free read-only access (no printing or saving) to three articles every two weeks (seventy-eight per year).
*[https://archive.org/details/jstor_ejc JSTOR Early Journal Content : Free Texts : Download & Streaming : Internet Archive]

{{DEFAULTSORT:JSTOR}}
[[Category:Andrew W. Mellon Foundation]]
[[Category:Academic publishing]]
[[Category:Educational publishing companies]]
[[Category:Commercial digital libraries]]
[[Category:Organizations established in 1995]]
[[Category:Full text scholarly online databases]]