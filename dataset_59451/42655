<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{lowercase|de Havilland Oxford}}
{|{{Infobox Aircraft Begin
  |name = DH.11  Oxford
  |image = Airco D.H.11 aircraft.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = [[Bomber]]
  |manufacturer = [[Airco]]
  |designer = [[Geoffrey de Havilland]]
  |first flight = 1919
  |introduced = 
  |introduction= 
  |retired = 
  |status = Prototype
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = One
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Airco DH.11''' was a [[United Kingdom|British]] twin-engined [[biplane]] bomber which was designed to replace the  earlier [[Airco DH.10]] bomber. It was designed to use the unsuccessful [[ABC Motors|ABC]] [[ABC Dragonfly|Dragonfly]] engine and was abandoned after the first prototype was built.

==Development==
The '''DH.11 Oxford''' was designed by [[Geoffrey de Havilland]] for the [[Airco|Aircraft Manufacturing Company]] as a twin-engined day bomber to replace the [[Airco DH.10]]. It was designed (as required by the Specification) to use the [[ABC Motors|ABC]] [[ABC Dragonfly|Dragonfly]] [[radial engine]] which promised to give excellent performance and had been ordered in large numbers to be the powerplant for most of the new types on order for the [[Royal Air Force]]. The DH.11 was a twin-engined biplane, with all-wood construction and three-bay wings. It had an aerodynamically clean, deep fuselage occupying the whole wing gap, giving a good field of fire for the gunners in the nose and mid-upper positions.<ref name="lewis bomber">{{cite book |title=The British Bomber since 1914 |last=Lewis |first=Peter |edition= Third |date= |year=1980 |publisher=Putnam |location= London |isbn= 0-370-30265-6}}</ref>

The first prototype flew in January 1919,<ref name ="mason bomber">{{Cite book |author=Mason, Francis K |title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books |year=1994 |isbn= 0-85177-861-5}}</ref> powered by two 320&nbsp;hp Dragonfly engines. The prototype encountered handling problems, and was handicapped by the Dragonfly engines, which were extremely unreliable, being prone to overheating and excessive vibration, while not delivering the expected power. Two further prototypes were cancelled in 1919, with no aircraft in the end being purchased to replace the DH.10.<ref name ="mason bomber"/>

==Variants==
* '''Oxford Mk I''' - Prototype - powered by two 320&nbsp;hp (239&nbsp;kW) ABC Dragonfly engines - one built.
* '''Oxford Mk II''' - Proposed version with two [[Siddeley Puma]] engines - not built.
* '''DH.12''' - Proposed version with Dragonfly engines and modified gunner's position - not built.

==Specifications (Oxford Mk I)==
{{aircraft specifications
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Mason, The British Bomber since 1914 <ref name ="mason bomber"/> 
|crew=three
|capacity=
|length main= 45 ft 2¾ in
|length alt= 13.79 m
|span main= 60 ft 2 in
|span alt= 18.34 m
|height main= 13 ft 6 in
|height alt= 4.12 m
|area main= 719 ft²
|area alt= 66.8 m²
|airfoil=
|empty weight main= 4,105 lb
|empty weight alt= 1,866 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main=  
|useful load alt=  
|max takeoff weight main= 7,020 lb
|max takeoff weight alt= 3,191 kg
|more general=
|engine (prop)= [[ABC Dragonfly]]
|type of prop= 9 cylinder radial
|number of props=2
|power main= 320 hp
|power alt= 239 kW
|power original=
   
|max speed main= 107 kn
|max speed alt= 123 mph, 198 km/h
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 
|range alt= 
|ceiling main= 14,500 ft
|ceiling alt= 4,400 m
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= hp/lb
|power/mass alt= kW/kg
|more performance=*'''Climb to 10,000 ft:''' 13 min 45 sec
*'''Endurance:''' 3 hours
|guns=1 × .303 in (7.7 mm) [[Lewis gun]] at [[Scarff ring]]s at both nose and midships gunners cockpits
|bombs=4 × 230 lb (104 kg) bombs carried internally
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence= 
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{Reflist}}

==External links==
{{commons category|De Havilland aircraft}}
*[http://www.britishaircraft.co.uk/aircraftpage.php?ID=854 British Aircraft Directory]

{{De Havilland aircraft}}

[[Category:British bomber aircraft 1910–1919]]
[[Category:De Havilland aircraft|Oxford]]
[[Category:Cancelled military aircraft projects of the United Kingdom]]
[[Category:Airco aircraft|DH.011]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Biplanes]]