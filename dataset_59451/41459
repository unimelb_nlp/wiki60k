{{original research|date=February 2015}}
[[File:MondoKayo07StChasC.jpg|thumbnail|right|Revelers on [[St. Charles Avenue]], 2007]]
The holiday of [[Mardi Gras]] is celebrated in Southern [[Louisiana]].  Celebrations are concentrated for about two weeks before and through [[Shrove Tuesday]], the day before [[Ash Wednesday]].  Usually there is one major parade each day (weather permitting); many days have several large parades.  The largest and most elaborate parades take place the last five days of the Mardi Gras season. In the final week, many events occur throughout New Orleans and surrounding communities, including [[parade]]s and [[ball (dance)|balls]] (some of them [[masquerade ball]]s).

The parades in New Orleans are organized by social clubs known as [[krewe]]s; most follow the same parade schedule and route each year. The earliest-established krewes were the [[Mistick Krewe of Comus]], the earliest, [[Rex Parade|Rex]], the [[Knights of Momus]] and the [[Krewe of Proteus]]. Several modern "super krewes" are well known for holding large parades and events, such as the [[Krewe of Endymion]] (which is best known for naming celebrities as grand marshals for their parades), the [[Krewe of Bacchus]] (similarly known for naming celebrities as their Kings), as well as the [[Zulu Social Aid & Pleasure Club]]—a predominantly [[African American]] krewe. [[float (parade)|Float]] riders traditionally toss ''throws'' into the crowds; the most common throws are strings of colorful plastic beads, [[doubloons]] (aluminum or wooden dollar-sized coins usually impressed with a krewe logo), decorated plastic "throw cups", [[Moon Pie]]s, and small inexpensive toys.  Major krewes follow the same parade schedule and route each year.

While many tourists center their Carnival season activities on [[Bourbon Street]] and in New Orleans and Dauphin, major parades originate in the Uptown and Mid-City districts and follow a route along [[St. Charles Avenue]] and [[Canal Street, New Orleans|Canal Street]], on the upriver side of the French Quarter. Mardi Gras day traditionally concludes with the "Meeting of the Courts" between Rex and Comus.<ref>http://www.theneworleansadvocate.com/news/elections/11507257-93/comus-brings-carnival-to-glittering</ref>

==History==
[[File:RexArrivesPostcard.jpg|thumb|right|Arrival of Rex, monarch of Mardi Gras, as seen on an early 20th-century postcard]]
[[File:RexFree0.jpg|thumbnail|right|Rex, presented with freedom of the city; early 20th century postcard]]
[[File:CanalStreetRexC1900.jpg|thumb|right|Rex in procession down Canal Street; postcard from around 1900]]
[[File:CardWhip.jpg|thumbnail|right|Mardi Gras maskers; circa 1915 postcard]]
[[File:The Rex pageant, Mardi Gras Day, New Orleans, La..jpg|thumbnail|right|The Rex pageant, Mardi Gras Day, New Orleans, La., c. 1907]]

The first record of Mardi Gras being celebrated in Louisiana was at  the mouth of the [[Mississippi River]] in what is now lower [[Plaquemines Parish, Louisiana]], on March 2, 1699.  [[Pierre Le Moyne d'Iberville|Iberville]],  [[Bienville]], and their men celebrated it as part of an observance of [[Catholic]] practice. The date of the first celebration of the festivities in New Orleans is unknown. A 1730 account by  Marc-Antione Caillot celebrating with [[music]] and [[dance]], [[mask]]ing and [[costume|costuming]] (including [[cross-dressing]]). [http://acompanymanbook.com/infobox/excerpt/] An account from 1743 that the custom of Carnival balls was already established.  Processions and wearing of masks in the streets on Mardi Gras took place. They were sometimes prohibited by law, and were quickly renewed whenever such restrictions were lifted or enforcement waned.  In 1833 [[Bernard de Marigny|Bernard Xavier de Marigny de Mandeville]], a rich plantation owner of French descent, raised money to fund an official Mardi Gras celebration.

James R. Creecy in his book ''Scenes in the South, and Other Miscellaneous Pieces'' describes New Orleans Mardi Gras in 1835:<ref>{{cite book |title=Scenes in the South, and Other Miscellaneous Pieces |last= Creecy|first=James R. |year=1860 |publisher=T. McGill |location=Washington |oclc=3302746 |pages=43, 44 |url= https://books.google.com/books?id=ciYUAAAAYAAJ&printsec=frontcover&dq=Scenes+in+the+South,+and+Other+Miscellaneous+Pieces#v=onepage&q=&f=false}}</ref>

<blockquote>
Shrove Tuesday is a day to be remembered by strangers in New Orleans, for that is the day for fun, frolic, and comic masquerading. All of the mischief of the city is alive and wide awake in active operation. Men and boys, women and girls, bond and free, white and black, yellow and brown, exert themselves to invent and appear in grotesque, quizzical, diabolic, horrible, strange masks, and disguises. Human bodies are seen with heads of beasts and birds, beasts and birds with human heads; demi-beasts, demi-fishes, snakes' heads and bodies with arms of apes; man-bats from the moon; mermaids; satyrs, beggars, monks, and robbers parade and march on foot, on horseback, in wagons, carts, coaches, cars, &c., in rich confusion, up and down the streets, wildly shouting, singing, laughing, drumming, fiddling, fifeing, and all throwing flour broadcast as they wend their reckless way.
</blockquote>

In 1856 six businessmen gathered at a club room in New Orleans's French Quarter to organize a secret society to observe Mardi Gras with a formal parade. They founded New Orleans' first and oldest krewe, the [[Mystick Krewe of Comus]]. According to one historian, "Comus was aggressively English in its celebration of what New Orleans had always considered a French festival. It is hard to think of a clearer assertion than this parade that the lead in the holiday had passed from French-speakers to Anglo-Americans. . . .To a certain extent, Americans 'Americanized' New Orleans and its Creoles. To a certain extent, New Orleans 'creolized' the Americans. Thus the wonder of Anglo-Americans boasting of how their business prowess helped them construct a more elaborate version than was traditional. The lead in organized Carnival passed from Creole to American just as political and economic power did over the course of the nineteenth century. The spectacle of Creole-American Carnival, with Americans using Carnival forms to compete with Creoles in the ballrooms and on the streets, represents the creation of a New Orleans culture neither entirely Creole nor entirely American."<ref>''All on a Mardi Gras Day: Episodes in the History of New Orleans Carnival'' by Reid Mitchell. Harvard University Press:1995. ISBN 0-674-01622-X pg 25, 26</ref>

In 1875 Louisiana declared ''Mardi Gras'' a legal state holiday.<ref name="sparks"/> War, economic, political, and weather conditions sometimes led to cancellation of some or all major parades, especially during the [[American Civil War]], [[World War I]] and [[World War II]], but the city has always celebrated Carnival.<ref name="sparks">Sparks, R. [http://nuevomundo.revues.org/document3941.html American Sodom: New Orleans Faces Its Critics and an Uncertain Future]. ''La Louisiane à la dérive''. [http://nuevomundo.revues.org/sommaire2899.html#rub3927 The École des Hautes Études en Sciences Sociales Coloquio] - December 16, 2005.</ref>

1972 was the last year in which large parades went through the narrow streets of the city's French Quarter section; larger floats, crowds, and fire safety concerns led the city government to prohibit parades in the Quarter. Major parades now skirt the French Quarter along Canal Street.

In 1979 the New Orleans police department went on strike.  The official parades were canceled or moved to surrounding communities, such as [[Jefferson Parish, Louisiana|Jefferson Parish]].  Significantly fewer tourists than usual came to the city.  Masking, costuming, and celebrations continued anyway, with [[United States National Guard|National Guard]] troops maintaining order.  Guardsmen prevented crimes against persons or property but made no attempt to enforce laws regulating morality or drug use; for these reasons, some in the French Quarter [[Bohemianism|bohemian]] community recall 1979 as the city's best Mardi Gras ever.

In 1991 the New Orleans City Council passed an ordinance that required social organizations, including Mardi Gras Krewes, to certify publicly that they did not discriminate on the basis of race, religion, gender or sexual orientation, to obtain parade permits and other public licenses.<ref name="carnival"/>  Shortly after the law was passed, the city demanded that these krewes provide them with membership lists, contrary to the long-standing traditions of secrecy and the distinctly private nature of these groups.  In protest—and because the city claimed the parade gave it jurisdiction to demand otherwise-private membership lists—the 19th-century krewes Comus and Momus stopped parading.<ref name="deja"/>  Proteus did parade in the 1992 Carnival season but also suspended its parade for a time, returning to the parade schedule in 2000.

Several organizations brought suit against the city, challenging the law as unconstitutional.  Two federal courts later declared that the ordinance was an unconstitutional infringement on First Amendment rights of free association, and an unwarranted intrusion on the privacy of the groups subject to the ordinance.<ref>The decision of the Fifth Circuit Court of Appeals appears at volume 42, page 1483 of the Federal Reporter (3rd Series), or 42 F.3d 1483 (5th Cir. 1995).</ref> The [[US Supreme Court]] refused to hear the city's appeal from this decision.

Today, New Orleans krewes operate under a business structure; membership is open to anyone who pays dues, and any member can have a place on a parade float.

The devastation caused by [[Hurricane Katrina]] in late 2005 caused a few people to question the future of the city's Mardi Gras celebrations. [[Ray Nagin|Mayor Nagin]], who was up for reelection in early 2006, tried to play this sentiment for electoral advantage{{Citation needed|date=February 2010}}. However, the economics of Carnival were, and are, too important to the city's revival.

The city government, essentially bankrupt after Hurricane Katrina, pushed for a scaled back celebration to limit strains on city services.  However, many krewes insisted that they wanted to and would be ready to parade, so negotiations between krewe leaders and city officials resulted in a compromise schedule.  It was scaled back but less severely than originally suggested.

[[File:ChaosCheifEngineer100Hades.jpg|thumb|left|2006: A Knights of Chaos float satirizes the [[U.S. Army Corps of Engineers]], responsible for the [[2005 levee failures in Greater New Orleans|failed levees]] in New Orleans]]
The 2006 New Orleans Carnival schedule included the [[Krewe du Vieux]] on its traditional route through Marigny and the French Quarter on February 11, the Saturday two weekends before Mardi Gras. There were several parades on Saturday, February 18, and Sunday the 19th a week before Mardi Gras.  Parades followed daily from Thursday night through Mardi Gras. Other than Krewe du Vieux and two Westbank parades going through Algiers, all New Orleans parades were restricted to the Saint Charles Avenue Uptown to Canal Street route, a section of the city which escaped significant flooding. Some krewes unsuccessfully pushed to parade on their traditional Mid-City route, despite the severe flood damage suffered by that neighborhood.

The city restricted how long parades could be on the street and how late at night they could end.  National Guard troops assisted with crowd control for the first time since 1979.  Louisiana State troopers also assisted, as they have many times in the past.  Many floats had been partially submerged in floodwaters for weeks.  While some krewes repaired and removed all traces of these effects, others incorporated flood lines and other damage into the designs of the floats.

Most of the locals who worked on the floats and rode on them were significantly affected by the storm's aftermath.  Many had lost most or all of their possessions, but enthusiasm for Carnival was even more intense as an affirmation of life.  The themes of many costumes and floats had more barbed satire than usual, with commentary on the trials and tribulations of living in the devastated city.  References included [[MRE]]s, [[Katrina refrigerator]]s and [[FEMA trailer]]s, along with much mocking of the [[Federal Emergency Management Agency]] (FEMA) and local and national politicians.

By the 2009 season, the [[Krewe of Endymion|Endymion]] parade had returned to the Mid-City route, and other Krewes expanding their parades Uptown.

==Traditional colors==
[[File:MardiGrasFlag.gif|thumb|right|Mardi Gras flag]]
{| class="wikitable" 
|-
! colspan="2" | Meaning of Colors
|-
| style="background:purple;"|&nbsp; &nbsp; ||&nbsp;Justice (purple)
|-
| style="background:gold;"|&nbsp; &nbsp; || &nbsp;Power (gold)
|-
| style="background:green;"|&nbsp; &nbsp; || &nbsp;Faith (green)
|}
The traditional colors of the New Orleans Mardi Gras are [[purple]], [[green]], and [[gold (color)|gold]]. All three colors were used by the Catholic Church throughout history and thus continued to be used in relation to Mardi Gras which is Catholic in origin.

In his book "Krewe: The Early New Orleans Carnival: Comus to Zulu," Errol Laborde shows the above-mentioned meanings of the Mardi Gras colors to be false. He gives a much simpler origin, having to do primarily with looking good.<ref>{{cite book |title= Krewe: The Early New Orleans Carnival: Comus to Zulu|last=Laborde |first=Errol |year=2007 |publisher=Carnival Press |location= Metairie, La.|isbn= 978-0-9792273-0-1 |pages=57–61 }}</ref>

== Contemporary Mardi Gras ==
[[File:Magazine.jpg|thumbnail|right|Float on Magazine Street, 1996]] 
[[File:ToHorses.jpg|thumbnail|right|Mounted Krewe Officers in the Thoth Parade, 1998]]
Each year the New Orleans Carnival season starts on January 6, called [[Twelfth Night (holiday)|Twelfth Night]]. The Twelfth Night Revelers, New Orleans' second-oldest Krewe, hold a masked ball each year to mark the beginning of New Orleans' Carnival season. Many of Carnival's oldest societies, such as the Independent Strikers' Society, hold masked balls but no longer parade in public.

Mardi Gras season continues through [[Shrove Tuesday]] or ''Fat Tuesday''.

=== Weekend before Mardi Gras Day ===
The population of New Orleans more than doubles during the five days before Mardi Gras Day, in anticipation of the biggest celebration.

Wednesday night begins with Druids, and is followed by the [[Mystic Krewe of NYX|Mystic Krewe of Nyx]], the newest all-female Krewe.  Nyx is famous for their highly decorated purses, and has reached Super Krewe status since their founding in 2011.

Thursday night starts off with another all-women's parade featuring the [[Krewe of Muses]].  The parade is relatively new, but its membership has tripled since its start in 2001. It is popular for its throws (highly sought-after decorated shoes and other trinkets) and themes poking fun at politicians and celebrities.

Friday night is the occasion of the large [[Krewe of Hermes]] and satirical [[Le Krewe d'Etat|Krewe D'État]] parades, ending with one of the fastest-growing krewes, the [[Krewe of Morpheus]].<ref>[http://www.kreweofmorpheus.com Krewe of Morpheus website]</ref> There are several smaller neighborhood parades like the [[Krewe of Barkus]] and the [[Krewe of OAK]].

Several daytime parades roll on Saturday (including [[Krewe of Tucks]] and [[Krewe of Isis]]) and on Sunday ([[Thoth]], [[Okeanos]], and [[Krewe of Mid-City]]).

The first of the "super krewes," [[Krewe of Endymion|Endymion]], parades on Saturday night, with the celebrity-led [[Krewe of Bacchus|Bacchus]] parade on Sunday night 1998

=== Lundi Gras ===
{{Unreferenced section|date=February 2015}}
Monday was recently{{when|date=February 2015}} declared to be ''Lundi Gras'' ("Fat Monday"). The monarchs of the [[Zulu Social Aid & Pleasure Club]] and [[Krewe of Rex]], who will parade the following day, arrive by boat on the [[Mississippi River]]front at the foot of Canal Street, where an all-day party is staged.

Uptown parades start with one of New Orleans' most prestigious organizations, the [[Krewe of Proteus]].  Dating to 1882, it is the second-oldest krewe still parading.  The Proteus parade is followed by a newer organization, the music-themed super-[[Krewe of Orpheus]], which is considered less prestigious as it draws a significant portion of its membership from outside of New Orleans.

===Mardi Gras ===
The celebrations begin early on Mardi Gras, which can fall on any Tuesday between February 3 and March 9 (depending on the date of [[Easter]], and thus of [[Ash Wednesday]]).<ref>[http://www.mardigrasneworleans.com/mgdates.html Mardi Gras dates]</ref>

In New Orleans, uptown, the Zulu parade rolls first, followed by the [[Rex parade]], which both end on Canal Street. A number of smaller parading organizations with "truck floats" follow the Rex parade. Numerous smaller parades and walking clubs also parade around the city. The [[Jefferson City Buzzards]], the [[Lyons Club]],  the [[Irish Channel Corner Club]], [[Pete Fountain]]'s [[The Half Fast Walking Club|Half Fast Walking Club]] and the [[KOE]] all start early in the day Uptown and make their way to the French Quarter with at least one [[jazz]] band. At the other end of the old city, the [[Society of Saint Anne]] journeys from the Bywater through Marigny and the French Quarter to meet Rex on Canal Street.  The Pair-O-Dice Tumblers rambles from bar to bar in Marigny and the French Quarter from noon to dusk. Various groups of [[Mardi Gras Indians]], divided into uptown and downtown tribes, parade in their finery.

For upcoming Mardi Gras Dates through 2050 see [[Shrove Tuesday#Calendar Dates|''Mardi Gras Dates'']].

== Costumes and masks ==
[[File:Mardi Gras morning, 2004, Bywater neighborhood.jpg|right|thumb|Reveler, Mardi Gras morning in the [[Bywater, New Orleans|Bywater neighborhood]], 2004]] 
In New Orleans, [[costume]]s and [[mask]]s are seldom publicly worn by non-Krewe members on the days before Fat Tuesday (other than at parties), but are frequently worn on Mardi Gras. Laws against concealing one's identity with a mask are suspended for the day. Banks are closed, and some businesses and other places with security concerns (such as convenience stores) post signs asking people to remove their masks before entering.

== Beads ==
[[File:Mardi Gras beads metallic style.jpg|thumb|left|Mardi Gras beads]]
Inexpensive strings of beads and toys have been thrown from floats to parade-goers since at least the late 19th century. Until the 1960s, the most common form was multi-colored strings of glass beads made in [[Czechoslovakia]].

Glass beads were supplanted by less expensive and more durable plastic beads, first from [[Hong Kong]], then from [[Taiwan]], and more recently from [[China]]. Lower-cost beads and toys allow float-riders to purchase greater quantities, and throws have become more numerous and common.

In the 1990s, many people lost interest in small, cheap beads, often leaving them where they landed on the ground. Larger, more elaborate metallic beads and strands with figures of animals, people, or other objects have become the sought-after throws. David Redmond's 2005 film of cultural and [[economic globalization]], [[IMDbTitle:0436569|''Mardi Gras: Made in China'']], follows the production and distribution of beads from a small factory in [[Fuzhou]], China to the streets of New Orleans during Carnival.<ref>David Redmon (2008). [http://www.cultureunplugged.com/play/568/Mardi-Gras---Made-in-China Mardi Gras: Made in China]. Culture Unplugged. Retrieved 2010-02-16.</ref> The publication of Redmon's book, ''Beads Bodies, and Trash'', follows up on the documentary by providing an ethnographic analysis of the social harms, the pleasures, and the consequences of the toxicity that Mardi Gras beads produce.

With the advent of the 21st century, more sophisticated throws began to replace simple metallic beads. Krewes started to produce limited edition beads and plush toys that are unique to the krewe. Fiber optic beads and LED-powered prizes are now among the most sought-after items. In a retro-inspired twist, glass beads have returned to parades. Now made in India, glass beads are one of the most valuable throws.

== Other Mardi Gras traditions ==

=== Social clubs ===
[[File:The Carnival at New Orleans.jpg|thumb|right|''The Carnival at New Orleans'', 1885]]
New Orleans Social clubs play a very large part in the Mardi Gras celebration as hosts of many of the parades on or around Mardi Gras. The two main Mardi Gras parades, Zulu and Rex, are both social club parades. Zulu is a mostly African-American club and Rex is mostly Caucasian.  Social clubs host Mardi Gras balls, starting in late January. At these social balls, the queen of the parade (usually a young woman between the ages of 18 and 21, not married and in high school or college) and the king (an older male member of the club) present themselves and their court of maids (young women aged 16 to 21), and different divisions of younger children with small roles in the ball and parade, such as a theme-beformal neighborhood Carnival club ball at local bar room.

In response to their exclusion from Rex, in 1909 Créole and black New Orleanians, led by a mutual aid group known as "The Tramps", adorned William Storey with a tin can crown and banana stalk scepter and named him King Zulu.<ref name="carnival">[http://www.carnaval.com/cityguides/neworleans/history.htm Three centuries of Mardi Gras history]. From: carnaval.com. Retrieved October 19, 2007.</ref><ref name="history">[http://www.mardigrasneworleans.com/history.html Mardi Gras History]. From: mardigrasneworleans.com. Retrieved October 19, 2007.</ref>  This display was meant as a mockery of Rex's overstated pageantry, but in time, Zulu became a grand parade in its own right.  By 1949, as an indication of Zulu's increase in prestige, the krewe named New Orleans' native son [[Louis Armstrong]] as its king.<ref name="sparks"/>

Being a member of the court requires much preparation, usually months ahead.  Women and girls must have dress fittings as early as the May before the parade, as the season of social balls allows little time between each parade. These balls are generally by invitation only.  Balls are held at a variety of venues in the city, large and small, depending on the size and budget of the organization.  In the late 19th and early 20th century, the [[French Opera House]] was a leading venue for New Orleans balls. From the mid 20th century until [[Hurricane Katrina]] the [[Municipal Auditorium (New Orleans)|Municipal Auditorim]] was the city's most famous site for Carnival balls.  In more recent years, most are at the ballrooms of various hotels throughout the city.  The largest "Super Krewes" use larger venues; Bacchus the [[New Orleans Morial Convention Center|Morial Convention Center]] and Endymion the [[Louisiana Superdome|Superdome]].

=== Doubloons ===
One of the many Mardi Gras throws which krewes fling into the crowds, [[doubloons]] are large coins, either wood or metal, made in Mardi Gras colors.  Artist H. Alvin Sharpe created the modern doubloon for The School of Design (the actual name of the Rex organization).  According to the krewe history, in January 1959 Sharpe arrived at the offices of the captain of the krewe with a handful of aluminum discs.  Upon entering the office, he threw the doubloons into the captain's face to prove that they would be safe to throw from the floats.  Standard krewe doubloons usually portray the Krewe's emblem, name, and founding date on one side, and the theme and year of the parade and ball on the other side.  Royalty and members of the court may throw specialty doubloons, such as the special Riding Lieutenant doubloons given out by men on horseback in the Rex parade.  In the last decade, krewes have minted doubloons specific to each float.  Krewes also mint special doubloons of cloisonné or pure silver for its members.  They never throw these from the floats.  Original Rex doubloons are valuable, but it is nearly impossible for aficionados to find a certified original doubloon.  The School of Design did not begin dating their doubloons until a few years after their introduction.

[[File:KChaos07FlambeauKnightRed.jpg|right|thumb|Carriers with lit flambeaux on Napoleon Avenue, just before the start of a parade, 2007]]

=== Flambeau carriers ===
The ''flambeau'' ("flahm-bo" meaning flame-torch) carrier originally, before electric lighting, served as a beacon for New Orleans parade goers to better enjoy the spectacle of night parades. The first flambeau carriers were [[History of slavery in Louisiana|slaves]].

Today, the flambeaux are a connection to the New Orleans version of Carnival and a valued contribution.  Many people view flambeau-carrying as a kind of performance art &ndash; a valid assessment given the wild gyrations and flourishes displayed by experienced flambeau carriers in a parade. Many individuals are descended from a long line of carriers.

Parades that commonly feature flambeaux include Babylon, Chaos, [[Le Krewe d'Etat]], Druids, Hermes, [[Krewe of Muses]], [[Krewe of Orpheus]], [[Krewe of Proteus]], Saturn, and Sparta. Flambeaux are powered by [[naphtha]]{{Citation needed|date=January 2010}}, a highly flammable aromatic.

It is a tradition, when the flambeau carriers pass by during a parade, to toss quarters to them in thanks for carrying the lights of Carnival. In the 21st century, though, handing dollar bills is common.

===Rex===
Each year in New Orleans, [[krewes]] are responsible for electing [[Rex (king)|Rex]], the king of the carnival.<ref>{{cite web| url= http://www.answers.com/topic/krewe|title= Krewe|publisher=American Heritage Dictionary|accessdate=2012-06-18}}</ref> The [[Rex Organization]] was formed to create a daytime parade for the residents of the city. The Rex motto is, "Pro Bono Publico—for the public good."<ref>{{cite web| url= http://www.rexorganization.com/History/|title= Rex King of Carnival|publisher=Rex Organization|accessdate=2012-06-18}}</ref>

=== Mardi Gras icons ===
* Faces of Comedy and Tragedy
* [[Feathered mask]]s
* Laissez Les Bons Temps Rouler! (French: "Let the good times roll!")
* Throw me something, Mister!

[[File:Zulu 100th Anniversary 15.jpg|right|thumb|Revelers on [[Basin Street]] examine their parade catches, including a Zulu Coconut, 2009]]

=== New Orleans Zulu or Mardi Gras Coconut ===
One of the most famous and the most sought after throws, is the [[Zulu Social Aid & Pleasure Club|Zulu Coconut]] also known as the Golden Nugget and the Mardi Gras Coconut.<ref name="deja">[http://www.nola.com/mardigras/about/index.ssf?/mardigras/about/content/stories/history.html Deja Krewe]. ''The Times-Picayune''. Retrieved October 19, 2007.</ref> The coconut is mentioned as far back as 1910, where they were given in a natural "hairy" state. The coconut was thrown as a cheap alternative, especially in 1910 when the bead throws were made of glass. Before the Krewe of Zulu threw coconuts, they threw walnuts that were painted gold. This is where the name "Golden Nugget" originally came from. It is thought that Zulu switched from walnuts to coconuts in the early 1920s when a local painter, Lloyd Lucus, started to paint coconuts. Most of the coconuts have two decorations. The first is painted gold with added glitter, and the second is painted like the famous black Zulu faces. In 1988, the city forbade Zulu from throwing coconuts due to the risk of injury; they are now handed to onlookers rather than thrown. In the year 2000, a local electronics engineer, Willie Clark, introduced an upgraded version of the classic, naming them Mardi Gras Coconuts. These new coconuts were first used by the club in 2002, giving the souvenirs to royalty and city notables.

===Ojen liqueur===
''[[Aguardiente de Ojén]]'' ([[:es:Aguardiente de Ojén|es]]), or simply "ojen" ("OH-hen") as it is known in English, is a [[Spain|Spanish]] [[anisette]] traditionally consumed during the New Orleans Mardi Gras festivities.<ref>[http://www.neworleansbar.org/uploads/files/OjenUpdate.3-2.pdf ''New Orleans Nostalgia'', "Banana Republics and ''Ojen'' Cocktails", Ned Hémard, 2007]</ref> In [[Ojén]], the original Spanish town where it is produced, production stopped for years, but it started again in early 2014 by means of the distillery company Dominique Mertens Impex. S.L.<ref>''[http://www.aguardienteojen.com/ Dominique Mertens Impex. S.L., Ojén, aguardiente superior]'', official website, in Spanish</ref>

== Exposure and Mardi Gras ==
[[File:MardiGras2009CoffeeCustomers.JPG|right|thumb|French Quarter coffee house, Mardi Gras afternoon, 2009]]
Women showing their breasts during Mardi Gras has been documented since 1889, when the ''Times-Democrat'' decried the "degree of immodesty exhibited by nearly all female masqueraders seen on the streets." The practice was mostly limited to tourists in the upper Bourbon Street area.<ref name="sparks"/><ref name="shrum"/> In the crowded streets of the French Quarter, generally avoided by locals on Mardi Gras Day, [[Exhibitionism|flashers]] on balconies cause crowds to form on the streets.

In the last decades of the 20th century, the rise in producing commercial [[videotape]]s catering to [[voyeurism|voyeurs]] helped encourage a tradition of women baring their breasts in exchange for beads and trinkets.  Social scientists studying "ritual disrobement" found, at Mardi Gras 1991, 1,200 instances of body-baring in exchange for beads or other favors.<ref name="shrum">Shrum, W. and J. Kilburn. [http://www.jstor.org/stable/2580408 "Ritual Disrobement at Mardi Gras: Ceremonial Exchange and Moral Order"]. ''Social Forces'', Vol. 75, No. 2. (Dec., 1996), pp. 423-458.</ref>

== End of New Orleans Mardi Gras ==
The formal end of New Orleans Mardi Gras arrives with the "Meeting of the Courts," a ceremony at which Rex and His Royal Consort, the King and Queen of Carnival, meet with Comus and his Queen, at the ball of the [[Mistick Krewe of Comus]], New Orleans' oldest active Carnival organization.  The Meeting of the Courts happens at the conclusion of the two groups' masked balls, which in modern times have both been held at the [[Municipal Auditorium (New Orleans)|New Orleans Municipal Auditorium]]. Following Hurricane Katrina, the ball has been held in the Marriott Hotel.

Promptly at the stroke of midnight at the end of Fat Tuesday, a [[Mounted police|mounted squad]] of New Orleans police officers make a show of clearing upper Bourbon Street where the bulk of out-of-town revelers congregate, announcing that Carnival is over, as it is the start of [[Lent]], commencing with [[Ash Wednesday]].

Ash Wednesday (the day after Big Tuesday) is sometimes jokingly referred to as "Trash Wednesday" because of the amount of refuse left in the streets during the previous day's celebrations and excesses. The tons of garbage picked up by the sanitation department is a local news item, partly because it reflects the positive economic impact of [[Tourism|tourist]] revenue.

== Additional photographs ==
* [[Faubourg Marigny Mardi Gras costumes]]
* [[French Quarter Mardi Gras costumes]]

== See also ==
* [[Mardi Gras Mambo]]

== References ==
{{Reflist}}

== External links ==
{{Commons category}}
{{wikivoyage|New Orleans Mardi Gras}}
* [http://carnivalneworleans.com/ Carnival New Orleans] History of Mardi Gras with vintage and modern pictures
* [http://www.mardigrasunmasked.com Mardi Gras Unmasked] Definitive Mardi Gras and king cake histories
* [http://www.mardigras.com/ MardiGras.com] Web site affiliated with New Orleans' ''Times-Picayune'' newspaper
* [http://2014mardigras.org/ Mardi Gras 2014 celebration photos]
* [http://life.time.com/culture/mardi-gras-rare-photos-from-new-orleans-biggest-party-1938/#1 Mardi Gras: ''LIFE'' at America's Most Famous Party, 1938] - slideshow by ''[[Life magazine]]''
* [http://mardigrasneworleans.com/ MardiGrasNewOrleans.com] Emphasizes the family and traditional aspects
* [http://carlnivale.theatricana.com/ Prof. Carl Nivale's Compleat Carnival Compendium and Mardi Gras Manual] A family-friendly site from WWL-TV's historian Carl Nivale
* [http://libmma.contentdm.oclc.org/cdm/search/collection/p15324coll12/searchterm/Mardi%20Gras/order/nosort Fashion plates featuring historic Mardi Gras costumes] from the Metropolitan Museum of Art libraries
* [http://emardigrasbeads.com/ Mardi Gras beads, masks, and decorations]
* [https://tulane.edu/news/newwave/upload/2014-Mardi-Gras-Impact-Study.pdf Economic Impact of the 2014 Mardi Gras Season] by Toni Weiss and the Freeman Consulting Group, 2015. Tulane University.

{{Mardi Gras in New Orleans}}

{{Carnival around the world}}

[[Category:Economy of New Orleans]]
[[Category:Festivals in Louisiana]]
[[Category:Mardi Gras in New Orleans| ]]
[[Category:Tourist attractions in New Orleans]]
[[Category:Carnivals in the United States]]