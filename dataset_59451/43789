{|{{Infobox Aircraft Begin
 | name=Tu-1
 | image=Tupolev Tu-1.jpg
 | caption=Front-quarter view of the Tu-1 (ANT-63P) prototype
}}{{Infobox Aircraft Type
 | type=[[Night fighter]]
 | national origin=[[Soviet Union]]
 | manufacturer=[[Tupolev]]
 | designer=
 | first flight=22 March 1947
 | introduced=
 | retired=
 | status=Cancelled
 | primary user=
 | number built=1
 | developed from=[[Tupolev Tu-2]] 
 | variants with their own articles=
}}
|}
The '''Tupolev Tu-1''' was a prototype [[Soviet Union|Soviet]] [[night fighter]] variant of the [[Tupolev Tu-2]] medium [[bomber]] that first flew after the end of [[World War II]]. It was cancelled when its experimental Mikulin AM-43V engines reached the end of their service life.

==Development==
Impressed by the performance of the [[de Havilland Mosquito]] the Soviets asked [[Tupolev]] to modify a Tu-2 as a high-speed day bomber with a reduced crew as the ANT-63. The second prototype of this project was ordered to be converted in February 1946 for use as a three-seat long-range interceptor capable of carrying an airborne [[radar]] set with the internal designation of ANT-63P and the official designation of Tu-1. It was given prototype Mikulin AM-43V engines driving four-bladed [[propeller (aircraft)|propeller]]s, and fitted with new radio equipment. It reverted to the standard Tu-2S undercarriage. Two {{convert|45|mm|abbr=on}} [[Nudelman-Suranov NS-45]] guns with 50 rounds each were fitted on the underside of the nose, two {{convert|23|mm|abbr=on}} [[Volkov-Yartsev VYa-23]] or [[Nudelman-Suranov NS-23]] cannon were fitted in the wing roots with 130 rounds per gun. The dorsal gunner was given a {{convert|12.7|mm|abbr=on}} [[Berezin UB|UBT machine gun]] with 200 rounds and the ventral gunner received a UBT with 350 rounds of ammunition. It retained the internal bomb bay which could carry up to {{convert|1000|kg|abbr=on}} of bombs.<ref name=g/>

The Tu-1 first flew on 22 March 1947 and underwent manufacturer's tests until 3 October<ref name=go>Gordon, p. 91</ref> or 3 November 1947.<ref name=g/> Sources disagree about the mounting of radar during these tests. [[Bill Gunston]] says that a Soviet derivative of the German [[Lichtenstein radar#FuG 220 Lichtenstein SN-2|FuG 220 Lichtenstein SN-2]] was tested,<ref name=g/> however Yefim Gordon believes that no radar was fitted at all and the short service life of the AM-43V prototype engines curtailed the planned tests and development. At any rate, the aircraft was not selected for production because its AM-43V engines were not ready for production.<ref name=go/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{aircraft specifications

|plane or copter?=<plane
|jet or prop?=<prop

|ref=''Gordon, OKB Tupolev: A History of the Design Bureau and its Aircraft''

|crew=three
|capacity=
|length main=13.6 m
|length alt=44 ft 7½ in
|span main=18.86 m
|span alt=61 ft 10½ in
|height main=3.32 m
|height alt= ft  in 
|area main=48.8 m²
|area alt=525 ft²
|airfoil=
|empty weight main=9,460 kg
|empty weight alt=20,855 lb<ref name=g>Gunston, p. 122</ref>
|loaded weight main=12,755 kg
|loaded weight alt=28,119 lb
|useful load main= kg
|useful load alt= lb
|max takeoff weight main=14,460 kg
|max takeoff weight alt=31,878 lb<ref name=g/>
|more general=

|engine (prop)=Mikulin AM-43V
|type of prop=liquid-cooled V-12
|number of props=1
|power main=1454 kW
|power alt=1950 hp
|power original=

|max speed main=641 km/h
|max speed alt=345.8 knots, 398 mph
|cruise speed main= km/h
|cruise speed alt= knots,  mph
|range main=2250 km
|range alt=1215 nm, 1400 mi
|ceiling main=11,000 m
|ceiling alt=36,090 ft
|climb rate main= m/s
|climb rate alt= ft/min
|loading main= 261.37kg/m²
|loading alt= 53.56lb/ft²
|thrust/weight=
|power/mass main= W/kg
|power/mass alt= hp/lb
|more performance=

|armament=
*2 × 45&nbsp;mm [[Nudelman-Suranov NS-45]] cannon
*2 × 23&nbsp;mm [[Volkov-Yartsev VYa-23]] cannon
*2 × 12.7&nbsp;mm [[Berezin UB|UBT machine gun]]s
*up to {{convert|1000|kg|abbr=on}} of bombs

|avionics=
}} 
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=* [[Junkers Ju 88]]
* [[Dornier Do 17]]
* [[Dornier Do 217]]
* [[Focke-Wulf Ta 154]]
* [[A-20 Havoc]]
* [[Petlyakov Pe-3]]
|lists=<!-- related lists -->
}}

==References==
{{commonscat|Tupolev Tu-1}}
{{reflist}}
{{refbegin}}
* {{cite book|last=Gordon|first=Yefim|author2=Rigamant, Vladimir |title=OKB Tupolev: A History of the Design Bureau and its Aircraft|publisher=Midland Publishing|location=Hinckley, England|date=2005|isbn=1-85780-214-4}}
* {{cite book|last=Gunston|first=Bill|authorlink=Bill Gunston|title=Tupolev Aircraft since 1922|publisher=Naval Institute Press|location=Annapolis, MD|date=1995|isbn=1-55750-882-8}}
{{refend}}
<!-- ==External links== -->

{{Tupolev aircraft}}

[[Category:Tupolev aircraft|Tu-0001]]