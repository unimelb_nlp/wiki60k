{{EngvarB|date=April 2014}}
{{Use dmy dates|date=April 2014}}
{{good article}}
{{Infobox military conflict
|conflict=Action of 20 October 1793
|partof=the [[French Revolutionary Wars]]
|image=[[File:Reunion and Crescent, Dixon 1901.jpg|300px|Two warships exchange fire on a choppy sea. The left ship flies a white flag with a red cross and the right one a red, white and blue tricolour. Both ships are badly damaged, particularly in their sails and rigging.]]
|caption=''Capture of the French ship Réunion by the Crescent, 1793'', [[Charles Dixon (artist)|Charles Dixon]], 1901
|date=20 October 1793
|place=Off Cape Barfleur, [[English Channel]]
|result=British victory
|combatant1= {{flagcountry|Kingdom of Great Britain}}
|combatant2= {{flagicon|France}} [[First French Republic|French Republic]]
|commander1=Captain [[James Saumarez]]
|commander2=Captain [[François A. Dénian]]
|strength1=[[Frigate]] [[HMS Crescent (1784)|HMS ''Crescent'']], distantly supported by [[HMS Circe (1785)|HMS ''Circe'']]
|strength2=[[Frigate]] [[French frigate Reunion (1786)|''Réunion'']] and [[cutter (ship)|cutter]] ''Espérance''
|casualties1=1 wounded
|casualties2=33 killed, 48 wounded, ''Réunion'' captured
}}
The '''Action of 20 October 1793''' was a minor naval engagement of the [[French Revolutionary Wars]] fought off Cape Barfleur on the French coast of the [[English Channel]]. The early months of the war, which had begun in February, had seen a number of French [[frigates]] raiding British merchant shipping in the Channel, and [[HMS Crescent (1784)|HMS ''Crescent'']] under Captain [[James Saumarez]] was deployed to watch the port of [[Cherbourg]] with the aim of disrupting the operations of the French frigates [[French frigate Reunion (1786)|''Réunion'']] and [[French frigate Sémillante (1792)|''Sémillante'']] that were based in the harbour. On 20 October, Saumarez was waiting off Cape Barfleur for French movement when his lookout sighted ''Réunion'' and the [[cutter (ship)|cutter]] ''Espérance'' approaching from open water.

Saumarez immediately moved to engage the French ship and managed to isolate the frigate and subject it to a fierce barrage of fire for more than two hours. Captain [[François A. Dénian]] on ''Réunion'' responded, but aside from inflicting minor damage to Saumarez's rigging achieved little while his own vessel was heavily battered, suffering severe damage to rigging masts and hull and more than 80 and possibly as many as 120 casualties. British losses were confined to a single man wounded by an accident aboard ''Crescent''. Eventually Dénian could not hold out any longer and was forced to surrender on the arrival of the 28-gun British frigate [[HMS Circe (1785)|HMS ''Circe'']]. ''Réunion'' was later repaired and commissioned into the [[Royal Navy]], while Saumarez was [[Knight Bachelor|knighted]] for his success.

==Background==
At the outbreak of war between the [[Kingdom of Great Britain]] and the [[First French Republic|French Republic]] in the early spring of 1793, the [[French Revolutionary Wars]] were already a year old. The [[French Navy]] was already suffering from the upheavals of the [[French Revolution]] and the consequent dissolution of the professional officer class, while the [[Royal Navy]] had been at a state of readiness since the summer of 1792.<ref name="RW19">Woodman, p. 19</ref> During the early months of the war the French Navy focused heavily on raiding and disrupting British commerce and deployed [[frigates]] on raiding operations against British commercial shipping. In the [[English Channel]], two of the most successful raiders were the frigates [[French frigate Reunion (1786)|''Réunion'']] and [[French frigate Sémillante (1792)|''Sémillante'']], based in [[Cherbourg]] on the [[Cotentin Peninsula]]. These frigates would make short cruises, leaving Cherbourg in the early evening and returning in the morning with any prizes they had encountered during the night.<ref name="WJ104">James, p. 104</ref>

To counter the depredations from Cherbourg, the [[Admiralty]] despatched a number of warships to [[blockade]] the French coast, including the 36-gun frigate [[HMS Crescent (1784)|HMS ''Crescent'']] under Captain [[James Saumarez]], which was sent from [[Portsmouth]] to the [[Channel Islands]] before operating off the Cotentin.<ref name="WLC479"/> On 19 October, Saumarez learned of the French routine and took up a station close inshore near Cape Barfleur, a rocky headland on the eastern extremity of the Cotentin Peninsula which the Cherbourg raiders passed whenever leaving or entering port. At dawn on 20 October lookouts on ''Crescent'' reported two sails approaching the land from the Channel, one significantly larger than the other. Saumarez immediately ordered his ship to edge into the wind towards the strange vessels and rapidly came up on the [[port (nautical)|port]] side of the new arrivals, with the wind behind him allowing freedom of movement.<ref name="WJ104"/>

==Battle==
[[File:HMS Crescent, capturing the French frigate Réunion off Cherbourg, 20th October 1793.jpg|thumb|''H.M.S. Crescent, under the command of Captain James Saumarez, capturing the French frigate Réunion off Cherbourg, 20 October 1793'', att. [[John Christian Schetky]]]] The two ships encountered by ''Crescent'' were the 38-gun frigate ''Réunion'' and a 14-gun [[cutter (ship)|cutter]] named ''Espérance'', returning from a raiding cruise in the Channel under the command of Captain [[François A. Dénian]] (in some sources Déniau).<ref name="WLC479">Clowes, p. 479</ref> ''Réunion'' was a substantially larger ship than ''Crescent'', weighing {{convert|951|LT|MT|0}} to the British ship's {{convert|888|LT|MT|0}} and carrying 300 men to ''Crescent''<nowiki>'</nowiki>s 257. However these advantages were countered by the slight advantage Saumarez held in weight of shot, which measured {{convert|315|lb|kg|0}} to {{convert|310|lb|kg|0}} in favour of the British vessel. ''Crescent'' was also faster than ''Réunion'', having only recently completed a dockyard refit.<ref name="WLC479"/>

Firing broke out between the frigates at 10:30, while the cutter steered away from the battle towards Cherbourg. One other ship was visible throughout the engagement, the 28-gun British frigate [[HMS Circe (1785)|HMS ''Circe'']] under Captain [[Joseph Sydney Yorke]], which lay stranded approximately {{convert|9|nmi|km}} distant, unable to approach the battling ships due to calm winds separating ''Circe'' from the engagement.<ref name="WJ104"/> In the opening exchanges, both frigates suffered damage to their rigging and sails, ''Crescent'' losing the fore topmast and ''Réunion'' the fore yard and mizzen topmast. In an effort to break the deadlock, Saumarez suddenly swung his ship onto the opposite tack and, taking advantage of the damage to Dénian's vessel that left it unable to effectively manoeuvre, managed to fire several [[raking fire|raking broadsides]] into ''Réunion{{'}}''s stern.<ref name="WJ104"/>

The raking fire inflicted massive damage and casualties on the French ship, and although Dénian continued to resist for some time, his ship was no longer effectively able to respond once Saumarez had crossed his bow. Eventually, with ''Circe'' now rapidly approaching with a strengthening of the wind, Dénian accepted that he had no choice but to surrender his vessel after an engagement lasting two hours and ten minutes. The cutter, which had been ignored during the battle, successfully escaped to Cherbourg while the captain of ''Sémillante'', anchored in the harbour, made a fruitless effort to reach the engagement, delayed by contrary wind and tides that prevented the frigate from sailing.<ref name="WJ105">James, p. 105</ref>

==Aftermath==
Both ships were damaged in the engagement, although Saumarez's damage was almost entirely confined to his rigging: very few shots had actually struck his hull during the battle, and the only one that provoked notice passed across the deck without causing injury and struck a cannon on the opposite site, setting it off in the direction of a number of small [[gunboats]] that were approaching from the shore.<ref name="WJ104"/> Casualties on ''Crescent'' were equally light, with just one man injured; he had been standing too close to his own cannon during the opening broadside and had been struck by the recoiling gun, suffering a broken leg.<ref name="RW45">Woodman, p. 45</ref> Damage and losses on the French ship were very severe, with the rigging in tatters, the hull and lower masts repeatedly struck and casualties that Saumarez initially estimated at more than 120 men killed or wounded, although French accounts give the lower figure of 33 killed and 48 wounded.<ref name="WJ105"/>

Saumarez was widely praised for his conduct in only the second successful frigate action of the war after [[Edward Pellew]]'s capture of [[French frigate Cléopâtre|''Cléopâtre'']] four months earlier at the [[Action of 18 June 1793]].<ref name="RW27">Woodman, p. 27</ref> In reward, Saumarez was [[Knight Bachelor|knighted]] by [[George III of the United Kingdom|King George III]] and given a presentation plate by the City of London, although Saumarez later received a bill for £103 6[[shilling|s]] and 8[[old pence|d]] (the equivalent of £{{Formatnum:{{Inflation|UK|103|1793|r=-2}}|0}} as of {{CURRENTYEAR}}),{{Inflation-fn|UK}} from a Mr. Cooke for "the honour of a knighthood". Saumarez refused to pay, telling Cooke to charge whomever had paid for Edward Pellew's knighthood after his successful action. Saumarez later wrote to his brother that "I think it hard to pay so much for an honour which my services have been thought to deserve".<ref name="TW56">Wareham, p. 56</ref> In recognition of his success, Saumarez was subsequently given command of a frigate squadron operating against the Normandy coast from the Channel Islands.<ref name="RG52">Gardiner, p. 52</ref> In addition, the first lieutenant, George Parker, was promoted to commander and the two other lieutenants were also praised. ''Réunion'' was purchased for service with the Royal Navy after repairs had been completed, and became HMS ''Reunion'', rated as a 36-gun frigate carrying 12 pounder cannon. Authorisation for the payment of [[prize money]] was published in the ''[[London Gazette]]'' on 4 February 1794,<ref name="LG1">{{London Gazette|issue=13621|startpage=120|date=4 February 1794}}</ref> amounting to £5,239 (the equivalent of £{{Formatnum:{{Inflation|UK|5239|1794|r=-2}}|0}} as of {{CURRENTYEAR}}){{Inflation-fn|UK}} divided between the men of ''Crescent'' and ''Circe''.<ref name="TW46">Wareham, p. 46</ref> More than five decades later the battle was among the actions recognised by a clasp attached to the [[Naval General Service Medal (1847)|Naval General Service Medal]], awarded upon application to all British participants from ''Crescent'' still living in 1847.<ref name="LG4">{{LondonGazette|issue=20939|startpage=236|date=26 January 1849|accessdate=8 February 2010}}</ref>

==References==
{{reflist|2}}

==Bibliography==
* {{cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997 |origyear=1900
 | chapter =
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume IV
 | publisher = Chatham Publishing
 | location =
 | isbn = 1-86176-013-2
}}
* {{cite book
 | editor =Gardiner, Robert
 | authorlink =
 | year = 2001 |origyear=1996
 | chapter =
 | title = Fleet Battle and Blockade
 | publisher = Caxton Editions
 | location =
 | isbn = 1-84067-363-X
}}
* {{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002 |origyear=1827
 | chapter =
 | title = The Naval History of Great Britain, Volume 1, 1793–1796
 | publisher = Conway Maritime Press
 | location =
 | isbn = 0-85177-905-0
}}
* {{cite book
 | last = Wareham
 | first = Tom
 | authorlink =
 | year = 2001
 | chapter =
 | title = The Star Captains, Frigate Command in the Napoleonic Wars
 | publisher = Chatham Publishing
 | location =
 | isbn = 1-86176-169-4
}}
* {{cite book
 | last = Woodman
 | first = Richard
 | authorlink = Richard Woodman
 | year = 2001
 | chapter =
 | title = The Sea Warriors
 | publisher = Constable Publishers
 | location =
 | isbn = 1-84119-183-3
}}

{{coord missing}}

[[Category:Conflicts in 1793]]
[[Category:Naval battles involving France]]
[[Category:Naval battles of the French Revolutionary Wars]]
[[Category:Naval battles involving Great Britain]]
[[Category:October 1793 events]]