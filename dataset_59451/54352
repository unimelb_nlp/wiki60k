{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Kent Town
| city                = Adelaide
| state               = SA
| image               = 
| caption             = 
| pop                 = 1,151
| pop_year = {{CensusAU|2006}}
| pop_footnotes       = <ref name="ABS">{{Census 2006 AUS|id =SSC41866|name=Kent Town (State Suburb)|accessdate=12 January 2011| quick=on}}</ref>
| est                 = 
| postcode            = 5067
| area                = 0.6
| dist1               = 
| dir1                = E
| location1           = Adelaide CBD
| lga                 = City of Norwood Payneham St Peters
| stategov            = [[Electoral district of Dunstan|Dunstan]]
| fedgov              = [[Division of Adelaide|Adelaide]]
| near-n              = [[College Park, South Australia|College Park]]
| near-ne             = [[Stepney, South Australia|Stepney]]
| near-e              = [[Norwood, South Australia|Norwood]]
| near-se             = [[Rose Park, South Australia|Rose Park]]
| near-s              = [[Adelaide city centre|Adelaide]]
| near-sw             = [[Adelaide city centre|Adelaide]]
| near-w              = [[Adelaide city centre|Adelaide]]
| near-nw             = [[Hackney, South Australia|Hackney]]
}}

'''Kent Town''' is an inner urban suburb of [[Adelaide]], [[South Australia]]. It is located in the [[City of Norwood Payneham St Peters|City of Norwood Payneham & St Peters]].

==History==
The suburb was named after Dr Benjamin Archer Kent, who established a farm and flour mill on which the suburb now stands.<ref>[http://www.slsa.sa.gov.au/manning/pn/k/k3.htm#kentT The Manning Index of South Australian History - Kent Town]</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 1,151 persons in Kent Town on census night. Of these, 57.1% were male and 42.9% were female.<ref name="ABS"/>

The majority of residents (62.3%) are of Australian birth, with other common census responses being [[Malaysia]] (3.8%), [[India]] (3.7%), [[China]] (3.7%) and [[England]] (3.6%).<ref name="ABS"/>

The age distribution of Kent Town residents is comparable to that of the greater Australian population. 66.2% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 33.8% were younger than 25 years, compared to the Australian average of 33.5%.<ref name="ABS"/>

==Community==

===Schools===
[[Prince Alfred College]] is located in the suburb.

==Attractions==
Kent Town is the site of a former brewery, now redeveloped into apartments.

==Transport==

===Roads===
The suburb is serviced by the following main roads:
*[[Dequetteville Terrace, Adelaide|Dequetteville Terrace]], forming the western boundary of the suburb.
*[[Fullarton Road, Adelaide|Fullarton Road]], running north-south between [[Norwood, South Australia|Norwood]] and [[Springfield, South Australia|Springfield]]
*Rundle Street, cutting horizontally across the middle of Kent Town.

===Public transport===
Kent Town is serviced by public transport run by the [[Adelaide Metro]].

==See also==
*[[List of Adelaide suburbs]]

==References==
{{Reflist}}

==External links==
*[http://www.npsp.sa.gov.au City of Norwood Payneham & St Peters]
*[http://www.lga.sa.gov.au/site/page.cfm?c=4141 Local Government Association of SA - City of Norwood Payneham & St Peters]
*[http://www.censusdata.abs.gov.au/ABSNavigation/prenav/ProductSelect?textversion=false&areacode=4&subaction=-1&action=201&period=2006&collection=census&navmapdisplayed=true&breadcrumb=L& 2006 ABS Census Data by Location]

{{Coord|-34.920278|138.619167|format=dms|type:city_region:AU-SA|display=title}}
{{City of Norwood Payneham St Peters suburbs}}

[[Category:Suburbs of Adelaide]]