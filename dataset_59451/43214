{|{{Infobox aircraft begin
 | name=M-7, MX-7, MXT-7
 | image=Maule.m7-235b.arp.jpg
 | caption=M-7-235 Super Rocket
}}{{Infobox aircraft type
 | type=Utility aircraft
 | national origin=United States
 | manufacturer=[[Maule Air]]
 | designer=[[Belford Maule]]
 | first flight=1984
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=ca. 500 by 1995
 | developed from=
 | variants with their own articles=
}}
|}
[[File:Maule.mx-7-235.superrocket.g-iton.arp.jpg|thumb|right|Maule M-7-235]]
[[File:Maule M-7-235C (N1936P).jpg|thumb|right|Maule M-7-235C on [[Amphibious aircraft|amphibious floats]]]]
[[File:Maule M-7-235C Orion C-FSRA 04.JPG|thumb|right|Maule M-7-235C on [[tundra tire]]s]]
[[File:Maule M-7-260C Orion C-GZAU 01.JPG|thumb|right|Maule M-7-260C Orion]]

The '''Maule M-7''' is a family of single-engine light aircraft that has been manufactured in the United States since the mid-1980s.

==Design and development==
Based on the [[Maule M-4]], it is a high-wing, strut-braced [[monoplane]] of conventional configuration, available with [[conventional landing gear|tailwheel]] or optional [[Tricycle gear|tricycle wheeled undercarriage]]<ref name="M7options">{{cite web|url = http://www.mauleairinc.com/pdf/literature/standard_equipment.pdf|title = Standard Equipment - MX(T)-7 Series and M(T)-7 Series Maule Aircraft|accessdate = 17 June 2012|last = [[Maule Air]]|date = 1 January 2012}}</ref> and frequently used  as a [[seaplane]] with twin pontoons. The basic M-7 has a longer cabin than its predecessors the M5 & M6, with two seats in front, a bench seat for up to three passengers behind them, and (like the [[Maule M-6|M-6]]) an optional third row of "kiddie seats" at the rear.<ref name="Simpson 243">Simpson 1995, 242</ref> Extra cabin windows can be fitted if the "kiddie seats" are to be used. The '''MX-7''' uses the same fuselage as the M-6,which is a modified M5 fuselage but the same wing span as the [[Maule M-5|M-5]],<ref name="Simpson 243"/><ref name="JAWA 85">''Jane's All the World's Aircraft 1985–86'', 448</ref> and incorporates the increased fuel tankage, hoerner style wing tip  and five-position flaps designed for the M-7.<ref name="Private">"Private Aircraft Buyers' Guide" 1986, 39.</ref>

The M-7 family has been produced both with piston and turboprop engines.<ref name="Simpson 243"/><ref name="JAWA 85"/><ref name="Turboprops">"Maule develops turboprops" 1987, 16</ref>
<!-- ==Development== -->
<!-- ==Operational history== -->

==Variants==

===M-7 series===
;M-7-235 Super Rocket
:Similar to M-6-235 with lengthened cabin. Tailwheel undercarriage and [[Lycoming O-540]] engine<ref name="Simpson 243" />
;M-7-235B Super Rocket<ref name="WAIF">''World Aircraft Information Files'', File 901 Sheet 08</ref>
:Same as M7-235 including Oleo-Strut main landing gear.
;M-7-235C Orion<ref name="WAIF" />
:Same as M7-235B but with sprung aluminum main landing gear and Lycoming IO-540 engine.
;M-7-260<ref name="WAIF" />
;M-7-260C<ref name="WAIF" />
;M-7-420 Starcraft Turboprop
:M-7-235 with Allison 250 turboprop engine<ref name="Simpson 243" />
;MT-7-235 Tri-Gear
:Super Rocket with tricycle undercarriage<ref name="Simpson 243" />
;MT-7-260<ref name="WAIF" />

===MX-7 series===
;MX-7 Rocket<ref name="WAIF" />
;MX-7-160 Sportplane
:M-6 fuselage with M-5 wings. [[Lycoming O-320]] engine<ref name="Simpson 243" /><ref name="WAIF" />
;MX-7-180 Star Rocket
:MX-7 with lengthened cabin. Optional third row of seats with windows. [[Lycoming O-360]] engine<ref name="Simpson 243" />
;MX-7-180A Sportplane and Comet
;MX-7-180B Star Rocket<ref name="WAIF" />
;MX-7-180C Millennium<ref name="WAIF" />
;MX-7-250 Starcraft
:MX-7 with [[Allison 250]] turboprop engine<ref name="Simpson 243" />
;MX-7-420 Starcraft Turboprop
:MX-7-235 with Allison 250 turboprop engine<ref name="Simpson 243" />
;MXT-7-160 Comet
:MX-7-160 with tricycle undercarriage
;MXT-7-180 Star Rocket
:MX-7-180 with tricycle undercarriage<ref name="Simpson 243" />
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (M-7-235)==
{{aerospecs
|ref=<!-- reference -->''Jane's All the World's Aircraft 1985–86'', p. 449
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->eng
|crew=One pilot
|capacity=4 passengers
|length m=7.16
|length ft=23
|length in=6
|span m=10.21
|span ft=33
|span in=6
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=1.89
|height ft=6
|height in=3
|wing area sqm=15.6
|wing area sqft=168
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=681
|empty weight lb=1,500
|gross weight kg=1,134
|gross weight lb=2,500
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Lycoming O-540|Lycoming IO-540-W]]
|eng1 kw=<!-- prop engines -->175
|eng1 hp=<!-- prop engines -->235
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->265
|cruise speed mph=<!-- if max speed unknown -->164
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=1,610
|range miles=1,001
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{Commons category}}
* {{cite book |title=Jane's All the World's Aircraft 1985–86 |publisher=Jane's Publishing |location=London }}
* {{cite journal |title=Maule develops turboprops |journal=[[Flight International]] |date=2 May 1987 |url=http://www.flightglobal.com/pdfarchive/view/1987/1987%20-%200292.html |accessdate=2008-12-27}}
* {{cite journal |title=Private Aircraft Buyers' Guide |journal=[[Flight International]] |date=15 March 1986 |pages=37–48 |url=http://www.flightglobal.com/pdfarchive/view/1986/1986%20-%200581.html |accessdate=2008-12-27}}
* {{cite book |last= Simpson |first= R. W. |title=Airlife's General Aviation |year=1995 |publisher=Airlife Publishing |location=Shrewsbury |isbn= 1-85310-577-5 }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |isbn= 0-7106-0710-5}}
* {{cite book |title=World Aircraft Information Files |publisher=Bright Star Publishing|location=London |isbn=1-156-94382-5 }}
<!-- ==External links== -->

{{Maule aircraft}}
{{Use dmy dates|date=January 2012}}

[[Category:United States civil utility aircraft 1980–1989]]
[[Category:Maule aircraft|M-07]]
[[Category:Single-engine aircraft]]
[[Category:1984 introductions]]