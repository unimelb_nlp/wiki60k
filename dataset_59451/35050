{{Infobox storm
| name = Windsor Locks, Connecticut tornado
| image = Windsor Locks tornado damage.jpg
| alt = 
| caption = Destroyed houses in Windsor Locks.  Image by Windsor Locks Fire Department.
| formed = 3:00 PM [[Eastern Daylight Time|EDT]], 3 October, 1979
| active =
| dissipated =
| lowest pressure =  
| lowest temperature =
| tornadoes = 
| fujitascale = F4
| tornado duration = 
| highest winds = 
| gusts =
| maximum snow = 
| power outages =
| casualties =  3 fatalities
| damages = $442 million (1997 [[United States dollar|USD]])<ref name="costliest">{{cite journal|last=Harold E.|first=Brooks|last2=Doswell |first2=Charles A., III|date=February 2001|title=Normalized Damage from Major Tornadoes in the United States: 1890–1999|journal=Weather and Forecasting|publisher=[[American Meteorological Society]]|volume=16|pages=168&ndash;176|url=http://ams.allenpress.com/archive/1520-0434/16/1/pdf/i1520-0434-16-1-168.pdf|accessdate=2009-06-03|doi=10.1175/1520-0434(2001)016<0168:NDFMTI>2.0.CO;2|bibcode = 2001WtFor..16..168B }}</ref><br>(${{Inflation|US|442|1997}} million {{CURRENTYEAR}} USD)
| affected = North-central [[Connecticut]]
| location = 
| current advisories =
| enhanced =
| notes = 
}}
The '''Windsor Locks, Connecticut tornado''' struck the towns of [[Windsor, Connecticut|Windsor]], [[Windsor Locks, Connecticut|Windsor Locks]], and [[Suffield, Connecticut]] and Feeding Hills, Massachusetts on October 3, 1979.  The short-lived but intense [[tornado]] struck without warning and caused three deaths and 500 injuries.

The storm, rated F4 on the [[Fujita scale]], also caused more than $400 million in property damage along an {{convert|11.3|mi|km|adj=on}} path, and ranks as the ninth most destructive tornado in American history.<ref name="severe plot">Data from the [[Storm Prediction Center]] archives, which are accessible through [http://www.spc.noaa.gov/software/svrplot2/ SeverePlot], free software created and maintained by [[John Hart (meteorologist)|John Hart]], lead forecaster for the SPC.</ref>

==Storm synopsis==
The storm system that caused the tornado had produced severe weather, including two weak tornadoes, in eastern [[Pennsylvania]] and [[New Jersey]] that morning.<ref name="MWR">{{cite journal| last = Riley| first = Gary T.|author2=Bosart, Lance F. |date=August 1987| title = The Windsor Locks, Connecticut Tornado of 3 October 1979: An Analysis of an Intermittent Severe Weather Event| journal = [[Monthly Weather Review]]| volume = 115| issue = 8| pages = 1655–1677| publisher = [[American Meteorological Society]]| doi = 10.1175/1520-0493(1987)115<1655:TWLCTO>2.0.CO;2| url = http://ams.allenpress.com/archive/1520-0493/115/8/pdf/i1520-0493-115-8-1655.pdf| format = PDF| accessdate = 2009-06-04|bibcode = 1987MWRv..115.1655R }} {{Dead link|date=September 2010|bot=H3llBot}}</ref> This was an unusual setup for a significant tornado, associated with a [[warm front]] near a [[low-pressure area|low-pressure]] center. A thunderstorm cell formed south of [[Long Island]] around 10:20 am, and became a [[supercell]] sometime later after interacting with a surface low-pressure center.<ref name="MWR"/> It turned north as a left-moving supercell, meaning it moved left with respect to the mean atmospheric flow. Left-moving supercells are very rare, as cyclonic storms usually turn to the right of the mean flow.<ref>[http://www.srh.noaa.gov/bmx/significant_events/2004/04_07/index.php Anticyclonic (Left-Moving) Supercell] [[National Weather Service]] Birmingham, Alabama. Accessed 2009-06-05.</ref> It is unknown whether this leftward movement was due to an atmospheric interaction or [[Orographic|terrain-induced]] movement, as the storm moved straight up the [[Connecticut River]] valley.<ref name="MWR"/>

[[File:Windsor Locks tornado 1900z satellite.PNG|thumb|250px|Infra-red [[weather satellite]] image of the northeastern United States at 3 pm local time (the same time the tornado touched down)]]
No [[tornado watch]]es or [[tornado warning|warnings]] were issued before the storm struck. This was later determined to be because of missing [[atmospheric sounding]] data, as well as an incorrect assessment of the height of the [[tropopause]], which led to an underestimation of the strength of the thunderstorm which produced the tornado.<ref name="MWR"/> Although a [[severe thunderstorm warning]] was issued at 2:57 pm, very few people received the warnings in time.<ref name="WJ">{{cite web| title = It's Been Five Years Tornado's Destruction Never to Be Forgotten| url = http://www.zwire.com/site/news.cfm?newsid=13050589&BRD=1633&PAG=461&dept_id=11608&rfi=6| publisher = [[Windsor Journal]]| pages = 5–6| date = 1984-10-05| accessdate = 2009-06-05}}</ref>

The tornado touched down in [[Poquonock, Connecticut]], a village in the town of Windsor, just north of [[Hartford, Connecticut|Hartford]]. Poquonock Elementary School was heavily damaged; fortunately, students were sent home early at 1:30 pm on Wednesdays.<ref name="SPT"/> Students at a [[Brownie (Girl Guides)|Brownie]] meeting were led into a hallway just before the auditorium they had been in was destroyed.<ref name="LJ">{{cite news|url=https://news.google.com/newspapers?id=PV4gAAAAIBAJ&sjid=JmUFAAAAIBAJ&pg=4830%2C358056|title=These people share common nightmare|author=Associated Press|date=1979-11-03|publisher=[[Lewiston Journal]]|location=[[Lewiston, Maine|Lewiston-Auburn, Me.]]|page=2A|accessdate=2015-11-01|via=[[Google News Archive]]}}</ref> The historic Poquonock Community Church building had its roof ripped off.  Miraculously, all but one of the stained glass windows from the old church were salvaged.<ref name="PCC">{{cite web| title = Poquonock Community Church History| url = http://www.pccwindsor.com/history.html| accessdate = 2014-07-14}}</ref> The tornado traveled almost due north, an unusual direction for a tornado. The most severe damage occurred along River Road, Hollow Brook Road, Pioneer Drive and Settler Circle, where large [[Framing (construction)|frame houses]] were left "in splinters".<ref name="WJ"/> The tornado roughly followed [[Connecticut Route 75]] just east of [[Bradley International Airport]]. The airport's weather station recorded a wind gust of {{convert|39|m/s|mph km/h|abbr=on}} as the tornado passed nearby.<ref name="MWR"/> A [[United Airlines]] flight with 114 passengers was attempting to land as the tornado was passing the airport; the pilot saw the tornado and was able to [[Go-around|abort the landing]] just in time.<ref>{{cite news|url=https://news.google.com/newspapers?id=FqQSAAAAIBAJ&sjid=LvkDAAAAIBAJ&pg=1882%2C1564816 | title=As Jet Is About to Land on Tornado, 'It's Not Noisy When All Are Praying'|author=[[Associated Press]]|date=1979-10-05|publisher=[[Spokane Daily Chronicle]]|location=[[Spokane, Washington|Spokane, Wash.]]|page=1|accessdate=2015-11-01|via=[[Google News Archive]]}}</ref> The tornado then crossed the northern portion of the airport, where the [[New England Air Museum]] was located. More than 20 vintage aircraft were completely destroyed, with many more damaged. The museum's hangar was also rendered unusable.<ref name="NEAM1">{{cite web| title = Tornado!| url = http://www.neam.org/tornado1.htm| archiveurl = https://web.archive.org/web/19980710071633/http://neam.org/tornado1.htm| publisher = [[New England Air Museum]]| location = [[Windsor Locks, Connecticut]]| archivedate = 1998-07-10| accessdate = 2009-06-05}}</ref> The tornado moved north into Feeding Hills before dissipating near the Westfield city line, about five miles north of the [[Massachusetts]] state line.

The tornado was accompanied by more than {{convert|7|cm|in}} of rain, and several instances of [[downburst]] winds.<ref name="MWR"/> Damage from downburst winds was reported across the Connecticut River in [[Enfield, Connecticut|Enfield]].<ref>{{cite web|url= http://www4.ncdc.noaa.gov/cgi-win/wwcgi.dll?wwevent~ShowEvent~16505|title= Event Record Details 03 Oct 1979, 1345 CST|accessdate= 2009-06-05|work= NCDC Storm Events Database|publisher= [[National Climatic Data Center]]}}</ref>

==Aftermath==
Because there were no tornado warnings before the storm (and it occurred in an area where tornadoes are rare), the initial damage reports claimed an explosion had damaged a roof.<ref name="WJ"/> Soon, however, the storm's nature and impact became apparent. Governor [[Ella Grasso]] lived just a block away from the tornado's path, though she was in Hartford at the time of the storm. She declared an 8 pm&ndash;5 am curfew in the days following the tornado.<ref name="SPT">{{cite news|url=https://news.google.com/newspapers?id=cwIOAAAAIBAJ&sjid=cHwDAAAAIBAJ&pg=3038,1613495&dq=hartford+tornado|title=Connecticut tornado kills one, injures 400|last=AP, UPI|date=1979-10-04|publisher=[[St. Petersburg Times]]|pages=1, 16|accessdate=2009-06-08}}</ref><ref name="Ledger">{{cite news|url=https://news.google.com/newspapers?id=nNsvAAAAIBAJ&sjid=DPsDAAAAIBAJ&pg=5976%2C1285193|title=Tornado-ravaged area declared disaster|author=Associated Press|date=1979-10-05|publisher=[[The Ledger]]|location=[[Lakeland, Florida|Lakeland, Fla.]]|page=5|accessdate=2015-11-01|via=[[Google News Archive]]}}</ref> About 500 [[National Guard (United States)|National Guardsmen]] were activated to prevent [[looting]] and direct traffic, and the area was declared a [[disaster area]] by [[Jimmy Carter|President Carter]].<ref name="Ledger"/> [[FEMA trailer]]s were provided within a few days, and were used by many residents until reconstruction or repairs could be completed.<ref name="WJ"/><ref name="LJ"/> In all, at least 38 businesses were damaged or destroyed, 65 homes were completely destroyed, and at least 75 homes were damaged. Twenty-five [[tobacco shed]]s were "extensively damaged".<ref name="Toledo">{{cite news|url=https://news.google.com/newspapers?id=jU5PAAAAIBAJ&sjid=mwIEAAAAIBAJ&pg=3259%2C1274145|title=Connecticut Ravaged by Tornado|author=Associated Press|date=1979-10-04|publisher=[[Toledo Blade]]|page=1|accessdate=2015-11-01|via=[[Google News Archive]]}}</ref> At the airport, at least 30 vintage aircraft were damaged or destroyed, as well as most of the state's National Guard helicopters.<ref name="NEAM1"/><ref name="Toledo"/> The final damage total reached $200 million (1979 [[United States dollar|USD]]), or $442 million in 1997 dollars.<ref name="costliest"/>

Because of the vast scope of the damage, initially Windsor town officials feared many, possibly even hundreds, of people could have been killed.<ref name="WJ"/> While there were many serious injuries, only three people were killed by the storm. Two victims, construction workers working in a bank parking lot, took shelter in a work truck when they saw the storm approach.  The first victim was killed immediately by a piece of flying lumber, the other died a few weeks later from his injuries becoming the 3rd victim.<ref name="WJ"/> The second victim was found the next day across the street from her obliterated house.<ref name="WJ"/><ref>{{cite news|url=http://pqasb.pqarchiver.com/courant/access/981508722.html?dids=981508722:981508722&FMT=ABS&FMTS=ABS:AI&date=Oct+22%2C+1979&author=&pub=The+Hartford+Courant&desc=Twister+Injuries+Fatal+to+Laborer&pqatl=google|title=Twister Injuries Fatal to Laborer|date=1979-10-22|publisher=[[The Hartford Courant]]|pages=10|accessdate=2009-06-05}}</ref> Over 400 people were hospitalized, mostly for injuries from flying glass or the victims' having been thrown by winds.<ref name="SPT"/>

==Records==
The tornado was the costliest on record in the [[Northeastern United States]], and the 10-costliest in US history.<ref name="costliest"/> The three people who were killed made it the deadliest tornado in Connecticut since the [[1878 Wallingford tornado]].<ref name="tornado project">[[Tornado Project]]. [http://www.tornadoproject.com/alltorns/worstts.htm#CT "Worst" Tornadoes] Retrieved on July 2, 2007.</ref>

==See also==
*[[Tornadoes of 1979]]
*[[List of North American tornadoes and tornado outbreaks]]
*[[List of Connecticut tornadoes]]

==References==
{{Reflist}}

==External links==
*[http://www.wlfd.com/photos/oct-3-1979-tornado-bia More pictures from the Windsor Locks Fire Department]
*[https://news.google.com/newspapers?id=jU5PAAAAIBAJ&sjid=mwIEAAAAIBAJ&pg=3259%2C1274145 Connecticut Ravaged by Tornado]
{{10 costliest US tornadoes}}
{{1979 tornado outbreaks}}

{{good article}}

{{DEFAULTSORT:1979-10-03 Windsor Locks, Connecticut tornado}}
[[Category:F4 tornadoes]]
[[Category:Tornadoes of 1979]]
[[Category:Tornadoes in Connecticut]]
[[Category:1979 in Connecticut]]
[[Category:1979 natural disasters in the United States]]