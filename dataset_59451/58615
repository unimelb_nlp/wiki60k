{{italic title}}
{{Infobox Journal
| title        = The Journal of Theological Studies
| cover        = [[File:Journal of Theological Studies.gif|150px]]
| editor       = [[Graham Gould]], [[Katharine Dell]]
| discipline   = [[Theology]]
| abbreviation = J. Theol. Stud.
| publisher    = [[Oxford University Press]]
| country      = [[United Kingdom]]
| frequency    = Biannually
| history      = 1899-present
| openaccess   =
| impact       =
| impact-year  =
| website      = http://jts.oxfordjournals.org/
| link1        =
| link1-name   =
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 0022-5185
| eISSN        = 1477-4607
}}
'''''The Journal of Theological Studies''''' is an [[academic journal]] established in 1899 and now published by [[Oxford University Press]] in April and October each year. It publishes theological research, scholarship, and interpretation, and hitherto unpublished ancient and modern texts, inscriptions, and documents. Volumes I to L (the Old Series) span 1899 to 1949, while volumes 1 to 63 (the New Series) span 1950 to 2012. {{As of|2012}}, the editors are [[Graham Gould]], who oversees the articles and book reviews in non-biblical fields of study (including patristics, church history, and systematic theology), and [[Katharine Dell]] ([[Reader in Old Testament Literature and Theology, Faculty of Divinity, University of Cambridge and Fellow of St Catharine's College]]), who oversees articles and book reviews in biblical studies and closely related fields. Previous editors have included the patristic scholars [[James Bethune-Baker]], [[Henry Chadwick (theologian)|Henry Chadwick]], and [[Maurice Wiles]], and the biblical scholars [[Robert Lightfoot (priest)|Robert Lightfoot]], [[Hedley F. D. Sparks]], [[G. B. Caird]], [[Morna Hooker]], [[John Barton (theologian)|John Barton]], and [[John Muddiman]].

== See also ==
* [[List of theological journals]]

== External links ==
* {{Official website|http://jts.oxfordjournals.org/}}
* [https://archive.org/search.php?query=Journal%20of%20theological%20studies%20AND%20collection%3Aamericana Internet Archive]

{{DEFAULTSORT:Journal Of Theological Studies, The}}
[[Category:Religious studies journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Biannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1899]]


{{reli-journal-stub}}