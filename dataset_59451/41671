{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox military person
| name= Hendrik Frederik Prinsloo
| image = 
| birth_date = {{Birth date|df=yes|1890|08|18}}
| death_date = {{Death date and age|df=yes|1966|11|20|1890|08|18}}
| birth_place =[[Carolina, Mpumalanga|Carolina]], [[South African Republic]]
| death_place =[[Ermelo, Mpumalanga|Ermelo]], [[Mpumalanga|Eastern Transvaal]]
| placeofburial_coordinates =
| nickname =
| allegiance ={{flagcountry|Union of South Africa}}
| branch =[[South African Army]]
| serviceyears =
| rank =[[Colonel]]
| unit =[[Regiment Botha]]
| commands =[[Zonderwater POW camp]]
| battles =South West Africa 1914<br>1916 France<br>1922 Bondelswart Rebellion
| awards =[[OBE]], [[Efficiency Decoration|ED]], [[Croix de Guerre]] avec Palmes, [[Mentioned in dispatches]]
| relations = Stephanie Cecilia Weidner, d. 19 November 1941 (two children)<br>Grace Madeleine Sedgwick
| laterwork =Farming<br>Provincial politics
}}

As a twelve-year-old boy '''Hendrik Frederik Prinsloo''' was interned by the British in a [[concentration camp]] during the [[Anglo-Boer War]] but served alongside the British in the South African forces during the two World Wars. He is best remembered for the humanitarian manner in which he, as [[Commandant]], ran the Zonderwater Italian POW camp.

==Early life==
Hendrik Frederik Prinsloo was the son of [[Commandant]] Hendrik Frederik Prinsloo (1861–1900), who commanded the Carolina [[Boer Commando]] at the [[Battle of Spion Kop]] and was killed in action at [[Battle of Leliefontein|Witkloof]], and his wife, Cecilia Maria Steyn. In 1926, [[Boer]] and [[United Kingdom|Briton]] erected a monument to commemorate his [[Courage|gallantry]] at Witkloof.<ref name="Middlesex">{{cite journal | title = Forty-seven Years after Spion Kop | journal = Military History Journal | volume = 1 | issue = 1 | publisher = The South African Military History Society | location = Johannesburg | date = December 1967 | url = http://samilitaryhistory.org/vol011jb.html | id = SA | accessdate = 20 December 2008}}</ref>

He was also a direct descendant of Hendrik Frederik Prinsloo (1784–1816), who was involved in the [[Slachter's Nek Rebellion|Slagter's Nek rebellion]] in 1815.<ref name="Middlesex"/>

As a boy of twelve he was taken [[prisoner-of-war]] by the British while carrying arms in his father's [[Boer Commando|Commando]] (the Carolinaers) during the [[Second Boer War]]. Because of his tender age, he was lodged in the [[Barberton, Mpumalanga|Barberton]] [[List of concentration and internment camps|concentration camp]] with his mother.<ref name="Middlesex"/>

After the war he became a [[magistrate]]'s court interpreter in [[Natal Province]] and eventually farmed in the Carolina district.<ref name="DSAB">{{cite encyclopedia | title = Prinsloo, Hendrik Frederik | encyclopedia = Dictionary of South African Biography | volume = III | pages = | publisher = Human Sciences Research Council | year = 1981 | isbn = 0-409-09183-9}}</ref>

==Military career==
During the [[Maritz Rebellion|Boer Revolt]] of 1914 Prinsloo sided with the South African government forces. On 12 January 1915 he was appointed "honorary lieutenant, supernumerary list (Active [[Union Defence Force (South Africa)|Citizen Force]])". During his subsequent service in [[German South-West Africa]] he became [[aide-de-camp]] to [[Colonel Commandant]] WR Collins from January to August 1915.<ref name="DSAB"/>

On 26 November 1915 he joined the 1st Regiment, Military Constabulary.

Although no specific reference to his service in France can be found, it would appear that he served with the South African Brigade which formed part of the [[9th (Scottish) Division]].<ref>[http://www.1914-1918.net/9div.htm The Long, Long Trail – The British Army of 1914–1918–for family historians]</ref> In his address to a 1947 reunion of Middlesex Regiment officers, he mentioned that members of the regiment had served under his command.<ref name="Middlesex"/> The 3/10th Battalion (The Middlesex Regiment) landed at Le Havre on 1 June 1917 and was attached to South African Brigade.<ref>[http://www.1914-1918.net/msex.htm The Long, Long Trail – The British Army of 1914–1918–for family historians]{{dead link|date=March 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> He was awarded the French '''[[Croix de Guerre]] avec Palmes'''<ref name="DSAB"/>

On 31 December 1919 he resigned from the army and assumed an appointment with the Permanent Police Force as headquarters sub-inspector. Later he settled in [[Windhoek]] where he became commander of the Windhoek Town and District Police.

In 1922, as a lieutenant in the Military Constabulary in [[South West Africa]] he took a leading part in ending the [[Bondelswarts affair|Bondelswart Rebellion]]. He was the leader of the force which tracked down and killed the rebel leader Abraham Morris. This brought about the subsequent [[Surrender (military)|surrender]] of the Bondel [[Insurgency|insurgents]].<ref name="Middlesex"/>

At the outbreak of World War II, he was [[Commanding Officer]] of the [[Regiment Botha]]. As he was about to depart for North Africa with his regiment, he was recalled at the instance of [[Field Marshal]] [[JC Smuts]] who was the Prime Minister and [[Minister of Defence]] and placed in command of the vast Italian [[prisoner of war camp]] at Zonderwater (also spelt Sonderwater) near [[Cullinan, Gauteng|Cullinan]].<ref name="Middlesex"/>

In 1947 he represented South Africa at the Diplomatic Conference held in [[Geneva]] for the purpose of revising the International Convention Relative to the Treatment of Prisoners-of-War, that is, the [[Geneva Convention (1929)|Geneva Convention]] of 1929.<ref name="Middlesex"/>

On 28 August 1947 he was transferred to the reserve of officers.<ref name="DSAB"/>

==Zonderwater POW camp==
[[File:ZONDERWATER WELFARE OFFICES.jpg|thumb|right|300px|Theatre, library, shop and welfare offices complex at Zonderwater POW camp]]
On 12 November 1941 Prinsloo was posted to No 8 Prisoner of War Battalion, [[First Reserve Brigade (South Africa)|First Reserve Brigade]]. In December 1942 he assumed duty as assistant camp commandant of the prisoner-of-war camp, Zonderwater, and in the following year he was promoted acting [[colonel]], an appointment he held until 1 April 1947, when he was released and taken on as supernumerary to the establishment.<ref name="DSAB"/>

Zonderwater camp, which can more accurately be described as a city, was the largest of the eighteen known [[World War II]] Italian POW camps and held nearly a hundred thousand prisoners of war before it closed down on 1 January 1947.<ref>[http://www.af.mil.za/NEWS/2005/zonderwater.htm South African Air Force official website]</ref> The appointment of Colonel Hendrik Prinsloo [[Order of the British Empire|OBE]] as [[commanding officer]] was inspired. As a result of his efforts Zonderwater became one of the best functioning military camps in South Africa.<ref name="Blumberg">{{cite journal | last = Blumberg ED | first = Lt-Col L | title = Italian POW in South Africa (Medical Services) | journal = Military History Journal | volume = 1 | issue = 4 | publisher = The South African Military History Society | location = Johannesburg | date = June 1969 | url = http://samilitaryhistory.org/vol014lb.html | id = SA | accessdate = 20 December 2008}}</ref>

There was an improvement in living conditions, which included the establishment of an orchestra and a 10 000 book library, craft exhibitions and art classes. Illiteracy dropped from 30% to 2%. In short, life at Zonderwater was as close to civilian life for the prisoners, all due to Colonel Prinsloo who cared for his charges.<ref name="McLennan">{{cite journal | title = NEWSLETTER NO. 325 – SEPTEMBER 2005 | publisher = The South African Military History Society | location = Cape Town | date = September 2005 | url = http://samilitaryhistory.org/5/c05sepne.html | id = SA | accessdate = 20 December 2008 | issn = 0026-4016}}</ref>

==Recognition==
He was awarded the French ''[[Croix de Guerre]] avec Palmes'' and was [[Mentioned in Despatches]] for services in the [[First World War]]. He received the [[Efficiency Decoration]] on 11 April 1944 and was made an Officer of the Military Division of the [[Order of the British Empire]] on 14 June 1945.<ref name="DSAB"/>

His efforts were recognised by the post-war Italian Government on 25 November 1949 when he, as the Camp Commandant, and three of his officers were invested with the '''Order of the Star of Italy''' '''(Stella della Solidarieta)'''. The award was made to those who had specially contributed to the re-building of post-war Italy.<ref name="DSAB"/> Prinsloo was further recognised by the award of the '''Order of Good Merit''' '''(Ordine di Bene Merente)''' by the Pope.<ref>[http://members.iinet.net.au/~gduncan/1941.html George Duncan’s Lesser Known Facts of World War II]</ref>

==Civilian life==
Between World War I and World War II he was a farmer in the [[Ermelo, Mpumalanga|Ermelo]] in the [[Mpumalanga|Eastern Transvaal]] district.<ref name="Middlesex"/>

He was at one time president of the Eastern Transvaal Agricultural Union and distinguished himself at sport, particularly at horse-riding.<ref name="DSAB"/>

In 1940 he doffed his uniform to win the Provincial Council by-election in the [[constituency]] of Carolina, and represented it in the Transvaal Provincial Council for six years.<ref name="DSAB"/>

==Family life==
Prinsloo's first wife, Stephanie Cecilia Weidner, died on 19 November 1941, leaving two children. On 10 April 1946 he married Grace Madeleine Sedgwick.<ref name="DSAB"/>

==Death==
He died at Ermelo on 20 November 1966.

==References==
{{Reflist}}

{{DEFAULTSORT:Prinsloo, Colonel Hendrik Frederik}}
[[Category:1890 births]]
[[Category:1966 deaths]]
[[Category:People from Albert Luthuli Local Municipality]]
[[Category:Afrikaner people]]
[[Category:South African people of Dutch descent]]
[[Category:South African military personnel of World War II]]
[[Category:Officers of the Order of the British Empire]]
[[Category:Recipients of the Croix de guerre 1914–1918 (France)]]
[[Category:South African military personnel of World War I]]
[[Category:South African Army officers]]
[[Category:People of the Second Boer War]]
[[Category:South African farmers]]