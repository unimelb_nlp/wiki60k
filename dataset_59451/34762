{{good article}}
{{DISPLAYTITLE:731 (''The X-Files'')}}
{{Infobox television episode 
| title = 731
| image =
| caption = 
| series = [[The X-Files]]
| season = 3
| episode = 10
| airdate = December 1, 1995 
| production = 3X10
| writer = [[Frank Spotnitz]]
| director = [[Rob Bowman (filmmaker)|Rob Bowman]]
| length = 45 minutes
| guests =
* [[Stephen McHattie]] as [[Men in Black (The X-Files)#Red-Haired Man|Red-Haired Man]]
* [[William B. Davis]] as [[The Smoking Man|Cigarette Smoking Man]]
* [[Don S. Williams]] as [[List of The X-Files characters#First Elder|First Elder]]
* [[Steven Williams]] as [[X (The X-Files)|X]]
* Michael Puttonen as Conductor
* [[Robert Ito]] as Dr. Takio Ishimaru/Shiro Zama
* [[Colin Cunningham]] as Escalante
* [[Brendan Beiser]] as [[Pendrell]]
| prev = [[Nisei (The X-Files)|Nisei]]
| next = [[Revelations (The X-Files)|Revelations]]
| episode_list = [[The X-Files (season 3)|List of season 3 episodes]]<br>[[List of The X-Files episodes|List of ''The X-Files'' episodes]]
}}

"'''731'''" is the tenth episode of the [[The X-Files (season 3)|third season]] of the American [[science fiction on television|science fiction]] television series ''[[The X-Files]]''. It premiered on the [[Fox Broadcasting Company|Fox network]] on {{nowrap|December 1, 1995}}. It was directed by [[Rob Bowman (filmmaker)|Rob Bowman]], and written by [[Frank Spotnitz]]. "731" featured guest appearances by [[Stephen McHattie]], [[Steven Williams]] and [[Don S. Williams]]. The episode helps explore the series' overarching [[Mythology of The X-Files|mythology]]. "731" earned a Nielsen household rating of 12, being watched by 17.68 million people in its initial broadcast.

The show centers on [[Federal Bureau of Investigation|FBI]] special agents [[Fox Mulder]] ([[David Duchovny]]) and [[Dana Scully]] ([[Gillian Anderson]]) who work on cases linked to the paranormal, called [[X-File]]s. In this episode, Mulder risks his life infiltrating a train carrying a human-alien hybrid. Meanwhile, Scully tries to uncover the truth about her abduction. "731" is a two-part episode, continuing the plot from the previous episode, "[[Nisei (The X-Files)|Nisei]]".

The production of "731" involved several stunts, including the explosion of a retired railway car. The episode's production was successful for two members of the crew—earning director of photography John Bartley an [[American Society of Cinematographers]] award nomination for his work, and securing Bowman the job of directing the series' subsequent film adaptation, ''[[The X-Files (film)|The X-Files]]''.

== Plot ==
In [[Quinnimont, West Virginia]], a team of soldiers arrives at an abandoned [[leprosy]] research compound, rounding up most of the patients. One patient, Escalante, hides beneath a [[trapdoor]] during the arrival and follows the group to a secluded field nearby. He watches as the soldiers shoot the other patients, including apparent alien-human hybrids, into a [[mass grave]].

[[Fox Mulder]] loses his cell phone after jumping on top of the moving train, losing contact with [[Dana Scully]]. When questioned by Scully, [[X (The X-Files)|X]] tells her to analyze her implant, saying that it will give her answers about the train and her sister [[Melissa Scully|Melissa]]'s murder. Meanwhile, Mulder enters the train and finds that the secret [[railcar]] is [[quarantine]]d and protected by a security system. He searches for Zama, enlisting the train [[Conductor (transportation)|conductor]] for help. In Zama's compartment, they find hand-written journals in [[Japanese language|Japanese]]. However, elsewhere on the train, the [[Men in Black (The X-Files)#Red-Haired Man|Red-Haired Man]] intercepts and strangles Zama.

Scully sees [[Pendrell]], who tells her that the implant contains highly advanced technology that can replicate the brain's memory functions and enable someone to know a person's very thoughts. The manufacturer of the chip was Zama, who created the implant at the West Virginia compound. Scully travels there, meeting a group of deformed patients who have eluded the "[[death squads]]." Escalante tells her that the patients were experimented on by Zama, who departed long ago; since then, the death squads have set out to massacre them. Escalante shows her the mass grave but is killed when soldiers arrive to capture Scully. She is brought before the [[List of The X-Files characters#First Elder|First Elder]].

Mulder returns to the railcar, seeing its door ajar; an alien-human test subject is locked in a room inside. The Red-Haired Man attacks Mulder, causing the conductor to lock them both in the car. The Red-Haired Man claims to work for the [[National Security Agency|NSA]], and that a bomb in the car was triggered after he gained entry with Zama's pass code. Mulder doesn't believe him, but he is called by Scully on the Red-Haired Man's cell phone. Scully—who is with the First Elder in a similar railcar—tells Mulder that unwitting subjects, including herself, were operated on by Zama in the secret railway, with the [[alien abduction]] theory used as a smokescreen. She also confirms that a bomb is in the car and believes that the quarantined patient is infected with [[hemorrhagic fever]]. She fears that thousands will die from the disease if the car explodes. Mulder finds the bomb in the ceiling.

After the car is disconnected from the rest of the train on a remote [[rail siding]], Mulder questions the Red-Haired Man, who says that the patient is immune to [[biological warfare]]. Zama had tried to sneak the patient out of the country, but the government would rather see it destroyed than let their research fall into Japanese hands; the Red-Haired Man was sent to kill them both. Mulder, however, believes that the patient is an alien-human hybrid. With help from Scully, Mulder successfully unlocks the door of the railcar, but he is knocked unconscious by the Red-Haired Man. As the Red-Haired Man is about to leave, X appears and shoots him. Realizing that the bomb is about to explode and that there is not enough time to both save Mulder and secure the patient, X decides to save Mulder. X exits carrying the still unconscious Mulder shortly before the bomb explodes.

After recovering from his injuries, Mulder attempts to find information on the railcar, but he is unable to do so. Scully returns the journal that he found on the car, but Mulder realizes that it is a rewritten substitute. Meanwhile, the real journal is translated in a shadowy room as the [[Cigarette Smoking Man|Smoking Man]] watches.<ref name="plot">Lowry, pp. 129–131</ref><ref name="plot2">Lovece, pp. 206–208</ref>

== Production ==
[[Image:Shiro-ishii.jpg|left|thumb|165px|The episode was inspired by Japan's [[Unit 731]] (''commander [[Shirō Ishii]] pictured'')]]

===Conception and writing===
Writer [[Frank Spotnitz]] has claimed that his inspiration for the episode came from having read a ''[[New York Times]]'' article on the war crimes committed by [[Unit 731]] of the [[Imperial Japanese Army]], after which the episode is named. The unit was responsible for [[human subject research]] on both prisoners of war and civilians. Further inspiration was drawn from the films ''[[North by Northwest]]'' and ''[[The Train (1964 film)|The Train]]'', which were the basis for the episode's train-car setting.<ref>Edwards, p. 157</ref> Spotnitz also noted that the episode offered the writers the opportunity to "set the counter back" for the series' premise, allowing the character of Scully to still maintain a skeptical standpoint after the events of "[[Paper Clip]]", an earlier third season episode in which the character witnesses a group of aliens.<ref>Edwards, pp. 157–158</ref> The tagline for this episode was switched to "Apology is Policy."<ref name="prod2">Lowry, p. 133</ref>

===Filming and post-production===
The scene at the start of the episode with Duchovny's character Fox Mulder clinging to the side of the train car was filmed using a harness cabled across the top of the car that was removed digitally during [[post production]]. Duchovny performed the stunt himself, without the use of a double.<ref name="effects">{{cite video|title=Special Effects Sequences: 731|work=[[The X-Files (season 3)|The X-Files: The Complete Third Season]] |origyear=1995–1996 |people=[[Mat Beck]] (narrator) |type =DVD|publisher=[[Fox Broadcasting Corporation|Fox]]}}</ref> [[Steven Williams]] and Duchovny separately recorded the scene in which Williams carries Duchovny from the car before it explodes against a blue screen. The results of this were flipped horizontally to aid the scene's "composition",<ref name="effects"/> and superimposed over the explosion. Twenty-five masked actors, mostly children, laid over prop bodies for the scene with the mass grave.<ref name="prod1">Lowry, pp. 131–133</ref>

The producers built train car interiors for the quarantine car where the patient was being kept and the sleeper cars, and floated the train sets on inner tubes to create the feeling of movement. Seven different cameras were used by director [[Rob Bowman (filmmaker)|Rob Bowman]] when filming the train car explosion. Forty-five gallons of gasoline and 120 black-powder bombs were used for the effect.<ref name="prod1"/> The car used had been obtained cheaply from a [[BC Rail|Vancouver-based rail company]], and had been considered scrap due to being bent. After the explosion was recorded, a bell from the train was recovered some distance from the site by the physical effects supervisor Dave Gauthier, who had it polished and engraved with a message for Bowman.<ref name="speaks">{{cite video|title=Chris Carter Talks About Season 3: 731|work=[[The X-Files (season 3)|The X-Files: The Complete Third Season]] |origyear=1995–1996 |people=[[Chris Carter (screenwriter)|Chris Carter]] (narrator) |type =DVD|publisher=[[Fox Broadcasting Corporation|Fox]]}}</ref>

Bowman made use of a [[Steadicam]] for those scenes featured Mulder inside the train, and kept the character away from the centre of the screen, to accentuate his "paranoid" mindset. This was deliberately contrasted with the concurrent scene featuring Scully, who is speaking to Mulder over a telephone. This was shot using a [[camera dolly]] and a "graphically balanced" [[mise-en-scène]], intended to leave the character seeming as "balanced, confident, strong" as "the [[Rock of Gibraltar]]".<ref>Edwards, p. 158</ref> Bowman, who felt that the finished episode "was really like a movie", has stated that his [[Film editing#Director's cut|cut]] of the episode impressed series creator [[Chris Carter (screenwriter)|Chris Carter]] enough for Carter to offer Bowman the job of directing the series' [[The X-Files (film)|film adaptation]].<ref name="truthabout">{{cite video |people=[[Chris Carter (screenwriter)|Chris Carter]], [[Kim Manners]] and [[Frank Spotnitz]] |origyear=1995–1996  |title=The Truth Behind Season 3 |medium= DVD |work=[[The X-Files (season 3)|The X-Files: The Complete Third Season]] |publisher=[[20th Century Fox Home Entertainment|FOX Home Entertainment]]}}</ref>

==Themes==
Jan Delasara, in her book '"''PopLit, PopCult and The X-Files''" argues that episodes such as "731" and "Nisei", or the earlier third season episode "[[Paper Clip]]", show the public's trust in science "eroding". Delasara proposes that "arrogated" scientists who are "rework[ing] the fabric of life" are causing the public's faith in the scientific method to fade drastically, "a concern ... that is directly addressed by ''X-Files'' episodes". Moreover, she notes that almost all of the scientists portrayed in ''The X-Files'' are depicted with a "connection to ancient evil," with the lone exception being Agent Scully.<ref name="d181"/> In "731," and earlier in "[[Nisei (The X-Files)|Nisei]]," the scientists are former Japanese scientists who worked for [[Unit 731]]. In their attempts to create a successful human-alien hybrid, they become the archetypical scientists who "[go] too far," a serious factor that Delasara argues "'alienates [the public] further from science and its practitioners".<ref name="d181">Delasara, p. 181</ref>

Critical opinion has also noted that both parts of the story arc offer an alternative explanation for the events of the series so far, a "less romantic" outcome that paints the ongoing plot as an elaborate hoax to defer attention from the government's experiments, both military and medical. Reviewer Todd VanDerWerff feels that such an explanation would "speak more to the sadness at the core of the ''X-Files'' to have Mulder find his answers and be forced to accept they weren't what he was looking for", comparing such a realization to the hero of ''[[Don Quixote]]''.<ref name="avc"/> This "hoax" plot device would later be revisited in both the fourth season finale "[[Gethsemane (The X-Files)|Gethsemane]]" and the fifth season's opening two-part episodes "[[Redux (The X-Files)|Redux and Redux II]]", although to a much lesser degree of effectiveness.<ref name="avc2">{{cite web |url=http://www.avclub.com/articles/demonsgethsemane,53003/ |title="Demons"/"Gethsemane" {{!}} The X-Files/Millennium {{!}} TV Club |publisher=[[The A.V. Club]] |first=Zack |last=Handlen |date=March 12, 2011 |accessdate=January 20, 2012}}</ref>

==Reception==
{{quote box|width=30em|bgcolor=#c6dbf7|Scully finds some of the answers she's been looking for, and Mulder gets as close as he's ever gotten to the truth, and both of them are the driving forces behind the story. In the end, when all is said and done, neither can walk away from what happens here, not in the same way they can from their other cases.|source=—[[The A.V. Club]]'s Zack Handlen on "731".<ref name="avc"/>}}

===Ratings===
"731" premiered on the [[Fox Broadcasting Company|Fox network]] on {{nowrap|December 1, 1995}}, and was first broadcast in the [[United Kingdom]] on [[BBC Two]] on {{nowrap|October 30, 1996}}.<ref name="BBCdate">{{cite AV media notes |title=The X-Files: The Complete Third Season |titlelink=The X-Files (season 3) |year=1995–1996 |others=[[Chris Carter (screenwriter)|Chris Carter]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> The episode earned a [[Nielsen rating|Nielsen household rating]] of 12 with a 21 share, meaning that roughly {{nowrap|12 percent}} of all television-equipped households, and {{nowrap|21 percent}} of households watching television, were tuned in to the episode.<ref name="rating"/> A total of {{nowrap|17.68 million}} viewers watched this episode during its original airing.<ref name="rating">Lowry, p. 251</ref>

===Reviews===
"731" received mostly positive reviews from critics. Writing for [[The A.V. Club]], reviewer Zack Handlen rated the episode an A, calling it "terrific". However, Handlen felt that the version of events told to Scully in this episode was perhaps a better explanation for the series' mythology than its eventual resolution, noting that it might have offered "a conclusion whose emotional impact would've lived up to the intensity of early mythology episodes" such as this.<ref name="avc">{{cite web |url=http://www.avclub.com/articles/731revelationswar-of-the-coprophages,43325/ |title="731"/"Revelations"/"War of the Coprophages" {{!}} The X-Files/Millennium {{!}} TV Club |publisher=[[The A.V. Club]] |first=Zack |last=Handlen |date=July 25, 2010 |accessdate=January 12, 2012}}</ref> In a retrospective of the third season in ''[[Entertainment Weekly]]'', "731" was rated a B. The review noted that the episode was "strangely tension-free", though it derided Scully's stubbornness to believe what the series had established as [[truth]].<ref name="EW">{{cite web |url=http://www.ew.com/ew/article/0,,295173_2,00.html |title=X Cyclopedia: The Ultimate Episode Guide, Season III {{!}} EW.com |publisher=''[[Entertainment Weekly]]'' |date=November 29, 1996 |accessdate=January 12, 2012}}</ref> Nick De Semlyen and James White of ''[[Empire (film magazine)|Empire]]'' named it the second "greatest" episode of the series, stating it is "arguably the greatest of The X-Files' many mythology episodes" and a "high-octane mix of action and intrigue, with the production values and pacing of a Hollywood blockbuster".<ref>{{cite web|url=http://www.empireonline.com/features/greatest-x-files-episodes/p19 |archiveurl=https://web.archive.org/web/20131114023846/http://www.empireonline.com/features/greatest-x-files-episodes/p19|title=The 20 Greatest X-Files Episodes|work=[[Empire (film magazine)|Empire]]|publisher=[[Bauer Media Group]]|author1=Semlyen, Nick De |author2=White, James |date=October 2013|archivedate=November 14, 2013}}</ref>

Director [[Rob Bowman (filmmaker)|Rob Bowman]] called the episode one of his all time favorites.<ref name="rec1"/> Actor [[Steven Williams]] felt that his portrayal of X in this episode helped endear him more to the show's viewers.<ref name="rec1">Lowry, pp. 131–132</ref>

===Awards===
[[Director of Photography]] John Bartley earned a nomination for Outstanding Achievement in Cinematography in a Regular Series by the [[American Society of Cinematographers]] for his work on this episode.<ref name="plot2"/><ref name="asc">{{cite web |url=http://www.theasc.com/asc_news/awards/awards_history.php|title=The ASC -- Past ASC Awards|publisher=[[American Society of Cinematographers]]|accessdate=January 12, 2012}}</ref>

==Footnotes==
{{reflist|2}}

===References===
*{{Cite book |publisher=Mcfarland & Co |first=Jan |last=Delasara |title=PopLit, PopCult and The X-Files: A Critical Exploration |year=2000 |isbn=0-7864-0789-1  }}
*{{Cite book |title=X-Files Confidential |first=Ted |last=Edwards |publisher=Little, Brown and Company |year=1996 |isbn=0-316-21808-1  }}
*{{Cite book |title=The X-Files Declassified |first=Frank |last=Lovece |publisher=Citadel Press |year=1996 |isbn=0-8065-1745-X  }}
*{{cite book | year=1996 | last=Lowry |first=Brian | title=Trust No One: The Official Guide to the X-Files|isbn=0-06-105353-8|publisher=Harper Prism}}

== External links ==
{{wikiquote|The_X-Files|TXF Season 3}}
*[https://web.archive.org/web/20010413102319/http://www.thexfiles.com/episodes/season3/3x10.html "731"] on ''The X-Files'' official website
* {{imdb episode|0751068|731}}
* {{tv.com episode|the-xfiles/731-2-549|731}}

{{TXF episodes|3}}

[[Category:1995 American television episodes]]
[[Category:Television episodes in multiple parts]]
[[Category:The X-Files (season 3) episodes]]
[[Category:Trains in fiction]]
[[Category:Washington, D.C. in fiction]]
[[Category:West Virginia in fiction]]