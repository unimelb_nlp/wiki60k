{{Use dmy dates|date=June 2014}}
{{Use Irish English|date=June 2014}}
{{italic title}}
{{Infobox Journal
|title=The Irish Naturalist
|country=[[Ireland]]
|frequency =Monthly
|discipline=[[biology]]
|editor = [[George H. Carpenter]], [[Robert Lloyd Praeger]]
|history=1892-1924
|website=
|ISSN=
}}
'''''The Irish Naturalist''''' was a [[scientific journal]] that was first published in [[Dublin]], [[Ireland]], in April 1892.

==History==
The journal owed its establishment to the efforts of several leading Dublin [[naturalist]]s, notably George H. Carpenter and R. M. Barrington. The first editors were Carpenter and [[Robert Lloyd Praeger]], of the [[National Library of Ireland]]. The journal was supported by a number of societies, including the [[Royal Zoological Society of Ireland]], the  Dublin Microscopical Club, the Belfast Naturalists' Field Club, and the Dublin Naturalists' Field Club.<ref>{{cite book|last=Praeger|first= Robert Lloyd |year=1969|title=The Way that I Went: An Irishman in Ireland|pages=10–12|location=Dublin|publisher= Allen Figgis}}</ref><ref>{{cite journal|last=Jackson |first=Patrick Wyse|author2=Peter Wyse Jackson|year=1992|title=The Irish Naturalist: 33 years of natural history in Ireland 1892-1924|journal=Irish Naturalists' Journal|volume=24|issue=3|pages=95–101}}</ref>

''The Irish Naturalist'' was published for 33 years and contained in total over 3000 pages. The journal ceased publication in December 1924. It had been having some financial problems, but the final blow came when Carpenter took up his appointment to the keepership of the Manchester Museum in 1923.<ref name=botan>[http://www.botanicgardens.ie/herb/books/irishnaturalists.htm Irish Naturalists]</ref>

==Contributors==
Among notable contributors to ''The Irish Naturalist'' were:<ref name=botan/>

*[[George James Allman]]
*[[Gerald Edwin Hamilton Barrett-Hamilton]]
*[[Alfred Cort Haddon]]
*[[Maxwell Henry Close]]

== See also ==
*''[[Irish Naturalists' Journal]]''

==References==
<references/>

==External links==
*[http://www.biodiversitylibrary.org/bibliography/6740 BDH] Digital versions of ''The Irish Naturalist''
*[https://archive.org/stream/irishnaturalist26roya/irishnaturalist26roya_djvu.txt] Online copy of the journal, January 1917

{{DEFAULTSORT:Irish Naturalist}}
[[Category:Biology journals]]
[[Category:Media in Dublin (city)]]
[[Category:Publications established in 1892]]
[[Category:Irish magazines]]
[[Category:Publications disestablished in 1924]]
[[Category:Monthly journals]]
[[Category:Defunct journals]]