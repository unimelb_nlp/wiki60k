{{Infobox journal
| title = NPG Asia Materials
| cover = 
| editor = Martin Vacha
| discipline = [[Materials science]]
| abbreviation = NPG Asia Mater.
| publisher = [[Nature Publishing Group]]
| country =
| frequency = Continuous
| history = 2009-present
| openaccess = Yes
| license = [[ Creative Commons Attribution 4.0 International License]]
| impact = 10.118
| impact-year = 2014
| website = http://www.nature.com/am/
| link2 = http://www.nature.com/am/archive/index.html
| link2-name = Online archive
| eISSN = 1884-4057
| OCLC = 861209929
| CODEN = NAMPCE
}}
'''''NPG Asia Materials''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] focusing on [[materials science]]. It was established in 2009 and is published by the [[Nature Publishing Group]]. The founding [[editor-in-chief]] was Hideo Takezoe ([[Tokyo Institute of Technology]]); the current editor-in-chief is Martin Vacha ([Tokyo Institute of Technology).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstract Services]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-07-10}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-10}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-10}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 10.118.<ref name=WoS>{{cite book |year=2015 |chapter=NPG Asia Materials |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.nature.com/am/}}

[[Category:Nature Publishing Group academic journals]]
[[Category:English-language journals]]
[[Category:Continuous journals]]
[[Category:Materials science journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Publications established in 2009]]


{{science-journal-stub}}