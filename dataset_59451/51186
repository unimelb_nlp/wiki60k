{{Infobox journal
| title = Electronic Transactions on Numerical Analysis
| cover         = 
| editor        =Lothar Reichel, Ronny Ramlau
| discipline    = Applied mathematics
| former_names  = 
| abbreviation  =Electron. Trans. Numer. Anal., ETNA
| publisher     =Kent State University, Johann Radon Institute (RICAM)
| country       = 
| frequency     = 
| history       = 
| openaccess    = Free to authors and readers
| license       = 
| impact        = 
| impact-year   = 
| website       =
| link1         = http://etna.ricam.oeaw.ac.at/
| link1-name    = ETNA homepage
| link2         = http://etna.mcs.kent.edu/
| link2-name    = ETNA homepage
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 
| eISSN         = 
}}

'''''Electronic Transactions on Numerical Analysis'''''  is a 
[[peer-reviewed]] scientific [[open access journal]]  publishing [[original research]] in 
[[applied mathematics]] with the focus on [[numerical analysis]] and 
[[scientific computing]],.<ref>http://etna.ricam.oeaw.ac.at/scope/</ref> 
It is published by the [[Kent State University]] and the  Johann Radon Institute for Computational and 
Applied Mathematics (RICAM). Articles for this journal are published in electronic form 
on the journal's web site.  The journal is one of the oldest scientific [[open access]] journals in 
mathematics.<ref>[http://www.qucosa.de/recherche/frontdoor/?tx_slubopus4frontend%5bid%5d=urn:nbn:de:bsz:ch1-qucosa-187673  R. Herzog, Das Access Journal Electronic Transactions on Numerical Analysis (in german)]</ref>

The Electronic Transactions on Numerical Analysis were
founded in 1992 by [[Richard S. Varga]], [[Arden Ruttan]], and  
[[Lothar Reichel]]<ref>[https://www.siam.org/news/news.php?id=761 Celebration of a  Wide-ranging Community at Kent State]</ref><ref>[http://www.jucs.org/jucs_0_0/tragic_loss_or_good A. Odlyzkov,Tragic Loss or Good Riddance?]</ref> (all  [[Kent State University]]) as a fully open access journal (no fee for reader or authors). 
The first issue appeared in September 1993. 
The current editors-in-chief are  [[Lothar Reichel]] and [[Ronny Ramlau]].

==Editors-in-chief==
* 1993–2008: Richard S. Varga
* 1993–1998: Arden Ruttan
* 2005–2013: Daniel Szyld
* since 1993: Lothar Reichel
* since 2010: Ronny Ramlau

==No-fee open access and copyright==
Since its foundation, the journal follows an open access policy that allows free access to readers and charges no fee for authors ("diamond open access"). Authors transfer the copyright of published articles to the editors. This publication model is based on the one hand on support of the editing institutions and on donations. On the other hand, the editing process is carried out by volunteers from the scientific community.

== Abstracting and indexing ==
The journal is abstracted and indexed in the
[[Science Citation Index Expanded]] <ref>[http://science.thomsonreuters.com/mjl/ Master journal list]</ref>
[[Mathematical Reviews]],<ref>http://www.ams.org/mathscinet/ MathSciNet</ref> 
and [[Zentralblatt MATH]].<ref>https://www.zbmath.org/journals/ Zentralblatt Math</ref> 
According to the ''[[Journal Citation Reports]]'', 
the journal has a 2015 [[impact factor]] of 
0.671 (highest 1.261 in 2012)
<ref name=WoS>{{cite book |year=2016 |chapter=Electronic Transactions on Numerical Analysis |title=2016 [[Journal Citation Reports]]|publisher=[[Thomson Reuters]]|edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== External links ==
* [http://etna.ricam.oeaw.ac.at/ Official website (RICAM)]
* [http://etna.mcs.kent.edu// Official website (Kent)]

== References ==
{{reflist}}

[[Category:Publications established in 1992]]
[[Category:Mathematics journals]]
[[Category:Open access journals]]
[[Category:English-language journals]]