The '''Bouzingo''' were a group of eccentric poets, novelists, and artists in [[France]] during the 1830s that practiced an extreme form of [[romanticism]] whose influence helped determine the course of culture in the 20th century including such movements as [[Bohemianism]], [[Parnassian]]ism, [[Symbolism (arts)|Symbolism]], [[Decadence]], [[Aestheticism]], [[Dadaism]], [[Surrealism]], the [[Lost Generation]] the [[Beat Generation]], [[Hippies]], [[Punk rock]], etc.

== Legacy ==
The stories the Bouzingo wrote about themselves were full of intentional exaggerations. The stories were meant to frighten the [[middle class|middle]] and [[upper class]].  They believed that people from the [[middle class|middle]] and [[upper class]] would be offended by the idea of [[poet]]s and [[artist]]s acting like barbarians and primitives.  This was the aim of the Bouzingo and for a time they spawned major controversies.  The actual truth is now nearly impossible to find out.  These artists were not well documented with any kind of journalistic objectivity during their prime.  The legends of the Bouzingo are captured most notably by [[Théophile Gautier|Gautier]] in “[[Les Jeunes-France]]” (1833) but also to a lesser extent in Henry Murger's "[[La Vie de Bohème]]" (1849).

===Truth or myth?===
These are a few of the most famous exaggerations invented by the Bouzingo:
*They hosted parties where clothes were banned and [[wine]] was consumed from human skulls.
*They played [[Musical instrument|instruments]] that they did not know how to play on street corners.
*Nerval was said to have walked a pet [[lobster]] on a leash because “it does not bark and knows the secrets of the sea”.

== Miscellaneous ==
Members of the Bouzingo became highly influential in the Avant-Garde Movements of the Late 19th Century and on into the 20th Century.

[[André Breton]] mentioned the influence of Nerval in the first Surrealist Manifesto.  He also included [[Petrus Borel]] and [[Xavier Forneret]] in his influential "Anthology of Black Humor".

André Breton wrote, "To be even fairer, we could probably have taken over the word SUPERNATURALISM employed by Gérard de Nerval in his dedication to the Filles de feu... It appears, in fact, that Nerval possessed to a tee the spirit with which we claim a kinship..." - The Surrealist Manifesto, 1924

[[Italo Calvino]] included [[Petrus Borel]] and [[Gérard de Nerval]] in his anthology of "Fantastic Tales".  ''La Main de gloire'' by Gérard de Nerval was a story intended to be published in the "Contes du Bouzingo".

[[Marcel Proust]], [[Joseph Cornell]], [[René Daumal]], and [[T. S. Eliot]] have all cited Gérard de Nerval as a major influence.  Eliot's ''[[The Waste Land]]'' borrowed one of its most enigmatic lines from Nerval's "El Desdichado".

[[Oscar Wilde]], [[Joris-Karl Huysmans]], and [[Lautréamont]] have all mentioned the works of Gautier as influential.  His thoughts on the philosophy of "Art for Art's Sake" have continued to be the source of debate.

Gautier with Nerval and Baudelaire created the infamous [[Club des Hashischins]] dedicated to exploring experiences with drugs.

== Members of the Bouzingo ==
*[[Gérard de Nerval]]
*[[Petrus Borel]] "the [[Lycanthrope]]"
*[[Théophile Gautier]]
*[[Augustus McKeat]]
*[[Philothée O'Neddy]]
*[[Xavier Forneret]]
*[[Aloysius Bertrand]]
*[[Joseph Bouchardy]]
*[[Alphonse Brot]]

==External links==
* [http://downlode.org/Etext/haschischins.html Club des Haschischins by Gautier (1846)]
* [http://www.mtholyoke.edu/courses/rschwart/hist255-s01/boheme/welcome.html Mount Holyoke College's Bohemianism and Counterculture]
* [http://www.bohemiabooks.com.au/eblinks/spirboho/index.html The Spirit of Bohemia, Bohemia Books]
* [https://web.archive.org/web/20110719212137/http://todd.pendu.org:80/bouzingo-means-noise-by-todd-brooks/ Bouzingo Means Noise: Freaks, Weirdos, Visionaries, and Outsiders by Todd Brooks]

== Further reading==
*Dumont, Francis, 1958. ''Nerval et les Bousingots'' (La Table ronde)
*Dumont, Francis, 1949 ''Les Petits Romantiques Francais'' (Les Cahiers Du Sud)
*Starkie, Enid, 1954. ''Petrus Borel: The Lycanthrope, His Life and Times''. (Faber and Faber Ltd.)
*André Breton, 1997. ''Anthology of Black Humor''.  (City Lights Publishers)  ISBN 0-87286-321-2
*Italo Calvino, 1998. ''Fantastic Tales''. (Vintage) ISBN 0-679-75544-6
*Mélanges tirés d'une petite bibliothèque romantique: bibliographie anecdotique et pittoresque...by Charles Asselineau, Théodore Faullain de Banville, Charles Baudelaire, 1866.
*On Bohemia by [[César Graña]], Marigay Grana, 1990 - Chapter: Bouzingos and Jeunes-France pp.&nbsp;365–369
*Lettre inédite de Philothée O'Neddy [pseud.] auteur de: Feu et flamme, sur le groupe littérai...by Théophile Dondey, 1875

[[Category:History of literature in France]]
[[Category:Romanticism]]
[[Category:19th-century French literature]]