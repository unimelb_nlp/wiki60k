{{other uses}}

{{refimprove|date=January 2017}}

{{Infobox writer <!-- for more information see [[:Template:Infobox writer/doc]] -->
 | name        = Novalis
 | image       = Franz Gareis - Novalis.jpg
 | imagesize   = 200px
 | caption     = <small>Novalis (1799), portrait by Franz Gareis</small>
 | pseudonym   = Novalis
 | birth_name  = Georg Philipp Friedrich Freiherr von Hardenberg
 | birth_date  = {{Birth date|1772|5|2|df=y}}
 | birth_place = [[Wiederstedt|Oberwiederstedt]],  [[Electorate of Saxony]]
 | death_date  = {{death date and age|1801|3|25|1772|5|2|df=y}}
 | death_place = [[Weißenfels]], Electorate of Saxony
 | alma_mater  = [[University of Jena]]<br>[[Leipzig University]]<br>[[University of Wittenberg]]
 | occupation  = Prose writer, poet, mystic, philosopher, civil engineer, mineralogist
 | nationality = [[Germany|German]]
 | period      =
 | genre       =
 | subject     =
 | movement    = [[Jena Romanticism]]<ref>Joel Faflak, Julia M. Wright (eds.), ''A Handbook of Romanticism Studies'', John Wiley & Sons, 2016, p. 334.</ref>
 | influences  = [[Jakob Böhme]], [[Johann Gottlieb Fichte]], [[Johann Wolfgang von Goethe]], [[Baruch Spinoza]]
 | influenced  = [[Otto Heinrich von Loeben]], [[Heinrich Heine]], [[Thomas Bernhard]], [[Hermann Hesse]], [[George MacDonald]], [[Walter Pater]]
 | signature   =
}}
'''Novalis''' ({{IPA-de|noˈvaːlɪs|lang}}) was the [[pseudonym]] and [[pen name]] of '''Georg Philipp Friedrich [[Freiherr]] von Hardenberg''' (2 May 1772 – 25 March 1801), a poet,  author, mystic, and [[philosopher]] of [[Early German Romanticism]]. Hardenberg's professional work and university background, namely his study of mineralogy and management of salt mines in Saxony, was often ignored by his contemporary readers. The first studies showing important relations between his literary and professional works started in the 1960s.<ref>Barbara Laman, ''James Joyce and German Theory: The Romantic School and All That'', Fairleigh Dickinson University Press, 2004, ISBN 9781611472844, p.37</ref>

==Early life and education==
Georg Philipp Friedrich von Hardenberg was born in 1772 at [[Wiederstedt|Oberwiederstedt]] manor (now part of [[Arnstein, Saxony-Anhalt]]), in the [[Harz]] mountains. In the church in Wiederstedt, he was christened Georg Philipp Friedrich.  An oil painting and a christening cap commonly assigned to him are Hardenberg's only possessions now extant.

The family seat was a manorial estate, not simply a stately home. Hardenberg descended from ancient, Low German nobility. Different lines of the family include such important, influential magistrates and ministry officials as the [[Prussia]]n chancellor [[Karl August von Hardenberg]] (1750–1822).He spent his childhood on the family estate and used it as the starting point for his travels into the Harz mountains.

His father, the estate owner and salt-mine manager Heinrich Ulrich Erasmus Freiherr von Hardenberg (1738–1814), was a strictly [[pietism|pietistic]] man, member of the [[Moravian Church|Moravian]] (Herrnhuter) Church. Heinrich Ulrich Erasmus' second marriage was to Auguste Bernhardine von Böltzig (1749–1818), who gave birth to eleven children:  their second child was Georg Philipp Friedrich. The Hardenbergs were a noble family but not rich. Young Georg Philipp was often short of cash, rode a small horse, and sometimes had to simply walk.<ref name=frank>"Dark Fates" by [[Frank Kermode]], ''[[London Review of Books]]'', 5 October 1995 - reproduced in Kermode's collection ''Bury Place Papers'', LRB, London, 2009, ISBN 9781873092040</ref>

At first, young Hardenberg was taught by private tutors. He attended the [[Martin Luther|Luther]]an grammar school in [[Eisleben]], where he acquired skills in [[rhetoric]] and ancient literature, common parts of the education of his time. From his twelfth year, he was in the charge of his uncle Friedrich Wilhelm Freiherr von Hardenberg at his stately home in Lucklum.

==Studies and employment==
Young Hardenberg studied [[legal studies|Law]] from 1790 to 1794 at [[University of Jena|Jena]], [[Leipzig University|Leipzig]] and [[University of Wittenberg|Wittenberg]]. He passed his exams with distinction. During his studies, he attended [[Friedrich Schiller|Schiller]]'s lectures on history and befriended him during Schiller's [[tuberculosis|illness]]. He also met [[Johann Wolfgang von Goethe|Goethe]], [[Johann Gottfried Herder|Herder]] and [[Jean Paul]], and befriended [[Ludwig Tieck]], [[Friedrich Wilhelm Joseph Schelling]], and the brothers [[Karl Wilhelm Friedrich Schlegel|Friedrich]] and [[August Wilhelm Schlegel]].

In October 1794, he started working as [[actuary]] for August Coelestin Just, who turned out to become not only his friend but, later, also his biographer. The following January, he was appointed [[auditor]] to the salt works at [[Weißenfels]].

==Sophie==
During the time he worked for August Coelestin Just, Novalis met the 12-year-old [[Sophie von Kühn]] (1782–1797), a girl who was, according to accounts, a "perfectly commonplace young girl, neither intelligent nor particularly beautiful."<ref name=frank/> Nonetheless, he fell in love with Sophie, since in the young Georg Philipp's view of the world "nothing is commonplace" because "all, when rightly seen, is [[symbol]]ic."<ref name=frank/> On 15 March 1795, when [[Sophie von Kühn|Sophie]] was 13 years old, the two became engaged to marry, despite her family's reluctance and the fact that she was already [[tuberculosis|tubercular]].<ref name=frank/>

The cruelly early death of Sophie in March 1797, from [[tuberculosis]], affected Novalis deeply and permanently. She was only 15 years old, and the two had not married yet.

==At the salt mines==
In 1795–1796, Novalis entered the [[Technische Universität Bergakademie Freiberg|Mining Academy of Freiberg]] in [[Saxony]], a leading academy of science, to study [[geology]] under Professor [[Abraham Gottlob Werner]] (1750–1817), who befriended him. During Novalis' studies in Freiberg, he immersed himself in a wide range of studies, including [[mining]], [[mathematics]], [[chemistry]], [[biology]], [[history]] and, not least, [[philosophy]]. It was here that he collected materials for his famous [[encyclopaedia]] project. Similar to other German authors of the Romantic age, his work in the mining industry, which was undergoing then the first steps to industrialization, was closely connected with his literary work.<ref>Theodore Ziolkowski: ''German Romanticism And Its Institutions'', Princeton University Press, 1992</ref>

==Pen name==
Young Hardenberg adopted the [[pen name]] '''Novalis''' from his 12th century ancestors who named themselves ''de Novali'', after their settlement [[Grossenrode]], or ''magna Novalis''.<ref>[http://www.litencyc.com/php/speople.php?rec=true&UID=1975 Novalis] in the ''Literary Encyclopedia'', The Literary Dictionary Company</ref>

==Literature & philosophy==
In the period 1795–1796, Novalis concerned himself with the scientific doctrine of [[Johann Gottlieb Fichte]], which greatly influenced his world view. He not only read Fichte's philosophies but also developed Fichte's concepts further, transforming Fichte's ''Nicht-Ich'' ([[German language|German]] "not I") to a ''Du'' ("you"), an equal subject to the ''Ich'' ("I"). This was the starting point for Novalis' ''Liebesreligion'' ("religion of love").

Novalis' first fragments were published in 1798 in the ''[[Athenaeum (German magazine)|Athenäum]]'', a magazine edited by the brothers Schlegel, who were also part of the early Romantic movement. Novalis' first publication was entitled ''Blüthenstaub'' (''Pollen'') and saw the first appearance of his pseudonym, "Novalis". In July 1799, he became acquainted with [[Ludwig Tieck]], and that autumn he met other authors of so-called "[[Jena Romanticism]]".

==Julie==
Novalis became engaged for the second time in December 1798. His fiancée was Julie von Charpentier (1776–1811), a daughter of Johann Friedrich Wilhelm Toussaint von Charpentier, a professor in [[Freiberg, Saxony|Freiberg]].

From [[Pentecost]] 1799, Novalis again worked in the management of salt mines. That December, he became an assessor of the salt mines and a director. On the 6 December 1800, the twenty-eight-year-old Hardenberg was appointed ''Supernumerar-Amtshauptmann'' for the [[Thuringia|district of Thuringia]], a position comparable to that of a present-day magistrate.

==Death==
From August 1800 onward, Hardenberg was suffering from [[tuberculosis]]. On 25 March 1801, he died in Weißenfels.  His body was buried in the old cemetery there.

Novalis lived long enough to see the publication only of ''Pollen'', ''Faith and Love or the King and the Queen'' and ''Hymns to the Night''. His unfinished novels ''Heinrich von Ofterdingen'' and ''The Novices at Sais'', his political speech ''Christendom or Europa'', and numerous other notes and fragments were published posthumously by his friends Ludwig Tieck and Friedrich Schlegel.

He was a [[Freemason]].<ref>CRFF Working Paper Series: "[https://www.academia.edu/199283/The_Cosmopolitan_Foundations_of_Freemasonry The Cosmopolitan Foundations of Freemasonry]" by Andreas Önnerfors, director of the Centre for Research into Freemasonry at the [[University of Sheffield]], Centre For Research Into Freemasonry And Fraternalism, p.13</ref>

==Writing==
Novalis, who was deeply read in science, law, philosophy, politics and political economy, started writing quite early. He left an abundance of notes on these fields and his early work displays his ease and familiarity with them. His later works are closely connected to his studies and his profession. Novalis collected everything that he had learned, reflected upon it and drew connections in the sense of an encyclopaedic overview on art, religion and science. These notes from the years 1798 and 1799 are called ''Das allgemeine Brouillon'', and are now available in English under the title ''Notes for a Romantic Encyclopaedia''.
Together with [[Karl Wilhelm Friedrich Schlegel|Friedrich Schlegel]], Novalis developed the fragment as a literary form of art. The core of Hardenberg’s literary works is the quest for the connection of science and poetry, and the result was supposed to be a "progressive universal poesy” (fragment no. 116 of the Athenaum journal). Novalis was convinced that philosophy and the higher-ranking poetry have to be continually related to each other.

The fact that the romantic fragment is an appropriate form for a depiction of "progressive universal poesy”, can be seen especially from the success of this new genre in its later reception.

Novalis' whole works are based upon an idea of education: "We are on a mission: we are called upon to educate the earth." It has to be made clear that everything is in a continual process. It is the same with humanity, which forever strives towards and tries to recreate a new [[Golden Age]] – a paradisical Age of harmony between man and nature that was assumed to have existed in earlier times. This Age was described by [[Plato]], [[Plotinus]], and [[Franz Hemsterhuis]], the last of whom was an extremely important figure for the [[German Romantics]].

This idea of a romantic universal poesy can be seen clearly in the romantic triad. This theoretical structure always shows its recipient that the described moment is exactly the moment (''[[kairos]]'') in which the future is decided. These frequently mentioned critical points correspond with the artist’s feeling for the present, which Novalis shares with many other contemporaries of his time. Thus a triadic structure can be found in most of his works. This means that there are three corresponding structural elements which are written differently concerning the content and the form.

Hardenberg’s intensive study of the works of [[Jakob Böhme]], from 1800, had a clear influence on his own writing.

A mystical world view, a high standard of education, and the frequently perceptible pietistic influences are combined in Novalis' attempt to reach a new concept of Christianity, faith, and God. He forever endeavours to align these with his own view of [[transcendental philosophy]], which acquired the mysterious name "Magical idealism". Magical idealism draws heavily from the critical or [[transcendental idealism]] of [[Immanuel Kant]] and [[J. G. Fichte]] (the earliest form of [[German idealism]]), and incorporates the artistic element central to [[Early German Romanticism]]. The subject must strive to conform the external, natural world to its own will and genius; hence the term "magical".<ref>See David W. Wood's introduction to the entry for "Novalis," ''Notes for a Romantic Encyclopaedia'', Albany: SUNY, 2007</ref> David Krell calls magical idealism "[[thaumaturgic]] idealism."<ref>David Farrell Krell, ''Contagion'', Indianapolis: Indiana State University, 1998.</ref>  This view can even be discerned in more religious works such as the ''Spiritual Songs'' (published 1802), which soon became incorporated into Lutheran hymn-books.

Novalis influenced, among others, the novelist and theologian [[George MacDonald]], who translated his 'Hymns to the Night' in 1897. More recently, Novalis, as well as the [[Early Romanticism]] (''Frühromantik'') movement as a whole, has been recognized as constituting a separate philosophical school, as opposed to simply a literary movement. Recognition of the distinctness of ''Frühromantik'' philosophy is owed in large part, in the English speaking world at least, to the writer [[Frederick Beiser]].

===Poetry===
In August 1800, eight months after completion, the revised edition of the ''Hymnen an die Nacht'' was published in the ''Athenaeum''. They are often considered to be the climax of Novalis’ lyrical works and the most important poetry of the German early Romanticism.

[[File:Novalis.jpg|thumb|right|200px|[[Romantic poetry|Romantic]] poet Novalis (1772–1801), portrait by [[Friedrich Eduard Eichens]] from 1845]]The six hymns contain many elements which can be understood as autobiographical. Even though a lyrical "I", rather than Novalis himself, is the speaker, there are many relationships between the hymns and Hardenberg’s experiences from 1797 to 1800.

The topic is the romantic interpretation of life and death, the threshold of which is symbolised by the night. Life and death are – according to Novalis – developed into entwined concepts. So in the end, death is the romantic principle of life.

Influences from the literature of that time can be seen. The metaphors of the hymns are closely connected to the books Novalis had read at about the time of his writing of the hymns. These are prominently [[Shakespeare]]’s ''[[Romeo and Juliet]]'' (in the translation by [[August Wilhelm Schlegel|A.W. Schlegel]], 1797) and [[Jean Paul]]’s ''Unsichtbare Loge'' (1793).

The ''Hymns to the Night'' display a universal religion with an intermediary. This concept is based on the idea that there is always a third party between a human and God. This intermediary can either be Jesus – as in Christian lore – or the dead beloved as in the hymns. These works consist of three times two hymns. These three components are each structured in this way: the first hymn shows, with the help of the Romantic triad, the development from an assumed happy life on earth through a painful era of alienation to salvation in the eternal night; the following hymn tells of the awakening from this vision and the longing for a return to it. With each pair of hymns, a higher level of experience and knowledge is shown.

===Prose===
The novel fragments ''Heinrich von Ofterdingen'' and ''Die Lehrlinge zu Sais (The Novices of Sais)'' reflect the idea of describing a universal world harmony with the help of poetry. The novel '[[Heinrich von Ofterdingen]]' contains the "[[blue flower]]", a symbol that became an emblem for the whole of German Romanticism. Originally the novel was supposed to be an answer to Goethe’s ''Wilhelm Meister'', a work that Novalis had read with enthusiasm but later on judged as being highly unpoetical. He disliked the victory of the economical over the poetic.

The speech called ''Die Christenheit oder Europa'' was written in 1799, but was first published in 1826. It is a poetical, cultural-historical speech with a focus on a political utopia with regard to the Middle Ages. In this text Novalis tries to develop a new Europe which is based on a new poetical [[Christendom]] which shall lead to unity and freedom. He got the inspiration for this text from [[Friedrich Schleiermacher|Schleiermacher]]’s ''[[On Religion|Über die Religion]]'' (1799). The work was also a response to the French Enlightenment and Revolution, both of which Novalis saw as catastrophic and irreligious. It anticipated, then, the growing German and Romantic theme of anti-Enlightenment visions of European spirituality and order.

==Influence==
[[Walter Pater]] includes Novalis's quote, ''"Philosophiren ist delphlegmatisiren, vivificiren"'' ("to philosophize is to throw off apathy, to become revived")<ref>Barbara Laman, ''James Joyce and German Theory: "The Romantic School and All That"'', (Fairleigh Dickinson University Press, 2004), 37.</ref> in his conclusion to ''Studies in the History of the Renaissance''. Novalis' poetry and writings were also an influence on [[Hermann Hesse]].

The libretto of [[Richard Wagner]]'s opera ''[[Tristan und Isolde]]'' contains strong allusions to Novalis' symbolic language, especially the dichotomy between the Night and the Day that animates his ''Hymns to the Night.''

Novalis was also an influence on [[George MacDonald]], and so indirectly on [[C. S. Lewis]], the [[Inklings]], and the whole modern [[fantasy]] genre. [[Jorge Luis Borges|Borges]] refers often to Novalis in his work.

[[File:Novalisplaque.jpg|thumb|right|240px|Novalis house plaque, [[Freiberg, Saxony|Freiberg]].]]

Novelist [[Penelope Fitzgerald]]'s last work, ''[[The Blue Flower]]'', is a historical fiction about Novalis, his education, his philosophical and poetic development, and his romance with Sophie.

The [[krautrock]] band [[Novalis (band)|Novalis]], beside taking their name from him, adapted or used directly poems by Novalis as lyrics on their albums.

The American avant-garde filmmaker [[Stan Brakhage]] made the film ''First Hymn to the Night – Novalis'' in 1994. The film was issued on Blu-ray and DVD by the [[The Criterion Collection|Criterion Collection]].<ref>[https://www.criterion.com/boxsets/722-by-brakhage-an-anthology-volumes-one-and-two By Brakhage: An Anthology, Volumes One and Two], Criterion</ref>

In tribute to his writings, Novalis records are produced by AVC Audio Visual Communications AG, Switzerland.

==Collected works==
Novalis' works were originally issued in two volumes by his friends [[Ludwig Tieck]] and [[Karl Wilhelm Friedrich Schlegel|Friedrich Schlegel]] (2 vols. 1802; a third volume was added in 1846). Editions of Novalis' collected works have since been compiled by [[C. Meisner]] and [[Bruno Wille]] (1898), by [[E. Heilborn]] (3 vols., 1901), and by [[J. Minor]] (3 vols., 1907). ''Heinrich von Ofterdingen'' was published separately by [[J. Schmidt]] in 1876.

Novalis's ''Correspondence'' was edited by [[J. M. Raich]] in 1880. See [[R. Haym]] ''Die romantische Schule'' (Berlin, 1870); [[A. Schubart]], ''Novalis' Leben, Dichten und Denken'' (1887); [[C. Busse]], ''Novalis' Lyrik'' (1898); [[J. Bing]], ''Friedrich von Hardenberg'' (Hamburg, 1899), E. Heilborn, ''Friedrich von Hardenberg'' (Berlin, 1901).

The German-language, six-volume edition of Novalis works ''Historische-Kritische Ausgabe - Novalis Schriften'' (HKA) is edited by Richard Samuel, Hans-Joachim Mähl & Gerhard Schulz. It is published by Verlag W. Kohlhammer, Stuttgart, 1960–2006.

===English translations===
Several of Novalis's notebooks and philosophical works or books about Novalis and his work have been translated in English.

* ''The Birth of Novalis: Friedrich von Hardenberg's Journal of 1797, With Selected Letters and Documents'', trans. and ed. [[Bruce Donehower]], State University of New York Press, 2007.
* ''Classic and Romantic German Aesthetics'', ed. Jay Bernstein, Cambridge University Press, 2003. This book is in the same series, the ''Fichte-Studies'' and contains a selection of fragments, plus Novalis' ''Dialogues''. Also in this collection are fragments by [[Friedrich von Schlegel|Schlegel]] and [[Hölderlin]].
* ''Fichte Studies'', trans. [[Jane Kneller]], [[Cambridge University Press]], 2003. This translation is part of the Cambridge Texts in the History of Philosophy Series.
* ''Henry von Ofterdingen'', trans. [[Palmer Hilty]], Waveland Press, 1990.
* ''Hymns to the Night'', trans. by [[Dick Higgins]], McPherson & Company: 1988. This modern translation includes the German text (with variants) ''en face''.
* ''Hymns to the Night / Spiritual Songs'', Tr. [[George MacDonald]], Forward by [[Sergei O. Prokofieff]], Temple Lodge Publishing, London, 2001.
* ''Klingsohr's Fairy Tale'', Unicorn Books, Llanfynydd, Carmarthen, 1974.
* ''Novalis: Notes for a Romantic Encyclopaedia (Das Allgemeine Brouillon)'', trans. and ed. [[David W. Wood]], State University of New York Press, 2007. First English translation of Novalis's unfinished project for a "universal science," it contains his thoughts on philosophy, the arts, religion, literature and poetry, and his theory of "Magical Idealism." The Appendix contains substantial extracts from Novalis' ''Freiberg Natural Scientific Studies 1798/1799''.
* ''Novalis: Philosophical Writings'', transl. and ed. [[Margaret Mahoney Stoljar]], State University of New York Press, 1997. This volume contains several of Novalis' works, including ''Pollen'' or ''Miscellaneous Observations'', one of the few complete works published in his lifetime (though it was altered for publication by Friedrich Schlegel); ''Logological Fragments I'' and ''II''; ''Monologue'', a long fragment on language; ''Faith and Love or The King and Queen'', a collection of political fragments also published during his lifetime;  ''On Goethe''; extracts from ''Das allgemeine Broullion'' or ''General Draft''; and his essay ''Christendom or Europe''.
* ''The Novices of Sais'', trans. by [[Ralph Manheim]], [[Archipelago Books]], 2005. This translation was originally published in 1949. This edition includes illustrations by [[Paul Klee]]. ''The Novices of Sais'' contains the fairy tale "Hyacinth and Rose Petal."

==References==
{{Reflist|2}}

==Further reading==
*Ameriks, Karl (ed.). ''The Cambridge Companion to German Idealism''. Cambridge: Cambridge University Press, 2000
*Arena, Leonardo Vittorio, ''La filosofia di Novalis'', Milano: Franco Angeli, 1987 (in [[Italian language|Italian]])
*Behler, Ernst. ''German Romantic Literary Theory''. Cambridge: Cambridge University Press, 1993
*Beiser, Frederick. ''German Idealism''. Cambridge: Harvard University Press, 2002
* [[Antoine Berman|Berman, Antoine]]. ''[[L'épreuve de l'étranger. Culture et traduction dans l'Allemagne romantique: Herder, Goethe, Schlegel, Novalis, Humboldt, Schleiermacher, Hölderlin]].'', Paris, Gallimard, Essais, 1984. ISBN 978-2-07-070076-9 (in [[French language|French]])
*[[Penelope Fitzgerald|Fitzgerald, Penelope]]. ''[[The Blue Flower]]''. Mariner Books, 1995. A novelization of Novalis' early life.
*[[Bruce Haywood|Haywood, Bruce]]. ''Novalis, the veil of imagery; a study of the poetic works of [[Friedrich von Hardenberg]], 1772–1801'', 's-Gravenhage, Mouton, 1959; Cambridge, Mass.: Harvard University Press, 1959.
*Krell, David Farrell.  ''Contagion''. Bloomington: Indiana University Press, 1998.
*Kuzniar, Alice. ''Delayed Endings''. Georgia: University of Georgia Press, 1987.
*Lacoue-Labarthe, Phillipe and Jean-Luc Nancy. ''The Literary Absolute''. Albany: State University of New York Press, 1988.
*Molnár, Geza von. ''Novalis' "Fichte Studies"''.
*O’Brien, William Arctander. ''Novalis: Signs of Revolution''. Durham: Duke University Press, 1995.  ISBN 0-8223-1519-X
*Pfefferkorn, Kristin. ''Novalis: A Romantic's Theory of Language and Poetry''. New Haven: Yale University Press, 1988.
* [[Sergei O. Prokofieff|Prokofieff, Sergei O.]] ''Eternal Individuality. Towards a Karmic Biography of Novalis''. Temple Lodge Publishing, London 1992.

==External links==
{{wikiquote}}
{{Americana Poster|year=1920|Hardenberg, Friedrich Leopold, Freiherr von|Novalis}}
* {{Gutenberg author |id=Novalis | name=Novalis}}
* {{Internet Archive author}}
* {{Librivox author |id=309}}
*[http://plato.stanford.edu/entries/novalis/ Novalis] by Kristin Gjesdal, ''Stanford Encyclopedia of Philosophy''
*[http://logopoeia.com/novalis/index.html Novalis: Hymns to The Night] – a translation of the work by George MacDonald
*[http://www.rspearson.com/novalis.html Novalis Online] – including a few (computer-generated) hybrid-English translations, and essays on and by Novalis.
*[http://www.schloss-oberwiederstedt.de Oberwiederstedt Manor], birthplace of Novalis and home to the International Novalis Society and the Novalis Foundation (in [[German language|German]])
*[http://novalis.autorenverzeichnis.de/english/index.html Aquarium: Friedrich von Hardenberg im Internet] – a highly useful multi-lingual web-site for information on Novalis. It provides updates and news in English, German, French, Spanish and Italian, on the latest Novalis translations and reviews, along with general discussions, odd trivia and scholarly articles.
*{{Cite NIE|wstitle=Novalis|year=1905 |short=x}}
*{{Cite EB1911|wstitle=Novalis |short=x}}

{{Romanticism|state=collapsed}}
{{Idealism}}
{{Authority control}}
{{Use dmy dates|date=October 2016}}

{{DEFAULTSORT:Novalis}}
[[Category:1772 births]]
[[Category:1801 deaths]]
[[Category:People from Arnstein, Saxony-Anhalt]]
[[Category:People from the Electorate of Saxony]]
[[Category:German Lutherans]]
[[Category:German poets]]
[[Category:Writers from Saxony-Anhalt]]
[[Category:German monarchists]]
[[Category:Romanticism]]
[[Category:Natural philosophers]]
[[Category:Leipzig University alumni]]
[[Category:Hardenberg family]]
[[Category:18th-century German poets]]
[[Category:19th-century German poets]]
[[Category:19th-century deaths from tuberculosis]]
[[Category:German male poets]]
[[Category:Freemasons]]
[[Category:German-language poets]]

[[nds:Novalis]]