{{italic title}}
{{taxobox
 | name = ''Agaricus bisporus''
 | image = ChampignonMushroom.jpg
 | image_width = 234px
 | regnum = [[Fungi]]
 | phylum = [[Basidiomycota]]
 | classis = [[Agaricomycetes]]
 | ordo = [[Agaricales]]
 | familia = [[Agaricaceae]]
 | genus = ''[[Agaricus]]''
 | species = '''''A. bisporus'''''
 | binomial = ''Agaricus bisporus''
 | binomial_authority = ([[Jakob Emanuel Lange|J.E.Lange]]) [[Emil J. Imbach|Imbach]] (1946)<ref name=Imbach1946/>
 | synonyms = *''Psalliota hortensis'' f. ''bispora'' <small>J.E.Lange (1926)</small>
}}
{{mycomorphbox
|name = ''Agaricus bisporus''
|whichGills = free
|capShape = convex
|hymeniumType = gills
|stipeCharacter = ring
|ecologicalType = saprotrophic
|sporePrintColor = brown
|howEdible = choice
}}

'''''Agaricus bisporus''''' is an [[edible mushroom|edible]] [[Basidiomycota|basidiomycete]] [[mushroom]] native to [[grassland]]s in [[Europe]] and [[North America]]. It has two color states while immature—white and brown—both of which have various names. When mature, it is known as '''portobello mushroom''', often shortened to just '''portobello'''.

When immature and ''white'', this mushroom may be known as '''common mushroom''', '''button mushroom''', '''white mushroom''', '''cultivated mushroom''', '''table mushroom''', and '''champignon mushroom'''. When immature and ''brown'', this mushroom may be known variously as '''Swiss brown mushroom''', '''Roman brown mushroom''', '''Italian brown''', '''Italian mushroom''', '''cremini''' or '''crimini mushroom''', '''baby bella''', '''brown cap mushroom''', or '''chestnut mushroom'''.<ref>[http://www.thinkvegetables.co.uk/vegetable.asp?VegetableID=19 Think Vegetables: ''Chestnut mushroom''] Retrieved 2013-04-01</ref>

''A. bisporus'' is [[fungiculture|cultivated]] in more than seventy countries,<ref name=Cappelli84/> and is one of the most commonly and widely consumed mushrooms in the world.

==Taxonomy==
The common mushroom has a complicated [[Taxonomy (biology)|taxonomic]] history. It was first described by English botanist [[Mordecai Cubitt Cooke]] in his 1871 ''Handbook of British Fungi'', as a [[Variety (botany)|variety]] (var. ''hortensis'') of ''[[Agaricus campestris]]''.<ref name=Cooke1871>{{cite book|title=Handbook of British Fungi|volume=1|author=Cooke MC|year=1871|publisher=Macmillan and Co. |location=London|page=138}}</ref><ref name="url Fungorum - Species synonymy">{{cite web|url=http://www.indexfungorum.org/Names/SynSpecies.asp?RecordID=531546 |title=Species Fungorum – Species synonymy |publisher=CAB International |work=Index Fungorum|accessdate=21 January 2010}}</ref> [[Danish people|Danish]] [[mycologist]] [[Jakob Emanuel Lange]] later reviewed a [[cultivar]] specimen, and dubbed it ''Psalliota hortensis'' var. ''bispora'' in 1926.<ref name=Lange1926>{{cite journal|author =Lange JE|year=1926|title=Studies in the agarics of Denmark. Part VI. ''Psalliota'', ''Russula''|journal=Dansk botanisk Arkiv|volume=4|issue=12|pages=1–52}}</ref> In 1938, it was promoted to [[species]] status and renamed ''Psalliota bispora''.<ref name=Schaffer1938>{{cite journal |vauthors =Schäffer J, Møller FH |year=1939|title=Beitrag zur Psalliota Forschung |journal=Annales Mycologici|volume=36|issue=1|pages=64–82|language=German}}</ref> Emil Imbach imparted the current scientific name of the species, ''Agaricus bisporus'', after the genus ''Psalliota'' was renamed to ''Agaricus'' in 1946.<ref name=Cappelli84>{{cite book|title=Fungi Europaei:Agaricus|author =Cappelli A. |year=1984|publisher=Giovanna Biella |place=[[Saronno]], Italy|pages=123–25|language=it}}</ref> The [[specific name (botany)|specific epithet]] ''bispora'' distinguishes the two-spored [[basidia]] from four-spored [[Variety (botany)|varieties]].

==Description==
The [[Pileus (mycology)|pileus]] or cap of the original wild species is a pale grey-brown in color, with broad, flat scales on a paler background and fading toward the margins. It is first hemispherical in shape before flattening out with maturity, and {{convert|5|–|10|cm|in|abbr=off|0}} in diameter. The narrow, crowded [[Lamella (mycology)|gills]] are free and initially, pink, then red-brown and finally a dark brown with a whitish edge from the [[Cystidium|cheilocystidia]]. The cylindrical [[Stipe (mycology)|stipe]] is up to {{convert|6|cm|in|frac=3|abbr=on}} tall by 1–2&nbsp;cm wide and bears a thick and narrow [[Annulus (mycology)|ring]], which may be streaked on the upper side. The firm flesh is white, although stains a pale pinkish-red on bruising.<ref>{{cite book|author =Zeitlmayr L|year=1976|title=Wild Mushrooms:An Illustrated Handbook|pages=82–83|publisher=Garden City Press, Hertfordshire|isbn=0-584-10324-7}}</ref><ref name=Carluccio03/> The [[spore print]] is dark brown. The spores are oval to round and measure approximately 4.5–5.5&nbsp;μm &times; 5–7.5&nbsp;μm, and the [[Basidium|basidia]] usually two-spored, although two-tetrasporic varieties have been described from the [[Mojave Desert]] and the [[Mediterranean Sea|Mediterranean]], with predominantly [[heterothallic]] and [[Homothallism|homothallic]] lifestyles, respectively.<ref>{{cite journal|doi=10.2307/3760617 |vauthors=Callac P, Billette C, Imbernon M, Kerrigan RW |title=Morphological, genetic, and interfertility analyses reveal a novel, tetrasporic variety of ''Agaricus bisporus'' from the Sonoran Desert of California|jstor=3760617|journal=[[Mycologia]]|volume=85|issue=5|pages=835–851|year=1993}}</ref><ref>{{cite journal |vauthors=Callac P, Imbernon M, Guinberteau J, Pirobe L, Granit S, Olivier JM, Theochari I |title=Discovery of a wild Mediterranean population of ''Agaricus bisporus'', and its usefulness for breeding work|journal=Mushroom Science|volume=15|pages=245–252|year=2000}}</ref>

This mushroom is commonly found worldwide in fields and grassy areas following rain, from late spring through to autumn, especially in association with [[manure]]. It is widely collected and eaten, even by those who would not normally experiment with [[mushroom hunting]].<ref name=Carluccio03>{{cite book|author =Carluccio A. |year=2003|title=The Complete Mushroom Book|pages=21–22|publisher=Quadrille|isbn=1-84400-040-0}}</ref>

===Similar species===
The common mushroom could be confused with young specimens of the deadly poisonous [[destroying angel]] (''Amanita'' sp.), but the latter may be distinguished by their [[Volva (mycology)|volva]] or cup at the base of the mushroom and pure white gills (as opposed to pinkish or brown of ''A. bisporus''). Thus it is always important to clear away debris and examine the base of such similar mushrooms, as well as cutting open young specimens to check the gills. Furthermore, the destroying angel grows in [[moss]]y woods and lives symbiotically with [[spruce]].

A more common and less dangerous mistake is to confuse ''A. bisporus'' with ''[[Agaricus xanthodermus]]'', an inedible mushroom found worldwide in grassy areas. ''A. xanthodermus'' has an odor reminiscent of [[phenol]]; its flesh turns yellow when bruised. This fungus causes [[nausea]] and vomiting in some people.

The poisonous European species, ''[[Entoloma sinuatum]]'', has a passing resemblance as well, but has yellowish gills, turning pink, and it lacks a ring.

==Cultivation history==
[[File:Agaricus bisporus mushroom.jpg|thumb|right|''A. bisporus'' being cultivated]]
The earliest scientific description of the commercial cultivation of ''A. bisporus'' was made by French botanist [[Joseph Pitton de Tournefort]] in 1707.<ref name=Spencer1985>{{cite book|author =Spencer DM|chapter=The mushroom–its history and importance|veditors=Flegg PB, Spencer DM, Wood DA|title=The Biology and Technology of the Cultivated Mushroom|publisher=[[John Wiley and Sons]]|place=[[New York City|New York]]|year=1985|pages=1–8|isbn=0-471-90435-X}}</ref> French agriculturist [[Olivier de Serres]] noted that transplanting mushroom [[mycelia]] would lead to the propagation of more mushrooms.

Originally, cultivation was unreliable as mushroom growers would watch for good flushes of mushrooms in fields before digging up the mycelium and replanting them in beds of composted manure or inoculating 'bricks' of compressed litter, [[loam]], and manure. Spawn collected this way contained pathogens and crops commonly, would be infected, or not grow at all.<ref>{{harvnb|Genders|1969|p=19}}</ref> In 1893, sterilized, or pure culture, spawn was discovered and produced by the [[Pasteur Institute]] in Paris, for cultivation on composted horse manure.<ref>{{harvnb|Genders|1969|p=18}}</ref>

Today's commercial variety of the common mushroom originally was a light brown color. In 1926, a [[Pennsylvania]] mushroom farmer found a clump of common mushrooms with white caps in his mushroom bed. As with the reception of white bread, it was seen as a more attractive food item and became very popular.<ref>{{harvnb|Genders|1969|p=121}}</ref> Similar to the commercial development history of the [[navel orange]] and [[Red Delicious]] apple, cultures were grown from the mutant individuals, and most of the cream-colored store mushrooms marketed today are products of this 1926 chance natural mutation.

''A. bisporus'' is now cultivated in at least seventy countries throughout the world.<ref name=Cappelli84/> Global production in the early 1990s was reported to be more than 1.5 million tons, worth more than US$2 billion.<ref name=Chang1993>{{cite book|author=Chang ST|veditors=((Chiu S-W)), Buswell J, ((Chang S-T)) |chapter=Mushroom biology: the impact on mushroom production and mushroom products|title=Mushroom Biology and Mushroom Products|publisher=[[Chinese University Press]]|place=[[Hong Kong]]|year=1993|pages=3–20|isbn=962-201-610-3}}</ref>

==Nutritional profile==
{{nutritional value
| name=Agaricus bisporus, white raw
| water=92.45 g
| kJ=93
| protein=3.09 g
| fat=0.34 g
| carbs=3.26 g
| fiber=1 g
| sugars=1.98 g
| iron_mg=0.5
| magnesium_mg=9
| phosphorus_mg=86
| potassium_mg=318
| sodium_mg=3
| zinc_mg=0.52
| vitC_mg=2.1
| thiamin_mg=0.081
| riboflavin_mg=0.402
| niacin_mg=3.607
| pantothenic_mg=1.497
| vitB6_mg=0.104
| folate_ug=17
| vitB12_ug=0.04
| vitD_ug=0.2
| source_usda = 1
| note=[http://ndb.nal.usda.gov/ndb/search/list?qlookup=11260&format=Full Link to USDA Database entry]
}}

In a 100-gram serving, raw white mushrooms provide {{convert|93|kJ|kcal|abbr=off}} of [[food energy]] and are an excellent source (> 19% of the [[Daily Value]], DV) of the [[B vitamins]], [[riboflavin]], [[niacin]], and [[pantothenic acid]] (table). Fresh mushrooms are also a good source (10–19% DV) of the [[dietary mineral]] [[phosphorus]] (table).

While fresh ''A. bisporus'' only contains 0.2 micrograms (8 IU) of [[vitamin D]] as [[ergocalciferol]] (vitamin D2), the ergocalciferol content increases substantially after exposure to [[UV light]].<ref>{{cite news|url=http://articles.latimes.com/2008/mar/31/health/he-eat31|title=Mushrooms and vitamin D|publisher=[[Los Angeles Times]]|accessdate=23 August 2003}}</ref><ref>{{cite journal|vauthors=Koyyalamudi SR, Jeong SC, Song CH, Cho KY, Pang G |title=Vitamin D2 formation and bioavailability from ''Agaricus bisporus'' button mushrooms treated with ultraviolet irradiation |journal=[[Journal of Agricultural and Food Chemistry]]|volume=57|issue=8|pages=3351–5|date=April 2009|pmid=19281276|doi=10.1021/jf803908q}}</ref>

==Research==
Mushrooms contain [[hydrazine]] derivatives, including [[agaritine]] and [[gyromitrin]], that have been evaluated for [[carcinogenic]] activity.<ref>{{cite journal |vauthors=Hashida C, Hayashi K, Jie L, Haga S, Sakurai M, Shimizu H |title=[Quantities of agaritine in mushrooms (''Agaricus bisporus'') and the carcinogenicity of mushroom methanol extracts on the mouse bladder epithelium]|language=Japanese|journal=Nippon Koshu Eisei Zasshi|volume=37|issue=6|pages=400–5|date=June 1990|pmid=2132000}}</ref> Agaritine, a hydrazine, poses no toxicological risk to humans when mushrooms are consumed in typical amounts.<ref>{{cite journal |vauthors=Roupasa P, Keogh J, Noakes M, Margettsa C, Taylor P |url=http://www.sciencedirect.com/science/article/pii/S1756464610000241 |title=Mushrooms and agaritine: A mini-review |journal=Journal of Functional Foods|volume=2| issue=2|date=April 2010|pages=91–8|doi=10.1016/j.jff.2010.04.003}}</ref>

==Gallery==
<gallery perrow="5">
Image:Portobello_mushrooms.jpg|Portobello mushrooms
Image:Giant mushroom cross-section.jpg|Cross-section of a portobello cultivar
Image:Giant mushroom underside.jpg|Ventral view of a portobello cultivar with a bisected [[Stipe (mycology)|stipe]]
Image:Agaricus bisporus (Cup mushroom, doubled).jpg|Two ''Agaricus bisporus'' mushrooms that have fused together
Image:Agaricus bisporus Zuchtchampignon2.jpg|White ''Agaricus bisporus''
File:2014 11 Portobello rocket feta toasted baguette.jpg|Grilled portobello, [[feta]], and [[Eruca sativa|rocket]] salad on toasted [[baguette]]
</gallery>

==See also==
{{Portal|Fungi}}
*[[Fungiculture]]
*[[List of Agaricus species|List of ''Agaricus'' species]]

==Notes==
{{Reflist|2|refs=<ref name=Imbach1946>{{cite journal|author =Imbach EJ|year=1946|title=Pilzflora des Kantons Luzern und der angrenzen Innerschweiz|journal=Mitteilungen der naturforschenden Gesellschaft Luzern|volume=15|pages=5–85|language=German}}</ref>}}

==References==
*{{cite book|last=Benjamin|first=Denis R.|title=Mushrooms: poisons and panaceas—a handbook for naturalists, mycologists and physicians|publisher=WH Freeman and Company|place=New York|year=1995|isbn=0-7167-2600-9}}
*{{cite book|last=Genders|first=Roy|title=Mushroom Growing for Everyone|year=1969|publisher=Faber|place=London|isbn=0-571-08992-5|ref=harv}}

== External links ==
{{Commons|Agaricus bisporus|Agaricus bisporus}}
{{Wikispecies}}

{{portalbar|Fungi|Biology|Europe|North America}}

{{Use dmy dates|date=May 2011}}
{{taxonbar}}
{{Authority control}}

{{DEFAULTSORT:Agaricus Bisporus}}
[[Category:Agaricus|bisporus]]
[[Category:Edible fungi]]
[[Category:Fungi in cultivation]]
[[Category:Fungi of Europe]]
[[Category:Fungi of North America]]
[[Category:Fungi described in 1926]]