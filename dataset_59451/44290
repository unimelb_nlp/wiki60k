{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox airport
| name         =Ramsgate Airport
| nativename   =RAF Ramsgate
| nativename-a =
| nativename-r =
| image        =Ensign of the Royal Air Force.svg
| image-width  = 90px
| caption      =
| IATA         =
| ICAO         =X2RT
| FAA          =
| TC           =
| LID          =
| GPS          =
| WMO          =
| type         = Closed
| owner-oper   =
| owner        =
| operator     =
| city-served  =[[Ramsgate]]
| location     =
| hub          =
| built        = <!-- military airports -->
| used         = <!-- military airports -->
| commander    = <!-- military airports -->
| occupants    = <!-- military airports -->
| elevation-f  =
| elevation-m  =
| website      =
| coordinates  = {{coord|51|21|14.67|N|1|24|27.52|E|type:airport_region:GB|display=inline,title}}
| pushpin_map            =Kent
| pushpin_label          = Ramsgate Airport
| pushpin_map_caption    = Location in Kent
| r1-number    =
| r1-length-f  = 3,000
| r1-length-m  = {{Convert|3000|ft|disp=output number only|0}}
| r1-surface   = Grass<!-- up to r8 -->
| stat-year    =
| stat1-header =
| stat1-data   = 
| footnotes    =
}}

'''Ramsgate Airport''' was a civil airfield at [[Ramsgate]], [[Kent]], United Kingdom which opened in July 1935. It was briefly taken over by the [[Royal Air Force]] in the Second World War, becoming '''RAF Ramsgate'''. The airfield was then closed and obstructed to prevent its use. It reopened in 1953 and served until final closure in 1968. The site has now been redeveloped as an industrial estate.

==History==

===1935 - 40===
Ramsgate was selected to be the site of a Landing Ground during the First World War, but no work was carried out.{{sfn|Delve|2005|p=202}} Built at a cost of [[Pound Sterling|£26,000]],<ref name=Times050737/> Ramsgate Airport opened on 1 July 1935,<ref name=Flight060236>{{cite journal|title=Ramsgate's Airport |journal=Flight |issue=6 February 1936 |page=147 |url=http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%200331.html}}</ref> on a {{convert|90|acre|ha|adj=on}} site.<ref name=Flight241035>{{cite journal |title=Ramsgate Extension |journal=Flight |issue=24 October 1935 |page=446 |url=http://www.flightglobal.com/pdfarchive/view/1935/1935%20-2-%200492.html}}</ref> With the opening of the airport, [[Hillman's Airways]] inaugurated a service to Belgium. As [[Ostend Stene Airport|Ostend Airport]] was not then ready, services were initially to [[Le Zoute Airfield]], [[Knokke]]. Four services per day were operated.<ref name=Times210635>{{Cite newspaper The Times |articlename=Expanded Air Services |day_of_week=Friday |date=21 June 1935 |page_number=11 |issue=47096 |column=D}}</ref> The airport was operated by Ramsgate Airport Ltd, which was registered as a private limited company on 20 July 1935 with capital of [[Pound Sterling|£5,000]] in £1 shares.<ref name=Flight080835>{{cite journal |title=New Companies |journal=Flight |issue=8 August 1935 |page=164 |url=http://www.flightglobal.com/pdfarchive/view/1935/1935%20-2-%200182.html}}</ref> The directors were a Mr F Gwatkin, [[Richard Seaman]] and [[Whitney Straight]]. The official opening ceremony was performed by the [[Mayor]] of Ramsgate, who was then flown to Belgium in a [[de Havilland Dragon]] by [[Neville Stack]]. A representative from [[Crilly Airways]] also attended the dinner to celebrate the opening of the airport.<ref name=Flight250735>{{cite journal|title=London, Ramsgate and Ostend |journal=Flight |issue=25 July 1935 |page=113 |url=http://www.flightglobal.com/pdfarchive/view/1935/1935%20-2-%200127.html}}</ref> Customs facilities were provided within a month of the airport opening.<ref name=Times250735>{{Cite newspaper The Times |articlename=News in Brief |day_of_week=Thursday |date=25 July 1935 |page_number=11 |issue=47125 |column=G}}</ref> In October 1935, plans were made to extend the airport, with a [[control tower]] and  a [[hangar]] amongst the facilities to be provided.<ref name=Flight241035/>

In February 1936, the site measured {{convert|850|by|860|yd|abbr=on}}. Fuel and customs facilities had been provided and a temporary hangar erected.<ref name=Flight060236/> By March 1936, the Thanet Aero Club had been formed at Ramsgate, operating a [[de Havilland Hornet Moth]].<ref name=Flight090436>{{cite journal|title=Ramsgate |journal=Flight |issue=9 April 1936 |page=378 |url=http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%200889.html}}</ref> [[Mignet HM.14]] "Flying Flea" aircraft had been grounded following a crash at [[Penshurst Airfield|Penshurst]] in May 1936,<ref name=LHS>{{cite web|url=http://www.leighhistorical.org.uk/References/Leigh_at_War.pdf |title=Leigh in the War, 1939–45 |publisher=Leigh and District Historical Society |date=September 1993 |accessdate=24 February 2010}}</ref><ref name=Flight070536>{{cite journal|title=Another Fatal "Pou" Accident |journal=Flight |issue=7 May 1936 |page=492 |url=http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%201189.html}}</ref> The ban had been lifted following [[wind tunnel]] tests which resulted in modifications to the wings.<ref name=Flight060836a>{{cite journal|title=Pou Redivivous |journal=Flight |issue=6 August 1936 |page=132 |url=http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%202147.html}}</ref> A race was held on 3 August in which seven aircraft took part, with a £100 prize at stake. An eighth aircraft was flown by  [[Henri Mignet]], who performed various aerobatics. The aircraft bore the inscription "Flying Flea flies in England. I thank the Air Ministry".<ref name=Times040836>{{Cite newspaper The Times |articlename="Flying Flea" Race |day_of_week=Tuesday |date=4 August 1936 |page_number=7 |issue=47444 |column=C}}</ref> Following the race, [[Short Scion]] aircraft flew pleasure flights.<ref name=Flight060836b>{{cite journal|title=The Four Winds |journal=Flight |issue=6 August 1936 |page=138 |url=http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%202155.html}}</ref> From 29 August - 26 September, an Aviation Camp was held at Ramsgate. It was sponsored by the National League of Airmen.<ref name=Times210836>{{Cite newspaper The Times |articlename=Aviation Camps |day_of_week=Friday |date=21 August 1936 |page_number=8 |issue=47459 |column=D}}</ref>

[[File:Ramsgate Airport clubhouse.jpg|thumb|right|The clubhouse with the main hangar behind. Summer 1968.]]
On 3 July 1937, the official opening of Ramsgate Airport was performed by [[Director-General of Civil Aviation]] Sir [[Francis Shelmerdine]].<ref name=Times050737>{{Cite newspaper The Times |articlename=Ramsgate Airport |day_of_week=Monday |date=5 July 1937 |page_number=11 |issue=47728 |column=D}}</ref> The airport building incorporated the [[control tower]] at its centre.<ref name=Flight080737>{{cite journal|title=A Thanet Inauguration |journal=Flight |issue=8 July 1937 |pages=63–64}} ([http://www.flightglobal.com/pdfarchive/view/1937/1937%20-%201949.html p63], [http://www.flightglobal.com/pdfarchive/view/1937/1937%20-%201950.html p64])</ref> From 17–31 July, [[No. 611 Squadron RAF|611 (West Lancashire) (Bomber) Squadron]], [[Royal Air Force]] held a camp at Ramsgate.<ref name=Flight100637>{{cite journal|title=Royal Air Force |journal=Flight |issue=10 June 1937 |page=583 |url=http://www.flightglobal.com/pdfarchive/view/1937/1937%20-%201539.html}}</ref> In August, an air race was held at Ramsgate. The race was won by Paul Elwell in [[Taylor Cub]] G-AESK. [[Alex Henshaw]] was third in [[Percival Mew Gull]] G-AEXF and [[Geoffrey de Havilland, Jr.|Geoffrey de Havilland]] was seventh in [[De Havilland T.K.2]] G-ADNO. Sixteen aircraft participated in the race. Thirteen of them were British, with two German and one [[Latvia]]n entry.<ref name=Flight260837>{{cite journal|title=Racing at Ramsgate |journal=Flight |issue=26 August 1937 |pages=218–19}} ([http://www.flightglobal.com/pdfarchive/view/1937/1937%20-%202382.html p218], [http://www.flightglobal.com/pdfarchive/view/1937/1937%20-%202383.html p219])</ref>

At Easter 1938, a week-long aviation camp was held at Ramsgate. This was followed by another held from 4 June - 17 September.<ref name=Flight070439>{{cite journal|title=From the Clubs and Schools - Ramsgate |journal=Flight |issue=7 April 1938 |page=342 |url=http://www.flightglobal.com/pdfarchive/view/1938/1938%20-%200964.html}}</ref> In July, [[Southern Airways Ltd]] started a twice-daily service between [[Ilford Aerodrome|Ilford]], [[Essex]] and Ramsgate.<ref name=Times260737>{{Cite newspaper The Times |articlename=Ilford to Ramsgate Air Service |day_of_week=Tuesday |date=26 July 1938 |page_number=14 |issue=48056 |column=D}}</ref>

In April 1939, the annual aviation camp was organised by the [[Civil Air Guard]]. An intensive two-week course enabled pilots to obtain their "A" licence at a cost of [[£sd|£10 3s]]. In previous years it would have cost £25 13s to obtain an A licence.<ref name=Times270439>{{Cite newspaper The Times |articlename=Ramsgate Flying Camp Reopened |day_of_week=Thursday |date=27 April 1939 |page_number=14 |issue=48290 |column=B}}</ref>

In 1940, Ramsgate was used as a satellite of [[RAF Manston]] during the [[Battle of Britain]]. In August, during one of the many raids on Manston, Ramsgate was also attacked and the field cratered.<ref>http://www.kenthistoryforum.co.uk/index.php?topic=2288.5;wap2</ref> With the need for Ramsgate as a satellite airfield diminishing following the end of the battle, the airfield was closed and obstructions placed on it to prevent its use.{{sfn|Delve|2005|p=202}} Post-war, the site was used for agricultural purposes.<ref name=Flight030753>{{cite journal|title=Ramsgate Reopened |journal=Flight |issue=3 July 1953 |page=5 |url=http://www.flightglobal.com/pdfarchive/view/1953/1953%20-%200849.html}}</ref>

===1952 - 68===
[[File:Ramsgate Airport Miles M65 Gemini 1A G-AJZO.jpg|thumb|right|The main hangar with the Air Kruise logo.]]
[[File:Ramsgate Airport Auster 5 G-AJAK.jpg|thumb|right|Thanet Flying Club Auster 5 G-AJAK.]]
On 1 June 1952, Ramsgate Airport reopened. [[Air Kruise|Air Kruise (Kent) Ltd]] had taken a 21-year lease on the land.<ref name=Flight120653/> They extended their [[Le Touquet – Côte d'Opale Airport|Le Touquet]] - [[Lympne Airport|Lympne]] service to Ramsgate. The war-damaged hangars and buildings were repaired. A {{convert|1000|yd|m}} grass runway was available, suitable to operate [[Avro Anson]], [[de Havilland Dragon Rapide]] and [[de Havilland Heron]] aircraft.<ref name=Flight090552>{{cite journal|title=Ramsgate To Re-open |journal=Flight |issue=9 May 1952 |page=569 |url=http://www.flightglobal.com/pdfarchive/view/1952/1952%20-%201301.html}}</ref> The first aircraft to land at Ramsgate was [[Auster Autocrat|Auster J/1 Autocrat]] G-AIZZ.{{sfn|Collyer|1992|p=118}} Early services from Ramsgate were operated by Dragon Rapides.{{sfn|Collyer|1992|p=143}}

Ramsgate Airport was officially reopened on 27 June 1953 by [[Minister for Civil Aviation]] [[Alan Lennox-Boyd, 1st Viscount Boyd of Merton|Alan Lennox-Boyd]].<ref name=Flight030753/> The [[prototype]] Heron was amongst the aircraft that gave displays at the opening ceremony.<ref name=Flight030753/> A new flying club was formed, the Ramsgate Flying Club.<ref name=Flight120653>{{cite journal |title=Ramsgate Re-opening |journal=Flight |issue=12 June 1953 |page=740 |url=http://www.flightglobal.com/pdfarchive/view/1953/1953%20-%200746.html}}</ref> Air Kruise moved its operations from Lympne to Ramsgate later that year.{{sfn|Collyer|1992|p=143}} Air Kruise traded as Trans-Channel Airways. It carried 22,500 passengers in 1953.<ref name=Flight210554>{{cite journal |title=Air Kruise, Ltd., (Trans-Channel Airways) |journal=Flight |issue=21 May 1954 |url=http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%201526.html}}</ref>

On 1 May 1954, Air Kruise was taken over by  [[British Aviation Services|Britavia]], which also owned [[Aquila Airways]] and [[Silver City Airways]], but kept its separate identity.<ref name=Flight141055>{{cite journal |title=Air Kruise |journal=Flight |issue=14 October 1955 |page=628 |url=http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201513.html}}</ref> In 1958, Air Kruise was absorbed into Silver City. [[Hugh Kennard]], joint managing director of Silver City, formed Aircraft Engineering and Maintenance Ltd (AEM) at Ramsgate.The company overhauled aircraft engine gearboxes,<ref name=Twilight2>[http://www.michaelsbookshop.com/tp/id3.htm Finnis 2007, Chapter Two]</ref> hydraulic systems and instruments. As of 2011, AEM is known as Aviation Engineering & Maintenance Ltd and is a part of [[Rio Tinto Zinc]].<ref name=AEAT>{{cite journal|title=AEM Offer Comprehensive Repair and Overhaul Service |journal=Aircraft Engineering and Aerospace Technology |volume= 59 |issue= 4 |page=13|url=http://www.emeraldinsight.com/journals.htm?articleid=1683723&show=abstract |doi=10.1108/eb036425}}</ref> By 1960, AEM assembled the first [[Champion Tri-Traveller]] aircraft in the UK, G-APYT, at Ramsgate.<ref name=Flight270560>{{cite journal |title=Sport and Business |journal=Flight |issue=27 May 1960 |page=734 |url=http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%200734.html}}</ref> Further Tri-Travellers were also assembled at Ramsgate.<ref name=Flight100660>{{cite journal |title=Champion Tri-Traveler in the Air |first=Mark |last=Lambert |journal=Flight |issue=10 June 1960 |pages=789–90}} ([http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%200781.html p789], [http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%200782.html p790])</ref>

In 1963, Chrisair were operating [[de Havilland Dragon]] G-ADDI from Ramsgate for pleasure flights.<ref name=FI210263>{{cite journal |title=Stevens Centralization |journal=Flight International |issue=21 February 1963 |page=270 |url=http://www.flightglobal.com/pdfarchive/view/1963/1963%20-%200284.html}}</ref> In March 1965, Kennard formed [[Invicta International Airlines]], based at [[Manston Airport]], but with a head office based at Ramsgate.<ref name=FI150465>{{cite journal |title=World Airline Survey... |journal=Flight International |issue=15 April 1965 |page=586 |url=http://www.flightglobal.com/pdfarchive/view/1965/1965%20-%202152.html}}</ref> Ramsgate Airport closed in 1968.{{sfn|Delve|2005|p=202}}

==Accidents and incidents==
*On 5 August 1935, an aircraft operating a pleasure flight from Ramsgate suffered a broken oil pipe. A forced landing was made at [[Northwood, Thanet]]. The aircraft tipped on its nose when it ran through a fence but the five passengers were uninjured. The aircraft sustained minor damage.<ref name=Times060835>{{Cite newspaper The Times |articlename=Children In Flying Mishap|day_of_week=Tuesday |date=6 August 1935 |page_number=12 |issue=47135 |column=D}}</ref>
*On 17 July 1938, a light aircraft belonging to Thanet Aero Club crashed into the sea off [[Cliftonville]], killing both occupants. The pilot had been performing aerobatics over {{HMS|Revenge|06|6}} when it entered a spin.<ref name=Times180738>{{Cite newspaper The Times |articlename=Two Killed In Flying Accident |day_of_week=Monday |date=18 July 1938 |page_number=12 |issue=48049 |column=G}}</ref>
*On 29 June 1957, [[de Havilland Dragon Rapide]] G-AGUE of [[Island Air Services]] crashed on take-off whilst operating a local pleasure flight. The aircraft was written off, but all on board escaped uninjured.{{sfn|Humphreys|2001|p=p169}}

==References==
{{reflist|30em}}

==Sources==
*{{cite book | first = David G| last = Collyer| year = 1992| month = | title = Lympne Airport in old photographs | publisher = Alan Sutton Publishing Ltd| location = Stroud| isbn = 0-7509-0169-1 |ref=harv}}
*{{cite book |last= Delve|first= Ken|authorlink= |coauthors= |title= The Military Airfields of Britain. Southern England: Kent, Hampshire, Surrey and Sussex |year=2005 |publisher= The Crowood Press Ltd |location=Ramsbury |isbn=1-86126-729-0 |ref=harv}}
*{{cite book |last= Finnis|first= Malcolm|authorlink= |coauthors= |title= Twilight of the Pistons. Air Ferry, a Manston Airline |year=2007 |publisher= Michael's Bookshop |location=Ramgate |isbn=978-1-905477-93-7 |ref=harv}}
*{{cite book |last=Humphreys |first=Roy | title = Kent Aviation, A Century of Flight | publisher = Sutton Publishing |location=Stroud | year = 2001 | isbn = 0-7509-2790-9 |ref=harv}}

{{Defunct airports in the United Kingdom}}

[[Category:Defunct airports in England]]
[[Category:Airports in Kent]]
[[Category:Ramsgate]]
[[Category:1935 establishments in England]]
[[Category:Airports established in 1935]]