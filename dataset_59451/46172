{{Orphan|date=September 2016}}

The '''Art for Amnesty-Atelier Pinton Tapestries''' are an ongoing collection of giant memorial tapestries designed by artist [[Peter Sís]] and created by renowned French tapestry manufacturer Ateliers Pinton for Art for Amnesty, [[Amnesty International]]'s global artist engagement program.<ref>{{cite web|url=https://www.amnesty.org/en/press-releases/2015/12/cape-town-international-airport-unveils-giant-mandela-memorial-tapestry/|title=Cape Town International Airport unveils giant Mandela memorial tapestry|publisher=}}</ref><ref>{{cite web|url=http://www.amnestyusa.org/news/press-releases/global-artists-fund-giant-cape-town-international-airport-memorial-dedicated-to-nelson-mandela|title=Global artists fund giant Cape Town International Airport memorial dedicated to Nelson Mandela|publisher=}}</ref><ref>{{cite web|url=http://www.belfasttelegraph.co.uk/news/republic-of-ireland/heaney-tapestry-unveiled-at-airport-30211503.html|title=Heaney tapestry unveiled at airport - BelfastTelegraph.co.uk|publisher=}}</ref><ref>{{cite web|url=http://www.hollywoodreporter.com/news/video-paul-simon-praises-irish-698946|title=Paul Simon Praises Irish Poet, Unveils Amnesty International Tapestry at Dublin Airport (Video)|publisher=}}</ref><ref>{{cite web|url=http://www.pinton1867.com/amnesty-international-pinton-peter-sis/?lang=en|title=Pinton - Amnesty International & Pinton & Peter Sis|publisher=}}</ref>

==Tapestries==
Five tapestries have been commissioned by Art for Amnesty founder Bill Shipsey so far with several others planned for 2017 and following years.

===''Flying Man''===
The first tapestry, "The Flying Man" in honour of Czech dramatist, dissident and then President Václav Havel, was commissioned in 2012. Following Havel's death in December 2011 Shipsey's long time friend, Czech born artist [[Peter Sís]], created an illustration in memory of Havel for the front page of Czech newspaper [[Hospodářské noviny]], commemorating Havel. Shortly thereafter an online campaign to name Prague Airport as the 'Vaclav Havel Airport' began. When this was successful, and the Czech Government decided to name the Airport in Havel's honour, Shipsey came up with the idea to transform Sís' image into a giant memorial tapestry for display at the airport.<ref name="slj.com">{{cite web|url=http://www.slj.com/2014/06/industry-news/author-peter-sis-soars-high-with-airport-tapestries/|title=Author Peter Sís Soars High with Airport Tapestries|publisher=}}</ref>

This tapestry was funded high-profile supporters of Amnesty International: musicians [[Bono]], Edge, [[Peter Gabriel]], Sting and [[Yoko Ono]]. The tapestries were hand-woven by Ateliers Pinton, the renowned tapestry weaving atelier in Felletin-Aubusson, France.<ref name="pinton1867.com">{{cite web|url=http://www.pinton1867.com/la-tapisserie/?lang=en|title=Pinton -   The tapestry|publisher=}}</ref><ref name="french-lifestyle.com">{{cite web|url=http://french-lifestyle.com/2012/07/16/ateliers-pinton-rare-tapestries-and-carpets/|title=French-LifeStyle - Ateliers Pinton – Rare Tapestries and Carpets|publisher=}}</ref>

The resulting 4 by 5 metre tapestry depicts a striking image of a flock of white birds, in the shape of a man, set against a vivid blue background. Beneath him is the silhouette of Prague Castle and the Vltava river. One of the birds at the centre is shown carrying a red heart, a symbol Havel frequently used when signing his name.<ref name="radio.cz">{{cite web|url=http://www.radio.cz/en/section/curraffrs/artwork-tribute-funded-by-international-rock-stars-unveiled-at-vaclav-havel-airport|title=Radio Prague - Artwork tribute funded by international rock stars unveiled at Václav Havel Airport|publisher=}}</ref>

[[Karel Schwarzenberg]], Czech Foreign Minister at the time, and long time friend of Havel, praised the tapestry, saying, "Václav Havel would have been glad about it. Of course, he would have been even happier if he had heard that this wonderful gift in his memory was financed by co-artists--people of the same thinking and talent like him. I know how glad he was to meet any artist, be it from abroad or from the Czech Republic, who came to Prague and visited him. That was his life. They were his people.”<ref name="radio.cz"/>

"Flying Man" tapestry currently hangs in Terminal 2 at Prague's Václav Havel Airport.

===''Out of the Marvellous''===
A second tapestry also designed by Sis and created by Pinton "Out of the Marvellous," was created in honour of Nobel Literature Laureate, Irish poet and long time Amnesty supporter [[Seamus Heaney]]. This tapestry, too, is themed around flight. It portrays Heaney flying over Ireland, using a giant book of his poetry as a parachute.<ref name="irishnews.com">{{cite web|url=http://www.irishnews.com/news/2014/04/24/news/out-of-the-marvellous-89911/|title=Out of the marvellous|publisher=}}</ref>

The name "Out of the Marvellous" comes from Heaney's poem "Lightenings viii." Lines from the poem are woven into the backdrop of the artwork. The tapestry took five months to be woven in Felletin, and measures 4 by 4.5 metres. It was unveiled by one of its patron's singer [[Paul Simon]] in April 2014 and hangs at Dublin Airport's T2.<ref>{{cite web|url=https://www.youtube.com/watch?v=TaifsE9848c|title=Seamus Heaney Honoured at Dublin Airport|first=|last=Dublin Airport|date=24 April 2014|publisher=|via=YouTube}}</ref>

Musician [[The Edge]], another of the funders of the tapestry, along with his band mate  [[Bono]], said, "Seamus Heaney was an inspiration to our band--as well as to politicians, artists, dreamers and all in between, from every corner of the world. I love the idea that the words of this great poet--and Sís's beautiful tapestry--will send travellers from Ireland and beyond safely on their way 'out of the marvellous.'"<ref name="irishnews.com"/>

===''Yellow Submarine''===

In July 2015, a third Pinton/Sis tapestry, in memory of musician and activist [[John Lennon]], and in gratitude for his wife Yoko Ono and her generosity to Amnesty International, was unveiled at the [[Ellis Island]] National Museum of Immigration, in New York City harbour. It features Manhattan as a yellow submarine with a small John Lennon figure peeping out of the yellow submarine, flashing his famous peace sign.<ref>{{cite web|url=https://www.theguardian.com/music/video/2015/jul/30/bono-yoko-ono-john-lennon-ellis-island-tapestry-video|title=Bono and Yoko Ono dedicate Ellis Island tapestry to honor John Lennon – video|first=Source:|last=Guardian|date=30 July 2015|publisher=|via=The Guardian}}</ref> The tapestry is 7.3 metres wide and 3 metres high. It was paid for by [[Bono]], Edge and [[Jimmy Iovine]].

===''Flying Madiba''===
In December 2015, a fourth Pinton/Sis tapestry, measuring 6 X 3 metres, was unveiled and dedicated at Cape Town International Airport. This artwork honours anti apartheid leader and former South African President [[Nelson Mandela]]. It is titled, "Flying Madiba" and was unveiled on December 10, 2015, which is International [[Human Rights Day]].<ref>{{cite web|url=http://ewn.co.za/Media/2015/12/10/Flying-Madiba-tapestry-unveiled|title='Flying Madiba' unveiled at Cape Town International Airport|first=Aletta|last=Harrison|publisher=}}</ref> The tapestry, somewhat similar to "Flying Man", depicts a figure flying over South Africa. This tapestry was funded by [[Bono]], Edge, Sting, [[Peter Gabriel]], and [[John Legend]].

===Flying Man ll===
In July 2016 a fifth tapestry, and a second honouring Vaclav Havel, was created by Pinton to a modified version of the original design by Peter Sis. This tapestry was funded by Czech philanthropists and art patrons [[Karel Janacek]] and [[Libor Winkler]]. This tapestry is ultimately destined for the main meeting room of the Vaclav Havel Building of the European Parliament in Strasbourg when its renovation is completed in the spring of 2017. Meanwhile, the tapestry will tour and be displayed in the National Czech & Slovak Museum in Cedar Rapids, Iowa. At the Bohemian National Hall in New York City. And at DOX Contemporary Art Centre in Prague. The dimensions of the tapestry are 3 metres long and 2 metres high.

===Future Projects===
Two further Pinton tapestries to Peter Sis' design are in development. One will honour U.S. Civil Rights Leader [[Dr. Martin Luther King]] and the second former U.S. First Lady and Chair of the U.N. Human Rights Committee that promulgated the Universal Declaration of Human Rights, [[Eleanor Roosevelt]].

==Peter Sís==
[[Peter Sís]] is a highly awarded Czech-born American illustrator and children's books writer. He won the [[Hans Christian Andersen Medal]] in 2012 for his large legacy of children's book illustrations, and is an eight-time winner of the New York Times Best Illustrated Book award.<ref>{{cite web|url=http://www.ibby.org/index.php?id=1186|title=2012|publisher=}}</ref>

He has also been awarded with the American Library Association's Caldecott Honor for the illustrations of his 1996 book, Starry Messenger, the 1998 book Tibet Through The Red Box, and his 2007 work, The Wall: Growing Up Behind the Iron Curtain. The latter book also received the ALA's 2008 Robert Silbert Medal for the most distinguished informational book for young readers. He has received a Boston Globe–Horn Book Award four times: for Komodo (1993), A Small Tall Tale From the Far Far North (1994), Tibet Through The Red Box (1999), and The Wall: Growing Up Behind the Iron Curtain (2008).[11]

He won the Deutscher Jugendliteraturpreis for Tibet Through the Red Box.

Sís has won the Golden Bear Award at the 1980 West Berlin Film Festival for an animated short. He has also won the Grand Prix Toronto and the Cine Golden Eagle Award.

In 2012, when "Flying Man" was unveiled at Prague airport, in memory of Havel, Sís explained his design to [[Radio Prague]]. He said, "You look at the sky, you see the flock of birds, you see somebody who can inspire you for one second, and then the birds fly in different directions and then the person is gone, like Václav Havel was gone… I grew up when he was active with Charter 77 and his Helsinki activities. At the same time, we thought we were into rock’n’roll and we thought we that were in opposition too. But only after all these years have I realised that we should have listened more, that he was amazingly brave and smart about what he was doing.”<ref name="radio.cz"/>

==Ateliers Pinton==
Ateliers Pinton have created fine tapestries in Felletin-Aubusson, France since 1867,<ref name="french-lifestyle.com"/> They use Basse-Lisse looms that allow weavers create large-scale tapestries. All of Pinton's fibres and threads are dyed in-house; they have a collection of 10,000 shades of yarn. Each Pinton tapestry carries with it a signature "bolduc," a label describing the carpet or tapestry and its edition number.<ref name="pinton1867.com"/> Pinton was responsible for weaving the original works of [[Fernand Léger|Léger]], [[Miró]], [[Calder]]{{disambiguation needed|date=September 2016}}, [[Delaunay]]{{disambiguation needed|date=September 2016}} and others, starting in the 1950‘s.

Pinton has the label « Entreprise du Patrimoine Vivant ». This certification was created by France to give recognition to companies which excel in their particular field of activity, whether their expertise lies in centuries old manufacturing traditions or in hightech industries.  Pinton  are also a member of the Association «Luxe & Excellence » created in 2010. This association brings together a number companies involved in the luxury business and based in the Limousin region. Its mission is to promote to the outside world the products and expertise of its members. Clients of such companies are safe in the knowledge that they are engaging with professionals who are widely recognised for their talents and for their attachment to their geographical roots and to the «Made in France» label.

Atelier Pinton, led by Lucas Pinton, is the last manufacturer of the Aubusson-Felletin Tapestry. Training facilities are offered to young artisans who are brought into contact with internationally recognised designers and artists. This approach, that links past and present, now translates into a new organisation with two different parts : PINTON Manufacture and PINTON Editions.

===The Tapestry Cartoon===
Long before weaving begins, the cartoonist and colourist of Pinton transposes the given artwork onto a scale model in paper of the eventual tapestry. This scale model, is placed beneath the threads of the warp, and is the only guide provided to the weavers.

===The Dying Process===
All yarn is produced and dyed to sample specification in the atelier in Felletin. Pinton's local expertise enables them to produce an infinite number of durable colours. They also have a stock of over ten
thousand samples of dyed yarn.

===The Yarns===
It is at this stage of the cartoon production that Pinton craftsmen prove their expertise in mixing colours to the best effects. Through constant exchanges between painters, weavers and the original artist, rope of coloured threads of different hues is made up to match the cartoon palette so to become a reference for the weaver.

===The Horizontal Loom===
Pinton uses the traditional tapestry weaving “Basse Lisse” technique, where the warp extends horizontally between the rolls. This centuries old technique is ideal for the production of very large scale tapestries, but is also very well suited for the manufacture of contemporary designs. Once the weaving and the hand finishes are completed, the tapestry is cut off from the loom, a moving and emotional moment for those involved who are at last able to contemplate the fruit of all their work.

===The 'Bolduc'===
Each creation is signed by the artist on the «bolduc» sewn to the back of the tapestry and bears the numbers and other identifiers of the piece.

==Art for Amnesty==
Art for Amnesty<ref>{{cite web|url=http://www.amnestyusa.org/about-us/art-for-amnesty|title=Art for Amnesty|publisher=}}</ref><ref>{{cite web|url=https://www.amnesty.org/en/art-for-amnesty/|title=Art for Amnesty|publisher=}}</ref> is [[Amnesty International]]'s program for engaging artists worldwide in the support of human rights. It is a global community of artists of all disciplines and nationalities who share Amnesty International’s vision of a world where human rights are enjoyed by all. It is home to the world’s storytellers, those willing to shine a light on injustice by using visual or fine art, film, literature, poetry, music, photography, design or performance to express what it means to be human. Art for Amnesty has produced multiple artist lead projects, such as:
*[[Instant Karma!|Instant Karma]], Amnesty's multi-star benefit album of John Lennon compositions. The album was nominated for a number of [[Grammies]] and took first spot on iTunes when it released.<ref>{{cite web|url=https://news.google.com/newspapers?nid=1110&dat=20070824&id=_V4lAAAAIBAJ&sjid=LRUGAAAAIBAJ&pg=1217,4458594&hl=en|title=Vineyard Gazette - Google News Archive Search|publisher=}}</ref>
*"Small Places Tour," a music concert project in 2008 which collaborated with over 800 concerts worldwide, in 40 countries.
* The "Toast to Freedom" song which featured 50 singers from around the globe, and was launched worldwide on 3 May 2012.<ref>{{cite web|url=http://toasttofreedom.org/bill-shipsey/|title=Bill Shipsey - Toast To Freedom|publisher=}}</ref>

Every year, Art for Amnesty produces and presents its  [[Ambassador of Conscience Award]]. Past winners include [[Nelson Mandela]], [[Václav Havel]], [[Aung San Suu Kyi]], [[Peter Gabriel]]  [[U2]]. [[Malala Yousafzai]], [[Ai Wei Wei]] and [[Joan Baez]] <ref>{{cite web|url=https://www.amnesty.org/en/latest/campaigns/2016/05/ambassador-of-conscience-award/|title=The Ambassador of Conscience Award|publisher=}}</ref>

==References==
{{reflist|30em}}

{{DEFAULTSORT:Amnesty-Sís-Pinton Tapestries}}
[[Category:Tapestries]]
[[Category:Amnesty International]]