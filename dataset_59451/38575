{{Infobox Politician (general)
| name= John Robert Boyle
| image=John Robert Boyle - ca. 1912-1918.jpg
| imagesize=
| title=[[List of Alberta official opposition leaders|Leader of the Official Opposition in Alberta]]
| term_start=1922
| term_end=1924
| predecessor=[[Albert Ewing]]
| successor=[[Charles R. Mitchell]]
| title1=Leader of the [[Alberta Liberal Party]]
| term_start1=1922
| term_end1=1924
| predecessor1=[[Charles Stewart (Canadian politician)|Charles Stewart]]
| successor1=[[Charles R. Mitchell]]
| title2=Alberta Attorney General
| term_start2=August 23, 1918
| term_end2=July 18, 1921
| predecessor2=[[Charles Wilson Cross]]
| successor2=[[John Edward Brownlee]]
| title3=Alberta Minister of Education
| term_start3=May 4, 1912
| term_end3=August 26, 1918
| predecessor3=[[Charles R. Mitchell]]
| successor3=[[George P. Smith]]
| title4=[[Legislative Assembly of Alberta|Member of the Legislative Assembly of Alberta]]
| constituency4 = [[Edmonton (provincial electoral district)|Edmonton]]
| term_start4=July 18, 1921
| term_end4=August 27, 1924
| constituency5=[[Sturgeon (provincial electoral district)|Sturgeon]]
| term_start5=November 9, 1905
| term_end5=July 18, 1921
| predecessor5='''New District'''
| successor5=[[Samuel Allen Carson|Samuel Carson]]
| title6=  Alderman on the [[Edmonton City Council]]
| term_start6= December 12, 1904
| term_end6= May 7, 1906
| birth_date= February 1 or 3, 1870 or 1871
| birth_place= [[Sykeston, Ontario|Sykeston]], [[Ontario]]
| death_date= February 15, 1936<br>(aged 65–66)
| death_place= [[Ottawa]], [[Ontario]]
| party= [[Alberta Liberal Party]]
| spouse= Dora Shaw (2 children)
| profession= Lawyer
| religion=[[Presbyterian]]
}}

'''John Robert Boyle''', [[King's Counsel|KC]] (February 1, 1870 or February 3, 1871 – February 15, 1936) was a Canadian politician and jurist who served as a Member of the [[Legislative Assembly of Alberta]], a cabinet minister in the [[Government of Alberta]], and a judge on the [[Supreme Court of Alberta]].  Born in Ontario, he came west and eventually settled in [[Edmonton]], where he practiced law.  After a brief stint on Edmonton's first [[Edmonton City Council|city council]], he was elected in [[Alberta general election, 1905|Alberta's inaugural provincial election]] as a [[Alberta Liberal Party|Liberal]].  During the [[Alberta and Great Waterways Railway scandal]], he was a leader of the Liberal insurgency that forced [[Premier of Alberta|Premier]] [[Alexander Cameron Rutherford]] from office.

Though initially left out of cabinet by [[Arthur Sifton]], Rutherford's successor, Boyle was named Minister of Education in 1912.  He served in this capacity until 1918, during which time he alienated many non-English speakers by insisting on a unilingual English school system.  In 1918 he was made Attorney-General.  He retained his seat in the legislature after the Liberal defeat in the [[Alberta general election, 1921|1921 election]] and briefly served as leader of the Liberal opposition, but was appointed to the bench in 1924.  He was still a judge when he died in 1936.

==Early life==

Boyle was born in [[Sykeston, Ontario]] on either February 1, 1870 or February 3, 1871,<ref name="EPL">{{cite web |url=http://www.epl.ca/edmonton-history/edmonton-elections/biographies-mayors-and-councillors?id=B |title=Boyle, John Robert |publisher=Edmonton Public Libraries |accessdate=2010-07-09}}</ref> of [[Scottish Canadian|Scottish]] and [[Irish Canadian|Irish]] descent.<ref name="REW">{{cite web |url=http://www.rewedmonton.ca/content_view2?CONTENT_ID=159 |first=Lawrence |last=Herzog |date=August 1, 2002 |work=Real Estate Weekly |title=Lambton Block |accessdate=2010-07-09}}</ref>  His father died in 1884, and Boyle had to leave school to support his family; he eventually completed high school at Sarnia Collegiate Institute in 1888 and 1889.<ref name="REW"/>  Following graduation, he taught school for three years in [[Lambton County, Ontario|Lambton County]].<ref name="EPL"/>  In 1894, he came west, though accounts vary as to exactly where he settled and for what purpose: he either studied law in [[Regina, Saskatchewan|Regina]],<ref name="REW"/> taught school in [[Pilot Butte, Saskatchewan|Pilot Butte]],<ref name="Glenbow">{{cite web |publisher=Glenbow Archives |title=John R. Boyle, Lawyer (image NA-4264-1) |url=http://ww2.glenbow.org/search/archivesPhotosSearch.aspx |accessdate=2010-07-09}}</ref> or settled in [[Edmonton]].<ref name="EPL"/>

[[File:John Robert Boyle and Family.jpg|thumb|left|Boyle and his family]]Sources agree that he was in the Edmonton area by 1896, and that he taught school there before being called to the bar in 1899.<ref name="EPL"/><ref name="REW"/><ref name="Glenbow"/>  In either 1892<ref name="EPL"/> or 1902<ref name="REW"/> he married Dora Shaw, with whom he had three<ref name="REW"/> children (Helen, Frederick and Jean).<ref>{{cite book|last=Boyle and District Historical Society|title=Forests, furrows and faith : a history of Boyle and districts|year=1982|location=Boyle|page=13|url=http://www.ourfutureourpast.ca/loc_hist/page.aspx?id=3730863}}</ref>   He partnered with [[Hedley C. Taylor]] to form Taylor & Boyle, which was later known as Boyle, Parlee, Freeman, Abbott & Mustard;<ref name="EPL"/><ref name="REW"/> the firm was a forerunner of the present day Parlee McLaws.<ref>{{cite news |url=http://www.canadianlawyermag.com/Best-in-the-West.html |title=Best in the West |work=Canadian Lawyer |first=Robert |last=Todd |date=April 2010 |accessdate=2010-07-17}}</ref> Boyle was made [[King's Counsel]] in 1913.<ref name="EPL"/>

He ran in the [[Edmonton municipal election, 1904|1904 Edmonton municipal election]] to elect the first [[Edmonton City Council]] (Edmonton had hitherto been a town).  He finished second of seventeen candidates in the aldermanic race, and was elected to a two-year term.{{#tag:ref|At the time, the city council included eight aldermen on staggered two-year terms.  In the 1904 election, the top four finishers were elected to two-year terms, and the next four to one-year terms.|group="Note"}}  He resigned in 1906, before the completion of his term.<ref name="1904 results"/>

==Provincial politics==

===Early provincial career===

In 1905, Boyle ran in [[Alberta general election, 1905|Alberta's inaugural provincial election]] as the [[Alberta Liberal Party|Liberal]] candidate in [[Sturgeon (provincial electoral district)|Sturgeon]], where he defeated [[Progressive Conservative Association of Alberta|Conservative]] Frank Knight by a wide margin.<ref name="1905 results"/>  Boyle served as Deputy [[Speaker (politics)|Speaker]] in the [[1st Alberta Legislative Assembly]].<ref name="EPL"/> (To focus on government affairs, Boyle resigned as Edmonton alderman on May 7, 1906 and a by-election was held to fill his empty seat.<ref>Edmonton Bulletin, May 26, 1906</ref>)

During his first term, Boyle supported the selection of Edmonton (over rival [[Calgary]]) as the new province's capital,<ref>Thomas 38</ref> and supported the Liberal government of [[Alexander Cameron Rutherford]] in its decision to borrow money to finance the creation of [[Alberta Government Telephones]] (abandoning its usual "[[PAYGO|pay as you go]]" approach).  Boyle predicted that "Alberta, the first to undertake [a provincial government telephone system] will become a model for every province in the Dominion."<ref>Thomas 53</ref>  He also sided with the government in its rejection of Conservative demands that it build and operate railways, as he felt that doing so would not be viable as long as the trunk lines were in private hands.<ref>Thomas 60–61</ref>  He enthusiastically backed private construction of railways, however, and greeted the announcement of the Alberta and Great Waterways Railway—which was to run northward from Edmonton to [[Lac la Biche (Alberta)|Lac la Biche]] and later [[Fort McMurray]]—with what historian L. G. Thomas describes as "an extravagant eulogy...[speaking] of Lac la Biche as another [[Lake Louise (Alberta)|Lake Louise]], of [[Pullman (car or coach)|Pullmans]] running from New Orleans to the Arctic circle, and of northern Alberta as a second [[Cobalt, Ontario|Cobalt]] region."<ref>Thomas 61</ref>

===Alberta and Great Waterways Railway scandal===
{{main|Alberta and Great Waterways Railway scandal}}

The Rutherford government was comfortably re-elected in the [[Alberta general election, 1909|1909 election]];<ref>Thomas 69</ref> Boyle himself was acclaimed in Sturgeon.<ref name="1909 results"/>  Shortly after the elections, rumours began to spread that all was not well with the Alberta and Great Waterways Railway (A&GWR), to which the government had given [[loan guarantee]]s and on whose behalf it had sold [[bond (finance)|bonds]] in the [[London]] bond market.  When the new legislature convened in February 1910, Boyle [[Table (parliamentary procedure)|tabled]] a list of eleven questions for the government about the A&GWR.  Rutherford, Minister of Railways as well as Premier, duly answered them in writing.<ref>Thomas 70–71</ref>  Boyle found these answers unsatisfactory, and on February 21 gave notice of a motion to [[Nationalization|expropriate]] the A&GWR's bond money; he held that the government had raised more money for the A&GWR than was needed for construction.  He also alleged that S. B. Woods, deputy to Attorney-General [[Charles Wilson Cross]], had removed key components from the government's files on the A&GWR, in advance of their having been inspected by Boyle and Conservative leader [[R. B. Bennett]].<ref name="Thomas 72">Thomas 72</ref>

Boyle's resolution rapidly divided the Liberal members between insurgents, led by Boyle and [[William Henry Cushing]] (who resigned his position as Minister of Public Works over the A&GWR issue), and loyalists, led by Rutherford and his remaining cabinet ministers, especially Cross.<ref name="Thomas 72"/>  In the ensuing debate, several charges were levelled against Boyle himself: Agriculture Minister [[Duncan Marshall]] accused him of being motivated by his rejection for the position of A&GWR solicitor.  Boyle admitted applying for the position, but denied that it had anything to do with his attacks on the government.<ref>Thomas 79–80</ref>  The ''[[Edmonton Bulletin]]'' accused him of approaching two Liberal members who were also hotel keepers, [[Lucien Boudreau]] and [[Robert L. Shaw]], and offering them immunity from prosecution for liquor offenses if they helped bring down Rutherford's government and replace it with one, led by Cushing, in which Boyle would be Attorney-General.<ref name="Thomas 84">Thomas 84</ref>

Though Rutherford survived a [[motion of non-confidence]] (moved by [[Ezra Riley]] and seconded by Boyle) by three votes,<ref name="Thomas 84"/> he was successfully pressured to resign by [[Lieutenant-Governor of Alberta]] [[George Bulyea]].  It had been expected that Cushing would replace Rutherford if the latter was defeated, but Bulyea and other prominent Liberals did not have confidence in him, and instead selected [[Arthur Sifton]], Alberta's Chief Justice.<ref>Thomas 89</ref>

===Minister of the Crown===

Sifton left all major figures of the A&GWR affair, including Boyle, out of his first cabinet, and instead appointed fellow judge [[Charles R. Mitchell]] Attorney-General.<ref>Thomas 90–91</ref>  However, in 1912 he decided that enough time had passed for old wounds to heal, and re-appointed Cross as Attorney-General.  At the same time, he brought Boyle into his cabinet as Minister of Education.<ref>Thomas 125</ref>  The law required that members newly admitted to cabinet resign their seats in the legislature and immediately contest a by-election; Boyle was re-elected in Sturgeon by a safe margin.<ref>Thomas 127</ref>

Boyle's time as Education Minister was tumultuous: many teachers enlisted to fight in [[World War I]], and many others left the profession for more lucrative opportunities elsewhere.  In its members' handbook, the [[Alberta Teachers' Association]] describes Boyle's efforts to remedy this situation as "heroic", citing in particular his convincing the legislature to set a minimum teachers' salary of $840 per year.<ref>{{cite news |url=http://www.teachers.ab.ca/Publications/ATA%20Magazine/Volume%2086/Number%202/Articles/Pages/The%20Early%20History%20of%20the%20Teachers%20Association.aspx |title=The Early History of the Teachers' Association |publisher=[[Alberta Teachers' Association]] |accessdate=2010-07-17 |date=Winter 2005}}</ref>  Another of Boyle's tactics to alleviate the teacher shortage was to make it easier for teachers qualified in [[Quebec]] to teach in Alberta.<ref name="Mahe">Mahe</ref>  However, this liberalization was subject to applicants' English proficiency: Boyle insisted that all instruction in Alberta schools be delivered in English.<ref name="Connors">Aunger (2005) 116–117</ref>  A Québécois teacher who passed an English language proficiency exam would be granted a temporary teaching license, which could be upgraded to a full Alberta Teaching Certificate with five months' study at a [[normal school]].<ref name="Mahe"/>  Boyle's insistence that Alberta was English offended not only the province's [[French Canadian]] minority, but also its [[Ukrainian Canadian|Ukrainian]]-speaking population; an editorial in a Ukrainian newspaper maintained angrily that "the minister of education lies when he says that Alberta is an English province.  Alberta is a Canadian province, where everyone has equal rights, including the Ukrainians."<ref>Hoerder 286</ref>  During a by-election in [[Whitford (provincial electoral district)|Whitford]] Boyle accused the Conservatives of promising Ukrainian language schools to court the immigrant vote.<ref>Aunger (2004) 474</ref>

In 1918, new premier [[Charles Stewart (Canadian politician)|Charles Stewart]], who had succeeded Sifton when the latter entered federal politics in 1917, fired Cross and appointed Boyle Attorney-General.<ref>Thomas 185</ref>  The following year Boyle introduced legislation formally making English Alberta's only official language.  At the time, he boasted that in the past election "my majority came from English electors" in contrast to a Conservative who supported "Russian schools for Russian people".<ref>Aunger (2004) 479–480</ref>  As Attorney-General, he also supported unsuccessful legislation to allow [[Imperial Oil]] to construct a [[pipeline transport|pipeline]] in Alberta; in response to bipartisan opposition calling for pipelines to be common carriers, he said that to adopt such a course would be to tell oil companies that they "were free to spend vast sums in exploration work but if oil were found, they were not to pipe it out."<ref>Breen 36</ref>

One of Boyle's chief responsibilities as Attorney-General was to enforce Alberta's recently enacted [[prohibition in Canada|prohibition]].  This proved difficult, as the law was widely disparaged—not least by judges, who reputedly presided over liquor trials while hungover.  In 1921, Boyle estimated that bootleggers were profiting from prohibition to the tune of [[C$]]7 million.  He was denounced by supporters of prohibition for his ineffectiveness at enforcing it, and by its opponents for "arrogating to himself the powers of a czar."<ref>Thomas 192–193</ref>

In the [[Alberta general election, 1921|1921 election]], Boyle both sought re-election in Sturgeon and election in the new multi-member constituency of [[Edmonton (provincial electoral district)|Edmonton]].  He was defeated in the former but victorious in the latter, making him one of two members from the [[1st Alberta Legislative Assembly]] to be elected to the [[5th Alberta Legislative Assembly|5th]]; the other was Cross, Boyle's predecessor as Attorney-General and rival from the Alberta and Great Waterways Affair.{{#tag:ref|Boyle's 1924 resignation would leave Cross as the sole survivor for slightly over a year, until he too resigned.|group="Note"}}  Provincially, the Liberals were soundly defeated by the [[United Farmers of Alberta]] (UFA), which, contesting their first election, won 39 seats to the Liberals' 14.<ref>Thomas 204</ref>

===Leader of the Alberta Liberals===

Late in Stewart's term as premier, there had been speculation that he would resign due to ill-health, and Boyle was among the candidates mentioned as possible successors.<ref>Thomas 194</ref>  When Stewart did resign, immediately following the 1921 election, Boyle was selected to replace him.  In the assessment of [[Lakeland College (Alberta)|Lakeland College]] historian Franklin Foster, Boyle "showed vigour" in the legislature, where he presented a strong opposition to the new UFA government of [[Herbert Greenfield]].<ref name="Foster 74">Foster 74</ref>  Even so, he showed some private courtesy: when [[John Edward Brownlee]], Greenfield's Attorney-General and his strongman in the legislature, missed a session due to illness, Boyle assured him that the Liberals would not attack the government too vigorously in his absence.<ref>Foster 107</ref>

[[File:Jrboyle1935.jpg|thumb|right|Boyle in 1935 with his daughter, Helen, and grandson, Ian]]
As leader of the Alberta Liberals, Boyle corresponded extensively with [[Liberal Party of Canada]] leader (and [[Prime Minister of Canada]]) [[William Lyon Mackenzie King]]; according to Foster, Boyle's letters to King were "a mixture of useless information and pleas to be rescued by an appointment to the bench."<ref name="Foster 74"/>  It is possible that one of his letters had some impact on history, however: in 1924, while Greenfield was attempting to negotiate control of Alberta's natural resources from King's federal government, Boyle sent King a letter warning him that the UFA was doomed in the next election unless "something extraordinary happens. That extraordinary thing which Greenfield wants to happen now is obtaining from you the natural resources at once."<ref name="Foster 114">Foster 114</ref>  King drew out negotiations until Greenfield returned to Alberta empty-handed; soon after, Greenfield was forced from office by his own backbenchers, and replaced by Brownlee.<ref>Foster 116</ref>

==Judicial career and later life==

In 1924, Boyle was appointed to the [[Supreme Court of Alberta]], and resigned from the legislature.<ref name="REW"/><ref name="Glenbow"/>  He was succeeded as Liberal leader by another former Attorney-General, [[Charles R. Mitchell]].<ref name="Foster 114"/>  As judge, Boyle once refused to issue an [[injunction]] to end a coal miners' [[strike action|strike]] that had turned violent, because he believed that the [[Alberta Provincial Police]] could contain the violence if properly instructed (another judge later issued the injunction).<ref>Fudge, Tucker 85</ref>  It was also before Boyle that the statement of claim was filed in ''[[MacMillan v. Brownlee]]'', the case that forced Brownlee to resign as premier.<ref>Foster 222</ref>{{#tag:ref|Boyle did not preside over the case itself, which was tried by [[William Carlos Ives]].|group="Note"}}

Boyle was a member of the [[Presbyterian Church]], the [[Freemasonry|Masonic Order]], and the [[Independent Order of Odd Fellows]].  He was still sitting as a judge when he died February 15, 1936, on his way to Jamaica.<ref name="EPL"/> The Edmonton neighbourhood of [[Boyle Street (Edmonton)|Boyle Street]] and the village of [[Boyle, Alberta|Boyle]] are named in his honour; perhaps ironically, the latter lies on what was once the Alberta and Great Waterways Railway line.<ref name="REW"/>

==Electoral record==

{{Alberta provincial election, 1921/Edmonton}}
{{Alberta provincial election, 1921/Sturgeon}}
{| class="wikitable"
|-
| colspan="3" align=center|'''[[Alberta general election, 1917|1917 Alberta general election]] results ([[Sturgeon (provincial electoral district)|Sturgeon]])'''<ref>{{cite web |url=http://www.abheritage.ca/abpolitics/administration/maps_choice.php?Year=1917&Constit=Sturgeon |title=Election results for Sturgeon, 1917 |publisher=Alberta Online Encyclopedia |accessdate=2010-07-06}}</ref>
| colspan="2"|<span style="font-size: 90%;">'''Turnout 93.5%'''</span>
|-
{{Canadian_politics/party colours/Liberal/row}}
|[[Alberta Liberal Party|Liberal]]
|John Robert Boyle
|align="right"|1,546
|align="right"|47.19%
{{Canadian_politics/party_colours/Progressive Conservatives/row}}
|[[Progressive Conservative Association of Alberta|Conservative]]
|J. Sutherland
|align="right"|1,212
|align="right"|37.00%
{{Canadian_politics/party colours/Independents/row}}
|[[Independent (politician)|Independent]]
|H. Mickleson
|align="right"|518
|align="right"|15.81%
|-
| colspan="3" align=center|'''[[Alberta general election, 1913|1913 Alberta general election]] results ([[Sturgeon (provincial electoral district)|Sturgeon]])'''<ref>{{cite web |url=http://www.abheritage.ca/abpolitics/administration/maps_choice.php?Year=1913&Constit=Sturgeon |title=Election results for Sturgeon, 1913 |publisher=Alberta Online Encyclopedia |accessdate=2010-07-06}}</ref>
| colspan="2"|<span style="font-size: 90%;">'''Turnout 69.9%'''</span>
|-
{{Canadian_politics/party colours/Liberal/row}}
|[[Alberta Liberal Party|Liberal]]
|John Robert Boyle
|align="right"|936
|align="right"|62.73%
{{Canadian_politics/party_colours/Progressive Conservatives/row}}
|[[Progressive Conservative Association of Alberta|Conservative]]
|[[James Duncan Hyndman]]
|align="right"|556
|align="right"|37.27%
|-
| colspan="3" align=center|'''1912 by-election results ([[Sturgeon (provincial electoral district)|Sturgeon]])'''<ref name="Results 1912">{{cite web |url=http://www.elections.ab.ca/Public%20Website/742.htm#1905-1973 |title=Past by-election results |publisher=Elections Alberta |accessdate=2010-07-06}}</ref>
| colspan="2"|<span style="font-size: 90%;">'''Turnout N.A.'''</span>
|-
{{Canadian_politics/party colours/Liberal/row}}
|[[Alberta Liberal Party|Liberal]]
|John Robert Boyle
|align="right"|1,173
|align="right"|66.08%
{{Canadian_politics/party_colours/Progressive Conservatives/row}}
|[[Progressive Conservative Association of Alberta|Conservative]]
|A. W. Taylor
|align="right"|602
|align="right"|33.92%
|-
| colspan="3" align=center|'''[[Alberta general election, 1909|1909 Alberta general election]] results ([[Sturgeon (provincial electoral district)|Sturgeon]])'''<ref name="1909 results">{{cite web |url=http://www.abheritage.ca/abpolitics/administration/maps_choice.php?Year=1909&Constit=Sturgeon |title=Election results for Sturgeon, 1909 |publisher=Alberta Online Encyclopedia |accessdate=2010-07-06}}</ref>
| colspan="2"|<span style="font-size: 90%;">'''Turnout N/A'''</span>
|-
{{Canadian_politics/party colours/Liberal/row}}
|[[Alberta Liberal Party|Liberal]]
|John Robert Boyle
|colspan="2" align="right"|Acclaimed
|-
| colspan="3" align=center|'''[[Alberta general election, 1905|1905 Alberta general election]] results ([[Sturgeon (provincial electoral district)|Sturgeon]])'''<ref name="1905 results">{{cite web |url=http://www.abheritage.ca/abpolitics/administration/maps_choice.php?Year=1905&Constit=Sturgeon |title=Election results for Sturgeon, 1905 |publisher=Alberta Online Encyclopedia |accessdate=2010-07-06}}</ref>
| colspan="2"|<span style="font-size: 90%;">'''Turnout N.A.'''</span>
|-
{{Canadian_politics/party colours/Liberal/row}}
|[[Alberta Liberal Party|Liberal]]
|John Robert Boyle
|align="right"|721
|align="right"|76.78%
{{Canadian_politics/party_colours/Progressive Conservatives/row}}
|[[Progressive Conservative Association of Alberta|Conservative]]
|Frank Knight
|align="right"|218
|align="right"|23.22%
|-
| colspan="3" align=center|'''[[Edmonton municipal election, 1904|1904 Edmonton municipal election]] results (aldermanic candidates)'''<ref name="1904 results">{{cite web |url=http://www.edmonton.ca/city_government/documents/Election_Results_1892_-_1944.doc |title=Election Results 1892–1944 |publisher=City of Edmonton |accessdate=2010-07-06}}</ref> (eight elected)
| colspan="2"|<span style="font-size: 90%;">'''Turnout N.A.'''</span>
|-
|colspan="2"|
|[[Charles May (Alberta politician)|Charles May]]
|align="right"|471
|align="right"|63.39%
|-
|colspan="2"|
|John Robert Boyle
|align="right"|349
|align="right"|46.97%
|-
|colspan="2"|
|[[Kenneth McLeod]]
|align="right"|330
|align="right"|44.41%
|-
|colspan="2"|
|[[Thomas Bellamy]]
|align="right"|310
|align="right"|41.72%
|-
|colspan="2"|
|[[William Harold Clark|William Clark]]
|align="right"|277
|align="right"|37.28%
|-
|colspan="2"|
|[[Joseph Henri Picard]]
|align="right"|262
|align="right"|35.26%
|-
|colspan="2"|
|[[Daniel R. Fraser|Daniel Fraser]]
|align="right"|257
|align="right"|34.59%
|-
|colspan="2"|
|[[William Antrobus Griesbach]]
|align="right"|239
|align="right"|32.17%
|-
|colspan="2"|
|[[Thomas Grindley]]
|align="right"|231
|align="right"|31.09%
|-
|colspan="2"|
|Gustave Koerman
|align="right"|224
|align="right"|30.15%
|-
|colspan="2"|
|Peter Butchart
|align="right"|204
|align="right"|27.46%
|-
|colspan="2"|
|Donald MacDonald
|align="right"|171
|align="right"|23.01%
|-
|colspan="2"|
|William Deyl
|align="right"|170
|align="right"|22.88%
|-
|colspan="2"|
|[[Herbert Charles Wilson]]
|align="right"|162
|align="right"|21.80%
|-
|colspan="2"|
|Frank Haldane
|align="right"|161
|align="right"|21.67%
|-
|colspan="2"|
|Samuel Paton
|align="right"|143
|align="right"|19.25%
|-
|colspan="2"|
|W. S. Weeks
|align="right"|80
|align="right"|10.77%
|}

==Notes==
{{Reflist|group=Note}}

==References==
{{reflist}}

==Works cited==
*{{cite journal |first=Edmund A. |last=Aunger |title=Legislating Language Use in Alberta: A Century of Incidental Provisions for a Fundamental Matter |journal=Alberta Law Review |volume=42 |number=2 |year=2004}}
*{{cite book |first=Edmund A. |last=Aunger |chapter=One Language and One Nationality |title=Forging Alberta's Constitutional Framework |editor1-first=Richard |editor1-last=Connors |editor2-first=John M. |editor2-last=Law |publisher=[[University of Alberta]] Centre for Constitutional Studies |year=2005 |location=Edmonton, Alberta |isbn=978-0-88864-458-9}}
*{{cite book |first=David |last=Breen |title=Alberta's petroleum industry and the Conservation Board |publisher=[[University of Alberta Press]] |year=1993 |location=Edmonton, Alberta |isbn=978-0-88864-245-5}}
*{{cite book |last=Foster |first=Franklin L. |title=John E. Brownlee: A Biography |year=1981 |publisher=Foster Learning Inc |location=[[Lloydminster, Alberta]] |isbn=978-1-55220-004-9}}
*{{cite book |first1=Judy |last1=Fudge |first2=Eric |last2=Tucker |title=Labour Before the Law: The Regulation of Workers' Collective Action in Canada, 1900-1948 |publisher=[[University of Toronto Press]] |location=[[Toronto]], [[Ontario]] |year=2004 |isbn=978-0-8020-3793-0}}
*{{cite book |first=Dirk |last=Hoerder |title=Creating societies: immigrant lives in Canada |year=1999 |publisher=McGill-Queens Press |isbn=978-0-7735-1882-7 |location=[[Montreal]], [[Quebec]]}}
*{{cite journal |url=http://library.queensu.ca/ojs/index.php/edu_hse-rhe/article/viewArticle/1698/1797 |title=French Teacher Shortages and Cultural Continuity in Alberta Districts, 1892-1940 |first=Yvette T. M. |last=Mahe |date=Fall 2002 |journal=Historical Studies in Education}}
*{{cite book |first=Lewis Gwynne |last=Thomas |title=The Liberal Party in Alberta |year=1959 |publisher=University of Toronto Press |location=Toronto, Ontario}}

==External links==
{{Commons category|John Robert Boyle}}

{{ABLOpp}}

{{Alberta Liberal Party}}

{{Good article}}

{{DEFAULTSORT:Boyle, John Robert}}
[[Category:1936 deaths]]
[[Category:Alberta Liberal Party MLAs]]
[[Category:Canadian Presbyterians]]
[[Category:Canadian schoolteachers]]
[[Category:Edmonton city councillors]]
[[Category:Judges in Alberta]]
[[Category:Lawyers in Alberta]]
[[Category:Leaders of the Alberta Liberal Party]]
[[Category:People from Lambton County]]
[[Category:Canadian people of Scottish descent]]
[[Category:Canadian people of Irish descent]]
[[Category:Year of birth uncertain]]
[[Category:1870 births]]
[[Category:Members of the Executive Council of Alberta]]
[[Category:Canadian Queen's Counsel]]