{{Good article}}
{{Infobox Hurricane
| Name=Subtropical Storm One
| Type=Subtropical storm
| Year=1982
| Basin=Atl
| Image location=STS 1 1982 Jun 18 1942Z.jpg
| Image name=Subtropical Storm One traveling up the Eastern Seaboard on June 18
| Formed=June 18, 1982
| Dissipated=June 22, 1982
| Extratropical=June 20, 1982
| 1-min winds=60
| Pressure=984
| Damages=10
| Inflated=0
| Fatalities=3 direct
| Areas=[[Cuba]], [[Florida]], [[Georgia (U.S. state)|Georgia]], [[Virginia]], [[North Carolina]], [[South Carolina]] and [[Atlantic Canada]]
| Hurricane season=[[1982 Atlantic hurricane season]]
}}
The '''1982 Florida subtropical storm''', officially known as '''Subtropical Storm One''', was the only [[subtropical cyclone]] of the inactive [[1982 Atlantic hurricane season]]. The storm originated from two different systems around the [[Gulf of Mexico]] and [[Caribbean Sea]] on June 16. The systems merged into trough while a circulation began to form off the coast of [[Florida]] on June 18. The depression made landfall in [[Florida]] and strengthened into a storm over land. The storm entered the [[Atlantic Ocean]] and headed to the northeast, becoming extratropical on June 20 near [[Newfoundland and Labrador|Newfoundland]]. The storm was the only system of 1982 to affect the eastern half of the [[United States]], and it caused three fatalities and caused $10&nbsp;million in damage (1982&nbsp;[[United States Dollar|USD]], $21&nbsp;million 2007&nbsp;USD).

==Meteorological history==
{{storm path|1982 Atlantic subtropical storm 1 track.png}}
The subtropical storm had an unusual origin by forming from an interaction of two different systems near the [[Yucatán Peninsula]]. This occurrence is unusual, but not unique, as Subtropical Storm One in the [[1974 Atlantic hurricane season|1974 season]] formed in similar pattern.<ref name="1974-STS1-TCR">{{
cite web
| author=Paul Herbert
| year=1974
| title=Subtropical Storm One (1974) Preliminary Report
| publisher=National Hurricane Center
| accessdate=2007-03-28
| url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1974-prelim/subtrop/prelim01.gif
}}</ref> A [[Reconnaissance aircraft|reconnaissance]] flight on June 17 reported that there appeared to be multiple transient circulations at the surface, but no well-defined center.<ref name="SS1Pg1">{{
cite web
| author=Joseph M. Pelissier
| date=1982-07-15
| title=Subtropical Storm One (1982) Preliminary Report - Section I - Storm History I
| publisher=National Hurricane Center
| accessdate=2007-04-05
| url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1982-prelim/subtrop/prelim01.gif
}}</ref> The first disturbance can be traced back to June 15 in the northwest [[Caribbean Sea]]. The disturbance moved north, along with a low-pressure area that formed over the [[Yucatán Peninsula]]. A [[Trough (meteorology)|trough]] merged with the low-pressure area creating an even stronger trough over the eastern [[Gulf of Mexico]]. Conditions in the [[Gulf of Mexico]] were favorable for tropical storm formation. Some severe weather had been occurring over the [[Florida|Florida Peninsula]] as early as June 16, as the fringes of disturbance moved across the Florida Straits and over the Peninsula.<ref name="SS1Pg1"/>

A circulation began to form on June 18 in the eastern [[Gulf of Mexico]], prompting gale warnings.<ref name="SS1Pg2">{{
cite web
| author=Joseph M. Pelissier
| date=1982-07-15
| title=Subtropical Storm One (1982) Preliminary Report - Section 2 - Storm History II + Impact
| publisher=National Hurricane Center
| accessdate=2007-04-05
| url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1982-prelim/subtrop/prelim02.gif
}}</ref> It was declared Subtropical Depression One the same day. The depression made landfall on the morning of June 18 just north of [[Spring Hill, Florida]] and gained subtropical storm status over land. The storm was classified as Subtropical Storm One by the [[National Hurricane Center]], as the rule was not to name subtropical storms. The subtropical storm moved to the northeast and crossed the [[Outer Banks]] of [[North Carolina]] on June 19. Even though the minimum central pressures remained low, the circulation expanded and became distorted. The subtropical storm peaked at 70&nbsp;mph winds (110&nbsp;km/h) on June 18, but did not reach its minimum pressure until June 20, when it dropped to 984&nbsp;[[bar (unit)|mbar]] (29.07&nbsp;[[Inch of mercury|inHg]]).<ref name="SS1Pg7">{{
cite web
| author=Joseph M. Pelissier
| date=1982-07-15
| title=Subtropical Storm One (1982) Preliminary Report - Section 7 - Meteorological stats chart
| publisher=National Hurricane Center
| accessdate=2007-12-08
| url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1982-prelim/subtrop/prelim07.gif
}}</ref> The subtropical storm was declared an extratropical storm near the [[Canadian Maritimes]] the same day.<ref name="CHC82">{{
cite web
| author=[[Canadian Hurricane Centre]]
| date=1982-07-15
| title=Canadian Hurricane Center Report
| publisher=Canadian Hurricane Center
| accessdate=2007-03-28
| url=http://www.atl.ec.gc.ca/weather/hurricane/storm82.html
}}</ref><ref name="SS1Pg3">{{
cite web
| author=Joseph M. Pelissier
| date=1982-07-15
| title=Subtropical Storm One (1982) Preliminary Report - Section 3 - Storm History + Impact II
| publisher=National Hurricane Center
| accessdate=2007-12-16
| url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1982-prelim/subtrop/prelim03.gif
}}</ref>

==Preparations==
Several warnings were issued in association with the subtropical storm.<ref name="SS1Pg4">{{
cite web
| author=Joseph M. Pelissier
| date=1982-07-15
| title=Subtropical Storm One (1982) Preliminary Report - Section 4 - Warnings/Watches
| publisher=National Hurricane Center
| accessdate=2007-04-05
| url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1982-prelim/subtrop/prelim04.gif
}}</ref>  On June 16, severe thunderstorm watches and warnings were issued for [[Florida]]. The next day, more warnings were issued, including tornado, small craft, and special marine warnings. There were also various types of special weather statements issued for heavy rains. Five gale warnings were issued on June 18 and June 19 for the cities on the western coast of Florida. All warnings were discontinued by 12&nbsp;p.m. EDT (1600&nbsp;UTC) June 19. The same day, a gale warning was issued from [[Cape Henlopen|Cape Henlopen, Delaware]] to [[Watch Hill, Rhode Island]].<ref name="SS1Pg4"/>

The subtropical storm postponed the bi-annual [[Newport, Rhode Island]] to [[Bermuda]] sailboat race for two consecutive days due to the forecast one of the storm. Subsequent reports from sailboats said that with the track of the storm, if not for the delay, there would have been a marine disaster.<ref name="SS1Pg4"/>

==Impact==
In its main inflow band over Cuba, rainfall amounts up to 28.66&nbsp;inches (728&nbsp;mm) were recorded.<ref name="CUBARAIN">{{cite web|author=Instituto Nacional de Recursos Hidráulicos|year=2003|title=Lluvias intensas observadas y grandes inundaciones reportadas|language=Spanish|accessdate=2007-02-10|url=http://www.hidro.cu/hidrologia1.htm| archiveurl= https://web.archive.org/web/20070312215337/http://www.hidro.cu/hidrologia1.htm| archivedate= 12 March 2007 <!--DASHBot-->| deadurl= yes}}</ref>

[[Image:Subtropical Storm One 1982 rainfall.gif|right|thumb|179px|Rainfall Totals for Subtropical Storm One]]
Most of the storm's effects were felt in Florida where it made landfall on the morning of June 18, causing gales, heavy rains, flooding, beach erosion and tornadoes.<ref name="erosion">{{cite web|author=Florida Department of Environmental Protection|year=2003|title=Strategic Beach Management Plan|accessdate=2007-12-17|url=http://www.dep.state.fl.us/beaches/publications/pdf/Southwest%20Gulf%20Coast%20Region.pdf}}</ref> The highest winds were recorded at 41&nbsp;mph (66&nbsp;km/h) and up to 48&nbsp;mph (77&nbsp;km/h) during a thunderstorm at [[Macdill Air Force Base]] in [[Tampa, Florida|Tampa]] on June 18. Rainfall peaked at 10.72&nbsp;inches (272.28&nbsp;mm) in [[DeSoto County, Florida|Desoto City]], while much of the rest of the state reported between 1&ndash;5&nbsp;inches (25&ndash;125&nbsp;inches) of precipitation.<ref name="MWR">{{
cite web
| author=Gilbert B. Clark
| year=1982
| title=1982 Monthly Weather Review
| publisher=American Meteorological Society
| accessdate=2007-03-28
| url=http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1982.pdf
}}</ref><ref name="rainfall">{{
cite web
| author=David Roth
| year=2006
| title=Rainfall Totals: Subtropical Storm One (1982)
| publisher=Hydrometeorological Prediction Center
| accessdate=2007-03-28
| url=http://www.wpc.ncep.noaa.gov/tropical/rain/stone1982.html
| authorlink=David M. Roth
}}</ref> High tides and waves caused flooding and beach erosion from [[Naples, Florida|Naples]] to the Tampa Bay area. Some waterfront buildings suffered damage from undermining including widespread damage of marinas and some boats. Several bulkheads and rock [[revetment]]s experienced minor damage in Naples and [[Marco Island, Florida|Marco Island]]. Several man‑made dune structures seaward of the Coastal Construction Control Line in [[Collier County, Florida|Collier County]] received minor damage at [[Vanderbilt Beach]] and [[Marco Island]].<ref name="Flood">{{
cite web
| author=Collier County, Florida
| year=2007
| title=Collier County Flood History
| publisher=Collier County, Florida
| accessdate=2007-03-28
| url=http://www.collierem.org/LMS/HistFlood/Hist-flood.htm
}}</ref> Three people were killed in Florida in association with the subtropical storm with a further thirteen people injured. A [[Brevard County, Florida|Brevard County]] woman died when a canoe overturned and an [[Orange County, Florida|Orange County]] child was killed when he was swept into a drainage ditch. 130 families were evacuated a few days after the storm from the Arcadia River area because the [[Peace River (Florida)|Peace River]] crested above flood stage.<ref name="SS1Pg2"/>

The subtropical storm spawned twelve tornadoes across Florida, including at least 2 tornadoes which reached a magnitude of F2 on the [[Fujita Scale]]. The first tornado formed in [[Hendry County, Florida|Hendry County]] on June 18 at 0115&nbsp;UTC reaching F2 strength.<ref name="Tornadoes">{{
cite web
| author=Tom Grazulis of The Tornado Project and Bill McCaul of USRA Huntsville
| year=2007
| title=List of Known Tropical Cyclones Which Have Spawned Tornadoes
| publisher=Tornado Project
| accessdate=2007-03-28
| url=http://www.tornadoproject.com/alltorns/allhurricanes.htm
}}</ref> The third fatality was a man from [[Hendry County, Florida|Hendry County]] who was killed in his mobile home, which was destroyed by a tornado. A total of twenty-five homes in Florida were destroyed. Total damage in Florida totaled out to $10&nbsp;million (1982&nbsp;USD, $21&nbsp;million 2007&nbsp;USD).<ref name="SS1Pg2"/>

The storm's effects north of Florida were minor. Rainfall reached up to nine inches (228&nbsp;mm) in extreme eastern [[South Carolina]] and four inches (101&nbsp;mm) in eastern [[North Carolina]]. Winds reached 50&nbsp;mph (60&nbsp;km/h) with gusts up to 66&nbsp;mph (101&nbsp;km/h) at the Oak Island Coast Guard Station near [[Cape Fear, North Carolina]] and 54&nbsp;mph (87&nbsp;km/h) with gusts of 77&nbsp;mph (123&nbsp;km/h) at the offshore tower at Frying Pan Shoals about forty miles southeast of Cape Fear on June 18 with 70&nbsp;mph winds (110&nbsp;km/h) the next day. A 68&nbsp;foot fishing trawler sunk off the coast of Cape Fear in [[North Carolina]] from the high waves; no one was killed as the sailors were rescued by the [[United States Coast Guard]] on June 19. Flooding was reported with tides of two to three feet in the Carolinas.<ref name="SS1Pg2"/> Rainfall from the subtropical storm reached both states, reaching maximum peaks of up to five inches (127&nbsp;mm) on the [[Georgia (U.S. state)|Georgia]] coast and seven inches on the Georgia/[[South Carolina]] border. Rainfall totaled to about one inch (25&nbsp;mm) on the Virginian coast.<ref name="rainfall"/> A ship encountered winds of a minimum tropical storm with gusts as high as a maximum tropical storm. The ship also encountered a tidal surge of 15 to 20&nbsp;feet.<ref name="SS1Pg2"/><ref name="MWR"/>

== See also ==
{{Portal|Tropical cyclones}}
*[[Subtropical cyclone]]
*[[1982 Atlantic hurricane season]]
*[[List of Atlantic hurricanes]]
* [[Timeline of the 1982 Atlantic hurricane season]]
* [[List of North Carolina hurricanes (1980–present)]]

==References==
{{Reflist}}

==External links==
* [http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1982-prelim/ Detailed information on all storms from 1982]

{{1982 Atlantic hurricane season buttons}}

{{DEFAULTSORT:1 (1982)}}
[[Category:Subtropical storms]]
[[Category:1982 Atlantic hurricane season]]
[[Category:Hurricanes in Florida|1982 Florida]]
[[Category:Hurricanes in North Carolina|1982 Florida]]
[[Category:Hurricanes in South Carolina|1982 Florida]]
[[Category:Hurricanes in Georgia (U.S. state)|1982 Florida]]
[[Category:Hurricanes in Virginia|1982 Florida]]
[[Category:1982 in Florida|Subtropical Storm One]]
[[Category:1982 natural disasters in the United States]]