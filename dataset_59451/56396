The '''''Connecticut Public Interest Law Journal''''' (CPILJ) is a student-run scholarly [[law review]] at the [[University of Connecticut School of Law]]. <ref> [https://www.law.uconn.edu/student-life-resources/law-review-journals/connecticut-public-interest-law-journal UConn Law – CPILJ]</ref> CPILJ's first volume was published in 2001<ref> [http://test.heinonline.org/HOL/TitleSummary?index=journals/cpilj&collection=journals&base=js HeinOnline – Connecticut Public Interest Law Journal]</ref> with the primary purpose of furthering discussion of the legal aspects of [[public interest]], primarily in the state of [[Connecticut]].<ref> [http://cpilj.com/about CPILJ – About Us]</ref>

CPILJ publishes works in print as well as online.<ref> [http://cpilj.com/archive CPILJ – Archive]</ref> Articles are authored by professors, judges, practitioners, and students.

Every fall, CPILJ hosts a [[symposium]] on important issues related to public interest law. During this event, practicing attorneys, professors, judges, and other professionals speak in small panels on a certain subject and take questions from audience members. The 2013 symposium was titled, "The Palliative Use of Marijuana: Demystifying Connecticut's Policy Concerning [[Medical Marijuana]]."<ref> [http://cpilj.com/symposium CPILJ – Symposium]</ref> Past symposia have been themed:  ''"The Future of [[Campaign_finance|Campaign Financing]] in America"'' (2012), ''"The Role of [[Mental Illness]] in Defining Guilt"'' (2011), ''"Undocumented Immigrants in the Workplace: Exploring Rights and Reforms of America's Shadow Labor Force"'' (2010), and ''"The Road to [[Prison_reform|Prison Reform]]: Treating the Causes and Conditions of Our Overburdened System"'' (2009).

== References ==
{{Reflist}}

{{Refbegin}} 

{{Refend}} 

==External links==
*[http://www.cpilj.com Official Website]
*[https://www.law.uconn.edu/student-life-resources/law-review-journals/connecticut-public-interest-law-journal Connecticut Public Interest Law Journal]

[[Category:American law journals]]
[[Category:Publications established in 2001]]
[[Category:Law and public policy journals]]


{{law-journal-stub}}