{{Infobox military person
|image= |
|name= Perence Shiri
|birth_date=11 January 1955 
|death_date= 
|birth_place=
|death_place=
||caption= 
|nickname=
|allegiance={{flagicon|Zimbabwe}} [[Zimbabwe]]
|serviceyears= 
|rank= [[Air Marshal]]
|branch= [[Air Force of Zimbabwe]]
|commands= [[Zimbabwean Fifth Brigade]]<br>[[Air Force of Zimbabwe]]
|unit=
|battles= 
|awards=Grand Commander of the Zimbabwe Order of Merit <ref>[http://www.jeanpaulleblanc.com/Zimbabwe.htm Orders, Decorations and Medals - Zimbabwe]</ref>
|laterwork= 
}}

[[Air Marshal]] '''Perence Shiri''' (born 11 January 1955<ref name="USExecOrd">[http://www.law.cornell.edu/uscode/uscode50/usc_sec_50_00001701----000-notes.html US Code Collection - Executive Order No. 13288], ''[[Cornell Law School]]''. Retrieved on 31 March 2007.</ref>) is the current commander of the [[Air Force of Zimbabwe]]<ref name="AFZ">[http://www.mod.gov.zw/airforce/airforce.htm Air Force of Zimbabwe], ''Zimbabwe Ministry of Defence''. Retrieved on 31 March 2007.</ref> and a member of the [[Joint Operations Command (Zimbabwe)|Joint Operations Command]] which exerts day-by-day control over Zimbabwe's government.<ref>{{cite web |url=http://www.telegraph.co.uk/news/worldnews/africaandindianocean/zimbabwe/2080992/Zimbabwean-generals-'have-taken-Robert-Mugabe's-power'.html |title=Zimbabwean generals have 'taken Robert Mugabe's power' |accessdate=2008-07-14 |last=Blair |first=David |date=2008-06-22 |publisher=telegraph.co.uk}}</ref>

Perence Shiri is a cousin of President [[Robert Mugabe]].<ref name="telegraph1">{{cite news |url=http://www.telegraph.co.uk/news/worldnews/africaandindianocean/zimbabwe/1404019/The-air-marshal-Gaddafi-and-the-big-grain-buy-up.html |title= The air marshal, Gaddafi and the big grain buy-up |publisher=[[telegraph.co.uk]] |date=2002-08-08 |accessdate=2007-03-31}}</ref>  He has called himself '''Black Jesus''',<ref name="blackjesus">{{cite book|last=St. John|first=Lauren|year=2007|publisher=Scribner|url=https://books.google.co.uk/books?redir_esc=y&id=3aZyAAAAMAAJ&focus=searchwithinvolume&q=Black+Jesus|title=Rainbow's End: A Memoir of Childhood, War, and an African Farm|page=234}}</ref> because according to an anonymous claim on [[BBC]] ''[[Panorama (TV series)|Panorama]]'' documentary "The Price of Silence", he "could determine your life like Jesus Christ.  He could heal, raise the dead, whatever.  So he claimed to be like that because he could say if you live or not."<ref>[http://news.bbc.co.uk/hi/english/static/audio_video/programmes/panorama/transcripts/transcript_10_03_02.txt "The Price of Silence", [[BBC]] ''[[Panorama (TV series)|Panorama]]''], 10 March 2002</ref>

==Military and political actions==
From 1983 to 1984, the [[Zimbabwean Fifth Brigade]], under Shiri's command, was responsible for a reign of terror in [[Matabeleland]].  During the slaughter, thousands of civilians were killed and thousands more were tortured.  Despite this, in 1986, Shiri was granted a place at the [[Royal College of Defence Studies]] in [[London]].<ref>{{cite news |url=http://news.bbc.co.uk/1/hi/programmes/panorama/1861719.stm |title= British Invitation to Mugabe's Butcher |publisher=[[bbc.co.uk]] |date=2002-03-08 |accessdate=2007-03-31}}</ref>

In 1992, Shiri was appointed as the commander of the Air Force of Zimbabwe, taking over from [[Air Chief Marshal]] [[Josiah Tungamirai]].<ref name="AFZ"/>

In the late 1990s and early 2000s, Shiri was reported to have organised farm invasions by war veterans.<ref>{{cite news |url=http://www.bbc.co.uk/pressoffice/pressreleases/stories/2002/03_march/10/panoramamugabe.shtml |title= Panorama reveals what British Government knew about Mugabe’s worst crimes |publisher=[[bbc.co.uk]] |date=2002-03-10 |accessdate=2007-03-31}}</ref>  In 2002, in response to the subsequent food shortage, Mugabe dispatched Shiri to [[South Africa]] to purchase maize.  This undertaking was backed by a credit note for the equivalent of [[Pound sterling|£]]17 million from the [[Libya]]n leader, [[Muammar al-Gaddafi|Colonel Gaddafi]].<ref name="telegraph1"/>

With the Mugabe government facing increasing problems, the Zimbabwean press reported in February 2007 that Shiri was regularly attending  General [[Solomon Mujuru]]’s unofficial meetings with other senior military commanders and some political leaders.  These meetings had discussed forcing Mugabe to the polls in 2008 with a view to his replacement as president.<ref>{{cite news |url=http://www.zimdaily.com/news/117/ARTICLE/1356/2007-02-23.html |title= Mujuru plots Mugabe's ouster |publisher=zimdaily.com |date=2007-02-23 |accessdate=2007-04-01 |archiveurl = http://web.archive.org/web/20070227223432/http://zimdaily.com/news/117/ARTICLE/1356/2007-02-23.html <!-- Bot retrieved archive --> |archivedate = 2007-02-27}}</ref>

In 2008 some Zimbabwean lawyers and opposition politicians from [[Mutare]] claimed that Shiri was the prime mover behind the military assaults on illegal diggers in the diamond mines in the east of Zimbabwe.<ref>{{cite news |url=https://www.theguardian.com/world/2008/dec/11/diamond-miners-zimbabwe-war-mugabe |title= Bodies pile up as Mugabe wages war on diamond miners |last=McGreal |first=Chris |publisher=guardian.co.uk |date=2008-12-11 |accessdate=2009-01-10}}</ref>

===2008 election===
In the days before the [[Zimbabwean presidential election, 2008|2008 Zimbabwean presidential election]] Shiri, along with other Zimbabwean Defence chiefs, held a press conference where they stated that defence and security forces had been deployed across the country to maintain order.  In a remark aimed against the [[Movement for Democratic Change - Tsvangirai|Movement for Democratic Change]], the defence chiefs stated that it would be a criminal act for anyone to declare himself the winner of the election.  They maintained that such a statement must only be made by the [[Zimbabwe Electoral Commission]].<ref>{{cite web |url=http://www.newzimbabwe.com/pages/army37.17961.html |title=Zimbabwe's defence chiefs issue threats on election eve |accessdate=2008-03-31 |last=Nkatazo |first=Lebo |date=2008-03-28 |work=newzimbabwe.com}}</ref>

==Sanctions against Shiri==
In 2002 the [[European Union]] barred Shiri from entering the EU<ref>{{cite news |url=http://www.telegraph.co.uk/news/main.jhtml;jsessionid=4AQI53KIKSGSTQFIQMFCFF4AVCBQYIV0?xml=/news/2002/02/19/wzim219.xml |title= EU targets the henchmen |publisher=[[telegraph.co.uk]] |date=2002-02-18 |accessdate=2007-03-31}}</ref> and on 6 March 2003, [[George W. Bush]] ordered the blocking of any of Shiri's property in the [[United States]].<ref name="USExecOrd"/>

==Assassination attempt==
Shiri was ambushed on 13 December 2008, while driving to his farm. According to police, he was accosted by unknown people who shot at his car. Thinking one of his tyres had burst he got out and was subsequently shot in the arm.<ref>[http://news.bbc.co.uk/2/hi/africa/7784975.stm  "Zimbabwe air force head 'wounded'"], BBC News</ref>  It has been speculated that the assassination attempt may have been a response to Shiri's attacks on illegal diamond miners in 2008 or because of his role in Matabeleland in the 1980s.<ref>{{cite web |url=https://www.theguardian.com/world/2008/dec/16/zimbabwe-assassination-attempt |title=Zimbabwe regime blames Mugabe ally 'assassination attempt' on opposition |accessdate=2009-01-10 |last=McGreal |first=Chris |date=2008-12-16 |work=guardian.co.uk}}</ref>

In October 2013, Shiri's son, Titus Takudzwa Chikerema, died aged 21.<ref>http://www.herald.co.zw/air-marshall-shiris-son-dies/</ref>

==References==
<!--See http://en.wikipedia.org/wiki/Wikipedia:Footnotes for an explanation of how to generate footnotes using the <ref(erences/)> tags-->
{{reflist|2}}

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Josiah Tungamirai|J Tungamirai]]}} 
{{s-ttl|title=Commander of the [[Air Force of Zimbabwe]]|years=1992&ndash;}}
{{s-inc}}
{{s-end}}

{{DEFAULTSORT:Shiri, Perence}}
[[Category:Air Force of Zimbabwe air marshals]]
[[Category:1955 births]]
[[Category:Living people]]
[[Category:Alumni of the Royal College of Defence Studies]]