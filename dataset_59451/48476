{{Orphan|date=February 2016}}

{{Use dmy dates|date=April 2015}}
{{Infobox film
| name           = Hiawatha (1913 film)
| image          = Hiawatha3.png
| caption        = Advertisement for ''Hiawatha'' 1913<ref name="Ad">"''Hiawatha'' Advertisement", ''[[Moving Picture World]]'', March 8, 1913, p. 962.</ref>
| director       = Edgar Lewis
| producer       = Frank E. Moore
| based on       = ''[[The Song of Hiawatha]]'' (1855) by [[Henry Wadsworth Longfellow]]
| starring       = [[Jesse Cornplanter]]<br>Soon-goot
| music          = [[John Joseph Braham, Sr.]]
| cinematography = [[Victor Milner]]
| distributor    = States Rights
| country        = United States
| released       = {{Film date|1913|03}}
| runtime        = 40 minutes (four reels)
| language       = Silent film<br />English intertitles
}}

'''''Hiawatha''''' is a 1913 silent film directed by Edgar Lewis and based upon [[Henry Wadsworth Longfellow]]'s  epic poem ''[[The Song of Hiawatha]]'' (1855). The film stars [[Jesse Cornplanter]] of the [[Seneca people]] and Soon-goot, a 17-year-old unknown actress.<ref>"Unspoiled Indians in True Portrayals", ''[[New York Dramatic Mirror]]'', April 9, 1913, p. 31.</ref>  The movie is the first feature film to use a cast of [[Native Americans in the United States|American Indians]].<ref name="Indian">{{cite web |url=http://indiancountrytodaymedianetwork.com/2015/03/24/what-was-first-feature-film-all-native-cast-159732 |title=What Was the First Feature Film With an All-Native Cast? |first1=Angela |last1=Aleiss  |work=[[Indian Country Today Media Network]] |date=March 24, 2015|accessdate=April 7, 2015}}</ref>

==Plot==
The story begins along the Lake Superior Michigan shoreline with the appearance of a mighty spirit that tells the Indians a peacekeeper will bring wisdom and unite the warring tribes. Hiawatha is born to Wenonah and after her death her mother [[Nokomis]] raises the child.  Hiawatha becomes an excellent hunter and later weds [[Minnehaha]], but she dies during a severe winter.

The final episodic adventure tells of the arrival of the "Black Robe" or missionary
who brings Christianity to the Indians.  Hiawatha welcomes him and proclaims the real prophet has arrived. As Hiawatha bids farewell to the warriors, he tells them to listen to the words of the missionary and then departs forever toward the sunset.

==Production==
[[File:Jesse Cornplanter.png|thumb|Jesse Cornplanter (center) in ''Hiawatha'' 1913.]]
Producer Frank E. Moore had previously staged the outdoor spectacle ''Hiawatha: The Indian Passion Play'' for nearly a decade before he launched his filmed version. Newspapers reported that 150 "full-blooded" Seneca of the [[Haudenosaunee]] (Iroquois Confederacy) from the [[Cattaraugus Reservation]] in upstate New York participated in the movie's production.<ref name="Ad"/>  For the lead role of Hiawatha, Moore hired Seneca actor-turned-artist [[Jesse Cornplanter]], who later collaborated as an illustrator with ethnographer/archaeologist [[Arthur C. Parker]] and  was the author of ''Legends of the Longhouse'' (1938).  Jesse Cornplanter was a descendent of the 18th-century Seneca war chief and diplomat [[Cornplanter]]<ref name="Indian"/>

The Anglo-American musical theater conductor [[John Joseph Braham, Sr.]] composed the musical score for Hiawatha.  Braham would later compose the score for the 1913 [[Edward S. Curtis]] film, ''[[In the Land of the Head Hunters]]''.<ref>Obituary of John Joseph Braham, Sr., ''[[The New York Times]]'', October 29, 1919.</ref>

The movie's cinematographer [[Victor Milner]] suggested to Moore that Edgar Lewis, a former stage actor, should direct.  Milner said that to achieve a superimposition of two images, he had to shoot the visioning of the famine of death on a separate negative and double print it.<ref>{{cite web |url=https://books.google.com/books?id=S08_AAAAYAAJ&pg=RA4-PA177&lpg=RA4-PA177&dq=edgar+lewis%22hiawatha%22&source=bl&ots=tbxgzk45qu&sig=Wfj5oFVvQWBDXU6yoPRB9tcgLCE&hl=en&sa=X&ei=V1j_VK68H8PzoATpk4DACA&ved=0CCgQ6AEwAg#v=onepage&q=edgar%20lewis%22hiawatha%22&f=false |title=Fade Out and Slowly Fade In |first1=Victor |last1=Milner |work=[[American Cinematographer]] |date=December 1923 |accessdate=April 7, 2015}}</ref>

Although other silent versions of ''Hiawatha'' existed before 1913, Moore's film was the first to use a cast of [[Native Americans in the United States|American Indians]].<ref>[[In the Land of the Headhunters]] (1914) was the first feature-length movie whose cast was composed entirely of Native North Americans [Native Canadians].  ''Hiawatha'' featured all American Indians except for the missionary.</ref>  In 1909, [[Carl Laemmle]], who founded [[Independent Moving Pictures]] (later absorbed into [[Universal Studios]]) had released an earlier one-reel version of ''Hiawatha''.  Years later Laemmle acknowledged that his “white cast smeared with bronze paint” was a target for ridicule. Laemmle's story ended with Hiawatha and Minnehaha happily embracing, but Moore's ''Hiawatha'' follows Longfellow's poem in which Minnehaha dies and Hiawatha welcomes the arrival of the missionary, who converts the Indians to the Christian faith.<ref name="Indian"/>  In 1910, Laemmle followed up his 1909 version of ''Hiawatha'' with the sequel, ''The Death of Minnehaha''.<ref>"The Death of Minehaha", ''[[Moving Picture World]]'', March 12, 1910, p. 384.</ref>

==Reception==
''Hiawatha'' opened at New York City's  Berkeley theater where it achieved "splendid sales", according to the ''Moving Picture News''.<ref>"''Hiawatha'' Selling Fast", ''Moving Picture News'', May 24, 1913.</ref>  Moore distributed ''Hiawatha'' selling the film by states rights to 12 states.<ref>[[Billboard (magazine)]], May 23, 1913.</ref> A review in [[Moving Picture World]] praised the film and said:

{{Quotation|We have had such a fearful surfeit of blood-thirsty Indians, scalping Indians, howling Indians, gambling Indians and murdering and burning Indians in the cheap films that it was like a breath of fresh air to see real human Indians enacting before us an old Indian legend.<ref>"Review of ''Hiawatha'' by W. Stephen Bush, [[Moving Picture World]], March 8, 1913, p. 980.</ref>}}

In April 1913, both the [[American Museum of Natural History]] and the [[American Scenic and Historic Preservation Society]] jointly presented ''Hiawatha'' at the museum with a simultaneous reading of [[Henry Wadsworth Longfellow]]'s poem during the film's projection.  The museum had lent Moore its expertise for the film and believed that ''Hiawatha'' had ethnographically redeeming features and educational appeal.<ref>{{cite web |url=https://books.google.com/books?id=2NKmvLXbZesC&pg=PA276&lpg=PA276&dq=%22alison+Griffiths%22wondrous%2Bhiawatha&source=bl&ots=XgcFgX9WRa&sig=zUtm3aijuLVQCvl0WAfFrPPwDkU&hl=en&sa=X&ei=2Xo6VZ32KMv3oATp6IDYBw&ved=0CEgQ6AEwBg#v=onepage&q=%22alison%20Griffiths%22wondrous%2Bhiawatha&f=false|title=Wondrous Difference: Cinema, Anthropology, & Turn-of-the-Century Visual Culture |first1=Alison |last1=Griffiths |date=2002 |accessdate=April 23, 2015}}</ref>  One of the movie’s highlights was a healing ritual of the sacred Iroquois [[False Face Society]].

''Hiawatha'' was originally four reels, or 40 minutes.  The Motion Picture, Broadcasting & Recorded Sound Division in the [[Library of Congress]] owns only an abridged copy of the film.<ref name="Indian"/>

==External links==
* {{IMDb name|nm0507150|Edgar Lewis}}
* {{AFI film|id=1793|title=Hiawatha}}
* {{IMDb title|tt0002954|Hiawatha}}
* Jesse Cornplanter {{Cite web|url=http://www.fofweb.com/History/MainPrintPage.asp?iPin=ENAIT127&DataType=Indian&WinType=Free |title=Facts on File History Database}}

==References==
{{reflist}}

*
*
*
*

[[Category:American silent feature films]]
[[Category:Films based on works by Henry Wadsworth Longfellow]]
[[Category:American films]]
[[Category:American black-and-white films]]