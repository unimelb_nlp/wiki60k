{{other uses2|Through the Looking Glass}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox book | 
| name          = Through the Looking-Glass
| title_orig    =
| translator    =
| image         = Through the looking glass.jpg <!--prefer 1st edition - It is the first edition-->
| caption       = First edition cover of ''Through the Looking-Glass''
| author        = [[Lewis Carroll]]
| illustrator   = [[John Tenniel]]
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = [[Children's fiction]]
| publisher     = [[Macmillan Publishers|Macmillan]]
| release_date  = 1871

| english_release_date =
| isbn          = <!-- NA -->
| preceded_by   = [[Alice's Adventures in Wonderland]]
| followed_by   =
}}

'''''Through the Looking-Glass, and What Alice Found There''''' (1871) is a novel by [[Lewis Carroll]] (Charles Lutwidge Dodgson), the sequel to ''[[Alice's Adventures in Wonderland]]'' (1865). Set some six months later than the earlier book, Alice again enters a fantastical world, this time by climbing through a mirror into the world that she can see beyond it. ''Through the Looking-Glass'' includes such celebrated verses as "[[Jabberwocky]]" and "[[The Walrus and the Carpenter]]", and the episode involving [[Tweedledum and Tweedledee]]. The mirror which inspired Carroll remains displayed in [[Charlton Kings]].

==Plot summary==
[[File:Aliceroom3.jpg|right|thumb|Alice entering the Looking Glass. Illustration by Sir [[John Tenniel]]]]
'''Chapter One – Looking-Glass House''':  [[Alice (Alice's Adventures in Wonderland)|Alice]] is playing with a white kitten (who she calls "Snowdrop") and a black kitten (who she calls "Kitty")—the offspring of Dinah, Alice's cat in ''Alice's Adventures in Wonderland''—when she ponders what the world is like on the other side of a mirror's reflection. Climbing up on the [[fireplace mantel]], she pokes at the wall-hung [[Portals in fiction|mirror]] behind the fireplace and discovers, to her surprise, that she is able to step through it to an [[Looking-Glass Land|alternative world]]. In this reflected version of her own house, she finds a book with looking-glass poetry, "[[Jabberwocky]]", whose [[Mirror writing|reversed printing]] she can read only by holding it up to the mirror. She also observes that the [[chess piece]]s have come to life, though they remain small enough for her to pick up.

'''Chapter Two – The Garden of Live Flowers''':  Upon leaving the house (where it had been a cold, snowy night), she enters a sunny spring garden where the flowers have the power of human speech; they perceive Alice as being a "flower that can move about." Elsewhere in the garden, Alice meets the [[Red Queen (Through the Looking-Glass)|Red Queen]], who is now human-sized, and who impresses Alice with her ability to [[Red Queen's race|run at breathtaking speeds]]. This is a reference to the [[Rules of chess|chess rule]] that [[Queen (chess)|queens]] are able to move any number of vacant squares at once, in any direction, which makes them the most "agile" of pieces.

'''Chapter Three – Looking-Glass Insects''':  The Red Queen reveals to Alice that the entire countryside is laid out in squares, like a gigantic chessboard, and offers to make Alice a queen if she can move all the way to the eighth rank/row in a chess match. This is a reference to the chess rule of [[Promotion (chess)|Promotion]]. Alice is placed in the second rank as one of the [[White Queen (Through the Looking-Glass)|White Queen's]] [[Pawn (chess)|pawn]]s, and begins her journey across the chessboard by boarding a train that literally jumps over the third row and directly into the fourth rank, thus acting on the rule that pawns can advance two spaces on their first move.

[[File:Tennieldumdee.jpg|thumb|right|Tenniel illustration of Tweedledum (centre) and Tweedledee (right) and Alice (left). 1871)]]
[[Image:Red King sleeping.jpg|thumb|Red King snoring, by John Tenniel]]
'''Chapter Four – Tweedledum and Tweedledee''':  She then meets the fat twin brothers [[Tweedledum and Tweedledee]], who she knows from the famous [[nursery rhyme]]. After reciting the long poem "[[The Walrus and the Carpenter]]", the Tweedles draw Alice's attention to the [[Red King (Through the Looking-Glass)|Red King]]—loudly snoring away under a nearby tree—and maliciously provoke her with idle philosophical banter that she [[Dream hypothesis|exists only as an imaginary figure]] in the Red King's dreams (thereby implying that she will cease to exist the instant he wakes up). Finally, the brothers begin acting out their nursery-rhyme by suiting up for battle, only to be frightened away by an enormous crow, as the nursery rhyme about them predicts.

[[File:Alice knight.jpg|thumb|right|Tenniel illustration of the White Knight. 1871]]
'''Chapter Five – Wool and Water''':  Alice next meets the [[White Queen (Through the Looking-Glass)|White Queen]], who is very absent-minded but boasts of (and demonstrates) her ability to [[precognition|remember future events]] before they have happened. Alice and the White Queen advance into the chessboard's fifth rank by crossing over a brook together, but at the very moment of the crossing, the Queen transforms into a [[The Sheep|talking Sheep]] in a [[Alice's Shop|small shop]]. Alice soon finds herself struggling to handle the oars of a small rowboat, where the Sheep annoys her with (seemingly) nonsensical shouting about "[[Glossary of rowing terms#Crab|crabs]]" and "[[Glossary of rowing terms#Feather|feathers]]". Unknown to Alice, these are standard terms in the jargon of rowing. Thus (for a change) the Queen/Sheep was speaking in a perfectly logical and meaningful way.

'''Chapter Six – Humpty Dumpty''':  After crossing yet another brook into the sixth rank, Alice immediately encounters [[Humpty Dumpty]], who, besides celebrating his [[unbirthday]], provides his own translation of the strange terms in "Jabberwocky". In the process, he introduces Alice (and the reader) to the concept of [[portmanteau]] words, before his inevitable fall.

'''Chapter Seven – The Lion and the Unicorn''':  ''"All the king's horses and all the king's men"'' come to Humpty Dumpty's assistance, and are accompanied by the [[White King (Through the Looking-Glass)|White King]], along with [[the Lion and the Unicorn]], who again proceed to act out a nursery rhyme by fighting with each other. In this chapter, the [[March Hare]] and [[The Hatter|Hatter]] of the first book make a brief re-appearance in the guise of "[[Anglo-Saxons|Anglo-Saxon]] messengers" called "Haigha" and "Hatta" (i.e. "Hare" and "Hatter"—these names are the only hint given as to their identities other than [[John Tenniel]]'s illustrations).

'''Chapter Eight – “It’s my own Invention”''':  Upon leaving the Lion and Unicorn to their fight, Alice reaches the seventh rank by crossing another brook into the forested territory of the Red Knight, who is intent on capturing the "white pawn"—who is Alice—until the [[White Knight (Through the Looking-Glass)|White Knight]] comes to her rescue. Escorting her through the forest towards the final brook-crossing, the Knight [[Haddocks' Eyes|recites a long poem]] of his own composition called Haddocks' Eyes, and repeatedly falls off his horse. His clumsiness is a reference to the "eccentric" L-shaped movements of chess [[knight (chess)|knights]], and may also be interpreted as a self-deprecating joke about Lewis Carroll's own physical awkwardness and [[stammering]] in real life.

'''Chapter Nine – Queen Alice''':  Bidding farewell to the White Knight, Alice steps across the last brook, and is automatically crowned a queen, with the crown materialising abruptly on her head. She soon finds herself in the company of both the White and Red Queens, who relentlessly confound Alice by using [[word play]] to thwart her attempts at logical discussion. They then invite one another to a party that will be hosted by the newly crowned Alice—of which Alice herself had no prior knowledge.

'''Chapter Ten – Shaking''':  Alice arrives and seats herself at her own party, which quickly turns to a chaotic uproar—much like the ending of the first book. Alice finally grabs the Red Queen, believing her to be responsible for all the day's nonsense, and begins shaking her violently with all her might. By thus "capturing" the Red Queen, Alice unknowingly puts the Red King (who has remained stationary throughout the book) into [[checkmate]], and thus is allowed to wake up.

'''Chapter Eleven – Waking''':  Alice suddenly awakes in her armchair to find herself holding the black kitten, who she deduces to have been the Red Queen all along, with the white kitten having been the White Queen.

'''Chapter Twelve – Which dreamed it?''':  The story ends with Alice recalling the speculation of the Tweedle brothers, that everything may have, in fact, been a dream of the Red King, and that Alice might herself be no more than a figment of ''his'' imagination. One final poem is inserted by the author as a sort of epilogue which suggests that life itself is but a dream.

==Characters==
[[File:Alice Looking Glass Guildford.jpg|thumb|160px|right|Life-size statue of ''Alice Through the Looking-Glass'' in the grounds of [[Guildford Castle]]]]
===Main characters===
{{col-begin}}
*[[Alice (Alice's Adventures in Wonderland)|Alice]]
*[[Bandersnatch]]
*Haigha ([[March Hare]])
*Hatta ([[The Hatter]])
*[[Humpty Dumpty]]
*[[Jabberwocky|The Jabberwock]]
*[[Jubjub bird]]
*[[Red King (Through the Looking-Glass)|Red King]]
*[[Red Queen (Through the Looking-Glass)|Red Queen]]
*[[The Lion and the Unicorn]]
*[[The Sheep]]
*[[The Walrus and the Carpenter]]
*[[Tweedledum and Tweedledee]]
*[[White King (Through the Looking-Glass)|White King]]
*[[White Knight (Through the Looking-Glass)|White Knight]]
*[[White Queen (Through the Looking-Glass)|White Queen]]
{{col-end}}

===Minor characters===
{{Main article|List of minor characters in the Alice series#Through the Looking Glass|l1=List of minor characters in ''Through the Looking Glass''}}
[[Image:Jabberwocky.jpg|thumb|''The Jabberwock'', as illustrated by [[John Tenniel]] for [[Lewis Carroll]]'s ''Through the Looking Glass'', including the poem "[[Jabberwocky]]".]]

===Returning characters===
The characters of Hatta and Haigha (pronounced as the English would have said "hatter" and "hare") make an appearance, and are pictured (by Sir John Tenniel, not by Carroll) to resemble their Wonderland counterparts, the Hatter and the March Hare. However, Alice does not recognise them as such.

Dinah, Alice's cat, also makes a return – this time with her two kittens, Kitty (the black one) and Snowdrop (the white one). At the end of the book they are associated with the Red Queen and the White Queen respectively in the looking-glass world.

Though she does not appear, Alice's sister is mentioned. In both ''Alice's Adventures in Wonderland'' and ''Through the Looking-Glass'', there are puns and quips about two non-existing characters, Nobody and Somebody. Paradoxically, the [[gnat]] calls Alice an old friend, though it was never introduced in ''Alice's Adventures in Wonderland''.

==Writing style and themes==

===Symbolism===
The themes and settings of ''Through the Looking-Glass'' make it a kind of [[mirror image]] of ''Wonderland'': the first book begins outdoors, in the warm month of May (4 May),<ref group=lower-alpha>In Chapter 7, "A Mad Tea-Party", Alice reveals that the date is "the fourth" and that the month is "May."</ref> uses frequent changes in size as a [[plot device]], and draws on the imagery of playing cards; the second opens indoors on a snowy, wintry night exactly six months later, on 4 November (the day before [[Guy Fawkes Night]]),<ref group=lower-alpha>In the first chapter, Alice speaks of the snow outside and the "bonfire" that "the boys" are building for a celebration "to-morrow", a clear reference to the traditional bonfires on Guy Fawkes Night, 5 November; in the fifth chapter, she affirms that her age is "seven and a half exactly."</ref> uses frequent changes in time and spatial directions as a plot device, and draws on the imagery of [[chess]]. In it, there are many mirror themes, including opposites, time running backwards, and so on.

The White Queen offers to hire Alice as her lady's maid and to pay her "Twopence a week, and jam every other day." Alice says that she doesn't want any jam today, and the Queen tells her: "You couldn't have it if you ''did'' want it. The rule is, jam tomorrow and jam yesterday—but never jam ''to-day''."  This is a reference to the rule in Latin that the word ''iam'' or ''jam'' meaning ''now'' in the sense of ''already'' or ''at that time'' cannot be used to describe ''now'' in the present, which is ''nunc'' in Latin.  ''Jam'' is therefore never available today.<ref>Cook, Eleanor (2006). ''Enigmas and Riddles in Literature''. Cambridge University Press. p. 163. ISBN 0521855101.</ref>

{{expand section|date=November 2012}}

===Chess===
[[File:Alice chess game.png|thumb|Lewis Carroll's diagram of the story as a chess game]]
Whereas the first book has the [[Playing card|deck of cards]] as a theme, this book is based on a game of chess, played on a giant chessboard with fields for squares. Most main characters in the story are represented by a chess piece or animals, with Alice herself being a pawn.

The looking-glass world is divided into sections by brooks or streams, with the crossing of each brook usually signifying a notable change in the scene and action of the story: the brooks represent the divisions between squares on the chessboard, and Alice's crossing of them signifies advancing of her piece one square. Furthermore, since the brook-crossings do not always correspond to the beginning and ends of chapters, most editions of the book visually represent the crossings by breaking the text with several lines of asterisks (&nbsp;*&nbsp;*&nbsp;*&nbsp;). The sequence of moves (white and red) is not always followed. The most extensive treatment of the chess motif in Carroll's novel is provided in [[Glen Downey (writer)|Glen Downey]]'s ''The Truth About Pawn Promotion: The Development of the Chess Motif in Victorian Fiction''.<ref>http://www.nlc-bnc.ca/obj/s4/f2/dsk2/tape15/PQDD_0006/NQ34258.pdf</ref>

==Poems and songs==
[[Image:Briny Beach.jpg|thumb|[[The Walrus and the Carpenter]]]]
{{Listen|filename=Bonnie Dundee.ogg|title=To the Looking-Glass world it was Alice that said...|description=Tune for ''To the Looking-Glass world it was Alice that said...''}}
* Prelude ("Child of the pure unclouded brow")
* "[[Jabberwocky]]" (seen in the mirror-house; [[wikisource:Jabberwocky|full poem here]] including readings)
* "[[Tweedledum and Tweedledee]]"
* "[[The Walrus and the Carpenter]]" ([[wikisource:The Walrus and the Carpenter|full poem here]])
* "[[Humpty Dumpty]]"
* "In Winter when the fields are white..."
* "[[The Lion and the Unicorn]]"
* "[[Haddocks' Eyes]]" / The Aged Aged Man / Ways and Means / A-sitting on a Gate, the song is A-sitting on a Gate, but its other names and callings are placed above.
* "[[Rock-a-bye Baby|Hush-a-by lady, in Alice's lap...]]" (Red Queen's lullaby)
* "[[Bonnie Dundee#Lewis Carroll|To the Looking-Glass world it was Alice that said...]]"
* White Queen's riddle
* "[[Alice Liddell#Comparison with fictional Alice|A boat beneath a sunny sky]]" is the first line of a titleless [[acrostic]] poem at the end of the book—the beginning letters of each line, when put together, spell Alice Pleasance Liddell.

==The Wasp in a wig==
Lewis Carroll decided to suppress a scene involving what was described as "a wasp in a wig" (possibly a play on the commonplace expression "bee in the bonnet"). It has been suggested in a biography by Carroll's nephew, Stuart Dodgson Collingwood, that one of the reasons for this suppression was a suggestion from his illustrator, John Tenniel,<ref>{{cite web |website=listverse.com| url=http://listverse.com/2013/01/14/deleted-book-chapters/|date=January 14, 2013| title=10 Deleted Chapters that Transformed Famous Books|author=Symon, Evan V.}}</ref> who wrote in a letter to Carroll dated 1 June 1870:

{{quote|...I am bound to say that the '' 'wasp' '' chapter doesn't interest me in the least, and I can't see my way to a picture. If you want to shorten the book, I can't help thinking – with all submission – that ''there'' is your opportunity.<ref>{{cite book |first=Martin |last=Gardner |authorlink=Martin Gardner |title=The Annotated Alice |year=2000 |publisher=W. W. Norton & Company |ISBN =0-393-04847-0 |page=283 |accessdate=29 May 2009}}</ref>}}

For many years no one had any idea what this missing section was or whether it had survived. In 1974, a document purporting to be the galley proofs of the missing section was sold at [[Sotheby's]]; the catalogue description read, in part, that "The proofs were bought at the sale of the author's ... personal effects ... Oxford, 1898...". The bid was won by John Fleming, a [[Manhattan]] book dealer. The winning bid was £1,700.{{Citation needed|date=March 2007}} The contents were subsequently published in Martin Gardner's ''[[The Annotated Alice]]: The Definitive Edition'', and is also available as a hardback book ''The Wasp in a Wig: A Suppressed Episode ...''.<ref>(Clarkson Potter, MacMillan & Co.; 1977)</ref>

The rediscovered section describes Alice's encounter with a wasp wearing a yellow wig, and includes a full previously unpublished poem. If included in the book, it would have followed, or been included at the end of, Chapter 8 – the chapter featuring the encounter with the White Knight.{{original research inline|date=January 2013}} The discovery is generally accepted as genuine, but the proofs have yet to receive any physical examination to establish age and authenticity.

==Dramatic adaptations==
{{redirect|Alice Through the Looking Glass|the film|Alice Through the Looking Glass (2016 film)}}
The book has been adapted several times, in combination with ''Alice's Adventures in Wonderland'' and as a stand-alone film or television special.

===Stand-alone versions===
The adaptations include [[Musical theatre|live]], [[Musical film|TV musicals]], live action and animated versions and radio adaptations. One of the earliest adaptations was a [[Silent film|silent movie]] directed by [[Walter Lang]], ''Alice Through a Looking Glass'', in 1928.<ref>{{IMDb title|0018640|Alice Through a Looking Glass|(1928)}}</ref>

A dramatised version directed by [[Douglas Cleverdon]] and starring [[Jane Asher]] was recorded in the late 1950s by [[Argo Records (UK)|Argo Records]], with actors [[Tony Church]], [[Norman Shelley]] and [[Carleton Hobbs]], and [[Margaretta Scott]] as the narrator.<ref>{{cite web |accessdate=15 November 2009 |url= http://myweb.tiscali.co.uk/tauspace/sound_only2.htm |title=Alice in Wonderland: Wired for Sound
|publisher=  |year=}}</ref>

Musical versions include the 1966 TV musical with songs by [[Moose Charlap]], and [[Judi Rolin]] in the role of Alice,<ref>{{cite web|url=http://www.kiddiematinee.com/a-alice66.html |title=ALICE THROUGH THE LOOKING GLASS (1966, U.S.) |publisher=Kiddiematinee.com |date=6 November 1966 |accessdate=16 January 2012}}</ref><ref>{{IMDb title|0060088|Alice Through the Looking Glass|(1966)}}</ref> a Christmas 2007 multimedia stage adaptation at The [[Tobacco Factory]] directed and conceived by Andy Burden, written by Hattie Naylor, music and lyrics by Paul Dodgson and a 2008 opera ''[[Through the Looking Glass (opera)|Through the Looking Glass]]'' by [[Alan John]].

Television versions include the 1973 [[BBC]] TV movie, ''Alice Through the Looking Glass'', with [[Sarah Sutton]] playing Alice,<ref>{{IMDb title|0071116|Alice Through the Looking Glass|(1973)}}</ref> a 1982 38-minute Soviet [[cutout animation|cutout-animated]] film made by [[Kievnauchfilm]] studio and directed by Yefrem Pruzhanskiy,<ref>{{cite web|url=http://www.animator.ru/db/?ver=eng&p=show_film&fid=3482 |title=Russian animation in letters and figures &#124; Films &#124; "ALICE IN THE LAND IN THE OTHER SIDE OF THE MIRROR" |publisher=Animator.ru |accessdate=16 January 2012}}</ref> [[Alice Through the Looking Glass (1987 film)|an animated TV movie]] in 1987, with [[Janet Waldo]] as the voice of Alice ([[Mr. T]] was the voice of the Jabberwock)<ref>{{IMDb title|0101294|Alice Through the Looking Glass|(1987)}}</ref> and the 1998 [[Channel 4]] TV movie, with [[Kate Beckinsale]] playing the role of Alice. This production restored the lost "Wasp in a Wig" episode.<ref>{{IMDb title|0167758|Alice Through the Looking Glass|(1998)}}</ref>

In March 2011, Japanese companies [[Toei Animation|Toei]] and [[Banpresto]] announced that a collaborative animation project based on ''Through the Looking-Glass'' tentatively titled {{Nihongo|''Kyōsō Giga''|京騒戯画}}<ref>{{cite web|url=http://www.kyousogiga.com |title=Kyousogiga.com |publisher=Kyousogiga.com |accessdate=5 November 2011}}</ref> was in production.

On 22 December 2011, [[BBC Radio 4]] broadcast an adaptation by [[Stephen Wyatt]] on ''Saturday Drama''<ref>http://www.bbc.co.uk/programmes/b01pf5d7</ref> with [[Lauren Mote]] as Alice, [[Julian Rhind-Tutt]] as Lewis Carroll (who not only narrates the story but is also an active character), [[Carole Boyd]] as the Red Queen, [[Sally Phillips]] as the White Queen, [[Nicholas Parsons]] as Humpty-Dumpty, [[Alistair McGowan]] as Tweedledum and Tweedledee and [[John Rowe (actor)|John Rowe]] as the White Knight.

===With ''Alice's Adventures in Wonderland''===
Adaptations combined with ''Alice's Adventures in Wonderland'' include the 1933 live-action movie ''[[Alice in Wonderland (1933 film)|Alice in Wonderland]]'', starring a huge [[all-star]] cast and [[Charlotte Henry]] in the role of Alice. It featured most of the elements from ''Through the Looking Glass'' as well, including [[W. C. Fields]] as Humpty Dumpty, and a [[Harman-Ising]] animated version of ''The Walrus and the Carpenter''.<ref>{{IMDb title|0023753|Alice in Wonderland|(1933)}}</ref> The 1951 animated [[The Walt Disney Company|Disney]] movie ''[[Alice in Wonderland (1951 film)|Alice in Wonderland]]'' also features several elements from ''Through the Looking-Glass'', including the talking flowers, Tweedledee and Tweedledum, and "The Walrus and the Carpenter".<ref>{{IMDb title|0043274|Alice in Wonderland|(1951)}}</ref> Another adaptation, ''[[Alice's Adventures in Wonderland (1972 film)|Alice's Adventures in Wonderland]]'', produced by ''Joseph Shaftel Productions'' in 1972 with [[Fiona Fullerton]] as Alice, included the twins [[The Cox Twins|Fred and Frank Cox]] as Tweedledum and Tweedledee.<ref>{{IMDb title|0068190|Alice's Adventures in Wonderland|(1972)}}</ref> The 2010 film ''[[Alice in Wonderland (2010 film)|Alice in Wonderland]]'' by [[Tim Burton]] contains elements of both ''Alice's Adventures in Wonderland'' and ''Through the Looking-Glass''.<ref>{{IMDb title|1014759|Alice in Wonderland|(2010)}}</ref>

The 1974 Italian TV series Nel Mondo Di Alice (In the World of Alice) which covers both novels, covers ''Through the Looking-Glass'' in episodes 3 and 4.<ref>{{Citation|last=fictionrare2|title=Nel mondo di Alice 3^p|date=2014-09-29|url=https://www.youtube.com/watch?v=aFCkAm6ZbLg|accessdate=2016-04-23}}</ref>

Combined stage productions include the 1980 version, produced and written by [[Elizabeth Swados]], ''Alice in Concert'' (aka ''Alice at the Palace''), performed on a bare stage. [[Meryl Streep]] played the role of Alice, with additional supporting cast by [[Mark Linn-Baker]] and [[Betty Aberlin]]. In 2007, Chicago-based Lookingglass Theater Company debuted an acrobatic interpretation of ''Alice's Adventures in Wonderland'' and ''Through the Looking Glass'' with ''Lookingglass Alice''.<ref>{{cite web|url=http://www.lookingglasstheatre.org/content/node/766 |title=Lookingglass Alice &#124; Lookingglass Theatre Company |publisher=Lookingglasstheatre.org |date=13 February 2007 |accessdate=16 January 2012}}</ref> ''Lookingglass Alice'' was performed in New York City, [[Philadelphia]], Chicago,<ref>{{cite web|url=http://lookingglasstheatre.org/content/node/2043 |title=Lookingglass Alice Video Preview |publisher=Lookingglasstheatre.org |accessdate=5 November 2011}}</ref> and in a version of the show which toured the United States.

Iris Theatre in London, England, had a 2 part version of both novels in which ''Through the Looking-Glass'' was part 2. Alice was played in both parts by Laura Wickham. It was staged in the summer of 2013.<ref>{{Cite web|url=https://all-in-the-golden-afternoon96.tumblr.com/theatreadaptations|title=Theatre adaptations (excluding reimaginings)|website=all-in-the-golden-afternoon96.tumblr.com|access-date=2016-04-23}}</ref>

Laura Wade's Alice, a modern adaptation of both books premiering at [[Crucible Theatre|the Crucible Theatre]] in Sheffield in 2010, adapted parts of both novels.

[[Wonder.land]] by Moira Buffini and Damon Albarn takes some characters from the second novel, notably Dum and Dee and Humpty Dumpty. The show also merges the Queen of Hearts and the Red Queen into one character.

Adrian Mitchell's Alice in Wonderland and Through the Looking-Glass adaptation for the [[Royal Shakespeare Company|RSC (Royal Shakespeare Company)]] adapted through the Looking-Glass in act 2.<ref>{{Cite web|url=https://all-in-the-golden-afternoon96.tumblr.com/theatreadaptations|title=Theatre adaptations (excluding reimaginings)|website=all-in-the-golden-afternoon96.tumblr.com|access-date=2016-04-23}}</ref>

The 1985 two-part TV musical ''[[Alice in Wonderland (1985 film)|Alice in Wonderland]]'', produced by [[Irwin Allen]], covers both books; Alice was played by [[Natalie Gregory]]. In this adaptation, the Jabberwock materialises into reality after Alice reads "Jabberwocky", and pursues her through the second half of the musical.<ref>{{IMDb title|0088693|Alice in Wonderland|(1985)}}</ref> The 1999 made-for-TV Hallmark/NBC film ''[[Alice in Wonderland (1999 film)|Alice in Wonderland]]'', with [[Tina Majorino]] as Alice, merges elements from ''Through the Looking Glass'' including the talking flowers, Tweedledee and Tweedledum, "The Walrus and the Carpenter", and the chess theme including the snoring Red King and White Knight.<ref>{{IMDb title|164993|Alice in Wonderland|(1999)}}</ref> The 2009 [[Syfy]] TV miniseries ''[[Alice (miniseries)|Alice]]'' contains elements from ''Alice's Adventures in Wonderland'' and ''Through the Looking-Glass''.<ref>{{IMDb title|1461312|Alice|(2009)}}</ref>

===Other===
* The 1977 film ''[[Jabberwocky (film)|Jabberwocky]]'' expands the story of the poem "Jabberwocky".<ref>{{IMDb title|0076221|Jabberwocky|(1977)}}</ref>
* The 1936 [[Mickey Mouse]] short film ''[[Thru the Mirror]]'' has Mickey travel through his mirror and into a bizarre world.
* The 1959 film ''[[Donald in Mathmagic Land]]'' includes a segment with [[Donald Duck]] dressed as Alice meeting the Red Queen on a chessboard.
* The 2011 ballet ''Through the Looking-Glass'' by American composer [[John Craton]]
* 2013 book (''Through the Zombie Glass'') by Gena Showalter
* The 2016 film ''[[Alice Through the Looking Glass (2016 film)|Alice Through the Looking Glass]]'' by [[James Bobin]], a sequel to the 2010 film ''[[Alice in Wonderland (2010 film)|Alice in Wonderland]]''.

==See also==
{{portal|Novels|Children's literature|United Kingdom}}
{{div col|colwidth=20em}}
* [[Alice chess]]
* "[[I Am the Walrus]]"
* [[Translations of Alice's Adventures in Wonderland|Translations of ''Alice's Adventures in Wonderland'']]
* [[Translations of Through the Looking-Glass|Translations of ''Through the Looking-Glass'']]
* [[Vorpal sword]]
* [[Works based on Alice in Wonderland|Works based on ''Alice in Wonderland'']]
{{div col end}}

==References==

===Notes===
{{Reflist|group=lower-alpha}}

===Citations===
{{Reflist|30em}}

===Other sources===
{{refbegin}}
*{{cite book | last=Tymn | first=Marshall B. |author2=Kenneth J. Zahorski and Robert H. Boyer | title=Fantasy Literature: A Core Collection and Reference Guide | location=New York | publisher=R.R. Bowker Co. | page=61 | year=1979 | isbn =0-8352-1431-1}}
*{{cite book | last=Gardner | first=Martin | title=More Annotated Alice | location=New York | publisher=Random House | page=363 | year=1990 | isbn =0-394-58571-2}}
*{{cite book | last=Gardner | first=Martin | title=The Annotated Alice | location=New York | publisher=Clarkson N. Potter | pages=180–181 | year=1960}}
{{refend}}

==External links==
{{wikisource|Through the Looking-Glass, and What Alice Found There}}
{{wikiquote|Through the Looking-Glass}}
{{Commons category-inline|Through the Looking-Glass and What Alice Found There}}
*[https://sites.google.com/site/lewiscarrollillustratedalice/ A catalogue of illustrated editions of the Alice books from 1899 to 2009]
;Online texts
* [http://epublib.info/through-the-looking-glass-by-lewis-carroll Through the Looking-Glass by Lewis Carroll] ePub, Mobi, PDF versions
{{Gutenberg|no=12|name=Through the Looking-Glass}}
* {{librivox book | title=Through the Looking-Glass | author=Lewis Carroll}}
* [http://www.sabian.org/alice.htm HTML version with commentary of Sabian religion]
*[http://www.alice-in-wonderland.net/alice4.html Text of ''A Wasp in a Wig'']
*[https://archive.org/details/throughlookinggl00carr7 c1917 edition from archive.org] – scanned for download or reading online
*[http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/sisu_manifest.html Multiple Formats] ( [http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/toc.html html], XML, opendocument [http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/opendocument.odt ODF], pdf ([http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/landscape.pdf landscape], [http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/portrait.pdf portrait]), [http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/plain.txt plaintext], [http://www.jus.uio.no/sisu/through_the_looking_glass.lewis_carroll/concordance.html concordance] ) [[SiSU]]

{{Alice|state=expanded}}
{{Lewis Carroll}}

{{DEFAULTSORT:Through The Looking-Glass}}
[[Category:Alice in Wonderland]]
[[Category:Novels about chess]]
[[Category:Children's fantasy novels]]
[[Category:British children's novels]]
[[Category:Literature featuring anthropomorphic characters]]
[[Category:Macmillan Publishers books]]
[[Category:Sequel novels]]
[[Category:Works by Lewis Carroll]]
[[Category:19th-century British novels]]
[[Category:1870s fantasy novels]]
[[Category:1871 novels]]
[[Category:British novels adapted into films]]
[[Category:Victorian novels]]
[[Category:Novels adapted into television programs]]