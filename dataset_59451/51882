{{Infobox journal
| cover = [[File:Journal of Health and Social Behavior.JPG]]
| formernames = Journal of Health and Human Behavior
| discipline = [[Medical sociology]]
| abbreviation = J. Health Soc. Behav.
| editors = Richard M. Carpiano, Brian C. Kelly
| publisher = [[SAGE Publications]] on behalf of the [[American Sociological Association]]
| country = United States
| frequency = Quarterly
| history = 1960-present
| impact = 3.333
| impact-year = 2012
| openaccess =
| website = http://www.sagepub.com/journals/Journal201971/title
| link1 = http://hsb.sagepub.com/content/current
| link1-name = Online access
| link2 = http://hsb.sagepub.com/content
| link2-name = Online archive
| OCLC = 1695738
| CODEN = JHSBA5
| ISSN = 0022-1465
| eISSN = 2150-6000
| JSTOR = 00221465
| LCCN = 65003408
}}
The '''''Journal of Health and Social Behavior''''' is a quarterly [[peer-reviewed]] [[academic journal]] published by [[SAGE Publications]] on behalf of  the [[American Sociological Association]]. It covers the application of [[Sociology|sociological]] concepts and methods to the understanding of health and illness and the organization of medicine and health care. The [[editors-in-chief]] are Richard M. Carpiano ([[University of British Columbia]]) and Brian C. Kelly ([[Purdue University]]). Previous editors-in-chief include Gilbert Gee ([[University of California, Los Angeles]]), Debra Umberson ([[University of Texas at Austin]]), and Eliza Pavalko ([[Indiana University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[AgeLine]], [[Applied Social Science Index and Abstracts]], [[Education Resources Information Center]], [[MEDLINE]]/[[PubMed]], [[PsycINFO]], [[Science Citation Index]], [[Scopus]], and [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 3.333, ranking it 4th out of 60 journals in the category "Psychology, Social"<ref name=WoS1>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Psychology, Social |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref> and 7th out of 136 journals in "Public, Environmental & Occupational Health".<ref name=WoS2>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Public, Environmental & Occupational Health |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> 

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201971/title}}

[[Category:Publications established in 1960]]
[[Category:Sociology journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Public health journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Social psychology journals]]