__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Besson H-3
 | image=Besson H-3 at Paris Salon 1919.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Touring [[flying-boat]]
 | national origin=France
 | manufacturer=[[Besson (aircraft)|Marcel Besson]]
 | designer=
 | first flight=1920
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Besson H-3''' was a [[France|French]] civil touring triplane [[flying boat]] designed by the [[Besson (aircraft)|Marcel Besson]] company of Boulogne.<ref name="orbis" /> Ony one aircraft was built and the type did not enter production.<ref name="orbis" />

==Design and development==
The H-3 was designed as a civil touring flying boat and had single-bay equal-span wings and room for two in a [[side-by-side configuration]] cockpit, it was fitted with dual-controls.<ref name="orbis" /> Initially powered by a {{convert|60|hp|abbr=on|disp=flip}} [[le Rhône 9Z]] rotary, the H-3 was found to be under-powered and re-engined with a {{convert|130|hp|kW|0|abbr=on|disp=flip}} [[Clerget 9B]] rotary, (from ''Société Clerget-Blin et Cie''), driving a tractor propeller.<ref name="orbis" />  The aircraft did not enter production and the sole H-3 was re-designated '''MB-12''' in 1922 when it was modified with an enlarged central wing.<ref name="orbis" />

==Specifications (H-3)==
{{Aircraft specs
|ref=<ref name="orbis" />
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=8.25
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note= 
|mid span m=11
|mid span ft=
|mid span in=
|mid span note=after modification to MB-12
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=3.10
|height ft=
|height in=
|height note=
|wing area sqm=30.99
|wing area sqft=
|wing area note=the MB-12 had a wing area of {{convert|32.20|m2|abbr=on}}
|empty weight kg=590
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=875
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Clerget 9B]]
|eng1 type=rotary piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->130
|eng1 note=
|more power=

|prop blade number=2
|prop name=fixed pitch wooden tractor propeller
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=155
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=450
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=3200
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[List of seaplanes and amphibious aircraft]]
}}

==References==

===Notes===
{{reflist|refs=
<ref name="orbis">Orbis 1985, p. 655</ref>
}}

===Bibliography===
{{refbegin}}
*{{cite book |last= |first= |authorlink= |coauthors= |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=|pages=}}
{{refend}}

<!-- ==External links== -->

[[Category:Flying boats]]
[[Category:French civil utility aircraft 1920–1929]]
[[Category:Triplanes]]
[[Category:Besson aircraft|H-3]]
[[Category:Single-engined pusher aircraft]]