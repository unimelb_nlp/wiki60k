'''Nilo W. Hovey''' (September 22, 1906 - March 14, 1986) was a clarinetist, [[composer]], [[Conductor (music)|conductor]], music educator and author of many musical instrument method books.<ref name="ABA">Victor William Zajec. [http://americanbandmasters.org/pdfs/ABA-Past-Presidents.pdf "33. Nilo W. Hovey, President 1970"], ''[[American Bandmasters Association]]''. Retrieved on 25 February 2015.</ref>

==Early life==

Nilo Wellington Hovey was born in Iowa on September 22, 1906 to Leroy Dana and Lois Graham Hovey. Raised in Cedar Falls, [[Iowa]], Hovey participated in the Cedar Falls Municipal Band, initially on saxophone, but eventually on most of the reed instruments, and attended Iowa State Teacher's College (now the [[University of Northern Iowa]]).<ref name="ABA" />

==Career==

During Hovey's tenure as Director of Instrumental Music in the public schools of Hammond, Indiana (1926–44), he wrote his first instructional book, a method book for clarinet titled Rubank Elementary Method: Clarinet.<ref name="NBAHOF">[http://nationalbandassociationhalloffame.org/directory/Hovey.php "Nilo W. Hovey, Induction: February 8, 1986"], ''[[National Band Association Hall of Fame]]''. Retrieved on 25 February 2015.</ref>

Among the bands he directed in Hammond were the Hammond Technical High School band and the George Rogers Clark High School band, both of which received accolades under his direction.<ref name="ABA" />

In 1944, Mr. Hovey began a position as director of the Concert Band and Chairman of the Music Education department at the Arthur Jordan College of Music at Butler University in Indianapolis, Indiana.<ref name="century">Krivin, Martin. ''A century of wind instrument manufacturing in the United States, 1860-1960''. State University of Iowa, 1961, p.167</ref><ref name="KMR">''Kansas Music Review, Volumes 27-29''. 1965</ref><ref name="James">Boyer, D. Royce, and Philip James. ''The World War I Army Bandsman: A Diary Account by Philip James''. American Music, 1996, pp.185-204.</ref>

Mr. Hovey started working for the Selmer Company in 1957 as the Education Director in Elkhart, Indiana and remained there for the next 18 years and received many awards and accolades.<ref name="KMR" />

Hovey served as the president of the Music Industry Council from 1962-1964, as well as the president of the American Bandmaster's Association from 1970-1971. He was also a member of [[Phi Mu Alpha Sinfonia]], and an honorary member of [[Kappa Kappa Psi]], [[Phi Beta Mu]] and the Canadian Bandmasters Association.<ref name="STGC">Selmer Teacher's Guide to the Clarinet</ref> Before his death, Hovey continued to direct numerous bands and ensembles and wrote or edited many music instruction books and methods which are still in use today.<ref name="IBM">[http://www.indianabandmasters.org/PhiBetaMu/Hovey.Nilo.pdf "Nilo W. Hovey"], ''[[Indiana Bandmasters Association]]'', Indiana. Retrieved on 10 February 2015.</ref> Hovey was also inducted into the National Band Association’s Hall of
Fame of Distinguished Band Conductors shortly before his death in March, 1986.<ref name="ABA"/>

==Personal life==

Hovey married Ruth Emily Sinden from [[Maquoketa, Iowa|Maquoketa]], Iowa in December 1927, whom he met while a student at Iowa State Teachers College.<ref name="ABA"/> After her death in 1978, Hovey married Helen Gowdy in 1979.<ref name="IBM"/> Hovey had two daughters by the previous marriage.
Hovey died on March 14, 1986.<ref name="ABA"/>

==List of works==

===Books===

* ''Advanced Technique for Bands: Trumpet'' By Nilo W. Hovey (M.M. Cole Publishing Co., 1980)
* ''Advanced Technique for Bands: Alto Saxophone'' By Nilo W. Hovey (M.M. Cole Publishing Co., 1980)
* ''Belwin Clarinet Method, Book One'' By Kenneth Gekeler, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Clarinet Method, Book 2'' (as editor) By Gekeler & Kenneth, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Saxophone Method'' By Kenneth Gekeler, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Oboe Method, Book One'' (as editor) By Kenneth Gekeler, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Flute Method Book Two'' By Kenneth Gekeler, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Elementary Band Method: Trombone (Bass Clef)'' By Fred Weber, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Elementary Band Method'' By Fred Weber, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Belwin Intermediate Band Method Bb Cornet (Trumpet)'' By Fred Weber, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Cimera-Hovey Method for Trombone and Baritone'' By Jaroslav Cimera and Nilo W. Hovey (Alfred Music, 1985)
* ''Daily Exercises for Saxophone'' By Nilo W. Hovey (Alfred Music, 1985)
* ''Edwards-Hovey Method for Cornet or Trumpet'' By Austyn R. Edwards, Nilo W. Hovey (Editor) (Alfred Music, 1985) 
* ''Edwards-Hovey Method for Cornet or Trumpet, Bk 2'' By Austyn R. Edwards and Nilo W. Hovey (Alfred Music, 1985)
* ''Efficient Rehearsal Procedures for School Bands'', (Elkhart: The Selmer Co., 1976) 
* ''First Book of Practical Studies: Cornet and Trumpet'' By Robert W. Getchell, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''First Book of Practical Studies for Clarinet'' By Nilo W. Hovey (Alfred Music, 1985)
* ''First Book of Practical Studies for Tuba'' By Robert W. Getchell, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''First Book of Practical Studies for Bassoon'' By D. McDowells, Nilo W. Hovey (Editor) (Alfred Music 1987)
* ''First Book of Practical Studies for Saxophone'' By Nilo W. Hovey (Alfred Music, 1985)
* ''Pottag-Hovey Method for French Horn, Book One'' By Max P. Pottag and Nilo W. Hovey (Alfred Music, 1985)
* ''Practical Studies for Cornet and Trumpet, Book II'' By Robert W. Getchell, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Rubank Elementary Method Oboe''
* ''Rubank Elementary Method Clarinet''
* ''Rubank Elementary Method Saxophone''
* ''Rubank Elementary Method E-flat/BB-flat Bass (Tuba or Sousaphone)''
* ''Rubank Instrumental Charts Complete Chromatic Fingering Charts for Basses (Eb & BBb)''
* ''2nd Book of Practical Studies for Cornet and Trumpet'' (as editor)
* ''Second Book of Practical Studies for Clarinet'' By Nilo W. Hovey (Alfred Music, 1985)
* ''Second Book of Practical Studies for Tuba'' By Robert W. Getchell, Nilo W. Hovey (Editor) (Alfred Music, 1985)
* ''Second Book of Practical Studies for Saxophone'' By Nilo W. Hovey (Alfred Music, 1985)
* ''Selmer Teacher’s Guide to the Clarinet'' (1967)
* ''Supplementary Duets for Flutes''
* ''T-I-P-P-S for Band: E-Flat Alto Saxophone'' By Nilo W. Hovey (Alfred Music, 1985)
* ''T-I-P-P-S for Band: E-Flat Baritone Saxophone''
* ''T-I-P-P-S for Band: Trombone''
* ''T-I-P-P-S for Band: C Flute (Piccolo)''
* ''T-I-P-P-S for Band: B-Flat Tenor Saxophone''
* ''T-I-P-P-S for Band: Bassoon''
* ''T-I-P-P-S for Band: Baritone''
* ''T-I-P-P-S for Band: E-Flat Alto Clarinet''<ref name="sm">[http://www.sheetmusicplus.com "Sheet Music Plus"], ''[[Sheet Music Plus]]''. Retrieved on 27 February 2015.</ref>
<!--
First Book of Practical Studies: Cornet and Trumpet
Robert W. Getchell (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1985)
ISBN-13: 978-0769219578

2nd Book of Practical Studies for Cornet and Trumpet
Nilo W. Hovey (Editor), Robert W. Getchell (Composer)
Publisher: Alfred Music (March 1985)
Language: English
ISBN-13: 978-0769221960

First Book of Practical Studies for Clarinet
Nilo W. Hovey (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769208107

Pottag-Hovey Method for French Horn, Book One
Max P. Pottag (Author), Nilo W. Hovey (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222165

First Book of Practical Studies for Tuba
Nilo W. Hovey (Editor), Robert W. Getchell (Composer)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222653

Second Book of Practical Studies for Tuba
Robert W. Getchell (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222646

Belwin Clarinet Method, Bk 1
Kenneth Gekeler  (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769218632

Daily Exercises for Saxophone
Nilo W. Hovey  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769224336

First Book of Practical Studies for Bassoon
D. McDowells (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1987)
Language: English
ISBN-13: 978-0769225944

Edwards-Hovey Method for Cornet or Trumpet, Bk 2
Austyn R. Edwards (Author), Nilo W. Hovey  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769228679

Belwin Saxophone Method
Gekeler (Author), Kenneth (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222431

Belwin Clarinet Method, Book 2 
Gekeler (Author), Kenneth (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769224428

First Book of Practical Studies for Saxophone
Hovey (Author), Nilo W.  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769221953

Second Book of Practical Studies for Saxophone
Hovey (Author), Nilo W.  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769226699

Second book of Practical Studies for Clarinet 
Hovey (Author), Nilo W.  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222578

Cimera - Hovey Method for Trombone and Baritone
Cimera (Author), Jaroslav (Author), Hovey (Author), Nilo W.  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769226019

T-I-P-P-S for Band: E-Flat Alto Saxophone
Nilo W. Hovey  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769221700

Belwin Elementary Band Method: Trombone (Bass Clef)
Fred Weber (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222073

Belwin Flute Method Book Two
Gekeler (Author), Kenneth (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222400

Edwards-Hovey Method for Cornet or Trumpet
Edwards (Author), Austyn R. (Author), Hovey (Author), Nilo W.  (Author)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-13: 978-0769222158

Belwin Elementary Band Method
Weber (Author), Fred (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Music (March 1, 1985)
Language: English
ISBN-10: 0769259316
ISBN-13: 978-0769259314

Belwin Oboe Method, Bk 1
Kenneth Gekeler  (Author), Nilo W. Hovey (Editor)
Publisher: Alfred Publishing (March 1, 1985)
Language: English
ISBN-13: 978-0769224190

Belwin Intermediate Band Method Bb Cornet (Trumpet)
Fred Weber  (Author), Nilo W. Hovey (Editor)
Publisher: Belwin, Inc. (1947)
ASIN: B002QWDVR4

Advanced Technique for Bands: Trumpet
Nilo W. Hovey  (Author)
Publisher: M.M. Cole Publishing Co. (1980)
ISBN-13: 978-0847102020

Advanced Technique for Bands: Alto Saxophone
Nilo W. Hovey  (Author)
Publisher: M.M. Cole Publishing Co. (1980)ISBN-13: 978-0847101993

Practical Studies for Cornet and Trumpet, Book II 
Robert W. Getchell  (Author), Nilo W. Hovey  (Author)
Publisher: BELWIN MILLS PUBL (1985)
ASIN: B0052GF3AE
-->

=== Chamber Compositions ===

* ''Aria Cantando, for clarinet and piano (1948, with Beldon Leonard)''
* ''Chanson Moderne, for clarinet and piano (1948, with Beldon Leonard)''
* ''Clouds in Summer, for clarinet and piano (1948, with Beldon Leonard)''
* ''Gypsy Moods, for clarinet and piano (1948, with Beldon Leonard)''
* ''Solo Semplice, for clarinet and piano (1948, with Beldon Leonard)''
* ''Valse Grazioso, for clarinet and piano (1948, with Beldon Leonard)''
* ''Song of Spring, for clarinet and piano (1962, with Beldon Leonard)''
* ''The Carnival of Venice, for clarinet and piano (1964, with Beldon Leonard)''
* ''Bagatelle, for clarinet and piano (1965, with Beldon Leonard)''
* ''Andante and Waltz, for clarinet and piano (1968, with Beldon Leonard)''
* ''Caprice, for clarinet and piano (1968, with Beldon Leonard)''
* ''Contra Danse, for clarinet and piano (1968, with Beldon Leonard)''
* ''Il Primo Canto, for clarinet and piano (1968, with Beldon Leonard)''
* ''In the Minor Mode, for clarinet and piano (1970, with Beldon Leonard)''
* ''Minuetto, for clarinet and piano (1970, with Beldon Leonard)''
* ''Reflections, for clarinet and piano (1970, with Beldon Leonard)''
* ''Concert Piece, for clarinet and piano (1973, with Beldon Leonard)''<ref name="sm"/>

==References==
{{reflist}}

{{DEFAULTSORT:Hovey, Nilo}}
[[Category:American composers]]
[[Category:1906 births]]
[[Category:1986 deaths]]