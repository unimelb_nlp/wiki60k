{{Good article}}

{{Infobox royalty
| name          = Bohemond III
| title         = 
| image         = Bohemond III.jpg 
| caption       = Bohemond in Jerusalem
| succession    = Prince of Antioch
| reign         = 1163–1201
| coronation    = 
| cor-type      =
| predecessor   = [[Constance of Antioch|Constance]]
| successor     = [[Bohemond IV of Antioch|Bohemond IV]]
| regent        = 
| spouse        = Orgueilleuse of Harenc<br/>[[Theodora Komnene, Princess of Antioch|Theodora]]<br/>Sybil
| issue         = [[Raymond IV of Tripoli|Raymond]]<br/>[[Bohemond IV of Antioch]]<br/>William
| house         = [[House of Poitiers]]
| house-type    =
| father        = [[Raymond of Poitiers]]
| mother        = [[Constance of Antioch]]
| birth_date    = {{circa}} 1148
| birth_place   = 
| death_date    = April {{Death year and age|1201|1148}}
| death_place   = 
| burial_place  =
| religion      = [[Catholicism]]
| signature     =
}}

'''Bohemond III of Antioch''', also known as '''Bohemond the Child''' or '''the Stammerer''' ({{lang-fr|Bohémond le Bambe ''or'' le Baube}}; {{circa}}{{nbsp}}1148–1201), was [[Prince of Antioch]] from 1163 to 1201. He was the elder son of [[Constance of Antioch]] and her first husband, [[Raymond of Poitiers]]. Bohemond ascended to the throne after the Antiochene noblemen dethroned his mother with the assistance of [[Thoros II, Prince of Armenia|Thoros II]], [[Cilician Armenia|Lord of Armenian Cilicia]]. He fell into captivity in the [[Battle of Harim|Battle of Artah]] in 1164, but the victorious [[Nur ad-Din, atabeg of Aleppo]] released him to avoid coming into conflict with the [[Byzantine Empire]]. Bohemond went to [[Constantinople]] to pay homage to [[Manuel I Komnenos]], who persuaded him to install a [[Eastern Orthodox Patriarch of Antioch|Greek Orthodox Patriarch]] in Antioch. The [[Latin Patriarch of Antioch]], [[Aimery of Limoges]], placed Antioch under [[interdict]]. Bohemond restored Aimery only after the Greek patriarch died during an earthquake in 1170.

Bohemond remained a close ally of the Byzantine Empire. He fought against [[Mleh, Prince of Armenia|Mleh]], Lord of Armenian Cilicia, assisting in the restoration of Byzantine rule in the Cilician plain. He also made alliances with the Muslim rulers of [[Aleppo]] and [[Damascus]] against [[Saladin]], who had begun to unite the Muslim countries along the borders of the [[crusader states]]. Since Bohemond repudiated his second wife and married an Antiochene lady, Patriarch Aimery excommunicated him in 1180.

Bohemond forced the Armenian rulers of [[Kingdom of Cilician Armenia|Cilicia]] to accept his suzerainty in the late 1180s. He also secured the [[County of Tripoli]] for his second son, [[Bohemond IV of Antioch|Bohemond]], in 1187. However, Saladin occupied almost the whole [[Principality of Antioch]] in the summer of 1188. To preserve the peace with Saladin, Bohemond did not provide military assistance to the crusaders during the [[Third Crusade]]. [[Leo I, King of Armenia|Leo of Cilicia]]'s expansionist policy in the 1190s gave rise to a lasting conflict between Antioch and Cilicia. Bohemond was captured in 1194 by Leo, who tried to seize Antioch, but the burghers formed a [[Medieval commune|commune]] and expelled the Armenian soldiers from the town. Bohemond was released only after he acknowledged Leo's independence.

New conflicts emerged after Bohemond's eldest son, [[Raymond IV of Tripoli|Raymond]], died in 1197. Raymond's widow, who was Leo's niece, gave birth to a [[Posthumous birth|posthumous son]], [[Raymond-Roupen]], but Bohemond's younger son, Bohemond of Tripoli, wanted to secure his succession in Antioch with the assistance of the commune. The elderly Bohemond seems to have supported his son during his last years. The [[War of the Antiochene Succession]] began with Bohemond's death and lasted until 1219.

== Early life ==

[[File:RecoveryOfRaymondOfPoitiersBody.jpg|thumb|right|Recovery of the body of Bohemond's father, [[Raymond of Poitiers]], after the [[Battle of Inab]]]]

Bohemond was the elder son of [[Constance of Antioch|Constance]], [[Prince of Antioch|Princess of Antioch]], and her first husband, [[Raymond of Poitiers]].{{sfn|Runciman|1989a|pp=183–184, 330, Appendix III (Genealogical tree No. 2.)}} He was born around 1148.{{sfn|Burgtorf|2016|p=197}} Prince Raymond died fighting against [[Nur ad-Din, atabeg of Aleppo]], in the [[Battle of Inab]] on 29{{nbsp}}June 1149.{{sfn|Lock|2006|p=50}}{{sfn|Barber|2012|p=193}}

Neither [[Baldwin III of Jerusalem]] nor the [[Byzantine Emperor]] [[Manuel I Komnenos]] could persuade the widowed Constance to take a new husband.{{sfn|Barber|2012|p=199}}{{sfn|Runciman|1989a|pp=331–333}} Finally, she chose [[Raynald of Châtillon]], a French knight who had recently settled in Syria.{{sfn|Baldwin|1969|p=540}}{{sfn|Barber|2012|p=206}} Raynald ruled the principality as Constance's husband from 1153 until he was captured by Majd al-Din, governor of Aleppo, in late November 1160 or 1161.{{sfn|Lock|2006|p=55}}{{sfn|Barber|2012|pp=206, 214–215}}

Urged by the Antiochene noblemen, Baldwin III proclaimed Bohemond the rightful ruler, charging [[Aimery of Limoges]], [[Latin Patriarch of Antioch]], with the administration of the principality during Bohemond's minority.{{sfn|Runciman|1989a|p=358}} However, Constance appealed to Manuel Komnenos, who confirmed her position as the sole ruler of Antioch.{{sfn|Runciman|1989a|p=360}} Constance wanted to retain power even after Bohemond reached the age of majority.{{sfn|Runciman|1989a|p=364}} However, the Antiochene noblemen rebelled against her with the assistance of [[Thoros II, Prince of Armenia|Thoros II]], [[Cilician Armenia|Lord of Armenian Cilicia]], forcing her to leave Antioch in February 1163.{{sfn|Lock|2006|p=56}}

== Prince of Antioch ==

=== First years ===

[[File:Map Crusader states 1165-en.svg|thumb|right|The [[crusader states]] around 1165]]

Bohemond was installed as prince after his mother was dethroned.{{sfn|Lock|2006|p=56}}{{sfn|Runciman|1989a|p=365}} Nur ad-Din laid siege to [[Krak des Chevaliers]] in the County of Tripoli in September 1163.{{sfn|Runciman|1989a|p=367}} [[Raymond III of Tripoli]] appealed to Bohemond for assistance.{{sfn|Runciman|1989a|p=367}} Bohemond and [[Constantine Kalamanos]], Byzantine governor of Cilicia, hurried to the castle.{{sfn|Runciman|1989a|p=367}} The united Christian armies defeated the besiegers in the [[Battle of al-Buqaia]].{{sfn|Runciman|1989a|p=367}}

[[Amalric of Jerusalem]] entrusted the government of the [[Kingdom of Jerusalem]] to Bohemond before departing for his campaign against Egypt{{sfn|Lock|2006|p=57}} in July 1164.{{sfn|Barber|2012|p=238}}{{sfn|Runciman|1989a|p=369}} Taking advantage of Bohemond's absence, Nur ad-Din attacked the fortress at Harenc in the Principality of Antioch (present-day [[Harem, Syria]]).{{sfn|Runciman|1989a|p=369}} Bohemond, Raymond{{nbsp}}III of Tripoli, Thoros{{nbsp}}II of Armenian Cilicia, and Constantine Kalamanos joined their forces and marched to Harenc, compelling Nur ad-Din to retreat.{{sfn|Runciman|1989a|p=369}}

Reynald of Saint-Valery, Lord of Harenc, tried to convince Bohemond not to pursue the enemy, but Bohemond did not follow his advice.{{sfn|Runciman|1989a|p=369}} The armies [[Battle of Harim|clashed at Artah]] on 10{{nbsp}}August.{{sfn|Barber|2012|p=240}} Nur ad-Din almost annihilated the Christian army.{{sfn|Barber|2012|p=240}}{{sfn|Riley-Smith|2005|p=105}} Most Christian commanders (including Bohemond) were captured.{{sfn|Barber|2012|p=240}}{{sfn|Baldwin|1969|p=551}} Two days later, Harenc fell to Nur ad-Din.{{sfn|Baldwin|1969|p=551}} Nur ad-Din took his prisoners to Aleppo.{{sfn|Runciman|1989a|p=369}}{{sfn|Riley-Smith|2005|p=105}} His advisors urged Nur ad-Din to proceed to Antioch, but he declined, fearing that an attack on Antioch could provoke Emperor Manuel into annexing the principality.{{sfn|Runciman|1989a|p=370}} Amalric of Jerusalem hurried to Antioch to start negotiations with Nur ad-Din.{{sfn|Runciman|1989a|p=370}} Before long, Nur ad-Din released Bohemond, along with Thoros of Cilicia, for a ransom because he regarded them as vassals of the Byzantine emperor.{{sfn|Runciman|1989a|p=370}}

{{quote|The Muslims advised [Nur ad-Din] to proceed to Antioch and seize it because it was devoid of defenders and fighting men to hold it, but he did not do so. He said, "The city is an easy matter but the citadel is strong. Perhaps they will surrender it to the Byzantine emperor because its ruler is his nephew. To have Bohemond as a neighbor I find preferable to being a neighbour of the ruler of the Constantinople." He sent out squadrons in those areas and they plundered, seized and killed the inhabitants. Later he ransomed Prince Bohemond for a large sum of money and the release of many Muslim captives.|[[Ali ibn al-Athir]]: ''[[The Complete History]]''<ref>''The Chronicle of Ibn Al-Athir for the Crusading Period from'' Al-Kamil Fi'l-Ta'rikh (The year 559)), p. 148.</ref>}}

=== Byzantine alliance ===

Soon after his release, Bohemond visited Emperor Manuel in [[Constantinople]] and paid homage to him.{{sfn|Runciman|1989a|p=371}}{{sfn|Hamilton|2000|p=66}} In return for monetary aid, Bohemond agreed to allow Athanasius, the [[Eastern Orthodox Patriarch of Antioch]], to accompany him back to Antioch.{{sfn|Runciman|1989a|p=371}} The Latin Patriarch, Aimery, left Antioch and imposed an [[interdict]] on the city.{{sfn|Runciman|1989a|p=371}}{{sfn|Barber|2012|p=242}} Manuel's cousin, [[Andronicus I Comnenus|Andronicus Komnenus]], who was made Byzantine governor of Cilicia in 1166, often visited Antioch to meet Bohemond's beautiful young sister, [[Philippa of Antioch|Philippa]].{{sfn|Runciman|1989a|p=378}} Bohemond appealed to Manuel, who dismissed Andronicus, replacing him with Constantine Kalamanos.{{sfn|Runciman|1989a|p=378}}

Bohemond granted [[Apamea, Syria|Apamea]] to the [[Knights Hospitaller]] in 1168.{{sfn|Hamilton|2000|p=56}} An earthquake destroyed most towns of northern Syria on 29{{nbsp}}June 1170.{{sfn|Runciman|1989a|p=389}} The Greek Patriarch, Athanasius, died when the edifice of the Cathedral of St.{{nbsp}}Peter collapsed on him during the [[Mass]].{{sfn|Runciman|1989a|p=389}} Bohemond went to Qosair (present-day [[Al-Qusayr, Syria]]) and persuaded the exiled Latin Patriarch to return to his [[Episcopal see|see]].{{sfn|Runciman|1989a|p=389}}

[[Mleh of Armenia|Mleh]], who had seized Cilicia with Nur ad-Din's help, besieged [[Bagras]], the fortress of the [[Knights Templars]] near Antioch, in early 1170.{{sfn|Runciman|1989a|pp=389–390}} Bohemond sought assistance from Amalric of Jerusalem, and their united army defeated Mleh, also forcing him to restore the towns of the Cilician plains to the Byzantine Empire.{{sfn|Runciman|1989a|p=390}} Bohemond's relationship with Armenian Cilicia remained tense, which prevented him from pursuing an active foreign policy until Mleh was dethroned in 1175.{{sfn|Hamilton|2000|p=103}}

[[File:Boh3 ray3.jpg|thumb|left|Bohemond and [[Raymond III of Tripoli]] ride to Jerusalem.]]

Bohemond concluded an alliance with Gumushtekin, atabeg of Aleppo, against [[Saladin]], the [[Ayyubid dynasty|Ayyubid]] ruler of Egypt and Syria, in May 1176.{{sfn|Hamilton|2000|p=103}}{{sfn|Lock|2006|p=63}} On Bohemond's demand, Gumushtekin released his Christian prisoners, including Bohemond's stepfather, [[Raynald of Châtillon]].{{sfn|Hamilton|2000|p=103}}{{sfn|Lock|2006|p=63}} To strengthen his alliance with the Byzantine Empire, in 1177 Bohemond married [[Theodora Komnene, Princess of Antioch|Theodora]], who was closely related to Emperor Manuel.{{sfn|Hamilton|2000|p=114}}{{sfn|Runciman|1989a|p=419}}

Bohemond met [[Philip, Count of Flanders]], who had come to the Kingdom of Jerusalem in September 1177.{{sfn|Hamilton|2000|p=128}}{{sfn|Runciman|1989a|p=414}} According to the contemporaneous [[William of Tyre]], many crusaders blamed Bohemond and Raymond{{nbsp}}III of Tripoli for dissuading Philip from participating in a military campaign against Egypt, preferring instead to take advantage of Philip's presence in their own realms.{{sfn|Hamilton|2000|p=128}} Indeed, in December Philip and Bohemond jointly laid siege to Harenc, a fortress of [[As-Salih Ismail al-Malik]], [[Emir of Damascus]], seizing the opportunity following a mutiny of the garrison.{{sfn|Runciman|1989a|p=416}}{{sfn|Hamilton|2000|p=136}} They lifted the siege soon after As-Salih informed them that Saladin (the common enemy of both As-Salih and Bohemond) had left Egypt for Syria.{{sfn|Runciman|1989a|p=416}} As-Salih paid 50,000 dinars and renounced half of the nearby villages in favor of Bohemond.{{sfn|Hamilton|2000|p=137}}

Bohemond and Raymond III of Tripoli marched to the Kingdom of Jerusalem in early 1180, according to William of Tyre.{{sfn|Hamilton|2000|pp=151, 154}} [[Baldwin IV of Jerusalem]] feared that the two princes (who were his father's cousins) had come to dethrone him, the symptoms of his [[leprosy]] having become "more and more evident" by that time.{{sfn|Hamilton|2000|p=152}} Historian Bernard Hamilton, who accepts William of Tyre's narration, says that Bohemond and Raymond came to Jerusalem to choose a husband for Baldwin's sister and heir, [[Sibylla, Queen of Jerusalem|Sibylla]], wishing to decrease the influence of the king's maternal relatives.{{sfn|Hamilton|2000|p=154}} However, Baldwin gave her in marriage to [[Guy of Lusignan]], who was supported by their mother, [[Agnes of Courtenay]].{{sfn|Baldwin|1969|pp=596–597}} Sybilla's marriage contributed to the formation of two parties of noblemen.{{sfn|Barber|2012|p=275}} Bohemond, Raymond{{nbsp}}III of Tripoli, and the [[House of Ibelin|Ibelin brothers]] became the leaders of the group that opposed Guy of Lusignan.{{sfn|Barber|2012|p=275}}

=== Conflicts ===

[[File:BohemondIII.jpg|thumb|right|alt=An old coin depicting a head on one side and a cross on the other side |A coin of Bohemond III:<br>+BOAMVNDVS +ANTIOCHIA]]

Manuel I Komnenos died on 24 September 1180.{{sfn|Barber|2012|p=276}} Bohemond soon repudiated his wife, Theodora, to marry an Antiochene lady of bad reputation, Sybil.{{sfn|Runciman|1989a|p=429}} [[Ali ibn al-Athir]] described Sybil as a spy who was "in correspondence with Saladin and exchanged gifts with him".<ref>''The Chronicle of Ibn Al-Athir for the Crusading Period from'' Al-Kamil Fi'l-Ta'rikh (The year 584)), p. 352.</ref>{{sfn|Hamilton|2000|p=165}} Patriarch Aimery accused Bohemond of [[adultery]] and excommunicated him.{{sfn|Hamilton|2000|p=165}}{{sfn|Runciman|1989a|p=429}} After Bohemond confiscated church property, Aimery imposed an interdict on Antioch and fled to his fortress at Qosair.{{sfn|Hamilton|2000|p=165}}{{sfn|Runciman|1989a|p=429}} Bohemond besieged the fortress, but Reynald Masoir, Lord of Margat, and other noblemen who supported the patriarch rose up against him.{{sfn|Hamilton|2000|p=165}}

Baldwin IV sent [[Patriarch Heraclius of Jerusalem|Heraclius]], [[Latin Patriarch of Jerusalem|Patriarch of Jerusalem]], along with other bishops, and Raynald of Châtillon to Antioch to mediate.{{sfn|Hamilton|2000|p=165}}{{sfn|Runciman|1989a|p=430}} After preparatory negotiations with the envoys in [[Latakia]], Bohemond and Aimery met in Antioch.{{sfn|Hamilton|2000|p=165}} Bohemond agreed to restore confiscated church property and Aimery lifted the interdict, but Bohemond's excommunication remained in force because he refused to return to Theodora.{{sfn|Runciman|1989a|p=430}}{{sfn|Hamilton|2000|p=166}} Peace was not fully restored, and the leaders of the opposition fled to Armenian Cilicia.{{sfn|Hamilton|2000|p=166}}

Bohemond made peace with Imad ad-Din Zengi II, the [[Zengid dynasty|Zengid]] ruler of Aleppo, in May 1182.{{sfn|Hamilton|2000|p=166}} However, Imad ad-Din was forced to surrender Aleppo to Saladin on 11{{nbsp}}June 1183.{{sfn|Barber|2012|p=280}} Fearing an attack on Antioch, Bohemond sold [[Tarsus, Mersin|Tarsus]] to [[Ruben III, Prince of Armenia|Roupen III]], Lord of Armenian Cilicia, to raise funds.{{sfn|Hamilton|2000|p=188}} Baldwin{{nbsp}}IV of Jerusalem promised to send 300 knights to Antioch.{{sfn|Barber|2012|p=280}} Saladin did not invade the principality and signed a peace treaty with Bohemond.{{sfn|Barber|2012|p=280}} Bohemond attended the assembly that Baldwin{{nbsp}}IV had summoned to discuss the administration of the Kingdom of Jerusalem in autumn 1183.{{sfn|Hamilton|2000|p=194}} At the meeting, Guy of Lusignan was dismissed as regent, and his five-year-old stepson, Baldwin, was proclaimed co-ruler.{{sfn|Hamilton|2000|p=194}}{{sfn|Barber|2012|p=282}} A charter shows that Bohemond was in [[Acre]] in April 1185, suggesting that he was present when the leper Baldwin{{nbsp}}IV died around that time.{{sfn|Hamilton|2000|p=209 (note 81)}}

Roupen III of Armenian Cilicia laid siege to Lampron, the seat of his rival, Hethum III of Lampron.{{sfn|Der Nersessian|1969|p=644}} Hethum sent envoys to Bohemond, seeking his assistance.{{sfn|Der Nersessian|1969|p=644}} Bohemond invited Roupen to a banquet to Antioch where he had Roupen captured and imprisoned in 1185.{{sfn|Der Nersessian|1969|p=644}}{{sfn|Runciman|1989a|p=430}} Bohemond invaded Cilicia, but he could not prevent Roupen's brother, [[Leo I, King of Armenia|Leo]], from seizing Lampron.{{sfn|Der Nersessian|1969|p=644}} An Armenian nobleman, Pagouran of Barbaron, medieted a peace treaty.{{sfn|Der Nersessian|1969|p=644}} Roupen agreed to pay a ransom and to renounce [[Sarventikar]], [[Toprakkale, Osmaniye|Tall Hamdun]], [[Mamistra]], and [[Adana]].{{sfn|Der Nersessian|1969|p=644}} He also acknowledged Bohemond's suzerainty.{{sfn|Burgtorf|2016|p=198}} After the ransom was paid in 1186, Bohemond released Roupen, who soon reconquered the fortresses and towns that he had ceded to Antioch.{{sfn|Der Nersessian|1969|p=644}}

=== Saladin's triumph ===

[[File:Baghras1.jpg|thumb|right|alt=A ruined building made of stone with bushes growing from the ruins on a cliff|Ruins of [[Bagras]]]]

The child Baldwin V of Jerusalem died in late summer 1186.{{sfn|Lock|2006|p=70}} Raymond of Tripoli and his supporters could not prevent Baldwin{{nbsp}}V's mother, Sybilla, and her husband, Guy of Lusignan, from seizing the throne.{{sfn|Lock|2006|p=70}} [[Baldwin of Ibelin]], who was the only Jerusalemite baron to refuse to pay homage to Sybilla and Guy after their coronation, moved to Antioch.{{sfn|Hamilton|2000|p=223)}} Bohemond granted a fief to him.{{sfn|Runciman|1989a|pp=449–450}}

Nomad [[Syrian Turkmen|Turkmen]] bands invaded Cilicia,{{sfn|Der Nersessian|1969|p=644}} forcing the new ruler, Leo, to swear fealty to Bohemond shortly after his ascension in 1186 or 1187.{{sfn|Runciman|1989b|p=87}} The Turkmens also broke into the Principality of Antioch, pillaging the lowlands around Latakia and the monasteries in the nearby mountains.{{sfn|Hamilton|2000|p=229}} Bohemond was forced to make a truce with [[Al-Muzaffar Umar]], Saladin's governor in Syria, who joined Saladin's invasion of the Kingdom of Jerusalem in May.{{sfn|Hamilton|2000|p=229}} Even so, Bohemond sent 50 knights under the command of his elder son, [[Raymond IV, Count of Tripoli|Raymond]], to Jerusalem after a Christian army was almost annihilated in the [[Battle of Cresson]].{{sfn|Hamilton|2000|p=229}}{{sfn|Barber|2012|pp=298–299}} The Turkmens continued their plundering raid until the Antiochene army defeated them and seized their booty.{{sfn|Hamilton|2000|p=229}}

Saladin launched a crushing defeat on the Christian army in the Battle of Hattin on 4{{nbsp}}July 1187.{{sfn|Lock|2006|p=71}} Bohemond's son was one of the few Christian leaders to flee from the battlefield.{{sfn|Barber|2012|p=303}} Within three months, Saladin captured almost all towns and fortresses of the Kingdom of Jerusalem.{{sfn|Lock|2006|p=71}} Raymond{{nbsp}}III of Tripoli, who died before the end of the year, willed the [[County of Tripoli]] to Bohemond's elder son and heir, Raymond.{{sfn|Lock|2006|p=72}} Bohemond sent his younger son and namesake to take control of Tripoli, convinced that one ruler could not defend both Antioch and Tripoli.{{sfn|Lock|2006|p=72}}{{sfn|Runciman|1989a|p=470}} After his son was installed in Tripoli, Bohemond became "the greatest of the Franks and their most extensive ruler",<ref>''The Chronicle of Ibn Al-Athir for the Crusading Period from'' Al-Kamil Fi'l-Ta'rikh (The year 584)), p. 353.</ref> according to Ibn Al-Athir.{{sfn|Barber|2012|p=322}} Bohemond offered to pay homage to [[William II of Sicily]] in exchange for military assistance.{{sfn|Burgtorf|2016|p=198}}{{sfn|Van Tricht|2011|p=434}}

Saladin started the invasion of northern Syria on 1{{nbsp}}July 1188.{{sfn|Runciman|1989a|p=470}} His troops captured Latakia on 22 or 23{{nbsp}}July, Sahyun six days later, and the fortresses along the Orontes River in August.{{sfn|Runciman|1989a|pp=470–471}}{{sfn|Barber|2012|pp=318–319}} After the Knights Templar surrendered their fortress at Bagras to Saladin on 26{{nbsp}}September, Bohemond pleaded for a truce, offering the release of his Muslim prisoners.{{sfn|Runciman|1989a|p=471}}{{sfn|Lock|2006|p=73}}{{sfn|Barber|2012|p=322}} Saladin granted the truce from 1{{nbsp}}October 1188 to 31{{nbsp}}May 1189.{{sfn|Barber|2012|p=322}} Bohemond managed to retain only his capital and the port of [[St Symeon]].{{sfn|Runciman|1989a|p=471}} Saladin stipulated that Antioch was to be surrendered without resistance if no reinforcements came before the end of May 1189.{{sfn|Barber|2012|p=322}} Bohemond urged the [[Holy Roman Emperor]], [[Frederick I Barbarossa]], to come to Syria, offering him the suzerainty over Antioch.{{sfn|Van Tricht|2011|p=434}}

{{Quote|This summer the unspeakable Saladin totally destroyed the city of [[Tartus|Tortosa]] except for the Templar citadel, burnt down the city of Valania before moving on to the region of Antioch where he claimed the famous cities of [[Jabala]] and Latakia, the stronholds of [[Citadel of Salah Ed-Din|Saône]], Gorda, Cavea and [Burzey] and the lands as far as Antioch. Beyond Antioch he besieged and captured [[Trapessac|Darbsak]] and [Bagras]. Thus, with the whole of the principality apart from our stronghold at [[Margat]], more or less destroyed and lost, the prince and the people of Antioch made a pitiful agreement with Saladin, that if no help was forthcoming in the seven months from the beginning of that month of October they would formally surrender Antioch, alas without even a stone being thrown, a [[Siege of Antioch|city acquired with the blood of valiant Christians]].|''Letter'' by Armengarde of Aspe, Master of the Hospital, to [[Leopold V, Duke of Austria]] (November 1188)<ref>''Letters from the East: Crusaders, Pilgrims and Settlers in the 12th–13th Centuries'' (Letter No. 48)), p. 86.</ref>}}

=== Third crusade ===

{{Main article|Third Crusade}}

[[File:Map Crusader states 1190-en.svg|thumb|right|The [[Crusader states]] around 1190]]

Guy of Lusignan, who had recently been released, came to Antioch in July or August 1188.{{sfn|Runciman|1989b|p=21}} Bohemond did not provide him with military assistance, and Guy left for Tripoli.{{sfn|Runciman|1989b|p=21}}

Frederick Barbarossa departed from the Holy Roman Empire in May 1189.{{sfn|Runciman|1989b|p=11}} The defence of Antioch was a principal aim of his crusade,{{sfn|Van Tricht|2011|p=434}} but he died unexpectedly near Seleucia in [[Asia Minor]] (present-day [[Silifke]] in Turkey) on 10{{nbsp}}June 1190.{{sfn|Runciman|1989b|p=15}} His son, [[Frederick VI, Duke of Swabia]], took over the command of the army, but most crusaders decided to return to Europe.{{sfn|Runciman|1989b|p=16}} The remnants of the German crusaders reached Antioch on 21{{nbsp}}June 1190.{{sfn|Runciman|1989b|p=17}} Bohemond paid homage to Frederick of Swabia.{{sfn|Burgtorf|2016|p=198}}{{sfn|Runciman|1989b|pp=16–17}}{{sfn|Barber|2012|p=328}} Barbarossa's body, which had been carried to Antioch, was buried in the cathedral before the duke continued his crusade toward the Holy Land.{{sfn|Runciman|1989b|p=17}}

In May 1191 Bohemond sailed to [[Limassol]] along with Guy of Lusignan and Leo of Cilicia to meet [[Richard I of England]], who had arrived to reconquer the Holy Land from Saladin.{{sfn|Runciman|1989b|p=44}} He once again met Richard during the [[Siege of Acre (1189–91)|siege of Acre]] in summer 1191, but he did not provide military support to the crusaders.{{sfn|Barber|2012|p=354}} Bohemond's relationship with Leo of Cilicia became tense when Leo captured Bagras and refused to cede it to the Knights Templar.{{sfn|Lock|2006|p=76}}

After Richard of England left the Holy Land, Bohemond met Saladin in [[Beirut]] on 30{{nbsp}}October 1192.{{sfn|Lock|2006|p=76}}{{sfn|Barber|2012|p=354}} According to Ibn Al-Athir, Bohemond "did obeisance" and Saladin "bestowed a [[robe of honour]] upon him"<ref>''The Chronicle of Ibn Al-Athir for the Crusading Period from'' Al-Kamil Fi'l-Ta'rikh (The year 588)), p. 402.</ref> at their meeting.{{sfn|Barber|2012|p=354}} They signed a ten-year truce that included both Antioch and Tripoli but did not cover Armenian Cilicia even though Leo of Cilicia was Bohemond's vassal.{{sfn|Lock|2006|p=79}}

=== Last years ===

Bohemond's wife, Sybil, wanted to secure Antioch for her son, William, with the assistance of Leo of Cilicia (whose wife, Isabel, was her niece).{{sfn|Burgtorf|2016|pp=198–199}}{{sfn|Runciman|1989b|p=87}} Leo invited Bohemond and his family to Bagras, saying that he wanted to start negotiations regarding the surrender of the fortress either to Antioch or to the Templars in early 1194.{{sfn|Lock|2006|p=79}}{{sfn|Burgtorf|2016|p=199}} The meeting was a trap: Bohemond was captured and taken to Leo's capital, [[Sis (ancient city)|Sis]].{{sfn|Burgtorf|2016|p=199}}{{sfn|Hardwicke|1969|p=527}}

[[File:Armoiries Boh&#xE9;mond VI d'Antioche.svg|thumb|left|100px|Coat-of-Arms of Poitiers of Antioch]]

Bohemond was compelled to surrender Antioch to Leo.{{sfn|Runciman|1989b|p=87}} He appointed [[Marshal of Antioch|his marshal]], Bartholomew Tirel, to accompany the Armenian troops, which were under the command of Hethoum of Sason, to Antioch.{{sfn|Hardwicke|1969|p=527}}{{sfn|Runciman|1989b|p=87}} The Antiochene noblemen allowed Leo's soldiers to enter the town, but the mainly Greek and Latin burgers opposed Leo's rule.{{sfn|Burgtorf|2016|p=199}}{{sfn|Runciman|1989b|p=87}} An Armenian soldier's rude remark about [[Hilary of Arles|Saint Hilary]], to whom the royal chapel was dedicated, provoked a riot, forcing the Armenians to withdraw from the town.{{sfn|Runciman|1989b|p=87}} The burghers assembled in the cathedral to form a [[Medieval commune|commune]] under the auspices of Patriarch Aimery.{{sfn|Runciman|1989b|p=87}} They declared Bohemond's eldest son, Raymond, regent for his imprisoned father.{{sfn|Runciman|1989b|p=87}} Raymond's younger brother, Bohemond, also hurried from Tripoli to Antioch, and the Armenian forces had to return to Cilicia.{{sfn|Runciman|1989b|p=89}}

[[Henry I of Jerusalem]] came to Antioch to mediate a peace treaty in early 1195.{{sfn|Hardwicke|1969|p=527}}{{sfn|Runciman|1989b|p=89}} After Bohemond renounced his claim to suzerainty over Cilicia and acknowledged Leo's possession of Bagras, Leo released him and his retainers.{{sfn|Hardwicke|1969|p=527}}{{sfn|Runciman|1989b|p=89}} Before long, Bohemond's son, Raymond, married Leo's niece and heir, [[Alice of Armenia|Alice]].{{sfn|Burgtorf|2016|p=199}}

Raymond died in early 1197, but his widow gave birth to a [[Posthumous birth|posthumous son]], [[Raymond-Roupen]].{{sfn|Burgtorf|2016|p=199}}{{sfn|Runciman|1989b|p=99}} The elderly Bohemond sent her and her infant son to Cilicia wanting either to secure Antioch for his son by Sybil, or to guarantee their security.{{sfn|Runciman|1989b|p=99}} Bohemond assisted [[Henry I, Duke of Brabant]] in capturing Beirut in October 1197.{{sfn|Runciman|1989b|pp=96, 99}} Before long, he decided to besiege Jabala and Latakia, but he had to return to Antioch to meet the papal legate, [[Conrad of Wittelsbach]], [[Archbishop of Mainz]].{{sfn|Runciman|1989b|p=99}} The archbishop had come to Antioch to secure Raymond-Roupen's right to succeed Bohemond.{{sfn|Runciman|1989b|p=99}} On Conrad's demand, Bohemond summoned the Antiochene noblemen, who swore fealty to his grandson.{{sfn|Runciman|1989b|p=99}}

Bohemond of Tripoli regarded himself his father's lawful heir, because he was Bohemond's elder surviving son.{{sfn|Burgtorf|2016|p=199}} He came to Antioch at the end of 1198 and persuaded the commune to accept his rule.{{sfn|Runciman|1989b|p=100}}{{sfn|Riley-Smith|2005|p=106}} Before long, the younger Bohemond returned to Tripoli, enabling his father to re-take control of state affairs, suggesting that the elder Bohemond had tacitly supported his son's [[Coup d'état|coup]].{{sfn|Burgtorf|2016|p=199}}{{sfn|Runciman|1989b|p=100}} Leo{{nbsp}}I of Cilicia appealed to the [[Holy See]] to protect Raymond-Roupen's interest, but the Knights Templar submitted a complaint against him for refusing to restore Bagras to them.{{sfn|Burgtorf|2016|p=199}}{{sfn|Runciman|1989b|p=100}}

Bohemond died in April 1201.{{sfn|Burgtorf|2016|p=200}} His son hurried to Antioch to attend his funeral.{{sfn|Burgtorf|2016|p=200}} The commune proclaimed him prince, but many noblemen who remained loyal to Raymond-Roupen fled to Cilicia.{{sfn|Burgtorf|2016|p=200}} The ensuing [[War of the Antiochene Succession]] lasted for years, until the death of Leo in May 1219.{{sfn|Burgtorf|2016|pp=200, 203}}

== Family ==

{{ahnentafel top|width=100%|Ancestors of Bohemond III of Antioch{{sfn|Runciman|1989a|p=330, Appendix III (Genealogical trees No. 1-2.)}}{{sfn|Dunbabin|2000|pp=384, 392}}}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Bohemond III of Antioch'''

|2= 2. [[Constance of Antioch]]
|3= 3. [[Raymond of Poitiers]]

|4= 4. [[Bohemond II of Antioch]]
|5= 5. [[Alice of Antioch|Alice of Jerusalem]]
|6= 6. [[William IX, Duke of Aquitaine]]
|7= 7. [[Philippa, Countess of Toulouse]]

|8= 8. [[Bohemond I of Antioch]]
|9= 9. [[Constance of France]]
|10= 10. [[Baldwin II of Jerusalem]] 
|11= 11. [[Morphia of Melitene]]
|12= 12. [[William VIII, Duke of Aquitaine]]
|13= 
|14= 14. [[William IV, Count of Toulouse]]
|15=

|16= 16. [[Robert Guiscard]] 
|17= 17. [[Alberada of Buonalbergo]]
|18= 18. [[Philip I of France]]
|19= 19. [[Bertha of Holland]]
|20= 20. [[Hugh I, Count of Rethel]]
|21= 21. Melisende of Montlhéry
|22= 22. [[Gabriel of Melitene]]
|23= 
|24= 24.  [[William V, Duke of Aquitaine]]
|25= 
|26= 
|27= 
|28= 28. [[Pons, Count of Toulouse]]
|29=
|30=
|31=
}}</center>
{{ahnentafel bottom}}

Bohemond's first wife, Orgueilleuse of Harenc, was first mentioned in charters issued in 1170, suggesting that Bohemond married her in or before that year.{{sfn|Barber|2012|p=418}}{{sfn|Hamilton|2000|p=114 (note 27)}} She was last mentioned in February or March 1175.{{sfn|Barber|2012|p=418}}{{sfn|Hamilton|2000|p=114 (note 27)}} She was the mother of Bohemond's two eldest sons, [[Raymond IV of Tripoli|Raymond]] and [[Bohemond IV of Antioch|Bohemond]].{{sfn|Runciman|1989a|p=470, Appendix III (Genealogical tree No. 2.)}}

Bohemond's second wife, [[Theodora Komnene, Princess of Antioch|Theodora]] (whom the ''[[Lignages d'Outremer]]'' mentioned as Irene) was a relative of the [[Byzantine Emperor]] [[Manuel I Komnenos]].{{sfn|Runciman|1989a|p=419 (note 2)}} Historian Charles M. Brand identifies her as the daughter of Manuel's nephew, [[John Doukas Komnenos]].{{sfn|Hamilton|2000|p=114}} According to the ''Lignages d'Outremer'', Theodora gave birth to a daughter, Constance, who was not mentioned in other sources.{{sfn|Runciman|1989a|p=419 (note 2)}}

William of Tyre described Sybil, the third wife of Bohemond, as a witch who "practised evil magics" to seduce Bohemond.{{sfn|Hamilton|2000|p=165}} [[Michael the Syrian]] stated that Sybil was a whore.{{sfn|Hamilton|2000|p=165}} Her sister was the wife of Bohemond's vassal, the lord of Burzey.{{sfn|Hamilton|2000|p=164}}{{sfn|Barber|2012|p=320}} Bohemond and Sybil's daughter, Alice, became the wife of the wealthy [[Guido I Embriaco|Guy I Embriaco, Lord of Jabala]].{{sfn|Runciman|1989b|p=361 (note 2), Appendix III (Genealogical tree No. 2.)}} William, the son of Bohemond and Sybil, may have been named for William II of Sicily.{{sfn|Burgtorf|2016|p=198}}

== References ==
{{Reflist|3}}

== Sources ==

=== Primary sources ===
{{Refbegin}}
* ''Letters from the East: Crusaders, Pilgrims and Settlers in the 12th–13th Centuries'' (Translated by Malcolm Barber and Keith Bate) (2010). Ashgate. ISBN 978-0-7546-6356-0.
* ''The Chronicle of Ibn Al-Athir for the Crusading Period from'' Al-Kamil Fi'l-Ta'rikh ''(Part{{nbsp}}2: The Years 541-582/1146-1193: The Age of Nur ad-Din and Saladin)'' (Translated by D.{{nbsp}}S. Richards) (2007). Ashgate. ISBN 978-0-7546-4078-3.
{{Refend}}

=== Secondary sources ===
{{Refbegin}}
* {{cite book |last=Baldwin |first=Marsall W. |editor1-last=Setton |editor1-first=Kenneth M. |editor2-last=Baldwin |editor2-first=Marshall W. |title=A History of the Crusades, Volume I: The First Hundred Years |publisher=The University of Wisconsin Press |year=1969 |pages=528–561 |chapter=The Latin States under Baldwin&nbsp;III and Amalric&nbsp;I, 1143–1174 |isbn=0-299-04844-6 |ref=harv}}
* {{cite book |last=Barber |first=Malcolm |author-link=Malcolm Barber |year=2012 |title=The Crusader States |publisher=Yale University Press |isbn=978-0-300-11312-9 |ref=harv}}
* {{cite book |last=Burgtorf |first=Jochen |editor-last=Boas |editor-first=Adrian J. |title=The Crusader World |publisher=The University of Wisconsin Press |year=2016 |pages=196–211 |chapter=The Antiochene war of succession |isbn=978-0-415-82494-1 |ref=harv}}
* {{cite book |last=Der Nersessian |first=Sirarpie |author-link=Sirarpie Der Nersessian |editor1-last=Setton |editor1-first=Kenneth M. |editor2-last=Wolff |editor2-first=Robert Lee |editor3-last=Hazard |editor3-first=Harry |title=A History of the Crusades, Volume II: The Later Crusades, 1189–1311 |publisher=The University of Wisconsin Press |year=1969 |pages=630–659 |chapter=The Kingdom of Cilician Armenia |isbn=0-299-04844-6 |ref=harv}}
* {{cite book |last=Dunbabin |first=Jean |author-link=Jean Dunbabin |year=2000 |title=France in the Making, 843-1180 |publisher=Oxford University Press |isbn=0-19-820846-4 |ref=harv}}
* {{cite book |last=Hamilton |url=https://books.google.co.il/books?id=IySQoHdviNkC&pg=PA165|first=Bernard |year=2000 |title=The Leper King and His Heirs: Baldwin IV and the Crusader Kingdom of Jerusalem |publisher=Cambridge University Press |isbn=978-0-521-64187-6 |ref=harv}}
* {{cite book |last=Hardwicke |first=Mary Nickerson |editor1-last=Setton |editor1-first=Kenneth M. |editor2-last=Wolff |editor2-first=Robert Lee |editor3-last=Hazard |editor3-first=Harry |title=A History of the Crusades, Volume II: The Later Crusades, 1189–1311 |publisher=The University of Wisconsin Press |year=1969 |pages=522–554 |chapter=The Crusader States, 1192–1243 |isbn=0-299-04844-6 |ref=harv}}
* {{cite book |last=Lock |first=Peter |year=2006 |title=The Routledge Companion to the Crusades |publisher=Routledge |isbn=978-0-415-39312-6 |ref=harv}}
* {{cite book |last=Riley-Smith |first=Jonathan Simon Christopher |year=2005 |title=The Crusades: A History |publisher=Continuum |isbn=0-8264-7269-9 |ref=harv}}
* {{cite book |last=Runciman |first=Steven |author-link=Steven Runciman |year=1989a |title=A History of the Crusades, Volume II: The Kingdom of Jerusalem and the Frankish East, 1100–1187 |publisher=Cambridge University Press |isbn=0-521-06163-6 |ref=harv}}
* {{cite book |last=Runciman |first=Steven |author-link=Steven Runciman |year=1989b |title=A History of the Crusades, Volume III: The Kingdom of Acre and the Later Crusades |publisher=Cambridge University Press |isbn=0-521-06163-6 |ref=harv}}
* {{cite book |last=Van Tricht |first=Filip |year=2011 |title=The Latin ''Renovatio'' of Byzantium: The Empire of Constantinople (1204–1228) |publisher=BRILL |isbn=978-90-04-20323-5 |ref=harv}}
{{Refend}}

== Further reading ==
{{Refbegin}}
* {{cite book |last=Richard |first=Jean |author-link=Jean Richard (historian) |year=1999 |title=The Crusades: c.&nbsp;1071–c.&nbsp;1291 |publisher=Cambridge University Press |isbn= 978-0-521-62566-1}}
{{Refend}}

== External links ==
{{Refbegin}}
* {{Cite web |author=The Editors of Encyclopædia Britannica |title=Bohemond III Prince of Antioch |publisher=Encyclopædia Britannica, Inc. |year=2016 |url=http://www.britannica.com/biography/Bohemond-III |access-date=25 April 2016}}
* {{Cite web |last=Cawley |first=Charles |title=Medieval Lands: A prosopography of medieval European noble and royal families; Antioch, Chapter&nbsp;2: Princes of Antioch 1136–1268 (Poitiers) |publisher=Foundation for Medieval Genealogy |date=30 May 2014 |url=http://fmg.ac/Projects/MedLands/ANTIOCH.htm |access-date=25 April 2016}}
{{Refend}}

{{S-start}}
{{s-hou|[[House of Poitiers]]| |1148|April|1201}}
{{s-reg|}}
{{s-bef|before=[[Constance of Antioch|Constance]]}} 
{{s-ttl|title=[[Prince of Antioch]]|years=1163–1201}} 
{{s-aft|after=[[Bohemond IV of Antioch|Bohemond IV]]}}
{{S-end}}

{{Antioch Monarchs}}

{{Authority control}}

{{DEFAULTSORT:Bohemond III of Antioch}}
[[Category:1148 births]]
[[Category:1201 deaths]]
[[Category:People excommunicated by the Roman Catholic Church]]
[[Category:Princes of Antioch]]
[[Category:House of Poitiers]]
[[Category:Medieval princes]]