{{Too few opinions|date=September 2008}}
'''Diaspora politics in the United States''' is the study of the [[Theories of political behavior|political behavior]] of transnational [[ethnic groups|ethnic]] [[diaspora]]s, their relationship with their ethnic [[homeland]]s and their host states, as well as their prominent role in ethnic conflicts.  This article describes case studies and theories of political scientists studying diaspora politics within the specific context of the United States.  The general study of [[diaspora politics]] is part of the broader field of [[diaspora studies]].

==Overview==
{{Main|Diaspora politics}}
To understand a diaspora's politics, one must first understand its historical context and attachments:<ref name=Ambrosio2002>[[Thomas Ambrosio|Ambrosio, Thomas]].  2002.  "Ethnic identity groups and U.S. foreign policy."  Praeger Publishers.  ISBN 0-275-97533-9</ref> A diaspora is a transnational community that defined itself as a singular [[ethnic group]] based upon its shared [[cultural identity|identity]].  Diasporas are created by a  forced or induced historical emigration from an original [[homeland]].  Diasporas place great importance on their homelands because of their long history and deep cultural association.  The importance of a homeland, especially if it has been lost, can result in an [[ethnic nationalism|ethnic nationalist movement]] within the diaspora, often resulting in the reestablishment of the homeland.  But even when homelands are established, it is rare for the complete diaspora population to migrate back to the homeland, leaving a remaining diaspora community which often retains significant emotional attachment to its foreign kin and homeland.

Ethnic diaspora communities are now recognized by scholars as "inevitable" and "endemic" features of the international system, writes [[Yossi Shain]] and [[Tamara Cofman Wittes]],<ref name=ShainWittes2002>[[Yossi Shain|Shain, Yossi]] & Tamara Cofman Wittes.  <u>Peace as a Three-Level Game: The Role of Diasporas in Conflict Resolution</u> in [[Thomas Ambrosio|Ambrosio, Thomas]].  2002.  "Ethnic identity groups and U.S. foreign policy."  Praeger Publishers.  ISBN 0-275-97533-9</ref> for the following reasons:
# First, within each of a diaspora's host states, resident members can organize domestically to maximize their political clout.
# Second, a diaspora can exert significant pressure in its homeland's domestic political arena regarding issues of diaspora concern.
# Lately, a diaspora's transnational community can engage directly with third-party states and international organizations, in effect bypassing its homeland and host state governments.

Diasporas are thus understood as transnational political entities, operating on "behalf of their entire people", and capable of acting independently from any individual state (be it their homeland or host states.)

==Political spheres==
Diaspora are politically active in three separate realms: their outsider influence on their homeland's domestic politics, the exercise of their domestic political rights within their host states, and their independent involvement at the international level.

===Homeland===

====Significance to the diaspora identity====
While all transnational diasporas retain objective components of a coherent ethnic identity such as a shared history and folkways such as food and music, in some cases, diasporas can share the objective reality of a territorial homeland.  When these ethnic homelands exist, they serve as "the physical embodiment", "a territorial, cultural and social focus for the ethnic identity of the diaspora community."  Shain writes:
:"In the homeland, the community's language is the language of daily interaction, and all the symbols of sovereignty - currency, stamps, military, flag, and the like -- are ingredients that reinforce the identity of the diaspora kin in ways similar to their functions in cultivating and sustaining the national identity of the homeland's citizens."<ref name=ShainWittes2002/>

Thus, from the perspective of the diaspora, the homeland's "political and territorial fate has profound implications."<ref name=ShainWittes2002/>

====Negotiating the national interest====
In international relations, it is assumed that a state, in order to act coherently in the international system, must identify what are termed its "[[national interest]]", its goals and ambitions in the economic, military or cultural domains.  The formulation of policy, both domestic and international, is then straightforward, it is simply the pursuit of the nation's identified national interest.

The national interest of a state usually derived from its closely linked national identity and national narrative.  For ethnic homelands with diasporas, there is conflict between the national identity of the homeland and the diaspora's ethnic identity—most obvious is the state's principal concern for only the people living within its boundaries, while the diaspora's is more broadly concerned for the transnational community.

While the homeland has the ability to independently formulate its national identity, narrative and interest, the homeland is highly motivated to accommodate, or at least appear to, the concerns of its ethnic diaspora because of "the diaspora's political clout and financial assistance, at home and internationally."<ref name=ShainWittes2002/>  Thus the homeland's formulations must accommodate the ethnic identity needs of the diaspora to allow for the homeland to retain its significance to them and thus their support.

Shain describes the negotiation process as:
:"Although national identity can be negotiated between homeland and diaspora, the structure of modern international relations give the prerogative of constituting, elaborating, and implementing the national interest to the government of the homeland state. [...]  [In] reality, neither the diaspora nor the homeland community ultimately dominates in constitute and communicating national identity. [...] The degree to which the one influences the other is associated with the relative strength that the homeland and the diaspora can exercise [[:wikt:vis-à-vis|vis-à-vis]] one another through monetary flows, cultural productions, community leadership, and the like."<ref name=ShainWittes2002/>

The conflict between the homeland's national identity and the diaspora's ethnic identity often results in the diaspora emphasizing different aspects of the national narrative allowing the diaspora to embrace a slightly different interpretation of the homeland's national interest than that of held by the homeland's citizens.  "A certain degree of flexibility can be preserved because of the distance between homeland and diaspora: each can, to a degree, put its own 'spin' on the national narrative and live out their shared identity in its own way."<ref name=ShainWittes2002/>  "Sufficient areas of overlap exist that homeland-diaspora ties can be quite close despite differences of emphasis in the national narrative."<ref name=ShainWittes2002/>

====Domestic activism====
Some diasporas have become significant players in the domestic circles of their homelands according to Shain and Wittes.<ref name=ShainWittes2002/>  Diasporas are vocal in their declarations of support for elected homeland politicians or in voicing their belief that certain [[politician]]s in their homeland may be "betraying the national causes" as they see it.  There have been mass demonstrations of support or opposition by diaspora communities in response to specific policy decisions by their homeland governments.  In addition, diasporas have targeted domestic [[public opinion]] in their homelands and its domestic political development via the use of "monetary contributions, affiliated political parties, and transnational communal organizations."

===Host states===
Diaspora communities can both influence the governments and public of their host countries, as well as have their social and political status in a host country affected by the polices of their homeland authorities.

====Lobbying for ethnic interests====
According to [[Thomas Ambrosio]],<ref name=Ambrosio2002/> "like other societal interest groups, ethnic identity groups establish formal organizations devoted to promoting group cohesiveness and addressing group concerns."  While many formal organizations established by ethnic identity groups are apolitical, others are created explicitly for political purposes.  In general, groups who seek to influence government policy on domestic or foreign issues are referred to as [[lobby groups]] or 'interest groups'.  Those groups established by ethnic identity groups are referred as to [[ethnic interest groups]].<ref name=Ambrosio2002/>

Homeland authorities can enlist diaspora communities to lobby their respective host governments on behalf of the homeland.<ref name=ShainWittes2002/>

====Potential liabilities====
These issues, can result in real danger to the local diaspora community.  May lead to racism directed towards the diaspora community, either directly or by being co-opted by opportunist extremists.  Diaspora communities are almost always [[minority group|minorities]] in their host states, and thus are at risk of [[xenophobia]] or [[persecution]] by other demographic groups in the host state.

=====Domestic accountability for foreign kin actions=====
Shain explains that "[when] kin states violate norms that are valued by the host state (such as, for Americans, democracy or human rights), diasporas are often implicated or held accountable morally and politically. [...] The [host state] government and perhaps even [its] public may expect diaspora leaders to persuade or pressure their homeland government to alter its policies in a more congenial direction" and the failure of the diaspora community to act as desired by the host state "can impinges on the diaspora's ability to achieve cherished political goals."<ref name=ShainWittes2002/>

Shain cites the situation of Arab-Americans as an example where diaspora members are held accountable and negatively impacted by the polices of foreign ethnic kin:
:"The violence sponsored by the Palestinian nationalist movement over many years, and Arab state' endorsement of that violence, severely hampered the ability of [[Arab-Americans]] general (and Palestinian-American more particularly) to integrate themselves into American electoral politics.  In 1999, a prominent Arab-American was removed from a government-convented panel examining U.S. [[counter-terrorism]] policy because of (perhaps prejudicially rooted) concerns about his attitude towards Arab terrorism against Israeli and American targets."<ref name=ShainWittes2002/>

=====Conflicting loyalties=====
Diaspora leaders can be presented with a dilemma of [[dual loyalty|dual loyalties]] when the interests of their homeland come in conflict with those of their host state.  This is most common, according to Shain, when the homeland is involved in a violent conflict or in negotiations to resolve such a conflict.

Shain describes one example:
:"[When] the Bush administration in 1991 threatened to withhold loan guarantees to Israel unless Israel agreed not to spend the money in the occupied West Bank and Gaza, Jewish-American advocacy organizations were forced to choose between their good relations with the U.S. foreign policy establishment and their loyal support of Israeli polices in its conflict with the Palestinians.  Most chose to support Israeli policy at the cost of incurring the wrath of their American partners.  But after the bilateral U.S.-Israel confrontation was resolved and the loan guarantees were put into place, many of those same organizations joined the effort to pressure the Israeli government to adopt a different attitude toward settlement activity in the West Bank and Gaza."<ref name=ShainWittes2002/>

===International===
A diaspora's transnational community can engage directly with third-party states and international organizations, in effect bypassing its homeland and host state governments.

====Bilateral relations====
Diasporas have, in addition to their domestic political involvement in the homeland and host states, also directly influenced bilateral [[international relations]] of states of concern.  In some cases, diasporas have appeared to "bypass" their own homeland traditional [[sovereignty]] over its own international relations via "privately funded activities, and by lobbying governments" of the diaspora host states as well as those of third-party states.<ref name=ShainWittes2002/>  Shain and Wittes cite the following as examples of international relations involvement:
* "[The] Armenian-American lobbying groups successfully pass a congressional ban on U.S. aid to [[Azerbaijan]] (known as Section 907 of the [[Freedom Support Act]]) that has withstood many years of White House efforts to have it overturned."
* "Jewish-American lobbying organizations have pressed for the United States to move its embassy in Israel from [[Tel Aviv]] to [[Jerusalem]], against the wishes of the U.S. administration and often those of the Israeli government as well."
* "[The] [[American Jewish Committee]] and [[B'nai B'rith]] both devoted impressive lobbying efforts to encourage newly independent post-Soviet states to establish diplomatic relations with Israel."

====Foreign cultivation of diaspora influence====
In some cases, foreign governments, in hopes of currying favor from distant diaspora communities believed to wield valuable political influence in their host counties, have dispensed generous benefits to local diaspora kin or improved relations with the diaspora's homeland.

Shain describes the [[Azerbaijan]] government's persistent frustration of with the influence of the [[Armenian-American]] lobby in Washington and the lack of a viable [[Azerbaijani American|Azerbaijani-American]] diaspora population to counter the Armenian's domestic presence.  The Azerbaijani response has been to cultivate Jewish organizations in Washington as their counterbalancing allies to the Armenian-American opposition.  The Azerbaijani [[ambassador]] to the United States described his efforts:
:"We understood that we need to make friends in this country.  We know how strong Jewish groups are.  They have asked us about the conditions of Jews in our country.  I helped them to go to Azerbaijan and open Jewish schools.  They came back with [a] good understanding [of the conflict]."<ref name=ShainWittes2002/>
Later, the son of the Azerbaijani ambassador was quoted: "We now have a lobby in the United States and that is the Jewish community."<ref name=ShainWittes2002/>

==Involvement with homeland conflicts==

===Dissident advocacy of homeland issues===
Diaspora communities, particular those predominantly composed of dissidents of the homeland authorities, can put significant effort towards undermining the homeland regime, going as far as to advocate or instigate for domestic coups.{{Citation needed|date=September 2010}}  There were segments of Iraqi-Americans who advocated strongly for the [[2003 invasion of Iraq]], segments of the Iranian-American population have similarly advocated for a regime change in [[Iran]] since the fall of the Shah, the [[Vietnamese American]] calls for democracy and religious freedom in Vietnam, and most prominent has been the consistent and vocal calls for ending [[Fidel Castro]]'s leadership of [[Cuba]] by the Florida-based [[Cuban-American lobby]].

Dhananjayan Sriskandarajah writes in reference to the Tamil diaspora that:
<blockquote>In the relatively permissive environment of Western host societies, [[Tamil diaspora]] association have articulated Tamil grievances, something that many argued was not possible because of repression in [[Sri Lanka]] (see, e.g., [[Ilankai Tamil Sangam]], n.d.). This activism stands in contrast to the marked lack of participation by Tamils in contemporary Sri Lankan civil society and the impossibility of gauging the views of northeastern Tamils during the conflict. Tamil diaspora activists claim to fill this gap, especially as it is illegal to articulate a Tamil secessionist position in Sri Lanka.<ref>[[Dhananjayan Sriskandarajah]], [http://www.tamilnation.org/diaspora/060209sriskandarajah.pdf Tamil Diaspora Politics], June 14, 2004, accessed January 3, 2006</ref></blockquote>

===Mobilization in response to outside threats===
When a homeland is threatened by another country, Shain writes, "the threat to a community's survival that the conflict represents can serve as an important mobilizing force for diasporic communities, enabling them to build institutions, raise funds, and promote activism among community members who might otherwise allow for their ethnic identity to fade to the level of mere 'folkways' [...] thus [playing] an important role in the diaspora community's ability to maintain and nourish its own ethnic identity."<ref name=ShainWittes2002/>

====Military aid====
Military aid from diasporas to their homelands can be vital in period of violent conflict.  Military aid offered by a diaspora, according to Shain,<ref name=ShainWittes2002/> can varying from fundraising in support of military purchases, directly supplying weapons, or serving "as a source of recruits."

Shain<ref name=ShainWittes2002/> cites the example of the military fundraising of the [[Eritrea]]n and [[Ethiopia]]n diaspora communities in the [[United States]] in response to the 1998-2000 [[Eritrean-Ethiopian War]], the eventual result of which was hundreds of millions of dollars in arm purchases by their respective homelands  Shain quotes from the account of [[Jesse Driscoll]] of [[Georgetown University]]:
:"The energy and organization of the Eritrean diaspora, however, was simply overpowering...  With none of the credibility baggage of the [ruling regime in Ethiopia], Eritrea called upon its wealthy and energetic... diaspora....  The fundraising efforts of President [[Isaias Afewerki]] in the United States have reached legendary status among those who following the conflict."<ref name=Driscoll2000>Driscoll, Jesse.  2000.  "The Economics of Insanity: Funding the Ethiopia-Eritrea War", Georgetown University.</ref>

====Public relations====
Diasporas, according to Shain and Wittes, can be "[[propaganda|propagandists]]" for their homelands.<ref name=ShainWittes2002/>

===Peace negotiations===
While in times of severe threat to the homeland, a diaspora suppresses its differences, once there is potential for peace, the conflict between the diaspora's ethnic interests and its homeland's national interests reemerge.<ref name=ShainWittes2002/>  In situations, where peaceful resolutions involve the homeland renouncing claims to historically meaningful territory, the preeminence in the diaspora's ethnic identity of the homeland's territory, which contrasts sharply with pragmatic valuations made by the homeland, can cause significant and deeply emotional debates and potential multi-level political battles.

Shain gives this description of the potential for diaspora-homeland conflict over potential territorial compromises:
:"[Consider] a state that gives up its claim to a piece of historically significant territory in order to achieve peaceful relations with a neighboring state.  Diaspora and homeland citizens often have different attitudes towards the implications such polices have for ethnic and national identity.  For many homeland citizens, territory services multiple functions: it provides sustenance, living space and security, as well as a geographic focus for national identity.  If giving up a certain territory, even one of significant symbolic value, would increase security and living conditions, a homeland citizen might find the trade-off worthwhile. By contrast, for the diaspora, the security of the homeland is of course important as well; but the territory's identity function is paramount.  Its practical value (and indeed the practical value of peace with a formal rival) is not directly relevant to the diaspora's daily experience. In such situations, altering the geographic configuration of the homeland state for the sake of peace may be far more disturbing to the diaspora elements than to segments of the homeland community."<ref name=ShainWittes2002/>

Again, while the leaders and public of the homeland may feel that their national interests trump those of the remote diaspora, the situation is complicated by the homeland's reliance on diaspora's political clout and financial assistance.  Such situations lead to the diaspora feeling threatened by actions of the homeland, which to the homeland are viewed as necessary, and if blocked by the diaspora result in harm to the nation's security.

====Negotiations as a "three-level game"====
Because of the potential of conflict between the homeland's national interests and the diasporas ethnic interests, and the ability of the diaspora to act independently as a deal-breaker when it feels its interests are at stake, [[Yossi Shain]] and [http://www.brookings.edu/experts/wittest.aspx Tamara Wittes] argue for explicitly including the involved diaspora communities in any [[Peacemaking|peace negotiations]].

Specifically, Shain and Wittes argue that the standard "[[Two-level game theory|two-level game]]" model for international peacemaking is inadequate for conflicts complicated by politically active diaspora.  The original "two-level game" model, introduced in 1988 by [[Robert Putnam]], recognizes only two levels of stakeholders as being relevant to a successful outcome, the domestic political constituencies of each state and each state's foreign negotiating counterparts.<ref name=Puntam1998>Robert D. Putnam.  "Diplomacy and Domestic Politics: The Logic of Two-Level Games."  ''International Organization.'' 42 (Summer 1988):427-460.</ref>  The solution, Shain advocates, is simply to expand the model from a "two-level game" to a "three-level game" in which political active diasporas are recognized as distinct and equally important stakeholders in the negotiation process.<ref name=ShainWittes2002/>

===Post-conflict demobilization===
Just as a threat to a homeland can mobilize a diaspora to organize, collect funds, and seek political influence, the peaceful end of a conflict, can lead to a parallel demobilization in the community.  The demobilization can be more disruptive for diaspora communities who have become deeply involved in their long-running homeland struggles.

Additionally, in the midst of a conflict, the diaspora community's status can be significantly elevated, both by the attention of the host state's foreign policy establishment seeking influence on the diaspora's homeland, and by the attention of homeland's leaders seeking influence in the diaspora's host states.  After the transition to peace, Shain writes, "[the] high-level meetings and phone calls may recede and diasporic community leaders find that internal communal prestige and their external levers of influence both degrade as a result."<ref name=ShainWittes2002/>

Shain hypothesizes:
:"If the Arab-Israeli conflict is resolved peacefully, for example, the AIPAC <nowiki>[</nowiki>[[American Israel Public Affairs Committee]]<nowiki>]</nowiki> is likely to see its mission greatly diminished, along with its membership, its funding, and its level of attention from elected officials in Washington."<ref name=ShainWittes2002/>

==Politically active diaspora in the United States==
{{Expand list|date=April 2013}}
{| class="wikitable" style="margin: 1em auto 1em auto;text-align: center;" border="1"
|+ Modern politically active diasporas
! Ethnic group
! Diaspora
! Homeland (est.)
! Nationalist movement
! Domestic lobby
! Concerns
|-
! [[African Americans]]
| [[African diaspora]]
| [[Africa]]
| various
| [[African-American lobby in foreign policy]]
| [[Anti-Apartheid Movement|Ending apartheid in South Africa]], foreign aid to Africa, [[Decolonization of Africa|support for independence of colonized African lands]]
|-
! [[Armenian Americans]]
| [[Armenian diaspora]]
| [[Armenia]] (1991)
| [[Armenian national movement|Armenian nationalism]]
| [[Armenian American lobby]][[Armenian National Committee of America|ANCA]], [[Armenian Assembly of America]], [[Armenian American Political Action Committee]]
| [[Anti-communism]], [[Nagorno-Karabakh]], [[Armenian Genocide]], Recognition as a major US ethnic group
|-
! [[Arab Americans]]
| [[Arab diaspora]]
| [[Arab world]]
| various
| [[Arab American Institute]], [[American Arab Anti-Discrimination Committee]], [[American Task Force on Palestine]]
| [[Arab–Israeli conflict]], [[2003 invasion of Iraq]], [[Anti-Arabism|ethnic and cultural discrimination]]
|-
! [[Azerbaijani Americans]]
| [[Azerbaijani diaspora]]
| [[Whole Azerbaijan]]
| [[Azerbaijani nationalism]]
| [[United States Azerbaijanis Network]]
| [[Nagorno-Karabakh War]], [[March days]], [[Khojaly Genocide]], [[Armenian–Azerbaijani War]], [[Black January]], etc.
|-
! [[Cuban Americans]]
| [[Cuban exile]]
| [[Cuba]]
|
| [[Cuban-American lobby]]
| Anti-communism, [[Opposition to Fidel Castro]], [[U.S. embargo against Cuba]], Status of Cuban political refugees legally entering the U.S.
|-
! [[Greek American]]s
| [[Greek diaspora]]
| [[Greece]] (1829)
|
| [[AHEPA]], American Hellenic Institute
| [[Aegean dispute]], [[Cyprus dispute]], [[Macedonia naming dispute]], [[Greek genocide]]
|-
! [[Irish Americans]]
| [[Irish diaspora]]
| [[Ireland]] (1920)
| [[Irish nationalism]]
| [[Irish American lobby]]
| [[United Ireland]], [[Northern Ireland]], Economic links, Undocumented immigration issue
|-
! [[Italian Americans]]
| [[Italian diaspora]]
| [[Italy]] (1861)
|
| [[National Italian American Foundation]], National Association of Italian Americans, [[Sons of Italy]]
| Risen influence in US political life, concerns of [[organized crime]], cultural integrity of Italians and [[Sicilian American|Sicilians]], fighting negative ethnic [[stereotypes]]
|-
! [[American Jews]]
| [[Jewish diaspora]]
| [[Israel]] (1948)
| [[Zionism]]
| [[Israel lobby in the United States]]
| [[Zionism]], [[Arab–Israeli conflict]], [[Antisemitism]], [[The Holocaust]]
|-
! [[Macedonian Americans]]
| [[Macedonian diaspora]]
| [[Republic of Macedonia]]
|
| [[United Macedonian Diaspora]]
| Anti-communism, [[Macedonia naming dispute]], [[NATO]], civil rights for Macedonians in Albania, Bulgaria, Greece, Serbia, and Kosovo, [[territorial integrity]]
|-
! [[Mexican Americans]]
| [[History of Mexican-Americans|Mexican diaspora]]
| [[Mexican Cession|Mexico (historic)]]
| [[Chicano nationalism]]
| [[Chicano Movement]], [[Mexican American Legal Defense and Education Fund]]
| [[Illegal Immigration]] Rights, socioeconomics, [[bilingualism]]
|-
! [[Islam in the United States|American Muslims]]
| various
| [[Muslim world]]
| various
| [[Council on American–Islamic Relations|CAIR]]
| [[War on Terrorism]], [[Islamophobia]]
|-
! [[Pakistani Americans]]
| [[Pakistani diaspora]]
| [[Pakistan]] (1947)
| [[Pakistani nationalism]]
| [[Pakistani lobby in the United States]]
| Economic and cultural links, combating stereotypes
|-
|-
! [[Polish Americans]]
| [[Polish diaspora|Polonia]]
| [[Poland]] (1918)
| [[Polish nationalism]]
| [[Polish American Congress]]
| Anti-communism, Economic links, Undocumented immigration issue
|-
! [[Puerto Ricans in the United States|Puerto Ricans]]
| [[Puerto Rican people]]
| [[Puerto Rico]]
| [[Puerto Rican nationalism]]
| [[Political parties of Puerto Rico]]
| [[Political status of Puerto Rico]], ending [[Puerto Rican Drug War|drug wars in Puerto Rico]], solidarity amongst other U.S. Latino groups, ending [[Poverty in the United States|poverty]] and [[disenfranchisement]] amongst the Puerto Rican community
|-
! [[Taiwanese Americans]]
| [[Taiwanese people|Taiwanese diaspora]]
| [[Taiwan]]
| [[Taiwan independence]]
| [[China lobby]]
| [[Political status of Taiwan]], Anti-communism, Racial issues as [[Asian Americans]]
|-
! [[Vietnamese Americans]]
| [[Vietnamese diaspora]]
| [[Vietnam]] (1975)
| various
| National Congress of Vietnamese Americans (NCVA), [[Boat People SOS]], [[Viet Tan]], Vietnamese-American Political Action Committee, Families of Vietnamese Political Prisoners Association, various others
| Anti-communism, Racial issues as [[Asian Americans]], [[Flag of South Vietnam|Vietnam Heritage and Freedom Flag]], [[Black April]], [[Undocumented immigration]] issue, political activism in the US, Vietnam Human Rights Day, political prisoners within Vietnam
|}

==See also==
* [[Diaspora studies]]
* [[Ethnic interest groups in the United States]]
* [[Ethnic nationalism]]
* [[Hyphenated American]]

==Further reading==
{{refbegin}}
* [[Robert J. Beck|Beck, Robert J.]] and [[Thomas Ambrosio]].  2001. "International Law and the Rise of Nations: The State System and the Challenge of Ethnic Groups."  CQ Press.  ISBN 1-889119-30-X
* Hockenos, Paul. 2003. "Homeland Calling: Exile Patriotism and the Balkan Wars." Cornell University Press.  ISBN 0-8014-4158-7
* [[Yossi Shain|Shain, Yossi]]. 2005. "The Frontier of Loyalty: Political Exile in the Age of the Nation State (New Edition)."  University of Michigan Press. ISBN 0-472-03042-6
* [[Yossi Shain|Shain, Yossi]]. 1999. "Marketing the American Creed Abroad: Diasporas in the UN and Their Homelands."  Cambridge University Press.  ISBN 0-521-64531-X
* [[Yossi Shain|Shain, Yossi]] & Tamara Cofman Wittes.  <u>Peace as a Three-Level Game: The Role of Diasporas in Conflict Resolution</u> in [[Thomas Ambrosio|Ambrosio, Thomas]].  2002.  "Ethnic identity groups and U.S. foreign policy."  Praeger Publishers.  ISBN 0-275-97533-9
* Shain, Yossi and M. Sherman. 1998. "Dynamics of disintegration: Diaspora, secession and the paradox of nation-states."  ''[[Nations and Nationalism (journal)|Nations and Nationalism]].'' 4(3):321-346.
{{refend}}

==References==
{{reflist|2}}

{{DEFAULTSORT:Diaspora Politics In The United States}}
[[Category:Diaspora studies|Politics in the United States, Diaspora*]]
[[Category:Ethnic groups in the United States]]
[[Category:Ethnicity in politics]]
[[Category:Foreign relations of the United States]]
[[Category:Identity politics in the United States]]