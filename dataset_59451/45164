{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Refimprove|date=August 2010}}
{{Infobox film
| name = An Airman's Letter to His Mother
| image =
| caption =
| director = [[Michael Powell]]
| producer = Michael Powell
| writer = Michael Powell
| narrator = [[John Gielgud]]<br>Michael Powell
| music =
| cinematography = Michael Powell<br>Bernard Browne (additional photography)<ref>Lazar 2013, p. XXXVIII.</ref>
| editing =Michael Powell
| studio = RHF Productions Ltd.<ref>[http://www.tcm.com/tcmdb/title/580310/Airman-s-Letter-to-His-Mother-An/original-print-info.html "Original print information: An Airman's Letter to His Mother (1941)."] ''Turner Classic Movies''. Retrieved: 25 December 2014.</ref>
| distributor =RHF Productions Ltd.
| released = June 1941 (UK)
| runtime = 5 minutes
| country = United Kingdom
| language = English
| budget =
}}
'''''An Airman's Letter to His Mother''''' (1941) is a documentary-style [[Cinema of the United Kingdom|British]] propaganda short film based on an actual letter from a British bomber pilot to his mother published in ''[[The Times]]'' in June 1940. Subsequently, the letter was published as a pamphlet and received wide distribution in the UK. "The letter in question had touched a nation's heart and made a popular film subject."<ref>Crook, Steve. [http://www.imdb.com/title/tt0033320/ "Storyline: 'An Airman's Letter to His Mother' (1941)."] ''IMDb''. Retrieved: 25 December 2014.</ref> In 1941, [[Michael Powell]] directed ''An Airman's Letter to His Mother'', based on a dramatization of the letter, and narrated by [[John Gielgud]].<ref name=amg>Butler, Craig. [http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:119923~T0 "Overview: An Airman's Letter to His Mother."] {{dead link|date=January 2017|bot=medic}}{{cbignore|bot=medic}} ''Allmovie''. Retrieved: 25 December 2014.</ref>
<!--spacing, please do not remove-->

==Plot==
A [[Royal Air Force]] (RAF) airman who dies during a mission has left a letter to be sent to his mother upon his death. The letter is delivered to his mother. As the letter is read out by his mother in the airman's room, she looks through his things and remembers him as a youth. The letter tells of his reasons for joining the air force and going to fight, knowing full well that he could die.

==Production==
Although ''An Airman's Letter to His Mother'' was not produced by the British Government, and was Powell's personal contribution to the war effort, the film had many of the hallmarks of "official" war films of the period. These films should explain: "What Britain is fighting for, including contrasting British values with Nazi Germany's values; how Britain fights and the need for sacrifice if the fight is to be won."<ref name="propaganda">[http://www.nationalarchives.gov.uk/education/focuson/film/activities/world-war-2/useful-notes/1.1-sources.htm "Useful notes: Extracts from a British Government document called ‘The Programme for Film Propaganda’ produced in January 1940."] ''National Archives'', 2014. Retrieved: 26 December 2014.</ref>

British propaganda films in [[World War II|Second World War]] also were in sharp contrast with the more racially tinged and strident examples from warring nations, especially Nazi Germany and the [[Soviet Union]]. Most often, British films were topical, informative and "entertaining or amusing."<ref name="propaganda"/> In the case of ''An Airman's Letter to His Mother'', the subject was the reading of a real letter left by an airman for his mother. [[John Gielgud]] reads the letter in a voiceover; the actress playing the part of the mother is unknown. Her face is not visible and she never speaks.{{refn|The tradition of leaving a letter to loved ones in your personal effects was a long-standing one for aviators. [[Amelia Earhart]] called them her "pop off" letters.<ref>Butler 1997, p. 326.</ref>|group=Note}}

==Historical accuracy==

===The author of the letter===
The author of the letter has subsequently been revealed to be [[Flying Officer]] Vivian Rosewarne, the [[Aviator|co-pilot]] of a [[Vickers Wellington]] [[bomber]], stationed at [[RAF Marham]], Norfolk. The 23-year-old flyer was killed during the [[Battle of Dunkirk]] in May 1940. Rosewarne's death notice was eventually published on 23 December 1940.<ref>[http://www.airmuseum.ca/mag/exag0311.html "Letter to Mother."] ''Commonwealth Air Training Plan Museum'' (Brandon, Manitoba, Canada). Retrieved: 25 December 2014.</ref>

Rosewarne was an only son whose early years were spent in Brentwood where he attended Brentwood School.

On 30 May 1940, a force of 17 Wellington bombers from RAF Marham took off to provide close ground support to the [[British Expeditionary Force (World War II)|British Expeditionary Force]] as they withdrew from the beaches of [[Dunkirk]]. Aircraft R3162 from [[No. 38 Squadron RAF]] was shot down near the town of Veurve in Belgium and the six-man crew were killed. On 31 May 1940, the co-pilot, [[Flying Officer]] Vivian Rosewarne, was reported missing, believed killed.

This is the text of the loss report and crew disposition:
{{Quotation|Type: Wellington Mk.1C
Serial number: R3162, HD-H
<br />Operation: Diksmuide, B
<br />Lost: 30 May 1940
<br />Flying Officer (Pilot) Vivian A.W. Rosewarne, RAF 40021, 38 Sqdn., age unknown, 30 May 1940, Veurne Communal Cemetery Extension, B
<br />Pilot Officer (Pilot) Roy Baynes, RAF 42479, 38 Sqdn., age unknown, 30 May 1940, Veurne Communal Cemetery Extension, B
<br />Sergeant (Obs.) John Knight, RAF 581515, 38 Sqdn., age 19, 30 May 1940, Veurne Communal Cemetery Extension, B
<br />Sergeant (W.Op./Air Gnr.) Dennis D.G. Spencer, RAFVR 755611, 38 Sqdn., age unknown, 30 May 1940, Veurne Communal Cemetery Extension, B
<br />Aircraftman 2nd Class (W. Op./Air Gnr.) James C. Adams, RAF 630069, 38 Sqdn., age 20, 30 May 1940, Veurne Communal Cemetery Extension, B
<br />Sergeant (Air Gnr.) John Dolan, RAFVR 902515, 38 Sqdn., age 34, 30 May 1940, Veurne Communal Cemetery Extension, B
Airborne 2225 30 May 40 from Marham.
<br />Crashed at Veurne (West Vlaanderen), 25&nbsp;km SW of Ostend, Belgium. Cause not established.}}

Rosewarne and his crew were laid to rest at Veurne Communal Cemetery Extension (West Vlaanderen Belgium).

===Publication===
His station commander, [[Group Captain]] [[Claude Hilton Keith]], found a letter among the missing airman's personal possessions. It had been left open, so that it could be passed by the censor. Keith was so moved by the letter that, with the mother's permission, it was anonymously published in ''The Times'' on 18 June 1940. {{refn|On this day, [[Winston Churchill]] announced, in his "[[This was their finest hour]]" speech to the [[House of Commons of the United Kingdom|House of Commons]] that the "Battle of France is over. I expect that the Battle of Britain is about to begin."<ref name="Williams"/>|group=Note}}

====The letter====
{{Quotation|Dearest Mother: Though I feel no premonition at all, events are moving rapidly and I have instructed that this letter be forwarded to you should I fail to return from one of the raids that we shall shortly be called upon to undertake. You must hope on for a month, but at the end of that time you must accept the fact that I have handed my task over to the extremely capable hands of my comrades of the Royal Air Force, as so many splendid fellows have already done.

First, it will comfort you to know that my role in this war has been of the greatest importance. Our patrols far out over the North Sea have helped to keep the trade routes clear for our convoys and supply ships, and on one occasion our information was instrumental in saving the lives of the men in a crippled lighthouse relief ship. Though it will be difficult for you, you will disappoint me if you do not at least try to accept the facts dispassionately, for I shall have done my duty to the utmost of my ability. No man can do more, and no one calling himself a man could do less.

I have always admired your amazing courage in the face of continual setbacks; in the way you have given me as good an education and background as anyone in the country: and always kept up appearances without ever losing faith in the future. My death would not mean that your struggle has been in vain. Far from it. It means that your sacrifice is as great as mine. Those who serve England must expect nothing from her; we debase ourselves if we regard our country as merely a place in which to eat and sleep.

History resounds with illustrious names who have given all; yet their sacrifice has resulted in the British Empire where there is a measure of peace, justice and freedom for all, and where a higher standard of civilization has evolved, and is still evolving, than anywhere else. But this is not only concerning our own land. Today we are faced with the greatest organized challenge to Christianity and civilization that the world has ever seen, and I count myself lucky and honoured to be the right age and fully trained to throw my full weight into the scale. For this I have to thank you. Yet there is more work for you to do. The home front will still have to stand united for years after the war is won. For all that can be said against it, I still maintain that this war is a very good thing: every individual is having the chance to give and dare all for his principle like the martyrs of old. However long the time may be, one thing can never be altered – I shall have lived and died an Englishman. Nothing else matters one jot nor can anything ever change it.

You must not grieve for me, for if you really believe in religion and all that it entails that would be hypocrisy. I have no fear of death; only a queer elation ... I would have it no other way. The universe is so vast and so ageless that the life of one man can only be justified by the measure of his sacrifice. We are sent to this world to acquire a personality and a character to take with us that can never be taken from us. Those who just eat and sleep, prosper and procreate, are no better than animals if all their lives they are at peace.

I firmly believe that evil things are sent into the world to try us; they are sent deliberately by our Creator to test our mettle because He knows what is good for us. The Bible is full of cases where the easy way out has been discarded for moral principles.

I count myself fortunate in that I have seen the whole country and known men of every calling. But with the final test of war I consider my character fully developed. Thus at my early age my earthly mission is already fulfilled and I am prepared to die with just one regret: that I could not devote myself to making your declining years more happy by being with you; but you will live in peace and freedom and I shall have directly contributed to that, so here again my life will not have been in vain.

Your loving son}}

====Reaction to the letter====
''The Times'' was inundated with over 10,000 requests for copies of the letter in the first few days after publication. The letter was subsequently published in a small book by ''The Times'' Publishing Company Ltd. (as ''An Airman's Letter to His Mother'') and reprinted three times. By the end of the year, over 500,000 copies had been sold. [[George VI of the United Kingdom|King George VI]] wrote personally to the mother. In the USA the book was reprinted 12 times by E.P. Dutton & Co.<ref name="Crook"/>

Suggestions that the letter was fictitious and a propaganda device eventually led to the identification of Flying Officer Rosewarne with his death notice finally published on 23 December 1940. A portrait of Flying Officer Rosewarne (painted from his mother's photographs) by [[Frank O. Salisbury]] was unveiled on 18 September 1941 and although his mother attended, she wished to remain anonymous desiring to be known only as "the mother of the young unknown warrior."<ref name="Crook">Crook, Steve. [http://www.powell-pressburger.org/Reviews/41_Airmans/Letter2.html "A son's stirring words."] ''The Powell & Pressburger Pages''.  Retrieved: 25 December 2014.</ref>

Rosewarne's letter continues to inspire and his letter features in the RAF's publication ''Leadership''. His portrait is displayed at Brentwood School and a copy at Cranwell.

The following pictures of Rosewarne come from his Commanding Officer Group Captain C. H. Keith's personal copy of the published book and are published here for the first time.
<gallery>
File:Vivian Rosewarne.JPG|
File:V.Rosewarne2.JPG|
File:Rosewarne.3.JPG|
</gallery>

==Reception==
Film critic Tony Williams reviewed ''An Airman's Letter to His Mother'', characterizing the work as an example of Powell's creative style, "... this five-minute short contains several examples of supreme visual composition and touching narration. They often transcend its original purpose as a work of British wartime propaganda making it far more compelling and enduring than Powell’s contribution to the now dated and unwatchable ''[[The Lion Has Wings]]'' (1939), which he does mention in his recollections."<ref name="Williams">Williams, Tony. [http://sensesofcinema.com/2005/cteq/airmans_letter/ "An Airman's Letter to His Mother."] ''Sense of Cinema'' (originally published in ''Cinémathèque Annotations on Film'', July 2005. Retrieved: 25 December 2014.</ref>

==References==

===Notes===
{{Reflist|group=Note}}

===Citations===
{{Reflist}}

===Bibliography===
{{Refbegin}}
* Butler, Susan. ''East to the Dawn: The Life of Amelia Earhart''. Reading, Massachusetts: Addison-Wesley, 1997. ISBN 0-306-80887-0.
* Lazar, David, ed. ''Michael Powell: Interviews''. Jackson, Mississippi: University Press of Mississippi, 2013. ISBN 978-1-57806-498-4.
{{Refend}}

==External links==
* [http://www.powell-pressburger.org/Reviews/41_Airmans ''An Airman's Letter to His Mother''] reviews and articles at the [http://www.powell-pressburger.org Powell & Pressburger Pages]
* {{IMDb title|id=0033320}}
* {{Amg movie|119923}}

{{Powell and Pressburger}}

{{DEFAULTSORT:Airman's Letter To His Mother, An}}
[[Category:1941 films]]
[[Category:English-language films]]
[[Category:British films]]
[[Category:British aviation films]]
[[Category:British short films]]
[[Category:British World War II propaganda films]]
[[Category:British black-and-white films]]
[[Category:Films about shot-down aviators]]
[[Category:Films by Powell and Pressburger]]
[[Category:Films directed by Michael Powell]]