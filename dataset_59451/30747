{{Use British English|date=January 2015}}
{{Infobox Doctor Who episode|
|image=[[File:Doomsday (Doctor Who).jpg|275px]]
|caption=The Daleks, the Cybermen and Torchwood battle in [[Canary Wharf]], in the first Dalek–Cyberman encounter and conflict forty-three years after the show's premiere.
|number = 177b
|serial_name=Doomsday
|show=DW
|type=episode
|doctor=[[David Tennant]] ([[Tenth Doctor]])
|companion=[[Billie Piper]] ([[Rose Tyler]])
|guests= 
*[[Camille Coduri]] – [[Jackie Tyler]]
*[[Noel Clarke]] – [[Mickey Smith]]
*[[Shaun Dingwall]] – [[Pete Tyler#Pete Tyler (parallel universe)|Pete Tyler]]
*[[Andrew Hayden-Smith]] – [[Jake Simmonds]]
*[[Tracy-Ann Oberman]] – [[List of Doctor Who villains#Yvonne Hartman|Yvonne Hartman]]
*[[Raji James]] – Dr. Rajesh Singh
*[[Paul Kasey]] – Cyber Leader
*[[Nicholas Briggs]] – Dalek and Cyberman voices
*[[Catherine Tate]] – [[Donna Noble|The Bride]]
|writer=[[Russell T Davies]]
|director=[[Graeme Harper]]
|script_editor=[[Helen Raynor]]
|producer=[[Phil Collinson]]
|executive_producer=Russell T Davies<br />[[Julie Gardner]]
|composer=[[Murray Gold]]
|length= 2nd of 2-part story, 45 minutes
|date=8 July 2006
|preceding="[[Army of Ghosts]]"
|following="[[The Runaway Bride (Doctor Who)|The Runaway Bride]]"
|series=[[Doctor Who (series 2)|Series 2]]
|series_link=Series 2 (2006)
|}}
"'''Doomsday'''" is the thirteenth and final episode in the [[Doctor Who (series 2)|second series]] of the revival of the British [[science fiction television]] programme ''[[Doctor Who]]''. It was first broadcast on 8 July 2006 and is the conclusion of a two-part story; the first part, "[[Army of Ghosts]]", was broadcast on 1 July 2006. The two-part story features the [[Dalek]]s, presumed extinct after the events of [[The Parting of the Ways|the 2005 series' finale]], and the [[Cyberman|Cybermen]], who appeared in a [[parallel universe (fiction)|parallel universe]] in "[[Rise of the Cybermen]]" and "[[The Age of Steel]]". Both species unexpectedly arrive on Earth at the conclusion of "Army of Ghosts".

The concept of the Daleks and the Cybermen both appearing on-screen was first proposed in 1967, but was vetoed by [[Terry Nation]], the creator of the Daleks. The episode is the first conflict between the two species in ''Doctor Who''{{'}}s 45-year history, and features [[Billie Piper]]'s last appearance in the lead [[companion (Doctor Who)|companion]] role as [[Rose Tyler]]; the final regular appearance of [[Noel Clarke]] as Rose's ex-boyfriend and previous companion [[Mickey Smith]]; and the final regular appearances of [[Camille Coduri]] and [[Shaun Dingwall]] as Rose's parents, [[Jackie Tyler|Jackie]] and [[Pete Tyler]]. The episode was filmed in December 2005 and January 2006, alongside the episodes "Rise of the Cybermen" and "The Age of Steel".

The plot consists mostly of the Daleks and Cybermen waging a global war, with humanity caught in the crossfire. The [[Doctor (Doctor Who)|Doctor]], the Tyler family, and Mickey Smith fight for their lives trying to reverse the situation. They are successful, but at an emotional cost to the Doctor and Rose, as they are left in separate universes.

The episode is one of the most popular ''Doctor Who'' episodes since the show's revival. It was nominated, along with "Army of Ghosts", for the 2007 [[Hugo Award for Best Dramatic Presentation, Short Form]]; the award was won by the fourth episode in the series, "[[The Girl in the Fireplace]]". It shared the revived series' highest [[Appreciation Index|Audience Appreciation]] rating of 89 with "[[The Parting of the Ways]]", "[[Silence in the Library]]", and "[[Forest of the Dead]]" until 28 June 2008—"[[The Stolen Earth]]" gained an AI rating of 91<ref name="SEAI" />—and is favoured by most critics for both the Dalek-Cyberman conflict and the farewell scene between the Doctor and Rose.

==Plot==
Continuing the events of "[[Army of Ghosts]]", the episode opens with Dr. Singh, [[Mickey Smith]], and [[Rose Tyler]] trapped in a sealed room within the [[Torchwood Institute]]. Four [[Daleks]], accompanied by a device known as the "[[List of Doctor Who items#Genesis Ark|Genesis Ark]]", have emerged from the [[List of Doctor Who vehicles#Void ship|Void ship]]. A black Dalek called Dalek Sec extracts information about Earth from Dr. Singh, killing him in the process. Dalek Sec discovers that a separate invasion is in progress, and sends Dalek Thay to investigate. The Cybermen who took control of Torchwood detect the Dalek technology and confront them, offering an alliance. The Daleks decline and kill two Cybermen, causing the Cyber Leader to declare war on the Daleks.

Ms. Hartman and Jackie are taken by the Cybermen for conversion, but the Cyber Leader orders the Doctor held aside because he has valuable information about the Daleks. The Cyber Leader is destroyed by a strike team led by Jake Simmonds, who have travelled from their universe in pursuit of the Cybermen. Ms. Hartman is converted, but Jackie escapes into a stairwell. Jake uses a device around his neck to take the Doctor to the parallel universe and their version of Torchwood. The Doctor meets with the alternate universe Pete Tyler, who tells him that after he left the Cybermen were sealed in their factories while humanity decided what to do with them. In the meantime, the Cybermen vanished and the breach began causing unprecedented global warming on the parallel Earth, which the Doctor theorises is the start of the process that will lead to both planets falling into the Void. Pete demands the Doctor seal the breach before his Earth is destroyed.

Meanwhile, Rose deduces that she and Mickey were kept alive because their touch can activate the Genesis Ark. Dalek Sec explains that they cannot open the Ark because it is stolen [[Time Lord]] technology. He demands that Rose open it, but she refuses and mocks the Daleks until the Doctor arrives. The Doctor realises that these Daleks are the enigmatic [[Cult of Skaro]], and uses his [[sonic screwdriver]] to allow the Cybermen into the sphere chamber. As the Cybermen attack the Daleks, Mickey accidentally activates the Ark while escaping. The Daleks battle the Cybermen and Dalek Sec takes the Ark outside. The Doctor and his companions flee into the tower. Along the way, Pete saves Jackie from the Cybermen and the two embrace. The Doctor then takes everyone to the control room where they witness the Genesis Ark open and millions of Daleks that were imprisoned during the [[Time War (Doctor Who)|Time War]] pour out and begin engaging the Cybermen firing on them from below, though humanity is caught in the crossfire and many are killed while running for cover.

The Doctor explains that crossing the Void causes a traveller to become saturated in Void material. If he opens the breach and reverses it, anything saturated in Void material will be pulled in. Everyone except Jackie has crossed the breach and is thus vulnerable to being trapped in the Void, so the Doctor sends them all to the parallel universe. Rose decides she would rather be with the Doctor than her family and jumps back to help him. The Cybermen attempt to stop the Doctor but are repelled by a converted Ms. Hartman, who has resisted the effects of her conversion and at least partially retains her sense of self. The Doctor and Rose open the breach and hang on to magnetic clamps as the Cybermen and Daleks are pulled in. The Cult of Skaro use an emergency temporal shift to escape. Rose's lever slips, and in resetting it she loses her grip and plunges toward the Void. At the last second, Pete reappears and grabs her, and together they transport back to the parallel universe. The breach closes, leaving a devastated Rose trapped in the parallel universe. The devices used to travel between the two worlds have also stopped working. The Doctor presses his ear against the wall, as a tearful Rose does the same in the other universe, as if trying to listen for each other.

Some time later, Rose has a dream where she hears the Doctor's voice calling her. The Tylers follow the voice to a remote bay in Norway called [[Story arcs in Doctor Who#Bad Wolf|Bad Wolf Bay]], where a holographic image of the Doctor appears. He tells Rose he is using the energy of a [[supernova]] to transmit to her via one last small breach between universes. Rose breaks down in tears and tells the Doctor that she loves him, but before the Doctor can reply, the breach seals and the Doctor's image disappears. In the TARDIS, tears run down the Doctor's cheeks as he slowly regains his composure and sets a course. As he gets back to piloting the TARDIS, he notices a mysterious woman in a wedding dress standing in the TARDIS console room. With the Doctor shocked and confused, the mysterious bride angrily demands to know where she is, leading to the events of "[[The Runaway Bride (Doctor Who)|The Runaway Bride]]".

==Production==

===Conception===
[[File:Space City 2016 - Billie Piper (26730678884).jpg|thumb|left|''Doomsday'' featured the departure of [[Rose Tyler]] portrayed by [[Billie Piper]] (''pictured'') who made the decision to leave the role a year prior.]]
The concept of the Daleks and Cybermen appearing together on screen is not new; in December 1967, the BBC approached [[Terry Nation]] to have both races in a serial, but Nation vetoed this idea. The concept came to Davies while mapping out the 2006 series: the story would both serve to resurrect the popular Daleks and provide a suitable exit for Piper, who had decided to leave ''Doctor Who''.<ref name="brief" /> "Doomsday" is the first episode in the history of ''Doctor Who'' where the Cybermen and the Daleks appear on-screen together; Cybermen and Daleks were both featured in ''[[The Five Doctors]]'' and "[[Army of Ghosts]]", but in separate scenes.<ref>{{cite serial | title = [[The Five Doctors]] | series = [[Doctor Who]] | credits = [[Terrance Dicks]] (writer), [[Peter Moffatt]], [[John Nathan-Turner]] (uncredited) (directors), John Nathan-Turner (producer) | network = [[BBC]] | station = [[BBC1]] | airdate = 23 November 1983}}</ref><ref>{{cite episode | title = [[Army of Ghosts]] | series = [[Doctor Who]] | series-no = 2 | number = 12 | credits = [[Russell T Davies]] (writer), [[Graeme Harper]] (director), [[Phil Collinson]] (producer) | network = [[BBC]] | station = [[BBC One]] | airdate = 1 July 2006}}</ref>

The two-part finale was originally going to take place in Cardiff on the [[Cardiff Rift|time rift]], which was the focus of the episodes "[[The Unquiet Dead]]" and "[[Boom Town (Doctor Who)|Boom Town]]". When ''Torchwood'' was commissioned in 2005, Davies decided to base the spin-off in Cardiff and relocate "Army of Ghosts" and "Doomsday" to [[Canary Wharf]] in London.<ref name="brief">{{cite web|url=http://www.shannonsullivan.com/drwho/serials/2006lm.html|title="Army of Ghosts"/"Doomsday"|first=Shannon|last=Sullivan|work=A Brief History of Time (Travel)|accessdate=30 October 2007|date=15 November 2006}}</ref>

An item of discussion between the production staff was over who would rescue Rose; Davies and Julie Gardner wanted Pete to rescue her, while Clarke and Phil Collinson wanted Mickey. The role was ultimately given to Pete, to emphasise that he had accepted Rose as a surrogate daughter.<ref name="brief" /> The Doctor's intended reply to Rose was also discussed; Davies, who left the reply unspecified, stated he didn't know when asked by Collinson on the episode's [[DVD commentary|commentary track]], and Gardner vehemently believed the Doctor would reciprocate Rose's love.<ref name="pod" />

Some elements of the story were inspired by [[Philip Pullman]]'s ''[[His Dark Materials]]'' trilogy. Pullman was "flattered" by the references in the episode, and compared Davies' actions to his own practice of referencing works.<ref>{{cite news | title = Would Pullman write for Dr Who? | publisher = [[Newsround]] | date = 7 December 2007 | url = http://news.bbc.co.uk/cbbcnews/hi/newsid_7130000/newsid_7132700/7132741.stm | accessdate=9 December 2007}}</ref>

===Filming===
[[File:Southerndown beach.jpg|thumb|[[Southerndown]] beach in Wales was used as the backdrop to the Doctor's farewell to Rose Tyler on Bad Wolf Bay.]]
To ensure that Clarke and Dingwall were available for filming, the story was filmed in the season's third production block with "[[Rise of the Cybermen]]" and "[[The Age of Steel]]". Filming for the story started on 2 November 2005 on location in [[Kennington]], London, but did not become the primary focus of the production crew until 29 November, when filming began on the scenes in and around the sphere chamber. The scene of the Tylers driving through Norway was filmed at Bridgend on 6 December. Scenes in the lever room, the main setting for the story, were filmed on 12–15 December 2005 and 3–5 January 2006. [[Greenscreen]] work for Rose being sucked into the void took place on 13 January, and the skirmish between the military and Cybermen on the bridge was filmed on 15 January.<ref name="brief" />

Other location shooting took place at the Coal Exchange and Mount Stuart Square, [[Cardiff Bay]].<ref>
{{cite web
|url= http://www.bbc.co.uk/wales/arts/sites/doctor-who-wales/alllocations/cardiff-coal-exchange-mount-stuart-square
|title= Walesarts, Coal Exchange and Mount Stuart Square, Cardiff Bay
|publisher = BBC
|author = 
|date = 
|accessdate =30 May 2010
}}
</ref>

The penultimate scene of the episode, the Doctor's farewell to Rose, was filmed on 16 January 2006; it was the last day of filming for Clarke and Dingwall. As with all scenes set at Bad Wolf Bay, these were in fact filmed at [[Southerndown]] beach in the [[Vale of Glamorgan]].<ref>
{{cite web
|url= http://www.bbc.co.uk/wales/arts/sites/doctor-who-wales/alllocations/southerndown-beach
|title= Walesarts, Southerndown beach, Vale of Glamorgan
|publisher = BBC
|author = 
|date = 
|accessdate =30 May 2010
}}
</ref> Piper's last scene was Rose's reunion with the Doctor in "[[The Satan Pit]]" on 31 March,<ref>{{cite web|url=http://www.shannonsullivan.com/drwho/serials/2006hi.html|title="The Impossible Planet"/"The Satan Pit"|first=Shannon|last=Sullivan|work=A Brief History of Time (Travel)|accessdate=30 October 2007}}</ref> but the shoot was rather emotional,<ref name="pod">{{cite video
 |people = [[Russell T Davies]], [[Julie Gardner]], [[Phil Collinson]]
 |title = Commentary for "Doomsday"
 |url = http://downloads.bbc.co.uk/doctorwho/mood-commentary.mp3
 |archiveurl=https://web.archive.org/web/20070120043211/http://downloads.bbc.co.uk/doctorwho/mood-commentary.mp3
 |archivedate=20 January 2007
 |format = mp3
 |publisher = [[BBC]]
 |accessdate =30 October 2007
}}</ref> to the point there were several tears on set.<ref>{{cite web|url=http://www.bbc.co.uk/doctorwho/confidential/index13.shtml|title=Episode 13: Finale|work=[[Doctor Who Confidential]]|publisher=[[BBC]]|accessdate=29 October 2007|format=Embedded [[.swf|Flash object]]}}</ref> The last scene of "Doomsday", Catherine Tate's appearance in the TARDIS as [[Donna Noble]] (credited as "The Bride"), was filmed on 31 March during the [[wrap party]]. To ensure the secrecy of Rose's departure and Tate's appearance, only Piper and Tennant were given scripts of the departure scene, and director Graeme Harper was not informed of the final scene until the last possible second.<ref name="brief" />

===Music===
{{listen|width=300px|filename=Doomsday.ogg|title="Doomsday"|description=The beginning of the song "Doomsday".|format=[[OGG]]}}

As well as using existing music, such as the themes for the Daleks, Cybermen, and Rose, [[Murray Gold]] specially composed a piece of music for Rose's farewell entitled "Doomsday", which featured vocal work from [[Melanie Pappenheim]]. Instead of using the swelling violins that Davies and the rest of the production team had expected, Gold took a minimalist approach. When pitching the track to the production team, Gold described the track as representing Rose's unbridled energy and determination as she searches for the Doctor. He later said, "I wanted to get that kind of throbbing, sort of hurt sound of quite emotional rock, because I thought that's what Rose would do if she was hurting and ran up to her bedroom and locked herself in her room and had a good old cry, really."<ref name="mam">{{cite episode | title = Music and Monsters | series = [[Doctor Who Confidential]] | network = [[BBC]] | station = [[BBC One]] | airdate = 25 December 2006}}</ref> The piece uses the same vocal work from "[[Rose (Doctor Who)|Rose]]", when Rose first enters the TARDIS, thus creating a [[framing device|bookend effect]].<ref name="mam" /> It is a favourite among fans and of executive producer Julie Gardner,<ref name="pod" /> and is one of the reasons, along with Pappenheim's overall contribution and the "Song for Ten" from "[[The Christmas Invasion]]", that the [[Doctor Who: Original Television Soundtrack|soundtrack of both series]] was released several months later.<ref>{{cite web|url=http://www.bbc.co.uk/doctorwho/news/cult/news/drwho/2006/07/17/33953.shtml|title=Who soundtrack soon|archiveurl=https://web.archive.org/web/20080317165950/http://www.bbc.co.uk/doctorwho/news/cult/news/drwho/2006/07/17/33953.shtml|archivedate=17 March 2008|date=17 July 2006|accessdate=6 July 2011|publisher=BBC}}</ref><ref>{{cite web|url=http://www.bbc.co.uk/doctorwho/news/cult/news/drwho/2006/11/06/37664.shtml|archiveurl=https://web.archive.org/web/20090202221723/http://www.bbc.co.uk/doctorwho/news/cult/news/drwho/2006/11/06/37664.shtml|archivedate=2 February 2009 |title=Soundtrack details|date=6 November 2006|accessdate=6 July 2011|publisher=BBC}}</ref>

==Broadcast, reception, and legacy==

===Broadcast and pre-airing media blackout===
To protect as much information concerning the episode as possible, the final scene of "Army of Ghosts" was withheld. The BBC website's Fear Forecasters, a panel who rate the episodes, were not allowed to see "Doomsday" before its airing,<ref>{{cite web |url=http://www.bbc.co.uk/doctorwho/episodes/2006/fear/f-armyofghosts.shtml |title=Fear Forecast: "Army of Ghosts" |accessdate=25 February 2007 |work=BBC ''Doctor Who'' website |publisher=[[BBC]]}}</ref> and access to copies was restricted; the website thus does not have a Fear Forecast for the episode.<ref name=FFMissing>{{cite web|title=Fear Forecast|url=http://www.bbc.co.uk/doctorwho/episodes/fearforecast.shtml|publisher=BBC|accessdate=5 July 2011}}</ref> Despite this, the Dalek Sec prop, which had been previously unused in the series, had invaded the stage at the 2006 [[British Academy Television Awards|BAFTA Television Awards]] while the production team were collecting an award.<ref name="pod" /> A similar moratorium would be placed on the following series' finale, "[[Last of the Time Lords]]".<ref name="lizo">{{cite news|url=http://news.bbc.co.uk/cbbcnews/hi/newsid_6760000/newsid_6763700/6763787.stm|publisher=[[CBBC]]|title=What did Lizo think of Doctor Who?|date=18 June 2007|accessdate=21 June 2007}}</ref>

The episode's finalised average viewing figure was 8.22 million viewers and was, excepting World Cup games, the second most-watched television programme of the week, behind an episode of ''[[Coronation Street]]'', and eighth most-watched overall. The companion episode of ''[[Doctor Who Confidential]]'' gained just over one million viewers, making it the second most watched programme on a non-terrestrial channel that week.<ref name="ratings">{{cite web|archiveurl=https://web.archive.org/web/20090216151446/http://www.gallifreyone.com/cgi-bin/viewnews.cgi?id=EEVFuyAyZybLSaHPyi&tmpl=newsrss|url=http://www.gallifreyone.com/cgi-bin/viewnews.cgi?id=EEVFuyAyZybLSaHPyi&tmpl=newsrss|title=Doomsday Final Ratings, and Series Two Recap|work=[[Doctor Who News Page]]|last=Lyon|first=Shaun|date=20 July 2006|archivedate=16 February 2009}}</ref> The ratings for the episode were higher than the following [[2006 FIFA World Cup|World Cup]] match between [[Germany national football team|Germany]] and [[Portugal national football team|Portugal]], which had a million fewer viewers.<ref name="regi">{{cite web|url=http://www.theregister.co.uk/2006/07/14/geektv_14_july/|title=World Cup streaming fails to score|first=Jane|last=Hoskyn|publisher=[[The Register]]; TV Scoop}}</ref>

===Critical reception and later release===
"Doomsday" is one of the most popular episodes of the revived ''Doctor Who''. It gained an audience [[Appreciation Index]] (AI) of 89, which was the highest figure for nearly two years—it was later surpassed by "[[The Stolen Earth]]", which had an AI of 91<ref name="SEAI">{{cite web|url=http://www.doctorwhonews.net/2008/06/stolen-earth-ai-figure-and-digital_4031.html|title=The Stolen Earth – AI and Digital Ratings|first=Matt|last=Hilton|date=30 June 2008|publisher=[[Doctor Who News Page]]|accessdate=30 June 2008}}</ref><ref name="SAJ88">{{cite web|url=http://www.doctorwhonews.net/2007/04/smith-and-jones-ai-figure_5305.html|title=Smith and Jones AI figure|date=2 April 2007|accessdate=24 January 2008|first=Matt|last=Hilton|publisher=[[Doctor Who News Page]]}}</ref>—and is the first episode of ''Doctor Who'' to receive a perfect 10 rating on ''[[IGN]]'',<ref name="IGN">{{cite web|url=http://uk.tv.ign.com/index/reviews.html?constraint.floor.article.overall_rating=10&constraint.return_all=is_true&sort.attribute=article.overall_rating&sort.order=desc |title=Television reviews; Score: 10 |publisher=IGN |accessdate=2 November 2007 |date=22 December 2006 |deadurl=yes |archiveurl=https://web.archive.org/web/20110713005614/http://uk.tv.ign.com/index/reviews.html?constraint.floor.article.overall_rating=10&constraint.return_all=is_true&sort.attribute=article.overall_rating&sort.order=desc |archivedate=13 July 2011 |df=dmy }}</ref> who congratulated Davies on making an action-packed episode so emotional.<ref name="ign_review">{{cite web|first=Ahsan|last=Haque|date=22 December 2006|url=http://uk.tv.ign.com/articles/752/752207p1.html|accessdate=2 November 2007|title=Doomsday review|publisher=IGN}}</ref> ''[[Television Without Pity]]'' gave the episode an A+ rating.<ref name="TWP">{{cite web|first=Jacob |last=Clifton |url=http://www.televisionwithoutpity.com/show/doctor_who/doomsday_2.php |publisher=[[Television Without Pity]] |title=Hold the Line With Me: Doomsday recap |work=Doctor Who reviews |date=31 December 2006 |accessdate=2 November 2007 |deadurl=yes |archiveurl=https://web.archive.org/web/20080421200137/http://www.televisionwithoutpity.com:80/show/doctor_who/doomsday_2.php |archivedate=21 April 2008 |df=dmy }}</ref> ''[[The Stage]]'' commented that the Dalek-Cybermen conflict was the "only thing worth watching" at the weekend, overshadowing even the [[2006 FIFA World Cup Final|World Cup Final]], and that the parting scene was "beautifully written and movingly played," with "not a dry eye in the universe".<ref>{{cite news|url=http://www.thestage.co.uk/features/feature.php/13256/tv-review|title=TV review|date=17 July 2006|accessdate=22 December 2007|archiveurl=https://web.archive.org/web/20120220193003/http://www.thestage.co.uk/features/feature.php/13256/tv-review|archivedate=20 February 2012|publisher=[[The Stage]]|first=Harry|last=Venning}}</ref> Dek Hogan of ''[[Digital Spy]]'' felt that the episode was "beautifully balanced and with moments of high excitement and touching poignancy" and that the single oil tear shed by the Cyberman version of Hartman was a "nice touch". He criticised Catherine Tate's appearance as being unnecessary to end the episode and for "breaking the mood".<ref>{{cite web|url=http://www.digitalspy.co.uk/tv/a34710/horses-for-courses.html|title=Horses for Courses|date=9 July 2006|accessdate=22 December 2007|publisher=Digital Spy|work=Dek's TV Diary|first=Dek|last=Hogan}}</ref> Stephen Brook of ''[[The Guardian]]'' thought that the episode was "a highpoint of the modern series, highly emotional, scary and genuinely exciting", while Rose's departure was "brilliantly handled". He positively compared the episode's plot of a war between "the greatest monsters in the programme history" against the film ''[[Alien vs. Predator (film)|Alien vs. Predator]]''.<ref>{{cite web|url=http://blogs.guardian.co.uk/organgrinder/2006/07/doctor_who_that_was_the_year_t.html|title=Doctor Who: that was the year that was|publisher=''[[The Guardian]]''|work=Organgrinder|date=10 July 2006|accessdate=25 January 2008|first=Stephen|last=Brook}}</ref>

After its initial airing, the episode was released on DVD, with "[[Fear Her]]" and "[[Army of Ghosts]]", on 25 September 2006.<ref name="dvd">{{cite web|url=http://www.bbcshop.com/invt/bbcdvd1964|title=Doctor Who: Series 2 Volume 5|work=BBC Shop|publisher=BBC|accessdate=7 January 2008}}</ref> It was first aired on [[CBC Television]] on 19 February 2007.<ref name="twidw">{{cite web|url=http://www.gallifreyone.com/thisweek.php|archiveurl=https://web.archive.org/web/20070209185153/http://www.gallifreyone.com/thisweek.php|title=Vol 10, No 6|work=This Week in Doctor Who|archivedate=9 February 2007 |accessdate=7 January 2008|publisher=[[Doctor Who News Page]]; [[Internet Archive]]}}</ref> The story ("Army of Ghosts" and "Doomsday") was one of three from the second series of ''Doctor Who'' to be nominated for the [[2007 Hugo Award for Best Dramatic Presentation, Short Form|2007]] [[Hugo Award]] for [[Hugo Award for Best Dramatic Presentation, Short Form|Best Dramatic Presentation, Short Form]]; the other stories nominated were "[[School Reunion (Doctor Who)|School Reunion]]" and "[[The Girl in the Fireplace]]",<ref>{{cite web
|url=http://www.nippon2007.us/hugo_nominees.php|title=Nippon 2007 Hugo Nominees|publisher=World Science Fiction Society|accessdate=29 March 2007}}</ref> the latter winning the award.<ref name="hugowin">{{cite web| url = http://www.thehugoawards.org/index.php?page_id=127| title = 2007 Hugo Awards| publisher = World Science Fiction Society|work=thehugoawards.org| date=  1 September 2007| accessdate =1 September 2007}}</ref>

In a poll by ''[[SFX (magazine)|SFX]]'', 90,000 readers voted the farewell scene between the Doctor and Rose at Bad Wolf Bay as the greatest Sci-fi moment ever.<ref>{{cite web |url=http://www.radiotimes.com/news/2014-06-25/david-tennant-and-billie-pipers-doctor-who-goodbye-voted-greatest-sci-fi-scene-ever |title=David Tennant and Billie Piper's Doctor Who goodbye voted greatest sci-fi scene ever |author=Susanna Lazarus |date=25 June 2014 |website=[[Radio Times]] |accessdate=28 June 2014 }}</ref><ref>{{cite web |url=http://www.huffingtonpost.co.uk/2014/06/25/doctor-who-david-tennant-billie-piper-top-sci-fi-scene_n_5528675.html |title=Doctor Who 'Doomsday' Scene With David Tennant, Billie Piper Voted SFX's Greatest Sci-Fi Scene Ever - Who Else Triumphed? |date=25 June 2014 |website=[[The Huffington Post]] |accessdate=28 June 2014 }}</ref>

==References==
<!--<nowiki>
 See http://en.wikipedia.org/wiki/Wikipedia:Footnotes for an explanation of how
 to generate footnotes using the <ref> and </ref> tags, and the template below
</nowiki>-->
{{Reflist|30em}}

==External links==
{{wikiquote|Tenth Doctor#Doomsday .5B2.13.5D|"Doomsday"}}
{{TardisIndexFile|Doomsday}}
* [http://www.bbc.co.uk/mediaselector/check/doctorwho/ram/tardisode13?size=16x9&bgc=CC0000&nbram=1&bbram=1&bbwm=1&nbwm=1 TARDISODE 13]
* [http://www.bbc.co.uk/mediaselector/check/doctorwho/ram/13preview?size=16x9&bgc=CC0000&nbram=1&bbram=1&nbwm=1&bbwm=1 Episode trailer shown at the end of "Army of Ghosts"]
* [https://web.archive.org/web/20070120043211/http://downloads.bbc.co.uk/doctorwho/mood-commentary.mp3 Episode commentary by Russell T Davies, Julie Gardner and Phil Collinson] (MP3)
* [http://www.bbc.co.uk/doctorwho/episodes/2006/flash/homepages/index-doom.shtml "Doomsday" episode homepage]
* {{BBCDWnew| year=2006 | id=doomsday |title=Doomsday}}
* {{Brief| id=2006lm | title=Army of Ghosts" / "Doomsday|quotes=y}}
* {{Doctor Who RG| id=who_tv21 | title=Army of Ghosts" / "Doomsday|quotes=y}}
* {{cite web|url=http://www.gallifreyone.com/episode.php?id=2006–13|title=Doomsday|series=|publisher=[[Outpost Gallifrey]]}}{{dead link|date=September 2016|bot=medic}}{{cbignore|bot=medic}}
* {{tv.com episode|doctor-who-2005/doomsday-2-588119|Doomsday}}
*{{imdb episode|0756450|Doomsday}}

;Reviews
* {{OG review| id=2006-13 | title=Doomsday|quotes=y}}
* {{OG review| id=2006-1213 | title=Army of Ghosts" / "Doomsday|quotes=y}}
* {{DWRG| id=doom | title=Doomsday|quotes=y}}
* {{DWRG| id=armyday | title=Army of Ghosts" / "Doomsday|quotes=y}}

{{Doctor Who episodes|N2}}
{{Doctor Who episodes by Russell T Davies |state=autocollapse}}
{{Cybermen stories}}
{{Dalek stories}}

{{featured article}}

{{Use dmy dates|date=November 2012}}
{{DISPLAYTITLE:Doomsday (''Doctor Who'')}}

[[Category:Cybermen television stories]]
[[Category:Dalek television stories]]
[[Category:Tenth Doctor episodes]]
[[Category:2006 British television episodes]]
[[Category:Doctor Who stories set on Earth]]
[[Category:Television episodes about parallel universes]]
[[Category:Screenplays by Russell T Davies]]