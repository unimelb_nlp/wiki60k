{{accounting}} 
'''Net profit''', also referred to as the '''bottom line''', '''net income''', or '''net earnings''' is a measure of the profitability of a venture after accounting for all costs. It is the actual profit without inclusion of working expense in the calculation of gross profit. In a survey of nearly 200 senior marketing managers, 91% responded that they found the ''net profit'' metric very useful.<ref name=Marketing_Metrics>Farris, Paul W.; Neil T. Bendle; Phillip E. Pfeifer; David J. Reibstein (2010). ''Marketing Metrics: The Definitive Guide to Measuring Marketing Performance.'' Upper Saddle River, New Jersey: Pearson Education, Inc. ISBN 0137058292. Content from this book used in this article has been licensed for modification and reuse under the Creative Commons Attribute Share Alike 3.0 and Gnu Free Documentation licenses. See talk. The [[Marketing Accountability Standards Board (MASB)]] endorses the definitions, purposes, and constructs of classes of measures that appear in ''Marketing Metrics'' as part of its ongoing [http://www.commonlanguage.wikispaces.net/ Common Language in Marketing Project].</ref> In [[accountancy|accounting]], net profit is equal to the [[gross profit]] minus [[overhead (business)|overheads]] and [[interest]] payable for a given time period (usually, the [[accounting period]]).{{Citation needed|date=October 2009}}

A common synonym for ''net profit'' when discussing [[financial statement]]s (which include a [[balance sheet]] and an [[income statement]]) is ''the [[bottom line]]''.  This term results from the traditional appearance of an income statement which shows all allocated revenues and expenses over a specified time period with the resulting summation on the bottom line of the report.

In simplistic terms, net profit is the money left over after paying all the expenses of an endeavor. In practice this can get very complex in large organizations or endeavors.  The [[bookkeeper]] or [[accountant]] must itemise and allocate revenues and expenses properly to the specific working scope and context in which the term is applied.

Definitions of the term can, however, vary between the UK and US. In the US, net profit is often associated with net income or profit after tax (see table below).

The net [[profit margin]] percentage is a related ratio. This figure is calculated by dividing net profit by revenue or turnover, and it represents profitability, as a percentage.

== Purpose ==
"How does a company decide whether it is successful or not? Probably the most common way is to look at the net profits of the business. Given that companies are collections of projects and markets, individual areas can be judged on how successful they are at adding to the corporate net profit."<ref name=Marketing_Metrics />

== Construction ==
Net profit: To calculate net profit for a venture (such as a company, division, or project), subtract all costs, including a fair share of total corporate overheads, from the gross revenues or turnover.

:Net profit = sales revenue − total costs

Net profit is a measure of the fundamental profitability of the venture. "It is the revenues of the activity less the costs of the activity. The main complication is . . . when needs to be allocated" across ventures. "Almost by definition, overheads are costs that cannot be directly tied to any specific" project, product, or division. "The classic example would be the cost of headquarters staff." "Although it is theoretically possible to calculate profits for any sub-(venture), such as a product or region, often the calculations are rendered suspect by the need to allocate overhead costs." Because overhead costs generally don’t come in neat packages, their allocation across ventures is not an exact science.<ref name=Marketing_Metrics />
	
===Example===
	 
Here is how you reach net profit on a P&L (Profit & Loss) account:	
#[[Revenue|Sales revenue]] = price (of product) × quantity sold 	
#[[Gross profit]] = sales revenue − cost of sales and other direct costs 	
#Operating profit  = gross profit − overheads and other indirect costs	
#[[Earnings before interest and taxes|EBIT]] (earnings before interest and taxes) = operating profit + non-operating income
#Pretax profit ([[earnings before taxes|EBT]], earnings before taxes) = operating profit − one off items and redundancy payments, staff restructuring − interest payable 	
#'''Net profit''' = Pre-tax profit − tax 	
#[[Retained earnings]] = Profit after tax − dividends

==Accounting terms==
{{accounting deductions}}

==See also==
* [[Earnings before interest, taxes, depreciation and amortization|EBITDA]] (earnings before interest, taxes, depreciation and amortization)
* [[Net income]]
* [[Non-profit organization]]
* [[Revenue]]

== References ==
{{reflist}}

{{accounting nav}}

{{DEFAULTSORT:Net Profit}}
[[Category:Profit]]