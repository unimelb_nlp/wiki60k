'''Pardo's Push''' was an aviation maneuver carried out by then-Captain (Lt Col USAF Ret) John R. "Bob" Pardo, USAF in order to move his wingman's badly damaged [[F-4 Phantom II]] to friendly air space during the [[Vietnam War]].<ref name="valor"/>
__NOTOC__

==History==
Captain Bob Pardo (with Weapon System Officer 1st Lt Steve Wayne) and wingman Captain Earl Aman (with Weapon System Officer 1st Lt Robert Houghton) were assigned to the [[8th Fighter Wing|8th Tactical Fighter Wing]], [[433rd Tactical Fighter Squadron]], at [[Ubon Royal Thai Air Force Base]], [[Thailand]]. In March 1967, they were trying to attack a steel mill in [[North Vietnam]] just north of [[Hanoi]].
[[File:8tfwF-4D.jpg|thumb|right|F-4Ds of the [[8th Fighter Wing|8th Tactical Fighter Wing]] from Ubon Royal Thai Air Force Base.]]
On March 10, 1967, the sky was clear for a bombing run, but both F-4 Phantom IIs were hit by [[anti-aircraft fire]]. Aman's plane took the worst damage; his fuel tank had been hit, and he quickly lost most of his fuel. He did not have enough fuel to make it to a [[KC-135]] [[tanker aircraft]] over [[Laos]].

To avoid having Aman and Houghton bail out over hostile territory, Pardo decided to try pushing the airplane.<ref name="AFSmith"/> Pardo first tried pushing the plane using Aman's drag chute compartment but [[turbulence]] interfered. 
[[File:F4 Phantom at JBER tailhook.jpg|thumb|right|Tailhook of an F-4C Phantom II on display at the [[Joint Base Elmendorf-Richardson]].]]
Pardo then tried to use Aman's [[tailhook]] to push the plane, the Phantom having been originally designed as a naval aircraft for the [[U.S. Navy]] and [[U.S. Marine Corps]], equipped with a heavy duty tailhook for landings aboard aircraft carriers.  Aman lowered his tailhook and Pardo moved behind Aman until the tailhook was against Pardo's windscreen. Aman shut down both of his J79 jet engines. The push worked, reducing the [[rate of descent]] considerably, but the tailhook slipped off the windscreen every 15 to 30 seconds, and Pardo would have to reposition his plane. Pardo also struggled with a fire in one of his own engines and eventually had to shut it down. In the remaining 10 minutes of flight time, Pardo used the one last engine to slow the descent of both planes.

With Pardo's plane running out of fuel after pushing Aman's plane almost 88 miles,<ref name=Guernica/> the planes reached Laotian airspace at an altitude of {{convert|6000|ft}}. This left them about two minutes of flying time.<ref name="AFSmith"/> The pilots ejected, evaded capture, and were picked up by rescue helicopters.<ref name=boeing/>

Pardo was initially reprimanded for not saving his own aircraft.<ref name=vietmag/> However, in 1989, the military re-examined the case and awarded both Pardo and Wayne the [[Silver Star]] for the maneuver, two decades after the incident.<ref name="valor"/><ref name=Davies/><ref>http://www.veterantributes.org/TributeDetail.php?recordID=986</ref>

==Postscript==
Pardo and Aman eventually completed their Air Force careers, both retiring in the rank of lieutenant colonel.  In later years, Pardo, learning that Aman was suffering from [[Amyotrophic lateral sclerosis|Lou Gehrig's disease]] and had lost his voice and mobility, created the '''Earl Aman Foundation''' that raised enough money to buy Aman a [[voice synthesizer]], a motorized wheelchair, and a computer. The foundation and the '''Red River Valley Fighter Pilots Association''' later raised funds to pay for a van, which Aman used for transportation until his death.<ref name="valor"/>

The flight maneuver was later the subject of [[list of JAG episodes#ep88|an episode]] of  ''[[JAG (TV series)|JAG]]''; the episode's credits "saluted" Pardo for his courage and ingenuity.

==See also==
{{Portal|United States Air Force}}
*[[James Robinson Risner]], a pilot who used his [[F-86 Sabre]] to push a fellow pilot's plane during the [[Korean War]].

==References==
{{Reflist|refs=
<ref name=Davies>{{cite book|title=US Air Force F-4 Phantom II MiG Killers 1965-68|author=Davies, Peter E.|page=43 |url= https://books.google.com/books?id=sOpMuLgVy0sC&pg=PA43&lpg=PA43&dq=%22pardo's+push%22|publisher=Osprey Publishing|year=2004 | isbn= 978-1-84176-656-0}}</ref>

<ref name="AFSmith">{{cite web |url=http://www.af.mil/news/airman/1296/pardo.htm |title=Pardo's Push|last=Smith|first=Steve|work=Airman|publisher=United States Airforce |date= December 1996 |accessdate=2008-12-14 |archiveurl = https://web.archive.org/web/20080204013921/http://www.af.mil/news/airman/1296/pardo.htm |archivedate = 2008-02-04}}</ref>

<ref name=vietmag>{{cite web|url=http://www.historynet.com/pardos-push-an-incredible-feat-of-airmanship.htm|title=Pardo's Push: An Incredible Feat of Airmanship|publisher=Vietnam Magazine|accessdate=2014-09-04}}</ref>

<ref name=boeing>{{cite web|url=http://www.boeing.com/defense-space/military/f4/pardopush.htm|title=Pardo's Push|publisher=Boeing|accessdate=2008-12-14}}</ref>

<ref name=Guernica>{{cite web|url=http://www.foxnews.com/story/0,2933,505345,00.html |title=American Heroes: Pilots of Rolling Thunder|last=Guernica | first=Kelly|publisher=Fox News|date=March 5, 2009 |accessdate= 2009-05-05}}</ref>

<ref name="AFSmith">{{cite web |url=http://www.af.mil/news/airman/1296/pardo.htm |title=Pardo's Push|last=Smith|first=Steve|work=Airman|publisher=United States Airforce |date= December 1996 |accessdate=2008-12-14 |archiveurl = https://web.archive.org/web/20080204013921/http://www.af.mil/news/airman/1296/pardo.htm |archivedate = 2008-02-04}}</ref>

<ref name="valor">{{cite web|url=http://www.airforce-magazine.com/MagazineArchive/Pages/1996/October%201996/1096valor.aspx|title=Valor: Pardo's Push|last=Frisbee|first=John L.|date=October 1996|publisher=AIR FORCE Magazine| archiveurl= https://web.archive.org/web/20120219010510/http://www.airforce-magazine.com/MagazineArchive/Pages/1996/October%201996/1096valor.aspx |archivedate= 2012-02-19| accessdate=2014-08-19}}</ref>

}}

[[Category:Vietnam War]]
[[Category:Aerial maneuvers]]
[[Category:Accidents and incidents involving United States Air Force aircraft]]