{{Infobox company
| name                 = Kirow Ardelt GmbH Kranbau Eberswalde
| logo                 = [[File:Logo Ardelt.svg|200px]]
| logo_alt             = Logo of Kirow Ardelt GmbH
| founded              = 1902
| hq_location_city     = [[Eberswalde]], [[Brandenburg]], Germany
| num_employees        = 180 (2010)
| revenue              = EUR 53 million
| revenue_year         = 2010
| products             = cranes, harbour equipment
| website              = [http://www.ardelt.de www.ardelt.de]
}}
'''Kirow Ardelt GmbH Eberswalde''', referred to as '''Ardelt''', is a German crane manufacturer. The company specialises in manufacturing double jib [[level luffing crane]]s which are based on the patented double jib principle. Ardelt has produced more than 4,700 harbour cranes.<ref name="HermannSimon">Hermann Simon: ''Hidden Champions des 21. Jahrhunderts: Die Erfolgsstrategien unbekannter Weltmarktführer.'' Campus-Verlag, Frankfurt am Main 2007, ISBN 978-3-593-38380-4, S. 17</ref>  Ardelt also makes single jib-level luffing cranes and balancer cranes. The range also includes a mobile harbour crane with a high gantry that is also based on the double jib system.

== History ==

=== Foundation and development ===

The company’s roots go back to 1902 when an engineer called Robert Ardelt (1847–1925) founded an engineering firm in the flourishing town of Eberswalde. In 1904 the company was already called Robert Ardelt & Söhne Maschinenfabrik, indicating that it was family-run. Responsibility for the business was then gradually handed over to the sons Max, Robert, Rudolf, and Paul.

In 1911 work started on expanding the site at the corner of Heegermühler Strasse/Boldtstrasse. The company was known the world over for manufacturing foundry machinery. It was particularly famous for a pipe ramming machine, patented in 1907. This device enabled the manufacture of cast-iron pipes on an industrial scale for the first time. However, cranes were the main products made. In 1912 the factory was inaugurated at its present location. In 1913 all four sons were made equal partners and the company’s title was changed to Ardeltwerke GmbH.  

In 1918 the company employed 2350 people. In 1921 a separate crane construction department was established. In 1932 the double jib system for slewing cranes was invented and patented.<ref name="Patent">Ministerium für Wirtschaft Brandenburg: [http://www.wirtschaft.brandenburg.de/cms/detail.php?id=337537&_siteid=20 Kranbau Eberswalde macht die Häfen der Welt schneller]</ref> The special feature of this jib system is that the hook remains at a constant height when the direction is changed. Until the present day, these cranes (also called gantry luffing and slewing cranes) are the key products in the company's product range.

Between 1927 and 1934 Ardelt played a major role in constructing the [[Niederfinow Boat Lift]]. In the 1930s locomotives were also produced.

=== Armaments production in the Third Reich from 1933 to 1945 ===

When war broke out, the company switched to producing armaments. In 1935 the company's workforce was around 4,000. During the Second World War the workforce swelled to over 8,000 which was also the result of forced labour.<ref name="GeschichtswissenschaftlichesInstitut">Geschichtswissenschaftliches Institut Eberswalde; BR.MSW.42 und EW.MSW.25</ref><ref name="DanutaCzech">Danuta Czech: ''Kalendarium der Ereignisse im Konzentrationslager Auschwitz-Birkenau 1939-1945''. S. 584–585, Rowohlt Verlag, Reinbek 1989, ISBN 3-498-00884-6</ref><ref name="psverlag">Holger Kliche, Kurt Berus und Ewa Stendel: [http://www.psverlag.de/bbp/bbp0602l.pdf ''Ardelt-Werke – Waffenschmiede des Führers'' (Teil&nbsp;1)] (PDF) Hrsg.: Geschichtswissenschaftliches Institut Eberswalde. Barnimer Bürgerpost.  {{webarchive |url=https://web.archive.org/web/20110719073529/http://www.psverlag.de/bbp/bbp0602l.pdf |date=July 19, 2011 }}</ref><ref name="werkbahn">http://www.werkbahn.de/eisenbahn/lokbau/ardelt.htm '' Ardelt-Werke GmbH, Eberswalde''</ref><ref>Holger Kliche: Ardelts Raketenmänner; Geschichtswissenschaftliches Institut Eberswalde, 1. Auflage 2009</ref>

=== State-owned from 1945 to 1990 ===

After 1945 the Ardeltwerke in the Soviet occupied zone were expropriated. The family founded new plants in West Germany in Wilhelmshaven and Osnabrück. In 1953 these were acquired by [[Krupp|Friedrich Krupp AG]] and were renamed Krupp-Ardelt GmbH. After the death of Rudolf Ardelt jr., the West German company was renamed Krupp-Kranbau in 1964.

The machines and equipment in the nationalised Ardeltwerke in Eberswalde were dismantled in 1945 to contribute towards reparations and transported to the Soviet Union. In 1948, as a state-owned enterprise the company called itself ABUS – Kranbau Eberswalde – VEB and started producing cranes again with 37 employees. In 1949 some 13 cranes had already been manufactured. In 1951, the company was renamed VEB Kranbau Eberswalde. In 1963 VEB Kranbau Eberswalde’s brief was to build cranes. By 1967 some 1500 cranes had been delivered. In 1974 the 2,500th crane rolled off the production lines. Subsequently Kranbau Eberswalde was integrated into the newly founded TAKRAF combine of state-owned enterprises. In the following years and until 1989, Ardelt mainly supplied cranes to the USSR, but also to South America, Africa and Europe. In 1989 over 3,000 employees and around 450 trainees worked in the firm. For the GDR, crane construction in Eberswalde was a successful source of hard currency. However, it was virtually impossible for the company to benefit from its own profits and re-invest. As a result, towards the end of the GDR, the machines and equipment were outdated and no longer adequate enough to allow the company to continue development in reunified Germany.<ref name="bare_url">http://hardconn.beepworld.de/vebkranbaueberswald.htm ''VEB Kranbau Eberswalde 1948-1990''</ref>

=== Privatisation from 1990 ===

The privatisation of the company started after the GDR published its ''Directive on converting state-owned combines, companies and facilities to stock companies'' on 1 March 1990. The company was converted into a [[GmbH]] (limited liability company). Business continued to be conducted under the company’s Kranbau Eberswalde GmbH name. In 1991 the slewing crane product range was extended to include container loading equipment and hydraulically operated balance cranes in 1992. 

The [[Treuhandanstalt]] (the government responsible for privatising concerns owned by the East German government) sold Kranbau Eberswalde GmbH in 1994 to Vulkan Kocks GmbH. The company then became a member of Bremen-based Vulkan AG and was renamed Vulkan Kranbau Eberswalde GmbH. In 1996 the workforce numbered just 239 people. In 1996 Vulkan AG went into receivership and Vulkan Kocks GmbH was taken over by Kirow Leipzig AG in 1997. The company was then called Kirow Leipzig KE Kranbau Eberswalde GmbH. 

In 2000 Kranbau Eberswalde was merged with the mechanical engineering company Kirow Leipzig Rail & Port AG and it continued operating under the name of KE Kranbau Eberswalde AG. In the same year the company was awarded the Berlin-Brandenburg Innovation Prize for developing the feeder server, the world's first mobile container bridge.<ref name="Patent" /> In 2002, the company celebrated its 100th anniversary. In 2008 the company returned to its original family name of Ardelt and is now officially called Kirow Ardelt GmbH.<ref name="HR">Handelsregister: [https://www.handelsregister.de/ Offizielle Website]</ref><ref>[http://www.neues-deutschland.de/artikel/130685.kranbau-verhebt-sich-mit-dem-namen.html ''Kranbau verhebt sich mit dem Namen'']. In: ''[[Neues Deutschland]]'', 21. Juni 2008</ref>

== References ==

{{reflist}}

== External links ==
{{Commons category|Kranbau Eberswalde|Kranbau Eberswalde|S}}
* {{Official website|www.kranunion.de}}
* [http://www.barnim.de/Besonderes.1154+M5c01635986b.0.html History of the Ardelt factory]{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}} 
* [http://www.werkbahn.de/eisenbahn/lokbau/ardelt.htm History of the Ardelt factory]


[[Category:Volkseigene Betriebe]]
[[Category:Companies based in Brandenburg]]
[[Category:Companies established in 1902]]
[[Category:Crane manufacturers]]
[[Category:Industrial machine manufacturers]]
[[Category:German brands]]
[[Category:Eberswalde]]