{{Infobox military person
|name=Vincent O'Rourke
|birth_name= Vincent Patrick O'Rourke
|birth_date= {{birth date|1922|05|19}}
|death_date= {{death date and age|2011|07|14|1922|05|19}}
|birth_place= [[New York City]]
|death_place= [[Coronado, California]]
|placeofburial= [[Arlington National Cemetery]]
|placeofburial_label=
|image= File:CAPT Vincent Patrick O'Rourke USS Tripoli.jpg
|image_size= 250px
|caption= O'Rourke while he was Commanding officer of USS ''Tripoli''
|nickname= 
|allegiance= {{Flag|United States of America|1974}}
|branch= {{flag|United States Navy}}
|serviceyears= 1942{{spaced ndash}}1974
|rank= [[File:US-O6 insignia.svg|24px]] [[Captain (United States O-6)|Captain]]
|servicenumber= 0-320529
|commands= {{plainlist|
*{{USS|Tripoli|LPH-10}}
*{{USS|Rainier|AE-5}}
}}
|unit= VT-47 on {{USS|Bataan|CVL-29}}
|battles= [[World War II]]<br />[[Vietnam War]]
|awards= {{ribbon devices|other_device= |number= 1|type= award-star|ribbon= Navy Cross ribbon.svg|width= 60}} [[Navy Cross]] (2)<br />{{ribbon devices|other_device= |number= 0|type= award-star|ribbon= Legion of Merit ribbon.svg|width= 60}} [[Legion of Merit]]<br />{{ribbon devices|other_device= |number= 0|type= award-star|ribbon= Distinguished Flying Cross ribbon.svg|width= 60}} [[Distinguished Flying Cross (United States)|DFC]]<br />{{ribbon devices|other_device= nv|number= 0|type= award-star|ribbon= Bronze Star ribbon.svg|width= 60}} [[Bronze Star Medal]]<br />{{ribbon devices|other_device= |number= 4|type= award-star|ribbon= Air Medal ribbon.svg|width= 60}} [[Air Medal]] (5)
|laterwork=
|signature=
|spouse= Harriett Julia Sokal (1922–2007)
}}

'''Vincent Patrick O'Rourke''' (19 May 1922{{spaced ndash}}14 July 2011) was a [[World War II]] [[United States]] [[naval aviator]] in the [[Pacific Ocean theater of World War II|Pacific theater]] and two time recipient of the [[Navy Cross]], the [[United States Navy|Navy's]] second highest award for valor after the [[Medal of Honor]]. After the war, he was [[Commanding officer]] of a number of aviation units as well as {{USS|Rainier|AE-5}} and {{USS|Tripoli|LPH-10}}.

== Background and education ==
Vincent Patrick O'Rourke was born on 19 May 1922 in [[New York City]]<ref name="Obit" /> to Francis and Alice O'Rourke. He grew up in [[Forest Hills, Queens]].<ref name="Obit" /> In 1940, he began studying [[Aeronautical Engineering]] at the [[Brooklyn Polytechnic Institute]].<ref name="Obit" /> In 1954, he received a [[Bachelor of Science]] degree in Aeronautical Engineering from the [[Naval Postgraduate School]].<ref>{{cite web |url= https://calhoun.nps.edu/bitstream/handle/10945/41194/1954-06%20Graduation%20Ceremony%20of%20the%20Engineering%20School.pdf |title= Graduation Ceremony of the Engineering School |date= 3 June 1954 |publisher= Naval Postgraduate School |page= 8 |accessdate= 10 September 2015 }}</ref> He also received a [[Master of Science]] degree from [[Purdue University]].<ref name="1964 Bio" /><ref name="Obit" /> He also graduated from the [[United States Naval Test Pilot School|Naval Test Pilot School]] and the [[Armed Forces Staff College]].<ref name="1970 Bio" /><ref name="Obit" /> He was married to Harriett Julia Sokal (1922–2007) of [[Middle Village, Queens]]<ref name="1970 Bio" /> for 63 years.<ref name="Obit" />

== Navy Career ==
O'Rourke enlisted in the Navy 27 July 1942 as an [[aviation cadet]]. He became an [[Ensign (rank)|Ensign]] in October 1943.<ref name="Obit" /> He served with VF-74 aboard {{USS|Kasaan Bay|CVE-69}} the Mediterranean theater.<ref name="1970 Bio" /> On 9 July 1944, while he was landing on ''Kasaan Bay'', his TBM-1C hit the catwalk and severely damaged a wing, but there were no injuries.<ref>{{cite report |url= https://www.fold3.com/image/279778127 |title= USS Kasaan Bay War Diary July 1944 |page= 11 |accessdate= 13 September 2015 |publisher = [[Ancestry.com]] |subscription=yes |quote= … without incident until 1804, at which time TBM-1C-#43 (BuAer No.46260) piloted by Ens. V. P. O'Rourke, A-V(N), USNR plunged off the flight deck into the cat walk, on making a landing, without injury to personnel but severely damaging its port wing. (9 July 1944)}}</ref>

He served as a pilot with torpedo squadron 47 ([[VT-47]]) on {{USS|Bataan|CVL-29}} flying a [[Grumman TBF Avenger|TBM-3 Avenger]] and was awarded a number of medals for valor including two Navy Cross medals and a Bronze star.<ref name="Navy Cross" /> LTJG O'Rourke received his first Navy Cross was for his attack on the Japanese fleet at [[Kobe]] Bay on 19 March 1945. He bombed a large aircraft carrier under heavy anti-aircraft fire and smoke-screening.<ref name="Navy Cross" /> His second Navy Cross related to his role in the attack on the [[Japanese cruiser Tone (1937)|Japanese cruiser Tone]] during the 28 July 1945 [[Bombing of Kure (July 1945)|attack on]] [[Kure, Hiroshima|Kure]] Harbor.<ref name="Navy Cross" /> O'Rourke flew just over 216 hours on 33 strike missions during VT-47's 18 March 1945 to 15 August 1945 combat deployment aboard ''Bataan''.<ref>{{cite report |url= https://www.fold3.com/image/302039327 |section= Enclosure D – Pilot Achievement Table |title= VT-47 War History 18 March to August 1945 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}</ref>

He served as [[Executive officer]] of fighter squadron [[VF-124]] and Commanding officer of fighter squadrons [[VF-96]] and [[VF-142]] prior to 1964.<ref name="1964 Bio" />

In 1964, [[Commander (United States)|Commander]] O'Rourke was executive officer of {{USS|Oriskany|CVA-34}} after serving there as [[Modern United States Navy carrier air operations#Air Officer|Air Officer]].<ref name="1964 Bio">{{cite journal |url= https://www.fold3.com/image/302467721 |title= Executive Officer |journal= USS Oriskany |year= 1964 |page= 7 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}</ref> He became a [[Captain (United States O-6)|Captain]] in December 1964.<ref name="Register 1965" /> O'Rourke was commanding officer of {{USS|Rainier|AE-5}} from 5 August 1966 to 2 August 1967.<ref>{{cite web |url= http://www.navsource.org/archives/09/05/0505a.htm |title= Commanding Officers of USS Rainier (AE-5) |website= NavSource.org |accessdate= 6 September 2015 }}</ref> O'Rourke was commanding officer of {{USS|Tripoli|LPH-10}} from 26 August 1970 to 10 September 1971.<ref>{{cite web |url= http://www.navsource.org/archives/10/11/1110a.htm |title= Commanding Officers of USS Tripoli (LPH-10) |website= NavSource.org |accessdate= 6 September 2015 }}</ref><ref name="1970 Bio">{{cite journal |url= https://www.fold3.com/image/301388542 |title= Commanding Officer |journal= USS Tripoli |year= 1970 |page= 15 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}</ref>

O'Rourke retired to [[Coronado, California]] in July 1974 and was a consultant in the US, Europe and Middle East.<ref name="Obit" /><ref name="Register 1984" /> He died 14 July 2011 in Coronado.<ref name="Obit">{{cite news |url= http://www.coronadonewsca.com/obituaries/vincent-patrick-o-rourke-captain-usn-ret/article_c0aff040-b954-11e0-b36a-001cc4c03286.html |title=  Vincent Patrick O’Rourke, Captain USN (ret) |newspaper= Coronado Eagle & Journal |date= 28 July 2011 |accessdate= 5 September 2015 }}</ref> He was buried at [[Arlington National Cemetery]], section 55, grave 3495 on 11 October 2012.<ref>{{cite web |url= http://www.arlingtoncemetery.mil/ANCExplorer |title= ANC Explorer |website= Arlington National Cemetery |accessdate= 5 September 2015 }}</ref>

== Awards ==
During his career in the Navy, O'Rourke received the [[Navy Cross]] with Gold Star for second award,<ref name="Navy Cross">{{cite web |url= http://valor.militarytimes.com/recipient.php?recipientid=20742 |title= Valor Awards for Vincent Patrick O'Rourke |website= Military Times |accessdate= 5 September 2015 }}</ref> [[Legion of Merit]], [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]], [[Bronze Star]] with [[valor device]], [[Air Medal]] with 4 Gold Stars for subsequent awards and the [[Meritorious Service Medal (United States)|Meritorious Service Medal]].<ref name="Obit" />

=== Award Citations ===
[[File:Navycross.jpg|thumb|upright|Navy Cross]]
The Commander of 1st Carrier Task Force awarded O'Rourke the Navy Cross for actions 19 March 1945 during a raid on Kobe Bay:
{{quotation|The President of the United States of America takes pleasure in presenting the Navy Cross to Lieutenant, Junior Grade Vincent Patrick O'Rourke (NSN: 0-320529), United States Naval Reserve, for extraordinary heroism in operations against the enemy while serving as Pilot of a carrier-based Navy Torpedo Plane in Torpedo Squadron FORTY-SEVEN (VT-47), attached to the U.S.S. ''Bataan'' (CVL-29), in action on 19 March 1945, in action against units of the Japanese Fleet in Kobe Bay, Honshu, Japan, on 19 March 1945. Skillfully maneuvering to locate his target under a heavy enemy smoke screen, Lieutenant, Junior Grade, O'Rourke launched an attack on a large Japanese carrier and, in the face of intense hostile anti-aircraft fire, scored several direct hits on the vessel. By his courage under fire, skilled airmanship and determination in the completion of his mission, Lieutenant, Junior Grade, O'Rourke contributed materially to the success of the attack in removing a menace to our own Fleet operations. His devotion to duty throughout was in keeping with the highest traditions of the United States Naval Service.<ref name="Navy Cross" />|Commander 1st Carrier Task Force: Serial 0948 (July 6, 1945)}}

The Commander of 2nd Carrier Task Force, Pacific awarded O'Rourke the Navy Cross for actions on 28 July 1945 during a raid on Kure Harbor and his role in sinking Japanese cruiser ''Tone''.
{{quotation|The President of the United States of America takes pleasure in presenting a Gold Star in lieu of a Second Award of the Navy Cross to Lieutenant, Junior Grade Vincent Patrick O'Rourke (NSN: 0-320529), United States Naval Reserve, for extraordinary heroism in operations against the enemy while serving as Pilot of a carrier-based Navy Torpedo Plane in Torpedo Squadron FORTY-SEVEN (VT-47), attached to the U.S.S. ''Bataan'' (CVL-29), in action against powerful units of the Japanese Fleet at Kure Harbor, Honshu Island, Japan, on 28 July 1945. Braving intense and accurate anti-aircraft gunfire from shore batteries and several Naval units in the vicinity, Lieutenant, Junior Grade, O'Rourke vigorously pressed home an attack against the enemy heavy cruiser ''Tone'' and, despite a heavy barrage of protective gunfire from the vessel, succeeded in scoring two hits on the port quarter and amidships abaft the stacks. His courage and devotion to duty contributed materially to her sinking immediately after the attack and were in keeping with the highest traditions of the United States Naval Service.<ref name="Navy Cross" />|Commander 2d Carrier Task Force Pacific: Serial 01880 (September 20, 1945)}}

== Dates of Rank ==
* 1 October 1943 Ensign.<ref>{{cite web |url= http://search.ancestry.com/search/db.aspx?dbid=2345 |work= U.S., Select Military Registers, 1862–1985 [database on-line] |location= Provo, UT, USA |publisher= Ancestry.com Operations, Inc. |year= 2013 |accessdate= 6 September 2015 |subscription=yes |title= Commissioned and Warrant Officers of the United States Naval and Reserve, 1 July 1944 |quote=320529 ORourke Vincent P ENS A-VN / / / / 10 01 43 22 7 27 42}}</ref>
* 1 January 1949 Lieutenant.<ref>{{cite web |url= http://search.ancestry.com/search/db.aspx?dbid=2345 |work= U.S., Select Military Registers, 1862–1985 [database on-line] |location= Provo, UT, USA |publisher= Ancestry.com Operations, Inc. |year= 2013 |accessdate= 6 September 2015 |subscription=yes |title= U.S. Navy and Marine Corps Officers, Lieutenants Line, 1950 |quote= 16187 ORourke Vincent Patrick LT 1 01 49 5 19 22 10 01 43 2 / 2}}</ref>
* 1 July 1959 Commander.<ref>{{cite web |url= http://search.ancestry.com/search/db.aspx?dbid=2345 |work= U.S., Select Military Registers, 1862–1985 [database on-line] |location= Provo, UT, USA |publisher= Ancestry.com Operations, Inc. |year= 2013 |accessdate= 6 September 2015 |subscription=yes |title= U.S. Navy and Marine Corps Officers, Line Commanders, 1960 |quote= 11797 95 ORourke Vincent Patrick 52 44 1310 07 01 59 5 19 22 2 02 40 49 7 27 42}}</ref>
* 1 December 1964 Captain.<ref name="Register 1965">{{cite web |url= http://search.ancestry.com/search/db.aspx?dbid=2345 |work= U.S., Select Military Registers, 1862–1985 [database on-line] |location= Provo, UT, USA |publisher= Ancestry.com Operations, Inc. |year= 2013 |accessdate= 6 September 2015 |subscription=yes |title=  U.S. Navy and Marine Corps Officers, Unrestricted Line Captains, 1965 |quote= 002830 40 Orourke Vincent Patrick 52 44 1310 12 01 64 05 19 22 2 01 09 22 2 53 07 27 42}}</ref>
* July 1974 Captain, Retired.<ref name="Register 1984">{{cite web |url= http://search.ancestry.com/search/db.aspx?dbid=2345 |work= U.S., Select Military Registers, 1862–1985 [database on-line] |location= Provo, UT, USA |publisher= Ancestry.com Operations, Inc. |year= 2013 |accessdate= 6 September 2015 |subscription=yes |title= Retired Commissioned and Warrant Officers, Regular and Reserve, of the United States Navy, 1 Oct 1984 |quote= Orourke Vincent Patrick M 1313 CAPT 0774 09 CAPT 120164 22}}</ref>

== Bibliography ==
* {{cite report |title= Personnel practices and the concepts of professional employees |last= O'Rourke |first= Vincent P. |year= 1955 |url= https://archive.org/details/personnelpractic00orou }} (Masters Thesis)

== Further reading ==
* Relating to bombing of the partially constructed [[Unryū-class aircraft carrier]] ''Ikoma'' at Kobe Bay: {{cite report |url= https://www.fold3.com/image/300860150 |title= USS Bataan Aircraft Action Report for March 19, 1945 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}
* {{cite report |url= https://www.fold3.com/image/300344889 |title= COMAIRGRU 47 – Report of air operation against the Ryukyu Islands and Japan, 3/1/45-8/15/45, including Squadron ACA |page= 2 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}
* Relating to March 19, 1945 strike: {{cite report |url= https://www.fold3.com/image/302039226 |title= VT-47 War History |page= 2 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}
* Relating to March 19, 1945 strike: {{cite report |url= https://www.fold3.com/image/302039258 |title= VT-47 War History |page= 37 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}
* Photo of Kobe Bay raid from Bataan after action report: {{cite report |url= https://www.fold3.com/image/296170270 |title= USS Bataan – Report of operations during carrier air strikes on Japan, Ryukyus & Japanese Task Force, 3/18/45-5/29/45 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}
* Air Group 47 photos on ''Bataan'' from USS ''Bataan'' history book: {{cite report |url= https://www.fold3.com/image/302156147 |title= History of USS Bataan August 1943 to October 1945 |page= 69 |accessdate= 6 September 2015 |publisher = [[Ancestry.com]] |subscription=yes}}

== References ==
{{reflist|30em}}

== External links ==
* {{cite web |url= http://www.homeofheroes.com/members/02_NX/citations/03_wwii-nc/nc_06wwii_navyN.html |title= WWII Navy Cross |website= Home of the Heroes |accessdate= 13 September 2015 }}
* {{cite journal |url= http://www.navy.mil/ah_online/archpdf/ah194904.pdf |title= Decorations and Citations – Navy Cross |journal= All Hands |issue= April 1949 |page= 57}}
* {{Find a grave|110469333}}

{{DEFAULTSORT:O'Rourke, Vincent P.}}
[[Category:1922 births]]
[[Category:2011 deaths]]
[[Category:People from Forest Hills, Queens]]
[[Category:People from Coronado, California]]
[[Category:Polytechnic Institute of New York University alumni]]<!--(for Polytechnic Institute of Brooklyn alumni)-->
[[Category:Purdue University alumni]]
[[Category:Naval Postgraduate School alumni]]
[[Category:Joint Forces Staff College alumni]]
[[Category:United States Naval Test Pilot School alumni]]
[[Category:United States Naval Aviators]]
<!--Awards-->
[[Category:Recipients of the Navy Cross (United States)]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Air Medal]]