{{Use dmy dates|date=September 2015}}
{{Infobox military person
|name= Hjalmar Riiser-Larsen
|image=Bundesarchiv Bild 102-14193, Hjalmar Riiser-Larsen.jpg
|image_size=260px
|caption=Riiser-Larsen in 1925
|allegiance=[[Norway]]
|rank=Major General
|commands=[[Royal Norwegian Navy Air Service]]<br />[[Royal Norwegian Air Force]]
|family= 
|nickname=
|birth_date=7 June 1890
|death_date= 3 June 1965 (aged 74)
|birth_place=Norway
|serviceyears=1909–1933, 1940–1946
|laterwork=Polar exploration<br />Aviation leader
|battles=[[World War II]]
|awards=
}}

'''Hjalmar Riiser-Larsen''' (7 June 1890 – 3 June 1965) was a [[Norwegians|Norwegian]] [[Aviator|aviation pioneer]], [[polar explorer]] and businessman. Among his achievements, he is generally regarded as the founder of the [[Royal Norwegian Air Force]].

Riiser-Larsen was born in [[Oslo]], Norway, in 1890. In 1909, aged nineteen, he joined the [[Norwegian Naval Academy]] and in 1915 the newly formed [[Royal Norwegian Navy Air Service]] (RNoNAS). After [[World War I]], he served as the acting head of the RNoNAS's factory until a more senior officer was appointed. In 1921, he joined the Aviation Council, then part of the [[Ministry of Defence (Norway)|Norwegian Ministry of Defence]], as a secretary. This gave him the opportunity to study the fledgling military and civil aviation infrastructure for which the Council was responsible. He also became a frequent pilot on the air routes used by the new aviation companies.

== Polar exploration ==
=== Flying over the North Pole ===
[[File:Hjalmar Riiser-Larsen Mondadori.jpg|thumb|Riiser-Larsen during one of Amundsen's expeditions]]
Riiser-Larsen's years of polar exploration began in 1925 when his compatriot [[Roald Amundsen]], the famed polar explorer, asked him to be his deputy and pilot for an attempt to fly over the [[North Pole]]. Riiser-Larsen agreed and secured the use of two [[Dornier Do J Wal]] seaplanes. The expedition, however, was forced to land close to the Pole, badly damaging one of the planes. After twenty-six days on an [[ice shelf]], first trying to shovel tons of snow to create an airstrip, until someone suggested the easier way of tramping the snow surface, the expedition's six members squeezed themselves into the remaining plane. Riiser-Larsen somehow managed to coax the overloaded plane into the air and flew the expedition back to the coast of Northern Svalbard..

The following year, Riiser-Larsen rejoined Amundsen for another attempt to fly over the Pole, this time with Italian aeronautical engineer [[Umberto Nobile]] in his recently renamed [[airship]], the [[Norge (airship)|''Norge'']]. Leaving [[Spitsbergen]] on 11 May 1926, the ''Norge'' completed the crossing two days later, landing near [[Teller, Alaska]]. The flight is considered by many to be the first successful flight over the North Pole, as the other claimants, [[Frederick Cook]], [[Robert Peary]] and [[Richard Evelyn Byrd|Richard Byrd]], were unable to verify their attempts in full.

In 1928, Riiser-Larsen became involved in searching the Arctic for Nobile after he had made a successful flight to the Siberian islands and visited the North Pole once more, but crashed near the coast of the North Eastern part of Svalbard. Riiser-Larsen also became involved in a search for Amundsen, when he as passenger in a French naval flyingboat went missing while he was en route to join the search for Nobile. Eventually Nobile and most of his team were found, but Amundsen was not.

=== The ''Norvegia'' expeditions ===
The ''Norvegia'' expeditions were a sequence of [[Antarctic]] expeditions financed by the Norwegian shipowner and [[whaling]] merchant [[Lars Christensen]] during the late 1920s and 1930s. Ostensibly their goal was scientific research and the discovery of new whaling grounds, but Christensen also requested permission from the Norwegian Foreign Office to claim for Norway any uncharted territory that was found. By the end of the second expedition, two small islands in the [[Southern Ocean]], [[Bouvet Island]] and [[Peter I Island]], had been annexed.

In 1929 Christensen decided to include aeroplanes in the next expedition and appointed Riiser-Larsen its leader. Riiser-Larsen then supervised and took part in mapping most of the Antarctic in this and three further expeditions. More territory was also annexed, this time the large area of the continent known as [[Queen Maud Land]].

== Commerce and war ==
In 1939, the Norwegian military was downsized and Riiser-Larssen was among those officers finding themselves out of work. However, he was quickly offered a new job by the shipping company [[Fred. Olsen & Co.]] as manager of its newly formed airline, [[Det Norske Luftfartsselskap|DNL]]. He invited some former naval pilots to join the airline and soon made it the most successful in Norway. In 1946, DNL would be one of the four [[Scandinavia]]n airlines merged to create the present-day [[Scandinavian Airlines System]] (SAS).

When [[Nazi Germany]] [[German invasion of Norway|invaded Norway in 1940]], Riiser-Larsen rejoined the Royal Norwegian Navy Air Service. However, both the [[Norwegian Army Air Service|Norwegian Army]] and Royal Norwegian Navy Air Services were quickly overwhelmed by the [[Wehrmacht]] before he saw combat. Instead, he accompanied the Norwegian cabinet and military leaders [[Norwegian Armed Forces in exile|into exile]] in London, before moving on to [[Toronto|Toronto, Ontario]], [[Canada]], to become the first commander of the Norwegian air forces' training camp, "[[Little Norway]]".

At the beginning of 1941, Riiser-Larsen returned to London to take up the post of Commander in Chief of the Naval Air Service; then of the Combined Arms Air Force; and finally, in 1944, of the fully amalgamated [[Royal Norwegian Air Force]]. By the end of the war, however, many of the pilots under his command had become critical of his leadership. He resigned, bitterly, from the Air Force in 1946.
[[File:Riisermotz.jpg|thumb|Riiser-Larsen (left) with [[Birger Fredrik Motzfeldt]] in 1959]]

== Return to business ==
In 1947, Riiser-Larsen again became the head of DNL, a few months before it merged with [[Det Danske Luftfartselskab|DDL]], [[Svensk Interkontinental Lufttrafik|SIL]] and [[Aktiebolaget Aerotransport|ABA]] to create [[Scandinavian Airlines System|SAS]]. He then became an advisor to the SAS executive and a regional manager with responsibility for transcontinental air routes. One of these routes, although established after his retirement in 1955, represented the "fulfilment of a vision" : the route to North America over the North Pole.

Riiser-Larsen died on 3 June 1965, four days before his seventy-fifth birthday.

== See also ==
*[[Aviation in Norway]]
* [[Riiser-Larsen Sea]]

== Bibliography ==
{{Commons category}}
*Hjalmar Riiser-Larsen, ''Femti År for Kongen'' (''Fifty Years for the King'', Riiser-Larsen's autobiography), Oslo: Gyldendal Norsk Forlag, 1958.

{{Polar exploration|state=collapsed}}
{{Authority control}}

{{DEFAULTSORT:Riiser-Larsen, Hjalmar}}
[[Category:1890 births]]
[[Category:1965 deaths]]
[[Category:Aviation pioneers]]
[[Category:Explorers of Antarctica]]
[[Category:Explorers of the Arctic]]
[[Category:Norwegian aviators]]
[[Category:Norwegian businesspeople]]
[[Category:Norwegian polar explorers]]
[[Category:Royal Norwegian Navy World War II admirals]]
[[Category:Royal Norwegian Air Force World War II generals]]
[[Category:Norwegian Air Lines people]]