{{Redirects here|Carolina Tar Heels|the 1920s string band|The Carolina Tar Heels}}
{{Use mdy dates|date=November 2013}}
{{Refimprove|date=March 2008}}
{{Infobox college athletics 
| name                  = North Carolina Tar Heels 
| logo                  = University of North Carolina Tarheels Interlocking NC logo.svg 
| logo_width            = 200px
| university            = [[University of North Carolina at Chapel Hill]]
| association           = NCAA
| conference            = [[Atlantic Coast Conference]]
| division              = [[NCAA Division I|Division I]]
| director              = [[Bubba Cunningham]]
| city                  = [[Chapel Hill, North Carolina|Chapel Hill]]
| state                 = [[North Carolina]]
| teams                 = 27
| stadium               = [[Kenan Memorial Stadium]]
| baseballfield         = [[Boshamer Stadium|Bryson Field at Boshamer Stadium]]
| basketballarena       = [[Dean Smith Center|Dean E. Smith Student Activities Center]]
| soccerstadium      = [[Fetzer Field]]
| arena2                = [[Carmichael Arena|William D. Carmichael, Jr. Arena]]
| mascot                = [[Rameses (mascot)|Rameses]]
| nickname              = Tar Heels
| fightsong             = ''[[I'm a Tar Heel Born]]''<br />'' Here Comes Carolina''
| pageurl               = http://goheels.com/
}}
The '''North Carolina Tar Heels''' are the athletic teams representing the [[University of North Carolina at Chapel Hill]]. The name [[Tar Heel]] is a nickname used to refer to individuals from the state of [[North Carolina]], the ''Tar Heel State''. The campus at Chapel Hill is referred to as the ''University of North Carolina'' for the purposes of the [[National Collegiate Athletic Association]].The University of North Carolina at Chapel Hill was chartered in 1789, and in 1795 it became the first state-supported university in the United States.<ref name="britannica1">{{cite web|last=Editors |first=The |url=http://www.britannica.com/EBchecked/topic/419083/University-of-North-Carolina |title=University of North Carolina &#124; university system, North Carolina, United States |publisher=Britannica.com |date=1945-10-24 |accessdate=2016-09-14}}</ref> Since the school fostered the oldest collegiate team in the Carolinas, the school took on the nickname '''"Carolina,"''' especially in athletics. The Tar Heels are also referred to as '''North Carolina''', '''UNC''', or '''The Heels'''.<ref name="britannica1"/>

The mascot of the Tar Heels is [[Rameses (mascot)|Rameses]], a [[Dorset Horn|Dorset Ram]]. It is represented as either a live [[Dorset sheep]] with its horns painted [[Carolina Blue]], or as a costumed character performed by a volunteer from the student body, usually an undergraduate student associated with the cheer leading team.

Carolina has won 43 NCAA Division I team national championships in seven different sports, [[List of NCAA schools with the most NCAA Division I championships|ninth all-time]], and 52 individual national championships.

==Baseball==
{{Main article|North Carolina Tar Heels baseball}}

[[File:UNCbaseball.JPG|right|thumb|[[North Carolina Tar Heels baseball]]]]
* Head Coach: [[Mike Fox (baseball coach)|Mike Fox]]
* Stadium: [[Boshamer Stadium|Bryson Field at Boshamer Stadium]]
* ACC Championships: 6 (1982, 1983, 1984, 1990, 2007, 2013)
* College World Series Appearances: 10 (1960, 1966, 1978, 1988, 2006, 2007, 2008, 2009, 2011, 2013)

The baseball team has had recent success, reaching the championship series of the [[College World Series]] in [[2006 College World Series|2006]] and [[2007 College World Series|2007]] losing both times to [[Oregon State Beavers|Oregon State]]. They also appeared in the College World Series in [[1960 College World Series|1960]], [[1966 College World Series|1966]], [[1978 College World Series|1978]], [[1989 College World Series|1989]], [[2008 College World Series|2008]], [[2009 College World Series|2009]], [[2011 College World Series|2011]] and [[2013 College World Series|2013]].

==Men's basketball==
[[File:Hansbrough Scoring Record.jpg|thumb|2008 men's basketball players [[Wayne Ellington]], [[Danny Green (basketball)|Danny Green]], [[Tyler Hansbrough]], [[Ty Lawson]], and [[Deon Thompson]]]]
{{Main article|North Carolina Tar Heels men's basketball}}

* Head Coach: [[Roy Williams (coach)|Roy Williams]]
* Arena: [[Dean E. Smith Center]]
* [[List of Southern Conference men's basketball champions|Southern Conference Championships]]: 13 (Tournament: 1922, 1924, 1925, 1926, 1935, 1936, 1940, 1945; Regular Season: 1935, 1938, 1941, 1944, 1946)
* ACC Championships: 48 (Tournament: 1957, 1967, 1968, 1969, 1972, 1975, 1977, 1979, 1981, 1982, 1989, 1991, 1994, 1997, 1998, 2007, 2008, 2016; Regular Season: 1956, 1957, 1959, 1960, 1961, 1967, 1968, 1969, 1971, 1972, 1976, 1977, 1978, 1979, 1982, 1983, 1984, 1985, 1987, 1988, 1993, 1995, 2001, 2005, 2007, 2008, 2009, 2011, 2012, 2016)
* NCAA National Championships: 6 (1957 (undefeated), 1982, 1993, 2005, 2009, 2017)
* Final Four Appearances: 20 (1946, 1957, 1967, 1968, 1969, 1972, 1977, 1981, 1982, 1991, 1993, 1995, 1997, 1998, 2000, 2005, 2008, 2009, 2016, 2017)
* Best Final Ranking: No. 1 (Associated Press: 1957, 1982, 1984, 1994, 1998, 2008, 2009; Coaches: 1957, 1982, 1984, 1993, 2005, 2009)
* ACC/National Players of the Year: 9 (Jack Cobb 1923–26, George Glamack 1938–41, Lennie Rosenbluth 1954–57, Phil Ford 1974–78, James Worthy 1979–82, Michael Jordan 1981–1984, Antawn Jamison 1995–98, Tyler Hansbrough 2005–09, Justin Jackson 2016-2017). 

Carolina has enjoyed long success as one of the top basketball programs in the country. Overall, the Tar Heels have won six [[NCAA Men's Division I Basketball Championship|NCAA National Championships]] and were retroactively awarded one for the [[1923–24 North Carolina Tar Heels men's basketball team|1923–24 season]] by the [[Helms Athletic Foundation|Helms Foundation]] and the [[Premo-Porretta Power Poll]].<ref>{{cite book|title=ESPN College Basketball Encyclopedia: The Complete History of the Men's Game|editor-last=ESPN|publisher=ESPN Books|location=New York, NY|year=2009|page=536|ISBN=978-0-345-51392-2}}</ref>

Under coach [[Frank McGuire]], the team won its 1st NCAA championship in 1957. After McGuire left, legendary coach [[Dean Smith]] established the team as a powerhouse in college basketball. In 31 years at Carolina, Smith set the record for the most wins of any men's college basketball head coach, a record broken in 2007 by [[Bob Knight]]. Under Smith, the Tar Heels won two national championships and had numerous talented players come through the program.  Smith is also credited with coming up with the four corners offense. More recently, the Tar Heels won the national championship in 2005, 2009, and 2017 under coach Roy Williams.

==Women's basketball==
{{Main article|North Carolina Tar Heels women's basketball}}
* Head coach: [[Sylvia Hatchell]]
* Arena: [[Carmichael Arena]]
* ACC Championships:
* National Championships: 1 (1994)

==Field hockey==
[[File:UNC field hockey with Bush.jpg|thumb|2007 field hockey team with President [[George W. Bush]]]]
{{Main article|North Carolina Tar Heels field hockey}}

* Head Coach: [[Karen Shelton]]
* Stadium: [[Henry Stadium]]
* ACC Championships:  16 (1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1993, 1994, 1995, 1996, 1997, 2004, 2007)
* National Championships: 6 (1989, 1995, 1996, 1997, 2007, 2009)

==Football==
[[File:2006 Virginia Tech at UNC Glennon under center.jpg|thumb|right|2006 football team playing Virginia Tech]]
{{Main article|North Carolina Tar Heels football}} 
* Head Coach: [[Larry Fedora]]
* Stadium: [[Kenan Memorial Stadium]]
* Southern Intercollegiate Athletic Association Championships: 1 (1895)
* Southern Conference Championships: 5 (1922, 1934, 1946, 1949)
* ACC Championships: 5 (1963, 1971, 1972, 1977, 1980)
* ACC Coastal Division Championships: 2 (2012,2015)
* Postseason Bowl Appearances: 30 (1947 Sugar, 1949 Sugar, 1950 Cotton, 1963 Gator, 1970 Peach, 1971 Gator, 1972 Sun, 1974 Sun, 1976 Peach, 1977 Liberty, 1979 Gator, 1980 Bluebonnet, 1981 Gator, 1982 Sun, 1983 Peach, 1986 Aloha, 1993 Peach, 1993 Gator, 1994 Sun, 1995 Carquest, 1997 Gator, 1998 Gator, 1998 Las Vegas, 2001 Peach, 2004 Continental Tire, 2008 Meineke Car Care, 2009 Meineke Car Care, 2010 Music City, 2011 Independence, 2013 Belk)
* Best Final Ranking: No. 3 (1948 Associated Press)

==Men's lacrosse==
[[File:UNC Lacrosse.jpg|thumb|right|Men's [[Field Lacrosse|lacrosse]] in the 2009 ACC tournament final.]]
{{Main article|North Carolina Tar Heels men's lacrosse}}

* Head coach: Joe Breschi
* Home fields: [[Fetzer Field]] and [[Kenan Memorial Stadium]]
* ACC tournament championships: 1989, 1990, 1991, 1992, 1993, 1994, 1996, 2013
* ACC regular season championships: 1981, 1982, 1985, 1988, 1991, 1992, 1994, 1996, 2016
* [[NCAA Men's Lacrosse Championship|NCAA tournament]] appearances: 1976, 1977, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1998, 2004, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016
* NCAA tournament championships: 5 (1981, 1982, 1986, 1991, 2016)

==Women's lacrosse==
{{Main article|North Carolina Tar Heels women's lacrosse}}
* ACC tournament championships: 2002, 2016
* NCAA Championship: 2 (2013, 2016)

==Men's soccer==
{{main article|North Carolina Tar Heels men's soccer}}

* Head Coach: [[Carlos Somoano]]
* Stadium: [[Fetzer Field]]
* ACC Tournament Championships: 1987, 2000, 2011
* College Cup Appearances: 1987, 2001, 2008, 2009, 2010, 2011
* NCAA National Championships: 2 (2001, 2011)

==Women's soccer==
[[File:Yael Averbuch with flag.jpg|thumb|Offensive Player of the Year [[Yael Averbuch]]]]
[[File:Robyn Gayle - UNC Tar Heels.jpg|thumb|2006 women's soccer player [[Robyn Gayle]]]]
{{Main article|North Carolina Tar Heels women's soccer}}

* Head Coach: [[Anson Dorrance]]
* Stadium: [[Fetzer Field]]
* ACC Championships:  38 (1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2005, 2006, 2007, 2008, 2009 Tournament, 1987, 1989, 1990, 1991, 1992, 1993, 1995, 1996, 1997, 1998, 1999, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2010 Regular Season)
* National Championships: 22 (1981 AIAW, 1982, 1983, 1984, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1996, 1997, 1999, 2000, 2003, 2006, 2008, 2009, 2012 NCAA)
* College Cup Appearances: 26 (1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2006, 2008, 2009, 2012)


==Men's golf==
The men's golf team has won 14 conference championships:<ref>{{cite web |url=http://www.goheels.com/fls/3350/Men%27s_Golf/201213MGolfonline.pdf |title=Carolina Men's Golf 2012–13 |accessdate=June 20, 2013}}</ref>
*[[Southern Conference]] (3): 1947, 1952, 1953
*[[Atlantic Coast Conference]] (11): 1956, 1960, 1965, 1977, 1981, 1983–84, 1986, 1995–96, 2006 (co-champion)

Two Tar Heels have won the [[NCAA Division I Men's Golf Championships|NCAA individual championship]], [[Harvie Ward]] in 1949 and [[John Inman (golfer)|John Inman]] in 1984. Ward also won the [[British Amateur]] in 1952 and the [[U.S. Amateur]] in 1955 and 1956. The team's best finish was second place in 1953 and 1991.

Tar Heel golfers who have had success at the professional level include [[Davis Love III]] (20 PGA Tour wins including [[1997 PGA Championship]]) and [[Mark Wilson (golfer)|Mark Wilson]] (five PGA Tour wins).

==Wrestling==
Following Coach Sam Barnes who built the modern wrestling program at UNC (1953-1971), Head coach Bill Lam led the Tar Heel wrestling program for 30 years until his retirement in 2002, where his former wrestler and 1982 NCAA Champion C.D. Mock became his replacement. Under Lam, the Tar Heels were a consistent top 25 NCAA team. Lam led the Tar Heels to 15 ACC tournament titles in addition to being named ACC coach of the year 10 times. Following the Lam era, Mock was named ACC Coach of the Year in 2005 and 2006 in addition to claiming two ACC team titles.<ref>{{cite web |title=UNC Wrestling C.D. Mock Bio|url=http://www.goheels.com/ViewArticle.dbml?DB_OEM_ID=3350&ATCLID=205497500|publisher=University of North Carolina Athletics|accessdate=2016-09-13}}</ref> In 2015, Mock was fired as head wrestling coach. He was shortly replaced by Olympic bronze medalist and [[Oklahoma State University]] graduate [[Coleman Scott]].

The Tar Heel wrestling program boasts many ACC champions, All-Americans, and has 3 individual NCAA champions: C.D. Mock (1982), [[Rob Koll]] (1988), and T.J. Jaworsky (1993, 1994, 1995). Jaworsky is known as one of the greatest college wrestlers of all time as he is the first and only ACC wrestler to win three NCAA titles in addition to winning the inaugural Dan Hodge Trophy, given to college wrestling's most dominant wrestler. Koll is now the head coach at [[Cornell University]] where he has led the program to new heights with multiple top 10 NCAA finishes.

UNC wrestling All-Americans: C.D. Mock, Dave Cook, Jan Michaels, Bob Monaghan, Mike Elinsky, Rob Koll, Bobby Shriner, Tad Wilson, Al Palacio, Lenny Bernstein, Doug Wyland, Enzo Catullo, Pete Welch, Shane Camera, Jody Staylor, Marc Taylor, Stan Banks, Justin Harty, Evan Sola, Chris Rodrigues, Evan Henderson, Ethan Ramos, and Joey Ward.

Other notable alumni include C.C. Fisher, 1998 ACC champion and Most Outstanding wrestler, who went on to become a successful wrestler on the international stage where he was as high as second on the United States Olympic latter. Fisher also went on to become a successful coach for multiple Division 1 wrestling programs including Iowa State and Stanford.

The Tar Heel wrestling program has won 17 total ACC championships: 1979, 1980, 1984, 1985, 1986, 1987, 1992, 1993, 1994, 1995, 1997, 1998, 1999, 2000, 2003, 2005, 2006

UNC's best finish at the NCAA tournament was 5th in 1982.<ref>{{cite web|title=Wrestling History in the NCAA |url=http://fs.ncaa.org/Docs/stats/wrestling_champs_records/2011-12/2011-12_d1wr.pdf|publisher=NCAA Wrestling|accessdate=2016-09-13}}</ref> They also took 6th in 1995.

[[Carmichael Arena]] is currently the home to the Tar Heels Wrestling team located centrally on campus.<ref>{{cite web|title=UNC Tar Heels Facilities|url=http://www.goheels.com/ViewArticle.dbml?ATCLID=205731428&DB_OEM_ID=3350&SITE=UNC&DB_OEM_ID=3350|publisher=University of North Carolina Athletics|accessdate=2016-09-13}}</ref>

==Other sports==
[[File:Fetzer Field.jpg|thumb|right|2005 men's soccer team playing SMU]]
{{See also|North Carolina Tar Heels softball}}
{{See also|North Carolina Tar Heels handball}}
Other national championship victories include the women's team handball team in 2004, 2009, 2010, 2011; and the men's handball team in 2004, 2005, and 2006. The men's crew won the 2004 ECAC National Invitational Collegiate Regatta in the varsity eight category. In 1994, Carolina's athletic programs won the [[Sears Directors Cup]] which is awarded for cumulative performance in NCAA competition.

===Rugby===
Carolina also fields non varsity sports teams.  North Carolina's rugby team competes in the [[Atlantic Coast Rugby League]] against its traditional ACC rivals. North Carolina finished second in its conference in 2010, led by conference co-player of the year Alex Lee. North Carolina finished second at the Atlantic Coast Invitational in 2009 and again in 2010. North Carolina has also competed in the [[Collegiate Rugby Championship]], finishing 11th in [[2011 Collegiate Rugby Championship|2011]] in a tournament broadcast live on NBC.<ref>{{cite web |title=Big turnout for Rugby Sevens tournament at PPL Park |url=http://www.philly.com/philly/sports/123210403.html}}</ref>

==Championships==
===NCAA team championships===
North Carolina has won 43 NCAA team national championships.<ref>{{cite web|url=http://fs.ncaa.org/Docs/stats/champs_records_book/Overall.pdf |title=NCAA Records |accessdate=2016-09-14}}</ref>

*'''Men's (13)'''
**[[NCAA Men's Division I Basketball Championship#Team titles|Basketball]] (6): 1957, 1982, 1993, 2005, 2009, 2017
**[[NCAA Division I Men's Lacrosse Championship#Team titles|Lacrosse]] (5): 1981, 1982, 1986, 1991, 2016
**[[NCAA Division I Men's Soccer Championship#Team titles|Soccer]] (2): 2001, 2011
*'''Women's (30)'''
**[[NCAA Women's Division I Basketball Championship#Team titles|Basketball]] (1): 1994
**[[NCAA Division I Field Hockey Championship#Team titles|Field Hockey]]  (6): 1985, 1995, 1996, 1997, 2007, 2009
**[[NCAA Division I Women's Lacrosse Championship#Team titles|Lacrosse]]  (2): 2013, 2016
**[[NCAA Division I Women's Soccer Championship#Team titles|Soccer]]  (21): 1982, 1983, 1984, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1996, 1997, 1999, 2000, 2003, 2006, 2008, 2009, 2012
*see also:
**[[Atlantic Coast Conference#NCAA team championships|ACC NCAA team championships]]
**[[List of NCAA schools with the most NCAA Division I championships#NCAA Division I Team Championships|List of NCAA schools with the most NCAA Division I championships]]

===Other national team championships===
Below are 5 national team titles that were not bestowed by the NCAA:
* Men's:
**Basketball (1): 1924*
**Tennis (1): 2016***
* Women's:
**Soccer (1): 1981**
**Tennis (2): 2013, 2015***

:(*) Pre-NCAA tournament championship ([[Helms Foundation]] and [[Premo-Porretta Power Poll]], retroactively selected)
:(**) There was only one [[AIAW]] soccer tournament, thus making North Carolina the only women's soccer team to win an [[AIAW Champions|AIAW championship]]
:(***) ITA National Team Indoor Championships

*see also:
**[[List of NCAA schools with the most Division I national championships]]

==Rivalries==
[[File:Duke-Carolina basketball tip-off 2006.jpg|thumb|right|Tip-off of a basketball game against Duke at the Dean Smith Center]]
{{Main article|Carolina-Duke rivalry|Carolina-NC State rivalry|South's Oldest Rivalry}}
{{See also|Tobacco Road}}

Carolina's most heated rivalries are with its Tobacco Road counterparts [[Duke University|Duke]], [[North Carolina State University|North Carolina State]], and [[Wake Forest University|Wake Forest]]. In recent years, the Carolina-Duke basketball series has attracted the most attention. HBO even made a documentary in 2009 called "Battle for Tobacco Road: Duke vs. Carolina".<ref>{{cite web|url=http://blogs.newsobserver.com/sportsmedia/hbos-duke-unc-documentary |title=HBO's Duke-UNC documentary |accessdate=2010-11-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20120312151817/http://blogs.newsobserver.com/sportsmedia/hbos-duke-unc-documentary |archivedate=March 12, 2012 |df=mdy }}</ref> The Tar Heels also have a rivalry with [[University of Virginia|Virginia]] in college football, known as the [[South's Oldest Rivalry]]. UNC and UVA are the two oldest schools in the Atlantic Coast Conference.

==North Carolina Cheer==

===I'm a Tar Heel Born===
Carolina's main fight song is  ''I'm a Tar Heel Born''.  Its lyrics appear in the 1907 edition of the university's yearbook, the "Yackety Yack," although how long it existed before that is not known.<ref>1907 Yackety Yack p.294.</ref> Some say that it was in the late 1920s that it began to be sung as an add-on (or "tag") to the school's alma mater, "[[Hark The Sound]]",although the current version of the sheet music for "Hark the Sound" includes the "I'm a Tar Heel Born" tag as an integral part of the alma mater, and credits the full song to William Starr Myers with a date of 1897.<ref>{{cite web|url=http://library.unc.edu/music/uncsongs/ |title=UNC School Songs &#124; UNC Chapel Hill Libraries |publisher=Library.unc.edu |date= |accessdate=2016-09-14}}</ref> Today, the song is almost always played immediately after the singing of "Hark The Sound", even during more formal occasions such as [[convocation]] and [[Graduation|commencement]]. Just before home football and basketball games, the song is played by the Bell Tower near the center of campus, and is often played after major victories.<ref>{{cite web |url=http://tarheelblue.cstv.com/trads/unc-trads-songs.html |title=UNC School Songs |publisher=tarheelblue.com |accessdate=March 9, 2008}}</ref> As it appears in its 1907 printed form, the final words of the song are "Rah Rah Rah Rah." For at least the last half century, however, the final words of the song have been "Go to hell [whoever the Tar Heels are playing that day]" such as "Go to hell State" or "Go to hell Wake."  When a game is not in progress, or when the song is sung in the formal settings mentioned above, it is considered unseemly to consign the Tar Heels' worthy rivals to the confines of hell, so the appropriate and accepted closing in all such settings is "Go to hell Duke."

Simply known as "The Tag" by many UNC Marching Band and Pep Band Alumni, and titled as such on some [https://open.spotify.com/track/23cCr6w4YgxQaYlr3eNTUr recorded albums], "I'm a Tar Heel Born" has been adapted by at least two other colleges for their use, including the [http://web.uri.edu/ramband/sightes-and-sounds/ University of Rhode Island] and the [http://www.richmondspiders.com/ViewArticle.dbml?ATCLID=205184649 University of Richmond].

===Here Comes Carolina===
Another popular song is ''Here Comes Carolina''.  
As its title implies, it is most commonly played when a Tar Heel team enters the field of play.  Traditionally, the band plays a version of the traditional orchestral warmup tune before launching into the song when the first player charges out of the tunnel.  During the warmup tune, fans stand and clap along.  The effect is similar to that of a train coming down the track.

For many years at basketball games, the band played the first seven notes of the song in different keys during player introductions, modulating a half step each time before launching into the song in the normal key after the final player was announced.

The last part of the song's melody come from an old revival song, "[[Jesus Loves the Little Children]]".

==Notable alumni==
{{Main article|List of University of North Carolina at Chapel Hill alumni|List of University of North Carolina at Chapel Hill Olympians|l1=List of alumni|l2=List of Olympians}}

Notable graduates from the athletic programs include [[Michael Jordan]] from men's basketball, [[Mia Hamm]] from women's soccer, [[Charlie Justice (American football player)|Charlie Justice]] from [[American football]], [[Davis Love III]] from golf, [[B.J. Surhoff]] from baseball and [[Marion Jones]] from women's basketball and track & field.

==References==
{{reflist|30em}}

==External links==
{{Portal|ACC}}
* {{Official website}}m

{{University of North Carolina at Chapel Hill}}
{{Navboxes
|titlestyle = {{CollegePrimaryStyle|North Carolina Tar Heels|color=black}}
|list =
{{Atlantic Coast Conference navbox}}
{{East Atlantic Gymnastics League navbox}}
{{North Carolina Sports}}
}}
[[Category:North Carolina Tar Heels|*]]