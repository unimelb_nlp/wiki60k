<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name=XFA
|image=General Aviation XFA.jpg
|caption=
}}{{Infobox Aircraft Type
|type=[[Fighter aircraft|Fighter]]
|national origin=[[United States]]
|manufacturer=[[General Aviation Company]]
|designer=
|first flight={{avyear|1932}}<ref name="Angel">Angelucci, 1987. pp. 203-204.</ref>
|introduced=
|retired=
|status=
|primary user=
|number built=1
|developed from= 
|variants with their own articles=
}}
|}

The '''General Aviation XFA''' was an American biplane [[fighter aircraft]] built by the [[General Aviation Company]] for the [[United States Navy]].

==Development==
The PW-4 was built for U.S. Navy [[Specification No. 96]], calling for a carrier-based light fighter. This specification was eventually revealed as a cover for the Navy's actual desire for an [[Parasite fighter|airship fighter]], the [[Curtiss F9C Sparrowhawk|Curtiss XF9C]]. The XFA was a single-bay biplane with an all-metal fuselage and metal laminate skin. The construction of its fuselage was innovative in that instead of using [[lap jointing]], the edges of each panel were bent inwards, with the rivets fastening them on the inside, instead of being visible on the surface. It had a [[Gull wing|gull-type]] upper wing which was fabric covered. The prototype was ordered in 1930, but the company was engaged in another reorganization, which delayed its work. Delivered for evaluations in 1932, it showed poor flying characteristics, including longitudinal instability and over-sensitive controls. General Aircraft increased the area of the tail surfaces and made other changes, then returned the prototype for more testing; but now the stability problems were worse. The plane would nose up with more throttle, but then drop its nose when the throttle was reduced. After another round of modifications, and some close calls, the plane was finally classed as unsafe and testing was abandoned.<ref name="Angel"/>

==Specifications==
{{aerospecs
|ref=Angelucci, 1987. pp. 203-204.<ref name="Angel"/>
|met or eng?=eng
|crew=1
|capacity=
|length m=6.75
|length ft=22
|length in=2
|span m=7.77
|span ft=25
|span in=6
|height m=2.81
|height ft=9
|height in=3
|wing area sqm=16.25
|wing area sqft=175
|empty weight kg=833
|empty weight lb=1,837
|gross weight kg=1,138
|gross weight lb=2,508
|eng1 number=1
|eng1 type=[[Pratt & Whitney R-1340]]-C
|eng1 kw=
|eng1 hp=450
|max speed kmh=274
|max speed mph=170
|cruise speed kmh=
|cruise speed mph=
|range km=834
|range miles=518
|endurance h=
|endurance min=
|ceiling m=6,157
|ceiling ft=20,200
|climb rate ms=7.47
|climb rate ftmin=1470
|armament1=2 × {{convert|.30|in|mm|2|abbr=on}} [[machine gun]]s
|armament2=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
|similar aircraft=
|lists=
}}

==References==
{{commons category|General Aviation XFA}}
===Citations===
{{Reflist}}

===Bibliography===
*{{cite book |last= Angelucci |first= Enzo |title=The American Fighter from 1917 to the present |year=1987 |publisher=Orion Books |location=New York |pages= }}
*Lloyd S. Jones, ''U.S. Naval Fighters'' (Fallbrook CA: Aero Publishers, 1977, ISBN 0-8168-9254-7), pp.&nbsp;89–91

{{USN fighters}}

[[Category:Gull-wing aircraft]]
[[Category:United States fighter aircraft 1930–1939|General Aviation FA]]