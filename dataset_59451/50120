{{Infobox Bilateral relations|Turkish-Abkazian|Abkhazia|Turkey}}
'''Abkhazia–Turkey relations''' refers to the relations between [[Abkhazia]] and [[Turkey]].  Although Turkey has not recognized Abkhazia's independence and regards it as [[de jure]] part of [[Georgia (country)|Georgia]], the two governments reportedly have secret ties.<ref name="Hürriyet">[http://www.hurriyetdailynews.com/n.php?n=turkey-squeezed-one-more-time-between-georgia-and-abkhazia-2009-09-03 Barçin Yinanç: ''Turkey squeezed once again between Georgia, Abkhazia'', hurriyetdailynews.com 3 September 2009.]</ref>

== Overview ==
On 22 September 1996, Turkey announced that residents of Abkhazia would no longer be allowed to travel to Turkey on Soviet-era identification documents, and would instead have to obtain [[Georgian passport]]s.<ref>{{cite journal|last=[[The Jamestown Foundation]]|title=ABKHAZIAN ACCESS TO TURKEY CURBED.|url=http://www.jamestown.org/single/?no_cache=1&tx_ttnews%5Bswords%5D=8fd5893941d69d0be3f378576261ae3e&tx_ttnews%5Bany_of_the_words%5D=abkhaz%20abkhazia%20ardzinba&tx_ttnews%5Bcategories_1%5D=49&tx_ttnews%5Bpointer%5D=28&tx_ttnews%5Btt_news%5D=14074&tx_ttnews%5BbackPid%5D=7&cHash=c58267a6dba4fc7f19915ddf008749a3|journal=Monitor|date=24 September 1996|volume=2|issue=177|accessdate=10 January 2012}}</ref>

In July, 2009, Abkhazian Foreign Minister [[Sergei Shamba]] said that the Abkhazian government has certain contacts with the government of Turkey; negotiations on resumption of air and sea communication are being held.<ref name="gt0611">{{cite news|url=http://www.georgiatimes.info/en/articles/14408.html|title=Shamba: Abkhazia establishes relations with Turkey|date=2009-06-11|publisher=Georgia Times|accessdate=2009-08-28}}</ref>

However, since Turkey does not want to antagonize its neighbor and important trading partner Georgia of which Abkhazia is de jure part of, Turkey maintains a strict trade embargo on Abkhazia.<ref name="Hürriyet" />

Several Turkish ships heading to Abkhazia have been seized by Georgian naval forces in [[international waters]] due to the [[Georgian sea blockade of Abkhazia]].<ref name="Hürriyet" />

In September 2009, the Foreign Ministry Deputy Undersecretary Ünal Çeviköz went to the Abkhaz capital of Sukhum, where he met with Abkhaz officials. This was the first visit to Abkhazia of a foreign national diplomat since the August 2008 war.<ref>[http://abkhazworld.com/aw/diaspora/168-turkey-abkhazia-relations-after-cevikoz Hasan Kanbolat: ''Turkey-Abkhazia relations after Çeviköz'', abkhazworld.com 17 September 2009.]</ref>

Today, Turkey is Abkhazia's second-most important trade partner with about 18 percent of Abkhazia’s trade turnover.<ref name="Jamestown">[http://www.jamestown.org/single/?tx_ttnews%5Btt_news%5D=44437&no_cache=1 Vasili Rukhadze: ''Defying Georgia, Turkey Gradually Cultivates its Influence in Separatist Abkhazia'', jamestown.org 1 October 2015.]</ref>

Turkey’s ambassador to Georgia, Murat Buhran, stated in 2014 that Turkey and Abkhazia had established a special group to deepen “bilateral” ties.<ref name="Jamestown" />

The importance of the Turkish factor in Abkhaz policy was demonstrated by the first visit by President Bagapsh to Ankara in April 2011.<ref>Thomas Frear: ''The foreign policy options of a small unrecognised state: the case of Abkhazia'', in: [[Caucasus Survey]], Vol. 1 (2014), No. 2, pp. 83-107 (here: p: 93).</ref>

== See also ==
* [[Muhajir (Caucasus)]]

== References ==
{{Reflist}}

{{Foreign relations of Abkhazia}}
{{Foreign relations of Turkey}}

{{DEFAULTSORT:Abkhazia-Turkey Relations}}
[[Category:Abkhazia–Turkey relations]]