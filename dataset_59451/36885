{{for|other bridges named after George Washington|Washington Bridge (disambiguation)}}
{{Use mdy dates|date=September 2015}}
{{Infobox Bridge
|bridge_name   = Aurora Bridge
|image         = [[File:George Washington Memorial Bridge.JPG|frameless|upright=1.35]]
|caption       = The main span of the bridge, looking west. The suspended truss is visible at the center of the cantilever arch structures
|official_name = George Washington Memorial Bridge
|carries       = {{jct|state=WA|WA|99|name1=[[Aurora Avenue (Seattle)|Aurora Avenue North]]}}
|crosses       = [[Lake Union]]
|locale        = [[Seattle]], [[Washington (U.S. state)|Washington]]
|maint         = [[Washington State Department of Transportation|Washington State DOT]]
|id            = 0001447A0000000
|design        = Mixed, [[Cantilever bridge|cantilever]] and [[Truss bridge|truss]]
|mainspan      = {{convert|475|ft|m|abbr=on}}
|length        = {{convert|2945|ft|m|abbr=on}}
|width         = {{convert|70|ft|m|abbr=on}}
|height        = 
|clearance     = 
|below         = {{convert|167|ft|m|abbr=on}}
|traffic       = 71,000 (2007)<ref name=WSDOT-AADT>{{cite web |url=http://www.wsdot.wa.gov/mapsdata/tdo/PDF_and_ZIP_Files/Annual_Traffic_Report_2007.pdf |format=PDF |title=2007 Annual Traffic Report |publisher=Transportation Data Office, [[Washington State Department of Transportation]] |pages=125 |year=2008 |accessdate=September 8, 2008}}</ref>
{{Infobox NRHP
  | embed = yes
  | name = Aurora Avenue Bridge
  | nrhp_type = 
  | image = 
  | caption = 
  | location= Aurora Ave., N. over Lake Washington Ship Canal, [[Seattle, Washington]]
  | coordinates = {{coord|47|38|47|N|122|20|51|W|display=inline,title}}
| locmapin = 
  | built = 1931–32
  | architect = Jacobs & Ober
  | builder = U.S. Steel Products Corp.
  | added = July 16, 1982
  | governing_body = [[Washington State Department of Transportation|Washington State DOT]]   
  | mpsub = {{NRHP url|id=64000902|title=Historic Bridges/Tunnels in Washington State TR}}
  | refnum = 82004230<ref name="nris">{{NRISref|version=2010a}}</ref>
}}
|open          = February 22, 1932
}}
The '''Aurora Bridge''' (officially called the '''George Washington Memorial Bridge''') is a [[Cantilever bridge|cantilever]] and [[Truss bridge|truss]] [[bridge]] that carries [[Washington State Route 99|State Route 99]] ([[Aurora Avenue (Seattle)|Aurora Avenue]] North) over the west end of [[Seattle]]'s [[Lake Union]] and connects [[Queen Anne, Seattle|Queen Anne]] and [[Fremont, Seattle|Fremont]]. The bridge is located just east of the [[Fremont Cut]], which itself is spanned by the [[Fremont Bridge (Seattle)|Fremont Bridge]].

The bridge is {{convert|2945|ft|abbr=on}} long, {{convert|70|ft|m|abbr=on}} wide, and {{convert|167|ft|m|abbr=on}} above the water,<ref name=historylink/> and is owned and operated by the [[Washington State Department of Transportation]].<ref name=historylink/> The bridge was opened to traffic on February 22, 1932. It was listed on the [[National Register of Historic Places]] in 1982. The bridge is a popular location for [[suicide]] jumpers and numerous reports have used the bridge as a case study in fields ranging from suicide prevention to the effects of prehospital care on [[Physical trauma|trauma]] victims.

In 1998, a [[bus driver]] was shot and killed while driving over the bridge, causing his bus to crash and resulting in the death of one of the passengers. In 2015, five people were killed and fifty were injured when an amphibious "[[duck tour]]" vehicle crashed into a [[charter bus]] on the bridge in an accident that also involved two smaller vehicles.

== Design ==
[[File:GW Memorial Bridge from Adobe Systems 11.jpg|thumb|left|The northern anchor of the bridge]]
[[File:Aurora Bridge.jpg|thumb|View from beneath the bridge]]
The bridge is {{convert|2945|ft|m|abbr=on}} long, {{convert|70|ft|m|abbr=on}} wide, {{convert|167|ft|m|abbr=on}} above the water and is owned and operated by the [[Washington State Department of Transportation]].<ref name=historylink/> There are two v-shaped cantilever sections supporting the bridge deck, each {{convert|325|ft|m|abbr=on}} long,  balanced on large concrete pilings at opposite sides of the ship canal which serve as the two main supporting anchors.<ref name=historylink/><ref name=Dorpat/> Some 828 timber [[Deep foundation|piles]] were driven for the foundation of the south anchor and 684 piles for the north. They range in size from {{convert|110|to|120|ft|m|0}} and rest {{convert|50|to|55|ft|m|0}} below the surface of the water. Together, the anchors support a load of 8,000 tons. Their construction required a [[pile driver]] that was specially designed to work underwater.<ref name=NRIS/>

A {{convert|150|ft|m|abbr=on}} long [[Truss bridge#Warren truss|Warren truss]] suspended span connects the two cantilevers in the middle. The bridge's main span is {{convert|475|ft|m|abbr=on}} long. At either end of the bridge there are additional Warren truss spans which connect the cantilevered spans to the highway.<ref name=historylink/><ref name=Dorpat>{{cite book |author=Paul Dorpat|authorlink=Paul Dorpat |author2=Genevieve McCoy |title= Building Washington: A History of Washington State Public Works |year= 1998 |publisher= Tartu Publications |isbn= 0-9614357-9-8 |pages= 117}}</ref>

== History ==
Construction on the bridge piers began in 1929,<ref name=nblcr>{{cite news |url=https://news.google.com/newspapers?id=MINfAAAAIBAJ&sjid=nzAMAAAAIBAJ&pg=1797%2C2383499 |newspaper=Lewiston Morning Tribune |location=Idaho |agency=(Associate Press photo) |title=New bridge links coastal route |date=October 3, 1930 |page=1}}</ref> with construction of the bridge following shortly afterwards in 1931, with its dedication held on February 22, 1932, [[George Washington]]'s 200th birthday.<ref name=historylink>{{cite web
|author=Priscilla Long
|title=Seattle's George Washington Memorial Bridge (Aurora Bridge) is dedicated on February 22, 1932.
|url=http://www.historylink.org/index.cfm?DisplayPage=output.cfm&file_id=5418
|publisher=[[HistoryLink]]
|date=March 3, 2003
|accessdate=October 20, 2007}}</ref><ref name=historylink2/> It opened to traffic the same day.<ref name=NRIS/>

The bridge was the final link in what was then called the Pacific Highway (later known as [[U.S. Route 99 in Washington|U.S. Route 99]]), which ran from Canada to Mexico. The bridge crosses the Lake Union section of the [[Lake Washington Ship Canal]] and, unlike earlier bridges across the canal, the height of the Aurora Bridge eliminated the need for a [[drawbridge]]. The Seattle City Council voted to build connecting portions of the highway through the [[Woodland Park Zoo]], a decision which generated considerable controversy at the time.<ref name=historylink2>{{cite web |url=http://historylink.org/index.cfm?DisplayPage=output.cfm&file_id=8093 |title=Seattle City Council votes to build Aurora Avenue through Woodland Park on June 30, 1930 |author=Kit Oldham |publisher=HistoryLink |date=February 17, 2007 |accessdate=September 7, 2008}}</ref>

It was designed by the Seattle architectural firm Jacobs & Ober, with Ralph Ober as the lead engineer on the project. Ober died in August 1931, of a [[brain hemorrhage]] while the bridge was still under construction.<ref>{{cite web
|url=http://www.pacificpublishingcompany.com/site/tab8.cfm?newsid=17697533&BRD=855&PAG=461&dept_id=515218&rfi=6
|title=The draw of the Aurora Bridge: Despite popular belief,the Aurora Bridge isn't prone to heartbreaking situations
|publisher=Pacific Publishing Company
|author=Kirby Lindsay
|accessdate=October 20, 2007
|date=January 11, 2007}} {{Dead link|date=September 2010|bot=H3llBot}}</ref> Federal funding programs were not yet available, so the bridge was funded by Seattle, King County, and the state of Washington.<ref name=Dorpat/>

The bridge was nominated for the [[National Register of Historic Places]] on January 2, 1980, for its "functional and aesthetic" design qualities and for its historical status as the first bridge constructed in the region without streetcar tracks.<ref name=NRIS>{{cite web |author = Elizabeth Atly |author2=Lisa Soderberg | title = National Register of Historic Places Nomination Form | year = 1982 | pages = 4 | publisher = [[National Park Service]] | url = http://commons.wikimedia.org/wiki/Image:George_Washington_Memorial_Bridge_Nomination_Form.PDF}}</ref> It was accepted to the National Register on July 16, 1982.<ref name=NRIS/>

A local landmark, the [[Fremont Troll]]—a large cement sculpture of a troll clutching a real-life [[Volkswagen Beetle]]—was installed under the bridge's north end in 1990.<ref name=seatimes-troll>{{cite news |url=http://community.seattletimes.nwsource.com/archive/?date=19901210&slug=1108785 |title=Monstrous New Fun In Fremont |newspaper=[[The Seattle Times]] |author=Constantine Angelos |date=December 10, 1990 |accessdate=August 21, 2008}}</ref> Up to half of the $40,000 cost for the artwork was donated from Seattle's Neighborhood Matching Fund, a local program to raise money for community projects.<ref name=seatimes-troll/><ref name=seatimes-tmf>{{cite news |url=http://community.seattletimes.nwsource.com/archive/?date=19910924&slug=1307293 |title=Neighborhood Improvement Program - A Home-Grown Idea Wins National Applause |newspaper=The Seattle Times |date=September 24, 1991 |accessdate=August 21, 2008}}</ref><ref name=seattlepi-pricetag>{{cite news |url=http://www.seattlepi.com/archives/1992/9204020096.asp |title=Troll-lific:Gnomes are multiplying |newspaper=[[Seattle Post-Intelligencer]] |author=Cecilia Goodnow |date=April 2, 1992 |accessdate=August 21, 2008}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> The sculpture was heavily vandalized in the year following its construction and large floodlights were installed on the bridge to discourage further damage.<ref name=seatimes-floodlights>{{cite news |url=http://community.seattletimes.nwsource.com/archive/?date=19910305&slug=1269886 |title=Fremont Troll Gets The Light Of His Life |newspaper=The Seattle Times |date=March 5, 1991 |accessdate=August 21, 2008}}</ref>

Following the [[I-35W Mississippi River bridge#Collapse|collapse of the Minneapolis I-35W]] arch-truss bridge on August 1, 2007, the Washington State Department of Transportation was directed to perform inspections of all steel cantilever bridges in the state that used [[Gusset plates#In architecture|gusset plates]] in their design, including the George Washington Memorial Bridge.<ref name=gusset>{{cite web |url=http://www.wsdot.wa.gov/Accountability/GussetPlates.htm |title=Washington State Bridge Construction Practices and Gusset Plates |publisher=Washington State Department of Transportation |year=2007 |accessdate=December 3, 2007}}</ref> The bridge had earlier been certified as structurally sound with no serious deficiencies detected.<ref name=bridgeupdate>{{cite web |url=http://www.wsdot.wa.gov/publications/fulltext/bridges.pdf |format=PDF| title=2007 Annual Bridge Update |publisher=Washington State Department of Transportation |date=June 30, 2007 |accessdate=December 3, 2007}}</ref>

In 2007, the [[Federal Highway Administration]] [[National Bridge Inventory]] found the bridge to be "functionally obsolete".<ref name=NBI>{{cite web|url= http://nationalbridges.com/nbi_record.php?StateCode=53&struct=0001447A0000000 |archive-url= https://web.archive.org/web/20090617103209/http://nationalbridges.com:80/nbi_record.php?StateCode=53&struct=0001447A0000000 |dead-url= yes |archive-date= June 17, 2009 | title = Place Name: Seattle, Washington; NBI Structure Number: 0001447A0000000 ; Facility Carried: SR 99; Feature Intersected: Aurora Avenue, Lake Union |author = [[Federal Highway Administration]] [[National Bridge Inventory]] |year=2007 |publisher =Nationalbridges.com (Alexander Svirsky) |accessdate = September 6, 2008}}  ''Note'': this is a formatted [[Web scraping|scrape]] of the 2007 official website, which can be found here for Washington: {{cite web| url = http://www.fhwa.dot.gov/bridge/nbi/2007/WA07.txt |title = WA07.txt |year = 2007 | publisher = Federal Highway Administration |accessdate = September 8, 2008}}</ref> The bridge was given a sufficiency rating of 55.2% and evaluated to be "better than minimum adequacy to tolerate being left in place as is".<ref name=NBI/> Its foundations and railings met the acceptable standards and no immediate corrective action was needed to improve it.<ref name=NBI/>

The George Washington Memorial Bridge underwent extensive seismic retrofitting from 2011 to 2012 at a cost of $5.7 million US dollars.<ref name=seismicretrofit>{{cite web|url=http://www.wsdot.wa.gov/projects/sr99/aurorabridgeseismicretrofit/|title = WSDOT - Project - SR 99 - Aurora Bridge and Column Seismic Retrofit|publisher = Washington State Department of Transportation|accessdate = June 6, 2011}}</ref>

=== Accidents and incidents ===
[[File:SB Route 358 crossing GW Memorial Bridge.jpg|thumb|upright|left|A southbound Route 358 articulated bus crosses the George Washington Memorial Bridge]]
On November 27, 1998, [[King County Metro]] driver Mark McLaughlin, the driver of a southbound [[List of King County Metro bus routes|route 359 Express]] [[articulated bus]], was shot and killed by a passenger, Silas Cool, while driving across the bridge.<ref name=busplunge>{{cite news
|title = Rider Shoots Driver; 2 Dead, Dozens Hurt -- Bus Careens Off Bridge -- Murder-Suicide A Possibility
|url = http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=2785924&date=19981128
|newspaper= The Seattle Times
|accessdate = October 20, 2007
|date = November 28, 1998
}}</ref><ref name=silascool>{{cite news
|title = Bus Driver Killed By Shot To Chest -- Search Of Gunman's Apartment Turns Up Additional Guns, Knives
|url = http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=2786260&date=19981130
|work = The Seattle Times
|accessdate = October 20, 2007
|date = November 30, 1998
|author=Deborah Nelson
|author2=Christine Clarridge |author3=J. Martin Mcomber |author4=Tan Vinh |author5=Eric Sorensen |author6= Chris Solomon 
}}</ref> Cool then shot himself as the bus veered across two lanes of traffic and [[bus plunge|plunge]]d off the bridge's eastern side onto the roof of an apartment building below.<ref name=busplunge/><ref name=silascool/> Herman Liebelt, a passenger on the bus, later died of injuries he sustained in the crash.<ref>{{cite news
|title = Victim Was Trying `To Make A Difference' - Herman Liebelt Had Varied Interests, Causes
|url = http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=2786115&date=19981129
|work = The Seattle Times
|author=Chris Solomon
|accessdate = October 20, 2007
|date = November 29, 1998
}}</ref>

A service for McLaughlin was held on December 8, 1998, at [[KeyArena]] in Seattle.<ref name=markm/> Numerous state and county officials and over 100 transit drivers attended the service, which included a procession of over eighty Metro buses and vans.<ref name=markm>{{cite news
|url=http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=2787861&date=19981208
|title=Bus Drivers Honor One Of Their Own -- Memorial For Mark Mclaughlin
|newspaper=The Seattle Times
|author=Jack Broom
|author2=Christine Clarridge
|accessdate=October 20, 2007
|date=December 8, 1998}}</ref> Metro retired the number 359 as a route designation and replaced it with route 358 in February 1999, as part of a restructure of service on Aurora Avenue.<ref>{{cite news |last=Schaefer |first=David |date=February 5, 1999 |title=Metro adds routes, buses tomorrow |page=B2 |url=http://community.seattletimes.nwsource.com/archive/?date=19990205&slug=2942596 |work=The Seattle Times |accessdate=August 18, 2016}}</ref> On February 15, 2014, Route 358 was retired, and replaced with the [[RapidRide E Line]].<ref>{{cite news |last=Lindblom |first=Mike |date=February 15, 2014 |title=Aurora Avenue North bus now RapidRide |url=http://old.seattletimes.com/html/localnews/2022921192_358rapidrideexml.html |work=The Seattle Times |accessdate=August 18, 2016}}</ref>

According to estimates from the Washington State Department of Transportation, repairs to the bridge cost over $18,000.<ref>{{cite news
|title = Part Of Bridge To Be Closed
|url = http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=2789803&date=19981219
|work = The Seattle Times
|accessdate = October 20, 2007
|date = December 19, 1998
}}</ref> [[Claim (legal)|Medical claims]] from the victims against King County amounted to $2.3&nbsp;million.<ref>{{cite news
|url=http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=4014922&date=20000411
|author=Roberto Sanchez
|title=Aurora bus wreck tops county list of $10.9 million for claims, suits
|newspaper=The Seattle Times
|accessdate=October 20, 2007
|date=April 11, 2000}}</ref>

On September 24, 2015, five people were killed<ref>{{cite news|url=http://www.usatoday.com/story/news/nation/2015/09/27/ntsb-duck-vehicle-didnt-have-recommended-axle-repair/72948216/ |title=NTSB: Duck vehicle didn't have recommended axle repair |newspaper=[[USA Today]] |date=September 27, 2015}}</ref> and fifty were injured when an amphibious "[[duck tour]]" vehicle crashed into a [[charter bus]] on the bridge in an accident that also involved two smaller vehicles. According to a representative from the Chinese consulate, all of the students were foreign-born. The students all attended [[North Seattle College]], and were on their way to [[Safeco Field]] for new student orientation. One witness reported that it appeared as though the duck boat veered into the oncoming bus, after crossing the center line.<ref name=King>{{cite news|title=4 killed as 'Ride The Ducks' vehicle collides with charter bus|url=http://www.king5.com/story/news/traffic/2015/09/24/collision-between-bus-duck-vehicle-several-injured/72746930/|accessdate=September 25, 2015|publisher=[[KING-TV]]|date=September 24, 2015}}</ref><ref name=KOMO>{{cite news|title=4 killed, 51 injured as 'Duck' vehicle, bus collide on Aurora Bridge|url=http://www.komonews.com/news/local/Ride-the-Ducks-vehicle-charter-bus-collide-on-Aurora-Bridge-329221111.html|accessdate=September 25, 2015|publisher=[[KOMO-TV]]|date=September 24, 2015}}</ref><ref name=Kim>{{cite news|last1=Kim|first1=Janet|title=International students killed on Aurora Bridge were from North Seattle College|url=http://q13fox.com/2015/09/24/students-killed-on-aurora-bridge-from-north-seattle-college/|accessdate=September 24, 2015|publisher=[[KCPQ]]|date=September 25, 2015}}</ref> Some blame for the accident was placed on the narrowness of the {{convert|57|ft|m|adj=mid|-wide}} bridge deck, which has {{convert|9.5|ft|m|adj=mid|-wide}} lanes, and the lack of a [[jersey barrier|median barrier]] to separate the two directions of traffic.<ref>{{cite news |last1=Lindblom |first1=Mike |last2=Baker |first2=Mike |date=September 24, 2015 |title=Span's narrow lanes a longtime safety concern |url=http://www.seattletimes.com/seattle-news/transportation/spans-narrow-lanes-a-longtime-safety-concern/ |newspaper=The Seattle Times |accessdate=September 25, 2015}}</ref> There have also been some calls to reduce the number of lanes in order to widen lanes,<ref>{{cite news |last=Johnson |first=Graham |date=September 24, 2015 |title=Should Aurora Bridge have fewer lanes and traffic barrier? |url=http://www.kirotv.com/news/news/should-aurora-bridge-have-fewer-lanes-and-traffic-/nnnSx/ |publisher=[[KIRO-TV]] |accessdate=September 25, 2015}}</ref> although early reports indicated that a mechanical failure of the duck tour vehicles' front axle may have also been a major factor in the crash.<ref>{{cite news|last=Warren|first= Ted S. |url=http://www.kitsapsun.com/news/state/feds-axle-from-duck-boat-in-deadly-crash-sheared-off |title=Feds: Axle from duck boat in deadly crash 'sheared off' |work=[[Kitsap Sun]] |date=September 26, 2015}}</ref>

== Suicides ==
[[File:Suicide hotline sign on GW Memorial Bridge 2.jpg|upright=.82|thumb|left|Sign for the [[Crisis hotline|suicide hotline]] on the George Washington Memorial Bridge]]
[[File:Emergency phone GW Memorial Bridge 2.jpg|thumb|upright=.455|right|One of six [[Emergency telephone|emergency phones]] on the bridge]]
The bridge's height and [[pedestrian]] access make it a popular location for [[suicide]] jumpers.<ref name=stranger>{{cite news
|url=http://www.thestranger.com/seattle/Content?oid=3664
|author=Charles Mudede
|title=Jumpers
|newspaper=[[The Stranger (newspaper)|The Stranger]]
|accessdate=October 20, 2007
|date=April 19, 2000}}</ref>  Since construction, there have been over 230 completed suicides from the bridge, with nearly 50 deaths occurring in the decade 1995–2005.<ref name=historylink/><ref>{{cite news
|url=http://abcnews.go.com/Health/suicide-prevention-week-seattle-bridge-worst-us-stems/story?id=17181654#.UFaEXPUvOZ4 |title=Suicide Prevention Week |publisher=abcnews.com. |accessdate = September 16, 2012 |date=September 10, 2012 }}</ref>  The first suicide occurred in January 1932, when a shoe salesman leapt from the bridge before it was completed.<ref name=seattlepi-suicide>{{cite news |url=http://www.seattlepi.com/local/287230_jumpers02.html
|title=City hopes to dissuade suicidal jumpers
|newspaper=Seattle Post-Intelligencer
|accessdate=October 20, 2007
|date=October 2, 2006
 }} {{Dead link|date=December 2011|bot=RjwilmsiBot}}
</ref>

Numerous reports have been written about the high incidence of suicide on the bridge, many of them using the bridge as a case study in fields ranging from suicide prevention to the effects of prehospital care on trauma victims.<ref>{{cite journal
|doi=10.1097/00005373-198311000-00003
|vauthors=Fortner GS, Oreskovich MR, Copass MK, Carrico CJ |title=The Effects of Prehospital Trauma Care on Survival from a 50 Meter Fall
|journal=Journal of Trauma
|volume=23
|issue=11
|pages=976–81
|year=1983
|pmid=6632028 }}</ref>

Despite the force of impact, jumpers occasionally survive the fall from the bridge, though not without sustaining serious injuries.<ref>{{cite news |url=http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=webjumper10&date=20070309
|title=Woman survives jump from Aurora Bridge
|newspaper=The Seattle Times
|accessdate=October 20, 2007
|date=September 30, 2007}}</ref><ref>{{cite news|url=http://www.seattlepi.com/archives/1996/9603060095.asp |title=Life After The Fall |newspaper=Seattle Post-Intelligencer |accessdate=October 20, 2007 |date=March 5, 1996 |deadurl=yes |archiveurl=http://www.webcitation.org/5x4IMbEyW?url=http%3A%2F%2Fwww.seattlepi.com%2Farchives%2F1996%2F9603060095.asp |archivedate=March 10, 2011 |df=mdy }}</ref>

News sources have referred to the George Washington Memorial Bridge as a [[suicide bridge]]<ref>{{cite news
|url=http://www.msnbc.msn.com/id/16829300/ |title='Suicide bridge' hurts workers' mental health |publisher=msnbc.com. |accessdate = October 20, 2007 |date=January 26, 2007
}}</ref> and, in December 2006, six [[Emergency telephone|emergency phones]] and 18 signs were installed on the bridge to encourage people to seek help instead of jumping.<ref name=WSDOT-Fence>{{cite web |title=SR 99 - Aurora Bridge Fence - Completed February 2011 |url=http://www.wsdot.wa.gov/Projects/SR99/AuroraBridgeFence |deadurl=yes |archiveurl=https://web.archive.org/web/20150419144359/http://www.wsdot.wa.gov/projects/sr99/aurorabridgefence/ |archivedate=April 19, 2015 |publisher=Washington State Department of Transportation |date=February 2011 |accessdate=September 27, 2015}}</ref><ref>{{cite news |date=December 18, 2007 |title=State budget includes suicide fence on Aurora Bridge |url=http://komonews.com/news/local/state-budget-includes-suicide-fence-on-aurora-bridge |work=KOMO News |agency=Associated Press |accessdate=July 21, 2016}}</ref><ref name=seatimes-budget/> In late 2006 a group of community activists and political leaders living near the bridge created the Fremont Individuals and Employees Nonprofit to Decrease Suicides (FRIENDS), their primary focus being the installation of a suicide barrier on the bridge.<ref>{{cite news
|url=http://archives.seattletimes.nwsource.com/cgi-bin/texis.cgi/web/vortex/display?slug=aurorabridge17&date=20070917
|title=Neighbors work to end bridge's tragic pull
|author=Marc Ramirez
|newspaper=The Seattle Times
|accessdate=October 20, 2007
|date=September 17, 2007}}</ref>

In 2007, Washington Governor [[Christine Gregoire]] allocated $1.4&nbsp;million in her supplemental budget for the construction of an {{convert|8|ft|m|adj=on}} high suicide-prevention fence to help reduce the number of suicides on the bridge.<ref name=seatimes-budget>{{cite news|url=http://seattletimes.nwsource.com/html/politics/2004081642_aurorabridge19m.html |title=Money for fence to cut Aurora Bridge suicides |author=Donna Gordon Blankinship |publisher=[[The Associated Press]] |date=December 19, 2007 |accessdate=August 21, 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20071221202541/http://seattletimes.nwsource.com:80/html/politics/2004081642_aurorabridge19m.html |archivedate=December 21, 2007 |df=mdy }}</ref> Construction of the fence began in spring 2010 and was completed in February 2011, at a total cost of $4.8 million.<ref name=WSDOT-Fence/><ref>{{cite web |title=SR 99 Aurora Bridge Fence - Common Questions |url=http://www.wsdot.wa.gov/Projects/SR99/AuroraBridgeFence/FAQ.htm |deadurl=yes |archiveurl=https://web.archive.org/web/20140913192847/http://www.wsdot.wa.gov/Projects/SR99/AuroraBridgeFence/FAQ.htm |archivedate=September 13, 2014 |publisher=Washington State Department of Transportation |date=February 2011 |accessdate=September 27, 2015}}</ref>

== See also ==
* {{Portal-inline|Bridges}}
* {{Portal-inline|Washington}}
* {{Portal-inline|Seattle}}
* [[List of bridges in Seattle]]

== References ==
{{Reflist|2}}

== External links ==
{{commons category|Aurora Bridge}}
* [http://www.seattlefriends.org Seattle FRIENDS]

{{Bridges of Seattle}}
{{Crossings navbox
|structure       = Crossings
|place           = [[Lake Washington Ship Canal]]
|bridge          = George Washington Memorial Bridge (Aurora Bridge)
|bridge signs    = 
|upstream        = [[Ship Canal Bridge]]
|upstream signs  = 
|downstream      = [[Fremont Bridge (Seattle)|Fremont Bridge]]
|downstream signs = 
}}
{{National Register of Historic Places}}
{{Lake Washington Ship Canal|state=collapsed}}
{{Good article}}

[[Category:1932 establishments in Washington (state)]]
[[Category:Bridges completed in 1932]]
[[Category:Bridges in Seattle]]
[[Category:Road bridges on the National Register of Historic Places in Washington (state)]]
[[Category:Cantilever bridges]]
[[Category:National Register of Historic Places in Seattle]]
[[Category:Road bridges in Washington (state)]]
[[Category:U.S. Route 99]]