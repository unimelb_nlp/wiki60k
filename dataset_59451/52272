{{Infobox journal
| title = Metallomics
| cover = [[File:CoverIssueMetallomics.jpg]]
| discipline = [[Chemistry]], [[biology]]
| peer-reviewed = 
| abbreviation = Metallomics
| impact = 3.585
| impact-year = 2014
| editor = May Copsey
| website = http://www.rsc.org/Publishing/Journals/mt/index.asp
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| history = 2009-present
| frequency = Monthly
| formernames =
| openaccess = [[Hybrid open access journal|Hybrid]]
| ISSN = 1756-5901
| eISSN = 1756-591X
| CODEN = METAJS
| LCCN = 2009243228
| OCLC = 437817445
}}
'''''Metallomics''''' is a monthly [[peer-reviewed]] [[scientific journal]] covering the growing research field of [[metallomics]]. It is published by the [[Royal Society of Chemistry]]. The [[editor-in-chief]] is May Copsey.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]/[[CASSI]]<ref name=cassi>{{Cite web
  | title =''Metallomics'' 
  | work =Chemical Abstracts Service Source Index (CASSI) (Displaying Record for Publication) 
  | publisher =[[American Chemical Society]]
  | date =
  | url = http://cassi.cas.org/publication.jsp?P=DXaT8ZV7yNcyz133K_ll3zLPXfcr-WXf_kLOFMYv1UYyz133K_ll3zLPXfcr-WXfXT3to5uCWtUyz133K_ll3zLPXfcr-WXfLfr2nVvq5a4c
  | format = 
  | accessdate =2011-01-29}}</ref>
* [[PubMed]]/[[MEDLINE]]
* [[Science Citation Index]]<ref name=masterList>{{Cite web
  | title =Master Journal search 
  | work =Coverage  
  | publisher =[[Thomson Reuters]]
  | date =
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1756-5901
  | format = 
  | accessdate =2011-03-01}}</ref>
* [[Scopus]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.585.<ref name=WoS>{{cite book |year=2015 |chapter=Metallomics |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
<references/>

== External links ==
{{Portal|Chemistry|Biology}}
* {{Official website|http://www.rsc.org/Publishing/Journals/mt/index.asp}}

{{Royal Society of Chemistry|state=collapsed}}

[[Category:Chemistry journals]]
[[Category:Biology journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 2009]]
[[Category:English-language journals]]
[[Category:Monthly journals]]