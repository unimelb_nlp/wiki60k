{{Redirect|Fighting Irish|the "30 Rock" episode|The Fighting Irish|the ''Family Guy'' episode|Fighting Irish (Family Guy)}}
{{Use mdy dates|date=August 2015}}
{{Refimprove|date=January 2012}}
{{Infobox school athletics
| name = Notre Dame Fighting Irish
| logo = NotreDameFightingIrish.svg
| logo_width = 180
| university = [[University of Notre Dame]]
| association = NCAA
| conference = [[Atlantic Coast Conference|ACC]]<br>[[Hockey East]]<br>[[NCAA Division I FBS independent schools|Independent]] (Football)
| division = [[NCAA Division I|Division I]]
| director = [[Jack Swarbrick]]
| city =  [[Notre Dame, Indiana|Notre Dame]]
| state = [[Indiana]]
| teams = 23
| stadium = [[Notre Dame Stadium]]
| baseballfield = [[Frank Eck Stadium]]
| basketballarena = [[Edmund P. Joyce Center]]
| arena2 = [[Compton Family Ice Arena]]
| mascot = [[Notre Dame Leprechaun|Leprechaun]]
| nickname = Fighting Irish
| fightsong = [[Notre Dame Victory March]]
| pageurl = http://www.und.com/
}}

The '''Notre Dame Fighting Irish''' are the [[Varsity team|varsity]] sports teams of the [[University of Notre Dame]]. The Fighting Irish participate in 23 [[National Collegiate Athletic Association]] (NCAA) [[NCAA Division I|Division I]] intercollegiate sports. The Fighting Irish participate in the NCAA's Division I in all sports, with many teams competing in the [[Atlantic Coast Conference]] (ACC).<ref name="nd_ACC_join_date">{{cite web |last=McMurphy |first=Brett|title=Big East, Notre Dame agree on exit|url=http://www.espn.com/college-sports/story/_/id/9042949/notre-dame-big-east-agree-irish-exit-2-years-early-join-acc|publisher=ESPN |accessdate=March 12, 2013}}</ref> Notre Dame is one of only 15 universities in the United States that plays Division I FBS football and Division I men's ice hockey. The school colors are Gold and Blue<ref>{{cite web | url=http://www.und.com/trads/nd-m-fb-goldandblue.html | publisher=University of Notre Dame | title=Gold and Blue | accessdate=March 28, 2015}}</ref> and the mascot is the Leprechaun.

==Moniker==
{{refimprove|section, except for one footnote,|date=March 2012}}
Just exactly where the moniker "Fighting Irish" came from is a matter of much debate and legend. One possibility is that the nickname is inherited from Irish immigrant soldiers who fought in the Civil War with the Union's [[Irish Brigade (U.S.)|Irish Brigade]]. Notre Dame's claim to the nickname would seem to come from the presence of Fr. [[William Corby]], CSC, the third president of Notre Dame, who was at the [[Battle of Gettysburg]]. Fr. Corby served as chaplain of the Irish Brigade and granted general absolution to the troops in the midst of the battle. This is commemorated in the painting "Absolution Under Fire," part of Notre Dame's permanent art collection. A print of the painting "The Original Fighting Irish" by former Fighting Irish lacrosse player [[Revere La Noue]] is on permanent display at Notre Dame's Arlotta Stadium. The print also hangs in the office of head Notre Dame football coach Brian Kelly, who said that he had to have the work which captures the "swagger" and "toughness" of the football program after seeing it online.<ref>{{cite web|url=http://www.espn.com/ncf/columns/story?columnist=schlabach_mark&id=5062143|title=Kelly changing Notre Dame's focus|accessdate=July 2, 2012|date=April 7, 2010|work=[[ESPN]]|author=Schlabach, Michael}}</ref><ref>{{cite web| title = "Inside The Walls Of Arlotta" To Re-Air At 4:30 p.m. (ET) On Friday – UND.COM – University of Notre Dame Official Athletic Site| url = http://www.und.com/sports/m-lacros/spec-rel/052412aac.html| accessdate = July 23, 2012}}</ref>

The athletes and teams at Notre Dame, now known as the Fighting Irish, were known by many different unofficial nicknames throughout the late 19th and early 20th centuries. During the [[Knute Rockne]] football era, Notre Dame had several unofficial nicknames, among them the "Rovers" and the "Ramblers". These names reflected the teams' propensity to travel the nation to play its football contests, long before such national travel became the collegiate norm. Later, Notre Dame was known unofficially as the "Terriers," after the Irish breed of the dog, and for some years, an [[Irish Terrier]] would be found on the ND football sidelines.

One theory traces back to the visit from Irish freedom fighter and later President of the [[Republic of Ireland]], [[Éamon de Valera]], who had been part of the 1916 Easter Rising and was imprisoned and sentenced to death. He was given amnesty, elected to Parliament and arrested by the English again. He escaped and slipped off to America to avoid recapture. Barnstorming the country, the future president of Ireland was welcomed as a hero at Notre Dame on October 15, 1919. Accounts in Scholastic, a student publication, indicate that his visit tilted campus opinion in favor of the "Fighting Irish" moniker — though not completely. De Valera planted a "tree of liberty" as a memorial of his visit — only to have it uprooted a week later and thrown in one of the campus lakes by a student "of Unionist persuasion."<ref>{{cite web|last1=O'Shaughnessy|first1=Brendan|title=What's In A Name? How Notre Dame became the Fighting Irish|url=https://www.nd.edu/features/whats-in-a-name/|website=University of Notre Dame|publisher=University of Notre Dame|accessdate=23 November 2015}}</ref>

There are several other legends of how Notre Dame came to be the "Fighting Irish." One story suggests the moniker was born in 1899 during a game between Notre Dame and [[Northwestern University|Northwestern]]. The Fighting Irish were leading 5–0 at halftime when the Wildcat fans began to chant, "Kill the Fighting Irish, kill the Fighting Irish," as the second half opened.<ref>{{cite web|title=The Fighting Irish|url=http://www.und.com/trads/nd-m-fb-name.html|publisher=University of Notre Dame|date=August 26, 2015|accessdate=August 26, 2015}}</ref> Another tale has the nickname originating at halftime of the Notre Dame-[[University of Michigan|Michigan]] game in 1909. With his team trailing, one Notre Dame player yelled to his teammates —who had names like Dolan, Kelly, Donnelly, Glynn, Duffy, and Ryan— "What's the matter with you guys? You're all Irish and you're not fighting worth a lick." Notre Dame came back to win the game, and (someone in) the press, after overhearing the remark, reported the game as a victory for the "Fighting Irish."

==Sports==
Notre Dame sanctions 23 varsity sports: 11 men's teams, 11 women's teams, and 1 co-ed team.<ref>{{cite web|url=http://www.und.com/|title=ND Sports website|accessdate=4 January 2016}}</ref>

{{col-begin|width=auto}}
{{col-break}}
*Men's teams
**[[Notre Dame Fighting Irish baseball|Baseball]]
**[[Notre Dame Fighting Irish men's basketball|Basketball]]
**Cross Country
**[[Notre Dame Fighting Irish football|Football]]
**Golf
**[[Notre Dame Fighting Irish men's ice hockey|Hockey]]
**[[Notre Dame Fighting Irish men's lacrosse|Lacrosse]]
**[[Notre Dame Fighting Irish men's soccer|Soccer]]
**Swimming & Diving
**Tennis
**Track and Field
{{col-break|gap=2em}}

*Women's teams
**[[Notre Dame Fighting Irish women's basketball|Basketball]]
**Cross Country
**Golf
**Lacrosse
**Rowing
**[[Notre Dame Fighting Irish women's soccer|Soccer]]
**[[Notre Dame Fighting Irish softball|Softball]]
**Swimming & Diving
**Tennis
**Track & Field
**Volleyball
{{col-end}}

*Co-ed sports
**[[Notre Dame Fighting Irish fencing|Fencing]]

==Conference affiliation==
Notre Dame was a member of the [[Big East Conference (1979–2013)|"old" Big East Conference]] until 2013. It is currently a member of the [[Atlantic Coast Conference]] in all sports except for the following:
*Football, in which it maintains its status as one of a handful of [[NCAA Division I FBS independent schools|Division I FBS Independents]].
*Men's [[ice hockey|hockey]] competes in [[Hockey East]]. Their former hockey conference, the [[Central Collegiate Hockey Association]], disbanded after the 2012–13 season due to [[2010–13 NCAA conference realignment#Hockey|a major realignment]] of hockey conferences. Notre Dame will join the Big Ten Conference in hockey starting in the 2017-2018 season.

According to men's basketball Coach Mike Brey, Notre Dame seriously considered joining the [[Big Ten Conference]] in 2003, with the decision to not proceed occurring at the "11th hour."<ref name="trib20100108">"[http://articles.chicagotribune.com/2010-01-08/sports/1001070969_1_big-ten-mike-brey-preparing Mike Brey was preparing to join Big Ten in 2003 / But Notre Dame basketball coach says Irish decided against move at last minute]" ''Chicago Tribune'', January 8, 2010.</ref>

==Championships==

===NCAA team championships===
Notre Dame has won 17 NCAA team national championships.<ref>{{cite web|url=http://fs.ncaa.org/Docs/stats/champs_records_book/Overall.pdf |title=NCAA Records |accessdate=2016-09-12}}</ref>

*'''Men's (7)'''
**[[NCAA Men's Division I Cross Country Championship#Team titles|Cross Country]] (1): 1957
**[[NCAA Fencing Championships#Mens team titles|Fencing]] (3): 1977, 1978, 1986
**[[NCAA Division I Men's Golf Championships#Team titles|Golf]] (1): 1944
**[[NCAA Division I Men's Soccer Championship#Team titles|Soccer]] (1): 2013
**[[NCAA Men's Tennis Championship#Team titles|Tennis]]  (1): 1959
*'''Women's (5)'''
**[[NCAA Women's Division I Basketball Championship#Team titles|Basketball]] (1): 2001
**[[NCAA Fencing Championships#Womens team titles|Fencing]] (1): 1987
**[[NCAA Division I Women's Soccer Championship#Team titles|Soccer]]  (3): 1995, 2004, 2010
*'''Co-ed (5)'''
**[[NCAA Fencing Championships#Co-ed team titles|Fencing]] (5): 1994, 2003, 2005, 2011, 2017
*see also:
**[[Atlantic Coast Conference#NCAA team championships|ACC NCAA team championships]]
**[[List of NCAA schools with the most NCAA Division I championships#NCAA Division I Team Championships|List of NCAA schools with the most NCAA Division I championships]]

===Other national team championships===
Below are 14 national team titles claimed by Notre Dame [[List of NCAA schools with the most Division I national championships|that were not bestowed by the NCAA]]:

*'''Men's (14)'''
**[[Helms Athletic Foundation#Basketball|Basketball]] (2): 1927, 1936
**Tennis (1): 1944
**[[College football national championships in NCAA Division I FBS|Football]] (11):1924, 1929, 1930, 1943, 1946, 1947, 1949, 1966, 1973, 1977, 1988
*see also:
**[[List of NCAA schools with the most Division I national championships]]

==Football==
{{main|Notre Dame Fighting Irish football}}

* Head Coach: [[Brian Kelly (coach)|Brian Kelly]]
* Stadium: [[Notre Dame Stadium]]
* National Championships: 11 Consensus (1924, 1929, 1930, 1943, 1946, 1947, 1949, 1966, 1973, 1977, 1988). There are other years (1919, 1920, 1938, 1953, 1964, 1989, 1993, 2012) where various polls claim Notre Dame as a National Champion, but those years are not consensus titles, and thus are not claimed by the university.

[[File:Notre-dame-stadium.jpg|thumb|[[Notre Dame Stadium]]]]
The school has a comprehensive and nationally competitive Division I athletic program, but it is most famous for its [[College football|football]] program. Notre Dame fielded its first football team in 1887. With eleven [[NCAA Division I-A national football champions|football championships acknowledged by the NCAA]], over 800 [[College football's ten most victorious programs|all-time wins]], seven [[Heisman Trophy]] winners, famous head coaches, a 73.6% winning percentage and the most consensus [[All-America]]ns of any school, Notre Dame football is one of the most storied programs both on the gridiron and college athletics in general. Recently, Notre Dame has struggled, going through several head coaches and setting the all-time bowl losing streak of nine straight with the loss to [[2006 LSU Tigers football team|LSU]] in the [[2007 Sugar Bowl]] before beating [[2008 Hawaii Warriors football team|Hawaii]] in the [[2008 Hawaii Bowl]]. Notre Dame's is also the only football program in the nation, including both collegiate and professional ones, with every home game being on national broadcast television.

In addition to having the oldest university marching band in the country, the school has many rivalries in football, the most famous ones being with [[USC Trojans football|USC]], [[Navy Midshipmen football|Navy]], [[Michigan State Spartans football|Michigan State]], [[Army Black Knights football|Army]], [[Purdue Boilermakers football|Purdue]], and [[Michigan Wolverines football|Michigan]]. Notre Dame played in arguably the greatest, although certainly not the most-watched (due to Notre Dame games' already having been broadcast nationally that season as many times as allowed, ABC had to relegate its broadcast to a regional one), college football game in history: the famous [[1966 Notre Dame vs. Michigan State football game|10–10 tie]] against [[1966 Michigan State Spartans football team|Michigan State]] at [[Spartan Stadium, East Lansing|Spartan Stadium]] on November 19, 1966. Other Notre Dame rivalries include those with [[Stanford Cardinal football|Stanford]], [[Boston College Eagles football|Boston College]], and [[Pittsburgh Panthers football|Pittsburgh]]. Former rivalries include a very intense rivalry in the 1980s with [[Miami Hurricanes football|Miami]] ([[Catholics vs. Convicts]]), and a rivalry with [[Penn State Nittany Lions football|Penn State]], which was renewed and played on September 9, 2006, and again during the 2007 season. The football program is also known for ending the [[Oklahoma Sooners football|Oklahoma]] [[NCAA]] record winning streak of 47 games. The streak-ending game was a 7–0 victory for the Fighting Irish on November 9, 1957. Incidentally, Oklahoma's 28–21 loss to Notre Dame to open the 1953 season was the last loss before the beginning of the streak. In 2012, Oklahoma (at the time 6-1) was favored to defeat Notre Dame (at the time 9-0) by 18 points. Notre Dame ending up winning 30–13 thanks to LB Manti T'eo's game clinching interception late in the 4th quarter, Notre Dame's stellar defense, and a bad snap in the first quarter by Oklahoma's center that sent them back to the 14-yard line. Thanks to the win by Notre Dame, the rivalry has been renewed. The last time Notre Dame reached the National Championship was in 2012 where they suffered a 42–14 loss to Alabama.

==Basketball==

===Men's===
{{main|Notre Dame Fighting Irish men's basketball}}

* Head Coach: [[Mike Brey]]
* Arena: Purcell Pavilion at the [[Joyce Center]]
* ACC Titles: 2015
* National Championships: 2 (1927*, 1936*)
* Final Fours: 1 (1978)

<nowiki>*</nowiki> Pre-tournament era Helms Trophy

The men's basketball team, coached by Mike Brey since 2000, has made 28 [[NCAA Men's Division I Basketball Championship|NCAA Tournament]] appearances and made it to the [[NCAA Men's Division I Basketball Championship#Final Four|Final Four]] in 1978 under coach [[Digger Phelps]]. They are also known for ending [[UCLA Bruins men's basketball|UCLA]]'s 88-game winning streak in 1974, a streak which had begun after Notre Dame had previously ended UCLA's 45-game winning streak in 1971. Notre Dame won the 2015 ACC Tournament and advanced to the Elite Eight only to fall to top-ranked Kentucky 68-66.

===Women's===
{{main|Notre Dame Fighting Irish women's basketball}}

* Head Coach: [[Muffet McGraw]]
* Arena: Purcell Pavilion at the [[Joyce Center]]
* ACC Titles: 4 (2014, 2015, 2016, 2017)
* National Championships: 1 (2001)
* Final Fours: 7 (1997, 2001, 2011, 2012, 2013, 2014, 2015)

Notre Dame's women's basketball team, coached by [[Muffet McGraw]], won the [[NCAA Women's Division I Basketball Championship|National Championship]] in 2001 by beating [[Purdue Boilermakers women's basketball|Purdue]] 68-66. The 2001 team was led by 6-foot-5 center [[Ruth Riley]], who is still active in the [[Women's National Basketball Association|WNBA]]. Notre Dame has made it to the Sweet Sixteen in 11 out of the last 14 seasons, and has had 20 win seasons in 18 out of the past 19 seasons. McGraw has led the Fighting Irish to 14 NCAA tournament appearances including a current streak of 12 straight. McGraw would take the Fighting Irish back to the Final Four in 2011, beating [[Pat Summitt]]'s [[Tennessee Lady Volunteers basketball|Tennessee Lady Volunteers]]; the program's first win against the Lady Vols in 21 tries. That win was followed by an upset of the number one-ranked [[2010-11 Connecticut Huskies women's basketball team|UConn Huskies]] (making Notre Dame the first team ever to beat both Tennessee and UConn in the same tournament) to advance the Fighting Irish to the 2011 championship game, where it lost to [[2010–11 Texas A&M Aggies women's basketball team|Texas A&M]]. The Irish would return to the championship game in 2012, losing to unbeaten [[2011–12 Baylor Lady Bears basketball team|Baylor]]. In 2014 the #2 ranked Fighting Irish lost to #1 ranked Connecticut in the NCAA Championship Game. In 2015 the # 2 ranked Fighting Irish again lost to top-ranked Connecticut 63-53 in the title game.

==Fencing==
The Notre Dame men's and women's [[fencing]] teams have won 9 national titles — the men's team won titles in 1977, 1978 and 1986 while the women's team won the 1987 title. After the [[NCAA]] replaced the individual men's and women's national titles with a combined fencing championship, Notre Dame won national titles in 1994, 2003, 2005, 2011, and most recently, in 2017. During the 2010 regular season, Notre Dame went undefeated in both men's and women's fencing.

==Ice hockey==
{{main|Notre Dame Fighting Irish men's ice hockey}}

* Head Coach: [[Jeff Jackson (ice hockey b. 1955)|Jeff Jackson]]
* Arena: [[Compton Family Ice Arena]]<ref name="University of Notre Dame Hockey website">{{cite web|url=http://www.und.com/sports/m-hockey/nd-m-hockey-body.html|title=UND.COM – University of Notre Dame Official Athletic Site – Ice Hockey|work=und.com}}</ref> (Formerly at Joyce Center, 1968–2011)
* Conference Titles (CCHA): 2 (2007, 2009)
* Frozen Four Appearances: 3 (2008, 2011, 2017)

Notre Dame's men's [[ice hockey]] team, coached by Jeff Jackson and captained by T.J. Jindra, won both the [[Central Collegiate Hockey Association]] (CCHA) season and [[CCHA Tournament|tournament]] championships in 2007 with a record of 28-6-3. They were the #2 overall seed in the [[2007 NCAA Division I Men's Ice Hockey Tournament|2007 NCAA Men's Hockey Tournament]], behind [[Minnesota Golden Gophers men's ice hockey|Minnesota]], and were the #1 seed in the Midwest bracket. They lost to Michigan State in the second round of the NCAA tournament.

Notre Dame was a #4 seed in the 2008 NCAA Tournament and faced #1 seed [[University of New Hampshire|New Hampshire]]. They beat New Hampshire 7-3 and then faced [[Michigan State Spartans|Michigan State]], the same team that knocked them out of the tournament the previous year. This time, the Fighting Irish defeated the Spartans 3-1 and earned their first trip in school history to the [[Frozen Four]]. In the semi-final they defeated the overall #1 seeded [[Michigan Wolverines|Michigan]] 5–4 in overtime, earning them their first ever national championship berth against [[Boston College]], in which they were defeated 4–1.

==Lacrosse==

===Men's===
{{main|Notre Dame Fighting Irish men's lacrosse}}

* Head Coach: Kevin Corrigan
* Field: Arlotta Stadium, Loftus Sports Center
* Conference Titles (MLA): 8 (1982, 1984, 1986, 1988, 1990, 1992, 1993)
* Conference Titles (GWLL): 12 (1994, 1995, 1996, 1997, 1999, 2000, 2001, 2002, 2003, 2007, 2008, 2009)
* NCAA Tournament Appearances: 20 (1990, 1992, 1993, 1994, 1995, 1996, 1997, 1999, 2000, 2001, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015 )
* Final Four Appearances: 4 (2001, 2010, 2014, 2015, 2016)

The Notre Dame men's [[lacrosse]] team has made the [[NCAA Division I Men's Lacrosse Championship|NCAA lacrosse tournament]] every year since 2006, reaching the national semifinals (Final Four) in [[2001 NCAA Division I Men's Lacrosse Championship|2001]] and [[2010 NCAA Division I Men's Lacrosse Championship|2010]] and the national championship game in 2010, in which it lost to [[Duke Blue Devils men's lacrosse|Duke]] by one goal in overtime, 6-5. In 2009, the Fighting Irish went undefeated in the regular season, reached #2 in national polls, and finished with an overall record of 15–1. In 2014 #5 ranked Notre Dame advanced to the NCAA Championship match only to lose to #1 ranked Duke 11-9.

===Women's===
The Notre Dame women's [[lacrosse]] team reached the NCAA semifinal round (Final Four) in 2006. In 2010, they reached the NCAA tournament for the 3rd straight year, the longest streak in school history. The Fighting Irish advanced to the second round of the 2014 NCAA Lacrosse Championship before losing to Duke 10-8.

==Soccer==

===Men's===
{{main|Notre Dame Fighting Irish men's soccer}}
* Head Coach: [[Bobby Clark (footballer)|Bobby Clark]]
* Field: Alumni Stadium
* National Championships: 1 (2013)
* College Cup Appearances: 1 (2013)

===Women's===
{{main|Notre Dame Fighting Irish women's soccer}}

* Head Coach: Theresa Romagnolo
* Field: Alumni Stadium
* Conference Titles (MCC)*: 4 (1991, 1992, 1993, 1994)
* Conference Titles (Big East*): 10 (1995, 1996, 1997, 1998, 1999, 2000, 2001, 2005, 2006, 2008)
* National Championships: 3 (1995, 2004, 2010)
* College Cup Appearances: 17 (1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010)

<nowiki>*</nowiki>Notre Dame was a member of the [[Horizon League|Midwestern Collegiate Conference]] and [[Big East Conference (1979–2013)|Big East Conference]] in soccer prior to joining the ACC in most sports.

Notre Dame's women's soccer team won the [[NCAA Women's Soccer Championship|National Championship]] in 1995, 2004 and 2010 and were the [[runner-up]] in 1994, 1996, 1999, 2006, and 2008. Notre Dame is one of only three schools with multiple national titles, the others being North Carolina (21) and Portland (2). Notre Dame also ranks second in all-time title game appearances (8) behind North Carolina (23). ND's women's soccer program started in 1988 under coach Chris Petrucelli. Their 1995 Big East title was the university's first in any sport. That same year, Petrucelli's squad, under the leadership of [[Cindy Daws]], won the program's first national title, defeating Portland 1–0. Notre Dame's current coach, Randy Waldrum, took over the program in 1999 and has maintained the Fighting Irish's success, winning the national title in 2004 by beating UCLA 4–3 as well as capturing six Big East titles. Waldrum's 2010 squad won the school's third national title, going 21-2-2 and posting 15 shutouts and became the lowest ranked team to do so, beating undefeated Stanford in a 1–0 decision. In doing so, they outscored their postseason opponents 15-1. They also reached the College Cup for the 5th straight year, a school record. Their senior class won 87 matches in their 4 years, the most in that span. Three Notre Dame players have won the [[Hermann Trophy]], given to the United States' best male and female collegiate soccer players. They are Cindy Daws (1996), [[Anne Mäkinen]] (2000) and [[Kerri Hanks]] (2006, 2008). Hanks is one of only four players to win the award twice. Notre Dame is also one of only two schools with three or more different Hermann Trophy recipients.<ref name="Women's soccer history">{{cite web |url=http://www.und.com/auto_pdf/p_hotos/s_chools/nd/sports/w-soccer/auto_pdf/08wsocmghistory|title= 2008 Women's Soccer Media Guide |accessdate= April 8, 2009}}</ref>

==Men's golf==
The men's golf team has won 11 conference championships:
*[[Horizon League]] (3): 1988–89, 1995
*[[Big East Conference (1979–2013)|Big East Conference]] (8): 1995–97, 2004–06, 2011–12

They won the [[NCAA Division I Men's Golf Championships|NCAA Championship]] in 1944.

==Notable non-varsity sports==

===Rugby===

{{main|Notre Dame Rugby Football Club}}
Founded in 1961, the Notre Dame rugby club was one of the oldest college rugby clubs in the Midwest, before the club was disbanded in 1995.<ref>{{cite web|url=http://www.nd.edu/~observer/04192002/Sports/0.html |title=Archived copy |accessdate=2012-05-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20120214070737/http://www.nd.edu/~observer/04192002/Sports/0.html |archivedate=February 14, 2012 |df=mdy }}</ref> Notre Dame reinstated rugby in 2007, however, due in part to the "explosive growth of rugby in the nation's Catholic high schools" and Notre Dame's desire to offer a program to attract rugby-playing students.<ref name="erugbynews.com">{{cite web|url=http://www.erugbynews.com/article.php?sec=364&a=2634 |title=eRugbyNews.com |publisher=eRugbyNews.com |date=1969-12-31 |accessdate=2016-09-12}}</ref> Notre Dame began the 2007–08 season in Division 2, but their 8-1-1 record merited a promotion to Division 1 in the spring of 2008.<ref>{{cite web|author=ENR // AgencyND // University of Notre Dame |url=http://rugby.nd.edu/about/ |title=About // Notre Dame Rugby // University of Notre Dame |publisher=Rugby.nd.edu |date= |accessdate=2016-09-12}}</ref>

Notre Dame finished the 2010–11 season ranked 19th in the nation.<ref>[https://web.archive.org/web/20120620060100/http://www.rugbymag.com:80/cpl/988-final-cpd-rankings-for-2010-2011.html]</ref> Notre Dame's rugby program has the support and commitment of the school and alumni, with an endowment fund rumored to be over $1 million.<ref name="erugbynews.com"/> The team is coached by Sean O'Leary, who has also coached the US U-17 national team. Notre Dame also plays every year in the [[Collegiate Rugby Championship]] (CRC). The CRC is the highest profile [[college rugby]] competition in the United States, broadcast live on NBC each year. Notre Dame finished 10th in the 2011 CRC, with wins over Boston College, Ohio State and Navy.

==Other sports==
John A. Kromkowski, (BA '60)(MA '61)(Phd '72), won the National Intercollegiate Men's Singles [[Table Tennis]] championship in 1959 defeating Paul S. Kochanowski (BA '61) 3–0. Playing together Kromkowski and Kochanowski won the Men's Doubles championship that year and they won the "Teams".<ref>Boggan, T. History of U.S. Table Tennis, Vol III, Chapter XXXII (2003)</ref>

==Pageantry==
:'''Team Colors:'''  Gold and Blue
:'''Outfitter:''' [[Under Armour]]
:'''Fight Song:''' [[Notre Dame Victory March]]
:'''Alma mater:''' [[Notre Dame, Our Mother]]
:'''Nicknames:''' [[Fighting Irish]]
:'''Rivalries:''' [[USC Trojans]], [[Michigan Wolverines]], [[Michigan State Spartans]], [[Purdue Boilermakers]], [[Boston College Eagles]] & [[Navy Midshipmen]]
:'''Mascot:''' The Leprechaun
:'''Marching Band:''' The [[Band of the Fighting Irish]]

==Athletic directors==
{| class="wikitable"
|-
!Athletic director
!Years
|-
|[[Jesse Harper]]
|1913–1917, 1931–1933
|-
|[[Knute Rockne]]
|1920–1930
|-
|[[Elmer Layden]]
|1934–1940
|-
|[[Hugh Devore]]
|1945
|-
|[[Frank Leahy]]
|1947–1949
|-
|[[Moose Krause]]
|1949–1981
|-
|[[Gene Corrigan]]
|1981–1987
|-
|[[Dick Rosenthal]]
|1987–1995
|-
|[[Mike Wadsworth]]
|1995–2000
|-
|[[Kevin White (athletic director)|Kevin White]]
|2000–2008
|-
|[[Jack Swarbrick]]
|2008–present
|}

==References==
{{Reflist|30em}}

==External links==
{{Portal|ACC}}
* {{Official website}}

{{University of Notre Dame}}
{{Navboxes
|titlestyle = {{CollegePrimaryStyle|Notre Dame Fighting Irish|color=white}}
|list =
{{Atlantic Coast Conference navbox}}
{{Hockey East}}
{{Indiana Sports}}
}}

[[Category:Notre Dame Fighting Irish|*]]