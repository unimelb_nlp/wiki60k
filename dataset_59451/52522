{{Infobox journal
| title = Pepperdine Law Review
| cover =
| editor = Helen Andrews
| discipline = [[Law review]]
| abbreviation = Pepperdine Law Rev.
| publisher = [[Pepperdine University School of Law]]
| country = United States
| frequency = 5/year
| history = 1973-present
| openaccess =
| impact =
| impact-year =
| website = http://pepperdinelawreview.com
| link1 = http://digitalcommons.pepperdine.edu/plr
| link1-name = Online access
| ISSN = 0092-430X
| LCCN = 73647780
| OCLC = 01789808
}}
The '''''Pepperdine Law Review''''' (''[[Bluebook]]'' abbreviation: ''Pepp. L. Rev.'') is a student-edited [[law journal]] published by the [[Pepperdine University School of Law]]. It is available on [[Westlaw]] and [[LexisNexis]].

== Membership ==
The law review is student-edited and staffed by second and third year students at Pepperdine.<ref>{{cite web|url=http://law.pepperdine.edu/law-review/staff/ |title=Staff Members &#124; Pepperdine Law Review &#124; School of Law &#124; Pepperdine University |publisher=Law.pepperdine.edu |date= |accessdate=2012-03-06}}</ref> Students in the top 10% of their first-year class may elect to join the journal's staff ("grading on"), and other students in the top 50% may seek membership by participating in an anonymously graded writing competition ("writing on").

=== Notable alumni ===
*[[Jeffrey S. Boyd]], Volume 18 [[editor-in-chief]]: Justice of the [[Texas Supreme Court]]<ref>{{cite web |url=http://law.pepperdine.edu/news-events/news/2012/10/jeffrey-boyd.htm |title=Jeffrey Boyd Appointed to Texas Supreme Court |publisher=Pepperdine University |work=News and Events |accessdate=2012-11-30}}</ref>
*[[Beverly Reid O'Connell]], Volume 17 managing editor: Judge of the United States District Court for the Central District of California<ref>{{cite web |url=http://law.pepperdine.edu/news-events/featured-stories/2013/04/oconnell-confirmation.htm |title=Judge Beverly Reid O’Connell Confirmed to U.S. District Court |publisher=Pepperdine University |work=News and Events |accessdate=2013-09-16}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.pepperdinelawreview.com/}}
{{Pepperdine University}}

[[Category:American law journals]]
[[Category:General law journals]]
[[Category:Law journals edited by students]]
[[Category:Publications established in 1973]]
[[Category:English-language journals]]
[[Category:Pepperdine University]]