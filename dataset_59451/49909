{{redirect|The Wreck of the Titan|the ''Doctor Who'' audio drama|The Wreck of the Titan (audio drama)}}
{{italic title}}
'''''The Wreck of the Titan: Or, Futility''''' (originally called '''''Futility''''') is an 1898 [[novella]] written by [[Morgan Robertson]]. The story features the fictional [[ocean liner]] ''Titan'', which sinks in the North Atlantic after striking an [[iceberg]]. The ''Titan'' and its sinking have been noted to be very similar to the real-life passenger ship {{RMS|Titanic}}, which [[Sinking of the RMS Titanic|sank]] fourteen years later.  Following the sinking of the ''Titanic'', the novel was reissued with some changes, particularly in the ship's gross tonnage.<ref name="historyonthenet">{{cite web | url=http://historyonthenet.com/Titanic/futility.htm | title=The Titanic -Futility | website=HistoryOnTheNet.com | accessdate=2015-01-09}}</ref>

==Plot==
The first half of ''Futility'' introduces the hero John Rowland. Rowland is a disgraced former US Navy officer. Now an alcoholic fallen to the lowest levels of society, he's been dismissed from the Navy and works as a [[deckhand]] on the ''Titan''. One April night the ship hits an iceberg, sinking somewhat before the halfway point of the novel.

The second half follows Rowland. He saves the young daughter of a former lover by jumping onto the iceberg with her. The pair find a lifeboat washed up on the iceberg, and are eventually rescued by a passing ship. But the girl is recovered by her mother and Rowland is arrested for her kidnapping. A sympathetic magistrate discharges him and rebukes the mother for unsympathy to her daughter's savior. Rowland disappears from the world.

In a brief final chapter covering several years, Rowland works his way up from homeless and anonymous fisherman to a desk job and finally, two years after passing his civil service exam, to "a lucrative position under the Government, and as he seated himself at the desk in his office, could have been heared to remark: 'Now John Rowland, your future is your own. You have merely suffered in the past from a mistaken estimate of the importance of women and whisky.' THE END" (1898 edition at Google Books).<ref name=1898novella/>

A later edition includes a coda. Rowland receives a letter from the mother, who congratulates him and pleads for him to visit her, and the girl who begs for him. (External links: undated edition at titanic-titanic.com)

== Similarities to the ''Titanic'' ==

Although the novel was written before the ''[[RMS Titanic]]'' was even conceptualized, there are some [[wikt:uncanny|uncanny]] similarities between both the fictional and real-life versions. Like the ''Titan'', the fictional ship sank in April in the North Atlantic, and there were not enough lifeboats for all the passengers. There are also similarities between the size ({{convert|800|ft|0|abbr=on}} long for ''Titan'' versus {{convert|882|ft|9|in|abbr=on|0}} long for the ''Titanic''<ref name="McCluskie, Anatomy of the Titanic, 22" >{{Cite book |title=Anatomy of the Titanic |last=McCluskie |first=Tom |year=1998 |publisher=PRC |isbn=1-85648-482-3 |ref=McCluskie, Anatomy of the Titanic |page=22
}}</ref>), speed (25 knots for ''Titan'', 22.5 knots for ''Titanic''<ref name="McCluskie, Anatomy of the Titanic, 23" >McCluskie, ''Anatomy of the Titanic'', p.&nbsp;23: ''Titanic''{{'s}} top speed was 21 knots, with a [[flank speed]] of 23.5 knots</ref>) and life-saving equipment.

Beyond the name, the similarities between the ''Titanic'' and the fictional ''Titan'' include:<ref name=1898novella>Robertson, Morgan (1898). ''[https://books.google.com/books?id=pOxEAQAAMAAJ Futility]''. New York: M. F. Mansfield.</ref>

*Both were triple screw (propeller)
*Described as "unsinkable"
**The ''Titan'' was the largest craft afloat and the greatest of the works of men (800 feet, displacing 75,000 tons, up from 45,000 in the 1898 edition).  The Titanic was 46,000 tons and 882 feet long and was deemed "practically unsinkable" (as quoted in Robertson's book).
*Shortage of lifeboats
**The ''Titanic'' carried only 16 [[Lifeboat (shipboard)|lifeboat]]s, plus 4 ''Engelhardt'' folding lifeboats,.<ref name="McCluskie, Anatomy of the Titanic, 120" >McCluskie, ''Anatomy of the Titanic'', p.&nbsp;120</ref>
**The ''Titan'' carried "as few as the law allowed", 24 lifeboats, which could carry less than half of her total complement of 3,000.
*Struck an iceberg
**Moving at 22½ [[knot (unit)|knots]],<ref>Mowbray, Jay Henry (1912). Sinking of the Titanic. Harrisburg, PA: The Minter Company. OCLC 9176732</ref> the ''Titanic'' struck an iceberg on the starboard side on the night of April 14, 1912, in the [[North Atlantic]], {{convert|400|nmi}} away from Newfoundland.
**Moving at 25 knots, the ''Titan'' also struck an iceberg on the starboard side on an April night in the North Atlantic, {{convert|400|nmi}} from Newfoundland (Terranova).
*Sinking
**The ''Titanic'' sank, and more than half of her 2,200 passengers and crew died.  Of the ''Titanic'''s crew and passengers, 705 survived. 1,523 were lost.
**The ''Titan'' also sank, and more than half of her 2,500 passengers also died. In fact, only 13 ultimately survived the disaster.

Following the ''Titanic'''s sinking, some people credited Robertson with clairvoyance.  Robertson denied this, claiming the similarities were explained by his extensive knowledge of shipbuilding and maritime trends.<ref>Hasan, Heba. "[http://newsfeed.time.com/2012/04/14/author-predicts-titanic-sinking-14-years-earlier/ Author 'Predicts' ''Titanic'' Sinking, 14 Years Earlier]".  ''Time'', April 14, 2012.</ref>

==See also==
*[[List of fictional ships]]
*[[Synchronicity]]

==References==
{{reflist|25em}}

== Further reading == <!-- formerly "Bibliography" but none is cited 2016-07-23 -->
*{{cite web|author=Cocksey, Brian |title=The Titan & the Titanic |work=Light Eternal Publishing Limited |url=http://www.light-eternal.com/Titan.htm |accessdate=2005-11-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20110807165439/http://www.light-eternal.com/Titan.htm |archivedate=2011-08-07 |df= }}
*{{cite book |author1=Rutman, Sharon |author2=Stevenson, Jay | title=The Complete Idiot's Guide to the Titanic | publisher=Alpha Books | year=1998 | isbn=0-02-862712-1}}
*{{cite web | author=Flavio Cenni | title=The Titanic before the Titanic| work=| url=http://digilander.libero.it/flavio.cenni/| accessdate=2009-01-13 }}

==External links==
{{Wikisource|The Wreck of the Titan: Or, Futility}}
* {{librivox book | title=Futility, Or the Wreck of the Titan | author=Morgan ROBERTSON}}
* [http://www.titanic-titanic.com/wreck_of_the_titan_1.shtml On-line edition] (undated novella, as '' 'The Wreck of the Titan' or 'Futility' ''
* [https://archive.org/details/wrecktitanorfut01robegoog On-line edition] (250-page novel, 1912)

{{RMS Titanic}}

[[Category:1898 novels]]
[[Category:19th-century American novels]]
[[Category:American novellas]]
[[Category:RMS Titanic]]