{{For2|other ships of the same name|{{HMS|Canada}}}}
{{Use dmy dates|date=April 2017}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:Chilean battleship Almirante Latorre.jpg|350px]]
|Ship caption=''Almirante Latorre'' in 1921
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag={{shipboxflag|UK|naval}}
|Ship name=''Canada''
|Ship namesake=
|Ship ordered=
|Ship awarded=
|Ship builder= [[Armstrong Whitworth]], [[Elswick, Tyne and Wear|Elswick]]
|Ship original cost= 
|Ship yard number=
|Ship way number=
|Ship laid down=27 November 1911
|Ship launched=27 November 1913, as ''Almirante Latorre''
|Ship sponsor=
|Ship christened=
|Ship completed=
|Ship acquired=9 September 1914
|Ship commissioned=15 October 1915
|Ship decommissioned=March 1919
|Ship renamed=
|Ship reclassified=
|Ship refit=
|Ship struck=
|Ship reinstated=
|Ship homeport=
|Ship identification=
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate=Resold to Chile, April 1920
|Ship status=
|Ship notes=
|Ship badge=
}}
{{Infobox ship career
|Hide header=title
|Ship country=Chile
|Ship flag=[[File:Naval Jack of Chile.svg|35px|]]
|Ship name= ''Almirante Latorre''
|Ship acquired=April 1920
|Ship commissioned=1 August 1920
|Ship decommissioned=October 1958
|Ship renamed=
|Ship reclassified=
|Ship refit=1929–1931
|Ship struck=
|Ship homeport=
|Ship identification=
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate=[[ship breaking|Scrapped]] in Japan, 1959
|Ship status=
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Almirante Latorre|battleship}}
|Ship displacement=*{{convert|25000|LT|t|0|lk=in|abbr=on}} standard
*{{convert|32000|LT|t|0|abbr=on}} full load
|Ship length= {{convert|625|ft|m|abbr=on}}
|Ship beam= {{convert|92.5|ft|m|abbr=on}}
|Ship draught= {{convert|33|ft|m|abbr=on}}
|Ship propulsion=*21 [[Water-tube boiler#Yarrow|Yarrow boilers]]
*Low pressure [[C. A. Parsons and Company|Parsons]] and High pressure [[John Brown & Company|Brown-Curtis]] [[steam turbine]]s
*{{convert|37000|shp|0|abbr=on}}
*Coal and oil fuel
|Ship speed= {{convert|22.75|kn|lk=in}}
|Ship range= 
|Ship complement=834 officers and men
|Ship armament=*10 × [[EOC 14 inch /45 naval gun|{{convert|14|in|mm|0|abbr=on}}/45 caliber]] [[British ordnance terms#BL|BL]] guns
*16 × [[BL_6_inch_naval_guns_Mk_XIII_-_XVIII#6-inch_Mark_XVII_gun_in_Royal_Navy_service|{{convert|6|in|mm|0|abbr=on}} guns]]
*2 × {{convert|3|in|mm|0|abbr=on}} anti-aircraft guns
*4 × 3-pounder guns
*4 × {{convert|21|in|mm|0|abbr=on}} [[torpedo tube]]s (submerged)
|Ship armor=*[[Belt armor|Belt]]: {{convert|9|in|mm|abbr=on}}
*Deck: {{convert|1.5|in|mm|abbr=on}}
*[[Barbette]]: {{convert|10|in|mm|abbr=on}}
*Turret: {{convert|10|in|mm|abbr=on}}
*[[Conning tower]]: {{convert|11|in|mm|abbr=on}}
|Ship aircraft=
|Ship aircraft facilities=
|Ship notes=
}}
|}
'''''Almirante Latorre''''', named after [[Juan José Latorre]], was a [[super-dreadnought]] [[battleship]] built for the [[Chilean Navy]] (''Armada de Chile''). She was the first of a [[Almirante Latorre-class battleship|planned two-ship class]] that would respond to [[South American dreadnought race|earlier warship purchases by other South American countries]]. Construction began at [[Elswick, Tyne and Wear|Elswick]], [[Newcastle upon Tyne]] soon after the ship was ordered in November 1911, and was approaching completion when she was bought by the United Kingdom's [[Royal Navy]] for use in the [[World War I|First World War]].  [[ship commissioning|Commissioned]] in September 1915, she served in the [[Grand Fleet]] as '''HMS ''Canada''''' for the duration of the war and saw action during the [[Battle of Jutland]].

Chile repurchased ''Canada'' in 1920. She took back her original name of ''Almirante Latorre'', and served as the Chilean flagship and frequently as presidential transport. She underwent a thorough modernization in the United Kingdom in 1929–1931.  In September 1931, crewmen aboard ''Almirante Latorre'' instigated [[Chilean naval mutiny of 1931|a mutiny]], which the majority of the Chilean fleet quickly joined. After divisions developed between the mutineers, the rebellion fell apart and the ships returned to government control. ''Almirante Latorre'' was placed in reserve for a time in the 1930s because of the [[Great Depression in Latin America|Great Depression]], but she was in good enough condition to receive interest from the United States after the [[attack on Pearl Harbor]]. The Chilean government declined the overture and the ship spent most of the Second World War on patrol for Chile. She was [[ship breaking|scrapped]] in Japan beginning in 1959.

== Background ==
{{main|South American dreadnought race}}
''Almirante Latorre''{{'}}s genesis can be traced to the numerous naval arms races between Chile and Argentina, which in turn were spawned by territorial disputes over their mutual borders in [[Patagonia]] and [[Puna de Atacama]], along with control of the [[Beagle Channel]]. Naval races flared up in the 1890s and in 1902; the latter was eventually settled via British mediation. Provisions in the dispute-ending treaty imposed restrictions on both countries' navies. The United Kingdom's [[Royal Navy]] bought the two [[Swiftsure-class battleship|''Constitución''-class]] [[pre-dreadnought]] battleships that were being built for Chile, and Argentina sold its two [[Kasuga-class cruiser|''Rivadavia''-class]] [[armored cruiser]]s under construction in Italy to Japan.<ref>Scheina, ''Naval History'', 45–52.</ref><ref>Garrett, "Beagle Channel Dispute", 86–88.</ref>

After {{HMS|Dreadnought|1906|6}} was commissioned, Brazil decided in early 1907 to halt construction of three obsolescent [[pre-dreadnought battleship|pre-dreadnoughts]] in favor of two or three [[dreadnought]]s.<ref name=Whitley24>Whitley, ''Battleships of World War Two'', 24.</ref> These ships, commissioned as the {{sclass-|Minas Geraes|battleship|4}}, were designed to carry the heaviest battleship armament in the world at the time.<ref>"[https://query.nytimes.com/gst/abstract.html?res=9E0CE4D9123EE233A2575AC0A96E9C946997D6CF Germany may buy English warships]", ''[[The New York Times]]'', 1 August 1908, C8.</ref> They came as a  shock to the navies of South America.<ref name=Whitley24/> Historian Robert Scheina commented that they "outclassed the entire [elderly] Argentinian fleet".<ref>Scheina, "Argentina", 400.</ref> Although debates raged in Argentina over whether it would be prudent to counter Brazil's purchase by acquiring their own expensive dreadnoughts, further border disputes—particularly near the [[Río de la Plata|River Plate]] with Brazil—decided the matter, and it ordered two {{sclass-|Rivadavia|battleship|1}}s (no relation to the earlier cruisers) from the [[Fore River Shipyard|Fore River Shipbuilding Company]] in the United States.<ref>Scheina, "Argentina", 401.</ref><ref>Livermore, "Battleship Diplomacy", 33.</ref> With its major rival acquiring dreadnoughts, Chile responded by [[Call for bids|asking for tenders]] from American and European countries that would give the country the most powerful battleships afloat.<ref>Scheina, ''Naval History'', 84.</ref>

== Construction, purchase and First World War service ==
{{See also|Almirante Latorre-class battleship#Specifications|l1=Specifications of the ''Almirante Latorre''-class battleships}}
On 6 July 1910, the [[National Congress of Chile]] passed a [[Bill (proposed law)|bill]] allocating 400,000 [[pounds sterling]] to the navy for two {{convert|28000|LT|t|0|lk=in|adj=on}} [[battleship]]s—which would eventually be named ''Almirante Latorre'' and [[HMS Eagle (1918)|''Almirante Cochrane'']]{{efn-ua|''Almirante Latorre'' was originally named ''Valparaíso'', after the [[Valparaíso|Chilean city]], but was renamed to ''Libertad'', Spanish for "freedom".<ref name=Appendix7/> After the death of the famed admiral [[Juan José Latorre]], she was renamed ''Almirante Latorre'' in July 1912.<ref name=Armada/> Sources are not specific as to when the ship was renamed for the first time.}}—six [[destroyer]]s, and two [[submarine]]s.<ref name=Armada>"[https://web.archive.org/web/20080608084301/http://www.armada.cl/site/unidades_navales/336.htm Acorazado Almirante Latorre]", Armada de Chile, archived 8 June 2008.</ref> The contract to build the battleships was awarded to [[Armstrong Whitworth]] on 25 July 1911.<ref>Livermore, "Battleship Diplomacy", 42.</ref> ''Almirante Latorre'' was officially ordered on 2 November 1911, and was [[keel laying|laid down]] less than a month later on 27 November,<ref name=Appendix7>Scheina, ''Naval History'', 322.</ref><ref name=Conways>Scheina, "Chile", 408.</ref> becoming the largest ship built by Armstrong at the time.<ref>Gill, "Professional Notes", 493.</ref> The ''[[New York Tribune]]'' reported on 2 November 1913 that [[Greece]] had reached an accord to purchase ''Almirante Latorre'' during a war scare with the [[Ottoman Empire]],<ref>"[http://chroniclingamerica.loc.gov/lccn/sn83030214/1913-11-02/ed-1/seq-12/ Turkey Threatened with Another War]", ''[[New York Tribune]]'', 2 November 1913, 12.</ref> but despite a developing sentiment within Chile to sell one or both of the dreadnoughts, no deal was made.<ref name=Livermore45>Livermore, "Battleship Diplomacy", 45.</ref><ref>Kaldis, "Background for Conflict", D1135.</ref>{{efn-ua|The nationalistic sentiments that exacerbated the naval arms race had given way to a slowing economy and growing public opinion that supported investing inside the country.<ref name=Livermore45/> The United States' [[United States Ambassador to Chile|Minister to Chile]], [[Henry Prather Fletcher]], commented to [[United States Secretary of State|Secretary of State]] [[William Jennings Bryan]]: "Since the naval rivalry began in 1910, financial conditions, which were none too good then, have grown worse; and as time approaches for the final payment, feeling has been growing in these countries that perhaps they are much more in need of money than of battleships."<ref>Quoted in Livermore, "Battleship Diplomacy", 45.</ref>}}

''Almirante Latorre'' was [[ship naming and launching|launched]] on 27 November 1913,<ref name=Burt240/><ref>Gill, "Professional Notes", 193.</ref>{{efn-ua|Scheina gives 17 November as the launching date.<ref name=Appendix7/>}} in an elaborate ceremony that was attended by various dignitaries and presided over by Chile's ambassador to the United Kingdom, [[Agustín Edwards Mac Clure]]. The battleship was [[ship christening|christened]] by the ambassador's wife, Olga Budge de Edwards.<ref name=Armada/> After the [[World War I|First World War]] broke out in Europe, ''Almirante Latorre'' was formally purchased by the United Kingdom on 9 September 1914;<ref name=Appendix7/><ref name=Burt240/><ref name=ConwaysGB>Preston, "Great Britain", 38.</ref><ref>"[https://query.nytimes.com/gst/abstract.html?res=9B00EFDC1238EE32A25754C0A9649D946996D6CF British Navy Gains]", ''The New York Times'', 7 December 1918, 14.</ref> she was not forcibly seized like the Ottoman [[Ottoman battleship Reşadiye|''Reşadiye'']] and [[Ottoman battleship Sultân Osmân-ı Evvel|''Sultân Osmân-ı Evvel'']], two other ships being built for a foreign navy, because the Allies' reliance on Chilean [[Sodium nitrate|munitions imports]] made retention of Chile's "friendly neutral" status with the United Kingdom a matter of vital importance.<ref name=ConwaysGB/>

''Almirante Latorre'' was renamed HMS ''Canada'' and slightly modified for British service. The [[Bridge (nautical)|bridge]] was taken off in favor of two open platforms, and a [[Mast (sailing)|mast]] was added in between the two funnels to support a derrick that would service [[Launch (boat)|launches]].<ref name=Burt240>Burt, ''British Battleships'', 240.</ref> The super-dreadnought completed [[fitting-out]] on 20 September 1915,<ref name=Appendix7/><ref name=ConwaysGB/> and was [[ship commissioning|commissioned]] into the [[Royal Navy]] on 15 October. She initially served with the [[4th Battle Squadron (United Kingdom)|4th Battle Squadron]] of the [[Grand Fleet]]. ''Canada'' saw action in the [[Battle of Jutland]] on 31 May–1 June 1916, firing 42 rounds from her 14-inch guns and 109 6-inch shells during the battle, and suffered no hits or casualties.<ref name=Burt240/> During the battle, she got off two salvoes at the disabled cruiser {{SMS|Wiesbaden||2}} at 18:40, and fired five more at an unknown ship around 19:20.<ref>Campbell, ''Jutland'', 157, 206–207.</ref> Her 6-inch guns were utilized for firing at German destroyers at 19:11.<ref>Campbell, ''Jutland'', 210.</ref>

''Canada'' was transferred to the [[1st Battle Squadron (United Kingdom)|1st Battle Squadron]] on 12 June 1916. In 1917–18, she was fitted with better rangefinders and [[range dial]]s, and two of the aft 6-inch secondary guns were removed after they suffered blast damage from the middle 14-inch turret. In the latter year, flying-off platforms for aircraft were added atop the [[superfire|superfiring]] turrets fore and aft. ''Canada'' was put into the reserve fleet in March 1919.<ref name=Burt240/>

== Chilean service ==
=== Early career ===
[[File:Chilean battleship Almirante Latorre launch.png|thumb|right|300px|''Almirante Latorre''{{'}}s launch, November 1913.]]
After the end of the war in Europe, Chile began to seek additional ships to bolster its fleet. The United Kingdom offered many of its surplus warships, including the two remaining {{sclass-|Invincible|battlecruiser|1}}s.{{efn-ua|Prior to the ''Invincible'' offer, Chile asked for {{HMS|Eagle|1918|6}}, which had been ''Almirante Cochrane'' but was in the process of being converted into an [[aircraft carrier]]. Chile, however, would not accept ''Eagle'' unless she was reconstructed into the original battleship configuration. This was found to be "impractical".<ref name=Conways/>}} The news that Chile could possibly acquire two ''Invincible''s kindled a major uproar in the country, with naval officers publicly denouncing such an action and instead promoting the virtues of submarines and aircraft on the basis of lower costs and their performance in the First World War.<ref>Somervell, "Naval Affairs", 389–390.</ref> The nations of South America worried that an attempt to regain the title of "the first naval power in South America"<ref>Graser Schornstheimer, "[https://query.nytimes.com/gst/abstract.html?res=9B04E6DF173AE532A25751C2A96E9C946195D6CF Chile as a Naval Power]", ''The New York Times'', 22 August 1920, X10.</ref> would start another naval arms race.<ref name="Livermore48">Livermore, "Battleship Diplomacy", 48.</ref>

In the end, Chile purchased only ''Canada'' and four destroyers in April 1920, all of which had been ordered by Chile prior to the war's outbreak and requisitioned by the British for the war.<ref name="Livermore48"/> The total cost of the five ships was less than a third of what Chile was due to pay for ''Almirante Latorre'' in 1914.<ref name=Armada/> ''Canada'' was renamed ''Almirante Latorre'' once again and formally handed over to the Chilean government on 27 November 1920.<ref name=Burt240/> She departed [[Plymouth]] the same day with two of the destroyers, {{ship|Chilean destroyer|Riveros||2}} and {{ship|Chilean destroyer|Almirante Uribe|1914|2}},<ref>"[https://query.nytimes.com/gst/abstract.html?res=9F05E6DA1639E133A2575BC2A9679D946195D6CF Chile's War Fleet Sails]", ''The New York Times'', 28 November 1920, 12.</ref> under the command of Admiral [[Luis Gomez Carreño]].<ref name=Armada/> They arrived in Chile on 20 February 1921, where they were welcomed by Chile's president, [[Arturo Alessandri]]. ''Almirante Latorre'' was made the [[flagship]] of the navy.<ref name=Armada/>

In her capacity as flagship of the Chilean Navy, ''Almirante Latorre'' was frequently utilized by the president for various functions. In the aftermath of the [[Moment magnitude scale|magnitude]] 8.5 [[1922 Vallenar earthquake]], ''Almirante Latorre'' was used to transport Alessandri to the affected area. The ship also brought "tents, medical supplies, rations, clothing and 2,000,000 [[Chilean peso|pesos]]" for those affected.<ref name=Armada/><ref>"[https://query.nytimes.com/gst/abstract.html?res=9E02E2DE1638EF3ABC4E52DFB7678389639EDE More Earthquakes Hit Northern Chile]", ''The New York Times'', 16 November 1922, 3.</ref> By 1923, Chile only had ''Almirante Latorre'', a cruiser, and five destroyers in commission, leading ''The New York Times'' to remark "experts would probably place Chile third in potential sea power [after Brazil and Argentina]". While ''Almirante Latorre'' was individually more powerful than the Brazilian or Argentine dreadnoughts, they had two each to Chile's one. Compounding this was a lack of modern cruisers to accompany the lone dreadnought.<ref>"[https://select.nytimes.com/gst/abstract.html?res=F70714FB385516738DDDA80A94DB405B838EF1D3 Armament Limitation at Santiago]", ''The New York Times'', 21 March 1923, 16.</ref> In 1924, ''Almirante Latorre'' hosted the president again when he visited [[Talcahuano]] for the grand opening of a new naval drydock there.<ref name=Armada/> After the fall of the [[Government Junta of Chile (1925)|January Junta]] in 1925, the dreadnought hosted the returning President Alessandri during a [[Naval Review]] in Valparaíso; while on board, he gave a speech to senior naval officials to assure them that his new government "was for all Chileans, and not partisan in its inspiration".<ref>Somervell, "Naval Affairs", 393.</ref> In September, the last month of his term, Alessandri received the United Kingdom's [[Edward VIII|Edward]], [[Prince of Wales]], on board the battleship. The visit briefly quelled domestic unrest, and it marked the beginning of negotiations for a British [[naval mission]], which arrived in the following year.<ref>Somervell, "Naval Affairs", 393–394.</ref>

''Almirante Latorre'' was sent to the United Kingdom for a modernization at the [[HMNB Devonport|Devonport Dockyard]] in 1929. Departing Chile on 15 May, she traveled past [[Balboa, Panama|Balboa]] before traversing the [[Panama Canal]] nine days later. After refueling at [[Port of Spain]] on 28 May, the dreadnought continued across the Atlantic, passing the [[Azores]] before arriving in [[Plymouth]] on 24 June.<ref name=Whitley33>Whitley, ''Battleships of World War Two'', 33.</ref> Major alterations included rebuilding the bridge, updating the main battery fire control to more modern standards and adding it for the secondary armament for the first time, and replacing her steam turbine engines. Also added were a new mast between the third and fourth turrets, [[anti-torpedo bulge]]s similar to the British {{sclass-|Queen Elizabeth|battleship|1}}s, and new [[anti-aircraft warfare|anti-aircraft]] guns.<ref name=Burt240/> Nearly two years after the modernization began, ''Almirante Latorre'' sailed back to Valparaíso on 5 March 1931 and put in on 12 April. Two {{convert|33|LT|t|adj=on}} [[tug boat]]s which had been acquired for use in the harbors of [[Punta Arenas]] and Valparaíso were carried on the battleship's deck during her voyage back to Chile.<ref name=Whitley33/>

=== 1931 mutiny ===
[[File:AG208-AA-158-NN-1-1338.jpg|thumb|left|''Almirante Latorre'' from the bow, date unknown.]]
{{Main|Chilean naval mutiny of 1931}}
Despite the goodwill brought on by the removal of the "strongman"<ref>Sater, "Kronstadt", 241.</ref> President [[Carlos Ibáñez del Campo]] in July 1931, Chile could not overcome the [[Great Depression]]'s severe economic effects, and wages for [[civil servant]]s making over 3,000 [[Chilean peso|pesos]] a year were cut by 12–30&nbsp;percent to reduce government expenditures. This triggered a severe reaction among the sailors of the navy, who had already suffered a 10&nbsp;percent salary cut and 50&nbsp;percent loss in overseas bonuses. Various members of the crew on board ''Almirante Latorre'', but no officers, met on 31 August and decided that a mutiny was the best course of action.<ref>Scheina, ''Naval History'', 107.</ref><ref>Scheina, ''Latin America's Wars'', 74.</ref><ref name="Sater, Kronstadt, 241–243">Sater, "Kronstadt", 241–243.</ref>

Shortly after midnight on 1 September, the junior crew members of ''Almirante Latorre'', an armored cruiser ({{ship|Chilean cruiser|O'Higgins|1897|2}}), seven destroyers, and a few submarines took over their ships while many of their shipmates were watching a boxing tournament in [[La Serena, Chile|La Serena]]. They imprisoned the officers, most without conflict, and secured the ships by about 02:00. They elected a committee, the ''Estado Mayor de Tripulacion'', to take control of the mutiny. Later that day, at 16:55, the mutineers radioed the minister of the navy, declaring that they were acting on their own accord, as opposed to acting in concert with a militant political party or communist insurgents. They asked for their full salaries to be restored and the punishment of those who had plunged Chile into a depression, while also stating that they would not use force to achieve these goals.<ref name="Sater, Kronstadt, 241–243"/><ref>Scheina, ''Naval History'', 107–109.</ref>

Just before midnight on 2 September, the mutineers messaged the Chilean government with a more "sophisticated"<ref>Quote from an "observer" given in Sater, "Kronstadt", 245.</ref> list of twelve demands.{{efn-ua|These demands included:<ref>Quoted in Scheina, ''Naval History'', 108.</ref>
* [A reinstatement of] full pay for enlisted men.
* The millionaires of Chile loan 300 million pesos to the government.
* That all Government-owned uncultivated land be divided among the workmen.
* That the government continue all public works.
* That employment be provided for the unemployed.
* That sailors be given a free clothing allowance.
* That rations be improved.
* That the ration include more sugar.
* That navy-yard watchmen be replaced by sailors.
* That contract pilots no longer be employed.
* That service trade [officer] schools be closed for two years.
* That retirement for enlisted men be optional at 15 years but compulsory at 20 years of service.}} Meanwhile, further south, junior members of the navy in the main naval base of [[Talcahuano]] joined the mutiny, taking several vessels in the process. Several of these sailed north to join the other rebels, while two cruisers, a few destroyers and submarines remained to guard the base. Other bases joined the now-full-fledged rebellion as well, including the Second Air Group based in [[Quintero]]. With so many rebels appearing, it was feared by many that the plethora of unemployed workers would join.{{efn-ua|Indeed, massive strikes—led by the Communist Party's candidate for president, [[Elías Lafertte]]—were held in [[Valparaíso]], and "the city appeared deserted" by 4 September.<ref>Sater, "Kronstadt", 248.</ref>}} The government attempted to solicit aid from the United States in the form of military intervention or war materiel (including two submarines and bombs capable of penetrating the armor of ''Almirante Latorre''), but they were rebuffed both publicly and privately. Acting Vice President [[Manuel Trucco]] now found himself in an undesirable position; he had to defeat the rebels before more units joined and bolstered their forces, but if he was too harsh, there was a risk that the populace would think that his policies were too similar to the former dictator Ibáñez del Campo. Trucco decided on a path of reconciliation. He sent a naval admiral, [[Edgardo von Schroeders]], to negotiate with the mutineers. They met on board ''Almirante Latorre'', where von Schroeders, seeing a potential split between sailors angry over their pay versus others with a more political agenda, tried to divide them along these lines and get them to surrender. However, a plea from the approaching southern fleet, asking for them to wait before any possible settlement, sealed the matter for the time being and von Schroders flew back to the capital.<ref>Sater, "Kronstadt", 244, 247–250.</ref>

5 September marked a turn in the rebels' fortunes, despite the arrival of the southern fleet a day earlier. All of their land gains were taken by government forces, leaving only the fleet in the mutineers' hands. By the next day, an air strike was mounted by government forces. The only damage done was to the submarine ''H4'', which was unable to dive, but at least one bomb landed about {{convert|50|yd|m}} from ''Almirante Latorre''. Despite the scant damage, the attack broke the mutineers' spirits; they quickly offered to send a delegation to Santiago to discuss terms, but the government, bolstered by its land victories, refused. While the mutiny devolved into arguing and anarchy, individual ships began leaving the bay and setting sail for Valparaíso, and the rest soon followed. ''Almirante Latorre'' ended up in the [[Bay of Tongoy]] with {{ship|Chilean cruiser|Blanco Encalada||2}}. Seven crewmen on the dreadnought received death sentences, later commuted to life in prison.<ref>Scheina, ''Naval History'', 112–114.</ref><ref>Scheina, ''Latin America's Wars'', 76.</ref><ref>Sater, "Kronstadt", 252–253, 255–256.</ref>

=== Later career ===
[[File:Review on Chilean battleship Latorre AG208-AA-158-NN-2-1339.jpg|thumb|right|Looking down from ''Almirante Latorre''{{'}}s bridge on the bow turrets, date unknown.]]
Still in the midst of the depression, ''Almirante Latorre'' was deactivated at Talcahuano in 1933 to lessen government expenditures,<ref>"[https://select.nytimes.com/gst/abstract.html?res=F40E11FA3D5F1A7A93CBA8178AD85F478385F9 Chile Lays Up All Battleships in Drastic Economy Measure]", ''The New York Times'', 19 January 1933, 7.</ref> and only a caretaker crew was assigned to tend to the [[:wikt:mothballed|mothballed]] ship into the mid-1930s.<ref>Scheina, ''Naval History'', 86, 359.</ref>{{efn-ua|It is not clear when ''Almirante Latorre'' was reactivated. Scheina gives two possible years, 1935 or after the 1937 refit.<ref name="Scheina, Naval History, 359">Scheina, ''Naval History'', 359.</ref>}} In a 1937 refit in the Talcahuano dockyard, the aircraft catapult was taken off and anti-aircraft weaponry was added.<ref name="Scheina, Naval History, 359"/> ''Almirante Latorre'' was never fully modernized, however, and by the Second World War her [[main battery]] was comparatively short-ranged and her armor protection, designed before the "[[all or nothing (armor)|all or nothing]]" principle was put into practice, was wholly inadequate.<ref>Worth, ''Fleets of World War II'', 7.</ref> Nevertheless, soon after Japan's [[attack on Pearl Harbor]], the United States approached the Chilean [[Military attaché|naval attaché]] and the vice admiral heading Chile's naval commission to the United States with the aim of purchasing ''Almirante Latorre'' and a few destroyers to bolster the United States' navy. The offer was declined,<ref name="Whitley33"/><ref>Scheina, ''Naval History'', 164.</ref> and ''Almirante Latorre'' was used for [[neutrality patrol]]s during the Second World War.<ref name=Burt240/>

''Almirante Latorre'' was active until 1951, when an accident in the ship's engine room killed three crewmen. Moored at Talcahuano, the battleship became a storage facility for fuel oil.<ref name=Whitley33/> She was [[ship decommissioning|decommissioned]] in October 1958, and was sold to [[Mitsubishi Heavy Industries]] in February 1959 for $881,110 to be [[ship breaking|broken up]] for scrap.<ref name=Appendix7/><ref name=Armada/> On 29 May 1959, to the salutes of the assembled Chilean fleet, the old dreadnought was taken under tow by the tug ''Cambrian Salvos'',<ref name=Armada/><ref name=Whitley33/> and reached [[Yokohama]], Japan, at the end of August,<ref name=Burt240/><ref name=Whitley33/><ref name="In Japan">"[https://select.nytimes.com/gst/abstract.html?res=F50A11FD3E5F1A7B93C2AA1783D85F4D8585F9 Chilean Warship in Japan]", ''The New York Times'', 30 August 1959, S13.</ref>{{efn-ua|Sources disagree as to the exact date. Whitley, ''The New York Times'', and Burt give 28, 29, and 30 August, respectively.<ref name=Burt240/><ref name=Whitley33/><ref name="In Japan"/>}} though the scrapping process did not begin immediately on arrival.<ref name=Burt240/> <!--A substantial amount of parts from ''Almirante Latorre'' were used in the restoration of the {{ship|Japanese battleship|Mikasa|2}}.-->

== Footnotes ==
{{notelist-ua}}

== Endnotes ==
{{reflist|colwidth=30em}}

== References ==
{{refbegin}}
* Burt, R. A. ''British Battleships of World War One''. [[Annapolis, Maryland|Annapolis, MD]]: [[Naval Institute Press]], 1986. ISBN 0-87021-863-8. {{oclc|14224148}}
* Campbell, John. ''Jutland: An Analysis of the Fighting''. New York: Lyons Press, 1998. ISBN 1-55821-759-2. {{oclc|41176817}}.
* Garrett, James L. "The Beagle Channel Dispute: Confrontation and Negotiation in the Southern Cone". ''[[Journal of Interamerican Studies and World Affairs]]'' 27: no. 3 (1985), 81–109. {{jstor|165601}}. {{issn|0022-1937}}. {{oclc|2239844}}.
* Gill, C.C. "Professional Notes". [[Proceedings (magazine)|''Proceedings'']] 40: no. 1 (1914), 186–272; 476–607. {{issn|0041-798X}}. {{oclc|2496995}}.
* Kaldis, William Peter. "Background for Conflict: Greece, Turkey, and the Aegean Islands, 1912–1914". ''[[The Journal of Modern History]]'' 51, no. 2 (1979), D1119–D1146. {{jstor|1881125}}. {{issn|0022-2801}}. {{oclc|62219150}}.
* [[Seward W. Livermore|Livermore, Seward W]]. "Battleship Diplomacy in South America: 1905–1925". ''The Journal of Modern History'' 16: no. 1 (1944), 31–44. {{jstor|1870986}}. {{issn|0022-2801}}. {{oclc|62219150}}.
* Preston, Anthony. "Great Britain" in ''Conway's All the World's Fighting Ships: 1906–1921'', edited by Robert Gardiner and Randal Gray, 1–104. Annapolis, MD: Naval Institute Press, 1984. ISBN 0-87021-907-3. {{oclc|12119866}}.
* Sater, William F. "The Abortive Kronstadt: The Chilean Naval Mutiny of 1931". ''[[The Hispanic American Historical Review]]'' 60: no. 2 (1980): 239–268. {{jstor|2513217}}. {{issn|0018-2168}}. {{oclc|421498310}}.
* Scheina, Robert L. "Argentina" and "Chile" in ''Conway's All the World's Fighting Ships: 1906–1921'', edited by Robert Gardiner and Randal Gray, 400–402 and 407–408. Annapolis, MD: Naval Institute Press, 1984. ISBN 0-87021-907-3. {{oclc|12119866}}.
* Scheina, Robert L. ''Latin America: A Naval History 1810–1987''. Annapolis, MD: Naval Institute Press, 1987. ISBN 0-87021-295-8. {{oclc|15696006}}.
* Scheina, Robert L. ''Latin America's Wars''. Washington, D.C.: Brassey's, 2003. ISBN 1-57488-452-2. {{oclc|49942250}}.
* Somervell, Philip. "Naval Affairs in Chilean Politics, 1910–1932". ''[[Journal of Latin American Studies]]'' 16: no. 2 (1984), 381–402. {{jstor|157427}}. {{issn|1469-767X}}. {{oclc|47076058}}.
* Whitley, M.J. ''Battleships of World War Two: An International Encyclopedia''. Annapolis, MD: Naval Institute Press, 1998. ISBN 1-55750-184-X. {{oclc|40834665}}.
* Worth, Richard. ''Fleets of World War II''. [[Cambridge, Massachusetts|Cambridge, MA]]: [[Da Capo Press]], 2001. ISBN 0-306-81116-2. {{oclc|48806166}}.
{{refend}}

== External links ==
{{Commons category|Almirante Latorre (ship, 1913)}}
{{Portal|Battleships}}
* [http://dreadnoughtproject.org/tfs/index.php/H.M.S._Canada_(1913) Dreadnought Project] Technical details of the ship's equipment and fire control.

{{Almirante Latorre class battleships}}
{{WWIBritishShips}}

{{featured article}}

{{DEFAULTSORT:Almirante Latorre}}
[[Category:1913 ships]]
[[Category:Almirante Latorre-class battleships]]
[[Category:Armstrong Whitworth ships]]
[[Category:Cold War battleships of Chile]]
[[Category:Tyne-built ships]]
[[Category:World War I battleships of the United Kingdom]]
[[Category:World War II battleships of Chile]]