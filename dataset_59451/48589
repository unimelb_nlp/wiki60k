{{Infobox person
| name        = Philip N. Howard
| image       = 
| alt         = 
| caption     = 
| birth_date  = 1970|12|09
| birth_place = [[Montreal]], Canada
| death_date  = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place = 
| occupation = Author, <br/>Professor, [[University of Washington]] and [[University of Oxford]]
| alma_mater  = [[Innis College, Toronto]] (B.A., Political Science)<br>[[London School of Economics]] (M.Sc., Economics)<br>[[Northwestern University]] (Ph.D., Sociology)
| fields = [[Social Science]]
| networth =
| awards      = Best Book awards from [[American Sociological Association]], [[American Political Science Association]], and [[International Communication Association]]<br>Fellow at the [[Center for Advanced Study in the Behavioral Sciences]]
| nationality = Canadian
| spouse = [[Gina Neff]]
| children =
| website = [http://www.philhoward.org/ www.philhoward.org/]
| known_for   = [[politics and technology]], [[political communication]], network ethnography, astroturf, bots, computational propaganda

}}

'''Philip N. Howard''' is a [[sociologist]] and communication researcher who studies the impact of information technologies on [[democracy]] and [[social inequality]].<ref>{{cite web|url=http://faculty.washington.edu/pnhoward/ |title=Dr. Philip N. Howard, University of Washington |publisher=University of Washington |date= |accessdate=December 7, 2011}}</ref> He is a faculty member of the Department of [[Communication]], the [[Jackson School of International Studies|Jackson School for International Studies]], and the [[University of Washington Information School|Information School]] at the [[University of Washington]]. He took up the professorship in Internet Studies at the [[University of Oxford]]'s [[Oxford Internet Institute]] on 1 July 2016.<ref>{{cite web|url=http://www.ox.ac.uk/gazette/2015-2016/17march2016-no5127/notices/#appt2 |title=Notices, Oxford University Gazette |publisher=University of Oxford |date=March 17, 2016 |accessdate=March 17, 2016}}</ref>

==Biography==

Philip N. Howard was born in [[Montreal]] in 1970. He is married to [[Gina Neff]], also a professor at the [[University of Washington]], and has two young sons.

Howard has been a Fellow at the [[Pew Internet & American Life Project]] in [[Washington D.C.]], the [[London School of Economics]]' Stanhope Centre for Communications Policy Research, [[Stanford University]]'s [[Center for Advanced Study in the Behavioral Sciences]], and [[Princeton University]]'s [[Center for Information Technology Policy]]. In 2013 he moved to Budapest, Hungary where he helped to found a new [[School of Public Policy at Central European University]].

==Research==

His research has demonstrated that the diffusion of [[digital media]] has long-term, often positive, implications for democratic institutions. Through [[information infrastructure]], some young democracies have become more entrenched and durable; some [[authoritarian]] regimes have made significant transitions towards democratic institutions and practices; and others have become less authoritarian and hybrid where information technologies support the work of particular actors such as [[Sovereign state|state]], [[political parties]], [[journalists]], or [[civil society]] groups.

===Astroturf Campaigns and Fake News===

Howard was one of the first to investigate the impact of digital media on political campaigning in advanced democracies, and he was the first political scientist to define and study "astroturf" political movements as the managed perception of grassroots support through [[astroturfing]] in his research on the Gore and Bush presidential campaigns.  [https://books.google.com/books/about/New_media_campaigns_and_the_managed_citi.html?id=5umWb7GM_jkC New Media Campaigns and the Managed Citizen] (2005) is about how politicians and lobbyists in the United States use the internet to manipulate the public and violate privacy.<ref>{{cite web|url=http://www.com.washington.edu/faculty/howard.html |title=Howard - Department of Communication, University of Washington |publisher=Com.washington.edu |date= |accessdate=December 7, 2011}}</ref>  His research on technology and social change has been prescient.

===Digital Media and the Arab Spring===

Howard wrote presciently about the role of the internet in transforming Political Islam, and is the author of [https://books.google.com/books?id=lpBb2FiFIjUC&printsec=frontcover&dq=The+Digital+Origins+of+Dictatorship+and+Democracy&hl=en&ei=54PBTt_0A8OLsgK06aGRDw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CC4Q6AEwAA#v=onepage&q&f=false ''The Digital Origins of Dictatorship and Democracy''] (2010) which argues that how states respond to new information technologies has become a defining feature of both democracy and authoritarianism.  Howard demonstrated that the internet was having an important impact on political Islam.  The book was published before the [[Arab Spring]], and shows how [[new social movements]] in [[North Africa]] and the [[Middle East]] were using [[social media]] to outmaneuver some of the region's dictators, partly because these regimes lacked effective responses to online evidence of their abuses.<ref>{{cite web|last=Rothman |first=Wilson |url=http://technolog.msnbc.msn.com/_news/2011/02/11/6035566-how-the-internet-brought-down-a-dictator |title=Technolog - How the Internet brought down a dictator |publisher=Technolog.msnbc.msn.com |date= |accessdate=December 7, 2011}}</ref>  Using Charles Ragin's method of "[[qualitative comparative analysis]]" Howard investigated technology diffusion and political Islam and explained trends in many countries, with the exception of Tunisia and Egypt.  But very shortly the trends in social activism and political Islam he had identified appeared in those two countries as well in the "Arab Spring."

[https://books.google.com/books/about/Democracy_s_Fourth_Wave.html?id=ayHOyrmmT8kC&redir_esc=y Democracy's Fourth Wave?] (2013), with Muzammil M. Hussain, suggests that turning off the Internet, as the Mubarak regime did on [[Timeline of the 2011 Egyptian revolution under Hosni Mubarak's rule|January 28, 2011]], actually strengthened the revolution by forcing people into the streets to seek information.<ref>{{cite web|url=http://www.cbsnews.com/stories/2011/01/29/eveningnews/main7297700.shtml |title=Egypt Cuts Off Communication amid Crisis |publisher=CBS News |date=January 29, 2011 |accessdate=December 7, 2011}}</ref> It sees events like the [[Arab Spring]] as "early signs of the next big wave of democratization. But this time, it will be wrestled into life in the digital living room of the global community." <ref>{{cite web|url=http://www.csmonitor.com/USA/2011/0510/In-Libya-perfecting-the-art-of-revolution-by-Twitter/%28page%29/2 |title=In Libya, perfecting the art of revolution by Twitter |publisher=CSMonitor.com |date=May 10, 2011 |accessdate=December 7, 2011}}</ref>  His research and commentary is regularly featured in the [[Mass media|media]], including recent contributions about media politics in the US, Hungary and around the world [[the New York Times]] and [[Washington Post]].<ref>{{cite web|url=https://www.nytimes.com/2014/09/09/opinion/hungarys-crackdown-on-the-press.html?_r=0 |title=Hungary's Crackdown on the Press |publisher=New York Times |date=September 8, 2014 |accessdate=December 20, 2014}}</ref><ref>{{cite web|url=http://www.washingtonpost.com/lifestyle/style/hashtagactivismmatters-some-experts-see-online-to-irl-change-in-police-protests/2014/12/14/4d943708-83d4-11e4-9534-f79a23c40e6c_story.html?tid=hpModule_1f58c93a-8a7a-11e2-98d9-3012c1cd8d1e |title=#HashtagActivismMatters: Some experts see online-to-IRL change in police protests |publisher=Washington Post |date=December 14, 2014 |accessdate=December 20, 2014}}</ref><ref>{{cite web|url=https://www.nytimes.com/2014/12/21/style/millennials-and-the-age-of-tumblr-activism.html |title=Millennials and the Age of Tumblr Activism |publisher=New York Times |date=December 19, 2014 |accessdate=December 20, 2014}}</ref>

===Politics and the Internet of Things===

In [https://books.google.com/books?id=qvC5BwAAQBAJ&pg=PA145&dq=pax+technica&hl=en&sa=X&ei=klFwVd78CIirsQGok4GwDg&ved=0CB8Q6AEwAA#v=onepage&q=pax%20technica&f=false Pax Technica] (2015) he argues that the [[Internet of Things]] will be the most important tool of political communication we have ever built.  He advocates for more public input in its design and more civic engagement with how this information infrastructure gets used.<ref>{{cite web|url=http://www.ft.com/intl/cms/s/0/035db35e-f3db-11e4-99de-00144feab7de.html#axzz3btAequH3 |title=Review: Pax Technica, by Philip Howard |publisher=Financial Times |date=May 20, 2015 |accessdate=June 5, 2015}}</ref>

===Computational Propaganda===

In 2014 he hypothesized that political elites in democracies would soon be using algorithms over social media to manipulate public opinion, a process he called "computational propaganda."  His research on political redlining, astroturf campaigns and fake news inspired a decade of work and became particularly relevant during the Brexit referendum and the 2016 U.S. Presidential Campaign.<ref>{{cite web|url=https://www.washingtonpost.com/news/the-intersect/wp/2016/10/19/one-in-four-debate-tweets-comes-from-a-bot-heres-how-to-spot-them/ |title=One in four debate tweets comes from a bot. Here's how to spot them. |publisher=Washington Post |date=October 19, 2016 |accessdate=January 29, 2016}}</ref><ref>{{cite web|url=https://www.nytimes.com/2016/11/18/technology/automated-pro-trump-bots-overwhelmed-pro-clinton-messages-researchers-say.html |title=Automated Pro-Trump Bots Overwhelmed Pro-Clinton Messages, Researchers Say |publisher=New York Times |date=November 17, 2016 |accessdate=January 29, 2016}}</ref>  His research has exposed the global impact of bots and trolls on public opinion.

==Education==
Ph.D. Sociology, [[Northwestern University]], 2002<br/>M.Sc. Economics, [[London School of Economics]], 1994<br/>B.A. Political Science, [[Innis College]], [[University of Toronto]], 1993

==Academic Positions==
*2016-present Endowed Chair and Professorial Fellow, [[Oxford Internet Institute]], [[Balliol College]], [[University of Oxford]]
*2013-16 Founding Professor [[School of Public Policy at Central European University]] at [[Central European University]]
*2008-2009 Fellow at the [[Center for Advanced Study in the Behavioral Sciences]] at [[Stanford University]]
*2010-2012 Fellow at [[Woodrow Wilson School of Public and International Affairs]] at [[Princeton University]]
*2002-2012 Assistant to Full Professor of Communication, Information, and International Affairs at [[University of Washington]] 

==Awards and Honors==

* 2011-2013, Fellow, [http://citp.princeton.edu/ Center for Technology Policy], [[Princeton University]]
* Best Book Award, Information Technology and Politics Section, [[American Political Science Association]], 2011
* 2008-9, Fellow, [http://www.casbs.org/ Center for Advanced Study in the Behavioral Sciences], [[Stanford University]]
* Outstanding Book Award 2008, [[International Communication Association]]
* Best Book Award 2006, Communication Technology & Society Section, [[American Sociological Association]]

==Books==

* Howard, Philip N. ''Pax Technica:  How the Internet of Things May Set Us Free or Lock Us Up''. Yale University Press, 2015.  Also published in German and Chinese.
* Howard, Philip N. (Editor). ''State Power 2.0:  Authoritarian Entrenchment and Civic Engagement Worldwide.'' Ashgate Press, 2013.
* Howard, Philip N., (Coauthor). ''Democracy's Fourth Wave? Digital Media and the Arab Spring.'' Oxford University Press, 2013.
* Howard, Philip N.  ''Castells on the Media.''  Polity Press, 2011. 
* Howard, Philip N.  ''The Digital Origins of Dictatorship and Democracy:  Information Technology and Political Islam.'' Oxford University Press, 2011. 
* Howard, Philip N. (Editor). ''Handbook of Internet Politics.'' Routledge, 2009. 
* Howard, Philip N. ''New Media Campaigns and the Managed Citizen.'' Cambridge University Press, 2006.
* Howard, Philip N. (Editor). ''Society Online:  The Internet in Context.'' Sage, 2004.  Also published in Spanish.

==Essays and Journalism==
* "Facebook and Twitter's Real Sin Goes Beyond Spreading Fake News," Reuters http://www.reuters.com/article/us-twitter-facebook-commentary-idUSKBN13W1WO
* "Hungary's Crackdown on the Press," New York Times http://www.nytimes.com/2014/09/09/opinion/hungarys-crackdown-on-the-press.html?_r=0
* "Let's Make Candidates Pledge Not to Use Bots," Reuters, http://blogs.reuters.com/great-debate/2014/01/02/lets-make-candidates-pledge-not-to-use-bots/
* "Politics won’t know what hit it: The Internet of Things is poised to change democracy itself," Politico, http://www.politico.com/agenda/story/2015/06/philip-howard-on-iot-transformation-000099
* "Bots Unite to Automate the Presidential Election", Wired Magazine, https://www.wired.com/2016/05/twitterbots-2/

==References==
{{reflist}}

==External links==
* [https://www.oii.ox.ac.uk/people/philip-howard/ Oxford University faculty page for Philip N. Howard ]
* [http://www.philhoward.org/ Author home page for Philip N. Howard ]
* [http://www.politicalbots.org/ Home page for Project on Computational Propaganda ]

{{DEFAULTSORT:Howard, Philip N.}}
[[Category:Canadian academics]]
[[Category:Canadian sociologists]]
[[Category:Living people]]
[[Category:University of Washington faculty]]
[[Category:1970 births]]
[[Category:Fellows of Balliol College, Oxford]]
[[Category:Alumni of the London School of Economics]]
[[Category:Academics of the University of Oxford]]