{{Infobox journal
| cover = [[File:Nuclear Medicine and Biology.gif]]
| title = Nuclear Medicine and Biology
| abbreviation = Nucl. Med. Biol.
| editor = William C. Eckelman
| discipline = [[Nuclear medicine]]
| publisher = [[Elsevier]]
| frequency = 8/year
| history = 1974-present
| openaccess =
| impact =  2.408
| impact-year = 2013
| website = http://www.journals.elsevier.com/nuclear-medicine-and-biology/
| link1 = http://www.nucmedbio.com/current
| link1-name = Online access
| link2 = http://www.nucmedbio.com/issues
| link2-name = Online archive
| ISSN = 0969-8051
| eISSN =
| OCLC = 644550135
| LCCN = 93648729
}}
'''''Nuclear Medicine and Biology''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by [[Elsevier]] that covers research on all aspects of [[nuclear medicine]], including [[radiopharmacology]], [[radiopharmacy]], and clinical studies of targeted [[radiotracer]]s. It is the official journal of the Society of Radiopharmaceutical Sciences . According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 3.023.<ref name=WoS>{{cite book |year=2013 |chapter=Nuclear Medicine and Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in:
* [[BIOSIS]]
* [[Elsevier BIOBASE]]
* [[Cambridge Scientific Abstracts]]
* [[Chemical Abstracts Service]]
* [[Current Contents]]/Life Sciences
* [[MEDLINE]]/[[PubMed]]
* [[EMBASE]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/nuclear-medicine-and-biology/}}

[[Category:Radiology and medical imaging journals]]
[[Category:Nuclear medicine]]
[[Category:Nuclear magnetic resonance]]
[[Category:Radiopharmaceuticals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1978]]