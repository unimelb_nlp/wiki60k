{{Use mdy dates|date=March 2017}}
{{Infobox Governor
|honorific-prefix    =
|name                =Donald Grant Nutter
|honorific-suffix    =
|image               =
|order               =[[List of Governors of Montana|15th]]
|office              =Governor of Montana
|term_start          =January 4, 1961
|term_end            =January 25, 1962
|lieutenant          =Tim M. Babcock
|predecessor         =[[J. Hugo Aronson]]
|successor           =[[Tim M. Babcock]]
|state_senate2       =Montana
|district2           =19th
|term_start2         =1951
|term_end2           =1959
|preceded2           =
|succeeded2          =
|birth_date          =November 28, 1915
|birth_place         =[[Lambert, Montana]]
|death_date          =January 25, 1962 (aged 46)
|death_place         =[[Wolf Creek, Montana]]
|nationality         =
|spouse              =Maxine Trotter
|party               =Republican
|relations           =
|children            =John Nutter
|residence           =
|alma_mater          =[[University of Montana-Missoula|University of Montana]]
|occupation          =
|profession          =
|religion            =[[Congregationalist]]
|signature           =
|website             =
|footnotes           =
|allegiance= [[United States of America]]
|branch= [[United States Army Air Forces]]
|serviceyears=1943 &ndash; 1945
|rank= [[Captain (United States)|Captain]]
|battles=[[World War II]]
}}
'''Donald Grant Nutter''' (November 28, 1915{{spaced ndash}}January 25, 1962) was an [[United States|American]] politician. A recipient of the [[Distinguished Flying Cross (U.S.)|Distinguished Flying Cross]] in [[World War II]],<ref>{{cite web|url= http://www.nga.org/cms/home/governors/past-governors-bios/page_montana/col2-content/main-content-list/title_nutter_donald.html|title=Montana Governor Donald Grant Nutter|publisher= National Governors Association|accessdate= October 14, 2012}}</ref> Nutter served in the [[Montana Senate|state senate]] and as the chair of the state [[Republican Party (United States)|Republican Party]] prior to being elected the [[List of Governors of Montana|15th Governor of Montana]] in 1960. After a year in office, he was killed in an airplane crash during a [[blizzard]] in January 1962.<ref>{{ cite web | url=http://www.netstate.com/states/government/mt_formergov.htm | title=Former Governors of Montana | publisher=www.netstate.com | accessdate=October 9, 2013}}</ref>

==Biography==

===Early life===
Donald Nutter was born November 28, 1915, in [[Lambert, Montana]],<ref name="nga">{{cite web |url=http://www.nga.org/portal/site/nga/menuitem.29fab9fb4add37305ddcbeeb501010a0/?vgnextoid=83cf348fbd997010VgnVCM1000001a01010aRCRD&vgnextchannel=e449a0ca9e3f1010VgnVCM1000001a01010aRCRD |title=Montana Governor Donald Grant Nutter |accessdate=June 2, 2007 |format= |work= |publisher=National Governors' Association }}</ref> the second of three sons born to Chesley E. Nutter and Anne Grant (Wood) Nutter. The family moved to [[Sidney, Montana|Sidney]] in 1918.

Nutter attended the [[North Dakota State College of Science|North Dakota State School of Science]] in [[Wahpeton, North Dakota|Wahpeton]] for two years before transferring to [[University of Montana-Missoula|University of Montana]] in [[Missoula, Montana|Missoula]] in 1935. He left school after his father became ill to return to Sidney, where he entered public service in 1937 as the deputy clerk of the [[Richland County, Montana|Richland County]] [[Montana District Courts|District Court]]. He held that position for a year, then served for another year as the undersheriff of Richland County.<ref name="nga"/>

===Marriage and children===
Nutter met his wife, Maxine Trotter, at an [[ice cream]] shop where she worked with her parents. They were married in [[Lewistown, Montana|Lewistown]] on April 16, 1938 and had one son, John.

===Career===
Following their wedding, Nutter entered the farm equipment business in Sidney. He did well and moved his young family to [[Glasgow, Montana|Glasgow]], where he managed a farm equipment sales company for two years.

With the outbreak of [[World War II]], Nutter joined the [[United States Army Air Forces|Army Air Forces]]. As a [[B-24 Liberator|B-24]] bomber pilot, he flew 62 combat missions, logging more than 500 hours of combat time. He spent 13 months in the China-Burma-India Theater and was discharged at the rank of [[Captain (U.S. Army)|captain]] after 39 months of service.

He returned to eastern Montana and opened his own farm implement dealership in Sidney and in 1948 began working toward a law degree. Nutter was elected to the Montana State Senate in 1950, serving his eastern Montana constituents while attending law school in western Montana. In 1954, he was admitted to the Montana Bar and re-elected in the state senate. During his time in the statehouse, his position changed from "a cautious reactionary to a conscientious, business-minded liberal with a host of friends and supporters throughout the state".<ref>{{cite news |title=Who's Who In The State House |url=http://www.time.com/time/magazine/article/0,9171,826757-1,00.html |work=TIME |publisher= |pages= |page= |date=November 16, 1960 |accessdate=June 2, 2007}}</ref>

After defeat as the incumbent seeking a third term, he served as the chairman of the Montana Republican Central Committee from 1958 to 1960.<ref name="Donald Grant Nutter">{{cite web|url= http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=7123819|title=Donald Grant Nutter |publisher= Find A Grave|accessdate= October 14, 2012}}</ref> He secured the Republican gubernatorial nomination and was elected [[Governor of Montana|governor]] in November 1960. During his tenure, state spending was reduced and new industrial developments were promoted.<ref name="nga"/>

===Death and legacy===
On the night of January 25, 1962, Nutter was en route to a speaking engagement in [[Cut Bank, Montana|Cut Bank]] when the [[C-47 Skytrain]] he was a passenger in crashed. Winds exceeding 100&nbsp;mph sheared off one of the wings of the plane, causing the aircraft to go down in Wolf Creek Canyon north of [[Helena, Montana|Helena]]. Also killed in the crash were Dennis Gordon, his executive secretary; Edward Wren, commissioner of agriculture; and three members of the Montana Air National Guard: Maj. Clifford Hanson, Maj. Joseph Devine and Master Sgt. Charles Ballard.

Nutter's casket lay in state in Montana's Capitol House Chambers, flanked by the caskets of Wren and Gordon and under watch by two Montana National Guardsmen,<ref>{{cite web |url=http://governor.mt.gov/news/pr.asp?ID=378 |title=Governor Tom Judge to Lie In State in Supreme Court Chambers this Wednesday |accessdate=June 2, 2007 |last=Elliott |first=Sarah |date=September 11, 2006 |format=press release |publisher=Montana Office of the Governor }}</ref> prior to being interred in the Sidney City Cemetery in Sidney, Montana.<ref name="Donald Grant Nutter"/>

Elsewhere in Sidney, Nutter is remembered with a statue in Central Park and by the Donald G. Nutter Building, which is home to the Richland County Extension Office.

At the Montana State Capitol in Helena, a bronze plaque outside the south entrance to the statehouse remembers Nutter and the others lost in the 1962 air crash.<ref>{{cite news |first=Charles S. |last=Johnson |title=Nutter, others honored with plaque |url=http://www.billingsgazette.net/articles/2006/09/15/news/state/40-nutter.txt |format= |work=Billings Gazette |date=September 15, 2006 |accessdate=June 2, 2007}}</ref>

==Works==

===Letters===
Nutter's correspondence, press releases, and speeches are included in the Montana Governors records collection at the Montana Historical Society Research Center Archives in Helena, Montana.<ref>{{cite web |url=http://nwda-db.wsulibs.wsu.edu/documents/retrieve.asp?docname=MtHiMC35.xml#c01_43 |title=Guide to the Montana Governors records 1889–1962 |accessdate=June 2, 2007 |year=2006 |publisher=[[Northwest Digital Archives]]}}</ref>  Dennis Gordon, his executive secretary had a bright career ahead.  As a young 28-year military officer in the U.S. Army, he had been appointed the interim Military Governor of Korea. He had returned to Montana after the War to enter a career in Gordon Construction of Missoula Montana.  After a brief time, he resigned a partnership in this company to pursue a legal degree and then one of politics.

==Awards==
*[[Air Medal]] with cluster
*[[Distinguished Flying Cross (U.S.)|Distinguished Flying Cross]] with clusters

==References==
{{Reflist}}

==External links==
{{Commons category|Donald Grant Nutter}} 
*[https://web.archive.org/web/20070610023803/http://www.richland.org/html/mayor__1.HTM Governor Donald G. Nutter] – timeline and photos on Richland County website
*[https://web.archive.org/web/20070929140851/http://www.bobstayton.com/media/commisions.php?subaction=showfull&id=1120499072&archive=&start_from=&ucat=3&show_cat=3 Governor Don Nutter] – statue images on sculptor Bob Stayton's website
*[http://mt.gov/gov2/formergov/ Montana Historical Society]
*[http://politicalgraveyard.com/geo/MT/RI-buried.html The Political Graveyard]
*{{Find a Grave|7123819}}

<br/><!--this break is to put visual space between the last information and the following template if needed-->

{{s-start}}
{{s-off}}
{{succession box
|before=[[J. Hugo Aronson]]
|title=[[List of Governors of Montana|Governor of Montana]]
|after=[[Tim M. Babcock]]
|years=1961{{spaced ndash}}1962
}}
{{s-end}}
{{Governors of Montana}}

{{Authority control}}

{{DEFAULTSORT:Nutter, Donald G.}}
[[Category:1915 births]]
[[Category:1962 deaths]]
[[Category:People from Richland County, Montana]]
[[Category:Victims of aviation accidents or incidents in the United States]]
[[Category:Accidental deaths in Montana]]
[[Category:American Congregationalists]]
[[Category:University of Montana alumni]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Governors of Montana]]
[[Category:Montana State Senators]]
[[Category:Montana Republicans]]
[[Category:United States Army Air Forces officers]]
[[Category:Recipients of the Air Medal]]
[[Category:North Dakota State College of Science alumni]]
[[Category:Republican Party state governors of the United States]]
[[Category:20th-century American politicians]]