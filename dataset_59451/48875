{{Infobox musical artist
| name            = Jeremey Frederick Hunsicker 
| image = Jeremey_Hunsicker.jpg
| caption = Hunsicker performing in 2009
| image_size      = 250
| background      = solo_singer
| birth_name      = Jeremey Frederick Hunsicker
| birth_date            = {{Birth date and age|1971|04|13}}
|birth_place =[[Roanoke, Virginia|Roanoke]], [[Virginia]], [[United States|US]]
| Death           = 
| instrument      = [[Singing|Vocals]], [[guitar]], [[piano]]
| genre           = [[Hard rock]], [[pop rock]], [[Pop music|pop]]
| occupation      = [[Singer-songwriter]], [[Record producer|producer]]
| years_active    = 2002-present
| label           = Our Qunitessential Music Group
| associated_acts = [[Journey (band)|Journey]], Frontiers Journey Tribute Band, Jeremey Frederick, The Waiting - Tom Petty Tribute 
| website = [http://www.jeremeyfrederick.com Official Music Website] [http://www.jeremeyhunsicker.com Official Copywriting Website]   [http://www.bigbangbands.com Official Booking Agency]}}
'''Jeremey Frederick Hunsicker''' (born April 13, 1971) is an [[Americans|American]] singer and songwriter best known as the [[singing|lead vocalist]] of the [[Journey (band)|Journey]] tribute band Frontiers. Hunsicker made national headlines in 2007 when he was briefly tapped by [[Journey (band)|Journey]] as a potential [[singing|lead vocalist]] and frontman for the iconic rock band.<ref name="roanoke">[http://www.roanoke.com/entertainment/wb/146327] at The Roanoke Times</ref>
 
With his tribute band, Frontiers, Hunsicker quickly became known for his uncanny ability to duplicate the signature sound of former [[Journey (band)|Journey]] [[singing|lead vocalist]] [[Steve Perry (musician)|Steve Perry]].

==Early Years & Personal Life==

Jeremey Hunsicker grew up in [[Fort Myers, Florida|Fort Myers]], [[Florida]] near McGregor and spent his college years in [[North Carolina]] where he attended [[Brevard College]]. It was at Brevard that he met his wife Sabrina.<ref name="roanoke" /> After college Hunsicker and his wife moved to [[Roanoke, Virginia|Roanoke]], [[Virginia]] where they currently reside. Hunsicker and his wife Sabrina have two children.
Some people may not realize that Jeremey is also a talented artist and writer. As a student at Fort Myers Middle School, he used to draw cartoons about far away fantastic worlds. Often he would create images of fighting robots and battle droids. Sometimes he would hold "Fantasy Duels" with other creative classmates and they would pit their battle droids against each other to see whose design would win.{{citation needed|date=December 2015}}

==Frontiers==

After performing in a number of bands, Hunsicker formed the [[Journey (band)|Journey]] tribute band Frontiers in 2002. Hunsicker named his tribute band after Journey's 1983 album  [[Frontiers (Journey album)|Frontiers]].<ref>[http://www.journeymusic.com/pages/disc/108] at Journey Music</ref> What began as a part-time regional venture on weekends evolved into a full-time nationally touring tribute act.

Frontiers had many personnel changes over the years with the only constants being Hunsicker and Greg Eanes on the bass. It was Hunsicker's vocals that kept fans and agents alike coming to Frontiers time and time again. At its peak, Frontiers was one of the nation's most popular Journey tribute bands, performing over 150 shows per year.

==Journey audition==

After [[Jonathan Cain]] and [[Neal Schon]] found videos of Hunsicker on [[YouTube]], they made a last-minute decision to fly across the country to [[Charlotte, North Carolina|Charlotte]], [[North Carolina]], to watch Hunsicker perform. After the show, Schon and Cain approached Hunsicker and invited him to fly out to California and audition for the position as [[singing|lead vocalist]] for [[Journey (band)|Journey]].<ref>[http://voices.yahoo.com/jeremey-hunsicker-different-guy-same-ol-sound-1357201.html] at Yahoo! Voices</ref>{{unreliable source?|date=October 2015}} Hunsicker rehearsed with the band and they wrote songs together for the band's upcoming album Revelation. Ultimately, things did not work out between [[Journey (band)|Journey]] and Hunsicker, and the band decided instead to hire [[Arnel Pineda]] from the [[Philippines]] as their new [[singing|lead vocalist]].

==Post-Journey==

Hunsicker's brief time with [[Journey (band)|Journey]] proved to be positive for his music career. Shortly after his brush with [[Journey (band)|Journey]], Hunsicker's band Frontiers, became so in-demand that he was able to quit his full-time job as a salesman for [[Saia|Saia Motor Freight]] and take his band to the national stage.<ref>[http://www.houmatoday.com/article/20090227/ARTICLES/902269973] at Houma Today</ref> In addition when [[Journey (band)|Journey's]] album [[Revelation]] was released, Hunsicker received credit for helping to write the albums leading track, "Never Walk Away." <ref>[http://current.timesfreepress.com/news/2008/dec/18/frontiers-does-journey-any-way-you-want-it/] at Current Times Free Press</ref> When the album was certified Platinum by the RIAA on December 3, 2008, Hunsicker was awarded as a songwriter on the album.<ref>[http://www.riaa.com/goldandplatinumdata.php?content_selector=gold-platinum-searchable-database] at RIAA</ref>

==Solo album==

In early 2011 Hunsicker announced that he was planning to release a solo album that was to be called "Every Little Thing" under the name Jeremey Frederick. Hunsicker chose to use the name Jeremey Frederick in an effort to separate himself from his status as a platinum selling songwriter with the band [[Journey (band)|Journey]].

On Father's Day, June 19, 2011, Hunsicker released a streaming version of the first single from the album, a song titled "This is Your Life." Hunsicker wrote the song for his children.<ref>[http://www.fabricationshq.com/jeremey-frederick---every-little-thing.html] at Fabrications HQ</ref> On June 21, 2011 the song was released for sale on iTunes.

In late August 2011 Hunsicker released a streaming version of the  second single from the album, "Stay." The song was released on iTunes on September 13, 2011. This date also marks the beginning of the [[Kickstarter]] campaign that Hunsicker used to help fund the album. During the summer of 2011 Hunsicker suffered financial setbacks that depleted much of the funds that he had set aside to produce the album. Without the funds needed to produce the album Hunsicker began seeking a source of funding. He discovered the website [[Kickstarter]] and launched a campaign to raise the $4,000 needed to complete the album on September 13, 2011.<ref>[http://articles.wdbj7.com/2011-10-12/tribute-band_30273250] at WDBJ7</ref> The [[Kickstarter]] campaign for Every Little Thing ended on October 15, 2011 with 66 backers fully funding the album.<ref>[http://www.kickstarter.com/profiles/jeremey/projects/created] at Kickstarter</ref> Hunsicker exceeded his $4,000 goal with backers pledging a total of $4,190.

With the funds to finish producing the album, a release date of December 26, 2011 was set for Every Little Thing. Due to a vocal injury, Hunsicker ultimately postponed the release of the album to March 6, 2012.

==Vocal injury==

In 2011 Hunsicker suffered vocal setbacks while touring with his tribute band Frontiers. In July 2011 Hunsicker had an incident with a fog machine in [[Toledo, Ohio|Toledo]], [[Ohio]] that caused his vocal cords to go into spasm leaving him unable to finish the show. Later that year Hunsicker announced that he was suffering from a hemorrhaged vocal cord and that he was reducing the number of shows that he would perform. On December 8, 2011 Hunsicker announced on his website that he would have to undergo vocal cord surgery to remove a vocal cord polyp. As a result, he cancelled all remaining shows that were scheduled for 2011. He also cancelled shows scheduled for January and February 2012 to allow himself the time necessary to recover from the surgery.<ref>[http://www.jeremeyfrederick.com/page/2/] at Jeremey Frederick</ref> Ultimately his vocal cords did not heal properly and Hunsicker had to have the surgery redone delaying his return to touring.<ref>[http://articles.wdbj7.com/2012-05-16/vocal-cord-surgery_31732656] at WDBJ7</ref>

==Return to touring==

In May 2012, Hunsicker returned to the stage with his band Frontiers. Due to his vocal injury Hunsicker was still unable to sing Journey songs. As a result, Hunsicker announced that he would have fill-in singers performing with the band to give his vocal cords additional time to heal. The band began relying on a handful of singers to perform at various shows. Despite the fact that he was unable to sing, Hunsicker accompanied his band playing keyboards for the summer 2012 shows. Because he did not know when he would be able to sing again, Hunsicker announced that he did not know if there would be a future for the band.<ref name="journeytributeband">[http://www.journeytributeband.com/] at Journey Tribute Band</ref>

==End of Frontiers==

On July 12, 2012 Hunsicker announced on the Frontiers website that he and his band were planning to call it quits after ten years. As a result of his vocal cord surgery that left him unable to sing [[Journey (band)|Journey]] tunes, Hunsicker decided that this was the end of the road for Frontiers.<ref name="journeytributeband" /> In the announcement, Hunsicker said that the band would perform all scheduled 2012 tour dates with fill-in lead singers and that they would not be booking any more shows. The last Frontiers show was on October 13, 2012 in Hickory, NC.<ref>[http://www.journeytributeband.com//?page_id=8] at Journey Tribute Band Tour</ref>

==Post Frontiers==
Upon announcing that his tribute band Frontiers was calling it quits, Hunsicker founded the Big Bang Agency in July 2012.<ref>[http://www.bigbangbands.com] at Big Bang Agency</ref> Big Bang is a booking agency specifically targeted towards tribute acts. In addition to founding Big Band, Hunsicker decided to found a Tom Petty tribute band called The Waiting.<ref>[http://www.tompettyshow.com] at The Waiting</ref> While the band initially booked very well,{{Clarify|reason=vague|date=July 2015}} The Waiting seemed to be short-lived. The band has not performed together since 2013. As of 2015 Hunsicker is self-employed as a copywriter.<ref>[http://www.jeremeyhunsicker.com] at Jeremey Hunsicker</ref>

==Discography==

===Solo===

===Albums===
{| class="wikitable"
|-
!Year
!Album
|-
|2012
|''Every Little Thing''
|}

===Singles===
{|class="wikitable"
|-
!Year
!Title
|-
|-
|2011
|''This is Your Life''
|-
|2011
|''Stay''
|}

===with Journey===
* ''[[Revelation (Journey album)|Revelation]]'' (2008) (Songwriter)

==References==
{{Reflist|2}}

==External links==
* [http://www.journeytributeband.com] 
* [http://www.jeremeyfrederick.com]
* [http://music.jeremeyfrederick.com/album/every-little-thing]

{{Authority control}}

{{DEFAULTSORT:Hunsicker, Jeremey}}
[[Category:1971 births]]
[[Category:Living people]]
[[Category:People from Roanoke, Virginia]]
[[Category:American singer-songwriters]]
[[Category:Songwriters from Virginia]]