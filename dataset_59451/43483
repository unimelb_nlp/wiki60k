<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name =XF-91 Thunderceptor
  |image =XF91-21republic.jpg
  |caption =
}}{{Infobox Aircraft Type
  |type =Prototype interceptor aircraft
  |manufacturer =[[Republic Aviation]]
  |designer =
  |first flight =9 May 1949
  |introduced =
  |retired =
  |status =Canceled
  |primary user =
  |more users =
  |produced =
  |number built =2
  |unit cost ={{US$|link=yes}}11.6 million for the program<ref name="knaack">Knaack 1978, p. 309.</ref>
  |developed from= [[Republic F-84 Thunderjet]]
  |variants with their own articles =
}}
|}

The '''Republic XF-91 Thunderceptor''' (originally designated '''XP-91''') was a mixed-propulsion [[prototype]] [[interceptor aircraft]], developed by [[Republic Aviation]]. The aircraft would use a [[jet engine]] for most flight, and a cluster of four small [[rocket engine]]s for added thrust during climb and interception. The design was largely obsolete by the time it was completed due to the rapidly increasing performance of contemporary jet engines, and only two prototypes were built. One of these was the first US fighter to exceed [[Mach (speed)|Mach]] 1 in level flight.

A unique feature of the Thunderceptor was its unusual [[inverse tapered wing]], in which the [[Chord (aeronautics)|chord]] length increased along the wing span from the root to the tip, the opposite of conventional [[swept wing]] designs. This was an attempt to address the problem of [[pitch-up]], a potentially deadly phenomenon that plagued early high-speed models. The Thunderceptor's design meant the entire wing [[Stall (flight)|stalled]] smoothly, more like a straight-wing design.

==Design and development==
[[File:XF-91.jpg|thumb|XF-91 in 1951 at Edwards Air Force Base.]]
During the development of the XP-84, Republic, under the guidance of Alexander Kartveli looked at the installation of rockets to fighters. The company was inspired by German wartime aircraft: the rocket-powered [[Messerschmitt Me 163]] and the experimental rocket-boosted turbojet [[Messerschmitt Me 262]]C ''Heimatschützer'' (home protector) series of [[Interceptor aircraft|interceptor]] prototypes.

The Thunderceptor design was one of two [[swept-wing]] modifications based on the original [[Republic F-84 Thunderjet]], the other being the [[Republic F-84F Thunderstreak]] which was developed later. A serious problem with most [[swept wing]] designs of the era was dangerous performance at low speeds and high [[angle of attack]]. The stagnant airflow over the wing tended to "slide" towards the wingtips, which caused them to [[Stall (flight)|stall]] before the rest of the wing. In this situation the center of lift would rapidly shift forward relative to the center of mass, pitching the nose up and leading to an even greater angle of attack or, in extreme cases, end-over-end tumbling of the aircraft. Aircraft caught in this regime would often stall and crash, and a rash of such accidents with the [[North American F-100 Super Sabre]] led to the term [[Sabre dance (aerodynamics)|"Sabre dance"]]. The most famous incident was the loss of an F-100C Super Sabre during an attempted emergency landing at Edwards AFB, California on 10 January 1956 which was caught by film cameras set up for an unrelated test. The pilot fought to retain control as he rode the knife-edge of the flight envelope but fell off on one wing, hit the ground and exploded with fatal results.

The Thunderceptor's most notable design feature was intended to address this problem. The wings were built to have considerably more [[Chord (aeronautics)|chord]] (distance from the leading edge to the trailing edge) at the tip than root, allowing them to generate more lift. This neatly addressed the problem of Sabre dance by delaying the point of stall on the tip to that of the entire wing. A side-effect of this design was that the tips had more internal room, so the [[landing gear]] was mounted to retract outwards with the wheels lying in the wingtips, using two smaller wheels in a [[tandem]] arrangement for each main gear strut, instead of one larger one. Another design change was the ability to vary the [[angle of incidence (aerodynamics)|angle of incidence]] of the wing as a whole, tilting it up for low speed operations during takeoff and landing, and then "leveling it off" for high-speed flight and cruise. This allowed the fuselage to remain closer to level while landing, greatly improving visibility.

In keeping with its intended role as an interceptor, the nose was redesigned to incorporate a [[radar]] antenna, forcing the air intake for the engine to be moved from its original nose-mounted position to a new intake below it. The fuselage was otherwise very similar to the F-84's. The first prototype did not include the radome, although this was fitted to the second prototype.

==Testing and evaluation==
[[File:Republic XF-91 banking away in flight.jpg|thumb|XF-91 Thunderceptor during testing]]
[[File:XF91-22republic.jpg|thumb|On the left is Republic XF-91 ([[United States military aircraft serials|S/N]] 46-680) after the nose radome installation and on the right is XF-91 ([[United States military aircraft serials|S/N]] 46-681) after the V-tail modification.]]

The first prototype made its first flight on 9 May 1949, breaking the speed of sound in December 1951. It was later modified with a small radome for gunnery ranging (although not the "full" radome from the second prototype). The second prototype included the full radome and chin-mounted intake, but was otherwise similar. With both the jet and rockets running, the aircraft could reach Mach&nbsp;1.71. Both prototypes completed 192 test flights over the course of five years.<ref>Yenne 1993, p. 114.</ref>

The second prototype, ''46-681'', had an engine failure during takeoff from Edwards AFB in the summer of 1951. Republic test pilot [[Carl Bellinger]] escaped from the aircraft just as the tail melted off - total flight time was a mere 90 seconds. By the time fire apparatus arrived, driving seven miles across the dry lake bed, the tail section had been reduced to ashes. 46-681 was then fitted with a [[V-tail|"V"]] or "butterfly" tail, and was flight-tested with this configuration. It was later used at Edwards AFB as a crash-crew training simulator, then scrapped.<ref>Pace 1991, p. 87.</ref>

As an interceptor the Thunderceptor was soon eclipsed by designs from other companies, but like the Thunderceptor none of these would go into production. The [[United States Air Force]] decided to wait the short time needed to introduce newer and much more capable designs created as a part of the [[1954 interceptor]] project. The Thunderceptor, like the other interceptor designs of the era, had extremely short flight times on the order of 25 minutes, making them almost useless for protecting an area as large as the United States. The 1954 designs outperformed the XF-91 in speed, range, loiter time, as well as including the radar and [[fire-control system]]s needed for night and all-weather operation. The era of the dedicated [[day fighter]]-type interceptor was over.

==Aircraft disposition==
[[File:Republic XF-91.jpg|thumb|XF-91 Thunderceptor, s/n ''46-680'' on display]]
*46-0680 - Research & Development Gallery at the [[National Museum of the United States Air Force]] at [[Wright-Patterson AFB]] in [[Dayton, Ohio]].<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=584 "XF-91 Thunderceptor/46-680"] ''National Museum of the United States Air Force.'' Retrieved: 13 June 2011.</ref>
*46-0681 - destroyed while testing at [[Edwards AFB]] in 1951.<ref>[http://www.joebaugher.com/usaf_serials/1946.html "XF-91 Thunderceptor/46-681."] ''Joe Baugher serial numbers.'' Retrieved: 10 May 2013.</ref>

==Specifications (XF-91 Thunderceptor)==
[[File:XF-91 3-View line art.svg|right|300px]]
{{aircraft specifications|
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=both
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref={{citation needed|date=July 2011}}
|crew=1
|length main=43 ft 3 in
|length alt=9.52 m
|span main=31 ft 3 in
|span alt=13.18 m
|height main=18 ft 1 in
|height alt=5.51 m
|area main= 320 ft²
|area alt= 29.73 m²
|empty weight main=14,140 lb
|empty weight alt=6,410 kg
|loaded weight main=18,600 lb
|loaded weight alt=8,400 kg
|max takeoff weight main=28,300 lb
|max takeoff weight alt=12,840 kg
|engine (jet)= [[General Electric J47|General Electric J47-GE-7 (later GE-17)]]
|type of jet= axial-flow turbojet
|number of jets=1 
|thrust main= 5,200 lbf 
|thrust original=; 6,900 lbf with afterburning 
|thrust alt= 30,6 kN
|engine (prop)=[[Reaction Motors XLR11|Reaction Motors XLR11-RM-9]]
|type of prop= rocket
|number of props=4 
|power main= 1,500 lbf 
|power alt= 7 kN
|max speed main=984 mph
|max speed alt=1,584 km/h
|range main=1,170 mi
|range alt=1,880 km
|ceiling main=50,000 to 55,000 ft
|ceiling alt=15,200 to 16,800 m
|climb rate main= 47,500 ft in 2.5 minutes
|climb rate alt= 14,500 m 
|loading main= 58.12 lb/ft²
|loading alt= 283 kg/m²
|thrust/weight= 0.60
|guns=4 × 20 mm (.79 in) cannon
}}

==See also==
{{Portal|Military of the United States|Aviation}}
{{aircontent|
|related=
|similar aircraft=
*[[Avro 720]]
*[[Saunders-Roe SR.53]]
*[[Saunders-Roe SR.177]]
|lists=
*[[List of military aircraft of the United States]]
*[[List of fighter aircraft]]
|see also=
}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{Refbegin}}
* Jenkins, Dennis R. and Tony R. Landis. ''Experimental & Prototype U.S. Air Force Jet Fighters.'' North Branch, Minnesota, USA: Specialty Press, 2008. ISBN 978-1-58007-111-6.
* Knaack, Marcelle Size.''Encyclopedia of US Air Force Aircraft and Missile Systems: Volume 1, Post-World War II Fighters, 1945-1973''. Washington, D.C.: Office of Air Force History, 1978. ISBN 0-912799-59-5.
* Pace, Steve. ''Republic XF-91 Thunderceptor Rocket Fighter'' (Air Force Legends N.210). Simi Valley, California, USA: Steve Ginter Books, 2000. ISBN 0-942612-91-4.
* Pace, Steve. ''X-Fighters: USAF Experimental and Prototype Fighters, XP-59 to YF-23''. St. Paul, Minnesota, USA: Motorbooks International, 1991. ISBN 0-87938-540-5.
* Winchester, Jim. ''The World's Worst Aircraft: From Pioneering Failures to Multimillion Dollar Disasters''. London: Amber Books Ltd., 2005. ISBN 1-904687-34-2.
* [[Chuck Yeager|Yeager, Chuck]] and Leo Janos. ''Yeager: An Autobiography''. New York: Bantam Books, 1986. ISBN 0-553-25674-2.
* Yenne, Bill. ''The World's Worst Aircraft''. New York: Dorset Press, 1993. ISBN 0-88029-490-6.
{{Refend}}

== Further reading ==
* {{cite book |last= Pace |first= Steve |year= 2000 |title= Republic XF-91 Thunderceptor |edition= First |editor-first= |editor-last= |publisher= Ginter Books |series=Air Force Legends |volume=Nº210 |language= |url= http://www.ginterbooks.com/AIRFORCE/AF210.htm |isbn= 0-942612-91-4 |location= California, United States |accessdate= 1 February 2015 }}

==External links==
{{Commons category|Republic XF-91}}
*[http://www.centennialofflight.gov/essay/Aerospace/Republic/Aero43G13.htm XF-91 in U.S Centennial of Flight Commission]
*[http://1000aircraftphotos.com/Fairchild/5419.htm Several pictures of the XF-91 ''46-680'']
*[http://www.flightglobal.com/pdfarchive/view/1949/1949%20-%200431.html Interceptor, 1949] - pictures from ''Flight'' 1949

{{Republic aircraft}}
{{USAF fighters}}

[[Category:Republic aircraft|F-091]]
[[Category:United States fighter aircraft 1940–1949|Republic F-91]]
[[Category:Single-engined jet aircraft]]
[[Category:Aircraft with auxiliary rocket engines]]
[[Category:Monoplanes]]