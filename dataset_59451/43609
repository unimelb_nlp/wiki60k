<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Sie-3
 | image=Siebert SIE3.jpeg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Glider competition classes|Standard class]] club [[glider (sailplane)|glider]]
 | national origin=Germany
 | manufacturer=Paul Siebert Sport und Segelflugzeugbau, Munster
 | designer=Wilhelm (Peter) Kurten
 | first flight=September 1968
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=27
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Siebert Sie-3''' is a single-seat [[Glider competition classes|Standard class]] [[glider (sailplane)|glider]] designed and produced in Germany in the 1960s for club use.

==Design and development==
Through the 1960s Paul Siebert's company had built [[Schleicher Ka-6 Rhönsegler|Kaiser Ka 6]] gliders under licence.  Anticipating the end of this agreement, they began to design a successor with a relatively low cost and simple wooden structure but with performance enhanced by a more modern [[Wortmann]] [[airfoil|wing profile]].  The outcome was the Sie 3, which first flew in September 1968.<ref name=simonsIII/>

The Sie-3 has a wooden structure and is largely [[plywood]] covered.  The [[monoplane#Types of monoplane|high set]] single spar wings have the same aerodynamic profile throughout, with an unswept centre section of parallel [[chord (aircraft)|chord]] and constant thickness over 60% of the span and strongly straight tapered panels outboard.  There are aluminium [[Air brake (aircraft)|Schempp-Hirth airbrakes]] near mid-chord on the outer centre section, just behind the spar, and [[ailerons]] on the outer panels.  The only [[Aircraft fabric covering|fabric covering]] on the wing is over the centre section aft of the spar.<ref name=simonsIII/>

The [[fuselage]] is a ply covered [[monocoque|semi-monocoque]] with a short [[glass fibre]] nose fairing; aft of the wing it gently tapers towards the tail. The single piece [[canopy (aircraft)|canopy]] blends into the fuselage profile. The narrow chord swept [[fin]] is an integral ply covered part of the fuselage carrying a broad, fabric covered [[rudder]] with a near vertical [[trailing edge]] that extends to the fuselage keel.  A straight tapered, largely fabric covered [[stabilator|all moving tailplane]] with a cut-out for rudder movement is mounted at the base of the fin.  The Sie 3 lands on a [[Landing gear#Gliders|monowheel]] well embedded within the fuselage, aided by a sprung tailskid.<ref name=simonsIII/>

The Sie-3 flew for the first time in September 1968, piloted by its designer.  Its performance was similar to contemporary wooden gliders.<ref name=simonsIII/>

==Operational history==
Siebert manufactured 27 Sie 3s in the four years after it received type approval in January 1972. Some were built by amateurs from kits.<ref name=simonsIII/> In 2010 twelve remained on the civil registers of European countries, seven in Germany, two in Denmark and one each in the [[Netherlands]], Belgium and [[Portugal]].<ref name=EuReg/> In addition, there is one in the United States.<cn|date=July 2016/cn>
<!-- ==Variants==-->

==Specifications==
{{Aircraft specs
|ref=Gliders and Sailplanes of the world<ref name=Hardy/>
|prime units?=met
        <!--General characteristics
-->
|crew=One
|length ft=22
|length in=0
|length note=. Simons<ref name=simonsIII/> gives the length as 6.966 m
|span m=15.00
|span note=
|height ft=3
|height in=11
|height note=
|wing area sqft=127.44
|wing area note=
|aspect ratio=19.5<ref name=simonsIII/>
|airfoil=Wortmann FX61-184<ref name=simonsIII/>
|empty weight lb=467
|empty weight note=
|gross weight lb=750
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|wing loading kg/m2=27.7
|wing loading note=<ref name=simonsIII/>
|more general=
<!--
        Performance
-->
|perfhide=
|max speed mph=124
|max speed note=in smooth air
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=

|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=best 34.3:1 at 90 km/h; 49 kn (56 mph)
|sink rate ftmin=134
|sink rate note=minimum, at 78 km/h; 42 kn (48.5 mph)
|lift to drag=
|more performance=
}}

{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=

<ref name=Hardy>{{cite book |title=Gliders & Sailplanes of the World|last= Hardy |first= Michael |coauthors= |edition= |year=1982|publisher=Ian Allen Ltd|location= London|isbn=0 7110 1152 4|page=107 }}</ref>

<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0|page=}}</ref>

<ref name=simonsIII>{{cite book |title=Sailplanes 1965-2000|last=Simons |first=Martin |edition=2nd revised |year=2005|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9808838 1 7|pages=171–3}}</ref>

}}
<!-- ==External links== -->

[[Category:German sailplanes 1960–1969]]