{{Infobox person
| name  =
| image =
| image_size  =
| caption  =
| birth_date  = {{birth date and age|1915|06|14}}
| birth_place =
| death_date  =
| death_place =
| occupation  = [[businessperson|businesswoman]], [[investment|investor]]
| salary=
| networth =
| spouse= [[Ervin J. Nutter]]
| children =
| website  =
}}
'''Zoe Dell Lantis Nutter''' (born June 14, 1915) is an [[United States|American]] [[dancer]], [[Model (person)|model]] and [[Hunting|huntress]] who later became a promoter, educator, marketer, [[commercial pilot]] and [[philanthropist]].

==Biography==
Lantis received early encouragement in public performing. Her family lived next door to a dance teacher, so Lantis did odd jobs to become her student. After high school, she moved to [[San Francisco]], where soon she was dancing for the [[San Francisco Ballet]] and in [[nightclub]]s. After one 1939 show, aviation executives approached her about promoting [[commercial aviation]] at the [[Golden Gate International Exposition]] on nearby [[Treasure Island (California)|Treasure Island]], to promote air travel in general, and overseas flights to [[East Asia]] in particular, via the new [[China Clipper]] [[flying boat]]s based at Treasure Island. Dressed in a pirate costume, she served as the official hostess for the exposition. Soon she had flown 100,000 passenger miles to meet mayors and governors across the U.S., demonstrating the safety and comforts of air travel.

In the middle of a campaign against [[American burlesque|burlesque theater]]s, New York mayor [[Fiorello LaGuardia]] insisted she wear her coat over her skimpy costume in order to be photographed with him. The story took off and soon she was labeled the "most photographed woman in the world," appearing in [[Life (magazine)|Life]] on February 7, 1938.

After a short film career with [[Paramount Pictures]], she joined the [[United Service Organizations]] to entertain [[United States armed forces|troop]]s during [[World War II]]. Surgery ended her dancing career. A friend suggested she pursue flying along with her first husband, with whom she earned her [[Pilot certification in the United States|pilot certification]]. Moving to [[northern California]] after the war, she regularly flew into San Francisco to go shopping. In 1958, she represented [[Chevron Corporation|Standard Oil of California]] at the [[Expo 58]] [[World's Fair]] in [[Brussels, Belgium]], and continued to promote commercial air travel around the world.

In the early 1960s, she took a job with [[Piper Aircraft|Piper Aircraft, Inc.]], one of the main manufacturers of private airplanes. President [[William T. Piper]], known as the Henry Ford of aviation for making inexpensive simple to operate aircraft, wanted her to be his spokesperson for general aviation, as she had been for commercial aviation. She demonstrated aircraft and sold flight training for Piper's subsidiary [[Piper Aircraft|Monarch Aviation]], which operated a base and flight school in [[Monterey, California]]. She soon learned to avoid flying over nearby mountains due to insufficient engine power. Among her responsibilities was to make flying accessible to ladies, to teach women to be able to read a map, handle the radio or land the plane in event of an emergency. Her work helped improve [[Air safety|flight safety]] and train the increasing numbers of [[Private Pilot|private pilot]]s. In addition, while in Monterey she helped found the local chapter of the [[Ninety-Nines]], the Organization of Licensed Women Pilots.

When Piper needed a cheaper trainer plane, she flew to [[Dayton, Ohio]] to evaluate a new plane there. Local businessman [[Ervin J. Nutter]],<ref name="UKEngrProfile">{{cite web|url=http://www.engr.uky.edu/alumni/hod/nutter.php|title=College of Engineering - Hall of Distinction - Ervin J. Nutter |publisher=[[University of Kentucky College of Engineering]]|accessdate=8 March 2010}}</ref> who built parts for the plane, assisted her. After three trips, she turned down the aircraft, but married Nutter, and moved to Ohio in 1965.

Nutter ran [[Elano Corporation]], which made [[Hollow structural section|tubing]] and [[Components of jet engines|engine components]] for the [[Aerospace manufacturer|aerospace industry]]. Her flying experience and marketing background complemented her husband's technical expertise. Soon she was directing promotions of the Small Aircraft Division and serving as one of the company pilots.

Elano developed a stainless steel manifold that boosted the performance of [[Diesel locomotive|diesel locomotive engines]] for [[General Electric]]. Given her experience flying over the mountains in California, she believed a better [[Manifold (automotive)|manifold]] would give pilots more power in the air. She interviewed several engineers before hiring [[John Warlick]], working with him to define the problem through promotion of the finished product. Despite initial rejections by all aircraft engine manufacturers, the manifold eventually became standard equipment on many models. In addition to adding power, the manifold reduced engine maintenance for annual inspections, and enabled a quieter, better-heated [[cockpit]].

As a pilot, she logged over 2,000 [[Flight manifest|flight hour]]s, earning commercial, instrument and multi-engine [[Instrument rating|ratings]]. She delivered rush jobs for Elano-20 minutes to the airport, ten minutes to [[Preflight Planning Dispatch Checklist|preflight]], an hour's flight to [[Cincinnati]] and [[Columbus, Ohio|Columbus]], and within a couple hours a part could be delivered to [[Boston]], [[Chicago]] or [[Atlanta]].

She joined the [[Civil Air Patrol|Ohio Civil Air Patrol]], flying [[search and rescue]] missions, and became active in the [[National Aviation Hall of Fame]], with 12 years service as a board member, serving as its first woman president in 1988. She has served on many other charitable, educational and civic boards, such as the [[San Francisco Aeronautical Society]] and the [[Ohio Humanities Council]]. She [[centenarian|turned 100]] in June 2015.<ref>{{cite web|url=http://www.daytondailynews.com/news/local/100th-birthday-celebration-for-zoe-dell-nutter/ykNya361zwME5C3ELn43lN/|title=100th Birthday Celebration for Zoe Dell Nutter|publisher=[[Dayton Daily News]]|author=Staff|date=2015-06-20|accessdate=2017-01-25}}</ref>

==Quotes==
{{cquote|Get out there with what you've got, because you never know how you'll get to use it.}}

{{cquote|The most dangerous part of flying is the trip to the airport.}}

{{cquote|In all the years I flew, I never lost my luggage, and I've never lost my lunch.}}

{{cquote|Few women can claim to be a dancer, a model, a huntress, an aviator, a philanthropist and a pirate... Zoe Dell Lantis Nutter is one of them.}} First Lady [[Laura Bush]] in 2006

==Awards==
*first recipient of [[Wright State University]]'s Spirit of Innovation Award.
*the Deeds-Kettering Award for outstanding contributions to the Engineers Club of Dayton<ref>{{cite web |url=http://www.daytoninnovationlegacy.org/zoedellnutter.html |title=Zoe Dell Lantis Nutter-Selling the Sky |author=Mark Martel |publisher=Dayton Innovation Legacy}}</ref>
*the Ford Theater's Lincoln Medal
*a 2009 recipient of the Bob Hoover Freedom of Flight Award

==See also==
*[[E.J. Nutter Training Facility]]
*[[Nutter Center]]

==Notes and references==
{{reflist}}

*Zoe Dell Lantis Nutter: A 70-Year Commitment to Aviation's Success." by Carol L. Osborne, Airport Journals July 2009

==External links==
* [http://www.engineersclubfoundation.org/zenphoto/index.php?album=zoedelllantisnutter Dayton Innovation Legacy | Engineers Club of Dayton Foundation | Zoe Dell Lantis Nutter] (photo gallery)
* [http://pacificastatueproject.org/blog/2011/03/22/zoe-dell-nutter-lantis/ The Most Photographed Woman of 1939], including a photo of the "Treasure Island Pirate Girl"

{{DEFAULTSORT:Nutter, Zoe Dell}}
[[Category:1915 births]]
[[Category:Living people]]
[[Category:American aviators]]
[[Category:American centenarians]]
[[Category:People from Dayton, Ohio]]