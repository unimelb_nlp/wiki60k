{{Infobox journal
| cover = [[File:Journal of the Insectivorous Plant Society.jpg|230px]]Volume 55, issue 1 (January 2004)
| discipline = [[Botany]]
| website = http://ips.2-d.jp/ips-newsletter.htm
| publisher = [[Insectivorous Plant Society]]
| country = Japan
| language = Japanese
| abbreviation = J. Insectiv. Pl. Soc.
| history = 1950–present
| frequency = Quarterly
| ISSN = 0286-6102
| OCLC = 28858571
}}

{{nihongo|'''''The Journal of Insectivorous Plant Society'''''|食虫植物研究会々誌|Shokuchūshokubutsu kenkyū-kai kaishi}} is a quarterly [[Japanese language|Japanese-language]] [[Periodical publication|periodical]] and the official publication of the [[Insectivorous Plant Society]] of [[Japan]].<ref name=Rice>Rice, B. 2010. [http://web.archive.org/web/20101010231948/http://sarracenia.com/faq/faq6100.html Carnivorous Plant Society Archives]. The Carnivorous Plant FAQ. [archived page from October 10, 2010]</ref> The journal was established in January 1950.<ref name=about>[http://www2.odn.ne.jp/~chr79360/English%20page/about%20us.htm About IPS, Japan]. Yoshiyuki Sodekawa's personal website.</ref> As of 2010, it is published in [[A4 paper|A4]] format<ref>{{jp icon}} [http://ips.2-d.jp/IPS-info.htm 食虫植物研究会 The Insectivorous Plant Society (IPS, Japan) の御案内]. Insectivorous Plant Society.</ref> and totals around 120 pages annually.<ref name=Rice /> The English title has been used alongside the original Japanese one from the April 1986 issue onwards.<ref>[http://www.worldcat.org/title/shokuchu-shokubutsu-kenkyukai-kaishi/oclc/28858571 Shokuchū Shokubutsu Kenkyūkai kaishi]. WorldCat.</ref>

Typical articles include matters of horticultural interest, field reports, literature reviews, and new [[taxon]] and [[cultivar]] descriptions.<ref name=about /> They are usually entirely in Japanese,<ref name=about /> although species descriptions may also be in English.<ref name=Wakabayashi1 /><ref name=Wakabayashi2 />

==Taxon descriptions==
''The Journal of Insectivorous Plant Society'' has published formal descriptions of the following [[taxon|taxa]].<ref name=Schlauer>Schlauer, J. N.d. [http://www.omnisterra.com/bot/cp_home.cgi?name=J.Insectiv.Pl.Soc. Query results: J. Insectiv. Pl. Soc.]. Carnivorous Plant Database.</ref>

{{Col-begin|width=70%}}
{{Col-break}}

===''Nepenthes''===
*''[[Nepenthes eymae]]'' (as ''N.&nbsp;eymai'')<ref name=C&J>Cheek, M.R. & M.H.P. Jebb 2001. [[Nepenthaceae (2001 monograph)|Nepenthaceae]]. ''Flora Malesiana'' '''15''': 1–157.</ref>
*''[[Nepenthes × ferrugineomarginata]]''
*''[[Nepenthes × kinabaluensis]]''
*''[[Nepenthes × kuchingensis]]''
*[[Nepenthes maxima|''Nepenthes maxima'' f. ''undulata'']]
*''[[Nepenthes mindanaoensis]]''
*''[[Nepenthes peltata]]''
*''[[Nepenthes × pyriformis]]'' (as a species)<ref>Kurata, S. 2001. Two new species of ''Nepenthes'' from Sumatra (Indonesia) and Mindanao (Philippines). ''The Journal of Insectivorous Plant Society'' '''52'''(2): 30–34.</ref>
*''[[Nepenthes glabrata|Nepenthes rubromaculata]]'' ([[later homonym]])<ref name=C&J />
*''[[Nepenthes saranganiensis]]''

===''Utricularia''===
*''[[Utricularia linearis]]''<ref name=Wakabayashi2>Wakabayashi, H. 2010. {{cite web|url= http://www.asahi-net.or.jp/~ze9h-wkby/U_linearis-WEB.pdf |title=''Utricularia linearis'' (Lentibulariaceae), a new species from the Howard Springs, Northern Territory, Australia. }} ''The Journal of Insectivorous Plant Society'' '''61'''(4): 88–92.</ref>
*''[[Utricularia ramosissima]]''<ref name=Wakabayashi1>Wakabayashi, H. 2010. {{cite web|url= http://www.asahi-net.or.jp/~ZE9H-WKBY/U_ramosissima.pdf |title=''Utricularia ramosissima'' (Lentibulariaceae), a new species from north-eastern Thailand. }} ''The Journal of Insectivorous Plant Society'' '''61'''(2): 33–38.</ref>
{{Col-break}}

===Cultivars===
The journal also published the following ''[[Nepenthes]]'' [[cultivar]] names of the 'Koto' series by K. Kawase.<ref name=Schlauer />

*[[Nepenthes 'Accentual Koto'|''Nepenthes'' 'Accentual Koto']]
*[[Nepenthes 'Dashy Koto'|''Nepenthes'' 'Dashy Koto']]
*[[Nepenthes 'Dinkum Koto'|''Nepenthes'' 'Dinkum Koto']]
*[[Nepenthes 'Dreamy Koto'|''Nepenthes'' 'Dreamy Koto']]
*[[Nepenthes 'Easeful Koto'|''Nepenthes'' 'Easeful Koto']]
*[[Nepenthes 'Ecstatic Koto'|''Nepenthes'' 'Ecstatic Koto']]
*[[Nepenthes 'Emotional Koto'|''Nepenthes'' 'Emotional Koto']]
*[[Nepenthes 'Facile Koto'|''Nepenthes'' 'Facile Koto']]
{{col-end}}

== References ==
{{reflist}}

== External links ==
* {{Official website|http://ips.2-d.jp/ips-newsletter.htm}} {{jp icon}}
* [http://ipsjapan.web.fc2.com/kaishi/index.html Complete article index (1950–2007)] {{jp icon}}
* [http://www2.odn.ne.jp/~chr79360/IPS,Japan/journal.htm Overview of issues from January 2000 to January 2004] {{jp icon}}

{{Carnivorous plant journals}}

{{DEFAULTSORT:Journal of Insectivorous Plant Society}}
[[Category:Magazines established in 1950]]
[[Category:Quarterly magazines]]
[[Category:Japanese magazines]]
[[Category:Carnivorous plant magazines]]
[[Category:1950 establishments in Japan]]
[[Category:Magazines published in Tokyo]]