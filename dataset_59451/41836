[[Image:Beekeper collecting swarm.jpg|thumb|upright=1.3|A beekeeper collecting a bee swarm. If the queen can be swept to the frame and placed into the hive the remaining bees will follow her scent.]]

'''[[Beekeeping]]''' in the '''[[United States]]''' dates back to the 1860s.<ref>{{cite web | title=Beekeeping History|url=http://www.outdoorplace.org/beekeeping/history.htm|work=John's Beekeeping Notebook|publisher=Outdoorplace.org|accessdate=6 April 2013}}</ref>

==Development of beekeeping in the United States==
John Harbison, originally from [[Pennsylvania]], successfully brought bee keeping to the US west coast in the 1860s, in an area now known as [[Harbison Canyon]], [[California]], and greatly expanded the market for honey throughout the country.

Beekeeping was traditionally practiced for the bees' [[honey]] harvest, although nowadays crop pollination service can often provide a greater part of a commercial beekeeper's income. Other hive products are pollen, [[royal jelly]], and [[propolis]], which are also used for nutritional and medicinal purposes, and [[wax|beeswax]], which is used in [[candles|candle making]], [[cosmetics]], wood polish, and for modelling. The modern use of hive products has changed little since ancient times.

[[Western honey bee]]s are not native to the Americas. American colonists imported honey bees from [[Europe]] for their honey and wax. Their value as pollinators began to be appreciated by the end of the nineteenth century. The first honey bee subspecies imported were likely [[European dark bee]]s. Later [[Italian bee]]s, [[Carniolan honey bee]]s and [[Caucasian bee]]s were added.

Western honey bees were also brought from the [[Primorsky Krai]] in [[Russia]] by [[Ukraine|Ukrainian]] settlers around the 1850s. These [[Russian honey bee]]s that are similar to the [[Carniolan bee]] were imported into the U.S. in 1997.<ref>[http://www.russianbee.com/russians.htm Foley's Russian Bees: The Primorsky Russian Honeybee] Accessed December, 2015</ref> The Russian honey bee has shown to be more resistant to the bee [[parasite]]s ''[[Varroa destructor]]'' and ''[[Acarapis woodi]]'', although their commercial use and availability are extremely limited in scope because other, better strains are available (e.g., VSH lines).

Before the 1980s, most U.S. hobby beekeepers were farmers or relatives of a farmer, lived in rural areas, and kept bees with techniques passed down for generations. The arrivals of [[Diseases of the honey bee#Acarine mites (Tracheal mites)|tracheal mites]] and [[varroa mite]]s in the 1980s and [[Diseases of the honey bee#Small hive beetle(small hive beetle)|small hive beetles]] in the 1990s have made the practice more challenging for the hobbyist.

==Types of beekeepers==
Beekeepers generally categorize themselves as:
* Commercial beekeeper &mdash; Beekeeping is the primary source of income.
* Sideliner &mdash; Beekeeping is a secondary source of income.
* [[Hobby]]ist &mdash; Beekeeping is not a significant source of income.

Some southern U.S. beekeepers keep bees primarily to raise queens and package bees for sale. Northern beekeepers can buy early spring queens and 3- or 4-pound packages of live worker bees from the South to replenish hives that die out during the winter, although this is becoming less practical due to the spread of the [[Africanized bee]].

In cold climates commercial beekeepers have to migrate with the seasons, hauling their hives on trucks to gentler southern climates for better wintering and early spring build-up. Many make "nucs" (small starter or nucleus colonies) for sale or replenishment of their own losses during the early spring. Some may pollinate [[Squash (fruit)|squash]] or [[cucumber]]s in Florida or make early honey from citrus groves in [[Florida]], [[Texas]] or [[California]]. The largest demand for pollination comes from the [[almond]] groves in California. As spring moves northward so do the beekeepers, to supply bees for tree fruits, blueberries, strawberries, cranberries and later vegetables. Some commercial beekeepers alternate between pollination service and honey production but usually cannot do both at the same time.

Beekeepers may harvest honey from July until October, according to the honey flows in their area. Good management requires keeping the hive free of [[pest (animal)|pests]] and [[disease]], and ensuring that the bee colony has room in the [[beehive (beekeeping)|hive]] to expand. Chemical treatments, if used for parasite control, must be done in the off-season to avoid any honey contamination. Exterminators therefore often offer bee-safe and honey-safe treatment methods targeted towards beekeepers. Success for the hobbyist also depends on locating the apiary so bees have a good [[nectar source]] and [[pollen source]] throughout the year.

Bee-related services in the United States are not limited only to beekeeping. A large sector is devoted to bee removal, especially in the case of [[Swarming (honey bee)]].  This is especially common in the [[Spring (season)|springtime]], usually within a two- or three-week period depending on the locale, but occasional swarms can happen throughout the producing season.

== US American honey production, and honey imports ==
{{Expand section|date=December 2014}}
According to the [[United States Department of Agriculture]], about {{convert|163000000|lb|kg}} of honey is produced in the United States each year.<ref>{{cite web
|url=http://www.rma.usda.gov/policies/ri-vi/apiculture.html
|title=Crop Policies and Pilots: Apiculture
|publisher=United States Department of Agriculture
|accessdate=April 13, 2013}}</ref>

Given that the demand for honey in the USA outweighs the supply, honey is also imported from other countries. A problem hereby is that in some countries (i.e. China), the food and safety regulations are considerably weaker, and in some instances, contaminants such as heavy metals and antibiotics are found in the honey.<ref>[http://www.theglobeandmail.com/technology/science/honey-laundering-the-sour-side-of-natures-golden-sweetener/article562759/?page=all Honey laundering]</ref><ref>Morgan Spurlock's Inside Man Honey, Bee-Ware documentary</ref> Pollen is often also removed from the honey, hereby masking its origin country; a practice that has not yet been outlawed in the USA. To make matters even worse, there is no active research on the origin of commercially sold honey, apart from one scientist, [[Vaughn Bryant]]. According to Bryant, up to 80% of the commercially sold honey has incorrect origin/contents labels.

==Bee rentals and migratory beekeeping==
[[Image:Bee migration 9045.JPG|250px|right|thumb|Moving spring bees from South Carolina to Maine for blueberry pollination]]
[[Image:Beehives on the road.jpg|250px|right|thumb|A load of supers]]

After the winter of 1907, U.S. beekeeper Nephi Miller decided to try moving his hives to different areas of the country to increase their productivity during winter. Since then, "migratory beekeeping" has become widespread in the U.S. It is a crucial element of U.S. agriculture, which could not produce anywhere near its current levels with native pollinators alone. Beekeepers earn much more from renting their bees out for pollination than they do from honey production.

One major U.S. beekeeper reports moving his hives from [[Idaho]] to [[California]] in January to prepare for [[almond]] pollination in February, then to [[apple]] [[orchard]]s in [[Washington (state)|Washington]] in March, to [[North Dakota]] two months later for honey production, and then back to Idaho by November &mdash; a journey of several thousands of miles. Others move from [[Florida]] to [[New Hampshire]] or to [[Texas]]. About two thirds of US domestic bees visit California for the almond bloom in February.

California currently leads production of almonds worldwide, with 80% of global production. Each spring, migratory beekeepers rent hives to almond farmers in the Central Valley for pollination. Honeybees increase almond yields from an expected 40&nbsp;lbs/acre to an average of 2,400&nbsp;lbs/acre.<ref>{{cite book
| last          = Nordhaus
| first         = Hannah
| title         = The Beekeeper's Lament
| url           = http://www.hannahnordhaus.com/
| year          = 2010
| publisher     = Harper Perennial
| isbn          = 978-0061873256
| pages         = 100–101
}}</ref>

The wider spread and intermingling in the US has resulted in far greater losses from ''Varroa'' mite infections in recent years, than in countries where beekeepers move bees around less.

==Honey Queen Program==
The American [[Honey Queens|Honey Queen Program]] is a national competition sponsored by the American Beekeeping Federation (ABF) with the main priorities of promoting the practice of beekeeping, increasing the national honey consumption, and educating the public about the value of honey bees. A queen and princess are crowned each year, serving as representatives of the entire beekeeping industry in the U.S.

==See also==
*[[Amos Root]]
*[[Agriculture in the United States]]

==References==
{{Reflist}}

==External links==
*[http://www.abfnet.org/ American Beekeeping Federation]
*[http://pollennation.blip.tv/#424386   ''Pollen Nation''], a documentary about commercial, itinerant beekeepers in the US
* [http://www.masterbeekeeper.org/ Dyce Laboratory for Honey Bee Studies] Cornell University
* [http://exhibits.mannlib.cornell.edu/beekeeping/ A Buzz About Bees: 400 Years of Bees and Beekeeping] E. F. Phillips Beekeeping Collection at Cornell University; Mann Library online virtual exhibit



{{BeeColonyMemberTypes}}

[[Category:Beekeeping in the United States| ]]
[[Category:Agriculture in the United States]]