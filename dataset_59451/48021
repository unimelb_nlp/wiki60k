{{Orphan|date=June 2014}}

'''Functional periodicity''' is a term that emerged around the late 19th century around the belief, later to be found invalid, that women suffered from physical and mental impairment during their [[menstrual cycle]]. Men held a higher status and were regarded as superior to women at this period in time. Many prominent male psychologists promoted the idea of functional periodicity. Women were not seen as being fit for certain types of work, responsibilities, and roles because of this idea.<ref name=":0">Stetter-Hollingworth,
L. (1914). Functional periodicity: An experimental study of the mental and
motor abilities of women during menstruation. ''Contributions to Education,'' 69.</ref> The idea of functional periodicity stems from ancient taboos and rituals that were passed on from generation to generation. It then developed into an actual theory in the twentieth century.<ref name=":0" />

Functional periodicity was investigated by a female psychologist named Leta Hollingworth. She made key contributions in the research of functional periodicity, as well as in the feminist movement at the time. Hollingworth, along with her husband Harry Hollingworth, established exceptional research on the idea of functional periodicity and created research studies investigating the science of motor/learning tasks involving the human body.<ref name=":1">Fancher, R., & Rutherford, A. (2012). ''Pioneers of psychology. ''(4th ed.). New York, NY: W.W. Norton & Company, Inc.</ref> Her research impacted how society viewed women, despite the patriarchal opinions held by many.

== Background and history ==

Functional periodicity was the idea of women being functionally impaired during their menstruation cycle. The untested hypothesis of the time was supported by men, because at this time they dominated society, and this idea helped to keep women in a subordinate position. Women were viewed as not qualified for certain types of work, achievements, and certain responsibilities. The belief also reinforced the stereotype of women being fragile in physical and emotional well-being.<ref name=":1" /> Multiple studies were performed to look at mental and physical (motor) abilities during menstruation. Many men concluded that women were not fit for certain work and responsibilities. For example, in the eighteenth and nineteenth century, there was debate about whether or not women should participate in higher education. Many argued that women should not go on to pursue higher education because of the dangers that may be involved relating to physiological circumstances.<ref name=":0" /> An example of reasoning using functional periodicity is a quote by [[Henry Maudsley]] in 1874. He further states:

‘"This is a matter of physiology, not a matter of sentiment… not a question of two bodies and minds that are in equal physical condition, but if one body and mind capable of sustained and regular hard labor, and of another body and mind which for one quarter of each month, during the best years of life, is more or less sick and unfit for hard work."<ref name=":0" />

This quote exemplifies the sexist beliefs expressed during this time period. Because of these opinions and beliefs, the idea of functional periodicity became more relevant in American society.

The idea of functional periodicity stems from past cultural superstitions. In the past, menstruation has been thought of as superstitious and taboo.<ref name=":0" /> An example of this line of thought comes from the British Medical Journal, which discusses the question of whether or not a menstruating woman can contaminate or damage food by touching it. Many individuals believed in this superstition and reinforced it.<ref name=":0" />

[[Leta Hollingworth]] was born on the Nebraska frontier in 1886. When she was an adolescent, her mother died, which lead to rough household conditions within the family.<ref name=":1" /> Despite the difficulties, she did exceptionally well in school. This drove her to pursue her education further, which resulted in her graduating from University of Nebraska. While studying there, she met Harry Hollingworth and married him soon after.<ref name=":1" /> She began teaching while her husband enrolled in a doctoral program at Columbia University, but because she was married she could no longer teach in the state of New York. At the time, this was against the law.<ref name=":1" /> This barrier fueled Hollingworth's feminist activism. When she finally got the opportunity to enroll in the psychology program at Columbia University under [[Edward Thorndike]], she took it. At the end of the program, she decided to study functional periodicity for her dissertation research.<ref name=":1" />

Hollingworth wanted to investigate the idea of functional periodicity and its assumptions about women.<ref name=":1" /> She started by designing two separate studies. The first study that was designed tested men and women on a series of mental and motor tasks while the second study monitored only females over a 30-day period. Her results showed there was no significant difference in physical and emotional tasks when a woman is menstruating and when she is not.<ref name=":1" />

== The influence of Leta Hollingworth ==

Leta Hollingworth was a clinical psychologist and feminist activist who conducted psychological research on several theories involving women. Some of these theories included the [[variability hypothesis]] and functional periodicity. She was quite instrumental in disproving the theory of functional periodicity, which was widely believed to be true by scientists as well as the general public.

For her dissertation research at Columbia Teacher's College, Hollingworth decided to conduct an investigation on functional periodicity, which she considered to be an unfair assumption about women that lacked any scientific grounds to justify itself.<ref name=":1" /> Interestingly enough, Hollingworth completed her dissertation under the supervision of the psychologist Edward Thorndike, a major proponent of the variability hypothesis. Though they held conflicting views, Hollingworth thanked him for aiding her. Hollingworth saw it as her duty as a feminist to prove through scientific study that women were equally as capable and intelligent as men in all of their pursuits, even during menstruation. Her dissertation included three studies dealing with functional periodicity among women, two of which were intensive  and one which was extensive. Her studies on functional periodicity helped to expand the view of menstruation and reduce bias towards women.

== Studies examining functional periodicity ==

=== Studies relating to motor deficits ===

Leta Hollingworth decided to use tests that had been used previously by psychologists to complete her study. She decided to use a familiar tapping test and steadiness test to assess motor ability.<ref>Green, C.D. (2000). Functional periodicity. ''Classics in the History of Psychology,'' 1-52.</ref> She had a total of 8 participants, including 6 women and 2 men. The participants ranged from 23–45 years old.

The tapping test was administered by having each participant tap a brass rod that was connected to a brass plate 400 times with their right hand to record maximal speed. It should be noted that Stanley Hall approved of this apparatus and stated how important it was for measuring muscle control. This is important because G.S. Hall is one of the leading male figures who stated, "Women... can make less accurate and energetic movements, and the mental activities are less brilliant."

After the data was analyzed, the researchers noted that there is no data from this study that suggests that women are experiencing more fatigue, less will power, and diminished energy during their menstrual cycle. Fatigue from the tests occurred similarly for men and women. The first 200 taps were faster compared to the last 200 taps.

The steadiness test was measured by having each participant hold a brass rod 2.5&nbsp;mm in diameter at arms length. This rod was in a hole 6&nbsp;mm in diameter. While standing they were asked to hold it for 30 seconds and make the least amount of contact with the hole. Each contact was measured by an electric counter. Due to high variability of the averages the data was deemed unreliable. This was proved by the fact that outside or external forces could affect the outcome greatly. Examples of this would be if the participant coughed, took a breath or got startled by a noise.

=== Studies relating to learning deficits ===

Not only did Leta Hollingworth study motor ability, she also sought to study mental ability as well. This was done by using two specific tests, color naming and saying opposites. Color naming was observed by having a card face down laid in front of the participant.<ref name=":1" /> Each participant was to name the color on the card as quickly as possible. The opposites test used a list of 50 words to test for mental ability as well. The words were presented to the individual in two columns and the words were typed. The participants went through the list as quickly as possible, naming the opposite to each word.

Lastly, she decided to do one more experiment involving 17 females. This study was to observe steadiness, tapping, and the opposites test. In this study, ages ranged from 20 to 40 years old. This extensive experiment was much the same as the intensive experiments involving the 8 participants. They were conducted every 3rd day for 30 days. Two trials were administered at every sitting to help with reliability. The experiment provided results that were very similar to her prior intensive experiment.

== References ==

{{reflist}}

<!-- Just press the "Save page" button below without changing anything! Doing so will submit your article submission for review. Once you have saved this page you will find a new yellow 'Review waiting' box at the bottom of your submission page. If you have submitted your page previously, either the old pink 'Submission declined' template or the old grey 'Draft' template will still appear at the top of your submission page, but you should ignore it. Again, please don't change anything in this text box. Just press the "Save page" button below. -->

[[Category:Psychological theories]]
[[Category:Menstrual cycle]]