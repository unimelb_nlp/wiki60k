{{Use dmy dates|date=April 2012}}
{{Infobox UN resolution
|number = 1524
|organ = SC
|date = 30 January
|year = 2004
|meeting = 4,906
|code = S/RES/1524
|document = http://undocs.org/S/RES/1524(2004)
|for = 15
|abstention = 0
|against = 0
|subject = The situation in Georgia
|result = Adopted 
|image = Tavisupleba square. Monument of St. George.jpg
|caption = [[Freedom Square, Tbilisi|Freedom Square]] in the Georgian capital [[Tblisi]]
}}

'''United Nations Security Council resolution 1524''', adopted unanimously on 30 January 2004, after reaffirming all [[United Nations Security Council resolution|resolutions]] on [[Abkhazia]] and [[Georgia (country)|Georgia]], particularly [[United Nations Security Council Resolution 1494|Resolution 1494]] (2003), the Council extended the mandate of the [[United Nations Observer Mission in Georgia]] (UNOMIG) until 31 July 2004.<ref>{{cite news|url=http://www.un.org/News/Press/docs/2004/sc7993.doc.htm|title=Security Council extends Georgia mission until 31 July|date=30 January 2004|publisher=United Nations}}</ref>

==Resolution==
===Observations===
In the preamble of the resolution, the Security Council stressed that the lack of progress on a settlement between the two parties was unacceptable. It condemned the shooting down of an UNOMIG helicopter in October 2001 which resulted in nine deaths and deplored that the perpetrators of the attack had not been identified. The contributions of UNOMIG and [[Commonwealth of Independent States]] (CIS) peacekeeping forces in the region were welcomed, in addition to the United Nations-led peace process. A [[Georgian presidential election, 2004|presidential election]] was held in Georgia in January 2004 and the new leadership was encouraged to pursue a settlement.

===Acts===
The Security Council welcomed political efforts to resolve the situation, in particular the "Basic Principles for the Distribution of Competences between [[Tbilisi]] and [[Sukhumi]]" to facilitate negotiations between Georgia and Abkhazia. It regretted the lack of progress on political status negotiations and the refusal of Abkhazia to discuss the document, further calling on both sides to overcome their mutual mistrust.<ref name=unnc>{{cite news|title=Georgia: Security Council extends UN mission through July|url=http://www.un.org/apps/news/story.asp?NewsID=9623&Cr=georgia&Cr1=|date=30 January 2004|agency=United Nations News Centre}}</ref> All violations of the 1994 [[Agreement on a Cease-fire and Separation of Forces]] were condemned. The Council also welcomed the easing of tensions in the [[Kodori Valley]] and the signing of a protocol by both parties on 2 April 2002. Concerns of the civilian population were noted and the Georgian side was asked to guarantee the safety of UNOMIG and CIS troops in the valley. 

The resolution urged the two parties to revitalise the peace process, including greater participation on issues relating to [[refugee]]s, [[internally displaced person]]s, economic co-operation and political and security matters. It also reaffirmed the unacceptability of demographic changes resulting from the conflict. Abkhazia in particular was called upon to improve law enforcement, address the lack of instruction to ethnic Georgians in their [[first language]] and ensure the safety of returning refugees.<ref name=unnc/> 

The Council called again on both parties to take measures to identify those responsible for the shooting down of an UNOMIG helicopter in October 2001. Both parties were also asked to dissociate themselves from military rhetoric and demonstrations in support of illegal armed groups, and ensure the safety of United Nations personnel. Furthermore, there were concerns about the security situation in [[Gali District, Abkhazia|Gali region]] with repeated killings and there were also abductions of UNOMIG and CIS peacekeeping personnel, which the Council condemned.<ref name=unnc/>

==See also==
* [[Georgian–Abkhazian conflict]]
* [[List of United Nations Security Council Resolutions 1501 to 1600]] (2003–2005)
* [[United Nations resolutions on Abkhazia]]

==References==
{{reflist|colwidth=30em}}

==External links==
*[http://undocs.org/S/RES/1524(2004) Text of the Resolution at undocs.org] 
{{wikisource}}

{{UNSCR 2004}}

[[Category:2004 United Nations Security Council resolutions]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:2004 in Georgia (country)]]
[[Category:2004 in Abkhazia]]
[[Category:United Nations Security Council resolutions concerning Georgia (country)]]
[[Category:United Nations Security Council resolutions concerning Abkhazia]]
[[Category:January 2004 events]]