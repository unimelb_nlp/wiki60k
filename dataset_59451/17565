{{Infobox company
| name             = Displays2go
| logo             =Displays2Gologo.png
| type             = [[Subsidiary]] of [[:de:TAKKT AG|TAKKT]]
| traded_as        =
| genre            = <!-- Only used with media and publishing companies -->
| fate             =
| predecessor      =
| successor        =
| foundation       = {{Start date|1974}}
| founder         = George Patton
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[Bristol, Rhode Island]]
| location_country = United States
| location         =
| locations        =
| area_served      =
| key_people       =
| industry         = [[Retail]] , [[Trade Show]] and [[Point of sale display|Display]]
| products         = Display fixtures
| services         =
| revenue          =
| operating_income =
| net_income       =
| aum              = <!-- Only used with financial services companies -->
| assets           =
| equity           =
| owner            =
| num_employees    = 260
| parent           = [[:de:TAKKT AG|TAKKT]]
| divisions        =
| subsid           =
| homepage         = {{URL|www.displays2go.com}}
| footnotes        =
| intl             =
}}
'''Displays2go''' is a manufacturer and [[e-commerce]] retailer of [[Point of sale display|point-of-sale displays]]<ref>{{cite web|title=Displays2Go BBB Page|url=http://www.bbb.org/boston/Business-Reviews/display-designers-and-producers/displays2go-com-in-bristol-ri-107575|website=bbb.org|accessdate=18 June 2014}}</ref> based in [[Bristol, RI]].<ref>{{cite web|title=Mapquest Location|url=http://www.mapquest.com/maps?name=Displays2go&address=55%20Broadcommon%20Rd&city=Bristol&state=RI&zipcode=02809|website=mapquest.com|accessdate=19 June 2014}}</ref>

A subsidiary of [[:de:Takkt|TAKKT AG]], this online [[business-to-business]] [[wholesaler]] markets its products mainly to retail stores and trade shows. The company adopted its current name in 1996 when it transitioned from a local acrylic [[Trade promotion (marketing)#In-store displays|display]] manufacturer to an online merchant,<ref>{{cite web|title=Google Economic Report|url=https://www.google.com/economicimpact/reports/ri.html |website=Google.com|accessdate=12 May 2015}}</ref> expanding its product line to include store fixtures, trade show supplies, and [[retail media]].

== History ==

=== 1974–2011 ===

Founded in 1974 by George Patton, the company was originally an in-house display manufacturer known as George Patton Associates, Inc.<ref>{{cite web|title=George Patton Associates, Inc.|url=https://www.bloomberg.com/research/stocks/private/snapshot.asp?privcapId=6941266|website=bloomberg.com|accessdate=12 May 2015}}</ref> Targeting mainly New England area businesses, GPA expanded rapidly due to Patton's persistent marketing efforts.<ref>{{cite web|title=Company History|url=http://www.displays2go.com/S-3721/company-history|website=displays2go.com|accessdate=18 June 2014}}</ref> In 1992, he sold the company to his sons, Tom and Chris.<ref>{{cite news|last1=Mello|first1=Bob|title=Local firm to purchase Tourister Building|url=https://secure.pqarchiver.com/projo/doc/397276460.html?FMT=FT&FMTS=ABS:FT&type=current&date=Dec+30%2C+1997&author=BOB+MELLO+Journal-Bulletin+Staff+Writer&pub=Providence+Journal+-+Bulletin&desc=Local+firm+to+purchase+Tourister+building+%2AGeorge+Patton+Associates+plans+to+redevelop+the+building+for+light+industrial+use+and+possibly+some+manufacturing|accessdate=18 June 2014|publisher=Providence Journal|date=30 Dec 1997}}</ref>
[[File:Fall River Displays2Go Building.jpg|thumbnail|right|Displays2Go's Fall River Location]]
Their [[Catalog merchant|catalog business]] quickly grew, forcing the company to acquire a larger warehouse. In 1997, plans were made to build a new 45,000 sq. ft plant near the company's location on Market St. in [[Warren, RI]], but Warren's water main expansion fell 1600 feet short of where the Pattons planned to build the new warehouse, preventing its construction.<ref>{{cite news|last1=Mello|first1=Bob|title=Firm's expansion at industrial park seems unlikely|url=https://secure.pqarchiver.com/projo/doc/397270896.html?FMT=FT&FMTS=ABS:FT&type=current&date=Sep+2%2C+1997&author=BOB+MELLO+Journal-Bulletin+Staff+Writer&pub=Providence+Journal+-+Bulletin&desc=Firm%27s+expansion+at+industrial+park+appears+unlikely+%2AA+businessman+had+said+his+plan+hinges+on+expansion+of+a+water+main+to+the+park+that+was+recently+approved+by+the+water+board|accessdate=18 June 2014|publisher=Providence Journal|date=2 Sep 1997}}</ref> Later that year, a [[Real estate contract|purchase and sales agreement]] was signed on a second location in Warren for an estimated $1.4 million, but it fell through for undisclosed reasons.<ref>{{cite news|last1=Mello|first1=Bob|title=Local firm to purchase Tourister Building|url=https://secure.pqarchiver.com/projo/doc/397276460.html?FMT=FT&FMTS=ABS:FT&type=current&date=Dec+30%2C+1997&author=BOB+MELLO+Journal-Bulletin+Staff+Writer&pub=Providence+Journal+-+Bulletin&desc=Local+firm+to+purchase+Tourister+building+%2AGeorge+Patton+Associates+plans+to+redevelop+the+building+for+light+industrial+use+and+possibly+some+manufacturing|accessdate=18 June 2014|publisher=Providence Journal|date=30 Dec 1997}}</ref> In 2000, GPA started building a 261,000 sq. ft warehouse and office building in [[Bristol, RI]], its present headquarters.<ref>{{cite news|last1=Nitze|first1=Sam|title=Companies snapping up industrial park plots|url=https://secure.pqarchiver.com/projo/doc/397351405.html?FMT=FT&FMTS=ABS:FT&type=current&date=Feb+22%2C+2000&author=SAM+NITZE+Journal+Staff+Writer&pub=The+Providence+Journal&desc=Companies+snapping+up+industrial+park+plots|accessdate=18 June 2014|publisher=Providence Journal|date=22 Feb 2000}}</ref>
The company began focusing on Internet marketing and was renamed to Displays2Go.

=== 2011–present ===

In 2012, Displays2go was acquired by the TAKKT Group (ETR: TTK), a subsidiary of the German conglomerate [[Franz Haniel & Cie.]], for $120 million. Felix Zimmerman, TAKKT Group's CEO, later on explained that the acquisition was made to "further diversify [the group's] product portfolio and noticeably extend [its] e-commerce competence."<ref>{{cite web|title=Acquisition of leading US direct marketing company for display articles|url=http://www.reuters.com/article/2012/03/22/idUS57304+22-Mar-2012+HUG20120322|website=reuters.com|publisher=TAKKT|format=Press release|date=22 March 2012|accessdate=18 June 2014}}</ref>

== References ==

{{reflist}}

[[Category:1974 establishments in the United States]]
[[Category:Commerce websites]]
[[Category:Companies based in Rhode Island]]
[[Category:Corporate subsidiaries]]
