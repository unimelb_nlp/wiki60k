<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Br 905 Fauvette
 | image=Breguet Br 905 Fauvette.png
 | caption=The first prototype, probably at the 1958 WGC, [[Leźno]]
}}{{Infobox Aircraft Type
 | type=Single-seat competition [[sailplane]]
 | national origin=[[France]]
 | manufacturer=Société des Ateliers d'Aviation Louis Breguet ([[Breguet Aviation]])
 | designer=Jean Cayla
 | first flight=15 April 1958
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=50
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Breguet Br 905 Fauvette''' ({{lang-en|Warbler}}) is a single-seat, [[Glider competition classes#Standard Class|standard class]], competition [[sailplane]], designed and produced in [[France]] from the late 1950s.  Some 50 were built but most remained grounded after a structural accident in 1969; a few remain airworthy.

==Design and development==
Following Breguet's success in the 1954 and 1956 [[World Gliding Championships]] with the [[Breguet Br 901 Mouette|Type&nbsp;901]], Jean Cayla designed the Type&nbsp;905 for the 1958 event. It is a Standard Class sailplane with a 15&nbsp;m (49&nbsp;ft&nbsp;3in) span. Like its predecessor, the 905 is a [[cantilever]] [[monoplane#Types|mid-wing monoplane]] but the its structure contains [[glass reinforced plastic]], more [[plastic foam]] and less [[aircraft fabric covering|fabric]]. It also has a [[V-tail|butterfly tail]]. It has a wing of straight tapered planform, terminated with small "salmon" fairings at the squared-off [[wingtips]].  The major structural component is the main spar plus nose D-box unit, skinned with a plastic foam-filled ("Klegecel") sandwich with 0.6&nbsp;mm (0.024&nbsp;in)-ply outer layers. Ribs, [[ailerons]] and [[Schempp-Hirth]] [[air brake (aircraft)|airbrakes]] are attached to this torsion box.  The whole upper wing surface and outboard lower surface is ply, supported by an internal Klegecell lining, with fabric below, aft of the spar.  Slotted ailerons occupy the outer 45% of the [[trailing edge]]; there are no [[flaps (aircraft)|flaps]].  Each complete wing weighs just 34&nbsp;kg (75&nbsp;lb).<ref name=SimonsII>{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=205–7}}</ref><ref name=JAWA62>{{cite book |title= Jane's All the World's Aircraft 1962-63|last= Taylor |first= John W R |edition= |year=1962|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|page=321}}</ref>

The Fauvettes's [[fuselage]] is built in three parts.  The nose section, with a moulded plastic foam shell over a steel frame contains the cockpit, which is covered by a high, one-piece [[canopy (aircraft)|canopy]] over the upright seating position, giving the Fauvette a somewhat humpbacked look.  The centre section also has a steel frame, covered by moulded polystyrene; wings, cockpit and twin fuselage side towing hooks are attached to this frame. Behind the cockpit the upper fuselage line is formed with a polystyrene [[fairing (aircraft)|fairing]] which overlaps the conical rear fuselage, made of ply-foam sandwich.  The V-tail is straight-tapered with sweep on both edges.  The fixed surfaces are ply-foam sandwich structures, carrying fabric covered control surfaces.  The Fauvette has a fixed, [[Landing gear#Gliders|monowheel undercarriage]], assisted by a tailskid.<ref name=JAWA62/>

The Type 905 Fauvette flew for the first time on 15 April 1958.<ref name=Flight>{{cite magazine |last= |first= |authorlink= |date=25 April 1958  |title= Club and gliding news|magazine= [[Flight International|Flight]]|volume=73 |issue=2070 |page=587 |id= |url= http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200571.html }}</ref>

==Breguet Bre 906 Choucas==
Breguet also designed and built a two-seat version of the Fauvette, the 906 Choucas ({{lang-en|Jackdaw}}). The Choucas, which first flew on 26 October 1959, was larger and heavier than the Fauvette with an 18&nbsp;m (59&nbsp;ft&nbsp;7&nbsp;in) span, a length of 8&nbsp;m (26&nbsp;ft&nbsp;6&nbsp;in) and an empty weight of 245&nbsp;kg (540&nbsp;lb). In 1962 they had plans for a production run of one hundred but gained no orders, so only one was built.<ref name=SimonsII/><ref name=JAWA62/>

==Operational history==

The Br 905 competed in the 1958 World Gliding Championship at [[Leźno]] in [[Poland]] as  was intended, though it failed to repeat the success of the earlier Breguet, coming in 9th out of 24 in the Standard Class. It was piloted by Camille Lebar. The Fauvette was well received by those who flew it, reporting light controls, good aileron response and general good behaviour.<ref name=JAWA62/>

Breguet set up a batch production line for fifty aircraft, all of which had been delivered to customers in several European countries and in North America by the end of January 1961.<ref name=JAWA62/> The Fauvette was available in both flyaway form or as a kit. Some were in private hands by 1959: for example, on 12 June 1959 Tony Goodhart set a new British National Distance record of 617&nbsp;km (343&nbsp;mi) in his Fauvette, the 5th, preproduction, aircraft.  He also participated in the Italian National Championships in it.<ref name=S&G1>{{cite web |url=http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Gliding%201955-1960/Volume%2010%20No%205%20Oct%201959.pdf|title=Goodright's Fauvette|pages=262, 265, 273 |accessdate=19 April 2012}}</ref>

On 11 August 1969 a Fauvette under airtow shed its tail unit, killing the pilot.  Investigators found that the bonding between the fuselage and tail unit had failed and the type was grounded.  Though a modification involving metal and wood straps to reinforce the bonding was devised, most Fauvettes never flew again. The strengthening added 32&nbsp;kg (82&nbsp;lb) to the weight.<ref name=SimonsII/> Of the minority which were modified, some are still registered; in 2010 five Fauvettes remained on the mainland European civil aircraft registers<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0}}</ref> and there were another two in the UK in 2012.<ref name=CAA>{{cite web |url=http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=breguet%20905|title=CAA register - Breguet 905 |author= |date= |work= |publisher= |accessdate=19 April 2012}}</ref>

==Variants==
''Data from'' Breguet production<ref name=Prod>{{cite web |url=http://www.airport-data.com/manuf/Breguet:2.html|title=Breguet production|author= |date= |work= |publisher= |accessdate=19 April 2012}}</ref>
;905: One Prototype.
;905PS: Preproduction aircraft. three built.
;905S: Production aircraft. 42 built as flyaway or kit.
;905SA: Three built.
;905BM: One built.
;906 Choucas: Larger, two-seat version. One built.

==Specifications (Fauvette / Choucas)==
{{Aircraft specs
|ref=''Jane's All the World's Aircraft 1962/63''<ref name=JAWA62/> The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II|year=1963|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages= 90–93|edition=1st|author2=K.G. Wilkinson|language=English, French, German}}</ref>
|prime units?=met
<!--        General characteristics
-->
|genhide=

|crew=
|capacity=one
|length m=6.30
|length note=('''905 Fauvette''')<br/> 
:::{{convert|7.9|m|ft|abbr=on|1}} ('''906 Choucas''')
|span m=15.0
|span note=('''905 Fauvette''')<br/> 
:::{{convert|18|m|ft|abbr=on|1}} ('''906 Choucas''')
|aspect ratio=20 ('''905 Fauvette''')<br/> 
:::19 ('''906 Choucas''')
|airfoil='''Root''' [[NACA airfoil#5-digit series|NACA 63420]], '''Tip''' [[NACA airfoil#5-digit series|NACA 63613]]
:::('''906 Choucas''') '''Root''' [[NACA airfoil#5-digit series|NACA 63820]], '''Tip''' [[NACA airfoil#5-digit series|NACA 63013]]
|empty weight kg=148
|empty weight note=('''905 Fauvette''')<br/> 
:::{{convert|267|kg|lb|abbr=on|1}} ('''906 Choucas''')
|gross weight kg=230
|gross weight note=
|max takeoff weight kg=275
|max takeoff weight note=('''905 Fauvette''')<br/> 
:::{{convert|460|kg|lb|abbr=on|1}} ('''906 Choucas''')
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=54
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=('''905 Fauvette''')<br/> 
:::{{convert|58|km/h|mph kn|abbr=on|0}} ('''906 Choucas''')
|never exceed speed kmh=200
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|170|km/h|mph kn|abbr=on|0}}
*'''Aerotow speed:''' {{convert|120|km/h|mph kn|abbr=on|0}}
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+5.33 -2.13 at {{convert|231|km/h|mph kn|abbr=on|0}}
|roll rate=<!-- aerobatic -->
|glide ratio= 30 at {{convert|78|km/h|mph kn|abbr=on|0}} ('''905 Fauvette''')<br/> 
::: 31 at {{convert|82|km/h|mph kn|abbr=on|0}} ('''906 Choucas''')
|sink rate ms=0.6
|sink rate note= minimum at {{convert|65|km/h|mph kn|abbr=on|0}}('''905 Fauvette''')<br/> 
:::{{convert|0.7|m/s|ft/min|abbr=on|0}} at {{convert|70|km/h|mph kn|abbr=on|0}} ('''906 Choucas''')
|lift to drag=
|wing loading kg/m2=24.5
|wing loading lb/sqft=
|wing loading note=('''905 Fauvette''')<br/> 
:::27 kg/m² (5.53 lb/sqft) ('''906 Choucas''')
|more performance=
}}

==See also==
{{Portal|Aviation}}
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<br/>
* [[Breguet 902]]
* [[Breguet 904 Nymphale]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|guet Br 905 Fauvette}}
{{refbegin}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II|year=1963|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages= 90–93|edition=1st|author2=K.G. Wilkinson|language=English, French, German}}
*{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=205–7}}
*{{cite book |title= Jane's All the World's Aircraft 1962-63|last= Taylor |first= John W R |edition= |year=1962|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|page=321}}
*{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0}}
*{{cite magazine |last= |first= |authorlink= |date=25 April 1958  |title= Club and gliding news|magazine= [[Flight International|Flight]]|volume=73 |issue=2070 |page=587 |id= |url= http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200571.html }}
*{{cite web |url=http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=breguet%20905|title=CAA register - Breguet 905 |author= |date= |work= |publisher= |accessdate=19 April 2012}}
*{{cite web |url=http://www.airport-data.com/manuf/Breguet:2.html|title=Breguet production|author= |date= |work= |publisher= |accessdate=19 April 2012}}
*{{cite web |url=http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Gliding%201955-1960/Volume%2010%20No%205%20Oct%201959.pdf|title=Goodright's Fauvette|pages=262, 265, 273 |accessdate=19 April 2012}}

{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->
{{Breguet aircraft}}

{{DEFAULTSORT:Breguet Br 905 Fauvette}}
[[Category:French sailplanes 1950–1959]]
[[Category:Breguet aircraft|905 Fauvette]]