{{Use British English|date=July 2015}}
{{About|the Royal Navy officer|Sir Christopher Clayton, industrialist and politician|Christopher Clayton (businessman)}}
{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name=Christopher Hugh Trevor Clayton
|image=
|caption=
|birth_date=
|death_date=
|birth_place=
|death_place=
|placeofburial=
|placeofburial_label=
|nickname=
|allegiance={{flagicon|GBR}} [[United Kingdom]]
|branch=[[File:Naval Ensign of the United Kingdom.svg|23px]] [[Royal Navy]]
|serviceyears=1972-2007
|rank=[[Rear Admiral]]
|unit=
|commands=[[HMS Chatham (F87)|HMS Chatham]] (1995)<br/>Commodore Naval&nbsp;Aviation (2000)<br/>[[HMS Ocean (L12)|HMS Ocean]] (2003)
|battles=[[Falklands War]]<br/>[[2003 invasion of Iraq]]
|awards=[[Mention in Dispatches]]
|relations=
|laterwork=
}}

[[Rear Admiral]] '''Christopher Hugh Trevor Clayton''' is a former [[Royal Navy]] officer who served as a [[Westland Lynx|Lynx helicopter]] pilot during the [[Falklands War]]. He went on to become a senior naval officer, commanding ships during the [[Hong Kong handover ceremony]] and [[2003 invasion of Iraq]] and later serving high-level positions in [[NATO]].<!-- Expand -->

== Early career and Falklands War ==

Clayton was educated at [[St John's School, Leatherhead]].<ref>{{cite web|url=http://www.ukwhoswho.com|title=Who's Who|publisher=}}</ref> After school he joined the Royal Navy as an [[aviator]]<!-- research --> and was appointed an acting [[sub-lieutenant]] on 29 February 1972. After flying training he was then commissioned as a sub-lieutenant on 29 November 1973.<ref>{{London Gazette|issue=46138 |date=1973-11-26 |startpage=14082|supp=yes |accessdate=2008-08-20}}</ref> After being promoted to [[lieutenant]] on 16 October 1974,<ref>{{London Gazette|issue=46318 |date=1974-06-11 |startpage=6864 |supp=yes |accessdate=2008-08-20}}</ref><ref>{{London Gazette|issue=48229 |date=1980-06-23 |startpage=8994 |supp=yes |accessdate=2008-08-20}}</ref> Clayton was selected for a Full Career Commission in 1980,{{Citation needed|date=August 2008}} transferring to the General List.

On 2 April 1982, the disputed [[British overseas territory]] of the [[Falkland Islands]] was invaded by neighbouring Argentina.<ref name="BBC introduction">{{cite news|url=http://news.bbc.co.uk/1/shared/spl/hi/guides/457000/457033/html/default.stm|archiveurl=https://web.archive.org/web/20080308095608/http://news.bbc.co.uk/2/shared/spl/hi/guides/457000/457033/html/default.stm|archivedate=8 March 2008 |title=Key facts: The Falklands War, Introduction|publisher=[[BBC News]]|accessdate=7 March 2008}}</ref> The United Kingdom, nearly {{convert|8000|mi|km}} away, assembled and dispatched a naval [[Events leading to the Falklands War#Task force|task force]] of 28,000&nbsp;troops to recapture the islands.<ref name="BBC introduction"/><ref>{{cite news|url=http://news.bbc.co.uk/1/shared/spl/hi/guides/457000/457033/html/nn2page1.stm|archiveurl=https://web.archive.org/web/20071007112348/http://news.bbc.co.uk/1/shared/spl/hi/guides/457000/457033/html/nn2page1.stm|archivedate=7 October 2007 |title=Key facts: The Falklands War, Task Force|publisher=[[BBC News]]|accessdate=7 October 2007}}</ref> The conflict ended that June with the [[Falklands War Argentine surrender|surrender of the Argentine forces]]; the battles fought on land, at sea, and in the air had cost the lives of some 900 British and Argentine servicemen.<ref name="BBC introduction"/>

Clayton served on {{HMS|Cardiff|D108|6}}, piloting their H[[Anti-submarine warfare|AS]].3 Lynx helicopter, serial no.&nbsp;335 or "IVOR", as part of [[815 Naval Air Squadron]]. ''Cardiff'' arrived at the islands late in the conflict on 26 May.<ref name="Report of Proceedings">{{cite web|url=http://hmscardiff.co.uk/rop.aspx |title=Report of Proceedings |publisher=HMS ''Cardiff''—The 1982 Ship's Company |accessdate=2008-02-12 |deadurl=yes |archiveurl=https://web.archive.org/web/20080526055504/http://hmscardiff.co.uk:80/rop.aspx |archivedate=26 May 2008 |df=dmy }}</ref> ''Cardiff''’s primary role was to form part of the anti-aircraft warfare [[Radar picket|picket]], using her anti-air [[Sea Dart missile|Sea Dart]] missiles to protect British ships and attempting to ambush Argentine re-supply aircraft. She was also required to fire at enemy positions with her [[4.5 inch (114 mm) Mark 8 naval gun|4.5-inch gun]].<ref>{{cite web|url=http://www.navynews.co.uk/falklands/stories/8202060201.asp |title=2 June 1982 |publisher=[[Navy News]] |accessdate=2008-04-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20041225190350/http://www.navynews.co.uk/falklands/stories/8202060201.asp |archivedate=25 December 2004 |df=dmy }}</ref>

[[File:Lynx 335 HMS Cardiff March 1982.jpg||thumb|Clayton piloting ''Cardiff'''s [[Westland Lynx|Lynx]] helicopter]]
On 13 June, around midday, Clayton was performing the routine forenoon clearance search of the area south of the [[Falkland Sound]].<ref>{{cite web |url=http://www.navynews.co.uk/falklands/stories/8202061301.asp |title=13th June 1982 |publisher=www.navynews.co.uk |accessdate=2010-02-04 }}</ref> Two Argentine [[IAI Nesher|Daggers]] of ''Gaucho'' flight{{#tag:ref|''[[Gaucho]]'' flight originally consisted of three Daggers, Lt Aguirre's aircraft had to remain at the [[Río Gallegos, Santa Cruz|Rio Gallegas]] airbase due to a brake failure.<ref name="13 de Junio">{{cite web |url=http://www.fuerzaaerea.mil.ar/conflicto/dias/jun13.html |title=13 de Junio |publisher=[[Argentine Air Force]]|language=Spanish |accessdate=2008-04-13}}</ref>|group="nb"}} spotted Clayton's Lynx and jettisoned their external [[Drop tank|fuel tanks]] in preparation to engage.{{#tag:ref|A common practice to reduce [[Drag (physics)|drag]] and weight in combat situations.<ref>{{cite web |url=http://www.luke.af.mil/shared/media/document/AFD-070119-105.pdf |title=Chapter 1 of BMGR INRMP|date=March 2006 |publisher=www.luke.af.mil |page=XXV (25) |format=PDF |accessdate=2010-02-05 }}</ref>|group="nb"}} They began strafing the helicopter with their cannons, but Clayton evaded the attacks and managed to escape. The Daggers returned home empty-handed, their original mission had been to attack British positions on [[Mount Longdon]] with [[retarded bomb]]s.<ref name="13 de Junio"/> After the initial [[Argentine surrender in the Falklands War|Argentine surrender]], Clayton flew the [[Officer Commanding|OC]] of [[40 Commando]], [[Lieutenant Colonel (United Kingdom)|Lt Col]] Malcolm Hunt, to [[Port Howard]] to accept the surrender of the Argentine garrison stationed there. In recognition of his service during the war, Clayton was awarded a [[Mention in Dispatches]].<ref>{{London Gazette|issue=49134 |date=1982-10-08 |startpage=12 |supp=yes |accessdate=2008-04-18}}</ref>

== Commands ==

Clayton was promoted to [[commander]] on 31 December 1988,<ref>{{London Gazette|issue=51609|supp=yes|startpage=325|date=9 January 1989|accessdate=2008-08-20}}</ref> then to a [[captain (Royal Navy)|captain]] on 31 December 1995.<ref>{{London Gazette|issue=54265|supp=yes|startpage=59|date=29 December 1995|accessdate=2008-08-20}}</ref> Clayton commanded the Type 22 frigate {{HMS|Chatham|F87|6}} during the 1997 [[Hong Kong handover ceremony]]. Hong Kong had been a British [[British overseas territories|colony]] since 1841, but Britain's lease was due to finish and control was to be handed to the [[People's Republic of China]]. ''Chatham'''s role was to act as [[guardship]] for the royal yacht [[HMY Britannia|HMY ''Britannia'']],<ref>{{cite web |url=http://www.navynews.co.uk/ships/chatham.asp# |title=HMS Chatham |publisher=www.navynews.co.uk |accessdate=2010-01-25 }}</ref> Clayton said of the experience; "There is no sense of withdrawal, this is very much one professional armed forces handing over the protection and sovereignty of Hong Kong to another, the [[People's Liberation Army]]. So I look on it as a classic military evolution and one which we hope to do with some style, orderly and professionally."<ref>{{cite web|url=https://turkishdailynews.com.tr/archives.php?id=3020 |title=UK Royal Navy warship takes up position in HK - Turkish Daily News Jun 03, 1997 |publisher=turkishdailynews.com.tr |accessdate=2008-08-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20060811025934/http://www.turkishdailynews.com.tr/archives.php?id=3020 |archivedate=11 August 2006 }}</ref>

He became the first officer to hold the post of "[[Commodore (Royal Navy)|Commodore]] Naval Aviation" (COMNA), he was based at [[Northwood Headquarters]] during this time.<ref>{{cite web|url=http://www.navynews.co.uk/articles/2000/0004/0000040602.asp|title=Admiral takes over as Harrier supremo|publisher=www.navynews.co.uk|accessdate=2008-08-26}}</ref> The post was created in 2000 to command the newly formed [[Joint Force Harrier]]. In 1998, the UK's recently elected [[Labour Party (UK)|labour]] government reassessed the country's defence needs and published their findings in a policy document titled the [[Strategic Defence Review]].<ref>{{cite web |url=http://www.mod.uk/DefenceInternet/AboutDefence/CorporatePublications/PolicyStrategyandPlanning/SDR/ |title=Ministry of Defence: Strategic Defence Review |publisher=www.mod.uk |accessdate=2010-02-24  }}</ref> One of its key initiatives was a call for the amalgamation of both Royal Navy and RAF Harriers into one force.<ref>{{cite web |url=http://www.raf.mod.uk/rafcottesmore/aboutus/ |title=RAF Cottesmore - About Us |publisher=www.raf.mod.uk |accessdate=2010-02-24 }}</ref> This would create a closer harmonization of common operating procedures and maintenance practice.<ref>{{cite web |url=http://www.mod.uk/NR/rdonlyres/65F3D7AC-4340-4119-93A2-20825848E50E/0/sdr1998_complete.pdf |title=Strategic Defence Review|date=July 1998 |publisher=www.mod.uk |page=119 |format=PDF |accessdate=2010-02-24 }}</ref>

Clayton commanded the helicopter carrier {{HMS|Ocean|L12|6}} during the [[2003 invasion of Iraq]]. ''Ocean'' was the UK's lead amphibious landing ship, providing sea-based logistics to the Royal Marine's [[3 Commando Brigade]] and acting as a launch pad for their attack on the [[Al-Faw Peninsula]].<ref>{{cite web |url=http://www.mod.uk/DefenceInternet/DefenceNews/EquipmentAndLogistics/HmsOceanInLineForMajorRefit.htm |title=HMS Ocean in line for major refit |publisher=www.mod.uk |accessdate=2010-01-27 }}</ref> Her helicopters also played a role in securing the city of [[Basra]].<ref name="HMS Ocean due home">{{cite news |url=http://news.bbc.co.uk/1/hi/england/devon/2939596.stm |title=HMS Ocean due home |publisher=news.bbc.co.uk |accessdate=2010-01-27 | date=2003-05-27}}</ref> When interviewed about the experience Clayton said; "There's <!--omitted "also" for flow --> a sense of pride in a job really well done. [Ocean] played a major role in the operations out there. But also we have to remember that we're the lucky ones coming back. Not everyone has. There are many we have to think about, including one of our ship's company who was killed in action, Marine Chris Maddison. Our thoughts will be about him and with his family."<ref name="HMS Ocean due home"/>

== Later career ==

Clayton became a [[rear admiral]] on 30 August 2004 and took a post at NATO's headquarters in [[Brussels]]. His role was as an assistant director, in charge of the [[Military intelligence|Intelligence]] Division of NATO's International Military Staff.<ref name="Senior appointments 2005">{{cite journal|url=http://www.rncom.mod.uk/uploadedFiles/RN/Reference_Library/broadsheet_16.pdf|archiveurl=https://web.archive.org/web/20061007181034/http://www.rncom.mod.uk/uploadedFiles/RN/Reference_Library/broadsheet_16.pdf|archivedate=2006-10-07 |format=PDF|title=Senior appointments 2005|publisher=Royal Navy|pages=33|accessdate=2006-10-07}}</ref><ref name="Navy List 2006"/> The Intelligence Division provides day-to-day strategic intelligence support to the [[Secretary General of NATO|Secretary General]]; it has no intelligence gathering capacity of its own and therefore relies on input from alliance members. On this basis it could be described as a "central coordinating body" for the collation, assessment and dissemination of intelligence within NATO Headquarters.<ref>{{cite web |url=http://www.nato.int/docu/handbook/2001/hb1103.htm |title=NATO Handbook: Military Organisation and Structures |publisher=www.nato.int |accessdate=2010-02-25 }}</ref> He retired on 29 December 2007.<ref>{{London Gazette|issue=58620|supp=yes |startpage=2915|date=26 February 2008|accessdate=2008-08-20}}</ref>

Over the course of his naval career, he attended the Advanced Command and Staff Course and the [[Higher Command and Staff Course]].<ref name="Navy List 2006">{{cite journal|url=http://www.rncom.mod.uk/uploadedFiles/RN/Reference_Library/e_navy_list_2006.pdf|archiveurl=https://web.archive.org/web/20080307043313/http://www.rncom.mod.uk/uploadedFiles/RN/Reference_Library/e_navy_list_2006.pdf|format=PDF|archivedate=2008-03-07 |title=The Navy list 2006|publisher=Royal Navy|accessdate=2008-03-07}}</ref><!-- lead -->

{{External media|image1= [http://www.royalnavy.mod.uk/server?show=ConMediaFile.28097&outputFormat=print Clayton in 2006] (far right)}}

== See also ==
*[[:File:Officers at Port Howard.JPG|Photo of Clayton during the Falklands War]]

== Notes ==
<references group="nb" />

==References==
{{Reflist|30em}}

{{DEFAULTSORT:Clayton, Christopher}}
[[Category:HMS Cardiff (D108)]]
[[Category:People educated at St John's School, Leatherhead]]
[[Category:Royal Navy admirals]]
[[Category:Royal Navy personnel of the Falklands War]]
[[Category:Living people]]
[[Category:Fleet Air Arm aviators]]
[[Category:Year of birth missing (living people)]]
[[Category:Royal Navy personnel of the Iraq War]]
[[Category:Helicopter pilots]]