{{Infobox person
| name          = John Blocki
| image         = John Blocki.jpg
| alt           = 
| caption       = 
| birth_name    = 
| birth_date    = {{Birth date|1845|06|15}}
| birth_place   = Hansewalde, Prussia
| death_date    = {{Death date and age|1934|05|07|1845|06|15}}
| death_place   = Chicago, Illinois, USA
| nationality   = 
| other_names   = 
| occupation    = perfumer
| years_active  =
| known_for     = flower-in-the-bottle floral waters and perfumes
| notable_works =
}}

'''John Blocki''' (15 June 1845 – 7 May 1934) was one of America's pioneer perfumers. His perfumes and cosmetics were widely sold and his unique presentation earned him a U.S. patent for perfumery packaging. He was well-known in the trade for his leadership and commitment to the advancement of the American perfume industry.

==Early life and family==
John Blocki was born on a feudal estate near [[Königsberg]], Prussia in 1845.<ref name="Leonard">Leonard, John William. ''The Book of Chicagoans: a biographical dictionary of leading living mean and women of the city of Chicago.'' Chicago: A.N. Marquis & Company 1917. Page 68</ref> His parents were Friedrich Wilhelm Blocki, a Prussian and Polish noble, and Emma (Doehling) of Pomerania.<ref name="Leonard"/> In 1850, the Friedrich Blockis with their six sons, two daughters, tutor, governess and several servants left the turbulence in Prussia for America.<ref name="Baker">Baker, Linda Blocki. "The Gale & Blocki Drug Store; History in a Perfume Bottle". ''Perfume Bottle Quarterly''. Spring 2006, volume 18, number 3, pages 5-7.</ref>

The voyage took six weeks and ended in a shipwreck off the shore of [[Sheboygan, Wisconsin]] where the family decided to settle. The only item salvaged from the shipwreck was a piano. It was the first piano in the state and people would come from miles around to hear Emma Blocki play on Sundays.

Blocki was educated in the public and private schools in Wisconsin and afterwards moved to Chicago where he lived the rest of his life. He married an English woman, Emma Leadbeater, in 1867. They had two children, Frederick William and Jeanette (a twin, Irene, died very young).<ref name="Leonard"/>

==Chemist and perfumer==
In 1859 when he was fourteen years old, Blocki apprenticed as a chemist with pharmacist F.A. Wheeler in Sheboygan.<ref>"Obituary of John Blocki". ''American Perfumer and Essential Oil Review''. May 1934, page 145.</ref> Blocki's older brother, William, had moved to Chicago to begin his pharmacy career as a clerk for Edwin Oscar Gale; the Gale drugstore was founded in 1847 making it one of the oldest apothecaries in Chicago.<ref>Gale, Edwin O. ''Reminiscences of Early Chicago and Vicinity''. 1902. Fleming H. Revell Company.</ref> William Blocki became a full partner in the business which changed its name to Gale & Blocki and grew to several stores including a prime location in the historic Palmer House Hotel (now the [[Palmer House Hilton]]).<ref>Andreas, Alfred Theodore. ''The History of Cook County, Illinois: From the Earliest Period to the Present Time''. 1884 A.T. Andreas. Google Book Search. Web. 24 July 2014.</ref>

Blocki moved to Chicago in 1862 and joined the drug firm of Fuller, French and Fuller; this was his first and only employer. On August 12, 1865, at the age of twenty, Blocki established his own retail and wholesale drug firm known as Blocki, Dietzsche & Co. The firm specialized in high quality chemicals, perfumes, essences and essential oils. The business was destroyed by the [[Great Chicago Fire]] of 1871 but quickly reopened at a new location in one of the few surviving business buildings in the city.<ref name="Notable Successes 1914"/>

Having accomplished himself as a chemist, Blocki sharpened his business skills by calling on large retailers in a wholesale capacity. From 1885 to 1895 he was appointed to represent the old New York house of Lehn & Fink in Chicago and vicinity.<ref name="Notable Successes 1914">"Notable Successes and How Achieved". ''The Practical Druggist and Pharmaceutical Review''. June 1914, volume 32, number 6, page 232.</ref> This endeavor was a success and Blocki became known as one of the best wholesale druggists in the country for his unusually strong ability to memorize market quotations.<ref>"An Interesting Sketch of Two Members of CVDA". ''NARD Journal''. 7 April 1910, volume 10, page 18. Google Book Search. Web. 26 July 2014.</ref>

Early in his career, Blocki began to specialize in perfumes and toilet waters.<ref>"Weekly Luncheon CVDA". ''NARD Journal''. 24 June 1920, volume 30, number 12, page 546. Google Book Search. Web. 26 July 2014.</ref> He gained a local reputation in Chicago for the excellence of his fragrance creations and opened a shop on Michigan Avenue for the exclusive sale of perfumery.<ref name="Notable Successes 1914"/> The perfume shop was connected to his laboratory. This was the first and only retail perfume store in Chicago at the time and eventually it had to be relocated to a larger space owing to its success.<ref>''American Druggist and Pharmaceutical Record''. Volume 43. 1903. Reprint. London: Forgotten Books, 2013. Page 293.</ref>

In 1895, Blocki and his son, Frederick, dropped the drug and chemical aspects of the business and focused entirely on creating and manufacturing perfumes and toiletries. The firm was named John Blocki & Son and it produced hundreds of fine perfumes and toiletries that were used by millions.<ref>"Honorable Frederick W. Blocki". ''The Broad Ax''. Chicago. 22 December 1917.</ref> The father-son partnership met an untimely and unexpected end in 1919 when Frederick Blocki died of pneumonia due to the influenza epidemic of 1918-1919.<ref name="obit">"Obituary Notes; Fred W. Blocki". ''The American Perfumer and Essential Oil Review''. March 1919, volume 14, number 1, page 19.</ref>

In addition to being a trained chemist and perfumer like his father, Frederick was a politician having served as Commissioner of Public Works and City Treasurer during Chicago's formative years. He was well respected for his management of city finances and for actions such as lending the city money from his own account to pay police and firemen before Christmas.<ref name="obit"/> Fred Blocki supervised some of the city's great public improvements, including construction of the first fixed trunnion bascule bridge in the United States, the [[Cortland Street Drawbridge]].<ref>"Frederick W. Blocki Democratic Candidate for Board of Review". ''The Sentinel''. 5 April 1912, volume 6, issue 1.</ref>

After Frederick Blocki's death, the firm was renamed John Blocki, Inc. and it continued to produce perfumes and toiletries for many years. Blocki died on May 7, 1934, at the age of eighty eight having been active in his business up to the end. He is buried in a family memorial room in the mausoleum at [[Rosehill Cemetery]]; a Victorian era cemetery that is the burial place of many well-known early Chicagoans. Blocki's daughter, Jeannette Peterson, operated the perfume company after his death but soon sold it to the Winter Group who eventually closed the business.<ref name="Baker"/>

==Industry advocate==
Blocki was an active advocate for the pharmaceutical industry and the burgeoning American perfume industry. He was a charter member and from 1909-1910 served as first vice president of the Manufacturing Perfumers' Association in New York (now the [[Personal Care Products Council]]).<ref>"Manufacturers see Bright Future for American Perfumery". ''The Pharmaceutical Era''. May 1910, volume 43, page 493. Google Book Search. Web. 24 July 2014.</ref> He was also a founding member and permanent corresponding secretary of the Chicago Veteran Druggists Association and the first president of the Perfumery, Soap and Extract Makers' Association of Chicago.<ref>"Perfumery, Soap and Extract Makers' Association of Chicago". ''Soap Gazette and Perfumer''. 1 January 1914, volume 16, number 1, page 15.</ref>

In the early 1900s, American perfumes began to increase in value and Blocki attributed this increase to progress made in the art by American perfumers.<ref>"Perfumes and Toilet Articles". ''The Western Druggist''. February 1905, volume 27, number 2, page 87.</ref> Well trained chemists were essential to this progress and Blocki was an early supporter of the [[UIC College of Pharmacy]] being an active member since 1867. He donated several perfumery trade items to the school's pharmaceutical museum: a musk pod, two containers of East Indian buffalo horn packed with civet as well as an essay on the use of musk and civet in perfume, and a small model of a copper still like the type used to distill rose oil in the [[Kazanlak]] district of Bulgaria.<ref>"Colleges of Pharmacy". ''The Practical Druggist and Pharmaceutical Review''. May 1920, volume 38, number 5, page 46. Google Book Search. Web. 24 July 2014.</ref> The copper still had been part of a perfumer exhibit at the [[St. Louis World's Fair]].<ref>"Meetings of the American Pharmaceutical Association Branches". ''The Druggists Circular''. January 1912, page 38.</ref>

Blocki's engaging character, dedication to the industry and involvement in numerous social clubs won him many loyal friends. The golden anniversary of his business was celebrated at a luncheon on August 12, 1915 with members of the Chicago Veteran Druggists Association and the local perfumery trade.<ref name="NARD">"News Happenings in Drugdom; Chicago Chatting". ''NARD Journal''. 26 August 1915, volume 20, number 21, page 1128. Google Book Search. Web. 24 July 2014.</ref> Blocki's friend and American League President [[Ban Johnson]] gave a eulogy. Among the many telegrams with good wishes was one from Hugo Kantrowitz of New York, the chairman of the American Pharmaceutical Association (now the [[American Pharmacists Association]]).<ref name="NARD"/>

Blocki's devotion to the Chicago Veteran Druggists Association was evident when he asked that a steamer being built in Philidephia to sail through the Panama Canal to California in February 1915 be named CVDA. Blocki had been selected to baptize the steamer due to his expertise in cracking perfume bottles for that purpose.<ref>"Bodemann's Aphorisms". ''The Practical Druggist and Pharmaceutical Review''. February 1915, volume 33, page 32.</ref> This was one of many voyages taken by Blocki; he was an avid traveler and spent many months at a time in Europe as well as trips to Cuba, South American and Minor Asia gaining inspiration and exploring new markets and materials for perfume.

==Presentation and patent==
In addition to being known for high quality fragrances, Blocki's perfumes and toiletries could be identified by their colorful and artistic presentations. His perfume boutique on Michigan Avenue was so large and had such a vast assortment of perfumes that it was referred to in the trade as a "perfume palace" that was worth a visit from any retailer.<ref>"Sketches of States, Cities and Mercantile Representatives". ''The Western Druggist''. October 1904, volume 26, number 10, page 551.</ref> Blocki frequently offered tours of his laboratory and showroom to visiting pharmacists and perfumers.

In 1907, Blocki obtained one of the first United States patents related to perfumery packaging: No. 840,105.<ref>Google Patent Search. Web. 26 July 2014.</ref> The patent claim involved placing a preserved natural flower of the same type as that of the odor of the perfume within the bottle. This was Blocki's solution to the problem of the loss or deterioration of perfume bottle labels as well as the evaporation of essential oils.

The resulting fragrance line was known as the Empress Floral Perfumes and Toilet Waters and also the Flower-in-the-Bottle perfumes. He created a vibrant holiday display for this line that included a colored felt mat, 2-foot tall etched display bottles, lithographed announcement cards and colored blotters.<ref>"This Spells Christmas". ''NARD Journal''. 7 December 1916, volume 23, number 10, page 434. Google Book Search. Web. 26 July 2014.</ref> He also created a sample set for this line that was in the form of a miniature book named "The Story of the Flower by Blocki" and contained four perfume samples each with a preserved flower.

The Flower-in-the-Bottle line bore Blocki's unique presentation style but he also produced several lines of perfume that followed the popular style of the times. His Regal and Bouquet lines of perfume that debuted in the early nineteen hundreds were typical of the Belle Epoque style. They were presented in classic flacons with cut glass stoppers and colorful, embossed labels depicting bold and beautiful women. Many of these perfumes were named after famous operas or historical figures; particularly Prussian and Polish noble families given his heritage.

Blocki also derived inspiration from his family and one of the more artistic results was a perfume named Psyche Rose. The fragrance was inspired by the [[White Rock Sparkling Water]] company's trademark [[Psyche (mythology)|Psyche]] logo; a nymph kneeling on an alabaster rock and admiring her reflection in pure spring water. White Rock was owned by Edwin Gale and Blocki's brother, William.<ref>http://www.whiterocking.org/gbhistory.html</ref> There are many famous stories about White Rock Sparking Water; one being that it was used by Charles Lindbergh to christen his plane the Spirit of St. Louis before its trans-Atlantic flight and another that it was served at the coronation of England's King Edward VII and was his preferred water for diluting wine.

In addition to perfumes, Blocki had an extensive line of toiletries and cosmetics that were sold under the Esprit d'Amour trade name in apothecaries and beauty shops throughout the country.<ref>"Trade Notes". ''The American Perfumer and Essential Oil Review''. March 1916, volume 11, number 1, page 25.</ref> He created a perfume with this name in 1916 along with scented sachets and powders. By the 1920s, this line grew to include night cream, face creams for different skin types, nail polish, shampoo and other cosmetics. The presentation was in dark blue tins with a floral design and gold accents.

==List of creations==
Blocki was one of America's pioneer perfumers and one of the largest manufacturers of perfumes and cosmetics of his time; his perfumes and toiletries were sold throughout the United States and abroad. A sample of Blocki perfumes includes <ref>''Ninth Annual Meeting of The Manufacturing Perfumers' Association of the United States''. New York. 1903. Page 195</ref><ref>''Trade Names of Perfumes and Toilet Articles''. The Manufacturing Perfumers' Association of the United States. Seventh Edition 1908, page 100-101</ref><ref>''Perfume Intelligence: The Encyclopedia of Perfume''. Volume B. Web. 4 September 2014.</ref>
{{Div col|colwidth=24em}}
* American Belle Rose 1903
* Blocki's Ideal 1908
* Bouquet de Vigne 1903
* Bouquet du Barry 1903
* Bouquet [[Helena Modjeska]] 1903
* Bouquet Kosciuszko 1903 (named for [[Tadeusz Kościuszko]])
* Bouquet Pulaski 1903
* Bouquet Sobieski 1903 (named for [[John III Sobieski]])
* Brazilian Lily 1903
* Colb's Veilchen 1903
* Empress Lily 1908
* Empress Rose 1908
* Empress Violet 1908
* Esprit d'Amour 1916
* Fairy Rose 1903
* Geisha Rose 1908
* Ilovit 1903
* Italian Violette 1903
* Naiad's Breath 1903
* Ollantay 1922
* Pearl of Persia 1903
* Psyche Rose 1903
* Queen Louise 1903
* Regal Frangipanni 1903
* Regal Heliotrope 1903
* Regal Peau d'Espagne 1903
* Sensation 1908
* Superba 1908
* Sweets of Araby 1908
* Thais 1911
* Thisbe 1908
* Uno 1908
{{div col end}}

==References==
{{reflist|32em}}

{{DEFAULTSORT:Blocki, John}}
[[Category:1845 births]]
[[Category:Perfumers]]
[[Category:1934 deaths]]