{{Infobox ice hockey player
| image= DanBain1900.jpg
| caption =
| alt = Upper body of a man with short hair and a thick moustache.  He is wearing what appears to be a fur coat over a suit.
| image_size= 225px
| position = [[Centre (ice hockey)|Centre]]
| shoots = Right
| height_ft = 6
| height_in = 0
| weight_lb = 185
| played_for = [[Winnipeg Victorias]]
| birth_date = {{birth date|1874|2|14|mf=y}}
| birth_place = [[Belleville, Ontario]], Canada
| death_date = {{death date and age|1962|8|15|1874|2|14|mf=y}}
| death_place = [[Winnipeg, Manitoba]], Canada
| career_start = 1894
| career_end = 1902
| halloffame = 1949<ref name="1949-ssp">{{cite news |work=Saskatoon Star-Phoenix |title=Ross One of Two New Men Elected to Hall of Fame |date=1949-10-22|page=18|url=https://news.google.com/newspapers?id=BCRgAAAAIBAJ&sjid=3W4NAAAAIBAJ&dq=bain%20hall%20of%20fame&pg=5188%2C2822583|accessdate=2012-02-07}}</ref><ref name="1949-oc">{{cite news|work=Ottawa Citizen|date=1949-10-21|title=Two Members Added to Hall of Fame|page=30|url=https://news.google.com/newspapers?id=O74vAAAAIBAJ&sjid=c90FAAAAIBAJ&dq=bain%20hall%20of%20fame&pg=5336%2C6376535 |accessdate=2012-02-07}}</ref>
}}
'''Donald Henderson''' "'''Dan'''" '''Bain''' (February 14, 1874&nbsp;– August 15, 1962) was a Canadian amateur athlete and merchant. Though he competed in and excelled in numerous sports, Bain is most notable for his [[ice hockey]] career. While a member of the [[Winnipeg Victorias]] hockey team, with whom he played for from 1894 until 1902, Bain helped the team win the [[Stanley Cup]] thrice as champions of Canada. A skilled athlete, Bain won championships and medals in several other sports, and was the Canadian [[trapshooting]] champion in 1903. In recognition of his play, Bain was inducted into multiple halls of fame, including the [[Hockey Hall of Fame]] in 1949, and was also voted Canada's top athlete of the last half of the 19th&nbsp;century.

In his professional life Bain was a prominent [[Winnipeg]] businessman and community leader. He became wealthy as a result of operating Donald H. Bain Limited, a grocery brokerage firm. Bain was an active member of numerous community associations, the president of the Winnipeg Winter Club and an avid outdoorsman. The Mallard Lodge, a building on the shores of [[Lake Manitoba]] built by Bain as a personal retreat, today serves as a research facility for the [[University of Manitoba]].

==Early life==
The son of Scottish immigrants, Bain was born in [[Belleville, Ontario]] and moved with his family to [[Winnipeg, Manitoba]], as a young child.<ref name="MBHistorical">{{cite web |url=http://www.mhs.mb.ca/docs/people/bain_dh.shtml |title=Donald Henderson "Dan" Bain (1874–1962) |publisher=The Manitoba Historical Society |accessdate=2010-01-06}}</ref> His father, James Henderson Bain, was a horse buyer for the British government and upon his arrival in Canada lived in Montreal before moving west. His mother, Helen Miller, was a seamstress. Bain was the sixth of seven children, having four sisters and two brothers.<ref>{{cite web|last=Goldsborough |first=Gordon |url=http://umanitoba.ca/science/delta_marsh/reports/1996/history.pdf |title=History of the University Field Station (Delta Marsh): Donald H. Bain (1874–1962) |publisher=University of Manitoba |year=1996 |accessdate=2010-11-11 |deadurl=yes |archiveurl=https://web.archive.org/web/20120320053916/http://umanitoba.ca/science/delta_marsh/reports/1996/history.pdf |archivedate=2012-03-20 |df= }}</ref>

==Sporting career==
Bain's first championship came in 1887 when he captured the Manitoba [[roller skating]] championship at the age of 13 by winning the three-mile race.<ref name="HockeyPeoplesHistory">{{cite book |last=McKinley |first=Michael |year=2006|title=Hockey: A People's History|publisher=McClelland & Stewart|location=Toronto|ISBN= 0-7710-5769-5 |pages=21–22}}</ref> At the age of 17 he won the provincial [[gymnastics]] competition, and at 20 he won the first of three consecutive Manitoba cycling championships.<ref name="MSHOF">{{cite web|url=http://www.halloffame.mb.ca/honoured/1981/dbain.htm |title=Honoured Members – Dan Bain |publisher=Manitoba Sports Hall of Fame |accessdate=2010-01-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20100117030507/http://www.halloffame.mb.ca:80/honoured/1981/dbain.htm |archivedate=2010-01-17 |df= }}</ref> In addition Bain was a top [[lacrosse]] player in his home province.<ref name="PodnieksPlayers">{{cite book |last=Podnieks |first=Andrew |title=Players: The ultimate A–Z guide of everyone who has ever played in the NHL |publisher=Doubleday Canada |year=2003 |location=Toronto |ISBN= 0-385-25999-9 |page=937}}</ref>

[[Image:WinnipegVictoriasFeb1896.jpg|thumb|left|The Winnipeg Victorias in 1896. Bain is in the front row, second from the left.|alt=Eight young men pose wearing identical sweaters with a buffalo logo on their right breast. They are all in hockey skates and holding sticks]]
In 1895 Bain first played competitive ice hockey when he answered a classified ad placed in a newspaper by the [[Winnipeg Victorias]], who were looking for new players.<ref name="HockeyPeoplesHistory"/> Though he played with a broken stick held together by wire, he made the team five minutes into his tryout.<ref name="HockeyPeoplesHistory"/> Bain quickly became a star [[Centre (ice hockey)|centre]] and leader for the Victorias. This was exemplified during a February 14, 1896 game against the [[Montreal Victorias]] for the [[Stanley Cup]], given to the national hockey champion in Canada. It was a 2–0 victory for Winnipeg that gave them the Cup.{{sfn|Coleman|1963|pp=29–30}} This victory marked the first time a team outside of Quebec had won the trophy.<ref>{{cite web|url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=STC&year=1895-96Feb |title=Winnipeg Victorias 1895–96Feb |publisher=Hockey Hall of Fame |accessdate=2010-01-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20070930080341/http://www.legendsofhockey.net:8080/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=STC&year=1895-96Feb |archivedate=September 30, 2007 }}</ref> The team was greeted by a huge crowd at the [[Canadian Pacific Railway]] station when their train, decorated with hockey sticks and the [[Union Jack]], returned to Winnipeg. They were led to a feast in their honour in a parade of open sleighs as fans gathered to celebrate the championship.<ref name="HockeyPeoplesHistory" />

The Montreal Victorias played Winnipeg in a challenge to reclaim the Cup in December 1896, a game described by the local press as "the greatest sporting event in the history of Winnipeg".<ref>{{cite book |last=McKinley |first=Michael |year=2006|title=Hockey: A People's History|publisher=McClelland & Stewart|location=Toronto|ISBN= 0-7710-5769-5 |page=23}}</ref> Though Bain scored two goals in the game, Montreal recaptured the Cup with a 6–5 victory.<ref name="LOH">{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p194501&page=bio&list=ByName#photo |title=The Legends – Dan Bain |publisher=Hockey Hall of Fame |accessdate=2010-01-06}}</ref> Winnipeg was involved in numerous further Stanley Cup challenges with Bain serving as the team's captain and manager. They again lost to their Montreal counterparts in 1898 before a record crowd of over 7,000 fans.<ref>{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=STC&year=1897-98 |title=Montreal Victorias 1897–98 |publisher=Hockey Hall of Fame |accessdate=2010-01-06}}</ref>

[[Image:WinnipegVictorias1901.jpg|thumb|right|The Winnipeg Victorias posing for a photo with the Stanley Cup in 1901. Bain is in the front row, fourth from the left.|alt=Fourteen men pose around a silver trophy. Several are wearing identical sweaters with a buffalo logo over the left breast and are holding hockey sticks.]]
During a 1900 challenge series against the [[Montreal Shamrocks]] Bain scored four goals in three games, but Winnipeg again lost the title.{{sfn|Coleman|1963|pp=57–58}} The Victorias challenged the Shamrocks again in 1901 in a best-of-three series. Winnipeg won the series in two games after Bain scored the clinching goal in overtime.<ref>{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SilverwareTrophyWinner.jsp?tro=STC&year=1900-01 |title=Winnipeg Victorias 1901 |publisher=Hockey Hall of Fame |accessdate=2010-01-06}}</ref> It was the first time in Stanley Cup history that the winning goal was scored in extra time.<ref name="LOH" /> Bain did so while playing with a broken nose that required him to wear a wooden face mask, earning the nickname "the masked man" as a result.<ref name="PodnieksPlayers" /> When the Victorias defended their title in a series against the [[Toronto Wellingtons]] in January 1902, Bain did not play in the series.{{sfn|Coleman|1963|pp=71–72}} The team lost their next challenge, against the [[Montreal Hockey Club]], in March of that year, which marked the end of Bain's hockey career.{{sfn|Coleman|1963|pp=72–74}}

Throughout his sporting career, Bain also earned medals in lacrosse and [[snowshoeing]]. He was the Canadian [[trapshooting]] champion in 1903.<ref name="HockeyPeoplesHistory" /> An avid [[figure skating|figure skater]] throughout much of his life, Bain won over a dozen titles, the last of which came at the age of 56, and he continued to skate until the age of 70;<ref name="CSHOF">{{cite web|url=http://www.sportshall.ca/honoured-members/27968/donald-dan-bain-2-2/ |title=Honoured Members – Donald "Dan" Bain |publisher=Canada's Sports Hall of Fame |accessdate=2013-06-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20131023120226/http://www.sportshall.ca/honoured-members/27968/donald-dan-bain-2-2/ |archivedate=2013-10-23 |df= }}</ref> he remained a competitive athlete until 1930.<ref name="MSHOF" /> On his skill in a variety of sports, Bain once said that "I couldn't see any sense in participating in a game unless I was good. I kept at a sport just long enough to nab a championship, then I'd try something else."<ref name="HockeyPeoplesHistory" />

In recognition of his sporting skill, Bain was inducted into several halls of fame. The first came in 1949 when he was elected a member of the [[Hockey Hall of Fame]].<ref name="1949-ssp"/><ref name="1949-oc"/> This was followed in 1971 with his induction into [[Canada's Sports Hall of Fame]], the [[Manitoba Sports Hall of Fame and Museum]] in 1981, and the [[Manitoba Hockey Hall of Fame]].<ref name="MSHOF"/><ref name="CSHOF"/><ref>{{cite web |url=http://www.mbhockeyhalloffame.ca/honoured/players.html?category=9&id=413|title=Honoured players – Dan Bain|publisher=Manitoba Hockey Hall of Fame|accessdate=2010-01-06}}</ref> He was also voted as Canada's top sportsman of the last half of the 19th century.<ref name="LOH"/><ref>{{cite book |last=McKinley |first=Michael |year=2006|title=Hockey: A People's History|publisher=McClelland & Stewart|location=Toronto|ISBN= 0-7710-5769-5 |page=24}}</ref>

==Personal life==
Outside of sports, Bain was a well known businessman in Winnipeg. He served as the president of Donald H. Bain Limited, a grocery brokerage firm headquartered in Winnipeg and operated in numerous cities.<ref name="MBHistorical" /> It was through his firm that he amassed a large fortune.<ref name="DUBio">{{cite web |last=Goldsborough |first=Gordon |url=http://www.ducks.ca/aboutduc/news/conservator/waterfowling/2007.pdf |archiveurl=https://web.archive.org/web/20120223160810/http://www.ducks.ca/aboutduc/news/conservator/waterfowling/2007.pdf |title=Mallard Lodge: Home of a marsh monarch |work=Ducks Unlimited Canada Conservator |year=2007 |accessdate=2010-01-06 |archivedate=2012-02-23 |pages=17–20}}</ref> Known as a community leader, he helped found the Winnipeg Winter Club on land that is now the {{HMCS|Chippawa}} naval reserve division, and after World War II he organized the current Winter Club.<ref name="MSHOF"/> Bain also belonged to many community groups and was the life governor of the Winnipeg General Hospital.<ref name="MBHistorical"/> He was also one of Western Canada's first automobile enthusiasts and owned many British vehicles.<ref name="MSHOF"/>

As a result of his trap-shooting career, Bain developed an appreciation for nature. He bought an ownership share of the Portage Country Club, on the [[Delta Marsh]] near the south shore of [[Lake Manitoba]], and later donated the land to [[Ducks Unlimited]].<ref name="MBHistorical"/><ref name="MSHOF"/><ref name="DUBio"/> Bain built the Mallard Lodge as a personal retreat on land adjacent to the club. He strictly enforced his privacy, even building a road to his lodge that he allowed no one else to use; members of the Portage Country Club were required to take a different route.<ref name="DUBio"/> Bain intended to donate his lodge to the government of Manitoba for preservation, though he died before he could do so. The lodge passed into the control of the government regardless, and was donated to the [[University of Manitoba]] as a research facility in 1966 that remains active today.<ref name="DUBio"/> Bain was also a member of the Manitoba Game and Fish Association and the Winnipeg Humane Society.<ref name="MBHistorical"/>

Bain never married and had no children.<ref name="DUBio"/> He was fond of his pets, in particular his [[Curly Coated Retriever]] dogs that he was said to value above human company.<ref name="DUBio"/> On August 15, 1962 Bain died in Winnipeg, aged 88. He left behind an estate in excess of [[Canadian dollar|C$]]1 million, (${{formatprice|{{Inflation|CA|1000000|1962}}}} in {{CURRENTYEAR}} dollars),{{inflation-fn|CA}} the majority of which he donated to charity and former employees.<ref name="MBHistorical"/>

==Career statistics==
{| border="0" cellpadding="1" cellspacing="0" ID="Table3" style="text-align:center; width:50em"
|- bgcolor="#e0e0e0"
! colspan="3" bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Regular season|Regular&nbsp;season]]
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Stanley Cup]] Finals
|- bgcolor="#e0e0e0"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|- 
| 1894–95
| [[Winnipeg Victorias]]
| MHL Sr. 
| 3 || 10 || 0 || 10 || — 
| — || — || — || — || — 
|- bgcolor="#f0f0f0"
| 1895–96
| Winnipeg Victorias
| MHL Sr.
| 5 || 10 || 3 || 13 || —
| 2 || 3 || 0 || 3 || — 
|-
| 1896–97
| Winnipeg Victorias
| MHL Sr.
| 5 || 7 || 1 || 8 || — 
| — || — || — || — || — 
|- bgcolor="#f0f0f0"
| 1897–98
| Winnipeg Victorias
| MHL Sr.
| 5 || 13 || 1 || 14 || —
| — || — || — || — || — 
|- 
| 1898–99
| Winnipeg Victorias
| MHL Sr.
| 3 || 11 || 1 || 12 || —
| 1 || 0 || 0 || 0 || 0 
|- bgcolor="#f0f0f0"
| 1899–00
| Winnipeg Victorias
| MHL Sr.
| 2 || 9 || 1 || 10 || 0
| 3 || 4 || 0 || 4 || — 
|-
| 1900–01
| Winnipeg Victorias
| MHL Sr.
| 3 || 3 || 0 || 3 || 1
| 2 || 3 || 0 || 3 || — 
|- bgcolor="#f0f0f0"
| 1901–02
| Winnipeg Victorias
| MHL Sr.
| 1 || 3 || 0 || 3 || 0
| 3|| 0 || 0 || 0 || 0 
|- bgcolor="#e0e0e0"
| colspan="3" | '''Totals'''     
! 27 !! 66 !! 7 !! 73 !! — 
! 11 !! 10 !! 0 !! 10 !! —
|}

==References==
* {{cite book |last=Coleman |first=Charles L |year=1963 |title=The Trail of the Stanley Cup, Volume 1: 1893–1926 inc. |location=Dubuque, Iowa |publisher=Kendall/Hunt Publishing|isbn=0-8403-2941-5 |ref=harv}}
;Notes
{{reflist|2}}

==External links==
*{{Legendsmember|P|p194501|Dan Bain}}

{{good article}}

{{DEFAULTSORT:Bain, Dan}}
[[Category:1874 births]]
[[Category:1962 deaths]]
[[Category:Canada's Sports Hall of Fame inductees]]
[[Category:Canadian ice hockey forwards]]
[[Category:Canadian people of Scottish descent]]
[[Category:Hockey Hall of Fame inductees]]
[[Category:Ice hockey people from Ontario]]
[[Category:Sportspeople from Belleville, Ontario]]
[[Category:Stanley Cup champions]]
[[Category:Winnipeg Victorias players]]