{{multiple issues|
{{Orphan|date=September 2014}}
{{refimprove|date=February 2017}}
}}
<!-- Please leave this line alone! -->
The '''Economic and Business Research Center''' in the  [[Eller College of Management]] at The [[University of Arizona]], [[Tucson]], is a unit that has been providing the citizens of [[Arizona]] with high quality{{citation needed|date=October 2013}} economic forecasts and applied economic research since 1949. The Economic and Business Research Center's mission is to provide the Arizona community of [[business]] and [[public-sector]] decision makers with applied r[[esearch]] and information on [[economic]], [[demographic]] and business trends in Arizona.

== Activities ==

* Providing the public with economic forecasts, [[socioeconomic]] and demographic [[data]], and economic [[analysis]] for the State of Arizona and its metro areas via public [[presentations]], [[publications]] and their [[website]].
* In addition to providing economic forecasts to the public in general, EBR also provides in-depth economic forecast analysis and databases on a subscription basis to government and business via its economic forecasting unit, ''The Forecasting Project''. This subscription includes access to extensive online databases of important economic, demographic, and quality of life indicators for the State of Arizona, its metro areas and counties.
* ''Arizona's Economy'', quarterly online magazine provides analysis of current economic trends and issues in Arizona.
*  Conducting research in the areas of:
** Regional economic, [[revenue]] and population forecasting
** Economic impact analysis
** Border and regional development
** Environmental/sustainable economics
** [[Renewable energy]]
** [[Public finance]] and [[taxation]]
** Industry Studies
* Responding to data requests and educating the public about data resources and best practices in the use of public data
* EBR's '''Annual Economic Outlook Luncheon''', held each December, provides the Arizona community with a summary analysis of the latest economic forecast for Arizona and the nation. Six months later each June, EBR revisits the forecast in a '''Mid-Year Economic Outlook Breakfast'''.
* Arizona Statistical Abstract

== Research Studies ==

The Economic and Business Research Center conducts applied research and analysis on a wide variety of topics pertaining to economic conditions and trends in Arizona and the Southwest and border region. 
The following are some of the significant studies conducted in the past:

* '''RTA Economic Impact Study and Revenue vs. Bid/Cost Comparison, Final Report'''
:: Alberta H. Charney, Ph.D. (11pp). December 21, 2009
* '''Arizona-Sonora Region: Economic Indicators and Regional Initiatives, 2009'''
:: Vera K. Pavlakovich-Kochi, Ph.D., and Jaewon Lim, Ph.D., 2009
* '''Economic And Revenue Impact of $1 Million in Sustained Cancer Research Funds'''
:: Alberta H. Charney, Ph.D., and Marshall J. Vest., February, 2008
* '''Town of Sahuarita Workforce Assessment Survey'''
:: Maile Nadelhoffer, M.S., M.A., August, 2008
* '''Mexican Visitors to Arizona: Visitor Characteristics and Economic Impacts, 2007-08'''
:: Vera K. Pavlakovich-Kochi, Ph.D. and Alberta H. Charney, Ph.D. (82pp). December, 2008
* '''Astronomy, Planetary and Space Sciences Research in Arizona: An Economic and Tax Revenue Impact Study'''
:: Vera K. Pavlakovich-Kochi, Ph.D., Alberta H. Charney, Ph.D., Lora Mwaniki-Lyman (29pp). October, 2007
* '''A Strategic Assessment of the Economic Benefits of Investments in Research in Arizona'''
:: Alberta H. Charney, Ph.D. , Kent Hill, Ph.D., Dennis Hoffman, Ph. D., Jose Lobo, Ph.D., Maile Nadelhoffer, M.S., M.A. (117pp). April, 2007
* '''The Role of Arizona Cities and Towns in the State's Economy'''
:: Tanis J. Salant, D.P.A.; Alberta H. Charney, Ph.D.; Marshall J. Vest. October, 2006.
* '''The University of Arizona Economic and Tax Revenue Impacts FY 2004'''
:: Alberta H. Charney, Ph.D. , Vera K. Pavlakovich-Kochi, Ph.D., Lora Mwaniki-Lyman and Sushila Umashankar, Ph.D. (30pp). November, 2005.

== The Forecasting Project ==

''The Forecasting Project'' is a community-sponsored research unit within the Economic and Business Research Center producing quarterly economic forecasts for Arizona. These forecasts are recognized as among the most accurate in the Western states.

The following are some of the services provided to sponsoring organizations:

* In-depth economic forecasts and analysis provided on a quarterly basis for Arizona, as well as, the [[Phoenix, Arizona|Phoenix]]-[[Mesa, Arizona|Mesa]]-[[Glendale, Arizona|Glendale]] and [[Tucson]] metro areas.
* A comprehensive examination of the forecast and business conditions four times a year in roundtable sessions with [[Eller College]] economists, other sponsors, and invited speakers.
* Quarterly report containing analysis and tables of forecasted variables, economic indicators, and 35 pages of graphs illustrating recent trends.
* Access to economic forecasting models and supporting data in AREMOS databanks and EXCEL spreadsheets. AREMOS is a product of Global Insight, a national leader in economic consulting and forecasting.
* Long run 30-year forecasts each August.
* Access to The Forecasting Project website for easy downloading of data, reports, and analysis.
* Consulting on issues related to corporate economics and forecasting.

== People ==
* [http://ebr.eller.arizona.edu/about/people/Hammond_George.asp George Hammond, Ph.D.], Director and Eller Research Professor
* [http://ebr.eller.arizona.edu/about/people/Alberta_Charney.asp Alberta H. Charney, Ph.D.], Senior Research Economist* 
* [http://ebr.eller.arizona.edu/about/people/Kinnear_Daniel.asp Dan Kinnear], Specialist, Business Research
* [http://ebr.eller.arizona.edu/about/people/Maile_Nadelhoffer.asp Maile Nadelhoffer], Research Economist and Webmaster
* [http://ebr.eller.arizona.edu/about/people/Pia_Montoya.asp Pia Montoya], Computer Database Specialist
* [http://ebr.eller.arizona.edu/about/people/Jennifer_Pullen.asp Jennifer Pullen], Research Economist
* [http://ebr.eller.arizona.edu/about/people/Valorie_Hanni_Rice.asp Valorie Rice], Senior Specialist, Business Information
* [http://ebr.eller.arizona.edu/about/people/Vera_Pavlakovich_Kochi.asp Vera Pavlakovich-Kochi, Ph.D.], Senior Research Scientist

<ref>{{cite web|title=Economic and Business Research Center : Eller College of Management : The University of Arizona|url=https://ebr.eller.arizona.edu/|website=ebr.eller.arizona.edu}}</ref>

==References==
{{Reflist}}

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:University of Arizona]]
[[Category:Economy of Arizona]]