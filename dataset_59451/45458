<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          =Adrien Louis Jacques Leps
| image         =
| caption       =
| birth_date          = {{Birth date|1893|09|27}}
| death_date          = Post 1945
| placeofburial_label = 
| placeofburial = 
| birth_place  =[[Angers]], France
| death_place  =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    =France
| branch        =Cavalry; aviation
| serviceyears  =1913&ndash;1919; World War II
| rank          =Lieutenant colonel
| unit          =''[[9e Regiment de Hussards]]'', ''[[1e Regiment de Hussards]]'', [[Escadrille 67]], [[Escadrille 81]] 
| commands      =[[Escadrille 81]]
| battles       =
| awards        =''[[Légion d'honneur]]'', ''[[Croix de Guerre]]'' with nine [[palmes]], 1939&ndash;1945 ''Croix de Guerre'', British [[Military Cross]]
| relations     =
| laterwork     =Also served in World War II
}}
Major '''Adrien Louis Jacques Leps''' was a World War I [[flying ace]] credited with twelve confirmed aerial victories, as well as two probables.<ref name="theaerodrome.com">http://www.theaerodrome.com/aces/france/leps.php Retrieved on 17 July 2010.</ref> He served originally in the French cavalry, before shifting to flying. In later years, he served under General [[Armand Pinsard]] during World War II.<ref name="ReferenceA">{{cite book |title= ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' |page= 184 }}</ref>

==Cavalry service==

Leps entered the ''9e Regiment de Hussards'' on 9 October 1913 as a ''"soldate de 2e classe"''. He was promoted to the rank of enlisted [[Brigadier]] on 19 April 1914. He went into action with his regiment on 2 August 1914. A month later, on 5 September 1914, he was promoted to ''[[Maréchal-des-logis]]''. Two months later, on 4 November 1914, he transferred to  the ''1e Regiment de Hussards''. On 24 December 1914, he had been dubbed an [[Aspirant]]. On 17 April 1915, he was commissioned as ''sous lieutenant''. He was severely wounded in action on 9 July 1915, and cited in dispatches. On 3 October 1915, he was forwarded to [[Armée de l'Air (Part I: From birth to "Torch", 1909-1942)|''Aéronautique Militaire'']] as an observer/gunner.<ref name="ReferenceA"/>

==Aerial service==

Leps was initially assigned to Escadrille N67 (the 'N' denoting that the unit used [[Nieuport]]s). On 15 July 1916, he was forwarded for pilot's training at Amberieu. He received Military Pilot's Brevet No. 4312 on 23 August 1916. He then reported for reassignment on 2 November. On 14 December 1916, he was posted to Escadrille N81.<ref name="ReferenceA"/>

Adrien Leps scored his first aerial victories on 16 March 1917, downing an ''[[Albatros Flugzeugwerke|Albatros]]'' reconnaissance plane and another unidentified German two-seater. On 30 March, his award of the ''Légion d'honneur'' cited these wins. He next shot down an enemy fighter plane on 27 June 1917.<ref name="theaerodrome.com"/> A week later, on 4 July, Leps was promoted to Lieutenant.<ref name="ReferenceA"/>  He would score two more triumphs before year's end, sharing with [[Marcel A. Hugues]] and [[André Herbelin]].<ref>{{cite book |title= ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' |page= 184&ndash;185 }}</ref>

Leps was elevated to command of Escadrille 81 on 24 February 1918, as the squadron re-equipped with [[Société Pour L'Aviation et ses Dérivés|Spad]]s.<ref name="ReferenceA"/> He knocked down a German plane in a solo victory on 6 April 1918. In May 1918, he teamed with [[Gabriel Guérin]] and [[Paul Santelli]] in wins over three German observation balloons and an Albatros;<ref name="theaerodrome.com"/> on the 11th of the month, he received the British Military Cross. On 6 June, he shared a win over another balloon with [[Pierre Cardon]]. On the 15th, he singlehandedly shot down his fifth enemy gasbag, becoming a [[balloon buster]] ace.<ref name="theaerodrome.com"/> In the wake of this dozenth victory, he was upgraded to capitaine on 14 August 1918. By war's end, he had the ''Croix de Guerre'' with nine ''[[palmes]]'' to accompany his ''Légion d'honneur'' and MC.<ref name="ReferenceA"/>

==Post World War I==
Leps commanded Escadrille 81 until its disbandment on 31 December 1919.<ref name="ReferenceA"/>

He returned to service for World War II, assigned as a major to ''[[Groupe de Chasse No. 21]]'' under General [[Armand Pinsard]]. For his service in his second war, he earned a rise in status to ''Commandeur of the Légion d'honneur'', the 1939&ndash;1945 ''Croix de Guerre'', and a final promotion to lieutenant colonel.<ref name="ReferenceA"/>

==References==
* ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' Norman L. R. Franks, Frank W. Bailey. Grub Street, 1992. ISBN 0-948817-54-2, ISBN 978-0-948817-54-0.

==Endnotes==
{{reflist}}

{{DEFAULTSORT:Leps, Adrien}}
[[Category:1893 births]]
[[Category:20th-century deaths]]
[[Category:French World War I flying aces]]