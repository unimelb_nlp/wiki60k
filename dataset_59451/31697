{{About|the American science-fiction magazine|the Australian screen-professionals magazine|If Magazine}}
{{Italic title}}
{{Infobox magazine
| title           =If
| image_file      =If cover May 1955.jpg
| image_size      = 200
| image_alt       = 
| image_caption   = May 1955 issue of ''If''. The cover is by [[Kenneth S. Fagg]], and is titled "Technocracy Versus the Humanities".
| editor          =  [[James L. Quinn (editor)|James L. Quinn]]
| editor_title    = 
| previous_editor = [[Paul W. Fairman]]
| staff_writer    = 
| photographer    =
| category        = Science fiction
| frequency       = Monthly
| circulation     = 67,000 (1967)
| publisher       = Quinn Publications (1952–1959)<br />Galaxy Publishing (1959–1974)
| founder         =  [[James L. Quinn (editor)|James L. Quinn]]
| founded         = {{start date and age|1952}}
| firstdate       = {{start date|1952|03}}
| lastdate = December 1974
| company         =
| country         = United States
| based           = [[New York City]]
| language        = English
| website         =
| issn            =
| oclc            =
}}
'''''If''''' was an American [[science fiction magazine]] launched in March 1952 by Quinn Publications, owned by [[James L. Quinn (editor)|James L. Quinn]].

The magazine was moderately successful, though it was never considered to be in the first tier of science fiction magazines. It achieved its greatest success under editor [[Frederik Pohl]], winning the [[Hugo Award]] for best professional magazine three years running from 1966 to 1968. ''If'' published many award-winning stories over its 22&nbsp;years, including [[Robert A. Heinlein]]'s novel ''[[The Moon Is a Harsh Mistress]]'', and [[Harlan Ellison]]'s short story "[[I Have No Mouth and I Must Scream]]". The most prominent writer to make his first sale to ''If'' was [[Larry Niven]], whose story "The Coldest Place" appeared in the December 1964 issue.

''If'' was merged into ''[[Galaxy Science Fiction]]'' after the December 1974 issue, its 175th issue overall.

==Publication history==
Although science fiction had been published in the United States before the 1920s, it did not begin to coalesce into a separately marketed genre until the appearance in 1926 of ''Amazing Stories'', a [[pulp magazine]] published by [[Hugo Gernsback]]. By the end of the 1930s the field was undergoing its first boom,<ref>Nicholls & Clute, "Genre SF"; Edwards & Nicholls, "Astounding Science-Fiction"; Stableford, "Amazing Stories"; Edwards & Nicholls, "SF Magazines", all in Nicholls & Clute, "Encyclopedia of Science Fiction".</ref> but [[World War II]] and its attendant paper shortages led to the demise of several titles. By the late 1940s the market began to recover again.<ref>Edwards & Nicholls, "SF Magazines", in Nicholls & Clute, ''Encyclopedia of Science Fiction'', p.&nbsp;1068.</ref> From a low of eight active magazines in 1946, the field expanded to 20 in 1950, and a further 22 had commenced publication by 1954.<ref name=AshleyV3_323>Magazine publishing dates for the period are tabulated in Ashley, ''History of the Science Fiction Magazine Vol. 3'', pp.&nbsp;323–325.</ref> ''If'' was launched in the middle of this second publishing boom.

===Origins and 1950s===
''If'''s origins can be traced to 1948 and 1949, when [[Raymond Palmer]] founded two magazines while working at [[Ziff-Davis]] in Chicago: ''[[Fate (magazine)|Fate]]'' and ''[[Other Worlds (magazine)|Other Worlds]]''. ''Fate'' published articles about occult and supernatural events, while ''Other Worlds'' was a science fiction magazine. The two were sufficiently successful to attract the notice of [[James L. Quinn (editor)|James L. Quinn]], a New York publisher. When Ziff-Davis moved to New York City in late 1950, [[Paul W. Fairman]], a prolific writer, went with them, and was soon in touch with Quinn, who decided to found a pair of magazines modelled after Palmer's. One was a non-fiction magazine entitled ''Strange''; the other was ''If''.<ref name=TM_45-8>Ashley, ''Transformations'', pp.&nbsp;45–48.</ref>

[[File:If cover June 1954.jpg|thumb|left|The June 1954 issue of ''If'', featuring a wraparound cover by [[Kenneth S. Fagg]], titled "Lava Falls on Mercury"]]
The first issue of ''If'' was dated March 1952, with Fairman as editor; it featured stories by Richard Shaver, Raymond Palmer, and Howard Browne, all writers who were regulars of the Ziff-Davis magazines. By the time the third issue reached the news stands, the disappointing sales figures for the first issue were in, and Quinn decided to let Fairman go. Quinn persevered with himself as editor. His first issue was dated July 1952, and he continued as editor on the masthead for some years. Quinn brought in [[Ed Valigursky]] as the art editor; he designed striking covers, including some wraparound artwork—an unusual feature—which helped improve circulation. Quinn began searching for a replacement editor: writer [[Lester del Rey]] turned down the job (a decision he is reported to have later regretted) but Quinn was able to engage [[Larry T. Shaw]], an active [[science fiction fan]] who had sold a few stories.<ref name=TM_45-8/><ref>Malcolm Edwards & John Clute, "Larry T. Shaw", in Nicholls & Clute, "Encyclopedia of Science Fiction".</ref> Shaw joined in May 1953 as associate editor and soon began writing editorials (beginning with the September 1953 issue) and assisting with story selection. The magazine's quality quickly improved and soon Quinn felt able to switch to a monthly schedule, instead of bi-monthly. Shaw left after only a year, and Quinn resumed full editorial responsibilities.<ref name=TM_45-8/>

In late 1953, Quinn decided to run a competition for short fiction from new writers. The competition was only open to college students who had not sold a story before. The first prize was $1,000, the second prize $500, and there were five runner-up prizes of $100 each. Entries came in from writers who were later to become well-known, including [[Harlan Ellison]], [[Roger Zelazny]], and [[Andrew J. Offutt]], whose story "And Gone Tomorrow", about a man unexpectedly sent a hundred years into the future, won first prize and appeared in the December 1954 issue of ''If''. The only other one of the seven announced winners who had a career as a science fiction writer was [[Leo P. Kelley]]. Quinn decided to move ''If'' to a monthly schedule with the March 1954 issue, perhaps because the competition had increased readership.<ref name=TM_45-8/> It reverted to a bimonthly schedule with the June 1956 issue, as circulation dropped again.<ref name=AshleyV4_433>Ashley, ''History of SF Magazine Part 4'', p.&nbsp;33.</ref>

In 1957, [[American News Company]], by far the largest magazine distributor, was liquidated.<ref>Distributors move magazines from publishers to news stands, and are a critical part of the magazine publishing industry.</ref> Almost all the science fiction magazines had to find a new distributor, and the smaller independent companies remaining in the market often demanded monthly publication and a larger format from the magazines they took on. Many of the magazines did not have the advertising revenue required to support these changes, and within two or three years many of them had disappeared:<ref name=TM_190>Ashley, ''Transformations'', p.&nbsp;190.</ref> the number of science fiction magazines being published dropped from a high of forty-six in 1953 to less than a dozen by the end of the decade.<ref name=Robinson_128>Robinson, ''SF of the 20th Century'', p.&nbsp;128.</ref> For a while ''If'' was hard to find on the news stands, but it survived. Quinn did try the [[Slick (magazine format)|slick format]] (using glossy paper, unlike the cheaper paper used for pulps and digests) for a companion magazine, ''Space Age'', which he launched in November 1958; the experiment was unsuccessful, however. In an attempt to improve ''If'''s circulation, Quinn hired writer [[Damon Knight]], whose first issue was October 1958. Circulation failed to increase, though this was at least partly due to the problems with distribution, and by early 1959 Quinn decided to sell the magazine. Knight's last issue was his third, dated February 1959.<ref name=TM_196-7>Ashley, ''Transformations'', pp.&nbsp;196–197.</ref>

===Early 1960s===
''If'''s new owner was Robert Guinn, of Galaxy Publishing. The change of ownership was abrupt and led to a delay in publication, with the first issue under new editorship not appearing until July 1959. The editor was [[Horace Gold]], who was also the editor of ''[[Galaxy Science Fiction]]''; ''Galaxy'' had gone from a monthly to a bimonthly schedule at the start of 1959, and ''If'' and ''Galaxy'' appeared in alternate months for the next few years. In a 1975 retrospective article, Gold commented that his policy with ''If'' was to experiment, using new writers that had not yet established themselves. In the judgement of science fiction historian [[Mike Ashley (writer)|Mike Ashley]], the effect was that ''If'' became the weaker of the two magazines, printing stories that were of lower quality than those Gold selected for ''Galaxy''.<ref name=TM_197>Ashley, ''Transformations'', p.&nbsp;197.</ref>

[[File:Annual circulation of If (magazine).gif|thumb|400px|Annual circulation from 1960–1974]]
[[Frederik Pohl]] took over the editorship of both ''If'' and ''Galaxy'' in 1961. Gold had had a car accident with sufficiently severe health consequences to prevent him from being able to continue as editor.<ref name=TWTFW_190-4>Pohl, ''Way the Future Was'', pp.&nbsp;190–194.</ref> Pohl, who had been intermittently helping Gold with editorial duties for some time prior to the car accident, is first listed as editor of ''If'' on the masthead of the November 1961 issue, and as editor of ''Galaxy'' for the December 1961 issue, but he had been acting as editor of both magazines since at least mid-year.<ref name=TWTFW_190-4/><ref name=issues/> Pohl paid one cent per word for the stories he bought for ''If'', whereas ''Galaxy'' paid three cents per word, and like Gold he regarded ''Galaxy'' as the leading magazine of the two, whereas ''If'' was somewhere he could work with new writers, and try experiments and whims. This developed into a selling point when a letter from a reader, Clayton Hamlin,<!-- Note Ashley mis-spells his name as "Hamling"; it is "Hamlin" in the magazine --> prompted Pohl to declare that he would publish a new writer in every single issue of the magazine,<ref name=TM_208-9>Ashley, ''Transformations'', pp.&nbsp;208–209.</ref><ref name=If_1962_09_129>''If'' vol.&nbsp;12, no&nbsp;4 (September 1962), p.&nbsp;129.</ref> though he was also able to attract well-known writers.<ref name=TM_210>Ashley, ''Transformations'', p.&nbsp;210.</ref> When Pohl began his stint as editor, both magazines were operating at a loss; despite ''If'''s lower budget Pohl found it more fun to edit, and commented that apparently the readers thought so too: he was able to make ''If'' show a profit before ''Galaxy'', adding "What was fun for me seemed to be fun for them."<ref name=TWTFW_199>Pohl, ''Way the Future Was'', p.&nbsp;199.</ref>

In April 1963, Galaxy Publishing brought out the first issue of ''[[Worlds of Tomorrow (magazine)|Worlds of Tomorrow]]'', another science fiction magazine, also edited by Pohl.<ref name=TM_207>Ashley, ''Transformations'', p.&nbsp;207.</ref> The magazine published some well-received material and was profitable, but Guinn, the publisher and owner, decided in 1967 that it would be better to have ''Galaxy'' resume a monthly schedule; both ''Worlds of Tomorrow'' and ''Galaxy'' were bimonthly at that time, while ''If'' was monthly. With the August 1967 issue ''Worlds of Tomorrow'' was merged with ''If'', though it was another year before ''Galaxy'' actually switched to a monthly schedule.<ref name=TM_273>Ashley, ''Transformations'', p.&nbsp;273.</ref> By this time ''If'' had become monthly again, starting with the July 1964 issue (though the schedule had an initial hiccup, omitting September 1964).<ref name=TM_210/>

The circulation rose from 64,000 in 1965 to 67,000 in 1967; the modest 5% increase was exceeded only by ''[[Analog Science Fiction and Fact|Analog]]'' among the other science fiction magazines, and ''If'' won the [[Hugo Award]] for best professional SF magazine three years running during this period. However, in March 1969, Robert Guinn sold all four of his magazines, including ''Galaxy'' and ''If'', to Arnold Abramson at Universal Publishing and Distribution Corporation (UPD). Pohl was in [[Rio de Janeiro]] when he heard the news, and decided to resign his position as editor rather than continue under the new management. He had been considering a return to a writing career for some time and the change in ownership precipitated his decision to leave.<ref name=TM_281-2>Ashley, ''Transformations'', pp.&nbsp;281–282.</ref><ref name=GF_34>Ashley, ''Gateways to Forever'', p.&nbsp;34.</ref>

===Decline and merger with ''Galaxy''===
The new editor was [[Ejler Jakobsson]], though Pohl continued to be listed as editor emeritus on the masthead until the July–August 1970 issue.<ref name=issues/> Much of the editorial work was actually done by [[Judy-Lynn del Rey|Judy-Lynn Benjamin]], who was hired by Pohl in 1969 as an editorial assistant. The new regime failed to impress readers, and circulation dropped from over 67,000 for the year ending October 1968 to under 45,000 the following year, a drop of over 30%. ''If'' went bimonthly in May 1970, as Abramson attempted to juggle the frequency of publication of several of his titles to maximize profits; the page count and price were also adjusted more than once over the next year, again increasing profitability. Abramson also began a British distribution of ''If'', reprinted with a separate cover, priced in British currency. Circulation figures of the time show an increase of about 6,000 copies, but it is not clear if this includes sales in the UK.<ref name=GF_53-6>Ashley, ''Gateways to Forever'', pp.&nbsp;53–56.</ref>

In May 1973, Judy-Lynn Benjamin (Judy-Lynn del Rey since her 1971 marriage to Lester del Rey) resigned. She was briefly replaced by Albert Dytch, but within four months Dytch in turn left, and in August 1973 [[Jim Baen|James Baen]] joined UPD. He was made managing editor of ''If'' with effect from the January 1974 issue, and full editor one issue later; Jakobsson was listed as editor emeritus until the August 1974 issue. Baen had little opportunity to work with ''If'', however, as financial problems at UPD combined with the increasing cost of paper (a consequence of the rising price of oil) led to a decision to combine ''If'' with ''Galaxy''. Despite the fact that in 1974 ''If''<nowiki>'</nowiki>s circulation had exceeded ''Galaxy''<nowiki>'</nowiki>s for the first time, it was ''Galaxy'' that was retained, and ''If'' was merged with it beginning with the January 1975 issue.<ref name=GF_56-62>Ashley, ''Gateways to Forever'', pp.&nbsp;56–62.</ref>

In 1986 an attempt was made to revive ''If'' as a semi-professional magazine. The only issue, dated September–October 1986, was edited by Clifford Hong.<ref name=Contento_WoI>{{cite web | url = http://www.locusmag.com/index/chklst/mg1196.htm| title = Worlds of If Checklist | accessdate=23 February 2008|publisher = Stephen G. Miller and William T. Contento}}</ref><ref name=Tuck_569>Tuck, "''If''", p.&nbsp;569.</ref><ref name=LM_If>{{cite web | url = http://www.locusmag.com/index/t53.htm#A5912| title = Contents List | accessdate=23 February 2008|publisher = Locus Press}}</ref>

==Contents and reception==
{| class="wikitable" style="font-size: 10pt; line-height: 11pt; margin-right: 2em; text-align: center; float: left"
! !!Jan !! Feb !!Mar !!Apr !!May !!Jun !!Jul !!Aug !!Sep !!Oct !!Nov !!Dec
|- 
!1952
|| || ||bgcolor=#ffff99|1/1 || ||bgcolor=#ffff99|1/2 || ||bgcolor=#ffff99|1/3 || ||bgcolor=#ccffff|1/4 || ||bgcolor=#ccffff|1/5 ||
|-
!1953
|bgcolor=#ccffff|1/6 || ||bgcolor=#ccffff|2/1 || ||bgcolor=#ed6fba|2/2 || ||bgcolor=#ed6fba|2/3 || ||bgcolor=#ed6fba|2/4 || ||bgcolor=#ed6fba|2/5 ||
|-
!1954
|bgcolor=#ed6fba|2/6 || ||bgcolor=#ed6fba|3/1 ||bgcolor=#ccffff|3/2 ||bgcolor=#ccffff|3/3 ||bgcolor=#ccffff|3/4 ||bgcolor=#ccffff|3/5 ||bgcolor=#ccffff|3/6 ||bgcolor=#ccffff|4/1 ||bgcolor=#ccffff|4/2 ||bgcolor=#ccffff|4/3 ||bgcolor=#ccffff|4/4
|-
!1955
|bgcolor=#ccffff|4/5 ||bgcolor=#ccffff|4/6 ||bgcolor=#ccffff|5/1 ||bgcolor=#ccffff|5/2 ||bgcolor=#ccffff|5/3 ||bgcolor=#ccffff|5/4 ||bgcolor=#ccffff|5/5 || ||bgcolor=#ccffff|5/6 || ||bgcolor=#ccffff|6/1 ||
|-
!1956
|| ||bgcolor=#ccffff|6/2 || ||bgcolor=#ccffff|6/3 || ||bgcolor=#ccffff|6/4 || ||bgcolor=#ccffff|6/5 || ||bgcolor=#ccffff|6/6 || ||bgcolor=#ccffff|7/1
|-
!1957
|| ||bgcolor=#ccffff|7/2 || ||bgcolor=#ccffff|7/3 || ||bgcolor=#ccffff|7/4 || ||bgcolor=#ccffff|7/5 || ||bgcolor=#ccffff|7/6 || ||bgcolor=#ccffff|8/1
|-
!1958
|| ||bgcolor=#ccffff|8/2 || ||bgcolor=#ccffff|8/3 || ||bgcolor=#ccffff|8/4 || ||bgcolor=#ccffff|8/5 || ||bgcolor=#ccc0d0|8/6 || ||bgcolor=#ccc0d0|9/1
|-
!1959
|| ||bgcolor=#ccc0d0|9/2 || || || || ||bgcolor=#c2d69a|8/6 || ||bgcolor=#c2d69a|9/4 || ||bgcolor=#c2d69a|9/5 || 
|-
!1960
|bgcolor=#c2d69a|9/6 || ||bgcolor=#c2d69a|10/1 || ||bgcolor=#c2d69a|10/2 || ||bgcolor=#c2d69a|10/3 || ||bgcolor=#c2d69a|10/4 || ||bgcolor=#c2d69a|10/5 ||
|-
!1961
|bgcolor=#c2d69a|10/6 || ||bgcolor=#c2d69a|11/1 || ||bgcolor=#c2d69a|11/2 || ||bgcolor=#c2d69a|11/3 || ||bgcolor=#c2d69a|11/4 || ||bgcolor=#c2d69a|11/5 ||
|-
|colspan="13" style="font-size: 8pt; text-align:left"|Issues of ''If'' from 1952 to 1961, showing volume/issue number.  Editors were<br/>Paul W. Fairman (yellow), James L. Quinn (blue), Larry T. Shaw (pink), Quinn<br/>again (blue), Damon Knight (purple) and H.L. Gold (green).
|}The first issue of ''If'', dated March 1952, went on sale on 7 January of that year. The lead story was [[Howard Browne]]'s "Twelve Times Zero", a murder mystery with a science-fictional resolution; other stories were from [[Raymond A. Palmer|Ray Palmer]], [[Richard Shaver]], and [[Rog Phillips]], all writers associated with the Ziff-Davis magazines.<ref name=issues/><ref name=TM_45>Ashley, ''Transformations'', p.&nbsp;45.</ref> Browne was the editor of Ziff-Davis's ''[[Amazing Stories|Amazing Science Fiction]]'', a leading magazine of the time, and had given Fairman his start in the field in the late 1940s.<ref name=TM_45/> Fairman was familiar with Ziff-Davis's stable of writers, and his preference for them was a reflection of his experience, though this did not necessarily serve the magazine well—he referred to the acquisition of Browne's story as "the scoop of the century" and spoke in glowing terms of him in an introductory note despite the fact that Browne was reputed to detest science fiction.<ref name=TM_45/><ref name=If_1952_03_6>The "scoop of the century" quote comes from an inset blurb on the first page of Browne's story; it is unsigned but appears to be by Fairman. ''If'' vol.&nbsp;1, no&nbsp;1 (March 1952), p.&nbsp;6.</ref><ref>Malcolm Edwards, "Howard Browne", in Nicholls & Clute, ''Encyclopedia of Science Fiction'', p.&nbsp;165.</ref> In addition to the fiction and the editorial by Fairman, there was a letter column, a profile of [[Wilson Tucker]], a selection of science news, a guest editorial by Ken Slater, a well-known British fan, and an approving review of the TV show ''[[Tales of Tomorrow]]''.<ref name=issues/>

After Quinn dismissed Fairman and engaged Larry Shaw, the magazine improved significantly, and published several well-received stories, including [[James Blish]]'s "A Case of Conscience" in the September 1953 issue, later to become the first part of Blish's Hugo Award-winning [[A Case of Conscience|novel of the same name]], about a [[Jesuit]] priest on a planet of aliens who have no religion but appear free of sin.<ref name=issues/><ref name=ESF_136>Peter Nicholls, "James Blish", in Nicholls & Clute, ''Encyclopedia of SF'', p.&nbsp;136.</ref><ref>Ashley comments "It is noticeable how soon after Shaw's arrival the quality of material in ''If'' began to rise". Ashley, ''Transformations'', p.&nbsp;47.</ref> The dominant science fiction magazines of the 1950s were ''[[Astounding Science Fiction|Astounding]]'', ''[[Galaxy Science Fiction|Galaxy]]'', and ''[[The Magazine of Fantasy and Science Fiction|Fantasy and Science Fiction]]'', but ''If'' was in the next rank in terms of quality:<ref name=TM_74>Ashley, ''Transformations'', p.&nbsp;74.</ref><ref name=TM_127>Ashley, ''Transformations'', p.&nbsp;127.</ref> SF historian [[Frank M. Robinson]], for example, describes ''If'' as the "most major of the minors".<ref name=Robinson_126>Robinson, ''SF of the 20th Century'', p.&nbsp;126.</ref> Well-known writers who appeared in ''If'' in the 1950s include [[Harlan Ellison]] and [[Arthur C. Clarke]]: the original short story version of Clarke's novel ''[[The Songs of Distant Earth]]'' appeared in the June 1958 issue. Isaac Asimov's widely reprinted story "[[The Feeling of Power]]" appeared in February 1958.<ref name=issues/>

{| class="wikitable" style="font-size: 10pt; line-height: 11pt; margin-left: 2em; text-align: center; float: right"
! !!Jan !! Feb !!Mar !!Apr !!May !!Jun !!Jul !!Aug !!Sep !!Oct !!Nov !!Dec
|-
!1962
|bgcolor=#fac090|11/6 || ||bgcolor=#fac090|12/1 || ||bgcolor=#fac090|12/2 || ||bgcolor=#fac090|12/3 || ||bgcolor=#fac090|12/4 || ||bgcolor=#fac090|12/5 ||
|-
!1963
|bgcolor=#fac090|12/6 || ||bgcolor=#fac090|13/1 || ||bgcolor=#fac090|13/2 || ||bgcolor=#fac090|13/3 || ||bgcolor=#fac090|13/4 || ||bgcolor=#fac090|13/5 ||
|-
!1964
|bgcolor=#fac090|13/6 || ||bgcolor=#fac090|14/1 || ||bgcolor=#fac090|14/2 || ||bgcolor=#fac090|14/3 ||bgcolor=#fac090|14/4 || ||bgcolor=#fac090|14/5 ||bgcolor=#fac090|14/6 ||bgcolor=#fac090|14/7
|-
!1965
|bgcolor=#fac090|15/1 ||bgcolor=#fac090|15/2 ||bgcolor=#fac090|15/3 ||bgcolor=#fac090|15/4 ||bgcolor=#fac090|15/5 ||bgcolor=#fac090|15/6 ||bgcolor=#fac090|15/7 ||bgcolor=#fac090|15/8 ||bgcolor=#fac090|15/9 ||bgcolor=#fac090|15/10 ||bgcolor=#fac090|15/11 ||bgcolor=#fac090|15/12
|-
!1966
|bgcolor=#fac090|16/1 ||bgcolor=#fac090|16/2 ||bgcolor=#fac090|16/3 ||bgcolor=#fac090|16/4 ||bgcolor=#fac090|16/5 ||bgcolor=#fac090|16/6 ||bgcolor=#fac090|16/7 ||bgcolor=#fac090|16/8 ||bgcolor=#fac090|16/9 ||bgcolor=#fac090|16/10 ||bgcolor=#fac090|16/11 ||bgcolor=#fac090|16/12
|-
!1967
|bgcolor=#fac090|17/1 ||bgcolor=#fac090|17/2 ||bgcolor=#fac090|17/3 ||bgcolor=#fac090|17/4 ||bgcolor=#fac090|17/5 ||bgcolor=#fac090|17/6 ||bgcolor=#fac090|17/7 ||bgcolor=#fac090|17/8 ||bgcolor=#fac090|17/9 ||bgcolor=#fac090|17/10 ||bgcolor=#fac090|17/11 ||bgcolor=#fac090|17/12
|-
!1968
|bgcolor=#fac090|18/1 ||bgcolor=#fac090|18/2 ||bgcolor=#fac090|18/3 ||bgcolor=#fac090|18/4 ||bgcolor=#fac090|18/5 ||bgcolor=#fac090|18/6 ||bgcolor=#fac090|18/7 ||bgcolor=#fac090|18/8 ||bgcolor=#fac090|18/9 ||bgcolor=#fac090|18/10 ||bgcolor=#fac090|18/11 ||bgcolor=#fac090|18/12
|-
!1969
|bgcolor=#fac090|19/1 ||bgcolor=#fac090|19/2 ||bgcolor=#fac090|19/3 ||bgcolor=#fac090|19/4 ||bgcolor=#fac090|19/5 || ||bgcolor=#e6b9b8|19/6 || ||bgcolor=#e6b9b8|19/7 ||bgcolor=#e6b9b8|19/8 ||bgcolor=#e6b9b8|19/9 ||bgcolor=#e6b9b8|19/10
|-
!1970
|bgcolor=#e6b9b8|20/1 ||bgcolor=#e6b9b8|20/2 ||bgcolor=#e6b9b8|20/3 ||bgcolor=#e6b9b8|20/4 ||bgcolor=#e6b9b8|20/5 || ||bgcolor=#e6b9b8|20/6 || ||bgcolor=#e6b9b8|20/7 || ||bgcolor=#e6b9b8|20/8 ||
|-
!1971
|bgcolor=#e6b9b8|20/9 || ||bgcolor=#e6b9b8|20/10 || ||bgcolor=#e6b9b8|20/11 || ||bgcolor=#e6b9b8|20/12 || ||bgcolor=#e6b9b8|21/1 || ||bgcolor=#e6b9b8|21/2 ||
|-
!1972
|bgcolor=#e6b9b8|21/3 || ||bgcolor=#e6b9b8|21/4 || ||bgcolor=#e6b9b8|21/5 || ||bgcolor=#e6b9b8|21/6 || ||bgcolor=#e6b9b8|21/7 || ||bgcolor=#e6b9b8|21/8 ||
|-
!1973
|bgcolor=#e6b9b8|21/9 || ||bgcolor=#e6b9b8|21/10 || ||bgcolor=#e6b9b8|21/11 || ||bgcolor=#e6b9b8|21/12 || ||bgcolor=#e6b9b8|22/1 || ||bgcolor=#e6b9b8|22/2 ||
|-
!1974
|bgcolor=#e6b9b8|22/3 || ||bgcolor=#d8d8d8|22/4 || ||bgcolor=#d8d8d8|22/5 || ||bgcolor=#d8d8d8|22/6 || ||bgcolor=#d8d8d8|22/7 || ||bgcolor=#d8d8d8|22/8 ||
|-
|colspan="13" style="font-size: 8pt; text-align:left"|Issues of ''If'' from 1962 to 1974, showing volume/issue number. Editors were<br/>Frederik Pohl (orange), Ejler Jakobsson (pink), and James L. Baen (gray).
|}The period under Pohl is regarded as the magazine's heyday; the three consecutive Hugo Awards won from 1966 to 1968 broke a long period in which the award had been monopolized by ''Analog'' (the name to which ''Astounding'' changed in 1960) and ''Fantasy and Science Fiction''.<ref name=Tuck_569/><ref name=ESF_609>Brian Stableford & Peter Nicholls, "If", in Peter Nicholls and John Clute, eds, ''The Encyclopedia of Science Fiction''</ref> Frank Robinson commented that "Pohl was the only one who was surprised when he won three Hugos in a row for editing ''IF''. It had been fun, and the fun had showed."<ref name=Robinson_129>Robinson, ''SF of the 20th Century'', p.&nbsp;129.</ref> Niven's "[[Neutron Star (short story)|Neutron Star]]" appeared in October 1966, and Harlan Ellison's "[[I Have No Mouth and I Must Scream]]" appeared in March 1967; both won Hugo Awards. Pohl also managed to secure a new Skylark novel, ''[[Skylark DuQuesne]]'', from [[E.E. Smith]]; the series had been started in the 1920s and was still popular with readers.<ref name=TM_274>Ashley, ''Transformations'', p.&nbsp;274.</ref> Pohl also bought [[A.E. van Vogt]]'s "The Expendables"; the story was van Vogt's first sale in 14&nbsp;years and attracted long-time readers to the magazine. Another coup was the serialization of three novels by [[Robert A. Heinlein]], including the award-winning ''[[The Moon Is a Harsh Mistress]]'', which ran in five parts from December 1965 to April 1966.<ref name=TM_210/>

Pohl's policy of publishing a story by a new writer in every issue led to a series called "''If''-firsts"; the first one, [[Joseph L. Green]]'s "Once Around Arcturus", about the courtship between a man and woman of different planets, appeared in the September 1962 issue. Several of the writers featured in the ''If''-first series, which were published from 1962 through 1965, became well-known, including [[Alexei Panshin]]; the most prominent was [[Larry Niven]], whose first story, "The Coldest Place", appeared in December 1964.<ref name="TM_208-9"/> Niven later remarked that the story was immediately outdated; the plot relied on the discovery that the dark side of [[Mercury (planet)|Mercury]] was the coldest place in the universe, but space probes had recently discovered that Mercury did in fact rotate asynchronously.<ref name=TM_209-10>Ashley, ''Transformations'', pp.&nbsp;209–210.</ref> [[Gardner Dozois]] also made his first sale to ''If'', with "The Empty Man", about a man possessed by an alien, in the September 1966 issue, and [[Gene Wolfe]]'s "Mountains Like Mice", about an abandoned group of colonists on Mars, appeared in the May 1966 issue. Technically this was not Wolfe's first sale, as he had already had "The Dead Man" published in the October 1965 issue of ''Sir!'', but "Mountains Like Mice" had been written earlier.<ref name=TM_275>Ashley, ''Transformations'', p.&nbsp;275.</ref>

''If'''s covers during the 1960s were typically action-oriented, showing monsters and aliens; and several of the stories Pohl published were directed at a younger audience. For example, Blish's ''Welcome to Mars'', serialized under the title ''The Hour Before Earthrise'' in July to September 1966, was about a teenage genius whose antigravity device stranded him and his girlfriend on Mars.<ref name=issues/><ref>{{cite web | url = http://www.isfdb.org/cgi-bin/pl.cgi?WOFIFJUL1966| title = Publication Listing| accessdate=25 February 2008|publisher = Texas A&M University}}; {{cite web | url = http://www.isfdb.org/cgi-bin/pl.cgi?WOFIFAUG1966| title = Publication Listing| accessdate=25 February 2008|publisher = Texas A&M University}}; and {{cite web | url = http://www.isfdb.org/cgi-bin/pl.cgi?WOFIFSEP1966| title = Publication Listing| accessdate=25 February 2008|publisher = Texas A&M University}}</ref> Ashley has suggested that ''If'' was attempting to acquire readership from the many new fans of science fiction who had been introduced to the genre through television, in particular via the popular 1960s shows ''[[Doctor Who]]'' and ''[[Star Trek]]''. ''If'' also ran a friendly letter column, with more fan-oriented discussions than the other magazines, and between 1966 and 1968 a column by [[Lin Carter]] introduced readers to various aspects of [[science fiction fandom]]. These features are also likely to have appealed to a younger audience.<ref name=TM_274/>

==Bibliographic details==
[[File:If cover variations.jpg|thumb|left|Twelve issues of ''If'', showing the major variations in cover design over the magazine's lifetime]]
''If'' was a digest-sized magazine throughout its life. It began at 164 pages and with only the fifth issue, November 1952, dropped to 124 pages. The page count increased again to 134 pages with the July 1959 issue, and to 164 pages with the September 1965 issue; it stayed at this length until the September–October 1970 issue. The page count was then increased to 180 with the June 1971 issue, and to 164 for the very last issue, December 1974.<ref name=issues/><ref name=Tuck_569/><ref name=page_count>The page count includes both the front and back covers; some references such as Tuck only count the pages between the covers. The magazine itself was inconsistent about this: for example the September 1969 issue treated the first page inside the cover as page 1, but July 1969 issue counted this as page 3, making the front cover page 1.</ref> It was priced at 35 cents to begin with, and increased to 40 cents with the March 1963 issue, to 50 cents with the December 1964 issue, to 60 cents with the August 1967 issue, and finally to 75 cents with the September–October 1970 issue.<ref name=issues/><ref name=Tuck_569/> With the April 1972 issue, UPD began using card stock for the covers, rather than paper, and continued to do so until the magazine ceased publication.<ref name=issues>See the individual issues. For convenience, an online index is available at {{cite web | url = http://www.isfdb.org/wiki/index.php/Magazine:If | title = Magazine:If&nbsp;— ISFDB | accessdate=23 February 2008 |publisher=Texas A&M University}}</ref>

The magazine was bimonthly until the March 1954 issue, which was followed by April 1954, inaugurating a monthly period that ran until June 1955. This was followed by August 1955, resuming a bimonthly schedule that ran until July 1964, with only one irregularity, when the February 1959 issue was followed by July 1959. After July 1964, ''If'' ran a monthly schedule until April 1970, with three omissions: there were no issues dated September 1964, June 1969, or August 1969. From May–June 1970, the issues were bimonthly and bore the names of two months. This bimonthly sequence ran through the last issue at the end of 1974. The date the magazine printed on the cover reverted to a single month with the June 1971 issue, though the contents page still used two months to identify the issue. The volume numbering began with six issues to a volume: there were three errors on the magazine contents page, with volume 8 number 1 incorrectly printed as volume 7 number 6; volume 9 number 3 printed as volume 8 number 6; and volume 10 number 1 printed as volume 10 number 6. Volume 14, which began in March 1964, ran through the end of the year, with seven numbers; the remaining volumes had 12 numbers each except for volume 19 which had 10 and volume 22 which had 8.<ref name=issues/><ref name=Tuck_569/><ref name=NESF_303>Brian Stableford, "If", in Peter Nicholls, "Encyclopedia of Science Fiction", p.&nbsp;303.</ref>

Several British editions of ''If'' were produced. In 1953 and 1954, Strato Publications reprinted 15 issues, numbering them from 1 to 15; another 18 were reprinted between 1959 and 1962, with the issue numbers started at 1 again. Between January and November 1967 a UK edition appeared from Gold Star Publications; these were identical to the US edition dated ten months previously. Between 1972 and 1974, 15 of the UPD editions of ''If'' were imported, renumbered and repriced for UK distribution. The numbering, inexplicably, ran from 1 to 9, and then 11, 1, 13, 3, 4 and 5.<ref name=ESF_609/>

The editorial succession at ''If'' was as follows:<ref name=issues/><ref name=Contento_WoI/>
* [[Paul W. Fairman]]: March–September 1952.
* [[James L. Quinn (editor)|James L. Quinn]]: November 1952&nbsp;– August 1958. From May 1953 to March 1954, [[Larry Shaw (editor)|Larry T. Shaw]] was Associate Editor; he wrote editorials for at least three issues, beginning with September 1953, and generally did most of the editorial work.<ref name=issues/>
* [[Damon Knight]]: October 1958&nbsp;– February 1959.
* [[H.L. Gold]]: July 1959&nbsp;– November 1961.
* [[Frederik Pohl]], January 1962&nbsp;– May 1969.
* [[Ejler Jakobsson]]: October 1969&nbsp;– January/February 1974
* [[Jim Baen]]: March/April–December 1974.
* Clifford Hong: September/November 1986.
Eight selections of stories from ''If'' have been published. Two were edited by Quinn: ''The First World of If'' (1957) and ''The Second World of If'' (1958); four by Pohl: ''The Best Science Fiction from If'' (1964), ''The If Reader of Science Fiction'' (1966), ''The Second If Reader of Science Fiction'' (1968), and ''Worlds of If'' (1986); and two by Jakobsson, both published as by "The Editors of If": ''The Best from If'' (1973) and ''The Best from If Vol II'' (1974).<ref name="Tuck_569" /><ref name="ESF_942-4">Brian Stableford, "Frederik Pohl", in Clute & Nicholls, eds., ''Encyclopedia of Science Fiction'', pp.&nbsp;942–944.</ref><ref name="ESF_637">Malcolm Edwards, "Ejler Jakobsson", in Clute & Nicholls, eds., ''Encyclopedia of Science Fiction'', p.&nbsp;637.</ref><ref name="Contento_Pohl">{{cite web | url = http://www.philsp.com/homeville/ISFAC/b25.htm#A617| title = Books, Listed by Author | accessdate=1 March 2008|publisher = Phil Stephenson-Payne}}</ref>  In addition, two anthologies drew all their contents from ''If'' without mentioning the magazine: ''The 6 Fingers of Time and 5 Other Science Fiction Novelets'' (1965) and ''The Frozen Planet and Other Stories'' (1966).  Both were edited by Samuel H Post, who was not credited.<ref>{{Cite web|url=http://sf-encyclopedia.com/entry/if|title=Culture : If : SFE : Science Fiction Encyclopedia|last=Stableford|first=Brian|last2=Ashley|first2=Mike|website=sf-encyclopedia.com|access-date=2016-04-08|last3=Nicholls|first3=Peter}}</ref>

== See also ==
* ''[[Esli]]'', {{lang-ru|Если}}, meaning "If"

==Notes==
{{Reflist|30em}}

==References==
{{refbegin}}
* {{cite book | first=Michael | last=Ashley | title=The History of the Science Fiction Magazine Vol. 3 1946–1955| publisher=Contemporary Books, Inc.| location=Chicago | year=1976 |isbn= 0-8092-7842-1}}
* {{cite book | first=Michael | last=Ashley | title=The History of the Science Fiction Magazine Part 4 1956–1965| publisher=New English Library| location=London | year=1978 | isbn= 0-450-03438-0}}
* {{cite book | first=Mike | last=Ashley | title=Gateways to Forever: The Story of the Science-Fiction Magazines from 1970 to 1980| publisher=Liverpool University Press| location=Liverpool| year=2007 | isbn= 978-1-84631-003-4}}
* {{cite book | first=Mike | last=Ashley | title=Transformations: The Story of the Science Fiction Magazines from 1950 to 1970| publisher=Liverpool University Press| location=Liverpool| year=2005 | isbn= 0-85323-779-4}}
* {{cite book|last=Clute|first= John|author2=Nicholls, Peter| title= The Encyclopedia of Science Fiction| year=1993| publisher= St. Martin's Press, Inc.| location= New York| isbn= 0-312-09618-6}}
* {{cite book|last= Nicholls|first= Peter| title= The Encyclopedia of Science Fiction| year=1979| publisher= Granada Publishing| location= St Albans| isbn= 0-586-05380-8}}
* {{cite book | first=Frederik | last=Pohl | title=The Way the Future Was| publisher=Gollancz| location=London | year=1979 |isbn= 0-575-02672-3}}
* {{cite book |title=Science Fiction of the 20th Century: An Illustrated History |last= Robinson|first=Frank M. |year= 1999|publisher=Barnes & Noble |location=New York |isbn=0-7607-6572-3}}
* {{cite book | first=Donald H. | last=Tuck | title=The Encyclopedia of Science Fiction and Fantasy | volume=3 | publisher=Advent: Publishers, Inc. | location=Chicago | year=1982 | isbn= 0-911682-26-0}}
{{refend}}

==External links==
{{Commons category}}
* [http://www.philsp.com/mags/if.html Gallery of ''IF'' cover images]
* [http://www.isfdb.org/wiki/index.php/Series:If ''If'' Magazine] on the [[Internet Speculative Fiction Database]]
* [https://archive.org/details/ifmagazine Complete scans of ''If'' Magazine] at the [[Internet Archive]]

{{featured article}}

[[Category:Magazines established in 1952]]
[[Category:Magazines disestablished in 1974]]
[[Category:Defunct science fiction magazines of the United States]]
[[Category:Science fiction magazines established in the 1950s]]
[[Category:Magazines established in 1986]]
[[Category:Magazines disestablished in 1986]]
[[Category:Magazines published in New York City]]