<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=T.II
 | image=US Navy Fokker FT-1 in flight near NAS Norfolk in April 1923.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Torpedo bomber]]
 | national origin=[[Netherlands]]
 | manufacturer=[[Fokker|Fokker-Flugzeugwerke]]
 | designer=
 | first flight=1921
 | introduced=1922
 | retired=ca. 1926
 | status=
 | primary user=[[US Navy]]
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Fokker T.II or T.2''' was a single engine [[floatplane]] designed in the [[Netherlands]] in the early 1920s as a [[torpedo bomber]].  Three were bought by the [[US Navy]] who tested them against other aircraft from the [[US]] and the [[UK]].  The T.IIs did not win further production orders but remained in service for several years.

==Design and development==

Fokker's T-designation included both [[bomber]]s and [[torpedo bomber]]s; the T.II was the first of this series, as the T.I was an unbuilt project.  Three were ordered by the [[US Navy]] early in 1921 and completed towards the end of that year.  Air power enthusiast General [[Billy Mitchell]] visited the Fokker works at [[Veere]] in early 1922.  General [[Clayton Bissell]], traveling with him, was encouraged by Fokker to fly the T.II.  He reported it unresponsive to the controls; Fokker responded by having about a meter of the rear fuselage cut out and the structure re-welded, which cured the problem.  The three T.IIs were delivered to the US later in 1922, where they were given the designation '''FT-1''' (Fokker torpedo).<ref name=DNV/>

The T.II was a [[cantilever]] [[monoplane#Types|low wing monoplane]] with straight tapered, square tipped wings. [[Aileron#Horns and aerodynamic counterbalances|Overhung ailerons]] were used.  The [[fuselage]] was flat topped and sided and deep from tail to nose, where a 400&nbsp;hp (300&nbsp;kW) [[Liberty 12A]] water-cooled [[V-12 engine]] drove a two blade [[propeller (aircraft)|propeller]].  The two crew sat in [[tandem]], separate, open round [[cockpit]]s over the wing. The [[tailplane]] was mounted on top of the fuselage; together, it and the [[elevator (aircraft)|elevator]]s were, like the wings, straight tapered in plan and square tipped.  The [[fin]] and [[rudder]] were quite short but the latter extended to the deep keel.  The T.II's twin float undercarriage was about 70% of the aircraft's length, projecting well forward of the nose. The floats were mounted on the fuselage by N-struts, two pairs, with diagonal transverse bracing between them, on each float. There were no transverse interfloat struts, as required by the torpedo dropping role.<ref name=DNV/>

The T.II's defensive armament was a single machine gun in the rear cockpit.  Its offensive [[torpedo]] was mounted externally on the fuselage between the floats.<ref name=DNV/>

==Operational history==
The US Navy conducted comparative tests of several types at their [[Anacostia]] base. The competitors in addition to the Fokker were the US [[Curtiss CT-1]], [[Douglas DT]]-1, [[Stout ST-1]] and the UK [[Blackburn Swift F]]. The Douglas machine won the production order and the Fokkers went into service at the [[Naval Air Station]], [[Hampton Roads]], remaining there until about 1926.<ref name=DNV/>

==Variants==
;T.II: Company designation of the torpedo bomber evaluated by the US Navy as the '''Fokker FT'''
;FT-1: The US Navy's designation for the T.II as delivered. 
;FT-2: The third aircraft modified by the US Navy.

==Operators==
;{{flag|USA|country}}
*[[US Navy]]

==Specifications==
{{Aircraft specs
|ref=Wesselink 1982<ref name=DNV/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Two
|length m=15.56
|length note=
|span m=19.83
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=3314
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Liberty 12A]] 
|eng1 type=water-cooled [[V-12 engine]]
|eng1 hp=400
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=167
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=

|more performance=
<!--
        Armament
-->
|armament=
*[[Machine gun]] in rear cockpit
*[[Torpedo]]<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|other armament=

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{commons category|Fokker T.II}}
{{reflist|refs=

<ref name=DNV>{{cite book |title= De Nederlandse vliegtuigen |last=Wesselink|first=Theo|last2= Postma|first2=Thijs|year=1982|publisher=Romem  |location=Haarlem |isbn=90 228 3792 0|page=24}}</ref>

}}
{{Fokker aircraft}}
{{USN torpedo aircraft}}

[[Category:Floatplanes]]
[[Category:Fokker aircraft|T.II]]
[[Category:Dutch military aircraft 1920–1929]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]