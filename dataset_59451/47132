{{Infobox mountain pass
| name= Col du Corbier 
| photo = Col du corbier 26 juin 2013 pancarte.JPG
| photo_caption = Panel view showing the top of the pass
| elevation_m = 1237
| range= [[Massif du Chablais]]
| coordinates = {{coord|46.2836|N|6.6444|E|source:wikidata-and-enwiki-cat-tree_region:FR|format=dms|display=inline,title}}
| country= France
| Valley1= [[Aulps Valley]]
| valley2= [[Abondance Valley]]
| loc1= North
| loc2= South
| town1= [[Le Biot]]
| town2= [[Bonnevaux (Haute-Savoie)|Bonnevaux]]
| access1= D 32
| access2= D 32

| geolocation= France
}}
''' Col du Corbier ''' is a French Alpine pass located in [[Haute-Savoie]] [[Departments of France|department]] in the [[Rhône-Alpes]] [[Regions of France|region]] in southeastern [[France]] between the towns of [[Le Biot]] and Bonnevaux (Haute-Savoie).
Col du Corbier is also the first name of the ski resort located on this site from 1966 to 1975, before being renamed Drouzin-le-Mont.
[[File:Col du corbier panorama sous ouzon.jpg|thumb|Panoramic view of the pass below the Mont Ouzon]]

== Geography ==

=== Location ===
Col du Corbier is located at 1,237m above sea level, about 26&nbsp;km from [[Thonon-les-Bains]], in the massif du Chablais, in the northwest quarter of the [[French Alps]], France.
Col du Corbier is located between two mountains: to the North Mount Ouzon culminating at 1,880 m altitude and to the South Drouzin mountain which rises to 1,621 m.

=== Climate ===
The climate in the pass is of the mountainous type. [[File:Col du Corbier 1961 avant la création de la station.jpg|thumb|left|Col du Corbier 1961 before the creation of the resort]]

== History ==

=== Historical Significance ===
The pathway of the Corbier was once very important in connecting the two valleys of Savoy Dranse and Valaisane Dranse, by the Morgins Pass. St. Guerin, founder of the Abbey of Aulps and [[Bishop of Sion]] passed there, and it was followed by many pilgrims, hence the given name of "pathway of the monks.".<ref>Claude Chatelain, ''Bonnevaux Vacheresse Chevenoz au fil du temps'', Imprimerie Sopizet, 1995, p. 40 et 181.</ref>
[[File:Col du corbier notre dame des sept douleurs 26 juin 2013.JPG|thumb|Col du corbier the Chapel our lady of sorrows]]
On the Bonnevaux territory, by the roadside about 5600 m below the pass, is the Chapelle Notre-Dame-des-Sept-Douleurs de Bonnevaux|or Our Lady of Sorrows registered in the cultural heritage's general inventory of the Rhône-Alpes region. Erected in 1844 by Rev. Girod, pastor of Bonnevaux, it was partially restored several times and fully restored in 1982. The bell of 1910 is called "the Virgin."
.<ref>Inventaire général du patrimoine culturel [http://www.culture.gouv.fr/public/mistral/mersri_fr?ACTION=CHERCHER&FIELD_1=REF&VALUE_1=IA00125112 Chapelle Notre-Dame-des-Sept-Douleurs], sur le site www.culture.gouv.fr.</ref>
The road was created in 1917 and redone in 1940; it has since been widened and tarred. Until 1964, the pass served as pasture for cows of the Abondance breed going to or returning from higher pastures. The large trough served not only for watering the cows but also for cooling the engines of cars that overheated going up the steep slopes. The road was plowed in winter on the Le Biot in 1964 and on the two sides in 1966.

=== First buildings (1961) ===
[[File:Col du Corbier Haute-Savoie, France, restaurant chez l'henri 1965.jpg|thumb||left|View of the Col du Corbier with "Chez L' Henri"]]
In April 1961, the town of Le Biot allowed Henri Teninge, restaurant owner in Morzine, originally from Bonnevaux, to build a café restaurant "Chez L'Henri" on a plot of 4945 m2 belonging to the town at a place called "On the Joux." In 1967, the town sold it to him for one franc per square meter. [[File:Col du Corbier 1961 restaurant la covagne et croix.jpg|thumb|Col du Corbier 1961 restaurant la covagne and the cross]]

At the end of 1961, André Favre-Miville of Bonnevaux built a bar on his own land, "The Covagne".
Both houses were entirely of wood without electricity or telephone, the waste was buried or burned, sewage going into a septic tank. Clean water came from ground springs in the pass. Until 1964, the restaurants were closed in the Winter.

=== Early Development (1966-1968) ===

[[File:Col du corbier Haute-Savoie France 1966 premier teleski.jpg|thumb|Col du corbier Haute-Savoie France 1966 first ski-lift]]
In 1966, Henri Teninge, assisted by Bernard Vulliez and authorized by the municipality, had the first ski tow (with generator) installed by the firm Montaz-Montino of Fontaine on the hill opposite his restaurant, under the Drouzain mountain.
The ski-lift started from the pass, was 200m long and had a capacity of 305 skiers per hour. The first ski race was organized by Henri Teninge and Georges Premat, mayor of Le Biot, with 45 competitors <ref>« Affluence record au concours de ski du col du corbier », ''[[Le Dauphiné Libéré]]'', février 1967.</ref>
In 1967, the commune considered building an urban development zone, but "the administration opposed to it in order to preserve a typical alpine site.".<ref>''[[Le Messager (France)|Le Messager]]'', 2 novembre 1967.</ref> A ski club was created in Le Biot. In February 1968, the Olympic flame was carried through the Col du Corbier. In August 1968 a public telephone was installed "Chez L'Henri."

=== Birth of the ski resort "Col du Corbier" (1969-1975) ===

The idea of a bigger station made its way within the Le Biot municipality and its mayor, Fernand Renevier. They launched a study and prepared a very modest project which was rejected by the tax collector who was very reserved about the financial resources of the commune and wished to "preserve a typical alpine site." The two towns of Le Biot and Bonnevaux therefore united in 1969 thanks to Messrs Cloppet and Mr.Lachenal, of the mountain commission, to create on January 23, 1970, an "inter-communal syndicate for the touristic development of the Corbier - La Joux Verte - Le Biot - Bonnevaux." The committee was composed of four directors under 40 years of age: René Ainoux (Bonnevaux), president, Georges Premat (Le Biot), vice-president, Gerard Delale (Le Biot), secretary, and Paul Favre-Miville (Bonnevaux). The latter became brother Paul, one of the seven monks murdered in Tibhirine Algeria in 1996 (a monument has been erected in Bonnevaux' cemetery).
"The shares were distributed as follows: 55% in Biot, 45% Bonnevaux and the protection perimeter defined: 863 hectares for Biot, 467 hectares Bonnevaux." Each municipality provided a 68.000 francs subsidy and the syndicate borrowed 1.150.000 francs from the [[Credit Agricole]] bank.

The committee reviewed the project, 24 homeowners gave a right of way, the departmental electricity union, thanks to the action of Messrs Lavy, Germain and Vannod, agreed to the establishment of an underground line of 15.000 volts, including two transformers.<ref>« Unies dans un même effort, les communes de Bonnevaux et du Biot ont fignolé l'aménagement du col du Corbier », ''[[Le Dauphiné libéré]]'', 27 novembre 1971.</ref>

For Christmas 1970 the firm Montagner, of Allonzier-la-Caille installed four lifts adding up to three kilometers of runs: a 1.055.m long lift, to a vertical drop of 265 m and a flow rate of 600 skiers per hour, and a 300 m long lift were installed at the Muret while in the pass, two lifts of 250 m and 200m, were built near the chalet occupied by the syndicate and the ski school managed by Michel Renevier. The staff included ten locals; the day pass was 10 francs. The municipalities widened the road to 14 meters and created a parking lot of 350 cars.<ref>« Le Syndicat Intercommunal du Corbier - La Joux-Verte aura quatre remontées mécaniques pour Noël », ''[[Le Dauphiné libéré]]'', 27 novembre 1970.</ref>

A team of architects composed of Messrs Branch Bonvalet Dutruel and Marchant, an economist and Dr. Rouille were responsible for drawing up plans for a large building zone in La Reblais, not far from the pass, under the mountain of Ouzon. Two forest roads were envisaged, one to Mount Ouzon and the other to Mount Drouzin.
The Le Biot school closed in June 1971 because there were only nine children (10 were needed for a class). This led to the mayor Fernand Renevier's resignation; he was replaced by Georges Premat. The school reopened in September 1972.

Winter 1971-1972 was the first official season for the Col du Corbier. Trees were cut down, the runs were remodelled on the advice of Jean Berthet (ski champion from Abondance) with the firm Vulliez-Noir de Marin. The run the Muret was approved for competitions by the French Ski Federation. The real estate extension was envisaged: "it is not a question of doubling the big resorts nearby, but if we want, to complete them on the theme: family and social skiing ".<ref>« Unies dans un même effort, les communes de Bonnevaux et du Biot ont fignolé l'aménagement du col du Corbier », ''[[Le Dauphiné libéré]]'', novembre 1971.</ref>
In 1973, water supply works amounted to 600.000 francs (150.000 francs subsidies under the rural renewal scheme) and 500.000 francs for the purification work (100.000 francs subsidies) <ref name="LM aout75">« Projet de construction d'un mini-Avoriaz de 2500 lits! », ''[[Le Messager (France)|Le Messager]]'', 22 août 1975.</ref> The water supply works were ongoing between 1974 and 1975. The municipality committed 600.000 francs (150.000 francs subsidies) and 100.000 francs for rural electrification work (20.000 francs subsidies).<ref name="LM aout75"/>
In early 1974, André Favre-Miville, the former owner of "The Covagne" (which was sold in 1970 to George Premat mayor of Le Biot) died. In July, Henry Teninge, owner of "Chez L'Henri," died in his turn. His restaurant was sold in November 1974 to Michel Renevier and renamed "Chez Michel and Mado." It completely burned in the night of July 11, 1975 <ref>« Grave incendie au Col du Corbier », ''[[Le Messager (France)|Le Messager]]'', 17 juillet 1975.</ref> so in its place, the owner began building a large hotel restaurant.

=== Ski resort "Drouzin-le-Mont" (1975) ===

In 1975, the station of the "Col du Corbier" became "Drouzin-le-Mont."
The municipality obtained a 390.000 francs subsidy for the installation of the telephone and another for 350.000 francs for servicing the entire pass. A property complex of thirty cottages was built in the Grands Prés, under the pass.
An extension of the resort was planned, which became Drouzin-le-Mont. The syndicate committed five million francs to service the Urban Development Zone: "two pumping stations, a water-treatment and all the necessary networks. A big investment for two small towns! ".<ref>« Projet de construction d'un mini-Avoriaz de 2.500 lits », ''[[Le Messager (France)|Le Messager]]'', 22 août 1975.</ref>
Then the projects were scaled down, a ZAC (Urban Development Zone) was created in 1975 for housing 300 units or 1000 beds.

=== Evolution of the ski resort (1976-2011) ===

==== Real estate ====

Between 1976 and 1996, Michel Renevier obtained a permit to rebuild the restaurant "Chez Michel and Mado", but various administrative difficulties led to cancellations and setbacks. The crane was on site for 15 years. In 1997, "in the Col du Corbier, there has been an unfinished restaurant for a good twenty years, let alone the surroundings of the building." <ref>« Les Verrues du Chablais », ''[[Le Messager (France)|Le Messager]]'', 13 mars 1997.</ref>

==== Crisis of the snow coverage ====

Culminating at {{convert|1237|m|ft}} and under the moderating influence of [[lac Léman]], the Col du Corbier knows only a modest snow coverage, which had entailed the reserves of the Mountain Commission: "O.K. to a family resort, but frankly we say no to more grandiose achievements." The syndicate considered for 1981 the creation of a chair lift to go up to 1700m on Drouzin, but the permit was refused.<ref>« Station refusée au col du Corbier », ''[[Le Messager (France)|Le Messager]]'', 7 novembre 1980.</ref>

==== Urban Development zone ====

A new project for a property complex was denied October 24, 1980 by the Commission on new tourism units (UTN) on the basis of the resort's uncertain financial future.<ref name="LM novembre81">P.-M Lemaire, « Vivre aujourd'hui au Biot », ''[[Le Messager (France)|Le Messager]]'', 13 novembre 1981</ref>
The change of national government in 1981 left the situation on hold. Georges Premat, mayor at that time, said: "all the constructions are and will be made under the control of the syndicate which does not intend to sacrifice a magnificent site to the appetite of one promoter."<ref name="LM novembre81"/>
The mayors - Georges Premat for Le Biot and Gilbert Favre-Derez for Bonnevaux - put pressure on the administration because every year the blocked projects cost 460.000 francs of annuities. Elected officials resigned en bloc. The builder Jacques Ribourel added more property.

==== Crisis in the council of Le Biot (1993) ====

In July, the regional audit called for a budget increase of an additional 600.000 francs (it is a quarter of the initial budget of the municipality) for works at the Col du Corbier. Mayor George Premat resigned (two councilors Prévot Annick and Jean-Michel Laurier had already resigned). Additional municipal elections were held in September.<ref>« Le Biot feint ignorer la crise municipale », ''[[Le Messager (France)|Le Messager]]'', 10 septembre 1993.</ref>

==== Financial difficulties and sale of the resort (1994-1998) ====

In February, the electricity was cut off due to non-payment of a 540.000 francs bill dating back to 1986 which had grown over the years. The two communes could not pay and the General Council could not intervene because the operator was solely responsible. Deputy [[Pierre Mazeaud]] intervened and power was restored during the day.
Proceeds from the winter season were 620.000 francs in 1993. The accumulated debt in February 1994 was a billion pennies<ref>« Les difficultés de Drouzin le Mont », ''[[Le Messager (France)|Le Messager]]'', 11 février 1994.</ref>
On December 18, 1998 - so it could open the next day - the ski resort "Drouzin-le-Mont" was sold to a builder, Michel Vivien for 400.000 francs. He had to insure the functioning and the maintenance of the installations for at least 18 years as stipulated in the lease agreement of the same date. This agreement also contained a clause which allowed Mr. Vivien to close the resort in case of chronic deficit. He did not have the right to dismantle the installations and whether he intended to sell, he would be subjected to the syndicate's preemption right. The developer had already built 40 cottages, he was planning to build 32 more in 1999.<ref>« Rachat de la station du col du Corbier », ''[[Le Messager (France)|Le Messager]]'', 18 mars 1999.</ref>
[[File:Col du corbier retenue collinaire.JPG|thumb|left|Col du corbier water reservoir]]A 3.000 sq.m. water reservoir of 20.000 cubic meters was built to power a series of artificial snow-making devices. A two million franc investment was realized by Michel Vivien. The parking lot was enlarged and reorganized, a summer sledging strip was created, runways were remodeled, numerous chalets were built including a shelter for skiers and tourist information. The staff consisted of 15 locals.

==== Dissolution of the Syndicat inter-communal for the development Corbier – La Joux Verte (2004) ====

On January 30, by prefect's order Nber 2004.31, the syndicate was dissolved. Assets and liabilities were divided between the municipalities of Bonnevaux and Le Biot according to contractual terms. Bonnevaux inherited the land behind the Chapel of our Lady of Sorrows.

==== Development Drouzin-le-Mont SARL and PLU (2005 à 2011) ====

In 2005, the town of Le Biot made some changes to the lease agreement with Mr. Vivien signed in 1998. The SARL Development Drouzin-le-Mont was responsible for the operation of ski lifts and the ski area.<ref name="Mairiede">Mairie de Le Biot, Conseil Municipal, 12 octobre 2012.</ref>
[[File:Col du Corbier Drouzin le mont Affiche 40ème anniversaire.jpg|thumb|Col du Corbier Drouzin-le-mont Poster for fortieth anniversary]]
On February 17, 2007 the resort Col du Corbier / Drouzin-le-Mont celebrated its fortieth anniversary (1966 - 2006).

In 2011, a simplified revision 1 of the Local urbanization planning (PLU) "Opening the urbanization of the area AU in the Col du Corbier." was opened for public observations. It planned five building sectors in the new zone created by the revision on a surface of 1.50 ha namely the area around the chapel of our Lady of Sorrows: a public square, shops and services, collective housing and several chalets. The project received a favorable opinion from Christian Gosseine, inquiry commissioner, despite opposition from various ecological groups, for example the Association [[Mountain Wilderness]] who was outraged by the project of the extension "in an area already so disfigured".<ref>Despitch Olivier http://www.mountainwilderness.fr/se-tenir-informe/actualites/drouzin-le-mont-vous-remettez</ref><ref>E. Rouxel, « Construction de nouveaux logements » une question difficile pour les stations », ''[[Le Messager (France)|Le Messager]]'', 10 mars 2011.</ref>
By decision dated May 20, 2011, the town of Le Biot approved the project.<ref>''[[Le Messager (France)|Le Messager]]'', 23 juin 2011.</ref> However, the request for a building permit was not made on time, and in the meantime the Schéma de cohérence territoriale|SCOT Chablais became official and overruled the PLU, which became invalid.
In 2011, there were 562 secondary residences for 179 main residences. 85% of secondary residencies in the town of Le Biot were in the col du Corbier.

=== Closure of the ski resort "Drouzin-le-Mont" (2012) ===

On April 3, 2012, the Development Drouzin-le-Mont SARL informed the municipality about its wish to stop its activity arguing a structural deficit financial situation.<ref name="Mairiede" /><ref>"Fermeture de la station de Drouzin-le-Mont : ambiance tendue dans la commune" E.Rouxel,''[[Le Messager (France)|Le Messager]]'' 23 août 2012</ref>
The information was relayed to the national and international levels because it raises the global warming issue with regards to moderate altitude ski resorts in the Alps.<ref>Climate change prompts French ski area to mulldownsize http://www.themalaysianinsider.com/travel/article/climate-change-prompts-french-ski-area-to-mull-downsize</ref>
A study highlighted that the exploitation deficit could amount to €450.000 plus annual investments from €100.000  to €150.000  as opposed to an income estimated between €150.000 and €170.000 per season. The village of Le Biot could not afford such an expense, so the municipality decided against buying the resort from Mr Vivien, but the tension caused the resignation of seven town councilors.<ref>Mairie du Biot, Communiqué du 15 octobre 2012.</ref>

=== Tourist reconversion ===

[[File:Col du Corbier 26 juin 2013 restaurant chez michel et mado.JPG|thumb|View of the Col du Corbier with "Chez Michel and Mado" June 2013]]

In March 2013, the new council chose to convert the resort to a so-called "environmental friendly mountain". With the support of the Haute-Savoie General Council. It acquired the reception center and adjacent grounds, two lifts, the rope tow and maintenance equipment in order to set up several Summer and Winter activities such as skiing, dog sledding, snowshoeing, hiking...SIVU Roc D'Enfer redeemed SARL both chairlifts and received on 7 October 2013 a subsidy for 80% of the cost of their dismantling and reassembling i.e€.193.000.
The council proposed to Michel Renevier to purchase his restaurant. Following his refusal, the council decided to turn the reception center into a café-restaurant, grocer's shop, pic-nic area. The application for a building permit was filed in July 2013.
For the entire conversion project, in the amount of 2.117.550 €, the General Council granted on 18 November 2013 a subsidy for 40% of this total, i.e.847.020 €. But the two lifts will not open for the 2013-2014 season.<ref>YT, "Drouzin-le-Mont: the General Council supports the project to convert the station,"'' [[The Dauphiné Libéré]]'' ,22 November 2013</ref>

== Activities ==

=== Sports ===

==== Cycling ====

[[File:Col du corbier route entre bonnevaux et le col.JPG|thumb|Col du corbier the road between bonnevaux and the summit]]
The pass is popular with cyclists and mountain bikers. Between the Solitude in the Abondance valley and the pass, there are {{convert|5.7|km|mi}} of ascent in 7.1%(difference in altitude: 407 m). Between Seytroux in Morzine valley and the pass Col du Corbier there are {{convert|6.0|km|mi}} of ascent in 8.6% (difference in altitude: 517 m).
The Tour de France passed seven times between 1947 and 1978.<ref>[http://ledicodutour.perso.sfr.fr/montagnes/cols__c/corbier.htm Le dico du Tour - Le col Corbier dans le Tour de France].</ref>
On June 10, 2012, the stage Morzine - Chatel of the cycling race the Critérium du Dauphiné passed through the col du Corbier.<ref>''[[Le Dauphiné Libéré]]'', édition du 8 juin 2012.</ref>

==== Hiking ====

From the pass, there are many opportunities for hiking in summer and winter on Mount Ouzon, Mount Écuelle or Mount Drouzin <ref>[http://florechablais.fr/randonnees/requetes/req_list_randos.php?depart=46&lieudep=Col%20du%20Corbier Liste des randonnées] sur le site florechablais.fr.</ref>

==== Climbing ====

Several cliffs offer the practice of climbing, from the hamlet of Le Corbier: cliff de l'ours, cliff Touvière, or from the pass: the cliff Arblay <ref>[http://www.lebiot.fr/-Site-d-escalade-falaise-du-pas-de- Mairie de Le Biot - Sites d'escalade]</ref>

=== Environmental protection ===

In 2007, the Inventaire national du patrimoine naturel created a [[Zone naturelle d'intérêt écologique, faunistique et floristique]] (ZNIEFF) type II Mount Ouzon of an area of 1552.ha, including a portion of the road on each side of the pass and the entire North side, and 22 critical species<ref>Inventaire national du Patrimoine naturel, [http://inpn.mnhn.fr/zone/znieff/820031588 ZNIEFF 820031566 - Mont Ouzon], sur le site inpn.mnhn.fr.</ref>

== References ==

{{Reflist}}

== Bibliography ==
* http://rhone-alpes.france3.fr/2012/12/13/la-station-du-biot-restera-fermee-163759.html

== External links ==

* Much of the content of this article comes from the [[:fr:Col du Corbier|equivalent French-language Wikipedia article]].
* [http://www.lebiot.fr Le Biot's official site]
* [http://www.siac-chablais.fr SIAC Chablais Inter-communal site]
* [[List of mountain passes#Europe|List of mountain passes]]
* [http://www.cg74.fr/pages/fr/menu-general/l-institution/les-actes-de-l-assemblee/les-deliberations-de-la-commission-permanente-350.html Conseil Général de Haute-Savoie - Proceedings of the Permanent Commission]

[[Category:Mountain passes of Auvergne-Rhône-Alpes]]