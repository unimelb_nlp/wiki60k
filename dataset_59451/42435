<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=BZ-1
 | image=Bowlus BZ-1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=[[United States]]
 | manufacturer=
 | designer=[[Michael Bowlus]]
 | first flight=September 13, 1984
 | introduced=1983
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=One
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Bowlus BZ-1''' is an [[United States|American]] single seat [[FAI 15 Meter Class]], [[V-tail]]ed [[Glider (sailplane)|glider]] that was designed and built by [[Michael Bowlus]].<ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?PlaneID=160|title = HP-11 Airmate HP Aircraft, LLC |accessdate = 4 May 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 7. [[Soaring Society of America]] November 1983. USPS 499-920</ref>

==Design and development==
The BZ-1 started as a design by Clark Frazier of [[Bluffton, Ohio]] in 1967. He started construction of a glider [[fuselage]] from the [[drop tank]] of a [[North American F-86 Sabre]] and called it the ''Zeus II''. Frazier never completed the project and it was subsequently purchased by Michael Bowlus of [[Worthington, Ohio]]. Bowlus constructed wings for the re-designated BZ-1, starting with the wing design of the [[Schreder Airmate HP-11]], but shortened to {{convert|15|m|ft|1|abbr=on}}. The wing incorporates [[aileron]]s hinged at the bottom and a [[NACA]] 65 (3)-618 [[laminar flow airfoil]]. The interconnected [[Flap (aircraft)|flaps]] and ailerons are all undercambered and assembled using the same [[Adhesive bonding|bonded]] construction technique used on the [[Schreder HP-18]]. The flaps extend to 90° for landing.<ref name="SD" /><ref name="SoaringNov83" />

The BZ-1 has a retractable monowheel landing gear, a V-tail and was built from [[aluminium]]. At the time of its completion in 1983 the designer intended to add water ballast tanks to allow the carriage of {{convert|115|lb|kg|1|abbr=on}} of water.<ref name="SD" /><ref name="SoaringNov83" /> The BZ-1 had its first flight on September 13, 1984.<ref name="SoaringMar86">Bowlus, Michael: ''The Bowlus BZ-1'', page 36. [[Soaring Society of America]] March 1986.</ref>

The aircraft was reported as complete and ready for first flight in 1983, but in May 2011 it was no longer on the [[Federal Aviation Administration]] registry.<ref name="SoaringNov83" /><ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=BZ-1&PageNo=1|title = Make / Model Inquiry Results|accessdate = 15 May 2011|last = [[Federal Aviation Administration]]|authorlink = |date=May 2011}}</ref>
<!-- ==Operational history== -->

==Specifications (BZ-1) ==
{{Aircraft specs
|ref=Sailplane Directory and Soaring<ref name="SD" /><ref name="SoaringNov83" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=15
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=102
|wing area note=
|aspect ratio=23.7
|airfoil=[[NACA airfoil|NACA 65(3)-618]]
|empty weight kg=
|empty weight lb=480
|empty weight note=
|gross weight kg=
|gross weight lb=700
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|g limits=
|roll rate=
|glide ratio=37:1 (projected) at 55 mph (89 km/h)
|sink rate ms=
|sink rate ftmin=120
|sink rate note=(projected) at 50 mph (80 km/h)
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
* [[List of gliders]]
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

<!-- ==External links== -->

[[Category:United States sailplanes 1980–1989]]
[[Category:Homebuilt aircraft]]