<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name=A.F.B.1
 |image=Austin AFB 1 Outside Longbridge Works.jpg
 |caption=The A.F.B.1 pictured outside Austin's [[Longbridge]] works.
}}{{Infobox aircraft type
 |type=[[Fighter plane|Fighter]]
 |manufacturer=[[Austin Motor Company]]
 |designer=C. H. Brooks, [[Albert Ball]]
 |first flight= 27 July 1917
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=1
 |variants with their own articles=
}}
|}
The '''Austin-Ball A.F.B.1''' ('''A'''ustin '''F'''ighting '''B'''iplane) was a [[United Kingdom|British]] [[fighter plane]] of [[World War I]] built by the [[Austin Motor Company]] with design input from Britain's leading [[fighter ace]] at the time, [[Albert Ball]]. Although trials with the prototype were on the whole excellent, and it could very probably have been developed into a useful operational type, the A.F.B.1 did not go into production, as both Austin's production capacity and its [[Hispano-Suiza 8|Hispano-Suiza]] engine were required for the [[Royal Aircraft Factory S.E.5a|S.E.5a]].

==Origins and design==

===Original concept===
Albert Ball's interest in [[fighter aircraft]] design predated his first aerial victory. As early as 14 April 1916, he had written home to his father about the plans for a new fighter "heaps better than the Hun Fokker", although he made no claim that the plans were his own work. In a further letter he remarked that he could not post the plans, and was carrying them home the next time he received leave.<ref name=Pengelly/> Some writers have taken this to be an early reference to the design that eventually emerged as the A.F.B.1; this has been since largely discounted, among other reasons because the A.F.B.1 was designed around the new [[Hispano-Suiza]] engine, which places the commencement of serious design at sometime after August 1916.<ref name=Bruce/>

It would be six months before the young pilot received his leave; it seems to have been during this period he began drawing up his own ideas for specifications for a single-seat fighter aircraft; these he mailed to his father.<ref name=Pengelly/>

===The A.F.B.1 prototype===
The [[Austin Motor Company]] was one of several firms not part of the pre-war aircraft industry to receive contracts to build aircraft for the war effort. An early contract for examples of the [[Royal Aircraft Factory R.E.7|R.E.7]] was followed by substantial contracts for the [[Royal Aircraft Factory R.E.8|R.E.8]] and the [[Royal Aircraft Factory S.E.5|S.E.5a]].  Austins also opened their own  design office sometime in late 1916; although several war-time Austin designed aircraft were built as prototypes none received production orders.  The first of these was the A.F.B.1.

[[Sir Albert Ball|Albert Ball Sr]], was (or had been) on the [[Board of Directors]] of Austins, and was certainly in a position where he might have presented plans for new aircraft to the company. The position of Ball’s biographer Colin Pengelly seems to be that he most certainly did present his son’s ideas and drawings to the company, and that these formed at least the basis of the design of the A.F.B.1.<ref name=Pengelly/>  Whatever the nature and extent of Ball's input, the bulk of the design work was carried out at Austins, under the leadership of C. H. Brooks.<ref name = Cheesman/>

Ball arrived on home leave on 5 October 1916, prior to taking up duty in England. By this time, Ball had scored 31 aerial victories, and was by far the most famous pilot in the RFC. In addition to his contacts with Austins, he used his celebrity to contact [[Sir David Henderson]], Director-General of Military Aeronautics about the proposed fighter; Ball subsequently also lobbied General [[Sefton Brancker]].<ref name=Pengelly/>
 
On 1 December 1916, events had progressed to the point of the [[War Office]] formally requesting technical data from the motor company.<ref name=Pengelly/> Ball visited the Austin works that month.<ref name= Bruce/> He recommended arming the plane with two [[Lewis gun]]s mounted on the centre section, and firing above the plane's propeller; however, this arrangement was replaced by an installation patented by [[Herbert Austin]], in which a [[Vickers machine gun]] fed by a 500-round belt of ammunition fired through a hollow propeller shaft, (A Lewis gun seems to have been actually mounted in this position in the prototype as completed).<ref name= Bruce/> A supplementary Lewis gun stocked with four 97-round magazines was in the event mounted on the centre section. He also added a note at the end of the specifications sheet, dated 8 December 1916, that the finished plane should have "neutral flying characteristics".<ref name=Pengelly/> An internal memo critiqued the proposed design, with an eye toward its fitness for production. It was noted that, in an attempt to lower the plane's weight and thus increase its performance, it would only carry fuel enough for two hours running at full throttle. The relatively high design wing loading of 7 pounds per square foot might increase speed, but decrease manoeuvrability. Mounting of the engine exhaust pipes alongside the cockpit would hinder downward view, (In the event shorter exhausts were fitted, that discharged forward and downwards). The A.F.B.1's "vaguely Germanic" appearance was also criticised. An endorsement atop the memo noted that Ball was to see Sefton Brancker.<ref name=Pengelly/>
[[File:AFB1SideView.jpg|thumb|350px|A.F.B.1 at Martlesham]]

On 13 February 1917, after Ball had seen Brancker, Austins requested a formal contract to produce the new fighter. It was issued soon after, for the fabrication of a pair of prototypes,<ref name=Pengelly/> although only one prototype was allocated a serial number, and this seems to have been after it was completed: suggesting it had been started, at least, as a private venture.

At the time Ball returned to combat on 6 April 1917, the prototype was still unfinished - Ball was thus thwarted in his desire to fly the A.F.B.1 into combat, instead of the new [[Royal Aircraft Factory SE.5]] of which he had been initially disparaging.<ref name=Pengelly/> Construction of the prototype was in fact not completed until after Ball's death;<ref name = Cheesman>{{cite book |last=Cheesman|title=Fighter Aircraft of the 1914–1918 War|page= 24}}</ref> its first flight took place on 27 July 1917.<ref name=Pengelly/>

==Description==
[[Image:SE5HighSeatBall.jpg|left|thumb|160px|Ball demonstrates his favourite mode of attack (the aircraft is an early S.E.5) - compare the angle of fire of the Lewis gun with that available on the A.F.B. 1.]]
The A.F.B.1 was a [[biplane]] with un-staggered, equal-span wings. They had no [[Dihedral (aircraft)|dihedral]] but were slightly swept. The tailplane was rather large, and triangular in shape - the rudder on the other hand was a rather small balanced affair, with no vertical fin.<ref name= Bruce/>

While the [[fuselage]] was of conventional construction it was unusually deep, almost filling the gap between the planes, in the manner of the [[LFG Roland C.II]]. The portly dimensions of the fuselage made for a fairly clean engine installation - but the radiators were rather clumsily attached to the fuselage sides, in a position that must have interfered with the pilot's view forward and down past the nose. Apparently either could be bypassed to limit loss of coolant in the event of battle damage. An advanced feature was that the controls were operated by rods mounted within the airframe rather than cables carried externally, as was more usual at the time.<ref name= Bruce/> The very small gap between the top wing and the fuselage gave the pilot excellent visibility above, but probably precluded the fitting of a standard (S.E.5 type) [[Foster mounting]] for the upper [[Lewis gun]]; existing photographic evidence points to a fixed gun pointing up at a slight angle to clear the propeller arc. This would not have been conducive to Ball's favourite attack from behind and below, as is often stated (see illustrations), although, if the type had gone into production it is possible that a variant of the "Nieuport type" Foster mounting would have been devised, to allow the Lewis to be fired at various upward angles, including the steep one favoured by Ball, as well as at a flat angle directly forwards.

==Flight testing and career==
Only a single [[prototype]] was built. It was assigned the serial number B9909, although this is not visible on surviving photographs of the type. Official flight testing started in July 1917 at [[RAF Martlesham Heath]].<ref name=Pengelly/> Performance was excellent - it had about the same speed as the S.E.5a, but climbed rather better. The only complaints about its handling were of rather poor lateral control (early S.E.5s had similar problems, which were quickly resolved).

The S.E.5a was by this time already in production, and was proving itself an excellent service type - it was however in chronically short supply, and this situation could only have been exacerbated by an attempt to introduce a new type that would have competed with it for production facilities (Austins already had a large SE.5a contract) and engines (since both fighters used the [[Hispano-Suiza 8]], of which there was at this stage a severe shortage). The A.F.B.1 therefore had no real chance of being accepted for a production order.<ref name= Bruce>{{cite book |last= Bruce|first=J.M.|title=War Planes of the First World War: Fighters, Volume One|year=1965 |publisher=Macdonald & Co.|location=London |pages=23–26}}</ref>
[[Image:AFB1 with Spad wings.jpg|right|thumb|Austin-Ball A.F.B.1 with Spad style wings]]
A photograph of an A.F.B.1 exists with straight SPAD-type wings, complete with the usual SPAD mid-bay reinforcing of the interplane bracing. They may well have been, in fact, a pair of [[SPAD S.7]] wings. Nothing is known about when and why this modification was tried, or if it improved the characteristics or performance of the machine in any way.

At the end of October 1917, the testers at Martlesham Heath were instructed to remove the A.F.B.1's engine and ship it to [[Ascot, Berkshire|Ascot]] by train. The fate of the aircraft after that is not reported.<ref name=Pengelly>{{cite book|last1=Pengelly|first1=Colin|title=Albert Ball VC : the fighter pilot hero of World War One|date=2010|publisher=Pen & Sword Aviation|location=Barnsley, South Yorkshire|isbn=9781844159048|pages=132, 159, 207, 209–213, 215–216, 219}}</ref>

==Specifications (A.F.B.1)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1919<ref name=JAWA1919>{{cite book |title=Jane's All the World's Aircraft 1919 |last=Gey |first=C.G. |edition=Facsimile |year=1969 |publisher=David & Charles (Publishers) Limited |location=London |isbn=0-7153-4647-4 |pages=61a-62a}}</ref>
|prime units?=imp
<!--
        General characteristics
-->
|crew=1
|length m=
|length ft=21
|length in=6
|length note=
|span m=
|span ft=30
|span in=
|span note=
|height m=
|height ft=9
|height in=3
|height note=
|wing area sqm=
|wing area sqft=290
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=1426
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=2075
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=Petrol:{{convert|31.5|impgal|l USgal|abbr=on}}; oil:{{convert|5|impgal|l USgal|abbr=on}}; water:{{convert|9|impgal|l USgal|abbr=on}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hispano-Suiza 8]]
|eng1 type=V-8 water-cooled piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=200
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|prop blade number=4
|prop name=wooden fixed-pitch propeller
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|max speed kmh=
|max speed mph=138
|max speed kts=
|max speed note=at sea level</li>
:::{{convert|126|mph|kph kn|abbr=on}} at {{convert|10000|ft|abbr=on}}
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=2 hours
|ceiling m=
|ceiling ft=22000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=1120
|climb rate note=
|time to altitude={{convert|10000|ft|abbr=on}} in 10 minutes
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=7
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=0.108 hp/lb (0.177 kW/kg)
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=2 × fixed, forward-firing {{convert|0.303|in|mm|abbr=on|2}} [[Lewis gun]]s (One firing through hollow propeller shaft, the other on an angled mount above the top mainplane)
}}
<!-- ==See also== -->

==References==
{{reflist}}

==Further reading==
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=85 }}
* {{cite book |title=World Aircraft Information Files |publisher=Bright Star Publishing|location=London |pages=File 889 Sheet 85 }}

==External links==
{{commons category|Austin-Ball A.F.B.1}}
* [http://www.austinmemories.com/page6/page6.html Aircraft types built by Austins]

{{Austin aircraft}}

[[Category:Cancelled military aircraft projects of the United Kingdom]]
[[Category:Austin aircraft|Ball A.F.B.1]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]