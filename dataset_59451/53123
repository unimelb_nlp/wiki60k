{{Use mdy dates|date=February 2012}}
{{Infobox publisher
| image        = [[File:Wiley Blackwell Logo.gif]]
| parent       = [[John Wiley & Sons]]
| status       = 
| founded      = {{start date and age|1922}}
| founder      = 
| successor    = 
| country      = United States
| headquarters = [[Hoboken, New Jersey]]
| distribution = 
| keypeople    = 
| publications = Books, [[academic journal]]s
| topics       = 
| genre        = 
| imprints     = 
| revenue      = 
| numemployees = 
| nasdaq       = 
| url          = {{URL|http://www.wiley.com/wiley-blackwell}}
}}
'''Wiley-Blackwell''' is the international scientific, technical, medical, and scholarly publishing business of [[John Wiley & Sons]]. It was formed by the merger of John Wiley's Global Scientific, Technical, and Medical business with Blackwell Publishing, after Wiley took over the latter in 2007.<ref name=aboutWB>[http://www.wiley.com/WileyCDA/Brand/id-35.html About Wiley-Blackwell]. John Wiley & Sons, Inc.</ref>

As a [[learned society]] publisher, Wiley-Blackwell partners with around 750 societies and associations.  It publishes nearly 1,500 peer-reviewed journals and more than 1,500 new books annually in print and online, as well as databases, major reference works, and laboratory protocols. Wiley-Blackwell is based in [[Hoboken, New Jersey]] (United States) and has offices in many international locations including Boston, [[Oxford]], [[Chichester]], Berlin, Singapore, Melbourne, Tokyo, and Beijing, among others.

Wiley-Blackwell publishes in a diverse range of academic and professional fields, including in [[biology]], [[medicine]], [[physical sciences]], [[technology]], [[social science]], and the [[humanities]].<ref>{{cite web |url= http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=44764174 |title=Wiley-Blackwell, Inc.: Private Company Information - Businessweek|work=investing.businessweek.com|accessdate=21 November 2012}}</ref>

Access to more than 1,500 journals, OnlineBooks, lab protocols, electronic major reference works and other online products published by Wiley-Blackwell is available through [[Wiley Online Library]],<ref>[http://www.wileyonlinelibrary.com WileyOnlineLibrary.com]</ref> which replaced the previous platform, Wiley InterScience, in August 2010.

==Blackwell Publishing history==
Blackwell Publishing was formed by the 2001 merger of two Oxford-based [[academic publishing]] companies, Blackwell Science (founded 1939 as Blackwell Scientific Publishing) and Blackwell Publishers (founded 1922), which had their origins in the nineteenth century [[Blackwell's]] family bookshop and publishing business.  The merger created the world's leading learned society publisher, partnered with 665 academic and professional societies.{{citation needed|date=January 2017}}  Blackwell published over 805 journals and 650 text and reference books in 2006, across a wide range of academic, medical, and professional subjects, and had 990 staff members with offices in the United States, UK, Australia, China, Denmark, Germany, Singapore and Japan.{{citation needed|date=January 2017}}

On November 17, 2006, [[John Wiley & Sons]] announced it had "entered into a definitive agreement to acquire" Blackwell Publishing.<ref>[http://www.blackwellpublishing.com/pdf/wiley.pdf Wiley to Acquire Blackwell Publishing (Holdings) Ltd.], John Wiley & Sons, Inc., November 17, 2006</ref> The acquisition was completed in February 2007, at a purchase price of £572 million. Blackwell Publishing was merged into Wiley's Global Scientific, Technical, and Medical business to create Wiley-Blackwell.<ref name=aboutWB/> From June 30, 2008, the journals previously on Blackwell Synergy are delivered through [[Wiley InterScience]].<ref>[http://www3.interscience.wiley.com Interscience.wiley.com], Wiley-Blackwell. June 16, 2008.</ref>

== See also ==
* [[:Category:Wiley-Blackwell academic journals|List of journals published by Wiley-Blackwell]]

==References==
{{reflist|colwidth=30em}}

==External links==
* {{Official website|http://www.wiley.com/WileyCDA/Brand/id-35.html}}
* {{cite web |title=Blackwell Timeline |url=http://www.ulib.niu.edu/publishers/index.htm |work=The Academic Publishing Industry: A Story of Merger and Acquisition |author= Mary H. Munroe |year=2004 |via= Northern Illinois University }}

[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1922]]
[[Category:Companies based in Oxford]]
[[Category:Commercial digital libraries]]
[[Category:Publishing companies of the United Kingdom]]
[[Category:American companies established in 1922]]