{{For|other places called Capon|Cacapon (disambiguation){{!}}Cacapon}}
{{featured article}}
{{Geobox|Bridge
<!-- *** Heading *** -->
| name = Capon Lake Whipple Truss Bridge
| native_name =
| category = [[National Register of Historic Places]]
<!-- *** Image *** -->
| image = Capon Lake Whipple Truss Bridge Capon Lake WV 2009 07 19 11.jpg
| image_size = 300
| image_caption = Capon Lake Whipple Truss Bridge, looking northwest from its southeast end, in 2009
| alt = An image viewing the center of a green Whipple truss bridge with a wooden walkway spanning the center
<!-- *** Names **** -->
| official_name = Capon Lake Whipple Truss Bridge
| other_name = South Branch Bridge, Romney Bridge
| etymology_type = Named for
| etymology = [[Capon Lake, West Virginia|Capon Lake]]
| nickname =
<!-- *** Symbols *** -->
| flag =
| flag_size =
| symbol =
| symbol_size =
<!-- *** Country *** -->
| country = United States
| country_flag = true
| state = West Virginia
| state_flag = true
| region_type = County
| region = [[Hampshire County, West Virginia|Hampshire]]
| municipality_type = [[Unincorporated area]]
| municipality = Capon Lake
<!-- *** Family *** -->
| parent_type = Road
| road = {{jct|state=WV|US|50}} <small>(1874–1937)</small><br/>{{jct|state=WV|CR|16|county1=Hampshire}} <small>(1938–1991)</small>
| road_note = 
| city =
| landmark =
| building =
| river = [[Potomac River#South Branch Potomac River|South Branch<br/>Potomac River]] <small>(1874–1937)</small><br>[[Cacapon River]] <small>(1938–present)</small>
| river_type = Crosses
<!-- *** Locations *** -->
| location = Carpers Pike ([[West Virginia Route 259|WV 259]])<br>Capon Lake, West Virginia
| elevation_imperial = 876
| lat_d = 39
| lat_m = 09
| lat_s = 30
| lat_NS = N
| long_d = 78
| long_m = 32
| long_s = 7
| long_EW = W
| coordinates_note =<ref name = "USGS1">{{Cite map |publisher=[[United States Geological Survey]]|title=Yellow Spring Quadrangle&nbsp;– West Virginia |year=1970 |scale=1 : 24,000 |series=7.5 Minute Series (Topographic) | OCLC = 36574404 |isbn= }}</ref>
<!-- *** Dimensions *** -->
| length_imperial = 176
| length_orientation =
| length_note =<ref name = "Fint1">{{Harvnb|Fint|2011|p=5 of the PDF file.}}</ref>
| width_imperial = 20
| width_round = 1
| width_orientation =
| width_note =<ref name = "Fint1"/>
| width_type =
| number =
| number_type =
| height_imperial = 23
| height_round = 1
| height_type = Clearance
| height_note =<ref name = "Fint1"/>
| depth_imperial =
| volume_imperial =
| area_imperial =
<!-- *** Features *** -->
| style_type = Design
| style = [[Truss bridge#Whipple truss|Whipple truss bridge]]
| material = [[Wrought iron]]
| author_type = Builder
| author = T. B. White and Sons
<!-- *** History & management *** -->
| established_type = Built
| established_label = Date of construction
| established = {{Start date|1874}}
| established1_type = Rebuilt
| established1 = 1938
| established1_label = Date of reconstruction
| established2_type = Restored
| established2 = 1991
| established2_label = Date of restoration
| established3_type = Added to [[National Register of Historic Places|NRHP]]
| established3 = December 15, 2011
| established3_label = Date of addition to the National Register of Historic Places
| established3_note =<ref name="nps">{{cite web|url=http://www.cr.nps.gov/nr/listings/20111223.htm|title=National Register of Historic Places Listings|date=2011-12-23|work=Weekly List of Actions Taken on Properties: 12/12/11 through 12/16/11|publisher=National Park Service|archive-url = https://web.archive.org/web/20151006002347/http://www.nps.gov/nr/listings/20111223.htm | archive-date = October 6, 2015 | accessdate= October 6, 2015}}</ref>
| management_type = Owned and Maintained by
| management = [[West Virginia Department of Transportation|West Virginia Division of Highways]], District 5
<!-- *** Codes *** -->
| code_type = NRHP #
| code_label = National Register of Historic Places reference number
| code = 11000929
| code_note =<ref name="nps"/>
| code1_type =
| code1_label = 
| code1 = 
| code1_note = 
<!-- *** Access *** -->
| public =
| visitation =
| visitation_date =
| access =
| access_type =
<!-- *** Free fields *** -->
| free_type = 
| free = 
| free1_type = 
| free1 = 
| free2_type =
| free2 = 
| free3_type = 
| free3 = 
<!-- *** Map section *** -->
<!-- | map = West Virginia Locator Map.PNG
| map_size = 200
| map_caption = Location of the Capon Lake Whipple Truss Bridge in West Virginia
| map_locator = West Virginia-->
<!-- | map1 = Map of USA WV.svg
| map1_size = 200
| map1_caption = Location of West Virginia in the United States -->
<!-- *** Website *** -->
| commons = Capon Lake Whipple Truss Bridge (Capon Lake, West Virginia)
| statistics =
| website =
<!-- *** Footnotes *** -->
| footnotes =
}}
The '''Capon Lake Whipple Truss Bridge''' ({{IPAc-en|local|k|eɪ|p|ən}}), formerly known as '''South Branch Bridge''' or '''Romney Bridge''', is a historic [[Truss bridge#Whipple truss|Whipple truss]] bridge in [[Capon Lake, West Virginia]]. It is located off Carpers Pike ([[West Virginia Route 259]]) and crosses the [[Cacapon River]]. The bridge formerly carried Capon Springs Road (West Virginia Secondary Route 16) over the river, connecting [[Capon Springs, West Virginia|Capon Springs]] and Capon Lake.

The bridge's Whipple truss technology was developed by civil engineer [[Squire Whipple]] in 1847. J. W. Murphy further modified Whipple's truss design in 1859 by designing the first truss bridge with pinned [[eyebar]] connections. The design of the Capon Lake Whipple Truss Bridge incorporates Murphy's later modifications with double-intersections and [[Truss|horizontal chords]], and is therefore considered a Whipple–Murphy truss bridge. The Capon Lake Whipple Truss Bridge is West Virginia's oldest remaining example of a Whipple truss bridge and its oldest extant metal truss bridge.

The Capon Lake Whipple Truss Bridge was originally constructed in 1874 as part of the South Branch Bridge (or alternatively, the Romney Bridge), a larger two-span Whipple truss bridge conveying the [[Northwestern Turnpike]] ([[U.S. Route 50]]) across the [[Potomac River#South Branch Potomac River|South Branch Potomac River]] near [[Romney, West Virginia|Romney]]. The larger Whipple truss bridge replaced an 1838 wooden covered bridge that was destroyed during the [[American Civil War]]. In 1874, T. B. White and Sons were charged with the construction of a Whipple truss bridge over the South Branch; that bridge served travelers along the Northwestern Turnpike for 63 years until a new bridge was constructed in 1937.

Dismantled in 1937, the bridge was relocated to Capon Lake in southeastern [[Hampshire County, West Virginia|Hampshire County]] to carry Capon Springs Road (West Virginia Secondary Route 16) between West Virginia Route 259 and Capon Springs. The bridge was dedicated on August 20, 1938. In 1991, a new bridge was completed to the south, and the Capon Lake Whipple Truss Bridge was preserved in place by the West Virginia Division of Highways, due to its rarity, age, and engineering significance. The Capon Lake Whipple Truss Bridge was listed on the [[National Register of Historic Places]] on December 15, 2011.

== Geography and setting ==
[[File:Cacapon River Capon Lake WV 2009 07 19 09.JPG|thumb|right|[[Cacapon River]], looking downstream from the bridge in 2009|alt=A wide stream strewn with rounded stones and surrounded by forests on either side, viewed from a green truss bridge]]
The Capon Lake Whipple Truss Bridge is located in a predominantly rural agricultural and forested area of southeastern [[Hampshire County, West Virginia|Hampshire County]] within the Cacapon River valley.<ref name = "USGS1"/><ref name = "Fint1"/> [[Baker Mountain (West Virginia)|Baker Mountain]], a forested narrow [[Anticline|anticlinal]] mountain ridge, rises to the immediate west, and the western rolling foothills of the anticlinal [[Great North Mountain]] rise to the bridge's east.<ref name = "USGS1"/> The [[confluence]] of [[Capon Springs Run]] with the Cacapon River lies just north (downstream) of the bridge.<ref name = "USGS1"/> [[George Washington and Jefferson National Forests|George Washington National Forest]] is located to the bridge's southeast, covering the forested area south of Capon Springs Road.<ref name = "USGS1"/>

The bridge is located along Carpers Pike ([[West Virginia Route 259]]) in the [[unincorporated community]] of [[Capon Lake, West Virginia|Capon Lake]], {{convert|2.05|mi}} southwest of [[Yellow Spring, West Virginia|Yellow Spring]] and {{convert|6.77|mi}} northeast of the town of [[Wardensville, West Virginia|Wardensville]].<ref name = "USGS1"/><ref name=GNIS1>{{cite web |url={{Gnis3|1554067}} |title=Geographic Names Information System: Feature Detail Report for Capon Lake (Feature ID: 1554067) |author=[[Geographic Names Information System]], [[United States Geological Survey]] |authorlink= |accessdate=October 6, 2015}}</ref><ref name=GNIS2>{{cite web |url={{Gnis3|36574404}} |title=Geographic Names Information System: Feature Detail Report for Yellow Spring (Feature ID: 36574404) |author=[[Geographic Names Information System]], [[United States Geological Survey]] |authorlink= |accessdate=October 6, 2015}}</ref><ref name=GNIS3>{{cite web |url={{Gnis3|1553382}} |title=Geographic Names Information System: Feature Detail Report for Wardensville (Feature ID: 1553382) |author=[[Geographic Names Information System]], [[United States Geological Survey]] |authorlink= |accessdate=October 6, 2015}}</ref> The historic [[Capon Springs Resort]] and the unincorporated community of [[Capon Springs, West Virginia|Capon Springs]] are located {{convert|3.5|mi|km}} east of Capon Lake on Capon Springs Road (West Virginia Secondary Route 16).<ref name = "USGS1"/><ref name = "Fint1"/> The bridge is located immediately north (downstream) of the intersection of Carpers Pike with Capon Springs Road, which is carried across the Cacapon River via the current Capon Lake Bridge, a [[Girder bridge|steel stringer bridge]] built in 1991 to replace the Whipple truss bridge for conveying vehicle traffic.<ref name = "USGS1"/><ref name = "Fint1"/> The property containing the Capon Lake Whipple Truss Bridge is less than {{convert|1|acre}} in size.<ref name = "Fint2">{{Harvnb|Fint|2011|p=4 of the PDF file.}}</ref>

== Architecture ==
[[File:Capon Lake Whipple Truss Bridge Capon Lake WV 2009 07 19 08.jpg|thumb|upright|left|Wooden pedestrian walkway added to the bridge in 1991|alt=An image of the wooden walkway spanning the length of the green Whipple truss bridge]]
The Capon Lake Whipple Truss Bridge is an early example of the use of metal [[truss bridge]] load-bearing [[superstructure]] technology, which defined highway bridge design well into the 20th century.<ref name = "Fint3"/> Because of "its uncommon innovative design and age", the bridge is one of West Virginia's most historically significant bridges.<ref name = "Fint3"/> It is the oldest remaining example of a Whipple truss bridge in West Virginia, and the oldest extant metal truss bridge in the state.<ref name = "Fint3"/><ref name = "Fint6">{{Harvnb|Fint|2011|p=8 of the PDF file.}}</ref> The metal truss technology of the bridge displays distinctive innovations developed by the prominent civil engineers and bridge designers [[Squire Whipple]] and J. W. Murphy; the innovations are evident in the bridge's double-intersection diagonals and counter-diagonals with pin connections.<ref name = "Fint3"/><ref name = "Fint5">{{Harvnb|Fint|2011|p=7 of the PDF file.}}</ref>

Approximately {{convert|20|ft|m}} in width and {{convert|176|ft|m}} in length, the bridge is built atop a [[reinforced concrete]] [[abutment]] and pier.<ref name = "Fint1"/> Its truss structure exhibits a double-intersection configuration, constructed of 14 [[Bay (architecture)|bays]], each measuring approximately {{convert|11|ft|m}} wide and {{convert|23|ft|m}} in height, with the diagonals extending across two bays each.<ref name = "Fint1"/> The bridge is fabricated of [[wrought iron]] bracketed with pins.<ref name = "Fint1"/> Spanning the full length of the bridge is a wooden pedestrian walkway that consists of an [[observation deck]] and wooden seating near the bridge's midspan.<ref name = "Fint1"/>

== History ==

=== Whipple truss development ===
The bridge's [[Truss bridge#Whipple truss|Whipple truss]] technology was developed in 1847 by civil engineer Squire Whipple, who received a patent from the [[United States Patent and Trademark Office|U.S. Patent Office]] the same year.<ref name = "Fint5"/><ref name = "Knoblock1">{{Harvnb|Knoblock|2012|p=33.}}</ref> Whipple was one of the first structural engineers to use scientific and mathematical methods analyzing the forces and stresses in framed structures to design the bridge, and his groundbreaking 1847 book, ''A Work on Bridge Building'', had a significant influence on bridge engineering.<ref name = "Fint5"/><ref name = "Knoblock1"/><ref name = "Fischetti1">{{Harvnb|Fischetti|2009|p=219.}}</ref> Whipple's truss bridge design incorporated double-intersection diagonals into the standard [[Truss bridge#Pratt truss|Pratt truss]], thus allowing the diagonals to extend across two truss bays.<ref name = "Fint5"/> Engineer J. W. Murphy further modified Whipple's truss design in 1859 when he designed the first truss bridge with pinned [[eyebar]] connections, which utilized pins instead of [[trunnion]]s.<ref name = "Fint5"/><ref name = "Tyrrell1">{{Harvnb|Tyrrell|1911|p=169.}}</ref> Murphy's design removed the need for riveted connections and allowed for easier and more widespread construction of truss bridges.<ref name = "Fint5"/> In 1863, Murphy designed the first pin-connected truss bridge with both wrought iron tension and compression components and [[cast iron]] joint blocks and pedestals.<ref name = "Johnson1">{{Harvnb|Johnson|Bryan|Turneaure|Kinne|1910|p=12.}}</ref><ref name = "Tyrrell2">{{Harvnb|Tyrrell|1911|p=33.}}</ref> Murphy's truss design consisted of double-intersection counter-diagonals, and along with the eyebar and pin connections, permitted longer iron bridge spans.<ref name = "Fint5"/><ref name = "Johnson2">{{Harvnb|Johnson|Bryan|Turneaure|Kinne|1910|p=13.}}</ref>
[[File:Squire Whipple.jpg|upright|thumb|[[Squire Whipple]]|alt=A black and white engraved portrait of an elderly man with a long beard and sporting a suit]]
The technological design advances made by Whipple and Murphy, in addition to further advances in steel and iron fabrication, made wrought iron truss bridges a major industry in the United States.<ref name = "Fint5"/> The Capon Lake bridge was a Whipple–Murphy truss bridge, since it incorporated Murphy's later modifications with double-intersections and [[Truss|horizontal chords]].<ref name = "Fint5"/><ref name = "Johnson2"/> At the time of the bridge's original fabrication in 1874, metal truss bridges were ordered from catalogs by county courts and other entities responsible for transportation construction and maintenance.<ref name = "Fint3"/><ref name = "Knoblock3">{{Harvnb|Knoblock|2012|p=48.}}</ref> These entities provided the desired width, length, and other specifications, and the truss materials were shipped to the construction site and assembled by local construction teams.<ref name = "Knoblock3"/> Metal truss bridges were more economically feasible, could span longer distances, and were simpler to construct than stone bridges, and they were more durable than wooden bridges.<ref name = "Fint3"/> They were also marketed as detachable and transportable structures that could be dismantled and reassembled.<ref name = "Fint3"/> The technology used in the Capon Lake Whipple Truss Bridge revolutionized transport throughout West Virginia.<ref name = "Fint3"/> While the Whipple truss bridge had waned in popularity by the 1890s, the bridges were commonly disassembled and re-erected for use on secondary roads, as was the case with the Capon Lake Whipple Truss Bridge in 1938.<ref name = "Fint1"/><ref name = "Knoblock2">{{Harvnb|Knoblock|2012|p=34.}}</ref>

=== T. B. White and Sons ===
The construction company that built the Capon Lake Whipple Truss Bridge, T. B. White and Sons, was established in 1868.<ref name = "Fint5"/><ref name = "Jordan1">{{Harvnb|Jordan|1914|p=532.}}</ref> Its founder Timothy B. White had been a carpenter and contractor in [[New Brighton, Pennsylvania]] since the 1840s.<ref name = "Fint5"/><ref name = "Jordan1"/> White also operated factories for iron cars and woolen mill machinery until 1859, when he began to concentrate solely on bridge construction.<ref name = "Jordan1"/> White's bridge company operated from a factory on the [[Beaver River (Pennsylvania)|Beaver River]] in New Brighton until the factory was destroyed by fire in 1878. After the fire, the company relocated across the river to [[Beaver Falls, Pennsylvania|Beaver Falls]] and restructured as the Penn Bridge and Machine Works.<ref name = "Fint5"/><ref name = "Jordan1"/> In addition to iron truss bridges, the company produced a range of structural and architectural components and continued to expand; it employed over 500 workers by 1908.<ref name = "Fint5"/> Penn Bridge and Machine Works fended off purchase by the [[American Bridge Company]] and continued to operate independently, unlike similar small bridge companies founded in the 19th century.<ref name = "Fint5"/> The most prolific of its kind in the [[Pittsburgh]] region, the company was responsible for the construction of bridges throughout the United States.<ref name = "Fint5"/>

=== South Branch Bridge ===
[[File:1838 Romney Bridge 1874.jpg|thumb|left|The 1838 covered bridge over the [[Potomac River#South Branch Potomac River|South Branch Potomac River]] destroyed during the [[American Civil War]], looking east toward [[Sycamore Dale]]|alt=A black and white engraved image of a wooden covered bridge crossing a river with a two-story house in the background]]
The Capon Lake Whipple Truss Bridge was originally constructed in 1874 as part of the South Branch Bridge (or the Romney Bridge), a larger two-span Whipple truss bridge conveying the [[Northwestern Turnpike]] ([[U.S. Route 50]]) across the [[Potomac River#South Branch Potomac River|South Branch Potomac River]] {{convert|0.57|mi|km}} west of [[Romney, West Virginia|Romney]].<ref name = "Fint1"/><ref name = "Fint3">{{Harvnb|Fint|2011|p=6 of the PDF file.}}</ref> The 1874 Whipple truss bridge across the South Branch replaced an 1838 wooden covered bridge that had been chartered by the [[Virginia General Assembly]] during the construction of the Northwestern Turnpike.<ref name = "Fint3"/> Before the construction of the covered bridge in 1838, a public ferry conveyed traffic across the river. [[Isaac Parsons (Virginia politician)|Isaac Parsons]] (1752–1796) operated a ferry there following its establishment by an act of the Virginia General Assembly in October 1786.<ref name = "VHD1">{{Harvnb|Virginia House of Delegates|1828|p=56}}</ref><ref name = "Hening2">{{Harvnb|Hening|1823|pp=403–404.}}</ref> The 1838 covered bridge remained in use until it was destroyed by retreating [[Confederate States Army|Confederate]] forces during the [[American Civil War]].<ref name = "Fint3"/> Throughout the course of the war, Romney reportedly changed hands 56 times between Confederate and [[Union Army|Union]] forces, and the crossing of the South Branch Potomac River served as a strategic point due to its position along the Northwestern Turnpike, an important east–west route.<ref name = "Fint3"/><ref name = "FWP1">{{Harvnb|Federal Writers' Project|1937|p=62.}}</ref><ref name = "Maxwell1">{{Harvnb|Maxwell|Swisher|1897|p=549.}}</ref>

Following the conclusion of the war, nearly all bridges along the Northwestern Turnpike had been destroyed, including the South Branch Bridge.<ref name = "FWP2">{{Harvnb|Federal Writers' Project|1937|p=67.}}</ref> In order to restore local businesses and industry, Hampshire County citizens called a meeting and steps were taken at the local level to proceed with the construction of new bridges.<ref name = "FWP2"/> Local citizens and the ''South Branch Intelligencer'' newspaper of Romney campaigned for the immediate replacement of the bridge because of "continual risk, danger and inconveniences arising from want of the South Branch Bridge at [[Sycamore Dale|Col. Gibson's]] (destroyed during the war)...".<ref name = "Fint3"/> Hampshire County began issuing bonds for the construction of a new bridge over the South Branch in 1868, and by 1874, construction of the Whipple truss bridge had commenced.<ref name = "Fint3"/> T. B. White and Sons were charged with the bridge's construction.<ref name = "Fint3"/>
[[File:South Branch Potomac River Romney WV 2008 06 22 01.jpg|thumb|right|The [[Potomac River#South Branch Potomac River|South Branch Potomac River]] looking north in 2008, downstream from the present-day South Branch Bridge|alt=An image of a river in the foreground and a forested mountain ridge in the background, topped with a blue sky and clouds]]
The ''South Branch Intelligencer'' published periodic updates on the progress of the South Branch Bridge's construction.<ref name = "Fint3"/> According to the newspaper, the bridge was scheduled to be completed by July 1875.<ref name = "Fint3"/> During the course of construction, John Ridenour lost a finger while working on the bridge.<ref name = "Fint3"/> The new South Branch Bridge was completed well ahead of schedule in October 1874.<ref name = "Fint3"/> The October 12, 1874, edition of the ''South Branch Intelligencer'' characterized the new bridge as a "complete, handsome and durable structure", and further recounted that "the contractors, Messrs. White & Sons, New Brighton, Pennsylvania 'Penn Bridge & Machine Works,' have given us, in general opinion, a first rate, durable work, and deserve our best commendations.... We are confident that ours will realize a very handsome income and fully vindicate the wisdom of the County Court in voting its construction."<ref name = "Fint4">{{Harvnb|Fint|2011|pp=6–7 of the PDF file.}}</ref><ref name = "Intelligencer1">{{cite news | last =  | first = | author-link =  | title = The South Branch Bridge | newspaper = South Branch Intelligencer | publication-place = [[Romney, West Virginia]] | volume =  | issue =  | date = October 12, 1874 | page = | url =  | accessdate = }}</ref>

Following its construction in 1874, the Whipple truss bridge over the South Branch Potomac River served Romney and travelers along the Northwestern Turnpike for 63 years.<ref name = "Fint5"/> In 1935, the West Virginia State Road Commission began organizing a project to replace the Whipple truss bridge, and construction of the new bridge had begun by 1936.<ref name = "Fint5"/><ref name = "WVSRC1">{{Harvnb|West Virginia State Road Commission|1936|p=}}</ref> In November of that year, a car collided with the south side of the eastern Whipple truss span, which knocked the span completely off its eastern abutment.<ref name = "Fint5"/><ref name = "Review1">{{cite news | last =  | first = | author-link =  | title = Span of Old River Bridge Goes Down | newspaper = Hampshire Review | publication-place = [[Romney, West Virginia]] | volume =  | issue =  | date = November 18, 1936 | page = | url =  | accessdate = }}</ref> The car plunged into the South Branch Potomac River, followed by the compromised truss span, which collapsed on top of the car.<ref name = "Fint5"/><ref name = "Review1"/> Unaware of the span's collapse, a car traveling from the west drove off the end of the west span at the bridge's center pier, and fell onto the collapsed span.<ref name = "Fint5"/><ref name = "Review1"/> According to the ''Hampshire Review'', the only serious injury sustained was a broken wooden leg.<ref name = "Fint5"/><ref name = "Review1"/> Following the collapse of the eastern Whipple truss span, a temporary wooden span was hastily constructed between the western truss span and the eastern abutment, so that traffic was uninterrupted until the new bridge was completed and opened on June 21, 1937.<ref name = "Fint5"/><ref name = "HH1">{{cite web | url = http://www.historichampshire.org/timeline.htm | title = Some Dates Relating to Hampshire County History | work = HistoricHampshire.org | publisher = Charles C. Hall | accessdate = October 6, 2015 | archive-url = https://web.archive.org/web/20150913002612/http://www.historichampshire.org/timeline.htm | archive-date=September 13, 2015}}</ref> The 1937 bridge was used until 2010 when it was replaced by the current South Branch Bridge.<ref name = "HH1"/><ref name = "HH2">{{cite web | url = http://www.historichampshire.org/building/new/new.htm#bridge | title = The New Rt. 50 Bridge west of Romney | work = HistoricHampshire.org | publisher = Charles C. Hall | accessdate = October 6, 2015 | archive-url = https://web.archive.org/web/20130507213556/http://www.historichampshire.org/building/new/new.htm | archive-date=May 7, 2013}}</ref>

=== Capon Lake Bridge ===
[[File:Capon Lake Whipple Truss Bridge Capon Lake WV 2009 07 19 03.jpg|thumb|upright|left|The bridge in 2009, looking southeast from Carpers Pike ([[West Virginia Route 259]])|alt=An image of the green Whipple truss bridge from along the side of a road]]
Because Whipple truss bridges were easily disassembled and re-erected, the remaining western span of the Whipple truss over the South Branch was dismantled in 1937 and relocated to Capon Lake in southeastern Hampshire County to convey Capon Springs Road (West Virginia Secondary Route 16) between West Virginia Route 259 and Capon Springs.<ref name = "Fint5"/><ref name = "Knoblock2"/><ref name = "WVSRC2">{{Harvnb|West Virginia State Road Commission|1938|p=}}</ref> According to Branson Himelwright, a Capon Springs resident who had been a construction worker involved in the re-erection of the Whipple truss span at Capon Lake, the only two ways to cross the Cacapon River to reach Capon Springs were to cross a swinging footbridge or ford the river.<ref name = "Fint6"/><ref name = "McKeown1">{{Harvnb|McKeown|1991|p=}}</ref> During the bridge's construction, a new pier and abutments were constructed to carry the Whipple truss span and a connected Pratt truss that had been salvaged from an unknown bridge.<ref name = "Fint1"/><ref name = "Fint6"/> Himelwright and Jacob "Moss" Rudolph, who had also participated in the bridge's construction, stated in interviews that both the site excavation and concrete work for the pier and abutments were completed by hand.<ref name = "Fint6"/><ref name = "McKeown1"/><ref name = "WVDOH5">{{Harvnb|West Virginia Division of Highways District 5|1991|p=}}</ref>

The newly erected Capon Lake Bridge was dedicated on August 20, 1938, with a ceremony including musical performances by the Romney High School and Capon Springs Resort bands.<ref name = "Fint6"/><ref name = "Review2">{{cite news | last = | first = | author-link =  | title = New Bridge at Capon Dedicated August 20 | newspaper = Hampshire Review | publication-place = [[Romney, West Virginia]] | volume =  | issue =  | date = August 17, 1938 | page = | url =  | accessdate = }}</ref> Former [[List of Governors of West Virginia|West Virginia Governor]] and Capon Springs native [[Herman G. Kump]], West Virginia State Road Commission Secretary Cy Hammill, and numerous other state officials were in attendance at the dedication.<ref name = "Fint6"/><ref name = "Review2"/>

In 1991, the new steel stringer Capon Lake Bridge was constructed {{convert|187|ft|m}} to the southwest of the Capon Lake Whipple Truss Bridge, after which the Whipple truss bridge was closed to vehicle traffic.<ref name = "Fint3"/><ref name = "Fint6"/> Due to its rarity, age, and engineering significance, [[West Virginia Division of Highways]] District&nbsp;5 decided to preserve the Whipple truss bridge.<ref name = "Fint6"/> During the bridge's restoration, the Pratt truss span was removed due to significant deterioration, and the roadway deck was also removed.<ref name = "Fint6"/> A wooden pedestrian walkway and observation deck were constructed across the full span of the remaining truss bridge.<ref name = "Fint1"/><ref name = "Fint6"/>
[[File:Capon Lake Whipple Truss Bridge Capon Lake WV 2009 07 19 09.jpg|thumb|right|Wooden observation deck looking downstream (north) on the [[Cacapon River]], 2009|alt=A detail of the wooden seat and observation platform at the center of the green Whipple truss bridge facing out over a river]]
The Capon Lake Whipple Truss Bridge was listed on the [[National Register of Historic Places]] on December 15, 2011, for its "engineering significance as an excellent example of a Whipple/Murphy Truss bridge."<ref name="nps"/><ref name = "Fint3"/> Since its listing, the bridge has been maintained as a historic site for pedestrians by the West Virginia Division of Highways District&nbsp;5.<ref name = "Fint3"/> In 2012, the West Virginia Division of Highways, in association with the West Virginia Archives and the history department of the West Virginia Division of Culture and History, installed a historical marker at the northwestern entry to the bridge as part of the West Virginia Highway Historical Marker Program. The marker reads:<ref name = "WVHMP1">{{cite web | url = http://www.wvculture.org/history/wvmemory/hm.aspx | title = West Virginia Highway Markers Database | work =West Virginia Highway Markers Database | publisher = West Virginia Archives and History; West Virginia Division of Culture and History | accessdate = October 6, 2015| archive-url = https://web.archive.org/web/20150128115356/http://www.wvculture.org/history/wvmemory/hm.aspx | archive-date=January 28, 2015}}</ref>

{{quote|First erected in 1874 as a two span bridge on US 50 near Romney, one span was moved here in 1938 and re-erected on a new foundation. The 17' wide by 176' long bridge is a Whipple–Murphy Truss. The state's oldest extant metal truss, the bridge is one of a few of its type in WV. Listed in the National Register of Historic Places in 2011.<ref name = "WVHMP1"/>}}

== See also ==
* {{Portal-inline|Bridges}}
* {{Portal-inline|West Virginia}}
* [[List of historic sites in Hampshire County, West Virginia]]
* [[National Register of Historic Places listings in Hampshire County, West Virginia]]

== References ==
{{reflist|30em}}

== Bibliography ==
* {{cite book  | author=Federal Writers' Project |authorlink=Federal Writers' Project | author-link =  | year=1937 | title=Historic Romney 1762–1937 | publisher= [[Federal Writers' Project]], The Town Council of Romney, West Virginia | location = [[Romney, West Virginia]] | isbn=  |  OCLC = 2006735 | url= | accessdate=|ref=harv}}
* {{Cite book| last=Fint | first=Courtney  | author-link = | date=August 3, 2011| title=National Register of Historic Places Registration Form: South Branch Bridge | publisher= [[United States Department of the Interior]], [[National Park Service]] | language=| isbn= | url=http://www.wvculture.org/shpo/nr/pdf/hampshire/11000929.pdf | accessdate=October 6, 2015|ref=harv | archive-url = https://web.archive.org/web/20140201210438/http://www.wvculture.org/shpo/nr/pdf/hampshire/11000929.pdf | archive-date=February 1, 2014 }}
* {{cite book | last=Fischetti | first=David C.| author-link =  | year=2009 | title=Structural Investigation of Historic Buildings: A Case Study Guide to Preservation Technology for Buildings, Bridges, Towers and Mills | publisher=  [[John Wiley & Sons]] | location = [[Hoboken, New Jersey]] | isbn= 978-0-470-18967-2 | OCLC = 233544924 |  url=https://books.google.com/books?id=u9Fl7Pjj9sUC | ref=harv | via = [[Google Books]] }}
* {{cite book | last=Hening | first=William Waller | author-link =  | year=1823 | title=The Statutes at Large: Being a Collection of All the Laws of Virginia, from the First Session of the Legislature in the Year 1619: Published Pursuant to an Act of the General Assembly of Virginia, Passed on the Fifth Day of February One Thousand Eight Hundred and Eight, Volume 12 | publisher=  R. & W. & G. Bartow | location = New York City | isbn=  | OCLC = 16810101 |  url=https://books.google.com/books?id=nc5OAQAAIAAJ | ref=harv | via = [[Google Books]] }}
* {{Cite book| last1=Johnson | first1= John Butler | last2 =Bryan | first2= Charles Walter | last3= Turneaure | first3 = Frederick Eugene | last4 =Kinne | first4 = William Spaulding | year = 1910 | title=The Theory and Practice of Modern Framed Structures: Designed for the Use of Schools and for Engineers in Professional Practice | publisher= [[John Wiley & Sons]] | language=| isbn= | url=https://books.google.com/books?id=smQPAAAAYAAJ | OCLC = 2454572  | location = New York City |ref=harv | via = [[Google Books]] }}
* {{Cite book| editor-last1=Jordan | editor-first1= John Woolf  | author-link = | year = 1914 | title=Genealogical and Personal History of Beaver County, Pennsylvania, Volume 1 | publisher= Lewis Historical Publishing Company | language=| isbn= | url=https://books.google.com/books?id=_88wAQAAMAAJ | OCLC = 6427397  | location = New York City |ref=harv | via = [[Google Books]] }}
* {{cite book | last=Knoblock | first=Glenn A.  | author-link =  | year=2012 | title=Historic Iron and Steel Bridges in Maine, New Hampshire and Vermont | publisher= McFarland & Company, Inc. | location = [[Jefferson, North Carolina]] | isbn= 978-0-7864-8699-1 | OCLC = 775680863|  url=https://books.google.com/books?id=LfT_gjoqnAEC | ref=harv | via = [[Google Books]] }}
* {{cite book |title=History of Hampshire County, West Virginia from its Earliest Settlement to the Present |last1=Maxwell |first1=Hu  |authorlink1=Hu Maxwell | last2= Swisher | first2 = Howard Llewellyn | authorlink2 = Howard Llewellyn Swisher | year= 1897 | editor= | publisher=A. Brown Boughner, Printer |location=[[Morgantown, West Virginia]] |isbn= | OCLC = 680931891 |page= |pages= |url=https://openlibrary.org/books/OL23304577M/History_of_Hampshire_County_West_Virginia |accessdate=|ref=harv | via = [[Internet Archive]] }}
* {{cite book  | last1=McKeown | first1= Bonnie | author-link =  | year=1991 | title=Capon Lake Bridge Interview with Branson Himelwright | publisher= West Virginia Division of Highways District 5 | location = [[Burlington, West Virginia]] | isbn=  |  OCLC =  | url= | accessdate=|ref=harv}}
* {{Cite book| last=Tyrrell | first= Henry Grattan  | author-link = | year = 1911 | title=History of Bridge Engineering | publisher= Henry Grattan Tyrrell | language=| isbn= | url=https://books.google.com/books?id=l0pDAAAAIAAJ | OCLC = 2898376  | location = Chicago |ref=harv  | via = [[Google Books]] }}
* {{Cite book| last=[[Virginia House of Delegates]] | first= | author2= [[Virginia General Assembly]]  | author-link = | year = 1828 | title=Journal of the House of Delegates of the Commonwealth of Virginia; Begun and Holden in the City of Richmond, In the County of Henrico | publisher= Thomas W. White | language=| isbn= | url=https://books.google.com/books?id=AjsUAAAAYAAJ | OCLC = |ref={{harvid|Virginia House of Delegates|1828}}  | via = [[Google Books]] | location = [[Richmond, Virginia]] }}
* {{cite book  | last1=West Virginia Division of Highways District 5 | first1=  | author-link =  | year=1991 | title=Capon Lake Bridge Interview with Jacob "Moss" Rudolph | publisher= West Virginia Division of Highways District 5 | location = [[Burlington, West Virginia]] | isbn=  |  OCLC =  | url= | accessdate=|ref=harv}}
* {{cite book  | author=West Virginia State Road Commission  | author-link =  | year=1936 | title=Annual Report of the State Road Commission of West Virginia, 1935–36 | publisher= West Virginia State Road Commission | location = [[Charleston, West Virginia]] | isbn=  |  OCLC = 16834834 | url= | accessdate=|ref=harv}}
* {{cite book  | author=West Virginia State Road Commission  | author-link =  | year=1938 | title=Annual Report of the State Road Commission of West Virginia, 1937–38 | publisher= West Virginia State Road Commission | location = [[Charleston, West Virginia]] | isbn=  |  OCLC = 16834834 | url= | accessdate=|ref=harv}}

== External links ==
* {{Commons category-inline|Capon Lake Whipple Truss Bridge (Capon Lake, West Virginia)}}

{{NRHP in Hampshire County, West Virginia}}
{{National Register of Historic Places in West Virginia}}
{{NRHP bridges}}
{{Portal bar|Architecture|Bridges|Geography|National Register of Historic Places|West Virginia}}

{{DEFAULTSORT:Capon Lake Whipple Truss Bridge}}
[[Category:1874 establishments in West Virginia]]
[[Category:1938 establishments in West Virginia]]
[[Category:Bridges completed in 1874]]
[[Category:Bridges completed in 1938]]
[[Category:Road bridges on the National Register of Historic Places in West Virginia]]
[[Category:Buildings and structures in Hampshire County, West Virginia]]
[[Category:Former road bridges in the United States]]
[[Category:National Register of Historic Places in Hampshire County, West Virginia]]
[[Category:Pedestrian bridges in West Virginia]]
[[Category:Relocated buildings and structures in West Virginia]]
[[Category:Transportation in Hampshire County, West Virginia]]
[[Category:Truss bridges in the United States]]
[[Category:U.S. Route 50]]
[[Category:Whipple truss bridges]]
[[Category:Road bridges in West Virginia]]