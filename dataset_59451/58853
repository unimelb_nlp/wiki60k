{{Infobox Magazine
| title           = Mathematics Magazine
| image_file      = Mathmag.gif
| image_size      = 145 px
| image_caption   = 
| editor          = Walter Stromquist
| editor_title    = 
| previous_editor = 
| staff_writer    = 
| frequency       = Bimonthly
| paid_circulation = 9,500
| unpaid_circulation = 500
| total_circulation = 10,000
| circulation_year = 2008
| category        = [[Mathematics]]
| company         = [[Mathematical Association of America]]
| publisher       = 
| firstdate       = 
| country         = [[United States]]
| based           = [[Washington, D.C.]]
| language        = English
| website         = http://www.maa.org/pubs/mathmag.html
| issn            = 0025-570X
}}

'''''Mathematics Magazine''''' is a [[peer review|refereed]] bimonthly publication of the [[Mathematical Association of America]].  Its intended audience is teachers of collegiate mathematics, especially at the junior/senior level, and their students. It is explicitly a journal of mathematics rather than pedagogy. Rather than articles in the terse "theorem-proof" style of [[research]] journals, it seeks articles which provide a context for the mathematics they deliver, with examples, applications, illustrations, and historical background.<ref>
{{cite web |url=http://www.maa.org/pubs/mm-guide.html |title=Mathematics Magazine: Guidelines for Authors |accessdate=2009-01-31 |work= |publisher=Mathematical Association of America |date=June 2, 2008 }}
</ref>
Paid circulation in 2008 was 9,500 and total circulation was 10,000.<ref>
{{cite journal |date=October 2008 |title=Statement of Ownership, Management, and Circulation  |journal=Mathematics Magazine |volume=81 |issue=4 |pages=316 |issn=0025-570X |url= |quote= }}</ref>

''Mathematics Magazine'' is a continuation of ''Mathematics News Letter'' (1926-1934) and ''National Mathematics Magazine'' (1934-1945.)<ref>[http://www.maa.org/press/periodicals/mathematics-magazine/history-of-mathematics-magazine ''History of Mathematics Magazine''], MAA.  Accessed on line Sep. 28, 2016.</ref>  [[Doris Schattschneider]] became the first female editor of ''Mathematics Magazine'' in 1981. <ref name="parson">{{citation|url=http://math.unca.edu/parsons-lecture/2005/bio|title=2005 Parson Lecturer - Dr. Doris Schattschneider|publisher=[[University of North Carolina at Asheville]], Department of Mathematics|accessdate=2013-07-13}}.</ref><ref name="bwm">{{citation|url=http://www.agnesscott.edu/lriddle/women/schatt.htm|title=Doris Schattschneider|series=Biographies of Women Mathematicians|publisher=[[Agnes Scott College]]|date=April 5, 2013|accessdate=2013-07-13|first=Larry|last=Riddle}}.</ref> 

The MAA gives the [[Carl B. Allendoerfer]] [[Carl B. Allendoerfer Award|Awards]] annually "for articles of expository excellence" published in ''Mathematics Magazine''.<ref>
{{cite web |url=http://www.maa.org/awards/allendoerfer.html|title=The Mathematical Association of America's Carl B. Allendoerfer Award |accessdate=2009-01-31 |work= |publisher=Mathematical Association of America |date=January 29, 2009 }}
</ref>

==See also==
*''[[American Mathematical Monthly]]''
* [[Carl B. Allendoerfer Award]]

==Notes==
<references />

==Further reading==
* {{cite book | editor1-last = Alexanderson | editor1-first = Gerald L. | editor1-link = Gerald L. Alexanderson | editor2-last = Ross | editor2-first = Peter | title = The Harmony of the World: 75 Years of Mathematics Magazine | publisher = Mathematical Association of America | location = Washington | year = 2007 | isbn = 978-0-88385-560-7 }}

[[Category:Mathematics education journals]]
[[Category:Academic journals published by learned and professional societies of the United States]]


{{math-journal-stub}}