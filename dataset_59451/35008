{{good article}}
{{Infobox hurricane season
| Basin=Atl
| Year=1967
| Track=1967 Atlantic hurricane season summary map.png
| First storm formed=June 10, 1967
| Last storm dissipated=October 31, 1967
| Strongest storm name=[[Hurricane Beulah|Beulah]]
| Strongest storm pressure=923
| Strongest storm winds=140
| Average wind speed=1
| Total depressions=29
| Total storms=8
| Total hurricanes=5
| Total intense=1
| Fatalities=79
| Damages=207
| Inflated=1
| five seasons=[[1965 Atlantic hurricane season|1965]], [[1966 Atlantic hurricane season|1966]], '''1967''', [[1968 Atlantic hurricane season|1968]], [[1969 Atlantic hurricane season|1969]]
}}
The '''1967 Atlantic hurricane season''' featured the bulk of its activity in September and October, although weaker [[tropical cyclone|tropical depressions]] were observed from June to October. The first depression originated on June&nbsp;10, and the final storm &ndash; Heidi &ndash; lost tropical characteristics on October&nbsp;31. [[Hurricane Beulah]] &ndash; the strongest storm of the season &ndash; was also the most damaging, causing 59&nbsp;deaths and $207&nbsp;million in damage (1967&nbsp;[[United States dollar|USD]]) along its 16-day path. Beulah formed on September&nbsp;5 and soon after crossed southern [[Martinique]] into the [[Caribbean Sea]]. On the island, it dropped {{convert|475|mm|in|abbr=on|order=flip}} of rainfall in [[Les Anses-d'Arlet]], causing severe flooding. Widespread evacuations occurred along the southern coast of the [[Dominican Republic]] due to fears of a repeat of [[Hurricane Inez]] from the previous year. After brushing the south coast of [[Hispaniola]], the hurricane weakened and re-intensified, striking the [[Yucatán Peninsula]] and later near the United States/Mexico border. There, it caused severe river flooding, killing 34&nbsp;people in the two countries.

Hurricanes Arlene and Chloe, as well as several tropical depressions, originated from [[tropical wave]]s that left the coast of Africa. Chloe lasted for 15&nbsp;days, eventually dissipating over France after wrecking a ship offshore northern Spain, killing 14&nbsp;people. [[Hurricane Doria (1967)|Hurricane Doria]] co-existed with Beulah and Chloe, taking an unusual trajectory over the eastern United States; it killed three people in a boating accident offshore [[New Jersey]]. In late September, Tropical Storm Edith was a minimal storm that moved through the Lesser Antilles without serious impact. Hurricane Fern killed three people when it struck Mexico north of [[Tampico]]. Tropical Storm Ginger existed in the far eastern Atlantic in early October, and Hurricane Heidi stalled over the northern Atlantic Ocean at the end of the month.

==Season summary==
[[File:Hurricanes Beulah Chloe Doria.png|thumb|Composite of ESSA 2 photographs showing hurricanes [[Hurricane Beulah|Beulah]], [[Hurricane Doria (1967)|Doria]], and Chloe]]
The [[Atlantic hurricane season|season]] began on June&nbsp;1, which was the date when the [[National Hurricane Center]] (NHC) activated radar stations across the Caribbean and Gulf of Mexico.<ref>{{cite news|agency=Associated Press|date=May 31, 1967|title=Hurricane Season Opens Thursday|accessdate=May 22, 2011|url=https://news.google.com/newspapers?id=OzsgAAAAIBAJ&sjid=9WUEAAAAIBAJ&pg=7367,7377231&dq=hurricane&hl=en|newspaper=Sarasota Herald-Tribune}}</ref> The season ended on November&nbsp;30, which ended the conventional delimitation of the time period when most [[tropical cyclone]]s form in the [[Atlantic basin]].<ref>{{cite news|agency=Associated Press|date=May 28, 1967|title=Weathermen Brace for Hurricane Season|newspaper=The News and Courier}}</ref> At the end of the season, NHC director [[Gordon Dunn (meteorologist)|Gordon Dunn]] retired and was replaced by [[Robert Simpson (meteorologist)|Robert Simpson]].<ref>{{cite web|author=Russell Pfost|date=August 15, 2013|author2=Pablo Santos|title=History of the National Weather Service Forecast Office Miami, Florida|publisher=National Oceanic and Atmospheric Administration|accessdate=August 27, 2016|url=http://www.srh.noaa.gov/mfl/?n=floridahistorypage}}</ref> For the first time in 1967, the NHC tracked weaker, developing tropical disturbances, observing that 90% of systems do not develop. [[Tropical cyclogenesis]] &ndash; the process in which a tropical cyclone develops &ndash; resulted mainly from [[tropical wave]]s, the [[Intertropical Convergence Zone]] (ITCZ), and decaying [[cold front|frontal systems]]. There were 30&nbsp;tropical waves that exited the west coast of Africa at [[Dakar]], [[Senegal]], of which 14 became tropical depressions. Another 20&nbsp;tropical disturbances originated offshore the [[Mid-Atlantic states]], and 7&nbsp;disturbances derived from [[cold-core low]]s.<ref name="dis"/>

The first [[tropical cyclone naming|named storm]] &ndash; Arlene &ndash; did not form until August&nbsp;28 and became a hurricane on September&nbsp;2. At the time, only seven known seasons began later, although 1967 would the most active among these late starting seasons. The latency was caused by a stronger than normal [[ridge (meteorology)|ridge]] across the Atlantic Ocean, which suppressed [[convection (meteorology)|convective]] activity across the basin and prevented the formation of strong [[low pressure area]]s.<ref name="mwr"/> From June to October, the NHC tracked 29&nbsp;tropical depressions, which is an area of disturbed weather that has a closed circulation and [[maximum sustained wind]]s of less than 39&nbsp;mph (63&nbsp;km/h). It was the first year that the NHC tracked the weaker depressions. Operationally, the agency followed and numbered 23&nbsp;depressions, and discovered in a post-season analysis that another six systems became depressions. Eight depressions attained [[gale]]-force winds and were [[tropical cyclone naming|named]] from a sequential list, and six storms intensified to hurricane-strength &ndash; 74&nbsp;mph (119&nbsp;km/h).<ref name="dis">{{cite journal|title=Atlantic Tropical Disturbances, 1967|journal=Monthly Weather Review|volume=96|number=4|date=April 1968|author=Bob Simpson|author2=Neil Frank|author3=David Shideler|author4=H. M. Johnson|url=http://docs.lib.noaa.gov/rescue/mwr/096/mwr-096-04-0251.pdf|format=PDF|accessdate=August 24, 2016}}</ref>

== Systems ==

=== Hurricane Arlene ===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Arlene September 3, 1967, 1530Z.jpg
|Track=Arlene 1967 track.png
|Formed=August 28
|Dissipated=September 4
|1-min winds=75
|Pressure=982
}}
Following a series of weak tropical depressions emerging from the west coast of Africa, the ITCZ became more active at the end of August. A tropical wave exited the coast of Africa on August&nbsp;24, and by the next day, a [[Pan American World Airways|Pan American]] flight observed a circulation with falling pressures. Based on the system's organization on satellite imagery, the NHC assessed that Tropical Depression Five developed late on August&nbsp;28 about 740&nbsp;mi (1,190&nbsp;km) west of [[Cabo Verde]]. Steered by a strong ridge to the northeast, the nascent system tracked northwestward. After two ships reported gale-force winds, the NHC began issuing advisories on Tropical Storm Arlene at 08:00&nbsp;[[Coordinated Universal Time|UTC]] on August&nbsp;30.<ref name="dis"/><ref name="mwr">{{cite journal|url=http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1967.pdf|title=The Hurricane Season of 1967|author=Arnold L. Sugg|author2=Joseph M. Pelissier|date=April 1968|volume=96|issue=4|journal=Monthly Weather Review|accessdate=September 2, 2014}}</ref> That day, the [[Hurricane Hunters]] observed winds of 70&nbsp;mph (110&nbsp;km/h),<ref name="mwr"/> although the storm was well below hurricane intensity at that time;{{Atlantic hurricane best track}} this was due to Arlene's interaction with the strong ridge.<ref name="mwr"/>

Arlene failed to intensify much for several days while passing through the Mid-Atlantic [[Tropical upper tropospheric trough|upper-level trough]], although the wind speeds gradually increased. On September&nbsp;2, Arlene turned to the north due to an approaching [[Trough (meteorology)|trough]] moving eastward from the [[Northeastern United States]]. Around that time, it passed about 500&nbsp;mi (800&nbsp;km) east of Bermuda. The storm's convection wrapped around the center and organized further as Arlene progressed northward. Turning to the northeast, the storm attained hurricane status early on September&nbsp;3. Later that day, the Hurricane Hunters recorded [[maximum sustained wind]]s of 85&nbsp;mph (140&nbsp;km/h) and a minimum barometric pressure of {{convert|982|mbar|inHg|abbr=on|lk=on}}, which would be Arlene's peak intensity. On the next day, the cyclone weakened to a tropical storm due to stronger wind shear, and Arlene slowed its forward motion due to a ridge to the north. Soon after, the storm transitioned into an [[extratropical cyclone]], and was absorbed by the trough late on September&nbsp;4 to the southeast of [[Newfoundland]].<ref name="mwr"/>{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Chloe===
{{Infobox Hurricane Small
|Basin=Atl
|Image=ChloeSep141967ESSA5.png
|Track=Chloe 1967 track.png
|Formed=September 5
|Dissipated=September 21
|1-min winds=95
|Pressure=958
}}
On September&nbsp;4, a tropical wave exited the west coast of Africa, and by 00:00&nbsp;UTC the following day, the system organized into Tropical Depression Eight between [[Cabo Verde]] and Senegal.<ref name="dis"/>{{Atlantic hurricane best track}} Soon after, the depression moved through Cabo Verde, and it continued northwestward due to a passing mid-latitude trough. On September&nbsp;8, the depression intensified into Tropical Storm Chloe, and it quickly intensified thereafter. The Hurricane Hunters observed winds of {{convert|86|mph|km/h|abbr=on}} on September&nbsp;9, indicating that Chloe attained hurricane status while the storm was turning sharply westward. Interaction with Hurricane Doria to the west turned Chloe more toward the northwest on September&nbsp;11. Two days later, Chloe reached peak winds of 110&nbsp;mph (175&nbsp;km/h) and a minimum pressure of {{convert|958|mbar|inHg|abbr=on}}.<ref name="mwr"/>{{Atlantic hurricane best track}}

After maintaining peak winds for about 36&nbsp;hours, Chloe began weakening while still over the open waters of the east-central Atlantic Ocean. The [[Westerlies]] turned Chloe east-northeastward on September&nbsp;17, and the storm was last observed by the Hurricane Hunters southeast of Newfoundland on the next day. On September&nbsp;21, Chloe reached the coastline of France near [[Bordeaux]] as an extratropical cyclone, although it was unknown when the cyclone lost its tropical characteristics. It quickly dissipated over central France.<ref name="mwr"/> Off the northern coast of [[A Coruña]], Spain, high waves from Chloe sank the [[:de:Fiete Schulze (ship)|Fiete Schulze]] &ndash; a German [[cargo ship]] attempting to circumnavigate the world. Of the 42&nbsp;person crew, 14&nbsp;people drowned, and the others were rescued and brought to a [[West Germany|West German]] hospital, sparking fear of kidnapping from the [[East Germany|East German]] government.<ref name="mwr"/><ref>{{cite web|publisher=Wreck Site|accessdate=August 26, 2013|title=MV Fiete Schulze (+1967)|url=http://wrecksite.eu/wreck.aspx?181555}}</ref>
{{Clear}}

===Hurricane Beulah===
{{Infobox Hurricane Small
|Basin=Atl
|Image=BeulahSep1919671919z.JPG
|Track=Beulah 1967 track.png
|Formed=September 5
|Dissipated=September 22
|1-min winds=140
|Pressure=923
}}
{{Main|Hurricane Beulah}}
A tropical wave exited the coast of Africa on August&nbsp;28. Moving westward, it organized into Tropical Depression Seven at 12:00&nbsp;UTC on September&nbsp;5, while located about 170&nbsp;mi (270&nbsp;km) northeast of [[Barbados]]. On September&nbsp;7, the depression intensified into Tropical Storm Beulah, which crossed into the Caribbean Sea that day. After continued strengthening, Beulah became a hurricane on September&nbsp;8, and two days later reached an initial peak of 150&nbsp;mph (240&nbsp;km/h) winds to the southwest of Puerto Rico. An [[anticyclone]] over the Bahamas turned the hurricane westward, as changing upper-level conditions from a passing trough to the north, as well as land interaction with Hispaniola, greatly weakened Beulah. The cyclone had winds of 85&nbsp;mph (140&nbsp;km/h) when it struck the [[Barahona Province|Barahona peninsula]] in southern Dominican Republic on September&nbsp;11. The track shifted to the southwest and weakened further to a tropical storm.<ref name="mwr"/>

On September&nbsp;13, Beulah began a steady track to the northwest while passing south of Jamaica. On the next day, it re-intensified into a hurricane due to favorable conditions, strengthening to a major hurricane by September&nbsp;15. Beulah weakened slightly before making landfall on [[Cozumel]] and later the east coast of the [[Yucatán Peninsula]]. Emerging into the Gulf of Mexico with much of its former intensity, the hurricane restrengthened over the warm waters, attaining peak winds of 160&nbsp;mph (260&nbsp;km/h) early on September&nbsp;20, making it a [[List of Category 5 Atlantic hurricanes|Category 5]] on the Saffir-Simpson scale. At the same time, Hurricane Hunters reported a minimum pressure of {{convert|923|mbar|inHg|abbr=on}}, the second-lowest aircraft reading at the time after [[Hurricane Hattie]] in 1961. Beulah weakened slightly before making its final landfall around 12:00&nbsp;UTC on September&nbsp;20, just south of the United States/Mexico border. It weakened quickly over land and stalled near [[Alice, Texas]], before turning to the southwest and dissipating over [[Nuevo León]] on September&nbsp;22.<ref name="mwr"/>

On [[Martinique]], Beulah dropped {{convert|475|mm|in|abbr=on|order=flip}} of rainfall in [[Les Anses-d'Arlet]].<ref>{{cite report|url=http://pluiesextremes.meteo.fr/antilles/1967-Beulah.html|title=1967 Beulah: Ouragan|type=Report|language=French|accessdate=September 6, 2015|publisher=Météo-France|work=Pluies extrêmes aux Antilles}}</ref> Flooding rains damaged roads, bridges, and houses on Martinique and neighboring [[Saint Lucia]]. In the Lesser Antilles, Beulah caused $7.5&nbsp;million in damage and 17&nbsp;deaths. The storm caused minor damage and one death in southern Puerto Rico. After the severe impacts of [[Hurricane Inez]] a year prior, about 200,000&nbsp;people evacuated the southern coast of the Dominican Republic. There, Beulah left heavy damage to roads, bridges, and the banana and coffee crops, but the evacuations led to a low death toll of two in the nation. Minor water damage occurred along Haiti's southern [[Tiburon Peninsula]]. On Cozumel, Beulah's strong winds destroyed 40% of the houses and heavily damaged many hotels, severely impacting the tourism industry. Along the northern Yucatán Peninsula, the winds wrecked a clock tower in [[Tizimín]], killing five.<ref name="mwr"/> Beulah dropped heavy rainfall in southern Texas and northeastern Mexico, peaking at {{convert|27.38|in|mm|abbr=on}} in [[Pettus, Texas]].<ref>{{Cite report|author=David M. Roth|title=Hurricane Beulah - September 8-24, 1967|publisher=Weather Prediction Center|accessdate=August 26, 2016|date=March 6, 2013|url=http://www.wpc.ncep.noaa.gov/tropical/rain/beulah1967.html}}</ref> The rains caused record river flooding, with a peak crest of {{convert|53.4|ft|m|abbr=on}} along the [[San Antonio River]] at [[Goliad, Texas|Goliad]]. In northeastern Mexico, Beulah killed 19&nbsp;people and left 100,000&nbsp;people homeless. In Texas, damage reached $200&nbsp;million, and there were 15&nbsp;deaths, 5 of whom related to a tornado outbreak.<ref name="mwr"/>
{{clear}}

===Hurricane Doria===
{{Main|Hurricane Doria (1967)}}
{{Infobox Hurricane Small
|Basin=Atl
|Image=DoriaSep141967ESSA5.png
|Track=Doria 1967 track.png
|Formed=September 8
|Dissipated=September 21
|1-min winds=75
|Pressure=973
}}
A decaying cold front led to Tropical Depression Nine developing on September&nbsp;8 off the east coast of Florida. It drifted to the west-southwest before turning sharply to the northeast on September&nbsp;9. That day it intensified into Tropical Storm Doria, becoming a hurricane on September&nbsp;10 while {{convert|200|mi|km|abbr=on}} east of the [[Florida]]&ndash;[[Georgia (U.S. state)|Georgia]] border. It briefly weakened to a tropical storm on September&nbsp;11, but by the next day had already re-strengthened into a hurricane. Doria attained peak winds of 85&nbsp;mph (140&nbsp;km/h) on September&nbsp;13, after which a ridge over New England turned the storm westward. On September&nbsp;16, the hurricane weakened to tropical storm status to the south of New Jersey. Later that day, Doria made landfall near [[Virginia Beach, Virginia]], and it later turned southward, re-emerging into the Atlantic Ocean east of [[Cape Lookout (North Carolina)|Cape Lookout]] as a tropical depression. It crossed over its former path before turning eastward, dissipating on September&nbsp;21 southwest of Bermuda. Doria left $150,000 in minor coastal damage and killed three people when capsizing a boat offshore [[Ocean City, New Jersey]].<ref name="mwr"/>{{Atlantic hurricane best track}} 
{{clear}}

===Tropical Storm Edith===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Edith (1967).JPG
|Track=Edith 1967 track.png
|Formed=September 26
|Dissipated=October 1
|1-min winds=50
|Pressure=1000
}}
A tropical disturbance moved westward from the African coast on September&nbsp;20. On September&nbsp;26, satellite imagery and ship observations indicated that a tropical depression about 830&nbsp;mi (1,340&nbsp;km) east of Barbados.<ref name="dis"/><ref name="mwr"/> At 21:00&nbsp;UTC, the NHC issued their first advisory, naming the system ''Edith''.<ref>{{cite report|title=Tropical Cyclone Discussion Edith|publisher=National Hurricane Center|date=September 26, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/edith/tropdisc/tcd2622z.gif|format=GIF}}</ref> A nearby ship reported {{convert|16|ft|m|abbr=on}} waves on September&nbsp;27, potentially indicating stronger winds during periods without meteorological observations,<ref>{{cite report|title=Tropical Cyclone Discussion Edith|publisher=National Hurricane Center|date=September 27, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/edith/tropdisc/tcd2709z.gif|format=GIF}}</ref> although unfavorable conditions prevented initial development.<ref>{{cite report|title=Tropical Cyclone Discussion Edith|publisher=National Hurricane Center|date=September 26, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/edith/tropdisc/tcd2703z.gif|format=GIF}}</ref> It was not until 12:00&nbsp;UTC that Edith attained tropical storm force winds. Twelve hours later, the storm reached peak winds of 60&nbsp;mph (95&nbsp;km/h),{{Atlantic hurricane best track}} a trend that spurred [[tropical cyclone warnings and watches|hurricane watches]] from [[Dominica]] northward through the [[Leeward Islands]].<ref>{{cite report|title=Tropical Cyclone Discussion Edith|publisher=National Hurricane Center|date=September 28, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/edith/tropdisc/tcd2815z.gif|format=GIF}}</ref> The storm failed to intensify due to its proximity to a cold upper-level trough and releasing too much latent heat. On September&nbsp;30, Edith passed over Dominica as a weakened tropical storm and dissipated the next day over the eastern Caribbean Sea. It caused gusty winds and minor damage during its passage through the Lesser Antilles.<ref name="mwr"/>
{{clear}}

===Hurricane Fern===
{{Infobox Hurricane Small
|Basin=Atl
|Track=Fern 1967 track.png
|Image=Fern Oct 3 1967.png
|Formed=October 1
|Dissipated=October 4
|1-min winds=75
|Pressure=987
}}
Toward the end of September, a powerful cold front moved through the Gulf of Mexico and stalled over the Bay of Campeche. Convection persisted across the region as the surface pressure dropped.<ref name="mwr"/> Satellite imagery suggested that Tropical Depression Fourteen developed on October&nbsp;1 about 140&nbsp;mi (220&nbsp;km) northwest of [[Ciudad del Carmen]].<ref name="dis"/><ref>{{cite report|title=Tropical Cyclone Discussion Fern|publisher=National Hurricane Center|date=October 2, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/fern/tropdisc/tcd0218z.gif|format=GIF}}</ref> As the system organized more, it tracked northward, although a ridge to the north steered the nascent system to the west. On October&nbsp;2, the British ship ''Plainsman'' observed gale-force winds, prompting the NHC to upgrade the depression to Tropical Storm Fern.<ref name="mwr"/> A small system, the storm intensified further to hurricane status on October&nbsp;3, reaching peak winds of 85&nbsp;mph (140&nbsp;km/h) and a minimum barometric pressure of {{convert|987|mbar|inHg|abbr=on}}.{{Atlantic hurricane best track}} [[Upwelling]] and cold air left in the wake of Hurricane Beulah caused Fern to weaken slightly as it approached the [[Gulf Coast of Mexico]]. Around 06:00&nbsp;UTC on October&nbsp;4, Fern made landfall about 30&nbsp;mi (50&nbsp;km) north of [[Tampico]], [[Tamaulipas]], possibly having weakened to a tropical storm. It rapidly weakened over land, dissipating by 18:00&nbsp;UTC.<ref name="mwr"/>{{Atlantic hurricane best track}}

Fern's landfall was accompanied by an area of heavy rainfall that extended into the mountainous areas of Veracruz.<ref>{{cite report|title=Tropical Cyclone Discussion Fern|publisher=National Hurricane Center|date=October 4, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/fern/tropdisc/tcd0409z.gif|format=GIF}}</ref> The rains caused additional flooding along the [[Pánuco River]], which became swollen during Hurricane Beulah two weeks prior. Three people drowned in the floodwaters. Damage was minor related to Fern.<ref name="mwr"/> Along the lower Texas coast, the threat from Fern spurred high tide and small craft warnings from the National Weather Service. Additional members of the Texas National Guard, in place after Beulah, were activated due to the threat from Fern.<ref>{{cite news|agency=Associated Press|title=Hurricane Fern Aiming at Mexico|newspaper=Spokane Daily Chronicle|date=October 3, 1967|accessdate=August 27, 2016|url=https://news.google.com/newspapers?nid=1338&dat=19671003&id=8WZYAAAAIBAJ&sjid=2vcDAAAAIBAJ&pg=3153,483909}}</ref>
{{clear}}

===Tropical Storm Ginger===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Ginger Oct 5 1967.png
|Track=Ginger 1967 track.png
|Formed=October 5
|Dissipated=October 8
|1-min winds=45
|Pressure=1002
}}
An area of convection developed off the west coast of Africa following the westward passage of Tropical Depression Sixteen. On October&nbsp;5, it is estimated that Tropical Depression Seventeen developed from this system,<ref name="dis"/> based on the convective appearance on satellite imagery. Three ships reported {{convert|40|to|45|mph|km/h|abbr=on}} winds on October&nbsp;6,<ref name="mwr"/> which was the basis for the NHC upgrading it to Tropical Storm Ginger, in conjunction with data from Cabo Verde.<ref name="g1">{{cite report|author=Bob Simpson|publisher=National Hurricane Center|date=October 6, 1967|accessdate=August 27, 2016|title=Tropical Storm Ginger Forms Near African Coast|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/ginger/marine/tcm0618z.gif|format=GIF}}</ref> At the time, the storm was located about 400&nbsp;mi (645&nbsp;km) north-northwest of [[Dakar, Senegal]], well east of [[35th meridian west|35° W]] where the NHC began issuing formal tropical cyclone advisories. Instead, the [[Naval Station Rota, Spain|Rota, Spain Naval Fleet Station]] issued gale warnings in relation to the storm.<ref name="g1"/><ref name="ginger">{{cite report|title=Ginger|publisher=National Hurricane Center|year=1967|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/ginger/prenhc/prelim01.gif|format=GIF|accessdate=August 27, 2016}}</ref> Later on October&nbsp;6, it was estimated that Ginger reached peak winds of 50&nbsp;mph (85&nbsp;km/h) and a minimum barometric pressure of {{convert|1002|mbar|inHg|abbr=on}}. On October&nbsp;7, the storm curved west-southwestward and quickly weakened into a tropical depression. Ginger dissipated on October&nbsp;8 to the north of Cabo Verde.{{Atlantic hurricane best track}} 
{{clear}}

===Hurricane Heidi===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Heidi Oct 27 1967.png
|Track=Heidi 1967 track.png
|Formed=October 19
|Dissipated=October 31
|1-min winds=80
|Pressure=981
}}
An area of convection persisted on October&nbsp;16 between the Lesser Antilles and Cabo Verde. It moved west-northwestward for several days, developing into Tropical Depression Twenty-Two on October&nbsp;19 about 500&nbsp;mi (800&nbsp;km) northeast of the Lesser Antilles.<ref name="dis"/><ref name="mwr"/> The ''S.S. Sunrana'' moved through the storm on October&nbsp;20, reporting winds of {{convert|58|mph|km/h|abbr=on}}. At the time, the system was not named due to the lack of a warm thermal core,<ref name="mwr"/> although it was later assessed as a tropical storm as of 18:00&nbsp;UTC that day.{{Atlantic hurricane best track}} On October&nbsp;21, the Hurricane Hunters observed a weak circulation with winds of only {{convert|35|mph|km/h|abbr=on}}. At the same time, the storm was located along the edge of a [[weather front|baroclinic zone]], which limited strengthening. Curving northeastward due to an approaching trough, the storm intensified more on October&nbsp;22, as a ship reported winds of {{convert|70|mph|km/h|abbr=on}}. That day, data from the Hurricane Hunters observed a warm core,<ref name="mwr"/> and the NHC classified the system as Tropical Storm Heidi. The storm was more of a [[subtropical cyclone|hybrid storm]] initially,<ref>{{cite report|title=Tropical Cyclone Discussion Heidi|publisher=National Hurricane Center|date=October 22, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/heidi/tropdisc/tcd2215z.gif|format=GIF}}</ref> with the strongest winds near the center, spurring gale warnings for Bermuda.<ref>{{cite report|title=Bureau Marine/Aviation/Military Advisory for Tropical Storm Heidi|date=October 22, 1967|accessdate=August 27, 2016|publisher=National Hurricane Center|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/heidi/marine/tcm2216z.gif|format=GIF}}</ref> However, winds there only reached about {{convert|15|mph|km/h|abbr=on}} during the storm's passage.<ref>{{cite news|title=Hurricane No Threat to Land|agency=Associated Press|date=October 23, 1967|accessdate=August 27, 2016|newspaper=Ocala Star-Banner|url=https://news.google.com/newspapers?nid=1356&dat=19671023&id=tWBIAAAAIBAJ&sjid=UwUEAAAAIBAJ&pg=5934,5002572}}</ref>

The NHC initially anticipated that Heidi would become extratropical within two days.<ref>{{cite report|title=Tropical Cyclone Discussion Heidi|publisher=National Hurricane Center|date=October 22, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/heidi/tropdisc/tcd2303z.gif|format=GIF}}</ref> Early on October&nbsp;23, the agency upgraded the storm to hurricane status about 105&nbsp;mi (175&nbsp;mi) southeast of Bermuda. Heidi moved quickly eastward with the approaching trough until October&nbsp;25, when a building ridge caused the hurricane to move slowly northeastward in an area of light wind shear. Early on October&nbsp;26, Heidi attained peak winds of 90&nbsp;mph (150&nbsp;km/h) about halfway between Bermuda and the [[Azores]].<ref name="mwr"/>{{Atlantic hurricane best track}} By the next day, the hurricane had become much larger, with characteristics of an extratropical storm despite maintaining the warm thermal core.<ref>{{cite report|title=Tropical Cyclone Discussion Heidi|publisher=National Hurricane Center|date=October 27, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/heidi/tropdisc/tcd2715z.gif|format=GIF}}</ref> After stalling on October&nbsp;29, Heidi turned westward and weakened to tropical storm status.{{Atlantic hurricane best track}} Another approaching trough turned the storm back to the northeast on October&nbsp;31.<ref>{{cite report|title=Tropical Cyclone Discussion Heidi|publisher=National Hurricane Center|date=October 31, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/heidi/tropdisc/tcd3109z.gif|format=GIF}}</ref> Later that day, Heidi started losing tropical characteristics,<ref>{{cite report|title=Tropical Cyclone Discussion Heidi|publisher=National Hurricane Center|date=October 31, 1967|accessdate=August 27, 2016|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1967/heidi/tropdisc/tcd3121z.gif|format=GIF}}</ref> transitioning into an extratropical cyclone by November&nbsp;1. Later that day, the remnants of Heidi were absorbed into the prevailing weather conditions of the north Atlantic Ocean.<ref name="mwr"/>{{Atlantic hurricane best track}}
{{clear}}

===Other systems===
[[File:Tropical Depression Three 1967 rainfall.gif|right|thumb|Rainfall map for Tropical Depression Three]]
On June&nbsp;10, the first tropical depression of the season developed in the western Caribbean Sea. It moved northwestward, dissipating over the eastern Yucatán Peninsula on June&nbsp;12. At the same time that the previous system developed, Tropical Depression Two originated north of the Lesser Antilles. This system moved northward and passed just east of Bermuda on June&nbsp;11. After turning to the northeast, the depression dissipated on June&nbsp;13. On June&nbsp;18, Tropical Depression Four also developed north of the Lesser Antilles, taking a similar trajectory to the north-northwest, and dissipating on June&nbsp;20 east of Bermuda.{{Atlantic hurricane best track}}<ref name="dis"/>

Satellite imagery and ship data indicated that Tropical Depression Three formed on June&nbsp;14 to the northeast of the northeastern Bahamas,<ref name="mwr"/> associated with an upper-level low. With a ridge to the northeast, the depression moved northwestward toward the southeastern United States.<ref name="3rain">{{cite report|author=David M. Roth|publisher=Weather Prediction Center|title=Tropical Depression Three - June 15-22, 1967|url=http://www.wpc.ncep.noaa.gov/tropical/rain/td03aof1967.html|date=March 6, 2013|accessdate=August 24, 2016}}</ref> Conditions were not favorable for strengthening, with cool air and minimal outflow, although the winds approached gale-force.<ref name="mwr"/> An approaching cold front turned the system to the northeast on June&nbsp;18.<ref name="3rain"/> That day, the depression moved ashore just southwest of [[Wilmington, North Carolina]], and soon lost tropical characteristics.<ref name="mwr"/> The remnants continued to the northeast, transitioning into an extratropical storm that passed just east of Massachusetts on June&nbsp;19. On the next day, the system was absorbed by the front, and continued to the northeast into Nova Scotia by June&nbsp;22. The system dropped locally heavy rainfall, peaking at {{convert|8.86|in|mm|abbr=on}} in [[North Wilkesboro, North Carolina]].<ref name="3rain"/> In South Carolina, the rains caused flooding near [[Myrtle Beach, South Carolina|Myrtle Beach]], concurrent with {{convert|2.5|ft|m|abbr=on}} above-normal tides, which caused $15,000 in damage, mostly to crops. Farther north, the rains were beneficial,<ref name="mwr"/> with totals as high as {{convert|6.71|in|mm|abbr=on}} in [[Saco, Maine]].<ref name="3rain"/>

There were two short-lived, unnumbered depressions in July that emerged off the coast of Africa, and both were unnumbered. The first was observed on July&nbsp;5 near the coast of [[Guinea-Bissau]]; moving westward, it was last noted on July&nbsp;9. The other depression emerged from Senegal on July&nbsp;21 and dissipated south of Cabo Verde on the next day.<ref name="dis"/>{{Atlantic hurricane best track}} Another series of unnumbered tropical depressions exited the coast of Africa in August. On August&nbsp;3, a tropical depression formed just off the coast of [[Mauritania]] and moved southwestward through Cabo Verde the next day. It was last noted on August&nbsp;6. A depression was first noted over eastern Senegal on August&nbsp;10, exiting the African coast the next day. It continued on a westward trajectory, passing south of Cabo Verde, and dissipated on August&nbsp;16. On the same day, another depression emerged from Mauritania and passed through northern Cabo Verde, dissipating on August&nbsp;19. The final in the series of four depressions was first noted on August&nbsp;20 near the Mauritania/[[Western Sahara]] border. The system moved southwestward through Cabo Verde, dissipating on August&nbsp;24.<ref name="dis"/>{{Atlantic hurricane best track}} Tropical Depression Six exited the west coast of Africa on August&nbsp;30, and soon after moved through Cabo Verde. It was last noted on September&nbsp;4.<ref name="dis"/>

A series of tropical waves in late August to early September became hurricanes Arlene, Beulah, and Chloe, and the wave between the latter two emerged from Africa on August&nbsp;30 as Tropical Depression Six. It crossed through Cabo Verde on September&nbsp;1, dissipating three days later over the eastern Atlantic Ocean. Tropical Depression Ten took a similar trajectory, exiting Senegal on September&nbsp;18 and progressing westward near Cabo Verde. It lasted longer than other depressions in the season, dissipating on September&nbsp;26 east of the Lesser Antilles. Tropical Depression Thirteen exited western Africa near [[Guinea]] on September&nbsp;22, which moved westward until September&nbsp;27. It turned northwestward that day and dissipated on September&nbsp;30. Tropical Depression Eleven formed on September&nbsp;25 and crossed Cabo Verde two days later, dissipating on September&nbsp;28.<ref name="dis"/>{{Atlantic hurricane best track}}

On October&nbsp;3, Tropical Depression Sixteen exited the west coast of Africa near [[The Gambia]], dissipating two days later after passing south of Cabo Verde. Tropical Depression Fifteen existed briefly on October&nbsp;4 to the southeast of Bermuda. Short-lived Tropical Depression Eighteen formed near the Bahamas on October&nbsp;8, dissipating the next day after moving northeastward. Tropical Depression Nineteen spawned on October&nbsp;12 north of Hispaniola and moved northwestward through the [[Turks and Caicos Islands]]. After turning to the northeast, it dissipated on October&nbsp;14. On the next day, Tropical Depression Twenty-One originated between the Lesser Antilles and Cabo Verde, which moved westward and dissipated on October&nbsp;17. Tropical Depression Twenty briefly existed on October&nbsp;16, which moved northwestward to the southwest of Bermuda. The final system of the season was Tropical Depression Twenty-Three, which formed northeast of the Bahamas on October&nbsp;26. It tracked eastward before looping back to the northwest, dissipating on October&nbsp;29 about 130&nbsp;mi (215&nbsp;km) southwest of Bermuda.<ref name="dis"/>{{Atlantic hurricane best track}}

==Storm names==
{{See also|List of retired Atlantic hurricane names}}
The following names were used for named storms (tropical storms and hurricanes) that formed in the North Atlantic in 1967. Storms were named Chloe, Doria, Fern, Ginger and Heidi for the first time in 1967. At the end of the season, the name [[Hurricane Beulah|Beulah]] was retired and replaced with Beth in [[1971 Atlantic hurricane season|1971]]. Names that were not assigned are marked in {{tcname unused}}.

{| width=80%
|
* Arlene
* [[Hurricane Beulah|Beulah]]
* Chloe
* [[Hurricane Doria (1967)|Doria]]
* Edith
* Fern
* Ginger
|
* Heidi
* {{tcname unused|Irene}}
* {{tcname unused|Janice}}
* {{tcname unused|Kristy}}
* {{tcname unused|Laura}}
* {{tcname unused|Margo}}
* {{tcname unused|Nona}}
|
* {{tcname unused|Orchid}}
* {{tcname unused|Portia}}
* {{tcname unused|Rachel}}
* {{tcname unused|Sandra}}
* {{tcname unused|Terese}}
* {{tcname unused|Verna}}
* {{tcname unused|Wallis}}
|}

== Contemporaneous seasons ==
{{Portal|Tropical cyclones}}
* [[1967 Pacific hurricane season]]
* [[1967 Pacific typhoon season]]
* [[1966–67 South-West Indian Ocean cyclone season|1966–67]]/[[1967–68 South-West Indian Ocean cyclone season]]

== Notes ==
{{Reflist|2}}

==External links==
* [http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1967.pdf Monthly Weather Review]

{{1967 Atlantic hurricane season buttons}}
{{TC Decades|Year=1960|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1967 Atlantic Hurricane Season}}
[[Category:1967 Atlantic hurricane season| ]]