{{Infobox person
| image       =Mia Kirshner Comic-Con 2012.jpg 
| name        = Mia Kirshner
|caption=Kirshner at the 2012 [[San Diego Comic-Con International]].
| birth_date   = {{birth date and age|1975|01|25}}
| birth_place  = [[Toronto]], [[Ontario]], [[Canada]]
| death_date   =
| death_place  =
| other_names   =
| occupation  = Actress, activist, writer
| years_active = 1989–present
| website    =
| spouse      =
}}

'''Mia Kirshner''' (born January 25, 1975) (though some sources say 1976<ref>{{cite web|url=http://www.filmreference.com/film/68/Mia-Kirshner.html|title=Mia Kirshner Biography (1976-)|publisher=}}</ref><ref>{{cite web|url=http://www.tv.com/people/mia-kirshner/|title=Mia Kirshner|first=|last=TV.com|publisher=}}</ref>) is a Canadian actress, writer and social activist who works in movies and television series. She is known for her role as [[Jenny Schecter]] on the cable TV series ''[[The L Word]]'' (2004–2009), and for her recurring guest role as the terrorist [[Mandy (24 character)|Mandy]] on the TV series ''[[24 (TV series)|24]]'' (2001–2005).

== Early life ==

Kirshner was born in [[Toronto]], Ontario, the daughter of Etti, a teacher, and Sheldon Kirshner, a journalist who wrote for The ''[[Canadian Jewish News]]''.<ref name="ref1">
{{cite news|last=|first=|coauthors= | title = Mia Kirshner surprised by character in 'Mad City' film|pages=|publisher=News Tribune|date=1997-11-15 | url = http://nl.newsbank.com/nl-search/we/Archives?p_product=NewsLibrary&p_multi=TNTB&d_place=TNTB&p_theme=newslibrary2&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EFE5EDE96979D03&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|accessdate=2007-12-12
}}
</ref><ref>{{cite web|url=http://www.cjnews.com/index.php?q=node/108220|title=Defiant actor has impressive goals - The Canadian Jewish News|publisher=|accessdate=22 April 2015}}</ref> Kirshner is a granddaughter of [[The Holocaust|Holocaust]] survivors;<ref name="ref3">
{{cite web | url = http://www.thelwordonline.com/Mia_NYTarticle.shtml | title = Mia Kirshner basks in an erotic mystery |publisher=Thelwordonline.com |date=2004-04-05 |accessdate=2010-08-19
}}
</ref> her father was born in a [[displaced persons camp]] in Germany in 1946, and met Kirshner's mother, a Bulgarian [[Jewish refugee]], after they escaped to [[Israel]].<ref name="ref3" /> Kirshner's paternal grandparents were Jews from Poland.<ref>{{cite web | url = http://thewarrenreport.com/2008/11/26/film-rap-mia-kirshner-i-live-here/ | title = Film Rap: Mia Kirshner – I Live Here |publisher=The Warren Report |date=2008-11-26 |accessdate=2011-07-25
}}
</ref><ref>{{cite news|last=Kirshner|first=Sheldon|title=Buildings in the former Lodz Ghetto still stand|publisher=Canadian Jewish News|date=2009-08-19|url=http://www.cjnews.com/index.php?option=com_content&task=view&id=17433&Itemid=86|accessdate=2011-07-25}}</ref> Kirshner had a [[middle class]] upbringing<ref name="ref4">
{{cite news|last=Aurthur|first=Kate|coauthors= | title = Mia Kirshner backs up her commitment|pages=|publisher=Los Angeles Times|date=2008-10-19 | url = http://www.latimes.com/entertainment/news/la-ca-miakirshner19-2008oct19,0,7058523.story|accessdate=2009-10-19
}}
</ref> and attended [[Forest Hill Collegiate Institute]] but later graduated from the [[Jarvis Collegiate Institute]]. Kirshner studied [[Russian literature]] and 20th-century movie industry at [[McGill University]] in Montreal. Her younger sister, Lauren Kirshner, a writer, was involved in the ''I Live Here'' project.<ref name="lauren">
{{cite news|last=|first=|coauthors= | title = 'Give yourself permission to write'|pages=|publisher=the Varsity (University of Toronto)|date=2009-09-24 | url = http://www.thevarsity.ca/articles/20376 | accessdate=2009-12-31
}}
</ref>

== Career ==

Kirshner found a [[talent agent]] at the age of 12, and was acting professionally by the age of 15.{{Citation needed|date=March 2011}} She made her film debut in 1993 at the age of 18 in [[Denys Arcand]]'s ''[[Love and Human Remains]]''. She convinced her father to sign a "nudity waiver" to play a [[dominatrix]].<ref>{{Cite news |first=Naomi |last=Pfefferman |title=Mia Kirshner Documents A Different ‘L’ Word: Living |date= March 16, 2009 |url=http://www.jewishjournal.com/arts/article/mia_kirshner_documents_a_different_l_word_living_20090204/ |newspaper=Baltimore Jewish Times | accessdate=June 8, 2014}}</ref> Kirshner won a [[Genie Award|Genie]] nomination for Best Performance by an Actress in a Supporting Role.{{Citation needed|date=March 2011}} The following year, she starred in [[Atom Egoyan]]'s ''[[Exotica (film)|Exotica]]''. In 1996, she appeared in ''[[The Crow: City of Angels]]''. She  also played Kitty Scherbatsky in the 1997 version of Anna Kareninna.

Kirshner also appeared in the first three episodes of ''[[24 (TV series)|24]]'' as the [[assassination|assassin]] [[Mandy (24 character)|Mandy]] in 2001. She would later reprise the role for the second season's finale and in the latter half of the show's fourth season.{{Citation needed|date=March 2011}} Also in 2001, Kirshner played Catherine Wyler, ''The Cruelest Girl in School'', in ''[[Not Another Teen Movie]]''. The character is primarily a spoof of [[Kathryn Merteuil]] (played by [[Sarah Michelle Gellar]]) in ''[[Cruel Intentions]]'', and was partially based on Mackenzie Siler (played by [[Anna Paquin]]) from ''[[She's All That]]''. In [[Marilyn Manson (band)|Marilyn Manson's]] music video for "[[Tainted Love]]", which was featured on the movie's soundtrack, she made a [[cameo role|cameo appearance]] as her character Catherine Wyler.

In 2004, Kirshner was cast as author [[Jenny Schecter]], a main character in the drama series ''[[The L Word]].'' She remained with the show for all of the show's six seasons through 2009.<ref name="AusielloFiles1">{{cite news | first=Michael | last=Ausiello | title = Exclusive: 'Vampire Diaries' lures 'L Word' babe Mia Kirshner | date=December 21, 2009 | url = http://ausiellofiles.ew.com/2009/12/21/vampire-diaries-hires-mia-kirshner | work=Entertainment Weekly | accessdate=May 18, 2010 }}</ref>

In 2006, she starred in [[Brian De Palma]]'s ''[[The Black Dahlia (film)|The Black Dahlia]]'' in which she plays the young aspiring actress [[Black Dahlia|Elizabeth Short]], who was mysteriously mutilated and murdered in 1947. While the film itself was critically panned, many reviews singled out her performance for acclaim.<ref name="auto">{{cite web | title=The Black Dahlia | author=Stephanie Zacharek | work=Salon.com | url=http://www.salon.com/entertainment/movies/review/2006/09/15/black_dahlia | date=2006-09-15 | accessdate=2011-08-29}}</ref><ref name="auto1">{{cite web | title='Black Dahlia' may look good, but it's noir lite | author=Mick LaSalle | work=San Francisco Chronicle | url=http://www.sfgate.com/cgi-bin/article.cgi?file=/c/a/2006/09/15/DDGHQL51DM1.DTL&type=movies | date=2006-09-15 | accessdate=2011-08-29}}</ref><ref name="auto2">{{cite web | title=The Black Dahlia | author=J. R. Jones | work=Chicago Reader | url=http://www.chicagoreader.com/chicago/the-black-dahlia/Film?oid=1062534 | date=2006-08-29 | accessdate=2011-08-29}}</ref><ref>{{cite web | title=In This Corner: The Most Notorious Unsolved Murder In California History | author=Timothy Brayton | work=Antagony & Ecstasy | url=http://antagonie.blogspot.com/2006/09/in-this-corner-most-notorious-unsolved.html |date=2007-09-18 | accessdate=2011-08-29}}</ref>  [[Stephanie Zacharek]] of [[Salon.com]], in a largely negative review, notes that the eponymous character was "played wonderfully by Mia Kirshner..."<ref name="auto"/>  Mick LaSalle wrote that Kirshner "makes a real impression of the Dahlia as a sad, lonely dreamer, a pathetic figure."<ref name="auto1"/> J. R. Jones described her performance as "haunting" and that the film's fictional screen tests "deliver the emotional darkness so lacking in the rest of the movie."<ref name="auto2"/> In 2010, Kirshner co-starred in the film ''[[30 Days of Night: Dark Days]]'' which began filming in the fall of 2009.<ref>
{{cite web | url = http://www.bloody-disgusting.com/news/17764 | title = BD's Got Your '30 Days of Night: Dark Days' Casting! |publisher=Bloody-disgusting.com |date= |accessdate=2010-08-19
}}
</ref> In 2010, she was cast as Isobel Flemming, a guest role on ''[[The Vampire Diaries (TV series)|The Vampire Diaries]]''.<ref name="AusielloFiles1" />

In 2011, she voiced the title character in ''[[Bear 71]]'', a [[National Film Board of Canada]] [[web documentary]] that premiered at the [[Sundance Film Festival]].<ref name=Monk>{{cite news|last=Monk|first=Katherine|title=Sundance: Interactive film, Bear 71, blurs lines between wild and wired|url=http://www.canada.com/travel/Sundance+Interactive+film+Bear+blurs+lines+between+wild+wired/6044593/story.html|accessdate=25 January 2012|newspaper=canada.com|publisher=[[Postmedia News]]}}</ref><ref name=huffpo>{{cite news|last=Makarechi|first=Kia|title='Bear 71': Interactive Film At Sundance Tells Dark Side Of Human Interaction With Wildlife|url=http://www.huffingtonpost.com/2012/01/23/bear-71-interactive-film-sundance_n_1225040.html|accessdate=25 January 2012|newspaper=[[Huffington Post]]|date=24 January 2012}}</ref>

On April 20, 2012, it was announced that Kirshner would join the new [[Syfy]] series [[Defiance (TV series)|''Defiance'']].<ref>{{cite web|url=http://www.deadline.com/2012/04/mia-kirshner-and-fionnula-flanagan-join-syfy-series-defiance/|title=Mia Kirshner And Fionnula Flanagan Join Syfy Series 'Defiance'|author=Nellie Andreeva|work=Deadline|accessdate=22 April 2015}}</ref>

On October 9, 2013, it was mentioned on the [[Showcase (Canadian TV channel)|Showcase]] blog that Kirshner would be one of several guest stars in season four of the television series ''[[Lost Girl]]''.<ref>{{cite web|url=http://www.showcase.ca/blogs/3127/lost-girl-makes-a-fierce-return-with-its-fourth-season-exclusively-on-showcase|title=Showcase|work=Showcase|accessdate=22 April 2015}}</ref>

== Philanthropy ==

In October 2008, after seven years in production,<ref name="tavis">
{{cite web | title=Tavis Smiley Show | work=Tavis Smiley Show&nbsp;– Mia Kirshner | date = December 16, 2008| url = http://www.pbs.org/kcet/tavissmiley/archive/200812/20081216_kirshner.html | accessdate=2009-12-31
}}
</ref> Kirshner published the book ''I Live Here'',<ref>
{{cite web | url = http://www.i-live-here.com | title = I Live Here Foundation |publisher=I-live-here.com |date= |accessdate=2010-08-19
}}
</ref> which she co-produced with ex-[[Adbusters]] staffers Paul Shoebridge and Michael Simons,<ref>
{{cite web | url = http://www.thegoggles.org | title = The Goggles |publisher=The Goggles |date= |accessdate=2010-08-19
}}
</ref> as well as writer James MacKinnon. In the book, four different groups of women and children refugees from places such as [[Chechnya]], [[Ciudad Juárez|Juárez]], [[Burma]] and [[Malawi]] tell their life stories. The book features original material from well-known comic and graphic artists including [[Joe Sacco]] and [[Phoebe Gloeckner]]. It was published in the U.S. by [[Random House]]/Pantheon. It was supported logistically by [[Amnesty International]], which will receive proceeds from the book. After the release of the book, the Center for International Studies at [[MIT]] invited Kirshner to run a 4-week course on ''I Live Here'' in January 2009.<ref name="mit">
{{cite web | title=MIT CIS: IAP 2009, I Live Here, Mia Kirshner | work=IAP COURSE: I Live Here—A Human Rights Multimedia Project | url = http://web.mit.edu/cis/eventposter_IAP2009_kirshner.html | accessdate=2009-12-31
}}
</ref>

== In popular culture ==
Kirshner was ranked #43 on the ''[[Maxim (magazine)|Maxim]]'' Hot 100 Women of 2002.<ref name="freejose2002">
{{cite web | title=FreeJose.com | work=Maxim Magazine Hot 100 Women of 2002 | url = http://www.freejose.com/lists/maxim/2002/ | accessdate=April 19, 2007
}}
</ref> She and [[Beverly Polcyn]] were nominated for Best Kiss at the [[2002 MTV Movie Awards]] for ''Not Another Teen Movie''.<ref name="bestkiss2002">
{{cite web | title=About.com | work=Best Kiss nomination, 2002 MTV Movie Awards | url = http://movies.about.com/library/weekly/aamtvawardsa.htm/ | accessdate=June 1, 2007
}}
</ref> In 2011 it was announced that Kirshner would be the face of [[Monica Rich Kosann]]'s jewelry collection.

== Filmography ==
[[Image:Mia Kirshner red lighted cropped 2.jpg|thumb|right|150px|Kirshner in September 2009, during interview]]

===Film===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
|rowspan=2|1993
|''[[Love and Human Remains]]''
|Benita
|
|-
|''Cadillac Girls''
|Page
|
|-
|1994
|''[[Exotica (film)|Exotica]]''
|Christina
|
|-
|rowspan=2|1995
|''[[Murder in the First (film)|Murder in the First]]''
|Adult Rosetta Young
|
|-
|''{{sortname|The|Grass Harp|The Grass Harp (film)}}''
|Maude Riordan
|
|-
|1996
|''{{sortname|The|Crow: City of Angels}}''
|Sarah
|
|-
|rowspan=2|1997
|''[[Anna Karenina (1997 film)|Anna Karenina]]''
|Princess Yekaterina Alexandrovna Shcherbatsky, "Kitty"
|
|-
|''[[Mad City (film)|Mad City]]''
|Laurie Callahan
|
|-
|rowspan=2|1999
|''Saturn''
|Sarah
|
|-
|''Out of the Cold''
|Deborah Berkowitz
|
|-
|rowspan=3|2000
|''James Draminski''
|James Draminski
|Short film
|-
|''Innocents''
|Dominique Denright
|
|-
|''Cowboys and Angels''
|Candice
|
|-
|rowspan=3|2001
|''[[Century Hotel]]''
|Dominique
|
|-
|''[[According to Spencer]]''
|Melora
|
|-
|''[[Not Another Teen Movie]]''
|Catherine Wyler
|
|-
|rowspan=2|2002
|''[[New Best Friend]]''
|Alicia Campbell
|
|-
|''[[Now & Forever (2002 film)|Now & Forever]]''
|Angela Wilson
|
|-
|2003
|''[[Party Monster (2003 film)|Party Monster]]''
|Natasha
|
|-
|2005
|''{{sortname|The|Iris Effect|nolink=1}}''
|Rebecca
|
|-
|2006
|''[[The Black Dahlia (film)|The Black Dahlia]]''
|[[Black Dahlia|Elizabeth Short]]
|
|-
|2008
|''[[Miss Conception]]''
|Clem
|
|-
|2010
|''[[30 Days of Night: Dark Days]]''
|Lilith
|Video
|-
|2011
|''[[388 Arletta Avenue]]''
|Amy
|
|-
|2012
|''{{sortname|The|Barrens|The Barrens (film)}}''
|Cynthia Vineyard
|
|-
|2013
|''I Think I Do''
|Julia
|
|-
|2016
|''[[Milton's Secret]]''
|Jane Adams
|
|}

===Television===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
|1989
|''[[War of the Worlds (TV series)|War of the Worlds]]''
|Jo
|Episode: "Loving the Alien"
|-
|1990
|''[[Danger Bay]]''
|Catherine Walker
|Episode: "Live Wires"
|-
|1990–1991
|''[[Dracula: The Series]]''
|Sophie Metternich
| 21 episodes
|-
|1991
|''[[My Secret Identity]]''
|Alana Porter
|Episode: "My Other Secret Identity"
|-
|1991
|''[[Tropical Heat]]''
|Cathy Paige
|Episode: "Runaway"
|-
|1992
|''Tropical Heat''
|Sandy
|Episode: "Stranger in Paradise"
|-
|1992
|''[[Road to Avonlea]]''
|Emily Everett-Smythe
|Episode: "High Society"
|-
|1992
|''[[Are You Afraid of the Dark?]]''
|Pam Pease / Dora Pease
|Episode: "The Tale of the Hungry Hounds"
|-
|1995
|''Johnny's Girl''
|Amy Ross
|TV film
|-
|2001–2005
|''[[24 (TV series)|24]]''
|[[Mandy (24 character)|Mandy]]
|7 episodes
|-
|2001–2002
|''[[Wolf Lake]]''
|Ruby Cates
| 9 episodes
|-
|2004–2009
|''{{sortname|The|L Word}}''
|[[Jenny Schecter]]
|Main role (70 episodes)
|-
|2007
|''They Come Back''
|Faith Hardy
|TV film
|-
|2009
|''{{sortname|The|Cleaner|The Cleaner (TV series)}}''
|April May
|Episode: "Does Everybody Have a Drink?"
|-
|2009
|''[[CSI: NY]]''
|Deborah Carter
|Episode: "Dead Reckoning"
|-
|2010–2011
|''{{sortname|The|Vampire Diaries}}''
|[[List of The Vampire Diaries characters#Isobel Flemming|Isobel Flemming]]
| 6 episodes
|-
|2012
|''Kiss at Pine Lake''
|Zoe McDowell
|TV film
|-
|2013
|''{{sortname|The|Surrogacy Trap|nolink=1}}''
|Christy Bennett
|TV film
|-
|2013
|''[[Graceland (TV series)|Graceland]]''
|Ashika Pearl
|Episode: "Pizza Box"
|-
|2013–2014
|''[[Defiance (TV series)|Defiance]]''
|Kenya Rosewater
| 15 episodes
|-
|2013
|''[[Lost Girl]]''
|Clio
|3 episodes
|-
|2014
|''Reluctant Witness''
|Erin Villenueve
|TV film, pre-production
|-
|2015
|''[[Bloodline (TV series)|Bloodline]]''
|Sarah Rayburn<ref>{{cite news|work=AV Club|url=http://www.avclub.com/tvclub/bloodline-part-5-217077|title=''Bloodline'': Part 5 (Season 1, Episode 5)|author= Kumari Upadhyaya, Kayla |date=March 25, 2015 }}</ref>
|
|}

===Video Games===
{| Class="wikitable Sortable"
|-
! Year
! Title
! Role
! Class="unsortable" | Notes
|-
|2006
|''[[24: The Game]]''
|Mandy (Voice)
|
|}

==See also==
*[[The I Live Here Projects]]

== References ==

{{Reflist
| colwidth = 30em
| refs =

}}

== External links ==

{{commons category}}

* {{IMDb name|477|Mia Kirshner}}
* [https://twitter.com/msmiakirshner Mia Kirshner] on [[Twitter]]

{{Authority control}}

{{DEFAULTSORT:Kirshner, Mia}}
[[Category:1975 births]]
[[Category:20th-century Canadian actresses]]
[[Category:21st-century Canadian actresses]]
[[Category:21st-century Canadian writers]]
[[Category:21st-century Canadian women writers]]
[[Category:Actresses from Toronto]]
[[Category:Canadian child actresses]]
[[Category:Canadian film actresses]]
[[Category:Canadian people of Bulgarian-Jewish descent]]
[[Category:Canadian people of Polish-Jewish descent]]
[[Category:Canadian television actresses]]
[[Category:Canadian voice actresses]]
[[Category:Canadian women writers]]
[[Category:Jewish Canadian activists]]
[[Category:Jewish Canadian actresses]]
[[Category:Jewish Canadian writers]]
[[Category:Jewish women writers]]
[[Category:LGBT rights activists from Canada]]
[[Category:Living people]]
[[Category:McGill University alumni]]
[[Category:Writers from Toronto]]