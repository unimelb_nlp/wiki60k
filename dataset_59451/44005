<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=O-1230
 | image=
 | caption=
}}{{Infobox Aircraft Engine
 |type=[[Flat engine]]
 |manufacturer=[[Lycoming Engines]]
 |designer=
 |national origin=USA
 |first run=
 |major applications= [[Vultee A-19]]
 |produced=
 |number built=
 |program cost=
 |unit cost=
 |developed from=
 |variants with their own articles= [[Lycoming H-2470]]
}}
|}
The '''Lycoming O-1230''' was a [[flat-twelve engine]] for aircraft designed and developed by [[Lycoming Engines]] in the 1930s. Although the engine was flown in an aircraft, it was not fitted to any aircraft selected for production. It later served as the basis for the [[Lycoming H-2470]] engine.

==Design and development==
In 1932, the engineers at Lycoming Engines became aware that the [[United States Army Air Corps]] (USAAC) wanted a high performance engine that could produce at least one [[horsepower]] per cubic inch (46&nbsp;kW/L) of engine displacement and that a contract had been made with [[Continental Motors, Inc.]], Lycoming's main rival in the general aviation engine market.<ref name="White379">White p 379</ref> Lycoming's management wanted to be considered for development of the next generation engine, but no USAAC development contract was signed. Still determined to become known as a high performance engine manufacturer, Lycoming began an experimental, high-performance engine of its own. After spending US $500,000, and after many attempts to develop a successful engine, it finally came close to the USAAC specifications with the 1,200&nbsp;hp (895&nbsp;kW) O-1230 engine.

Lycoming's O-1230 engine design was a 12-cylinder liquid-cooled horizontally-opposed low-profile piston engine that could be mounted either horizontally, buried in the wing of a multi-engine aircraft; or vertically, in the fuselage of a single engine fighter. From 1935 the engine design proceeded at a faster pace after a number of former Continental engineers, who had become unhappy with the working conditions there, joined Lycoming.<ref name="White380">White p 380</ref>
[[File:Vultee XA-19A.jpg|thumb|right|Vultee XA-19A with Lycoming O-1230 installed]]
The same year the USAAC became interested in the O-1230, and began supporting the engine development program. In 1936, the single-cylinder development tests exceeded expectations, passing its 50 hour test requirement. The full-size engine was ready for testing in 1937, and was rated at 1,000&nbsp;hp.

The last [[Vultee A-19]] that had been ordered by the USAAC was delivered as the XA-19A, fitted with an O-1230-1 offering 1200&nbsp;hp. It first flew on May 22, 1940. This aircraft was subsequently re-engined with a Pratt & Whitney R-1830-51 and redesignated.

Continued development of the O-1230 reached its peak when the engine was rated at over 1200&nbsp;hp. It was as powerful as the [[Allison V-1710]], but with a narrower cowling than the Allison [[V12 engine]]. The O-1230 was not well received by aircraft manufacturers, because it was not very reliable at that power setting.<ref name="White380"/>

==Applications==
*[[Vultee A-19|Vultee XA-19A]]

<!-- Survivors -->
<!-- Engines on display== -->

==Specifications (O-1230-1)==
<!-- choose one to copy here -->

{{pistonspecs| <!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] --> <!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with ) and start a new, fully-formatted line with * -->
|ref= White p 379
|type= 12-cylinder, Geared, liquid-cooled, horizontally-opposed piston engine
|bore= 5.25&nbsp;in (133.4&nbsp;mm)
|stroke= 4.75&nbsp;in (120.7&nbsp;mm)
|displacement= 1233.9&nbsp;in³ (20.219&nbsp;L)
|length=
|diameter=
|width=
|height=
|weight=1342&nbsp;lb
|valvetrain=Overhead camshaft
|supercharger=
|turbocharger=
|fuelsystem=
|fueltype=
|oilsystem=
|coolingsystem=Liquid
|power=1000&nbsp;hp@3100&nbsp;rpm, 1200&nbsp;hp@3400&nbsp;rpm, 1,275&nbsp;hp (951&nbsp;kW)
|specpower=1.03&nbsp;hp/in³
|compression=
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight= .95&nbsp;hp/lb
|designer=
|reduction_gear= 0.40
|general_other=
|components_other=separate cylinders
* hemispheric cylinder head design
* one-piece cylinder head for each bank
|performance_other=
 }}

==See also==
{{aircontent
<!-- first, the related articles that do not fit the specific entries: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Reflist}}

* White, Graham, ''Allied Aircraft Piston Engines of World War II'', SAE International, 1995
* Balzer, Gerald C., ''American Secret Pusher Fighters of World War II'', Specialty Press. 2008

{{Lycoming aeroengines}}
{{US military piston aeroengines}}

[[Category:Lycoming aircraft engines|O-1230]]
[[Category:Aircraft piston engines 1930–1939]]
[[Category:Abandoned military aircraft engine projects of the United States]]