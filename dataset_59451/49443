{{Infobox person
| name        = Stefano Magno
| image       = Magno.png
| alt         = 
| caption     = Coat of arms of Magno family
| birth_name  = 
| birth_date  = around 1499
| birth_place = 
| death_date  = 14 October 1572
| death_place = 
| nationality = Venetian
| other_names = 
| occupation  = chronicler
| known_for   = 
}}

'''Stefano Magno''' (around 1499 – 14 October 1572) was a [[Venetian Republic|Venetian]] [[chronicler]].

== Biography ==

According to [[Kenneth Setton]], Stefano Magno was born around 1499 (his father's name was Andrea) and died on 14 October 1572.<ref name="Setton">{{Citation |title=The Papacy and the Levant, 1204–1571 |volume=four volumes |url=https://books.google.com/books?id=0Sz2VYI0l1IC&pg=PA329 |year= 1976–1984 |publisher=American Philosophical Society |isbn=978-0-87169-114-9 |page=329}}</ref> According to the Marios Philippides he was born in 1490 and died in 1557.{{sfn|Philippides|Hanak|2011|pp=105–106}} He was a member of the noble Venetian Magno family.

== Works ==

=== Cronaca Magno ===

The authorship of the [[manuscript]] often named as ''Cronaca Magno'' is attributed to Stefano Magno.{{sfn|Philippides|Hanak|2011|p=105}} This work is based on the work of [[Aeneas Sylvius]] (Pope Pius II).{{sfn|Philippides|Hanak|2011|pp=105–106}} Stefano Magno frequently quotes ''dispacci'' of [[Bartolomeo Minio]] in his chronicle.<ref>{{cite web|last=Gilliland Wright|first=Diana|title=The Greek Correspondence of Bartolomeo Minio: Dispacci from Nauplion, 1479–1483 |url=http://www.docstoc.com/docs/4711476/i-pm-INTRODUCTION-Bartolomeo-Minio-s-letters-are-preserved|accessdate=19 January 2012|location=Washington}}</ref>

=== Annali Veneti e del Mondo ===

His work ''Annali Veneti e del Mondo'' is a five-volume manuscript archived in the library of the [[Museo Correr]].<ref name=Setton /> This manuscript is described as "one of the more important literary sources for the last two decades of fifteenth century", providing "extraordinary coverage" of events almost all over Europe and [[Levant]].<ref name=Setton /> It also covers the process of Islamization of [[Albanians]] and presents information about the Muslim conquest of [[Skanderbeg|Skanderbeg's]] stronghold, [[Krujë]].<ref name=Setton /><ref>{{Citation |last= |first= |author= |authorlink= |editor= |editorn= |editorn-last= |editorn-first= |editor-link= |editorn-link= |others= |title=Islamic studies, Volume 36, Issues 1–4 |url= https://books.google.com/books?ei=9-YVT_-pLIPO4QTuzbyfAg&id=w1DrAAAAMAAJ&dq=%22stefano+magno%22+skanderbeg&q=%22Stefano+Magno%27s+chronicle+Annali+Veneti+e+del+Mondo+contains+the+most+objective+Christian+coverage+of+Islamization+of+Albanians+%28Shqeptaret%29+and+the+historical+details+about+the+Muslim+conquest+of+George+Kastrioti-Skanderbeg%27s%22#search_anchor |accessdate= 17 January 2012 |year= 1997 |publisher=Islamic Research Institute |location=Pakistan |page=201 |quote=Stefano Magno's chronicle Annali Veneti e del Mondo contains the most objective Christian coverage of Islamization of Albanians (Shqeptaret) and the historical details about the Muslim conquest of George Kastrioti-Skanderbeg's stronghold in Akce Hisar (Kruje, Kroia)}}</ref>
A [[Greeks|Greek]] historian and researcher [[Constantine Sathas]] published extracts of the Venetian chronicle of Stefano Magno connected with the history of Greece (''Mnemeia Hellenikes Historias [Monuments of Greek History]''), which Kenneth Setton considers carelessly transcribed.<ref name=Setton />

The first volume of his ''Annali Veneti e del Mondo'' describes the origins of the [[Republic of Venice|Venetian]] noble families and presents the alphabetically arranged list with dates of their admission to the [[Great Council of Venice]], with their [[coat of arms|coats of arms]] presented in color.<ref name=Setton /> The fourth volume describes the period from 1478 to 1481, and contains a description of the [[Siege of Krujë (1478)|Siege of Krujë]] in 1478.<ref name=Setton />

== Bibliography ==
* ''Cronaca Magno''
* ''Annali Veneti e del Mondo''

== References ==
;Notes
{{Reflist|colwidth=33em}}

;Bibliography
{{refbegin}}
* {{Citation |last1=Philippides |first1=Marios |last2=Hanak |first2=Walter K. |title=The siege and the fall of Constantinople in 1453: historiography, topography, and military studies |url=https://books.google.com/books?id=qvvdVXckfqQC&pg=PA105 |year=2011 |publisher=Ashgate Publishing |isbn=978-1-4094-1064-5 |oclc=758725978 |quote= ... based clearly on the text of Aenas Sylvius}}
{{refend}}

== Further reading ==
* {{Citation |last=Hösch  |first=Edgar |author2=Konrad Clewing |author3=[[Oliver Jens Schmitt]] |title= Südosteuropa : von vormoderner Vielfalt und nationalstaatlicher Vereinheitlichung : Festschrift für Edgar Hösch |url=http://www.worldcat.org/title/sudosteuropa-von-vormoderner-vielfalt-und-nationalstaatlicher-vereinheitlichung-festschrift-fur-edgar-hosch/oclc/62309552&referer=brief_results  |archiveurl= |archivedate= |format= |accessdate= |edition= |series= |volume= |origyear= |publisher= |location= |language= |isbn=978-3-486-57888-1  |oclc=62309552 |pages=133–183 |nopp= |at= |chapter=Die venezianischen Jahrbücher des Stefano Magno (ÖNB Codd 6215–6217) als Quelle zur albanischen und epirotischen Geschichte  im späten Mittelalter (1433–1477) |chapterurl= https://books.google.com/books?id=4eXlZq6aBrkC&pg=PA181 }}

{{DEFAULTSORT:Magno, Stefano}}
[[Category:1490 births]]
[[Category:1557 deaths]]
[[Category:16th-century historians]]