{{Good article}}
{{Infobox University Boat Race
| name= 113th Boat Race
| winner = Oxford
| margin = 3 and 1/4 lengths
| winning_time= 18 minutes 52 seconds
| date= {{Start date|1967|03|30|df=y}}
| umpire = G. D. Clapperton<br>(Oxford)
| prevseason= [[The Boat Race 1966|1966]]
| nextseason= [[The Boat Race 1968|1968]]
| overall =61&ndash;51
| reserve_winner =Goldie
| women_winner =Cambridge
}}
The 113th [[The Boat Race|Boat Race]] took place on 30 March 1967.  Held annually, the event is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  The race was won by Oxford by three-and-a-quarter-lengths.  [[Goldie (Cambridge University Boat Club)|Goldie]] won the reserve race while Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 24 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 24 August 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 20 August 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities, followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=24 August 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 24 August 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1966|previous year's race]] by three-and-three-quarter lengths.  Cambridge, however, held the overall lead with 61 victories to Oxford's 50 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatrace.org/men/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 12 October 2014}}</ref><ref name=results/>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

The race was umpired by George Douglas "Jock" Clapperton who had coxed Oxford in the [[The Boat Race 1923|1923]] and [[The Boat Race 1924|1924 races]] as well as umpiring in the [[The Boat Race 1959|1959 boat race]].<ref>Burnell, pp. 49, 71&ndash;72</ref><ref>{{Cite web | url = http://www.magd.ox.ac.uk/libraries-and-archives/archives/online-catalogues/george-stevens-papers/photo-album/ | publisher = [[Magdalen College, Oxford]] | title = MC:P37/P1 Photograph Album | accessdate = 28 September 2014}}</ref><ref name=crushed>{{Cite news | title = Cambridge are crushed by power display | first = Donald | last = Legget | date = 26 March 1967 | work = [[The Observer]] | page = 16}}</ref>

Cambridge's coaching team included Norman Addison (rowed for Cambridge in the [[The Boat Race 1939|1939 race]]), [[James Crowden]] ([[The Boat Race 1951|1951]] and [[The Boat Race 1952|1952 races]]), [[David Jennens]] ([[The Boat Race 1949|1949]], [[The Boat Race 1950|1950]] and 1951 races), Mike Muir-Smith ([[The Boat Race 1964|1964 race]]), Mike Nicholson (non-rowing boat club president for the [[The Boat Race 1947|1947 race]]), J. R. Owen ([[The Boat Race 1959|1959]] and [[The Boat Race 1960|1960 races]]) and M. Wolfson while Oxford's comprised [[Hugh Edwards (rower)|Hugh "Jumbo" Edwards]] (rowed for Oxford in the [[The Boat Race 1926|1926]] and [[The Boat Race 1930|1930 races]]) and Ronnie Howard ([[The Boat Race 1957|1957]] and [[The Boat Race 1959|1959 races]]).<ref>Burnell, pp. 96&ndash;111</ref>

==Crews==
The Cambridge crew weighed an average of 13&nbsp;[[Stone (unit)|st]] 11&nbsp;[[Pound (mass)|lb]] (87.3&nbsp;kg), {{convert|1.75|lb|kg|1}} per rower more than their opponents.<ref name=burn81>Burnell, p. 81</ref>  Oxford's crew containing three former [[Blue (university sport)|Blues]] in Martin Kennard, Chris Freeman and Jock Mullard, while Cambridge saw bow-man Lindsay Henderson and [[Patrick Delafield]] return.<ref name=burn81/>  Oxford's American number four, Josh Jensen, was the heaviest oarsman in the history of the race at 15&nbsp;st 4&nbsp;lb (96.8&nbsp;kg).<ref name=both/>  The former Cambridge Blue Donald Legget, writing in ''[[The Observer]]'' suggested that the Light Blue crew was "possibly their fastest ever", but nevertheless predicted a two-length victory for Oxford.<ref name=both>{{Cite news | title = Both the crews are tough and heavy | work = [[The Observer]] | first = Donald | last = Legget | date = 19 March 1967 | page = 18}}</ref>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] ||J. R. Bockstoce (P) || [[St Edmund Hall, Oxford|St Edmund Hall]] || 14 st 0 lb || L. M. Henderson || [[Selwyn College, Cambridge|Selwyn]] || 13 st 5 lb
|-
| 2 || M. S. Kennard || [[St Edmund Hall, Oxford|St Edmund Hall]] || 13 st 0 lb || C. D. C. Challis || [[Selwyn College, Cambridge|Selwyn]] || 13 st 6 lb
|-
| 3 || C. H. Freeman || [[Keble College, Oxford|Keble]] || 14 st 0 lb || R. D. Yarrow || [[Lady Margaret Boat Club]] || 13 st 9 lb
|-
| 4 || J. E. Jensen || [[New College, Oxford|New College]] || 15 st 4 lb || G. C. M. Leggett || [[St Catharine's College, Cambridge|St Catharine's]] || 13 st 3 lb
|-
| 5 || J. K. Mullard || [[Keble College, Oxford|Keble]] || 14 st 0 lb || [[Patrick Delafield|P. G. R. Delafield]] (P) || [[Jesus College, Cambridge|Jesus]] || 14 st 9 lb
|-
| 6 || C. I. Blackwall || [[Keble College, Oxford|Keble]] || 13 st 6 lb || N. J. Hornsby || [[Trinity Hall, Cambridge|Trinity Hall]]  || 14 st 9 lb
|-
| 7 || [[Daniel Topolski|D. Topolski]] || [[New College, Oxford|New College]] || 11 st 13 lb || D. F. Earl || [[Lady Margaret Boat Club]]  || 13 st 11 lb
|-
| [[Stroke (rowing)|Stroke]] ||  P. G. Saltmarsh  || [[Keble College, Oxford|Keble]] || 14 st 0 lb || R. N. Winckless || [[Fitzwilliam College, Cambridge|Fitzwilliam]] || 13 st 9 lb
|-
| [[Coxswain (rowing)|Cox]] || P. D. Miller || [[St Catherine's College, Oxford|St Catherine's]] || 9 st 6 lb || W. R. Lawes|| [[Pembroke College, Cambridge|Pembroke]]  || 8 st 13 lb
|-
!colspan="7"|Source:<ref name=dodd342>Dodd, p. 342</ref><br>(P) &ndash; Boat club president<ref>Burnell, pp. 51&ndash;52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] for the third successive year and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=burn81/>  The race commenced at 1.17pm.<ref name=crushed/>  Despite the conditions favouring the Light Blues, Oxford were ahead from the start and led by two seconds the Mile Post in a record-equalling time of 3 minutes 47 seconds.  According to Legget, Cambridge "were untidy and rather rushed".<ref name=crushed/>  Near [[Harrods Furniture Depository]], the crews nearly clashed oars, but Oxford held firm and reached [[Hammersmith Bridge]] with a three-second lead.  Rounding the corner, Cambridge chose to stay on the tide, while Oxford headed for shelter towards the Surrey shore.  The Light Blues reduced the lead marginally but by Chiswick Steps, Oxford were six seconds ahead and moved back to the Middlesex shore, with Cambridge resolute in midstream.  Oxford briefly left the shelter of the shoreline to shoot [[Barnes Railway Bridge|Barnes Bridge]] through the centre arch, before heading back, with a lead of eight seconds.  Despite pushing their rating to 36 strokes per minute, Cambridge could not reduce the deficit, and as Oxford accelerated to a rating of 38, they passed the finishing post three-and-a-half lengths ahead, in a time of 18 minutes 52 seconds.<ref name=burn81/><ref name=doubts/>  It was the first time in 54 years that Oxford had won three consecutive Boat Races.<ref name=doubts>{{Cite news | title = Doubts dispelled at Chiswick Steps| first =Richard | last = Burnell | authorlink=Dickie Burnell | date = 27 March 1967 | issue = 56899 | page = 14 | work = [[The Times]]}}</ref>  Upon the conclusion of the race, the Oxford boat club president Mullard hailed his coaches from the boat: "Thanks Ronnie, thanks Jumbo".<ref>{{Cite news | title = Oxford's mastery soon established | first = Stanley | last = Baker | date = 27 March 1967 | page = 10 | work = [[The Guardian]]}}</ref>

In the reserve race, Cambridge's Goldie beat Oxford's Isis by two lengths and five seconds, their inaugural victory on the third running of the contest, in a time of 19 minutes 11 seconds.<ref name=results>{{Cite web | url = http://theboatrace.org/men/results | title =Men &ndash; Results | publisher = The Boat Race Company Limited | accessdate = 27 September 2014}}</ref><ref name=doubts/> In the 22nd running of the [[Henley Boat Races#Women's Boat Race|Women's Boat Race]], Cambridge triumphed, their fifth consecutive victory.<ref name=results/>

==References==
'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink=Dickie Burnell| year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}

'''Notes'''
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1967}}
[[Category:1967 in English sport]]
[[Category:1967 in rowing]]
[[Category:The Boat Race]]