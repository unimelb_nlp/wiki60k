{|{{Infobox Aircraft Begin
 |name = La-15 <!--please avoid stating manufacturer in this field; it's stated two lines below -->
 |logo =
 |image =La-15.1.jpg
 |caption =La-15 at Central Air Force Museum, Monino
}}{{Infobox Aircraft Type
 |type = Fighter
 |manufacturer = [[Lavochkin]] / [[Sokol plant|Plant 21 "Sokol"]] <ref name="LLJ">{{cite book|last=Gordon|first=Yefim|title=Lavochkin's Last Jets|publisher=Midland publishing|location=Hinkley|series=Red Star|volume=32|isbn=1-85780-253-5}}</ref>
 |designer =
 |first flight = January 8, 1948 (Aircraft 174)<ref name="LLJ"/>
 |introduction = 1949<ref name="LLJ"/>
 |retired = 1953<ref name="LLJ"/>
 |status =
 |primary user = [[Soviet Air Force]]<ref name="LLJ"/>
 |more users =
 |produced =
 |number built = 235<ref name="LLJ"/>
 |unit cost =
 |developed from = [[Lavochkin Aircraft 174]]<ref name="LLJ"/>
 |variants with their own articles =
}}
|}

The '''Lavochkin La-15''' ([[Sokol plant|Plant 21]] product code '''Izdeliye 52''', [[USAF]] reporting name '''Type 21''',<ref>http://www.designation-systems.net/non-us/soviet.html#_DOD_Type</ref> [[NATO]] reporting name '''Fantail'''<ref>http://www.designation-systems.net/non-us/soviet.html#_Listings_Fighter</ref>), was an early [[Soviet Union|Soviet]] jet fighter and a contemporary of the [[Mikoyan-Gurevich MiG-15]].<ref name="LLJ"/>

==Design and development==

Lavochkin had produced a line of prop powered fighters in World War II. The [[Lavochkin La-150]] was its first response to a 1945 order to build a single-seat jet fighter using a single German [[Junkers Jumo 004]] turbojet from the [[Me-262]] which first flew in September 1946. The [[Lavochkin La-152]] which flew in December 1946 moved the engine to the front of the nose, which reduced thrust loss. The [[Lavochkin La-160]] was the first Soviet fighter to apply swept wings, and flew in June 1947.

The [[Lavochkin La-168]] first flew on April 22, 1948. (The American [[F-86 Sabre]] had first flown in October 1947.) It was designed to use the new turbojet based on the Rolls-Royce Nene in response to a 1946 request for an advanced swept-wing jet fighter capable of transonic performance. The engine was placed behind the pilot, but with a high-mounted wing and T-tail compared to the similar [[MiG-15]].

The La-15 which reached mass production was the outcome of a series of development aircraft that began with the [[OKB-1 150|Aircraft 150]] bomber in 1945 and culminated in Aircraft 176, later in 1948. These aircraft were designed for British engines, [[Rolls-Royce Derwent]] V and [[Rolls-Royce Nene]], acquired by the Soviets in 1947 and then copied as the [[Klimov RD-500]] and [[Klimov RD-45]] respectively. The Derwent-powered '''Aircraft 174''' was designed as a backup for the main program, the Nene-powered Aircraft 168, in case the British failed to deliver more powerful Nene engines with afterburners (which they did fail to deliver). The first prototype of Aircraft 174 was flown just 9 days after its  counterpart the [[Mikoyan-Gurevich I-310]], on January 8, 1948. The first prototype was however lost on May 11, 1948 due to vibrations. Trials were continued with an improved second prototype, designated '''Aircraft 174D''', which underwent State Acceptance Tests from August to September 25, 1948. In comparison with the Nene-powered MiG-15 it had almost the same maximum speed and better maneuverability, with somewhat reduced rate of climb. The type was ordered into production in September 1948, even while Aircraft 174D was undergoing flight trials, and given the official designation La-15 in April 1949.<ref name="LLJ"/>

The La-15 had a barrel-like fuselage, shoulder-mounted swept wings with 6 degrees anhedral, and stabilizers mounted high on the fin, almost a T-tail. It was popular with pilots because of its easy handling and reliability, and its pressurized cockpit was an advantage at high altitude. Nevertheless, official enthusiasm for the La-15 was mild, largely because it was a complex design that required complicated and expensive production tooling. Only 235 La-15s were built, serving with the Soviet Air Force until 1953.<ref name="LLJ"/>

[[File:La-15.jpg|thumb]]

==Operational history==
The La-15 was tested operationally by the 192nd Fighter Wing, based at [[Kubinka]] from 19 March 1949, and began appearing in front-line combat units later the same year. Introduction was accompanied by numerous accidents, but the competing MiG-15 design fared little better. However, although the La-15 had a number of technical advantages over the MiG-15, a combination of easier manufacture and lower costs led to the MiG-15 being favoured.  The Soviet authorities decided to produce only one fighter, and they chose the MiG-15bis. The remaining La-15s in service were disarmed by 1953, and their engines reused on the [[KS-1 Komet]] air-to-surface missile. The aircraft were expended as targets at various nuclear bomb tests.<ref name="LLJ"/>

==Variants==
;Aircraft 174
:Rolls-Royce Derwent powered first prototype of La-15. Crashed due to structural vibrations caused by sympathetic resonant frequencies of tailplane and rear fuselage.<ref name="LLJ"/>
;Aircraft 174D
:(Dooblyor-second)- Second prototype with modifications shown to be required from Aircraft 174's flight tests.<ref name="LLJ"/>
;Aircraft 180
:A two-seat trainer version was also developed as '''Aircraft 180''' and was to be put into production as the '''UTI La-15''' or '''La-15UTI''', but as official interest in the La-15 waned, the trainer was cancelled before mass production began and only two were made.<ref name="LLJ"/>

==Operators==
; {{USSR}}
* [[Soviet Air Force]]<ref name="LLJ"/>

==Survivors==
A La-15 is on display at the [[Central Air Force Museum]] at [[Monino]], outside of Moscow, Russia.<ref>[http://www.moninoaviation.com/14a.html] Monino home page</ref>

==Specifications (La-15)==
[[File:La-15.svg|thumb|right|300px]]
{{Aircraft specs
|ref=Lavochkins Last Jets<ref name="LLJ"/>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->

<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=2,575
|empty weight lb=
|empty weight note=
|gross weight kg=3,850
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=1,060l (233.2Imp Gal)
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name= [[Klimov RD-500]]
|eng1 type=centrifugal compressor turbojet
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->15.59
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=<!-- for different engine types -->
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=<!-- for different engine types -->
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=1,007
|max speed mph=
|max speed kts=
|max speed note=at 8,000m (26,350ft)
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1,145
|range miles=
|range nmi=
|range note=at 10,000m (32,810ft)
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=13,500
|ceiling ft=
|ceiling note=at nominal power - prone to surge at military power above 8,000m (26,250ft)
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=31.7
|climb rate ftmin=
|climb rate note=
|time to altitude=<br/>
*5,000m (16,400ft) in 3.1min
*10,000m (32,180ft) in 9min
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=238
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=1:2.4
|more performance=
<!--
        Armament
-->
|guns= 3 * 23mm [[Nudelman-Suranov NS-23]] cannon with 100 rounds per gun
|bombs= 
|avionics=
}}

==See also==
{{aircontent|
|related=
* [[Lavochkin Aircraft 168|Aircraft 168]]
* [[Lavochkin Aircraft 176|Aircraft 176]]
|similar aircraft=
* [[Mikoyan-Gurevich MiG-15|MiG-15]]
* [[FMA IAe 33 Pulqui II]]
* [[Yakovlev Yak-25 (1947)]]
* [[Republic F-84F Thunderstreak]]
* [[North American F-86 Sabre]]
|lists=
* [[List of fighter aircraft]]
* [[List of military aircraft of the Soviet Union and the CIS]]
|see also=
}}

==References==

===Citations===
{{reflist}}

===Bibliography===
*Gordon,Yefim. ''Lavochkin's Last Jets''. Midland Publishing. Hinkley. 2007. ISBN 1-85780-253-5

===Further reading===
{{commons category}}
*Gunston, Bill. ''The Osprey Encyclopedia of Russian Aircraft 1875-1995''. London:Osprey, 1995. ISBN 1-85532-405-9.

{{Lavochkin aircraft}}

[[Category:Lavochkin aircraft|La-15]]
[[Category:Soviet fighter aircraft 1940–1949]]