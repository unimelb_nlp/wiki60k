<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=901 Mouette
 | image=Displays at the Musee de l'Air et de l'Espace, Le Bourget, Paris, France, September 2008 (2).JPG
 | caption=Breguet Br 901 Mouette (F-CAJA)
}}{{Infobox Aircraft Type
 | type=Single seat competition [[sailplane]]
 | national origin=[[France]]
 | manufacturer=Société des Ateliers d'Aviation Louis Breguet ([[Breguet Aviation]])
 | designer=Jean Cayla
 | first flight=11 March 1954
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced=1955-9
 | number built=c.36
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Breguet 901 Mouette''' ({{lang-en|Seagull}}) is a very successful single seat [[France|French]] competition [[sailplane]] from the 1950s. It was the winner at both the 1954 and 1956 [[World Gliding Championships]].

==Design and development==
Breguet's first sailplane, the [[Breguet Br 900 Louisette|Type 900]] had some success in national competitions but failed to impress at the two World Championships of 1950 and 1952, partly because of its short [[wingspan]].<ref name=SimonsII>{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=59–61}}</ref>  The 901 is a development of this aircraft, retaining its [[monoplane#Types|mid wing]] layout and largely wooden construction. The wing of the 901 is straight tapered and built around a single [[spar (aviation)|spar]], with a leading edge torsion box and [[aircraft fabric covering|fabric covered]] aft of the spar. On the 901 the torsion box was skinned with a plywood-[[klegecell]] (a plastic foam) sandwich rather than the ply of the 900. At 17.32&nbsp;m (57&nbsp;ft&nbsp;8&nbsp;in), its span is 2.97&nbsp;m (9&nbsp;ft&nbsp;9&nbsp;in) greater than the 900, raising the aspect ratio from 12.9 to 20.  There are long span, short [[chord (aircraft)|chord]] [[flap (aircraft)#Types of flaps|slotted flaps]] inboard, mid-chord [[air brake (aircraft)|airbrakes]] and tips finished with small "salmon" fairings. Both designs have plywood skinned [[fuselage]]s, though that of the 901 is longer in the nose where the cockpit has an extended, single piece [[canopy (aircraft)|canopy]].<ref name=SimonsII/><ref name=JAWA56>{{cite book |title= Jane's All the World's Aircraft 1956-57|last= Bridgman |first= Leonard |coauthors= |edition= |year=1956|publisher= Jane's All the World's Aircraft Publishing Co. Ltd|location= London|isbn=|page=131 }}</ref><ref name="Brütting">{{cite book|title=Die berümtesten Segelflugzeuge|last=Brütting |first=Georg|year=1973|publisher=Motorbuch Verlag|location=Stuttgart |isbn=3 87943171 X|pages=141–2}}</ref>

The first two 901s built retained the curved vertical tail of the 900<ref name=prot1>{{cite web |url=http://www.airliners.net/photo/Breguet-901/2017328/L/
|title=First prototpye 901 production list |author= |date= |work= |publisher= |accessdate=18 April 2012}}</ref><ref name=prod>{{cite web |url=http://www.airport-data.com/manuf/Breguet.html
|title=Breguet production list |author= |date= |work= |publisher= |accessdate=18 April 2012}}</ref> but the third<ref name=JAWA56/><ref name=prod/> had a straight topped shape with a rudder that was straight edged except at the heel. The 901's undercarriage is a retractable [[Landing gear#Gliders|monowheel]], fitted with a brake, plus a tail bumper.<ref name=JAWA56/>

The 901 flew for the first time in March 1954.  In 1956 it was developed into the 901S, which had a fuselage 510&nbsp;mm (20&nbsp;in) longer with a similar large area rudder like that of the 901 third prototype.  A further development, the 901S1, had a more angular rudder and a [[fin]] without a fuselage [[fillet (mechanics)|fillet]].<ref name="Brütting"/>

==Operational history==
Gerard Pierre won the 1954 World Gliding Championships in the first prototype 901 only four months after its first flight.  The second prototype, flown by G. Rousselet, finished in seventh place.<ref name=WGC1954>{{cite journal |last= |first= |authorlink= |coauthors= |date=Autumn 1954 |title= Single-Seater Championship Results|journal=Gliding|volume=5 |issue=3 |page=79 |id= |url= http://www.lakesgc.co.uk/mainwebpages/Gliding%201950-1955/Volume%205%20No%203%20autumn%201954.pdf}}</ref>  By the time of the 1956 Championships the 901 had been developed into the 901S; [[Paul MacCready]] piloted it to a second Breguet championship victory.<ref name=WGC1954/>

As well as its international achievements the 901 set, and sometimes reset, numerous French national records.<ref name="Brütting"/>

Nine 901S remained on the French register in 2010.<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0}}</ref>

==Variants==
Production numbers from<ref name=prod/>
;901: Original version.  Third example had a straight topped vertical tail with a straight edged rudder apart from a rounded heel. Retained fin-fuselage fillet. 3 built.
;901S: 510&nbsp;mm longer, larger, tail similar to third 901, modified flaps, heavier. 21 built.
;901S1: As 901 but with rudder straight edged without rounded heel, no fillet. 9 built.
;901S2: 3 built.

== Aircraft on display ==
Of the numerous 901s with French museums, two are on public display:
*[[Musée de l'Air et de l'Espace]], [[Le Bourget]], 901; ''F-CAJA'' the first prototype and 1954 World Championship winner.
*[[Musée Régional de l'Air]], [[Marcé]], 901S; ''F-CCCP''

== Specifications (901S) ==
{{Aircraft specs
|ref=<ref name="Brütting"/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=
|crew=
|capacity=1
|length m=7.57
|length note=
|span m=17.32
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=15.0
|wing area note=
|aspect ratio=20
|airfoil=[[NACA 63]] series
|empty weight kg=265
|empty weight note=
|gross weight kg=430
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=
|max speed kmh=220
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=62
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=220
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|180|km/h|mph kn|abbr=on|1}}
*'''Aerotow speed:''' {{convert|150|km/h|mph kn|abbr=on|1}}
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=35 at {{convert|85|km/h|mph kn|abbr=on|1}}
|sink rate ms=0.60
|sink rate note= at {{convert|72|km/h|mph kn|abbr=on|1}}
|lift to drag=
|wing loading kg/m2=28.0
|wing loading note=
|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*{{cite book |title= Jane's All the World's Aircraft 1956-57|last= Bridgman |first= Leonard |coauthors= |edition= |year=1956|publisher= Jane's All the World's Aircraft Publishing Co. Ltd|location= London|isbn=|page=131 }}
*{{cite book|title=Die berümtesten Segelflugzeuge|last=Brütting |first=Georg|year=1973|publisher=Motorbuch Verlag|location=Stuttgart |isbn=3 87943171 X|pages=141–2}}
*{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0}}
*{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=59–61}}
*{{cite journal |last= |first= |authorlink= |coauthors= |date=Autumn 1954 |title= Single-Seater Championship Results|journal=Gliding|volume=5 |issue=3 |page=79 |id= |url= http://www.lakesgc.co.uk/mainwebpages/Gliding%201950-1955/Volume%205%20No%203%20autumn%201954.pdf}}
*{{cite journal |last= |first= |authorlink= |coauthors= |date=August 1956  |title=1956 World Gliding Championship Results|journal= Sailplane & Gliding|volume=VII|issue=4 |page=170 |id= |url= http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Gliding%201955-1960/Volume%207%20No%204%20Aug%201956.pdf |accessdate= |quote= }}
*{{cite web |url=http://www.airport-data.com/manuf/Breguet.html
|title=Breguet production list |author= |date= |work= |publisher= |accessdate=18 April 2012}}

*{{cite web |url=http://www.airliners.net/photo/Breguet-901/2017328/L/
|title=First prototpye 901 production list |author= |date= |work= |publisher= |accessdate=18 April 2012}}

{{refend}}
<!-- ==Further reading== -->

==External links==
{{commons category|Breguet aircraft}}
*[http://www.airliners.net/photo/Breguet-901/2017328/L/ Breguet-901]

<!-- Navboxes go here -->
{{Breguet aircraft}}

{{DEFAULTSORT:Breguet Br 901 Mouette}}
[[Category:French sailplanes 1950–1959]]
[[Category:Breguet aircraft|901 Mouettte]]