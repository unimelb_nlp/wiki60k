:''For the 1950s helicopter , see [[Westland Widgeon (helicopter)]], for the 1940s Grumman amphibian, see [[Grumman G-44 Widgeon]]''
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Widgeon
 | image=StateLibQld 1 192503 G-AUHU, Westland Widgeon III aeroplane, ca. 1928.jpg
 | caption=Westland Widgeon III
}}{{Infobox Aircraft Type
 |type = Light Aircraft 
 |national origin=United Kingdom
 |manufacturer = [[Westland Aircraft]]
 |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
 |first flight = 22 September 1924<!--if it hasn't happened, leave it out!-->
 |introduced = 1927<!--date the aircraft entered or will enter military or revenue service-->
 |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
 |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
 |status = <!--in most cases, this field is redundant; use it sparingly-->
 |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
 |more users = <!--limited to three "more users" total. please separate with <br/>-->
 |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
 |number built = 26
 |unit cost = 
 |developed from = <!-- the aircraft which formed the basis for the topic type -->
 |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Westland Widgeon''' was a [[United Kingdom|British]] light aircraft of the 1920s.  A single engined [[Parasol wing|parasol]] [[monoplane]], the Widgeon was built in small numbers before Westland abandoned production in 1929.

==Development and design==
In 1924, the British [[Air Ministry]], eager to encourage the development of cheap civil aircraft suitable for use by private owners and flying clubs, sponsored a competition for a two seat ultralight aircraft, which had to be powered by an engine of 1100 cc [[Engine displacement|displacement]] or less and capable of carrying a load of at least 340&nbsp;lb (155&nbsp;kg). To meet this requirement, [[Westland Aircraft]] produced two designs, the [[Westland Woodpigeon|Woodpigeon]] [[biplane]], and the Widgeon parasol monoplane.  Unable to decide which design would be superior, Westland decided to build both types.<ref name="James 104">James 1991, p.104.</ref>

The Widgeon first flew at Westland's [[Yeovil]] factory on 22 September 1924, eight days after the first of two Woodpigeons.<ref name="James p111">James 1991, p.111.</ref>  Its fuselage, which was very similar to that of the Woodpigeon, was of mixed steel tube and wooden construction, while the wooden parasol wing, which was tapered in both chord and thickness,<ref name="Flight p624">Flight 25 September 1924, p.624.</ref> folded for easy storage.  It was powered by a single 1,090&nbsp;cc [[Blackburne Thrush]] three cylinder radial engine, which produced 35&nbsp;hp (26&nbsp;kW).<ref name="James p110">James 1991, p.110.</ref>

The Air Ministry Light Aircraft competition began at [[Lympne]] Aerodrome, [[Kent]] on 27 September. The Widgeon, which due to the use of the Thrush engine was badly underpowered (as was the Woodpigeon), crashed during the first day of trials.<ref name="James p111"/> Despite this setback, it was clear that the Widgeon had promise and was superior to the Woodpigeon, and the damaged prototype was rebuilt with a more powerful 60&nbsp;hp (45&nbsp;kW) [[Armstrong Siddeley Genet]] engine as the '''Widgeon II'''.  Despite its much greater weight, the new engine transformed the Widgeon, the rebuilt aircraft being almost 40&nbsp;mph (64&nbsp;km/h) faster.<ref name="James p112">James 1991, p.112.</ref>

Based on this experience, Westland decided to enter the Widgeon into production for the private owner.  It was therefore redesigned with a simpler, constant chord, wing replacing the tapered wing of the Widgeon I and II to ease production.  The resulting '''Widgeon III''' could be powered either a radial engine like the Genet or an inline engine such as the Cirrus. The first Widgeon III flew in March 1927, with production starting later that year.<ref name="James p114">James 1991, p.114.</ref>  The design was further refined with a [[duralumin]] tube fuselage and a new undercarriage to produce the '''Widgeon IIIA'''.

The Widgeon proved expensive compared to its competitors and a total of only 26 of all types, including the prototype, were built and sold before production was stopped in 1930 in order to allow Westland to concentrate of the [[Westland Wapiti|Wapiti]] general purpose military aircraft and the [[Westland IV|Wessex]] airliner.<ref name="James 118">James 1991, p.118.</ref>

==Variants==
;Widgeon I
:Powered by one 35&nbsp;hp [[Blackburne Thrush]] radial engine.  One built.
;Widgeon II
:Rebuild of Widgeon I with 60&nbsp;hp [[Armstrong Siddeley Genet]] radial.
;Widgeon III
:Redesign for production. Powered by [[Cirrus Engine|ADC Cirrus]] II or III inline engine, Genet II radial, [[ABC Hornet]] or [[de Havilland Gipsy]].<ref name="Jackson v3 p243"/> 18 built.<ref name="James p120"/> 
;Widgeon IIIA
:Variant of Widgeon III with metal fuselage and new undercarriage.  Powered  by Cirrus or Gipsy engine.  Seven built.<ref name="James p120">James 1991, p.120.</ref>

==Specifications (IIIA)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=British Civil Aircraft since 1919 Volume III <ref name="Jackson v3 p243">Jackson 1988, p.243.</ref>
|crew=Two
|capacity=
|length main= 23 ft 5¼ in
|length alt= 7.15 m
|span main= 36 ft 4½ in
|span alt= 11.09 m
|height main= 8 ft 5 in
|height alt= 2.57 m
|area main= 200 ft²
|area alt= 18.6 m²
|airfoil=RAF 34 <ref>Flight 28 July 1928, p.518.</ref>
|empty weight main= 935 lb
|empty weight alt= 425 kg
|loaded weight main=  
|loaded weight alt=  
|useful load main=  
|useful load alt=  
|max takeoff weight main= 1,650 lb
|max takeoff weight alt= 750 kg
|more general=
|engine (prop)=[[ADC Cirrus|Cirrus Hermes]] II
|type of prop=4-cylinder air-cooled inline engine
|number of props=1
|power main= 120 hp
|power alt= 90 kW
|power original=
   
|max speed main= 104 mph 
|max speed alt= 90 knots, 167 km/h
|cruise speed main=86 mph
|cruise speed alt= 75 knots, 138 km/h
|never exceed speed main= <!-- knots -->
|never exceed speed alt= <!-- mph,  km/h -->
|stall speed main=<!--  knots -->
|stall speed alt= <!-- mph,  km/h -->
|range main=315 mi 
|range alt= 274 [[nautical mile|NM]], 507 km
|ceiling main=15,000 ft
|ceiling alt= 4,570 m
|climb rate main= 640 ft/min
|climb rate alt= 3.25 m/s
|loading main= <!-- lb/ft² -->
|loading alt= <!-- kg/m² -->
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= <!-- hp/lb -->
|power/mass alt= <!-- W/kg -->
|more performance=
|armament=
|avionics=

}}

==See also==
{{aircontent
|related=
*[[Westland Woodpigeon]]
|similar aircraft=*[[Hawker Cygnet]]
*[[de Havilland Moth]]<!-- similar or comparable aircraft -->
|lists=
|see also=* ''[[Kookaburra (aircraft)|Kookaburra]]'', a Widgeon lost during a 1929 search for another airplane in Australia and rediscovered in 1978
}}

==References==
{{Reflist}}

*"[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200624.html THE WESTLAND "WIDGEON" LIGHT MONOPLANE (No. 6)]". ''[[Flight International|Flight]]''. 25 September 1924, p.&nbsp;6243
*"[http://www.flightglobal.com/pdfarchive/view/1927/1927%20-%200555.html The Westland "Widgeon III"]". ''Flight''. 28 July 1928. pp.&nbsp;513–518.
*"[http://www.flightglobal.com/pdfarchive/view/1929/1929%20-%200512.html The Westland "Widgeon IIIA"]".  ''Flight''. 14 March 1929. pp.&nbsp;206–207.
*Jackson, A.J. ''British Civil Aircraft 1919-1972: Volume III''. London:Putnam, 1988. ISBN 0-85177-818-6.
* James, Derek M. ''Westland Aircraft since 1915''. London:Putnam, 1991. ISBN 0-85177-847-X.

==External links==
{{commons category|Westland Widgeon (fixed wing)}}
*[http://www.jaapteeuwen.com/ww2aircraft/html%20pages/WESTLAND%20WIDGEON.htm Westland Widgeon]. ''British Aircraft of World War II''.
*[http://www.britishaircraft.co.uk/aircraftpage.php?ID=510 Westland Widgeon] ''British Aircraft Directory''

{{Westland aircraft}}

[[Category:British sport aircraft 1920–1929]]
[[Category:Westland aircraft|Widgeon]]