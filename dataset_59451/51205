{{Infobox Bibliographic Database
| title =Energy Technology Data Exchange (ETDE)
| image = [[File:ETDEWEB subjects coverage.jpg|320px]]
| caption ='''Pie chart represents the subject contents of the database for 2004-2008.'''
| producer = Member Countries
| country =(Brazil, Canada, Denmark, Finland, Germany, Korea, Netherlands, Norway, Mexico, Spain, South Africa, Switzerland, Sweden, and United States) and partners.
| history =1987 to present
| languages =Multilingual
| officers = Chair: Brian Hitson (United States); Vice Chairs: Silke Rehme (Germany); Hillebrand Verkroost (Netherlands)
| operating Agent = U.S. Department of Energy Office of Scientific and Technical Information
| providers =ETDE World Energy Base (ETDEWEB)
| cost =
| disciplines =Energy and Environmental sciences
| depth =Bibliographic indexing & abstracts. Links to full text.
| formats =Journal articles, reports, conference papers, books, websites, and other miscellaneous formats
| temporal =1974 to present day
| geospatial =Member countries, Partner countries, Approved developing nations
| number =4.5 million +
| updates = 2 / month
| p_title =
| p_dates =
| ISSN =
| web =http://www.etde.org/
| titles =http://www.etde.org/edb/access.html
}}
The '''Energy Technology Data Exchange''' (ETDE) was formed in 1987 and officially ended 30 June 2014.<ref name=home2016>
{{cite web
  | last =Energy Technology Data Exchange
  | title =Home page
  | publisher =International Energy Agency
  | date =October 2016
  | url =https://www.etde.org/
  | accessdate =2016-08-24}}</ref> It was initiated as a [[multilateral agreement]] under the [[International Energy Agency]] (IEA) agreement network, replacing numerous other [[bilateral agreement]]s. The multilateral agreement was for the international exchange of [[energy research]] and development and information. The exchange resulted in a [[database]] which was the world's largest collection of energy research, technology, and development (RTD) information ([[ETDEWEB]] - described below).  The collection of information was generated from energy RTD literature published in member countries and through other partnering arrangements with organizations such as the [[International Nuclear Information System]] (a unit of the [[International Atomic Energy Agency]]).  This had the effect of creating a broad spectrum of information that was included in the ETDE database. The range of content included [[fossil fuel]]s, [[renewable energies]] (including [[Hydrogen]]), End-Use ([[Building]]s, [[Industry]] and [[Transport]]), [[fusion power|fusion]], energy policy, conservation, and efficiency, and cross-sectional activities. This fulfilled the need for timely exchange of global information towards the goal of a sustainable energy future.  ETDE operated under an IEA Implementing Agreement and was governed by an Executive Committee of delegates from ETDE member countries.  Officers of the Executive Committee included a Chair and two Vice-Chairs, elected to three-year terms.  Day-to-day operations were managed through an operating agent organization, which reported to the Executive Committee.

==Energy Database==
ETDE's '''Energy Database''' was a substantial collection that focused on [[energy research]] literature and [[technology]] literature. This database contained more than 4.5 million abstracted and indexed records, and was updated twice per month. Temporal coverage was from 1974 to 2014. The principle access point for this database was [[ETDEWEB]] (see next section). However, access was also available through commercial online hosts, and some countries offered their own products for access. Member country representatives supplied the best options for their citizens to access this database. Furthermore, the United States fed this database to [[Dialog (online database)|Dialog]] which provided online access. Likewise, Germany fed this database to [[STN International]].

Broad subject coverage included information on energy research and development; [[energy policy]] and planning; basic sciences (e.g., [[physics]], [[chemistry]] and [[biomedical]]) and [[materials research]]; the [[Sustainability|environmental impact]] of [[energy production]] and use, including [[climate change]]; [[energy conservation]]; nuclear (e.g., [[Nuclear reactor technology|reactors]], isotopes, [[nuclear waste management|waste management]]); coal and [[fossil fuels]]; and renewable energy technologies (e.g., [[solar energy]], [[wind energy]], [[biomass]], [[geothermal]], hydro). The scope of topical coverage was worldwide in some areas. The database was used by scientists, researchers, engineers, policymakers, information specialists, librarians, industry leaders, university faculty, and university students, among others.

== ETDEWEB ==
Energy Technology Data Exchange employed an [[internet]] related database to disseminate the energy research and technology information which was collected and exchanged.  The database was named '''''ETDE World Energy Base''''' or '''''ETDEWEB'''''.<ref name=home>
{{cite web
  | last =Energy Technology Data Exchange
  | title =Home page
  | publisher =International Energy Agency
  | date =July and August 2010
  | url =http://www.etde.org/
  | accessdate =2010-07-08}}</ref>

ETDEWEB was produced and made available by ETDE.  It had over 5 million references for literature that encompassed broad topical coverage, and allowed access to more than 500 000 full text documents and reports, which amounted to more than 1 million pages. ETDEWEB had unique access to these reports, which were often not available through other conventional sources.  Over a million other references linked to sites containing cited documents. Open access was provided to member countries, countries with developing country status, or by Executive Committee decision.<ref name=etdewebpg>
{{cite web
  | last =Energy Technology Data Exchange
  | title =ETDEWEB basic information
  | publisher =International Energy Agency
  | date =August 2010
  | url =http://www.etde.org/
  | accessdate =2010-07-08}}</ref>

Online access was extended through the wordwideenergy.org website.

===WorldWideENERGY.org (WWE)===
After the previous ETDE consortium ended in 2014, the WWE application allowed ETDEWEB and other content to remain accessible, thanks to remaining funds and former member country support. But this extended access also ended in July 2016.<ref name=WWE>
{{cite web
  | last =WorldWideENERGY.org
  | title =Home page
  | publisher =International Energy Agency
  | date =October 2016
  | url =http://worldwideenergy.org/
  | accessdate =2016-08-24}}</ref> There may be a possibility that ETDEWEB will again become accessible at some unspecified time in the future via the US DOE’s Office of Scientific and Technical Information (OSTI) systems.

=== Subject coverage ===
ETDEWEB covered an extensive base of topics,  the main areas included information on energy research and development along with energy policy and planning.  Other ares of coverage included basic sciences (e.g., physics, chemistry and biomedical); materials research; the environmental impacts of energy production and use (including climate change); energy conservation; nuclear energy (e.g., reactors, isotopes, and nuclear waste management); coal and fossil fuels; and renewable energy technologies (e.g., solar, wind, biomass, geothermal, hydro).<ref name=energyDB>
{{cite web
  | last =Energy Technology Data Exchange
  | title =ETDE's Energy Database
  | publisher =International Energy Agency
  | date =July 2010
  | url =http://www.etde.org/edb/energy.html
  | accessdate =2010-07-08}}</ref><ref name=subj-coverage>
{{cite web
  | last =Energy Technology Data Exchange
  | title =Subject Contents
  | publisher =International Energy Agency
  | date =July 2010
  | url =http://www.etde.org/edb/subject.html
  | accessdate =2010-07-08}}</ref>

== See also ==
* [[Energy Science and Technology Database]]
* [[Energy Citations Database]]
* [[Dialog (online database)]]

== References ==
{{Reflist}}

== External links ==
* [http://www.etde.org/ Energy Technology Data Exchange]
* [http://www.etde.org/edb/energy.html ETDEWEB]
* [http://journal-indexes.uwaterloo.ca/display.cfm?navbar=uw&list=all List of free research databases]. University of Waterloo, Ontario, Canada. July 2010.

{{Environmental technology}}
{{Environmental science}}

[[Category:Bibliographic databases and indexes]]
[[Category:Bibliographic database providers]]
[[Category:International energy organizations]]
[[Category:Organizations established in 1987]]
[[Category:International Energy Agency]]