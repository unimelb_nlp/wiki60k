{{Infobox journal
| title = Focus on Alternative and Complementary Therapies
| cover =
| former_name =
| abbreviation = Focus Altern. Complement. Ther.
| discipline = [[Alternative medicine]]
| editor = [[Edzard Ernst]]
| publisher = [[Wiley-Blackwell]] on behalf of the [[Royal Pharmaceutical Society of Great Britain]]
| country = United Kingdom
| history = 1996-present
| frequency = Quarterly
| openaccess =
| license =
| impact =
| impact-year =
| ISSN = 1465-3753
| eISSN = 2042-7166
| CODEN =
| JSTOR =
| LCCN = 2011206010
| OCLC = 728924758
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2042-7166
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2042-7166/currentissue
| link1-name = Online access
| link2 =http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2042-7166/issues
| link2-name =Online archive
}}
'''''Focus on Alternative and Complementary Therapies''''' is a [[peer-reviewed]] [[medical journal|medical]] [[review journal]] covering [[complementary and alternative medicine]]. The journal's founder and [[editor-in-chief]] is [[Edzard Ernst]] ([[University of Exeter]]). It is published by [[Wiley-Blackwell]] on behalf of the [[Royal Pharmaceutical Society of Great Britain]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=overview>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2042-7166/homepage/ProductInformation.html |title=Overview |publisher=Wiley-Blackwell |accessdate=13 October 2014}}</ref>
{{columns-list|colwidth=30em|
* [[Abstracts on Hygiene and Communicable Diseases]]
* [[Allied & Complementary Medicine Database]]
* [[CAB Abstracts]]
* [[CINAHL]]
* [[Embase]]
* [[EMCare]]
* [[Global Health]]
* [[Horticultural Science Abstracts]]
* [[Scopus]]
* [[Tropical Diseases Bulletin]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2042-7166}}

[[Category:Review journals]]
[[Category:Quarterly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Alternative and traditional medicine journals]]
[[Category:Publications established in 1996]]
[[Category:English-language journals]]