{{Infobox journal
| title = Journal of Studies in International Education
| cover = [[File:Journal of Studies in International Education.tif]]
| editor = Betty Leask
| discipline = [[Education]]
| former_names = 
| abbreviation = J. Stud. Int. Educ.
| publisher = [[Sage Publications]] on behalf of the [[Association for Studies in International Education]]
| country = 
| frequency = Quarterly 
| history = 1997-present
| openaccess = 
| license = 
| impact = 0.980
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201378/title
| link1 = http://jsi.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jsi.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1028-3153
| eISSN = 1552-7808
| OCLC = 663324762
| LCCN = 97657707
}}
The '''''Journal of Studies in International Education''''' is a quarterly [[peer-reviewed]] [[academic journal]] that covers the field of [[education]]. The journal's [[editor-in-chief]] is Betty Leask ([[La Trobe University]]). It was established in 1997 and is published by [[Sage Publications]] on behalf of the [[Association for Studies in International Education]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.980, ranking it 67th out of 219 journals in the category "Education & Educational Research".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Education & Educational Research|title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science}}</ref> 

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201378/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1997]]
[[Category:Education journals]]
[[Category:Quarterly journals]]