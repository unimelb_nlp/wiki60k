{{Infobox publisher
| image = 
| parent = [[EBSCO Publishing]]
| status = 
| founded = 1898
| founder = [[Halsey (H.W.) Wilson|Halsey William Wilson]]
| successor = 
| country = United States
| headquarters = [[The Bronx|The Bronx, New York]]
| distribution = 
| keypeople = 
| publications = [[Book]]s, [[database]]s
| topics = 
| genre = 
| imprints = 
| revenue = 
| numemployees = 
| url  = {{URL|http://www.ebscohost.com/wilson}} (online)<br>{{URL|http://www.hwwilsoninprint.com}} (print)
}}
The '''H. W. Wilson Company, Inc.''', was founded in 1898 and is located in [[The Bronx]], New York. It provides print and digital content aimed at patrons of public school, college, and professional libraries in both the United States and internationally. The company also provides indexing services that include text, retrospective, abstracting and indexing, as well other types of [[database]]s. Image gallery indexing includes art museum and cinema. The company also indexed reference monographs. An online retrieval system with various features, including language translation, is also available. The company merged with [[EBSCO Publishing]] in June 2011.<ref name=blmbg> 
{{cite web
 | title = Company Overview of The HW Wilson Company, Inc.
 | publisher = [[Bloomberg Businessweek]]
 | date =
 | url = http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=4305434
 | format =
 | accessdate = 2012-07-24}}</ref><ref name=annce>
{{cite web
 | last =
 | title = EBSCO Publishing and The H.W. Wilson Company Make Joint Announcement of Merger Agreement
 | publisher = PR Web
 | date = June 2, 2011
 | url = http://www.prweb.com/releases/EBSCOHWWilson/Merger/prweb8516676.htm
 | accessdate = 2012-07-24}}</ref> [[Grey House Publishing]] currently publishes print editions of H. W. Wilson products under license.

==History==
[[File:HW Wilson lighthouse W162 Bx jeh.jpg|thumb|Lighthouse building, South Bronx]]
The H. W. Wilson Company was founded in 1889 by [[Halsey (H.W.) Wilson|Halsey William Wilson]], a student working his way through the [[University of Minnesota]], Minneapolis. Together with his roommate, Henry S. Morris, Wilson started a book selling business serving educators and students at the university. When it was time for Morris to graduate, he sold his share of the business to Wilson. The H. W. Wilson Company's first original reference title was the ''Cumulative Book Index'', first published in 1898. This was followed by the ''Readers' Guide to Periodical Literature'' in 1901.

In 1911, Wilson relocated the company to [[White Plains, New York]], to be nearer to its main markets. By 1917, demand for more specialized indexes had grown to the point where the company had to move again. Wilson bought a five-story building in The Bronx on the banks of the Harlem River. The building's distinctive lighthouse was added in 1929. On June 2, 2011, H. W. Wilson Company merged with [[EBSCO Publishing]].<ref>{{cite web|title=Questions & Answers regarding EBSCO Publishing's Merger with The H.W. Wilson Company|url=http://support.epnet.com/knowledge_base/detail.php?id=5482|accessdate=6/1/2011}}</ref> Staff at the Dublin office of the company went to the Labour Court in Ireland and protested at EBSCO's failure to abide by its recommendations.<ref>{{cite web |url=http://www.thejournal.ie/ex-hw-wilson-staff-to-protest-over-redundancy-package-444783-May2012/ |title=Ex-HW Wilson staff to protest over redundancy package · TheJournal.ie |format= |work= |accessdate=2012-07-24}}</ref>

==Products==
The company also published some print references, such as ''Facts About the Presidents'', ''Famous First Facts'', and the monthly [[magazine]] ''[[Current Biography]]''.

==John Cotton Dana Library Public Relations Award==
The John Cotton Dana Award, sponsored by H.W. Wilson, honors outstanding library public relations, whether a summer reading program, a year-long centennial celebration, fundraising for a new college library, an awareness campaign, or an innovative partnership in the community. Award winners receive a cash development grant of $5,000 from the H.W. Wilson Foundation. The awards are presented at a reception hosted by H.W. Wilson Company, held during the [[American Library Association]]'s annual conference.

==Database descriptions==

===Applied Sciences ===
H. W. Wilson Company's applied sciences indexing encompasses services under the title "Applied Science & Technology". These are  "abstracts", "full text", and "retrospective". Broad subject coverage includes the major scientific disciplines and their respective sub disciplines in abstracts or full text. For example, topical coverage includes [[acoustics]], environmental and [[earth sciences]], [[engineering]] (chemical, civil, environmental, electrical, industrial, etc.), [[geology|geological sciences]] (including space science), food industry (and sciences), plastics, textile industry (including fabrics), transportation, and waste management. The retrospective indexing of Applied Science and Technology covers more than 1,350 periodicals from 1913 to 1983.

===Art indexes===
H. W. Wilson Company's indexing pertaining to art encompasses "Art Abstracts", "Art Full Text", "Art Index", and "Art Index Retrospective: 1929-1984". It includes 304 full-text journals that focus on  fine art, decorative art, commercial art, photography, folk art, film, architecture and other areas. Subjects covered include art history and criticism, architecture and architectural history, archaeology, antiques, museum studies, graphic arts, industrial design, landscape architecture, interior design, folk art, painting, photography, pottery, sculpture, decorative arts, costume design, television, video, motion pictures, advertising art, non-western art, textiles and other related subjects.<ref name=databs>[http://www.ebscohost.com/superdatabases  EBSCO and H.W. Wilson Source Databases]. EBSCO Industries. 2012</ref>

==WilsonWeb==
WilsonWeb is "an online based information retrieval system that offers an interface, multiple search modes, interactive help messages, and text translation into various languages".<ref name=blmbg/>

==References==
{{Reflist}}

== Further reading==
* "A Modern-Day Scriptorium" By Ernest Rubinstein. ''[[The Chronicle Review]]''. January 10, 2014.

==External links==
* {{Official website|http://www.hwwilsoninprint.com}}
* [http://www.fundinguniverse.com/company-histories/the-h-w-wilson-company-history/ The H.W. Wilson Company History]
* [http://www.enotes.com/topic/H._W._Wilson_Company Overview and history] of The H.W. Wilson Company, Inc.
* [http://www.nova.edu/library/dils/lessons/wilsonweb/ About WilsonWeb]. Nova Southeastern University. 2013.

[[Category:Bibliographic database providers]]
[[Category:Publishing companies of the United States]]
[[Category:Publishing companies established in 1889]]
[[Category:Companies based in New York City]]
[[Category:1889 establishments in New York]]