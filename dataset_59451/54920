'''Anchabadze''' ({{lang-ka|ანჩაბაძე}}), also known as '''Achba''' ({{lang-ab|А́чба}}), is an [[Abkhaz people|Abkhaz]]-[[Georgians|Georgian]] family, and the oldest surviving noble house originating in [[Abkhazia]]. 

The Anchabadze family is supposed to have its roots in the early medieval ruling dynasty of [[Abasgia]]. After the break-up of the [[Kingdom of Georgia]] in the late 15th century, Abkhazia came under the influence of the [[Ottoman Empire]] and [[Islam]], forcing several members of the family into flight to the eastern Georgian lands – [[Kartli]] and [[Kakheti]]. Thus, they formed the three principal branches: the Abkhazian line of the princes Anchabadze, the Kartlian [[Machabeli]], and the Kakhetian [[Abkhazi]]. All these three families were later integrated into the [[Imperial Russia]]n [[knyaz|princely nobility]]: Machabeli and Abkhazi in 1826/1850, and Anchabadze in 1903.<ref>[[Cyril Toumanoff|Toumanoff, Cyril]] (1967). ''Studies in Christian Caucasian History'', p. 269. [[Georgetown University Press]].</ref>  

The descendants of this family have survived in Abkhazia and [[Tbilisi]], and bear the surnames based on the two letter [[Abkhaz language|Abkhazian]] root Ach ({{lang-ab|А́ч}}) (Achba) and on the three letter [[Georgian language|Georgian]] root Anch ({{lang-ka|ანჩ}}) (Anchabadze).

== References ==

{{Reflist}}

== See also ==

*[[List of Georgian princely families]]

[[Category:Noble families of Georgia (country)]]
[[Category:Abkhazian nobility]]
[[Category:Russian noble families]]

{{abkhazia-stub}}
{{Georgia-noble-stub}}