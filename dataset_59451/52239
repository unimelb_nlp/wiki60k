{{Infobox journal
| title = Mathematics of Control, Signals, and Systems
| cover = [[File:Mathematics of Control, Signals, and Systems.jpg]]
| abbreviation = Math. Control Signals Syst.
| editor = Jan H. van Schuppen
| discipline = [[Mathematics]]
| publisher = [[Springer Science+Business Media]]
| country =
| history = 1988-present
| frequency = Quarterly
| openaccess =
| impact = 0.500
| impact-year = 2011
| website = http://www.springer.com/journal/498
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=0932-4194&issue=current
| link1-name = Online access
| link2 = http://link.springer.com/journal/volumesAndIssues/498
| link2-name = Online archive
| JSTOR =
| OCLC = 782072556
| LCCN = 88640596
| CODEN =
| ISSN = 0932-4194
| eISSN = 1435-568X
}}
'''''Mathematics of Control, Signals, and Systems''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] that covers research concerned with mathematically rigorous system theoretic aspects of [[Control theory|control]] and [[signal processing]]. The [[editor-in-chief]] is [[Jan H. van Schuppen]].

== Abstracting and indexing ==
The journal is abstracted and indexed in Digital Mathematics Registry, [[Mathematical Reviews]], [[Science Citation Index]], [[Scopus]], [[VINITI Database RAS]], and [[Zentralblatt Math]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.500.<ref name=WoS>{{cite book |year=2012 |chapter=Mathematics of Control, Signals, and Systems |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-11-24 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/journal/498}}

[[Category:Publications established in 1988]]
[[Category:Mathematics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]