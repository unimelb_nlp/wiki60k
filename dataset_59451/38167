{{Infobox motorsport championship
| logo =
| pixels =
| caption =
| category = [[One-Design|One-make series]]
| inaugural = 1979
| drivers = Varied
| teams = Varied
| constructors = [[BMW]]
| engines =
| country/region = [[Europe]]
| folded = 1980
| champion driver = [[Niki Lauda]] <small>(1979)</small><br>[[Nelson Piquet]] <small>(1980)</small>
| champion team =
| manufacturer =
| website =
}}
[[File:BMW Motorsport M1 Procar.jpg|thumb|right|250px|A Procar BMW M1, painted in the colour scheme used by [[BMW Motorsport]]]]
:''For the later German touring car series, see [[ADAC Procar Series]].''
The '''BMW M1 Procar Championship''', sometimes known simply as '''Procar''', was a [[one-design|one-make]] [[auto racing]] series created by [[Jochen Neerpasch]],<ref name="M1 Procar">{{cite web|url=http://www.qv500.com/bmwm1p3.php |title=BMW (E26) M1 Part 3: M1 Group 4 Procar Championship 1979&nbsp;– 80 |publisher=QV500 |accessdate=2008-07-05 |archiveurl=https://web.archive.org/web/20080514034348/http://www.qv500.com/bmwm1p3.php |archivedate=2008-05-14 |deadurl=no |df= }}</ref> head of [[BMW M|BMW Motorsport GmbH]], the racing division of automobile manufacturer [[BMW]].  The series pitted professional drivers from the [[Formula One|Formula One World Championship]], [[World Sportscar Championship]], [[European Touring Car Championship]], and other international series against one another using identically modified [[BMW M1]] [[sports car]]s.

Billed as an opportunity to see a mix of drivers from various motorsport disciplines,<ref name="Revival">{{cite web | url = http://www.formula1.com/news/headlines/2008/6/8013.html | title = Past masters to revive BMW M1 Procar at Hockenheim | publisher = [[Formula One Management]] | date = 2008-06-30 | accessdate = 2008-07-03| archiveurl= https://web.archive.org/web/20080702183638/http://www.formula1.com/news/headlines/2008/6/8013.html| archivedate= 2 July 2008 <!--DASHBot-->| deadurl= no}}</ref> the championship served as support races for various European rounds of the [[1979 Formula One season]], with Formula One drivers earning automatic entry into the Procar event based on their performance in their Formula One cars.<ref name="M1 Procar" />  [[Austria]]n [[Niki Lauda]] won the inaugural championship.  In 1980, the series held some events outside of Formula One schedule, and was won by [[Brazil]]ian [[Nelson Piquet]].  BMW chose not to continue the championship in 1981 to concentrate on their entrance into Formula One.<ref name="M1 Procar" />

==Origin==
:{{see also|BMW M1}}
[[Jochen Neerpasch]], the head of BMW's Motorsports division, was the first to propose the idea of a one-make championship.<ref name="Revival" />  The division had started construction of the first sports car for BMW in 1978, the [[BMW M1|M1]], and had planned from the start to enter the new sports car in the [[World Sportscar Championship]] in 1979, as well as offering the cars to customers for other series.  BMW Motorsport planned to build M1s to meet regulations known as [[Group 5 (racing)|Group 5]], but a rule change instituted by the [[Fédération Internationale du Sport Automobile]] (FISA) in 1977 altered the requirements for Group 5.  The new regulations required a minimum of 400 examples of the M1 to be built to meet [[Group 4 (racing)|Group 4]] regulations before the car could be further [[homologation|homologated]] for the Group 5 category.<ref name="M1 Group 4">{{cite web|url=http://www.qv500.com/bmwm1p2.php |title=BMW (E26) M1 Part 2: M1 Group 4 |publisher=QV500 |accessdate=2008-07-05 |archiveurl=https://web.archive.org/web/20080419172703/http://www.qv500.com/bmwm1p2.php |archivedate=2008-04-19 |deadurl=no |df= }}</ref>
[[File:BMW M1 1.jpg|thumb|right|200px|The BMW M1 road car]]
Development of the Group 4 racing car was already under way at the time of the regulation change.  Neerpasch believed that rather than delaying their racing program until 400 road cars had been built, racing cars could be built at the same time since they too counted toward the 400 example minimum.  A [[One-design|one-make series]] consisting of the M1 racing cars intended for Group 4 was devised by Neerpasch since the racing cars could not yet legally compete elsewhere, while at the same time allowing BMW to develop the race cars through experience.<ref name="Revival" /><ref name="M1 Group 4" />

To attract drivers to the series, Neerpasch entered into discussions with [[Max Mosley]].  Mosley was the head of [[March Engineering]], a constructor which BMW was partnered with in their factory efforts in the [[Formula Two|European Formula Two Championship]].  Mosley was at the time a member of the [[Formula One Constructors Association]], and was able to use his position to convince other [[Formula One]] constructors to support the use of Neerpasch's one-make series as a support race for European [[Formula One]] events.<ref name="M1 Procar" />  A ruling and organisation body for the series, known as The Procar Association, was set up by BMW and approved by FISA.<ref name="British Grand Prix">{{cite book | title = Marlboro British Grand Prix Official Program | year = 1980 | pages = 6 | url = http://www.racingsportscars.com/photo/Brands_Hatch-1980-07-13-photo.html | accessdate = 2008-07-05}}</ref>

==Format==
With the Procar Championship announced in spring 1978 at the official unveiling of the M1 road car, Neerpasch and the newly formed Procar Association laid out regulations for the 1979 season.  Races were planned for the middle of the Formula One season, when the championship remained in Europe for several months.  Practice and qualifying were held on Friday of the race weekend, while the race itself occurred on Saturday.  The winner of each race received US&nbsp;$5,000, second place received $3,000, and third place $1,000.<ref name="M1 Procar" />  Races varied in length, but each lasted for approximately half an hour.<ref name="British Grand Prix" />

To attract Formula One drivers to the championship, seats were guaranteed in the Procar races by cars which were entered by the factory BMW team.  The five fastest Formula One drivers from Friday practice were assigned to the factory team, and were even guaranteed the first five grid positions for the Procar race, regardless of their qualifying times in the cars.<ref name="British Grand Prix" />  This not only guaranteed Formula One driver participation, but also allowed for a variety of drivers to be seen over the course of the season.  However, because the series ran [[Goodyear Tire & Rubber Company|Goodyear]] tyres, several Formula One drivers were not allowed to compete due to [[contract|contractual obligations]] with competing tyre manufacturer [[Michelin]].<ref name="M1 Procar" />  As [[Scuderia Ferrari]] and [[Renault F1|Renault]] were selling roadcars themselves, they did not allow their Formula One drivers to participate and implicitly endorse BMW.<ref name="BMW Cars">{{cite book | title = BMW Cars | first = Martin | last = Buckley |author2=Nick Dimbleby | publisher = MotorBooks/MBI Publishing Company | year = 2002 | url = https://books.google.com/books?id=Mo1tyh2HM18C | accessdate = 2008-08-19 | isbn = 0-7603-0921-3 | pages = 142}}</ref><ref>{{cite web |title=Remembering the Procar days |url=http://www.formula1.com/news/features/2004/7/1930.html |publisher=Formula One Management |work=Formula1.com |date=2004-07-21}}</ref>

Other teams were allowed to participate, assigning a variety of [[sports car racing|sportscar]] and [[touring car racing|touring car]] drivers as well as Formula One drivers who had not earned spots in the factory cars.  Points were awarded to the top ten finishers, starting with 20 points and decreasing down to 15, 12, 10, 8, 6, 4, 3, 2, and finally 1 for the nine remaining drivers.  An overall championship was awarded to the driver who had accumulated the most points at the end of the season.<ref name="British Grand Prix" />  A new M1 road car was awarded to the championship winner.<ref name="Classic Driver">{{cite web | url = http://www.classicdriver.com/uk/magazine/3700.asp?id=13848 | title = Return of the BMW M1 Procars | publisher = Classic Driver | author = Charis Whitcombe | accessdate = 2008-07-26}}</ref>

===Cars===
All cars used in the Procar Championship were built to identical standards, although their origins varied.  [[BS Fabrications]] constructed five cars for the [[BMW Motorsport|BMW factory team]], while cars for other competitors were constructed by the British Formula Two team [[Project Four Racing]] (led by [[Ron Dennis]]) and the Italian constructor [[Osella]].<ref name="M1 Procar" />  The racing cars, designed to meet Group 4 technical regulations, shared only some basics from the M1 road cars.
[[File:BMW M1 Motor.jpg|thumb|left|200px|The engine bay of a Procar M1.  The M88/1 engine is a heavily tuned variant of the road car's [[BMW M88|M88]] [[straight-6]].]]
For the M1's exterior, simple modifications were made to adapt the car aerodynamically.  A deep [[spoiler (automotive)|spoiler]] was added under the nose of the car, while an adjustable wing was added on two mounts at the rear of the engine cover.<ref name="M1 Procar" />  The arches for the wheels were extended outward to shroud the wider {{convert|28|cm|in}} wheels in the front, and {{convert|32|cm|in|1|abbr=on}} wheels at the rear.<ref name="M1 Dimensions">{{cite web | url = http://www.bmw.com/bmwhistory/com/en/1979/motorsport/media4/MSP_TW_79.pdf | title = BMW history&nbsp;– BMW M1 ProCar | publisher = [[BMW]] | format = PDF | accessdate = 2008-07-25}}</ref>  The alloy wheels, designed by [[Campagnolo]], featured a central locking nut and mimicked the straked design of the production car's wheels.<ref name="Original M-Series">{{cite book | title = Original BMW M-Series | last = Taylor | first = James | year = 2001 | accessdate = 2008-08-19 | publisher = MotorBooks/MBI International Company | isbn = 0-7603-0898-5 | url = https://books.google.com/books?id=s5f34jrigrUC | pages = 28}}</ref>  Inside, the car featured no finishing or luxuries from the road car, only a [[rollcage]] for driver protection.  The glass windows were replaced with clear plastic.<ref name="M1 Procar" />

Mechanically, the Procar M1s were extensively modified to meet the requirements of circuit racing.  The standard M1 featured the cast iron block [[BMW M88]] [[straight-6]] with {{convert|3453|cc|cuin}} displacement.<ref name="M1 Dimensions" />  Modified into the M88/1 by a team led by [[Paul Rosche]],<ref name="Rosche">{{cite web | url = http://www.bmw-sauber-f1.com/en/#/interactive/m1-revival/~2%7c0/ | title = Interactive. M1 Revival. | publisher = [[BMW Sauber]] | accessdate = 2008-07-29| archiveurl= https://web.archive.org/web/20080728211305/http://www.bmw-sauber-f1.com/en/| archivedate= 28 July 2008 <!--DASHBot-->| deadurl= no}}</ref> it was capable of producing 470&nbsp;[[horsepower]] at 9000&nbsp;[[revolutions per minute|rpm]],<ref name="M1 Dimensions" /> compared to 277&nbsp;hp from the standard M1's engine.  The 5-speed [[ZF Friedrichshafen|ZF]] [[gearbox]] housing from the road car was retained,<ref name="M1 Dimensions" /> but with multiple gear ratios allowed, as well as its own oil cooler.<ref name="M1 Procar" />

The suspension was entirely new, adding adjustable [[anti-roll bar]]s.  The brakes were redesigned to adapt the use of a driver-adjustable brake pressure balance, while the [[Servomechanism|servo]]-activated braking system was removed.  A racing steering rack was used in place of the standard power steering unit.  Pneumatic jacks were later added to the cars in the 1980 season to allow for quicker [[pit stop]]s.  [[Goodyear Tire & Rubber Company|Goodyear]] racing tires were used on all cars.<ref name="M1 Procar" />  All other non-essential items from the road car were not included, bringing the Procar M1's weight down to {{convert|1020|kg|lb}}.<ref name="M1 Dimensions" />  The performance of the Procar M1s was increased to a top speed of {{convert|311|km/h|mph}} dependent upon [[gear ratio]]s,<ref name="M1 Dimensions" /> and acceleration from 0–{{convert|100|km/h|mph|abbr=on}} in 4.3&nbsp;seconds.  Each Procar M1 cost approximately US&nbsp;$60,000.<ref name="M1 Procar" />

==Series history==

===1979 season===
[[File:BMW M1 Procar Cropped.jpg|thumb|right|200px|A Procar M1 driven by [[Clay Regazzoni]] for the [[BMW Motorsport]] team]]
An announced schedule for the inaugural season of the Procar Championship featured events taking place from May to September, when the Formula One World Championship ran eight consecutive [[List of Formula One Grands Prix|Grands Prix]] in Europe.  A ninth event was scheduled at [[Donington Park]] as part of the [[Gunnar Nilsson Memorial Trophy]], a charity event for the Gunnar Nilsson Cancer Fund, although it did not award points towards the overall championship.<ref name="Gunnar Nilsson Trophy">{{cite web | url = http://www.forix.com/8w/nilsson-trophy.html | title = The fan car raced twice! | publisher = Forix&nbsp;– 8W | author = Mattijs Diepraam and Tom Prankerd |date=December 2000 | accessdate = 2008-07-05}}</ref>

Different Formula One drivers earned spots on the factory team over the season based on their performance in Formula One practice.  These included [[Mario Andretti]], [[Patrick Depailler]], [[Emerson Fittipaldi]], [[James Hunt]], [[Jean-Pierre Jarier]], [[Alan Jones (Formula 1)|Alan Jones]], [[Jacques Laffite]], [[Niki Lauda]], [[Nelson Piquet]], [[Didier Pironi]], [[Clay Regazzoni]], and [[John Watson (racing driver)|John Watson]].  [[Teo Fabi]], [[Tiff Needell]], [[Hans-Georg Bürger]] and [[Michael Bleekemolen]] were invited to drive in the factory BMW cars as well although they were not Formula One drivers at the time.<ref name="1979 season">{{cite web|url=http://www.qv500.com/bmwm1p379a.php |title=BMW M1 Procar 1979 Season |publisher=QV500 |accessdate=2008-07-26 |archiveurl=https://web.archive.org/web/20080720035021/http://www.qv500.com/bmwm1p379a.php |archivedate=2008-07-20 |deadurl=no |df= }}</ref>

As well as the standard five car entry from BMW, several other notable teams participated.  Procar constructor Project Four entered a car for [[Niki Lauda]] when he was not in the factory entries, while Osella entered cars for a rotating driver line-up.  Touring car teams [[Tom Walkinshaw Racing]], [[Eggenberger Motorsport|Eggenberger Racing]], Ecurie Arvor, and [[Schnitzer Motorsport]] entered cars, as did [[Konrad Motorsport|Team Konrad]] and GS Tuning from sports cars.<ref name="1979 season" />

====Race results====
{| class="wikitable" style="font-size: 100%;"
|- valign="top"
! Round
! Date
! Event
! Circuit
! Winning Driver
! Winning Team
|-
! 1
| align="center" | May 12
| {{flagicon|BEL}} [[Belgian Grand Prix]]
| [[Circuit Zolder]]
| {{flagicon|ITA}} [[Elio de Angelis]]
| [[Osella|Squadra Osella Corse]]
|-
! 2
| align="center" | May 26
| {{flagicon|MON}} [[Monaco Grand Prix]]
| [[Circuit de Monaco]]
| {{flagicon|AUT}} [[Niki Lauda]]
| [[Project Four Racing]]
|- style="background:#dddddd;"
!&nbsp;–
| align="center" | June 3
| {{flagicon|GBR}} [[Gunnar Nilsson Memorial Trophy|Gunnar Nilsson Trophy]]
| [[Donington Park]]
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
|-
! 3
| align="center" | June 30
| {{flagicon|FRA}} [[French Grand Prix]]
| [[Dijon-Prenois]]
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
|-
! 4
| align="center" | July 13
| {{flagicon|GBR}} [[British Grand Prix]]
| [[Silverstone Circuit]]
| {{flagicon|AUT}} [[Niki Lauda]]
| [[Project Four Racing]]
|-
! 5
| align="center" | July 28
| {{flagicon|DEU}} [[German Grand Prix]]
| [[Hockenheimring]]
| {{flagicon|AUT}} [[Niki Lauda]]
| [[Project Four Racing]]
|-
! 6
| align="center" | August 11
| {{flagicon|AUT}} [[Austrian Grand Prix]]
| [[Österreichring]]
| {{flagicon|FRA}} [[Jacques Laffite]]
| [[BMW Motorsport]]
|-
! 7
| align="center" | August 25
| {{flagicon|NED}} [[Dutch Grand Prix]]
| [[Circuit Zandvoort]]
| {{flagicon|DEU}} [[Hans-Joachim Stuck]]
| [[Cassani Racing]]
|-
! 8
| align="center" | September 8
| {{flagicon|ITA}} [[Italian Grand Prix]]
| [[Autodromo Nazionale Monza]]
| {{flagicon|DEU}} [[Hans-Joachim Stuck]]
| [[Cassani Racing]]
|}

====Championship standings====
The following are the ten highest finishing drivers in the points standings.<ref name="Standings">{{cite web | url = http://wspr-racing.com/wspr/results/procar/tableprocar.html | title = Procar BMW M1&nbsp;– final positions and tables | publisher = World Sports Prototype Racing | accessdate = 2008-07-03}}</ref>  [[Niki Lauda]] won the inaugural championship, initially earning a spot in the factory BMW team in the first round, before running the remaining seven races for Project Four.

{| class="wikitable" style="font-size: 90%;"
|- valign="top"
! Pos.
! Driver
! Team(s)
! Points
|-
! 1
| {{flagicon|AUT}} [[Niki Lauda]]
| [[BMW Motorsport]] / [[Project Four Racing]]
! 78
|-
! 2
| {{flagicon|DEU}} [[Hans-Joachim Stuck]]
| [[Cassani Racing]]
! 73
|-
! 3
| {{flagicon|SUI}} [[Clay Regazzoni]]
| [[BMW Motorsport]]
! 61
|-
! 4
| {{flagicon|AUT}} [[Markus Höttinger]]
| [[GS Tuning]]
! 45
|-
! 5
| {{flagicon|NED}} [[Toine Hezemans]]
| [[Alimpo Sport]]
! 44
|-
! 6=
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
! 35
|-
! 6=
| {{flagicon|FRA}} [[Jacques Laffite]]
| [[BMW Motorsport]]
! 35
|-
! 8
| {{flagicon|FRA}} [[Didier Pironi]]
| [[BMW Motorsport]]
! 34
|-
! 9
| {{flagicon|DEU}} [[Helmut Kelleners]]
| [[Eggenberger Motorsport|Eggenberger Racing]]
! 33
|-
! 10
| {{flagicon|AUS}} [[Alan Jones (Formula 1)|Alan Jones]]
| [[BMW Motorsport]]
! 26
|}

===1980 season===
[[File:Piquet BMW M1 Procar.jpg|thumb|right|200px|A Procar BMW M1 driven by 1980 champion [[Nelson Piquet]]]]
For the second running of the Procar Championship, the schedule was altered and expanded so that the races did not rely exclusively on Formula One events.  This allowed the season to start slightly earlier, this time in April.  [[Donington Park]] was retained from the previous year but now counted towards the championship.  Two German events were added, with the Procar Championship designated as that year's [[Avusrennen]] at the [[AVUS]] circuit,<ref name="Avus">{{cite web | url = http://theracingline.net/racingcircuits/racingcircuits/Germany/_gpAvusrennen.html | title = Avusrennen Winners | publisher = Motor Racing Circuits Database | accessdate = 2008-08-15}}</ref> and the Procars serving as a support race for the [[Deutsche Rennsport Meisterschaft]]'s [[200 Miles of Norisring]].<ref name="DRM">{{cite web | url = http://wsrp.ic.cz/drm1980.html#7 | title = Deutsche Rennsport Meisterschaft 1980 | publisher = World Sports Racing Prototypes | date = 2008-04-15 | accessdate = 2008-08-15| archiveurl= https://web.archive.org/web/20080916180149/http://www.wsrp.ic.cz/drm1980.html| archivedate= 16 September 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="http://theracingline.net/racingcircuits/racingcircuits/_series/gt_bmw-procar/index.html">{{cite web | url = http://theracingline.net/racingcircuits/racingcircuits/_series/gt_bmw-procar/index.html | title = BMW M1 Procar Series 1980 Season | publisher = Motor Racing Circuits Database | accessdate = 2008-08-15}}</ref>  Six Formula One Grands Prix remained on the schedule.

Unlike the previous year where the fastest drivers in practice earned seats with the factory BMW team, five drivers were designated as the primary drivers for events which did not share a Grand Prix weekend.  These five were [[Alan Jones (Formula 1)|Alan Jones]], [[Jacques Laffite]], [[Nelson Piquet]], [[Didier Pironi]], and [[Carlos Reutemann]].  Other Formula One drivers to participate during Grands Prix were [[Mario Andretti]], [[Derek Daly]], [[Jean-Pierre Jarier]], [[Riccardo Patrese]], and [[Alain Prost]].<ref name="1980 season">{{cite web|url=http://www.qv500.com/bmwm1p380a.php |title=BMW M1 Procar 1980 Season |publisher=QV500 |accessdate=2008-07-26 |archiveurl=https://web.archive.org/web/20080720144740/http://www.qv500.com/bmwm1p380a.php |archivedate=2008-07-20 |deadurl=no |df= }}</ref>

Several teams from the previous year entered once more, including Project Four, GS Tuning, Eggenberger Racing, Cassani Racing, and [[Schnitzer Motorsport]].  Newcomers included personal teams from [[Arturo Merzario]], [[Dieter Quester]], and [[Helmut Marko]], as well as Swiss sportscar manufacturer [[Sauber]].<ref name="1980 season" />

====Race results====
{| class="wikitable" style="font-size: 100%;"
|- valign="top"
! Round
! Date
! Event
! Circuit
! Winning Driver
! Winning Team
|-
! 1
| align="center" | April 26
| {{flagicon|GBR}} International Procar Meeting
| [[Donington Park]]
| {{flagicon|NED}} [[Jan Lammers]]
| [[BMW Holland]]
|-
! 2
| align="center" | May 11
| {{flagicon|DEU}} [[Avusrennen]]
| [[AVUS]]
| {{flagicon|LIE}} [[Manfred Schurti]]
| [[Cassani Racing]]
|-
! 3
| align="center" | May 17
| {{flagicon|MON}} [[Monaco Grand Prix]]
| [[Circuit de Monaco]]
| {{flagicon|DEU}} [[Hans-Joachim Stuck]]
| [[Project Four Racing]]
|-
! 4
| align="center" | June 22
| {{flagicon|DEU}} [[200 Miles of Norisring]]
| [[Norisring]]
| {{flagicon|DEU}} [[Hans-Joachim Stuck]]
| [[Project Four Racing]]
|-
! 5
| align="center" | July 12
| {{flagicon|GBR}} [[British Grand Prix]]
| [[Brands Hatch]]
| {{flagicon|ARG}} [[Carlos Reutemann]]
| [[BMW Motorsport]]
|-
! 6
| align="center" | August 9
| {{flagicon|DEU}} [[German Grand Prix]]
| [[Hockenheimring]]
| {{flagicon|FRA}} [[Didier Pironi]]
| [[BMW Motorsport]]
|-
! 7
| align="center" | August 16
| {{flagicon|AUT}} [[Austrian Grand Prix]]
| [[Österreichring]]
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
|-
! 8
| align="center" | August 31
| {{flagicon|NED}} [[Dutch Grand Prix]]
| [[Circuit Zandvoort]]
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
|-
! 9
| align="center" | September 13
| {{flagicon|ITA}} [[Italian Grand Prix]]
| [[Autodromo Enzo e Dino Ferrari|Autodromo Dino Ferrari]]
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
|}

====Championship standings====
The following are the ten highest finishing drivers in the points standings.<ref name="Standings" />  [[Nelson Piquet]] won the championship, aided by three consecutive wins to finish the year.

{| class="wikitable" style="font-size: 90%;"
|- valign="top"
! Pos.
! Driver
! Team
! Points
|-
! 1
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[BMW Motorsport]]
! 90
|-
! 2
| {{flagicon|AUS}} [[Alan Jones (Formula 1)|Alan Jones]]
| [[BMW Motorsport]]
! 77
|-
! 3
| {{flagicon|DEU}} [[Hans-Joachim Stuck]]
| [[Project Four Racing]]
! 71
|-
! 4
| {{flagicon|NED}} [[Jan Lammers]]
| [[BMW Holland]]
! 69
|-
! 5
| {{flagicon|ARG}} [[Carlos Reutemann]]
| [[BMW Motorsport]]
! 64
|-
! 6
| {{flagicon|LIE}} [[Manfred Schurti]]
| [[Cassani Racing]]
! 48
|-
! 7
| {{flagicon|DEU}} [[Hans Heyer]]
| [[GS Tuning]]
! 41
|-
! 8=
| {{flagicon|FRA}} [[Jacques Laffite]]
| [[BMW Motorsport]]
! 37
|-
! 8=
| {{flagicon|SUI}} [[Marc Surer]]
| [[Sauber]]
! 37
|-
! 10
| {{flagicon|FRA}} [[Didier Pironi]]
| [[BMW Motorsport]]
! 34
|}

==Afterwards==
At the start of the 1980 season, BMW announced their intention to enter Formula One as an engine supplier for [[Brabham]].<ref name="BMW F1">{{cite web|url=http://bmw-motorsport.com/ms/en/fascination/history/f1/overview/index.html |title=F1 World Championship after 630 days |publisher=[[BMW Motorsport]] |accessdate=2008-07-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20080407221610/http://bmw-motorsport.com:80/ms/en/fascination/history/f1/overview/index.html |archivedate=2008-04-07 |df= }}</ref>  The deal came in part due to [[Bernie Ecclestone]], then head of Brabham, as well as the head of [[BS Fabrications]], who had built several of the Procar M1s.  BMW planned to enter the series in 1982, and [[BMW Motorsport]] was tasked with concentrating their efforts on the new Formula One engines instead of the Procar series.<ref name="BMW F1" />
[[File:1980-05-24 Nelson Piquet im BMW M1, Nürburgring Südkehre.jpg|thumb|right|200px|A Group 4 M1 competing in the [[1000 km Nürburgring|1000&nbsp;Kilometres of Nürburgring]].  Procar veterans [[Hans-Joachim Stuck]] and [[Nelson Piquet]] shared the car.]]
That same year, BMW officially met FISA's requirements by having built approximately 400 cars.  The M1 was therefore homologated for Group 4 on December 1, 1980, allowing BMW to enter the Championship for Makes in 1981.<ref name="Group 4">{{cite web|url=http://www.qv500.com/bmwm1p5.php |title=BMW (E26) M1 Part 5: Group 4 World Manufacturers Championship |publisher=QV500 |accessdate=2008-07-25 |archiveurl=https://web.archive.org/web/20080803213910/http://www.qv500.com/bmwm1p5.php |archivedate=2008-08-03 |deadurl=no |df= }}</ref>  As BMW shifted towards Formula One, the company ended their plans to enter the Group 5 category after the construction of only two race cars.<ref name="Group 5">{{cite web|url=http://www.qv500.com/bmwm1p6.php |title=BMW (E26) M1 Part 6: M1 Group 5 'March' |publisher=QV500 |accessdate=2008-07-25 |archiveurl=https://web.archive.org/web/20080803213551/http://www.qv500.com/bmwm1p6.php |archivedate=2008-08-03 |deadurl=no |df= }}</ref>  With BMW no longer needing to quickly build race cars, and with BMW Motorsport shifting to Formula One work, the series was not held in 1981.  The M1s used in the Procar series were sold to various customers for use in the World Championship, as well as smaller series such as the [[Deutsche Rennsport Meisterschaft]] and [[IMSA GT Championship|Camel GT Championship]].<ref name="Group 4" />

In 1988, Motor Racing Developments, the owners of the Brabham Formula One Team, were sold to [[Alfa Romeo]].  Wishing to revive the Procar series, Brabham and Alfa Romeo developed a prototype racing car using a mid-mounted Formula One-based [[V10 engine]], covered in a bodywork silhouette mimicking the [[Alfa Romeo 164]].<ref>[[Paul Frère|Frère, Paul]]. [[Road & Track]]. New York: Aug. 1988 issue. Vol. 39, Iss. 12;  pg. 96</ref> Alfa Romeo intended to use identical copies of the car for a resurrected Procar series, but the plans never came to fruition and only the prototype was built.<ref name="164 Procar">{{cite web | url = http://www.ultimatecarpage.com/car/1205/Alfa-Romeo-164-ProCar.html | author = Wouter Melissen | title = 1988 Alfa Romeo 164 ProCar | publisher = Ultimate Car Pages | date = 2004-01-12 | accessdate = 2008-07-25}}</ref>

===Revival===
On June 30, 2008, BMW announced plans to revive the Procar series in an exhibition event at the [[2008 German Grand Prix]] at the [[Hockenheimring]].  The races involved ten original M1 Procars driven by several drivers who had participated in the original series as well as modern drivers.<ref name="Revival" />  Each car included a passenger seat for a guest.<ref name="Revival Speedhunters">{{cite web|url=http://speedhunters.com/archive/2008/07/20/event-gt-gt-bmw-m1-30th-anniversary-race.aspx |title=BMW M1 30th ANNIVERSARY RACE |publisher=Speedhunters |date=2008-07-20 |accessdate=2008-07-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20110716114730/http://speedhunters.com/archive/2008/07/20/event-gt-gt-bmw-m1-30th-anniversary-race.aspx |archivedate=2011-07-16 |df= }}</ref>  The line-up included former competitors [[Christian Danner]], [[Harald Grohs]], [[Niki Lauda]], [[Dieter Quester]], and [[Marc Surer]].  Former BMW driver [[Prince Leopold of Bavaria (b. 1943)|Prince Leopold of Bavaria]], [[BMW Sauber]] test driver [[Christian Klien]], and a current M1 Procar owner, Marco Wagner, also competed.  The BMW M1 [[Art Car]] designed by [[Andy Warhol]] was involved, driven by series founder Jochen Neerpasch and with fellow [[Art Car]] artist [[Frank Stella]] as his passenger.<ref name="Revival Results">{{cite web | url = http://www.formula1.com/news/headlines/2008/7/8115.html | title = Lauda wins first BMW M1 Procar Revival race | publisher = [[Formula One Management]] | date = 2008-07-19 | accessdate = 2008-07-25| archiveurl= https://web.archive.org/web/20080724120443/http://www.formula1.com/news/headlines/2008/7/8115.html| archivedate= 24 July 2008 <!--DASHBot-->| deadurl= no}}</ref>

Lauda won the first race held on Saturday, July 19,<ref name="Revival Results" /> while Neerpasch was able to win the race held on Sunday, July 20, the morning of the Grand Prix.<ref name="Revival Results 2">{{cite web | url = http://www.motorsport.com/photos/popup.asp?N=46&I=f1/2008/ger/f1-2008-ger-xp-0941.jpg | title = Race 2 Finish | publisher = Motorsport.com | date = 2008-07-20 | accessdate = 2008-07-26}}</ref>

==See also==
* [[BMW in motorsport]]

==References==
{{reflist|2}}

==External links==
{{commons category|BMW M1 in competition}}
* [http://www.bmw-sauber-f1.com/en/#/interactive/m1-revival/ BMW Sauber F1 Team]&nbsp;– BMW M1 Procar Revival

{{BMW}}
{{good article}}

{{DEFAULTSORT:Bmw M1 Procar Championship}}
[[Category:BMW in motorsport|M1 Procar Championship]]
[[Category:One-make series]]
[[Category:Recurring sporting events established in 1979]]
[[Category:Recurring events disestablished in 1980]]
[[Category:Defunct auto racing series]]