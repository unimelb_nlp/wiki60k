{{Infobox journal
| title = Africa
| cover =
| discipline = [[African studies]]
| editor = [[Karin Barber]], David Pratten
| publisher = [[Cambridge University Press]]
| country =
| abbreviation = Africa
| history = 1928–present
<ref name="Editorial">{{cite journal|last=Barber|first=Karin|year=2008|title=Editorial|journal=Africa|volume=78|issue=3|doi=10.3366/E000197200800020X|quote=In his inaugural editorial in Africa in 1928....|url=http://www.internationalafricaninstitute.org/downloads/Africa%20editorial1.pdf|accessdate=30 October 2016}}</ref><ref name="IAI">{{cite web|url=http://www.internationalafricaninstitute.org/journal.html|title=Africa Journal|date=April 26, 2013|publisher=IAI|accessdate=26 June 2013}}</ref>
| frequency = Quarterly
| openaccess    = 
| license       = 
| impact = 1.017<ref name="IAI"/>
| impact-year = 2015
| website = http://journals.cambridge.org/action/displayJournal?jid=AFR
| link1 = http://www.internationalafricaninstitute.org
| link1-name = International African Institute homepage
| ISSN = 0001-9720
| eISSN = 1750-0184
| JSTOR = 00019720
| OCLC = 43039811
| LCCN = 29010790
}}
'''''Africa''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] published by [[Cambridge University Press]] on behalf of the [[International African Institute]]. The journal takes an [[interdisciplinarity|interdisciplinary]] approach considering the [[humanities]], [[social sciences]], and [[environmental studies]]s in [[Africa]]. Every year there is a special issue treating a specific theme.

''Africa'' is currently edited by Wale Adebanwi, Deborah James and David Pratten.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Modern Language Association|MLA International Bibliography]], the [[European Reference Index of Research Journals in the Humanities]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2009 [[impact factor]] of 0.592, ranking it 37th out of 68 journals in the category "[[Anthropology]]" and 14th out of 44 in the category [[Area studies|Area Studies]].<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2011-06-23| archiveurl= https://web.archive.org/web/20110713061611/http://isiwebofknowledge.com/| archivedate= 13 July 2011 <!--DASHBot-->| deadurl= no}}</ref> By 2011 the figures were an impact factor of 0.604, 11th out of 66 Area  Studies journals and 42nd out of 79 journals in the Anthropology category. In 2012 the Impact Factor was 0.855 ranking the journal in the top ten of the Area Studies category. Africa remains the top  anthropology/[[ethnography]] journal in Area Studies and the 3rd [[African studies]] journal in the same category.<ref name="IAI"/>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=AFR}}
* [http://muse.jhu.edu/journals/africa_the_journal_of_the_international_african_institute/ ''Africa''] at [[Project MUSE]]

{{DEFAULTSORT:Africa (Journal)}}
[[Category:African studies journals]]
[[Category:Publications established in 1928]]
[[Category:Cambridge University Press academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Academic journals associated with non-profit organizations]]

{{Area-studies-journal-stub}}